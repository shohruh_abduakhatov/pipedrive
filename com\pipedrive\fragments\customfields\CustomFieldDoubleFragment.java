package com.pipedrive.fragments.customfields;

import android.support.annotation.NonNull;
import android.text.method.ScrollingMovementMethod;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.views.common.DecimalEditText;

public class CustomFieldDoubleFragment extends CustomFieldEditTextFragment<DecimalEditText> {
    Integer getLayoutResId() {
        return Integer.valueOf(R.layout.fragment_custom_field_double);
    }

    void initialSetup(@NonNull String customFieldValue) {
        ((DecimalEditText) this.mEditText).setMovementMethod(new ScrollingMovementMethod());
        ((DecimalEditText) this.mEditText).setDecimalValueWithoutTailingZeroesFromText(customFieldValue);
    }

    void initialSetup(@NonNull Session session) {
        ((DecimalEditText) this.mEditText).init(session);
    }
}
