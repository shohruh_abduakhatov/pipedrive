package com.pipedrive.util.formatter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.datetime.LocaleHelper;
import com.pipedrive.model.Currency;
import com.pipedrive.model.Deal;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.model.products.Price;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class MonetaryFormatter extends BaseFormatter {
    public MonetaryFormatter(@NonNull Session session) {
        super(session);
    }

    @NonNull
    NumberFormat instantiateNumberFormat(@NonNull Locale locale) {
        NumberFormat numberFormat = DecimalFormat.getCurrencyInstance(locale);
        if (numberFormat instanceof DecimalFormat) {
            String newNegativePrefix = stripBrackets(((DecimalFormat) numberFormat).getNegativePrefix());
            if (!newNegativePrefix.contains("-")) {
                newNegativePrefix = "-" + newNegativePrefix;
            }
            String newNegativeSuffix = stripBrackets(((DecimalFormat) numberFormat).getNegativeSuffix());
            ((DecimalFormat) numberFormat).setNegativePrefix(newNegativePrefix);
            ((DecimalFormat) numberFormat).setNegativeSuffix(newNegativeSuffix);
        }
        return numberFormat;
    }

    @NonNull
    private String stripBrackets(@NonNull String string) {
        return string.replaceAll("\\(", "").replaceAll("\\)", "");
    }

    @NonNull
    private String getCurrencySymbol(@Nullable Currency currency) {
        if (currency == null) {
            return "";
        }
        if (currency.getSymbol() != null) {
            return currency.getSymbol();
        }
        return currency.getCode();
    }

    @NonNull
    protected String format(@NonNull Double value, @Nullable Currency currency) {
        String formattedValueWithWrongCurrencySymbol = super.format(value, currency);
        NumberFormat numberFormat = getNumberFormatForCurrency(currency);
        return formattedValueWithWrongCurrencySymbol.replace(numberFormat.getCurrency().getSymbol(LocaleHelper.getAppLocaleForNumbersDatesFormatting(getSession())), getCurrencySymbol(currency));
    }

    @NonNull
    public String formatDealValue(@NonNull Deal deal) {
        return format(Double.valueOf(deal.getValue()), getCurrencyByCode(deal.getCurrencyCode()));
    }

    @NonNull
    public String formatMonetaryCustomField(CustomField customField) {
        double monetaryValue = 0.0d;
        try {
            monetaryValue = new DecimalFormatter(getSession()).getNumberFormatForDefaultCurrency().parse(customField.getTempValue()).doubleValue();
        } catch (ParseException e) {
        }
        return format(Double.valueOf(monetaryValue), getCurrencyByCode(customField.getSecondaryTempValue()));
    }

    @NonNull
    public String formatDealProductPrice(@NonNull DealProduct dealProduct) {
        return format(Double.valueOf(dealProduct.getPrice().getValue().doubleValue()), dealProduct.getPrice().getCurrency());
    }

    @NonNull
    public String formatDealProductSum(@NonNull DealProduct dealProduct) {
        return format(Double.valueOf(dealProduct.getSum().doubleValue()), dealProduct.getPrice().getCurrency());
    }

    @NonNull
    public String format(@NonNull Double value, @Nullable String currencyCode) {
        return format(value, getCurrencyByCode(currencyCode));
    }

    @NonNull
    public String formatDealSum(@NonNull Float dealSumValue) {
        return format(Double.valueOf(dealSumValue.doubleValue()), getCurrencyByCode(getSession().getUserSettingsDefaultCurrencyCode()));
    }

    @NonNull
    public String formatPrice(@NonNull Price price) {
        return format(Double.valueOf(price.getValue().doubleValue()), price.getCurrency());
    }
}
