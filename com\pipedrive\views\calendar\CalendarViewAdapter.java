package com.pipedrive.views.calendar;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import com.pipedrive.views.calendar.recyclerview.CalendarRecyclerViewAdapter;
import java.util.Calendar;

abstract class CalendarViewAdapter<VH extends ViewHolder> extends CalendarRecyclerViewAdapter<VH> {
    public abstract void setInternalListener(@Nullable OnDateChangedInternalListener onDateChangedInternalListener);

    public abstract void setSelectedDate(@Nullable Calendar calendar);

    CalendarViewAdapter() {
    }
}
