package rx;

import rx.Scheduler.Worker;
import rx.functions.Action0;
import rx.subscriptions.Subscriptions;

class Single$18 implements Single$OnSubscribe<T> {
    final /* synthetic */ Single this$0;
    final /* synthetic */ Scheduler val$scheduler;

    Single$18(Single single, Scheduler scheduler) {
        this.this$0 = single;
        this.val$scheduler = scheduler;
    }

    public void call(final SingleSubscriber<? super T> t) {
        final SingleSubscriber<T> single = new SingleSubscriber<T>() {
            public void onSuccess(T value) {
                t.onSuccess(value);
            }

            public void onError(Throwable error) {
                t.onError(error);
            }
        };
        t.add(Subscriptions.create(new Action0() {
            public void call() {
                final Worker w = Single$18.this.val$scheduler.createWorker();
                w.schedule(new Action0() {
                    public void call() {
                        try {
                            single.unsubscribe();
                        } finally {
                            w.unsubscribe();
                        }
                    }
                });
            }
        }));
        this.this$0.subscribe(single);
    }
}
