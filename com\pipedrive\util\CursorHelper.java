package com.pipedrive.util;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.pipedrive.datasource.CurrenciesDataSource;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.model.Currency;
import com.pipedrive.model.products.Price;
import java.math.BigDecimal;
import rx.Observable;
import rx.Observable$OnSubscribe;

public enum CursorHelper {
    ;

    private static boolean isSane(@NonNull Cursor cursor, @NonNull String columnName) {
        return (cursor.isClosed() || TextUtils.isEmpty(columnName) || cursor.getPosition() == -1 || cursor.isNull(cursor.getColumnIndex(columnName))) ? false : true;
    }

    @Nullable
    public static Integer getInteger(@NonNull Cursor cursor, @NonNull String columnName) {
        if (isSane(cursor, columnName)) {
            return Integer.valueOf(cursor.getInt(cursor.getColumnIndex(columnName)));
        }
        return null;
    }

    @NonNull
    public static Integer getInteger(@NonNull Cursor cursor, @NonNull String columnName, int ifNotRetrieved) {
        Integer integer = getInteger(cursor, columnName);
        if (integer != null) {
            ifNotRetrieved = integer.intValue();
        }
        return Integer.valueOf(ifNotRetrieved);
    }

    @Nullable
    public static Boolean getBoolean(@NonNull Cursor cursor, @NonNull String columnName) {
        if (!isSane(cursor, columnName)) {
            return null;
        }
        return Boolean.valueOf(cursor.getInt(cursor.getColumnIndex(columnName)) != 0);
    }

    public static boolean getBoolean(@NonNull Cursor cursor, @NonNull String columnName, boolean ifNotRetrieved) {
        Boolean bool = getBoolean(cursor, columnName);
        return bool == null ? ifNotRetrieved : bool.booleanValue();
    }

    @Nullable
    public static Long getLong(@NonNull Cursor cursor, @NonNull String columnName) {
        if (isSane(cursor, columnName)) {
            return Long.valueOf(cursor.getLong(cursor.getColumnIndex(columnName)));
        }
        return null;
    }

    @Nullable
    public static String getString(@NonNull Cursor cursor, @NonNull String columnName) {
        if (isSane(cursor, columnName)) {
            return cursor.getString(cursor.getColumnIndex(columnName));
        }
        return null;
    }

    @Nullable
    public static byte[] getBlob(@NonNull Cursor cursor, @NonNull String columnName) {
        if (isSane(cursor, columnName)) {
            return cursor.getBlob(cursor.getColumnIndex(columnName));
        }
        return null;
    }

    @Nullable
    public static Float getFloat(@NonNull Cursor cursor, @NonNull String columnName) {
        if (isSane(cursor, columnName)) {
            return Float.valueOf(cursor.getFloat(cursor.getColumnIndex(columnName)));
        }
        return null;
    }

    @Nullable
    private static BigDecimal getBigDecimal(@NonNull Cursor cursor, @NonNull String columnName) {
        if (isSane(cursor, columnName)) {
            return BigDecimal.valueOf(cursor.getDouble(cursor.getColumnIndex(columnName)));
        }
        return null;
    }

    @Nullable
    public static Double getDouble(@NonNull Cursor cursor, @NonNull String columnName) {
        if (isSane(cursor, columnName)) {
            return Double.valueOf(cursor.getDouble(cursor.getColumnIndex(columnName)));
        }
        return null;
    }

    @Nullable
    public static PipedriveDateTime getPipedriveDateTimeFromColumnHeldInSeconds(@NonNull Cursor cursor, @NonNull String columnName) {
        if (isSane(cursor, columnName)) {
            return PipedriveDateTime.instanceFromUnixTimeRepresentation(Long.valueOf(cursor.getLong(cursor.getColumnIndex(columnName))));
        }
        return null;
    }

    @Nullable
    private static Currency getCurrency(@NonNull Cursor cursor, @NonNull String columnName, @NonNull SQLiteDatabase database) {
        Long currencySqlId = getLong(cursor, columnName);
        if (currencySqlId == null) {
            return null;
        }
        return (Currency) new CurrenciesDataSource(database).findBySqlId(currencySqlId.longValue());
    }

    @Nullable
    public static Price getPrice(@NonNull Cursor cursor, @NonNull String valueColumnName, @NonNull String currencySqlIdColumnName, @NonNull SQLiteDatabase database) {
        BigDecimal value = getBigDecimal(cursor, valueColumnName);
        Currency currency = getCurrency(cursor, currencySqlIdColumnName, database);
        if (value == null || currency == null) {
            return null;
        }
        return Price.create(value, currency);
    }

    public static Observable<Cursor> observeAllRows(@Nullable final Cursor cursor) {
        return Observable.create(new Observable$OnSubscribe<Cursor>() {
            public void call(rx.Subscriber<? super android.database.Cursor> r4) {
                /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Can't find block by offset: 0x0028 in list [B:15:0x0025]
	at jadx.core.utils.BlockUtils.getBlockByOffset(BlockUtils.java:42)
	at jadx.core.dex.instructions.IfNode.initBlocks(IfNode.java:60)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.initBlocksInIfNodes(BlockFinish.java:48)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.visit(BlockFinish.java:33)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                /*
                r3 = this;
                r1 = r1;	 Catch:{ Exception -> 0x000c, all -> 0x005e }
                if (r1 != 0) goto L_0x0029;	 Catch:{ Exception -> 0x000c, all -> 0x005e }
            L_0x0004:
                r1 = new java.lang.RuntimeException;	 Catch:{ Exception -> 0x000c, all -> 0x005e }
                r2 = "Cursor should not be null";	 Catch:{ Exception -> 0x000c, all -> 0x005e }
                r1.<init>(r2);	 Catch:{ Exception -> 0x000c, all -> 0x005e }
                throw r1;	 Catch:{ Exception -> 0x000c, all -> 0x005e }
            L_0x000c:
                r0 = move-exception;
                r1 = r4.isUnsubscribed();	 Catch:{ Exception -> 0x000c, all -> 0x005e }
                if (r1 != 0) goto L_0x0016;	 Catch:{ Exception -> 0x000c, all -> 0x005e }
            L_0x0013:
                r4.onError(r0);	 Catch:{ Exception -> 0x000c, all -> 0x005e }
            L_0x0016:
                r1 = r1;
                if (r1 == 0) goto L_0x001f;
            L_0x001a:
                r1 = r1;
                r1.close();
            L_0x001f:
                r1 = r4.isUnsubscribed();
                if (r1 != 0) goto L_0x0028;
            L_0x0025:
                r4.onCompleted();
            L_0x0028:
                return;
            L_0x0029:
                r1 = r1;	 Catch:{ Exception -> 0x000c, all -> 0x005e }
                r2 = -1;	 Catch:{ Exception -> 0x000c, all -> 0x005e }
                r1.moveToPosition(r2);	 Catch:{ Exception -> 0x000c, all -> 0x005e }
                r1 = r1;	 Catch:{ Exception -> 0x000c, all -> 0x005e }
                r1 = r1.isAfterLast();	 Catch:{ Exception -> 0x000c, all -> 0x005e }
                if (r1 == 0) goto L_0x004a;
            L_0x0037:
                r1 = r1;
                if (r1 == 0) goto L_0x0040;
            L_0x003b:
                r1 = r1;
                r1.close();
            L_0x0040:
                r1 = r4.isUnsubscribed();
                if (r1 != 0) goto L_0x0028;
            L_0x0046:
                r4.onCompleted();
                goto L_0x0028;
            L_0x004a:
                r1 = r1;	 Catch:{ Exception -> 0x000c, all -> 0x005e }
                r1 = r1.moveToNext();	 Catch:{ Exception -> 0x000c, all -> 0x005e }
                if (r1 == 0) goto L_0x0085;	 Catch:{ Exception -> 0x000c, all -> 0x005e }
            L_0x0052:
                r1 = r4.isUnsubscribed();	 Catch:{ Exception -> 0x000c, all -> 0x005e }
                if (r1 != 0) goto L_0x0072;	 Catch:{ Exception -> 0x000c, all -> 0x005e }
            L_0x0058:
                r1 = r1;	 Catch:{ Exception -> 0x000c, all -> 0x005e }
                r4.onNext(r1);	 Catch:{ Exception -> 0x000c, all -> 0x005e }
                goto L_0x004a;
            L_0x005e:
                r1 = move-exception;
                r2 = r1;
                if (r2 == 0) goto L_0x0068;
            L_0x0063:
                r2 = r1;
                r2.close();
            L_0x0068:
                r2 = r4.isUnsubscribed();
                if (r2 != 0) goto L_0x0071;
            L_0x006e:
                r4.onCompleted();
            L_0x0071:
                throw r1;
            L_0x0072:
                r1 = r1;
                if (r1 == 0) goto L_0x007b;
            L_0x0076:
                r1 = r1;
                r1.close();
            L_0x007b:
                r1 = r4.isUnsubscribed();
                if (r1 != 0) goto L_0x0028;
            L_0x0081:
                r4.onCompleted();
                goto L_0x0028;
            L_0x0085:
                r1 = r1;
                if (r1 == 0) goto L_0x008e;
            L_0x0089:
                r1 = r1;
                r1.close();
            L_0x008e:
                r1 = r4.isUnsubscribed();
                if (r1 != 0) goto L_0x0028;
            L_0x0094:
                r4.onCompleted();
                goto L_0x0028;
                */
                throw new UnsupportedOperationException("Method not decompiled: com.pipedrive.util.CursorHelper.1.call(rx.Subscriber):void");
            }
        });
    }

    public static void put(@Nullable Object valueOrNull, @NonNull ContentValues intoContentValues, @NonNull String storedInColumn) {
        if (valueOrNull == null) {
            intoContentValues.putNull(storedInColumn);
        } else if (valueOrNull instanceof String) {
            intoContentValues.put(storedInColumn, (String) valueOrNull);
        } else if (valueOrNull instanceof Integer) {
            intoContentValues.put(storedInColumn, (Integer) valueOrNull);
        } else if (valueOrNull instanceof Long) {
            intoContentValues.put(storedInColumn, (Long) valueOrNull);
        } else if (valueOrNull instanceof Boolean) {
            intoContentValues.put(storedInColumn, Integer.valueOf(((Boolean) valueOrNull).booleanValue() ? 1 : 0));
        } else if (valueOrNull instanceof BigDecimal) {
            intoContentValues.put(storedInColumn, Double.valueOf(((BigDecimal) valueOrNull).doubleValue()));
        } else if (valueOrNull instanceof Double) {
            intoContentValues.put(storedInColumn, (Double) valueOrNull);
        } else {
            throw new RuntimeException("Unsupported type. Please support it!");
        }
    }
}
