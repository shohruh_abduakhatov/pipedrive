package com.pipedrive.util.networking.entities;

import android.support.annotation.NonNull;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pipedrive.datetime.PipedriveDate;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.datetime.PipedriveDuration;
import com.pipedrive.model.Activity;
import java.util.Date;

public class ActivityEntity {
    private static final String JSON_PARAM_ACTIVE_FLAG = "active_flag";
    private static final String JSON_PARAM_ASSIGNED_USER_ID_FROM_API = "assigned_to_user_id";
    private static final String JSON_PARAM_ASSIGNED_USER_ID_TO_API = "user_id";
    private static final String JSON_PARAM_DEAL_ID = "deal_id";
    private static final String JSON_PARAM_DONE = "done";
    private static final String JSON_PARAM_DUE_DATE = "due_date";
    private static final String JSON_PARAM_DUE_TIME = "due_time";
    private static final String JSON_PARAM_DURATION = "duration";
    private static final String JSON_PARAM_ID = "id";
    private static final String JSON_PARAM_NOTE = "note";
    private static final String JSON_PARAM_ORG_ID = "org_id";
    private static final String JSON_PARAM_PERSON_ID = "person_id";
    private static final String JSON_PARAM_SUBJECT = "subject";
    private static final String JSON_PARAM_TYPE = "type";
    @SerializedName("active_flag")
    @Expose
    public Boolean activeFlag;
    @SerializedName("assigned_to_user_id")
    @Expose
    public Long assignedToUserId;
    @SerializedName("deal_id")
    @Expose
    public Long dealId;
    @SerializedName("done")
    @Expose
    public Boolean done;
    @SerializedName("due_date")
    @Expose
    public Date dueDate;
    @SerializedName("due_time")
    @Expose
    public Date dueTime;
    @SerializedName("duration")
    @Expose
    public Date duration;
    @SerializedName("note")
    @Expose
    public String note;
    @SerializedName("org_id")
    @Expose
    public Long orgId;
    @SerializedName("person_id")
    @Expose
    public Long personId;
    @SerializedName("id")
    @Expose
    public int pipedriveId;
    @SerializedName("subject")
    @Expose
    public String subject;
    @SerializedName("type")
    @Expose
    public String type;

    @NonNull
    public static JsonObject getJSON(@NonNull Activity activity) {
        String dueDateJsonString;
        String dueTimeJsonString;
        Number number;
        Number number2 = null;
        JsonObject activityJson = new JsonObject();
        if (activity.isExisting()) {
            activityJson.addProperty("id", Integer.valueOf(activity.getPipedriveId()));
        }
        activityJson.addProperty("subject", activity.getSubject());
        activityJson.addProperty(JSON_PARAM_DONE, Integer.valueOf(activity.isDone() ? 1 : 0));
        PipedriveDateTime activityStartAsDateTime = activity.getActivityStartAsDateTime();
        if (activityStartAsDateTime != null) {
            dueDateJsonString = activityStartAsDateTime.getPipedriveDate().getRepresentationForApiLongRepresentation();
            dueTimeJsonString = activityStartAsDateTime.getPipedriveTime().getRepresentationForApiShortRepresentation();
        } else {
            dueTimeJsonString = null;
            PipedriveDate activityStartAsDate = activity.getActivityStartAsDate();
            if (activityStartAsDate != null) {
                dueDateJsonString = activityStartAsDate.getRepresentationForApiLongRepresentation();
            } else {
                dueDateJsonString = null;
            }
        }
        activityJson.addProperty(JSON_PARAM_DUE_DATE, dueDateJsonString);
        activityJson.addProperty(JSON_PARAM_DUE_TIME, dueTimeJsonString);
        activityJson.addProperty("type", activity.getType() == null ? null : activity.getType().getKeyString());
        PipedriveDuration activityDuration = activity.getDuration();
        activityJson.addProperty(JSON_PARAM_DURATION, activityDuration == null ? null : activityDuration.getRepresentationInUtcForApiShortRepresentation());
        if (activity.getAssignedUserId() > 0) {
            activityJson.addProperty("user_id", Long.valueOf(activity.getAssignedUserId()));
        }
        activityJson.addProperty("note", activity.getNote());
        String str = JSON_PARAM_DEAL_ID;
        if (activity.getDeal() == null || !activity.getDeal().isExisting()) {
            number = null;
        } else {
            number = Integer.valueOf(activity.getDeal().getPipedriveId());
        }
        activityJson.addProperty(str, number);
        str = JSON_PARAM_PERSON_ID;
        if (activity.getPerson() == null || !activity.getPerson().isExisting()) {
            number = null;
        } else {
            number = Integer.valueOf(activity.getPerson().getPipedriveId());
        }
        activityJson.addProperty(str, number);
        String str2 = JSON_PARAM_ORG_ID;
        if (activity.getOrganization() != null && activity.getOrganization().isExisting()) {
            number2 = Integer.valueOf(activity.getOrganization().getPipedriveId());
        }
        activityJson.addProperty(str2, number2);
        activityJson.addProperty(JSON_PARAM_ACTIVE_FLAG, Boolean.valueOf(activity.isActive()));
        return activityJson;
    }

    public String toString() {
        return "ActivityEntity{pipedriveId=" + this.pipedriveId + ", subject='" + this.subject + '\'' + ", type='" + this.type + '\'' + ", dueDate=" + this.dueDate + ", dueTime=" + this.dueTime + ", duration=" + this.duration + ", note='" + this.note + '\'' + ", done=" + this.done + ", activeFlag=" + this.activeFlag + ", assignedToUserId=" + this.assignedToUserId + ", dealId=" + this.dealId + ", orgId=" + this.orgId + ", personId=" + this.personId + '}';
    }
}
