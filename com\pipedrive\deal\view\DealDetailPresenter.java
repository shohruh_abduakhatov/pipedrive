package com.pipedrive.deal.view;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.model.Deal;
import com.pipedrive.model.DealStage;
import com.pipedrive.model.DealStatus;
import com.pipedrive.presenter.Presenter;

abstract class DealDetailPresenter extends Presenter<DealDetailsView> {
    @Nullable
    protected Deal mDeal;

    abstract void retrieveDeal(@NonNull Long l);

    abstract void toggleActivityDoneStatus(long j, boolean z);

    abstract boolean updateDealStage(@Nullable DealStage dealStage);

    abstract void updateDealStatus(@NonNull DealStatus dealStatus);

    public DealDetailPresenter(@NonNull Session session) {
        super(session);
    }

    @Nullable
    public Deal getDeal() {
        return this.mDeal;
    }
}
