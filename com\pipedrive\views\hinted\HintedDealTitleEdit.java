package com.pipedrive.views.hinted;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;
import com.pipedrive.R;

public class HintedDealTitleEdit extends HintedEditText<EditText> {
    public HintedDealTitleEdit(Context context) {
        super(context);
    }

    public HintedDealTitleEdit(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HintedDealTitleEdit(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected int getMainViewLayoutResource() {
        return R.layout.view_hinted_title;
    }

    protected boolean hintValueShouldBeShared() {
        return false;
    }
}
