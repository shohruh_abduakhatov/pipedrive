package com.pipedrive.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.PipeSQLiteSharedHelper.TableCompany;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.settings.Company;
import com.pipedrive.util.CursorHelper;
import com.pipedrive.util.networking.entities.self.CompanyEntity;
import java.util.ArrayList;
import java.util.List;

public class CompaniesDataSource extends BaseDataSource<Company> {
    public CompaniesDataSource(@NonNull Session session) {
        super(session.getSharedSession().getDatabase());
    }

    @NonNull
    protected String getColumnNameForPipedriveId() {
        return TableCompany.COLUMN_COMPANY_PIPEDRIVE_ID;
    }

    @NonNull
    protected String getColumnNameForSqlId() {
        return PipeSQLiteHelper.COLUMN_ID;
    }

    @NonNull
    protected String getTableName() {
        return TableCompany.TABLE_NAME;
    }

    @NonNull
    protected String[] getAllColumns() {
        return new String[]{PipeSQLiteHelper.COLUMN_ID, TableCompany.COLUMN_COMPANY_PIPEDRIVE_ID, "c_name", TableCompany.COLUMN_NAME_API_TOKEN, TableCompany.COLUMN_NAME_USER_PIPEDRIVE_ID, TableCompany.COLUMN_NAME_DOMAIN_NAME};
    }

    @NonNull
    protected ContentValues getContentValues(@NonNull Company loginCompany) {
        ContentValues contentValues = super.getContentValues(loginCompany);
        String loginCompanyName = loginCompany.getName();
        if (loginCompanyName != null) {
            contentValues.put("c_name", loginCompanyName);
        }
        String loginCompanyApiToken = loginCompany.getApiToken();
        if (loginCompanyApiToken != null) {
            contentValues.put(TableCompany.COLUMN_NAME_API_TOKEN, loginCompanyApiToken);
        }
        Long loginCompanyUserPipedriveId = loginCompany.getUserPipedriveId();
        if (loginCompanyUserPipedriveId != null) {
            contentValues.put(TableCompany.COLUMN_NAME_USER_PIPEDRIVE_ID, loginCompanyUserPipedriveId);
        }
        String loginCompanyDomain = loginCompany.getDomain();
        if (loginCompanyDomain != null) {
            contentValues.put(TableCompany.COLUMN_NAME_DOMAIN_NAME, loginCompanyDomain);
        }
        return contentValues;
    }

    @Nullable
    protected Company deflateCursor(@NonNull Cursor cursor) {
        Company loginCompany = new Company();
        Long sqlId = CursorHelper.getLong(cursor, PipeSQLiteHelper.COLUMN_ID);
        if (sqlId != null) {
            loginCompany.setSqlId(sqlId.longValue());
        }
        Integer pdId = CursorHelper.getInteger(cursor, TableCompany.COLUMN_COMPANY_PIPEDRIVE_ID);
        if (pdId != null) {
            loginCompany.setPipedriveId(pdId.intValue());
        }
        loginCompany.setName(CursorHelper.getString(cursor, "c_name"));
        loginCompany.setApiToken(CursorHelper.getString(cursor, TableCompany.COLUMN_NAME_API_TOKEN));
        loginCompany.setUserPipedriveId(CursorHelper.getLong(cursor, TableCompany.COLUMN_NAME_USER_PIPEDRIVE_ID));
        loginCompany.setDomain(CursorHelper.getString(cursor, TableCompany.COLUMN_NAME_DOMAIN_NAME));
        return loginCompany;
    }

    public void replaceExistingCompanies(@NonNull List<Company> companies) {
        if (!companies.isEmpty()) {
            deleteAll();
            beginTransactionNonExclusive();
            for (Company company : companies) {
                createOrUpdate((BaseDatasourceEntity) company);
            }
            commit();
        }
    }

    public void updateExistingCompanies(@NonNull List<CompanyEntity> companyEntities) {
        ArrayList<Company> newCompaniesToSet = new ArrayList();
        for (CompanyEntity companyEntity : companyEntities) {
            if (companyEntity.mPipedriveId != null) {
                Company companyFoundByPipedriveId = (Company) findByPipedriveId(companyEntity.mPipedriveId.longValue());
                Company companyToSet = companyFoundByPipedriveId != null ? companyFoundByPipedriveId : new Company();
                companyToSet.setName(companyEntity.mName);
                companyToSet.setDomain(companyEntity.mDomain);
                if (companyEntity.mPipedriveId != null) {
                    companyToSet.setPipedriveId(companyEntity.mPipedriveId.intValue());
                }
                newCompaniesToSet.add(companyToSet);
            }
        }
        replaceExistingCompanies(newCompaniesToSet);
    }
}
