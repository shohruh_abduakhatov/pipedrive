package com.google.android.gms.analytics.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.analytics.zzi;
import com.google.android.gms.common.internal.zzaa;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class zzb extends zzd {
    private final zzl cI;

    public zzb(zzf com_google_android_gms_analytics_internal_zzf, zzg com_google_android_gms_analytics_internal_zzg) {
        super(com_google_android_gms_analytics_internal_zzf);
        zzaa.zzy(com_google_android_gms_analytics_internal_zzg);
        this.cI = com_google_android_gms_analytics_internal_zzg.zzj(com_google_android_gms_analytics_internal_zzf);
    }

    void onServiceConnected() {
        zzzx();
        this.cI.onServiceConnected();
    }

    public void setLocalDispatchPeriod(final int i) {
        zzacj();
        zzb("setLocalDispatchPeriod (sec)", Integer.valueOf(i));
        zzacc().zzg(new Runnable(this) {
            final /* synthetic */ zzb cK;

            public void run() {
                this.cK.cI.zzw(((long) i) * 1000);
            }
        });
    }

    public void start() {
        this.cI.start();
    }

    public long zza(zzh com_google_android_gms_analytics_internal_zzh) {
        zzacj();
        zzaa.zzy(com_google_android_gms_analytics_internal_zzh);
        zzzx();
        long zza = this.cI.zza(com_google_android_gms_analytics_internal_zzh, true);
        if (zza == 0) {
            this.cI.zzc(com_google_android_gms_analytics_internal_zzh);
        }
        return zza;
    }

    public void zza(final zzab com_google_android_gms_analytics_internal_zzab) {
        zzaa.zzy(com_google_android_gms_analytics_internal_zzab);
        zzacj();
        zzb("Hit delivery requested", com_google_android_gms_analytics_internal_zzab);
        zzacc().zzg(new Runnable(this) {
            final /* synthetic */ zzb cK;

            public void run() {
                this.cK.cI.zza(com_google_android_gms_analytics_internal_zzab);
            }
        });
    }

    public void zza(final zzw com_google_android_gms_analytics_internal_zzw) {
        zzacj();
        zzacc().zzg(new Runnable(this) {
            final /* synthetic */ zzb cK;

            public void run() {
                this.cK.cI.zzb(com_google_android_gms_analytics_internal_zzw);
            }
        });
    }

    public void zza(final String str, final Runnable runnable) {
        zzaa.zzh(str, "campaign param can't be empty");
        zzacc().zzg(new Runnable(this) {
            final /* synthetic */ zzb cK;

            public void run() {
                this.cK.cI.zzfa(str);
                if (runnable != null) {
                    runnable.run();
                }
            }
        });
    }

    public void zzabr() {
        zzacj();
        zzaby();
        zzacc().zzg(new Runnable(this) {
            final /* synthetic */ zzb cK;

            {
                this.cK = r1;
            }

            public void run() {
                this.cK.cI.zzabr();
            }
        });
    }

    public void zzabs() {
        zzacj();
        Context context = getContext();
        if (zzaj.zzat(context) && zzak.zzau(context)) {
            Intent intent = new Intent("com.google.android.gms.analytics.ANALYTICS_DISPATCH");
            intent.setComponent(new ComponentName(context, "com.google.android.gms.analytics.AnalyticsService"));
            context.startService(intent);
            return;
        }
        zza(null);
    }

    public boolean zzabt() {
        zzacj();
        try {
            zzacc().zzc(new Callable<Void>(this) {
                final /* synthetic */ zzb cK;

                {
                    this.cK = r1;
                }

                public /* synthetic */ Object call() throws Exception {
                    return zzdo();
                }

                public Void zzdo() throws Exception {
                    this.cK.cI.zzado();
                    return null;
                }
            }).get(4, TimeUnit.SECONDS);
            return true;
        } catch (InterruptedException e) {
            zzd("syncDispatchLocalHits interrupted", e);
            return false;
        } catch (ExecutionException e2) {
            zze("syncDispatchLocalHits failed", e2);
            return false;
        } catch (TimeoutException e3) {
            zzd("syncDispatchLocalHits timed out", e3);
            return false;
        }
    }

    public void zzabu() {
        zzacj();
        zzi.zzzx();
        this.cI.zzabu();
    }

    public void zzabv() {
        zzes("Radio powered up");
        zzabs();
    }

    void zzabw() {
        zzzx();
        this.cI.zzabw();
    }

    public void zzaw(final boolean z) {
        zza("Network connectivity status changed", Boolean.valueOf(z));
        zzacc().zzg(new Runnable(this) {
            final /* synthetic */ zzb cK;

            public void run() {
                this.cK.cI.zzaw(z);
            }
        });
    }

    protected void zzzy() {
        this.cI.initialize();
    }
}
