package com.pipedrive.store;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.changes.DealProductChanger;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.model.Deal;
import com.pipedrive.model.products.DealProduct;
import java.math.BigDecimal;

public class StoreDealProduct extends Store<DealProduct> {
    public StoreDealProduct(@NonNull Session session) {
        super(session);
    }

    boolean implementCreate(@NonNull DealProduct newDealProduct) {
        return newDealProduct.isMetadataOkForCreate() && new DealProductChanger(getSession()).create(newDealProduct);
    }

    boolean implementUpdate(@NonNull DealProduct updatedDealProduct) {
        return updatedDealProduct.isMetadataOkForUpdate() && new DealProductChanger(getSession()).update(updatedDealProduct);
    }

    void cleanupBeforeDelete(@NonNull DealProduct dealProductToDelete) {
        dealProductToDelete.delete();
    }

    boolean implementDelete(@NonNull DealProduct dealProductToDelete) {
        return dealProductToDelete.isMetadataOkForUpdate() && new DealProductChanger(getSession()).delete(dealProductToDelete);
    }

    void postProcessAfterCreate(@NonNull DealProduct entityStored) {
        super.postProcessAfterCreate(entityStored);
        updateDeal(entityStored);
    }

    void postProcessAfterDelete(@NonNull DealProduct deletedEntity) {
        super.postProcessAfterDelete(deletedEntity);
        updateDeal(deletedEntity);
    }

    void postProcessAfterUpdate(@NonNull DealProduct entityStored) {
        super.postProcessAfterUpdate(entityStored);
        updateDeal(entityStored);
    }

    private void updateDeal(@NonNull DealProduct dealProduct) {
        Long dealSqlId = dealProduct.getDealSqlId();
        if (dealSqlId != null) {
            DealsDataSource dealsDataSource = new DealsDataSource(getSession().getDatabase());
            Deal deal = (Deal) dealsDataSource.findBySqlId(dealSqlId.longValue());
            if (deal != null) {
                BigDecimal value = BigDecimal.ZERO;
                Double productCount = Double.valueOf(0.0d);
                if (deal.getDealProducts() != null) {
                    for (DealProduct item : deal.getDealProducts()) {
                        if (item.isActive().booleanValue()) {
                            value = value.add(item.getSum());
                            productCount = Double.valueOf(productCount.doubleValue() + item.getQuantity().doubleValue());
                        }
                    }
                }
                deal.setValue(value.doubleValue());
                deal.setProductCount(productCount);
                dealsDataSource.createOrUpdate(deal);
            }
        }
    }
}
