package com.google.android.gms.location.internal;

import android.os.DeadObjectException;
import android.os.IInterface;

public interface zzp<T extends IInterface> {
    void zzavf();

    T zzavg() throws DeadObjectException;
}
