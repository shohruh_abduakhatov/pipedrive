package com.pipedrive.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog.Builder;
import com.pipedrive.R;
import com.pipedrive.SplashActivity;
import com.pipedrive.SplashActivity.CompanySwitch;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.AnalyticsEvent;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.CompaniesDataSource;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.settings.Company;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.util.networking.ConnectionUtil;
import java.util.List;

public class ChangeCompanyDialogFragment extends BaseDialogFragment {
    private static final String TAG = ChangeCompanyDialogFragment.class.getSimpleName();

    public static void show(FragmentManager fragmentManager) {
        new ChangeCompanyDialogFragment().show(fragmentManager, TAG);
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        boolean haveActiveSessionToFillTheDialog = PipedriveApp.getSessionManager().hasActiveSession();
        Builder alertDialogBuilder = new Builder(getContext(), R.style.Theme.Pipedrive.Dialog.Alert).setTitle((int) R.string.btn_change_company);
        if (haveActiveSessionToFillTheDialog) {
            final Session activeSession = PipedriveApp.getActiveSession();
            if (activeSession == null) {
                return alertDialogBuilder.create();
            }
            final List<Company> companies = new CompaniesDataSource(activeSession).findAll();
            CharSequence[] companyNames = new String[companies.size()];
            int checkedItem = -1;
            long selectedCompanyID = activeSession.getSelectedCompanyID();
            for (int i = 0; i < companies.size(); i++) {
                Company company = (Company) companies.get(i);
                companyNames[i] = company.getName();
                Long companyPipedriveId = company.getPipedriveIdOrNull();
                if (companyPipedriveId != null && companyPipedriveId.longValue() == selectedCompanyID) {
                    checkedItem = i;
                }
            }
            final Context applicationContext = getContext().getApplicationContext();
            alertDialogBuilder.setSingleChoiceItems(companyNames, checkedItem, new OnClickListener() {
                public void onClick(@NonNull DialogInterface dialog, int which) {
                    Company newlySelectedCompany = (Company) companies.get(which);
                    if (ConnectionUtil.isNotConnected(applicationContext)) {
                        dialog.dismiss();
                        return;
                    }
                    CompanySwitch companySwitch = SplashActivity.relaunchApplicationForCompanySwitch(ChangeCompanyDialogFragment.this.getContext(), activeSession, newlySelectedCompany);
                    Analytics.sendEvent(applicationContext, AnalyticsEvent.COMPANY_CHANGED);
                    if (!(companySwitch == CompanySwitch.Success || companySwitch == CompanySwitch.CompanyAlreadyChosen)) {
                        LogJourno.reportEvent(EVENT.Authorization_companyDataCompanySwitchFailed, String.format("err:[%s]company[%s]", new Object[]{companySwitch, newlySelectedCompany}));
                        ViewUtil.showErrorToast(applicationContext, R.string.to_access_this_company_please_sign_out_and_in);
                    }
                    dialog.dismiss();
                }
            });
        }
        return alertDialogBuilder.create();
    }
}
