package com.zendesk.sdk.model.settings;

import android.support.annotation.Nullable;
import com.zendesk.sdk.model.access.AuthenticationType;
import java.io.Serializable;
import java.util.Date;

public class SdkSettings implements Serializable {
    private String authentication;
    private ContactUsSettings contactUs;
    private ConversationsSettings conversations;
    private HelpCenterSettings helpCenter;
    private RateMyAppSettings rma;
    private TicketFormSettings ticketForms;
    private Date updatedAt;

    @Nullable
    public RateMyAppSettings getRateMyAppSettings() {
        return this.rma;
    }

    @Nullable
    public ConversationsSettings getConversationsSettings() {
        return this.conversations;
    }

    @Nullable
    public HelpCenterSettings getHelpCenterSettings() {
        return this.helpCenter;
    }

    @Nullable
    public ContactUsSettings getContactUsSettings() {
        return this.contactUs;
    }

    @Nullable
    public Date getUpdatedAt() {
        return this.updatedAt == null ? null : new Date(this.updatedAt.getTime());
    }

    @Nullable
    public AuthenticationType getAuthentication() {
        return AuthenticationType.getAuthType(this.authentication);
    }

    @Nullable
    public TicketFormSettings getTicketFormSettings() {
        return this.ticketForms;
    }
}
