package com.zendesk.sdk.storage;

import com.zendesk.sdk.network.impl.StubPushRegistrationResponseStorage;

class StubStorageStore implements StorageStore {
    private final HelpCenterSessionCache helpCenterSessionCache = new StubHelpCenterSessionCache();
    private final IdentityStorage identityStorage = new StubIdentityStorage();
    private final PushRegistrationResponseStorage pushRegistrationResponseStorage = new StubPushRegistrationResponseStorage();
    private final RequestStorage requestStorage = new StubRequestStorage();
    private final SdkSettingsStorage sdkSettingsStorage = new StubSdkSettingsStorage();
    private final SdkStorage sdkStorage = new StubSdkStorage();

    StubStorageStore() {
    }

    public SdkStorage sdkStorage() {
        return this.sdkStorage;
    }

    public IdentityStorage identityStorage() {
        return this.identityStorage;
    }

    public RequestStorage requestStorage() {
        return this.requestStorage;
    }

    public SdkSettingsStorage sdkSettingsStorage() {
        return this.sdkSettingsStorage;
    }

    public HelpCenterSessionCache helpCenterSessionCache() {
        return this.helpCenterSessionCache;
    }

    public PushRegistrationResponseStorage pushStorage() {
        return this.pushRegistrationResponseStorage;
    }
}
