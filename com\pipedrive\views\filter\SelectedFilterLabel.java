package com.pipedrive.views.filter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.pipedrive.R;

public class SelectedFilterLabel extends FrameLayout {
    @NonNull
    @BindView(2131821002)
    TextView mSelectedFilterLabel;

    public SelectedFilterLabel(Context context) {
        this(context, null);
    }

    public SelectedFilterLabel(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SelectedFilterLabel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupLayout(context);
    }

    private void setupLayout(@NonNull Context context) {
        LayoutInflater.from(context).inflate(R.layout.layout_selected_filter_label, this, true);
        ButterKnife.bind(this);
    }

    public void setLabel(@Nullable String label) {
        if (label == null) {
            disableLabel();
            return;
        }
        enableLabel();
        this.mSelectedFilterLabel.setText(label);
    }

    public void disableLabel() {
        setVisibility(8);
    }

    private void enableLabel() {
        setVisibility(0);
    }
}
