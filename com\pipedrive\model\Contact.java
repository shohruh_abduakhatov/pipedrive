package com.pipedrive.model;

import android.os.Parcel;
import android.text.TextUtils;
import com.newrelic.agent.android.instrumentation.JSONArrayInstrumentation;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.interfaces.ContactOrgInterface;
import com.pipedrive.logging.Log;
import com.pipedrive.model.delta.ModelProperty;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class Contact<T> extends BaseCloneableDatasourceEntity<T> implements ContactOrgInterface {
    private String addTime;
    private String dropBoxAddress;
    private String firstLetter;
    private boolean isActive = true;
    private JSONArray mCustomFields = new JSONArray();
    @ModelProperty
    private String name;
    private String ownerName;
    @ModelProperty
    private int ownerPipedriveId = 0;
    private String updateTime;
    @ModelProperty
    private int visibleTo;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
        if (!TextUtils.isEmpty(name)) {
            setFirstLetter(name.substring(0, 1));
        }
    }

    public String getFirstLetter() {
        return this.firstLetter;
    }

    private void setFirstLetter(String firstLetter) {
        this.firstLetter = firstLetter;
    }

    public String getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getAddTime() {
        return this.addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public int getVisibleTo() {
        return this.visibleTo;
    }

    public void setVisibleTo(int visibleTo) {
        this.visibleTo = visibleTo;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public int getOwnerPipedriveId() {
        return this.ownerPipedriveId;
    }

    public void setOwnerPipedriveId(int ownerPipedriveId) {
        this.ownerPipedriveId = ownerPipedriveId;
    }

    public JSONArray getCustomFields() {
        return this.mCustomFields;
    }

    public void setCustomFields(JSONArray customFields) {
        this.mCustomFields = customFields;
    }

    public void setCustomFields(String customFields) {
        this.mCustomFields = null;
        if (customFields != null) {
            try {
                this.mCustomFields = JSONArrayInstrumentation.init(customFields);
            } catch (JSONException e) {
                Log.e(e);
            }
        }
    }

    public String getDropBoxAddress() {
        return this.dropBoxAddress;
    }

    public void setDropBoxAddress(String dropBoxAddress) {
        this.dropBoxAddress = dropBoxAddress;
    }

    public String getOwnerName() {
        return this.ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        String jSONArray;
        super.writeToParcel(dest, flags);
        dest.writeString(this.name);
        dest.writeString(this.firstLetter);
        dest.writeString(this.updateTime);
        dest.writeString(this.addTime);
        dest.writeInt(this.ownerPipedriveId);
        dest.writeInt(this.visibleTo);
        dest.writeInt(this.isActive ? 1 : 0);
        if (getCustomFields() != null) {
            JSONArray customFields = getCustomFields();
            jSONArray = !(customFields instanceof JSONArray) ? customFields.toString() : JSONArrayInstrumentation.toString(customFields);
        } else {
            jSONArray = null;
        }
        dest.writeString(jSONArray);
        dest.writeString(this.dropBoxAddress);
        dest.writeString(this.ownerName);
    }

    protected void readFromParcel(Parcel in) {
        boolean z = true;
        super.readFromParcel(in);
        this.name = in.readString();
        this.firstLetter = in.readString();
        this.updateTime = in.readString();
        this.addTime = in.readString();
        this.ownerPipedriveId = in.readInt();
        this.visibleTo = in.readInt();
        if (in.readInt() != 1) {
            z = false;
        }
        this.isActive = z;
        setCustomFields(in.readString());
        this.dropBoxAddress = in.readString();
        this.ownerName = in.readString();
    }

    public void addToCustomFieldArray(JSONObject obj) {
        if (this.mCustomFields == null) {
            this.mCustomFields = new JSONArray();
        }
        this.mCustomFields.put(obj);
    }

    protected final void appendJSONParams(JSONObject appendTo) throws JSONException {
        if (appendTo != null) {
            if (getPipedriveId() > 0) {
                appendTo.put(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, getPipedriveId());
            }
            appendTo.put("name", getName());
            if (getOwnerPipedriveId() > 0) {
                appendTo.put(PipeSQLiteHelper.COLUMN_OWNER_ID, getOwnerPipedriveId());
            }
            appendTo.put("active_flag", this.isActive);
            appendTo.put(PipeSQLiteHelper.COLUMN_VISIBLE_TO, Integer.toString(getVisibleTo()));
        }
    }
}
