package com.zendesk.sdk.deeplinking.actions;

public abstract class Action<Handler extends ActionHandler, Data extends ActionData> {
    private Data mActionData;
    private ActionType mActionType;

    public abstract boolean canHandleData(Data data);

    public abstract void execute(Handler handler, Data data);

    public Action(ActionType actionType, Data actionData) {
        this.mActionType = actionType;
        this.mActionData = actionData;
    }

    public ActionType getActionType() {
        return this.mActionType;
    }

    public Data getActionData() {
        return this.mActionData;
    }
}
