package com.pipedrive.nearby.map;

import android.support.annotation.NonNull;
import com.google.android.gms.maps.LocationSource;

public interface LocationSourceProvider {
    @NonNull
    LocationSource getLocationSource();
}
