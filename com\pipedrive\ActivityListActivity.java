package com.pipedrive;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import com.pipedrive.adapter.filter.FilterContentActivityAdapter;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.ScreensMapper;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.fragments.ActivityListFragment;
import com.pipedrive.inlineintro.InlineIntroPopup;
import com.pipedrive.model.ActivityType;
import com.pipedrive.model.User;
import com.pipedrive.util.TimeFilter;
import com.pipedrive.util.UsersUtil;
import com.pipedrive.util.activities.ActivityNetworkingUtil;
import com.pipedrive.views.filter.ActivityFilterListView;
import com.pipedrive.views.filter.FilterDrawerLayout;
import com.pipedrive.views.filter.FilterDrawerLayout.DrawerLayoutListener;
import com.pipedrive.views.navigation.NavigationToolbarButton;
import java.util.List;

public class ActivityListActivity extends NavigationActivity {
    @NonNull
    private FilterDrawerLayout filterDrawer;
    @NonNull
    private ActivityListFragment mActivityListFragment;
    private InlineIntroPopup mInlineIntroPopupForCalendarIcon;

    public static void startActivity(Activity activity, @NonNull Session session) {
        ContextCompat.startActivity(activity, getStartIntent(session, activity), null);
    }

    public static Intent getStartIntent(@NonNull Session session, @NonNull Context context) {
        return new Intent(context, session.isCalendarOpen() ? CalendarActivity.class : ActivityListActivity.class);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_list);
        this.mActivityListFragment = (ActivityListFragment) getSupportFragmentManager().findFragmentById(R.id.list_fragment);
        this.mInlineIntroPopupForCalendarIcon = new InlineIntroPopup(this, findViewById(16908290), 1, getSession(), 1);
    }

    public void setContentView(int layoutResID) {
        this.filterDrawer = new FilterDrawerLayout(this, getLayoutInflater(), getLayoutInflater().inflate(layoutResID, new FrameLayout(this)), R.layout.layout_activitylist_filter);
        super.setContentView(this.filterDrawer);
    }

    public void onResume() {
        super.onResume();
        if (PipedriveApp.getSessionManager().hasActiveSession()) {
            Session activeSession = PipedriveApp.getActiveSession();
            if (activeSession != null) {
                setupFilterDrawer(activeSession, this.filterDrawer);
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activities_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void setupFilterDrawer(@NonNull final Session session, @NonNull FilterDrawerLayout filterDrawer) {
        final ActivityFilterListView activityFilterListView = setupFilterContent(session, filterDrawer);
        if (activityFilterListView != null) {
            filterDrawer.setDrawerLayoutListener(new DrawerLayoutListener() {
                public boolean onClear() {
                    ActivityListActivity.this.filterCleared(session, activityFilterListView);
                    ActivityListActivity.this.displayPopupIntros();
                    return true;
                }

                public void onClose() {
                    ActivityListActivity.this.displayPopupIntros();
                    ActivityListActivity.this.onBackPressed();
                }

                public boolean onApply() {
                    ActivityListActivity.this.filterApplied(session, activityFilterListView);
                    ActivityListActivity.this.displayPopupIntros();
                    return true;
                }
            });
        }
    }

    private void filterCleared(@NonNull Session session, @NonNull ActivityFilterListView activityFilterListView) {
        activityFilterListView.setFirstActivityTypeIdPositionChecked();
        activityFilterListView.setFirstTimeFilterTimeIdPosition();
        activityFilterListView.setFirstUserPosition();
        filterApplied(session, activityFilterListView);
        supportInvalidateOptionsMenu();
    }

    private void filterApplied(@NonNull Session session, @NonNull ActivityFilterListView activityFilterListView) {
        Integer selectedActivityTypeId = activityFilterListView.getSelectedActivityTypeId();
        Integer selectedTimeFilterTimeId = activityFilterListView.getSelectedTimeFilterTimeId();
        Integer selectedUserId = activityFilterListView.getSelectedUserId();
        boolean readyToChangeFilter = (selectedActivityTypeId == null || selectedTimeFilterTimeId == null || selectedUserId == null) ? false : true;
        if (readyToChangeFilter) {
            this.mActivityListFragment.onFilterChanged(session, selectedUserId.intValue(), selectedActivityTypeId.intValue(), selectedTimeFilterTimeId.intValue());
            supportInvalidateOptionsMenu();
        }
    }

    @Nullable
    private ActivityFilterListView setupFilterContent(@NonNull Session session, @NonNull FilterDrawerLayout filterDrawer) {
        if (!(filterDrawer.getDrawerContentView() instanceof ActivityFilterListView)) {
            return null;
        }
        List<ActivityType> activityTypesToFilter = ActivityNetworkingUtil.loadActivityTypesList(session, true);
        activityTypesToFilter.add(0, new ActivityType(-1, 0, getString(R.string.all_types)));
        ActivityFilterListView filterContentListView = (ActivityFilterListView) filterDrawer.getDrawerContentView();
        filterContentListView.setAdapter(new FilterContentActivityAdapter(this, activityTypesToFilter, TimeFilter.TIME_IDS, getUsersList(session)));
        setFilterSelection(session, filterDrawer);
        return filterContentListView;
    }

    @NonNull
    private List<User> getUsersList(@NonNull Session session) {
        List<User> users = UsersUtil.getActiveCompanyUsersWithCurrentlyAuthenticatedUserAsFirst(session);
        if (!users.isEmpty()) {
            User authenticatedUser = (User) users.get(0);
            if (authenticatedUser.getName() != null) {
                authenticatedUser.setName(getString(R.string.you, new Object[]{authenticatedUser.getName()}));
            }
            if (users.size() > 1) {
                users.add(0, new User(-1, getResources().getString(R.string.special_user_name_everyone)));
            }
        }
        return users;
    }

    private void setFilterSelection(@NonNull Session session, @NonNull FilterDrawerLayout filterDrawer) {
        if (filterDrawer.getDrawerContentView() instanceof ActivityFilterListView) {
            ActivityFilterListView filterContentListView = (ActivityFilterListView) filterDrawer.getDrawerContentView();
            if (!filterContentListView.setActivityTypeId(session.getActivityFilterActivityTypeId(Integer.MIN_VALUE))) {
                filterContentListView.setFirstActivityTypeIdPositionChecked();
            }
            if (!filterContentListView.setTimeFilterTimeId(session.getActivityFilterTime(Integer.MIN_VALUE))) {
                filterContentListView.setFirstTimeFilterTimeIdPosition();
            }
            if (!filterContentListView.setUserId(session.getActivityFilterAssigneeId(Integer.MIN_VALUE))) {
                filterContentListView.setFirstUserPosition();
            }
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                onBackPressed();
                return true;
            case R.id.menu_filter_calendar:
                onCalendarMenuItemClicked();
                return true;
            case R.id.menu_filter_activities:
                hidePopupIntrosOnce();
                openFilter();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onCalendarMenuItemClicked() {
        this.mInlineIntroPopupForCalendarIcon.dismissAndDoNotShowAgain();
        getSession().setIsCalendarOpen(true);
        CalendarActivity.startActivity(this);
        finish();
        overrideTransitions(true);
    }

    public void onBackPressed() {
        boolean closeDrawerOnly = this.filterDrawer.isDrawerOpen() && PipedriveApp.getSessionManager().hasActiveSession();
        if (closeDrawerOnly) {
            this.filterDrawer.closeDrawer();
            Session activeSession = PipedriveApp.getActiveSession();
            if (activeSession != null) {
                setFilterSelection(activeSession, this.filterDrawer);
                return;
            }
            return;
        }
        super.onBackPressed();
    }

    private void openFilter() {
        this.filterDrawer.openDrawer();
        Analytics.hitScreen(ScreensMapper.SCREEN_NAME_ACTIVITY_LIST_FILTER, this);
    }

    public void onNavigationButtonClicked(NavigationToolbarButton button) {
        if (button != NavigationToolbarButton.ACTIVITIES) {
            super.onNavigationButtonClicked(button);
        }
    }

    private void showCalendarInlineIntroIfRequired() {
        final View calendarIcon = findViewById(R.id.menu_filter_calendar);
        boolean firstPopupMessageIsDismissed = nearbyItemIsDismissedForGood();
        if (calendarIcon != null && firstPopupMessageIsDismissed) {
            calendarIcon.post(new Runnable() {
                public void run() {
                    ActivityListActivity.this.mInlineIntroPopupForCalendarIcon.showWithTitleOnly(calendarIcon, ActivityListActivity.this.getResources().getString(R.string.try_the_new_Calendar_view));
                }
            });
        }
    }

    protected void onNearbyIntroDismissed() {
        showCalendarInlineIntroIfRequired();
    }

    public void displayPopupIntros() {
        super.displayPopupIntros();
        showCalendarInlineIntroIfRequired();
    }

    public void hidePopupIntrosOnce() {
        super.hidePopupIntrosOnce();
        this.mInlineIntroPopupForCalendarIcon.dismissOnce();
    }
}
