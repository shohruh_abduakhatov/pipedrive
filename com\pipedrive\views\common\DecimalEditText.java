package com.pipedrive.views.common;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import com.google.maps.android.heatmaps.WeightedLatLng;
import com.pipedrive.application.Session;
import com.pipedrive.util.formatter.DecimalFormatter;
import java.text.ParseException;

public class DecimalEditText extends EditText {
    @Nullable
    OnTextOrFocusChangeListener mOnTextOrFocusChangeListener;
    private Session mSession;

    public interface OnTextOrFocusChangeListener {
        void onAfterTextChanged(double d);

        void onFocusGained();

        void onFocusLost();
    }

    public DecimalEditText(Context context) {
        super(context);
    }

    public DecimalEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DecimalEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void init() {
        setFocusableInTouchMode(true);
        setInputType(12290);
        addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void afterTextChanged(Editable editable) {
                double newValue = DecimalEditText.this.getDoubleValueOrZeroFromText(editable.toString());
                if (DecimalEditText.this.mOnTextOrFocusChangeListener != null) {
                    DecimalEditText.this.mOnTextOrFocusChangeListener.onAfterTextChanged(newValue);
                }
            }
        });
        setOnFocusChangeListener(new OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (DecimalEditText.this.mOnTextOrFocusChangeListener != null) {
                    if (hasFocus) {
                        DecimalEditText.this.mOnTextOrFocusChangeListener.onFocusGained();
                    } else {
                        DecimalEditText.this.mOnTextOrFocusChangeListener.onFocusLost();
                    }
                }
            }
        });
        setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (DecimalEditText.this.mOnTextOrFocusChangeListener != null && event.getAction() == 0 && DecimalEditText.this.hasFocus()) {
                    DecimalEditText.this.mOnTextOrFocusChangeListener.onFocusGained();
                }
                return false;
            }
        });
    }

    double getDoubleValueOrZeroFromText(@NonNull String text) {
        String strippedText = text.replaceAll("[^0-9.,\\-]*", "");
        double newValue = 0.0d;
        if (!strippedText.isEmpty()) {
            try {
                newValue = new DecimalFormatter(this.mSession).getNumberFormatForDefaultCurrency().parse(strippedText).doubleValue();
            } catch (ParseException e) {
            }
        }
        return newValue;
    }

    public void setDecimalValueWithoutTailingZeroesFromText(@NonNull String textValue) {
        setDecimalValueWithoutTailingZeroes(getDoubleValueOrZeroFromText(textValue));
    }

    public void setDecimalValueWithoutTailingZeroes(double value) {
        if (value % WeightedLatLng.DEFAULT_INTENSITY == 0.0d) {
            setText(new DecimalFormatter(this.mSession).format(Double.valueOf(value)));
        } else {
            setText(String.valueOf(value));
        }
    }

    public boolean nothingIsEntered() {
        return getText().toString().isEmpty();
    }

    public void init(@NonNull Session session) {
        init(session, null);
    }

    public void init(@NonNull Session session, @Nullable OnTextOrFocusChangeListener onTextOrFocusChangeListener) {
        this.mSession = session;
        this.mOnTextOrFocusChangeListener = onTextOrFocusChangeListener;
        init();
    }
}
