package com.pipedrive.deal.view;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.model.Activity;
import com.pipedrive.model.Deal;
import com.pipedrive.model.DealStage;
import com.pipedrive.model.DealStatus;
import com.pipedrive.store.StoreActivity;
import com.pipedrive.store.StoreDeal;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished.TaskResult;
import com.pipedrive.tasks.flow.DownloadFlowTask;
import com.pipedrive.tasks.flow.DownloadFlowTask.DownloadDealFlowTask;
import com.pipedrive.util.flowfiles.FlowFileUploadNotificationManager;
import com.pipedrive.util.flowfiles.FlowFileUploadNotificationManager.Notification;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

public class DealDetailPresenterImpl extends DealDetailPresenter {
    @Nullable
    private Subscription mFlowFileUploadNotificationManagerSubscription;

    @Nullable
    public /* bridge */ /* synthetic */ Deal getDeal() {
        return super.getDeal();
    }

    public DealDetailPresenterImpl(@NonNull Session session) {
        super(session);
    }

    public synchronized void bindView(@NonNull DealDetailsView view) {
        super.bindView(view);
        this.mFlowFileUploadNotificationManagerSubscription = FlowFileUploadNotificationManager.INSTANCE.getFlowFileUploadNotificationBus().observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Notification>() {
            public void call(Notification notification) {
                if (DealDetailPresenterImpl.this.getView() != null) {
                    ((DealDetailsView) DealDetailPresenterImpl.this.getView()).onFlowUpdated();
                }
            }
        });
    }

    public synchronized void unbindView() {
        if (this.mFlowFileUploadNotificationManagerSubscription != null) {
            this.mFlowFileUploadNotificationManagerSubscription.unsubscribe();
            this.mFlowFileUploadNotificationManagerSubscription = null;
        }
        super.unbindView();
    }

    void retrieveDeal(@NonNull Long dealId) {
        Deal deal = (Deal) new DealsDataSource(getSession().getDatabase()).findBySqlId(dealId.longValue());
        if (deal != null) {
            if (!DownloadFlowTask.isExecuting(getSession(), DownloadDealFlowTask.class)) {
                new DownloadDealFlowTask(getSession(), new OnTaskFinished<Integer>() {
                    public void taskFinished(Session session, TaskResult taskResult, Integer... params) {
                        if (DealDetailPresenterImpl.this.getView() != null) {
                            ((DealDetailsView) DealDetailPresenterImpl.this.getView()).onFlowUpdated();
                            ((DealDetailsView) DealDetailPresenterImpl.this.getView()).hideProgress();
                        }
                    }
                }).executeOnAsyncTaskExecutor(this, new Integer[]{Integer.valueOf(deal.getPipedriveId())});
                if (getView() != null) {
                    ((DealDetailsView) getView()).showProgress();
                }
            }
            cacheDealAndUpdateTheView(deal);
        }
    }

    private void cacheDealAndUpdateTheView(@NonNull Deal deal) {
        this.mDeal = deal;
        if (getView() != null) {
            ((DealDetailsView) getView()).onDealRetrieved(this.mDeal);
        }
    }

    void updateDealStatus(@NonNull DealStatus dealStatus) {
        if (this.mDeal != null) {
            this.mDeal.setStatus(dealStatus);
            new StoreDeal(getSession()).update(this.mDeal);
            if (getView() != null) {
                ((DealDetailsView) getView()).onDealUpdated(this.mDeal);
            }
        }
    }

    boolean updateDealStage(@Nullable DealStage currentStage) {
        if (this.mDeal == null || currentStage == null || this.mDeal.getStage() == currentStage.getPipedriveId()) {
            return false;
        }
        this.mDeal.setStage(currentStage.getPipedriveId());
        return new StoreDeal(getSession()).update(this.mDeal);
    }

    void toggleActivityDoneStatus(long activitySqlId, boolean done) {
        Activity activity = (Activity) new ActivitiesDataSource(getSession()).findBySqlId(activitySqlId);
        if (activity != null) {
            activity.setDone(done);
            new StoreActivity(getSession()).update(activity);
            if (getView() != null) {
                ((DealDetailsView) getView()).onFlowUpdated();
            }
        }
    }
}
