package com.pipedrive.nearby.cards.cards;

import android.app.Activity;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.OnClick;
import com.google.android.gms.maps.model.LatLng;
import com.pipedrive.application.Session;
import com.pipedrive.call.CallSummaryListener;
import com.pipedrive.call.CallSummaryListener.CallSummaryRegistration;
import com.pipedrive.dialogs.communicationmediumlist.CommunicationMediumDialogController;
import com.pipedrive.dialogs.communicationmediumlist.CommunicationMediumListDialogFragment;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Person;
import com.pipedrive.model.contact.CommunicationMedium;
import com.pipedrive.nearby.model.NearbyItem;
import com.pipedrive.util.EmailHelper;
import com.pipedrive.util.PipedriveUtil;
import com.pipedrive.util.communicatiomedium.PersonCommunicationMediumUtil;
import java.util.List;

public abstract class SingleCardWithCommunicationButtons<NEARBY_ITEM extends NearbyItem<ENTITY>, ENTITY extends BaseDatasourceEntity> extends SingleCard<NEARBY_ITEM, ENTITY> implements CommunicationMediumDialogController<Person> {
    @BindView(2131821165)
    View callButton;
    @BindView(2131821166)
    View messageButton;

    @Nullable
    public abstract Person getEntity();

    @CallSuper
    public /* bridge */ /* synthetic */ void bind(@NonNull Session session, @NonNull NearbyItem nearbyItem, @NonNull LatLng latLng) {
        super.bind(session, nearbyItem, latLng);
    }

    public /* bridge */ /* synthetic */ void unbind() {
        super.unbind();
    }

    SingleCardWithCommunicationButtons(@NonNull ViewGroup parent, @LayoutRes @NonNull Integer layoutResId) {
        super(parent, layoutResId);
    }

    void bind(@NonNull Session session, @NonNull NEARBY_ITEM nearbyItem, @NonNull ENTITY entity, @NonNull LatLng currentLocation) {
        super.bind(session, nearbyItem, entity, currentLocation);
        setupCommunicationButtonsVisibility();
    }

    public void onCallRequested(@NonNull CommunicationMedium medium, @NonNull Person person) {
        CallSummaryListener.makeCallWithCallSummaryRegistration(this.session, this.itemView.getContext(), new CallSummaryRegistration(medium.getValue(), person.getSqlIdOrNull()));
    }

    public void onSmsRequested(@NonNull CommunicationMedium medium, @NonNull Person person) {
        PipedriveUtil.sendSms(this.itemView.getContext(), medium.getValue());
    }

    public void onEmailRequested(@NonNull CommunicationMedium medium, @NonNull Person person) {
        if (this.itemView.getContext() instanceof Activity) {
            EmailHelper.composeAndSendEmail((Activity) this.itemView.getContext(), medium.getValue(), person.getDropBoxAddress());
        }
    }

    private void setupCommunicationButtonsVisibility() {
        boolean personExists;
        int i;
        int i2 = 0;
        Person person = getEntity();
        if (person != null) {
            personExists = true;
        } else {
            personExists = false;
        }
        View view = this.callButton;
        if (personExists && person.hasPhones()) {
            i = 0;
        } else {
            i = 8;
        }
        view.setVisibility(i);
        View view2 = this.messageButton;
        if (!(personExists && (person.hasPhones() || person.hasEmails()))) {
            i2 = 8;
        }
        view2.setVisibility(i2);
    }

    @NonNull
    public List<? extends CommunicationMedium> getPhones(@NonNull Person person) {
        return new PersonCommunicationMediumUtil().getPhones(person);
    }

    @NonNull
    public List<? extends CommunicationMedium> getEmails(@NonNull Person person) {
        return new PersonCommunicationMediumUtil().getEmails(person);
    }

    @OnClick({2131821166})
    void onMessageButtonClicked() {
        if (getEntity() != null && (this.itemView.getContext() instanceof FragmentActivity)) {
            CommunicationMediumListDialogFragment.showSendMessageSelectorForCardWithCommunicationButtons(((FragmentActivity) this.itemView.getContext()).getSupportFragmentManager(), this);
        }
    }

    @OnClick({2131821165})
    void onCallButtonClicked() {
        if (getEntity() != null && getEntity().hasPhones()) {
            if (!getEntity().hasMultiplePhones()) {
                CallSummaryListener.makeCallWithCallSummaryRegistration(this.session, this.itemView.getContext(), new CallSummaryRegistration(getEntity().getPhone(), getEntity().getSqlIdOrNull()));
            } else if (this.itemView.getContext() instanceof FragmentActivity) {
                CommunicationMediumListDialogFragment.showCallSelectorForCardWithCommunicationButtons(((FragmentActivity) this.itemView.getContext()).getSupportFragmentManager(), this);
            }
        }
    }
}
