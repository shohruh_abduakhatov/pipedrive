package io.fabric.sdk.android.services.common;

import com.zendesk.service.HttpConstants;

public class ResponseParser {
    public static final int ResponseActionDiscard = 0;
    public static final int ResponseActionRetry = 1;

    public static int parse(int statusCode) {
        if (statusCode >= 200 && statusCode <= 299) {
            return 0;
        }
        if (statusCode >= HttpConstants.HTTP_MULT_CHOICE && statusCode <= 399) {
            return 1;
        }
        if (statusCode >= HttpConstants.HTTP_BAD_REQUEST && statusCode <= 499) {
            return 0;
        }
        if (statusCode >= HttpConstants.HTTP_INTERNAL_ERROR) {
            return 1;
        }
        return 1;
    }
}
