package com.google.android.gms.identity.intents;

import android.app.Activity;
import android.content.Context;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.HasOptions;
import com.google.android.gms.common.api.Api.zzf;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzvx;

public final class Address {
    public static final Api<AddressOptions> API = new Api("Address.API", hh, hg);
    static final zzf<zzvx> hg = new zzf();
    private static final com.google.android.gms.common.api.Api.zza<zzvx, AddressOptions> hh = new com.google.android.gms.common.api.Api.zza<zzvx, AddressOptions>() {
        public zzvx zza(Context context, Looper looper, com.google.android.gms.common.internal.zzf com_google_android_gms_common_internal_zzf, AddressOptions addressOptions, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            zzaa.zzb(context instanceof Activity, (Object) "An Activity must be used for Address APIs");
            if (addressOptions == null) {
                addressOptions = new AddressOptions();
            }
            return new zzvx((Activity) context, looper, com_google_android_gms_common_internal_zzf, addressOptions.theme, connectionCallbacks, onConnectionFailedListener);
        }
    };

    private static abstract class zza extends com.google.android.gms.internal.zzqo.zza<Status, zzvx> {
        public zza(GoogleApiClient googleApiClient) {
            super(Address.API, googleApiClient);
        }

        public Status zzb(Status status) {
            return status;
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    class AnonymousClass2 extends zza {
        final /* synthetic */ UserAddressRequest ahM;
        final /* synthetic */ int val$requestCode;

        AnonymousClass2(GoogleApiClient googleApiClient, UserAddressRequest userAddressRequest, int i) {
            this.ahM = userAddressRequest;
            this.val$requestCode = i;
            super(googleApiClient);
        }

        protected void zza(zzvx com_google_android_gms_internal_zzvx) throws RemoteException {
            com_google_android_gms_internal_zzvx.zza(this.ahM, this.val$requestCode);
            zzc(Status.xZ);
        }
    }

    public static final class AddressOptions implements HasOptions {
        public final int theme;

        public AddressOptions() {
            this.theme = 0;
        }

        public AddressOptions(int i) {
            this.theme = i;
        }
    }

    public static void requestUserAddress(GoogleApiClient googleApiClient, UserAddressRequest userAddressRequest, int i) {
        googleApiClient.zza(new AnonymousClass2(googleApiClient, userAddressRequest, i));
    }
}
