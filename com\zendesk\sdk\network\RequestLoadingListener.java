package com.zendesk.sdk.network;

import com.zendesk.service.ErrorResponse;

public interface RequestLoadingListener {
    void onLoadError(ErrorResponse errorResponse);

    void onLoadFinished(int i);

    void onLoadStarted();
}
