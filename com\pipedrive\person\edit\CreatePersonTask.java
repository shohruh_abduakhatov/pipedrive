package com.pipedrive.person.edit;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.model.Person;
import com.pipedrive.store.StorePerson;
import com.pipedrive.tasks.AsyncTask;

class CreatePersonTask extends AsyncTask<Person, Void, Boolean> {
    @Nullable
    private OnTaskFinished mOnTaskFinished;

    @MainThread
    interface OnTaskFinished {
        void onPersonCreated(boolean z);
    }

    public CreatePersonTask(@NonNull Session session, @Nullable OnTaskFinished onTaskFinished) {
        super(session);
        this.mOnTaskFinished = onTaskFinished;
    }

    protected Boolean doInBackground(Person... params) {
        return Boolean.valueOf(new StorePerson(getSession()).create(params[0]));
    }

    protected void onPostExecute(Boolean success) {
        if (this.mOnTaskFinished != null) {
            this.mOnTaskFinished.onPersonCreated(success.booleanValue());
        }
    }
}
