package com.pipedrive.pipeline;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.TextView;
import com.pipedrive.R;
import com.pipedrive.util.CompatUtil;
import com.pipedrive.util.CursorHelper;

class PipelineSpinnerAdapter extends CursorAdapter {
    public PipelineSpinnerAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(17367049, parent, false);
            CompatUtil.setTextAppearance((TextView) convertView, R.style.TextAppearance.Body.Dark);
        }
        ((TextView) convertView).setText(CursorHelper.getString((Cursor) getItem(position), "name"));
        return convertView;
    }

    @NonNull
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(17367048, parent, false);
        CompatUtil.setTextAppearance((TextView) view, R.style.TextAppearance.NavBarMediumDark);
        view.setPadding(0, view.getPaddingTop(), view.getPaddingRight(), view.getPaddingBottom());
        return view;
    }

    public void bindView(@NonNull View view, Context context, @NonNull Cursor cursor) {
        ((TextView) view).setText(CursorHelper.getString(cursor, "name"));
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        int selectedPosition = position;
        if (parent instanceof AdapterView) {
            selectedPosition = ((AdapterView) parent).getSelectedItemPosition();
        }
        return super.getView(selectedPosition, convertView, parent);
    }
}
