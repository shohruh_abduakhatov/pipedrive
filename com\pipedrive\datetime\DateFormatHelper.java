package com.pipedrive.datetime;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.text.format.DateUtils;
import android.text.format.Time;
import com.pipedrive.R;
import com.pipedrive.util.time.TimeManager;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public enum DateFormatHelper {
    ;
    
    public static final String DATE_FORMAT_LONG = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_NORMAL = "yyyy-MM-dd HH:mm";
    private static final String DATE_FORMAT_SHORT = "yyyy-MM-dd";
    public static final long ONE_DAY_IN_MILLIS = 0;
    public static final long ONE_HOUR_IN_MILLIS = 0;
    public static final long ONE_MINUTE_IN_MILLIS = 0;
    public static final long ONE_SECOND_IN_MILLIS = 0;
    private static final String TIME_FORMAT_LONG = "HH:mm:ss";
    private static final String TIME_FORMAT_NORMAL = "HH:mm";

    static {
        ONE_SECOND_IN_MILLIS = TimeUnit.SECONDS.toMillis(1);
        ONE_MINUTE_IN_MILLIS = TimeUnit.MINUTES.toMillis(1);
        ONE_HOUR_IN_MILLIS = TimeUnit.HOURS.toMillis(1);
        ONE_DAY_IN_MILLIS = TimeUnit.DAYS.toMillis(1);
    }

    private static CharSequence getRelativeTimeSpanString(long time, long now, long minResolution, int flags, Context c) {
        long count;
        int resId;
        Resources r = c.getResources();
        boolean past = now >= time;
        long duration = Math.abs(now - time);
        if (duration < 60000 && minResolution < 60000) {
            count = duration / 1000;
            if (past) {
                resId = R.plurals.num_seconds_ago;
            } else {
                resId = R.plurals.in_num_seconds;
            }
        } else if (duration < 3600000 && minResolution < 3600000) {
            count = duration / 60000;
            if (past) {
                resId = R.plurals.num_minutes_ago;
            } else {
                resId = R.plurals.in_num_minutes;
            }
        } else if (duration < 86400000 && minResolution < 86400000) {
            count = duration / 3600000;
            if (past) {
                resId = R.plurals.num_hours_ago;
            } else {
                resId = R.plurals.in_num_hours;
            }
        } else if (duration >= 604800000 || minResolution >= 604800000) {
            return DateUtils.formatDateRange(null, time, time, flags);
        } else {
            return getRelativeDayString(r, time, now);
        }
        return String.format(r.getQuantityString(resId, (int) count), new Object[]{Long.valueOf(count)});
    }

    private static String getRelativeDayString(Resources r, long day, long today) {
        Time startTime = new Time();
        startTime.set(day);
        int startDay = Time.getJulianDay(day, startTime.gmtoff);
        Time currentTime = new Time();
        currentTime.set(today);
        int days = Math.abs(Time.getJulianDay(today, currentTime.gmtoff) - startDay);
        boolean past = today > day;
        if (days == 1) {
            if (past) {
                return r.getString(R.string.yesterday);
            }
            return r.getString(R.string.tomorrow);
        } else if (days == 0) {
            return r.getString(R.string.today);
        } else {
            int resId;
            if (past) {
                resId = R.plurals.num_days_ago;
            } else {
                resId = R.plurals.in_num_days;
            }
            return String.format(r.getQuantityString(resId, days), new Object[]{Integer.valueOf(days)});
        }
    }

    public static String getTimePassedInHumanReadableForm(Context context, int placeholderStringResId, Date fromTime) {
        if (fromTime == null || fromTime.after(new Date(TimeManager.getInstance().currentTimeMillis().longValue()))) {
            return null;
        }
        String timeInHumanReadableFrom = String.valueOf(getRelativeTimeSpanString(fromTime.getTime(), TimeManager.getInstance().currentTimeMillis().longValue(), 1000, 0, context));
        if (placeholderStringResId <= 0) {
            return timeInHumanReadableFrom;
        }
        return context.getString(placeholderStringResId, new Object[]{timeInHumanReadableFrom});
    }

    private static SimpleDateFormat getUtcSimpleDateFormat(String formatString) {
        TimeZone timezoneUTC = TimeManager.getInstance().getUtcTimeZone();
        SimpleDateFormat format = new SimpleDateFormat(formatString);
        format.setTimeZone(timezoneUTC);
        return format;
    }

    public static SimpleDateFormat fullDateFormat2UTC() {
        return getUtcSimpleDateFormat(DATE_FORMAT_LONG);
    }

    public static SimpleDateFormat fullDateFormat3UTC() {
        return getUtcSimpleDateFormat(DATE_FORMAT_NORMAL);
    }

    public static SimpleDateFormat dateFormat2() {
        return new SimpleDateFormat(DATE_FORMAT_SHORT);
    }

    public static SimpleDateFormat dateFormat2UTC() {
        return getUtcSimpleDateFormat(DATE_FORMAT_SHORT);
    }

    public static SimpleDateFormat timeFormat2() {
        return new SimpleDateFormat(TIME_FORMAT_NORMAL);
    }

    public static SimpleDateFormat timeFormat2UTC() {
        return getUtcSimpleDateFormat(TIME_FORMAT_NORMAL);
    }

    public static SimpleDateFormat timeFormat4UTC() {
        return getUtcSimpleDateFormat(TIME_FORMAT_LONG);
    }

    public static String calculateAndFormatTimeSinceArgumentDate(Context ctx, Date date) {
        return getRelativeDayString(ctx.getResources(), date.getTime(), TimeManager.getInstance().currentTimeMillis().longValue());
    }

    public static SimpleDateFormat dateFormat6(@NonNull Locale locale) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE", locale);
        simpleDateFormat.setTimeZone(TimeManager.getInstance().getUtcTimeZone());
        return simpleDateFormat;
    }

    public static SimpleDateFormat monthInStandaloneFormat(@NonNull Locale locale) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("LLLL", locale);
        simpleDateFormat.setTimeZone(TimeManager.getInstance().getUtcTimeZone());
        return simpleDateFormat;
    }

    public static boolean isToday(Date date, boolean allDayEvent) {
        Calendar cal = TimeManager.getInstance().getLocalCalendar();
        cal.setTime(date);
        Calendar today = TimeManager.getInstance().getLocalCalendar();
        if (today.get(5) != cal.get(5) || today.get(2) != cal.get(2) || today.get(1) != cal.get(1)) {
            return false;
        }
        if (allDayEvent || today.get(11) < cal.get(11) || ((today.get(11) == cal.get(11) && today.get(12) < cal.get(12)) || (today.get(12) == cal.get(12) && today.get(13) <= cal.get(13)))) {
            return true;
        }
        return false;
    }

    public static boolean isTodayUtc(long dateMillis) {
        return isSameDayUtc(dateMillis, TimeManager.getInstance().getLocalCalendar());
    }

    public static boolean isSameDayUtc(long timeMillis, @NonNull Calendar calendarToCompare) {
        Calendar calendarFromMillis = TimeManager.getInstance().getUtcCalendar();
        calendarFromMillis.setTimeInMillis(timeMillis);
        if (calendarFromMillis.get(6) == calendarToCompare.get(6) && calendarFromMillis.get(1) == calendarToCompare.get(1)) {
            return true;
        }
        return false;
    }
}
