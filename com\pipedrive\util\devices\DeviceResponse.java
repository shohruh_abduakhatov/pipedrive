package com.pipedrive.util.devices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0011\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004R \u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/pipedrive/util/devices/DeviceResponse;", "", "deviceEntity", "Lcom/pipedrive/util/devices/DeviceEntity;", "(Lcom/pipedrive/util/devices/DeviceEntity;)V", "getDeviceEntity", "()Lcom/pipedrive/util/devices/DeviceEntity;", "setDeviceEntity", "pipedrive_prodRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: DeviceResponse.kt */
public final class DeviceResponse {
    @SerializedName("data")
    @Nullable
    @Expose
    private DeviceEntity deviceEntity;

    public DeviceResponse() {
        this(null, 1, null);
    }

    public DeviceResponse(@Nullable DeviceEntity deviceEntity) {
        this.deviceEntity = deviceEntity;
    }

    public /* synthetic */ DeviceResponse(DeviceEntity deviceEntity, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? (DeviceEntity) null : deviceEntity);
    }

    @Nullable
    public final DeviceEntity getDeviceEntity() {
        return this.deviceEntity;
    }

    public final void setDeviceEntity(@Nullable DeviceEntity <set-?>) {
        this.deviceEntity = <set-?>;
    }
}
