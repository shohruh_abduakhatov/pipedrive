package com.pipedrive.util.recents;

import com.pipedrive.model.ActivityType;
import com.pipedrive.model.Deal;
import com.pipedrive.model.DealStage;
import com.pipedrive.model.Filter;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.Pipeline;
import com.pipedrive.util.networking.entities.ActivityEntity;
import com.pipedrive.util.networking.entities.EmailMessageEntity;
import com.pipedrive.util.networking.entities.EmailThreadEntity;
import com.pipedrive.util.networking.entities.FlowFileEntity;
import com.pipedrive.util.networking.entities.NoteEntity;
import com.pipedrive.util.networking.entities.product.ProductEntity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class RecentsResponsePage extends RecentsResponse {
    static final int NEXT_PAGE_START_UNDEFINED = -1;
    final List<ActivityEntity> activities = new ArrayList();
    final List<Integer> activitiesToDelete = new ArrayList();
    final HashMap<Integer, ActivityType> activityTypesFromRecents = new HashMap();
    final List<Integer> contactsToDelete = new ArrayList();
    final List<DealStage> dealStages = new ArrayList();
    final List<Integer> dealStagesToDelete = new ArrayList();
    final List<Deal> deals = new ArrayList();
    final List<Integer> dealsToDelete = new ArrayList();
    final List<EmailMessageEntity> emailMessagesFromRecents = new ArrayList();
    final List<EmailThreadEntity> emailThreadsFromRecents = new ArrayList();
    final List<FlowFileEntity> filesFromRecents = new ArrayList();
    final HashMap<Integer, Filter> filtersFromRecents = new HashMap();
    String lastTimestampOnPage = null;
    int mNextPage = -1;
    final HashMap<Integer, NoteEntity> notesFromRecents = new HashMap();
    final List<Organization> organizations = new ArrayList();
    final List<Integer> organizationsToDelete = new ArrayList();
    final List<Person> persons = new ArrayList();
    final HashMap<Integer, Pipeline> pipelinesFromRecents = new HashMap();
    final List<ProductEntity> productEntitiesFromRecents = new ArrayList();

    RecentsResponsePage(String sinceTimestamp) {
        this.lastTimestampOnPage = sinceTimestamp;
    }

    protected void clear() {
        this.activities.clear();
        this.deals.clear();
        this.persons.clear();
        this.organizations.clear();
        this.dealStages.clear();
        this.activitiesToDelete.clear();
        this.dealsToDelete.clear();
        this.contactsToDelete.clear();
        this.organizationsToDelete.clear();
        this.dealStagesToDelete.clear();
        this.activityTypesFromRecents.clear();
        this.filtersFromRecents.clear();
        this.pipelinesFromRecents.clear();
        this.notesFromRecents.clear();
        this.filesFromRecents.clear();
        this.emailMessagesFromRecents.clear();
        this.emailThreadsFromRecents.clear();
        this.productEntitiesFromRecents.clear();
    }
}
