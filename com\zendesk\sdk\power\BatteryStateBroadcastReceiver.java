package com.zendesk.sdk.power;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.zendesk.logger.Logger;

public class BatteryStateBroadcastReceiver extends BroadcastReceiver {
    private static final String LOG_TAG = BatteryStateBroadcastReceiver.class.getSimpleName();

    public void onReceive(Context context, Intent intent) {
        Logger.d(LOG_TAG, "onReceive entry()", new Object[0]);
        if (intent == null) {
            Logger.w(LOG_TAG, "Cannot determine battery state", new Object[0]);
        } else {
            Logger.d(LOG_TAG, "Received a battery status update with action: " + intent.getAction(), new Object[0]);
            if ("android.intent.action.BATTERY_OKAY".equals(intent.getAction())) {
                Logger.i(LOG_TAG, "Battery state is OK.", new Object[0]);
                PowerConfig.getInstance(context).setBatteryOk(true);
            } else if ("android.intent.action.BATTERY_LOW".equals(intent.getAction())) {
                Logger.i(LOG_TAG, "Battery is low.", new Object[0]);
                PowerConfig.getInstance(context).setBatteryOk(false);
            }
        }
        Logger.d(LOG_TAG, "onReceive exit()", new Object[0]);
    }
}
