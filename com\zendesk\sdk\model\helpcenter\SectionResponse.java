package com.zendesk.sdk.model.helpcenter;

public class SectionResponse {
    private Section section;

    public Section getSection() {
        return this.section;
    }
}
