package com.pipedrive.datetime;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.pipedrive.gson.UtcDateTypeAdapter;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class PipedriveDuration {
    private final long durationInMillis;

    PipedriveDuration(long durationInMillis) {
        this.durationInMillis = durationInMillis;
    }

    @Nullable
    public static PipedriveDuration instanceFromDate(@Nullable Date date) {
        if (date == null) {
            return null;
        }
        return new PipedriveDuration(date.getTime());
    }

    @Nullable
    public static PipedriveDuration instanceFromUnixTimeRepresentation(@Nullable Long durationInUnixTime) {
        if (durationInUnixTime == null) {
            return null;
        }
        return new PipedriveDuration(TimeUnit.SECONDS.toMillis(durationInUnixTime.longValue()));
    }

    @Nullable
    public static PipedriveDuration instanceFormApiShortRepresentation(@Nullable String time) {
        if (time == null) {
            return null;
        }
        return instanceFromDate(UtcDateTypeAdapter.parseTime(time));
    }

    @NonNull
    public String getRepresentationInUtcForApiShortRepresentation() {
        long durationMinutesOnly = TimeUnit.SECONDS.toMinutes(getRepresentationInUnixTime()) - (TimeUnit.HOURS.toMinutes(1) * TimeUnit.SECONDS.toHours(getRepresentationInUnixTime()));
        return String.format(Locale.getDefault(), "%02d:%02d", new Object[]{Long.valueOf(TimeUnit.SECONDS.toHours(getRepresentationInUnixTime())), Long.valueOf(durationMinutesOnly)});
    }

    public long getRepresentationInUnixTime() {
        return TimeUnit.MILLISECONDS.toSeconds(toMillis());
    }

    public long toMillis() {
        return this.durationInMillis;
    }

    public float toSeconds() {
        return ((float) toMillis()) / 1000.0f;
    }

    public float toMinutes() {
        return toSeconds() / BitmapDescriptorFactory.HUE_YELLOW;
    }

    public float toHours() {
        return toMinutes() / BitmapDescriptorFactory.HUE_YELLOW;
    }
}
