package com.pipedrive.pipeline;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.util.CursorHelper;
import java.util.ArrayList;
import java.util.List;

class PipelinePagerAdapter extends FragmentStatePagerAdapter {
    @NonNull
    private List<StageInfo> mStages = new ArrayList();

    private class StageInfo {
        private int mStageId;
        private String mStageName;

        private StageInfo(int stageId, String stageName) {
            this.mStageId = stageId;
            this.mStageName = stageName;
        }
    }

    public PipelinePagerAdapter(@NonNull FragmentManager fm, @Nullable Cursor stagesCursor) {
        super(fm);
        if (stagesCursor != null) {
            try {
                stagesCursor.moveToPosition(-1);
                while (stagesCursor.moveToNext()) {
                    Integer stageId = CursorHelper.getInteger(stagesCursor, PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID);
                    String stageName = CursorHelper.getString(stagesCursor, "name");
                    if (stageId != null) {
                        this.mStages.add(new StageInfo(stageId.intValue(), stageName));
                    }
                }
            } finally {
                stagesCursor.close();
            }
        }
    }

    public int getCount() {
        return this.mStages.size();
    }

    public Fragment getItem(int position) {
        StageInfo stageInfo = (StageInfo) this.mStages.get(position);
        return PipelineDealListFragment.newInstance(stageInfo.mStageId, stageInfo.mStageName, position);
    }

    public int getStageIdForPosition(int position) {
        return ((StageInfo) this.mStages.get(position)).mStageId;
    }
}
