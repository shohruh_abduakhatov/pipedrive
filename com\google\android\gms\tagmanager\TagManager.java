package com.google.android.gms.tagmanager;

import android.annotation.TargetApi;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.RawRes;
import android.support.annotation.RequiresPermission;
import com.google.android.gms.common.api.PendingResult;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class TagManager {
    private static TagManager aHJ;
    private final DataLayer aDZ;
    private final zzt aGC;
    private final zza aHG;
    private final zzdb aHH;
    private final ConcurrentMap<String, zzo> aHI;
    private final Context mContext;

    public interface zza {
        zzp zza(Context context, TagManager tagManager, Looper looper, String str, int i, zzt com_google_android_gms_tagmanager_zzt);
    }

    TagManager(Context context, zza com_google_android_gms_tagmanager_TagManager_zza, DataLayer dataLayer, zzdb com_google_android_gms_tagmanager_zzdb) {
        if (context == null) {
            throw new NullPointerException("context cannot be null");
        }
        this.mContext = context.getApplicationContext();
        this.aHH = com_google_android_gms_tagmanager_zzdb;
        this.aHG = com_google_android_gms_tagmanager_TagManager_zza;
        this.aHI = new ConcurrentHashMap();
        this.aDZ = dataLayer;
        this.aDZ.zza(new zzb(this) {
            final /* synthetic */ TagManager aHK;

            {
                this.aHK = r1;
            }

            public void zzaz(Map<String, Object> map) {
                Object obj = map.get("event");
                if (obj != null) {
                    this.aHK.zzpt(obj.toString());
                }
            }
        });
        this.aDZ.zza(new zzd(this.mContext));
        this.aGC = new zzt();
        zzcgz();
        zzcha();
    }

    @RequiresPermission(allOf = {"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
    public static TagManager getInstance(Context context) {
        TagManager tagManager;
        synchronized (TagManager.class) {
            if (aHJ == null) {
                if (context == null) {
                    zzbo.e("TagManager.getInstance requires non-null context.");
                    throw new NullPointerException();
                }
                aHJ = new TagManager(context, new zza() {
                    public zzp zza(Context context, TagManager tagManager, Looper looper, String str, int i, zzt com_google_android_gms_tagmanager_zzt) {
                        return new zzp(context, tagManager, looper, str, i, com_google_android_gms_tagmanager_zzt);
                    }
                }, new DataLayer(new zzx(context)), zzdc.zzcgt());
            }
            tagManager = aHJ;
        }
        return tagManager;
    }

    @TargetApi(14)
    private void zzcgz() {
        if (VERSION.SDK_INT >= 14) {
            this.mContext.registerComponentCallbacks(new ComponentCallbacks2(this) {
                final /* synthetic */ TagManager aHK;

                {
                    this.aHK = r1;
                }

                public void onConfigurationChanged(Configuration configuration) {
                }

                public void onLowMemory() {
                }

                public void onTrimMemory(int i) {
                    if (i == 20) {
                        this.aHK.dispatch();
                    }
                }
            });
        }
    }

    private void zzcha() {
        zza.zzdw(this.mContext);
    }

    private void zzpt(String str) {
        for (zzo zzov : this.aHI.values()) {
            zzov.zzov(str);
        }
    }

    public void dispatch() {
        this.aHH.dispatch();
    }

    public DataLayer getDataLayer() {
        return this.aDZ;
    }

    public PendingResult<ContainerHolder> loadContainerDefaultOnly(String str, @RawRes int i) {
        PendingResult zza = this.aHG.zza(this.mContext, this, null, str, i, this.aGC);
        zza.zzced();
        return zza;
    }

    public PendingResult<ContainerHolder> loadContainerDefaultOnly(String str, @RawRes int i, Handler handler) {
        PendingResult zza = this.aHG.zza(this.mContext, this, handler.getLooper(), str, i, this.aGC);
        zza.zzced();
        return zza;
    }

    public PendingResult<ContainerHolder> loadContainerPreferFresh(String str, @RawRes int i) {
        PendingResult zza = this.aHG.zza(this.mContext, this, null, str, i, this.aGC);
        zza.zzcef();
        return zza;
    }

    public PendingResult<ContainerHolder> loadContainerPreferFresh(String str, @RawRes int i, Handler handler) {
        PendingResult zza = this.aHG.zza(this.mContext, this, handler.getLooper(), str, i, this.aGC);
        zza.zzcef();
        return zza;
    }

    public PendingResult<ContainerHolder> loadContainerPreferNonDefault(String str, @RawRes int i) {
        PendingResult zza = this.aHG.zza(this.mContext, this, null, str, i, this.aGC);
        zza.zzcee();
        return zza;
    }

    public PendingResult<ContainerHolder> loadContainerPreferNonDefault(String str, @RawRes int i, Handler handler) {
        PendingResult zza = this.aHG.zza(this.mContext, this, handler.getLooper(), str, i, this.aGC);
        zza.zzcee();
        return zza;
    }

    public void setVerboseLoggingEnabled(boolean z) {
        zzbo.setLogLevel(z ? 2 : 5);
    }

    public int zza(zzo com_google_android_gms_tagmanager_zzo) {
        this.aHI.put(com_google_android_gms_tagmanager_zzo.getContainerId(), com_google_android_gms_tagmanager_zzo);
        return this.aHI.size();
    }

    public boolean zzb(zzo com_google_android_gms_tagmanager_zzo) {
        return this.aHI.remove(com_google_android_gms_tagmanager_zzo.getContainerId()) != null;
    }

    synchronized boolean zzv(Uri uri) {
        boolean z;
        zzcj zzcfz = zzcj.zzcfz();
        if (zzcfz.zzv(uri)) {
            String containerId = zzcfz.getContainerId();
            switch (zzcfz.zzcga()) {
                case NONE:
                    zzo com_google_android_gms_tagmanager_zzo = (zzo) this.aHI.get(containerId);
                    if (com_google_android_gms_tagmanager_zzo != null) {
                        com_google_android_gms_tagmanager_zzo.zzox(null);
                        com_google_android_gms_tagmanager_zzo.refresh();
                        break;
                    }
                    break;
                case CONTAINER:
                case CONTAINER_DEBUG:
                    for (String str : this.aHI.keySet()) {
                        zzo com_google_android_gms_tagmanager_zzo2 = (zzo) this.aHI.get(str);
                        if (str.equals(containerId)) {
                            com_google_android_gms_tagmanager_zzo2.zzox(zzcfz.zzcgb());
                            com_google_android_gms_tagmanager_zzo2.refresh();
                        } else if (com_google_android_gms_tagmanager_zzo2.zzcea() != null) {
                            com_google_android_gms_tagmanager_zzo2.zzox(null);
                            com_google_android_gms_tagmanager_zzo2.refresh();
                        }
                    }
                    break;
            }
            z = true;
        } else {
            z = false;
        }
        return z;
    }
}
