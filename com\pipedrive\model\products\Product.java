package com.pipedrive.model.products;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Currency;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

public class Product extends BaseDatasourceEntity {
    private static final List<ProductVariation> DEFAULT_PRODUCT_VARIATIONS = Collections.emptyList();
    @NonNull
    private final Boolean isActive;
    @NonNull
    private final Boolean isRemoved;
    @NonNull
    private final String name;
    @NonNull
    private final List<ProductPrice> prices;
    @NonNull
    private final List<ProductVariation> productVariations;

    @NonNull
    public static Product create(@NonNull Long pipedriveId, @NonNull String name, @NonNull Boolean isActive, @NonNull Boolean isRemoved, @NonNull List<ProductPrice> prices, @NonNull List<ProductVariation> variations) {
        return create(null, pipedriveId, name, isActive, isRemoved, prices, variations);
    }

    @NonNull
    public static Product create(@Nullable Long sqlId, @Nullable Long pipedriveId, @NonNull String name, @NonNull Boolean isActive, @NonNull Boolean isRemoved, @NonNull List<ProductPrice> prices, @Nullable List<ProductVariation> productVariations) {
        return new Product(sqlId, pipedriveId, name, isActive, isRemoved, prices, productVariations);
    }

    private Product(@Nullable Long sqlId, @Nullable Long pipedriveId, @NonNull String name, @NonNull Boolean isActive, @NonNull Boolean isRemoved, @NonNull List<ProductPrice> prices, @Nullable List<ProductVariation> productVariations) {
        if (sqlId != null) {
            setSqlId(sqlId.longValue());
        }
        if (pipedriveId != null) {
            setPipedriveId(pipedriveId.intValue());
        }
        this.name = name;
        this.isActive = isActive;
        this.isRemoved = isRemoved;
        this.prices = prices;
        if (productVariations == null) {
            productVariations = DEFAULT_PRODUCT_VARIATIONS;
        }
        this.productVariations = productVariations;
    }

    @NonNull
    public String getName() {
        return this.name;
    }

    @NonNull
    public Boolean isActive() {
        return this.isActive;
    }

    @NonNull
    public Boolean isRemoved() {
        return this.isRemoved;
    }

    @NonNull
    public List<ProductPrice> getPrices() {
        return this.prices;
    }

    @NonNull
    public List<ProductVariation> getProductVariations() {
        return this.productVariations;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass() || !super.equals(o)) {
            return false;
        }
        Product product = (Product) o;
        if (this.name.equals(product.name) && this.isActive.equals(product.isActive) && this.isRemoved.equals(product.isRemoved) && this.prices.equals(product.prices)) {
            return this.productVariations.equals(product.productVariations);
        }
        return false;
    }

    public int hashCode() {
        return (((((((((super.hashCode() * 31) + this.name.hashCode()) * 31) + this.isActive.hashCode()) * 31) + this.isRemoved.hashCode()) * 31) + this.prices.hashCode()) * 31) + this.productVariations.hashCode();
    }

    public String toString() {
        return "Product{name='" + this.name + '\'' + ", isActive=" + this.isActive + ", isRemoved=" + this.isRemoved + ", prices=" + this.prices + ", productVariations=" + this.productVariations + '}';
    }

    @NonNull
    public Price getPriceForDealsCurrency(@NonNull Currency dealCurrency) {
        return getPriceByCurrencyFromListOfPrices(dealCurrency, this.prices);
    }

    @NonNull
    public Price getProductVariationPriceForCurrency(@NonNull Currency currency, @Nullable ProductVariation productVariation) {
        if (productVariation == null) {
            return getPriceForDealsCurrency(currency);
        }
        return getPriceByCurrencyFromListOfPrices(currency, productVariation.getPrices());
    }

    private Price getPriceByCurrencyFromListOfPrices(@NonNull Currency dealCurrency, @NonNull List<ProductPrice> priceList) {
        Price priceForCurrency = Price.create(BigDecimal.ZERO, dealCurrency);
        for (ProductPrice price : priceList) {
            if (price.getPrice().getCurrency().getCode().equals(dealCurrency.getCode())) {
                return price.getPrice();
            }
        }
        return priceForCurrency;
    }
}
