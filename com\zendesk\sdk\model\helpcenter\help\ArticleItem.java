package com.zendesk.sdk.model.helpcenter.help;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.annotations.SerializedName;
import java.util.Collections;
import java.util.List;

public class ArticleItem implements HelpItem {
    private Long id;
    private String name;
    @SerializedName("section_id")
    private Long sectionId;

    public int getViewType() {
        return 3;
    }

    @NonNull
    public String getName() {
        return this.name == null ? "" : this.name;
    }

    @Nullable
    public Long getId() {
        return this.id;
    }

    @Nullable
    public Long getParentId() {
        return this.sectionId;
    }

    @NonNull
    public List<HelpItem> getChildren() {
        return Collections.emptyList();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ArticleItem that = (ArticleItem) o;
        if (this.id == null ? that.id != null : !this.id.equals(that.id)) {
            return false;
        }
        if (this.sectionId != null) {
            return this.sectionId.equals(that.sectionId);
        }
        if (that.sectionId != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        if (this.id != null) {
            result = this.id.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.sectionId != null) {
            i = this.sectionId.hashCode();
        }
        return i2 + i;
    }
}
