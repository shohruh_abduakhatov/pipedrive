package com.pipedrive.fragments;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.pipedrive.R;
import com.pipedrive.util.StringUtils;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.Locale;

public enum LocaleHelper {
    INSTANCE;
    
    private static String DEFAULT_LANGUAGE_TITLE = null;
    private static String DEFAULT_LANGUAGE_VALUE = null;
    private static final String KEY_LANGUAGE_CODE_APP = "language_code_app";
    private static final String KEY_LANGUAGE_CODE_WEB = "language_code_web";
    private static final String SHARED_PREFS_FILENAME = "locale_helper";
    private String mLanguageTitle;
    private String mLanguageValue;

    public void init(Context context) {
        boolean z = false;
        DEFAULT_LANGUAGE_TITLE = context.getString(R.string.default_language_title);
        DEFAULT_LANGUAGE_VALUE = context.getString(R.string.default_language_value);
        String languageFromAppSettings = context.getSharedPreferences(SHARED_PREFS_FILENAME, 0).getString(KEY_LANGUAGE_CODE_APP, null);
        String languageFromWebappSettings = context.getSharedPreferences(SHARED_PREFS_FILENAME, 0).getString(KEY_LANGUAGE_CODE_WEB, null);
        if (!TextUtils.isEmpty(languageFromAppSettings)) {
            languageFromWebappSettings = languageFromAppSettings;
        }
        this.mLanguageValue = languageFromWebappSettings;
        if (TextUtils.isEmpty(this.mLanguageValue)) {
            this.mLanguageValue = context.getResources().getConfiguration().locale.toString();
        }
        String str = this.mLanguageValue;
        if (!TextUtils.isEmpty(languageFromAppSettings)) {
            z = true;
        }
        setLanguage(context, str, z);
    }

    public String getLanguageTitle() {
        return this.mLanguageTitle;
    }

    public String getLanguageValue() {
        return this.mLanguageValue;
    }

    @NonNull
    public Locale getLocale() {
        String[] split = this.mLanguageValue.split(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        return split.length > 1 ? new Locale(split[0], split[1]) : new Locale(split[0]);
    }

    @Nullable
    public static String getLanguageAsLocale(@Nullable String languageCode, @Nullable String languageCountry) {
        if (StringUtils.isTrimmedAndEmpty(languageCode) || StringUtils.isTrimmedAndEmpty(languageCountry)) {
            return null;
        }
        return languageCode + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + languageCountry;
    }

    public void setLanguage(@NonNull Context context, @Nullable String languageCode, @Nullable String languageCountry, boolean fromSettingsView) {
        String languageAsLocale = getLanguageAsLocale(languageCode, languageCountry);
        if (languageAsLocale != null) {
            setLanguage(context, languageAsLocale, fromSettingsView);
        }
    }

    public void setLanguage(Context context, String languageValue, boolean fromSettingsView) {
        String langTitle = "";
        String langValue = languageValue;
        String[] languageValues = context.getResources().getStringArray(R.array.language_values);
        for (int i = 0; i < languageValues.length; i++) {
            if (languageValues[i].equalsIgnoreCase(langValue)) {
                langTitle = context.getResources().getStringArray(R.array.language_titles)[i];
                break;
            }
        }
        if (TextUtils.isEmpty(langTitle)) {
            langValue = DEFAULT_LANGUAGE_VALUE;
            langTitle = DEFAULT_LANGUAGE_TITLE;
        }
        context.getSharedPreferences(SHARED_PREFS_FILENAME, 0).edit().putString(fromSettingsView ? KEY_LANGUAGE_CODE_APP : KEY_LANGUAGE_CODE_WEB, langValue).commit();
        String languageFromAppSettings = context.getSharedPreferences(SHARED_PREFS_FILENAME, 0).getString(KEY_LANGUAGE_CODE_APP, null);
        if (fromSettingsView || TextUtils.isEmpty(languageFromAppSettings)) {
            this.mLanguageValue = langValue;
            this.mLanguageTitle = langTitle;
            Locale locale = getLocale();
            Resources resources = context.getResources();
            Configuration configuration = resources.getConfiguration();
            if (VERSION.SDK_INT < 17) {
                configuration.locale = locale;
            } else {
                configuration.setLocale(locale);
            }
            resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        }
    }
}
