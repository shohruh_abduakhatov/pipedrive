package com.google.android.gms.wearable.internal;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataItem;

public final class zzz extends zzc implements DataEvent {
    private final int YC;

    public zzz(DataHolder dataHolder, int i, int i2) {
        super(dataHolder, i);
        this.YC = i2;
    }

    public /* synthetic */ Object freeze() {
        return zzcmu();
    }

    public DataItem getDataItem() {
        return new zzaf(this.zy, this.BU, this.YC);
    }

    public int getType() {
        return getInteger("event_type");
    }

    public String toString() {
        String str = getType() == 1 ? "changed" : getType() == 2 ? "deleted" : "unknown";
        String valueOf = String.valueOf(getDataItem());
        return new StringBuilder((String.valueOf(str).length() + 32) + String.valueOf(valueOf).length()).append("DataEventRef{ type=").append(str).append(", dataitem=").append(valueOf).append(" }").toString();
    }

    public DataEvent zzcmu() {
        return new zzy(this);
    }
}
