package com.pipedrive.customfields.views;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Pair;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.model.Organization;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.util.CustomFieldsUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class OrganizationCustomFieldListView extends CustomFieldListView {
    public OrganizationCustomFieldListView(Context context) {
        this(context, null);
    }

    public OrganizationCustomFieldListView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public OrganizationCustomFieldListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @NonNull
    protected String getSpecialFieldsType() {
        return "organization";
    }

    @NonNull
    protected List<CustomField> getCustomFields(@NonNull Session session) {
        Organization organization = (Organization) new OrganizationsDataSource(session.getDatabase()).findBySqlId(this.mSqlId.longValue());
        if (organization == null) {
            return Collections.emptyList();
        }
        Pair<ArrayList<CustomField>, Integer> strangeCustomFieldArray = CustomFieldsUtil.populateCustomAndSpecialFields("organization", organization.getCustomFields(), null);
        return ((ArrayList) strangeCustomFieldArray.first).subList(((Integer) strangeCustomFieldArray.second).intValue(), ((ArrayList) strangeCustomFieldArray.first).size());
    }
}
