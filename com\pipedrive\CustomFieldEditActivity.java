package com.pipedrive;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import com.pipedrive.fragments.customfields.CustomFieldBaseFragment;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.navigator.Navigator;
import com.pipedrive.navigator.Navigator.Type;

public class CustomFieldEditActivity extends BaseActivity {
    @NonNull
    private CustomFieldBaseFragment mFragment;

    public static void startActivity(@NonNull Context context, @NonNull CustomField customField, @IntRange(from = 1) long itemSqlId) {
        context.startActivity(new Intent(context, CustomFieldEditActivity.class).putExtra(CustomFieldBaseFragment.ARG_CUSTOM_FIELD, customField).putExtra(CustomFieldBaseFragment.ARG_ITEM_SQL_ID, itemSqlId));
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_custom_field_edit);
        setSupportActionBar((Toolbar) findViewById(R.id.customFieldEdit.toolbar));
        setTitle(((CustomField) getIntent().getParcelableExtra(CustomFieldBaseFragment.ARG_CUSTOM_FIELD)).getName());
        if (savedInstanceState == null) {
            this.mFragment = CustomFieldBaseFragment.newInstance(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction().add(R.id.customFieldEdit.fragmentContainer, this.mFragment, CustomFieldBaseFragment.TAG).commit();
        } else {
            this.mFragment = (CustomFieldBaseFragment) getSupportFragmentManager().findFragmentByTag(CustomFieldBaseFragment.TAG);
        }
        setNavigatorType(Type.UPDATE);
    }

    @Nullable
    protected Navigator getNavigatorImplementation() {
        return new Navigator(this.mFragment, (Toolbar) findViewById(R.id.customFieldEdit.toolbar));
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.mFragment.onActivityResult(requestCode, resultCode, data);
    }
}
