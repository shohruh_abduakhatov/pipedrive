package rx;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;
import rx.plugins.RxJavaHooks;
import rx.subscriptions.CompositeSubscription;

class Completable$4 implements Completable$OnSubscribe {
    final /* synthetic */ Iterable val$sources;

    Completable$4(Iterable iterable) {
        this.val$sources = iterable;
    }

    public void call(final CompletableSubscriber s) {
        final CompositeSubscription set = new CompositeSubscription();
        s.onSubscribe(set);
        try {
            Iterator<? extends Completable> it = this.val$sources.iterator();
            if (it == null) {
                s.onError(new NullPointerException("The iterator returned is null"));
                return;
            }
            boolean empty = true;
            final AtomicBoolean once = new AtomicBoolean();
            CompletableSubscriber inner = new CompletableSubscriber() {
                public void onCompleted() {
                    if (once.compareAndSet(false, true)) {
                        set.unsubscribe();
                        s.onCompleted();
                    }
                }

                public void onError(Throwable e) {
                    if (once.compareAndSet(false, true)) {
                        set.unsubscribe();
                        s.onError(e);
                        return;
                    }
                    RxJavaHooks.onError(e);
                }

                public void onSubscribe(Subscription d) {
                    set.add(d);
                }
            };
            while (!once.get() && !set.isUnsubscribed()) {
                try {
                    if (it.hasNext()) {
                        empty = false;
                        if (!once.get() && !set.isUnsubscribed()) {
                            try {
                                Completable c = (Completable) it.next();
                                if (c == null) {
                                    NullPointerException npe = new NullPointerException("One of the sources is null");
                                    if (once.compareAndSet(false, true)) {
                                        set.unsubscribe();
                                        s.onError(npe);
                                        return;
                                    }
                                    RxJavaHooks.onError(npe);
                                    return;
                                } else if (!once.get() && !set.isUnsubscribed()) {
                                    c.unsafeSubscribe(inner);
                                } else {
                                    return;
                                }
                            } catch (Throwable e) {
                                if (once.compareAndSet(false, true)) {
                                    set.unsubscribe();
                                    s.onError(e);
                                    return;
                                }
                                RxJavaHooks.onError(e);
                                return;
                            }
                        }
                        return;
                    } else if (empty) {
                        s.onCompleted();
                        return;
                    } else {
                        return;
                    }
                } catch (Throwable e2) {
                    if (once.compareAndSet(false, true)) {
                        set.unsubscribe();
                        s.onError(e2);
                        return;
                    }
                    RxJavaHooks.onError(e2);
                    return;
                }
            }
        } catch (Throwable e22) {
            s.onError(e22);
        }
    }
}
