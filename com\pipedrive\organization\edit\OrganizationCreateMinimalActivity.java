package com.pipedrive.organization.edit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.pipedrive.R;
import com.pipedrive.util.FullCreationFormExpansionWithTransition;

public class OrganizationCreateMinimalActivity extends OrganizationCreateActivity {
    @BindView(2131820738)
    View mDescriptionContainer;
    @BindView(2131820969)
    TextView mDescriptionText;
    @BindView(2131820737)
    LinearLayout mMainContainer;
    @BindView(2131820552)
    View mMoreDetails;

    @MainThread
    public static void startActivityForResult(@NonNull Activity activity, @Nullable String nameProvided, int requestCode) {
        Intent intent = new Intent(activity, OrganizationCreateMinimalActivity.class);
        if (nameProvided != null) {
            intent.putExtra("NEW_ORGANIZATION_NAME", nameProvided);
        }
        ActivityCompat.startActivityForResult(activity, intent, requestCode, null);
    }

    int getLayoutId() {
        return R.layout.activity_create_org_minimal_form;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind((Activity) this);
        this.mDescriptionText.setText(R.string.we_can_create_a_new_organization_with_just_the_name_);
    }

    @OnClick({2131820970})
    void onClicked() {
        new FullCreationFormExpansionWithTransition(this, this.mMainContainer).startTransition(this.mMainContainer, this.mDescriptionContainer, this.mMoreDetails);
    }
}
