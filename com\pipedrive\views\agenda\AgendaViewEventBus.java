package com.pipedrive.views.agenda;

import android.support.annotation.NonNull;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;

public class AgendaViewEventBus {
    private static AgendaViewEventBus instance;
    @NonNull
    private final Subject<Integer, Integer> agendaDayViewScrollYChangesBus = BehaviorSubject.create().toSerialized();
    @NonNull
    private final Subject<Void, Void> allDayActivityAnimationEndBus = PublishSubject.create().toSerialized();
    @NonNull
    private final Subject<Boolean, Boolean> allDayActivityExpandBus = BehaviorSubject.create().toSerialized();

    @NonNull
    public static AgendaViewEventBus getInstance() {
        if (instance == null) {
            synchronized (AgendaViewEventBus.class) {
                if (instance == null) {
                    instance = new AgendaViewEventBus();
                }
            }
        }
        return instance;
    }

    private AgendaViewEventBus() {
        this.agendaDayViewScrollYChangesBus.onNext(Integer.valueOf(0));
        this.allDayActivityExpandBus.onNext(Boolean.valueOf(false));
    }

    @NonNull
    Observable<Integer> agendaDayViewScrollYChanges() {
        return this.agendaDayViewScrollYChangesBus;
    }

    @NonNull
    Observable<Boolean> allDayActivityExpandBus() {
        return this.allDayActivityExpandBus;
    }

    @NonNull
    Subject<Void, Void> allDayActivityAnimationEndBus() {
        return this.allDayActivityAnimationEndBus;
    }

    void allDayActivityExpandCollapseStateChanged(boolean expanded) {
        if (this.allDayActivityExpandBus.hasObservers()) {
            this.allDayActivityExpandBus.onNext(Boolean.valueOf(expanded));
        }
    }

    public void allDayActivityAnimationEnd() {
        if (this.allDayActivityAnimationEndBus.hasObservers()) {
            this.allDayActivityAnimationEndBus.onNext(null);
        }
    }

    void agendaDayViewScrollYChanged(int scrollY) {
        if (this.agendaDayViewScrollYChangesBus.hasObservers()) {
            this.agendaDayViewScrollYChangesBus.onNext(Integer.valueOf(scrollY));
        }
    }
}
