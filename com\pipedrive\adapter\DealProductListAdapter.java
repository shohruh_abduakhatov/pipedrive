package com.pipedrive.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.views.viewholder.deal.DealProductTotalPriceViewHolder;
import com.pipedrive.views.viewholder.deal.DealProductViewHolder;
import java.util.List;

public class DealProductListAdapter extends Adapter<ViewHolder> {
    private static final int TYPE_FOOTER = 0;
    private static final int TYPE_PRODUCT = 1;
    @Nullable
    private final String mCurrencyCode;
    @NonNull
    private final List<DealProduct> mDealProductList;
    @Nullable
    private final OnDealProductItemClickListener mOnDealProductItemClickListener;
    @NonNull
    private final Session mSession;

    public interface OnDealProductItemClickListener {
        void onItemClicked(long j);
    }

    public DealProductListAdapter(@NonNull List<DealProduct> dealProductList, @NonNull Session session, @Nullable String currencyCode, @Nullable OnDealProductItemClickListener onDealProductItemClickListener) {
        this.mDealProductList = dealProductList;
        this.mSession = session;
        this.mCurrencyCode = currencyCode;
        this.mOnDealProductItemClickListener = onDealProductItemClickListener;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case 1:
                View itemView = inflater.inflate(R.layout.row_dealproduct, parent, false);
                final ViewHolder viewHolder = new DealProductViewHolder(itemView);
                itemView.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        if (DealProductListAdapter.this.mOnDealProductItemClickListener != null) {
                            DealProductListAdapter.this.mOnDealProductItemClickListener.onItemClicked(((DealProduct) DealProductListAdapter.this.mDealProductList.get(viewHolder.getAdapterPosition())).getSqlId());
                        }
                    }
                });
                return viewHolder;
            default:
                return new DealProductTotalPriceViewHolder(inflater.inflate(R.layout.row_dealproduct_total, parent, false));
        }
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case 1:
                ((DealProductViewHolder) holder).fill(this.mSession, (DealProduct) this.mDealProductList.get(position));
                return;
            default:
                ((DealProductTotalPriceViewHolder) holder).fill(this.mSession, this.mDealProductList, this.mCurrencyCode);
                return;
        }
    }

    public int getItemCount() {
        return this.mDealProductList.size() + 1;
    }

    public int getItemViewType(int position) {
        if (position == this.mDealProductList.size()) {
            return 0;
        }
        return 1;
    }
}
