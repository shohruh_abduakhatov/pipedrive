package com.pipedrive.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.background.ApplicationStateMonitor;
import com.newrelic.agent.android.instrumentation.Instrumented;
import com.pipedrive.R;
import com.pipedrive.analytics.Analytics;

@Instrumented
public class BaseFragment extends Fragment implements TraceFieldInterface {
    Toolbar mToolbar;

    protected void onStop() {
        super.onStop();
        ApplicationStateMonitor.getInstance().activityStopped();
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        this.mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        if (this.mToolbar != null && getActivity() != null && (getActivity() instanceof AppCompatActivity)) {
            ((AppCompatActivity) getActivity()).setSupportActionBar(this.mToolbar);
        }
    }

    public void onStart() {
        ApplicationStateMonitor.getInstance().activityStarted();
        super.onStart();
        Analytics.hitFragment((Fragment) this);
    }
}
