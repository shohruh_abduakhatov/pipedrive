package com.pipedrive.products.edit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import com.pipedrive.navigator.Navigator.Type;

public class DealProductCreateActivity extends DealProductEditActivity {
    public static void startActivity(@NonNull Activity activity, @NonNull Long productSqlId, @NonNull Long dealSqlId, @NonNull Integer requestCode) {
        ActivityCompat.startActivityForResult(activity, new Intent(activity, DealProductCreateActivity.class).putExtra("product_sql_id", productSqlId).putExtra("deal_sql_id", dealSqlId), requestCode.intValue(), null);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavigatorType(Type.CREATE);
    }

    public boolean isChangesMadeToFieldsAfterInitialLoad() {
        updateDealProductWithEnteredData();
        return true;
    }

    public void onDealProductCreatedOrUpdated() {
        setResult(-1);
        finish();
    }
}
