package com.zendesk.sdk.network.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.SdkConfiguration;
import com.zendesk.sdk.model.access.AccessToken;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.model.access.AuthenticationType;
import com.zendesk.sdk.model.access.Identity;
import com.zendesk.sdk.model.access.JwtIdentity;
import com.zendesk.sdk.model.settings.SafeMobileSettings;
import com.zendesk.sdk.network.AccessProvider;
import com.zendesk.sdk.network.BaseProvider;
import com.zendesk.sdk.network.SdkSettingsProvider;
import com.zendesk.sdk.storage.IdentityStorage;
import com.zendesk.sdk.storage.SdkSettingsStorage;
import com.zendesk.service.ErrorResponse;
import com.zendesk.service.ErrorResponseAdapter;
import com.zendesk.service.ZendeskCallback;
import java.util.concurrent.TimeUnit;

class ZendeskBaseProvider implements BaseProvider {
    private static final String LOG_TAG = "BaseProvider";
    private static final int SDK_SETTINGS_MAX_AGE_HOURS = 1;
    private final AccessProvider accessProvider;
    private final IdentityStorage identityStorage;
    private final SdkSettingsStorage sdkSettingsStorage;
    private SdkSettingsProvider settingsProvider;

    public ZendeskBaseProvider(@NonNull AccessProvider accessProvider, @NonNull SdkSettingsStorage settingsStorage, @NonNull IdentityStorage identityStorage, @NonNull SdkSettingsProvider sdkSettingsProvider) {
        this.accessProvider = accessProvider;
        this.identityStorage = identityStorage;
        this.sdkSettingsStorage = settingsStorage;
        this.settingsProvider = sdkSettingsProvider;
    }

    public void configureSdk(@Nullable final ZendeskCallback<SdkConfiguration> callback) {
        getSdkSettings(new ZendeskCallback<SafeMobileSettings>() {
            public void onSuccess(final SafeMobileSettings mobileSettings) {
                ZendeskBaseProvider.this.getAccessToken(mobileSettings, new ZendeskCallback<AccessToken>() {
                    public void onSuccess(AccessToken accessToken) {
                        if (callback != null) {
                            callback.onSuccess(new SdkConfiguration(accessToken, mobileSettings == null ? new SafeMobileSettings(null) : mobileSettings));
                        }
                    }

                    public void onError(ErrorResponse errorResponse) {
                        if (callback != null) {
                            callback.onError(errorResponse);
                        }
                    }
                });
            }

            public void onError(ErrorResponse errorResponse) {
                if (callback != null) {
                    callback.onError(errorResponse);
                }
            }
        });
    }

    public void getSdkSettings(@Nullable ZendeskCallback<SafeMobileSettings> callback) {
        boolean hasStoredSetting = this.sdkSettingsStorage.hasStoredSdkSettings();
        boolean areSettingsUpToDate = this.sdkSettingsStorage.areSettingsUpToDate(1, TimeUnit.HOURS);
        if (hasStoredSetting && areSettingsUpToDate) {
            Logger.d(LOG_TAG, "Settings available - skipping download", new Object[0]);
            if (callback != null) {
                callback.onSuccess(this.sdkSettingsStorage.getStoredSettings());
                return;
            }
            return;
        }
        Logger.d(LOG_TAG, "Downloading settings: 'hasStoredSetting': %b | 'areSettingsUpToDate:' %b", Boolean.valueOf(hasStoredSetting), Boolean.valueOf(areSettingsUpToDate));
        this.settingsProvider.getSettings(callback);
    }

    public void getAccessToken(@NonNull SafeMobileSettings mobileSettings, @Nullable final ZendeskCallback<AccessToken> callback) {
        AccessToken accessToken = this.identityStorage.getStoredAccessToken();
        if (accessToken != null) {
            Logger.d(LOG_TAG, "We have a stored access token so we will use that.", new Object[0]);
            if (callback != null) {
                callback.onSuccess(accessToken);
                return;
            }
        }
        Logger.d(LOG_TAG, "We do not have a stored access token.", new Object[0]);
        Identity identity = this.identityStorage.getIdentity();
        AuthenticationType authenticationType = mobileSettings.getAuthenticationType();
        if (AuthenticationType.ANONYMOUS == authenticationType && (identity instanceof AnonymousIdentity)) {
            this.accessProvider.getAndStoreAuthTokenViaAnonymous((AnonymousIdentity) identity, new PassThroughErrorZendeskCallback<AccessToken>(callback) {
                public void onSuccess(AccessToken accessToken) {
                    if (callback != null) {
                        callback.onSuccess(accessToken);
                    }
                }
            });
        } else if (AuthenticationType.JWT == authenticationType && (identity instanceof JwtIdentity)) {
            this.accessProvider.getAndStoreAuthTokenViaJwt((JwtIdentity) identity, new PassThroughErrorZendeskCallback<AccessToken>(callback) {
                public void onSuccess(AccessToken accessToken) {
                    if (callback != null) {
                        callback.onSuccess(accessToken);
                    }
                }
            });
        } else {
            this.sdkSettingsStorage.deleteStoredSettings();
            String error = new AuthenticationLoggerHelper(authenticationType, identity).getLogMessage();
            Logger.e(LOG_TAG, error, new Object[0]);
            if (callback != null) {
                callback.onError(new ErrorResponseAdapter(error));
            }
        }
    }
}
