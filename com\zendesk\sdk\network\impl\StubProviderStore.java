package com.zendesk.sdk.network.impl;

import com.zendesk.sdk.network.HelpCenterProvider;
import com.zendesk.sdk.network.NetworkInfoProvider;
import com.zendesk.sdk.network.PushRegistrationProvider;
import com.zendesk.sdk.network.RequestProvider;
import com.zendesk.sdk.network.SdkSettingsProvider;
import com.zendesk.sdk.network.SettingsHelper;
import com.zendesk.sdk.network.UploadProvider;
import com.zendesk.sdk.network.UserProvider;

class StubProviderStore implements ProviderStore {
    final HelpCenterProvider helpCenterProvider = ((HelpCenterProvider) StubProviderFactory.getStubProvider(HelpCenterProvider.class));
    final NetworkInfoProvider networkInfoProvider = ((NetworkInfoProvider) StubProviderFactory.getStubProvider(NetworkInfoProvider.class));
    final PushRegistrationProvider pushRegistrationProvider = ((PushRegistrationProvider) StubProviderFactory.getStubProvider(PushRegistrationProvider.class));
    final RequestProvider requestProvider = ((RequestProvider) StubProviderFactory.getStubProvider(RequestProvider.class));
    final SdkSettingsProvider sdkSettingsProvider = ((SdkSettingsProvider) StubProviderFactory.getStubProvider(SdkSettingsProvider.class));
    final SettingsHelper settingsHelper = ((SettingsHelper) StubProviderFactory.getStubProvider(SettingsHelper.class));
    final UploadProvider uploadProvider = ((UploadProvider) StubProviderFactory.getStubProvider(UploadProvider.class));
    final UserProvider userProvider = ((UserProvider) StubProviderFactory.getStubProvider(UserProvider.class));

    StubProviderStore() {
    }

    public UserProvider userProvider() {
        return this.userProvider;
    }

    public HelpCenterProvider helpCenterProvider() {
        return this.helpCenterProvider;
    }

    public PushRegistrationProvider pushRegistrationProvider() {
        return this.pushRegistrationProvider;
    }

    public RequestProvider requestProvider() {
        return this.requestProvider;
    }

    public UploadProvider uploadProvider() {
        return this.uploadProvider;
    }

    public SdkSettingsProvider sdkSettingsProvider() {
        return this.sdkSettingsProvider;
    }

    public NetworkInfoProvider networkInfoProvider() {
        return this.networkInfoProvider;
    }

    public SettingsHelper uiSettingsHelper() {
        return this.settingsHelper;
    }
}
