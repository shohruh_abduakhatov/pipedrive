package com.zendesk.sdk.model.helpcenter.help;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.annotations.SerializedName;
import com.zendesk.util.CollectionUtils;
import java.util.ArrayList;
import java.util.List;

public class SectionItem implements HelpItem {
    @SerializedName("category_id")
    private Long categoryId;
    private List<HelpItem> children;
    @SerializedName("name")
    private String name;
    @SerializedName("id")
    private Long sectionId;
    @SerializedName("article_count")
    private int totalArticlesCount;

    public int getViewType() {
        return 2;
    }

    @NonNull
    public String getName() {
        return this.name == null ? "" : this.name;
    }

    @Nullable
    public Long getId() {
        return this.sectionId;
    }

    @Nullable
    public Long getParentId() {
        return this.categoryId;
    }

    @NonNull
    public List<HelpItem> getChildren() {
        return CollectionUtils.copyOf(this.children);
    }

    public void addChild(HelpItem child) {
        if (this.children == null) {
            this.children = new ArrayList(1);
        }
        if (child != null) {
            this.children.add(child);
        }
    }

    public void addChildren(List<HelpItem> children) {
        if (this.children == null) {
            this.children = CollectionUtils.copyOf((List) children);
        } else {
            this.children.addAll(children);
        }
    }

    public void removeChild(HelpItem helpItem) {
        if (this.children != null) {
            this.children.remove(helpItem);
        }
    }

    public int getTotalArticlesCount() {
        return this.totalArticlesCount;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SectionItem that = (SectionItem) o;
        if (this.sectionId == null ? that.sectionId != null : !this.sectionId.equals(that.sectionId)) {
            return false;
        }
        if (this.categoryId != null) {
            return this.categoryId.equals(that.categoryId);
        }
        if (that.categoryId != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        if (this.sectionId != null) {
            result = this.sectionId.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.categoryId != null) {
            i = this.categoryId.hashCode();
        }
        return i2 + i;
    }
}
