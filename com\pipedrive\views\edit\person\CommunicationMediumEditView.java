package com.pipedrive.views.edit.person;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import butterknife.ButterKnife;
import com.pipedrive.R;
import com.pipedrive.model.contact.CommunicationMedium;
import com.pipedrive.views.edit.person.rows.CommunicationMediumEditRow;
import java.util.List;

public final class CommunicationMediumEditView extends FrameLayout {
    @NonNull
    private final LinearLayout mContainer;

    public interface OnAddAnotherClickListener {
        void onAddAnotherClicked();
    }

    public interface OnClearClickListener {
        void onClearClicked(@NonNull CommunicationMedium communicationMedium);
    }

    public CommunicationMediumEditView(@NonNull Context context) {
        this(context, null);
    }

    public CommunicationMediumEditView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CommunicationMediumEditView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.layout_communication_medium_edit_view, this, true);
        this.mContainer = (LinearLayout) ButterKnife.findById(this, R.id.container);
    }

    public final void init(@NonNull List<? extends CommunicationMedium> items, final OnAddAnotherClickListener onAddAnotherClickListener, final OnClearClickListener onClearClickListener) {
        this.mContainer.removeAllViews();
        for (CommunicationMedium item : items) {
            View itemView = CommunicationMediumEditRow.buildRowFor(getContext(), this, item, this.mContainer.getChildCount() == 0, new com.pipedrive.views.edit.person.rows.CommunicationMediumEditRow.OnClearClickListener() {
                public void onClearClicked(@NonNull CommunicationMedium communicationMedium) {
                    if (onClearClickListener != null) {
                        onClearClickListener.onClearClicked(communicationMedium);
                    }
                }
            });
            if (itemView != null) {
                this.mContainer.addView(itemView.getRootView());
            }
        }
        ButterKnife.findById(this, R.id.add_another).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (onAddAnotherClickListener != null) {
                    onAddAnotherClickListener.onAddAnotherClicked();
                }
            }
        });
    }
}
