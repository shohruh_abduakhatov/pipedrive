package com.google.android.gms.wearable.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.wearable.zzd;

public final class AncsNotificationParcelable extends AbstractSafeParcelable implements zzd {
    public static final Creator<AncsNotificationParcelable> CREATOR = new zzh();
    private final String JB;
    private final String Yu;
    private final String aSU;
    private final byte aSV;
    private final byte aSW;
    private final byte aSX;
    private final byte aSY;
    private final String jh;
    private int mId;
    final int mVersionCode;
    private final String nb;
    private final String zzcjc;
    private final String zzctj;

    AncsNotificationParcelable(int i, int i2, String str, String str2, String str3, String str4, String str5, String str6, byte b, byte b2, byte b3, byte b4, String str7) {
        this.mId = i2;
        this.mVersionCode = i;
        this.zzctj = str;
        this.aSU = str2;
        this.nb = str3;
        this.JB = str4;
        this.Yu = str5;
        this.jh = str6;
        this.aSV = b;
        this.aSW = b2;
        this.aSX = b3;
        this.aSY = b4;
        this.zzcjc = str7;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AncsNotificationParcelable ancsNotificationParcelable = (AncsNotificationParcelable) obj;
        if (this.mVersionCode != ancsNotificationParcelable.mVersionCode || this.mId != ancsNotificationParcelable.mId || this.aSV != ancsNotificationParcelable.aSV || this.aSW != ancsNotificationParcelable.aSW || this.aSX != ancsNotificationParcelable.aSX || this.aSY != ancsNotificationParcelable.aSY || !this.zzctj.equals(ancsNotificationParcelable.zzctj)) {
            return false;
        }
        if (this.aSU != null) {
            if (!this.aSU.equals(ancsNotificationParcelable.aSU)) {
                return false;
            }
        } else if (ancsNotificationParcelable.aSU != null) {
            return false;
        }
        if (!this.nb.equals(ancsNotificationParcelable.nb) || !this.JB.equals(ancsNotificationParcelable.JB) || !this.Yu.equals(ancsNotificationParcelable.Yu)) {
            return false;
        }
        if (this.jh != null) {
            if (!this.jh.equals(ancsNotificationParcelable.jh)) {
                return false;
            }
        } else if (ancsNotificationParcelable.jh != null) {
            return false;
        }
        if (this.zzcjc != null) {
            z = this.zzcjc.equals(ancsNotificationParcelable.zzcjc);
        } else if (ancsNotificationParcelable.zzcjc != null) {
            z = false;
        }
        return z;
    }

    public String getDisplayName() {
        return this.jh == null ? this.zzctj : this.jh;
    }

    public int getId() {
        return this.mId;
    }

    public String getPackageName() {
        return this.zzcjc;
    }

    public String getTitle() {
        return this.JB;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((((((((((this.jh != null ? this.jh.hashCode() : 0) + (((((((((this.aSU != null ? this.aSU.hashCode() : 0) + (((((this.mVersionCode * 31) + this.mId) * 31) + this.zzctj.hashCode()) * 31)) * 31) + this.nb.hashCode()) * 31) + this.JB.hashCode()) * 31) + this.Yu.hashCode()) * 31)) * 31) + this.aSV) * 31) + this.aSW) * 31) + this.aSX) * 31) + this.aSY) * 31;
        if (this.zzcjc != null) {
            i = this.zzcjc.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        int i = this.mVersionCode;
        int i2 = this.mId;
        String str = this.zzctj;
        String str2 = this.aSU;
        String str3 = this.nb;
        String str4 = this.JB;
        String str5 = this.Yu;
        String str6 = this.jh;
        byte b = this.aSV;
        byte b2 = this.aSW;
        byte b3 = this.aSX;
        byte b4 = this.aSY;
        String str7 = this.zzcjc;
        return new StringBuilder(((((((String.valueOf(str).length() + 234) + String.valueOf(str2).length()) + String.valueOf(str3).length()) + String.valueOf(str4).length()) + String.valueOf(str5).length()) + String.valueOf(str6).length()) + String.valueOf(str7).length()).append("AncsNotificationParcelable{versionCode=").append(i).append(", id=").append(i2).append(", appId='").append(str).append("'").append(", dateTime='").append(str2).append("'").append(", notificationText='").append(str3).append("'").append(", title='").append(str4).append("'").append(", subtitle='").append(str5).append("'").append(", displayName='").append(str6).append("'").append(", eventId=").append(b).append(", eventFlags=").append(b2).append(", categoryId=").append(b3).append(", categoryCount=").append(b4).append(", packageName='").append(str7).append("'").append("}").toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzh.zza(this, parcel, i);
    }

    public String zzbiv() {
        return this.Yu;
    }

    public String zzcml() {
        return this.aSU;
    }

    public String zzcmm() {
        return this.nb;
    }

    public byte zzcmn() {
        return this.aSV;
    }

    public byte zzcmo() {
        return this.aSW;
    }

    public byte zzcmp() {
        return this.aSX;
    }

    public byte zzcmq() {
        return this.aSY;
    }

    public String zzup() {
        return this.zzctj;
    }
}
