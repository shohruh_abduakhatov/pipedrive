package com.pipedrive.nearby.filter.filteritem;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.filter.FilterItem;
import com.pipedrive.nearby.filter.FilterItemList;

public class NearbyFilterItemAdapter extends Adapter<FilterItemViewHolder> {
    @NonNull
    private final FilterItemList list;
    @NonNull
    private Long selectedItemPipedriveId = FilterItemList.UNDEFINED_SELECTED_ITEM_ID;
    @NonNull
    private final Session session;

    public NearbyFilterItemAdapter(@NonNull FilterItemList list, @NonNull Session session, @NonNull Long selectedItemPipedriveId) {
        this.list = list;
        this.session = session;
        this.selectedItemPipedriveId = selectedItemPipedriveId;
    }

    public FilterItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_filter_content, parent, false);
        if (viewType == 3) {
            return new UserFilterItemViewHolder(v);
        }
        return new FilterItemViewHolder(v);
    }

    public int getItemViewType(int position) {
        FilterItem item = this.list.getItem(position);
        return item != null ? item.getItemType() : 0;
    }

    public void onBindViewHolder(FilterItemViewHolder holder, int position) {
        FilterItem entity = this.list.getItem(position);
        if (entity != null) {
            holder.bind(entity, this.session);
            if (entity.getPipedriveId().equals(this.selectedItemPipedriveId)) {
                holder.setChecked(true);
            }
        }
    }

    public int getItemCount() {
        return this.list.getSize();
    }

    public void onViewRecycled(FilterItemViewHolder holder) {
        holder.unbind();
        super.onViewRecycled(holder);
    }
}
