package com.pipedrive.tasks;

import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.logging.Log;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.store.StoreOrganization;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished.TaskResult;
import com.pipedrive.util.ContactsUtil;
import com.pipedrive.util.CustomFieldsUtil;
import com.pipedrive.util.deals.DealsUtil;
import org.json.JSONException;

public class CustomFieldSavingTask extends AsyncTaskWithCallback<CustomField, Void> {
    private long mParentId;

    public CustomFieldSavingTask(Session session, OnTaskFinished<CustomField> onTaskFinished) {
        super(session, onTaskFinished);
    }

    protected TaskResult doInBackgroundWithCallback(CustomField... params) {
        if (params.length == 0) {
            return TaskResult.FAILED;
        }
        CustomField customField = params[0];
        Session session = PipedriveApp.getActiveSession();
        if ("deal".equalsIgnoreCase(customField.getFieldType())) {
            Deal deal = (Deal) new DealsDataSource(session.getDatabase()).findBySqlId(this.mParentId);
            if (Deal.EXPECTED_CLOSE_DATE.equalsIgnoreCase(customField.getKey())) {
                deal.setExpectedCloseDate(customField.getTempValue());
            } else {
                try {
                    deal.setCustomFields(CustomFieldsUtil.getModifiedCustomFieldsJsonArray(deal.getCustomFields(), customField));
                } catch (JSONException e) {
                    Log.e(e);
                }
            }
            DealsUtil.saveDeal(deal, session);
        } else if ("organization".equalsIgnoreCase(customField.getFieldType())) {
            Organization org = (Organization) new OrganizationsDataSource(session.getDatabase()).findBySqlId(this.mParentId);
            try {
                org.setCustomFields(CustomFieldsUtil.getModifiedCustomFieldsJsonArray(org.getCustomFields(), customField));
                if (org.isStored()) {
                    new StoreOrganization(session).update(org);
                } else {
                    new StoreOrganization(session).create(org);
                }
            } catch (JSONException e2) {
                Log.e(e2);
            }
        } else if ("person".equalsIgnoreCase(customField.getFieldType())) {
            Person person = (Person) new PersonsDataSource(session.getDatabase()).findBySqlId(this.mParentId);
            try {
                person.setCustomFields(CustomFieldsUtil.getModifiedCustomFieldsJsonArray(person.getCustomFields(), customField));
                ContactsUtil.saveContact(person, session);
            } catch (JSONException e22) {
                Log.e(e22);
            }
        }
        return TaskResult.SUCCEEDED;
    }

    public void setParentId(long parentId) {
        this.mParentId = parentId;
    }
}
