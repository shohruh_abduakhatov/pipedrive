package com.pipedrive.more;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class HtmlOrFileLoadActivity_ViewBinding implements Unbinder {
    private HtmlOrFileLoadActivity target;

    @UiThread
    public HtmlOrFileLoadActivity_ViewBinding(HtmlOrFileLoadActivity target) {
        this(target, target.getWindow().getDecorView());
    }

    @UiThread
    public HtmlOrFileLoadActivity_ViewBinding(HtmlOrFileLoadActivity target, View source) {
        this.target = target;
        target.webView = (WebView) Utils.findRequiredViewAsType(source, R.id.webView, "field 'webView'", WebView.class);
        target.textView = (TextView) Utils.findRequiredViewAsType(source, R.id.text, "field 'textView'", TextView.class);
    }

    @CallSuper
    public void unbind() {
        HtmlOrFileLoadActivity target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.webView = null;
        target.textView = null;
    }
}
