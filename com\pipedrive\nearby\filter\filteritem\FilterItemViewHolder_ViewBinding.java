package com.pipedrive.nearby.filter.filteritem;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class FilterItemViewHolder_ViewBinding implements Unbinder {
    private FilterItemViewHolder target;

    @UiThread
    public FilterItemViewHolder_ViewBinding(FilterItemViewHolder target, View source) {
        this.target = target;
        target.label = (TextView) Utils.findRequiredViewAsType(source, R.id.label, "field 'label'", TextView.class);
    }

    @CallSuper
    public void unbind() {
        FilterItemViewHolder target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.label = null;
    }
}
