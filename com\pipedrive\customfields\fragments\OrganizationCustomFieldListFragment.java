package com.pipedrive.customfields.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.customfields.views.CustomFieldListView;
import com.pipedrive.customfields.views.OrganizationCustomFieldListView;

public final class OrganizationCustomFieldListFragment extends CustomFieldListFragment {
    private static final String KEY_ORGANIZATION_SQL_ID = "ORGANIZATION_SQL_ID";

    public static OrganizationCustomFieldListFragment newInstance(@NonNull Long organizationSqlId) {
        Bundle args = new Bundle();
        args.putSerializable("ORGANIZATION_SQL_ID", organizationSqlId);
        OrganizationCustomFieldListFragment fragment = new OrganizationCustomFieldListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    protected CustomFieldListView createCustomFieldListView(@NonNull Context context) {
        return new OrganizationCustomFieldListView(context);
    }

    @Nullable
    protected Long getSqlId() {
        return (Long) getArguments().getSerializable("ORGANIZATION_SQL_ID");
    }
}
