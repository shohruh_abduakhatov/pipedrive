package com.zendesk.sdk.model.helpcenter;

import android.support.annotation.Nullable;
import java.io.Serializable;

public class User implements Serializable {
    private Long id;
    private String name;

    @Nullable
    public Long getId() {
        return this.id;
    }

    @Nullable
    public String getName() {
        return this.name;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        if (this.id != null) {
            if (this.id.equals(user.id)) {
                return true;
            }
        } else if (user.id == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.id != null ? this.id.hashCode() : 0;
    }
}
