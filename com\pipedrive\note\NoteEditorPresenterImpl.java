package com.pipedrive.note;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.NotesDataSource;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.notes.Note;

class NoteEditorPresenterImpl extends NoteEditorPresenter {
    private final OnTaskFinished CREATE_TASK_CALLBACK = new OnTaskFinished() {
        public void onNoteCreated(boolean success) {
            if (NoteEditorPresenterImpl.this.getView() != null) {
                ((NoteEditorView) NoteEditorPresenterImpl.this.getView()).onNoteCreated(success);
            }
        }
    };
    private final OnTaskFinished DELETE_TASK_CALLBACK = new OnTaskFinished() {
        public void onNoteDeleted(boolean success) {
            if (NoteEditorPresenterImpl.this.getView() != null) {
                ((NoteEditorView) NoteEditorPresenterImpl.this.getView()).onNoteDeleted(success);
            }
        }
    };
    private final OnTaskFinished UPDATE_TASK_CALLBACK = new OnTaskFinished() {
        public void onNoteUpdated(boolean success) {
            if (NoteEditorPresenterImpl.this.getView() != null) {
                ((NoteEditorView) NoteEditorPresenterImpl.this.getView()).onNoteUpdated(success);
            }
        }
    };

    public NoteEditorPresenterImpl(@NonNull Session session, @Nullable Bundle savedState) {
        super(session, savedState);
    }

    void requestNote(long sqlId) {
        cacheDataAndUpdateTheView((Note) new NotesDataSource(getSession().getDatabase()).findBySqlId(sqlId));
    }

    void requestNewNoteForDeal(long dealSqlId) {
        Note note = new Note();
        Deal deal = (Deal) new DealsDataSource(getSession().getDatabase()).findBySqlId(dealSqlId);
        note.setDeal(deal);
        if (deal != null) {
            note.setPerson(deal.getPerson());
            note.setOrganization(deal.getOrganization());
        }
        cacheDataAndUpdateTheView(note);
    }

    void requestNewNoteForPerson(long personSqlId) {
        Note note = new Note();
        Person person = (Person) new PersonsDataSource(getSession().getDatabase()).findBySqlId(personSqlId);
        note.setPerson(person);
        if (person != null) {
            note.setOrganization(person.getCompany());
        }
        cacheDataAndUpdateTheView(note);
    }

    void requestNewNoteForOrg(long orgSqlId) {
        Note note = new Note();
        note.setOrganization((Organization) new OrganizationsDataSource(getSession().getDatabase()).findBySqlId(orgSqlId));
        cacheDataAndUpdateTheView(note);
    }

    void changeNote() {
        if (getData() != null) {
            if (((Note) getData()).isStored()) {
                new UpdateNoteTask(getSession(), this.UPDATE_TASK_CALLBACK).executeOnAsyncTaskExecutor(this, new Note[]{(Note) getData()});
                return;
            }
            new CreateNoteTask(getSession(), this.CREATE_TASK_CALLBACK).executeOnAsyncTaskExecutor(this, new Note[]{(Note) getData()});
        }
    }

    void deleteNote() {
        if (getData() != null) {
            new DeleteNoteTask(getSession(), this.DELETE_TASK_CALLBACK).executeOnAsyncTaskExecutor(this, new Note[]{(Note) getData()});
        }
    }
}
