package com.pipedrive.organization.edit;

import android.support.annotation.CallSuper;
import android.support.annotation.MainThread;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.model.Organization;
import com.pipedrive.tasks.AsyncTask;

abstract class OrganizationCrudTask extends AsyncTask<Organization, Void, Boolean> {
    @Nullable
    private final OnTaskFinished mOnTaskFinished;

    @MainThread
    interface OnTaskFinished {
        void taskFinished(boolean z);
    }

    public OrganizationCrudTask(Session session, @Nullable OnTaskFinished onTaskFinished) {
        super(session);
        this.mOnTaskFinished = onTaskFinished;
    }

    @CallSuper
    protected void onPostExecute(@Nullable Boolean success) {
        if (this.mOnTaskFinished != null) {
            OnTaskFinished onTaskFinished = this.mOnTaskFinished;
            boolean z = success != null && success.booleanValue();
            onTaskFinished.taskFinished(z);
        }
    }
}
