package com.zendesk.sdk.network.impl;

import com.zendesk.sdk.model.access.AccessToken;
import com.zendesk.sdk.model.access.AuthenticationRequestWrapper;
import com.zendesk.sdk.model.access.AuthenticationResponse;
import com.zendesk.sdk.model.access.Identity;
import com.zendesk.sdk.network.AccessService;
import com.zendesk.service.RetrofitZendeskCallbackAdapter;
import com.zendesk.service.RetrofitZendeskCallbackAdapter.RequestExtractor;
import com.zendesk.service.ZendeskCallback;

class ZendeskAccessService {
    private static final RequestExtractor<AuthenticationResponse, AccessToken> AUTH_EXTRACTOR = new RequestExtractor<AuthenticationResponse, AccessToken>() {
        public AccessToken extract(AuthenticationResponse data) {
            return data.getAuthentication();
        }
    };
    private static final String LOG_TAG = "ZendeskAccessService";
    private final AccessService accessService;

    public ZendeskAccessService(AccessService accessService) {
        this.accessService = accessService;
    }

    void getAuthTokenViaJWT(Identity user, ZendeskCallback<AccessToken> callback) {
        AuthenticationRequestWrapper wrapper = new AuthenticationRequestWrapper();
        wrapper.setUser(user);
        this.accessService.getAuthToken(wrapper).enqueue(new RetrofitZendeskCallbackAdapter(callback, AUTH_EXTRACTOR));
    }

    void getAuthTokenViaMobileSDK(Identity user, ZendeskCallback<AccessToken> callback) {
        AuthenticationRequestWrapper wrapper = new AuthenticationRequestWrapper();
        wrapper.setUser(user);
        this.accessService.getAuthTokenForAnonymous(wrapper).enqueue(new RetrofitZendeskCallbackAdapter(callback, AUTH_EXTRACTOR));
    }
}
