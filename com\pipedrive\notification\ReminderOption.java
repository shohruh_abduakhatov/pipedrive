package com.pipedrive.notification;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.model.Activity;

interface ReminderOption {
    @Nullable
    PipedriveDateTime getRemindDateTime(@Nullable Activity activity);

    @NonNull
    String getTitle(@NonNull Context context);
}
