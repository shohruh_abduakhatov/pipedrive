package com.pipedrive.datasource.nearby;

import android.support.annotation.NonNull;
import com.google.android.gms.maps.model.LatLng;

class NearbyItemData {
    @NonNull
    private String address;
    @NonNull
    private LatLng location;
    @NonNull
    private Long sqlId;

    NearbyItemData(@NonNull Long sqlId, @NonNull Double latitude, @NonNull Double longitude, @NonNull String address) {
        this.location = new LatLng(latitude.doubleValue(), longitude.doubleValue());
        this.address = address;
        this.sqlId = sqlId;
    }

    @NonNull
    LatLng getLocation() {
        return this.location;
    }

    @NonNull
    Long getSqlId() {
        return this.sqlId;
    }

    @NonNull
    String getAddress() {
        return this.address;
    }
}
