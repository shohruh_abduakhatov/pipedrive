package com.pipedrive.flow.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.flow.views.FlowView;
import com.pipedrive.flow.views.FlowView.OnItemClickListener;
import com.pipedrive.fragments.BaseFragment;

abstract class FlowFragment extends BaseFragment {
    @Nullable
    protected FlowView flowView;
    @Nullable
    protected OnItemClickListener onItemClickListener;

    @NonNull
    protected abstract FlowView createFlowView(@NonNull Context context);

    @Nullable
    protected abstract Long getSqlId();

    FlowFragment() {
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.onItemClickListener = (OnItemClickListener) context;
        } catch (Exception e) {
        }
    }

    public void refreshContent(@NonNull Session session) {
        if (this.flowView != null) {
            this.flowView.refreshContent(session);
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.flowView = createFlowView(getContext());
        return this.flowView;
    }

    @NonNull
    protected Session getActiveSession() {
        return PipedriveApp.getActiveSession();
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        Long dealSqlId = getSqlId();
        if (dealSqlId != null && this.flowView != null) {
            this.flowView.setup(getActiveSession(), dealSqlId, this.onItemClickListener);
        }
    }
}
