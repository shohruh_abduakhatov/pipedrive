package com.pipedrive.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.text.TextUtils;
import com.newrelic.agent.android.instrumentation.JSONArrayInstrumentation;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.application.Session;
import com.pipedrive.contentproviders.DealsContentProvider;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Currency;
import com.pipedrive.model.Deal;
import com.pipedrive.model.DealStatus;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.util.CursorHelper;
import com.pipedrive.util.StringUtils;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;
import rx.Observable;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class DealsDataSource extends BaseDataSource<Deal> {
    private static final String[] ALL_COLUMNS = new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_DEALS_PIPEDRIVE_ID, PipeSQLiteHelper.COLUMN_DEALS_TITLE, PipeSQLiteHelper.COLUMN_DEALS_VALUE, PipeSQLiteHelper.COLUMN_DEALS_CURRENCY, PipeSQLiteHelper.COLUMN_DEALS_STAGE, PipeSQLiteHelper.COLUMN_DEALS_STATUS, PipeSQLiteHelper.COLUMN_DEALS_PERSON_SQL_ID, PipeSQLiteHelper.COLUMN_DEALS_ORGANIZATION_SQL_ID, PipeSQLiteHelper.COLUMN_DEALS_PIPELINE_ID, PipeSQLiteHelper.COLUMN_DEALS_NEXT_TASK_DATE, PipeSQLiteHelper.COLUMN_DEALS_NEXT_TASK_TIME, PipeSQLiteHelper.COLUMN_DEALS_UNDONE_ACTIVITIES_COUNT, PipeSQLiteHelper.COLUMN_DEALS_FORMATTED_VALUE, PipeSQLiteHelper.COLUMN_DEALS_ROTTEN_TIME, PipeSQLiteHelper.COLUMN_DEALS_DEAL_OWNER_PD_ID, PipeSQLiteHelper.COLUMN_DEALS_DROP_BOX_ADDRESS, PipeSQLiteHelper.COLUMN_DEALS_CUSTOM_FIELDS, PipeSQLiteHelper.COLUMN_DEALS_VISIBLE_TO, PipeSQLiteHelper.COLUMN_DEALS_OWNER_NAME, PipeSQLiteHelper.COLUMN_DEALS_EXPECTED_CLOSE_DATE, PipeSQLiteHelper.COLUMN_DEALS_ADD_TIME, PipeSQLiteHelper.COLUMN_DEALS_UPDATE_TIME, PipeSQLiteHelper.COLUMN_DEALS_WON_TIME, PipeSQLiteHelper.COLUMN_DEALS_LOST_TIME, PipeSQLiteHelper.COLUMN_DEALS_PRODUCT_COUNT, "deals._id AS d_sql_id"};
    public static final String COLUMN_DEAL_SQL_ID_ALIAS = "d_sql_id";
    private static final String COLUMN_NAME_COUNT = "count";
    private static final String COLUMN_NAME_SUM = "sum";
    @Nullable
    private ForJoin dealsDataSourceForJoin;
    @Nullable
    private com.pipedrive.datasource.OrganizationsDataSource.ForJoin organizationsDataSourceForJoin;
    @Nullable
    private com.pipedrive.datasource.PersonsDataSource.ForJoin personsDataSourceForJoin;
    @NonNull
    private final SearchHelper searchHelper = new SearchHelper("deals.d_title_search_field", "persons.c_name_search_field", "persons.c_phones_search_field", "organizations.org_name_search_field", "organizations.address");

    public static class ForJoin extends DealsDataSource {
        @Nullable
        public /* bridge */ /* synthetic */ BaseDatasourceEntity deflateCursor(@NonNull Cursor cursor) {
            return super.deflateCursor(cursor);
        }

        @NonNull
        protected /* bridge */ /* synthetic */ ContentValues getContentValues(@NonNull BaseDatasourceEntity baseDatasourceEntity) {
            return super.getContentValues((Deal) baseDatasourceEntity);
        }

        public ForJoin(SQLiteDatabase dbConnection) {
            super(dbConnection);
        }

        @NonNull
        public String[] getAllColumns() {
            return (String[]) Arrays.copyOfRange(DealsDataSource.ALL_COLUMNS, 1, DealsDataSource.ALL_COLUMNS.length);
        }
    }

    public DealsDataSource(SQLiteDatabase dbConnection) {
        super(dbConnection);
    }

    @NonNull
    private synchronized ForJoin getDealsDataSourceForJoinLazy() {
        if (this.dealsDataSourceForJoin == null) {
            this.dealsDataSourceForJoin = new ForJoin(getTransactionalDBConnection());
        }
        return this.dealsDataSourceForJoin;
    }

    @NonNull
    private synchronized com.pipedrive.datasource.PersonsDataSource.ForJoin getPersonsDataSourceForJoinLazy() {
        if (this.personsDataSourceForJoin == null) {
            this.personsDataSourceForJoin = new com.pipedrive.datasource.PersonsDataSource.ForJoin(getTransactionalDBConnection());
        }
        return this.personsDataSourceForJoin;
    }

    @NonNull
    private synchronized com.pipedrive.datasource.OrganizationsDataSource.ForJoin getOrganizationsDataSourceForJoinLazy() {
        if (this.organizationsDataSourceForJoin == null) {
            this.organizationsDataSourceForJoin = new com.pipedrive.datasource.OrganizationsDataSource.ForJoin(getTransactionalDBConnection());
        }
        return this.organizationsDataSourceForJoin;
    }

    public void deleteDeal(int pipedriveId) {
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String str = "deals";
        String str2 = "d_pd_id = " + pipedriveId;
        if (transactionalDBConnection instanceof SQLiteDatabase) {
            SQLiteInstrumentation.delete(transactionalDBConnection, str, str2, null);
        } else {
            transactionalDBConnection.delete(str, str2, null);
        }
    }

    @Nullable
    public Deal findDealByDealId(long dealId) {
        return (Deal) findByPipedriveId(dealId);
    }

    @NonNull
    protected String getColumnNameForPipedriveId() {
        return PipeSQLiteHelper.COLUMN_DEALS_PIPEDRIVE_ID;
    }

    @NonNull
    protected String getColumnNameForSqlId() {
        return PipeSQLiteHelper.COLUMN_ID;
    }

    @NonNull
    protected String getTableName() {
        return "deals";
    }

    @NonNull
    protected ContentValues getContentValues(@NonNull Deal deal) {
        String str;
        ContentValues values = new ContentValues();
        if (deal.getSqlId() != 0) {
            values.put(PipeSQLiteHelper.COLUMN_ID, Long.valueOf(deal.getSqlId()));
        }
        if (deal.isExisting()) {
            values.put(PipeSQLiteHelper.COLUMN_DEALS_PIPEDRIVE_ID, Integer.valueOf(deal.getPipedriveId()));
        }
        values.put(PipeSQLiteHelper.COLUMN_DEALS_TITLE, deal.getTitle());
        values.put(PipeSQLiteHelper.COLUMN_DEALS_TITLE_SEARCH_FIELD, StringUtils.getNormalizedDealTitle(deal));
        values.put(PipeSQLiteHelper.COLUMN_DEALS_VALUE, Double.valueOf(deal.getValue()));
        values.put(PipeSQLiteHelper.COLUMN_DEALS_CURRENCY, deal.getCurrencyCode());
        values.put(PipeSQLiteHelper.COLUMN_DEALS_STAGE, Integer.valueOf(deal.getStage()));
        String str2 = PipeSQLiteHelper.COLUMN_DEALS_STATUS;
        if (deal.getStatus() == null) {
            str = "";
        } else {
            str = deal.getStatus().toString();
        }
        values.put(str2, str);
        values.put(PipeSQLiteHelper.COLUMN_DEALS_PIPELINE_ID, Integer.valueOf(deal.getPipelineId()));
        values.put(PipeSQLiteHelper.COLUMN_DEALS_NEXT_TASK_DATE, deal.getNextActivityDateStr());
        values.put(PipeSQLiteHelper.COLUMN_DEALS_NEXT_TASK_TIME, deal.getNextActivityTime());
        values.put(PipeSQLiteHelper.COLUMN_DEALS_UNDONE_ACTIVITIES_COUNT, Integer.valueOf(deal.getUndoneActivitiesCount()));
        values.put(PipeSQLiteHelper.COLUMN_DEALS_FORMATTED_VALUE, deal.getFormattedValue());
        values.put(PipeSQLiteHelper.COLUMN_DEALS_DEAL_OWNER_PD_ID, Integer.valueOf(deal.getOwnerPipedriveId()));
        values.put(PipeSQLiteHelper.COLUMN_DEALS_ROTTEN_TIME, deal.getRottenDateTime());
        Person person = deal.getPerson();
        if (person == null || person.getSqlId() <= 0) {
            values.putNull(PipeSQLiteHelper.COLUMN_DEALS_PERSON_SQL_ID);
        } else {
            values.put(PipeSQLiteHelper.COLUMN_DEALS_PERSON_SQL_ID, Long.valueOf(person.getSqlId()));
        }
        Organization organization = deal.getOrganization();
        if (organization == null || organization.getSqlId() <= 0) {
            values.putNull(PipeSQLiteHelper.COLUMN_DEALS_ORGANIZATION_SQL_ID);
        } else {
            values.put(PipeSQLiteHelper.COLUMN_DEALS_ORGANIZATION_SQL_ID, Long.valueOf(organization.getSqlId()));
        }
        str2 = PipeSQLiteHelper.COLUMN_DEALS_CUSTOM_FIELDS;
        JSONArray customFields = deal.getCustomFields();
        values.put(str2, !(customFields instanceof JSONArray) ? customFields.toString() : JSONArrayInstrumentation.toString(customFields));
        values.put(PipeSQLiteHelper.COLUMN_DEALS_DROP_BOX_ADDRESS, deal.getDropBoxAddress());
        values.put(PipeSQLiteHelper.COLUMN_DEALS_VISIBLE_TO, Integer.valueOf(deal.getVisibleTo()));
        values.put(PipeSQLiteHelper.COLUMN_DEALS_OWNER_NAME, deal.getOwnerName());
        values.put(PipeSQLiteHelper.COLUMN_DEALS_EXPECTED_CLOSE_DATE, deal.getExpectedCloseDate());
        values.put(PipeSQLiteHelper.COLUMN_DEALS_ADD_TIME, deal.getAddTime());
        values.put(PipeSQLiteHelper.COLUMN_DEALS_UPDATE_TIME, deal.getUpdateTime());
        values.put(PipeSQLiteHelper.COLUMN_DEALS_WON_TIME, deal.getWonTime());
        values.put(PipeSQLiteHelper.COLUMN_DEALS_LOST_TIME, deal.getLostTime());
        values.put(PipeSQLiteHelper.COLUMN_DEALS_PRODUCT_COUNT, deal.getProductCount());
        return values;
    }

    @NonNull
    public String[] getAllColumns() {
        return ALL_COLUMNS;
    }

    @Nullable
    public Deal deflateCursor(@NonNull Cursor cursor) {
        return DealsContentProvider.dealsTableCursorToDeal(getTransactionalDBConnection(), cursor);
    }

    private static String getSortOrderForPipeline() {
        String date = PipeSQLiteHelper.COLUMN_DEALS_NEXT_TASK_DATE;
        String time = PipeSQLiteHelper.COLUMN_DEALS_NEXT_TASK_TIME;
        String title = PipeSQLiteHelper.COLUMN_DEALS_TITLE;
        return "CASE WHEN d_next_task_date IS NULL THEN     strftime('%Y-%m-%d %H:%M:%S', datetime('now', 'start of day', '+1 day', '-1 second')) ELSE     CASE WHEN d_next_task_time IS NULL THEN         CASE WHEN date(d_next_task_date) < date('now') THEN             strftime('%Y-%m-%d %H:%M:%S', datetime(d_next_task_date, 'start of day', '+1 day', '-1 second'))         ELSE             CASE WHEN date(d_next_task_date) > date('now') THEN                 strftime('%Y-%m-%d %H:%M:%S', datetime(d_next_task_date, 'start of day'))             ELSE                 strftime('%Y-%m-%d %H:%M:%S', datetime('now', 'localtime'))             END         END     ELSE         strftime('%Y-%m-%d %H:%M:%S', datetime(d_next_task_date || ' ' || d_next_task_time, '+1 second', 'localtime'))     END END, d_title";
    }

    @NonNull
    public List<Deal> getSearchResults(@NonNull SearchConstraint searchConstraint) {
        return deflateCursorToList(getSearchCursor(searchConstraint));
    }

    @NonNull
    public Cursor getSearchCursor(@NonNull SearchConstraint searchConstraint) {
        return query("deals LEFT JOIN persons ON deals.d_person_id_sql = persons._id LEFT JOIN organizations ON deals.d_org_id_sql = organizations._id", new String[]{"deals._id", TextUtils.join(Table.COMMA_SEP, getDealsDataSourceForJoinLazy().getAllColumns()), TextUtils.join(Table.COMMA_SEP, getPersonsDataSourceForJoinLazy().getAllColumns()), TextUtils.join(Table.COMMA_SEP, getOrganizationsDataSourceForJoinLazy().getAllColumns())}, getSelectionForSearch(searchConstraint), "d_update_time DESC");
    }

    @NonNull
    public Cursor getDealsForStage(@NonNull Session session, long stageId, @Nullable Long filterId, @Nullable Long userId) {
        Cursor query;
        SelectionHolder selectionForPipeline = getSelectionForPipeline(stageId, filterId, userId, false, null);
        String tables = getTableName() + (filterId != null ? " LEFT JOIN filter_deals ON d_pd_id=filter_deals_pipedrive_id" : "");
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String[] strArr = new String[]{"deals._id", TextUtils.join(Table.COMMA_SEP, getDealsDataSourceForJoinLazy().getAllColumns())};
        String selection = selectionForPipeline.getSelection();
        String[] selectionArgs = selectionForPipeline.getSelectionArgs();
        String sortOrderForPipeline = getSortOrderForPipeline();
        if (transactionalDBConnection instanceof SQLiteDatabase) {
            query = SQLiteInstrumentation.query(transactionalDBConnection, tables, strArr, selection, selectionArgs, null, null, sortOrderForPipeline);
        } else {
            query = transactionalDBConnection.query(tables, strArr, selection, selectionArgs, null, null, sortOrderForPipeline);
        }
        query.setNotificationUri(session.getApplicationContext().getContentResolver(), new DealsContentProvider(session).createAllDealsUri());
        return query;
    }

    @Nullable
    public Pair<Integer, Float> getDealsValueForStage(@NonNull Session session, int stageId, @Nullable Long filterId, @Nullable Long userId) {
        Currency defaultCurrency = new CurrenciesDataSource(session.getDatabase()).getCurrencyByCode(session.getUserSettingsDefaultCurrencyCode());
        if (defaultCurrency == null) {
            return null;
        }
        Cursor dealsSumCursor = getDealsValueForStage(defaultCurrency, stageId, filterId, userId);
        try {
            dealsSumCursor.moveToFirst();
            Integer count = CursorHelper.getInteger(dealsSumCursor, COLUMN_NAME_COUNT);
            Float sum = (count == null || count.intValue() != 0) ? CursorHelper.getFloat(dealsSumCursor, COLUMN_NAME_SUM) : Float.valueOf(0.0f);
            dealsSumCursor.close();
            return new Pair(count, sum);
        } catch (Throwable th) {
            dealsSumCursor.close();
        }
    }

    private Cursor getDealsValueForStage(@NonNull Currency defaultCurrency, int stageId, @Nullable Long filterId, @Nullable Long userId) {
        boolean isCustomCurrencySetAsDefaultCurrency = defaultCurrency.isCustom();
        SelectionHolder selectionForPipeline = getSelectionForPipeline((long) stageId, filterId, userId, true, isCustomCurrencySetAsDefaultCurrency ? defaultCurrency.getCode() : null);
        String[] columns = isCustomCurrencySetAsDefaultCurrency ? new String[]{"SUM(d_value) AS sum", "COUNT(*) AS count"} : new String[]{"SUM(d_value / currencies.currency_rate) AS sum", "COUNT(*) AS count"};
        String tables = getTableName() + " INNER JOIN " + "currencies" + " ON " + "currencies" + "." + PipeSQLiteHelper.COLUMN_CODE + "=" + PipeSQLiteHelper.COLUMN_DEALS_CURRENCY;
        if (filterId != null) {
            tables = tables + " LEFT JOIN filter_deals ON d_pd_id=filter_deals_pipedrive_id";
        }
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String selection = selectionForPipeline.getSelection();
        String[] selectionArgs = selectionForPipeline.getSelectionArgs();
        if (transactionalDBConnection instanceof SQLiteDatabase) {
            return SQLiteInstrumentation.query(transactionalDBConnection, tables, columns, selection, selectionArgs, null, null, null);
        }
        return transactionalDBConnection.query(tables, columns, selection, selectionArgs, null, null, null);
    }

    @NonNull
    public Double getDealValueSumForAggregatedCardInDefaultCurrency(@NonNull Currency defaultCurrency, @NonNull List<Long> sqlIdList) {
        String code;
        boolean isCustomCurrencySetAsDefaultCurrency = defaultCurrency.isCustom();
        String[] columns = isCustomCurrencySetAsDefaultCurrency ? new String[]{"SUM(d_value) AS sum"} : new String[]{"SUM(d_value / currencies.currency_rate) AS sum"};
        String tables = getTableName() + " INNER JOIN " + "currencies" + " ON " + "currencies" + "." + PipeSQLiteHelper.COLUMN_CODE + "=" + PipeSQLiteHelper.COLUMN_DEALS_CURRENCY;
        if (isCustomCurrencySetAsDefaultCurrency) {
            code = defaultCurrency.getCode();
        } else {
            code = null;
        }
        SelectionHolder selectionHolder = getSelectionForAggregatedSumValueInDefaultCurrency(sqlIdList, code);
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String selection = selectionHolder.getSelection();
        String[] selectionArgs = selectionHolder.getSelectionArgs();
        Cursor cursor = !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.query(tables, columns, selection, selectionArgs, null, null, null) : SQLiteInstrumentation.query(transactionalDBConnection, tables, columns, selection, selectionArgs, null, null, null);
        Double sum = Double.valueOf(0.0d);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            sum = Double.valueOf(cursor.getDouble(cursor.getColumnIndex(COLUMN_NAME_SUM)));
        }
        cursor.close();
        return sum;
    }

    @NonNull
    private SelectionHolder getSelectionForPipeline(long stageId, @Nullable Long filterId, @Nullable Long userId, boolean isSummaryCalculation, @Nullable String customCurrencyCode) {
        StringBuilder selection = new StringBuilder("d_stage=?");
        String[] selectionArgs = new String[]{String.valueOf(stageId)};
        boolean isFilterSelected = filterId != null;
        boolean isUserSelected = userId != null;
        if (isFilterSelected) {
            selection.append(" AND ").append(PipeSQLiteHelper.COLUMN_FILTER_DEALS_FILTER_PIPEDRIVE_ID).append("=?");
            selectionArgs = DatabaseUtils.appendSelectionArgs(selectionArgs, new String[]{String.valueOf(filterId)});
        } else if (isUserSelected) {
            selection.append(" AND ").append(PipeSQLiteHelper.COLUMN_DEALS_DEAL_OWNER_PD_ID).append("=?").append(" AND ").append(PipeSQLiteHelper.COLUMN_DEALS_STATUS).append("=?");
            selectionArgs = DatabaseUtils.appendSelectionArgs(selectionArgs, new String[]{String.valueOf(userId), DealStatus.OPEN.toString()});
        } else {
            selection.append(" AND ").append(PipeSQLiteHelper.COLUMN_DEALS_STATUS).append("=?");
            selectionArgs = DatabaseUtils.appendSelectionArgs(selectionArgs, new String[]{DealStatus.OPEN.toString()});
        }
        if (isSummaryCalculation) {
            boolean isCustomCurrencySetAsDefaultCurrency = customCurrencyCode != null;
            int isCustomCurrencyArg = isCustomCurrencySetAsDefaultCurrency ? 1 : 0;
            selection.append(" AND ").append("currencies").append(".").append(PipeSQLiteHelper.COLUMN_IS_CUSTOM).append("=?");
            selectionArgs = DatabaseUtils.appendSelectionArgs(selectionArgs, new String[]{String.valueOf(isCustomCurrencyArg)});
            if (isCustomCurrencySetAsDefaultCurrency) {
                selection.append(" AND ").append(PipeSQLiteHelper.COLUMN_DEALS_CURRENCY).append("=?");
                selectionArgs = DatabaseUtils.appendSelectionArgs(selectionArgs, new String[]{customCurrencyCode});
            }
        }
        return new SelectionHolder(selection.toString(), selectionArgs);
    }

    private int getOpenDealCountForItem(@NonNull Long itemSqlId, @NonNull String relatedItemColumn) {
        String[] columns = new String[]{"COUNT(*)"};
        String selection = "d_status =? AND " + relatedItemColumn + " =?";
        String[] selectionArgs = new String[]{String.valueOf(DealStatus.OPEN), String.valueOf(itemSqlId)};
        String tableName = getTableName();
        Cursor cursor = !(this instanceof SQLiteDatabase) ? query(tableName, columns, selection, selectionArgs, null, null, null) : SQLiteInstrumentation.query((SQLiteDatabase) this, tableName, columns, selection, selectionArgs, null, null, null);
        int i = 0;
        try {
            cursor.moveToFirst();
            i = cursor.getInt(0);
            return i;
        } finally {
            cursor.close();
        }
    }

    @NonNull
    public Observable<Integer> getOpenDealCountForItemRx(@NonNull Long sqlId, @NonNull final String relatedItemColumn) {
        return Observable.just(sqlId).subscribeOn(Schedulers.io()).map(new Func1<Long, Integer>() {
            public Integer call(Long id) {
                return Integer.valueOf(DealsDataSource.this.getOpenDealCountForItem(id, relatedItemColumn));
            }
        });
    }

    @NonNull
    public SelectionHolder getSelectionForSearch(@NonNull SearchConstraint searchConstraint) {
        return new SelectionHolder("deals.d_status !=?", new String[]{String.valueOf(DealStatus.DELETED)}).add(this.searchHelper.getSelectionHolderForSearchConstraint(searchConstraint));
    }

    @NonNull
    private SelectionHolder getSelectionForAggregatedSumValueInDefaultCurrency(@NonNull List<Long> sqlIdList, @Nullable String customCurrencyCode) {
        boolean isCustomCurrencySetAsDefaultCurrency;
        int i;
        if (customCurrencyCode != null) {
            isCustomCurrencySetAsDefaultCurrency = true;
        } else {
            isCustomCurrencySetAsDefaultCurrency = false;
        }
        String selection = getTableName() + "." + PipeSQLiteHelper.COLUMN_ID + " IN (" + sqlIdList.toString().substring(1, sqlIdList.toString().length() - 1) + ") " + "AND " + "currencies" + "." + PipeSQLiteHelper.COLUMN_IS_CUSTOM + "= ? ";
        String[] selectionArgs = new String[1];
        if (isCustomCurrencySetAsDefaultCurrency) {
            i = 1;
        } else {
            i = 0;
        }
        selectionArgs[0] = String.valueOf(i);
        if (isCustomCurrencySetAsDefaultCurrency) {
            selection = selection + " AND " + "currencies" + "." + PipeSQLiteHelper.COLUMN_CODE + "=?";
            selectionArgs = DatabaseUtils.appendSelectionArgs(selectionArgs, new String[]{customCurrencyCode});
        }
        return new SelectionHolder(selection, selectionArgs);
    }
}
