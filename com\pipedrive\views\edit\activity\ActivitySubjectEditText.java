package com.pipedrive.views.edit.activity;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import com.pipedrive.R;
import com.pipedrive.views.common.EditTextWithUnderlineAndLabel.OnValueChangedListener;
import com.pipedrive.views.common.TitleEditText;

public class ActivitySubjectEditText extends TitleEditText {

    public interface OnSubjectChange {
        void onNewSubjectAdded();

        void onSubjectCleared();
    }

    public ActivitySubjectEditText(Context context) {
        this(context, null);
    }

    public ActivitySubjectEditText(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ActivitySubjectEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
    }

    public void setOnSubjectChange(@Nullable final OnSubjectChange onSubjectChange) {
        super.setOnValueChangedListener(new OnValueChangedListener() {
            int previousSubjectTextLength = ActivitySubjectEditText.this.length();

            public void onValueChanged(@NonNull String value) {
                if (onSubjectChange != null) {
                    boolean subjectCleared;
                    boolean newSubjectAdded;
                    int currentSubjectTextLength = value.length();
                    if (currentSubjectTextLength == 0) {
                        subjectCleared = true;
                    } else {
                        subjectCleared = false;
                    }
                    if (this.previousSubjectTextLength != 0 || currentSubjectTextLength <= 0) {
                        newSubjectAdded = false;
                    } else {
                        newSubjectAdded = true;
                    }
                    this.previousSubjectTextLength = value.length();
                    if (subjectCleared) {
                        onSubjectChange.onSubjectCleared();
                    }
                    if (newSubjectAdded) {
                        onSubjectChange.onNewSubjectAdded();
                    }
                }
            }
        });
    }

    private void initView() {
        EditText subjectEditText = (EditText) getMainView();
        subjectEditText.setLayoutParams(new LayoutParams(-1, -1));
        subjectEditText.setHint(R.string.hint_subject);
        subjectEditText.setSingleLine(false);
        subjectEditText.setMaxLines(3);
        subjectEditText.setGravity(16);
        subjectEditText.setPadding(0, getResources().getDimensionPixelSize(R.dimen.dimen2), getResources().getDimensionPixelSize(R.dimen.dimen11), 0);
    }
}
