package com.zendesk.sdk.support;

import com.zendesk.sdk.model.helpcenter.HelpCenterSearch.Builder;
import com.zendesk.sdk.model.helpcenter.SearchArticle;
import com.zendesk.sdk.model.settings.SafeMobileSettings;
import com.zendesk.sdk.network.HelpCenterProvider;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.support.SupportMvp.Model;
import com.zendesk.service.ZendeskCallback;
import java.util.List;

class SupportModel implements Model {
    private HelpCenterProvider provider = ZendeskConfig.INSTANCE.provider().helpCenterProvider();

    SupportModel() {
    }

    public void search(List<Long> categoryIds, List<Long> sectionIds, String query, String[] labelNames, ZendeskCallback<List<SearchArticle>> callback) {
        this.provider.searchArticles(new Builder().withQuery(query).withCategoryIds(categoryIds).withSectionIds(sectionIds).withLabelNames(labelNames).build(), callback);
    }

    public void getSettings(ZendeskCallback<SafeMobileSettings> callback) {
        if (ZendeskConfig.INSTANCE.storage().sdkSettingsStorage().hasStoredSdkSettings()) {
            callback.onSuccess(ZendeskConfig.INSTANCE.storage().sdkSettingsStorage().getStoredSettings());
        } else {
            ZendeskConfig.INSTANCE.provider().sdkSettingsProvider().getSettings(callback);
        }
    }
}
