package com.zendesk.sdk.storage;

import android.support.annotation.NonNull;
import com.zendesk.sdk.model.request.fields.TicketForm;
import com.zendesk.util.CollectionUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class ZendeskRequestSessionCache implements RequestSessionCache {
    private final Map<Long, TicketForm> cachedTicketForms = new HashMap();

    ZendeskRequestSessionCache() {
    }

    public void updateTicketFormCache(@NonNull List<TicketForm> ticketForms) {
        List<TicketForm> ensuredTicketFormList = CollectionUtils.ensureEmpty(ticketForms);
        Map<Long, TicketForm> ticketFormMap = new HashMap();
        for (TicketForm ticketForm : ensuredTicketFormList) {
            ticketFormMap.put(Long.valueOf(ticketForm.getId()), ticketForm);
        }
        synchronized (this.cachedTicketForms) {
            this.cachedTicketForms.putAll(ticketFormMap);
        }
    }

    @NonNull
    public synchronized List<TicketForm> getTicketFormsById(@NonNull List<Long> ticketFormIds) {
        List<TicketForm> ticketForms;
        ticketForms = new ArrayList();
        List<Long> ensuredTicketFormIdList = CollectionUtils.ensureEmpty(ticketFormIds);
        synchronized (this.cachedTicketForms) {
            for (Long id : ensuredTicketFormIdList) {
                ticketForms.add(this.cachedTicketForms.get(id));
            }
        }
        return ticketForms;
    }

    public boolean containsAllTicketForms(@NonNull List<Long> ticketFormIds) {
        List<Long> ensuredTicketFormIdList = CollectionUtils.ensureEmpty(ticketFormIds);
        boolean contains = true;
        synchronized (this.cachedTicketForms) {
            for (Long id : ensuredTicketFormIdList) {
                if (!this.cachedTicketForms.containsKey(id)) {
                    contains = false;
                    break;
                }
            }
        }
        return contains;
    }
}
