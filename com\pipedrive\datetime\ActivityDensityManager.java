package com.pipedrive.datetime;

import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.support.annotation.WorkerThread;
import android.util.Pair;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.datasource.ActivitiesDataSource.OnActivityRetrieved;
import com.pipedrive.model.Activity;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import rx.Observable;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class ActivityDensityManager {
    private static ActivityDensityManager instance;
    @NonNull
    private final HashMap<Long, Double> cachedDensities = new HashMap();
    @NonNull
    private final HashMap<Long, ActivityDensityTask> runningTasks = new HashMap();
    @NonNull
    private final Scheduler threadPoolExecutorScheduler = Schedulers.from(Executors.newCachedThreadPool());

    private static class ActivityDensityTask {
        private PipedriveDateTime dateTime;
        private WeakReference<View> weakReference;

        ActivityDensityTask(@NonNull PipedriveDateTime dateTime, @NonNull View view) {
            this.dateTime = dateTime;
            this.weakReference = new WeakReference(view);
        }
    }

    private static class Period {
        @NonNull
        private PipedriveDateTime end;
        @NonNull
        private PipedriveDateTime start;

        Period(@NonNull PipedriveDateTime start, @NonNull PipedriveDateTime end) {
            this.start = start;
            this.end = end;
        }

        float getDurationHours() {
            return ((float) (this.end.getTime() - this.start.getTime())) / Long.valueOf(TimeUnit.HOURS.toMillis(1)).floatValue();
        }

        boolean extendIfOverlap(@NonNull PipedriveDateTime start, @NonNull PipedriveDateTime end) {
            if ((this.start.getTime() < start.getTime() || this.start.getTime() > end.getTime()) && (this.end.getTime() < start.getTime() || this.end.getTime() > end.getTime())) {
                return false;
            }
            long newStartMillis = Math.min(this.start.getTime(), start.getTime());
            long newEndMillis = Math.max(this.end.getTime(), end.getTime());
            this.start = PipedriveDateTime.instanceFromUnixTimeRepresentation(Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(newStartMillis)));
            this.end = PipedriveDateTime.instanceFromUnixTimeRepresentation(Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(newEndMillis)));
            return true;
        }

        boolean extendIfOverlap(@NonNull Period another) {
            return extendIfOverlap(another.start, another.end);
        }
    }

    private ActivityDensityManager() {
    }

    public static ActivityDensityManager getInstance() {
        if (instance == null) {
            synchronized (ActivityDensityTask.class) {
                if (instance == null) {
                    instance = new ActivityDensityManager();
                }
            }
        }
        return instance;
    }

    public synchronized void displayDensity(@NonNull Session session, @NonNull PipedriveDateTime dateTime, @NonNull View indicatorView) {
        displayDensity(session, new ActivityDensityTask(dateTime, indicatorView));
    }

    private synchronized void displayDensity(@NonNull final Session session, @NonNull ActivityDensityTask task) {
        final long taskId = task.dateTime.getTime();
        if (this.cachedDensities.containsKey(Long.valueOf(taskId))) {
            displayCachedValue(task, taskId);
        } else if (this.runningTasks.containsKey(Long.valueOf(taskId))) {
            ((ActivityDensityTask) this.runningTasks.get(Long.valueOf(taskId))).weakReference = task.weakReference;
        } else {
            this.runningTasks.put(Long.valueOf(taskId), task);
            Observable.just(task).subscribeOn(this.threadPoolExecutorScheduler).map(new Func1<ActivityDensityTask, Pair<ActivityDensityTask, Double>>() {
                public Pair<ActivityDensityTask, Double> call(ActivityDensityTask task) {
                    return new Pair(task, Double.valueOf(ActivityDensityManager.this.getTotalActivityDurationHours(session, task.dateTime)));
                }
            }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Pair<ActivityDensityTask, Double>>() {
                public void call(Pair<ActivityDensityTask, Double> pair) {
                    ActivityDensityManager.this.cachedDensities.put(Long.valueOf(((ActivityDensityTask) pair.first).dateTime.getTime()), pair.second);
                    ActivityDensityManager.this.runningTasks.remove(Long.valueOf(taskId));
                    ActivityDensityManager.this.updateViewForDensity((ActivityDensityTask) pair.first, (Double) pair.second);
                }
            });
        }
    }

    @UiThread
    private void displayCachedValue(@NonNull ActivityDensityTask task, long dateMillis) {
        updateViewForDensity(task, (Double) this.cachedDensities.get(Long.valueOf(dateMillis)));
    }

    @UiThread
    private synchronized void updateViewForDensity(@NonNull ActivityDensityTask task, Double densityHours) {
        int widthDips;
        boolean lessThan4HoursOfActivitiesInDay = densityHours.doubleValue() <= 4.0d;
        boolean lessThan6HoursOfActivitiesInDay = densityHours.doubleValue() <= 6.0d;
        boolean lessThan8HoursOfActivitiesInDay = densityHours.doubleValue() <= 8.0d;
        boolean lessThan12HoursOfActivitiesInDay = densityHours.doubleValue() <= 12.0d;
        if (lessThan4HoursOfActivitiesInDay) {
            widthDips = (int) (Math.ceil(densityHours.doubleValue()) * 2.0d);
        } else if (lessThan6HoursOfActivitiesInDay) {
            widthDips = 10;
        } else if (lessThan8HoursOfActivitiesInDay) {
            widthDips = 12;
        } else if (lessThan12HoursOfActivitiesInDay) {
            widthDips = 14;
        } else {
            widthDips = 16;
        }
        View view = (View) task.weakReference.get();
        if (!(view == null)) {
            LayoutParams layoutParams = view.getLayoutParams();
            layoutParams.width = (int) TypedValue.applyDimension(1, (float) widthDips, view.getResources().getDisplayMetrics());
            view.setLayoutParams(layoutParams);
        }
    }

    @WorkerThread
    double getTotalActivityDurationHours(@NonNull Session session, @NonNull final PipedriveDateTime date) {
        final ArrayList<Period> periods = new ArrayList();
        new ActivitiesDataSource(session).getNonAllDayActiveActivitiesForDateForAuthenticatedUserOnly(date.toCalendar(), new OnActivityRetrieved() {
            public void activity(@NonNull Activity activity) {
                PipedriveDateTime startTimestamp = activity.getActivityStartAsDateTime();
                if (startTimestamp != null) {
                    if (!startTimestamp.isDayOfMonthSameAs(date)) {
                        startTimestamp = date.resetTimeLocal();
                    }
                    PipedriveDateTime endTimestamp = activity.getActivityEnd();
                    if (endTimestamp == null) {
                        endTimestamp = PipedriveDateTime.instanceFromUnixTimeRepresentation(Long.valueOf(startTimestamp.getRepresentationInUnixTime() + Activity.DEFAULT_ACTIVITY_DURATION_SECONDS));
                    }
                    if (!endTimestamp.isDayOfMonthSameAs(date)) {
                        endTimestamp = date.setTimeToEndOfTheDayLocal();
                    }
                    boolean belongsToAnyPeriod = false;
                    Iterator it = periods.iterator();
                    while (it.hasNext()) {
                        if (((Period) it.next()).extendIfOverlap(startTimestamp, endTimestamp)) {
                            belongsToAnyPeriod = true;
                        }
                    }
                    if (!belongsToAnyPeriod) {
                        periods.add(new Period(startTimestamp, endTimestamp));
                    }
                }
            }
        });
        return (double) getPeriodsDurationHours(foldPeriods(periods));
    }

    private ArrayList<Period> foldPeriods(@NonNull ArrayList<Period> periods) {
        if (!periods.isEmpty()) {
            while (true) {
                Period period = null;
                for (int i = 0; i < periods.size() - 1; i++) {
                    for (int j = i + 1; j < periods.size(); j++) {
                        if (((Period) periods.get(j)).extendIfOverlap((Period) periods.get(i))) {
                            period = (Period) periods.get(i);
                            break;
                        }
                    }
                    if (period != null) {
                        break;
                    }
                }
                if (period == null) {
                    break;
                }
                periods.remove(period);
            }
        }
        return periods;
    }

    private float getPeriodsDurationHours(@NonNull ArrayList<Period> periods) {
        float total = 0.0f;
        Iterator it = periods.iterator();
        while (it.hasNext()) {
            total += ((Period) it.next()).getDurationHours();
        }
        return total;
    }

    public synchronized void clearDensityCache() {
        this.cachedDensities.clear();
    }
}
