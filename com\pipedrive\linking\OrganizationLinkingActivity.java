package com.pipedrive.linking;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import com.pipedrive.R;
import com.pipedrive.adapter.BaseSectionedListAdapter;
import com.pipedrive.adapter.OrganizationListAdapter;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.organization.edit.OrganizationCreateMinimalActivity;

public class OrganizationLinkingActivity extends BaseLinkingActivity {
    private static final int GET_NEW_ORG_REQUEST_CODE = 1;
    public static final String KEY_ORG_SQL_ID = "ORG_SQL_ID";

    public static void startActivityForResult(@NonNull Activity context, int requestCode) {
        ActivityCompat.startActivityForResult(context, new Intent(context, OrganizationLinkingActivity.class), requestCode, null);
    }

    public int getSearchHint() {
        return R.string.search_organizations;
    }

    @Nullable
    public Drawable getFabIcon() {
        return ContextCompat.getDrawable(this, R.drawable.icon_add);
    }

    public Cursor getCursor() {
        return new OrganizationsDataSource(getSession().getDatabase()).getSearchCursor(this.mSearchConstraint);
    }

    @NonNull
    public String getKey() {
        return KEY_ORG_SQL_ID;
    }

    @NonNull
    public BaseSectionedListAdapter getAdapter() {
        return new OrganizationListAdapter(this, getCursor(), getSession());
    }

    void onAddClicked() {
        OrganizationCreateMinimalActivity.startActivityForResult(this, this.mSearchConstraint.toString(), 1);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == -1) {
            setSelectedItemSqlId(data.getLongExtra(OrganizationActivity.KEY_ORGANIZATION_SQL_ID, 0));
        }
    }
}
