package rx;

import java.util.concurrent.Callable;
import rx.exceptions.Exceptions;

class Single$17 implements Single$OnSubscribe<T> {
    final /* synthetic */ Callable val$singleFactory;

    Single$17(Callable callable) {
        this.val$singleFactory = callable;
    }

    public void call(SingleSubscriber<? super T> singleSubscriber) {
        try {
            ((Single) this.val$singleFactory.call()).subscribe(singleSubscriber);
        } catch (Throwable t) {
            Exceptions.throwIfFatal(t);
            singleSubscriber.onError(t);
        }
    }
}
