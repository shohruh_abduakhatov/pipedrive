package com.pipedrive.customfields.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.customfields.views.CustomFieldListView;
import com.pipedrive.customfields.views.PersonCustomFieldListView;

public class PersonCustomFieldListFragment extends CustomFieldListFragment {
    private static final String KEY_PERSON_SQL_ID = "PERSON_SQL_ID";

    public static PersonCustomFieldListFragment newInstance(@NonNull Long personSqlId) {
        Bundle args = new Bundle();
        args.putSerializable("PERSON_SQL_ID", personSqlId);
        PersonCustomFieldListFragment fragment = new PersonCustomFieldListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    protected CustomFieldListView createCustomFieldListView(@NonNull Context context) {
        return new PersonCustomFieldListView(context);
    }

    @Nullable
    protected Long getSqlId() {
        return (Long) getArguments().getSerializable("PERSON_SQL_ID");
    }
}
