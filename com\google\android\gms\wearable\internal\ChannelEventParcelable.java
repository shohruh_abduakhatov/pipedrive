package com.google.android.gms.wearable.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.Log;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.wearable.ChannelApi.ChannelListener;

public final class ChannelEventParcelable extends AbstractSafeParcelable {
    public static final Creator<ChannelEventParcelable> CREATOR = new zzn();
    final int aTn;
    final int aTo;
    final ChannelImpl aTp;
    final int mVersionCode;
    final int type;

    ChannelEventParcelable(int i, ChannelImpl channelImpl, int i2, int i3, int i4) {
        this.mVersionCode = i;
        this.aTp = channelImpl;
        this.type = i2;
        this.aTn = i3;
        this.aTo = i4;
    }

    private static String zzadr(int i) {
        switch (i) {
            case 1:
                return "CHANNEL_OPENED";
            case 2:
                return "CHANNEL_CLOSED";
            case 3:
                return "INPUT_CLOSED";
            case 4:
                return "OUTPUT_CLOSED";
            default:
                return Integer.toString(i);
        }
    }

    private static String zzads(int i) {
        switch (i) {
            case 0:
                return "CLOSE_REASON_NORMAL";
            case 1:
                return "CLOSE_REASON_DISCONNECTED";
            case 2:
                return "CLOSE_REASON_REMOTE_CLOSE";
            case 3:
                return "CLOSE_REASON_LOCAL_CLOSE";
            default:
                return Integer.toString(i);
        }
    }

    public String toString() {
        int i = this.mVersionCode;
        String valueOf = String.valueOf(this.aTp);
        String valueOf2 = String.valueOf(zzadr(this.type));
        String valueOf3 = String.valueOf(zzads(this.aTn));
        return new StringBuilder(((String.valueOf(valueOf).length() + 104) + String.valueOf(valueOf2).length()) + String.valueOf(valueOf3).length()).append("ChannelEventParcelable[versionCode=").append(i).append(", channel=").append(valueOf).append(", type=").append(valueOf2).append(", closeReason=").append(valueOf3).append(", appErrorCode=").append(this.aTo).append("]").toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzn.zza(this, parcel, i);
    }

    public void zza(ChannelListener channelListener) {
        switch (this.type) {
            case 1:
                channelListener.onChannelOpened(this.aTp);
                return;
            case 2:
                channelListener.onChannelClosed(this.aTp, this.aTn, this.aTo);
                return;
            case 3:
                channelListener.onInputClosed(this.aTp, this.aTn, this.aTo);
                return;
            case 4:
                channelListener.onOutputClosed(this.aTp, this.aTn, this.aTo);
                return;
            default:
                Log.w("ChannelEventParcelable", "Unknown type: " + this.type);
                return;
        }
    }
}
