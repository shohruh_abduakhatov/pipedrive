package rx;

import rx.functions.Func6;
import rx.functions.FuncN;

class Single$7 implements FuncN<R> {
    final /* synthetic */ Func6 val$zipFunction;

    Single$7(Func6 func6) {
        this.val$zipFunction = func6;
    }

    public R call(Object... args) {
        return this.val$zipFunction.call(args[0], args[1], args[2], args[3], args[4], args[5]);
    }
}
