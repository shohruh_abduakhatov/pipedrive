package com.zendesk.sdk.storage;

import android.support.annotation.NonNull;
import com.zendesk.sdk.network.impl.ApplicationScope;
import com.zendesk.sdk.util.BaseInjector;
import com.zendesk.sdk.util.DependencyProvider;
import com.zendesk.sdk.util.LibraryInjector;
import com.zendesk.sdk.util.ScopeCache;

public class StorageInjector {
    static SdkStorage injectSdkStorage(ApplicationScope applicationScope) {
        return new ZendeskSdkStorage();
    }

    public static SdkStorage injectCachedSdkStorage(ApplicationScope applicationScope) {
        return injectCachedStorageModule(applicationScope).getSdkStorage();
    }

    static RequestStorage injectRequestStorage(ApplicationScope applicationScope) {
        return new ZendeskRequestStorage(BaseInjector.injectApplicationContext(applicationScope));
    }

    public static RequestStorage injectCachedRequestStorage(ApplicationScope applicationScope) {
        return injectCachedStorageModule(applicationScope).getRequestStorage();
    }

    static SdkSettingsStorage injectSdkSettingsStorage(ApplicationScope applicationScope) {
        return new ZendeskSdkSettingsStorage(BaseInjector.injectApplicationContext(applicationScope), LibraryInjector.injectGson(applicationScope));
    }

    public static SdkSettingsStorage injectCachedSdkSettingsStorage(ApplicationScope applicationScope) {
        return injectCachedStorageModule(applicationScope).getSdkSettingsStorage();
    }

    static IdentityStorage injectIdentityStorage(ApplicationScope applicationScope) {
        return new ZendeskIdentityStorage(BaseInjector.injectApplicationContext(applicationScope), LibraryInjector.injectGson(applicationScope));
    }

    public static IdentityStorage injectCachedIdentityStorage(ApplicationScope applicationScope) {
        return injectCachedStorageModule(applicationScope).getIdentityStorage();
    }

    static HelpCenterSessionCache injectHelpCenterSessionCache(ApplicationScope applicationScope) {
        return new ZendeskHelpCenterSessionCache();
    }

    public static HelpCenterSessionCache injectCachedHelpCenterSessionCache(ApplicationScope applicationScope) {
        return injectCachedStorageModule(applicationScope).getHelpCenterSessionCache();
    }

    static PushRegistrationResponseStorage injectPushRegistrationResponseStorage(ApplicationScope applicationScope) {
        return new ZendeskPushRegistrationResponseStorage(BaseInjector.injectApplicationContext(applicationScope), LibraryInjector.injectGson(applicationScope));
    }

    public static PushRegistrationResponseStorage injectCachedPushRegistrationResponseStorage(ApplicationScope applicationScope) {
        return injectCachedStorageModule(applicationScope).getPushRegistrationResponseStorage();
    }

    static StorageModule injectStorageModule(ApplicationScope applicationScope) {
        return new StorageModule(injectSdkStorage(applicationScope), injectIdentityStorage(applicationScope), injectRequestStorage(applicationScope), injectSdkSettingsStorage(applicationScope), injectHelpCenterSessionCache(applicationScope), injectRequestSessionCache(applicationScope), injectPushRegistrationResponseStorage(applicationScope));
    }

    private static ScopeCache<StorageModule> injectStorageModuleCache(ApplicationScope applicationScope) {
        return applicationScope.getStorageModuleCache();
    }

    static StorageModule injectCachedStorageModule(final ApplicationScope applicationScope) {
        return (StorageModule) injectStorageModuleCache(applicationScope).get(new DependencyProvider<StorageModule>() {
            @NonNull
            public StorageModule provideDependency() {
                return StorageInjector.injectStorageModule(applicationScope);
            }
        });
    }

    public static StorageStore injectStorageStore(ApplicationScope applicationScope) {
        return new ZendeskStorageStore(injectCachedSdkStorage(applicationScope), injectCachedIdentityStorage(applicationScope), injectCachedRequestStorage(applicationScope), injectCachedSdkSettingsStorage(applicationScope), injectCachedHelpCenterSessionCache(applicationScope), injectCachedPushRegistrationResponseStorage(applicationScope));
    }

    public static StubStorageStore injectStubStorageStore(ApplicationScope applicationScope) {
        return new StubStorageStore();
    }

    static RequestSessionCache injectRequestSessionCache(ApplicationScope applicationScope) {
        return new ZendeskRequestSessionCache();
    }

    public static RequestSessionCache injectCachedRequestSessionCache(ApplicationScope applicationScope) {
        return injectCachedStorageModule(applicationScope).getRequestSessionCache();
    }
}
