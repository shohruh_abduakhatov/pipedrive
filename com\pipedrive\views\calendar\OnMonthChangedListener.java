package com.pipedrive.views.calendar;

import android.support.annotation.NonNull;
import java.util.Calendar;

public interface OnMonthChangedListener {
    void onMonthChanged(@NonNull Calendar calendar);
}
