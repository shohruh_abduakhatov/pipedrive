package com.pipedrive.linking;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.ActivityCompat;
import com.pipedrive.R;
import com.pipedrive.adapter.BaseSectionedListAdapter;
import com.pipedrive.adapter.PersonListAdapterForLinking;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.person.edit.PersonCreateMinimalActivity;

public class PersonLinkingActivity extends BaseLinkingActivity {
    private static final int GET_NEW_PERSON_REQUEST_CODE = 1;
    public static final String KEY_PERSON_SQL_ID = "PERSON_SQL_ID";

    public static void startActivityForResult(@NonNull Activity context, int requestCode) {
        ActivityCompat.startActivityForResult(context, new Intent(context, PersonLinkingActivity.class), requestCode, null);
    }

    public int getSearchHint() {
        return R.string.search_contacts;
    }

    public Drawable getFabIcon() {
        return VectorDrawableCompat.create(getResources(), R.drawable.ic_person_add_black, null);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == -1) {
            setSelectedItemSqlId(data.getLongExtra("PERSON_SQL_ID", 0));
        }
    }

    public Cursor getCursor() {
        return new PersonsDataSource(getSession().getDatabase()).getSearchCursor(this.mSearchConstraint);
    }

    @NonNull
    public String getKey() {
        return "PERSON_SQL_ID";
    }

    @NonNull
    public BaseSectionedListAdapter getAdapter() {
        return new PersonListAdapterForLinking(this, getCursor(), getSession());
    }

    void onAddClicked() {
        PersonCreateMinimalActivity.startActivityForResult(this, this.mSearchConstraint.toString(), 1);
    }
}
