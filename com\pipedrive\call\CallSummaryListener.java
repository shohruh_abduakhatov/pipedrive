package com.pipedrive.call;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.application.SessionStoreHelper;
import com.pipedrive.util.PipedriveUtil;

public class CallSummaryListener extends CallReceiver {
    private static final String PREFS_ACTIVITY_SQL_ID = "com.pipedrive.call.CallSummaryListener.PREFS_ACTIVITY_SQL_ID";
    private static final String PREFS_PERSON_SQL_ID = "com.pipedrive.call.CallSummaryListener.PREFS_PERSON_SQL_ID";
    private static final String PREFS_PHONE_NUMBER = "com.pipedrive.call.CallSummaryListener.PREFS_PHONE_NUMBER";
    private static final String PREFS_RELATED_DEAL_SQL_ID = "com.pipedrive.call.CallSummaryListener.PREFS_RELATED_DEAL_SQL_ID";

    public static class CallSummaryRegistration {
        @Nullable
        final Long mActivitySqlId;
        @Nullable
        final Long mPersonSqlId;
        @Nullable
        final String mPhoneNumber;
        @Nullable
        final Long mRelatedDealSqlId;

        public CallSummaryRegistration(@Nullable String phoneNumber, @Nullable Long personSqlId) {
            this(phoneNumber, personSqlId, null, null);
        }

        public CallSummaryRegistration(@Nullable String phoneNumber, @Nullable Long personSqlId, @Nullable Long relatedDealSqlId) {
            this(phoneNumber, personSqlId, relatedDealSqlId, null);
        }

        public CallSummaryRegistration(@Nullable String phoneNumber, @Nullable Long personSqlId, @Nullable Long relatedDealSqlId, @Nullable Long activitySqlId) {
            this.mPhoneNumber = phoneNumber;
            this.mPersonSqlId = personSqlId;
            this.mRelatedDealSqlId = relatedDealSqlId;
            this.mActivitySqlId = activitySqlId;
        }

        public String toString() {
            return "CallSummaryRegistration{mPersonSqlId=" + this.mPersonSqlId + ", mRelatedDealSqlId=" + this.mRelatedDealSqlId + ", mPhoneNumber='" + this.mPhoneNumber + ", mActivitySqlId=" + this.mActivitySqlId + '\'' + '}';
        }
    }

    public static boolean makeCallWithCallSummaryRegistration(@Nullable Session session, @Nullable Context nonApplicationContext, @NonNull CallSummaryRegistration callSummaryRegistration) {
        String phoneNumber = callSummaryRegistration.mPhoneNumber;
        if (session == null || !session.getSharedSession().isCallLoggingEnabled()) {
            if (nonApplicationContext == null || phoneNumber == null) {
                return false;
            }
            PipedriveUtil.call(nonApplicationContext, phoneNumber);
            return true;
        } else if (nonApplicationContext == null || phoneNumber == null) {
            return false;
        } else {
            registerCallSummaryListenerAndEnableCallListener(session, callSummaryRegistration);
            PipedriveUtil.call(nonApplicationContext, phoneNumber);
            return true;
        }
    }

    private static void enableCallReceiver(@NonNull Context context, boolean enabled) {
        CallReceiver.enableCallReceiver(context, CallSummaryListener.class, enabled);
    }

    private static void registerCallSummaryListenerAndEnableCallListener(@NonNull Session session, @NonNull CallSummaryRegistration callSummaryRegister) {
        SessionStoreHelper.Session.setSessionPrefsString(session, PREFS_PHONE_NUMBER, callSummaryRegister.mPhoneNumber);
        SessionStoreHelper.Session.setSessionPrefsLong(session, PREFS_PERSON_SQL_ID, callSummaryRegister.mPersonSqlId);
        SessionStoreHelper.Session.setSessionPrefsLong(session, PREFS_RELATED_DEAL_SQL_ID, callSummaryRegister.mRelatedDealSqlId);
        SessionStoreHelper.Session.setSessionPrefsLong(session, PREFS_ACTIVITY_SQL_ID, callSummaryRegister.mActivitySqlId);
        enableCallReceiver(session.getApplicationContext(), true);
    }

    private void clearRegistrationForCallSummaryAndDisableCallListener(@NonNull Session session) {
        SessionStoreHelper.Session.setSessionPrefsString(session, PREFS_PHONE_NUMBER, null);
        SessionStoreHelper.Session.setSessionPrefsLong(session, PREFS_PERSON_SQL_ID, null);
        enableCallReceiver(session.getApplicationContext(), false);
    }

    @Nullable
    private CallSummaryRegistration getRegistrationForCallSummary(@NonNull Session session) {
        String phoneNumber = SessionStoreHelper.Session.getSessionPrefsString(session, PREFS_PHONE_NUMBER, null);
        Long personSqlId = SessionStoreHelper.Session.getSessionPrefsLong(session, PREFS_PERSON_SQL_ID, null);
        Long relatedDealSqlId = SessionStoreHelper.Session.getSessionPrefsLong(session, PREFS_RELATED_DEAL_SQL_ID, null);
        Long activitySqlId = SessionStoreHelper.Session.getSessionPrefsLong(session, PREFS_ACTIVITY_SQL_ID, null);
        boolean registrationMissing = phoneNumber == null || personSqlId == null;
        if (registrationMissing) {
            return null;
        }
        return new CallSummaryRegistration(phoneNumber, personSqlId, relatedDealSqlId, activitySqlId);
    }

    void callFinished(@NonNull Context context, @Nullable Long callDurationInMillis, @Nullable Long callStartTimeInMillis) {
        boolean haveActiveSessionToReadRegisteredCalls;
        boolean noRegistrationForCallSummary = false;
        Session session = PipedriveApp.getActiveSession();
        if (session != null) {
            haveActiveSessionToReadRegisteredCalls = true;
        } else {
            haveActiveSessionToReadRegisteredCalls = false;
        }
        if (haveActiveSessionToReadRegisteredCalls) {
            CallSummaryRegistration registrationForCallSummary = getRegistrationForCallSummary(session);
            if (registrationForCallSummary == null || registrationForCallSummary.mPersonSqlId == null) {
                noRegistrationForCallSummary = true;
            }
            if (!noRegistrationForCallSummary) {
                clearRegistrationForCallSummaryAndDisableCallListener(session);
                RelatedActivityListActivity.startInNewTask(context, registrationForCallSummary.mPersonSqlId, registrationForCallSummary.mRelatedDealSqlId, registrationForCallSummary.mActivitySqlId, callDurationInMillis, callStartTimeInMillis);
                return;
            }
            return;
        }
        enableCallReceiver(context, false);
    }
}
