package com.pipedrive.views.viewholder.deal;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;
import com.pipedrive.views.ColoredImageView;

public class DealRowViewHolder_ViewBinding implements Unbinder {
    private DealRowViewHolder target;

    @UiThread
    public DealRowViewHolder_ViewBinding(DealRowViewHolder target, View source) {
        this.target = target;
        target.mRootView = Utils.findRequiredView(source, R.id.rowDealRoot, "field 'mRootView'");
        target.mTitle = (TextView) Utils.findRequiredViewAsType(source, R.id.title, "field 'mTitle'", TextView.class);
        target.mSubTitle = (TextView) Utils.findRequiredViewAsType(source, R.id.subtitle, "field 'mSubTitle'", TextView.class);
        target.mStatus = (ImageView) Utils.findRequiredViewAsType(source, R.id.status, "field 'mStatus'", ImageView.class);
        target.mIcon = (ColoredImageView) Utils.findOptionalViewAsType(source, R.id.icon, "field 'mIcon'", ColoredImageView.class);
    }

    @CallSuper
    public void unbind() {
        DealRowViewHolder target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mRootView = null;
        target.mTitle = null;
        target.mSubTitle = null;
        target.mStatus = null;
        target.mIcon = null;
    }
}
