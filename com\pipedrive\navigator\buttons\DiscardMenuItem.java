package com.pipedrive.navigator.buttons;

import com.pipedrive.R;

public abstract class DiscardMenuItem implements NavigatorMenuItem {
    public int getMenuItemId() {
        return R.id.menu_discard;
    }

    public boolean isEnabled() {
        return true;
    }
}
