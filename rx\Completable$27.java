package rx;

import rx.plugins.RxJavaHooks;
import rx.subscriptions.MultipleAssignmentSubscription;

class Completable$27 implements CompletableSubscriber {
    final /* synthetic */ Completable this$0;
    final /* synthetic */ MultipleAssignmentSubscription val$mad;

    Completable$27(Completable completable, MultipleAssignmentSubscription multipleAssignmentSubscription) {
        this.this$0 = completable;
        this.val$mad = multipleAssignmentSubscription;
    }

    public void onCompleted() {
        this.val$mad.unsubscribe();
    }

    public void onError(Throwable e) {
        RxJavaHooks.onError(e);
        this.val$mad.unsubscribe();
        Completable.deliverUncaughtException(e);
    }

    public void onSubscribe(Subscription d) {
        this.val$mad.set(d);
    }
}
