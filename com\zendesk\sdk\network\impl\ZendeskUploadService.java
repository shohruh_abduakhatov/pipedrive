package com.zendesk.sdk.network.impl;

import com.zendesk.sdk.model.request.UploadResponseWrapper;
import com.zendesk.sdk.network.UploadService;
import com.zendesk.service.RetrofitZendeskCallbackAdapter;
import com.zendesk.service.ZendeskCallback;
import java.io.File;
import okhttp3.MediaType;
import okhttp3.RequestBody;

class ZendeskUploadService {
    private static final String LOG_TAG = "ZendeskUploadService";
    private final UploadService uploadService;

    public ZendeskUploadService(UploadService uploadService) {
        this.uploadService = uploadService;
    }

    void uploadAttachment(String header, String fileName, File file, String mimeType, ZendeskCallback<UploadResponseWrapper> callback) {
        this.uploadService.uploadAttachment(header, fileName, RequestBody.create(MediaType.parse(mimeType), file)).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }

    void deleteAttachment(String header, String token, ZendeskCallback<Void> callback) {
        this.uploadService.deleteAttachment(header, token).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }
}
