package com.pipedrive.views.hinted;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import com.pipedrive.R;

public class HintedTextView extends HintedEditText<TextView> {
    public HintedTextView(Context context) {
        super(context);
    }

    public HintedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HintedTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected int getMainViewLayoutResource() {
        return R.layout.view_hinted_text_view;
    }
}
