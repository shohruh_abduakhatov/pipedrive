package com.google.android.gms.tagmanager;

interface zzbp {
    void e(String str);

    void setLogLevel(int i);

    void v(String str);

    void zzb(String str, Throwable th);

    void zzc(String str, Throwable th);

    void zzdg(String str);

    void zzdh(String str);

    void zzdi(String str);
}
