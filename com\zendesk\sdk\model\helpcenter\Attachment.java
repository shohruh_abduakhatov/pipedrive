package com.zendesk.sdk.model.helpcenter;

import android.support.annotation.Nullable;
import java.util.Date;

public class Attachment {
    private Long articleId;
    private String contentType;
    private String contentUrl;
    private Date createdAt;
    private String fileName;
    private Long id;
    private Long size;
    private Date updatedAt;
    private String url;

    @Nullable
    public Long getId() {
        return this.id;
    }

    @Nullable
    public String getUrl() {
        return this.url;
    }

    @Nullable
    public Long getArticleId() {
        return this.articleId;
    }

    @Nullable
    public String getFileName() {
        return this.fileName;
    }

    @Nullable
    public String getContentUrl() {
        return this.contentUrl;
    }

    @Nullable
    public String getContentType() {
        return this.contentType;
    }

    @Nullable
    public Long getSize() {
        return this.size;
    }

    @Nullable
    public Date getCreatedAt() {
        return this.createdAt == null ? null : new Date(this.createdAt.getTime());
    }

    @Nullable
    public Date getUpdatedAt() {
        return this.updatedAt == null ? null : new Date(this.updatedAt.getTime());
    }
}
