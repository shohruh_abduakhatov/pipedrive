package com.zendesk.sdk.network;

public interface ZendeskTracker {
    void helpCenterArticleViewed();

    void helpCenterLoaded();

    void helpCenterSearched(String str);

    void rateMyAppFeedbackSent();

    void rateMyAppRated();

    void requestCreated();

    void requestUpdated();
}
