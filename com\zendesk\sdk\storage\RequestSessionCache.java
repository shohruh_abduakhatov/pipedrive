package com.zendesk.sdk.storage;

import android.support.annotation.NonNull;
import com.zendesk.sdk.model.request.fields.TicketForm;
import java.util.List;

public interface RequestSessionCache {
    boolean containsAllTicketForms(@NonNull List<Long> list);

    @NonNull
    List<TicketForm> getTicketFormsById(@NonNull List<Long> list);

    void updateTicketFormCache(@NonNull List<TicketForm> list);
}
