package com.zendesk.belvedere;

public enum BelvedereSource {
    Camera,
    Gallery
}
