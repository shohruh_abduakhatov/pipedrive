package com.pipedrive.views.common;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.EditText;
import com.pipedrive.R;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.views.common.EditTextWithUnderlineAndLabel.OnValueChangedListener;

public class TitleEditText extends EditTextWithUnderlineAndLabel {
    public TitleEditText(Context context) {
        super(context);
    }

    public TitleEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TitleEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @NonNull
    protected EditText createMainView(@NonNull Context context, AttributeSet attrs, int defStyle) {
        EditText editText = super.createMainView(context, attrs, defStyle);
        editText.setHint(R.string.name);
        editText.setTextAppearance(getContext(), R.style.TextAppearance.Heading);
        editText.setInputType(16385);
        return editText;
    }

    public void setOnValueChangedListener(@Nullable OnValueChangedListener onValueChangedListener) {
        setupListener(onValueChangedListener);
    }

    public void setup(@NonNull Person person, @Nullable OnValueChangedListener onValueChangedListener) {
        ((EditText) this.mMainView).setText(person.getName());
        setupListener(onValueChangedListener);
    }

    public void setup(@NonNull Organization organization, @Nullable OnValueChangedListener onValueChangedListener) {
        ((EditText) this.mMainView).setText(organization.getName());
        setupListener(onValueChangedListener);
    }
}
