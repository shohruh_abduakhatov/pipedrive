package com.pipedrive.changes;

import android.support.annotation.NonNull;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.newrelic.agent.android.instrumentation.JSONObjectInstrumentation;
import com.pipedrive.application.Session;
import com.pipedrive.changes.Change.ChangeOperationType;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.FilterDealsDataSource;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.Deal;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.ConnectionUtil.JsonReaderInterceptor;
import com.pipedrive.util.networking.Response;
import java.io.IOException;
import java.util.List;
import org.json.JSONObject;

class DealChangeHandler extends ChangeHandler {
    private static final String JSON_PARAM_GET_MATCHING_FILTERS = "get_matching_filters";
    private static final String JSON_VALUE_ALL = "all";

    DealChangeHandler() {
    }

    @NonNull
    public ChangeHandled handleChange(@NonNull Session session, @NonNull Change dealChange) {
        if (dealChange.getChangeOperationType() != ChangeOperationType.OPERATION_CREATE && dealChange.getChangeOperationType() != ChangeOperationType.OPERATION_UPDATE) {
            LogJourno.reportEvent(EVENT.ChangesHandler_corruptedChangeFound, String.format("Change:[%s]", new Object[]{dealChange}));
            Log.e(new Throwable(String.format("Unknown change type requested for deal: %s", new Object[]{dealChange})));
            return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
        } else if (!ConnectionUtil.isConnected(session.getApplicationContext())) {
            return ChangeHandled.NOT_PROCESSED_PLEASE_RETRY;
        } else {
            boolean isUpdateDealOperation;
            if (dealChange.getChangeOperationType() == ChangeOperationType.OPERATION_UPDATE) {
                isUpdateDealOperation = true;
            } else {
                isUpdateDealOperation = false;
            }
            JsonReaderInterceptor<AnonymousClass1DealResponse> dealResponseParser = new JsonReaderInterceptor<AnonymousClass1DealResponse>() {
                public AnonymousClass1DealResponse interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull AnonymousClass1DealResponse responseTemplate) throws IOException {
                    if (jsonReader.peek() == JsonToken.NULL) {
                        jsonReader.skipValue();
                    } else {
                        Deal dealFromResponse = Deal.readDeal(jsonReader);
                        if (dealFromResponse == null) {
                            LogJourno.reportEvent(EVENT.ChangesHandler_changeDataResponseToJsonParsingFailed, String.format("DealResponseTemplate:[%s]", new Object[]{responseTemplate}));
                            Log.e(new Throwable(String.format("Parsing of Deal from response failed! Response: %s", new Object[]{responseTemplate})));
                        } else if (responseTemplate.dealUnderChangeSqlId <= 0 || !dealFromResponse.isExisting()) {
                            LogJourno.reportEvent(EVENT.ChangesHandler_cannotUpdatePipedriveIdFromChangeDataResponse, String.format("DealResponseTemplate:[%s] DealFromResponse:[%s]", new Object[]{responseTemplate, dealFromResponse}));
                            Log.e(new Throwable(String.format("Cannot update PD ID! SQL ID: %s; PD ID: %s", new Object[]{Long.valueOf(responseTemplate.dealUnderChangeSqlId), Integer.valueOf(dealFromResponse.getPipedriveId())})));
                        } else {
                            new DealsDataSource(session.getDatabase()).updatePipedriveIdBySqlId(responseTemplate.dealUnderChangeSqlId, dealFromResponse.getPipedriveId());
                        }
                    }
                    return responseTemplate;
                }
            };
            try {
                Long dealSqlId = dealChange.getMetaData();
                if (dealSqlId == null) {
                    LogJourno.reportEvent(EVENT.ChangesHandler_changeMetadataNotFound, String.format("Change:[%s]", new Object[]{dealChange}));
                    Log.e(new Throwable("Change metadata is missing! Cannot find the Deal to change!"));
                    return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
                }
                Deal dealUnderChange = (Deal) new DealsDataSource(session.getDatabase()).findBySqlId(dealSqlId.longValue());
                if (dealUnderChange == null) {
                    LogJourno.reportEvent(EVENT.ChangesHandler_changeDataNotFound, String.format("Change:[%s] SQL id:[%s]", new Object[]{dealChange, dealSqlId}));
                    Log.e(new Throwable(String.format("Change metadata is present (%s) but unable to find Deal that was registered for the change!", new Object[]{dealSqlId})));
                    return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
                }
                AnonymousClass1DealResponse result;
                if (!dealUnderChange.isAllAssociationsExisting()) {
                    LogJourno.reportEvent(EVENT.ChangesHandler_notAllChangePipedriveAssociationsExist, String.format("Change:[%s]", new Object[]{dealChange}));
                    Log.e(new Throwable(String.format("Deal associations are not all existing! Change: %s", new Object[]{dealChange})));
                }
                ApiUrlBuilder apiUriBuilder = new ApiUrlBuilder(session, "deals");
                if (isUpdateDealOperation) {
                    apiUriBuilder.appendEncodedPath(Integer.toString(dealUnderChange.getPipedriveId()));
                }
                apiUriBuilder.param(JSON_PARAM_GET_MATCHING_FILTERS, JSON_VALUE_ALL);
                AnonymousClass1DealResponse dealResponse = new Response() {
                    long dealUnderChangeSqlId;

                    public String toString() {
                        return "DealResponse:[dealUnderChangeSqlId=" + this.dealUnderChangeSqlId + "][super.toString:[" + super.toString() + "]]";
                    }
                };
                dealResponse.dealUnderChangeSqlId = dealSqlId.longValue();
                JSONObject json;
                if (isUpdateDealOperation) {
                    json = dealUnderChange.getJSON();
                    result = (AnonymousClass1DealResponse) ConnectionUtil.requestPut(apiUriBuilder, !(json instanceof JSONObject) ? json.toString() : JSONObjectInstrumentation.toString(json), dealResponseParser, dealResponse);
                } else {
                    json = dealUnderChange.getJSON();
                    result = (AnonymousClass1DealResponse) ConnectionUtil.requestPost(apiUriBuilder, !(json instanceof JSONObject) ? json.toString() : JSONObjectInstrumentation.toString(json), dealResponseParser, dealResponse);
                }
                FilterDealsDataSource filterDealsDataSource = new FilterDealsDataSource(session.getDatabase());
                filterDealsDataSource.unrelateAllDealAssociations(dealUnderChange);
                List<Long> matchedFilters = result.getMatchedFilters();
                if (matchedFilters != null) {
                    for (Long longValue : matchedFilters) {
                        filterDealsDataSource.relateDealToFilterId(dealUnderChange, longValue.longValue());
                    }
                }
                return parseResponse(result, this, dealChange);
            } catch (Exception e) {
                LogJourno.reportEvent(EVENT.ChangesHandler_changeDataRequestToJsonParsingFailed, String.format("Change:[%s]", new Object[]{dealChange}), e);
                Log.e(new Throwable("Deal change failed", e));
                return ChangeHandled.CHANGE_FAILED;
            }
        }
    }
}
