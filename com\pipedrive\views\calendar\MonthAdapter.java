package com.pipedrive.views.calendar;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.view.ViewGroup;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

class MonthAdapter extends CalendarViewAdapter<MonthViewHolder> {
    @Nullable
    private OnDateChangedInternalListener internalListener;
    @NonNull
    final List<Calendar> items;
    private final Locale locale;
    @Nullable
    private Calendar selectedDate;

    public MonthAdapter(@NonNull Locale locale, @NonNull List<Calendar> items, @Nullable Calendar selectedDate) {
        this.locale = locale;
        this.items = items;
        this.selectedDate = selectedDate;
    }

    public void setInternalListener(@Nullable OnDateChangedInternalListener internalListener) {
        this.internalListener = internalListener;
    }

    public MonthViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MonthView monthView = new MonthView(parent.getContext());
        monthView.setLayoutParams(new LayoutParams(parent.getMeasuredWidth(), -2));
        monthView.setOnDateChangedInternalListener(this.internalListener);
        return new MonthViewHolder(monthView);
    }

    public void onBindViewHolder(MonthViewHolder holder, int position) {
        Calendar item = (Calendar) this.items.get(position);
        holder.bind(this.locale, item.get(1), item.get(2), this.selectedDate);
    }

    public int getItemCount() {
        return this.items.size();
    }

    public void setSelectedDate(@Nullable Calendar selectedDate) {
        this.selectedDate = selectedDate;
        notifyDataSetChanged();
    }

    public Calendar getItem(int position) {
        return (Calendar) this.items.get(position);
    }
}
