package com.pipedrive.tasks.authorization;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentResolver;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.common.Scopes;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.instrumentation.JSONObjectInstrumentation;
import com.newrelic.agent.android.tracing.Trace;
import com.pipedrive.LoginActivity;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.CompaniesDataSource;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.datasource.PipeSQLiteSharedHelper.TableCompany;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.model.settings.Company;
import com.pipedrive.sync.SyncManager;
import com.pipedrive.util.devices.MobileDevices;
import com.pipedrive.util.networking.HttpClientManager;
import com.pipedrive.util.networking.entities.LoginDataEntity;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;

public class UserLoginTask extends AsyncTask<String, Void, Integer> implements TraceFieldInterface {
    private static final String JSON_ADDITIONAL_DATA = "additional_data";
    private static final String JSON_API_TOKEN = "api_token";
    static final int LOGIN_FAILED = 0;
    public static final int LOGIN_FAILED_DEVICE = 3;
    public static final int LOGIN_OK_FROM_ACCOUNT_AUTH_SERVICE = 2;
    private static final int LOGIN_OK_FROM_APPLICATION = 1;
    private static final String TAG = UserLoginTask.class.getSimpleName();
    public Trace _nr_trace;
    @NonNull
    final Activity mCallerActivity;
    @Nullable
    private final OnEnoughDataToShowUI mOnEnoughDataToShowUI;
    @Nullable
    private final OnLoginListener mOnLoginListener;

    public interface OnLoginListener {
        void loginNotSuccessful(int i);

        void loginSuccessful(int i);
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface LoginResult {
    }

    public void _nr_setTrace(Trace trace) {
        try {
            this._nr_trace = trace;
        } catch (Exception e) {
        }
    }

    public UserLoginTask(@NonNull Activity callerActivity, @Nullable OnLoginListener onLoginListener, @Nullable OnEnoughDataToShowUI onEnoughDataToShowUI) {
        this.mCallerActivity = callerActivity;
        this.mOnLoginListener = onLoginListener;
        this.mOnEnoughDataToShowUI = onEnoughDataToShowUI;
    }

    protected Integer doInBackground(String... params) {
        String email = params[0];
        return parseLoginData(email, getLoginData(email, params[1]));
    }

    protected void onPostExecute(Integer result) {
        if (this.mOnLoginListener != null) {
            switch (result.intValue()) {
                case 0:
                case 3:
                    this.mOnLoginListener.loginNotSuccessful(result.intValue());
                    return;
                case 1:
                case 2:
                    this.mOnLoginListener.loginSuccessful(result.intValue());
                    return;
                default:
                    return;
            }
        }
    }

    private Integer parseLoginData(String email, @Nullable String loginData) {
        String tag = TAG + ".parseLoginData()";
        Session session = loginData != null ? saveAndVerifyLoginData(loginData) : null;
        if (session == null) {
            return Integer.valueOf(0);
        }
        Log.d(tag, "Login successful.");
        if (session.getLastSyncTime(null) == null) {
            session.setLastSyncTimeToNow();
        }
        Log.setupCrashLoggersMetadata(session);
        try {
            new MobileDevices(session).loginDevice().await();
            AccountManager accountManager = AccountManager.get(this.mCallerActivity);
            for (Account account : accountManager.getAccountsByType("com.pipedrive.account")) {
                if (!account.name.equalsIgnoreCase(email)) {
                    Log.d(tag, "Account " + account + " already exists. Requesting to remove it in order to add new account for " + email);
                    if (VERSION.SDK_INT >= 22) {
                        accountManager.removeAccount(account, null, null, null);
                    } else {
                        accountManager.removeAccount(account, null, null);
                    }
                }
            }
            Account account2 = new Account(email, "com.pipedrive.account");
            Log.d(tag, "Adding new account: " + account2);
            boolean isAccountCreated = accountManager.addAccountExplicitly(account2, null, null);
            Log.d(tag, "New account " + account2 + " added yes/no? " + isAccountCreated);
            if (isAccountCreated) {
                ContentResolver.setSyncAutomatically(account2, "com.android.contacts", false);
                ContentResolver.setSyncAutomatically(account2, "com.pipedrive.contentproviders.recentsprovider", true);
                ContentResolver.addPeriodicSync(account2, "com.pipedrive.contentproviders.recentsprovider", new Bundle(), LoginActivity.RECENTS_SYNC_INTERVAL_IN_SECONDS);
                Bundle extras = this.mCallerActivity.getIntent().getExtras();
                if (extras != null) {
                    AccountAuthenticatorResponse response = (AccountAuthenticatorResponse) extras.getParcelable("accountAuthenticatorResponse");
                    if (response != null) {
                        Bundle result = new Bundle();
                        result.putString("authAccount", email);
                        result.putString("accountType", "com.pipedrive.account");
                        response.onResult(result);
                        return Integer.valueOf(2);
                    }
                }
                Session activeSession = PipedriveApp.getActiveSession();
                if (activeSession == null) {
                    return Integer.valueOf(0);
                }
                SyncManager.getInstance().requestLoginDataDownload(activeSession, this.mOnEnoughDataToShowUI);
                return Integer.valueOf(1);
            }
            Log.d(tag, String.format("Account (%s) creation failed. Cannot log in!", new Object[]{account2}));
            return Integer.valueOf(0);
        } catch (Exception e) {
            Log.e(e);
            PipedriveApp.getSessionManager().clearDatabaseAndDisposeSession(session);
            PipedriveApp.getSessionManager().releaseActiveSession();
            return Integer.valueOf(3);
        }
    }

    @Nullable
    protected String getLoginData(@Nullable String email, @Nullable String password) {
        String response = null;
        try {
            Response authorizationResponse = HttpClientManager.INSTANCE.authorization(this.mCallerActivity.getApplicationContext(), email, password);
            boolean isAuthorized = (authorizationResponse == null || authorizationResponse.body() == null) ? false : true;
            if (isAuthorized) {
                response = authorizationResponse.body().string();
            }
        } catch (IOException e) {
            LogJourno.reportEvent(EVENT.Authorization_pipedriveAuthIOException, e.getMessage());
            Log.e(new Throwable("Login request failed with an exception", e));
        }
        return response;
    }

    @Nullable
    private Session saveAndVerifyLoginData(String loginData) {
        LoginDataEntity data = parseLoginData(loginData);
        if (data == null) {
            return null;
        }
        Company defaultCompany = data.getDefaultCompany();
        if (defaultCompany == null) {
            return null;
        }
        String apiToken = defaultCompany.getApiToken();
        Long companyUserId = defaultCompany.getUserPipedriveId();
        Long defaultCompanyID = defaultCompany.getPipedriveIdOrNull();
        boolean createSessionForLoggedInUser = (apiToken == null || companyUserId == null || defaultCompanyID == null) ? false : true;
        if (!createSessionForLoggedInUser) {
            return null;
        }
        Session createdSession = PipedriveApp.getSessionManager().createActiveSession(companyUserId.longValue(), defaultCompanyID.longValue(), apiToken, defaultCompany.getDomain());
        if (createdSession != null) {
            new CompaniesDataSource(createdSession).replaceExistingCompanies(data.getCompanies());
            createdSession.setUserSettingsIsAdmin(data.isAdmin());
            createdSession.enableLastProcessChangesAndRecents(true);
        }
        return createdSession;
    }

    @Nullable
    private static LoginDataEntity parseLoginData(@Nullable String loginJsonString) {
        if (TextUtils.isEmpty(loginJsonString)) {
            return null;
        }
        try {
            LoginDataEntity data = new LoginDataEntity();
            JSONObject userDataJson = JSONObjectInstrumentation.init(loginJsonString);
            JSONArray dataArr = userDataJson.optJSONArray(com.pipedrive.util.networking.Response.JSON_PARAM_DATA);
            if (dataArr != null && dataArr.length() > 0) {
                for (int i = 0; i < dataArr.length(); i++) {
                    JSONObject userJson = dataArr.optJSONObject(i);
                    if (userJson != null) {
                        Company loginCompany = new Company();
                        loginCompany.setApiToken(userJson.optString(JSON_API_TOKEN, null));
                        long userId = userJson.optLong(PipeSQLiteHelper.COLUMN_USER_ID);
                        if (userId > 0) {
                            loginCompany.setUserPipedriveId(Long.valueOf(userId));
                        }
                        JSONObject companyObj = userJson.optJSONObject(TableCompany.TABLE_NAME);
                        if (companyObj != null) {
                            JSONObject companyInfoObj = companyObj.optJSONObject("info");
                            if (companyInfoObj != null) {
                                int companyId = companyInfoObj.optInt(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID);
                                if (companyId > 0) {
                                    loginCompany.setPipedriveId(Long.valueOf((long) companyId));
                                }
                                loginCompany.setName(companyInfoObj.optString("name", null));
                                loginCompany.setDomain(companyInfoObj.optString("domain", null));
                            }
                        }
                        data.addLoginCompany(loginCompany);
                    }
                }
            }
            JSONObject additionalDataObj = userDataJson.optJSONObject("additional_data");
            if (additionalDataObj == null) {
                return data;
            }
            JSONObject userObj = additionalDataObj.optJSONObject(CustomField.FIELD_DATA_TYPE_USER);
            if (userObj != null) {
                JSONObject profileObj = userObj.optJSONObject(Scopes.PROFILE);
                if (profileObj != null) {
                    data.setAdmin(profileObj.optBoolean("is_admin", false));
                    data.setUserProfileEmail(profileObj.optString("email", null));
                    data.setUserProfileName(profileObj.optString("name", null));
                }
            }
            long defaultCompanyId = additionalDataObj.optLong("default_company_id");
            if (defaultCompanyId <= 0) {
                return data;
            }
            data.setDefaultCompanyId(defaultCompanyId);
            return data;
        } catch (Throwable e) {
            LogJourno.reportEvent(EVENT.Authorization_loginDataParsingFailed, e);
            Log.e(e);
            return null;
        }
    }
}
