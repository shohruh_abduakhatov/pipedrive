package com.pipedrive.application;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.util.StringUtils;
import java.util.ArrayList;
import java.util.List;

public enum SessionStoreHelper {
    ;
    
    private static final String PIPEDRIVE_PREFS_FILE_NAME = "Pipedrive_Prefs";

    public enum Session {
        private static final /* synthetic */ Session[] $VALUES = null;

        static {
            $VALUES = new Session[0];
        }

        private Session(String str, int i) {
        }

        public static Session valueOf(String name) {
            return (Session) Enum.valueOf(Session.class, name);
        }

        public static Session[] values() {
            return (Session[]) $VALUES.clone();
        }

        public static void setSessionPrefsString(@NonNull Session session, @NonNull String prefsKey, @Nullable String prefsValue) {
            String sessionUniquePrefsKeyName = SessionStoreHelper.getSessionUniquePrefsKeyName(session, prefsKey);
            Context applicationContext = session.getApplicationContext();
            if (prefsValue == null) {
                SessionStoreHelper.removePref(applicationContext, sessionUniquePrefsKeyName);
            } else {
                SessionStoreHelper.savePrefsStringValue(applicationContext, sessionUniquePrefsKeyName, prefsValue);
            }
        }

        public static String getSessionPrefsString(@NonNull Session session, @NonNull String prefsKey, @Nullable String returnIfNotFound) {
            SharedPreferences prefs = SessionStoreHelper.getPrefs(session.getApplicationContext());
            return prefs == null ? returnIfNotFound : prefs.getString(SessionStoreHelper.getSessionUniquePrefsKeyName(session, prefsKey), returnIfNotFound);
        }

        public static void setSessionPrefsLong(@NonNull Session session, @NonNull String prefsKey, @Nullable Long prefsValue) {
            String sessionUniquePrefsKeyName = SessionStoreHelper.getSessionUniquePrefsKeyName(session, prefsKey);
            Context applicationContext = session.getApplicationContext();
            if (prefsValue == null) {
                SessionStoreHelper.removePref(applicationContext, sessionUniquePrefsKeyName);
            } else {
                SessionStoreHelper.savePrefsLongValue(applicationContext, sessionUniquePrefsKeyName, prefsValue);
            }
        }

        public static Long getSessionPrefsLong(@NonNull Session session, @NonNull String prefsKey, @Nullable Long returnIfNotFound) {
            SharedPreferences prefs = SessionStoreHelper.getPrefs(session.getApplicationContext());
            if (prefs == null) {
                return returnIfNotFound;
            }
            long prefsLong = prefs.getLong(SessionStoreHelper.getSessionUniquePrefsKeyName(session, prefsKey), Long.MIN_VALUE);
            return prefsLong != Long.MIN_VALUE ? Long.valueOf(prefsLong) : returnIfNotFound;
        }

        public static void setSessionPrefsInt(@NonNull Session session, @NonNull String prefsKey, @Nullable Integer prefsValue) {
            String sessionUniquePrefsKeyName = SessionStoreHelper.getSessionUniquePrefsKeyName(session, prefsKey);
            Context applicationContext = session.getApplicationContext();
            if (prefsValue == null) {
                SessionStoreHelper.removePref(applicationContext, sessionUniquePrefsKeyName);
            } else {
                SessionStoreHelper.savePrefsIntValue(applicationContext, sessionUniquePrefsKeyName, prefsValue);
            }
        }

        public static Integer getSessionPrefsInt(@NonNull Session session, @NonNull String prefsKey, @Nullable Integer returnIfNotFound) {
            SharedPreferences prefs = SessionStoreHelper.getPrefs(session.getApplicationContext());
            if (prefs == null) {
                return returnIfNotFound;
            }
            int prefsInt = prefs.getInt(SessionStoreHelper.getSessionUniquePrefsKeyName(session, prefsKey), Integer.MIN_VALUE);
            return prefsInt != Integer.MIN_VALUE ? Integer.valueOf(prefsInt) : returnIfNotFound;
        }

        public static void setSessionPrefsLongArray(@NonNull Session session, @NonNull String prefsKey, @Nullable List<Long> prefsValue) {
            long[] prefsValuePrimitiveArray;
            if (prefsValue == null) {
                prefsValuePrimitiveArray = null;
            } else {
                prefsValuePrimitiveArray = new long[prefsValue.size()];
                for (int i = 0; i < prefsValue.size(); i++) {
                    prefsValuePrimitiveArray[i] = ((Long) prefsValue.get(i)).longValue();
                }
            }
            setSessionPrefsLongArray(session, prefsKey, prefsValuePrimitiveArray);
        }

        public static void setSessionPrefsLongArray(@NonNull Session session, @NonNull String prefsKey, @Nullable long[] prefsValue) {
            String sessionUniquePrefsKeyName = SessionStoreHelper.getSessionUniquePrefsKeyName(session, prefsKey);
            Context applicationContext = session.getApplicationContext();
            if (prefsValue == null) {
                SessionStoreHelper.removePref(applicationContext, sessionUniquePrefsKeyName);
                return;
            }
            StringBuilder prefsValueInStringFormatBuilder = new StringBuilder();
            prefsValueInStringFormatBuilder.append(prefsValue.length);
            prefsValueInStringFormatBuilder.append(':');
            int i = 0;
            while (i < prefsValue.length) {
                if (i > 0) {
                    prefsValueInStringFormatBuilder.append(',');
                }
                prefsValueInStringFormatBuilder.append(Long.toString(prefsValue[i]));
                i++;
            }
            SessionStoreHelper.savePrefsStringValue(applicationContext, sessionUniquePrefsKeyName, prefsValueInStringFormatBuilder.toString());
        }

        @Nullable
        public static List<Long> getSessionPrefsLongArray(@NonNull Session session, @NonNull String prefsKey, @Nullable List<Long> returnIfNotFound) {
            long[] returnIfNotFoundPrimitiveArray;
            if (returnIfNotFound == null) {
                returnIfNotFoundPrimitiveArray = null;
            } else {
                returnIfNotFoundPrimitiveArray = new long[returnIfNotFound.size()];
                for (int i = 0; i < returnIfNotFound.size(); i++) {
                    returnIfNotFoundPrimitiveArray[i] = ((Long) returnIfNotFound.get(i)).longValue();
                }
            }
            long[] sessionPrefsPrimitiveArray = getSessionPrefsLongArray(session, prefsKey, returnIfNotFoundPrimitiveArray);
            if (sessionPrefsPrimitiveArray == null) {
                return null;
            }
            List<Long> sessionPrefsArray = new ArrayList();
            for (long sessionPrefsPrimitiveArrayEntry : sessionPrefsPrimitiveArray) {
                sessionPrefsArray.add(Long.valueOf(sessionPrefsPrimitiveArrayEntry));
            }
            return sessionPrefsArray;
        }

        public static long[] getSessionPrefsLongArray(@NonNull Session session, @NonNull String prefsKey, @Nullable long[] returnIfNotFound) {
            SharedPreferences prefs = SessionStoreHelper.getPrefs(session.getApplicationContext());
            if (prefs == null) {
                return returnIfNotFound;
            }
            String notFound = "_not_found";
            String prefsString = prefs.getString(SessionStoreHelper.getSessionUniquePrefsKeyName(session, prefsKey), "_not_found");
            if (TextUtils.equals(prefsString, "_not_found") || StringUtils.isTrimmedAndEmpty(prefsString)) {
                return returnIfNotFound;
            }
            String[] prefsStringSplit = TextUtils.split(prefsString, ":");
            if (prefsStringSplit.length != 2) {
                return returnIfNotFound;
            }
            try {
                int numberOfInts = Integer.parseInt(prefsStringSplit[0]);
                String[] intsAsStrings = TextUtils.split(prefsStringSplit[1], Table.COMMA_SEP);
                long[] ints = new long[numberOfInts];
                for (int i = 0; i < intsAsStrings.length; i++) {
                    ints[i] = (long) Integer.parseInt(intsAsStrings[i]);
                }
                return ints;
            } catch (Exception e) {
                return returnIfNotFound;
            }
        }
    }

    public enum SharedSession {
        private static final /* synthetic */ SharedSession[] $VALUES = null;

        static {
            $VALUES = new SharedSession[0];
        }

        private SharedSession(String str, int i) {
        }

        public static SharedSession valueOf(String name) {
            return (SharedSession) Enum.valueOf(SharedSession.class, name);
        }

        public static SharedSession[] values() {
            return (SharedSession[]) $VALUES.clone();
        }

        protected static void setSessionPrefsString(@NonNull Context context, @NonNull String prefsKey, @Nullable String prefsValue) {
            Context applicationContext = context.getApplicationContext();
            if (prefsValue == null) {
                SessionStoreHelper.removePref(applicationContext, prefsKey);
            } else {
                SessionStoreHelper.savePrefsStringValue(applicationContext, prefsKey, prefsValue);
            }
        }

        protected static String getSessionPrefsString(@NonNull Context context, @NonNull String prefsKey, @Nullable String returnIfNotFound) {
            SharedPreferences prefs = SessionStoreHelper.getPrefs(context.getApplicationContext());
            return prefs == null ? returnIfNotFound : prefs.getString(prefsKey, returnIfNotFound);
        }

        protected static void setSessionPrefsLong(@NonNull Context context, @NonNull String prefsKey, @Nullable Long prefsValue) {
            Context applicationContext = context.getApplicationContext();
            if (prefsValue == null) {
                SessionStoreHelper.removePref(applicationContext, prefsKey);
            } else {
                SessionStoreHelper.savePrefsLongValue(applicationContext, prefsKey, prefsValue);
            }
        }

        protected static Long getSessionPrefsLong(@NonNull Context context, @NonNull String prefsKey, @Nullable Long returnIfNotFound) {
            SharedPreferences prefs = SessionStoreHelper.getPrefs(context.getApplicationContext());
            if (prefs == null) {
                return returnIfNotFound;
            }
            long prefsLong = prefs.getLong(prefsKey, Long.MIN_VALUE);
            return prefsLong != Long.MIN_VALUE ? Long.valueOf(prefsLong) : returnIfNotFound;
        }

        protected static void setSessionPrefsInt(@NonNull Context context, @NonNull String prefsKey, @Nullable Integer prefsValue) {
            Context applicationContext = context.getApplicationContext();
            if (prefsValue == null) {
                SessionStoreHelper.removePref(applicationContext, prefsKey);
            } else {
                SessionStoreHelper.savePrefsIntValue(applicationContext, prefsKey, prefsValue);
            }
        }

        protected static Integer getSessionPrefsInt(@NonNull Context context, @NonNull String prefsKey, @Nullable Integer returnIfNotFound) {
            SharedPreferences prefs = SessionStoreHelper.getPrefs(context.getApplicationContext());
            if (prefs == null) {
                return returnIfNotFound;
            }
            int prefsInt = prefs.getInt(prefsKey, Integer.MIN_VALUE);
            return prefsInt != Integer.MIN_VALUE ? Integer.valueOf(prefsInt) : returnIfNotFound;
        }
    }

    private static void savePrefsStringValue(@NonNull Context context, @NonNull String key, @NonNull String value) {
        SharedPreferences prefs = getPrefs(context);
        if (prefs != null) {
            prefs.edit().putString(key, value).commit();
        }
    }

    private static void savePrefsLongValue(@NonNull Context context, @NonNull String key, @NonNull Long value) {
        SharedPreferences prefs = getPrefs(context);
        if (prefs != null) {
            prefs.edit().putLong(key, value.longValue()).commit();
        }
    }

    private static void savePrefsIntValue(@NonNull Context context, @NonNull String key, @NonNull Integer value) {
        SharedPreferences prefs = getPrefs(context);
        if (prefs != null) {
            prefs.edit().putInt(key, value.intValue()).commit();
        }
    }

    private static void removePref(@NonNull Context context, @NonNull String key) {
        SharedPreferences prefs = getPrefs(context);
        if (prefs != null) {
            prefs.edit().remove(key).commit();
        }
    }

    @Nullable
    private static SharedPreferences getPrefs(@NonNull Context context) {
        SharedPreferences sharedPreferencesNotFound = null;
        try {
            sharedPreferencesNotFound = context.getSharedPreferences(PIPEDRIVE_PREFS_FILE_NAME, 0);
        } catch (Throwable e) {
            LogJourno.reportEvent(EVENT.Session_getSharedPreferencesFailed, e);
            Log.e(e);
        }
        return sharedPreferencesNotFound;
    }

    private static String getSessionUniquePrefsKeyName(@NonNull Session session, @NonNull String preferenceKeyName) {
        return String.format("%s_%s", new Object[]{session.getSessionID(), preferenceKeyName});
    }

    protected static void removeAllSessionPreferences(@NonNull Session session) {
        SharedPreferences sharedPreferences = getPrefs(session.getApplicationContext());
        if (sharedPreferences != null) {
            Editor editor = sharedPreferences.edit();
            String sessionUniqueKeyPart = getSessionUniquePrefsKeyName(session, "");
            for (String key : sharedPreferences.getAll().keySet()) {
                if (key.contains(sessionUniqueKeyPart)) {
                    editor.remove(key);
                }
            }
            editor.commit();
        }
    }

    protected static void clearAllPreferences(@NonNull Context context) {
        SharedPreferences prefs = getPrefs(context.getApplicationContext());
        if (prefs != null) {
            prefs.edit().clear().commit();
        }
    }
}
