package com.pipedrive.deal.view;

import android.support.annotation.UiThread;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;
import com.pipedrive.views.DealStateButtonsView;
import com.pipedrive.views.fab.FabWithOverlayMenu;
import com.pipedrive.views.stageselector.StagesSelector;

public final class DealDetailsActivity_ViewBinding implements Unbinder {
    private DealDetailsActivity target;
    private View view2131820965;

    @UiThread
    public DealDetailsActivity_ViewBinding(DealDetailsActivity target) {
        this(target, target.getWindow().getDecorView());
    }

    @UiThread
    public DealDetailsActivity_ViewBinding(final DealDetailsActivity target, View source) {
        this.target = target;
        target.mToolbar = (Toolbar) Utils.findRequiredViewAsType(source, R.id.dealDetails.toolbar, "field 'mToolbar'", Toolbar.class);
        target.mTitle = (TextView) Utils.findRequiredViewAsType(source, R.id.dealDetails.title, "field 'mTitle'", TextView.class);
        target.mPerson = (TextView) Utils.findRequiredViewAsType(source, R.id.dealDetails.person, "field 'mPerson'", TextView.class);
        target.mOrganization = (TextView) Utils.findRequiredViewAsType(source, R.id.dealDetails.organization, "field 'mOrganization'", TextView.class);
        target.mStagesSelector = (StagesSelector) Utils.findRequiredViewAsType(source, R.id.dealDetails.stageSelector, "field 'mStagesSelector'", StagesSelector.class);
        target.mValue = (TextView) Utils.findRequiredViewAsType(source, R.id.dealDetails.value, "field 'mValue'", TextView.class);
        target.mProducts = (TextView) Utils.findRequiredViewAsType(source, R.id.dealDetails.products, "field 'mProducts'", TextView.class);
        target.mDealStateButtonsView = (DealStateButtonsView) Utils.findRequiredViewAsType(source, R.id.dealDetails.dealStateButtons, "field 'mDealStateButtonsView'", DealStateButtonsView.class);
        View view = Utils.findRequiredView(source, R.id.dealDetails_productsAndValueContainer, "field 'mProductsAndValueContainer' and method 'onProductsAndValueContainerClicked'");
        target.mProductsAndValueContainer = view;
        this.view2131820965 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onProductsAndValueContainerClicked();
            }
        });
        target.mTabLayout = (TabLayout) Utils.findRequiredViewAsType(source, R.id.dealDetails.tabs, "field 'mTabLayout'", TabLayout.class);
        target.mViewPager = (ViewPager) Utils.findRequiredViewAsType(source, R.id.dealDetails.pager, "field 'mViewPager'", ViewPager.class);
        target.mFabWithOverlayMenu = (FabWithOverlayMenu) Utils.findRequiredViewAsType(source, R.id.dealDetails.fab, "field 'mFabWithOverlayMenu'", FabWithOverlayMenu.class);
    }

    public void unbind() {
        DealDetailsActivity target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mToolbar = null;
        target.mTitle = null;
        target.mPerson = null;
        target.mOrganization = null;
        target.mStagesSelector = null;
        target.mValue = null;
        target.mProducts = null;
        target.mDealStateButtonsView = null;
        target.mProductsAndValueContainer = null;
        target.mTabLayout = null;
        target.mViewPager = null;
        target.mFabWithOverlayMenu = null;
        this.view2131820965.setOnClickListener(null);
        this.view2131820965 = null;
    }
}
