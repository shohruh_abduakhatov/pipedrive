package com.pipedrive.util.devices;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import retrofit2.HttpException;
import rx.functions.Func2;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u000e\u0010\u0003\u001a\n \u0002*\u0004\u0018\u00010\u00040\u00042\u000e\u0010\u0005\u001a\n \u0002*\u0004\u0018\u00010\u00060\u0006H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"<anonymous>", "", "kotlin.jvm.PlatformType", "repeatCount", "", "lastError", "", "call", "(Ljava/lang/Integer;Ljava/lang/Throwable;)Ljava/lang/Boolean;"}, k = 3, mv = {1, 1, 6})
/* compiled from: MobileDevices.kt */
final class MobileDevices$registerDevice$1<T1, T2, R> implements Func2<Integer, Throwable, Boolean> {
    final /* synthetic */ MobileDevices this$0;

    MobileDevices$registerDevice$1(MobileDevices mobileDevices) {
        this.this$0 = mobileDevices;
    }

    public final Boolean call(Integer repeatCount, Throwable lastError) {
        boolean z = false;
        if (lastError instanceof HttpException) {
            return Boolean.valueOf(false);
        }
        if (Intrinsics.compare(repeatCount.intValue(), this.this$0.maxRepeatCount) < 0) {
            z = true;
        }
        return Boolean.valueOf(z);
    }
}
