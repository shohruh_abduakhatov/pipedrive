package com.zendesk.sdk.requests;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.squareup.picasso.Callback;
import com.zendesk.sdk.R;
import com.zendesk.sdk.model.request.Attachment;
import com.zendesk.sdk.model.request.CommentResponse;
import com.zendesk.sdk.network.impl.ZendeskPicassoProvider;
import com.zendesk.sdk.ui.ListRowView;
import com.zendesk.sdk.ui.ZendeskPicassoTransformationFactory;
import com.zendesk.util.CollectionUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

class RequestCommentsListAdapter extends ArrayAdapter<CommentWithUser> {
    private static final int VIEW_COUNT = 2;
    private static final int VIEW_TYPE_AGENT = 0;
    private static final int VIEW_TYPE_ENDUSER = 1;
    private AttachmentOnClickListener mAttachmentOnClickListener;
    private final Context mContext;

    private interface IdHolder {
        int getAttachmentsContainerId();

        int getAvatarId();

        int getContainerId();

        int getDateId();

        int getNameId();

        int getResponseId();
    }

    private static class AgentRowIdHolder implements IdHolder {
        private AgentRowIdHolder() {
        }

        public int getContainerId() {
            return R.layout.row_agent_comment;
        }

        public int getAvatarId() {
            return R.id.view_request_agent_avatar_imageview;
        }

        public int getNameId() {
            return R.id.view_request_agent_name_textview;
        }

        public int getResponseId() {
            return R.id.view_request_agent_response_textview;
        }

        public int getDateId() {
            return R.id.view_request_agent_comment_date;
        }

        public int getAttachmentsContainerId() {
            return R.id.view_request_agent_response_attachment_container;
        }
    }

    interface AttachmentOnClickListener {
        void onAttachmentClicked(View view, Attachment attachment);
    }

    @SuppressLint({"ViewConstructor"})
    private static class CommentRow extends RelativeLayout implements ListRowView<CommentWithUser> {
        private static final String CREATED_AT_DATE_FORMAT = "dd MMMM yyy";
        private ViewGroup mAttachmentsContainer;
        private ImageView mAvatarImageView;
        private TextView mCommentDateTextView;
        private final Context mContext;
        private final IdHolder mIdHolder;
        private AttachmentOnClickListener mLoadImageCallback;
        private TextView mNameTextView;
        private TextView mResponseTextView;

        public CommentRow(Context context, IdHolder idHolder, AttachmentOnClickListener attachmentOnClickListener) {
            super(context);
            this.mContext = context;
            this.mIdHolder = idHolder;
            this.mLoadImageCallback = attachmentOnClickListener;
            initialise();
        }

        private void initialise() {
            View container = LayoutInflater.from(this.mContext).inflate(this.mIdHolder.getContainerId(), this);
            this.mAvatarImageView = (ImageView) container.findViewById(this.mIdHolder.getAvatarId());
            this.mNameTextView = (TextView) container.findViewById(this.mIdHolder.getNameId());
            this.mResponseTextView = (TextView) container.findViewById(this.mIdHolder.getResponseId());
            this.mCommentDateTextView = (TextView) container.findViewById(this.mIdHolder.getDateId());
            this.mAttachmentsContainer = (ViewGroup) container.findViewById(this.mIdHolder.getAttachmentsContainerId());
        }

        public void bind(final CommentWithUser comment) {
            this.mNameTextView.setText(comment.getAuthor().getName());
            this.mResponseTextView.setText(comment.getComment().getBody());
            this.mCommentDateTextView.setText(new SimpleDateFormat(CREATED_AT_DATE_FORMAT, Locale.getDefault()).format(comment.getComment().getCreatedAt()));
            Map<Long, Integer> savedHeightMap = (Map) CommentRowAttachmentHelper.heightMap.get(comment.getComment().getId());
            if (savedHeightMap != null) {
                int savedHeight = 0;
                for (Long attachmentId : savedHeightMap.keySet()) {
                    savedHeight += ((Integer) savedHeightMap.get(attachmentId)).intValue();
                }
                this.mAttachmentsContainer.setLayoutParams(new LayoutParams(-1, savedHeight));
            }
            int dp = (int) (getResources().getDimension(R.dimen.view_request_comment_avatar_size) / getResources().getDisplayMetrics().density);
            if (comment.getAuthor().getPhoto() != null) {
                ZendeskPicassoProvider.getInstance(this.mContext).load(comment.getAuthor().getPhoto().getContentUrl()).error(R.drawable.zd_user_default_avatar).placeholder(R.drawable.zd_user_default_avatar).transform(ZendeskPicassoTransformationFactory.INSTANCE.getRoundedTransformation(dp * 2, 0)).into(this.mAvatarImageView);
            } else {
                ZendeskPicassoProvider.getInstance(this.mContext).load(R.drawable.zd_user_default_avatar).transform(ZendeskPicassoTransformationFactory.INSTANCE.getRoundedTransformation(dp * 2, 0)).into(this.mAvatarImageView);
            }
            new Handler().post(new Runnable() {
                public void run() {
                    new CommentRowAttachmentHelper().attachPhotoToCommentRow(CommentRow.this.mAttachmentsContainer, comment.getComment(), CommentRow.this.mContext, CommentRow.this.mLoadImageCallback);
                }
            });
        }

        public View getView() {
            return this;
        }
    }

    private static class CommentRowAttachmentHelper {
        private static final String IMAGE_MIME_TYPE_START = "image/";
        public static Map<Long, Map<Long, Integer>> heightMap = new HashMap();

        private CommentRowAttachmentHelper() {
        }

        private void attachPhotoToCommentRow(ViewGroup container, CommentResponse comment, Context context, AttachmentOnClickListener attachmentOnClickListener) {
            int horizontalMargin = context.getResources().getDimensionPixelSize(R.dimen.attachments_horizontal_margin);
            final int verticalMargin = context.getResources().getDimensionPixelSize(R.dimen.attachments_vertical_margin);
            int widthPixels = container.getMeasuredWidth();
            if (widthPixels == 0) {
                widthPixels = context.getResources().getDisplayMetrics().widthPixels;
            }
            widthPixels -= horizontalMargin * 2;
            List<Attachment> attachments = comment.getAttachments();
            List<Attachment> inlineAttachments = new ArrayList();
            if (CollectionUtils.isNotEmpty(attachments)) {
                for (Attachment attachment : attachments) {
                    if (!(attachment == null || attachment.getContentType() == null || !attachment.getContentType().startsWith(IMAGE_MIME_TYPE_START))) {
                        inlineAttachments.add(attachment);
                    }
                }
            }
            final Map<Long, Integer> attachmentHeightMap = new HashMap();
            heightMap.put(comment.getId(), attachmentHeightMap);
            int inlineAttachmentIndex = 0;
            for (final Attachment attachment2 : inlineAttachments) {
                int bottomMargin = 0;
                int inlineAttachmentIndex2 = inlineAttachmentIndex + 1;
                if (inlineAttachmentIndex == inlineAttachments.size() - 1) {
                    bottomMargin = verticalMargin;
                }
                View inlineContainer = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.view_attachment_inline_item, container, false);
                LayoutParams layoutParams = (LayoutParams) inlineContainer.getLayoutParams();
                layoutParams.setMargins(horizontalMargin, verticalMargin, horizontalMargin, bottomMargin);
                inlineContainer.setLayoutParams(layoutParams);
                final ImageView imageView = (ImageView) inlineContainer.findViewById(R.id.attachment_inline_image);
                final ProgressBar progressBar = (ProgressBar) inlineContainer.findViewById(R.id.attachment_inline_progressbar);
                progressBar.setVisibility(0);
                final int finalBottomMargin = bottomMargin;
                ZendeskPicassoProvider.getInstance(context).load(attachment2.getContentUrl()).transform(ZendeskPicassoTransformationFactory.INSTANCE.getResizeTransformationWidth(widthPixels)).noFade().into(imageView, new Callback() {
                    public void onSuccess() {
                        attachmentHeightMap.put(attachment2.getId(), Integer.valueOf((verticalMargin + finalBottomMargin) + imageView.getDrawable().getIntrinsicHeight()));
                        progressBar.setVisibility(8);
                    }

                    public void onError() {
                        progressBar.setVisibility(8);
                    }
                });
                final AttachmentOnClickListener attachmentOnClickListener2 = attachmentOnClickListener;
                imageView.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        attachmentOnClickListener2.onAttachmentClicked(v, attachment2);
                    }
                });
                container.addView(inlineContainer);
                inlineAttachmentIndex = inlineAttachmentIndex2;
            }
        }
    }

    private static class EndUserRowIdHolder implements IdHolder {
        private EndUserRowIdHolder() {
        }

        public int getContainerId() {
            return R.layout.row_end_user_comment;
        }

        public int getAvatarId() {
            return R.id.view_request_end_user_avatar_imageview;
        }

        public int getNameId() {
            return R.id.view_request_end_user_name_textview;
        }

        public int getResponseId() {
            return R.id.view_request_end_user_response_textview;
        }

        public int getDateId() {
            return R.id.view_request_end_user_comment_date;
        }

        public int getAttachmentsContainerId() {
            return R.id.view_request_end_user_response_attachment_container;
        }
    }

    public RequestCommentsListAdapter(Context context, int layoutResource, List<CommentWithUser> listOfCommentsForRequest, AttachmentOnClickListener attachmentOnClickListener) {
        super(context, layoutResource, listOfCommentsForRequest);
        this.mContext = context;
        this.mAttachmentOnClickListener = attachmentOnClickListener;
    }

    public int getViewTypeCount() {
        return 2;
    }

    public int getItemViewType(int position) {
        return ((CommentWithUser) getItem(position)).getAuthor().isAgent() ? 0 : 1;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListRowView row;
        CommentWithUser commentWithUserForRow = (CommentWithUser) getItem(position);
        if (commentWithUserForRow.getAuthor().isAgent()) {
            row = new CommentRow(this.mContext, new AgentRowIdHolder(), this.mAttachmentOnClickListener);
        } else {
            row = new CommentRow(this.mContext, new EndUserRowIdHolder(), this.mAttachmentOnClickListener);
        }
        row.bind(commentWithUserForRow);
        return row.getView();
    }
}
