package com.google.android.gms.common.api;

import java.util.Map;
import java.util.WeakHashMap;

public abstract class zze {
    private static final Map<Object, zze> xX = new WeakHashMap();
    private static final Object zzaox = new Object();

    public abstract void remove(int i);
}
