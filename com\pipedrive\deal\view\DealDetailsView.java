package com.pipedrive.deal.view;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import com.pipedrive.model.Deal;

@MainThread
public interface DealDetailsView {
    void hideProgress();

    void onDealRetrieved(@NonNull Deal deal);

    void onDealUpdated(@NonNull Deal deal);

    void onFlowUpdated();

    void showProgress();
}
