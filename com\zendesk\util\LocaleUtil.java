package com.zendesk.util;

import android.os.Build.VERSION;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.zendesk.logger.Logger;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

public class LocaleUtil {
    private static final String LOG_TAG = LocaleUtil.class.getSimpleName();
    private static final List<String> NEW_ISO_CODES = Arrays.asList(new String[]{"he", "yi", PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID});

    private LocaleUtil() {
    }

    public static String toLanguageTag(Locale locale) {
        if (locale == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(locale.getLanguage());
        if (StringUtils.hasLength(locale.getCountry())) {
            sb.append("-");
            sb.append(locale.getCountry().toLowerCase(Locale.US));
        }
        return sb.toString();
    }

    public static Locale forLanguageTag(String localeString) {
        int expectedStringLength = 2;
        Logger.d(LOG_TAG, "Assuming Locale.getDefault()", new Object[0]);
        Locale locale = Locale.getDefault();
        if (!StringUtils.hasLength(localeString)) {
            return locale;
        }
        boolean hasExpectedTokenCount;
        StringTokenizer st = new StringTokenizer(localeString, "-");
        int numberOfTokens = st.countTokens();
        if (numberOfTokens == 1 || numberOfTokens == 2) {
            hasExpectedTokenCount = true;
        } else {
            hasExpectedTokenCount = false;
        }
        if (hasExpectedTokenCount) {
            if (numberOfTokens != 1) {
                expectedStringLength = 5;
            }
            if (expectedStringLength != localeString.length()) {
                Logger.d(LOG_TAG, "number of tokens is correct but the length of the locale string does not match the expected length", new Object[0]);
                return locale;
            }
            String language = st.nextToken();
            String country = (st.hasMoreTokens() ? st.nextToken() : "").toUpperCase(Locale.US);
            if (!NEW_ISO_CODES.contains(language)) {
                return new Locale(language, country);
            }
            Logger.d(LOG_TAG, "New ISO-6390-Alpha3 locale detected trying to create new locale per reflection", new Object[0]);
            Locale iso639Alpha3Locale = createIso639Alpha3LocaleJdk(language, country);
            if (iso639Alpha3Locale == null) {
                iso639Alpha3Locale = createIso639Alpha3LocaleAndroid(language, country);
            }
            if (iso639Alpha3Locale == null) {
                return new Locale(language, country);
            }
            return iso639Alpha3Locale;
        }
        Logger.w(LOG_TAG, "Unexpected number of tokens, must be at least one and at most two", new Object[0]);
        return locale;
    }

    private static Locale createIso639Alpha3LocaleJdk(String language, String country) {
        try {
            Method getInstance = Locale.class.getDeclaredMethod("createConstant", new Class[]{String.class, String.class});
            getInstance.setAccessible(true);
            return (Locale) getInstance.invoke(null, new Object[]{language, country});
        } catch (Exception e) {
            Logger.e(LOG_TAG, "Unable to create ISO-6390-Alpha3 per reflection", e, new Object[0]);
            return null;
        }
    }

    private static Locale createIso639Alpha3LocaleAndroid(String language, String country) {
        try {
            if (VERSION.SDK_INT >= 14) {
                Constructor<Locale> constructor = Locale.class.getDeclaredConstructor(new Class[]{Boolean.TYPE, String.class, String.class});
                constructor.setAccessible(true);
                return (Locale) constructor.newInstance(new Object[]{Boolean.valueOf(true), language, country});
            }
            Constructor constructor2 = Locale.class.getDeclaredConstructor(new Class[0]);
            constructor2.setAccessible(true);
            Locale locale = (Locale) constructor2.newInstance(new Object[0]);
            Class<? extends Locale> aClass = locale.getClass();
            Field languageCode = aClass.getDeclaredField("languageCode");
            languageCode.setAccessible(true);
            languageCode.set(locale, language);
            Field countryCode = aClass.getDeclaredField("countryCode");
            countryCode.setAccessible(true);
            countryCode.set(locale, country);
            return locale;
        } catch (Exception e) {
            Logger.e(LOG_TAG, "Unable to create ISO-6390-Alpha3 per reflection", e, new Object[0]);
            return null;
        }
    }
}
