package com.zendesk.sdk.model.push;

import io.fabric.sdk.android.services.common.AbstractSpiCall;

public abstract class PushRegistrationRequest {
    private final String deviceType = AbstractSpiCall.ANDROID_CLIENT_TYPE;
    private String identifier;
    private String locale;
    private String tokenType;

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getIdentifier() {
        return this.identifier;
    }

    public String getLocale() {
        return this.locale;
    }

    public String getTokenType() {
        return this.tokenType;
    }
}
