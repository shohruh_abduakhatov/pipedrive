package com.pipedrive.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.adapter.filter.FilterContentPipelineAdapter.FilterRow;
import com.pipedrive.util.networking.entities.UserEntity;

public class User extends BaseDatasourceEntity implements FilterRow {
    @Nullable
    private Boolean active;
    @Nullable
    private String iconUrl;
    @Nullable
    private String name;

    @Deprecated
    public User(int pipedriveId, @Nullable String name) {
        setPipedriveId(Long.valueOf((long) pipedriveId));
        this.name = name;
    }

    public User(@Nullable Long pipedriveId, @Nullable String name) {
        if (pipedriveId != null) {
            setPipedriveId(pipedriveId);
        }
        this.name = name;
    }

    @Nullable
    public static User fromEntity(@Nullable UserEntity userEntity) {
        if (userEntity == null) {
            return null;
        }
        User user = new User();
        user.setPipedriveId(userEntity.pipedriveId);
        user.setName(userEntity.name);
        user.setActive(userEntity.activeFlag);
        user.setIconUrl(userEntity.iconUrl);
        return user;
    }

    @Nullable
    public String getName() {
        return this.name;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }

    @Nullable
    public String getIconUrl() {
        return this.iconUrl;
    }

    public void setIconUrl(@Nullable String iconUrl) {
        this.iconUrl = iconUrl;
    }

    @Nullable
    public Boolean isActive() {
        return this.active;
    }

    public void setActive(@NonNull Boolean active) {
        this.active = active;
    }

    @Nullable
    public String getLabel() {
        return getName();
    }

    @NonNull
    public User getUser() {
        return this;
    }

    @Nullable
    public Filter getFilter() {
        return null;
    }

    public String toString() {
        return "User{name='" + this.name + '\'' + ", iconUrl='" + this.iconUrl + '\'' + ", active=" + this.active + '}';
    }
}
