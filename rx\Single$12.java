package rx;

class Single$12 extends SingleSubscriber<T> {
    final /* synthetic */ Single this$0;
    final /* synthetic */ Observer val$observer;

    Single$12(Single single, Observer observer) {
        this.this$0 = single;
        this.val$observer = observer;
    }

    public void onSuccess(T value) {
        this.val$observer.onNext(value);
        this.val$observer.onCompleted();
    }

    public void onError(Throwable error) {
        this.val$observer.onError(error);
    }
}
