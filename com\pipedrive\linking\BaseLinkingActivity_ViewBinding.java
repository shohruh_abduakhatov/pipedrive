package com.pipedrive.linking;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;
import com.pipedrive.views.CustomSearchViewForLinking;

public class BaseLinkingActivity_ViewBinding implements Unbinder {
    private BaseLinkingActivity target;
    private View view2131820590;
    private View view2131820825;

    @UiThread
    public BaseLinkingActivity_ViewBinding(BaseLinkingActivity target) {
        this(target, target.getWindow().getDecorView());
    }

    @UiThread
    public BaseLinkingActivity_ViewBinding(final BaseLinkingActivity target, View source) {
        this.target = target;
        target.mEmptyView = (TextView) Utils.findRequiredViewAsType(source, R.id.empty, "field 'mEmptyView'", TextView.class);
        View view = Utils.findRequiredView(source, R.id.list, "field 'mListView' and method 'onItemClick'");
        target.mListView = (ListView) Utils.castView(view, R.id.list, "field 'mListView'", ListView.class);
        this.view2131820825 = view;
        ((AdapterView) view).setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View p1, int p2, long p3) {
                target.onItemClick(p2);
            }
        });
        target.mCustomSearchViewForLinking = (CustomSearchViewForLinking) Utils.findRequiredViewAsType(source, R.id.search, "field 'mCustomSearchViewForLinking'", CustomSearchViewForLinking.class);
        target.toolbar = (Toolbar) Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
        view = Utils.findRequiredView(source, R.id.add, "field 'mFab' and method 'onAddClicked'");
        target.mFab = (FloatingActionButton) Utils.castView(view, R.id.add, "field 'mFab'", FloatingActionButton.class);
        this.view2131820590 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onAddClicked();
            }
        });
    }

    @CallSuper
    public void unbind() {
        BaseLinkingActivity target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mEmptyView = null;
        target.mListView = null;
        target.mCustomSearchViewForLinking = null;
        target.toolbar = null;
        target.mFab = null;
        ((AdapterView) this.view2131820825).setOnItemClickListener(null);
        this.view2131820825 = null;
        this.view2131820590.setOnClickListener(null);
        this.view2131820590 = null;
    }
}
