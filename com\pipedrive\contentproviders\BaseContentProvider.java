package com.pipedrive.contentproviders;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.util.StringUtils;

abstract class BaseContentProvider extends ContentProvider {
    private static final String URI_PARAMETER_SESSION_ID = "sessionId";
    @Nullable
    private String mSessionId = null;

    BaseContentProvider() {
    }

    BaseContentProvider(@Nullable Session session) {
        if (session == null) {
            LogJourno.reportEvent(EVENT.ContentProviders_providerInitiatedWithoutSession);
            Log.e(new Throwable("Session was null for base content provider! Content providers will not work!"));
            return;
        }
        this.mSessionId = session.getSessionID();
    }

    @Nullable
    private SQLiteDatabase getSQLiteDatabase(@Nullable Uri uri) {
        if (uri == null) {
            return null;
        }
        return getSQLiteDatabase(uri.getQueryParameter("sessionId"));
    }

    @Nullable
    private SQLiteDatabase getSQLiteDatabase(@Nullable String sessionId) {
        boolean sessionWasNotFoundFromActiveSessions;
        Session session = PipedriveApp.getSessionManager().findActiveSession(sessionId);
        if (session == null) {
            sessionWasNotFoundFromActiveSessions = true;
        } else {
            sessionWasNotFoundFromActiveSessions = false;
        }
        if (!sessionWasNotFoundFromActiveSessions) {
            return session.getDatabase();
        }
        LogJourno.reportEvent(EVENT.ContentProviders_databaseForSessionIdNotFound, String.format("sessionId:[%s]", new Object[]{sessionId}));
        Log.e(new Throwable("Active session not found! Cannot return DB."));
        return null;
    }

    @Nullable
    protected SQLiteDatabase getSQLiteDatabase() {
        return getSQLiteDatabase(this.mSessionId);
    }

    @NonNull
    protected Uri attachSessionIdToUri(@NonNull Uri uri) {
        if (StringUtils.isTrimmedAndEmpty(this.mSessionId)) {
            LogJourno.reportEvent(EVENT.ContentProviders_unableToAttachSessionIdToUri, String.format("sessionId:[%s] uri:[%s]", new Object[]{this.mSessionId, uri}));
            Log.e(new Throwable("Cannot attach session ID to the URI. For not crashing the app, returning original URI."));
            return uri;
        }
        Builder uriBuilder = uri.buildUpon();
        uriBuilder.appendQueryParameter("sessionId", this.mSessionId);
        return uriBuilder.build();
    }

    @Nullable
    @Deprecated
    public Uri attachSessionIdToUri_hackMethodToEnableCPUriBuildOutsideCP_BAD_DESIGN_AND_USAGE(Uri uri) {
        return attachSessionIdToUri(uri);
    }

    public boolean onCreate() {
        return false;
    }

    @Nullable
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    public final Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        boolean sqLiteDatabaseFoundFromUri;
        SQLiteDatabase sqLiteDatabase = getSQLiteDatabase(uri);
        if (sqLiteDatabase != null) {
            sqLiteDatabaseFoundFromUri = true;
        } else {
            sqLiteDatabaseFoundFromUri = false;
        }
        if (sqLiteDatabaseFoundFromUri) {
            return query(uri, projection, selection, selectionArgs, sortOrder, sqLiteDatabase);
        }
        LogJourno.reportEvent(EVENT.ContentProviders_databaseNotFoundForQueryTypeQuery, String.format("uri:[%s]", new Object[]{uri}));
        Log.e(new Throwable(String.format("Database not found for query. URI: %s", new Object[]{uri.toString()})));
        return null;
    }

    @Nullable
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder, @NonNull SQLiteDatabase sqLiteDatabase) {
        return null;
    }

    @Nullable
    public final Uri insert(@NonNull Uri uri, ContentValues values) {
        boolean sqLiteDatabaseFoundFromUri;
        SQLiteDatabase sqLiteDatabase = getSQLiteDatabase(uri);
        if (sqLiteDatabase != null) {
            sqLiteDatabaseFoundFromUri = true;
        } else {
            sqLiteDatabaseFoundFromUri = false;
        }
        if (sqLiteDatabaseFoundFromUri) {
            return insert(uri, values, sqLiteDatabase);
        }
        LogJourno.reportEvent(EVENT.ContentProviders_databaseNotFoundForQueryTypeInsert, String.format("uri:[%s]", new Object[]{uri}));
        Log.e(new Throwable(String.format("Database not found for insert. URI: %s", new Object[]{uri.toString()})));
        return null;
    }

    @Nullable
    public Uri insert(@NonNull Uri uri, ContentValues values, @NonNull SQLiteDatabase sqLiteDatabase) {
        return null;
    }

    public final int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        boolean sqLiteDatabaseFoundFromUri;
        SQLiteDatabase sqLiteDatabase = getSQLiteDatabase(uri);
        if (sqLiteDatabase != null) {
            sqLiteDatabaseFoundFromUri = true;
        } else {
            sqLiteDatabaseFoundFromUri = false;
        }
        if (sqLiteDatabaseFoundFromUri) {
            return delete(uri, selection, selectionArgs, sqLiteDatabase);
        }
        LogJourno.reportEvent(EVENT.ContentProviders_databaseNotFoundForQueryTypeDelete, String.format("uri:[%s]", new Object[]{uri}));
        Log.e(new Throwable(String.format("Database not found for delete. URI: %s", new Object[]{uri.toString()})));
        return 0;
    }

    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs, @NonNull SQLiteDatabase sqLiteDatabase) {
        return 0;
    }

    public final int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        boolean sqLiteDatabaseFoundFromUri;
        SQLiteDatabase sqLiteDatabase = getSQLiteDatabase(uri);
        if (sqLiteDatabase != null) {
            sqLiteDatabaseFoundFromUri = true;
        } else {
            sqLiteDatabaseFoundFromUri = false;
        }
        if (sqLiteDatabaseFoundFromUri) {
            return update(uri, values, selection, selectionArgs, sqLiteDatabase);
        }
        LogJourno.reportEvent(EVENT.ContentProviders_databaseNotFoundForQueryTypeUpdate, String.format("uri:[%s]", new Object[]{uri}));
        Log.e(new Throwable(String.format("Database not found for update. URI: %s", new Object[]{uri.toString()})));
        return 0;
    }

    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs, @NonNull SQLiteDatabase sqLiteDatabase) {
        return 0;
    }
}
