package com.pipedrive.util.networking.search;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.model.Organization;

public class OrganizationSearchResultEntity extends SearchResultEntity<Organization> {
    public OrganizationSearchResultEntity(@Nullable Integer pipedriveId, @Nullable String name) {
        super(pipedriveId, name);
    }

    @NonNull
    public Boolean isDifferentFrom(@NonNull Organization organization) {
        return Boolean.valueOf(!organization.getName().equals(getName()));
    }
}
