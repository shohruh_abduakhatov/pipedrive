package com.pipedrive.views.viewholder.contact;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class ContactRowViewHolder_ViewBinding implements Unbinder {
    private ContactRowViewHolder target;

    @UiThread
    public ContactRowViewHolder_ViewBinding(ContactRowViewHolder target, View source) {
        this.target = target;
        target.mName = (TextView) Utils.findRequiredViewAsType(source, R.id.name, "field 'mName'", TextView.class);
        target.mOrganization = (TextView) Utils.findRequiredViewAsType(source, R.id.organization, "field 'mOrganization'", TextView.class);
    }

    @CallSuper
    public void unbind() {
        ContactRowViewHolder target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mName = null;
        target.mOrganization = null;
    }
}
