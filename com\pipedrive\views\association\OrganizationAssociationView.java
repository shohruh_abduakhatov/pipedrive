package com.pipedrive.views.association;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.pipedrive.OrganizationDetailActivity;
import com.pipedrive.R;
import com.pipedrive.linking.OrganizationLinkingActivity;
import com.pipedrive.model.Organization;
import com.pipedrive.views.association.AssociationView.AssociationListener;

public class OrganizationAssociationView extends AssociationView<Organization> {
    public /* bridge */ /* synthetic */ void enableAssociationDetailViewOpening() {
        super.enableAssociationDetailViewOpening();
    }

    public /* bridge */ /* synthetic */ void setAssociationListener(@Nullable AssociationListener associationListener) {
        super.setAssociationListener(associationListener);
    }

    public OrganizationAssociationView(Context context) {
        super(context);
    }

    public OrganizationAssociationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OrganizationAssociationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @NonNull
    String getAssociationName(@NonNull Organization association) {
        return association.getName();
    }

    boolean canAssociationBeViewed(@NonNull Organization association) {
        return association.isStored();
    }

    void openAssociationDetailView(@NonNull Organization association) {
        OrganizationDetailActivity.startActivity(getContext(), Long.valueOf(association.getSqlId()));
    }

    int getAssociationNameHint() {
        return R.string.lbl_choose_organization;
    }

    @Nullable
    Integer getAssociationTypeIconImageLevel() {
        return Integer.valueOf(1);
    }

    void requestNewAssociation() {
        Context context = getContext();
        if (context != null && (context instanceof Activity) && getRequestCode() != null) {
            OrganizationLinkingActivity.startActivityForResult((Activity) getContext(), getRequestCode().intValue());
        }
    }
}
