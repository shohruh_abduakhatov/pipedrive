package com.zendesk.logger;

import com.zendesk.util.StringUtils;
import java.util.ArrayList;
import java.util.List;

class LoggerHelper {
    private static final String DEFAULT_LOG_TAG = "Zendesk";
    private static final int MAXIMUM_ANDROID_LOG_TAG_LENGTH = 23;

    private LoggerHelper() {
    }

    static List<String> splitLogMessage(String message, int maxLength) {
        List<String> messages = new ArrayList();
        if (maxLength < 1) {
            if (StringUtils.hasLength(message)) {
                messages.add(message);
            } else {
                messages.add("");
            }
        } else if (!StringUtils.hasLength(message)) {
            messages.add("");
        } else if (message.length() < maxLength) {
            messages.add(message);
        } else {
            int i = 0;
            int len = message.length();
            while (i < len) {
                int newLine;
                int indexOfSeparator = message.indexOf(StringUtils.LINE_SEPARATOR, i);
                if (indexOfSeparator != -1) {
                    newLine = indexOfSeparator;
                } else {
                    newLine = len;
                }
                do {
                    int end = Math.min(newLine, i + maxLength);
                    messages.add(message.substring(i, end));
                    i = end;
                } while (i < newLine);
                i++;
            }
        }
        return messages;
    }

    static char getLevelFromPriority(int priority) {
        switch (priority) {
            case 2:
                return 'V';
            case 3:
                return 'D';
            case 5:
                return 'W';
            case 6:
                return 'E';
            case 7:
                return 'A';
            default:
                return 'I';
        }
    }

    static String getAndroidTag(String tag) {
        if (StringUtils.isEmpty(tag)) {
            return DEFAULT_LOG_TAG;
        }
        return tag.length() > 23 ? tag.substring(0, 23) : tag;
    }
}
