package com.pipedrive.views.assignment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindString;
import butterknife.ButterKnife;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.model.User;
import com.pipedrive.util.UsersUtil;
import com.pipedrive.views.Spinner;
import com.pipedrive.views.Spinner.LabelBuilder;
import com.pipedrive.views.profilepicture.ProfilePictureRoundUserNoInitials;
import java.util.List;

abstract class SpinnerWithUserProfilePicture extends LinearLayout {
    @BindString(2131296356)
    String hiddenUserName;
    @NonNull
    private ProfilePictureRoundUserNoInitials mProfilePicture;
    @NonNull
    private Spinner<User> mSpinner;

    public interface OnItemSelectedListener {
        void onItemSelected(@NonNull User user);
    }

    @StringRes
    protected abstract int getSpinnerTitleResId();

    protected void loadAssignments(@NonNull Session session, long itemOwnerId, @Nullable OnItemSelectedListener onItemSelectedListener) {
        loadAssignments(session, itemOwnerId);
        setOnItemSelectedListener(onItemSelectedListener);
    }

    public SpinnerWithUserProfilePicture(Context context) {
        this(context, null);
    }

    public SpinnerWithUserProfilePicture(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SpinnerWithUserProfilePicture(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
        ButterKnife.bind(this);
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_spinner_with_profile_picture_view, this, true);
        this.mSpinner = (Spinner) ButterKnife.findById(this, R.id.spinner);
        this.mProfilePicture = (ProfilePictureRoundUserNoInitials) ButterKnife.findById(this, R.id.profilePicture);
        ((TextView) ButterKnife.findById(this, R.id.title)).setText(getSpinnerTitleResId());
    }

    protected void loadAssignments(@NonNull Session session, long selectedUserPipedriveId) {
        long selectedOrAuthenticatedUserPipedriveId;
        if (selectedUserPipedriveId == 0) {
            selectedOrAuthenticatedUserPipedriveId = session.getAuthenticatedUserID();
        } else {
            selectedOrAuthenticatedUserPipedriveId = selectedUserPipedriveId;
        }
        List<User> spinnerItems = buildSpinnerItems(session, selectedOrAuthenticatedUserPipedriveId);
        final long authenticatedUserPipedriveId = session.getAuthenticatedUserID();
        this.mSpinner.load(spinnerItems, new LabelBuilder<User>() {
            public String getLabel(User user) {
                String label = user.getName();
                if (authenticatedUserPipedriveId == ((long) user.getPipedriveId())) {
                    return label + SpinnerWithUserProfilePicture.this.getResources().getString(R.string._me);
                }
                return label;
            }

            public boolean isSelected(User user) {
                return ((long) user.getPipedriveId()) == selectedOrAuthenticatedUserPipedriveId;
            }
        }, R.layout.row_simple_spinner_selected_item_new, R.layout.row_simple_spinner_dropdown_item);
        setOnItemSelectedListener(null);
    }

    @NonNull
    private List<User> buildSpinnerItems(@NonNull Session session, long selectedUserPipedriveId) {
        List<User> activeCompanyUsers = UsersUtil.getActiveCompanyUsers(session);
        boolean selectedUserFoundAmongActiveOnes = false;
        for (User activeCompanyUser : activeCompanyUsers) {
            if (((long) activeCompanyUser.getPipedriveId()) == selectedUserPipedriveId) {
                selectedUserFoundAmongActiveOnes = true;
                break;
            }
        }
        if (!selectedUserFoundAmongActiveOnes) {
            activeCompanyUsers.add(new User(Long.valueOf(selectedUserPipedriveId).intValue(), this.hiddenUserName));
        }
        return activeCompanyUsers;
    }

    public void setOnItemSelectedListener(@Nullable final OnItemSelectedListener onItemSelectedListener) {
        this.mSpinner.setOnItemSelectedListener(new com.pipedrive.views.Spinner.OnItemSelectedListener<User>() {
            public void onItemSelected(User user) {
                SpinnerWithUserProfilePicture.this.mProfilePicture.loadPicture(user);
                if (onItemSelectedListener != null) {
                    onItemSelectedListener.onItemSelected(user);
                }
            }
        });
    }
}
