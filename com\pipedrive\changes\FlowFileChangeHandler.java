package com.pipedrive.changes;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.changes.Change.ChangeOperationType;
import com.pipedrive.datasource.FlowFilesDataSource;
import com.pipedrive.gson.GsonHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.Deal;
import com.pipedrive.model.FlowFile;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.util.flowfiles.FlowFileUploadNotificationManager;
import com.pipedrive.util.flowfiles.UriFileHandler;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.ConnectionUtil.JsonReaderInterceptor;
import com.pipedrive.util.networking.RequestMultipart;
import com.pipedrive.util.networking.Response;
import com.pipedrive.util.networking.entities.FlowFileEntity;
import com.pipedrive.util.snackbar.SnackBarManager;
import java.io.IOException;
import java.lang.reflect.Type;

public class FlowFileChangeHandler extends ChangeHandler {
    @NonNull
    ChangeHandled handleChange(@NonNull Session session, @NonNull Change flowFileChange) {
        boolean supportedCreateOperation = flowFileChange.getChangeOperationType() == ChangeOperationType.OPERATION_CREATE;
        boolean unsupportedUpdateOperation = flowFileChange.getChangeOperationType() == ChangeOperationType.OPERATION_UPDATE;
        if (supportedCreateOperation) {
            if (!ConnectionUtil.isConnected(session.getApplicationContext())) {
                return ChangeHandled.NOT_PROCESSED_PLEASE_RETRY;
            }
            JsonReaderInterceptor<AnonymousClass1FileResponse> fileResponseParser = new JsonReaderInterceptor<AnonymousClass1FileResponse>() {
                public AnonymousClass1FileResponse interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull AnonymousClass1FileResponse responseTemplate) throws IOException {
                    boolean ignoreNokResponse;
                    if (!responseTemplate.isRequestSuccessful || jsonReader.peek() == JsonToken.NULL) {
                        ignoreNokResponse = true;
                    } else {
                        ignoreNokResponse = false;
                    }
                    if (ignoreNokResponse) {
                        jsonReader.skipValue();
                    } else {
                        FlowFileEntity flowFileEntity = (FlowFileEntity) GsonHelper.fromJSON(jsonReader, (Type) FlowFileEntity.class);
                        if (flowFileEntity == null) {
                            LogJourno.reportEvent(EVENT.ChangesHandler_changeDataResponseToJsonParsingFailed, String.format("FileResponseTemplate:[%s]", new Object[]{responseTemplate}));
                            Log.e(new Throwable(String.format("Parsing of File from response failed! Response: %s", new Object[]{responseTemplate})));
                        } else if (responseTemplate.fileUnderChangeSqlId <= 0 || flowFileEntity.getPipedriveId() == null || flowFileEntity.getPipedriveId().intValue() <= 0) {
                            LogJourno.reportEvent(EVENT.ChangesHandler_cannotUpdatePipedriveIdFromChangeDataResponse, String.format("FileResponseTemplate:[%s] FileFromResponse:[%s]", new Object[]{responseTemplate, flowFileEntity}));
                            Log.e(new Throwable(String.format("Cannot update PD ID! SQL ID: %s; PD ID: %s", new Object[]{Long.valueOf(responseTemplate.fileUnderChangeSqlId), flowFileEntity.getPipedriveId()})));
                        } else {
                            new FlowFilesDataSource(session.getDatabase()).updatePipedriveIdBySqlId(responseTemplate.fileUnderChangeSqlId, flowFileEntity.getPipedriveId().intValue());
                        }
                    }
                    return responseTemplate;
                }
            };
            Long fileSqlId = flowFileChange.getMetaData();
            if (fileSqlId == null) {
                LogJourno.reportEvent(EVENT.ChangesHandler_changeMetadataNotFound, String.format("Change:[%s]", new Object[]{flowFileChange}));
                Log.e(new Throwable("Change metadata is missing! Cannot find the File to change!"));
                SnackBarManager.INSTANCE.showSnackBar(R.string.file_upload_failed_attach_it_again);
                return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
            }
            FlowFilesDataSource flowFilesDataSource = new FlowFilesDataSource(session.getDatabase());
            FlowFile flowFileUnderChange = (FlowFile) flowFilesDataSource.findBySqlId(fileSqlId.longValue());
            if (flowFileUnderChange == null) {
                LogJourno.reportEvent(EVENT.ChangesHandler_changeDataNotFound, String.format("Change:[%s] SQL id:[%s]", new Object[]{flowFileChange, fileSqlId}));
                Log.e(new Throwable(String.format("Change metadata is present (%s) but unable to find FlowFile that was registered for the change!", new Object[]{fileSqlId})));
                SnackBarManager.INSTANCE.showSnackBar(R.string.file_upload_failed_attach_it_again);
                return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
            }
            if (!flowFileUnderChange.isAllAssociationsExisting()) {
                LogJourno.reportEvent(EVENT.ChangesHandler_notAllChangePipedriveAssociationsExist, String.format("Change:[%s]", new Object[]{flowFileChange}));
                Log.e(new Throwable(String.format("FlowFile associations are not all existing! Change: %s", new Object[]{flowFileChange})));
            }
            UriFileHandler uriFileHandler = new UriFileHandler(session.getApplicationContext());
            try {
                Uri fileUri = Uri.parse(flowFileUnderChange.getUrl());
                Deal associateFileToThisDeal = flowFileUnderChange.getDeal();
                Person associateFileToThisPerson = flowFileUnderChange.getPerson();
                Organization associateFileToThisOrganization = flowFileUnderChange.getOrganization();
                Uri locationOfFileOnDisc = uriFileHandler.getLocalFileUri(fileUri, flowFileUnderChange.getFileType());
                if (locationOfFileOnDisc == null) {
                    LogJourno.reportEvent(EVENT.ChangesHandler_corruptedChangeFound, String.format("UriFileHandler failed to get file reference. fileUri:[%s] Change:[%s]", new Object[]{fileUri, flowFileChange}));
                    Log.e(new Throwable(String.format("Upload file request composition failed. FlowFile: %s", new Object[]{flowFileChange})));
                    fileUploadFailedTerminally(flowFilesDataSource, fileSqlId.longValue());
                    return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
                }
                RequestMultipart uploadFileRequest = RequestMultipart.uploadFileRequest(session, locationOfFileOnDisc, flowFileUnderChange.getName(), associateFileToThisDeal, associateFileToThisPerson, associateFileToThisOrganization);
                AnonymousClass1FileResponse fileFlowResponse = new Response() {
                    long fileUnderChangeSqlId;

                    public String toString() {
                        return "FileResponse:[fileUnderChangeSqlId=" + this.fileUnderChangeSqlId + "][super.toString:[" + super.toString() + "]]";
                    }
                };
                fileFlowResponse.fileUnderChangeSqlId = fileSqlId.longValue();
                fileUploadInProgress(flowFilesDataSource, fileSqlId.longValue());
                Response uploadFileResponse = ConnectionUtil.requestMultipartFormData(uploadFileRequest, fileResponseParser, fileFlowResponse);
                if (uploadFileResponse.isRequestSuccessful) {
                    fileUploadSuccessful(flowFilesDataSource, fileSqlId.longValue());
                } else {
                    fileUploadFailed(flowFilesDataSource, fileSqlId.longValue());
                }
                uriFileHandler.disposeLocalFileUri();
                return parseResponse(uploadFileResponse, this, flowFileChange);
            } catch (NullPointerException e) {
                LogJourno.reportEvent(EVENT.ChangesHandler_corruptedChangeFound, String.format("File location Uri parsing failed: [%s] Change:[%s]", new Object[]{e.getMessage(), flowFileChange}));
                Log.e(new Throwable(String.format("Upload file request composition failed. FlowFile: %s", new Object[]{flowFileChange}), e));
                uriFileHandler.disposeLocalFileUri();
                fileUploadFailedTerminally(flowFilesDataSource, fileSqlId.longValue());
                return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
            }
        } else if (unsupportedUpdateOperation) {
            LogJourno.reportEvent(EVENT.ChangesHandler_corruptedChangeFound, String.format("Operation update not supported. Change:[%s]", new Object[]{flowFileChange}));
            Log.e(new Throwable(String.format("Operation update not supported. FlowFile: %s", new Object[]{flowFileChange})));
            return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
        } else {
            LogJourno.reportEvent(EVENT.ChangesHandler_corruptedChangeFound, String.format("Change:[%s]", new Object[]{flowFileChange}));
            Log.e(new Throwable(String.format("Unknown change type requested for FlowFile: %s", new Object[]{flowFileChange})));
            return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
        }
    }

    private void fileUploadInProgress(@NonNull FlowFilesDataSource flowFilesDataSource, long flowFileSqlId) {
        updateFlowFileStatusWithUiUpdate(flowFilesDataSource, flowFileSqlId, Integer.valueOf(1));
    }

    private void fileUploadSuccessful(@NonNull FlowFilesDataSource flowFilesDataSource, long flowFileSqlId) {
        updateFlowFileStatusWithUiUpdate(flowFilesDataSource, flowFileSqlId, Integer.valueOf(2));
        SnackBarManager.INSTANCE.showSnackBar(R.string.file_uploaded);
    }

    private void fileUploadFailed(@NonNull FlowFilesDataSource flowFilesDataSource, long flowFileSqlId) {
        fileUploadFailedTerminally(flowFilesDataSource, flowFileSqlId);
    }

    private void fileUploadFailedTerminally(@NonNull FlowFilesDataSource flowFilesDataSource, long flowFileSqlId) {
        flowFilesDataSource.deleteBySqlId(flowFileSqlId);
        FlowFileUploadNotificationManager.INSTANCE.notifyFileUploadFailed(Long.valueOf(flowFileSqlId));
        SnackBarManager.INSTANCE.showSnackBar(R.string.file_upload_failed_attach_it_again);
    }

    private void updateFlowFileStatusWithUiUpdate(@NonNull FlowFilesDataSource flowFilesDataSource, long flowFileSqlId, @Nullable Integer fileUploadStatus) {
        FlowFileUploadNotificationManager.INSTANCE.notifyFileUploadSuccessful(Long.valueOf(flowFileSqlId));
        flowFilesDataSource.updateFlowFileUploadStatus(flowFileSqlId, fileUploadStatus);
    }
}
