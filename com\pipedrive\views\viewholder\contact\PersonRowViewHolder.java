package com.pipedrive.views.viewholder.contact;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import butterknife.BindView;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.model.Person;
import com.pipedrive.util.StringUtils;
import com.pipedrive.views.profilepicture.ProfilePictureRoundPerson;

public class PersonRowViewHolder extends ContactRowViewHolder {
    @BindView(2131821096)
    ImageView mCallButton;
    @Nullable
    private OnCallButtonClickListener mOnCallButtonClickListener;
    @BindView(2131820782)
    ProfilePictureRoundPerson mProfilePictureRoundPerson;

    public interface OnCallButtonClickListener {
        void onCallButtonClicked(@NonNull String str, @Nullable Long l);
    }

    public int getLayoutResourceId() {
        return R.layout.row_person;
    }

    public void fill(@NonNull Session session, @NonNull Person person, @NonNull SearchConstraint searchConstraint) {
        String contactOrganizationName;
        Long valueOf;
        if (person.getCompany() != null) {
            contactOrganizationName = person.getCompany().getName();
        } else {
            contactOrganizationName = null;
        }
        String name = person.getName();
        String phone = person.getPhone();
        if (person.isStored()) {
            valueOf = Long.valueOf(person.getSqlId());
        } else {
            valueOf = null;
        }
        fill(searchConstraint, name, contactOrganizationName, phone, valueOf);
        this.mProfilePictureRoundPerson.loadPicture(session, person);
    }

    void fill(@NonNull SearchConstraint searchConstraint, @Nullable String contactName, @Nullable String contactOrganizationName, @Nullable String contactPhone, @Nullable Long personSqlId) {
        setupCallButton(contactPhone, personSqlId);
        super.fill(searchConstraint, contactName, contactOrganizationName);
    }

    protected void setupCallButton(@Nullable final String contactPhone, @Nullable final Long personSqlId) {
        boolean hasPhoneNumber;
        int i = 0;
        if (StringUtils.isTrimmedAndEmpty(contactPhone)) {
            hasPhoneNumber = false;
        } else {
            hasPhoneNumber = true;
        }
        if (hasPhoneNumber) {
            this.mCallButton.setOnClickListener(new OnClickListener() {
                public void onClick(@NonNull View v) {
                    if (PersonRowViewHolder.this.mOnCallButtonClickListener != null && contactPhone != null) {
                        PersonRowViewHolder.this.mOnCallButtonClickListener.onCallButtonClicked(contactPhone, personSqlId);
                    }
                }
            });
        } else {
            this.mCallButton.setOnClickListener(null);
        }
        ImageView imageView = this.mCallButton;
        if (!hasPhoneNumber) {
            i = 8;
        }
        imageView.setVisibility(i);
    }

    public void setOnCallButtonClickListener(@Nullable OnCallButtonClickListener onCallButtonClickListener) {
        this.mOnCallButtonClickListener = onCallButtonClickListener;
    }
}
