package com.pipedrive.changes;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import com.pipedrive.application.Session;
import com.pipedrive.changes.Change.ChangeOperationType;
import com.pipedrive.datasource.ChangesDataSource;
import com.pipedrive.datasource.ChangesDataSource.Change;
import com.pipedrive.datasource.ChangesDataSource.DuplicateChanges;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import java.util.List;

public final class ChangesHelper {
    @NonNull
    private final ChangesDataSource mChangesDataSource = new ChangesDataSource(this.mSession);
    @NonNull
    private final Session mSession;

    ChangesHelper(@NonNull Session session) {
        this.mSession = session;
    }

    @WorkerThread
    public static void syncOfflineChanges(@Nullable Session session) {
        if (session == null) {
            LogJourno.reportEvent(EVENT.ChangesHandler_nullSessionPassedToHandler);
            Log.e(new Throwable("Cannot sync offline changes. Session is null!"));
            return;
        }
        new ChangesHelper(session).handleAllChanges();
    }

    void handleAllChanges() {
        for (DuplicateChanges noteChangeDuplicates : this.mChangesDataSource.getDuplicateUpdateChanges()) {
            boolean multipleChangesFound;
            if (noteChangeDuplicates.getCount() > 1) {
                multipleChangesFound = true;
            } else {
                multipleChangesFound = false;
            }
            boolean changeAcquiredFromDatabase;
            if (noteChangeDuplicates.getChange() == null || noteChangeDuplicates.getChange().change == null) {
                changeAcquiredFromDatabase = false;
            } else {
                changeAcquiredFromDatabase = true;
            }
            if (multipleChangesFound && changeAcquiredFromDatabase) {
                String logString = String.format("Multiple update changes (count: %s) found for type %s.", new Object[]{Integer.valueOf(noteChangeDuplicates.getCount()), noteChangeDuplicates.getChange().change.getChangeOperationType()});
                LogJourno.reportEvent(EVENT.ChangesHandler_multipleChangesCountFound, logString);
                Log.e(new Throwable(logString));
            }
        }
        handleAllCreateOperations();
        handleAllUpdateOperations();
        handleAllDeleteOperations();
    }

    void handleAllDeleteOperations() {
        handleAllChangeOperationsForType(ChangeOperationType.OPERATION_DELETE);
    }

    void handleAllUpdateOperations() {
        handleAllChangeOperationsForType(ChangeOperationType.OPERATION_UPDATE);
    }

    void handleAllCreateOperations() {
        handleAllChangeOperationsForType(ChangeOperationType.OPERATION_CREATE);
    }

    private void handleAllChangeOperationsForType(@NonNull ChangeOperationType operationTypeToHandle) {
        handleChanges(this.mChangesDataSource.getChanges(new OrganizationChanger(this.mSession), operationTypeToHandle), new OrganizationChangeHandler());
        handleChanges(this.mChangesDataSource.getChanges(new PersonChanger(this.mSession), operationTypeToHandle), new PersonChangeHandler());
        handleChanges(this.mChangesDataSource.getChanges(new DealChanger(this.mSession), operationTypeToHandle), new DealChangeHandler());
        handleChanges(this.mChangesDataSource.getChanges(new DealProductChanger(this.mSession), operationTypeToHandle), new DealProductChangeHandler());
        handleChanges(this.mChangesDataSource.getChanges(new ActivityChanger(this.mSession), operationTypeToHandle), new ActivityChangeHandler());
        handleChanges(this.mChangesDataSource.getChanges(new NoteChanger(this.mSession), operationTypeToHandle), new NoteChangeHandler());
        handleChanges(this.mChangesDataSource.getChanges(new FlowFileChanger(this.mSession), operationTypeToHandle), new FlowFileChangeHandler());
    }

    private void handleChanges(@NonNull List<Change> changesForType, @NonNull ChangeHandler changeHandler) {
        for (Change change : changesForType) {
            ChangeHandled changeHandled = changeHandler.handleChange(this.mSession, change.change);
            boolean removeTheChange = changeHandled == ChangeHandled.CHANGE_SUCCESSFUL || changeHandled == ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
            if (removeTheChange) {
                this.mChangesDataSource.deleteBySqlId(change.getSqlId());
            }
        }
    }
}
