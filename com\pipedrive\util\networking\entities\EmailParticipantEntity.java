package com.pipedrive.util.networking.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmailParticipantEntity {
    @SerializedName("address")
    @Expose
    private String mEmail;
    @SerializedName("name")
    @Expose
    private String mName;
    @SerializedName("person_id")
    @Expose
    private Integer mPersonId;
    @SerializedName("user_id")
    @Expose
    private Integer mUserId;

    public String getName() {
        return this.mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getEmail() {
        return this.mEmail;
    }

    public void setEmail(String email) {
        this.mEmail = email;
    }

    public Integer getPersonId() {
        return this.mPersonId;
    }

    public void setPersonId(Integer personId) {
        this.mPersonId = personId;
    }

    public Integer getUserId() {
        return this.mUserId;
    }

    public void setUserId(Integer userId) {
        this.mUserId = userId;
    }
}
