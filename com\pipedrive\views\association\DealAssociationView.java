package com.pipedrive.views.association;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.pipedrive.R;
import com.pipedrive.deal.view.DealDetailsActivity;
import com.pipedrive.linking.DealLinkingActivity;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.views.association.AssociationView.AssociationListener;

public class DealAssociationView extends AssociationView<Deal> {
    @Nullable
    private Organization mRelatedOrganization;
    @Nullable
    private Person mRelatedPerson;

    public /* bridge */ /* synthetic */ void enableAssociationDetailViewOpening() {
        super.enableAssociationDetailViewOpening();
    }

    public /* bridge */ /* synthetic */ void setAssociationListener(@Nullable AssociationListener associationListener) {
        super.setAssociationListener(associationListener);
    }

    public DealAssociationView(Context context) {
        super(context);
    }

    public DealAssociationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DealAssociationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @NonNull
    String getAssociationName(@NonNull Deal association) {
        return association.getTitle();
    }

    boolean canAssociationBeViewed(@NonNull Deal association) {
        return association.isStored();
    }

    void openAssociationDetailView(@NonNull Deal association) {
        if (getContext() instanceof Activity) {
            DealDetailsActivity.start((Activity) getContext(), Long.valueOf(association.getSqlId()));
        }
    }

    int getAssociationNameHint() {
        return R.string.lbl_choose_deal;
    }

    public void setAssociation(@Nullable Deal association, @Nullable Integer requestCode, @Nullable Person person, @Nullable Organization organization) {
        setAssociation(association, requestCode);
        this.mRelatedPerson = person;
        this.mRelatedOrganization = organization;
    }

    @Nullable
    Integer getAssociationTypeIconImageLevel() {
        return Integer.valueOf(2);
    }

    void requestNewAssociation() {
        Context context = getContext();
        if (context != null && (context instanceof Activity) && getRequestCode() != null) {
            Long personSqlId;
            Long organizationSqlId;
            if (this.mRelatedPerson != null) {
                personSqlId = Long.valueOf(this.mRelatedPerson.getSqlId());
            } else {
                personSqlId = null;
            }
            if (this.mRelatedOrganization != null) {
                organizationSqlId = Long.valueOf(this.mRelatedOrganization.getSqlId());
            } else {
                organizationSqlId = null;
            }
            DealLinkingActivity.startActivityForResult((Activity) getContext(), getRequestCode().intValue(), personSqlId, organizationSqlId);
        }
    }
}
