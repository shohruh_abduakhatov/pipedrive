package com.pipedrive.views;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class SelectedStagesIndicator_ViewBinding implements Unbinder {
    private SelectedStagesIndicator target;

    @UiThread
    public SelectedStagesIndicator_ViewBinding(SelectedStagesIndicator target) {
        this(target, target);
    }

    @UiThread
    public SelectedStagesIndicator_ViewBinding(SelectedStagesIndicator target, View source) {
        this.target = target;
        target.progress = (ImageView) Utils.findRequiredViewAsType(source, R.id.stages, "field 'progress'", ImageView.class);
        target.rootLayout = (RelativeLayout) Utils.findRequiredViewAsType(source, R.id.root, "field 'rootLayout'", RelativeLayout.class);
        target.defaultWidthPixelSize = source.getContext().getResources().getDimensionPixelSize(R.dimen.deal_card_selected_stage_indicator_default_width_offset);
    }

    @CallSuper
    public void unbind() {
        SelectedStagesIndicator target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.progress = null;
        target.rootLayout = null;
    }
}
