package com.pipedrive.sync;

import com.pipedrive.application.Session;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished.TaskResult;

class SyncManager$6 implements OnTaskFinished<Void> {
    final /* synthetic */ SyncManager this$0;
    final /* synthetic */ Runnable val$onEnoughDataToShowUISingleRunnable;

    SyncManager$6(SyncManager this$0, Runnable runnable) {
        this.this$0 = this$0;
        this.val$onEnoughDataToShowUISingleRunnable = runnable;
    }

    public void taskFinished(Session session, TaskResult taskResult, Void... params) {
        SyncManager.access$300(this.this$0, session, new SyncManager$OnReleaseSplashScreen() {
            public void release() {
                SyncManager$6.this.val$onEnoughDataToShowUISingleRunnable.run();
            }
        }, true);
    }
}
