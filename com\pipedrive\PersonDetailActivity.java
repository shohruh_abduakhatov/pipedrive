package com.pipedrive;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.pipedrive.activity.ActivityDetailActivity;
import com.pipedrive.application.Session;
import com.pipedrive.customfields.views.PersonCustomFieldListView.OnPersonCustomFieldListItemClickListener;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.flow.views.FlowView.OnItemClickListener;
import com.pipedrive.fragments.PersonDetailFragment;
import com.pipedrive.fragments.PersonDetailFragment.SetupProgressBarCallback;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.note.NoteUpdateActivity;
import com.pipedrive.store.StoreActivity;
import com.pipedrive.tasks.AsyncTask;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished.TaskResult;
import com.pipedrive.tasks.flow.DownloadFlowTask.DownloadPersonFlowTask;
import com.pipedrive.util.EmailHelper;
import com.pipedrive.util.PipedriveUtil;
import java.util.concurrent.Executors;

public class PersonDetailActivity extends BaseActivity implements SetupProgressBarCallback, OnItemClickListener, OnPersonCustomFieldListItemClickListener {
    private static final String KEY_PERSON_SQL_ID = "com.pipedrive.PERSON_SQL_ID";
    public static final int REQUEST_CODE_EDIT_PERSON = 0;
    public static final int RESULT_DELETED = 2;
    @Nullable
    private AsyncTask mDownloadFlowTask;
    @Nullable
    private PersonDetailFragment mPersonDetailFragment;

    public static void startActivity(@NonNull Context context, @NonNull Long personSqlId) {
        context.startActivity(new Intent(context, PersonDetailActivity.class).putExtra(KEY_PERSON_SQL_ID, personSqlId));
    }

    @Deprecated
    public static void startActivity(@NonNull Context context, Uri personUri) {
    }

    protected void onCreate(Bundle savedInstanceState) {
        boolean personDetailFragmentNotInStack;
        super.onCreate(savedInstanceState);
        this.mPersonDetailFragment = (PersonDetailFragment) getSupportFragmentManager().findFragmentByTag(PersonDetailFragment.TAG);
        if (this.mPersonDetailFragment == null) {
            personDetailFragmentNotInStack = true;
        } else {
            personDetailFragmentNotInStack = false;
        }
        if (personDetailFragmentNotInStack) {
            this.mPersonDetailFragment = PersonDetailFragment.newInstance(getPersonSqlId());
            getSupportFragmentManager().beginTransaction().disallowAddToBackStack().replace(16908290, this.mPersonDetailFragment, PersonDetailFragment.TAG).setTransition(0).commit();
        }
    }

    private Long getPersonSqlId() {
        return (Long) getIntent().getSerializableExtra(KEY_PERSON_SQL_ID);
    }

    public void onBackPressed() {
        if (this.mPersonDetailFragment == null || !this.mPersonDetailFragment.consumeBackPress()) {
            super.onBackPressed();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && resultCode == 2) {
            finish();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void showProgressIndicator() {
        showProgress();
    }

    public void hideProgressIndicator() {
        hideProgress();
    }

    public void setupProgressBarWithToolbar(Toolbar toolbar) {
        setupProgressBar(toolbar);
    }

    public void onActivityClicked(long activitySqlId) {
        ActivityDetailActivity.startActivityForActivity((Activity) this, activitySqlId);
    }

    public void onNoteClicked(long noteSqlId) {
        NoteUpdateActivity.updateNote((Context) this, noteSqlId);
    }

    public void onEmailClicked(long emailSqlId) {
        Person person = (Person) new PersonsDataSource(getSession().getDatabase()).findBySqlId(getPersonSqlId().longValue());
        if (person != null) {
            EmailDetailViewActivity.startActivity(this, emailSqlId, person.getDropBoxAddress());
        }
    }

    public void onFileClicked(long fileSqlId) {
        FileOpenActivity.startActivityForFlowFile(this, fileSqlId);
    }

    public void onActivityDoneChanged(long activitySqlId, boolean done) {
        changeActivityDoneState(activitySqlId, done);
    }

    private void changeActivityDoneState(long activitySqlId, boolean done) {
        com.pipedrive.model.Activity activity = (com.pipedrive.model.Activity) new ActivitiesDataSource(getSession()).findBySqlId(activitySqlId);
        if (activity != null) {
            activity.setDone(done);
            new StoreActivity(getSession()).update(activity);
            if (this.mPersonDetailFragment != null) {
                this.mPersonDetailFragment.refreshFlowView(getSession());
            }
        }
    }

    public void onNewActivityClicked() {
        ActivityDetailActivity.startActivityForPerson((Context) this, getPersonSqlId().longValue());
    }

    public void onResume() {
        super.onResume();
        startFlowDownloadIfNotStartedAlready();
    }

    private void startFlowDownloadIfNotStartedAlready() {
        if (!AsyncTask.isExecuting(getSession(), DownloadPersonFlowTask.class) && ((Person) new PersonsDataSource(getSession().getDatabase()).findBySqlId(getPersonSqlId().longValue())) != null) {
            this.mDownloadFlowTask = new DownloadPersonFlowTask(getSession(), new OnTaskFinished<Integer>() {
                public void taskFinished(Session session, TaskResult taskResult, Integer... params) {
                    if (PersonDetailActivity.this.mPersonDetailFragment != null) {
                        PersonDetailActivity.this.mPersonDetailFragment.refreshFlowView(PersonDetailActivity.this.getSession());
                    }
                    PersonDetailActivity.this.hideProgressIndicator();
                }
            }).executeOnExecutor(Executors.newSingleThreadExecutor(), new Integer[]{Integer.valueOf(person.getPipedriveId())});
            showProgressIndicator();
        }
    }

    protected void onPause() {
        stopFlowDownloadIfStarted();
        super.onPause();
    }

    private void stopFlowDownloadIfStarted() {
        boolean downloadFlowTaskIsExecuting;
        if (this.mDownloadFlowTask == null || !this.mDownloadFlowTask.isExecuting() || this.mDownloadFlowTask.isCancelled()) {
            downloadFlowTaskIsExecuting = false;
        } else {
            downloadFlowTaskIsExecuting = true;
        }
        if (downloadFlowTaskIsExecuting) {
            this.mDownloadFlowTask.cancel(false);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onEmailClicked(@NonNull String email, @NonNull String emailBccAddress) {
        EmailHelper.composeAndSendEmail(this, email, emailBccAddress);
    }

    public void onCustomFieldClicked(@NonNull CustomField customField) {
        CustomFieldEditActivity.startActivity(this, customField, getPersonSqlId().longValue());
    }

    public void onCustomFieldLongClicked(@NonNull String value) {
        PipedriveUtil.copyTextToClipboard(value, this);
    }

    public void onPhoneClicked(@NonNull String number, boolean clickedFromCustomField) {
        if (clickedFromCustomField) {
            PipedriveUtil.call(this, number);
            return;
        }
        Person person = (Person) new PersonsDataSource(getSession().getDatabase()).findBySqlId(getPersonSqlId().longValue());
        if (this.mPersonDetailFragment != null && person != null) {
            this.mPersonDetailFragment.makeCallWithCallSummary(number, person);
        }
    }

    public void onPersonClicked(@NonNull Person person) {
        startActivity((Context) this, Long.valueOf(person.getSqlId()));
    }

    public void onOrganizationClicked(@NonNull Organization organization) {
        OrganizationDetailActivity.startActivity((Context) this, Long.valueOf(organization.getSqlId()));
    }

    public void onAddressClicked(@NonNull Uri addressUri) {
        startActivity(new Intent("android.intent.action.VIEW", addressUri));
    }
}
