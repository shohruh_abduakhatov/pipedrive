package com.pipedrive.views.viewholder.activity;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datetime.LocaleHelper;
import com.pipedrive.datetime.PipedriveDate;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.model.Activity;
import com.pipedrive.model.Activity.DateTimeInfo;
import com.pipedrive.model.ActivityType;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.util.StringUtils;
import com.pipedrive.views.viewholder.ViewHolder;

public class ActivityRowViewHolder implements ViewHolder {
    @BindView(2131820952)
    TextView mNote;
    @BindView(2131820954)
    TextView mSubTitle;
    @BindView(2131820650)
    TextView mTitle;
    @BindView(2131820953)
    ImageView mType;

    public int getLayoutResourceId() {
        return R.layout.row_activity;
    }

    public void fill(@NonNull Session session, @NonNull Context context, @NonNull Activity activity) {
        Person person = activity.getPerson();
        final String personName = person == null ? null : person.getName();
        Organization organization = activity.getOrganization();
        final String organizationName = organization == null ? null : organization.getName();
        final Session session2 = session;
        final Context context2 = context;
        final Activity activity2 = activity;
        activity.request(new DateTimeInfo() {
            public void activityHasStartDateTime(@NonNull PipedriveDateTime dateTime, @NonNull Activity inActivity) {
                ActivityRowViewHolder.this.fill(context2, activity2.getType(), activity2.getSubject(), personName, organizationName, LocaleHelper.getLocaleBasedDateTimeStringInCurrentTimeZone(session2, dateTime), activity2.isDone(), activity2.isOverdue(), activity2.isDueToday(), activity2.getNote());
            }

            public void activityHasStartDate(@NonNull PipedriveDate date, @NonNull Activity inActivity) {
                ActivityRowViewHolder.this.fill(context2, activity2.getType(), activity2.getSubject(), personName, organizationName, LocaleHelper.getLocaleBasedDateString(session2, date), activity2.isDone(), activity2.isOverdue(), activity2.isDueToday(), activity2.getNote());
            }
        });
    }

    protected void fill(@NonNull Context context, @Nullable ActivityType type, @Nullable String subject, @Nullable String personName, @Nullable String organizationName, @NonNull String dateString, boolean done, @Nullable Boolean overdue, @Nullable Boolean dueToday, @Nullable String note) {
        String subtitle;
        if (type != null) {
            this.mType.setImageResource(type.getImageResourceId());
        }
        this.mTitle.setText(subject);
        if (buildAssociationString(context, personName, organizationName).isEmpty()) {
            subtitle = dateString;
        } else {
            subtitle = context.getString(R.string.activity_list_item_subtitle, new Object[]{dateString, buildAssociationString(context, personName, organizationName)});
        }
        SpannableString spannableString = new SpannableString(subtitle);
        if (!done) {
            if (overdue != null && overdue.booleanValue()) {
                spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.red)), 0, dateString.length(), 33);
            } else if (dueToday != null && dueToday.booleanValue()) {
                spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.green)), 0, dateString.length(), 33);
            }
        }
        this.mSubTitle.setText(spannableString);
        if (TextUtils.isEmpty(note)) {
            this.mNote.setVisibility(8);
            return;
        }
        String noteInHtmlFormat;
        String safeContentForWebView = "";
        if (TextUtils.isEmpty(note)) {
            noteInHtmlFormat = "";
        } else {
            noteInHtmlFormat = note;
        }
        this.mNote.setText(Html.fromHtml(noteInHtmlFormat).toString().replaceAll("\\n\\n", "\n"));
        this.mNote.setVisibility(0);
    }

    @NonNull
    private String buildAssociationString(@NonNull Context context, @Nullable String personName, @Nullable String organizationName) {
        boolean personNameExist;
        if (StringUtils.isTrimmedAndEmpty(personName)) {
            personNameExist = false;
        } else {
            personNameExist = true;
        }
        boolean organizationNameExist;
        if (StringUtils.isTrimmedAndEmpty(organizationName)) {
            organizationNameExist = false;
        } else {
            organizationNameExist = true;
        }
        if (!personNameExist && !organizationNameExist) {
            return "";
        }
        if (!personNameExist || !organizationNameExist) {
            return !personNameExist ? organizationName : personName;
        } else {
            return context.getString(R.string.activity_list_item_subtitle, new Object[]{personName, organizationName});
        }
    }
}
