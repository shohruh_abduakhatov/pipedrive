package com.pipedrive.util.communicatiomedium;

import android.support.annotation.NonNull;
import com.pipedrive.model.Deal;
import com.pipedrive.model.contact.CommunicationMedium;
import java.util.Collections;
import java.util.List;

public class DealCommunicationMediumUtil implements CommunicationMediumUtil<Deal> {
    public boolean hasPhones(@NonNull Deal deal) {
        return deal.getPerson() != null && deal.getPerson().hasPhones();
    }

    public boolean hasEmails(@NonNull Deal deal) {
        return deal.getPerson() != null && deal.getPerson().hasEmails();
    }

    public boolean hasMultiplePhones(@NonNull Deal deal) {
        return deal.getPerson() != null && deal.getPerson().hasMultiplePhones();
    }

    @NonNull
    public List<? extends CommunicationMedium> getPhones(@NonNull Deal deal) {
        return deal.getPerson() != null ? deal.getPerson().getPhones() : Collections.emptyList();
    }

    @NonNull
    public List<? extends CommunicationMedium> getEmails(@NonNull Deal deal) {
        return deal.getPerson() != null ? deal.getPerson().getEmails() : Collections.emptyList();
    }
}
