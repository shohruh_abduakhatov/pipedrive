package com.pipedrive.model.contact;

import android.support.annotation.Nullable;
import org.json.JSONObject;

public class Email extends CommunicationMedium {
    public Email(String email, String label) {
        this(email, label, false);
    }

    public Email(String email, String label, boolean isPrimary) {
        super(email, label, isPrimary);
    }

    public Email(@Nullable JSONObject jsonObject) {
        super(jsonObject);
    }

    public final String getEmail() {
        return getValue();
    }
}
