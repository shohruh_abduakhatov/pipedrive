package com.pipedrive.nearby.searchbutton;

import android.support.annotation.NonNull;
import com.pipedrive.nearby.util.Irrelevant;
import rx.Observable;

public interface SearchButtonClickProvider {
    @NonNull
    Observable<Irrelevant> searchButtonClicks();
}
