package com.zendesk.sdk.power;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.zendesk.logger.Logger;
import java.util.Locale;

public class PowerConfig {
    public static final float FIFTEEN_PERCENT_BATTERY_CHARGE = 0.15f;
    private static final String LOG_TAG = PowerConfig.class.getSimpleName();
    public static final float THIRTY_PERCENT_BATTERY_CHARGE = 0.3f;
    private static PowerConfig sInstance;
    private boolean mBatteryOk;
    private boolean mHasReadSyncSettingsPermission;

    private PowerConfig(Context context) {
        Logger.d(LOG_TAG, "PowerConfig is initialising...", new Object[0]);
        Intent batteryStatus = context.getApplicationContext().registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (batteryStatus == null) {
            Logger.w(LOG_TAG, "Battery status is unknown, assuming it is ok...", new Object[0]);
            this.mBatteryOk = true;
            return;
        }
        boolean pluggedIntoAc;
        boolean z;
        float batteryPercentage = ((float) batteryStatus.getIntExtra(Param.LEVEL, -1)) / ((float) batteryStatus.getIntExtra("scale", -1));
        if (batteryStatus.getIntExtra("plugged", -1) == 1) {
            pluggedIntoAc = true;
        } else {
            pluggedIntoAc = false;
        }
        Logger.d(LOG_TAG, String.format(Locale.US, "Ac charging is %s and battery capacity is %s", new Object[]{Boolean.valueOf(pluggedIntoAc), Float.valueOf(batteryPercentage)}), new Object[0]);
        if ((!pluggedIntoAc || batteryPercentage <= FIFTEEN_PERCENT_BATTERY_CHARGE) && batteryPercentage <= 0.3f) {
            z = false;
        } else {
            z = true;
        }
        this.mBatteryOk = z;
        Logger.d(LOG_TAG, String.format(Locale.US, "Initial battery state is OK?: %s", new Object[]{Boolean.valueOf(this.mBatteryOk)}), new Object[0]);
        if (context.checkCallingOrSelfPermission("android.permission.READ_SYNC_SETTINGS") == 0) {
            z = true;
        } else {
            z = false;
        }
        this.mHasReadSyncSettingsPermission = z;
        Logger.d(LOG_TAG, String.format(Locale.US, "Has permission to read sync settings: %s", new Object[]{Boolean.valueOf(this.mHasReadSyncSettingsPermission)}), new Object[0]);
    }

    public static synchronized PowerConfig getInstance(Context context) {
        PowerConfig powerConfig;
        synchronized (PowerConfig.class) {
            if (sInstance == null) {
                sInstance = new PowerConfig(context);
            }
            powerConfig = sInstance;
        }
        return powerConfig;
    }

    public boolean isBatteryOk() {
        return this.mBatteryOk;
    }

    public boolean isBatteryLow() {
        return !this.mBatteryOk;
    }

    void setBatteryOk(boolean batteryOk) {
        Logger.d(LOG_TAG, "Setting batteryOk to: " + batteryOk, new Object[0]);
        this.mBatteryOk = batteryOk;
    }

    public boolean isGlobalSyncEnabled() {
        if (this.mHasReadSyncSettingsPermission) {
            return ContentResolver.getMasterSyncAutomatically();
        }
        return true;
    }

    public boolean isGlobalSyncDisabled() {
        return !isGlobalSyncEnabled();
    }
}
