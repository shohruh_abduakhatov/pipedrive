package com.pipedrive.model;

public interface AssociatedDatasourceEntity {
    boolean isAllAssociationsExisting();
}
