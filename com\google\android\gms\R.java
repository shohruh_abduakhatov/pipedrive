package com.google.android.gms;

public final class R {

    public static final class attr {
        public static final int ambientEnabled = 2130772253;
        public static final int buttonSize = 2130772298;
        public static final int cameraBearing = 2130772238;
        public static final int cameraMaxZoomPreference = 2130772255;
        public static final int cameraMinZoomPreference = 2130772254;
        public static final int cameraTargetLat = 2130772239;
        public static final int cameraTargetLng = 2130772240;
        public static final int cameraTilt = 2130772241;
        public static final int cameraZoom = 2130772242;
        public static final int circleCrop = 2130772236;
        public static final int colorScheme = 2130772299;
        public static final int imageAspectRatio = 2130772235;
        public static final int imageAspectRatioAdjust = 2130772234;
        public static final int latLngBoundsNorthEastLatitude = 2130772258;
        public static final int latLngBoundsNorthEastLongitude = 2130772259;
        public static final int latLngBoundsSouthWestLatitude = 2130772256;
        public static final int latLngBoundsSouthWestLongitude = 2130772257;
        public static final int liteMode = 2130772243;
        public static final int mapType = 2130772237;
        public static final int scopeUris = 2130772300;
        public static final int uiCompass = 2130772244;
        public static final int uiMapToolbar = 2130772252;
        public static final int uiRotateGestures = 2130772245;
        public static final int uiScrollGestures = 2130772246;
        public static final int uiTiltGestures = 2130772247;
        public static final int uiZoomControls = 2130772248;
        public static final int uiZoomGestures = 2130772249;
        public static final int useViewLifecycle = 2130772250;
        public static final int zOrderOnTop = 2130772251;
    }

    public static final class color {
        public static final int common_google_signin_btn_text_dark = 2131755282;
        public static final int common_google_signin_btn_text_dark_default = 2131755070;
        public static final int common_google_signin_btn_text_dark_disabled = 2131755071;
        public static final int common_google_signin_btn_text_dark_focused = 2131755072;
        public static final int common_google_signin_btn_text_dark_pressed = 2131755073;
        public static final int common_google_signin_btn_text_light = 2131755283;
        public static final int common_google_signin_btn_text_light_default = 2131755074;
        public static final int common_google_signin_btn_text_light_disabled = 2131755075;
        public static final int common_google_signin_btn_text_light_focused = 2131755076;
        public static final int common_google_signin_btn_text_light_pressed = 2131755077;
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2130837672;
        public static final int common_google_signin_btn_icon_dark = 2130837673;
        public static final int common_google_signin_btn_icon_dark_disabled = 2130837674;
        public static final int common_google_signin_btn_icon_dark_focused = 2130837675;
        public static final int common_google_signin_btn_icon_dark_normal = 2130837676;
        public static final int common_google_signin_btn_icon_dark_pressed = 2130837677;
        public static final int common_google_signin_btn_icon_light = 2130837678;
        public static final int common_google_signin_btn_icon_light_disabled = 2130837679;
        public static final int common_google_signin_btn_icon_light_focused = 2130837680;
        public static final int common_google_signin_btn_icon_light_normal = 2130837681;
        public static final int common_google_signin_btn_icon_light_pressed = 2130837682;
        public static final int common_google_signin_btn_text_dark = 2130837683;
        public static final int common_google_signin_btn_text_dark_disabled = 2130837684;
        public static final int common_google_signin_btn_text_dark_focused = 2130837685;
        public static final int common_google_signin_btn_text_dark_normal = 2130837686;
        public static final int common_google_signin_btn_text_dark_pressed = 2130837687;
        public static final int common_google_signin_btn_text_light = 2130837688;
        public static final int common_google_signin_btn_text_light_disabled = 2130837689;
        public static final int common_google_signin_btn_text_light_focused = 2130837690;
        public static final int common_google_signin_btn_text_light_normal = 2130837691;
        public static final int common_google_signin_btn_text_light_pressed = 2130837692;
        public static final int ic_plusone_medium_off_client = 2130837755;
        public static final int ic_plusone_small_off_client = 2130837756;
        public static final int ic_plusone_standard_off_client = 2130837757;
        public static final int ic_plusone_tall_off_client = 2130837758;
    }

    public static final class id {
        public static final int adjust_height = 2131820622;
        public static final int adjust_width = 2131820623;
        public static final int auto = 2131820597;
        public static final int button = 2131820862;
        public static final int center = 2131820599;
        public static final int dark = 2131820635;
        public static final int email = 2131821158;
        public static final int hybrid = 2131820624;
        public static final int icon_only = 2131820632;
        public static final int light = 2131820636;
        public static final int none = 2131820580;
        public static final int normal = 2131820576;
        public static final int radio = 2131820669;
        public static final int satellite = 2131820625;
        public static final int standard = 2131820633;
        public static final int terrain = 2131820626;
        public static final int text = 2131820776;
        public static final int text1 = 2131820945;
        public static final int text2 = 2131821031;
        public static final int toolbar = 2131820700;
        public static final int wide = 2131820634;
        public static final int wrap_content = 2131820596;
    }

    public static final class integer {
        public static final int google_play_services_version = 2131689488;
    }

    public static final class raw {
        public static final int gtm_analytics = 2131230740;
    }

    public static final class string {
        public static final int common_google_play_services_enable_button = 2131296275;
        public static final int common_google_play_services_enable_text = 2131296276;
        public static final int common_google_play_services_enable_title = 2131296277;
        public static final int common_google_play_services_install_button = 2131296278;
        public static final int common_google_play_services_install_text = 2131296279;
        public static final int common_google_play_services_install_title = 2131296280;
        public static final int common_google_play_services_notification_ticker = 2131296281;
        public static final int common_google_play_services_unknown_issue = 2131296282;
        public static final int common_google_play_services_unsupported_text = 2131296283;
        public static final int common_google_play_services_update_button = 2131296284;
        public static final int common_google_play_services_update_text = 2131296285;
        public static final int common_google_play_services_update_title = 2131296286;
        public static final int common_google_play_services_updating_text = 2131296287;
        public static final int common_google_play_services_wear_update_text = 2131296288;
        public static final int common_open_on_phone = 2131296289;
        public static final int common_signin_button_text = 2131296290;
        public static final int common_signin_button_text_long = 2131296291;
    }

    public static final class styleable {
        public static final int[] LoadingImageView = new int[]{com.pipedrive.R.attr.imageAspectRatioAdjust, com.pipedrive.R.attr.imageAspectRatio, com.pipedrive.R.attr.circleCrop};
        public static final int LoadingImageView_circleCrop = 2;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 0;
        public static final int[] MapAttrs = new int[]{com.pipedrive.R.attr.mapType, com.pipedrive.R.attr.cameraBearing, com.pipedrive.R.attr.cameraTargetLat, com.pipedrive.R.attr.cameraTargetLng, com.pipedrive.R.attr.cameraTilt, com.pipedrive.R.attr.cameraZoom, com.pipedrive.R.attr.liteMode, com.pipedrive.R.attr.uiCompass, com.pipedrive.R.attr.uiRotateGestures, com.pipedrive.R.attr.uiScrollGestures, com.pipedrive.R.attr.uiTiltGestures, com.pipedrive.R.attr.uiZoomControls, com.pipedrive.R.attr.uiZoomGestures, com.pipedrive.R.attr.useViewLifecycle, com.pipedrive.R.attr.zOrderOnTop, com.pipedrive.R.attr.uiMapToolbar, com.pipedrive.R.attr.ambientEnabled, com.pipedrive.R.attr.cameraMinZoomPreference, com.pipedrive.R.attr.cameraMaxZoomPreference, com.pipedrive.R.attr.latLngBoundsSouthWestLatitude, com.pipedrive.R.attr.latLngBoundsSouthWestLongitude, com.pipedrive.R.attr.latLngBoundsNorthEastLatitude, com.pipedrive.R.attr.latLngBoundsNorthEastLongitude};
        public static final int MapAttrs_ambientEnabled = 16;
        public static final int MapAttrs_cameraBearing = 1;
        public static final int MapAttrs_cameraMaxZoomPreference = 18;
        public static final int MapAttrs_cameraMinZoomPreference = 17;
        public static final int MapAttrs_cameraTargetLat = 2;
        public static final int MapAttrs_cameraTargetLng = 3;
        public static final int MapAttrs_cameraTilt = 4;
        public static final int MapAttrs_cameraZoom = 5;
        public static final int MapAttrs_latLngBoundsNorthEastLatitude = 21;
        public static final int MapAttrs_latLngBoundsNorthEastLongitude = 22;
        public static final int MapAttrs_latLngBoundsSouthWestLatitude = 19;
        public static final int MapAttrs_latLngBoundsSouthWestLongitude = 20;
        public static final int MapAttrs_liteMode = 6;
        public static final int MapAttrs_mapType = 0;
        public static final int MapAttrs_uiCompass = 7;
        public static final int MapAttrs_uiMapToolbar = 15;
        public static final int MapAttrs_uiRotateGestures = 8;
        public static final int MapAttrs_uiScrollGestures = 9;
        public static final int MapAttrs_uiTiltGestures = 10;
        public static final int MapAttrs_uiZoomControls = 11;
        public static final int MapAttrs_uiZoomGestures = 12;
        public static final int MapAttrs_useViewLifecycle = 13;
        public static final int MapAttrs_zOrderOnTop = 14;
        public static final int[] SignInButton = new int[]{com.pipedrive.R.attr.buttonSize, com.pipedrive.R.attr.colorScheme, com.pipedrive.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
    }
}
