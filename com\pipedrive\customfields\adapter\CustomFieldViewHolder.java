package com.pipedrive.customfields.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.google.android.gms.plus.PlusShare;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.contentproviders.OrganizationContentProvider;
import com.pipedrive.customfields.views.CustomFieldListView.OnItemClickListener;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.datetime.DateFormatHelper;
import com.pipedrive.datetime.LocaleHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.User;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.util.CustomFieldsUtil;
import com.pipedrive.util.PipedriveUtil;
import com.pipedrive.util.UsersUtil;
import com.pipedrive.util.formatter.DecimalFormatter;
import com.pipedrive.util.formatter.MonetaryFormatter;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import org.json.JSONException;

class CustomFieldViewHolder extends ViewHolder {
    @Nullable
    @BindView(2131821062)
    ImageView mIcon;
    @BindView(2131821063)
    public TextView mTitle;
    @Nullable
    @BindView(2131821064)
    public TextView mValue;

    CustomFieldViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind((Object) this, itemView);
    }

    public void bind(@NonNull Session session, @NonNull CustomField customField, @Nullable OnItemClickListener onItemClickListener) {
        this.mTitle.setText(customField.getName());
        String fieldDataType = customField.getFieldDataType();
        Object obj = -1;
        switch (fieldDataType.hashCode()) {
            case -1325958191:
                if (fieldDataType.equals(CustomField.FIELD_DATA_TYPE_DOUBLE)) {
                    obj = 12;
                    break;
                }
                break;
            case -1147692044:
                if (fieldDataType.equals("address")) {
                    obj = 11;
                    break;
                }
                break;
            case -991716523:
                if (fieldDataType.equals("person")) {
                    obj = 2;
                    break;
                }
                break;
            case -332506163:
                if (fieldDataType.equals(CustomField.FIELD_DATA_TYPE_MONEY)) {
                    obj = 9;
                    break;
                }
                break;
            case -231872945:
                if (fieldDataType.equals(CustomField.FIELD_DATA_TYPE_DATE_RANGE)) {
                    obj = 5;
                    break;
                }
                break;
            case 113762:
                if (fieldDataType.equals(CustomField.FIELD_DATA_TYPE_MULTIPLE_OPTION)) {
                    obj = 1;
                    break;
                }
                break;
            case 3076014:
                if (fieldDataType.equals("date")) {
                    obj = 6;
                    break;
                }
                break;
            case 3118337:
                if (fieldDataType.equals(CustomField.FIELD_DATA_TYPE_SINGLE_OPTION)) {
                    obj = null;
                    break;
                }
                break;
            case 3560141:
                if (fieldDataType.equals("time")) {
                    obj = 8;
                    break;
                }
                break;
            case 3599307:
                if (fieldDataType.equals(CustomField.FIELD_DATA_TYPE_USER)) {
                    obj = 4;
                    break;
                }
                break;
            case 53649040:
                if (fieldDataType.equals(CustomField.FIELD_DATA_TYPE_TIME_RANGE)) {
                    obj = 7;
                    break;
                }
                break;
            case 106642798:
                if (fieldDataType.equals("phone")) {
                    obj = 10;
                    break;
                }
                break;
            case 1178922291:
                if (fieldDataType.equals("organization")) {
                    obj = 3;
                    break;
                }
                break;
        }
        switch (obj) {
            case null:
                bindSingleOptionField(customField);
                break;
            case 1:
                bindMultipleOptionField(customField);
                break;
            case 2:
                bindPersonField(session, customField, onItemClickListener);
                break;
            case 3:
                bindOrganizationField(session, customField, onItemClickListener);
                break;
            case 4:
                bindUserField(session, customField);
                break;
            case 5:
                bindDateRangeField(session, customField);
                break;
            case 6:
                bindDateField(session, customField);
                break;
            case 7:
                bindTimeRangeField(session, customField);
                break;
            case 8:
                bindTimeField(session, customField);
                break;
            case 9:
                bindMonetaryField(session, customField);
                break;
            case 10:
                bindPhoneField(customField, onItemClickListener);
                break;
            case 11:
                bindAddressField(customField, onItemClickListener);
                break;
            case 12:
                bindDecimalField(session, customField);
                break;
            default:
                if (this.mValue != null) {
                    this.mValue.setText(customField.getTempValue());
                    break;
                }
                break;
        }
        bindListeners(customField, onItemClickListener);
    }

    private void bindDecimalField(@NonNull Session session, @NonNull CustomField customField) {
        if (this.mValue != null) {
            this.mValue.setText(new DecimalFormatter(session).formatDecimalCustomField(customField));
        }
    }

    private void bindListeners(@NonNull final CustomField customField, @Nullable final OnItemClickListener onItemClickListener) {
        this.itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onCustomFieldClicked(customField);
                }
            }
        });
        this.itemView.setOnLongClickListener(new OnLongClickListener() {
            public boolean onLongClick(View v) {
                if (onItemClickListener == null || CustomFieldViewHolder.this.mValue == null) {
                    return false;
                }
                onItemClickListener.onCustomFieldLongClicked(CustomFieldViewHolder.this.mValue.getText().toString());
                return true;
            }
        });
    }

    private void bindAddressField(@NonNull CustomField customField, @Nullable final OnItemClickListener onItemClickListener) {
        if (this.mValue != null) {
            this.mValue.setText(customField.getTempValue());
        }
        final Uri mapUri = PipedriveUtil.getMapUriForAddress(customField.getTempValue());
        if (this.mIcon != null) {
            this.mIcon.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onAddressClicked(mapUri);
                    }
                }
            });
        }
    }

    private void bindPhoneField(@NonNull final CustomField customField, @Nullable final OnItemClickListener onItemClickListener) {
        if (this.mValue != null) {
            this.mValue.setText(customField.getTempValue());
        }
        if (this.mIcon != null) {
            this.mIcon.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onPhoneClicked(customField.getTempValue(), true);
                    }
                }
            });
        }
    }

    private void bindOrganizationField(@NonNull Session session, @NonNull CustomField customField, @Nullable final OnItemClickListener onItemClickListener) {
        Uri orgUri = OrganizationContentProvider.getOrganizationUriFromCustomField(customField, session, CustomFieldsUtil.getIdFromCustomField(customField));
        if (orgUri != null) {
            final Organization org = OrganizationContentProvider.getOrganizationFromUri(session.getApplicationContext().getContentResolver(), orgUri);
            if (org != null) {
                if (this.mValue != null) {
                    this.mValue.setText(org.getName());
                }
                if (this.mIcon != null) {
                    this.mIcon.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            if (onItemClickListener != null) {
                                onItemClickListener.onOrganizationClicked(org);
                            }
                        }
                    });
                }
            }
        }
    }

    private void bindMonetaryField(@NonNull Session session, @NonNull CustomField customField) {
        if (this.mValue != null) {
            this.mValue.setText(new MonetaryFormatter(session).formatMonetaryCustomField(customField));
        }
    }

    private void bindTimeField(@NonNull Session session, @NonNull CustomField customField) {
        Date date = null;
        try {
            date = DateFormatHelper.timeFormat2().parse(customField.getTempValue());
        } catch (ParseException e) {
            Log.e(e);
        }
        if (this.mValue != null && date != null) {
            this.mValue.setText(LocaleHelper.getLocaleBasedTimeStringInCurrentTimeZone(session, date));
        }
    }

    private void bindTimeRangeField(@NonNull Session session, @NonNull CustomField customField) {
        String toTimeString = null;
        Date from = null;
        Date to = null;
        try {
            if (!TextUtils.isEmpty(customField.getTempValue())) {
                from = DateFormatHelper.timeFormat2().parse(customField.getTempValue());
            }
            if (!TextUtils.isEmpty(customField.getSecondaryTempValue())) {
                to = DateFormatHelper.timeFormat2().parse(customField.getSecondaryTempValue());
            }
        } catch (ParseException e) {
            Log.e(e);
        }
        String fromTimeString = from == null ? null : LocaleHelper.getLocaleBasedTimeStringInCurrentTimeZone(session, from);
        if (to != null) {
            toTimeString = LocaleHelper.getLocaleBasedTimeStringInCurrentTimeZone(session, to);
        }
        String valueText = TextUtils.isEmpty(toTimeString) ? fromTimeString : session.getApplicationContext().getString(R.string.placeholder.customFieldRange, new Object[]{fromTimeString, toTimeString});
        if (this.mValue != null) {
            this.mValue.setText(valueText);
        }
    }

    private void bindDateField(@NonNull Session session, @NonNull CustomField customField) {
        Date date = null;
        try {
            date = DateFormatHelper.dateFormat2().parse(customField.getTempValue());
        } catch (ParseException e) {
            Log.e(e);
        }
        if (this.mValue != null && date != null) {
            this.mValue.setText(LocaleHelper.getLocaleBasedDateStringInCurrentTimeZone(session, date));
        }
    }

    private void bindDateRangeField(@NonNull Session session, @NonNull CustomField customField) {
        String toDateString = null;
        Date from = null;
        Date to = null;
        try {
            if (!TextUtils.isEmpty(customField.getTempValue())) {
                from = DateFormatHelper.dateFormat2().parse(customField.getTempValue());
            }
            if (!TextUtils.isEmpty(customField.getSecondaryTempValue())) {
                to = DateFormatHelper.dateFormat2().parse(customField.getSecondaryTempValue());
            }
        } catch (ParseException e) {
            Log.e(e);
        }
        String fromDateString = from == null ? null : LocaleHelper.getLocaleBasedDateStringInCurrentTimeZone(session, from);
        if (to != null) {
            toDateString = LocaleHelper.getLocaleBasedDateStringInCurrentTimeZone(session, to);
        }
        String valueText = TextUtils.isEmpty(toDateString) ? fromDateString : session.getApplicationContext().getString(R.string.placeholder.customFieldRange, new Object[]{fromDateString, toDateString});
        if (this.mValue != null) {
            this.mValue.setText(valueText);
        }
    }

    private void bindUserField(@NonNull Session session, @NonNull CustomField customField) {
        List<User> users = UsersUtil.getActiveCompanyUsers(session);
        int userPipedriveId = 0;
        if (TextUtils.isDigitsOnly(customField.getTempValue())) {
            try {
                userPipedriveId = Integer.parseInt(customField.getTempValue());
            } catch (NumberFormatException e) {
                Log.e(e);
            }
        }
        User user = null;
        for (User companyUser : users) {
            if (companyUser != null && companyUser.getPipedriveIdOrNull() != null && companyUser.getPipedriveIdOrNull().longValue() == ((long) userPipedriveId)) {
                user = companyUser;
                break;
            }
        }
        if (user != null && !TextUtils.isEmpty(user.getName()) && this.mValue != null) {
            this.mValue.setText(user.getName());
        }
    }

    private void bindPersonField(@NonNull Session session, @NonNull CustomField customField, @Nullable final OnItemClickListener onItemClickListener) {
        final Person person = new PersonsDataSource(session.getDatabase()).findPersonByPipedriveId((long) CustomFieldsUtil.getIdFromCustomField(customField));
        if (person != null) {
            if (this.mValue != null) {
                this.mValue.setText(person.getName());
            }
            if (this.mIcon != null) {
                this.mIcon.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        if (onItemClickListener != null) {
                            onItemClickListener.onPersonClicked(person);
                        }
                    }
                });
            }
        }
    }

    private void bindMultipleOptionField(@NonNull CustomField customField) {
        String optionsText = "";
        StringTokenizer tokenizer = new StringTokenizer(customField.getTempValue(), Table.COMMA_SEP);
        while (tokenizer.hasMoreTokens()) {
            String optionId = tokenizer.nextToken();
            for (int i = 0; i < customField.getOptions().length(); i++) {
                try {
                    if (customField.getOptions().getJSONObject(i).optString(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, null).equalsIgnoreCase(optionId)) {
                        if (!optionsText.isEmpty()) {
                            optionsText = optionsText + ", ";
                        }
                        optionsText = optionsText + customField.getOptions().getJSONObject(i).optString(PlusShare.KEY_CALL_TO_ACTION_LABEL);
                    }
                } catch (JSONException e) {
                    Log.e(e);
                }
            }
        }
        if (this.mValue != null) {
            this.mValue.setText(optionsText);
        }
    }

    private void bindSingleOptionField(@NonNull CustomField customField) {
        int i = 0;
        while (i < customField.getOptions().length()) {
            try {
                if (!customField.getOptions().getJSONObject(i).optString(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, null).equalsIgnoreCase(customField.getTempValue())) {
                    i++;
                } else if (this.mValue != null) {
                    this.mValue.setText(customField.getOptions().getJSONObject(i).optString(PlusShare.KEY_CALL_TO_ACTION_LABEL));
                    return;
                } else {
                    return;
                }
            } catch (JSONException e) {
                Log.e(e);
            }
        }
    }

    void bindEmpty(@NonNull final CustomField customField, @Nullable final OnItemClickListener onClickListener) {
        this.mTitle.setText(this.itemView.getContext().getString(R.string.placeholder.customFieldEmpty, new Object[]{customField.getName()}));
        this.itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (onClickListener != null) {
                    onClickListener.onCustomFieldClicked(customField);
                }
            }
        });
    }
}
