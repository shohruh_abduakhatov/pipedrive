package rx;

import rx.functions.Func0;

class Completable$33 implements Single$OnSubscribe<T> {
    final /* synthetic */ Completable this$0;
    final /* synthetic */ Func0 val$completionValueFunc0;

    Completable$33(Completable completable, Func0 func0) {
        this.this$0 = completable;
        this.val$completionValueFunc0 = func0;
    }

    public void call(final SingleSubscriber<? super T> s) {
        this.this$0.unsafeSubscribe(new CompletableSubscriber() {
            public void onCompleted() {
                try {
                    T v = Completable$33.this.val$completionValueFunc0.call();
                    if (v == null) {
                        s.onError(new NullPointerException("The value supplied is null"));
                    } else {
                        s.onSuccess(v);
                    }
                } catch (Throwable e) {
                    s.onError(e);
                }
            }

            public void onError(Throwable e) {
                s.onError(e);
            }

            public void onSubscribe(Subscription d) {
                s.add(d);
            }
        });
    }
}
