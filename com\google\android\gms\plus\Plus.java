package com.google.android.gms.plus;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.Optional;
import com.google.android.gms.common.api.Api.zzf;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.util.zzu;
import com.google.android.gms.internal.zzwu;
import com.google.android.gms.internal.zzwv;
import com.google.android.gms.internal.zzww;
import com.google.android.gms.internal.zzwx;
import com.google.android.gms.plus.internal.PlusCommonExtras;
import com.google.android.gms.plus.internal.PlusSession;
import com.google.android.gms.plus.internal.zze;
import java.util.HashSet;
import java.util.Set;

@Deprecated
public final class Plus {
    @Deprecated
    public static final Api<PlusOptions> API = new Api("Plus.API", hh, hg);
    @Deprecated
    public static final Account AccountApi = new zzwu();
    @Deprecated
    public static final People PeopleApi = new zzwx();
    public static final Scope SCOPE_PLUS_LOGIN = new Scope(Scopes.PLUS_LOGIN);
    public static final Scope SCOPE_PLUS_PROFILE = new Scope(Scopes.PLUS_ME);
    @Deprecated
    public static final zzb aAJ = new zzww();
    public static final zza aAK = new zzwv();
    public static final zzf<zze> hg = new zzf();
    static final com.google.android.gms.common.api.Api.zza<zze, PlusOptions> hh = new com.google.android.gms.common.api.Api.zza<zze, PlusOptions>() {
        public int getPriority() {
            return 2;
        }

        public zze zza(Context context, Looper looper, com.google.android.gms.common.internal.zzf com_google_android_gms_common_internal_zzf, PlusOptions plusOptions, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            if (plusOptions == null) {
                plusOptions = new PlusOptions();
            }
            return new zze(context, looper, com_google_android_gms_common_internal_zzf, new PlusSession(com_google_android_gms_common_internal_zzf.zzave().name, zzu.zzd(com_google_android_gms_common_internal_zzf.zzavq()), (String[]) plusOptions.aAM.toArray(new String[0]), new String[0], context.getPackageName(), context.getPackageName(), null, new PlusCommonExtras()), connectionCallbacks, onConnectionFailedListener);
        }
    };

    public static abstract class zza<R extends Result> extends com.google.android.gms.internal.zzqo.zza<R, zze> {
        public zza(GoogleApiClient googleApiClient) {
            super(Plus.hg, googleApiClient);
        }
    }

    public static final class PlusOptions implements Optional {
        final String aAL;
        final Set<String> aAM;

        public static final class Builder {
            String aAL;
            final Set<String> aAM = new HashSet();

            public Builder addActivityTypes(String... strArr) {
                zzaa.zzb((Object) strArr, (Object) "activityTypes may not be null.");
                for (Object add : strArr) {
                    this.aAM.add(add);
                }
                return this;
            }

            public PlusOptions build() {
                return new PlusOptions();
            }

            public Builder setServerClientId(String str) {
                this.aAL = str;
                return this;
            }
        }

        private PlusOptions() {
            this.aAL = null;
            this.aAM = new HashSet();
        }

        private PlusOptions(Builder builder) {
            this.aAL = builder.aAL;
            this.aAM = builder.aAM;
        }

        public static Builder builder() {
            return new Builder();
        }
    }

    private Plus() {
    }

    public static zze zzf(GoogleApiClient googleApiClient, boolean z) {
        zzaa.zzb(googleApiClient != null, (Object) "GoogleApiClient parameter is required.");
        zzaa.zza(googleApiClient.isConnected(), (Object) "GoogleApiClient must be connected.");
        zzaa.zza(googleApiClient.zza(API), (Object) "GoogleApiClient is not configured to use the Plus.API Api. Pass this into GoogleApiClient.Builder#addApi() to use this feature.");
        boolean hasConnectedApi = googleApiClient.hasConnectedApi(API);
        if (!z || hasConnectedApi) {
            return hasConnectedApi ? (zze) googleApiClient.zza(hg) : null;
        } else {
            throw new IllegalStateException("GoogleApiClient has an optional Plus.API and is not connected to Plus. Use GoogleApiClient.hasConnectedApi(Plus.API) to guard this call.");
        }
    }
}
