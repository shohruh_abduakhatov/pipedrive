package com.zendesk.sdk.support.help;

import android.support.annotation.Nullable;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.helpcenter.help.CategoryItem;
import com.zendesk.sdk.model.helpcenter.help.HelpItem;
import com.zendesk.sdk.model.helpcenter.help.SectionItem;
import com.zendesk.sdk.model.helpcenter.help.SeeAllArticlesItem;
import com.zendesk.sdk.network.NetworkInfoProvider;
import com.zendesk.sdk.network.RetryAction;
import com.zendesk.sdk.support.SupportMvp;
import com.zendesk.sdk.support.SupportMvp.ErrorType;
import com.zendesk.sdk.support.SupportUiConfig;
import com.zendesk.sdk.support.help.HelpMvp.Model;
import com.zendesk.sdk.support.help.HelpMvp.Presenter;
import com.zendesk.sdk.support.help.HelpMvp.View;
import com.zendesk.service.ErrorResponse;
import com.zendesk.service.ZendeskCallback;
import com.zendesk.util.CollectionUtils;
import java.util.ArrayList;
import java.util.List;

class HelpAdapterPresenter implements Presenter {
    private static final String LOG_TAG = "HelpAdapterPresenter";
    private static final Integer RETRY_ACTION_ID = Integer.valueOf(5);
    private static final String TAG = "HelpAdapterPresenter";
    private ZendeskCallback<List<HelpItem>> callback = new ZendeskCallback<List<HelpItem>>() {
        public void onSuccess(List<HelpItem> result) {
            HelpAdapterPresenter.this.hasError = false;
            HelpAdapterPresenter.this.helpItems = CollectionUtils.copyOf(result);
            if (HelpAdapterPresenter.this.supportUiConfig.isCollapseCategories()) {
                HelpAdapterPresenter.this.filteredItems = HelpAdapterPresenter.this.getCollapsedCategories(HelpAdapterPresenter.this.helpItems);
            } else {
                HelpAdapterPresenter.this.filteredItems = CollectionUtils.copyOf(HelpAdapterPresenter.this.helpItems);
            }
            HelpAdapterPresenter.this.noResults = CollectionUtils.isEmpty(HelpAdapterPresenter.this.filteredItems);
            HelpAdapterPresenter.this.view.showItems(HelpAdapterPresenter.this.filteredItems);
            HelpAdapterPresenter.this.contentPresenter.onLoad();
        }

        public void onError(ErrorResponse error) {
            ErrorType errorType;
            if (CollectionUtils.isNotEmpty(HelpAdapterPresenter.this.supportUiConfig.getCategoryIds())) {
                errorType = ErrorType.CATEGORY_LOAD;
            } else if (CollectionUtils.isNotEmpty(HelpAdapterPresenter.this.supportUiConfig.getSectionIds())) {
                errorType = ErrorType.SECTION_LOAD;
            } else {
                errorType = ErrorType.ARTICLES_LOAD;
            }
            HelpAdapterPresenter.this.contentPresenter.onErrorWithRetry(errorType, new RetryAction() {
                public void onRetry() {
                    HelpAdapterPresenter.this.hasError = false;
                    HelpAdapterPresenter.this.view.showItems(HelpAdapterPresenter.this.filteredItems);
                    HelpAdapterPresenter.this.requestHelpContent();
                }
            });
            HelpAdapterPresenter.this.hasError = true;
            HelpAdapterPresenter.this.view.showItems(HelpAdapterPresenter.this.filteredItems);
        }
    };
    private SupportMvp.Presenter contentPresenter;
    private List<HelpItem> filteredItems = new ArrayList();
    private boolean hasError;
    private List<HelpItem> helpItems = new ArrayList();
    private Model model;
    private NetworkInfoProvider networkInfoProvider;
    private boolean noResults;
    private RetryAction retryAction;
    private SupportUiConfig supportUiConfig;
    private View view;

    public HelpAdapterPresenter(View view, Model model, NetworkInfoProvider networkInfoProvider, SupportUiConfig supportUiConfig) {
        this.view = view;
        this.model = model;
        this.networkInfoProvider = networkInfoProvider;
        this.supportUiConfig = supportUiConfig;
    }

    public void setContentPresenter(SupportMvp.Presenter presenter) {
        this.contentPresenter = presenter;
    }

    public void onAttached() {
        this.networkInfoProvider.register();
        if (CollectionUtils.isEmpty(this.helpItems)) {
            requestHelpContent();
        }
    }

    public void onDetached() {
        this.networkInfoProvider.removeRetryAction(RETRY_ACTION_ID);
        this.networkInfoProvider.unregister();
    }

    public boolean onCategoryClick(CategoryItem categoryItem, int position) {
        boolean z = false;
        if (categoryItem == null) {
            return false;
        }
        if (!categoryItem.isExpanded()) {
            z = true;
        }
        boolean expanded = categoryItem.setExpanded(z);
        if (expanded) {
            expandItem(categoryItem, this.filteredItems.indexOf(categoryItem));
        } else {
            collapseItem(this.filteredItems.indexOf(categoryItem));
        }
        return expanded;
    }

    public void onSeeAllClick(SeeAllArticlesItem item) {
        loadMoreArticles(item);
    }

    public HelpItem getItem(int position) {
        return (HelpItem) this.filteredItems.get(position);
    }

    public int getItemCount() {
        if (this.hasError) {
            return 0;
        }
        return Math.max(this.filteredItems.size() + getPaddingItemCount(), 1);
    }

    private int getPaddingItemCount() {
        return this.supportUiConfig.isAddListPaddingBottom() ? 1 : 0;
    }

    public int getItemViewType(int position) {
        if (this.noResults) {
            return 7;
        }
        if (this.filteredItems.size() > 0) {
            return position == this.filteredItems.size() ? 8 : ((HelpItem) this.filteredItems.get(position)).getViewType();
        } else {
            return 5;
        }
    }

    @Nullable
    public HelpItem getItemForBinding(int position) {
        if (this.filteredItems.size() <= 0 || position >= this.filteredItems.size()) {
            return null;
        }
        return (HelpItem) this.filteredItems.get(position);
    }

    private void requestHelpContent() {
        if (this.networkInfoProvider.isNetworkAvailable()) {
            this.model.getArticles(this.supportUiConfig.getCategoryIds(), this.supportUiConfig.getSectionIds(), this.supportUiConfig.getLabelNames(), this.callback);
            return;
        }
        this.retryAction = new RetryAction() {
            public void onRetry() {
                HelpAdapterPresenter.this.requestHelpContent();
            }
        };
        this.networkInfoProvider.addRetryAction(RETRY_ACTION_ID, this.retryAction);
    }

    private List<HelpItem> getCollapsedCategories(List<HelpItem> helpItems) {
        List<HelpItem> collapsedCategories = new ArrayList();
        if (!(helpItems == null || helpItems.size() == 0)) {
            int count = helpItems.size();
            for (int i = 0; i < count; i++) {
                if (1 == ((HelpItem) helpItems.get(i)).getViewType()) {
                    collapsedCategories.add(helpItems.get(i));
                    ((CategoryItem) helpItems.get(i)).setExpanded(false);
                }
            }
        }
        return collapsedCategories;
    }

    private void expandItem(CategoryItem categoryItem, int position) {
        int positionToAddAt = position + 1;
        for (HelpItem categoryChildItem : categoryItem.getChildren()) {
            addItem(positionToAddAt, categoryChildItem);
            positionToAddAt++;
            try {
                for (HelpItem sectionChildItem : ((SectionItem) categoryChildItem).getChildren()) {
                    addItem(positionToAddAt, sectionChildItem);
                    positionToAddAt++;
                }
            } catch (ClassCastException ex) {
                Logger.e("HelpAdapterPresenter", "Error expanding item", ex, new Object[0]);
            }
        }
    }

    private void collapseItem(int position) {
        if (position < getItemCount() - 1) {
            int positionToRemoveFrom = position + 1;
            while (positionToRemoveFrom < this.filteredItems.size() && 1 != ((HelpItem) this.filteredItems.get(positionToRemoveFrom)).getViewType()) {
                removeItem(positionToRemoveFrom);
            }
        }
    }

    private void addItem(int position, HelpItem item) {
        this.filteredItems.add(position, item);
        this.view.addItem(position, item);
    }

    private void removeItem(int position) {
        this.filteredItems.remove(position);
        this.view.removeItem(position);
    }

    private void loadMoreArticles(final SeeAllArticlesItem seeAllItem) {
        final SectionItem section = seeAllItem.getSection();
        final RetryAction loadMoreRetryAction = new RetryAction() {
            public void onRetry() {
                HelpAdapterPresenter.this.loadMoreArticles(seeAllItem);
            }
        };
        if (this.networkInfoProvider.isNetworkAvailable()) {
            this.model.getArticlesForSection(section, this.supportUiConfig.getLabelNames(), new ZendeskCallback<List<HelpItem>>() {
                public void onSuccess(List<HelpItem> itemsForSection) {
                    int positionInFullList = HelpAdapterPresenter.this.helpItems.indexOf(seeAllItem);
                    int positionInFilteredList = HelpAdapterPresenter.this.filteredItems.indexOf(seeAllItem);
                    for (HelpItem item : itemsForSection) {
                        if (!HelpAdapterPresenter.this.helpItems.contains(item)) {
                            int positionInFullList2 = positionInFullList + 1;
                            HelpAdapterPresenter.this.helpItems.add(positionInFullList, item);
                            section.addChild(item);
                            if (positionInFilteredList != -1) {
                                HelpAdapterPresenter.this.filteredItems.add(positionInFilteredList, item);
                                HelpAdapterPresenter.this.view.addItem(positionInFilteredList, item);
                                positionInFilteredList++;
                                positionInFullList = positionInFullList2;
                            } else {
                                positionInFullList = positionInFullList2;
                            }
                        }
                    }
                    HelpAdapterPresenter.this.helpItems.remove(seeAllItem);
                    int removedItemPosition = HelpAdapterPresenter.this.filteredItems.indexOf(seeAllItem);
                    HelpAdapterPresenter.this.filteredItems.remove(seeAllItem);
                    section.removeChild(seeAllItem);
                    HelpAdapterPresenter.this.view.removeItem(removedItemPosition);
                }

                public void onError(ErrorResponse errorResponse) {
                    HelpAdapterPresenter.this.helpItems.remove(seeAllItem);
                    Logger.e("HelpAdapterPresenter", "Failed to load more articles", errorResponse);
                    HelpAdapterPresenter.this.contentPresenter.onErrorWithRetry(ErrorType.ARTICLES_LOAD, loadMoreRetryAction);
                }
            });
            return;
        }
        this.retryAction = loadMoreRetryAction;
        this.networkInfoProvider.addRetryAction(RETRY_ACTION_ID, this.retryAction);
    }
}
