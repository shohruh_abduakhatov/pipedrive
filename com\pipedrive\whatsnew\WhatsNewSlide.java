package com.pipedrive.whatsnew;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager.PageTransformer;

class WhatsNewSlide {
    @Nullable
    String googleAnalyticsScreenHitValue;
    final int layoutResId;
    @Nullable
    PageTransformer pageTransformer;

    WhatsNewSlide(@LayoutRes int layoutResId) {
        this.layoutResId = layoutResId;
    }
}
