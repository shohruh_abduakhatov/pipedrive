package com.pipedrive.util;

import android.support.annotation.NonNull;
import rx.Observable;

public enum RxUtil {
    ;

    @NonNull
    public static <T> T blockingFirst(@NonNull Observable<T> observable) {
        return observable.toBlocking().first();
    }
}
