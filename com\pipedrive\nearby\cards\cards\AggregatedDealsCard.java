package com.pipedrive.nearby.cards.cards;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.ViewGroup;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.CurrenciesDataSource;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.model.Currency;
import com.pipedrive.nearby.cards.adapter.CardAdapterForAggregatedItemsList;
import com.pipedrive.nearby.cards.adapter.DealCardAdapterForAggregatedItemsList;
import com.pipedrive.nearby.model.DealNearbyItem;
import com.pipedrive.nearby.model.NearbyItem;
import com.pipedrive.util.formatter.MonetaryFormatter;
import java.util.List;
import rx.Observable;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class AggregatedDealsCard extends AggregatedCard<DealNearbyItem> {
    public AggregatedDealsCard(@NonNull ViewGroup parent, @LayoutRes @NonNull Integer layoutResId) {
        super(parent, layoutResId);
    }

    @NonNull
    Observable<String> getAggregatedInformation(@NonNull final Session session, @NonNull DealNearbyItem nearbyItem) {
        final Currency defaultCurrency = new CurrenciesDataSource(session.getDatabase()).getCurrencyByCode(session.getUserSettingsDefaultCurrencyCode());
        if (defaultCurrency == null) {
            return Observable.empty();
        }
        return nearbyItem.getItemSqlIds().toList().subscribeOn(Schedulers.io()).map(new Func1<List<Long>, String>() {
            public String call(List<Long> sqlIdList) {
                return new MonetaryFormatter(session).format(new DealsDataSource(session.getDatabase()).getDealValueSumForAggregatedCardInDefaultCurrency(defaultCurrency, sqlIdList), defaultCurrency.getCode());
            }
        });
    }

    @NonNull
    Observable<List<DealNearbyItem>> getSingleItemsList(@NonNull final DealNearbyItem nearbyItem) {
        return nearbyItem.getItemSqlIds().map(new Func1<Long, DealNearbyItem>() {
            public DealNearbyItem call(Long aLong) {
                return new DealNearbyItem(nearbyItem.getLatitude(), nearbyItem.getLongitude(), nearbyItem.getAddress(), aLong);
            }
        }).toList();
    }

    @NonNull
    Integer getQuantityStringResId() {
        return Integer.valueOf(R.plurals.deals);
    }

    @NonNull
    CardAdapterForAggregatedItemsList getAdapter(@NonNull List<? extends NearbyItem> nearbyItems) {
        return new DealCardAdapterForAggregatedItemsList(this.session, nearbyItems);
    }

    int getInitialPrefetch() {
        return this.itemView.getContext().getResources().getInteger(R.integer.initial_prefetch_items_deals);
    }
}
