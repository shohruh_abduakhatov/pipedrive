package com.pipedrive.tasks;

import android.support.annotation.NonNull;
import com.pipedrive.LoginActivity;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;

public final class SessionLogoutTask extends NonSessionDependentTask<Void, Void, Void> {
    private SessionLogoutTask(Session session) {
        super(session);
    }

    protected Void doInBackground(Void... params) {
        PipedriveApp.getSessionManager().clearDatabaseAndDisposeSession(getSession());
        return null;
    }

    public static void startIfNotRunning(@NonNull Session session) {
        if (!AsyncTask.isExecuting(session, SessionLogoutTask.class)) {
            session.setInvalid();
            if (session == PipedriveApp.getActiveSession()) {
                if (session.getRuntimeCache().runningAsyncTaskRegister.size() > 0) {
                    LogJourno.reportEvent(EVENT.NOT_SPECIFIED_LOGOUT_RECEIVED_WITH_ASYNCTASKS_SCHEDULED, "session.getRuntimeCache().runningAsyncTaskRegister.size = " + session.getRuntimeCache().runningAsyncTaskRegister.size());
                }
                LoginActivity.logOut(session.getApplicationContext(), true);
            }
            new SessionLogoutTask(session).execute(new Void[0]);
        }
    }
}
