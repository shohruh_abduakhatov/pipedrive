package com.pipedrive.sync;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.tasks.AsyncTask;
import rx.subjects.Subject;

final class SyncManager$DownloadEverythingInProgressMarker extends AsyncTask<Void, Void, Void> {
    final Subject<SyncManager$StateOfDownloadAll, SyncManager$StateOfDownloadAll> stateOfDownloadAllSubject = SyncManager.access$100(SyncManager.getInstance(), getSession());

    SyncManager$DownloadEverythingInProgressMarker(@NonNull Session session) {
        super(session);
    }

    protected void onPreExecute() {
        this.stateOfDownloadAllSubject.onNext(SyncManager$StateOfDownloadAll.REGISTERED);
    }

    protected Void doInBackground(Void... params) {
        this.stateOfDownloadAllSubject.onNext(SyncManager$StateOfDownloadAll.DOWNLOADING);
        return null;
    }

    protected void onPostExecute(Void v) {
        this.stateOfDownloadAllSubject.onNext(SyncManager$StateOfDownloadAll.IDLE);
    }
}
