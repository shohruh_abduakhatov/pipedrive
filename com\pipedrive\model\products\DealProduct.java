package com.pipedrive.model.products;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.maps.android.heatmaps.WeightedLatLng;
import com.pipedrive.model.AssociatedDatasourceEntity;
import com.pipedrive.model.ChildDataSourceEntity;
import java.math.BigDecimal;

public class DealProduct extends ChildDataSourceEntity implements AssociatedDatasourceEntity {
    private static final Double DEFAULT_DURATION = Double.valueOf(WeightedLatLng.DEFAULT_INTENSITY);
    @NonNull
    private Double discountPercentage;
    @NonNull
    private Double duration = DEFAULT_DURATION;
    @NonNull
    private Boolean isActive;
    @Nullable
    private final Integer order;
    @NonNull
    private Price price;
    @NonNull
    private final Product product;
    @Nullable
    private ProductVariation productVariation;
    @NonNull
    private Double quantity;

    @Nullable
    public static DealProduct create(@NonNull Product product, @Nullable ProductVariation productVariation, @NonNull Price price, @NonNull Double quantity, @Nullable Double duration, @NonNull Double discountPercentage, @NonNull Boolean isActive) {
        return new DealProduct(null, null, null, product, productVariation, price, quantity, duration, discountPercentage, isActive, null);
    }

    @NonNull
    public static DealProduct create(@Nullable Long pipedriveId, @Nullable Long dealSqlId, @NonNull Product product, @Nullable ProductVariation productVariation, @NonNull Price price, @NonNull Double quantity, @Nullable Double duration, @NonNull Double discountPercentage, @NonNull Boolean isActive, @Nullable Integer order) {
        return new DealProduct(null, pipedriveId, dealSqlId, product, productVariation, price, quantity, duration, discountPercentage, isActive, order);
    }

    @NonNull
    public static DealProduct create(@Nullable Long sqlId, @Nullable Long pipedriveId, @Nullable Long dealSqlId, @NonNull Product product, @Nullable ProductVariation productVariation, @NonNull Price price, @NonNull Double quantity, @Nullable Double duration, @NonNull Double discountPercentage, @NonNull Boolean isActive, @Nullable Integer order) {
        return new DealProduct(sqlId, pipedriveId, dealSqlId, product, productVariation, price, quantity, duration, discountPercentage, isActive, order);
    }

    private DealProduct(@Nullable Long sqlId, @Nullable Long pipedriveId, @Nullable Long parentSqlId, @NonNull Product product, @Nullable ProductVariation productVariation, @NonNull Price price, @NonNull Double quantity, @Nullable Double duration, @NonNull Double discountPercentage, @NonNull Boolean isActive, @Nullable Integer order) {
        super(sqlId, pipedriveId, parentSqlId);
        this.product = product;
        this.productVariation = productVariation;
        this.price = price;
        this.quantity = quantity;
        setDuration(duration);
        this.discountPercentage = discountPercentage;
        this.isActive = isActive;
        this.order = order;
    }

    public void update(@Nullable ProductVariation productVariation, @NonNull Price price, @NonNull Double quantity, @Nullable Double duration, @NonNull Double discountPercentage) {
        this.productVariation = productVariation;
        this.price = price;
        this.quantity = quantity;
        setDuration(duration);
        this.discountPercentage = discountPercentage;
    }

    public void delete() {
        this.isActive = Boolean.valueOf(false);
    }

    private void setDuration(@Nullable Double duration) {
        if (duration == null) {
            duration = DEFAULT_DURATION;
        }
        this.duration = duration;
    }

    @NonNull
    public Product getProduct() {
        return this.product;
    }

    @Nullable
    public ProductVariation getProductVariation() {
        return this.productVariation;
    }

    @NonNull
    public Price getPrice() {
        return this.price;
    }

    @NonNull
    public Double getQuantity() {
        return this.quantity;
    }

    @NonNull
    public Double getDuration() {
        return this.duration;
    }

    @NonNull
    public Double getDiscountPercentage() {
        return this.discountPercentage;
    }

    @NonNull
    public Boolean isActive() {
        return this.isActive;
    }

    @Nullable
    public Integer getOrder() {
        return this.order;
    }

    public boolean isAllAssociationsExisting() {
        if (this.productVariation == null || this.productVariation.isExisting()) {
            return this.product.isExisting();
        }
        return false;
    }

    @NonNull
    public BigDecimal getSum() {
        return getCalculatedDealProductSum(getPrice().getValue(), this.quantity, this.duration, this.discountPercentage);
    }

    @NonNull
    public static BigDecimal getCalculatedDealProductSum(@NonNull BigDecimal priceValue, @NonNull Double quantityValue, @Nullable Double durationValue, @NonNull Double discountValue) {
        BigDecimal multiply = BigDecimal.ONE.multiply(priceValue).multiply(BigDecimal.valueOf(quantityValue.doubleValue()));
        if (durationValue == null) {
            durationValue = DEFAULT_DURATION;
        }
        return multiply.multiply(BigDecimal.valueOf(durationValue.doubleValue())).multiply(BigDecimal.valueOf(100).subtract(BigDecimal.valueOf(discountValue.doubleValue())).multiply(BigDecimal.valueOf(0.01d)));
    }

    public boolean isMetadataOkForCreate() {
        boolean dataDoesNotExistAndIsNotStored;
        if (isExisting() || isStored()) {
            dataDoesNotExistAndIsNotStored = false;
        } else {
            dataDoesNotExistAndIsNotStored = true;
        }
        return dataDoesNotExistAndIsNotStored && isMetadataOkForAnyOperation();
    }

    public boolean isMetadataOkForUpdate() {
        return isStored() && isMetadataOkForAnyOperation();
    }

    private boolean isMetadataOkForAnyOperation() {
        boolean hasProperlyDefinedAndStoredVariation;
        if (getProductVariation() == null || getProductVariation().isStored()) {
            hasProperlyDefinedAndStoredVariation = true;
        } else {
            hasProperlyDefinedAndStoredVariation = false;
        }
        return hasProperlyDefinedAndStoredVariation && getProduct().isStored() && hasParent().booleanValue();
    }

    @Nullable
    public Long getDealSqlId() {
        return getParentSqlId();
    }
}
