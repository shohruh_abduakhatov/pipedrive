package com.pipedrive.sync;

import com.pipedrive.application.Session;
import com.pipedrive.tasks.authorization.OnEnoughDataToShowUI;
import rx.functions.Action1;

class SyncManager$3 implements Action1<SyncManager$StateOfDownloadAll> {
    final /* synthetic */ SyncManager this$0;
    final /* synthetic */ OnEnoughDataToShowUI val$onEnoughDataToShowUI;
    final /* synthetic */ Session val$session;

    SyncManager$3(SyncManager this$0, OnEnoughDataToShowUI onEnoughDataToShowUI, Session session) {
        this.this$0 = this$0;
        this.val$onEnoughDataToShowUI = onEnoughDataToShowUI;
        this.val$session = session;
    }

    public void call(SyncManager$StateOfDownloadAll stateOfDownloadAll) {
        boolean downloadAllIsInProgressForThisSession;
        if (stateOfDownloadAll != SyncManager$StateOfDownloadAll.IDLE) {
            downloadAllIsInProgressForThisSession = true;
        } else {
            downloadAllIsInProgressForThisSession = false;
        }
        if (downloadAllIsInProgressForThisSession) {
            this.val$onEnoughDataToShowUI.readyToShowUI(this.val$session);
        } else {
            SyncManager.access$200(this.this$0, this.val$session, this.val$onEnoughDataToShowUI, true, false);
        }
    }
}
