package com.pipedrive.util.recents;

import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.newrelic.agent.android.instrumentation.GsonInstrumentation;
import com.pipedrive.application.Session;
import com.pipedrive.changes.ChangesHelper;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.gson.GsonHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.ActivityType;
import com.pipedrive.model.Deal;
import com.pipedrive.model.DealStage;
import com.pipedrive.model.Filter;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.Pipeline;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.model.pagination.Pagination;
import com.pipedrive.pipeline.InvalidatePipelinesTask;
import com.pipedrive.util.FiltersUtil;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.Response;
import com.pipedrive.util.networking.entities.ActivityEntity;
import com.pipedrive.util.networking.entities.EmailMessageEntity;
import com.pipedrive.util.networking.entities.EmailThreadEntity;
import com.pipedrive.util.networking.entities.FlowFileEntity;
import com.pipedrive.util.networking.entities.FlowItem;
import com.pipedrive.util.networking.entities.NoteEntity;
import com.pipedrive.util.networking.entities.dealstage.DealStageParser;
import com.pipedrive.util.networking.entities.product.ProductEntity;
import com.pipedrive.util.organizations.OrganizationsNetworkingUtil;
import com.pipedrive.util.persons.PersonNetworkingUtil;
import com.pipedrive.util.time.TimeManager;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;

public enum RecentsQueryUtil {
    ;
    
    private static final String TAG = null;

    static {
        TAG = RecentsQueryUtil.class.getSimpleName();
    }

    @WorkerThread
    public static void processChangesAndDownloadRecentsBlocking(@NonNull Session session) {
        UnsupportedEncodingException e;
        String tag = TAG + ".processChangesAndDownloadRecentsBlocking()";
        ChangesHelper.syncOfflineChanges(session);
        String apiToken = session.getApiToken();
        String storedRecentsTimestamp = session.getLastSyncTime(Session.PREFS_NOT_SET_STRING);
        Log.d(tag, String.format("Requesting recents with apiToken: %s; timestamp: %s.", new Object[]{apiToken, storedRecentsTimestamp}));
        boolean apiTokenMissing = Session.PREFS_NOT_SET_STRING.equalsIgnoreCase(apiToken);
        boolean sinceTimestampMissing = Session.PREFS_NOT_SET_STRING.equalsIgnoreCase(storedRecentsTimestamp);
        if (apiTokenMissing || sinceTimestampMissing) {
            LogJourno.reportEvent(EVENT.Recents_sessionIsMissingApiTokenOrSinceTimestampForRecentsRequest, String.format("ApiToken:[%s] SinceTimestamp:[%s]", new Object[]{Boolean.valueOf(apiTokenMissing), Boolean.valueOf(sinceTimestampMissing)}));
            Log.e(new Throwable(String.format("%s Cannot perform sync!", new Object[]{keys})));
            return;
        }
        InputStream in = null;
        JsonReader jsonReader = null;
        RecentsResponsePage responsePage = new RecentsResponsePage(storedRecentsTimestamp);
        responsePage.moreItemsInCollection = false;
        do {
            JsonReader jsonReader2 = jsonReader;
            try {
                in = downloadChangesListStream(session, storedRecentsTimestamp, responsePage.mNextPage);
                if (in == null) {
                    jsonReader = jsonReader2;
                    break;
                }
                jsonReader = new JsonReader(new InputStreamReader(in, HttpRequest.CHARSET_UTF8));
                try {
                    responsePage = parseResponsePage(session, jsonReader, responsePage);
                } catch (UnsupportedEncodingException e2) {
                    e = e2;
                }
            } catch (UnsupportedEncodingException e3) {
                e = e3;
                jsonReader = jsonReader2;
            } catch (Throwable th) {
                Throwable th2 = th;
                jsonReader = jsonReader2;
            }
        } while (responsePage.moreItemsInCollection);
        RecentsQueryProcessor.processResponse(session, responsePage);
        if (responsePage.lastTimestampOnPage != null) {
            session.setLastSyncTime(responsePage.lastTimestampOnPage);
        }
        session.setLastProcessChangesAndRecentsTimeInMillis(TimeManager.getInstance().currentTimeMillis().longValue());
        if (in != null) {
            try {
                in.close();
            } catch (IOException e4) {
            }
        }
        if (jsonReader != null) {
            try {
                jsonReader.close();
                return;
            } catch (IOException e5) {
                return;
            }
        }
        return;
        try {
            LogJourno.reportEvent(EVENT.Recents_unsupportedEncodingOfRecentsResponse, e);
            Log.e(new Throwable("Cannot update recents!", e));
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e6) {
                }
            }
            if (jsonReader != null) {
                try {
                    jsonReader.close();
                } catch (IOException e7) {
                    return;
                }
            }
        } catch (Throwable th3) {
            th2 = th3;
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e8) {
                }
            }
            if (jsonReader != null) {
                try {
                    jsonReader.close();
                } catch (IOException e9) {
                }
            }
            throw th2;
        }
        if (jsonReader != null) {
            jsonReader.close();
        }
    }

    @Nullable
    private static InputStream downloadChangesListStream(@NonNull Session session, @NonNull String sinceTimestamp, @IntRange(from = -1) int start) {
        ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder(session, ApiUrlBuilder.ENDPOINT_RECENTS);
        apiUrlBuilder.param("since_timestamp", sinceTimestamp);
        if (start != -1) {
            apiUrlBuilder.param(Pagination.JSON_PARAM_PAGINATION_START, Integer.valueOf(start));
        }
        return ConnectionUtil.requestGet(apiUrlBuilder);
    }

    private static RecentsResponsePage parseResponsePage(Session session, JsonReader reader, RecentsResponsePage responsePage) {
        if (reader == null || session == null) {
            responsePage.moreItemsInCollection = false;
            return responsePage;
        }
        try {
            reader.beginObject();
            while (reader.hasNext()) {
                String fieldName = reader.nextName();
                if (fieldName.equals(Response.JSON_PARAM_SUCCESS)) {
                    if (!reader.nextBoolean()) {
                        responsePage.moreItemsInCollection = false;
                        return responsePage;
                    }
                } else if (fieldName.equals(Response.JSON_PARAM_DATA) && reader.peek() == JsonToken.BEGIN_ARRAY) {
                    reader.beginArray();
                    while (reader.hasNext()) {
                        Object item = readItem(session, reader);
                        if (item != null) {
                            if (item instanceof ActivityEntity) {
                                responsePage.activities.add((ActivityEntity) item);
                            } else if (item instanceof Deal) {
                                responsePage.deals.add((Deal) item);
                            } else if (item instanceof Person) {
                                responsePage.persons.add((Person) item);
                            } else if (item instanceof Organization) {
                                responsePage.organizations.add((Organization) item);
                            } else if (item instanceof ActivityType) {
                                ActivityType activityType = (ActivityType) item;
                                responsePage.activityTypesFromRecents.put(Integer.valueOf(activityType.getId()), activityType);
                            } else if (item instanceof Filter) {
                                Filter filter = (Filter) item;
                                responsePage.filtersFromRecents.put(Integer.valueOf(filter.getPipedriveId()), filter);
                            } else if (item instanceof DealStage) {
                                responsePage.dealStages.add((DealStage) item);
                            } else if (item instanceof Pipeline) {
                                Pipeline pipeline = (Pipeline) item;
                                responsePage.pipelinesFromRecents.put(Integer.valueOf(pipeline.getPipedriveId()), pipeline);
                            } else if (item instanceof NoteEntity) {
                                NoteEntity noteJSONEntity = (NoteEntity) item;
                                responsePage.notesFromRecents.put(Integer.valueOf(noteJSONEntity.pipedriveId), noteJSONEntity);
                            } else if (item instanceof FlowFileEntity) {
                                responsePage.filesFromRecents.add((FlowFileEntity) item);
                            } else if (item instanceof EmailMessageEntity) {
                                responsePage.emailMessagesFromRecents.add((EmailMessageEntity) item);
                            } else if (item instanceof EmailThreadEntity) {
                                responsePage.emailThreadsFromRecents.add((EmailThreadEntity) item);
                            } else if (item instanceof ProductEntity) {
                                responsePage.productEntitiesFromRecents.add((ProductEntity) item);
                            } else if (item instanceof DeleteItem) {
                                DeleteItem deleteItem = (DeleteItem) item;
                                if (TextUtils.equals("deal", deleteItem.mItemTypeMarker)) {
                                    responsePage.dealsToDelete.add(Integer.valueOf(deleteItem.mItemPipedriveId));
                                } else if (TextUtils.equals(FlowItem.ITEM_TYPE_OBJECT_ACTIVITY, deleteItem.mItemTypeMarker)) {
                                    responsePage.activitiesToDelete.add(Integer.valueOf(deleteItem.mItemPipedriveId));
                                } else if (TextUtils.equals("person", deleteItem.mItemTypeMarker)) {
                                    responsePage.contactsToDelete.add(Integer.valueOf(deleteItem.mItemPipedriveId));
                                } else if (TextUtils.equals("organization", deleteItem.mItemTypeMarker)) {
                                    responsePage.organizationsToDelete.add(Integer.valueOf(deleteItem.mItemPipedriveId));
                                } else if (!(TextUtils.equals("activityType", deleteItem.mItemTypeMarker) || TextUtils.equals("filter", deleteItem.mItemTypeMarker))) {
                                    if (TextUtils.equals("stage", deleteItem.mItemTypeMarker)) {
                                        responsePage.dealStagesToDelete.add(Integer.valueOf(deleteItem.mItemPipedriveId));
                                    } else if (!TextUtils.equals("pipeline", deleteItem.mItemTypeMarker) && TextUtils.equals(FlowItem.ITEM_TYPE_OBJECT_NOTE, deleteItem.mItemTypeMarker)) {
                                    }
                                }
                            }
                        }
                    }
                    reader.endArray();
                } else if (Response.JSON_PARAM_ADDITIONAL_DATA.equals(fieldName) && reader.peek() == JsonToken.BEGIN_OBJECT) {
                    reader.beginObject();
                    while (reader.hasNext()) {
                        fieldName = reader.nextName();
                        if (Pagination.JSON_PARAM_OF_PAGINATION.equals(fieldName)) {
                            reader.beginObject();
                            while (reader.hasNext()) {
                                fieldName = reader.nextName();
                                if ("next_start".equals(fieldName)) {
                                    responsePage.mNextPage = reader.nextInt();
                                } else if ("more_items_in_collection".equals(fieldName)) {
                                    responsePage.moreItemsInCollection = reader.nextBoolean();
                                } else {
                                    reader.skipValue();
                                }
                            }
                            reader.endObject();
                        } else if (!"last_timestamp_on_page".equals(fieldName) || reader.peek() == JsonToken.NULL) {
                            reader.skipValue();
                        } else {
                            responsePage.lastTimestampOnPage = reader.nextString();
                        }
                    }
                    reader.endObject();
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
        } catch (Exception e) {
            LogJourno.reportEvent(EVENT.Recents_pageOfRecentsJsonParsingFailed, e);
            Log.e(new Throwable("Error in reading the recents query JSON!", e));
        }
        return RecentsQueryProcessor.processResponsePage(session, responsePage);
    }

    private static Object readItem(Session session, JsonReader reader) throws IOException {
        reader.beginObject();
        String itemType = null;
        int itemPipedriveId = 0;
        Object obj = null;
        while (reader.hasNext()) {
            String debugLastItemTypeRead = "_NOT KNOWN_";
            if (reader.peek() == JsonToken.NAME) {
                String fieldName = reader.nextName();
                if ("item".equals(fieldName) && reader.peek() != JsonToken.NULL) {
                    itemType = reader.nextString();
                    debugLastItemTypeRead = itemType;
                } else if (PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID.equals(fieldName) && reader.peek() != JsonToken.NULL) {
                    itemPipedriveId = reader.nextInt();
                } else if (!fieldName.equals(Response.JSON_PARAM_DATA) || reader.peek() == JsonToken.NULL) {
                    if (Response.JSON_PARAM_DATA.equals(fieldName) && reader.peek() == JsonToken.NULL) {
                        obj = new DeleteItem(itemType, itemPipedriveId);
                        reader.skipValue();
                    } else {
                        reader.skipValue();
                    }
                } else if ("deal".equalsIgnoreCase(itemType)) {
                    obj = Deal.readDeal(reader);
                } else if (FlowItem.ITEM_TYPE_OBJECT_ACTIVITY.equalsIgnoreCase(itemType)) {
                    obj = GsonHelper.fromJSON(reader, ActivityEntity.class);
                } else if ("person".equalsIgnoreCase(itemType)) {
                    obj = PersonNetworkingUtil.readContact(reader);
                } else if ("organization".equalsIgnoreCase(itemType)) {
                    obj = OrganizationsNetworkingUtil.readOrg(reader, session.getOrgAddressField(null));
                } else if ("activityType".equalsIgnoreCase(itemType)) {
                    try {
                        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                        Type type = ActivityType.class;
                        obj = !(gson instanceof Gson) ? gson.fromJson(reader, type) : GsonInstrumentation.fromJson(gson, reader, type);
                    } catch (Exception e) {
                        Log.e(new Throwable("Unable to read the ActivityType update for recents!" + e.getMessage()));
                        obj = null;
                    }
                } else if ("filter".equalsIgnoreCase(itemType)) {
                    obj = FiltersUtil.readFilter(reader);
                } else if ("stage".equalsIgnoreCase(itemType)) {
                    obj = DealStageParser.getStageFromReader(reader);
                } else if ("pipeline".equalsIgnoreCase(itemType)) {
                    obj = InvalidatePipelinesTask.readPipeline(reader);
                } else if (FlowItem.ITEM_TYPE_OBJECT_NOTE.equalsIgnoreCase(itemType)) {
                    obj = GsonHelper.fromJSON(reader, NoteEntity.class);
                } else if (FlowItem.ITEM_TYPE_OBJECT_FILE.equalsIgnoreCase(itemType)) {
                    obj = GsonHelper.fromJSON(reader, FlowFileEntity.class);
                } else if (FlowItem.ITEM_TYPE_OBJECT_EMAILMESSAGE.equalsIgnoreCase(itemType)) {
                    obj = GsonHelper.fromJSON(reader, EmailMessageEntity.class);
                } else if ("emailThread".equalsIgnoreCase(itemType)) {
                    obj = GsonHelper.fromJSON(reader, EmailThreadEntity.class);
                } else if (CustomField.FIELD_DATA_TYPE_USER.equalsIgnoreCase(itemType)) {
                    reader.skipValue();
                } else if ("product".equalsIgnoreCase(itemType)) {
                    obj = GsonHelper.fromJSON(reader, ProductEntity.class);
                }
            } else {
                try {
                    Log.e(new Throwable(String.format("Skipping unknown recents type: fieldName: '%s' Last type read: '%s'", new Object[]{reader.nextName(), debugLastItemTypeRead})));
                } catch (Exception e2) {
                    Log.e(new Throwable(String.format("Skipping unknown recents type: Error in reading next field. Last type read: '%s' Error: '%s'", new Object[]{debugLastItemTypeRead, e2.getMessage()})));
                }
                reader.skipValue();
            }
        }
        reader.endObject();
        return obj;
    }
}
