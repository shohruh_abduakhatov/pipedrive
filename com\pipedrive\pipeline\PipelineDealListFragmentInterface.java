package com.pipedrive.pipeline;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;

@MainThread
public interface PipelineDealListFragmentInterface {
    void attach(int i, int i2, PipelineDealListFragment pipelineDealListFragment);

    void detach(int i, int i2, PipelineDealListFragment pipelineDealListFragment);

    void refresh(int i);

    void requery(@NonNull Integer num);
}
