package com.pipedrive.views.viewholder;

import android.support.annotation.LayoutRes;

public interface ViewHolder {
    @LayoutRes
    int getLayoutResourceId();
}
