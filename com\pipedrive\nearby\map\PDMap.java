package com.pipedrive.nearby.map;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.cards.CardSelectionProvider;
import com.pipedrive.nearby.searchbutton.SearchButtonClickProvider;
import com.pipedrive.nearby.toolbar.NearbyItemTypeChangesProvider;

public interface PDMap extends NearbyItemsProvider, ViewportChangeProvider, MarkerSelectionProvider {
    void setup(@NonNull Session session, @NonNull LocationProvider locationProvider, @NonNull LocationSourceProvider locationSourceProvider, @NonNull NearbyItemTypeChangesProvider nearbyItemTypeChangesProvider, @NonNull SearchButtonClickProvider searchButtonClickProvider, @NonNull CardSelectionProvider cardSelectionProvider);
}
