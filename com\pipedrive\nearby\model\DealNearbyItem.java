package com.pipedrive.nearby.model;

import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.BaseDataSource;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.datasource.PipelineDataSource;
import com.pipedrive.model.Deal;
import java.util.List;
import rx.Single;

public final class DealNearbyItem extends NearbyItem<Deal> {
    public DealNearbyItem(@NonNull Double latitude, @NonNull Double longitude, @NonNull String address, @NonNull Long singleItemSqlId) {
        super(latitude, longitude, address, singleItemSqlId);
    }

    public DealNearbyItem(@NonNull Double latitude, @NonNull Double longitude, @NonNull String address, @NonNull List<Long> multipleItemSqlIds) {
        super(latitude, longitude, address, (List) multipleItemSqlIds);
    }

    @NonNull
    protected final String getActivitiesColumnForNearbyItem() {
        return PipeSQLiteHelper.COLUMN_ACTIVITIES_DEAL_ID_SQL;
    }

    @NonNull
    protected final BaseDataSource<Deal> getDataSource(@NonNull Session session) {
        return new DealsDataSource(session.getDatabase());
    }

    @NonNull
    public final Single<Pair<Integer, Integer>> getStagesTotalCountAndSelectedStageOrder(@NonNull Session session, @IntRange(from = 1) @NonNull Integer stageId) {
        return new PipelineDataSource(session.getDatabase()).getSelectedStageOrderRx(stageId);
    }

    public int hashCode() {
        return super.hashCode();
    }

    public boolean equals(Object o) {
        return super.equals(o);
    }
}
