package com.pipedrive.presenter;

import android.support.annotation.CallSuper;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.tasks.AsyncTask;
import com.pipedrive.tasks.AsyncTask.AsyncTaskExecutor;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@MainThread
public class Presenter<T> implements AsyncTaskExecutor {
    @Nullable
    private ExecutorService mPresenterAsyncTaskExecutor;
    @NonNull
    private final LinkedList<AsyncTask> mPresenterAsyncTaskRegister = new LinkedList();
    @NonNull
    private final Session mSession;
    @Nullable
    protected T mView;

    public Presenter(@NonNull Session session) {
        this.mSession = session;
    }

    @NonNull
    protected Session getSession() {
        return this.mSession;
    }

    @Nullable
    protected T getView() {
        return this.mView;
    }

    @CallSuper
    public synchronized void bindView(@NonNull T view) {
        this.mView = view;
    }

    @CallSuper
    public synchronized void unbindView() {
        cancelAllPresenterTasks();
        this.mView = null;
    }

    private void cancelAllPresenterTasks() {
        getAsyncTaskExecutor().shutdown();
        Iterator it = this.mPresenterAsyncTaskRegister.iterator();
        while (it.hasNext()) {
            ((AsyncTask) it.next()).cancel(true);
        }
    }

    @NonNull
    public ExecutorService getAsyncTaskExecutor() {
        boolean createNewExecutorService = this.mPresenterAsyncTaskExecutor == null || this.mPresenterAsyncTaskExecutor.isShutdown() || this.mPresenterAsyncTaskExecutor.isTerminated();
        if (createNewExecutorService) {
            this.mPresenterAsyncTaskExecutor = Executors.newSingleThreadExecutor();
        }
        return this.mPresenterAsyncTaskExecutor;
    }

    public void registerAsyncTask(@NonNull AsyncTask asyncTask) {
        this.mPresenterAsyncTaskRegister.add(asyncTask);
    }
}
