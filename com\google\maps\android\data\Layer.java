package com.google.maps.android.data;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnPolygonClickListener;
import com.google.android.gms.maps.GoogleMap.OnPolylineClickListener;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.Polyline;
import com.google.maps.android.data.geojson.GeoJsonLineStringStyle;
import com.google.maps.android.data.geojson.GeoJsonPointStyle;
import com.google.maps.android.data.geojson.GeoJsonPolygonStyle;
import com.google.maps.android.data.geojson.GeoJsonRenderer;
import com.google.maps.android.data.kml.KmlContainer;
import com.google.maps.android.data.kml.KmlGroundOverlay;
import com.google.maps.android.data.kml.KmlRenderer;
import java.io.IOException;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParserException;

public abstract class Layer {
    private Renderer mRenderer;

    public interface OnFeatureClickListener {
        void onFeatureClick(Feature feature);
    }

    protected void addKMLToMap() throws IOException, XmlPullParserException {
        if (this.mRenderer instanceof KmlRenderer) {
            ((KmlRenderer) this.mRenderer).addLayerToMap();
            return;
        }
        throw new UnsupportedOperationException("Stored renderer is not a KmlRenderer");
    }

    protected void addGeoJsonToMap() {
        if (this.mRenderer instanceof GeoJsonRenderer) {
            ((GeoJsonRenderer) this.mRenderer).addLayerToMap();
            return;
        }
        throw new UnsupportedOperationException("Stored renderer is not a GeoJsonRenderer");
    }

    public void removeLayerFromMap() {
        if (this.mRenderer instanceof GeoJsonRenderer) {
            ((GeoJsonRenderer) this.mRenderer).removeLayerFromMap();
        } else if (this.mRenderer instanceof KmlRenderer) {
            ((KmlRenderer) this.mRenderer).removeLayerFromMap();
        }
    }

    public void setOnFeatureClickListener(final OnFeatureClickListener listener) {
        GoogleMap map = getMap();
        map.setOnPolygonClickListener(new OnPolygonClickListener() {
            public void onPolygonClick(Polygon polygon) {
                if (Layer.this.getFeature(polygon) != null) {
                    listener.onFeatureClick(Layer.this.getFeature(polygon));
                } else if (Layer.this.getContainerFeature(polygon) != null) {
                    listener.onFeatureClick(Layer.this.getContainerFeature(polygon));
                } else {
                    listener.onFeatureClick(Layer.this.getFeature(Layer.this.multiObjectHandler(polygon)));
                }
            }
        });
        map.setOnMarkerClickListener(new OnMarkerClickListener() {
            public boolean onMarkerClick(Marker marker) {
                if (Layer.this.getFeature(marker) != null) {
                    listener.onFeatureClick(Layer.this.getFeature(marker));
                } else if (Layer.this.getContainerFeature(marker) != null) {
                    listener.onFeatureClick(Layer.this.getContainerFeature(marker));
                } else {
                    listener.onFeatureClick(Layer.this.getFeature(Layer.this.multiObjectHandler(marker)));
                }
                return false;
            }
        });
        map.setOnPolylineClickListener(new OnPolylineClickListener() {
            public void onPolylineClick(Polyline polyline) {
                if (Layer.this.getFeature(polyline) != null) {
                    listener.onFeatureClick(Layer.this.getFeature(polyline));
                } else if (Layer.this.getContainerFeature(polyline) != null) {
                    listener.onFeatureClick(Layer.this.getContainerFeature(polyline));
                } else {
                    listener.onFeatureClick(Layer.this.getFeature(Layer.this.multiObjectHandler(polyline)));
                }
            }
        });
    }

    private ArrayList<?> multiObjectHandler(Object mapObject) {
        for (ArrayList<?> value : this.mRenderer.getValues()) {
            if (value.getClass().getSimpleName().equals("ArrayList")) {
                ArrayList<?> mapObjects = value;
                if (mapObjects.contains(mapObject)) {
                    return mapObjects;
                }
            }
        }
        return null;
    }

    protected void storeRenderer(Renderer renderer) {
        this.mRenderer = renderer;
    }

    public Iterable<? extends Feature> getFeatures() {
        return this.mRenderer.getFeatures();
    }

    public Feature getFeature(Object mapObject) {
        return this.mRenderer.getFeature(mapObject);
    }

    public Feature getContainerFeature(Object mapObject) {
        return this.mRenderer.getContainerFeature(mapObject);
    }

    protected boolean hasFeatures() {
        return this.mRenderer.hasFeatures();
    }

    protected boolean hasContainers() {
        if (this.mRenderer instanceof KmlRenderer) {
            return ((KmlRenderer) this.mRenderer).hasNestedContainers();
        }
        return false;
    }

    protected Iterable<KmlContainer> getContainers() {
        if (this.mRenderer instanceof KmlRenderer) {
            return ((KmlRenderer) this.mRenderer).getNestedContainers();
        }
        return null;
    }

    protected Iterable<KmlGroundOverlay> getGroundOverlays() {
        if (this.mRenderer instanceof KmlRenderer) {
            return ((KmlRenderer) this.mRenderer).getGroundOverlays();
        }
        return null;
    }

    public GoogleMap getMap() {
        return this.mRenderer.getMap();
    }

    public void setMap(GoogleMap map) {
        this.mRenderer.setMap(map);
    }

    public boolean isLayerOnMap() {
        return this.mRenderer.isLayerOnMap();
    }

    protected void addFeature(Feature feature) {
        this.mRenderer.addFeature(feature);
    }

    protected void removeFeature(Feature feature) {
        this.mRenderer.removeFeature(feature);
    }

    public GeoJsonPointStyle getDefaultPointStyle() {
        return this.mRenderer.getDefaultPointStyle();
    }

    public GeoJsonLineStringStyle getDefaultLineStringStyle() {
        return this.mRenderer.getDefaultLineStringStyle();
    }

    public GeoJsonPolygonStyle getDefaultPolygonStyle() {
        return this.mRenderer.getDefaultPolygonStyle();
    }
}
