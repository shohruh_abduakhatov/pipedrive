package com.pipedrive.activity;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.contentproviders.FlowContentProvider;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.model.Activity;
import com.pipedrive.store.StoreActivity;
import com.pipedrive.tasks.AsyncTask;

class DeleteActivityTask extends AsyncTask<Long, Void, Boolean> {
    private final OnTaskFinished mOnTaskFinished;

    @MainThread
    interface OnTaskFinished {
        void onActivityDeleted(boolean z);
    }

    public DeleteActivityTask(@NonNull Session session, @Nullable OnTaskFinished onTaskFinished) {
        super(session);
        this.mOnTaskFinished = onTaskFinished;
    }

    protected Boolean doInBackground(Long... params) {
        Activity activityToDelete = (Activity) new ActivitiesDataSource(getSession()).findBySqlId(params[0].longValue());
        if (activityToDelete == null) {
            return Boolean.valueOf(false);
        }
        activityToDelete.setActive(false);
        return Boolean.valueOf(new StoreActivity(getSession()).update(activityToDelete));
    }

    protected void onPostExecute(Boolean success) {
        if (success.booleanValue()) {
            FlowContentProvider.notifyChangesMadeInFlow(getSession().getApplicationContext());
        }
        if (this.mOnTaskFinished != null) {
            this.mOnTaskFinished.onActivityDeleted(success.booleanValue());
        }
    }
}
