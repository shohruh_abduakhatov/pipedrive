package com.pipedrive.tasks.activities;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.pipedrive.application.Session;
import com.pipedrive.tasks.AsyncTask;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;

public class ActivityTypesTask extends AsyncTask<String, Integer, Void> {
    public ActivityTypesTask(@NonNull Session session) {
        super(session);
    }

    protected Void doInBackground(String... params) {
        String activitiesTypes = downloadActivityTypesList(getSession());
        if (!TextUtils.isEmpty(activitiesTypes)) {
            getSession().setActivityTypes(activitiesTypes);
        }
        return null;
    }

    @Nullable
    private static String downloadActivityTypesList(Session session) {
        return ConnectionUtil.inputStreamToString(ConnectionUtil.requestGet(new ApiUrlBuilder(session, ApiUrlBuilder.ENDPOINT_ACTIVITY_TYPES)));
    }
}
