package com.zendesk.sdk.model.helpcenter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.util.CollectionUtils;
import java.util.List;

public class ArticlesListResponse implements ArticlesResponse {
    private List<Article> articles;
    private List<Category> categories;
    private String nextPage;
    private String previousPage;
    private List<Section> sections;
    private List<User> users;

    @NonNull
    public List<Article> getArticles() {
        return CollectionUtils.copyOf(this.articles);
    }

    @NonNull
    public List<Category> getCategories() {
        return CollectionUtils.copyOf(this.categories);
    }

    @NonNull
    public List<Section> getSections() {
        return CollectionUtils.copyOf(this.sections);
    }

    @NonNull
    public List<User> getUsers() {
        return CollectionUtils.copyOf(this.users);
    }

    @Nullable
    public String getNextPage() {
        return this.nextPage;
    }

    @Nullable
    public String getPreviousPage() {
        return this.previousPage;
    }
}
