package rx;

import rx.functions.Func7;
import rx.functions.FuncN;

class Single$8 implements FuncN<R> {
    final /* synthetic */ Func7 val$zipFunction;

    Single$8(Func7 func7) {
        this.val$zipFunction = func7;
    }

    public R call(Object... args) {
        return this.val$zipFunction.call(args[0], args[1], args[2], args[3], args[4], args[5], args[6]);
    }
}
