package com.zendesk.sdk.model.helpcenter;

import android.support.annotation.Nullable;

public class ArticleVoteResponse {
    private ArticleVote vote;

    @Nullable
    public ArticleVote getVote() {
        return this.vote;
    }
}
