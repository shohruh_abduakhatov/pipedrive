package com.pipedrive.util.communicatiomedium;

import android.support.annotation.NonNull;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.contact.CommunicationMedium;
import java.util.List;

public interface CommunicationMediumUtil<T extends BaseDatasourceEntity> {
    List<? extends CommunicationMedium> getEmails(@NonNull T t);

    List<? extends CommunicationMedium> getPhones(@NonNull T t);

    boolean hasEmails(@NonNull T t);

    boolean hasMultiplePhones(@NonNull T t);

    boolean hasPhones(@NonNull T t);
}
