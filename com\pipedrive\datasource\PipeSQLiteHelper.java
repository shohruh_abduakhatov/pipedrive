package com.pipedrive.datasource;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.application.Session;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;

public class PipeSQLiteHelper extends PipeSQLiteOpenHelper {
    public static final String COLUMN_ACTIVE = "active";
    public static final String COLUMN_ACTIVITIES_ASSIGNED_TO_USER_ID = "ac_assigned_to_user_id";
    public static final String COLUMN_ACTIVITIES_DEAL_ID_SQL = "ac_deal_id_sql";
    public static final String COLUMN_ACTIVITIES_DONE = "ac_done";
    public static final String COLUMN_ACTIVITIES_END_DATETIME = "ac_end";
    public static final String COLUMN_ACTIVITIES_IS_ACTIVE = "ac_is_active";
    public static final String COLUMN_ACTIVITIES_NOTE = "ac_note";
    public static final String COLUMN_ACTIVITIES_ORGANIZATION_ID_SQL = "ac_org_id_sql";
    public static final String COLUMN_ACTIVITIES_PERSON_ID_SQL = "ac_person_id_sql";
    public static final String COLUMN_ACTIVITIES_PIPEDRIVE_ID = "ac_pd_id";
    public static final String COLUMN_ACTIVITIES_START_DATETIME = "ac_start";
    public static final String COLUMN_ACTIVITIES_START_DATETIME_TYPE = "ac_start_type";
    public static final String COLUMN_ACTIVITIES_SUBJECT = "ac_subject";
    public static final String COLUMN_ACTIVITIES_SUBJECT_SEARCH_FIELD = "ac_subject_search_field";
    public static final String COLUMN_ACTIVITIES_TYPE = "ac_type";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_CHANGES_JOURNAL_OPERATION = "cj_op";
    public static final String COLUMN_CHANGES_JOURNAL_OPERATION_METADATA = "cj_op_metadata";
    public static final String COLUMN_CHANGES_JOURNAL_OPERATION_TYPE = "cj_op_type";
    public static final String COLUMN_CODE = "cur_code";
    public static final String COLUMN_CUSTOM_FIELDS_ACTIVE = "custom_fields_active";
    public static final String COLUMN_CUSTOM_FIELDS_FIELD_DATA_TYPE = "custom_fields_field_data_type";
    public static final String COLUMN_CUSTOM_FIELDS_KEY = "custom_fields_key";
    public static final String COLUMN_CUSTOM_FIELDS_NAME = "custom_fields_name";
    public static final String COLUMN_CUSTOM_FIELDS_OPTIONS = "custom_fields_options";
    public static final String COLUMN_CUSTOM_FIELDS_ORDER_NR = "custom_fields_order_nr";
    public static final String COLUMN_CUSTOM_FIELDS_PIPEDRIVE_ID = "custom_fields_id";
    public static final String COLUMN_CUSTOM_FIELDS_TYPE = "custom_fields_type";
    public static final String COLUMN_DEALS_ADD_TIME = "d_add_time";
    public static final String COLUMN_DEALS_CURRENCY = "d_currency";
    public static final String COLUMN_DEALS_CUSTOM_FIELDS = "d_custom_fields";
    public static final String COLUMN_DEALS_DEAL_OWNER_PD_ID = "d_owner_pd_id";
    public static final String COLUMN_DEALS_DROP_BOX_ADDRESS = "d_dropbox_address";
    public static final String COLUMN_DEALS_EXPECTED_CLOSE_DATE = "d_expected_close_date";
    public static final String COLUMN_DEALS_FORMATTED_VALUE = "d_formatted_value";
    public static final String COLUMN_DEALS_LOST_TIME = "d_won_time";
    public static final String COLUMN_DEALS_NEXT_TASK_DATE = "d_next_task_date";
    public static final String COLUMN_DEALS_NEXT_TASK_TIME = "d_next_task_time";
    public static final String COLUMN_DEALS_ORGANIZATION_SQL_ID = "d_org_id_sql";
    public static final String COLUMN_DEALS_OWNER_NAME = "d_owner_name";
    public static final String COLUMN_DEALS_PERSON_SQL_ID = "d_person_id_sql";
    public static final String COLUMN_DEALS_PIPEDRIVE_ID = "d_pd_id";
    public static final String COLUMN_DEALS_PIPELINE_ID = "d_pipeline_id";
    public static final String COLUMN_DEALS_PRODUCT_COUNT = "d_product_count";
    public static final String COLUMN_DEALS_ROTTEN_TIME = "d_rotten_time";
    public static final String COLUMN_DEALS_STAGE = "d_stage";
    public static final String COLUMN_DEALS_STATUS = "d_status";
    public static final String COLUMN_DEALS_TITLE = "d_title";
    public static final String COLUMN_DEALS_TITLE_SEARCH_FIELD = "d_title_search_field";
    public static final String COLUMN_DEALS_UNDONE_ACTIVITIES_COUNT = "d_undone_activities_count";
    public static final String COLUMN_DEALS_UPDATE_TIME = "d_update_time";
    public static final String COLUMN_DEALS_VALUE = "d_value";
    public static final String COLUMN_DEALS_VISIBLE_TO = "d_visible_to";
    public static final String COLUMN_DEALS_WON_TIME = "d_lost_time";
    public static final String COLUMN_DEAL_PROBABILITY = "deal_probability";
    public static final String COLUMN_DEAL_PRODUCTS_CURRENCY_SQL_ID = "deal_products_currency_sql_id";
    public static final String COLUMN_DEAL_PRODUCTS_DEAL_SQL_ID = "deal_products_deal_sql_id";
    public static final String COLUMN_DEAL_PRODUCTS_DISCOUNT_PERCENTAGE = "deal_products_discount_percentage";
    public static final String COLUMN_DEAL_PRODUCTS_DURATION = "deal_products_duration";
    public static final String COLUMN_DEAL_PRODUCTS_IS_ACTIVE = "deal_products_is_active";
    public static final String COLUMN_DEAL_PRODUCTS_ORDER = "deal_products_order";
    public static final String COLUMN_DEAL_PRODUCTS_PIPEDRIVE_ID = "deal_products_pipedrive_id";
    public static final String COLUMN_DEAL_PRODUCTS_PRICE = "deal_products_price";
    public static final String COLUMN_DEAL_PRODUCTS_PRODUCT_SQL_ID = "deal_products_product_sql_id";
    public static final String COLUMN_DEAL_PRODUCTS_PRODUCT_VARIATION_SQL_ID = "deal_products_product_variation_sql_id";
    public static final String COLUMN_DEAL_PRODUCTS_QUANTITY = "deal_products_quantity";
    public static final String COLUMN_DECIMAL_POINTS = "cur_dec_points";
    public static final String COLUMN_DEFAULT = "pipe_default";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_EMAIL_ATTACHMENT_ACTIVE = "email_attachments_active";
    public static final String COLUMN_EMAIL_ATTACHMENT_ADD_TIME = "email_attachments_add_time";
    public static final String COLUMN_EMAIL_ATTACHMENT_CLEAN_NAME = "email_attachments_clean_name";
    public static final String COLUMN_EMAIL_ATTACHMENT_COMMENT = "email_attachments_comment";
    public static final String COLUMN_EMAIL_ATTACHMENT_COMPANY_ID = "email_attachments_company_id";
    public static final String COLUMN_EMAIL_ATTACHMENT_FILE_NAME = "email_attachments_file_name";
    public static final String COLUMN_EMAIL_ATTACHMENT_FILE_SIZE = "email_attachments_file_size";
    public static final String COLUMN_EMAIL_ATTACHMENT_FILE_TYPE = "email_attachments_file_type";
    public static final String COLUMN_EMAIL_ATTACHMENT_INLINE = "email_attachments_inline";
    public static final String COLUMN_EMAIL_ATTACHMENT_MESSAGE_ID = "email_attachments_message_id";
    public static final String COLUMN_EMAIL_ATTACHMENT_PIPEDRIVE_ID = "email_attachments_pipedrive_id";
    public static final String COLUMN_EMAIL_ATTACHMENT_URL = "email_attachments_url";
    public static final String COLUMN_EMAIL_MESSAGES_ACTIVE = "email_messages_active";
    public static final String COLUMN_EMAIL_MESSAGES_ADD_TIME = "email_messages_add_time";
    public static final String COLUMN_EMAIL_MESSAGES_ATTACHMENT_COUNT = "email_messages_attachment_count";
    public static final String COLUMN_EMAIL_MESSAGES_BODY = "email_messages_body";
    public static final String COLUMN_EMAIL_MESSAGES_DRAFT = "email_messages_draft";
    public static final String COLUMN_EMAIL_MESSAGES_MESSAGE_TIME = "email_messages_message_time";
    public static final String COLUMN_EMAIL_MESSAGES_PIPEDRIVE_ID = "email_messages_pipedrive_id";
    public static final String COLUMN_EMAIL_MESSAGES_RECEPIENTS_DISPLAY_STRING = "email_messages_recepients_display_string";
    public static final String COLUMN_EMAIL_MESSAGES_SENDERS_DISPLAY_STRING = "email_messages_senders_display_string";
    public static final String COLUMN_EMAIL_MESSAGES_SUBJECT = "email_messages_subject";
    public static final String COLUMN_EMAIL_MESSAGES_SUMMARY = "email_messages_summary";
    public static final String COLUMN_EMAIL_MESSAGES_THREAD_ID = "email_messages_thread_id";
    public static final String COLUMN_EMAIL_MESSAGES_UPDATE_TIME = "email_messages_update_time";
    public static final String COLUMN_EMAIL_PARTICIPANTS_TO_MESSAGE_EMAIL_MESSAGE_ID = "email_participants_to_messages_email_message_id";
    public static final String COLUMN_EMAIL_PARTICIPANTS_TO_MESSAGE_PARTICIPANT_ID = "email_participants_to_messages_participant_id";
    public static final String COLUMN_EMAIL_PARTICIPANTS_TO_MESSAGE_POSITION = "email_participants_to_messages_email_position";
    public static final String COLUMN_EMAIL_PARTICIPANT_EMAIL_ADDRESS = "email_participants_email_address";
    public static final String COLUMN_EMAIL_PARTICIPANT_NAME = "email_participants_name";
    public static final String COLUMN_EMAIL_PARTICIPANT_PERSON_ID = "email_participants_person_id";
    public static final String COLUMN_EMAIL_PARTICIPANT_PIPEDRIVE_ID = "email_participants_pipedrive_id";
    public static final String COLUMN_EMAIL_PARTICIPANT_USER_ID = "email_participants_user_id";
    public static final String COLUMN_EMAIL_THREAD_DEAL_ID = "email_threads_deal_id";
    public static final String COLUMN_EMAIL_THREAD_PIPEDRIVE_ID = "email_threads_pipedrive_id";
    public static final String COLUMN_EMAIL_THREAD_TO_ORG_ORG_ID = "email_threads_to_orgs_org_id";
    public static final String COLUMN_EMAIL_THREAD_TO_ORG_THREAD_ID = "email_threads_to_orgs_thread_id";
    public static final String COLUMN_FILES_ACTIVE = "files_active";
    public static final String COLUMN_FILES_ACTIVITY_ID = "files_ac_pd_id";
    public static final String COLUMN_FILES_ADD_TIME = "files_add_time";
    public static final String COLUMN_FILES_COMMENT = "files_comment";
    public static final String COLUMN_FILES_DEAL_ID = "files_d_pd_id";
    public static final String COLUMN_FILES_EMAIL_MESSAGE_ID = "files_email_message_id";
    public static final String COLUMN_FILES_FILE_NAME = "files_file_name";
    public static final String COLUMN_FILES_FILE_SIZE = "files_file_size";
    public static final String COLUMN_FILES_FILE_TYPE = "files_file_type";
    public static final String COLUMN_FILES_INLINE = "files_inline";
    public static final String COLUMN_FILES_LOG_ID = "files_log_id";
    public static final String COLUMN_FILES_NAME = "files_name";
    public static final String COLUMN_FILES_NOTE_ID = "files_n_pd_id";
    public static final String COLUMN_FILES_ORG_ID = "files_org_id";
    public static final String COLUMN_FILES_PEOPLE_NAME = "files_people_name";
    public static final String COLUMN_FILES_PERSON_ID = "files_person_id";
    public static final String COLUMN_FILES_PIPEDRIVE_ID = "files_id";
    public static final String COLUMN_FILES_PRODUCT_ID = "files_product_id";
    public static final String COLUMN_FILES_PRODUCT_NAME = "files_product_name";
    public static final String COLUMN_FILES_REMOTE_ID = "files_remote_id";
    public static final String COLUMN_FILES_REMOTE_LOCATION = "files_remote_location";
    public static final String COLUMN_FILES_UPDATE_TIME = "files_update_time";
    public static final String COLUMN_FILES_UPLOAD_STATUS = "files_upload_status";
    public static final String COLUMN_FILES_URL = "files_url";
    public static final String COLUMN_FILES_USER_ID = "files_user_id";
    public static final String COLUMN_FILTERS_IS_ACTIVE = "filters_is_active";
    public static final String COLUMN_FILTERS_IS_PRIVATE = "filters_is_private";
    public static final String COLUMN_FILTERS_NAME = "filters_name";
    public static final String COLUMN_FILTERS_PIPEDRIVE_ID = "filters_pipedrive_id";
    public static final String COLUMN_FILTERS_TYPE = "filters_type";
    public static final String COLUMN_FILTER_DEALS_DEAL_PIPEDRIVE_ID = "filter_deals_pipedrive_id";
    public static final String COLUMN_FILTER_DEALS_FILTER_PIPEDRIVE_ID = "filter_deals_filter_pipedrive_id";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_IS_CUSTOM = "is_custom";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_NOTES_ACTIVE = "n_is_active";
    public static final String COLUMN_NOTES_ADD_TIME = "n_add_time";
    public static final String COLUMN_NOTES_CONTENT = "n_content";
    public static final String COLUMN_NOTES_DEAL_SQL_ID = "n_deal_sql_id";
    public static final String COLUMN_NOTES_ORGANIZATION_SQL_ID = "n_org_sql_id";
    public static final String COLUMN_NOTES_PERSON_SQL_ID = "n_person_sql_id";
    public static final String COLUMN_NOTES_PIPEDRIVE_ID = "n_pd_id";
    public static final String COLUMN_ORDER_NR = "order_nr";
    public static final String COLUMN_ORG_ADDRESS_LATITUDE = "org_address_latitude";
    public static final String COLUMN_ORG_ADDRESS_LONGITUDE = "org_address_longitude";
    public static final String COLUMN_ORG_DROP_BOX_ADDRESS = "org_dropbox_address";
    public static final String COLUMN_ORG_IS_ACTIVE = "org_is_active";
    public static final String COLUMN_ORG_IS_SHADOW = "org_is_shadow";
    public static final String COLUMN_ORG_NAME_SEARCH_FIELD = "org_name_search_field";
    public static final String COLUMN_ORG_OWNER_NAME = "org_owner_name";
    public static final String COLUMN_OTHER_FIELDS = "other_fields";
    public static final String COLUMN_OWNER_ID = "owner_id";
    public static final String COLUMN_PERSONS_DROP_BOX_ADDRESS = "c_dropbox_address";
    public static final String COLUMN_PERSONS_EMAILS = "c_emails";
    public static final String COLUMN_PERSONS_IMS = "c_ims";
    public static final String COLUMN_PERSONS_IS_ACTIVE = "c_is_active";
    public static final String COLUMN_PERSONS_IS_SHADOW = "c_is_shadow";
    public static final String COLUMN_PERSONS_NAME = "c_name";
    public static final String COLUMN_PERSONS_NAME_SEARCH_FIELD = "c_name_search_field";
    public static final String COLUMN_PERSONS_ORGANIZATION_SQL_ID = "c_org_id_sql";
    public static final String COLUMN_PERSONS_OTHER_FIELDS = "c_other_fields";
    public static final String COLUMN_PERSONS_OWNER_ID = "c_owner_id";
    public static final String COLUMN_PERSONS_OWNER_NAME = "c_owner_name";
    public static final String COLUMN_PERSONS_PHONES = "c_phones";
    public static final String COLUMN_PERSONS_PHONES_SEARCH_FIELD = "c_phones_search_field";
    public static final String COLUMN_PERSONS_PICTURE_ID = "c_pic_id";
    public static final String COLUMN_PERSONS_PIPEDRIVE_ID = "c_pd_id";
    public static final String COLUMN_PERSONS_POSTAL_ADDRESS = "c_postal_address";
    public static final String COLUMN_PERSONS_VISIBLE_TO = "c_visible_to";
    public static final String COLUMN_PHONE = "phone";
    public static final String COLUMN_PIPEDRIVE_ID = "id";
    public static final String COLUMN_PRODUCTS_IS_ACTIVE = "products_is_active";
    public static final String COLUMN_PRODUCTS_IS_REMOVED = "products_is_removed";
    public static final String COLUMN_PRODUCTS_NAME = "products_name";
    public static final String COLUMN_PRODUCTS_PIPEDRIVE_ID = "products_pipedrive_id";
    public static final String COLUMN_PRODUCT_PRICES_CURRENCY_SQL_ID = "product_prices_currency_sql_id";
    public static final String COLUMN_PRODUCT_PRICES_PIPEDRIVE_ID = "product_prices_pipedrive_id";
    public static final String COLUMN_PRODUCT_PRICES_PRICE = "product_prices_price";
    public static final String COLUMN_PRODUCT_PRICES_PRODUCT_SQL_ID = "product_prices_product_sqlId";
    public static final String COLUMN_PRODUCT_VARIATIONS_NAME = "product_variations_name";
    public static final String COLUMN_PRODUCT_VARIATIONS_PIPEDRIVE_ID = "product_variations_pipedrive_id";
    public static final String COLUMN_PRODUCT_VARIATIONS_PRODUCT_SQL_ID = "product_variations_product_sql_id";
    public static final String COLUMN_PRODUCT_VARIATION_PRICES_CURRENCY_SQL_ID = "product_variation_prices_currency_sql_id";
    public static final String COLUMN_PRODUCT_VARIATION_PRICES_PIPEDRIVE_ID = "product_variation_prices_pipedrive_id";
    public static final String COLUMN_PRODUCT_VARIATION_PRICES_PRICE = "product_variation_prices_price";
    public static final String COLUMN_PRODUCT_VARIATION_PRICES_PRODUCT_VARIATION_SQL_ID = "product_variation_prices_product_variation_sql_id";
    public static final String COLUMN_RATE_TO_DEFAULT = "currency_rate";
    public static final String COLUMN_SELECTED = "selected";
    public static final String COLUMN_SYMBOL = "cur_symbol";
    public static final String COLUMN_TYPE = "item_type";
    public static final String COLUMN_USERS_ICON_URL = "u_icon_url";
    public static final String COLUMN_USERS_IS_ACTIVE = "u_is_active";
    public static final String COLUMN_USERS_NAME = "u_name";
    public static final String COLUMN_USERS_PIPEDRIVE_ID = "u_pd_id";
    public static final String COLUMN_USER_ID = "user_id";
    public static final int COLUMN_VALUE_FALSE = 0;
    public static final int COLUMN_VALUE_TRUE = 1;
    public static final String COLUMN_VISIBLE_TO = "visible_to";
    private static final String DATABASE_CREATE_ACTIVITIES_TABLE = "create table activities(_id integer primary key autoincrement, ac_pd_id integer, ac_subject text, ac_start integer,ac_end integer,ac_start_type integer,ac_note text, ac_done integer, ac_type text, ac_person_id_sql integer, ac_org_id_sql integer, ac_deal_id_sql integer, ac_assigned_to_user_id integer, ac_is_active integer, ac_subject_search_field text );";
    private static final String DATABASE_CREATE_CURRENCIES_TABLE = "create table currencies(_id integer primary key autoincrement, id integer, name text not null, cur_dec_points integer, cur_code text, currency_rate real, active integer, is_custom integer, cur_symbol text,  UNIQUE(id));";
    private static final String DATABASE_CREATE_CUSTOM_FIELDS_TABLE = "create table custom_fields(_id integer primary key autoincrement, custom_fields_id integer, custom_fields_key text not null, custom_fields_name text not null, custom_fields_order_nr integer,custom_fields_active integer, custom_fields_field_data_type text, custom_fields_options text, custom_fields_type text,  UNIQUE(custom_fields_id));";
    private static final String DATABASE_CREATE_DEALS_TABLE = "create table deals(_id integer primary key autoincrement, d_pd_id integer, d_pipeline_id integer, d_title text, d_value real, d_currency text, d_stage integer, d_person_id_sql integer, d_org_id_sql integer, d_undone_activities_count integer, d_next_task_date text, d_next_task_time text, d_status text, d_formatted_value text, d_owner_pd_id integer, d_rotten_time text, d_custom_fields blob, d_dropbox_address text, d_title_search_field text, d_visible_to integer, d_owner_name text, d_expected_close_date text, d_add_time text, d_update_time text, d_lost_time text, d_won_time text, d_product_count real );";
    private static final String DATABASE_CREATE_DEAL_PRODUCTS_TABLE = "create table deal_products(_id integer primary key autoincrement, deal_products_pipedrive_id integer, deal_products_deal_sql_id integer, deal_products_product_sql_id integer, deal_products_product_variation_sql_id integer, deal_products_order integer, deal_products_price real, deal_products_quantity real, deal_products_duration real, deal_products_discount_percentage real, deal_products_currency_sql_id integer, deal_products_is_active integer);";
    private static final String DATABASE_CREATE_EMAIL_ATTACHMENTS_TABLE = "create table email_attachments(_id integer primary key autoincrement, email_attachments_pipedrive_id integer not null, email_attachments_message_id integer not null, email_attachments_company_id integer, email_attachments_active integer, email_attachments_add_time integer, email_attachments_clean_name text, email_attachments_comment text, email_attachments_file_name text, email_attachments_file_size integer, email_attachments_file_type text, email_attachments_inline integer, email_attachments_url text );";
    private static final String DATABASE_CREATE_EMAIL_MESSAGES_TABLE = "create table email_messages(_id integer primary key autoincrement, email_messages_pipedrive_id integer, email_messages_thread_id integer not null, email_messages_subject text, email_messages_summary text, email_messages_body blob, email_messages_message_time integer, email_messages_add_time integer, email_messages_update_time integer, email_messages_active integer, email_messages_draft integer, email_messages_senders_display_string text, email_messages_recepients_display_string text, email_messages_attachment_count integer, UNIQUE(email_messages_pipedrive_id));";
    private static final String DATABASE_CREATE_EMAIL_PARTICIPANTS_TABLE = "create table email_participants(_id integer primary key autoincrement, email_participants_pipedrive_id integer, email_participants_email_address text, email_participants_name text, email_participants_person_id integer,email_participants_user_id integer );";
    private static final String DATABASE_CREATE_EMAIL_PARTICIPANTS_TO_MESSAGES_TABLE = "create table email_participants_to_messages(_id integer primary key autoincrement, email_participants_to_messages_email_message_id integer not null, email_participants_to_messages_participant_id integer not null, email_participants_to_messages_email_position text not null, UNIQUE(email_participants_to_messages_email_message_id, email_participants_to_messages_participant_id, email_participants_to_messages_email_position));";
    private static final String DATABASE_CREATE_EMAIL_THREADS_TABLE = "create table email_threads(_id integer primary key autoincrement, email_threads_pipedrive_id integer, email_threads_deal_id integer );";
    private static final String DATABASE_CREATE_EMAIL_THREADS_TO_ORGS_TABLE = "create table email_threads_to_orgs(_id integer primary key autoincrement, email_threads_to_orgs_thread_id integer not null, email_threads_to_orgs_org_id integer not null);";
    private static final String DATABASE_CREATE_FILES_TABLE = "create table files(_id integer primary key autoincrement, files_id integer, files_user_id integer, files_d_pd_id integer, files_person_id integer, files_org_id integer, files_product_id integer, files_product_name text, files_email_message_id integer, files_ac_pd_id integer, files_n_pd_id integer, files_log_id integer, files_add_time integer, files_update_time integer, files_file_name text, files_file_type text, files_file_size integer, files_active integer default 0, files_inline integer default 0, files_comment text, files_remote_location text, files_remote_id text, files_people_name text, files_url text, files_name text, files_upload_status integer );";
    private static final String DATABASE_CREATE_FILTERS_TABLE = "create table filters(_id integer primary key autoincrement, filters_pipedrive_id integer, filters_name text not null, filters_is_active integer, filters_type text, filters_is_private integer );";
    private static final String DATABASE_CREATE_FILTER_DEALS_TABLE = "create table filter_deals(_id integer primary key autoincrement, filter_deals_pipedrive_id integer, filter_deals_filter_pipedrive_id integer,  UNIQUE(filter_deals_pipedrive_id, filter_deals_filter_pipedrive_id));";
    private static final String DATABASE_CREATE_NOTES_TABLE = "create table notes(_id integer primary key autoincrement, n_pd_id integer, n_deal_sql_id integer, n_person_sql_id integer, n_org_sql_id integer, n_is_active integer, n_add_time integer, n_content text );";
    private static final String DATABASE_CREATE_ORG_TABLE = "create table organizations(_id integer primary key autoincrement, id integer, name text, phone text, address text, email text, visible_to integer, owner_id integer, other_fields blob, org_dropbox_address text, org_is_active integer, org_name_search_field text, org_owner_name text, org_address_latitude real, org_address_longitude real, org_is_shadow integer );";
    private static final String DATABASE_CREATE_PERSONS_TABLE = "create table persons(_id integer primary key autoincrement, c_org_id_sql integer, c_pd_id integer, c_name text, c_phones text, c_emails text, c_visible_to integer, c_owner_id integer, c_other_fields blob, c_dropbox_address text, c_is_active integer, c_name_search_field text, c_owner_name text, c_ims text, c_postal_address text, c_pic_id integer,c_phones_search_field text,c_is_shadow integer );";
    private static final String DATABASE_CREATE_PIPELINES_TABLE = "create table pipelines(_id integer primary key autoincrement, id integer, name text not null, order_nr integer, active integer, pipe_default integer, selected integer,  UNIQUE(id));";
    private static final String DATABASE_CREATE_PIPELINE_STAGES_TABLE = "create table pipeline_stages(_id integer primary key autoincrement, id integer, d_pipeline_id integer, name text not null, order_nr integer, deal_probability integer,  UNIQUE(id));";
    private static final String DATABASE_CREATE_PRODUCTS_TABLE = "create table products(_id integer primary key autoincrement, products_pipedrive_id integer, products_name text, products_is_active integer, products_is_removed integer);";
    private static final String DATABASE_CREATE_PRODUCT_PRICES_TABLE = "create table product_prices(_id integer primary key autoincrement, product_prices_pipedrive_id integer, product_prices_product_sqlId integer, product_prices_price real, product_prices_currency_sql_id integer);";
    private static final String DATABASE_CREATE_PRODUCT_VARIATIONS_TABLE = "create table product_variations(_id integer primary key autoincrement, product_variations_pipedrive_id integer, product_variations_product_sql_id integer,product_variations_name text);";
    private static final String DATABASE_CREATE_PRODUCT_VARIATION_PRICES_TABLE = "create table product_variation_prices(_id integer primary key autoincrement, product_variation_prices_pipedrive_id integer, product_variation_prices_product_variation_sql_id integer, product_variation_prices_price real, product_variation_prices_currency_sql_id integer);";
    private static final String DATABASE_CREATE_TABLE_CHANGES_JOURNAL = "create table changes_journal(_id integer primary key autoincrement, cj_op_type integer, cj_op integer, cj_op_metadata integer );";
    private static final String DATABASE_CREATE_USERS_TABLE = "create table users(_id integer primary key autoincrement, u_pd_id integer, u_name text, u_is_active integer, u_icon_url text );";
    private static final int DATABASE_VERSION = 100;
    public static final String TABLE_ACTIVITIES = "activities";
    public static final String TABLE_CHANGES_JOURNAL = "changes_journal";
    public static final String TABLE_CURRENCIES = "currencies";
    public static final String TABLE_CUSTOM_FIELDS = "custom_fields";
    public static final String TABLE_DEALS = "deals";
    public static final String TABLE_DEAL_PRODUCTS = "deal_products";
    public static final String TABLE_EMAIL_ATTACHMENTS = "email_attachments";
    public static final String TABLE_EMAIL_MESSAGES = "email_messages";
    public static final String TABLE_EMAIL_PARTICIPANTS = "email_participants";
    public static final String TABLE_EMAIL_PARTICIPANTS_TO_MESSAGES = "email_participants_to_messages";
    public static final String TABLE_EMAIL_THREADS = "email_threads";
    public static final String TABLE_EMAIL_THREADS_TO_ORGS = "email_threads_to_orgs";
    public static final String TABLE_FILES = "files";
    public static final String TABLE_FILTERS = "filters";
    public static final String TABLE_FILTER_DEALS = "filter_deals";
    public static final String TABLE_NOTES = "notes";
    public static final String TABLE_ORG = "organizations";
    public static final String TABLE_PERSONS = "persons";
    public static final String TABLE_PIPELINES = "pipelines";
    public static final String TABLE_PIPELINE_STAGES = "pipeline_stages";
    public static final String TABLE_PRODUCTS = "products";
    public static final String TABLE_PRODUCT_PRICES = "product_prices";
    public static final String TABLE_PRODUCT_VARIATIONS = "product_variations";
    public static final String TABLE_PRODUCT_VARIATION_PRICES = "product_variation_prices";
    public static final String TABLE_USERS = "users";
    private static final String TAG = PipeSQLiteHelper.class.getSimpleName();
    private final String[] ACTIVE_TABLE_SCRIPTS = new String[]{"persons", DATABASE_CREATE_PERSONS_TABLE, "deals", DATABASE_CREATE_DEALS_TABLE, "organizations", DATABASE_CREATE_ORG_TABLE, TABLE_PIPELINE_STAGES, DATABASE_CREATE_PIPELINE_STAGES_TABLE, "pipelines", DATABASE_CREATE_PIPELINES_TABLE, "filters", DATABASE_CREATE_FILTERS_TABLE, "users", DATABASE_CREATE_USERS_TABLE, "activities", DATABASE_CREATE_ACTIVITIES_TABLE, "currencies", DATABASE_CREATE_CURRENCIES_TABLE, TABLE_FILTER_DEALS, DATABASE_CREATE_FILTER_DEALS_TABLE, "notes", DATABASE_CREATE_NOTES_TABLE, TABLE_CUSTOM_FIELDS, DATABASE_CREATE_CUSTOM_FIELDS_TABLE, TABLE_CHANGES_JOURNAL, DATABASE_CREATE_TABLE_CHANGES_JOURNAL, "files", DATABASE_CREATE_FILES_TABLE, TABLE_EMAIL_MESSAGES, DATABASE_CREATE_EMAIL_MESSAGES_TABLE, TABLE_EMAIL_PARTICIPANTS, DATABASE_CREATE_EMAIL_PARTICIPANTS_TABLE, TABLE_EMAIL_PARTICIPANTS_TO_MESSAGES, DATABASE_CREATE_EMAIL_PARTICIPANTS_TO_MESSAGES_TABLE, TABLE_EMAIL_THREADS, DATABASE_CREATE_EMAIL_THREADS_TABLE, TABLE_EMAIL_THREADS_TO_ORGS, DATABASE_CREATE_EMAIL_THREADS_TO_ORGS_TABLE, TABLE_EMAIL_ATTACHMENTS, DATABASE_CREATE_EMAIL_ATTACHMENTS_TABLE, "products", DATABASE_CREATE_PRODUCTS_TABLE, TABLE_PRODUCT_PRICES, DATABASE_CREATE_PRODUCT_PRICES_TABLE, TABLE_DEAL_PRODUCTS, DATABASE_CREATE_DEAL_PRODUCTS_TABLE, TABLE_PRODUCT_VARIATIONS, DATABASE_CREATE_PRODUCT_VARIATIONS_TABLE, TABLE_PRODUCT_VARIATION_PRICES, DATABASE_CREATE_PRODUCT_VARIATION_PRICES_TABLE};
    @Nullable
    private Session mSession = null;

    public PipeSQLiteHelper(@NonNull Session session) {
        super(session.getApplicationContext(), session.getSessionID(), 100);
        this.mSession = session;
        Log.d(TAG, "SQL connection initialized to database: " + session.getSessionID());
    }

    public void onCreate(@NonNull SQLiteDatabase database) {
        createTables(database);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(PipeSQLiteHelper.class.getName(), "Downgrading database from version " + oldVersion + " to " + newVersion + ".");
        boolean recreateTables = false;
        if (newVersion < 100) {
            recreateTables = true;
        }
        if (recreateTables) {
            clearDatabase(db);
        }
    }

    public void onUpgrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(PipeSQLiteHelper.class.getName(), "Upgrading database from version " + oldVersion + " to " + newVersion + ".");
        boolean recreateTables = false;
        if (oldVersion < 100) {
            recreateTables = true;
        }
        if (recreateTables) {
            clearDatabase(db);
        }
    }

    public void clearDatabase(@NonNull SQLiteDatabase db) {
        String tag = TAG + ".deleteTablesAndLogout()";
        try {
            String str = "SELECT _id FROM changes_journal";
            Cursor cursor = !(db instanceof SQLiteDatabase) ? db.rawQuery(str, null) : SQLiteInstrumentation.rawQuery(db, str, null);
            if (cursor != null && cursor.getCount() > 0) {
                Log.e(new Throwable(String.format("Clearing DB with offline changes count: %s", new Object[]{Integer.valueOf(cursor.getCount())})));
                LogJourno.reportEvent(EVENT.Database_updateMadeWithOfflineChangesPresent, Integer.toString(cursor.getCount()));
                cursor.close();
            }
        } catch (Exception e) {
        }
        for (int i = 0; i < this.ACTIVE_TABLE_SCRIPTS.length; i += 2) {
            String tableName = this.ACTIVE_TABLE_SCRIPTS[i];
            Log.i(tag, "Drop table: " + tableName);
            str = "DROP TABLE IF EXISTS " + tableName;
            if (db instanceof SQLiteDatabase) {
                SQLiteInstrumentation.execSQL(db, str);
            } else {
                db.execSQL(str);
            }
        }
        createTables(db);
        if (this.mSession != null) {
            Log.i(tag, "Notifying the session that DBs where recreated!");
            this.mSession.getRuntimeCache().DBTablesRecreated = true;
        }
    }

    private void createTables(SQLiteDatabase db) {
        for (int i = 0; i < this.ACTIVE_TABLE_SCRIPTS.length; i += 2) {
            String tableCreateScript = this.ACTIVE_TABLE_SCRIPTS[i + 1];
            Log.i(TAG, "Create table: " + tableCreateScript);
            if (db instanceof SQLiteDatabase) {
                SQLiteInstrumentation.execSQL(db, tableCreateScript);
            } else {
                db.execSQL(tableCreateScript);
            }
        }
        Log.i(TAG, "Recreation done.");
    }
}
