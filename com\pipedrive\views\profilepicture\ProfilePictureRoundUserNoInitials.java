package com.pipedrive.views.profilepicture;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.pipedrive.model.User;

public class ProfilePictureRoundUserNoInitials extends ProfilePictureRound {
    public /* bridge */ /* synthetic */ void loadPicture(@Nullable String str, @Nullable String str2) {
        super.loadPicture(str, str2);
    }

    public /* bridge */ /* synthetic */ void setTextSize(float f) {
        super.setTextSize(f);
    }

    public ProfilePictureRoundUserNoInitials(Context context) {
        super(context);
    }

    public ProfilePictureRoundUserNoInitials(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProfilePictureRoundUserNoInitials(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public boolean loadPicture(@Nullable User user) {
        boolean canLoadPicture;
        if (user != null) {
            canLoadPicture = true;
        } else {
            canLoadPicture = false;
        }
        if (!canLoadPicture) {
            return false;
        }
        super.loadPicture(null, user.getIconUrl());
        return true;
    }
}
