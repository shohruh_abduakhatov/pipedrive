package com.pipedrive.util.networking.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.Date;
import java.util.List;

public class EmailMessageEntity {
    @SerializedName("active_flag")
    @Expose
    private Boolean mActive;
    @SerializedName("add_time")
    @Expose
    private Date mAddTime;
    @SerializedName("attachment_count")
    @Expose
    private Integer mAttachmentCount;
    @SerializedName("attachments")
    @Expose
    private List<EmailAttachmentEntity> mAttachments;
    @SerializedName("bcc")
    @Expose
    private List<EmailParticipantEntity> mBcc;
    @SerializedName("body")
    @Expose
    private String mBody;
    @SerializedName("cc")
    @Expose
    private List<EmailParticipantEntity> mCc;
    @SerializedName("draft_flag")
    @Expose
    private Boolean mDraft;
    @SerializedName("from")
    @Expose
    private List<EmailParticipantEntity> mFrom;
    @SerializedName("message_time")
    @Expose
    private Date mMessageTime;
    @SerializedName("id")
    @Expose
    private Integer mPipedriveId;
    @SerializedName("subject")
    @Expose
    private String mSubject;
    @SerializedName("summary")
    @Expose
    private String mSummary;
    @SerializedName("thread_id")
    @Expose
    private Integer mThreadId;
    @SerializedName("to")
    @Expose
    private List<EmailParticipantEntity> mTo;
    @SerializedName("update_time")
    @Expose
    private Date mUpdateTime;

    public Integer getAttachmentCount() {
        return this.mAttachmentCount;
    }

    public void setAttachmentCount(Integer attachmentCount) {
        this.mAttachmentCount = attachmentCount;
    }

    public List<EmailAttachmentEntity> getAttachments() {
        return this.mAttachments;
    }

    public void setAttachments(List<EmailAttachmentEntity> attachments) {
        this.mAttachments = attachments;
    }

    public Integer getPipedriveId() {
        return this.mPipedriveId;
    }

    public void setPipedriveId(Integer pipedriveId) {
        this.mPipedriveId = pipedriveId;
    }

    public Integer getThreadId() {
        return this.mThreadId;
    }

    public void setThreadId(Integer threadId) {
        this.mThreadId = threadId;
    }

    public String getSubject() {
        return this.mSubject;
    }

    public void setSubject(String subject) {
        this.mSubject = subject;
    }

    public String getSummary() {
        return this.mSummary;
    }

    public void setSummary(String summary) {
        this.mSummary = summary;
    }

    public String getBody() {
        return this.mBody;
    }

    public void setBody(String body) {
        this.mBody = body;
    }

    public Date getMessageTime() {
        return this.mMessageTime;
    }

    public void setMessageTime(Date messageTime) {
        this.mMessageTime = messageTime;
    }

    public Date getAddTime() {
        return this.mAddTime;
    }

    public void setAddTime(Date addTime) {
        this.mAddTime = addTime;
    }

    public Date getUpdateTime() {
        return this.mUpdateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.mUpdateTime = updateTime;
    }

    public Boolean getActive() {
        return this.mActive;
    }

    public void setActive(Boolean active) {
        this.mActive = active;
    }

    public Boolean getDraft() {
        return this.mDraft;
    }

    public void setDraft(Boolean draft) {
        this.mDraft = draft;
    }

    public List<EmailParticipantEntity> getFrom() {
        return this.mFrom;
    }

    public void setFrom(List<EmailParticipantEntity> from) {
        this.mFrom = from;
    }

    public List<EmailParticipantEntity> getTo() {
        return this.mTo;
    }

    public void setTo(List<EmailParticipantEntity> to) {
        this.mTo = to;
    }

    public List<EmailParticipantEntity> getCc() {
        return this.mCc;
    }

    public void setCc(List<EmailParticipantEntity> cc) {
        this.mCc = cc;
    }

    public List<EmailParticipantEntity> getBcc() {
        return this.mBcc;
    }

    public void setBcc(List<EmailParticipantEntity> bcc) {
        this.mBcc = bcc;
    }
}
