package com.pipedrive.nearby.cards;

import android.support.annotation.NonNull;
import com.pipedrive.nearby.model.NearbyItem;
import rx.Observable;

public interface CardSelectionProvider {
    @NonNull
    Observable<NearbyItem> cardSelection();
}
