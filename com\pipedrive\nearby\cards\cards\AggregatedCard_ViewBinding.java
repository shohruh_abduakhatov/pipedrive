package com.pipedrive.nearby.cards.cards;

import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class AggregatedCard_ViewBinding extends Card_ViewBinding {
    private AggregatedCard target;

    @UiThread
    public AggregatedCard_ViewBinding(AggregatedCard target, View source) {
        super(target, source);
        this.target = target;
        target.directionsButtonTitle = (TextView) Utils.findRequiredViewAsType(source, R.id.aggregatedNearbyCard.directionsButtonTitle, "field 'directionsButtonTitle'", TextView.class);
        target.recyclerView = (RecyclerView) Utils.findRequiredViewAsType(source, R.id.aggregatedRecyclerView, "field 'recyclerView'", RecyclerView.class);
        target.titleTextView = (TextView) Utils.findRequiredViewAsType(source, R.id.nearbyCard.title, "field 'titleTextView'", TextView.class);
        target.subtitleTextView = (TextView) Utils.findRequiredViewAsType(source, R.id.nearbyCard.subtitle, "field 'subtitleTextView'", TextView.class);
    }

    public void unbind() {
        AggregatedCard target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.directionsButtonTitle = null;
        target.recyclerView = null;
        target.titleTextView = null;
        target.subtitleTextView = null;
        super.unbind();
    }
}
