package com.pipedrive.views.filter;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.pipedrive.R;
import com.pipedrive.views.CheckableFrameLayout;

public class CheckableFilterRow extends CheckableFrameLayout {
    public CheckableFilterRow(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setChecked(boolean checked) {
        super.setChecked(checked);
        ((ImageView) findViewById(R.id.checkbox)).setVisibility(checked ? 0 : 4);
    }
}
