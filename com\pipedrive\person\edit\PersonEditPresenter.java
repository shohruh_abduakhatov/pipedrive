package com.pipedrive.person.edit;

import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.contact.CommunicationMedium;
import com.pipedrive.presenter.PersistablePresenter;

@MainThread
abstract class PersonEditPresenter extends PersistablePresenter<PersonEditView> {
    abstract void addEmail();

    abstract void addPhone();

    abstract void associateOrganization(@Nullable Organization organization);

    abstract void associateOrganizationBySqlId(@NonNull Long l);

    abstract void changePerson();

    abstract void deletePerson();

    @Nullable
    abstract Person getPerson();

    abstract boolean personWasModified();

    abstract void removeEmail(@NonNull CommunicationMedium communicationMedium);

    abstract void removePhone(@NonNull CommunicationMedium communicationMedium);

    abstract void requestOrRestoreNewPerson();

    abstract void requestOrRestoreNewPersonWithName(@Nullable String str);

    abstract void requestOrRestorePerson(@NonNull Long l);

    public PersonEditPresenter(@NonNull Session session, @Nullable Bundle savedState) {
        super(session, savedState);
    }
}
