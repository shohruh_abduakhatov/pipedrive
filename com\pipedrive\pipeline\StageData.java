package com.pipedrive.pipeline;

import android.database.Cursor;
import android.support.annotation.Nullable;

class StageData {
    @Nullable
    private Float mDealSumValue;
    @Nullable
    private Cursor mDealsCursor;
    @Nullable
    private Integer mNumberOfDealsCountedForSumValue;

    StageData(@Nullable Float dealSumValue, @Nullable Integer numberOfDealsCountedForSumValue, @Nullable Cursor dealsCursor) {
        this.mDealSumValue = dealSumValue;
        this.mNumberOfDealsCountedForSumValue = numberOfDealsCountedForSumValue;
        this.mDealsCursor = dealsCursor;
    }

    @Nullable
    Float getDealSumValue() {
        return this.mDealSumValue;
    }

    @Nullable
    Integer getNumberOfDealsCountedForSumValue() {
        return this.mNumberOfDealsCountedForSumValue;
    }

    @Nullable
    Cursor getDealsCursor() {
        return this.mDealsCursor;
    }
}
