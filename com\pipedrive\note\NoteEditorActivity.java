package com.pipedrive.note;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.contentproviders.FlowContentProvider;
import com.pipedrive.model.notes.Note;
import com.pipedrive.navigator.buttons.DeleteMenuItem;
import com.pipedrive.navigator.buttons.DiscardMenuItem;
import com.pipedrive.navigator.buttons.NavigatorMenuItem;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.util.snackbar.SnackBarManager;
import java.util.Arrays;
import java.util.List;

abstract class NoteEditorActivity extends HtmlEditorActivity<NoteEditorPresenter> implements NoteEditorView {
    private final DeleteMenuItem mDeleteMenuItem = new DeleteMenuItem() {
        public boolean isEnabled() {
            return ((NoteEditorPresenter) NoteEditorActivity.this.mPresenter).getData() != null && ((Note) ((NoteEditorPresenter) NoteEditorActivity.this.mPresenter).getData()).isStored() && NoteEditorActivity.this.getSession().canUserDeleteNote();
        }

        public void onClick() {
            NoteEditorActivity.this.onDelete();
        }
    };
    private final DiscardMenuItem mDiscardMenuItem = new DiscardMenuItem() {
        public void onClick() {
            NoteEditorActivity.this.onCancel();
        }
    };

    protected abstract boolean requestNote(@Nullable Bundle bundle);

    NoteEditorActivity() {
    }

    public void onResume() {
        super.onResume();
        Note note = (Note) ((NoteEditorPresenter) this.mPresenter).getData();
        if (note != null) {
            onDataRequested(note);
        } else if (!requestNote(getIntent().getExtras())) {
            finish();
        }
    }

    public void onCreateOrUpdate() {
        if (((NoteEditorPresenter) this.mPresenter).getData() == null) {
            onCancel();
            return;
        }
        ((Note) ((NoteEditorPresenter) this.mPresenter).getData()).setHtmlContent(getNoteContentAsHtml());
        ((NoteEditorPresenter) this.mPresenter).changeNote();
    }

    public void createOrUpdateDiscardedAsRequiredFieldsAreNotSet() {
        onCancel();
    }

    private void onDelete() {
        ViewUtil.showDeleteConfirmationDialog(this, getResources().getString(R.string.dialog_delete_note), new Runnable() {
            public void run() {
                ((NoteEditorPresenter) NoteEditorActivity.this.mPresenter).deleteNote();
            }
        });
    }

    public void onCancel() {
        finish();
    }

    public void onNoteCreated(boolean success) {
        processNoteCrudResponse(success);
        if (success) {
            SnackBarManager.INSTANCE.setDelayedSnackBar(R.string.note_added);
        }
    }

    public void onNoteUpdated(boolean success) {
        processNoteCrudResponse(success);
        if (success) {
            SnackBarManager.INSTANCE.setDelayedSnackBar(R.string.note_updated);
        }
    }

    public void onNoteDeleted(boolean success) {
        processNoteCrudResponse(success);
        if (success) {
            SnackBarManager.INSTANCE.setDelayedSnackBar(R.string.note_deleted);
        }
    }

    private void processNoteCrudResponse(boolean success) {
        if (success) {
            FlowContentProvider.notifyChangesMadeInFlow(this);
            finish();
            return;
        }
        ViewUtil.showErrorToast(this, R.string.error_note_save_failed);
    }

    protected NoteEditorPresenter instantiatePresenter(@NonNull Session session, @Nullable Bundle savedState) {
        return new NoteEditorPresenterImpl(session, savedState);
    }

    @Nullable
    public List<? extends NavigatorMenuItem> getAdditionalButtons() {
        return Arrays.asList(new NavigatorMenuItem[]{this.mDeleteMenuItem, this.mDiscardMenuItem});
    }
}
