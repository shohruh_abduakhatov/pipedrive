package com.pipedrive.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.pipedrive.PersonDetailActivity;
import com.pipedrive.R;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.contentproviders.PersonsContentProvider;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.util.ViewUtil;

public class OrganizationPeopleListFragment extends BaseFragment implements LoaderCallbacks<Cursor> {
    private static final String KEY_ORG_SQL_ID = "ORG_SQL_ID";
    private static final String KEY_SHOULD_ADD_FOOTER_FOR_FAB = "SHOULD_ADD_FOOTER_FOR_FAB";
    static final String TAG = OrganizationPeopleListFragment.class.getSimpleName();
    private SimpleCursorAdapter adapter;
    private ListView orgPersonList;
    private long orgSqlId;

    public static OrganizationPeopleListFragment newInstance(long sqlId, boolean shouldAddFooterForFab) {
        Bundle args = new Bundle();
        args.putLong("ORG_SQL_ID", sqlId);
        args.putBoolean(KEY_SHOULD_ADD_FOOTER_FOR_FAB, shouldAddFooterForFab);
        OrganizationPeopleListFragment fragment = new OrganizationPeopleListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.orgSqlId = getArguments().getLong("ORG_SQL_ID");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_organization_people, container, false);
        if (getArguments() != null && getArguments().containsKey("ORG_SQL_ID")) {
            this.orgSqlId = getArguments().getLong("ORG_SQL_ID");
        }
        if (savedInstanceState != null && savedInstanceState.containsKey("ORG_SQL_ID")) {
            this.orgSqlId = savedInstanceState.getLong("ORG_SQL_ID");
        }
        this.orgPersonList = (ListView) mainView.findViewById(R.id.orgPersonList);
        this.orgPersonList.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (id > 0) {
                    PersonDetailActivity.startActivity(OrganizationPeopleListFragment.this.getActivity(), Long.valueOf(id));
                }
            }
        });
        if (getArguments().getBoolean(KEY_SHOULD_ADD_FOOTER_FOR_FAB, false)) {
            this.orgPersonList.addFooterView(ViewUtil.getColoredFooterForListWithFloatingButton(getContext(), R.color.frost));
        }
        View layoutEmpty = mainView.findViewById(16908292);
        this.orgPersonList.setEmptyView(layoutEmpty);
        TextView labelEmptyTextExplanation = (TextView) layoutEmpty.findViewById(R.id.labelEmptyTextExplanation);
        ((TextView) layoutEmpty.findViewById(R.id.labelEmptyTextHeader)).setText(getString(R.string.label_empty_header_organization_people));
        labelEmptyTextExplanation.setHeight(0);
        fillData();
        return mainView;
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putLong("ORG_SQL_ID", this.orgSqlId);
        super.onSaveInstanceState(outState);
    }

    private void fillData() {
        String[] from = new String[]{"c_name"};
        int[] to = new int[]{R.id.labelPersonName};
        getLoaderManager().initLoader(1, null, this);
        this.adapter = new SimpleCursorAdapter(getActivity(), R.layout.person_row_minimal, null, from, to, 0) {
            public long getItemId(int position) {
                Session activeSession = PipedriveApp.getActiveSession();
                if (activeSession == null) {
                    return super.getItemId(position);
                }
                Cursor cursor = getCursor();
                if (cursor == null || cursor.isClosed()) {
                    return super.getItemId(position);
                }
                cursor.moveToPosition(position);
                return new PersonsDataSource(activeSession.getDatabase()).deflateCursor(cursor).getSqlId();
            }
        };
        this.orgPersonList.setAdapter(this.adapter);
    }

    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        CursorLoader organizationPersonsCursorLoader = new CursorLoader(getActivity());
        organizationPersonsCursorLoader.setUri(new PersonsContentProvider(PipedriveApp.getActiveSession()).createOrganizationPersonsUri(this.orgSqlId));
        return organizationPersonsCursorLoader;
    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        this.adapter.swapCursor(data);
    }

    public void onLoaderReset(Loader<Cursor> loader) {
        this.adapter.swapCursor(null);
    }
}
