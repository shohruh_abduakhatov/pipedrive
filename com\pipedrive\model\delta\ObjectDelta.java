package com.pipedrive.model.delta;

import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.BaseDataSource;
import com.pipedrive.logging.Log;
import com.pipedrive.model.BaseDatasourceEntity;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class ObjectDelta<HOST extends BaseDatasourceEntity> {
    private final Constructor<? extends BaseDataSource<? extends BaseDatasourceEntity>> mDataSourceConstructor;
    private final Class<HOST> mHostType;
    private final List<ChangeSetItem<HOST, ?>> mProperties;

    public ObjectDelta(Class<HOST> hostType) {
        this.mHostType = hostType;
        this.mDataSourceConstructor = getDataSourceConstructor(hostType);
        this.mProperties = getProperties(hostType, hostType);
    }

    private List<ChangeSetItem<HOST, ?>> getProperties(Class<? super HOST> fieldsSource, Class<HOST> hostType) {
        List<ChangeSetItem<HOST, ?>> propertyList = new ArrayList();
        for (Field field : fieldsSource.getDeclaredFields()) {
            ModelProperty modelProperty = (ModelProperty) field.getAnnotation(ModelProperty.class);
            if (modelProperty != null) {
                Class<?> valueType = modelProperty.dataType().equals(NONE.class) ? field.getType() : modelProperty.dataType();
                String jsonParamName = TextUtils.isEmpty(modelProperty.jsonParamName()) ? field.getName() : modelProperty.jsonParamName();
                if (!(valueType == null || TextUtils.isEmpty(jsonParamName))) {
                    propertyList.add(ChangeSetItem.of(hostType, valueType, field.getName(), jsonParamName));
                }
            }
        }
        Class<? super HOST> superClass = fieldsSource.getSuperclass();
        if ((superClass != null) && BaseDatasourceEntity.class.isAssignableFrom(superClass)) {
            List<ChangeSetItem<HOST, ?>> propertiesFromSuperClass = getProperties(superClass, hostType);
            if (propertiesFromSuperClass != null) {
                propertyList.addAll(propertiesFromSuperClass);
            }
        }
        return propertyList.isEmpty() ? null : propertyList;
    }

    private Constructor<? extends BaseDataSource<? extends BaseDatasourceEntity>> getDataSourceConstructor(Class<HOST> hostType) {
        Constructor<? extends BaseDataSource<? extends BaseDatasourceEntity>> constructor = null;
        Model annotation = (Model) hostType.getAnnotation(Model.class);
        if (annotation != null) {
            try {
                constructor = annotation.dataSource().getConstructor(new Class[]{SQLiteDatabase.class});
            } catch (NoSuchMethodException e) {
                Log.e(e);
            }
        }
        return constructor;
    }

    public ChangeSet getDelta(@NonNull Session session, HOST item) {
        if (item == null || session.getDatabase() == null || !session.isValid() || !item.getClass().equals(this.mHostType)) {
            return null;
        }
        BaseDataSource<HOST> dataSource = getDataSource(session.getDatabase());
        if (dataSource != null) {
            return getChangedProperties(item, dataSource.findBySqlId(item.getSqlId()));
        }
        return null;
    }

    private ChangeSet<HOST> getChangedProperties(@NonNull HOST item, @Nullable HOST originalItem) {
        List<ChangeSetItem<HOST, ?>> changedProperties = new ArrayList();
        for (ChangeSetItem<HOST, ?> property : this.mProperties) {
            if (!areEqual(property.get(item), property.get(originalItem))) {
                changedProperties.add(property);
            }
        }
        return new ChangeSet(changedProperties);
    }

    private BaseDataSource<HOST> getDataSource(SQLiteDatabase database) {
        if (this.mDataSourceConstructor == null) {
            return null;
        }
        BaseDataSource<HOST> dataSource = null;
        try {
            return (BaseDataSource) this.mDataSourceConstructor.newInstance(new Object[]{database});
        } catch (InstantiationException e) {
            Log.e(e);
            return dataSource;
        } catch (IllegalAccessException e2) {
            Log.e(e2);
            return dataSource;
        } catch (InvocationTargetException e3) {
            Log.e(e3);
            return dataSource;
        }
    }

    public boolean hasChanges(@NonNull Session session, @NonNull HOST item) {
        if (session.getDatabase() == null || !session.isValid() || !item.getClass().equals(this.mHostType)) {
            return false;
        }
        BaseDataSource<HOST> dataSource = getDataSource(session.getDatabase());
        if (dataSource != null) {
            return hasChanges((BaseDatasourceEntity) item, dataSource.findBySqlId(item.getSqlId()));
        }
        return false;
    }

    public boolean hasChanges(@NonNull HOST item, HOST originalItem) {
        for (ChangeSetItem<HOST, ?> property : this.mProperties) {
            if (!areEqual(property.get(item), property.get(originalItem))) {
                return true;
            }
        }
        return false;
    }

    public static boolean areEqual(Object a, Object b) {
        if (a == null) {
            return b == null;
        } else {
            return a.equals(b);
        }
    }
}
