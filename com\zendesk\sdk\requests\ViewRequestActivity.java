package com.zendesk.sdk.requests;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.sdk.ui.NetworkAwareActionbarActivity;
import com.zendesk.sdk.util.UiUtils;
import com.zendesk.util.StringUtils;

public class ViewRequestActivity extends NetworkAwareActionbarActivity {
    public static final String EXTRA_REQUEST_ID = "requestId";
    public static final String EXTRA_SUBJECT = "subject";
    private static final String FRAGMENT_TAG = ViewRequestActivity.class.getSimpleName();
    private static final String LOG_TAG = "ViewRequestActivity";
    private ViewRequestFragment mViewRequestFragment;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UiUtils.setThemeIfAttributesAreMissing(this, R.attr.viewRequestAttachmentIcon, R.attr.viewRequestSendIcon);
        setContentView(R.layout.activity_view_request);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        Bundle extras = getIntent().getExtras();
        if (extras == null || !extras.containsKey("requestId")) {
            abort();
            return;
        }
        String requestId = extras.getString("requestId");
        String subject = extras.getString("subject");
        if (StringUtils.hasLength(requestId)) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment fragment = fragmentManager.findFragmentByTag(FRAGMENT_TAG);
            if (fragment == null || !(fragment instanceof ViewRequestFragment)) {
                this.mViewRequestFragment = ViewRequestFragment.newInstance(requestId, subject);
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(R.id.activity_request_view_root, this.mViewRequestFragment, FRAGMENT_TAG);
                fragmentTransaction.commit();
                return;
            }
            this.mViewRequestFragment = (ViewRequestFragment) fragment;
            return;
        }
        abort();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != 16908332) {
            return false;
        }
        onBackPressed();
        return true;
    }

    public void onNetworkAvailable() {
        super.onNetworkAvailable();
        if (this.mViewRequestFragment != null) {
            this.mViewRequestFragment.onNetworkAvailable();
        }
    }

    public void onNetworkUnavailable() {
        super.onNetworkUnavailable();
        if (this.mViewRequestFragment != null) {
            this.mViewRequestFragment.onNetworkUnavailable();
        }
    }

    private void abort() {
        Logger.w(LOG_TAG, "Insufficient data to start activity", new Object[0]);
        finish();
    }
}
