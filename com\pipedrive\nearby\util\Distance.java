package com.pipedrive.nearby.util;

import android.support.annotation.NonNull;

public enum Distance {
    MILES {
        @NonNull
        public Double toMeters(@NonNull Double value) {
            return Double.valueOf(value.doubleValue() * Distance.MILE_IN_METERS.doubleValue());
        }

        @NonNull
        public Double toKilometers(@NonNull Double value) {
            return Double.valueOf(toMeters(value).doubleValue() * Distance.KILOMETER_IN_METERS.doubleValue());
        }

        @NonNull
        public Double toMiles(@NonNull Double value) {
            return value;
        }
    },
    KILOMETERS {
        @NonNull
        public Double toMeters(@NonNull Double value) {
            return Double.valueOf(value.doubleValue() * Distance.KILOMETER_IN_METERS.doubleValue());
        }

        @NonNull
        public Double toKilometers(@NonNull Double value) {
            return value;
        }

        @NonNull
        public Double toMiles(@NonNull Double value) {
            return Double.valueOf(toMeters(value).doubleValue() / Distance.MILE_IN_METERS.doubleValue());
        }
    },
    METERS {
        @NonNull
        public Double toMeters(@NonNull Double value) {
            return value;
        }

        @NonNull
        public Double toKilometers(@NonNull Double value) {
            return Double.valueOf(value.doubleValue() / Distance.KILOMETER_IN_METERS.doubleValue());
        }

        @NonNull
        public Double toMiles(@NonNull Double value) {
            return Double.valueOf(value.doubleValue() / Distance.MILE_IN_METERS.doubleValue());
        }
    };
    
    private static final Double KILOMETER_IN_METERS = null;
    private static final Double MILE_IN_METERS = null;

    static {
        MILE_IN_METERS = Double.valueOf(1609.344d);
        KILOMETER_IN_METERS = Double.valueOf(1000.0d);
    }

    @NonNull
    public Double toMeters(@NonNull Double value) {
        throw new AbstractMethodError();
    }

    @NonNull
    public Double toKilometers(@NonNull Double value) {
        throw new AbstractMethodError();
    }

    @NonNull
    public Double toMiles(@NonNull Double value) {
        throw new AbstractMethodError();
    }
}
