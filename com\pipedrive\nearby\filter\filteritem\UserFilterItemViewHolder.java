package com.pipedrive.nearby.filter.filteritem;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewStub;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.UsersDataSource;
import com.pipedrive.model.User;
import com.pipedrive.nearby.filter.FilterItem;
import com.pipedrive.views.profilepicture.ProfilePictureRoundUser;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

class UserFilterItemViewHolder extends FilterItemViewHolder {
    @BindView(2131821090)
    ViewStub profilePictureViewStub;
    @Nullable
    private Subscription subscription;

    UserFilterItemViewHolder(View itemView) {
        super(itemView);
    }

    void bind(@NonNull FilterItem entity, @NonNull Session session) {
        super.bind(entity, session);
        if (entity.getItemType() == 3 && entity.getSqlId().longValue() != -1) {
            this.subscription = new UsersDataSource(session.getDatabase()).findBySqlIdRx(entity.getSqlId()).toObservable().subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<User>() {
                public void call(User user) {
                    UserFilterItemViewHolder.this.loadUserPicture(user);
                }
            }, new Action1<Throwable>() {
                public void call(Throwable throwable) {
                }
            });
        }
    }

    private void loadUserPicture(@NonNull User user) {
        ProfilePictureRoundUser profilePictureRoundUser = getInflatedPicture();
        if (profilePictureRoundUser != null) {
            profilePictureRoundUser.loadPicture(user);
        }
    }

    @Nullable
    private ProfilePictureRoundUser getInflatedPicture() {
        if (getProfilePictureRoundUser() == null) {
            this.profilePictureViewStub.inflate();
        }
        return getProfilePictureRoundUser();
    }

    @Nullable
    private ProfilePictureRoundUser getProfilePictureRoundUser() {
        return (ProfilePictureRoundUser) ButterKnife.findById(this.itemView, (int) R.id.profilePicture);
    }

    public void unbind() {
        if (this.subscription != null && !this.subscription.isUnsubscribed()) {
            this.subscription.unsubscribe();
        }
    }
}
