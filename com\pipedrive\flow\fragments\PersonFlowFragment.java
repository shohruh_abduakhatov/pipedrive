package com.pipedrive.flow.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.pipedrive.application.Session;
import com.pipedrive.flow.views.FlowView;
import com.pipedrive.flow.views.PersonFlowView;

public final class PersonFlowFragment extends FlowFragment {
    private static final String KEY_PERSON_SQL_ID = "PERSON_SQL_ID";

    public /* bridge */ /* synthetic */ void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    public /* bridge */ /* synthetic */ View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return super.onCreateView(layoutInflater, viewGroup, bundle);
    }

    public /* bridge */ /* synthetic */ void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
    }

    public /* bridge */ /* synthetic */ void refreshContent(@NonNull Session session) {
        super.refreshContent(session);
    }

    public static PersonFlowFragment newInstance(@NonNull Long personSqlId) {
        Bundle args = new Bundle();
        args.putLong("PERSON_SQL_ID", personSqlId.longValue());
        PersonFlowFragment fragment = new PersonFlowFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    protected Long getSqlId() {
        return (Long) getArguments().getSerializable("PERSON_SQL_ID");
    }

    @NonNull
    protected FlowView createFlowView(@NonNull Context context) {
        return new PersonFlowView(context);
    }
}
