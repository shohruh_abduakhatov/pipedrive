package com.zendesk.sdk.model.request;

import android.support.annotation.NonNull;
import com.google.gson.annotations.SerializedName;
import com.zendesk.util.CollectionUtils;
import java.util.List;

public class EndUserComment {
    @SerializedName("uploads")
    private List<String> attachments;
    private String value;

    public void setValue(String value) {
        this.value = value;
    }

    @NonNull
    public List<String> getAttachments() {
        return CollectionUtils.copyOf(this.attachments);
    }

    public void setAttachments(List<String> attachments) {
        this.attachments = attachments;
    }
}
