package com.pipedrive.nearby.filter.filterlist;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.filter.FilterItemList;
import com.pipedrive.nearby.filter.filteritem.NearbyFilterItemAdapter;

class NearbyFilterListViewHolder extends ViewHolder {
    @BindView(2131820976)
    TextView label;
    @NonNull
    private final LinearLayoutManager layout;
    @BindView(2131820977)
    RecyclerView recyclerView;

    NearbyFilterListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind((Object) this, itemView);
        this.layout = new LinearLayoutManager(itemView.getContext(), 1, false);
        this.recyclerView.setLayoutManager(this.layout);
    }

    public void bind(@NonNull FilterItemList list, @NonNull Session session) {
        this.label.setText(list.getLabelResId());
        if (this.recyclerView.getAdapter() == null) {
            NearbyFilterItemAdapter adapter = new NearbyFilterItemAdapter(list, session, list.getSelectedItemPipedriveId());
            this.layout.setInitialPrefetchItemCount(list.getSize());
            this.recyclerView.setAdapter(adapter);
        }
    }
}
