package com.zendesk.sdk.network.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.zendesk.sdk.model.helpcenter.help.ArticleItem;
import com.zendesk.sdk.model.helpcenter.help.CategoryItem;
import com.zendesk.sdk.model.helpcenter.help.HelpItem;
import com.zendesk.sdk.model.helpcenter.help.HelpResponse;
import com.zendesk.sdk.model.helpcenter.help.SectionItem;
import com.zendesk.sdk.model.helpcenter.help.SeeAllArticlesItem;
import com.zendesk.util.CollectionUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class HelpResponseConverter {
    private HelpResponse helpResponse;

    public HelpResponseConverter(HelpResponse helpResponse) {
        this.helpResponse = helpResponse;
    }

    @NonNull
    List<HelpItem> convert() {
        if (this.helpResponse == null) {
            return Collections.emptyList();
        }
        Map<Long, List<HelpItem>> sectionArticlesMap = getSectionArticlesMap(this.helpResponse);
        Map<Long, List<SectionItem>> categorySectionMap = getCategorySectionsMap(this.helpResponse);
        if (CollectionUtils.isNotEmpty(this.helpResponse.getCategories()) && CollectionUtils.isNotEmpty(this.helpResponse.getSections())) {
            return convertByCategories(categorySectionMap, sectionArticlesMap);
        }
        if (CollectionUtils.isNotEmpty(this.helpResponse.getSections())) {
            return convertBySections(sectionArticlesMap);
        }
        return convertByArticles(this.helpResponse.getArticles());
    }

    @NonNull
    private List<HelpItem> convertByCategories(@NonNull Map<Long, List<SectionItem>> categorySectionMap, @NonNull Map<Long, List<HelpItem>> sectionArticlesMap) {
        List<CategoryItem> categories = CollectionUtils.ensureEmpty(this.helpResponse.getCategories());
        List<HelpItem> helpItems = new ArrayList((sectionArticlesMap.size() + categorySectionMap.size()) + categories.size());
        for (CategoryItem category : categories) {
            List<SectionItem> sections = CollectionUtils.ensureEmpty((List) categorySectionMap.get(category.getId()));
            category.setSections(sections);
            helpItems.add(category);
            for (SectionItem section : sections) {
                helpItems.addAll(convertSection(section, (List) sectionArticlesMap.get(section.getId())));
            }
        }
        return helpItems;
    }

    @NonNull
    private List<HelpItem> convertBySections(@NonNull Map<Long, List<HelpItem>> sectionArticlesMap) {
        List<SectionItem> sections = CollectionUtils.ensureEmpty(this.helpResponse.getSections());
        List<HelpItem> helpItems = new ArrayList(sectionArticlesMap.size() + sections.size());
        for (SectionItem section : sections) {
            helpItems.addAll(convertSection(section, (List) sectionArticlesMap.get(section.getId())));
        }
        return helpItems;
    }

    @NonNull
    private List<HelpItem> convertSection(@NonNull SectionItem sectionItem, @Nullable List<HelpItem> articles) {
        List<HelpItem> sectionArticles = CollectionUtils.copyOf(articles);
        if (sectionItem.getTotalArticlesCount() > sectionArticles.size() && sectionArticles.size() > 0) {
            sectionArticles.add(new SeeAllArticlesItem(sectionItem));
        }
        sectionItem.addChildren(sectionArticles);
        List<HelpItem> helpItems = CollectionUtils.copyOf(sectionArticles);
        helpItems.add(0, sectionItem);
        return helpItems;
    }

    @NonNull
    private List<HelpItem> convertByArticles(List<ArticleItem> articles) {
        if (articles != null) {
            return new ArrayList(articles);
        }
        return Collections.emptyList();
    }

    @VisibleForTesting
    @NonNull
    Map<Long, List<HelpItem>> getSectionArticlesMap(HelpResponse helpResponse) {
        if (CollectionUtils.isEmpty(helpResponse.getSections()) || CollectionUtils.isEmpty(helpResponse.getArticles())) {
            return Collections.emptyMap();
        }
        Map<Long, List<HelpItem>> sectionArticlesMap = new HashMap(helpResponse.getSections().size());
        for (ArticleItem article : helpResponse.getArticles()) {
            Long sectionId = article.getParentId();
            List<HelpItem> articles = sectionArticlesMap.containsKey(sectionId) ? (List) sectionArticlesMap.get(sectionId) : new ArrayList();
            if (!sectionArticlesMap.containsKey(sectionId)) {
                sectionArticlesMap.put(sectionId, articles);
            }
            articles.add(article);
        }
        return sectionArticlesMap;
    }

    @VisibleForTesting
    @NonNull
    Map<Long, List<SectionItem>> getCategorySectionsMap(HelpResponse helpResponse) {
        if (CollectionUtils.isEmpty(helpResponse.getCategories()) || CollectionUtils.isEmpty(helpResponse.getSections())) {
            return Collections.emptyMap();
        }
        Map<Long, List<SectionItem>> categorySectionMap = new HashMap(helpResponse.getCategories().size());
        for (SectionItem section : helpResponse.getSections()) {
            Long categoryId = section.getParentId();
            List<SectionItem> sections = categorySectionMap.containsKey(categoryId) ? (List) categorySectionMap.get(categoryId) : new ArrayList();
            if (!categorySectionMap.containsKey(categoryId)) {
                categorySectionMap.put(categoryId, sections);
            }
            sections.add(section);
        }
        return categorySectionMap;
    }
}
