package com.pipedrive.util.recents;

class DeleteItem {
    protected int mItemPipedriveId;
    protected String mItemTypeMarker;

    protected DeleteItem(String itemTypeMarker, int itemPipedriveId) {
        this.mItemTypeMarker = itemTypeMarker;
        this.mItemPipedriveId = itemPipedriveId;
    }

    public String toString() {
        return String.format("DeleteItem[item:%s; pipedriveid:%s]", new Object[]{this.mItemTypeMarker, Integer.valueOf(this.mItemPipedriveId)});
    }
}
