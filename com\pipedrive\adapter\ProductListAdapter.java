package com.pipedrive.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.products.ProductsDataSource;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.model.Currency;
import com.pipedrive.views.viewholder.ViewHolderBuilder;
import com.pipedrive.views.viewholder.product.ProductViewHolder;

public class ProductListAdapter extends BaseSectionedListAdapter {
    @NonNull
    private final ProductsDataSource mDataSource;
    @Nullable
    private final Currency mDealCurrency;
    @NonNull
    private SearchConstraint mSearchConstraint = SearchConstraint.EMPTY;
    @NonNull
    private final Session mSession;

    public ProductListAdapter(Context context, Cursor c, @NonNull Session session, @Nullable Currency dealCurrency) {
        super(context, c, true);
        this.mDataSource = new ProductsDataSource(session.getDatabase());
        this.mDealCurrency = dealCurrency;
        this.mSession = session;
    }

    public void setSearchConstraint(@NonNull SearchConstraint searchConstraint) {
        this.mSearchConstraint = searchConstraint;
        changeCursor(this.mDataSource.getSearchCursor(searchConstraint));
    }

    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return ViewHolderBuilder.inflateViewAndTagWithViewHolder(context, parent, false, new ProductViewHolder());
    }

    public void bindView(View view, Context context, Cursor cursor) {
        ((ProductViewHolder) view.getTag()).fill(this.mDataSource.deflateCursor(cursor), this.mDealCurrency, this.mSearchConstraint, this.mSession);
    }
}
