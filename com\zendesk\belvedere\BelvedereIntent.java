package com.zendesk.belvedere;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

public class BelvedereIntent implements Parcelable {
    public static final Creator<BelvedereIntent> CREATOR = new Creator<BelvedereIntent>() {
        public BelvedereIntent createFromParcel(@NonNull Parcel in) {
            return new BelvedereIntent(in);
        }

        @NonNull
        public BelvedereIntent[] newArray(int size) {
            return new BelvedereIntent[size];
        }
    };
    private final Intent intent;
    private final int requestCode;
    private final BelvedereSource source;

    public BelvedereIntent(@NonNull Intent intent, int requestCode, @NonNull BelvedereSource source) {
        this.intent = intent;
        this.requestCode = requestCode;
        this.source = source;
    }

    @NonNull
    public BelvedereSource getSource() {
        return this.source;
    }

    @NonNull
    public Intent getIntent() {
        return this.intent;
    }

    public int getRequestCode() {
        return this.requestCode;
    }

    public void open(@NonNull Activity activity) {
        activity.startActivityForResult(this.intent, this.requestCode);
    }

    public void open(@NonNull Fragment fragment) {
        fragment.startActivityForResult(this.intent, this.requestCode);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeInt(this.requestCode);
        dest.writeParcelable(this.intent, flags);
        dest.writeSerializable(this.source);
    }

    private BelvedereIntent(Parcel in) {
        this.requestCode = in.readInt();
        this.intent = (Intent) in.readParcelable(BelvedereIntent.class.getClassLoader());
        this.source = (BelvedereSource) in.readSerializable();
    }
}
