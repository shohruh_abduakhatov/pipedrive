package com.pipedrive.more;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.pipedrive.BaseActivity;
import com.pipedrive.R;
import com.pipedrive.fragments.LanguageSelectorDialogFragment;
import com.pipedrive.fragments.LanguageSelectorDialogFragment.LanguageSelectorCallback;
import com.pipedrive.fragments.LocaleHelper;
import com.pipedrive.notification.ActivityReminderSelectorDialogFragment;
import com.pipedrive.notification.AllDayActivityReminderSelectorDialogFragment;
import com.pipedrive.notification.AllDayActivityReminderSelectorDialogFragment.Callback;

public class PreferencesActivity extends BaseActivity implements Callback, LanguageSelectorCallback, ActivityReminderSelectorDialogFragment.Callback {
    @BindView(2131820814)
    MoreButton activityReminderPreference;
    @BindView(2131820815)
    MoreButton allDayActivityReminderPreference;
    @BindView(2131820813)
    MoreButton analyticsPreference;
    @BindView(2131820812)
    MoreButton callLoggingPreference;
    @BindView(2131820811)
    MoreButton languagePreference;

    public static void startActivity(@NonNull Activity activity) {
        ContextCompat.startActivity(activity, new Intent(activity, PreferencesActivity.class), ActivityOptionsCompat.makeBasic().toBundle());
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_preferences);
        ButterKnife.bind((Activity) this);
    }

    public void onResume() {
        super.onResume();
        setupLanguagePreference();
        setupAllDayActivityReminderPreference();
        setupActivityReminderPreference();
        setupAnalyticsPreference();
        setupCallLoggingPreference();
    }

    private void setupLanguagePreference() {
        this.languagePreference.setSubtitle(LocaleHelper.INSTANCE.getLanguageTitle());
    }

    @OnClick({2131820811})
    void onLanguageClicked() {
        new LanguageSelectorDialogFragment().show(getFragmentManager(), LanguageSelectorDialogFragment.TAG);
    }

    public void onLanguageChanged() {
        recreate();
    }

    private void setupAllDayActivityReminderPreference() {
        this.allDayActivityReminderPreference.setSubtitle(getSession().getSharedSession().getAllDayActivityReminderOption().getTitle(this));
    }

    @OnClick({2131820815})
    void onAllDayActivityReminderClicked() {
        AllDayActivityReminderSelectorDialogFragment.show(getSession(), getFragmentManager());
    }

    public void onAllDayActivityReminderOptionChanged() {
        setupAllDayActivityReminderPreference();
    }

    private void setupActivityReminderPreference() {
        this.activityReminderPreference.setSubtitle(getSession().getSharedSession().getActivityReminderOption().getTitle(this));
    }

    @OnClick({2131820814})
    void onActivityReminderClicked() {
        ActivityReminderSelectorDialogFragment.show(getSession(), getFragmentManager());
    }

    public void onActivityReminderOptionChanged() {
        setupActivityReminderPreference();
    }

    private void setupAnalyticsPreference() {
        this.analyticsPreference.setChecked(getSession().getSharedSession().isAnalyticsEnabled());
        this.analyticsPreference.setOnCheckListenerChecked(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                PreferencesActivity.this.getSession().getSharedSession().enableAnalytics(isChecked);
            }
        });
    }

    private void setupCallLoggingPreference() {
        this.callLoggingPreference.setChecked(getSession().getSharedSession().isCallLoggingEnabled());
        this.callLoggingPreference.setOnCheckListenerChecked(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                PreferencesActivity.this.getSession().getSharedSession().enableCallLogging(isChecked);
            }
        });
    }
}
