package com.pipedrive.util.networking;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.webkit.MimeTypeMap;
import com.pipedrive.application.Session;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.util.flowfiles.FileHelper;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class RequestMultipart {
    public static final String ENDPOINT_FILES = "files";
    private static final String MIME_TYPE_DEFAULT = "application/octet-stream";
    @NonNull
    private final ApiUrlBuilder mApiUrlBuilder;
    @Nullable
    private String mFileNameToBeUsed;
    @NonNull
    private final Uri mLocalFileUri;
    @NonNull
    private final Map<String, String> requestNameValueEntries = new HashMap();

    @NonNull
    public static RequestMultipart uploadFileRequest(@NonNull Session session, @NonNull Uri locationOfFileOnDisc, @Nullable String fileNameToBeUsed, @Nullable Deal associateFileToThisDeal, @Nullable Person associateFileToThisPerson, @Nullable Organization associateFileToThisOrganization) {
        boolean dealIsAssociatedWithTheFlowFile;
        boolean personIsAssociatedWithTheFlowFile;
        boolean organisationIsAssociatedWithTheFlowFile = true;
        RequestMultipart requestMultipart = new RequestMultipart(new ApiUrlBuilder(session, "files"), locationOfFileOnDisc, fileNameToBeUsed);
        if (associateFileToThisDeal == null || !associateFileToThisDeal.isExisting()) {
            dealIsAssociatedWithTheFlowFile = false;
        } else {
            dealIsAssociatedWithTheFlowFile = true;
        }
        if (dealIsAssociatedWithTheFlowFile) {
            requestMultipart.addNameValueEntry("deal_id", Long.toString((long) associateFileToThisDeal.getPipedriveId()));
        }
        if (associateFileToThisPerson == null || !associateFileToThisPerson.isExisting()) {
            personIsAssociatedWithTheFlowFile = false;
        } else {
            personIsAssociatedWithTheFlowFile = true;
        }
        if (personIsAssociatedWithTheFlowFile) {
            requestMultipart.addNameValueEntry("person_id", Long.toString((long) associateFileToThisPerson.getPipedriveId()));
        }
        if (associateFileToThisOrganization == null || !associateFileToThisOrganization.isExisting()) {
            organisationIsAssociatedWithTheFlowFile = false;
        }
        if (organisationIsAssociatedWithTheFlowFile) {
            requestMultipart.addNameValueEntry("org_id", Long.toString((long) associateFileToThisOrganization.getPipedriveId()));
        }
        return requestMultipart;
    }

    private RequestMultipart(@NonNull ApiUrlBuilder apiUrlBuilder, @NonNull Uri localFileUri, @Nullable String fileNameToBeUsed) {
        this.mApiUrlBuilder = apiUrlBuilder;
        this.mLocalFileUri = localFileUri;
        this.mFileNameToBeUsed = fileNameToBeUsed;
    }

    @NonNull
    protected ApiUrlBuilder getApiUrlBuilder() {
        return this.mApiUrlBuilder;
    }

    private void addNameValueEntry(@NonNull String name, @NonNull String value) {
        this.requestNameValueEntries.put(name, value);
    }

    @Nullable
    protected File getFile() {
        return FileHelper.getFile(this.mLocalFileUri);
    }

    @NonNull
    protected String getFileNameToBeUsed(@NonNull String returnIfNotFound) {
        return this.mFileNameToBeUsed == null ? returnIfNotFound : this.mFileNameToBeUsed;
    }

    @NonNull
    protected String getMimeType() {
        String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(this.mLocalFileUri.getPath()));
        if (mimeType == null) {
            return MIME_TYPE_DEFAULT;
        }
        return mimeType;
    }

    @NonNull
    protected Map<String, String> getRequestNameValueEntries() {
        return this.requestNameValueEntries;
    }

    public String toString() {
        return "RequestMultipart{mApiUrlBuilder=" + this.mApiUrlBuilder + ", mLocalFileUri=" + this.mLocalFileUri + ", getFileNameToBeUsed()=" + getFileNameToBeUsed("") + ", requestNameValueEntries=" + Arrays.toString(this.requestNameValueEntries.values().toArray()) + '}';
    }
}
