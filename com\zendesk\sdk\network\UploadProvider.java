package com.zendesk.sdk.network;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.sdk.model.request.UploadResponse;
import com.zendesk.service.ZendeskCallback;
import java.io.File;

public interface UploadProvider {
    void deleteAttachment(@NonNull String str, @Nullable ZendeskCallback<Void> zendeskCallback);

    void uploadAttachment(@NonNull String str, @NonNull File file, @NonNull String str2, @Nullable ZendeskCallback<UploadResponse> zendeskCallback);
}
