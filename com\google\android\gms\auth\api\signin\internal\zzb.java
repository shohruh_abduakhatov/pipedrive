package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.internal.zzsa;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class zzb extends AsyncTaskLoader<Void> implements zzsa {
    private Set<GoogleApiClient> jA;
    private Semaphore jz = new Semaphore(0);

    public zzb(Context context, Set<GoogleApiClient> set) {
        super(context);
        this.jA = set;
    }

    public /* synthetic */ Object loadInBackground() {
        return zzaja();
    }

    protected void onStartLoading() {
        this.jz.drainPermits();
        forceLoad();
    }

    public Void zzaja() {
        int i = 0;
        for (GoogleApiClient zza : this.jA) {
            i = zza.zza((zzsa) this) ? i + 1 : i;
        }
        try {
            this.jz.tryAcquire(i, 5, TimeUnit.SECONDS);
        } catch (Throwable e) {
            Log.i("GACSignInLoader", "Unexpected InterruptedException", e);
            Thread.currentThread().interrupt();
        }
        return null;
    }

    public void zzajb() {
        this.jz.release();
    }
}
