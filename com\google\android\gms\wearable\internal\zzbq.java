package com.google.android.gms.wearable.internal;

import android.content.IntentFilter;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzrr;
import com.google.android.gms.internal.zzrr.zzc;
import com.google.android.gms.wearable.CapabilityApi.CapabilityListener;
import com.google.android.gms.wearable.ChannelApi.ChannelListener;
import com.google.android.gms.wearable.DataApi.DataListener;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.MessageApi.MessageListener;
import com.google.android.gms.wearable.NodeApi.NodeListener;
import com.google.android.gms.wearable.internal.zzaw.zza;
import java.util.List;

public class zzbq<T> extends zza {
    private zzrr<Object> aUC;
    private zzrr<Object> aUD;
    private zzrr<DataListener> aUE;
    private zzrr<NodeListener> aUF;
    private zzrr<Object> aUG;
    private zzrr<ChannelListener> aUH;
    private zzrr<CapabilityListener> aUI;
    private final String aUJ;
    private final IntentFilter[] aUf;
    private zzrr<MessageListener> axN;

    class AnonymousClass1 implements zzc<DataListener> {
        final /* synthetic */ DataHolder aSE;

        AnonymousClass1(DataHolder dataHolder) {
            this.aSE = dataHolder;
        }

        public void zza(DataListener dataListener) {
            try {
                dataListener.onDataChanged(new DataEventBuffer(this.aSE));
            } finally {
                this.aSE.close();
            }
        }

        public void zzasm() {
            this.aSE.close();
        }

        public /* synthetic */ void zzt(Object obj) {
            zza((DataListener) obj);
        }
    }

    class AnonymousClass2 implements zzc<MessageListener> {
        final /* synthetic */ MessageEventParcelable aSG;

        AnonymousClass2(MessageEventParcelable messageEventParcelable) {
            this.aSG = messageEventParcelable;
        }

        public void zza(MessageListener messageListener) {
            messageListener.onMessageReceived(this.aSG);
        }

        public void zzasm() {
        }

        public /* synthetic */ void zzt(Object obj) {
            zza((MessageListener) obj);
        }
    }

    class AnonymousClass3 implements zzc<NodeListener> {
        final /* synthetic */ NodeParcelable aSH;

        AnonymousClass3(NodeParcelable nodeParcelable) {
            this.aSH = nodeParcelable;
        }

        public void zza(NodeListener nodeListener) {
            nodeListener.onPeerConnected(this.aSH);
        }

        public void zzasm() {
        }

        public /* synthetic */ void zzt(Object obj) {
            zza((NodeListener) obj);
        }
    }

    class AnonymousClass4 implements zzc<NodeListener> {
        final /* synthetic */ NodeParcelable aSH;

        AnonymousClass4(NodeParcelable nodeParcelable) {
            this.aSH = nodeParcelable;
        }

        public void zza(NodeListener nodeListener) {
            nodeListener.onPeerDisconnected(this.aSH);
        }

        public void zzasm() {
        }

        public /* synthetic */ void zzt(Object obj) {
            zza((NodeListener) obj);
        }
    }

    class AnonymousClass5 implements zzc<ChannelListener> {
        final /* synthetic */ ChannelEventParcelable aSM;

        AnonymousClass5(ChannelEventParcelable channelEventParcelable) {
            this.aSM = channelEventParcelable;
        }

        public void zzasm() {
        }

        public void zzb(ChannelListener channelListener) {
            this.aSM.zza(channelListener);
        }

        public /* synthetic */ void zzt(Object obj) {
            zzb((ChannelListener) obj);
        }
    }

    class AnonymousClass6 implements zzc<CapabilityListener> {
        final /* synthetic */ CapabilityInfoParcelable aUK;

        AnonymousClass6(CapabilityInfoParcelable capabilityInfoParcelable) {
            this.aUK = capabilityInfoParcelable;
        }

        public void zza(CapabilityListener capabilityListener) {
            capabilityListener.onCapabilityChanged(this.aUK);
        }

        public void zzasm() {
        }

        public /* synthetic */ void zzt(Object obj) {
            zza((CapabilityListener) obj);
        }
    }

    private zzbq(IntentFilter[] intentFilterArr, String str) {
        this.aUf = (IntentFilter[]) zzaa.zzy(intentFilterArr);
        this.aUJ = str;
    }

    public static zzbq<ChannelListener> zza(zzrr<ChannelListener> com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_ChannelApi_ChannelListener, String str, IntentFilter[] intentFilterArr) {
        zzbq<ChannelListener> com_google_android_gms_wearable_internal_zzbq = new zzbq(intentFilterArr, (String) zzaa.zzy(str));
        com_google_android_gms_wearable_internal_zzbq.aUH = (zzrr) zzaa.zzy(com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_ChannelApi_ChannelListener);
        return com_google_android_gms_wearable_internal_zzbq;
    }

    public static zzbq<DataListener> zza(zzrr<DataListener> com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_DataApi_DataListener, IntentFilter[] intentFilterArr) {
        zzbq<DataListener> com_google_android_gms_wearable_internal_zzbq = new zzbq(intentFilterArr, null);
        com_google_android_gms_wearable_internal_zzbq.aUE = (zzrr) zzaa.zzy(com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_DataApi_DataListener);
        return com_google_android_gms_wearable_internal_zzbq;
    }

    private static zzc<CapabilityListener> zzb(CapabilityInfoParcelable capabilityInfoParcelable) {
        return new AnonymousClass6(capabilityInfoParcelable);
    }

    private static zzc<ChannelListener> zzb(ChannelEventParcelable channelEventParcelable) {
        return new AnonymousClass5(channelEventParcelable);
    }

    private static zzc<MessageListener> zzb(MessageEventParcelable messageEventParcelable) {
        return new AnonymousClass2(messageEventParcelable);
    }

    public static zzbq<MessageListener> zzb(zzrr<MessageListener> com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_MessageApi_MessageListener, IntentFilter[] intentFilterArr) {
        zzbq<MessageListener> com_google_android_gms_wearable_internal_zzbq = new zzbq(intentFilterArr, null);
        com_google_android_gms_wearable_internal_zzbq.axN = (zzrr) zzaa.zzy(com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_MessageApi_MessageListener);
        return com_google_android_gms_wearable_internal_zzbq;
    }

    private static zzc<DataListener> zzbs(DataHolder dataHolder) {
        return new AnonymousClass1(dataHolder);
    }

    private static zzc<NodeListener> zzc(NodeParcelable nodeParcelable) {
        return new AnonymousClass3(nodeParcelable);
    }

    public static zzbq<NodeListener> zzc(zzrr<NodeListener> com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_NodeApi_NodeListener, IntentFilter[] intentFilterArr) {
        zzbq<NodeListener> com_google_android_gms_wearable_internal_zzbq = new zzbq(intentFilterArr, null);
        com_google_android_gms_wearable_internal_zzbq.aUF = (zzrr) zzaa.zzy(com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_NodeApi_NodeListener);
        return com_google_android_gms_wearable_internal_zzbq;
    }

    private static zzc<NodeListener> zzd(NodeParcelable nodeParcelable) {
        return new AnonymousClass4(nodeParcelable);
    }

    public static zzbq<ChannelListener> zzd(zzrr<ChannelListener> com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_ChannelApi_ChannelListener, IntentFilter[] intentFilterArr) {
        zzbq<ChannelListener> com_google_android_gms_wearable_internal_zzbq = new zzbq(intentFilterArr, null);
        com_google_android_gms_wearable_internal_zzbq.aUH = (zzrr) zzaa.zzy(com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_ChannelApi_ChannelListener);
        return com_google_android_gms_wearable_internal_zzbq;
    }

    public static zzbq<CapabilityListener> zze(zzrr<CapabilityListener> com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_CapabilityApi_CapabilityListener, IntentFilter[] intentFilterArr) {
        zzbq<CapabilityListener> com_google_android_gms_wearable_internal_zzbq = new zzbq(intentFilterArr, null);
        com_google_android_gms_wearable_internal_zzbq.aUI = (zzrr) zzaa.zzy(com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_CapabilityApi_CapabilityListener);
        return com_google_android_gms_wearable_internal_zzbq;
    }

    private static void zzo(zzrr<?> com_google_android_gms_internal_zzrr_) {
        if (com_google_android_gms_internal_zzrr_ != null) {
            com_google_android_gms_internal_zzrr_.clear();
        }
    }

    public void clear() {
        zzo(null);
        this.aUC = null;
        zzo(null);
        this.aUD = null;
        zzo(this.aUE);
        this.aUE = null;
        zzo(this.axN);
        this.axN = null;
        zzo(this.aUF);
        this.aUF = null;
        zzo(null);
        this.aUG = null;
        zzo(this.aUH);
        this.aUH = null;
        zzo(this.aUI);
        this.aUI = null;
    }

    public void onConnectedNodes(List<NodeParcelable> list) {
    }

    public void zza(AmsEntityUpdateParcelable amsEntityUpdateParcelable) {
    }

    public void zza(AncsNotificationParcelable ancsNotificationParcelable) {
    }

    public void zza(CapabilityInfoParcelable capabilityInfoParcelable) {
        if (this.aUI != null) {
            this.aUI.zza(zzb(capabilityInfoParcelable));
        }
    }

    public void zza(ChannelEventParcelable channelEventParcelable) {
        if (this.aUH != null) {
            this.aUH.zza(zzb(channelEventParcelable));
        }
    }

    public void zza(MessageEventParcelable messageEventParcelable) {
        if (this.axN != null) {
            this.axN.zza(zzb(messageEventParcelable));
        }
    }

    public void zza(NodeParcelable nodeParcelable) {
        if (this.aUF != null) {
            this.aUF.zza(zzc(nodeParcelable));
        }
    }

    public void zzb(NodeParcelable nodeParcelable) {
        if (this.aUF != null) {
            this.aUF.zza(zzd(nodeParcelable));
        }
    }

    public void zzbq(DataHolder dataHolder) {
        if (this.aUE != null) {
            this.aUE.zza(zzbs(dataHolder));
        } else {
            dataHolder.close();
        }
    }

    public IntentFilter[] zzcmy() {
        return this.aUf;
    }

    public String zzcmz() {
        return this.aUJ;
    }
}
