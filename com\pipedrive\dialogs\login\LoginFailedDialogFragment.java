package com.pipedrive.dialogs.login;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog.Builder;
import com.pipedrive.R;
import com.pipedrive.dialogs.BaseDialogFragment;

public abstract class LoginFailedDialogFragment extends BaseDialogFragment {
    private static final String BUTTON_LABEL_NEGATIVE = "BUTTON_LABEL_NEGATIVE";
    private static final String BUTTON_LABEL_POSITIVE = "BUTTON_LABEL_POSITIVE";
    private static final String CONTENT = "CONTENT";
    private static final int NO_RESOURCE_ID = -1;
    private static final String TITLE = "TITLE";

    static void showDialog(@NonNull FragmentManager supportFragmentManager, @NonNull String tag, @NonNull DialogFragment dialogFragment, @StringRes @NonNull Integer messageResId, @StringRes @NonNull Integer titleResId, @Nullable @StringRes Integer positiveButtonLabelResId, @Nullable @StringRes Integer negativeButtonLabelResId) {
        Bundle args = new Bundle();
        args.putInt(CONTENT, messageResId.intValue());
        args.putInt(TITLE, titleResId.intValue());
        if (negativeButtonLabelResId != null) {
            args.putInt(BUTTON_LABEL_NEGATIVE, negativeButtonLabelResId.intValue());
        }
        if (positiveButtonLabelResId != null) {
            args.putInt(BUTTON_LABEL_POSITIVE, positiveButtonLabelResId.intValue());
        }
        dialogFragment.setArguments(args);
        dialogFragment.show(supportFragmentManager, tag);
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setStyle(0, R.style.Theme.Pipedrive.Dialog.Alert);
        int content = getArguments().getInt(CONTENT);
        int dialogTitle = getArguments().getInt(TITLE);
        int negativeBtnLabel = getArguments().getInt(BUTTON_LABEL_NEGATIVE, -1);
        int positiveBtnLabel = getArguments().getInt(BUTTON_LABEL_POSITIVE, -1);
        Builder builder = new Builder(getActivity()).setTitle(dialogTitle).setMessage(content);
        if (positiveBtnLabel != -1) {
            builder.setPositiveButton(positiveBtnLabel, new OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    LoginFailedDialogFragment.this.onPositiveButtonClicked();
                }
            });
        }
        if (negativeBtnLabel != -1) {
            builder.setNegativeButton(negativeBtnLabel, new OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    LoginFailedDialogFragment.this.onNegativeButtonClicked();
                }
            });
        }
        return builder.create();
    }

    protected void onNegativeButtonClicked() {
    }

    protected void onPositiveButtonClicked() {
    }
}
