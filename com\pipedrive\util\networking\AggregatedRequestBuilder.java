package com.pipedrive.util.networking;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.newrelic.agent.android.instrumentation.GsonInstrumentation;
import com.pipedrive.logging.Log;
import com.pipedrive.util.networking.AggregatedRequest.Response;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class AggregatedRequestBuilder {
    static final String TAG = AggregatedRequestBuilder.class.getSimpleName();
    private List<AggregatedRequest> mAggregatedRequest;
    private boolean mBail;
    private boolean mResponsesProcessedSuccessfully;

    public enum RequestMethod {
        GET,
        POST,
        PUT,
        DELETE
    }

    public AggregatedRequestBuilder() {
        this.mBail = false;
        this.mResponsesProcessedSuccessfully = false;
        this.mAggregatedRequest = null;
        this.mAggregatedRequest = new ArrayList();
    }

    public AggregatedRequestBuilder appendRequest(String url, RequestMethod requestMethod) {
        return appendRequest(url, requestMethod, null);
    }

    public AggregatedRequestBuilder appendRequest(String url, RequestMethod requestMethod, String requestPayloadJSON) {
        this.mAggregatedRequest.add(new AggregatedRequest(url, requestMethod, requestPayloadJSON));
        return this;
    }

    String getAggregateRequestJSON() {
        JsonArray requests = new JsonArray();
        for (AggregatedRequest aggregatedRequest : this.mAggregatedRequest) {
            JsonObject request = new JsonObject();
            request.addProperty("url", aggregatedRequest.mRequestUrl);
            request.addProperty("method", aggregatedRequest.mRequestMethod.toString());
            if (aggregatedRequest.mRequestPayloadJSON != null) {
                request.add("payload", new JsonParser().parse(aggregatedRequest.mRequestPayloadJSON));
            }
            requests.add(request);
        }
        JsonObject aggregatedRequest2 = new JsonObject();
        aggregatedRequest2.addProperty("bail", Boolean.valueOf(this.mBail));
        aggregatedRequest2.add("requests", requests);
        return aggregatedRequest2.toString();
    }

    AggregatedRequestBuilder processResponse(JsonReader reader) throws IOException {
        String tag = TAG + ".processResponse()";
        this.mResponsesProcessedSuccessfully = false;
        Response[] responses = new Response[0];
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (Response.JSON_PARAM_SUCCESS.equalsIgnoreCase(name)) {
                this.mResponsesProcessedSuccessfully = reader.nextBoolean();
            } else if (Response.JSON_PARAM_DATA.equalsIgnoreCase(name)) {
                try {
                    Gson gson = new Gson();
                    Type type = Response[].class;
                    responses = (Response[]) (!(gson instanceof Gson) ? gson.fromJson(reader, type) : GsonInstrumentation.fromJson(gson, reader, type));
                } catch (Exception e) {
                    Log.e(new Throwable("Error in parsing JSON for aggregated request!", e));
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        for (int i = 0; i < this.mAggregatedRequest.size(); i++) {
            AggregatedRequest aggregatedRequest = (AggregatedRequest) this.mAggregatedRequest.get(i);
            if (i < responses.length) {
                aggregatedRequest.setResponse(responses[i]);
            }
        }
        return this;
    }

    public List<AggregatedRequest> getAggregatedRequests() {
        return this.mAggregatedRequest;
    }

    public boolean isResponsesProcessedSuccessfully() {
        return this.mResponsesProcessedSuccessfully;
    }
}
