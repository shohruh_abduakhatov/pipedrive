package com.pipedrive.nearby.cards.cards;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.internal.Utils;
import com.pipedrive.R;
import com.pipedrive.views.SelectedStagesIndicator;

public class DealCardHeader_ViewBinding extends CardHeader_ViewBinding {
    private DealCardHeader target;

    @UiThread
    public DealCardHeader_ViewBinding(DealCardHeader target, View source) {
        super(target, source);
        this.target = target;
        target.selectedStagesIndicator = (SelectedStagesIndicator) Utils.findRequiredViewAsType(source, R.id.nearbyDealCard.selectedStagesIndicator, "field 'selectedStagesIndicator'", SelectedStagesIndicator.class);
        target.personTextView = (TextView) Utils.findRequiredViewAsType(source, R.id.nearbyDealCard.person, "field 'personTextView'", TextView.class);
        target.organizationTextView = (TextView) Utils.findRequiredViewAsType(source, R.id.nearbyDealCard.organization, "field 'organizationTextView'", TextView.class);
        target.dealIconImageView = (ImageView) Utils.findRequiredViewAsType(source, R.id.dealImage, "field 'dealIconImageView'", ImageView.class);
    }

    public void unbind() {
        DealCardHeader target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.selectedStagesIndicator = null;
        target.personTextView = null;
        target.organizationTextView = null;
        target.dealIconImageView = null;
        super.unbind();
    }
}
