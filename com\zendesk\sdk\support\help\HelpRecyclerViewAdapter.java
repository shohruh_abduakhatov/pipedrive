package com.zendesk.sdk.support.help;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.annotation.VisibleForTesting;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.sdk.model.helpcenter.SimpleArticle;
import com.zendesk.sdk.model.helpcenter.help.CategoryItem;
import com.zendesk.sdk.model.helpcenter.help.HelpItem;
import com.zendesk.sdk.model.helpcenter.help.SeeAllArticlesItem;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.support.SupportMvp;
import com.zendesk.sdk.support.SupportUiConfig;
import com.zendesk.sdk.support.ViewArticleActivity;
import com.zendesk.sdk.support.help.HelpMvp.Presenter;
import com.zendesk.sdk.support.help.HelpMvp.View;
import com.zendesk.sdk.util.UiUtils;
import java.util.List;

class HelpRecyclerViewAdapter extends Adapter<HelpViewHolder> implements View {
    private static final String LOG_TAG = "HelpRecyclerViewAdapter";
    private Context context;
    private int defaultCategoryTitleColour;
    private int highlightCategoryTitleColour;
    private Presenter presenter;

    static abstract class HelpViewHolder extends ViewHolder {
        TextView textView;

        public abstract void bindTo(HelpItem helpItem, int i);

        HelpViewHolder(android.view.View itemView) {
            super(itemView);
        }
    }

    @VisibleForTesting
    class ArticleViewHolder extends HelpViewHolder {
        ArticleViewHolder(android.view.View itemView) {
            super(itemView);
            this.textView = (TextView) itemView;
        }

        public void bindTo(final HelpItem item, int position) {
            if (item == null) {
                Logger.e(HelpRecyclerViewAdapter.LOG_TAG, "Article item was null, cannot bind", new Object[0]);
                return;
            }
            this.textView.setText(item.getName());
            this.textView.setOnClickListener(new OnClickListener() {
                public void onClick(android.view.View v) {
                    ViewArticleActivity.startActivity(HelpRecyclerViewAdapter.this.context, new SimpleArticle(item.getId(), item.getName()));
                }
            });
        }
    }

    @VisibleForTesting
    class CategoryViewHolder extends HelpViewHolder {
        private static final int ROTATION_END_LEVEL = 10000;
        private static final String ROTATION_PROPERTY_NAME = "level";
        private static final int ROTATION_START_LEVEL = 0;
        private boolean expanded;
        private Drawable expanderDrawable;

        CategoryViewHolder(android.view.View itemView) {
            super(itemView);
            this.textView = (TextView) itemView;
            this.expanderDrawable = DrawableCompat.wrap(ContextCompat.getDrawable(itemView.getContext(), R.drawable.ic_expand_more)).mutate();
            DrawableCompat.setTint(this.expanderDrawable, UiUtils.themeAttributeToColor(16842808, HelpRecyclerViewAdapter.this.context, R.color.fallback_text_color));
            DrawableCompat.setTintMode(this.expanderDrawable, Mode.SRC_IN);
            TextView textView = (TextView) itemView;
            if (VERSION.SDK_INT >= 17) {
                textView.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, this.expanderDrawable, null);
            } else {
                textView.setCompoundDrawablesWithIntrinsicBounds(null, null, this.expanderDrawable, null);
            }
        }

        public void bindTo(HelpItem item, final int position) {
            int i = 0;
            if (item == null) {
                Logger.e(HelpRecyclerViewAdapter.LOG_TAG, "Category item was null, cannot bind", new Object[0]);
                return;
            }
            this.textView.setText(item.getName());
            final CategoryItem categoryItem = (CategoryItem) item;
            this.expanded = categoryItem.isExpanded();
            Drawable drawable = this.expanderDrawable;
            if (this.expanded) {
                i = 10000;
            }
            drawable.setLevel(i);
            setHighlightColor(categoryItem.isExpanded());
            this.textView.setOnClickListener(new OnClickListener() {
                public void onClick(android.view.View v) {
                    int i = 10000;
                    CategoryViewHolder.this.expanded = HelpRecyclerViewAdapter.this.presenter.onCategoryClick(categoryItem, position);
                    Drawable access$300 = CategoryViewHolder.this.expanderDrawable;
                    String str = "level";
                    int[] iArr = new int[2];
                    iArr[0] = CategoryViewHolder.this.expanded ? 0 : 10000;
                    if (!CategoryViewHolder.this.expanded) {
                        i = 0;
                    }
                    iArr[1] = i;
                    ObjectAnimator.ofInt(access$300, str, iArr).start();
                    CategoryViewHolder.this.setHighlightColor(CategoryViewHolder.this.expanded);
                }
            });
        }

        private void setHighlightColor(boolean expanded) {
            if (expanded) {
                this.textView.setTextColor(HelpRecyclerViewAdapter.this.highlightCategoryTitleColour);
                this.expanderDrawable.setColorFilter(HelpRecyclerViewAdapter.this.highlightCategoryTitleColour, Mode.SRC_IN);
                return;
            }
            this.textView.setTextColor(HelpRecyclerViewAdapter.this.defaultCategoryTitleColour);
            this.expanderDrawable.setColorFilter(HelpRecyclerViewAdapter.this.defaultCategoryTitleColour, Mode.SRC_IN);
        }

        public boolean isExpanded() {
            return this.expanded;
        }
    }

    private class ExtraPaddingViewHolder extends HelpViewHolder {
        ExtraPaddingViewHolder(android.view.View itemView) {
            super(itemView);
        }

        public void bindTo(HelpItem item, int position) {
        }
    }

    private class LoadingViewHolder extends HelpViewHolder {
        LoadingViewHolder(android.view.View itemView) {
            super(itemView);
        }

        public void bindTo(HelpItem item, int position) {
        }
    }

    private class NoResultsViewHolder extends HelpViewHolder {
        NoResultsViewHolder(android.view.View itemView) {
            super(itemView);
        }

        public void bindTo(HelpItem item, int position) {
        }
    }

    @VisibleForTesting
    class SectionViewHolder extends HelpViewHolder {
        SectionViewHolder(android.view.View itemView) {
            super(itemView);
            this.textView = (TextView) itemView.findViewById(R.id.section_title);
        }

        public void bindTo(HelpItem item, int position) {
            if (item == null) {
                Logger.e(HelpRecyclerViewAdapter.LOG_TAG, "Section item was null, cannot bind", new Object[0]);
            } else {
                this.textView.setText(item.getName());
            }
        }
    }

    private class SeeAllViewHolder extends HelpViewHolder {
        private ProgressBar progressBar;

        SeeAllViewHolder(android.view.View itemView) {
            super(itemView);
            this.textView = (TextView) itemView.findViewById(R.id.help_section_action_button);
            this.progressBar = (ProgressBar) itemView.findViewById(R.id.help_section_loading_progress);
        }

        public void bindTo(final HelpItem item, int position) {
            if (item instanceof SeeAllArticlesItem) {
                String sectionLabel;
                final SeeAllArticlesItem seeAllArticlesItem = (SeeAllArticlesItem) item;
                if (seeAllArticlesItem.isLoading()) {
                    this.textView.setVisibility(8);
                    this.progressBar.setVisibility(0);
                } else {
                    this.textView.setVisibility(0);
                    this.progressBar.setVisibility(8);
                }
                if (seeAllArticlesItem.getSection() != null) {
                    sectionLabel = HelpRecyclerViewAdapter.this.context.getString(R.string.help_see_all_n_articles_label, new Object[]{Integer.valueOf(section.getTotalArticlesCount())});
                } else {
                    sectionLabel = HelpRecyclerViewAdapter.this.context.getString(R.string.help_see_all_articles_label);
                }
                this.textView.setText(sectionLabel);
                this.textView.setOnClickListener(new OnClickListener() {
                    public void onClick(android.view.View v) {
                        SeeAllViewHolder.this.textView.setVisibility(8);
                        SeeAllViewHolder.this.progressBar.setVisibility(0);
                        HelpRecyclerViewAdapter.this.presenter.onSeeAllClick((SeeAllArticlesItem) item);
                        seeAllArticlesItem.setLoading(true);
                    }
                });
                return;
            }
            Logger.e(HelpRecyclerViewAdapter.LOG_TAG, "SeeAll item was null, cannot bind", new Object[0]);
        }
    }

    public HelpRecyclerViewAdapter(SupportUiConfig supportUiConfig) {
        this.presenter = new HelpAdapterPresenter(this, new HelpModel(ZendeskConfig.INSTANCE.provider().helpCenterProvider()), ZendeskConfig.INSTANCE.provider().networkInfoProvider(), supportUiConfig);
    }

    void setContentUpdateListener(SupportMvp.Presenter listener) {
        if (this.presenter != null) {
            this.presenter.setContentPresenter(listener);
        }
    }

    public void showItems(List<HelpItem> list) {
        notifyDataSetChanged();
    }

    public void addItem(int position, HelpItem item) {
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        notifyItemRemoved(position);
    }

    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.context = recyclerView.getContext();
        this.highlightCategoryTitleColour = UiUtils.themeAttributeToColor(R.attr.colorPrimary, this.context, R.color.fallback_text_color);
        this.defaultCategoryTitleColour = UiUtils.themeAttributeToColor(16842806, this.context, R.color.fallback_text_color);
        this.presenter.onAttached();
    }

    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        this.presenter.onDetached();
        this.context = null;
    }

    public HelpViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1:
                return new CategoryViewHolder(inflateView(parent, R.layout.row_category));
            case 2:
                return new SectionViewHolder(inflateView(parent, R.layout.row_section));
            case 3:
                return new ArticleViewHolder(inflateView(parent, R.layout.row_article));
            case 4:
                return new SeeAllViewHolder(inflateView(parent, R.layout.row_action));
            case 5:
                return new LoadingViewHolder(inflateView(parent, R.layout.row_loading_progress));
            case 7:
                return new NoResultsViewHolder(inflateView(parent, R.layout.row_no_articles_found));
            case 8:
                return new ExtraPaddingViewHolder(inflateView(parent, R.layout.row_padding));
            default:
                Logger.w(LOG_TAG, "Unknown item type, returning null for holder", new Object[0]);
                return null;
        }
    }

    private android.view.View inflateView(ViewGroup parent, int layoutId) {
        return LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
    }

    public int getItemCount() {
        return this.presenter.getItemCount();
    }

    public void onBindViewHolder(HelpViewHolder holder, int position) {
        if (holder == null) {
            Logger.w(LOG_TAG, "Holder was null, possible unexpected item type", new Object[0]);
        } else {
            holder.bindTo(this.presenter.getItemForBinding(position), position);
        }
    }

    public int getItemViewType(int position) {
        return this.presenter.getItemViewType(position);
    }
}
