package com.pipedrive.util.networking.entities.self;

import android.support.annotation.Nullable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompanyEntity {
    @Nullable
    @SerializedName("domain")
    @Expose
    public String mDomain;
    @Nullable
    @SerializedName("name")
    @Expose
    public String mName;
    @Nullable
    @SerializedName("id")
    @Expose
    public Long mPipedriveId;

    public String toString() {
        return "CompanyEntity{mPipedriveId=" + this.mPipedriveId + ", mName='" + this.mName + '\'' + ", mDomain='" + this.mDomain + '\'' + '}';
    }
}
