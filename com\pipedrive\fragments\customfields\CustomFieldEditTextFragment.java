package com.pipedrive.fragments.customfields;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.pipedrive.R;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;

public class CustomFieldEditTextFragment<EDITTEXT extends EditText> extends CustomFieldBaseFragment {
    EDITTEXT mEditText;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(getLayoutResId().intValue(), container, false);
        this.mEditText = (EditText) mainView.findViewById(getEditTextResId().intValue());
        return mainView;
    }

    @IdRes
    Integer getEditTextResId() {
        return Integer.valueOf(R.id.customFieldTextEdit);
    }

    @LayoutRes
    Integer getLayoutResId() {
        return Integer.valueOf(R.layout.fragment_custom_field_edit_text);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        Session session = PipedriveApp.getActiveSession();
        if (session != null) {
            initialSetup(session);
            String customFieldValue = this.mCustomField.getTempValue();
            if (customFieldValue != null) {
                initialSetup(customFieldValue);
            }
        }
    }

    void initialSetup(@NonNull String customFieldValue) {
        this.mEditText.setMovementMethod(LinkMovementMethod.getInstance());
        Linkify.addLinks(new SpannableString(customFieldValue), 1);
        this.mEditText.setText(TextUtils.concat(new CharSequence[]{spannable, getString(R.string.zero_width_space)}));
    }

    void initialSetup(@NonNull Session session) {
        if ("phone".equalsIgnoreCase(this.mCustomField.getFieldDataType())) {
            this.mEditText.setInputType(3);
        }
    }

    protected void clearContent() {
        this.mEditText.setText("");
    }

    protected void saveContent() {
        this.mCustomField.setTempValue(this.mEditText.getText().toString());
    }
}
