package com.pipedrive.changes;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.changes.Change.ChangeOperationType;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Organization;

public class OrganizationChanger extends Changer<Organization> {
    public OrganizationChanger(Session session) {
        super(session, 10, null);
    }

    protected OrganizationChanger(Changer parentChanger) {
        super(parentChanger.getSession(), 10, parentChanger);
    }

    @Nullable
    protected Long getChangeMetaData(@NonNull Organization organization, @NonNull ChangeOperationType changeOperationType) {
        return Long.valueOf(organization.getSqlId());
    }

    @NonNull
    protected ChangeResult createChange(@NonNull Organization newOrganization) {
        OrganizationsDataSource orgDataSource = new OrganizationsDataSource(getSession().getDatabase());
        if (newOrganization.isExisting() || newOrganization.isStored()) {
            LogJourno.reportEvent(EVENT.ChangesChanger_createChangeRequestedWithExistingChange, OrganizationChanger.class.getSimpleName());
            Log.e(new Throwable("I can only handle new Organizations!"));
            return ChangeResult.FAILED;
        }
        long newOrgSqlId = orgDataSource.createOrUpdate((BaseDatasourceEntity) newOrganization);
        if (newOrgSqlId == -1) {
            return ChangeResult.FAILED;
        }
        newOrganization.setSqlId(newOrgSqlId);
        return ChangeResult.SUCCESSFUL;
    }

    @NonNull
    protected ChangeResult updateChange(@NonNull Organization existingOrganization) {
        OrganizationsDataSource organizationsDataSource = new OrganizationsDataSource(getSession().getDatabase());
        if (existingOrganization.isStored() && organizationsDataSource.findBySqlId(existingOrganization.getSqlId()) != null) {
            return organizationsDataSource.createOrUpdate((BaseDatasourceEntity) existingOrganization) == -1 ? ChangeResult.FAILED : ChangeResult.SUCCESSFUL;
        } else {
            LogJourno.reportEvent(EVENT.ChangesChanger_updateChangeRequestedWithNewChange, OrganizationChanger.class.getSimpleName());
            Log.e(new Throwable("I can only handle existing Organizations!"));
            return ChangeResult.FAILED;
        }
    }
}
