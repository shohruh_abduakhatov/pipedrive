package com.pipedrive.model;

import android.os.Parcel;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class ChildDataSourceEntity extends BaseDatasourceEntity {
    @Nullable
    private Long parentSqlId;

    protected ChildDataSourceEntity(@Nullable Long sqlId, @Nullable Long pipedriveId, @Nullable Long parentSqlId) {
        if (sqlId != null) {
            setSqlId(sqlId.longValue());
        }
        if (pipedriveId != null) {
            setPipedriveId(pipedriveId.intValue());
        }
        this.parentSqlId = parentSqlId;
    }

    @Nullable
    public final Long getParentSqlId() {
        return this.parentSqlId;
    }

    public final void setParentEntity(@NonNull BaseDatasourceEntity parentEntity) {
        if (parentEntity.isStored()) {
            this.parentSqlId = Long.valueOf(parentEntity.getSqlId());
        }
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeSerializable(this.parentSqlId);
    }

    protected void readFromParcel(Parcel in) {
        super.readFromParcel(in);
        this.parentSqlId = (Long) in.readSerializable();
    }

    @NonNull
    public final Boolean hasParent() {
        boolean z = this.parentSqlId != null && this.parentSqlId.longValue() > 0;
        return Boolean.valueOf(z);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        ChildDataSourceEntity entity = (ChildDataSourceEntity) o;
        if (this.parentSqlId != null) {
            return this.parentSqlId.equals(entity.parentSqlId);
        }
        if (entity.parentSqlId != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (super.hashCode() * 31) + (this.parentSqlId != null ? this.parentSqlId.hashCode() : 0);
    }
}
