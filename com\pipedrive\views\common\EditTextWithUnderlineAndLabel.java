package com.pipedrive.views.common;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import com.pipedrive.R;

public abstract class EditTextWithUnderlineAndLabel extends TextViewLayoutWithUnderlineAndLabel<EditText> {

    public interface OnValueChangedListener {
        void onValueChanged(@NonNull String str);
    }

    public EditTextWithUnderlineAndLabel(Context context) {
        this(context, null);
    }

    public EditTextWithUnderlineAndLabel(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EditTextWithUnderlineAndLabel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @NonNull
    protected EditText createMainView(@NonNull Context context, AttributeSet attrs, int defStyle) {
        EditText editText = new EditText(context, attrs, defStyle);
        editText.setBackgroundColor(0);
        editText.setTextAppearance(getContext(), R.style.TextAppearance.Title);
        TypedValue colorAccentValue = new TypedValue();
        getContext().getTheme().resolveAttribute(R.attr.colorAccent, colorAccentValue, true);
        final int colorAccent = colorAccentValue.data;
        final ColorStateList labelTextColors = this.mLabelView.getTextColors();
        editText.setOnFocusChangeListener(new OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    EditTextWithUnderlineAndLabel.this.mLabelView.setTextColor(colorAccent);
                    EditTextWithUnderlineAndLabel.this.mUnderlineView.getBackground().setColorFilter(colorAccent, Mode.SRC_ATOP);
                    return;
                }
                EditTextWithUnderlineAndLabel.this.mLabelView.setTextColor(labelTextColors);
                EditTextWithUnderlineAndLabel.this.mUnderlineView.getBackground().clearColorFilter();
            }
        });
        editText.setFocusableInTouchMode(true);
        return editText;
    }

    public int length() {
        return ((EditText) getMainView()).length();
    }

    public void setHint(@Nullable CharSequence hint) {
        ((EditText) getMainView()).setHint(hint);
    }

    public CharSequence getHint() {
        return ((EditText) getMainView()).getHint();
    }

    public void setMinHeight(int minHeightInPixels) {
        ((EditText) getMainView()).setMinHeight(minHeightInPixels);
    }

    protected void setupListener(@Nullable final OnValueChangedListener onValueChangedListener) {
        ((EditText) this.mMainView).addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
                if (onValueChangedListener != null) {
                    onValueChangedListener.onValueChanged(s.toString());
                }
            }
        });
    }
}
