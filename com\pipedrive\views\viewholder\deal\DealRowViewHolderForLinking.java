package com.pipedrive.views.viewholder.deal;

import com.pipedrive.R;

public class DealRowViewHolderForLinking extends DealRowViewHolderForSearch {
    public int getLayoutResourceId() {
        return R.layout.row_deal_deals_list;
    }
}
