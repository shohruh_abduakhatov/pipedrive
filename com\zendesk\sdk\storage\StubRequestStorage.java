package com.zendesk.sdk.storage;

import android.support.annotation.NonNull;
import com.zendesk.logger.Logger;
import java.util.ArrayList;
import java.util.List;

class StubRequestStorage implements RequestStorage {
    private static final String LOG_TAG = "StubRequestStorage";

    StubRequestStorage() {
    }

    @NonNull
    public List<String> getStoredRequestIds() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
        return new ArrayList();
    }

    public void storeRequestId(@NonNull String requestId) {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
    }

    public void setCommentCount(@NonNull String requestId, int commentCount) {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
    }

    public Integer getCommentCount(@NonNull String requestId) {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
        return Integer.valueOf(0);
    }

    public void clearUserData() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
    }

    public String getCacheKey() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
        return "";
    }
}
