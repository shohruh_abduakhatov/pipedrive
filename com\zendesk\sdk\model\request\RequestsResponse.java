package com.zendesk.sdk.model.request;

import android.support.annotation.NonNull;
import com.zendesk.sdk.model.network.ResponseWrapper;
import com.zendesk.util.CollectionUtils;
import java.util.List;

public class RequestsResponse extends ResponseWrapper {
    private List<Request> requests;

    @NonNull
    public List<Request> getRequests() {
        return CollectionUtils.copyOf(this.requests);
    }
}
