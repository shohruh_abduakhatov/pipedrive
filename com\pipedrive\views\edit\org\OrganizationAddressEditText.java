package com.pipedrive.views.edit.org;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.EditText;
import com.pipedrive.R;
import com.pipedrive.model.Organization;
import com.pipedrive.views.common.EditTextWithUnderlineAndLabel;
import com.pipedrive.views.common.EditTextWithUnderlineAndLabel.OnValueChangedListener;

public class OrganizationAddressEditText extends EditTextWithUnderlineAndLabel {
    public OrganizationAddressEditText(Context context) {
        super(context);
    }

    public OrganizationAddressEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OrganizationAddressEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @NonNull
    protected EditText createMainView(@NonNull Context context, AttributeSet attrs, int defStyle) {
        EditText editText = super.createMainView(context, attrs, defStyle);
        editText.setInputType(16385);
        return editText;
    }

    public void setup(@NonNull Organization organization, @Nullable OnValueChangedListener listener) {
        ((EditText) getMainView()).setText(organization.getAddress());
        setupListener(listener);
    }

    protected int getLabelTextResourceId() {
        return R.string.address;
    }
}
