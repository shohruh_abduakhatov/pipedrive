package com.pipedrive.nearby.cards.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.ViewGroup;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.cards.cards.CardHeader;
import com.pipedrive.nearby.model.NearbyItem;
import java.util.List;

public abstract class CardAdapterForAggregatedItemsList extends Adapter<AggregatedItemListViewHolder> {
    @NonNull
    private final List<? extends NearbyItem> mNearbyItemsList;
    @NonNull
    private final Session mSession;

    abstract AggregatedItemListViewHolder createViewHolder(@NonNull ViewGroup viewGroup);

    abstract CardHeader getCardHeader();

    CardAdapterForAggregatedItemsList(@NonNull Session session, @NonNull List<? extends NearbyItem> itemsList) {
        this.mSession = session;
        this.mNearbyItemsList = itemsList;
    }

    public void onBindViewHolder(AggregatedItemListViewHolder holder, int position) {
        holder.bind(this.mSession, (NearbyItem) this.mNearbyItemsList.get(position), getCardHeader());
    }

    public void onViewRecycled(AggregatedItemListViewHolder holder) {
        holder.unbind();
    }

    public int getItemCount() {
        return this.mNearbyItemsList.size();
    }

    public AggregatedItemListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return createViewHolder(parent);
    }
}
