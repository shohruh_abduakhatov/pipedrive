package com.pipedrive.datasource.products;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.datasource.BaseChildDataSource;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.model.ChildDataSourceEntity;
import com.pipedrive.model.products.Price;
import com.pipedrive.model.products.ProductPrice;
import com.pipedrive.model.products.ProductVariation;
import com.pipedrive.util.CursorHelper;

public class ProductVariationPriceDataSource extends BaseChildDataSource<ProductPrice> {
    private static final String[] ALL_COLUMNS = new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_PRODUCT_VARIATION_PRICES_PIPEDRIVE_ID, PipeSQLiteHelper.COLUMN_PRODUCT_VARIATION_PRICES_PRODUCT_VARIATION_SQL_ID, PipeSQLiteHelper.COLUMN_PRODUCT_VARIATION_PRICES_PRICE, PipeSQLiteHelper.COLUMN_PRODUCT_VARIATION_PRICES_CURRENCY_SQL_ID};

    public ProductVariationPriceDataSource(SQLiteDatabase dbconn) {
        super(dbconn);
    }

    @NonNull
    protected String getColumnNameForParentSqlId() {
        return PipeSQLiteHelper.COLUMN_PRODUCT_VARIATION_PRICES_PRODUCT_VARIATION_SQL_ID;
    }

    @NonNull
    protected ContentValues getContentValues(@NonNull ProductPrice price) {
        ContentValues contentValues = super.getContentValues((ChildDataSourceEntity) price);
        CursorHelper.put(price.getPrice().getValue(), contentValues, PipeSQLiteHelper.COLUMN_PRODUCT_VARIATION_PRICES_PRICE);
        CursorHelper.put(Long.valueOf(price.getPrice().getCurrency().getSqlId()), contentValues, PipeSQLiteHelper.COLUMN_PRODUCT_VARIATION_PRICES_CURRENCY_SQL_ID);
        return contentValues;
    }

    @Nullable
    protected ProductPrice deflateCursor(@NonNull Cursor cursor, @Nullable Long sqlId, @Nullable Long pipedriveId, @Nullable Long parentSqlId) {
        Price price = CursorHelper.getPrice(cursor, PipeSQLiteHelper.COLUMN_PRODUCT_VARIATION_PRICES_PRICE, PipeSQLiteHelper.COLUMN_PRODUCT_VARIATION_PRICES_CURRENCY_SQL_ID, getTransactionalDBConnection());
        if (price == null) {
            return null;
        }
        return ProductPrice.create(sqlId, pipedriveId, parentSqlId, price);
    }

    @NonNull
    protected String getColumnNameForPipedriveId() {
        return PipeSQLiteHelper.COLUMN_PRODUCT_VARIATION_PRICES_PIPEDRIVE_ID;
    }

    @NonNull
    protected String getColumnNameForSqlId() {
        return PipeSQLiteHelper.COLUMN_ID;
    }

    @NonNull
    protected String getTableName() {
        return PipeSQLiteHelper.TABLE_PRODUCT_VARIATION_PRICES;
    }

    @NonNull
    protected String[] getAllColumns() {
        return ALL_COLUMNS;
    }

    void createOrUpdateProductVariation(@NonNull ProductVariation productVariation) {
        createOrUpdate(productVariation.getPrices(), productVariation);
    }
}
