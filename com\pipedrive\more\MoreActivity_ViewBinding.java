package com.pipedrive.more;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;
import com.pipedrive.views.profilepicture.ProfilePictureRoundUserGrey;

public class MoreActivity_ViewBinding implements Unbinder {
    private MoreActivity target;
    private View view2131820784;
    private View view2131820786;
    private View view2131820787;
    private View view2131820788;
    private View view2131820789;
    private View view2131820790;
    private View view2131820791;

    @UiThread
    public MoreActivity_ViewBinding(MoreActivity target) {
        this(target, target.getWindow().getDecorView());
    }

    @UiThread
    public MoreActivity_ViewBinding(final MoreActivity target, View source) {
        this.target = target;
        View view = Utils.findRequiredView(source, R.id.synchronize_now, "field 'synchronizeNowButton' and method 'onSynchronizeNowClicked'");
        target.synchronizeNowButton = (MoreButton) Utils.castView(view, R.id.synchronize_now, "field 'synchronizeNowButton'", MoreButton.class);
        this.view2131820787 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onSynchronizeNowClicked();
            }
        });
        view = Utils.findRequiredView(source, R.id.about, "field 'aboutButton' and method 'onAboutClicked'");
        target.aboutButton = (MoreButton) Utils.castView(view, R.id.about, "field 'aboutButton'", MoreButton.class);
        this.view2131820790 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onAboutClicked();
            }
        });
        target.userName = (TextView) Utils.findRequiredViewAsType(source, R.id.userName, "field 'userName'", TextView.class);
        target.selectedCompanyName = (TextView) Utils.findRequiredViewAsType(source, R.id.companyName, "field 'selectedCompanyName'", TextView.class);
        target.profilePicture = (ProfilePictureRoundUserGrey) Utils.findRequiredViewAsType(source, R.id.profilePicture, "field 'profilePicture'", ProfilePictureRoundUserGrey.class);
        target.arrow = Utils.findRequiredView(source, R.id.arrow, "field 'arrow'");
        view = Utils.findRequiredView(source, R.id.companySwitchContainer, "field 'companySwitchContainer' and method 'companySwitchClicked'");
        target.companySwitchContainer = view;
        this.view2131820784 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.companySwitchClicked();
            }
        });
        view = Utils.findRequiredView(source, R.id.nearby, "method 'onNearbyClicked'");
        this.view2131820786 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onNearbyClicked();
            }
        });
        view = Utils.findRequiredView(source, R.id.preferences, "method 'onPreferencesClicked'");
        this.view2131820788 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onPreferencesClicked();
            }
        });
        view = Utils.findRequiredView(source, R.id.help_and_feedback, "method 'onHelpAndFeedbackClicked'");
        this.view2131820789 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onHelpAndFeedbackClicked();
            }
        });
        view = Utils.findRequiredView(source, R.id.sign_out, "method 'onSignOutClicked'");
        this.view2131820791 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onSignOutClicked();
            }
        });
    }

    @CallSuper
    public void unbind() {
        MoreActivity target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.synchronizeNowButton = null;
        target.aboutButton = null;
        target.userName = null;
        target.selectedCompanyName = null;
        target.profilePicture = null;
        target.arrow = null;
        target.companySwitchContainer = null;
        this.view2131820787.setOnClickListener(null);
        this.view2131820787 = null;
        this.view2131820790.setOnClickListener(null);
        this.view2131820790 = null;
        this.view2131820784.setOnClickListener(null);
        this.view2131820784 = null;
        this.view2131820786.setOnClickListener(null);
        this.view2131820786 = null;
        this.view2131820788.setOnClickListener(null);
        this.view2131820788 = null;
        this.view2131820789.setOnClickListener(null);
        this.view2131820789 = null;
        this.view2131820791.setOnClickListener(null);
        this.view2131820791 = null;
    }
}
