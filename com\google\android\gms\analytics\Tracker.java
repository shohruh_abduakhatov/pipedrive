package com.google.android.gms.analytics;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.analytics.internal.zzab;
import com.google.android.gms.analytics.internal.zzad;
import com.google.android.gms.analytics.internal.zzan;
import com.google.android.gms.analytics.internal.zzao;
import com.google.android.gms.analytics.internal.zzd;
import com.google.android.gms.analytics.internal.zze;
import com.google.android.gms.analytics.internal.zzf;
import com.google.android.gms.analytics.internal.zzh;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzms;
import com.pipedrive.model.pagination.Pagination;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

public class Tracker extends zzd {
    private boolean bu;
    private final Map<String, String> bv = new HashMap();
    private final zzad bw;
    private final zza bx;
    private ExceptionReporter by;
    private zzan bz;
    private final Map<String, String> zzbly = new HashMap();

    private class zza extends zzd implements zza {
        final /* synthetic */ Tracker bH;
        private boolean bI;
        private int bJ;
        private long bK = -1;
        private boolean bL;
        private long bM;

        protected zza(Tracker tracker, zzf com_google_android_gms_analytics_internal_zzf) {
            this.bH = tracker;
            super(com_google_android_gms_analytics_internal_zzf);
        }

        private void zzaac() {
            if (this.bK >= 0 || this.bI) {
                zzza().zza(this.bH.bx);
            } else {
                zzza().zzb(this.bH.bx);
            }
        }

        public void enableAutoActivityTracking(boolean z) {
            this.bI = z;
            zzaac();
        }

        public void setSessionTimeout(long j) {
            this.bK = j;
            zzaac();
        }

        public synchronized boolean zzaab() {
            boolean z;
            z = this.bL;
            this.bL = false;
            return z;
        }

        boolean zzaad() {
            return zzabz().elapsedRealtime() >= this.bM + Math.max(1000, this.bK);
        }

        public void zzo(Activity activity) {
            if (this.bJ == 0 && zzaad()) {
                this.bL = true;
            }
            this.bJ++;
            if (this.bI) {
                Intent intent = activity.getIntent();
                if (intent != null) {
                    this.bH.setCampaignParamsOnNextHit(intent.getData());
                }
                Map hashMap = new HashMap();
                hashMap.put("&t", "screenview");
                this.bH.set("&cd", this.bH.bz != null ? this.bH.bz.zzr(activity) : activity.getClass().getCanonicalName());
                if (TextUtils.isEmpty((CharSequence) hashMap.get("&dr"))) {
                    CharSequence zzq = Tracker.zzq(activity);
                    if (!TextUtils.isEmpty(zzq)) {
                        hashMap.put("&dr", zzq);
                    }
                }
                this.bH.send(hashMap);
            }
        }

        public void zzp(Activity activity) {
            this.bJ--;
            this.bJ = Math.max(0, this.bJ);
            if (this.bJ == 0) {
                this.bM = zzabz().elapsedRealtime();
            }
        }

        protected void zzzy() {
        }
    }

    Tracker(zzf com_google_android_gms_analytics_internal_zzf, String str, zzad com_google_android_gms_analytics_internal_zzad) {
        super(com_google_android_gms_analytics_internal_zzf);
        if (str != null) {
            this.zzbly.put("&tid", str);
        }
        this.zzbly.put("useSecure", "1");
        this.zzbly.put("&a", Integer.toString(new Random().nextInt(Integer.MAX_VALUE) + 1));
        if (com_google_android_gms_analytics_internal_zzad == null) {
            this.bw = new zzad("tracking", zzabz());
        } else {
            this.bw = com_google_android_gms_analytics_internal_zzad;
        }
        this.bx = new zza(this, com_google_android_gms_analytics_internal_zzf);
    }

    private static boolean zza(Entry<String, String> entry) {
        String str = (String) entry.getKey();
        String str2 = (String) entry.getValue();
        return str.startsWith("&") && str.length() >= 2;
    }

    private static String zzb(Entry<String, String> entry) {
        return !zza((Entry) entry) ? null : ((String) entry.getKey()).substring(1);
    }

    private static void zzb(Map<String, String> map, Map<String, String> map2) {
        zzaa.zzy(map2);
        if (map != null) {
            for (Entry entry : map.entrySet()) {
                String zzb = zzb(entry);
                if (zzb != null) {
                    map2.put(zzb, (String) entry.getValue());
                }
            }
        }
    }

    private static void zzc(Map<String, String> map, Map<String, String> map2) {
        zzaa.zzy(map2);
        if (map != null) {
            for (Entry entry : map.entrySet()) {
                String zzb = zzb(entry);
                if (!(zzb == null || map2.containsKey(zzb))) {
                    map2.put(zzb, (String) entry.getValue());
                }
            }
        }
    }

    static String zzq(Activity activity) {
        zzaa.zzy(activity);
        Intent intent = activity.getIntent();
        if (intent == null) {
            return null;
        }
        CharSequence stringExtra = intent.getStringExtra("android.intent.extra.REFERRER_NAME");
        return !TextUtils.isEmpty(stringExtra) ? stringExtra : null;
    }

    private boolean zzzz() {
        return this.by != null;
    }

    public void enableAdvertisingIdCollection(boolean z) {
        this.bu = z;
    }

    public void enableAutoActivityTracking(boolean z) {
        this.bx.enableAutoActivityTracking(z);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void enableExceptionReporting(boolean z) {
        synchronized (this) {
            if (zzzz() == z) {
            } else if (z) {
                this.by = new ExceptionReporter(this, Thread.getDefaultUncaughtExceptionHandler(), getContext());
                Thread.setDefaultUncaughtExceptionHandler(this.by);
                zzes("Uncaught exceptions will be reported to Google Analytics");
            } else {
                Thread.setDefaultUncaughtExceptionHandler(this.by.zzzb());
                zzes("Uncaught exceptions will not be reported to Google Analytics");
            }
        }
    }

    public String get(String str) {
        zzacj();
        return TextUtils.isEmpty(str) ? null : this.zzbly.containsKey(str) ? (String) this.zzbly.get(str) : str.equals("&ul") ? zzao.zza(Locale.getDefault()) : str.equals("&cid") ? zzacf().zzady() : str.equals("&sr") ? zzaci().zzafm() : str.equals("&aid") ? zzach().zzadg().zzup() : str.equals("&an") ? zzach().zzadg().zzaae() : str.equals("&av") ? zzach().zzadg().zzaaf() : str.equals("&aiid") ? zzach().zzadg().zzaag() : null;
    }

    public void send(Map<String, String> map) {
        final long currentTimeMillis = zzabz().currentTimeMillis();
        if (zzza().getAppOptOut()) {
            zzet("AppOptOut is set to true. Not sending Google Analytics hit");
            return;
        }
        final boolean isDryRunEnabled = zzza().isDryRunEnabled();
        final Map hashMap = new HashMap();
        zzb(this.zzbly, hashMap);
        zzb(map, hashMap);
        final boolean zzg = zzao.zzg((String) this.zzbly.get("useSecure"), true);
        zzc(this.bv, hashMap);
        this.bv.clear();
        final String str = (String) hashMap.get("t");
        if (TextUtils.isEmpty(str)) {
            zzaca().zzh(hashMap, "Missing hit type parameter");
            return;
        }
        final String str2 = (String) hashMap.get("tid");
        if (TextUtils.isEmpty(str2)) {
            zzaca().zzh(hashMap, "Missing tracking id parameter");
            return;
        }
        final boolean zzaaa = zzaaa();
        synchronized (this) {
            if ("screenview".equalsIgnoreCase(str) || "pageview".equalsIgnoreCase(str) || "appview".equalsIgnoreCase(str) || TextUtils.isEmpty(str)) {
                int parseInt = Integer.parseInt((String) this.zzbly.get("&a")) + 1;
                if (parseInt >= Integer.MAX_VALUE) {
                    parseInt = 1;
                }
                this.zzbly.put("&a", Integer.toString(parseInt));
            }
        }
        zzacc().zzg(new Runnable(this) {
            final /* synthetic */ Tracker bH;

            public void run() {
                boolean z = true;
                if (this.bH.bx.zzaab()) {
                    hashMap.put("sc", Pagination.JSON_PARAM_PAGINATION_START);
                }
                zzao.zzd(hashMap, "cid", this.bH.zzza().zzze());
                String str = (String) hashMap.get("sf");
                if (str != null) {
                    double zza = zzao.zza(str, 100.0d);
                    if (zzao.zza(zza, (String) hashMap.get("cid"))) {
                        this.bH.zzb("Sampling enabled. Hit sampled out. sample rate", Double.valueOf(zza));
                        return;
                    }
                }
                com.google.android.gms.analytics.internal.zza zzb = this.bH.zzacg();
                if (zzaaa) {
                    zzao.zzb(hashMap, "ate", zzb.zzabc());
                    zzao.zzc(hashMap, "adid", zzb.zzabn());
                } else {
                    hashMap.remove("ate");
                    hashMap.remove("adid");
                }
                zzms zzadg = this.bH.zzach().zzadg();
                zzao.zzc(hashMap, "an", zzadg.zzaae());
                zzao.zzc(hashMap, "av", zzadg.zzaaf());
                zzao.zzc(hashMap, "aid", zzadg.zzup());
                zzao.zzc(hashMap, "aiid", zzadg.zzaag());
                hashMap.put("v", "1");
                hashMap.put("_v", zze.cS);
                zzao.zzc(hashMap, "ul", this.bH.zzaci().zzafl().getLanguage());
                zzao.zzc(hashMap, "sr", this.bH.zzaci().zzafm());
                boolean z2 = str.equals("transaction") || str.equals("item");
                if (z2 || this.bH.bw.zzagf()) {
                    long zzfj = zzao.zzfj((String) hashMap.get("ht"));
                    if (zzfj == 0) {
                        zzfj = currentTimeMillis;
                    }
                    if (isDryRunEnabled) {
                        this.bH.zzaca().zzc("Dry run enabled. Would have sent hit", new zzab(this.bH, hashMap, zzfj, zzg));
                        return;
                    }
                    String str2 = (String) hashMap.get("cid");
                    Map hashMap = new HashMap();
                    zzao.zza(hashMap, "uid", hashMap);
                    zzao.zza(hashMap, "an", hashMap);
                    zzao.zza(hashMap, "aid", hashMap);
                    zzao.zza(hashMap, "av", hashMap);
                    zzao.zza(hashMap, "aiid", hashMap);
                    String str3 = str2;
                    if (TextUtils.isEmpty((CharSequence) hashMap.get("adid"))) {
                        z = false;
                    }
                    hashMap.put("_s", String.valueOf(this.bH.zzzg().zza(new zzh(0, str2, str3, z, 0, hashMap))));
                    this.bH.zzzg().zza(new zzab(this.bH, hashMap, zzfj, zzg));
                    return;
                }
                this.bH.zzaca().zzh(hashMap, "Too many hits sent too quickly, rate limiting invoked");
            }
        });
    }

    public void set(String str, String str2) {
        zzaa.zzb((Object) str, (Object) "Key should be non-null");
        if (!TextUtils.isEmpty(str)) {
            this.zzbly.put(str, str2);
        }
    }

    public void setAnonymizeIp(boolean z) {
        set("&aip", zzao.zzax(z));
    }

    public void setAppId(String str) {
        set("&aid", str);
    }

    public void setAppInstallerId(String str) {
        set("&aiid", str);
    }

    public void setAppName(String str) {
        set("&an", str);
    }

    public void setAppVersion(String str) {
        set("&av", str);
    }

    public void setCampaignParamsOnNextHit(Uri uri) {
        if (uri != null && !uri.isOpaque()) {
            CharSequence queryParameter = uri.getQueryParameter("referrer");
            if (!TextUtils.isEmpty(queryParameter)) {
                String str = "http://hostname/?";
                String valueOf = String.valueOf(queryParameter);
                Uri parse = Uri.parse(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                str = parse.getQueryParameter("utm_id");
                if (str != null) {
                    this.bv.put("&ci", str);
                }
                str = parse.getQueryParameter("anid");
                if (str != null) {
                    this.bv.put("&anid", str);
                }
                str = parse.getQueryParameter("utm_campaign");
                if (str != null) {
                    this.bv.put("&cn", str);
                }
                str = parse.getQueryParameter("utm_content");
                if (str != null) {
                    this.bv.put("&cc", str);
                }
                str = parse.getQueryParameter("utm_medium");
                if (str != null) {
                    this.bv.put("&cm", str);
                }
                str = parse.getQueryParameter("utm_source");
                if (str != null) {
                    this.bv.put("&cs", str);
                }
                str = parse.getQueryParameter("utm_term");
                if (str != null) {
                    this.bv.put("&ck", str);
                }
                str = parse.getQueryParameter("dclid");
                if (str != null) {
                    this.bv.put("&dclid", str);
                }
                str = parse.getQueryParameter("gclid");
                if (str != null) {
                    this.bv.put("&gclid", str);
                }
                valueOf = parse.getQueryParameter("aclid");
                if (valueOf != null) {
                    this.bv.put("&aclid", valueOf);
                }
            }
        }
    }

    public void setClientId(String str) {
        set("&cid", str);
    }

    public void setEncoding(String str) {
        set("&de", str);
    }

    public void setHostname(String str) {
        set("&dh", str);
    }

    public void setLanguage(String str) {
        set("&ul", str);
    }

    public void setLocation(String str) {
        set("&dl", str);
    }

    public void setPage(String str) {
        set("&dp", str);
    }

    public void setReferrer(String str) {
        set("&dr", str);
    }

    public void setSampleRate(double d) {
        set("&sf", Double.toString(d));
    }

    public void setScreenColors(String str) {
        set("&sd", str);
    }

    public void setScreenName(String str) {
        set("&cd", str);
    }

    public void setScreenResolution(int i, int i2) {
        if (i >= 0 || i2 >= 0) {
            set("&sr", i + "x" + i2);
        } else {
            zzev("Invalid width or height. The values should be non-negative.");
        }
    }

    public void setSessionTimeout(long j) {
        this.bx.setSessionTimeout(1000 * j);
    }

    public void setTitle(String str) {
        set("&dt", str);
    }

    public void setUseSecure(boolean z) {
        set("useSecure", zzao.zzax(z));
    }

    public void setViewportSize(String str) {
        set("&vp", str);
    }

    void zza(zzan com_google_android_gms_analytics_internal_zzan) {
        zzes("Loading Tracker config values");
        this.bz = com_google_android_gms_analytics_internal_zzan;
        if (this.bz.zzahc()) {
            String trackingId = this.bz.getTrackingId();
            set("&tid", trackingId);
            zza("trackingId loaded", trackingId);
        }
        if (this.bz.zzahd()) {
            trackingId = Double.toString(this.bz.zzahe());
            set("&sf", trackingId);
            zza("Sample frequency loaded", trackingId);
        }
        if (this.bz.zzahf()) {
            int sessionTimeout = this.bz.getSessionTimeout();
            setSessionTimeout((long) sessionTimeout);
            zza("Session timeout loaded", Integer.valueOf(sessionTimeout));
        }
        if (this.bz.zzahg()) {
            boolean zzahh = this.bz.zzahh();
            enableAutoActivityTracking(zzahh);
            zza("Auto activity tracking loaded", Boolean.valueOf(zzahh));
        }
        if (this.bz.zzahi()) {
            zzahh = this.bz.zzahj();
            if (zzahh) {
                set("&aip", "1");
            }
            zza("Anonymize ip loaded", Boolean.valueOf(zzahh));
        }
        enableExceptionReporting(this.bz.zzahk());
    }

    boolean zzaaa() {
        return this.bu;
    }

    protected void zzzy() {
        this.bx.initialize();
        String zzaae = zzzh().zzaae();
        if (zzaae != null) {
            set("&an", zzaae);
        }
        zzaae = zzzh().zzaaf();
        if (zzaae != null) {
            set("&av", zzaae);
        }
    }
}
