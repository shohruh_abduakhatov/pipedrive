package com.pipedrive.views.viewholder.communicationmedium;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.TextView;
import butterknife.BindView;
import com.pipedrive.R;
import com.pipedrive.model.contact.CommunicationMedium;
import com.pipedrive.views.viewholder.ViewHolder;

public class CallRowViewHolder implements ViewHolder {
    @BindView(2131820953)
    TextView mType;
    @BindView(2131820749)
    TextView mValue;

    public void fill(@NonNull Context context, @NonNull CommunicationMedium communicationMedium) {
        this.mValue.setText(communicationMedium.getValue());
        this.mType.setText(communicationMedium.getLabel());
    }

    public int getLayoutResourceId() {
        return R.layout.row_communication_medium_list;
    }
}
