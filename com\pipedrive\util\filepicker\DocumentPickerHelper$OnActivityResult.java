package com.pipedrive.util.filepicker;

import android.content.Intent;

class DocumentPickerHelper$OnActivityResult {
    final Intent data;
    final int requestCode;
    final int resultCode;

    public DocumentPickerHelper$OnActivityResult(int requestCode, int resultCode, Intent data) {
        this.requestCode = requestCode;
        this.resultCode = resultCode;
        this.data = data;
    }
}
