package com.pipedrive.organization.edit;

import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.model.Organization;
import com.pipedrive.store.StoreOrganization;

class DeleteOrganizationTask extends OrganizationCrudTask {
    public DeleteOrganizationTask(Session session, @Nullable OnTaskFinished onTaskFinished) {
        super(session, onTaskFinished);
    }

    protected Boolean doInBackground(Organization... params) {
        Organization organization = params[0];
        organization.setIsActive(false);
        return Boolean.valueOf(new StoreOrganization(getSession()).update(organization));
    }
}
