package com.pipedrive.nearby.cards.cards;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.OnClick;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.model.Activity;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.nearby.model.NearbyItem;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

abstract class SingleCard<NEARBY_ITEM extends NearbyItem<ENTITY>, ENTITY extends BaseDatasourceEntity> extends Card<NEARBY_ITEM> {
    @Nullable
    ENTITY entity;
    @Nullable
    private CardHeader<NEARBY_ITEM, ENTITY> header;
    @BindView(2131821177)
    ActivitySummary lastActivityView;
    @BindView(2131821176)
    ActivitySummary nextActivityView;

    @NonNull
    abstract CardHeader<NEARBY_ITEM, ENTITY> getHeader();

    abstract void onDetailsRequested(@NonNull Long l);

    SingleCard(@NonNull ViewGroup parent, @LayoutRes @NonNull Integer layoutResId) {
        super(parent, layoutResId);
    }

    @CallSuper
    public void bind(@NonNull final Session session, @NonNull final NEARBY_ITEM nearbyItem, @NonNull final LatLng currentLocation) {
        super.bind(session, nearbyItem, currentLocation);
        addSubscription(nearbyItem.getData(session).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<ENTITY>() {
            public void call(ENTITY entity) {
                SingleCard.this.entity = entity;
                SingleCard.this.bind(session, nearbyItem, entity, currentLocation);
            }
        }, getDefaultOnErrorActionForCards()));
    }

    void bind(@NonNull Session session, @NonNull NEARBY_ITEM nearbyItem, @NonNull ENTITY entity, @NonNull LatLng currentLocation) {
        this.header = getHeader();
        this.header.bind(this.headerContainer, session, nearbyItem, entity, Double.valueOf(SphericalUtil.computeDistanceBetween(currentLocation, nearbyItem.getItemLatLng())));
        setupNextAndLastActivities(session, nearbyItem);
    }

    private void setupNextAndLastActivities(@NonNull final Session session, @NonNull NEARBY_ITEM nearbyItem) {
        addSubscription(nearbyItem.getLastActivity(session).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Activity>() {
            public void call(Activity lastActivity) {
                SingleCard.this.lastActivityView.setup(session, lastActivity, R.string.last);
            }
        }, getDefaultOnErrorActionForCards()));
        addSubscription(nearbyItem.getNextActivity(session).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Activity>() {
            public void call(Activity nextActivity) {
                SingleCard.this.nextActivityView.setup(session, nextActivity, R.string.next);
            }
        }, getDefaultOnErrorActionForCards()));
    }

    @OnClick({2131821167})
    void onDetailsButtonClicked() {
        if (this.entity != null && this.entity.getSqlIdOrNull() != null) {
            onDetailsRequested(this.entity.getSqlIdOrNull());
        }
    }

    public void unbind() {
        if (this.header != null) {
            this.header.releaseResources();
        }
        super.unbind();
    }
}
