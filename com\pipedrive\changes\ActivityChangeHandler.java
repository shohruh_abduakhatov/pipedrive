package com.pipedrive.changes;

import android.support.annotation.NonNull;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.pipedrive.application.Session;
import com.pipedrive.changes.Change.ChangeOperationType;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.gson.GsonHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.Activity;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.ConnectionUtil.JsonReaderInterceptor;
import com.pipedrive.util.networking.Response;
import com.pipedrive.util.networking.entities.ActivityEntity;
import java.io.IOException;
import java.lang.reflect.Type;

class ActivityChangeHandler extends ChangeHandler {
    ActivityChangeHandler() {
    }

    @NonNull
    public ChangeHandled handleChange(@NonNull Session session, @NonNull Change activityChange) {
        if (activityChange.getChangeOperationType() != ChangeOperationType.OPERATION_CREATE && activityChange.getChangeOperationType() != ChangeOperationType.OPERATION_UPDATE) {
            LogJourno.reportEvent(EVENT.ChangesHandler_corruptedChangeFound, String.format("Change:[%s]", new Object[]{activityChange}));
            Log.e(new Throwable(String.format("Unknown change type requested for activity: %s", new Object[]{activityChange})));
            return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
        } else if (!ConnectionUtil.isConnected(session.getApplicationContext())) {
            return ChangeHandled.NOT_PROCESSED_PLEASE_RETRY;
        } else {
            boolean isUpdateActivityOperation;
            if (activityChange.getChangeOperationType() == ChangeOperationType.OPERATION_UPDATE) {
                isUpdateActivityOperation = true;
            } else {
                isUpdateActivityOperation = false;
            }
            JsonReaderInterceptor<AnonymousClass1ActivityResponse> activityResponseParser = new JsonReaderInterceptor<AnonymousClass1ActivityResponse>() {
                public AnonymousClass1ActivityResponse interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull AnonymousClass1ActivityResponse responseTemplate) throws IOException {
                    if (jsonReader.peek() == JsonToken.NULL) {
                        jsonReader.skipValue();
                    } else {
                        ActivityEntity activityEntity = (ActivityEntity) GsonHelper.fromJSON(jsonReader, (Type) ActivityEntity.class);
                        if (activityEntity == null) {
                            LogJourno.reportEvent(EVENT.ChangesHandler_changeDataResponseToJsonParsingFailed, String.format("ActivityResponseTemplate:[%s]", new Object[]{responseTemplate}));
                            Log.e(new Throwable(String.format("Parsing of Activity from response failed! Response: %s", new Object[]{responseTemplate})));
                        } else if (responseTemplate.activityUnderChangeSqlId <= 0 || activityEntity.pipedriveId <= 0) {
                            LogJourno.reportEvent(EVENT.ChangesHandler_cannotUpdatePipedriveIdFromChangeDataResponse, String.format("ActivityResponseTemplate:[%s] ActivityFromResponse:[%s]", new Object[]{responseTemplate, activityEntity}));
                            Log.e(new Throwable(String.format("Cannot update PD ID! SQL ID: %s; PD ID: %s", new Object[]{Long.valueOf(responseTemplate.activityUnderChangeSqlId), Integer.valueOf(activityEntity.pipedriveId)})));
                        } else {
                            new ActivitiesDataSource(session).updatePipedriveIdBySqlId(responseTemplate.activityUnderChangeSqlId, activityEntity.pipedriveId);
                        }
                    }
                    return responseTemplate;
                }
            };
            Long activitySqlId = activityChange.getMetaData();
            if (activitySqlId == null) {
                LogJourno.reportEvent(EVENT.ChangesHandler_changeMetadataNotFound, String.format("Change:[%s]", new Object[]{activityChange}));
                Log.e(new Throwable("Change metadata is missing! Cannot find the Activity to change!"));
                return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
            }
            Activity activityUnderChange = (Activity) new ActivitiesDataSource(session).findBySqlId(activitySqlId.longValue());
            if (activityUnderChange == null) {
                LogJourno.reportEvent(EVENT.ChangesHandler_changeDataNotFound, String.format("Change:[%s] SQL id:[%s]", new Object[]{activityChange, activitySqlId}));
                Log.e(new Throwable(String.format("Change metadata is present (%s) but unable to find Activity that was registered for the change!", new Object[]{activitySqlId})));
                return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
            }
            AnonymousClass1ActivityResponse result;
            if (!activityUnderChange.isAllAssociationsExisting()) {
                LogJourno.reportEvent(EVENT.ChangesHandler_notAllChangePipedriveAssociationsExist, String.format("Change:[%s]", new Object[]{activityChange}));
                Log.e(new Throwable(String.format("Activity associations are not all existing! Change: %s", new Object[]{activityChange})));
            }
            ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder(session, "activities");
            if (isUpdateActivityOperation) {
                apiUrlBuilder.appendEncodedPath(Integer.toString(activityUnderChange.getPipedriveId()));
            }
            AnonymousClass1ActivityResponse activityResponse = new Response() {
                long activityUnderChangeSqlId;

                public String toString() {
                    return "ActivityResponse:[activityUnderChangeSqlId=" + this.activityUnderChangeSqlId + "][super.toString:[" + super.toString() + "]]";
                }
            };
            activityResponse.activityUnderChangeSqlId = activitySqlId.longValue();
            if (isUpdateActivityOperation) {
                result = (AnonymousClass1ActivityResponse) ConnectionUtil.requestPut(apiUrlBuilder, ActivityEntity.getJSON(activityUnderChange).toString(), activityResponseParser, activityResponse);
            } else {
                result = (AnonymousClass1ActivityResponse) ConnectionUtil.requestPost(apiUrlBuilder, ActivityEntity.getJSON(activityUnderChange).toString(), activityResponseParser, activityResponse);
            }
            return parseResponse(result, this, activityChange);
        }
    }
}
