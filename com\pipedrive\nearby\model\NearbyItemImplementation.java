package com.pipedrive.nearby.model;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.BaseDataSource;
import com.pipedrive.model.Activity;
import com.pipedrive.model.BaseDatasourceEntity;
import rx.Observable;

interface NearbyItemImplementation<T extends BaseDatasourceEntity> {
    @NonNull
    Observable<T> getData(@NonNull BaseDataSource<T> baseDataSource);

    @NonNull
    Observable<Long> getItemSqlIds();

    @NonNull
    Observable<Activity> getLastActivity(@NonNull Session session, @NonNull String str);

    @NonNull
    Observable<Activity> getNextActivity(@NonNull Session session, @NonNull String str);

    @NonNull
    Boolean hasMultipleIds();
}
