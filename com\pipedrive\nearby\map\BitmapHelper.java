package com.pipedrive.nearby.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.SparseArray;
import com.google.maps.android.ui.IconGenerator;
import com.pipedrive.R;

class BitmapHelper {
    private static final int AGGREGATED_POS = 4;
    private static final int GREEN_CIRCLE_POS = 1;
    private static final int GREY_CIRCLE_POS = 3;
    private static final int NON_AGGREGATED_MAX_CACHE_SIZE = 4;
    private static final int NO_CIRCLE_POS = 0;
    private static final int RED_CIRCLE_POS = 2;
    @NonNull
    private final SparseArray<Bitmap> aggregatedSelectedMarkerBitmaps = new SparseArray();
    @NonNull
    private final SparseArray<Bitmap> aggregatedUnselectedMarkerBitmaps = new SparseArray();
    @NonNull
    private final Context context;
    @NonNull
    private final IconGenerator iconGenerator;
    @NonNull
    private final SparseArray<Bitmap> nonAggregatedSelectedMarkerBitmaps = new SparseArray(4);
    @NonNull
    private final SparseArray<Bitmap> nonAggregatedUnselectedMarkerBitmaps = new SparseArray(4);

    private enum IconDrawableResHolder {
        NO_CIRCLE(R.drawable.shape_marker_unselected, R.drawable.shape_marker_selected, 0),
        GREEN_CIRCLE(R.drawable.shape_marker_unselected_green_circle, R.drawable.shape_marker_selected_green_circle, 1),
        RED_CIRCLE(R.drawable.shape_marker_unselected_red_circle, R.drawable.shape_marker_selected_red_circle, 2),
        GREY_CIRCLE(R.drawable.shape_marker_unselected_grey_circle, R.drawable.shape_marker_selected_grey_circle, 3),
        AGGREGATED(R.drawable.shape_marker_unselected_aggregated, R.drawable.shape_marker_selected_aggregated, 4);
        
        final int position;
        @DrawableRes
        final int selectedDrawableRes;
        @DrawableRes
        final int unselectedDrawableRes;

        private IconDrawableResHolder(int unselectedDrawableRes, int selectedDrawableRes, int position) {
            this.unselectedDrawableRes = unselectedDrawableRes;
            this.selectedDrawableRes = selectedDrawableRes;
            this.position = position;
        }
    }

    BitmapHelper(@NonNull Context context) {
        this.context = context;
        this.iconGenerator = new IconGenerator(context);
    }

    @NonNull
    Bitmap getIconNoCircle(boolean isSelected) {
        return findNonAggregatedBitmapOrCreateAndCache(IconDrawableResHolder.NO_CIRCLE, isSelected);
    }

    @NonNull
    Bitmap getIconGreenCircle(boolean isSelected) {
        return findNonAggregatedBitmapOrCreateAndCache(IconDrawableResHolder.GREEN_CIRCLE, isSelected);
    }

    @NonNull
    Bitmap getIconRedCircle(boolean isSelected) {
        return findNonAggregatedBitmapOrCreateAndCache(IconDrawableResHolder.RED_CIRCLE, isSelected);
    }

    @NonNull
    Bitmap getIconGreyCircle(boolean isSelected) {
        return findNonAggregatedBitmapOrCreateAndCache(IconDrawableResHolder.GREY_CIRCLE, isSelected);
    }

    @NonNull
    Bitmap getAggregatedIcon(int aggregatedItemCount, boolean isSelected) {
        Bitmap cachedBitmap = (Bitmap) (isSelected ? this.aggregatedSelectedMarkerBitmaps : this.aggregatedUnselectedMarkerBitmaps).get(aggregatedItemCount);
        return cachedBitmap != null ? cachedBitmap : createAggregatedBitmapAndCache(aggregatedItemCount, isSelected);
    }

    private void setupIconGenerator(int aggregatedItemCount, boolean isSelected) {
        Drawable drawable = getDrawable(IconDrawableResHolder.AGGREGATED, isSelected);
        int digits = String.valueOf(aggregatedItemCount).length();
        int paddingTop = this.context.getResources().getDimensionPixelSize(R.dimen.aggregated_marker_padding_top);
        int textStyleId = R.style.TextAppearance.Marker;
        int paddingLeft = 0;
        switch (digits) {
            case 1:
                paddingLeft = this.context.getResources().getDimensionPixelSize(R.dimen.aggregated_marker_padding_left_single_digit);
                break;
            case 2:
                paddingLeft = this.context.getResources().getDimensionPixelSize(R.dimen.aggregated_marker_padding_left_double_digit);
                break;
            case 3:
                paddingLeft = this.context.getResources().getDimensionPixelSize(R.dimen.aggregated_marker_padding_left_triple_digit);
                paddingTop = this.context.getResources().getDimensionPixelSize(R.dimen.aggregated_marker_padding_top_triple_digit);
                textStyleId = R.style.TextAppearance.Marker.99_plus;
                break;
        }
        this.iconGenerator.setBackground(drawable);
        this.iconGenerator.setContentPadding(paddingLeft, paddingTop, 0, 0);
        this.iconGenerator.setTextAppearance(textStyleId);
    }

    private Bitmap createAggregatedBitmapAndCache(int aggregatedItemCount, boolean isSelected) {
        String itemCountAsString;
        setupIconGenerator(aggregatedItemCount, isSelected);
        if (aggregatedItemCount < 100) {
            itemCountAsString = String.valueOf(aggregatedItemCount);
        } else {
            itemCountAsString = this.context.getResources().getString(R.string.ninety_nine_plus);
        }
        Bitmap bitmap = this.iconGenerator.makeIcon(itemCountAsString);
        if (isSelected) {
            this.aggregatedSelectedMarkerBitmaps.put(aggregatedItemCount, bitmap);
        } else {
            this.aggregatedUnselectedMarkerBitmaps.put(aggregatedItemCount, bitmap);
        }
        return bitmap;
    }

    private Bitmap findNonAggregatedBitmapOrCreateAndCache(@NonNull IconDrawableResHolder iconDrawableResHolder, boolean isSelected) {
        SparseArray<Bitmap> bitmapSparseArray = isSelected ? this.nonAggregatedSelectedMarkerBitmaps : this.nonAggregatedUnselectedMarkerBitmaps;
        Bitmap cachedBitmap = (Bitmap) bitmapSparseArray.get(iconDrawableResHolder.position);
        if (cachedBitmap != null) {
            return cachedBitmap;
        }
        Bitmap bitmap = drawableToBitmap(iconDrawableResHolder, isSelected);
        bitmapSparseArray.put(iconDrawableResHolder.position, bitmap);
        return bitmap;
    }

    @NonNull
    private Bitmap drawableToBitmap(@NonNull IconDrawableResHolder iconDrawableResHolder, boolean isSelected) {
        Drawable drawable = getDrawable(iconDrawableResHolder, isSelected);
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    private Drawable getDrawable(@NonNull IconDrawableResHolder iconDrawableResHolder, boolean isSelected) {
        return ContextCompat.getDrawable(this.context, isSelected ? iconDrawableResHolder.selectedDrawableRes : iconDrawableResHolder.unselectedDrawableRes);
    }
}
