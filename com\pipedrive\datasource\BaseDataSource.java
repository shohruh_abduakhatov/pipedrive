package com.pipedrive.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.util.CursorHelper;
import java.util.Collections;
import java.util.List;
import rx.Single;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public abstract class BaseDataSource<T extends BaseDatasourceEntity> extends SQLTransactionManager {
    public static final long ERROR_ON_CREATE_OR_UPDATE = -1;

    @NonNull
    protected abstract String getColumnNameForPipedriveId();

    @NonNull
    protected abstract String getColumnNameForSqlId();

    @NonNull
    protected abstract String getTableName();

    public BaseDataSource(SQLiteDatabase dbconn) {
        super(dbconn);
    }

    public synchronized long createOrUpdate(@Nullable T modelEntity) {
        long j;
        if (modelEntity == null) {
            j = -1;
        } else {
            j = createOrUpdate(getContentValues(modelEntity));
            if (j != -1) {
                modelEntity.setSqlIdOrNull(Long.valueOf(j));
                onSuccessfulCreateOrUpdate(modelEntity);
            }
        }
        return j;
    }

    protected void onSuccessfulCreateOrUpdate(@NonNull T t) {
    }

    private synchronized long createOrUpdate(@Nullable ContentValues values) {
        long j;
        if (values == null) {
            j = -1;
        } else {
            String[] whereArgs;
            SQLiteDatabase transactionalDBConnection;
            String tableName;
            int nrOfRowsAffected;
            long sqlId = values.getAsLong(getColumnNameForSqlId()) != null ? values.getAsLong(getColumnNameForSqlId()).longValue() : 0;
            int pipedriveId = values.getAsInteger(getColumnNameForPipedriveId()) != null ? values.getAsInteger(getColumnNameForPipedriveId()).intValue() : 0;
            boolean sqlIDExists = sqlId != 0;
            boolean pdIDExists = pipedriveId != 0;
            boolean updateRow = sqlIDExists || pdIDExists;
            String whereClause = sqlIDExists ? String.format("%s == ?", new Object[]{getColumnNameForSqlId()}) : pdIDExists ? String.format("%s == ?", new Object[]{getColumnNameForPipedriveId()}) : null;
            if (updateRow) {
                whereArgs = new String[1];
                whereArgs[0] = sqlIDExists ? String.valueOf(sqlId) : String.valueOf(pipedriveId);
            } else {
                whereArgs = null;
            }
            if (updateRow) {
                transactionalDBConnection = getTransactionalDBConnection();
                tableName = getTableName();
                nrOfRowsAffected = !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.update(tableName, values, whereClause, whereArgs) : SQLiteInstrumentation.update(transactionalDBConnection, tableName, values, whereClause, whereArgs);
            } else {
                nrOfRowsAffected = 0;
            }
            if (nrOfRowsAffected <= 0) {
                transactionalDBConnection = getTransactionalDBConnection();
                tableName = getTableName();
                j = !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.insert(tableName, null, values) : SQLiteInstrumentation.insert(transactionalDBConnection, tableName, null, values);
                if (j == -1) {
                    j = -1;
                }
            } else {
                if (!sqlIDExists) {
                    sqlId = getCreateOrUpdateSqlId(pipedriveId);
                }
                j = sqlId;
            }
        }
        return j;
    }

    private long getCreateOrUpdateSqlId(int pipedriveId) {
        String[] columns = new String[]{getColumnNameForSqlId()};
        String selection = String.format("%s == ?", new Object[]{getColumnNameForPipedriveId()});
        String[] selectionArgs = new String[]{String.valueOf(pipedriveId)};
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String tableName = getTableName();
        Cursor cursor = !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.query(tableName, columns, selection, selectionArgs, null, null, null) : SQLiteInstrumentation.query(transactionalDBConnection, tableName, columns, selection, selectionArgs, null, null, null);
        try {
            cursor.moveToFirst();
            int sqlIdColumnIndex = cursor.getColumnIndex(getColumnNameForSqlId());
            if (cursor.isNull(sqlIdColumnIndex)) {
                cursor.close();
                return -1;
            }
            long j = cursor.getLong(sqlIdColumnIndex);
            return j;
        } finally {
            cursor.close();
        }
    }

    @NonNull
    public Cursor findBySqlIdCursor(long sqlId) {
        return query(getTransactionalDBConnection(), getTableName(), getAllColumns(), String.format("%s = ?", new Object[]{getColumnNameForSqlId()}), new String[]{String.valueOf(sqlId)}, null, null, null);
    }

    @Nullable
    public T findBySqlId(@IntRange(from = 0) long sqlId) {
        Cursor cursor = findBySqlIdCursor(sqlId);
        try {
            if (cursor.getCount() <= 0) {
                return null;
            }
            cursor.moveToFirst();
            T deflateCursor = deflateCursor(cursor);
            cursor.close();
            return deflateCursor;
        } finally {
            cursor.close();
        }
    }

    @NonNull
    Cursor findByPipedriveIdCursor(long pipedriveId) {
        return query(getTransactionalDBConnection(), getTableName(), getAllColumns(), String.format("%s = ?", new Object[]{getColumnNameForPipedriveId()}), new String[]{String.valueOf(pipedriveId)}, null, null, null);
    }

    @Nullable
    public T findByPipedriveId(long pipedriveId) {
        Cursor cursor = findByPipedriveIdCursor(pipedriveId);
        try {
            if (cursor.getCount() <= 0) {
                return null;
            }
            cursor.moveToFirst();
            T deflateCursor = deflateCursor(cursor);
            cursor.close();
            return deflateCursor;
        } finally {
            cursor.close();
        }
    }

    @NonNull
    private Cursor findAllCursor() {
        return query(getTransactionalDBConnection(), getTableName(), getAllColumns(), null, null, null, null, null);
    }

    @NonNull
    public List<T> findAll() {
        return deflateCursorToList(findAllCursor());
    }

    public boolean deleteBySqlId(long sqlId) {
        String whereClause = String.format("%s == ?", new Object[]{getColumnNameForSqlId()});
        String[] whereArgs = new String[]{String.valueOf(sqlId)};
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String tableName = getTableName();
        if ((!(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.delete(tableName, whereClause, whereArgs) : SQLiteInstrumentation.delete(transactionalDBConnection, tableName, whereClause, whereArgs)) > 0) {
            return true;
        }
        return false;
    }

    public int deleteAll() {
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String tableName = getTableName();
        return !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.delete(tableName, null, null) : SQLiteInstrumentation.delete(transactionalDBConnection, tableName, null, null);
    }

    public long updatePipedriveIdBySqlId(long sqlId, int newPipedriveId) {
        if (sqlId <= 0 || newPipedriveId <= 0) {
            return -1;
        }
        ContentValues values = new ContentValues();
        values.put(getColumnNameForPipedriveId(), Integer.valueOf(newPipedriveId));
        String whereClause = String.format("%s = ?", new Object[]{getColumnNameForSqlId()});
        String[] whereArgs = new String[]{String.valueOf(sqlId)};
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String tableName = getTableName();
        int rowsAffected = !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.update(tableName, values, whereClause, whereArgs) : SQLiteInstrumentation.update(transactionalDBConnection, tableName, values, whereClause, whereArgs);
        if (rowsAffected <= 0) {
            return -1;
        }
        return (long) rowsAffected;
    }

    private Cursor query(SQLiteDatabase sqLiteDatabase, String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        return !(sqLiteDatabase instanceof SQLiteDatabase) ? sqLiteDatabase.query(table, columns, selection, selectionArgs, groupBy, having, orderBy) : SQLiteInstrumentation.query(sqLiteDatabase, table, columns, selection, selectionArgs, groupBy, having, orderBy);
    }

    protected Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        return query(getTransactionalDBConnection(), table, columns, selection, selectionArgs, groupBy, having, orderBy);
    }

    protected Cursor query(@NonNull String table, @Nullable String[] columns, @NonNull SelectionHolder selectionHolder, @Nullable String orderBy) {
        String selection = selectionHolder.getSelection();
        String[] selectionArgs = selectionHolder.getSelectionArgs();
        return !(this instanceof SQLiteDatabase) ? query(table, columns, selection, selectionArgs, null, null, orderBy) : SQLiteInstrumentation.query((SQLiteDatabase) this, table, columns, selection, selectionArgs, null, null, orderBy);
    }

    @NonNull
    protected ContentValues getContentValues(@NonNull T modelClass) {
        ContentValues contentValues = new ContentValues();
        if (modelClass.isStored()) {
            contentValues.put(getColumnNameForSqlId(), modelClass.getSqlIdOrNull());
        }
        if (modelClass.isExisting()) {
            contentValues.put(getColumnNameForPipedriveId(), modelClass.getPipedriveIdOrNull());
        }
        return contentValues;
    }

    @NonNull
    protected String[] getAllColumns() {
        throw new UnsupportedOperationException("For using findBySqlIdCursor please implement getAllColumns()!");
    }

    @Nullable
    protected T deflateCursor(@NonNull Cursor cursor) {
        throw new UnsupportedOperationException("For using findBySqlIdCursor please implement deflateCursor()!");
    }

    @NonNull
    protected List<T> deflateCursorToList(@NonNull Cursor cursor) {
        return (List) CursorHelper.observeAllRows(cursor).map(new Func1<Cursor, T>() {
            public T call(Cursor cursor) {
                return BaseDataSource.this.deflateCursor(cursor);
            }
        }).toList().toBlocking().singleOrDefault(Collections.emptyList());
    }

    @NonNull
    public Single<T> findBySqlIdRx(@NonNull Long sqlId) {
        return Single.just(sqlId).subscribeOn(Schedulers.io()).map(new Func1<Long, T>() {
            public T call(Long id) {
                return BaseDataSource.this.findBySqlId(id.longValue());
            }
        });
    }
}
