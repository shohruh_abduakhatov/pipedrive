package com.pipedrive.nearby.cards.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.ViewGroup;
import com.google.android.gms.maps.model.LatLng;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.cards.cards.Card;
import com.pipedrive.nearby.model.NearbyItem;
import java.util.List;

public abstract class CardAdapter extends Adapter<Card> {
    private static final int VIEW_TYPE_MULTIPLE = 0;
    private static final int VIEW_TYPE_SINGLE = 1;
    @NonNull
    private final LatLng mCurrentLocation;
    @NonNull
    private final List<? extends NearbyItem> mNearbyItemsList;
    @NonNull
    private final Session mSession;

    abstract Card createAggregatedCard(@NonNull ViewGroup viewGroup);

    abstract Card createSingleCard(@NonNull ViewGroup viewGroup);

    CardAdapter(@NonNull Session session, @NonNull List<? extends NearbyItem> itemsList, @NonNull LatLng currentLocation) {
        this.mSession = session;
        this.mNearbyItemsList = itemsList;
        this.mCurrentLocation = currentLocation;
    }

    public Card onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0) {
            return createAggregatedCard(parent);
        }
        return createSingleCard(parent);
    }

    public void onBindViewHolder(Card holder, int position) {
        holder.bind(this.mSession, (NearbyItem) this.mNearbyItemsList.get(position), this.mCurrentLocation);
    }

    public int getItemCount() {
        return this.mNearbyItemsList.size();
    }

    public int getItemViewType(int position) {
        if (((NearbyItem) this.mNearbyItemsList.get(position)).isAggregated().booleanValue()) {
            return 0;
        }
        return 1;
    }

    public void onViewRecycled(Card holder) {
        holder.unbind();
    }

    @Nullable
    public NearbyItem getItem(@NonNull Integer index) {
        if (index.intValue() < 0 || index.intValue() >= this.mNearbyItemsList.size()) {
            return null;
        }
        return (NearbyItem) this.mNearbyItemsList.get(index.intValue());
    }

    @NonNull
    public Integer indexOf(@NonNull NearbyItem nearbyItem) {
        return Integer.valueOf(this.mNearbyItemsList.indexOf(nearbyItem));
    }
}
