package com.pipedrive.nearby.cards.cards;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import butterknife.BindDimen;
import butterknife.ButterKnife;
import com.pipedrive.R;
import com.pipedrive.logging.Log;
import com.pipedrive.nearby.cards.NearbyRequestEventBus;
import com.pipedrive.nearby.util.Irrelevant;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class EmptyCard extends CardView {
    @NonNull
    private final CompositeSubscription compositeSubscription = new CompositeSubscription();
    @BindDimen(2131493068)
    int defaultElevation;

    public EmptyCard(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.nearby_empty_card_view, this);
        ButterKnife.bind((View) this);
        setCardElevation((float) this.defaultElevation);
    }

    private void hide() {
        if (getVisibility() == 0) {
            setVisibility(8);
        }
    }

    private void show() {
        if (getVisibility() == 8) {
            setVisibility(0);
        }
    }

    public void releaseResources() {
        this.compositeSubscription.clear();
    }

    public void bind() {
        this.compositeSubscription.add(CardEventBus.INSTANCE.showEmptyCardEvents().observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Irrelevant>() {
            public void call(Irrelevant irrelevant) {
                EmptyCard.this.show();
            }
        }, onError()));
        this.compositeSubscription.add(NearbyRequestEventBus.INSTANCE.requestStartedEvents().observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Irrelevant>() {
            public void call(Irrelevant irrelevant) {
                EmptyCard.this.hide();
            }
        }, onError()));
    }

    @NonNull
    private Action1<Throwable> onError() {
        return new Action1<Throwable>() {
            public void call(Throwable throwable) {
                Log.e(throwable);
            }
        };
    }
}
