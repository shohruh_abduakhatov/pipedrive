package com.pipedrive.note;

import android.support.annotation.MainThread;
import android.support.annotation.Nullable;
import com.pipedrive.model.HtmlContent;

@MainThread
public interface HtmlEditorView {
    void onDataRequested(@Nullable HtmlContent htmlContent);
}
