package com.pipedrive.views.navigation;

public interface OnNavigationButtonClickListener {
    void onNavigationButtonClicked(NavigationToolbarButton navigationToolbarButton);
}
