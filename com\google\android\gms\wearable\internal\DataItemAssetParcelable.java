package com.google.android.gms.wearable.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.wearable.DataItemAsset;

@KeepName
public class DataItemAssetParcelable extends AbstractSafeParcelable implements ReflectedParcelable, DataItemAsset {
    public static final Creator<DataItemAssetParcelable> CREATOR = new zzab();
    final int mVersionCode;
    private final String zzbcn;
    private final String zzboa;

    DataItemAssetParcelable(int i, String str, String str2) {
        this.mVersionCode = i;
        this.zzboa = str;
        this.zzbcn = str2;
    }

    public DataItemAssetParcelable(DataItemAsset dataItemAsset) {
        this.mVersionCode = 1;
        this.zzboa = (String) zzaa.zzy(dataItemAsset.getId());
        this.zzbcn = (String) zzaa.zzy(dataItemAsset.getDataItemKey());
    }

    public /* synthetic */ Object freeze() {
        return zzcmv();
    }

    public String getDataItemKey() {
        return this.zzbcn;
    }

    public String getId() {
        return this.zzboa;
    }

    public boolean isDataValid() {
        return true;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("DataItemAssetParcelable[");
        stringBuilder.append("@");
        stringBuilder.append(Integer.toHexString(hashCode()));
        if (this.zzboa == null) {
            stringBuilder.append(",noid");
        } else {
            stringBuilder.append(Table.COMMA_SEP);
            stringBuilder.append(this.zzboa);
        }
        stringBuilder.append(", key=");
        stringBuilder.append(this.zzbcn);
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzab.zza(this, parcel, i);
    }

    public DataItemAsset zzcmv() {
        return this;
    }
}
