package com.pipedrive.views.common;

import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.pipedrive.R;

public abstract class LayoutWithUnderlineAndLabel<V extends View> extends FrameLayout {
    @NonNull
    @BindView(2131821006)
    ViewGroup mContentLayout;
    @NonNull
    @BindView(2131821005)
    TextView mLabelView;
    @NonNull
    protected V mMainView;
    @NonNull
    @BindView(2131820640)
    View mUnderlineView;

    @NonNull
    protected abstract V createMainView(@NonNull Context context, @Nullable AttributeSet attributeSet, int i);

    public LayoutWithUnderlineAndLabel(Context context) {
        this(context, null);
    }

    public LayoutWithUnderlineAndLabel(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LayoutWithUnderlineAndLabel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        inflate(context, R.layout.layout_underlined_layout_with_label, this);
        ButterKnife.bind(this);
        setupLabel();
        setupMainView(context, attrs, defStyle);
    }

    private void setupLabel() {
        int labelTextResourceId = getLabelTextResourceId();
        if (labelTextResourceId > 0) {
            this.mLabelView.setText(labelTextResourceId);
        } else {
            this.mLabelView.setVisibility(8);
        }
    }

    private void setupMainView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        ViewGroup viewGroup = this.mContentLayout;
        View createMainView = createMainView(context, attrs, defStyle);
        this.mMainView = createMainView;
        viewGroup.addView(createMainView, new LayoutParams(-1, -2));
    }

    @StringRes
    protected int getLabelTextResourceId() {
        return 0;
    }

    protected void hideLabel() {
        this.mLabelView.setVisibility(4);
    }

    protected void showLabel() {
        this.mLabelView.setVisibility(0);
    }

    @NonNull
    protected V getMainView() {
        return this.mMainView;
    }

    protected void dispatchSaveInstanceState(SparseArray<Parcelable> container) {
        super.dispatchFreezeSelfOnly(container);
    }

    protected void dispatchRestoreInstanceState(SparseArray<Parcelable> container) {
        super.dispatchThawSelfOnly(container);
    }
}
