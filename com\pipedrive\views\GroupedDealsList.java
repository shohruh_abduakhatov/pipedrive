package com.pipedrive.views;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CursorAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.instrumentation.AsyncTaskInstrumentation;
import com.newrelic.agent.android.tracing.Trace;
import com.pipedrive.R;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.contentproviders.DealsContentProvider;
import com.pipedrive.loaders.DealsValueLoader;
import com.pipedrive.logging.Log;
import com.pipedrive.model.Deal;
import com.pipedrive.model.DealStatus;
import com.pipedrive.tasks.CurrencyTaskResult;
import com.pipedrive.util.FiltersUtil;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.util.formatter.MonetaryFormatter;
import com.pipedrive.views.viewholder.ViewHolderBuilder;
import com.pipedrive.views.viewholder.deal.DealRowViewHolderForDealsList;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.Executor;

public class GroupedDealsList extends LinearLayout {
    public static final int ID_UNDEFINED = -1;
    private static final String TAG = GroupedDealsList.class.getSimpleName();
    private final int LOADER_ID_UNDEFINED;
    private DealsAdapter adapter;
    private boolean canShowEmptyView;
    private int currentSectionHeaderForStickyHeader;
    private LoaderCallbacks<Cursor> cursorLoaderListener;
    private ListView dealsListView;
    private LoaderCallbacks<CurrencyTaskResult> dealsSumLoaderListener;
    private RelativeLayout layoutEmpty;
    private View layoutStickyDealRowHeader;
    private long mContactSqlId;
    private Context mContext;
    private boolean mEmptyRowVisible;
    private LoaderManager mLoaderManager;
    @Nullable
    private OnDealClickListener mOnDealClickListener;
    private long mOrganizationSqlId;
    private Session mSession;
    private int mStageId;
    private View mainView;
    private DealHeaderViewHolder stickyDealHeaderViewHolder;

    private class DealHeaderViewHolder {
        TextView dealsCount = null;
        TextView status = null;
        TextView value = null;

        DealHeaderViewHolder(View v) {
            this.status = (TextView) v.findViewById(R.id.status);
            this.dealsCount = (TextView) v.findViewById(R.id.deals_count);
            this.value = (TextView) v.findViewById(R.id.value);
        }
    }

    private static class DealRow {
        static final int ROW_TYPE_DATA = -2147483647;
        static final int ROW_TYPE_SECTION_LOST = -2147483643;
        static final int ROW_TYPE_SECTION_OPEN = -2147483645;
        static final int ROW_TYPE_SECTION_WON = -2147483644;
        static final int ROW_TYPE_UNDEFINED = Integer.MIN_VALUE;
        public Deal deal;
        public RowSection section;
        public int type;

        private DealRow() {
            this.type = ROW_TYPE_DATA;
        }
    }

    private final class DealsAdapter extends CursorAdapter {
        private DealRow getItem_cachedItem = null;
        private int getItem_previousPosition = Integer.MIN_VALUE;
        private LayoutInflater mLayoutInflater;
        private ConcurrentSkipListMap<Integer, Integer> mSectionPositions = new ConcurrentSkipListMap();
        private Map<Integer, RowSection> mSections = new ConcurrentSkipListMap();
        @NonNull
        private Session mSession;

        DealsAdapter(@NonNull Session session, Context context) {
            super(context, null, 2);
            this.mLayoutInflater = LayoutInflater.from(context);
            this.mSession = session;
        }

        void setSectionPositions(Pair<ConcurrentSkipListMap<Integer, Integer>, ConcurrentSkipListMap<Integer, RowSection>> result) {
            this.mSectionPositions = (ConcurrentSkipListMap) result.first;
            this.mSections = (Map) result.second;
        }

        private int getNextHeaderPosition(int firstVisibleItem) {
            if (this.mSectionPositions != null) {
                for (Integer sectionPos : this.mSectionPositions.keySet()) {
                    if (sectionPos.intValue() > firstVisibleItem) {
                        return sectionPos.intValue();
                    }
                }
            }
            return Integer.MAX_VALUE;
        }

        private int getCurrentSectionHeader(int position) {
            if (this.mSectionPositions != null) {
                Set<Integer> reversedKeySet = this.mSectionPositions.descendingKeySet();
                int i = reversedKeySet.size() - 1;
                for (Integer sectionMarkerPosition : reversedKeySet) {
                    if (sectionMarkerPosition.intValue() <= position) {
                        return ((Integer) this.mSectionPositions.get(sectionMarkerPosition)).intValue();
                    }
                    i--;
                }
            }
            return Integer.MIN_VALUE;
        }

        public int getCount() {
            return getCursor() == null ? 0 : super.getCount() + this.mSectionPositions.size();
        }

        Pair<ConcurrentSkipListMap<Integer, Integer>, ConcurrentSkipListMap<Integer, RowSection>> composeHeadersBlocking(Cursor cursor) {
            ConcurrentSkipListMap<Integer, Integer> headerSectionsPositions = new ConcurrentSkipListMap();
            ConcurrentSkipListMap<Integer, RowSection> headerSections = new ConcurrentSkipListMap();
            int openCount = 0;
            int wonCount = 0;
            int lostCount = 0;
            if (cursor != null) {
                int listViewPosition = 0;
                int i = 0;
                while (i < cursor.getCount()) {
                    int sectionType = -2147483647;
                    cursor.moveToPosition(i);
                    Deal deal = DealsContentProvider.dealsTableCursorToDeal(this.mSession.getDatabase(), cursor);
                    if (deal.getStatus() == DealStatus.OPEN) {
                        if (headerSectionsPositions.containsValue(Integer.valueOf(-2147483645))) {
                            openCount++;
                        } else {
                            sectionType = -2147483645;
                        }
                    } else if (deal.getStatus() == DealStatus.WON) {
                        if (headerSectionsPositions.containsValue(Integer.valueOf(-2147483644))) {
                            wonCount++;
                        } else {
                            sectionType = -2147483644;
                        }
                    } else if (!headerSectionsPositions.containsValue(Integer.valueOf(-2147483643)) && deal.getStatus() == DealStatus.LOST) {
                        sectionType = -2147483643;
                    }
                    if (sectionType != -2147483647) {
                        headerSectionsPositions.put(Integer.valueOf(listViewPosition), Integer.valueOf(sectionType));
                        headerSections.put(Integer.valueOf(sectionType), new RowSection(sectionType));
                        if (sectionType == -2147483643) {
                            break;
                        }
                    } else {
                        i++;
                    }
                    listViewPosition++;
                }
                lostCount = (cursor.getCount() - openCount) - wonCount;
            }
            setDealsCountForHeaders(headerSections, openCount, wonCount, lostCount);
            return new Pair(headerSectionsPositions, headerSections);
        }

        private void setDealsCountForHeaders(Map<Integer, RowSection> headerSections, int openDealsCount, int wonDealsCount, int lostDealsCount) {
            if (headerSections != null) {
                if (headerSections.containsKey(Integer.valueOf(-2147483645))) {
                    getDealHeaderSection(-2147483645, headerSections).count = openDealsCount;
                }
                if (headerSections.containsKey(Integer.valueOf(-2147483644))) {
                    getDealHeaderSection(-2147483644, headerSections).count = wonDealsCount;
                }
                if (headerSections.containsKey(Integer.valueOf(-2147483643))) {
                    getDealHeaderSection(-2147483643, headerSections).count = lostDealsCount;
                }
            }
        }

        RowSection getDealHeaderSection(int sectionType) {
            return getDealHeaderSection(sectionType, this.mSections);
        }

        RowSection getDealHeaderSection(int sectionType, Map<Integer, RowSection> headerSections) {
            return (RowSection) headerSections.get(Integer.valueOf(sectionType));
        }

        private int getCursorPositionForListViewPosition(int listViewPosition) {
            if (this.mSectionPositions.isEmpty()) {
                return listViewPosition;
            }
            if (this.mSectionPositions.containsKey(Integer.valueOf(listViewPosition))) {
                return ((Integer) this.mSectionPositions.get(Integer.valueOf(listViewPosition))).intValue();
            }
            Set<Integer> reversedKeySet = this.mSectionPositions.descendingKeySet();
            int i = reversedKeySet.size() - 1;
            for (Integer sectionMarkerPosition : reversedKeySet) {
                if (listViewPosition > sectionMarkerPosition.intValue()) {
                    return listViewPosition - (i + 1);
                }
                i--;
            }
            return listViewPosition;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            DealRowViewHolderForDealsList viewHolderDeal = null;
            DealHeaderViewHolder viewHolderDealHeader = null;
            if (!(convertView == null || convertView.getTag() == null)) {
                if (convertView.getTag() instanceof DealRowViewHolderForDealsList) {
                    viewHolderDeal = (DealRowViewHolderForDealsList) convertView.getTag();
                } else if (convertView.getTag() instanceof DealHeaderViewHolder) {
                    viewHolderDealHeader = (DealHeaderViewHolder) convertView.getTag();
                }
            }
            DealRow dealRow = (DealRow) getItem(position);
            View rowView = convertView;
            if (dealRow.type == -2147483647) {
                if (viewHolderDeal == null) {
                    rowView = ViewHolderBuilder.inflateViewAndTagWithViewHolder(parent.getContext(), parent, false, new DealRowViewHolderForDealsList());
                }
            } else if (viewHolderDealHeader == null && (dealRow.type == -2147483645 || dealRow.type == -2147483644 || dealRow.type == -2147483643)) {
                rowView = this.mLayoutInflater.inflate(R.layout.row_deal_deals_list_header, parent, false);
                viewHolderDealHeader = new DealHeaderViewHolder(rowView);
                rowView.setTag(viewHolderDealHeader);
            }
            bindView(dealRow, viewHolderDeal, viewHolderDealHeader);
            return rowView;
        }

        public Object getItem(int position) {
            if (this.getItem_cachedItem != null && position == this.getItem_previousPosition) {
                return this.getItem_cachedItem;
            }
            Object dealRow = new DealRow();
            if (getCursor() == null || getCursor().isClosed()) {
                this.getItem_cachedItem = null;
                this.getItem_previousPosition = Integer.MIN_VALUE;
                return dealRow;
            }
            int cursorPosition = getCursorPositionForListViewPosition(position);
            if (cursorPosition < 0 || getCursor().moveToPosition(cursorPosition)) {
                if (cursorPosition == -2147483645 || cursorPosition == -2147483644 || cursorPosition == -2147483643) {
                    dealRow.type = cursorPosition;
                } else {
                    dealRow.deal = DealsContentProvider.dealsTableCursorToDeal(this.mSession.getDatabase(), getCursor());
                }
                this.getItem_cachedItem = dealRow;
                this.getItem_previousPosition = position;
                return this.getItem_cachedItem;
            }
            this.getItem_cachedItem = null;
            this.getItem_previousPosition = Integer.MIN_VALUE;
            throw new IllegalStateException("Couldn't move cursor to position " + cursorPosition);
        }

        public int getViewTypeCount() {
            return 3;
        }

        public int getItemViewType(int position) {
            switch (((DealRow) getItem(position)).type) {
                case Integer.MIN_VALUE:
                    return 0;
                case -2147483647:
                    return 1;
                case -2147483645:
                case -2147483644:
                case -2147483643:
                    return 2;
                default:
                    return -1;
            }
        }

        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return null;
        }

        public void bindView(View v, Context context, Cursor cursor) {
        }

        void fillSectionHeaderData(DealHeaderViewHolder viewHolderDealHeader, int dealRowType) {
            if (viewHolderDealHeader != null) {
                if (viewHolderDealHeader.status != null) {
                    String status;
                    if (dealRowType == -2147483645) {
                        status = GroupedDealsList.this.getResources().getString(R.string.deals_list_section_open);
                    } else if (dealRowType == -2147483644) {
                        status = GroupedDealsList.this.getResources().getString(R.string.deals_list_section_won);
                    } else {
                        status = GroupedDealsList.this.getResources().getString(R.string.deals_list_section_lost);
                    }
                    viewHolderDealHeader.status.setText(status);
                }
                RowSection section = (RowSection) this.mSections.get(Integer.valueOf(dealRowType));
                if (section != null) {
                    if (viewHolderDealHeader.value != null) {
                        if (TextUtils.isEmpty(section.value)) {
                            viewHolderDealHeader.value.setVisibility(8);
                        } else {
                            viewHolderDealHeader.value.setVisibility(0);
                            viewHolderDealHeader.value.setText("· " + section.value);
                        }
                    }
                    if (viewHolderDealHeader.dealsCount == null) {
                        return;
                    }
                    if (section.count > 0) {
                        viewHolderDealHeader.dealsCount.setVisibility(0);
                        viewHolderDealHeader.dealsCount.setText(GroupedDealsList.this.getResources().getQuantityString(R.plurals.contact_activity_summary_deals, section.count, new Object[]{Integer.valueOf(section.count)}));
                        return;
                    }
                    viewHolderDealHeader.dealsCount.setVisibility(8);
                }
            }
        }

        private void bindView(@NonNull DealRow dealRow, @Nullable DealRowViewHolderForDealsList viewHolderDeal, @Nullable DealHeaderViewHolder viewHolderDealHeader) {
            if ((dealRow.type == -2147483645 || dealRow.type == -2147483644 || dealRow.type == -2147483643) && viewHolderDealHeader != null) {
                fillSectionHeaderData(viewHolderDealHeader, dealRow.type);
            } else if (dealRow.type == -2147483647 && viewHolderDeal != null && dealRow.deal != null) {
                viewHolderDeal.fill(this.mSession, GroupedDealsList.this.mContext, dealRow.deal);
            }
        }

        void setDealsGroupSum(CurrencyTaskResult result) {
            if (this.mSections != null && this.mSections.containsKey(Integer.valueOf(result.getDealsGroupId()))) {
                String currencyCode;
                if (result.getDefaultCurrency() != null) {
                    currencyCode = result.getDefaultCurrency().getCode();
                } else {
                    currencyCode = this.mSession.getUserSettingsDefaultCurrencyCode();
                }
                ((RowSection) this.mSections.get(Integer.valueOf(result.getDealsGroupId()))).value = new MonetaryFormatter(this.mSession).format(Double.valueOf(result.getDealsGroupValue()), currencyCode);
                if (GroupedDealsList.this.adapter != null) {
                    GroupedDealsList.this.adapter.notifyDataSetChanged();
                }
            }
        }
    }

    public interface OnDealClickListener {
        void onDealClicked(Deal deal);
    }

    public static class RowSection {
        static final int POSITION_UNDEFINED = Integer.MAX_VALUE;
        public int count = 0;
        int sectionType = Integer.MIN_VALUE;
        public String value = null;

        RowSection(int sectionType) {
            this.sectionType = sectionType;
        }
    }

    public GroupedDealsList(Context context) {
        this(context, null);
    }

    public void setEmptyRowVisible(boolean emptyRowVisible) {
        this.mEmptyRowVisible = emptyRowVisible;
    }

    public void setLoaderManager(LoaderManager loaderManager) {
        this.mLoaderManager = loaderManager;
        fillData();
    }

    public void setStageId(int stageId) {
        this.mStageId = stageId;
    }

    public void setContactSqlId(long contactSqlId) {
        this.mContactSqlId = contactSqlId;
    }

    public void setOrganizationSqlId(long organizationSqlId) {
        this.mOrganizationSqlId = organizationSqlId;
    }

    public GroupedDealsList(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.canShowEmptyView = true;
        this.mEmptyRowVisible = false;
        this.LOADER_ID_UNDEFINED = Integer.MIN_VALUE;
        this.mLoaderManager = null;
        this.mStageId = -1;
        this.mContactSqlId = -1;
        this.mOrganizationSqlId = -1;
        this.stickyDealHeaderViewHolder = null;
        this.cursorLoaderListener = new LoaderCallbacks<Cursor>() {
            public Loader<Cursor> onCreateLoader(int id, Bundle args) {
                return GroupedDealsList.this.createLoader(id);
            }

            public void onLoadFinished(Loader<Cursor> loader, final Cursor data) {
                int loaderID = Integer.MIN_VALUE;
                if (GroupedDealsList.this.mStageId != -1) {
                    loaderID = GroupedDealsList.this.mStageId + 100;
                } else if (GroupedDealsList.this.mContactSqlId > 0) {
                    loaderID = 50;
                } else if (GroupedDealsList.this.mOrganizationSqlId > 0) {
                    loaderID = 70;
                }
                if (data != null && loader.getId() == loaderID) {
                    AnonymousClass1 anonymousClass1 = new TraceFieldInterface() {
                        public Trace _nr_trace;

                        public void _nr_setTrace(Trace trace) {
                            try {
                                this._nr_trace = trace;
                            } catch (Exception e) {
                            }
                        }

                        protected Pair<ConcurrentSkipListMap<Integer, Integer>, ConcurrentSkipListMap<Integer, RowSection>> doInBackground(Integer... params) {
                            return GroupedDealsList.this.adapter.composeHeadersBlocking(data);
                        }

                        protected void onPostExecute(Pair<ConcurrentSkipListMap<Integer, Integer>, ConcurrentSkipListMap<Integer, RowSection>> result) {
                            GroupedDealsList.this.adapter.setSectionPositions(result);
                            GroupedDealsList.this.adapter.swapCursor(data);
                            GroupedDealsList.this.initDealsSumLoaders();
                            GroupedDealsList.this.toggleLayoutEmptyVisibility();
                        }
                    };
                    Executor executor = AsyncTask.THREAD_POOL_EXECUTOR;
                    Integer[] numArr = new Integer[0];
                    if (anonymousClass1 instanceof AsyncTask) {
                        AsyncTaskInstrumentation.executeOnExecutor(anonymousClass1, executor, numArr);
                    } else {
                        anonymousClass1.executeOnExecutor(executor, numArr);
                    }
                }
            }

            public void onLoaderReset(Loader<Cursor> loader) {
                int loaderID = Integer.MIN_VALUE;
                if (GroupedDealsList.this.mStageId != -1) {
                    loaderID = GroupedDealsList.this.mStageId + 100;
                } else if (GroupedDealsList.this.mContactSqlId > 0) {
                    loaderID = 50;
                } else if (GroupedDealsList.this.mOrganizationSqlId > 0) {
                    loaderID = 70;
                }
                if (loader.getId() == loaderID) {
                    GroupedDealsList.this.adapter.swapCursor(null);
                    GroupedDealsList.this.toggleLayoutEmptyVisibility();
                }
            }
        };
        this.dealsSumLoaderListener = new LoaderCallbacks<CurrencyTaskResult>() {
            public Loader<CurrencyTaskResult> onCreateLoader(int id, Bundle args) {
                return GroupedDealsList.this.createLoader(id);
            }

            public void onLoadFinished(Loader<CurrencyTaskResult> loader, CurrencyTaskResult data) {
                int openDealsCurrenciesLoaderID = Integer.MIN_VALUE;
                int wonDealsCurrenciesLoaderID = Integer.MIN_VALUE;
                int lostDealsCurrenciesLoaderID = Integer.MIN_VALUE;
                if (GroupedDealsList.this.mContactSqlId > 0 || GroupedDealsList.this.mOrganizationSqlId > 0) {
                    openDealsCurrenciesLoaderID = 60;
                    wonDealsCurrenciesLoaderID = 61;
                    lostDealsCurrenciesLoaderID = 62;
                }
                if (loader.getId() == openDealsCurrenciesLoaderID || loader.getId() == wonDealsCurrenciesLoaderID || loader.getId() == lostDealsCurrenciesLoaderID) {
                    GroupedDealsList.this.valueCalculated(data);
                }
            }

            public void onLoaderReset(Loader<CurrencyTaskResult> loader) {
            }
        };
        this.mContext = context;
        this.mSession = PipedriveApp.getActiveSession();
        setOrientation(0);
        setGravity(16);
        ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(R.layout.deals_list, this, true);
        this.mainView = getChildAt(0);
        this.dealsListView = (ListView) this.mainView.findViewById(R.id.dealsList);
        this.layoutEmpty = (RelativeLayout) this.mainView.findViewById(16908292);
        this.layoutStickyDealRowHeader = this.mainView.findViewById(R.id.layoutDealRowHeader);
        this.layoutStickyDealRowHeader.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.frost));
        toggleLayoutEmptyVisibility();
        Log.w(TAG, "start - inflated");
    }

    private void toggleLayoutEmptyVisibility() {
        if (this.adapter == null || this.adapter.getCount() <= 0) {
            this.layoutEmpty.setVisibility(0);
        } else {
            this.layoutEmpty.setVisibility(8);
        }
    }

    private void fillData() {
        if (this.dealsListView != null) {
            this.dealsListView.setOnScrollListener(new OnScrollListener() {
                private int previousFirstVisibleItem = -1;
                private LayoutParams stickyHeaderLayoutParams = null;
                private int upcomingHeaderPosition = 0;

                private void queryAdapterForDataAndComposeStickyHeader(int firstVisibleItem) {
                    if (GroupedDealsList.this.adapter != null) {
                        GroupedDealsList.this.currentSectionHeaderForStickyHeader = GroupedDealsList.this.adapter.getCurrentSectionHeader(firstVisibleItem);
                        this.upcomingHeaderPosition = GroupedDealsList.this.adapter.getNextHeaderPosition(firstVisibleItem);
                        if (GroupedDealsList.this.currentSectionHeaderForStickyHeader == Integer.MIN_VALUE || GroupedDealsList.this.currentSectionHeaderForStickyHeader == -2147483647) {
                            GroupedDealsList.this.layoutStickyDealRowHeader.setVisibility(8);
                            return;
                        }
                        GroupedDealsList.this.layoutStickyDealRowHeader.setVisibility(0);
                        if (GroupedDealsList.this.stickyDealHeaderViewHolder == null) {
                            GroupedDealsList.this.stickyDealHeaderViewHolder = new DealHeaderViewHolder(GroupedDealsList.this.layoutStickyDealRowHeader);
                        }
                        GroupedDealsList.this.adapter.fillSectionHeaderData(GroupedDealsList.this.stickyDealHeaderViewHolder, GroupedDealsList.this.adapter.getDealHeaderSection(GroupedDealsList.this.currentSectionHeaderForStickyHeader).sectionType);
                    }
                }

                public void onScrollStateChanged(AbsListView view, int scrollState) {
                }

                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    if (visibleItemCount > 0) {
                        if (this.previousFirstVisibleItem != firstVisibleItem) {
                            queryAdapterForDataAndComposeStickyHeader(firstVisibleItem);
                            this.previousFirstVisibleItem = firstVisibleItem;
                        }
                        if (GroupedDealsList.this.layoutStickyDealRowHeader.getVisibility() == 0) {
                            int topMargin = 0;
                            if (this.upcomingHeaderPosition == firstVisibleItem + 1) {
                                topMargin = view.getChildAt(1).getTop() - GroupedDealsList.this.layoutStickyDealRowHeader.getHeight();
                                if (topMargin > 0) {
                                    topMargin = 0;
                                }
                            }
                            if (this.stickyHeaderLayoutParams == null) {
                                this.stickyHeaderLayoutParams = (LayoutParams) GroupedDealsList.this.layoutStickyDealRowHeader.getLayoutParams();
                            }
                            this.stickyHeaderLayoutParams.setMargins(this.stickyHeaderLayoutParams.leftMargin, topMargin, this.stickyHeaderLayoutParams.rightMargin, this.stickyHeaderLayoutParams.bottomMargin);
                            GroupedDealsList.this.layoutStickyDealRowHeader.setLayoutParams(this.stickyHeaderLayoutParams);
                        }
                    }
                }
            });
        }
        TextView labelEmptyTextExplanation = (TextView) this.layoutEmpty.findViewById(R.id.labelEmptyTextExplanation);
        ((TextView) this.layoutEmpty.findViewById(R.id.labelEmptyTextHeader)).setText(getResources().getString(R.string.label_empty_header_deals_list));
        labelEmptyTextExplanation.setText(getResources().getString(R.string.label_empty_description_deals_list));
        if (this.mLoaderManager != null) {
            int loaderID = Integer.MIN_VALUE;
            if (this.mContactSqlId > 0) {
                loaderID = 50;
            } else if (this.mOrganizationSqlId > 0) {
                loaderID = 70;
            }
            if (loaderID != Integer.MIN_VALUE) {
                this.mLoaderManager.initLoader(loaderID, null, this.cursorLoaderListener);
                if (this.adapter == null) {
                    this.adapter = new DealsAdapter(this.mSession, this.mContext);
                    if (isEmptyLineVisible()) {
                        this.dealsListView.addHeaderView(LayoutInflater.from(this.mContext).inflate(R.layout.row_narrow_empty, null, false));
                    }
                    this.dealsListView.setAdapter(this.adapter);
                    this.dealsListView.setOnItemClickListener(new OnItemClickListener() {
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                            if (GroupedDealsList.this.isEmptyLineVisible() && position > 0) {
                                position--;
                            }
                            if (GroupedDealsList.this.adapter.getCount() > position) {
                                DealRow dealRow = (DealRow) GroupedDealsList.this.adapter.getItem(position);
                                if (GroupedDealsList.this.mOnDealClickListener != null && dealRow.type == -2147483647 && dealRow.deal != null) {
                                    GroupedDealsList.this.mOnDealClickListener.onDealClicked(dealRow.deal);
                                }
                            }
                        }
                    });
                }
            }
        }
    }

    @Nullable
    private DealStatus getDealStatusByLoaderId(int loaderId) {
        switch (loaderId) {
            case 60:
                return DealStatus.OPEN;
            case 61:
                return DealStatus.WON;
            case 62:
                return DealStatus.LOST;
            default:
                return null;
        }
    }

    private void initDealsSumLoaders() {
        if (this.mLoaderManager != null) {
            int openDealsCurrenciesLoaderID = Integer.MIN_VALUE;
            int wonDealsCurrenciesLoaderID = Integer.MIN_VALUE;
            int lostDealsCurrenciesLoaderID = Integer.MIN_VALUE;
            if (this.mContactSqlId > 0 || this.mOrganizationSqlId > 0) {
                openDealsCurrenciesLoaderID = 60;
                wonDealsCurrenciesLoaderID = 61;
                lostDealsCurrenciesLoaderID = 62;
            }
            if (openDealsCurrenciesLoaderID != Integer.MIN_VALUE) {
                this.mLoaderManager.initLoader(openDealsCurrenciesLoaderID, null, this.dealsSumLoaderListener);
            }
            if (wonDealsCurrenciesLoaderID != Integer.MIN_VALUE) {
                this.mLoaderManager.initLoader(wonDealsCurrenciesLoaderID, null, this.dealsSumLoaderListener);
            }
            if (lostDealsCurrenciesLoaderID != Integer.MIN_VALUE) {
                this.mLoaderManager.initLoader(lostDealsCurrenciesLoaderID, null, this.dealsSumLoaderListener);
            }
        }
    }

    private Loader createLoader(int id) {
        StringBuilder selectionBuilder = new StringBuilder();
        String loadOnlyOpenDeals = "d_status = ?";
        List<String> selectionArgumentsList = new ArrayList();
        Session activeSession = PipedriveApp.getActiveSession();
        if (this.mStageId != -1) {
            selectionBuilder.append("d_pipeline_id=?");
            selectionArgumentsList.add("" + activeSession.getPipelineSelectedPipelineId(-1));
            selectionBuilder.append(" AND ");
            selectionBuilder.append("d_stage=?");
            selectionArgumentsList.add("" + this.mStageId);
        }
        Uri dealsURI = null;
        DealsContentProvider dealsContentProvider = new DealsContentProvider(activeSession);
        if (this.mStageId != -1) {
            long filterId = activeSession.getPipelineFilter(-1);
            if (filterId != -1) {
                dealsURI = dealsContentProvider.createFilterIdUri(filterId);
            } else {
                selectionBuilder.append(" AND ");
                selectionBuilder.append(loadOnlyOpenDeals);
                selectionArgumentsList.add(DealStatus.OPEN.toString());
                Long userId = activeSession.getPipelineUserOrNull();
                if (userId != null) {
                    dealsURI = dealsContentProvider.createUserIdUri(userId.longValue());
                } else {
                    dealsURI = dealsContentProvider.createAllDealsUri();
                }
            }
        } else if (this.mContactSqlId > 0) {
            dealsURI = dealsContentProvider.createPersonSqlIdUri(this.mContactSqlId, getDealStatusByLoaderId(id));
        } else if (this.mOrganizationSqlId > 0) {
            dealsURI = dealsContentProvider.createOrganizationSqlIdUri(this.mOrganizationSqlId, getDealStatusByLoaderId(id));
        }
        String[] selectionArgs = new String[selectionArgumentsList.size()];
        for (int i = 0; i < selectionArgumentsList.size(); i++) {
            selectionArgs[i] = (String) selectionArgumentsList.get(i);
        }
        int openDealsCurrenciesLoaderID = Integer.MIN_VALUE;
        int wonDealsCurrenciesLoaderID = Integer.MIN_VALUE;
        int lostDealsCurrenciesLoaderID = Integer.MIN_VALUE;
        if (this.mContactSqlId > 0 || this.mOrganizationSqlId > 0) {
            openDealsCurrenciesLoaderID = 60;
            wonDealsCurrenciesLoaderID = 61;
            lostDealsCurrenciesLoaderID = 62;
        }
        if (id != openDealsCurrenciesLoaderID && id != wonDealsCurrenciesLoaderID && id != lostDealsCurrenciesLoaderID) {
            return new CursorLoader(this.mContext, dealsURI, null, selectionBuilder.toString(), selectionArgs, null);
        }
        int dealsGroupId = -2147483645;
        if (id == wonDealsCurrenciesLoaderID) {
            dealsGroupId = -2147483644;
        } else if (id == lostDealsCurrenciesLoaderID) {
            dealsGroupId = -2147483643;
        }
        return new DealsValueLoader(this.mContext, dealsURI, null, selectionBuilder.toString(), selectionArgs, null, this.mSession, dealsGroupId);
    }

    public void canShowEmptyView(boolean canShowEmptyView) {
        this.canShowEmptyView = canShowEmptyView;
    }

    private boolean isEmptyLineVisible() {
        return this.mEmptyRowVisible && FiltersUtil.getSelectedFilterName(this.mSession) != null;
    }

    public void valueCalculated(CurrencyTaskResult result) {
        if (this.adapter != null) {
            this.adapter.setDealsGroupSum(result);
            this.adapter.fillSectionHeaderData(this.stickyDealHeaderViewHolder, this.currentSectionHeaderForStickyHeader);
        }
    }

    public void setOnDealClickListener(@Nullable OnDealClickListener onDealClickListener) {
        this.mOnDealClickListener = onDealClickListener;
    }

    public void showFooterForFab(boolean shouldAddFooterForFab) {
        if (shouldAddFooterForFab) {
            this.dealsListView.addFooterView(ViewUtil.getColoredFooterForListWithFloatingButton(getContext(), R.color.frost));
        }
    }
}
