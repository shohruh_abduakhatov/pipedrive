package com.zendesk.sdk.model.helpcenter;

import android.support.annotation.NonNull;
import com.zendesk.util.CollectionUtils;
import java.util.List;

public class AttachmentResponse {
    private List<Attachment> articleAttachments;

    @NonNull
    public List<Attachment> getArticleAttachments() {
        return CollectionUtils.copyOf(this.articleAttachments);
    }
}
