package com.pipedrive.views.agenda;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.views.calendar.recyclerview.CalendarRecyclerViewAdapter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.subscriptions.CompositeSubscription;

class AgendaViewAdapter extends CalendarRecyclerViewAdapter<AgendaViewDayViewHolder> {
    private static final int DEBOUNCE_TIME_MILLIS = 100;
    private int agendaDayViewScrollY = ((Integer) AgendaViewEventBus.getInstance().agendaDayViewScrollYChanges().toBlocking().first()).intValue();
    private final int allDayActivitiesViewMaxHeight;
    private boolean allDayActivityListExpanded = ((Boolean) AgendaViewEventBus.getInstance().allDayActivityExpandBus().toBlocking().first()).booleanValue();
    private final CompositeSubscription compositeSubscription = new CompositeSubscription();
    @NonNull
    private final ArrayList<Calendar> items = new ArrayList(7);
    @Nullable
    private OnActivityClickListener onActivityClickListener;
    @NonNull
    private final Session session;
    private boolean smoothScrollToCurrentTime;

    AgendaViewAdapter(@NonNull Session session, @NonNull Calendar date, boolean smoothScrollToCurrentTime, int allDayActivitiesViewMaxHeight) {
        this.session = session;
        this.smoothScrollToCurrentTime = smoothScrollToCurrentTime;
        this.allDayActivitiesViewMaxHeight = allDayActivitiesViewMaxHeight;
        this.items.add(date);
        Calendar prevDay = (Calendar) date.clone();
        Calendar nextDay = (Calendar) date.clone();
        for (int i = 0; i < 3; i++) {
            prevDay.add(6, -1);
            nextDay.add(6, 1);
            this.items.add(0, (Calendar) prevDay.clone());
            this.items.add((Calendar) nextDay.clone());
        }
        this.compositeSubscription.add(AgendaViewEventBus.getInstance().agendaDayViewScrollYChanges().filter(new Func1<Integer, Boolean>() {
            public Boolean call(Integer newValue) {
                return Boolean.valueOf(newValue.intValue() != AgendaViewAdapter.this.agendaDayViewScrollY);
            }
        }).debounce(100, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).doOnNext(new Action1<Integer>() {
            public void call(Integer newValue) {
                AgendaViewAdapter.this.smoothScrollToCurrentTime = false;
                AgendaViewAdapter.this.agendaDayViewScrollY = newValue.intValue();
                AgendaViewAdapter.this.notifyDataSetChanged();
            }
        }).subscribe());
        this.compositeSubscription.add(AgendaViewEventBus.getInstance().allDayActivityExpandBus().filter(new Func1<Boolean, Boolean>() {
            public Boolean call(Boolean newValue) {
                return Boolean.valueOf(AgendaViewAdapter.this.allDayActivityListExpanded != newValue.booleanValue());
            }
        }).debounce(100, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).doOnNext(new Action1<Boolean>() {
            public void call(Boolean newValue) {
                AgendaViewAdapter.this.allDayActivityListExpanded = newValue.booleanValue();
            }
        }).subscribe());
        this.compositeSubscription.add(AgendaViewEventBus.getInstance().allDayActivityAnimationEndBus().observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Void>() {
            public void call(Void ignored) {
                AgendaViewAdapter.this.notifyDataSetChanged();
            }
        }));
    }

    void setOnActivityClickListener(@Nullable OnActivityClickListener onActivityClickListener) {
        this.onActivityClickListener = onActivityClickListener;
    }

    public AgendaViewDayViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AgendaViewDayViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_agenda_view, parent, false), this.allDayActivitiesViewMaxHeight);
    }

    public void onBindViewHolder(AgendaViewDayViewHolder holder, int position) {
        holder.bind(this.session, getItem(position), this.smoothScrollToCurrentTime, this.onActivityClickListener, this.agendaDayViewScrollY, this.allDayActivityListExpanded);
        this.smoothScrollToCurrentTime = false;
    }

    public Calendar getItem(int position) {
        return (Calendar) this.items.get(position);
    }

    public long getItemId(int position) {
        return ((Calendar) this.items.get(position)).getTimeInMillis();
    }

    public int getItemCount() {
        return 7;
    }

    public void unsubscribe() {
        this.compositeSubscription.clear();
    }
}
