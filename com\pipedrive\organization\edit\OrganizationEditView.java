package com.pipedrive.organization.edit;

import android.support.annotation.MainThread;
import android.support.annotation.Nullable;
import com.pipedrive.model.Organization;

@MainThread
interface OrganizationEditView {
    void onOrganizationCreated(boolean z);

    void onOrganizationDeleted(boolean z);

    void onOrganizationUpdated(boolean z);

    void onRequestOrganization(@Nullable Organization organization);
}
