package com.pipedrive.organization.edit;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.MainThread;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import com.pipedrive.model.Organization;
import com.pipedrive.navigator.Navigator.Type;
import java.util.List;

public class OrganizationUpdateActivity extends OrganizationActivity {
    public /* bridge */ /* synthetic */ void createOrUpdateDiscardedAsRequiredFieldsAreNotSet() {
        super.createOrUpdateDiscardedAsRequiredFieldsAreNotSet();
    }

    @Nullable
    public /* bridge */ /* synthetic */ List getAdditionalButtons() {
        return super.getAdditionalButtons();
    }

    public /* bridge */ /* synthetic */ boolean isAllRequiredFieldsSet() {
        return super.isAllRequiredFieldsSet();
    }

    public /* bridge */ /* synthetic */ boolean isChangesMadeToFieldsAfterInitialLoad() {
        return super.isChangesMadeToFieldsAfterInitialLoad();
    }

    public /* bridge */ /* synthetic */ void onCancel() {
        super.onCancel();
    }

    public /* bridge */ /* synthetic */ void onCreateOrUpdate() {
        super.onCreateOrUpdate();
    }

    public /* bridge */ /* synthetic */ void onOrganizationCreated(boolean z) {
        super.onOrganizationCreated(z);
    }

    public /* bridge */ /* synthetic */ void onOrganizationDeleted(boolean z) {
        super.onOrganizationDeleted(z);
    }

    public /* bridge */ /* synthetic */ void onOrganizationUpdated(boolean z) {
        super.onOrganizationUpdated(z);
    }

    public /* bridge */ /* synthetic */ void onResume() {
        super.onResume();
    }

    @MainThread
    public static void startActivityForResult(@Nullable Activity activity, @Nullable Organization organization, int requestCode) {
        boolean cannotStartActivity = activity == null || organization == null || !organization.isStored();
        if (!cannotStartActivity) {
            ActivityCompat.startActivityForResult(activity, new Intent(activity, OrganizationUpdateActivity.class).putExtra(OrganizationActivity.KEY_ORGANIZATION_SQL_ID, organization.getSqlId()), requestCode, null);
        }
    }

    public void onRequestOrganization(@Nullable Organization organization) {
        setNavigatorType(Type.UPDATE);
        super.onRequestOrganization(organization);
    }
}
