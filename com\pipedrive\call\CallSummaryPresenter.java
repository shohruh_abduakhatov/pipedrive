package com.pipedrive.call;

import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.model.Activity;
import com.pipedrive.model.Deal;
import com.pipedrive.presenter.PersistablePresenter;

abstract class CallSummaryPresenter extends PersistablePresenter<CallSummaryView> {
    abstract void appendOrAddNote(String str);

    abstract void associateDeal(@Nullable Deal deal);

    abstract void associateDealBySqlId(@NonNull Long l);

    abstract void changeCallActivity();

    @Nullable
    abstract Activity getActivity();

    abstract boolean isModified();

    abstract void requestOrRestoreExistingActivity(@Nullable Long l, long j, @IntRange(from = 0) long j2);

    abstract void requestOrRestoreNewCallActivityForPerson(@NonNull Long l, @Nullable Long l2, long j, @IntRange(from = 0) long j2);

    abstract void setDone(boolean z);

    public CallSummaryPresenter(@NonNull Session session, @Nullable Bundle savedState) {
        super(session, savedState);
    }
}
