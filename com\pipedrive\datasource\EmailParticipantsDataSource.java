package com.pipedrive.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.model.Person;
import com.pipedrive.model.email.EmailParticipant;
import com.pipedrive.util.CursorHelper;

public class EmailParticipantsDataSource extends BaseDataSource<EmailParticipant> {
    private PersonsDataSource mPersonsDataSource;

    public EmailParticipantsDataSource(SQLiteDatabase dbconn) {
        super(dbconn);
    }

    private PersonsDataSource getPersonDataSource() {
        if (this.mPersonsDataSource != null) {
            return this.mPersonsDataSource;
        }
        PersonsDataSource personsDataSource = new PersonsDataSource(getTransactionalDBConnection());
        this.mPersonsDataSource = personsDataSource;
        return personsDataSource;
    }

    @NonNull
    protected String getColumnNameForPipedriveId() {
        return PipeSQLiteHelper.COLUMN_EMAIL_PARTICIPANT_PIPEDRIVE_ID;
    }

    @NonNull
    protected String getColumnNameForSqlId() {
        return PipeSQLiteHelper.COLUMN_ID;
    }

    @NonNull
    protected String getTableName() {
        return PipeSQLiteHelper.TABLE_EMAIL_PARTICIPANTS;
    }

    @NonNull
    protected ContentValues getContentValues(@NonNull EmailParticipant emailParticipant) {
        ContentValues contentValues = super.getContentValues(emailParticipant);
        if (!TextUtils.isEmpty(emailParticipant.getEmail())) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_PARTICIPANT_EMAIL_ADDRESS, emailParticipant.getEmail());
        }
        if (!TextUtils.isEmpty(emailParticipant.getName())) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_PARTICIPANT_NAME, emailParticipant.getName());
        }
        if (emailParticipant.getPerson() != null && emailParticipant.getPerson().isStored()) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_PARTICIPANT_PERSON_ID, Long.valueOf(emailParticipant.getPerson().getSqlId()));
        }
        if (emailParticipant.getUser() != null && emailParticipant.getUser().isStored()) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_PARTICIPANT_USER_ID, Long.valueOf(emailParticipant.getUser().getSqlId()));
        }
        return contentValues;
    }

    @NonNull
    protected String[] getAllColumns() {
        return new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_EMAIL_PARTICIPANT_PIPEDRIVE_ID, PipeSQLiteHelper.COLUMN_EMAIL_PARTICIPANT_EMAIL_ADDRESS, PipeSQLiteHelper.COLUMN_EMAIL_PARTICIPANT_NAME, PipeSQLiteHelper.COLUMN_EMAIL_PARTICIPANT_PERSON_ID};
    }

    @Nullable
    protected EmailParticipant deflateCursor(@NonNull Cursor cursor) {
        EmailParticipant emailParticipant = new EmailParticipant();
        Long sqlId = CursorHelper.getLong(cursor, getColumnNameForSqlId());
        if (sqlId != null) {
            emailParticipant.setSqlId(sqlId.longValue());
        }
        Integer pipedriveId = CursorHelper.getInteger(cursor, getColumnNameForSqlId());
        if (pipedriveId != null) {
            emailParticipant.setPipedriveId(pipedriveId.intValue());
        }
        emailParticipant.setEmail(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_EMAIL_PARTICIPANT_EMAIL_ADDRESS));
        emailParticipant.setName(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_EMAIL_PARTICIPANT_NAME));
        Long personId = CursorHelper.getLong(cursor, PipeSQLiteHelper.COLUMN_EMAIL_PARTICIPANT_PERSON_ID);
        if (personId != null) {
            emailParticipant.setPerson((Person) getPersonDataSource().findBySqlId(personId.longValue()));
        }
        return emailParticipant;
    }

    public long createOrUpdate(EmailParticipant emailParticipant) {
        if (emailParticipant == null) {
            return -1;
        }
        ContentValues contentValues = getContentValues(emailParticipant);
        long sqlId = -1;
        SQLiteDatabase transactionalDBConnection;
        String tableName;
        String str;
        String[] strArr;
        if (emailParticipant.isStored()) {
            transactionalDBConnection = getTransactionalDBConnection();
            tableName = getTableName();
            str = getColumnNameForSqlId() + " =?";
            strArr = new String[]{String.valueOf(emailParticipant.getSqlId())};
            if (transactionalDBConnection instanceof SQLiteDatabase) {
                SQLiteInstrumentation.update(transactionalDBConnection, tableName, contentValues, str, strArr);
            } else {
                transactionalDBConnection.update(tableName, contentValues, str, strArr);
            }
            sqlId = emailParticipant.getSqlId();
        } else if (TextUtils.isEmpty(emailParticipant.getEmail())) {
            transactionalDBConnection = getTransactionalDBConnection();
            tableName = getTableName();
            sqlId = !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.insert(tableName, null, contentValues) : SQLiteInstrumentation.insert(transactionalDBConnection, tableName, null, contentValues);
        } else {
            transactionalDBConnection = getTransactionalDBConnection();
            tableName = getTableName();
            str = "email_participants_email_address=?";
            strArr = new String[]{String.valueOf(emailParticipant.getEmail())};
            if ((!(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.update(tableName, contentValues, str, strArr) : SQLiteInstrumentation.update(transactionalDBConnection, tableName, contentValues, str, strArr)) > 0) {
                transactionalDBConnection = getTransactionalDBConnection();
                tableName = getTableName();
                String[] strArr2 = new String[]{getColumnNameForSqlId()};
                String str2 = "email_participants_email_address=?";
                String[] strArr3 = new String[]{emailParticipant.getEmail()};
                Cursor cursor = !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.query(tableName, strArr2, str2, strArr3, null, null, null) : SQLiteInstrumentation.query(transactionalDBConnection, tableName, strArr2, str2, strArr3, null, null, null);
                try {
                    cursor.moveToFirst();
                    sqlId = cursor.getLong(0);
                } finally {
                    cursor.close();
                }
            } else {
                transactionalDBConnection = getTransactionalDBConnection();
                tableName = getTableName();
                sqlId = !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.insert(tableName, null, contentValues) : SQLiteInstrumentation.insert(transactionalDBConnection, tableName, null, contentValues);
            }
        }
        if (sqlId <= 0) {
            return sqlId;
        }
        emailParticipant.setSqlId(sqlId);
        return sqlId;
    }
}
