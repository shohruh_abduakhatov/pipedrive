package com.google.android.gms.auth.api.credentials.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.auth.api.credentials.PasswordSpecification;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzi implements Creator<GeneratePasswordRequest> {
    static void zza(GeneratePasswordRequest generatePasswordRequest, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zza(parcel, 1, generatePasswordRequest.zzaid(), i, false);
        zzb.zzc(parcel, 1000, generatePasswordRequest.mVersionCode);
        zzb.zzaj(parcel, zzcs);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzaq(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzde(i);
    }

    public GeneratePasswordRequest zzaq(Parcel parcel) {
        int zzcr = zza.zzcr(parcel);
        int i = 0;
        PasswordSpecification passwordSpecification = null;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    passwordSpecification = (PasswordSpecification) zza.zza(parcel, zzcq, PasswordSpecification.CREATOR);
                    break;
                case 1000:
                    i = zza.zzg(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new GeneratePasswordRequest(i, passwordSpecification);
        }
        throw new zza.zza("Overread allowed size end=" + zzcr, parcel);
    }

    public GeneratePasswordRequest[] zzde(int i) {
        return new GeneratePasswordRequest[i];
    }
}
