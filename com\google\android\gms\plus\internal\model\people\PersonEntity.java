package com.google.android.gms.plus.internal.model.people;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.server.converter.StringToIntConverter;
import com.google.android.gms.common.server.response.FastJsonResponse.Field;
import com.google.android.gms.common.server.response.FastSafeParcelableJsonResponse;
import com.google.android.gms.plus.PlusShare;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.Person.AgeRange;
import com.google.android.gms.plus.model.people.Person.Cover;
import com.google.android.gms.plus.model.people.Person.Cover.CoverInfo;
import com.google.android.gms.plus.model.people.Person.Cover.CoverPhoto;
import com.google.android.gms.plus.model.people.Person.Image;
import com.google.android.gms.plus.model.people.Person.Name;
import com.google.android.gms.plus.model.people.Person.Organizations;
import com.google.android.gms.plus.model.people.Person.PlacesLived;
import com.google.android.gms.plus.model.people.Person.Urls;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.pipedrive.datasource.PipeSQLiteHelper;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class PersonEntity extends FastSafeParcelableJsonResponse implements Person {
    public static final Creator<PersonEntity> CREATOR = new zza();
    private static final HashMap<String, Field<?, ?>> aBq = new HashMap();
    boolean aBA;
    NameEntity aBB;
    String aBC;
    int aBD;
    List<OrganizationsEntity> aBE;
    List<PlacesLivedEntity> aBF;
    int aBG;
    int aBH;
    String aBI;
    List<UrlsEntity> aBJ;
    boolean aBK;
    final Set<Integer> aBr;
    String aBs;
    AgeRangeEntity aBt;
    String aBu;
    String aBv;
    int aBw;
    CoverEntity aBx;
    String aBy;
    ImageEntity aBz;
    String bZ;
    String jh;
    final int mVersionCode;
    String zzae;
    int zzazc;
    String zzboa;

    public static final class AgeRangeEntity extends FastSafeParcelableJsonResponse implements AgeRange {
        public static final Creator<AgeRangeEntity> CREATOR = new zzb();
        private static final HashMap<String, Field<?, ?>> aBq = new HashMap();
        int aBL;
        int aBM;
        final Set<Integer> aBr;
        final int mVersionCode;

        static {
            aBq.put("max", Field.zzk("max", 2));
            aBq.put("min", Field.zzk("min", 3));
        }

        public AgeRangeEntity() {
            this.mVersionCode = 1;
            this.aBr = new HashSet();
        }

        AgeRangeEntity(Set<Integer> set, int i, int i2, int i3) {
            this.aBr = set;
            this.mVersionCode = i;
            this.aBL = i2;
            this.aBM = i3;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof AgeRangeEntity)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            AgeRangeEntity ageRangeEntity = (AgeRangeEntity) obj;
            for (Field field : aBq.values()) {
                if (zza(field)) {
                    if (!ageRangeEntity.zza(field)) {
                        return false;
                    }
                    if (!zzb(field).equals(ageRangeEntity.zzb(field))) {
                        return false;
                    }
                } else if (ageRangeEntity.zza(field)) {
                    return false;
                }
            }
            return true;
        }

        public /* synthetic */ Object freeze() {
            return zzccf();
        }

        public int getMax() {
            return this.aBL;
        }

        public int getMin() {
            return this.aBM;
        }

        public boolean hasMax() {
            return this.aBr.contains(Integer.valueOf(2));
        }

        public boolean hasMin() {
            return this.aBr.contains(Integer.valueOf(3));
        }

        public int hashCode() {
            int i = 0;
            for (Field field : aBq.values()) {
                int hashCode;
                if (zza(field)) {
                    hashCode = zzb(field).hashCode() + (i + field.zzaxf());
                } else {
                    hashCode = i;
                }
                i = hashCode;
            }
            return i;
        }

        public boolean isDataValid() {
            return true;
        }

        public void writeToParcel(Parcel parcel, int i) {
            zzb.zza(this, parcel, i);
        }

        protected boolean zza(Field field) {
            return this.aBr.contains(Integer.valueOf(field.zzaxf()));
        }

        public /* synthetic */ Map zzawz() {
            return zzccd();
        }

        protected Object zzb(Field field) {
            switch (field.zzaxf()) {
                case 2:
                    return Integer.valueOf(this.aBL);
                case 3:
                    return Integer.valueOf(this.aBM);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + field.zzaxf());
            }
        }

        public HashMap<String, Field<?, ?>> zzccd() {
            return aBq;
        }

        public AgeRangeEntity zzccf() {
            return this;
        }
    }

    public static final class CoverEntity extends FastSafeParcelableJsonResponse implements Cover {
        public static final Creator<CoverEntity> CREATOR = new zzc();
        private static final HashMap<String, Field<?, ?>> aBq = new HashMap();
        CoverInfoEntity aBN;
        CoverPhotoEntity aBO;
        int aBP;
        final Set<Integer> aBr;
        final int mVersionCode;

        public static final class CoverInfoEntity extends FastSafeParcelableJsonResponse implements CoverInfo {
            public static final Creator<CoverInfoEntity> CREATOR = new zzd();
            private static final HashMap<String, Field<?, ?>> aBq = new HashMap();
            int aBQ;
            int aBR;
            final Set<Integer> aBr;
            final int mVersionCode;

            static {
                aBq.put("leftImageOffset", Field.zzk("leftImageOffset", 2));
                aBq.put("topImageOffset", Field.zzk("topImageOffset", 3));
            }

            public CoverInfoEntity() {
                this.mVersionCode = 1;
                this.aBr = new HashSet();
            }

            CoverInfoEntity(Set<Integer> set, int i, int i2, int i3) {
                this.aBr = set;
                this.mVersionCode = i;
                this.aBQ = i2;
                this.aBR = i3;
            }

            public boolean equals(Object obj) {
                if (!(obj instanceof CoverInfoEntity)) {
                    return false;
                }
                if (this == obj) {
                    return true;
                }
                CoverInfoEntity coverInfoEntity = (CoverInfoEntity) obj;
                for (Field field : aBq.values()) {
                    if (zza(field)) {
                        if (!coverInfoEntity.zza(field)) {
                            return false;
                        }
                        if (!zzb(field).equals(coverInfoEntity.zzb(field))) {
                            return false;
                        }
                    } else if (coverInfoEntity.zza(field)) {
                        return false;
                    }
                }
                return true;
            }

            public /* synthetic */ Object freeze() {
                return zzcch();
            }

            public int getLeftImageOffset() {
                return this.aBQ;
            }

            public int getTopImageOffset() {
                return this.aBR;
            }

            public boolean hasLeftImageOffset() {
                return this.aBr.contains(Integer.valueOf(2));
            }

            public boolean hasTopImageOffset() {
                return this.aBr.contains(Integer.valueOf(3));
            }

            public int hashCode() {
                int i = 0;
                for (Field field : aBq.values()) {
                    int hashCode;
                    if (zza(field)) {
                        hashCode = zzb(field).hashCode() + (i + field.zzaxf());
                    } else {
                        hashCode = i;
                    }
                    i = hashCode;
                }
                return i;
            }

            public boolean isDataValid() {
                return true;
            }

            public void writeToParcel(Parcel parcel, int i) {
                zzd.zza(this, parcel, i);
            }

            protected boolean zza(Field field) {
                return this.aBr.contains(Integer.valueOf(field.zzaxf()));
            }

            public /* synthetic */ Map zzawz() {
                return zzccd();
            }

            protected Object zzb(Field field) {
                switch (field.zzaxf()) {
                    case 2:
                        return Integer.valueOf(this.aBQ);
                    case 3:
                        return Integer.valueOf(this.aBR);
                    default:
                        throw new IllegalStateException("Unknown safe parcelable id=" + field.zzaxf());
                }
            }

            public HashMap<String, Field<?, ?>> zzccd() {
                return aBq;
            }

            public CoverInfoEntity zzcch() {
                return this;
            }
        }

        public static final class CoverPhotoEntity extends FastSafeParcelableJsonResponse implements CoverPhoto {
            public static final Creator<CoverPhotoEntity> CREATOR = new zze();
            private static final HashMap<String, Field<?, ?>> aBq = new HashMap();
            final Set<Integer> aBr;
            final int mVersionCode;
            String zzae;
            int zzakh;
            int zzaki;

            static {
                aBq.put(SettingsJsonConstants.ICON_HEIGHT_KEY, Field.zzk(SettingsJsonConstants.ICON_HEIGHT_KEY, 2));
                aBq.put("url", Field.zzm("url", 3));
                aBq.put(SettingsJsonConstants.ICON_WIDTH_KEY, Field.zzk(SettingsJsonConstants.ICON_WIDTH_KEY, 4));
            }

            public CoverPhotoEntity() {
                this.mVersionCode = 1;
                this.aBr = new HashSet();
            }

            CoverPhotoEntity(Set<Integer> set, int i, int i2, String str, int i3) {
                this.aBr = set;
                this.mVersionCode = i;
                this.zzaki = i2;
                this.zzae = str;
                this.zzakh = i3;
            }

            public boolean equals(Object obj) {
                if (!(obj instanceof CoverPhotoEntity)) {
                    return false;
                }
                if (this == obj) {
                    return true;
                }
                CoverPhotoEntity coverPhotoEntity = (CoverPhotoEntity) obj;
                for (Field field : aBq.values()) {
                    if (zza(field)) {
                        if (!coverPhotoEntity.zza(field)) {
                            return false;
                        }
                        if (!zzb(field).equals(coverPhotoEntity.zzb(field))) {
                            return false;
                        }
                    } else if (coverPhotoEntity.zza(field)) {
                        return false;
                    }
                }
                return true;
            }

            public /* synthetic */ Object freeze() {
                return zzcci();
            }

            public int getHeight() {
                return this.zzaki;
            }

            public String getUrl() {
                return this.zzae;
            }

            public int getWidth() {
                return this.zzakh;
            }

            public boolean hasHeight() {
                return this.aBr.contains(Integer.valueOf(2));
            }

            public boolean hasUrl() {
                return this.aBr.contains(Integer.valueOf(3));
            }

            public boolean hasWidth() {
                return this.aBr.contains(Integer.valueOf(4));
            }

            public int hashCode() {
                int i = 0;
                for (Field field : aBq.values()) {
                    int hashCode;
                    if (zza(field)) {
                        hashCode = zzb(field).hashCode() + (i + field.zzaxf());
                    } else {
                        hashCode = i;
                    }
                    i = hashCode;
                }
                return i;
            }

            public boolean isDataValid() {
                return true;
            }

            public void writeToParcel(Parcel parcel, int i) {
                zze.zza(this, parcel, i);
            }

            protected boolean zza(Field field) {
                return this.aBr.contains(Integer.valueOf(field.zzaxf()));
            }

            public /* synthetic */ Map zzawz() {
                return zzccd();
            }

            protected Object zzb(Field field) {
                switch (field.zzaxf()) {
                    case 2:
                        return Integer.valueOf(this.zzaki);
                    case 3:
                        return this.zzae;
                    case 4:
                        return Integer.valueOf(this.zzakh);
                    default:
                        throw new IllegalStateException("Unknown safe parcelable id=" + field.zzaxf());
                }
            }

            public HashMap<String, Field<?, ?>> zzccd() {
                return aBq;
            }

            public CoverPhotoEntity zzcci() {
                return this;
            }
        }

        static {
            aBq.put("coverInfo", Field.zza("coverInfo", 2, CoverInfoEntity.class));
            aBq.put("coverPhoto", Field.zza("coverPhoto", 3, CoverPhotoEntity.class));
            aBq.put("layout", Field.zza("layout", 4, new StringToIntConverter().zzj("banner", 0), false));
        }

        public CoverEntity() {
            this.mVersionCode = 1;
            this.aBr = new HashSet();
        }

        CoverEntity(Set<Integer> set, int i, CoverInfoEntity coverInfoEntity, CoverPhotoEntity coverPhotoEntity, int i2) {
            this.aBr = set;
            this.mVersionCode = i;
            this.aBN = coverInfoEntity;
            this.aBO = coverPhotoEntity;
            this.aBP = i2;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof CoverEntity)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            CoverEntity coverEntity = (CoverEntity) obj;
            for (Field field : aBq.values()) {
                if (zza(field)) {
                    if (!coverEntity.zza(field)) {
                        return false;
                    }
                    if (!zzb(field).equals(coverEntity.zzb(field))) {
                        return false;
                    }
                } else if (coverEntity.zza(field)) {
                    return false;
                }
            }
            return true;
        }

        public /* synthetic */ Object freeze() {
            return zzccg();
        }

        public CoverInfo getCoverInfo() {
            return this.aBN;
        }

        public CoverPhoto getCoverPhoto() {
            return this.aBO;
        }

        public int getLayout() {
            return this.aBP;
        }

        public boolean hasCoverInfo() {
            return this.aBr.contains(Integer.valueOf(2));
        }

        public boolean hasCoverPhoto() {
            return this.aBr.contains(Integer.valueOf(3));
        }

        public boolean hasLayout() {
            return this.aBr.contains(Integer.valueOf(4));
        }

        public int hashCode() {
            int i = 0;
            for (Field field : aBq.values()) {
                int hashCode;
                if (zza(field)) {
                    hashCode = zzb(field).hashCode() + (i + field.zzaxf());
                } else {
                    hashCode = i;
                }
                i = hashCode;
            }
            return i;
        }

        public boolean isDataValid() {
            return true;
        }

        public void writeToParcel(Parcel parcel, int i) {
            zzc.zza(this, parcel, i);
        }

        protected boolean zza(Field field) {
            return this.aBr.contains(Integer.valueOf(field.zzaxf()));
        }

        public /* synthetic */ Map zzawz() {
            return zzccd();
        }

        protected Object zzb(Field field) {
            switch (field.zzaxf()) {
                case 2:
                    return this.aBN;
                case 3:
                    return this.aBO;
                case 4:
                    return Integer.valueOf(this.aBP);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + field.zzaxf());
            }
        }

        public HashMap<String, Field<?, ?>> zzccd() {
            return aBq;
        }

        public CoverEntity zzccg() {
            return this;
        }
    }

    public static final class ImageEntity extends FastSafeParcelableJsonResponse implements Image {
        public static final Creator<ImageEntity> CREATOR = new zzf();
        private static final HashMap<String, Field<?, ?>> aBq = new HashMap();
        final Set<Integer> aBr;
        final int mVersionCode;
        String zzae;

        static {
            aBq.put("url", Field.zzm("url", 2));
        }

        public ImageEntity() {
            this.mVersionCode = 1;
            this.aBr = new HashSet();
        }

        public ImageEntity(String str) {
            this.aBr = new HashSet();
            this.mVersionCode = 1;
            this.zzae = str;
            this.aBr.add(Integer.valueOf(2));
        }

        ImageEntity(Set<Integer> set, int i, String str) {
            this.aBr = set;
            this.mVersionCode = i;
            this.zzae = str;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof ImageEntity)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            ImageEntity imageEntity = (ImageEntity) obj;
            for (Field field : aBq.values()) {
                if (zza(field)) {
                    if (!imageEntity.zza(field)) {
                        return false;
                    }
                    if (!zzb(field).equals(imageEntity.zzb(field))) {
                        return false;
                    }
                } else if (imageEntity.zza(field)) {
                    return false;
                }
            }
            return true;
        }

        public /* synthetic */ Object freeze() {
            return zzccj();
        }

        public String getUrl() {
            return this.zzae;
        }

        public boolean hasUrl() {
            return this.aBr.contains(Integer.valueOf(2));
        }

        public int hashCode() {
            int i = 0;
            for (Field field : aBq.values()) {
                int hashCode;
                if (zza(field)) {
                    hashCode = zzb(field).hashCode() + (i + field.zzaxf());
                } else {
                    hashCode = i;
                }
                i = hashCode;
            }
            return i;
        }

        public boolean isDataValid() {
            return true;
        }

        public void writeToParcel(Parcel parcel, int i) {
            zzf.zza(this, parcel, i);
        }

        protected boolean zza(Field field) {
            return this.aBr.contains(Integer.valueOf(field.zzaxf()));
        }

        public /* synthetic */ Map zzawz() {
            return zzccd();
        }

        protected Object zzb(Field field) {
            switch (field.zzaxf()) {
                case 2:
                    return this.zzae;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + field.zzaxf());
            }
        }

        public HashMap<String, Field<?, ?>> zzccd() {
            return aBq;
        }

        public ImageEntity zzccj() {
            return this;
        }
    }

    public static final class NameEntity extends FastSafeParcelableJsonResponse implements Name {
        public static final Creator<NameEntity> CREATOR = new zzg();
        private static final HashMap<String, Field<?, ?>> aBq = new HashMap();
        String aBS;
        String aBT;
        String aBU;
        String aBV;
        final Set<Integer> aBr;
        String is;
        String it;
        final int mVersionCode;

        static {
            aBq.put("familyName", Field.zzm("familyName", 2));
            aBq.put("formatted", Field.zzm("formatted", 3));
            aBq.put("givenName", Field.zzm("givenName", 4));
            aBq.put("honorificPrefix", Field.zzm("honorificPrefix", 5));
            aBq.put("honorificSuffix", Field.zzm("honorificSuffix", 6));
            aBq.put("middleName", Field.zzm("middleName", 7));
        }

        public NameEntity() {
            this.mVersionCode = 1;
            this.aBr = new HashSet();
        }

        NameEntity(Set<Integer> set, int i, String str, String str2, String str3, String str4, String str5, String str6) {
            this.aBr = set;
            this.mVersionCode = i;
            this.it = str;
            this.aBS = str2;
            this.is = str3;
            this.aBT = str4;
            this.aBU = str5;
            this.aBV = str6;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof NameEntity)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            NameEntity nameEntity = (NameEntity) obj;
            for (Field field : aBq.values()) {
                if (zza(field)) {
                    if (!nameEntity.zza(field)) {
                        return false;
                    }
                    if (!zzb(field).equals(nameEntity.zzb(field))) {
                        return false;
                    }
                } else if (nameEntity.zza(field)) {
                    return false;
                }
            }
            return true;
        }

        public /* synthetic */ Object freeze() {
            return zzcck();
        }

        public String getFamilyName() {
            return this.it;
        }

        public String getFormatted() {
            return this.aBS;
        }

        public String getGivenName() {
            return this.is;
        }

        public String getHonorificPrefix() {
            return this.aBT;
        }

        public String getHonorificSuffix() {
            return this.aBU;
        }

        public String getMiddleName() {
            return this.aBV;
        }

        public boolean hasFamilyName() {
            return this.aBr.contains(Integer.valueOf(2));
        }

        public boolean hasFormatted() {
            return this.aBr.contains(Integer.valueOf(3));
        }

        public boolean hasGivenName() {
            return this.aBr.contains(Integer.valueOf(4));
        }

        public boolean hasHonorificPrefix() {
            return this.aBr.contains(Integer.valueOf(5));
        }

        public boolean hasHonorificSuffix() {
            return this.aBr.contains(Integer.valueOf(6));
        }

        public boolean hasMiddleName() {
            return this.aBr.contains(Integer.valueOf(7));
        }

        public int hashCode() {
            int i = 0;
            for (Field field : aBq.values()) {
                int hashCode;
                if (zza(field)) {
                    hashCode = zzb(field).hashCode() + (i + field.zzaxf());
                } else {
                    hashCode = i;
                }
                i = hashCode;
            }
            return i;
        }

        public boolean isDataValid() {
            return true;
        }

        public void writeToParcel(Parcel parcel, int i) {
            zzg.zza(this, parcel, i);
        }

        protected boolean zza(Field field) {
            return this.aBr.contains(Integer.valueOf(field.zzaxf()));
        }

        public /* synthetic */ Map zzawz() {
            return zzccd();
        }

        protected Object zzb(Field field) {
            switch (field.zzaxf()) {
                case 2:
                    return this.it;
                case 3:
                    return this.aBS;
                case 4:
                    return this.is;
                case 5:
                    return this.aBT;
                case 6:
                    return this.aBU;
                case 7:
                    return this.aBV;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + field.zzaxf());
            }
        }

        public HashMap<String, Field<?, ?>> zzccd() {
            return aBq;
        }

        public NameEntity zzcck() {
            return this;
        }
    }

    public static final class OrganizationsEntity extends FastSafeParcelableJsonResponse implements Organizations {
        public static final Creator<OrganizationsEntity> CREATOR = new zzh();
        private static final HashMap<String, Field<?, ?>> aBq = new HashMap();
        String JB;
        String aBW;
        String aBX;
        String aBY;
        boolean aBZ;
        final Set<Integer> aBr;
        String aCa;
        String cg;
        String mName;
        final int mVersionCode;
        int nV;

        static {
            aBq.put("department", Field.zzm("department", 2));
            aBq.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, Field.zzm(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, 3));
            aBq.put("endDate", Field.zzm("endDate", 4));
            aBq.put(Param.LOCATION, Field.zzm(Param.LOCATION, 5));
            aBq.put("name", Field.zzm("name", 6));
            aBq.put("primary", Field.zzl("primary", 7));
            aBq.put("startDate", Field.zzm("startDate", 8));
            aBq.put("title", Field.zzm("title", 9));
            aBq.put("type", Field.zza("type", 10, new StringToIntConverter().zzj("work", 0).zzj("school", 1), false));
        }

        public OrganizationsEntity() {
            this.mVersionCode = 1;
            this.aBr = new HashSet();
        }

        OrganizationsEntity(Set<Integer> set, int i, String str, String str2, String str3, String str4, String str5, boolean z, String str6, String str7, int i2) {
            this.aBr = set;
            this.mVersionCode = i;
            this.aBW = str;
            this.cg = str2;
            this.aBX = str3;
            this.aBY = str4;
            this.mName = str5;
            this.aBZ = z;
            this.aCa = str6;
            this.JB = str7;
            this.nV = i2;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof OrganizationsEntity)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            OrganizationsEntity organizationsEntity = (OrganizationsEntity) obj;
            for (Field field : aBq.values()) {
                if (zza(field)) {
                    if (!organizationsEntity.zza(field)) {
                        return false;
                    }
                    if (!zzb(field).equals(organizationsEntity.zzb(field))) {
                        return false;
                    }
                } else if (organizationsEntity.zza(field)) {
                    return false;
                }
            }
            return true;
        }

        public /* synthetic */ Object freeze() {
            return zzccl();
        }

        public String getDepartment() {
            return this.aBW;
        }

        public String getDescription() {
            return this.cg;
        }

        public String getEndDate() {
            return this.aBX;
        }

        public String getLocation() {
            return this.aBY;
        }

        public String getName() {
            return this.mName;
        }

        public String getStartDate() {
            return this.aCa;
        }

        public String getTitle() {
            return this.JB;
        }

        public int getType() {
            return this.nV;
        }

        public boolean hasDepartment() {
            return this.aBr.contains(Integer.valueOf(2));
        }

        public boolean hasDescription() {
            return this.aBr.contains(Integer.valueOf(3));
        }

        public boolean hasEndDate() {
            return this.aBr.contains(Integer.valueOf(4));
        }

        public boolean hasLocation() {
            return this.aBr.contains(Integer.valueOf(5));
        }

        public boolean hasName() {
            return this.aBr.contains(Integer.valueOf(6));
        }

        public boolean hasPrimary() {
            return this.aBr.contains(Integer.valueOf(7));
        }

        public boolean hasStartDate() {
            return this.aBr.contains(Integer.valueOf(8));
        }

        public boolean hasTitle() {
            return this.aBr.contains(Integer.valueOf(9));
        }

        public boolean hasType() {
            return this.aBr.contains(Integer.valueOf(10));
        }

        public int hashCode() {
            int i = 0;
            for (Field field : aBq.values()) {
                int hashCode;
                if (zza(field)) {
                    hashCode = zzb(field).hashCode() + (i + field.zzaxf());
                } else {
                    hashCode = i;
                }
                i = hashCode;
            }
            return i;
        }

        public boolean isDataValid() {
            return true;
        }

        public boolean isPrimary() {
            return this.aBZ;
        }

        public void writeToParcel(Parcel parcel, int i) {
            zzh.zza(this, parcel, i);
        }

        protected boolean zza(Field field) {
            return this.aBr.contains(Integer.valueOf(field.zzaxf()));
        }

        public /* synthetic */ Map zzawz() {
            return zzccd();
        }

        protected Object zzb(Field field) {
            switch (field.zzaxf()) {
                case 2:
                    return this.aBW;
                case 3:
                    return this.cg;
                case 4:
                    return this.aBX;
                case 5:
                    return this.aBY;
                case 6:
                    return this.mName;
                case 7:
                    return Boolean.valueOf(this.aBZ);
                case 8:
                    return this.aCa;
                case 9:
                    return this.JB;
                case 10:
                    return Integer.valueOf(this.nV);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + field.zzaxf());
            }
        }

        public HashMap<String, Field<?, ?>> zzccd() {
            return aBq;
        }

        public OrganizationsEntity zzccl() {
            return this;
        }
    }

    public static final class PlacesLivedEntity extends FastSafeParcelableJsonResponse implements PlacesLived {
        public static final Creator<PlacesLivedEntity> CREATOR = new zzi();
        private static final HashMap<String, Field<?, ?>> aBq = new HashMap();
        boolean aBZ;
        final Set<Integer> aBr;
        String mValue;
        final int mVersionCode;

        static {
            aBq.put("primary", Field.zzl("primary", 2));
            aBq.put(Param.VALUE, Field.zzm(Param.VALUE, 3));
        }

        public PlacesLivedEntity() {
            this.mVersionCode = 1;
            this.aBr = new HashSet();
        }

        PlacesLivedEntity(Set<Integer> set, int i, boolean z, String str) {
            this.aBr = set;
            this.mVersionCode = i;
            this.aBZ = z;
            this.mValue = str;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof PlacesLivedEntity)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            PlacesLivedEntity placesLivedEntity = (PlacesLivedEntity) obj;
            for (Field field : aBq.values()) {
                if (zza(field)) {
                    if (!placesLivedEntity.zza(field)) {
                        return false;
                    }
                    if (!zzb(field).equals(placesLivedEntity.zzb(field))) {
                        return false;
                    }
                } else if (placesLivedEntity.zza(field)) {
                    return false;
                }
            }
            return true;
        }

        public /* synthetic */ Object freeze() {
            return zzccm();
        }

        public String getValue() {
            return this.mValue;
        }

        public boolean hasPrimary() {
            return this.aBr.contains(Integer.valueOf(2));
        }

        public boolean hasValue() {
            return this.aBr.contains(Integer.valueOf(3));
        }

        public int hashCode() {
            int i = 0;
            for (Field field : aBq.values()) {
                int hashCode;
                if (zza(field)) {
                    hashCode = zzb(field).hashCode() + (i + field.zzaxf());
                } else {
                    hashCode = i;
                }
                i = hashCode;
            }
            return i;
        }

        public boolean isDataValid() {
            return true;
        }

        public boolean isPrimary() {
            return this.aBZ;
        }

        public void writeToParcel(Parcel parcel, int i) {
            zzi.zza(this, parcel, i);
        }

        protected boolean zza(Field field) {
            return this.aBr.contains(Integer.valueOf(field.zzaxf()));
        }

        public /* synthetic */ Map zzawz() {
            return zzccd();
        }

        protected Object zzb(Field field) {
            switch (field.zzaxf()) {
                case 2:
                    return Boolean.valueOf(this.aBZ);
                case 3:
                    return this.mValue;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + field.zzaxf());
            }
        }

        public HashMap<String, Field<?, ?>> zzccd() {
            return aBq;
        }

        public PlacesLivedEntity zzccm() {
            return this;
        }
    }

    public static final class UrlsEntity extends FastSafeParcelableJsonResponse implements Urls {
        public static final Creator<UrlsEntity> CREATOR = new zzj();
        private static final HashMap<String, Field<?, ?>> aBq = new HashMap();
        final Set<Integer> aBr;
        private final int aCb;
        String ce;
        String mValue;
        final int mVersionCode;
        int nV;

        static {
            aBq.put(PlusShare.KEY_CALL_TO_ACTION_LABEL, Field.zzm(PlusShare.KEY_CALL_TO_ACTION_LABEL, 5));
            aBq.put("type", Field.zza("type", 6, new StringToIntConverter().zzj("home", 0).zzj("work", 1).zzj("blog", 2).zzj(Scopes.PROFILE, 3).zzj("other", 4).zzj("otherProfile", 5).zzj("contributor", 6).zzj("website", 7), false));
            aBq.put(Param.VALUE, Field.zzm(Param.VALUE, 4));
        }

        public UrlsEntity() {
            this.aCb = 4;
            this.mVersionCode = 1;
            this.aBr = new HashSet();
        }

        UrlsEntity(Set<Integer> set, int i, String str, int i2, String str2, int i3) {
            this.aCb = 4;
            this.aBr = set;
            this.mVersionCode = i;
            this.ce = str;
            this.nV = i2;
            this.mValue = str2;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof UrlsEntity)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            UrlsEntity urlsEntity = (UrlsEntity) obj;
            for (Field field : aBq.values()) {
                if (zza(field)) {
                    if (!urlsEntity.zza(field)) {
                        return false;
                    }
                    if (!zzb(field).equals(urlsEntity.zzb(field))) {
                        return false;
                    }
                } else if (urlsEntity.zza(field)) {
                    return false;
                }
            }
            return true;
        }

        public /* synthetic */ Object freeze() {
            return zzcco();
        }

        public String getLabel() {
            return this.ce;
        }

        public int getType() {
            return this.nV;
        }

        public String getValue() {
            return this.mValue;
        }

        public boolean hasLabel() {
            return this.aBr.contains(Integer.valueOf(5));
        }

        public boolean hasType() {
            return this.aBr.contains(Integer.valueOf(6));
        }

        public boolean hasValue() {
            return this.aBr.contains(Integer.valueOf(4));
        }

        public int hashCode() {
            int i = 0;
            for (Field field : aBq.values()) {
                int hashCode;
                if (zza(field)) {
                    hashCode = zzb(field).hashCode() + (i + field.zzaxf());
                } else {
                    hashCode = i;
                }
                i = hashCode;
            }
            return i;
        }

        public boolean isDataValid() {
            return true;
        }

        public void writeToParcel(Parcel parcel, int i) {
            zzj.zza(this, parcel, i);
        }

        protected boolean zza(Field field) {
            return this.aBr.contains(Integer.valueOf(field.zzaxf()));
        }

        public /* synthetic */ Map zzawz() {
            return zzccd();
        }

        protected Object zzb(Field field) {
            switch (field.zzaxf()) {
                case 4:
                    return this.mValue;
                case 5:
                    return this.ce;
                case 6:
                    return Integer.valueOf(this.nV);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + field.zzaxf());
            }
        }

        public HashMap<String, Field<?, ?>> zzccd() {
            return aBq;
        }

        @Deprecated
        public int zzccn() {
            return 4;
        }

        public UrlsEntity zzcco() {
            return this;
        }
    }

    public static class zza {
        public static int zznt(String str) {
            if (str.equals("person")) {
                return 0;
            }
            if (str.equals("page")) {
                return 1;
            }
            String str2 = "Unknown objectType string: ";
            String valueOf = String.valueOf(str);
            throw new IllegalArgumentException(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        }
    }

    static {
        aBq.put("aboutMe", Field.zzm("aboutMe", 2));
        aBq.put("ageRange", Field.zza("ageRange", 3, AgeRangeEntity.class));
        aBq.put("birthday", Field.zzm("birthday", 4));
        aBq.put("braggingRights", Field.zzm("braggingRights", 5));
        aBq.put("circledByCount", Field.zzk("circledByCount", 6));
        aBq.put("cover", Field.zza("cover", 7, CoverEntity.class));
        aBq.put("currentLocation", Field.zzm("currentLocation", 8));
        aBq.put("displayName", Field.zzm("displayName", 9));
        aBq.put("gender", Field.zza("gender", 12, new StringToIntConverter().zzj("male", 0).zzj("female", 1).zzj("other", 2), false));
        aBq.put(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, Field.zzm(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, 14));
        aBq.put("image", Field.zza("image", 15, ImageEntity.class));
        aBq.put("isPlusUser", Field.zzl("isPlusUser", 16));
        aBq.put("language", Field.zzm("language", 18));
        aBq.put("name", Field.zza("name", 19, NameEntity.class));
        aBq.put("nickname", Field.zzm("nickname", 20));
        aBq.put("objectType", Field.zza("objectType", 21, new StringToIntConverter().zzj("person", 0).zzj("page", 1), false));
        aBq.put("organizations", Field.zzb("organizations", 22, OrganizationsEntity.class));
        aBq.put("placesLived", Field.zzb("placesLived", 23, PlacesLivedEntity.class));
        aBq.put("plusOneCount", Field.zzk("plusOneCount", 24));
        aBq.put("relationshipStatus", Field.zza("relationshipStatus", 25, new StringToIntConverter().zzj("single", 0).zzj("in_a_relationship", 1).zzj("engaged", 2).zzj("married", 3).zzj("its_complicated", 4).zzj("open_relationship", 5).zzj("widowed", 6).zzj("in_domestic_partnership", 7).zzj("in_civil_union", 8), false));
        aBq.put("tagline", Field.zzm("tagline", 26));
        aBq.put("url", Field.zzm("url", 27));
        aBq.put("urls", Field.zzb("urls", 28, UrlsEntity.class));
        aBq.put("verified", Field.zzl("verified", 29));
    }

    public PersonEntity() {
        this.mVersionCode = 1;
        this.aBr = new HashSet();
    }

    public PersonEntity(String str, String str2, ImageEntity imageEntity, int i, String str3) {
        this.mVersionCode = 1;
        this.aBr = new HashSet();
        this.jh = str;
        this.aBr.add(Integer.valueOf(9));
        this.zzboa = str2;
        this.aBr.add(Integer.valueOf(14));
        this.aBz = imageEntity;
        this.aBr.add(Integer.valueOf(15));
        this.aBD = i;
        this.aBr.add(Integer.valueOf(21));
        this.zzae = str3;
        this.aBr.add(Integer.valueOf(27));
    }

    PersonEntity(Set<Integer> set, int i, String str, AgeRangeEntity ageRangeEntity, String str2, String str3, int i2, CoverEntity coverEntity, String str4, String str5, int i3, String str6, ImageEntity imageEntity, boolean z, String str7, NameEntity nameEntity, String str8, int i4, List<OrganizationsEntity> list, List<PlacesLivedEntity> list2, int i5, int i6, String str9, String str10, List<UrlsEntity> list3, boolean z2) {
        this.aBr = set;
        this.mVersionCode = i;
        this.aBs = str;
        this.aBt = ageRangeEntity;
        this.aBu = str2;
        this.aBv = str3;
        this.aBw = i2;
        this.aBx = coverEntity;
        this.aBy = str4;
        this.jh = str5;
        this.zzazc = i3;
        this.zzboa = str6;
        this.aBz = imageEntity;
        this.aBA = z;
        this.bZ = str7;
        this.aBB = nameEntity;
        this.aBC = str8;
        this.aBD = i4;
        this.aBE = list;
        this.aBF = list2;
        this.aBG = i5;
        this.aBH = i6;
        this.aBI = str9;
        this.zzae = str10;
        this.aBJ = list3;
        this.aBK = z2;
    }

    public static PersonEntity zzaf(byte[] bArr) {
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(bArr, 0, bArr.length);
        obtain.setDataPosition(0);
        PersonEntity personEntity = (PersonEntity) CREATOR.createFromParcel(obtain);
        obtain.recycle();
        return personEntity;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof PersonEntity)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        PersonEntity personEntity = (PersonEntity) obj;
        for (Field field : aBq.values()) {
            if (zza(field)) {
                if (!personEntity.zza(field)) {
                    return false;
                }
                if (!zzb(field).equals(personEntity.zzb(field))) {
                    return false;
                }
            } else if (personEntity.zza(field)) {
                return false;
            }
        }
        return true;
    }

    public /* synthetic */ Object freeze() {
        return zzcce();
    }

    public String getAboutMe() {
        return this.aBs;
    }

    public AgeRange getAgeRange() {
        return this.aBt;
    }

    public String getBirthday() {
        return this.aBu;
    }

    public String getBraggingRights() {
        return this.aBv;
    }

    public int getCircledByCount() {
        return this.aBw;
    }

    public Cover getCover() {
        return this.aBx;
    }

    public String getCurrentLocation() {
        return this.aBy;
    }

    public String getDisplayName() {
        return this.jh;
    }

    public int getGender() {
        return this.zzazc;
    }

    public String getId() {
        return this.zzboa;
    }

    public Image getImage() {
        return this.aBz;
    }

    public String getLanguage() {
        return this.bZ;
    }

    public Name getName() {
        return this.aBB;
    }

    public String getNickname() {
        return this.aBC;
    }

    public int getObjectType() {
        return this.aBD;
    }

    public List<Organizations> getOrganizations() {
        return (ArrayList) this.aBE;
    }

    public List<PlacesLived> getPlacesLived() {
        return (ArrayList) this.aBF;
    }

    public int getPlusOneCount() {
        return this.aBG;
    }

    public int getRelationshipStatus() {
        return this.aBH;
    }

    public String getTagline() {
        return this.aBI;
    }

    public String getUrl() {
        return this.zzae;
    }

    public List<Urls> getUrls() {
        return (ArrayList) this.aBJ;
    }

    public boolean hasAboutMe() {
        return this.aBr.contains(Integer.valueOf(2));
    }

    public boolean hasAgeRange() {
        return this.aBr.contains(Integer.valueOf(3));
    }

    public boolean hasBirthday() {
        return this.aBr.contains(Integer.valueOf(4));
    }

    public boolean hasBraggingRights() {
        return this.aBr.contains(Integer.valueOf(5));
    }

    public boolean hasCircledByCount() {
        return this.aBr.contains(Integer.valueOf(6));
    }

    public boolean hasCover() {
        return this.aBr.contains(Integer.valueOf(7));
    }

    public boolean hasCurrentLocation() {
        return this.aBr.contains(Integer.valueOf(8));
    }

    public boolean hasDisplayName() {
        return this.aBr.contains(Integer.valueOf(9));
    }

    public boolean hasGender() {
        return this.aBr.contains(Integer.valueOf(12));
    }

    public boolean hasId() {
        return this.aBr.contains(Integer.valueOf(14));
    }

    public boolean hasImage() {
        return this.aBr.contains(Integer.valueOf(15));
    }

    public boolean hasIsPlusUser() {
        return this.aBr.contains(Integer.valueOf(16));
    }

    public boolean hasLanguage() {
        return this.aBr.contains(Integer.valueOf(18));
    }

    public boolean hasName() {
        return this.aBr.contains(Integer.valueOf(19));
    }

    public boolean hasNickname() {
        return this.aBr.contains(Integer.valueOf(20));
    }

    public boolean hasObjectType() {
        return this.aBr.contains(Integer.valueOf(21));
    }

    public boolean hasOrganizations() {
        return this.aBr.contains(Integer.valueOf(22));
    }

    public boolean hasPlacesLived() {
        return this.aBr.contains(Integer.valueOf(23));
    }

    public boolean hasPlusOneCount() {
        return this.aBr.contains(Integer.valueOf(24));
    }

    public boolean hasRelationshipStatus() {
        return this.aBr.contains(Integer.valueOf(25));
    }

    public boolean hasTagline() {
        return this.aBr.contains(Integer.valueOf(26));
    }

    public boolean hasUrl() {
        return this.aBr.contains(Integer.valueOf(27));
    }

    public boolean hasUrls() {
        return this.aBr.contains(Integer.valueOf(28));
    }

    public boolean hasVerified() {
        return this.aBr.contains(Integer.valueOf(29));
    }

    public int hashCode() {
        int i = 0;
        for (Field field : aBq.values()) {
            int hashCode;
            if (zza(field)) {
                hashCode = zzb(field).hashCode() + (i + field.zzaxf());
            } else {
                hashCode = i;
            }
            i = hashCode;
        }
        return i;
    }

    public boolean isDataValid() {
        return true;
    }

    public boolean isPlusUser() {
        return this.aBA;
    }

    public boolean isVerified() {
        return this.aBK;
    }

    public void writeToParcel(Parcel parcel, int i) {
        zza.zza(this, parcel, i);
    }

    protected boolean zza(Field field) {
        return this.aBr.contains(Integer.valueOf(field.zzaxf()));
    }

    public /* synthetic */ Map zzawz() {
        return zzccd();
    }

    protected Object zzb(Field field) {
        switch (field.zzaxf()) {
            case 2:
                return this.aBs;
            case 3:
                return this.aBt;
            case 4:
                return this.aBu;
            case 5:
                return this.aBv;
            case 6:
                return Integer.valueOf(this.aBw);
            case 7:
                return this.aBx;
            case 8:
                return this.aBy;
            case 9:
                return this.jh;
            case 12:
                return Integer.valueOf(this.zzazc);
            case 14:
                return this.zzboa;
            case 15:
                return this.aBz;
            case 16:
                return Boolean.valueOf(this.aBA);
            case 18:
                return this.bZ;
            case 19:
                return this.aBB;
            case 20:
                return this.aBC;
            case 21:
                return Integer.valueOf(this.aBD);
            case 22:
                return this.aBE;
            case 23:
                return this.aBF;
            case 24:
                return Integer.valueOf(this.aBG);
            case 25:
                return Integer.valueOf(this.aBH);
            case 26:
                return this.aBI;
            case 27:
                return this.zzae;
            case 28:
                return this.aBJ;
            case 29:
                return Boolean.valueOf(this.aBK);
            default:
                throw new IllegalStateException("Unknown safe parcelable id=" + field.zzaxf());
        }
    }

    public HashMap<String, Field<?, ?>> zzccd() {
        return aBq;
    }

    public PersonEntity zzcce() {
        return this;
    }
}
