package com.pipedrive.notification;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat.BigTextStyle;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import com.newrelic.agent.android.instrumentation.BitmapFactoryInstrumentation;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.datetime.PipedriveDate;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.Activity;
import com.pipedrive.model.Activity.DateTimeInfoWithReturn;
import com.pipedrive.util.CursorHelper;
import com.pipedrive.util.StringUtils;
import com.pipedrive.util.time.TimeManager;
import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import rx.functions.Action1;
import rx.functions.Func1;

public class UINotificationManager {
    private final Set<Long> pendingActivityAlarmsCache;

    private static class Lazy {
        public static final UINotificationManager instance = new UINotificationManager();

        private Lazy() {
        }
    }

    public static UINotificationManager getInstance() {
        return Lazy.instance;
    }

    private UINotificationManager() {
        this.pendingActivityAlarmsCache = new HashSet();
    }

    public void processNotificationForActivity(@NonNull final Session session, @Nullable Activity activity) {
        if (activity != null) {
            Long activitySqlId = activity.getSqlIdOrNull();
            if (activitySqlId != null) {
                boolean activityIsUndone;
                boolean activityShouldBeScheduled;
                cancelActivityAlarmAndNotification(session, activitySqlId.longValue());
                PipedriveDateTime remindDateTime = ((ReminderOption) activity.request(new DateTimeInfoWithReturn<ReminderOption>() {
                    public ReminderOption activityHasStartDateTime(@NonNull PipedriveDateTime dateTime, @NonNull Activity inActivity) {
                        return session.getSharedSession().getActivityReminderOption();
                    }

                    public ReminderOption activityHasStartDate(@NonNull PipedriveDate date, @NonNull Activity inActivity) {
                        return session.getSharedSession().getAllDayActivityReminderOption();
                    }
                })).getRemindDateTime(activity);
                if (activity.isDone()) {
                    activityIsUndone = false;
                } else {
                    activityIsUndone = true;
                }
                boolean remindTimeIsInFuture;
                if (remindDateTime == null || !remindDateTime.after(PipedriveDateTime.instanceWithCurrentDateTime())) {
                    remindTimeIsInFuture = false;
                } else {
                    remindTimeIsInFuture = true;
                }
                boolean activityIsAssignedToAuthenticatedUser;
                if (activity.getAssignedUserId() == session.getAuthenticatedUserID()) {
                    activityIsAssignedToAuthenticatedUser = true;
                } else {
                    activityIsAssignedToAuthenticatedUser = false;
                }
                if (activityIsUndone && activityIsAssignedToAuthenticatedUser && remindTimeIsInFuture) {
                    activityShouldBeScheduled = true;
                } else {
                    activityShouldBeScheduled = false;
                }
                if (activityShouldBeScheduled) {
                    scheduleActivityNotification(session, activitySqlId.longValue(), remindDateTime.getTime());
                }
            }
        }
    }

    public void processNotificationsForAllActivities(@NonNull final Session session) {
        final ActivitiesDataSource activitiesDataSource = new ActivitiesDataSource(session);
        CursorHelper.observeAllRows(activitiesDataSource.getUndoneActiveFutureActivitiesCursor()).map(new Func1<Cursor, Activity>() {
            public Activity call(Cursor cursor) {
                return activitiesDataSource.deflateCursor(cursor);
            }
        }).subscribe(new Action1<Activity>() {
            public void call(Activity activity) {
                UINotificationManager.getInstance().processNotificationForActivity(session, activity);
            }
        }, new Action1<Throwable>() {
            public void call(Throwable throwable) {
            }
        });
    }

    public void cancelAllActivityAlarmsAndNotifications(@NonNull Session session) {
        for (Long activitySqlId : this.pendingActivityAlarmsCache) {
            cancelActivityAlarmAndNotification(session, activitySqlId.longValue());
        }
        this.pendingActivityAlarmsCache.clear();
    }

    private void scheduleActivityNotification(@NonNull Session session, long activitySqlId, long triggerAtMillis) {
        AlarmManager alarmManager = (AlarmManager) session.getApplicationContext().getSystemService("alarm");
        PendingIntent operation = UINotificationReceiver.getIntentForActivityNotification(session, activitySqlId);
        this.pendingActivityAlarmsCache.add(Long.valueOf(activitySqlId));
        try {
            if (VERSION.SDK_INT < 19) {
                alarmManager.set(0, triggerAtMillis, operation);
            } else {
                alarmManager.setExact(0, triggerAtMillis, operation);
            }
        } catch (SecurityException e) {
            Log.e(e);
            LogJourno.reportEvent(EVENT.NotSpecified_scheduledAlarmLimitExceeded);
        }
    }

    void showActivityNotification(@NonNull Session session, @NonNull Activity activity) {
        Long activitySqlId = activity.getSqlIdOrNull();
        if (!(activitySqlId == null)) {
            this.pendingActivityAlarmsCache.remove(activitySqlId);
            if (!activity.isDone()) {
                Long notificationTimestamp = (Long) activity.request(new DateTimeInfoWithReturn<Long>() {
                    public Long activityHasStartDateTime(@NonNull PipedriveDateTime dateTime, @NonNull Activity inActivity) {
                        return Long.valueOf(dateTime.getTime());
                    }

                    public Long activityHasStartDate(@NonNull PipedriveDate date, @NonNull Activity inActivity) {
                        TimeZone defaultTimeZone = TimeManager.getInstance().getDefaultTimeZone();
                        long activityStartDateTime = TimeUnit.SECONDS.toMillis(date.getRepresentationInUnixTime());
                        return Long.valueOf((TimeUnit.HOURS.toMillis(9) + activityStartDateTime) - ((long) defaultTimeZone.getOffset(activityStartDateTime)));
                    }
                });
                if (notificationTimestamp != null) {
                    Context context = session.getApplicationContext();
                    String associationString = activity.getAssociationString(context);
                    boolean noteExist = !StringUtils.isTrimmedAndEmpty(activity.getNote());
                    boolean associationsExist = !StringUtils.isTrimmedAndEmpty(associationString);
                    Builder notificationBuilder = new Builder(context).setDefaults(-1).setSmallIcon(R.drawable.ic_notification).setLargeIcon(getLargeIcon(context, activity)).setContentTitle(activity.getSubject()).setColor(ContextCompat.getColor(context, R.color.dark)).setContentText(associationString).setTicker(activity.getSubject()).setContentIntent(UINotificationReceiver.getIntentForOpeningActivityFromNotification(session, activitySqlId.longValue())).setWhen(notificationTimestamp.longValue()).setAutoCancel(true).addAction(R.drawable.ic_check, context.getString(R.string.mark_as_done), UINotificationReceiver.getIntentForMarkingActivityAsDone(session, activitySqlId.longValue()));
                    if (noteExist) {
                        StringBuilder textContent = new StringBuilder();
                        if (associationsExist) {
                            textContent.append(associationString);
                            textContent.append("\n");
                        }
                        textContent.append(Html.fromHtml(activity.getNote()));
                        notificationBuilder.setStyle(new BigTextStyle().bigText(textContent.toString()));
                    }
                    ((NotificationManager) context.getSystemService("notification")).notify(getNotificationId(activitySqlId.longValue()), notificationBuilder.build());
                }
            }
        }
    }

    @NonNull
    private Bitmap getLargeIcon(@NonNull Context context, @NonNull Activity activity) {
        int size = context.getResources().getDimensionPixelSize(R.dimen.notification.largeIconSize);
        Bitmap largeIcon = Bitmap.createBitmap(size, size, Config.ARGB_8888);
        Canvas canvas = new Canvas(largeIcon);
        Paint backgroundFillPaint = new Paint();
        backgroundFillPaint.setColor(ContextCompat.getColor(context, R.color.fog));
        canvas.drawCircle(((float) size) / 2.0f, ((float) size) / 2.0f, (float) (size / 2), backgroundFillPaint);
        Bitmap activityTypeIcon = BitmapFactoryInstrumentation.decodeResource(context.getResources(), activity.getType() != null ? activity.getType().getImageResourceId() : R.drawable.actionbar_call);
        canvas.drawBitmap(activityTypeIcon, (float) ((size - activityTypeIcon.getWidth()) / 2), (float) ((size - activityTypeIcon.getHeight()) / 2), new Paint(2));
        return largeIcon;
    }

    private void cancelActivityAlarmAndNotification(@NonNull Session session, long activitySqlId) {
        cancelActivityAlarm(session, activitySqlId);
        cancelActivityNotification(session, activitySqlId);
    }

    private void cancelActivityNotification(@NonNull Session session, long activitySqlId) {
        ((NotificationManager) session.getApplicationContext().getSystemService("notification")).cancel(getNotificationId(activitySqlId));
    }

    private int getNotificationId(long activitySqlId) {
        return Long.valueOf(activitySqlId).intValue();
    }

    private void cancelActivityAlarm(@NonNull Session session, long activitySqlId) {
        PendingIntent alarmIntent = UINotificationReceiver.getIntentForActivityNotification(session, activitySqlId);
        ((AlarmManager) session.getApplicationContext().getSystemService("alarm")).cancel(alarmIntent);
        alarmIntent.cancel();
    }
}
