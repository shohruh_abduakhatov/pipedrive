package com.google.android.gms.tagmanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build.VERSION;
import android.text.TextUtils;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.common.util.zzh;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

class zzx implements zzc {
    private static final String aEP = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' STRING NOT NULL, '%s' BLOB NOT NULL, '%s' INTEGER NOT NULL);", new Object[]{"datalayer", "ID", "key", Param.VALUE, "expires"});
    private final Executor aEQ;
    private zza aER;
    private int aES;
    private final Context mContext;
    private zze zzaql;

    class zza extends SQLiteOpenHelper {
        final /* synthetic */ zzx aEV;

        zza(zzx com_google_android_gms_tagmanager_zzx, Context context, String str) {
            this.aEV = com_google_android_gms_tagmanager_zzx;
            super(context, str, null, 1);
        }

        private boolean zza(String str, SQLiteDatabase sQLiteDatabase) {
            String str2;
            Cursor cursor;
            String str3;
            Throwable th;
            Cursor cursor2 = null;
            try {
                str2 = "SQLITE_MASTER";
                String[] strArr = new String[]{"name"};
                String str4 = "name=?";
                String[] strArr2 = new String[]{str};
                Cursor query = !(sQLiteDatabase instanceof SQLiteDatabase) ? sQLiteDatabase.query(str2, strArr, str4, strArr2, null, null, null) : SQLiteInstrumentation.query(sQLiteDatabase, str2, strArr, str4, strArr2, null, null, null);
                try {
                    boolean moveToFirst = query.moveToFirst();
                    if (query == null) {
                        return moveToFirst;
                    }
                    query.close();
                    return moveToFirst;
                } catch (SQLiteException e) {
                    cursor = query;
                    try {
                        str3 = "Error querying for table ";
                        str2 = String.valueOf(str);
                        zzbo.zzdi(str2.length() == 0 ? new String(str3) : str3.concat(str2));
                        if (cursor != null) {
                            cursor.close();
                        }
                        return false;
                    } catch (Throwable th2) {
                        cursor2 = cursor;
                        th = th2;
                        if (cursor2 != null) {
                            cursor2.close();
                        }
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    cursor2 = query;
                    if (cursor2 != null) {
                        cursor2.close();
                    }
                    throw th;
                }
            } catch (SQLiteException e2) {
                cursor = null;
                str3 = "Error querying for table ";
                str2 = String.valueOf(str);
                if (str2.length() == 0) {
                }
                zzbo.zzdi(str2.length() == 0 ? new String(str3) : str3.concat(str2));
                if (cursor != null) {
                    cursor.close();
                }
                return false;
            } catch (Throwable th4) {
                th = th4;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
        }

        private void zzc(SQLiteDatabase sQLiteDatabase) {
            String str = "SELECT * FROM datalayer WHERE 0";
            Cursor rawQuery = !(sQLiteDatabase instanceof SQLiteDatabase) ? sQLiteDatabase.rawQuery(str, null) : SQLiteInstrumentation.rawQuery(sQLiteDatabase, str, null);
            Set hashSet = new HashSet();
            try {
                String[] columnNames = rawQuery.getColumnNames();
                for (Object add : columnNames) {
                    hashSet.add(add);
                }
                if (!hashSet.remove("key") || !hashSet.remove(Param.VALUE) || !hashSet.remove("ID") || !hashSet.remove("expires")) {
                    throw new SQLiteException("Database column missing");
                } else if (!hashSet.isEmpty()) {
                    throw new SQLiteException("Database has extra columns");
                }
            } finally {
                rawQuery.close();
            }
        }

        public SQLiteDatabase getWritableDatabase() {
            SQLiteDatabase sQLiteDatabase = null;
            try {
                sQLiteDatabase = super.getWritableDatabase();
            } catch (SQLiteException e) {
                this.aEV.mContext.getDatabasePath("google_tagmanager.db").delete();
            }
            return sQLiteDatabase == null ? super.getWritableDatabase() : sQLiteDatabase;
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            zzan.zzfd(sQLiteDatabase.getPath());
        }

        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (VERSION.SDK_INT < 15) {
                String str = "PRAGMA journal_mode=memory";
                Cursor rawQuery = !(sQLiteDatabase instanceof SQLiteDatabase) ? sQLiteDatabase.rawQuery(str, null) : SQLiteInstrumentation.rawQuery(sQLiteDatabase, str, null);
                try {
                    rawQuery.moveToFirst();
                } finally {
                    rawQuery.close();
                }
            }
            if (zza("datalayer", sQLiteDatabase)) {
                zzc(sQLiteDatabase);
                return;
            }
            String zzcey = zzx.aEP;
            if (sQLiteDatabase instanceof SQLiteDatabase) {
                SQLiteInstrumentation.execSQL(sQLiteDatabase, zzcey);
            } else {
                sQLiteDatabase.execSQL(zzcey);
            }
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        }
    }

    private static class zzb {
        final byte[] aEY;
        final String zzbcn;

        zzb(String str, byte[] bArr) {
            this.zzbcn = str;
            this.aEY = bArr;
        }

        public String toString() {
            String str = this.zzbcn;
            return new StringBuilder(String.valueOf(str).length() + 54).append("KeyAndSerialized: key = ").append(str).append(" serialized hash = ").append(Arrays.hashCode(this.aEY)).toString();
        }
    }

    public zzx(Context context) {
        this(context, zzh.zzayl(), "google_tagmanager.db", 2000, Executors.newSingleThreadExecutor());
    }

    zzx(Context context, zze com_google_android_gms_common_util_zze, String str, int i, Executor executor) {
        this.mContext = context;
        this.zzaql = com_google_android_gms_common_util_zze;
        this.aES = i;
        this.aEQ = executor;
        this.aER = new zza(this, this.mContext, str);
    }

    private void zzaaa(int i) {
        int zzcew = (zzcew() - this.aES) + i;
        if (zzcew > 0) {
            List zzaab = zzaab(zzcew);
            zzbo.zzdh("DataLayer store full, deleting " + zzaab.size() + " entries to make room.");
            zzg((String[]) zzaab.toArray(new String[0]));
        }
    }

    private List<String> zzaab(int i) {
        SQLiteException e;
        String str;
        String valueOf;
        Throwable th;
        Cursor cursor = null;
        List<String> arrayList = new ArrayList();
        if (i <= 0) {
            zzbo.zzdi("Invalid maxEntries specified. Skipping.");
            return arrayList;
        }
        SQLiteDatabase zzpf = zzpf("Error opening database for peekEntryIds.");
        if (zzpf == null) {
            return arrayList;
        }
        Cursor query;
        try {
            String str2 = "datalayer";
            String[] strArr = new String[]{"ID"};
            String format = String.format("%s ASC", new Object[]{"ID"});
            String num = Integer.toString(i);
            query = !(zzpf instanceof SQLiteDatabase) ? zzpf.query(str2, strArr, null, null, null, null, format, num) : SQLiteInstrumentation.query(zzpf, str2, strArr, null, null, null, null, format, num);
            try {
                if (query.moveToFirst()) {
                    do {
                        arrayList.add(String.valueOf(query.getLong(0)));
                    } while (query.moveToNext());
                }
                if (query != null) {
                    query.close();
                }
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    str = "Error in peekEntries fetching entryIds: ";
                    valueOf = String.valueOf(e.getMessage());
                    zzbo.zzdi(valueOf.length() == 0 ? str.concat(valueOf) : new String(str));
                    if (query != null) {
                        query.close();
                    }
                    return arrayList;
                } catch (Throwable th2) {
                    th = th2;
                    cursor = query;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            query = null;
            str = "Error in peekEntries fetching entryIds: ";
            valueOf = String.valueOf(e.getMessage());
            if (valueOf.length() == 0) {
            }
            zzbo.zzdi(valueOf.length() == 0 ? str.concat(valueOf) : new String(str));
            if (query != null) {
                query.close();
            }
            return arrayList;
        } catch (Throwable th3) {
            th = th3;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return arrayList;
    }

    private List<zza> zzaj(List<zzb> list) {
        List<zza> arrayList = new ArrayList();
        for (zzb com_google_android_gms_tagmanager_zzx_zzb : list) {
            arrayList.add(new zza(com_google_android_gms_tagmanager_zzx_zzb.zzbcn, zzak(com_google_android_gms_tagmanager_zzx_zzb.aEY)));
        }
        return arrayList;
    }

    private Object zzak(byte[] bArr) {
        ObjectInputStream objectInputStream;
        Object readObject;
        Throwable th;
        ObjectInputStream objectInputStream2 = null;
        InputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        try {
            objectInputStream = new ObjectInputStream(byteArrayInputStream);
            try {
                readObject = objectInputStream.readObject();
                if (objectInputStream != null) {
                    try {
                        objectInputStream.close();
                    } catch (IOException e) {
                    }
                }
                byteArrayInputStream.close();
            } catch (IOException e2) {
                if (objectInputStream != null) {
                    try {
                        objectInputStream.close();
                    } catch (IOException e3) {
                    }
                }
                byteArrayInputStream.close();
                return readObject;
            } catch (ClassNotFoundException e4) {
                if (objectInputStream != null) {
                    try {
                        objectInputStream.close();
                    } catch (IOException e5) {
                    }
                }
                byteArrayInputStream.close();
                return readObject;
            } catch (Throwable th2) {
                th = th2;
                if (objectInputStream != null) {
                    try {
                        objectInputStream.close();
                    } catch (IOException e6) {
                        throw th;
                    }
                }
                byteArrayInputStream.close();
                throw th;
            }
        } catch (IOException e7) {
            objectInputStream = objectInputStream2;
            if (objectInputStream != null) {
                objectInputStream.close();
            }
            byteArrayInputStream.close();
            return readObject;
        } catch (ClassNotFoundException e8) {
            objectInputStream = objectInputStream2;
            if (objectInputStream != null) {
                objectInputStream.close();
            }
            byteArrayInputStream.close();
            return readObject;
        } catch (Throwable th3) {
            Throwable th4 = th3;
            objectInputStream = objectInputStream2;
            th = th4;
            if (objectInputStream != null) {
                objectInputStream.close();
            }
            byteArrayInputStream.close();
            throw th;
        }
        return readObject;
    }

    private List<zzb> zzak(List<zza> list) {
        List<zzb> arrayList = new ArrayList();
        for (zza com_google_android_gms_tagmanager_DataLayer_zza : list) {
            arrayList.add(new zzb(com_google_android_gms_tagmanager_DataLayer_zza.zzbcn, zzal(com_google_android_gms_tagmanager_DataLayer_zza.zzcyd)));
        }
        return arrayList;
    }

    private byte[] zzal(Object obj) {
        Throwable th;
        byte[] bArr = null;
        OutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream;
        try {
            objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            try {
                objectOutputStream.writeObject(obj);
                bArr = byteArrayOutputStream.toByteArray();
                if (objectOutputStream != null) {
                    try {
                        objectOutputStream.close();
                    } catch (IOException e) {
                    }
                }
                byteArrayOutputStream.close();
            } catch (IOException e2) {
                if (objectOutputStream != null) {
                    try {
                        objectOutputStream.close();
                    } catch (IOException e3) {
                    }
                }
                byteArrayOutputStream.close();
                return bArr;
            } catch (Throwable th2) {
                th = th2;
                if (objectOutputStream != null) {
                    try {
                        objectOutputStream.close();
                    } catch (IOException e4) {
                        throw th;
                    }
                }
                byteArrayOutputStream.close();
                throw th;
            }
        } catch (IOException e5) {
            objectOutputStream = bArr;
            if (objectOutputStream != null) {
                objectOutputStream.close();
            }
            byteArrayOutputStream.close();
            return bArr;
        } catch (Throwable th3) {
            Throwable th4 = th3;
            objectOutputStream = bArr;
            th = th4;
            if (objectOutputStream != null) {
                objectOutputStream.close();
            }
            byteArrayOutputStream.close();
            throw th;
        }
        return bArr;
    }

    private synchronized void zzb(List<zzb> list, long j) {
        try {
            long currentTimeMillis = this.zzaql.currentTimeMillis();
            zzbu(currentTimeMillis);
            zzaaa(list.size());
            zzc(list, currentTimeMillis + j);
            zzcex();
        } catch (Throwable th) {
            zzcex();
        }
    }

    private void zzbu(long j) {
        SQLiteDatabase zzpf = zzpf("Error opening database for deleteOlderThan.");
        if (zzpf != null) {
            try {
                String str = "datalayer";
                String str2 = "expires <= ?";
                String[] strArr = new String[]{Long.toString(j)};
                zzbo.v("Deleted " + (!(zzpf instanceof SQLiteDatabase) ? zzpf.delete(str, str2, strArr) : SQLiteInstrumentation.delete(zzpf, str, str2, strArr)) + " expired items");
            } catch (SQLiteException e) {
                zzbo.zzdi("Error deleting old entries.");
            }
        }
    }

    private void zzc(List<zzb> list, long j) {
        SQLiteDatabase zzpf = zzpf("Error opening database for writeEntryToDatabase.");
        if (zzpf != null) {
            for (zzb com_google_android_gms_tagmanager_zzx_zzb : list) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("expires", Long.valueOf(j));
                contentValues.put("key", com_google_android_gms_tagmanager_zzx_zzb.zzbcn);
                contentValues.put(Param.VALUE, com_google_android_gms_tagmanager_zzx_zzb.aEY);
                String str = "datalayer";
                if (zzpf instanceof SQLiteDatabase) {
                    SQLiteInstrumentation.insert(zzpf, str, null, contentValues);
                } else {
                    zzpf.insert(str, null, contentValues);
                }
            }
        }
    }

    private List<zza> zzceu() {
        try {
            zzbu(this.zzaql.currentTimeMillis());
            List<zza> zzaj = zzaj(zzcev());
            return zzaj;
        } finally {
            zzcex();
        }
    }

    private List<zzb> zzcev() {
        SQLiteDatabase zzpf = zzpf("Error opening database for loadSerialized.");
        List<zzb> arrayList = new ArrayList();
        if (zzpf == null) {
            return arrayList;
        }
        String[] strArr = new String[]{"key", Param.VALUE};
        String str = "datalayer";
        String str2 = "ID";
        Cursor query = !(zzpf instanceof SQLiteDatabase) ? zzpf.query(str, strArr, null, null, null, null, str2, null) : SQLiteInstrumentation.query(zzpf, str, strArr, null, null, null, null, str2, null);
        while (query.moveToNext()) {
            try {
                arrayList.add(new zzb(query.getString(0), query.getBlob(1)));
            } finally {
                query.close();
            }
        }
        return arrayList;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int zzcew() {
        int i;
        Throwable th;
        Cursor cursor = null;
        SQLiteDatabase zzpf = zzpf("Error opening database for getNumStoredEntries.");
        if (zzpf == null) {
            return 0;
        }
        try {
            String str = "SELECT COUNT(*) from datalayer";
            cursor = !(zzpf instanceof SQLiteDatabase) ? zzpf.rawQuery(str, null) : SQLiteInstrumentation.rawQuery(zzpf, str, null);
            i = cursor.moveToFirst() ? (int) cursor.getLong(0) : 0;
            if (cursor != null) {
                cursor.close();
            }
        } catch (SQLiteException e) {
            Cursor cursor2 = cursor;
            try {
                zzbo.zzdi("Error getting numStoredEntries");
                if (cursor2 == null) {
                    i = 0;
                } else {
                    cursor2.close();
                    i = 0;
                }
                return i;
            } catch (Throwable th2) {
                cursor = cursor2;
                th = th2;
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return i;
    }

    private void zzcex() {
        try {
            this.aER.close();
        } catch (SQLiteException e) {
        }
    }

    private void zzg(String[] strArr) {
        if (strArr != null && strArr.length != 0) {
            SQLiteDatabase zzpf = zzpf("Error opening database for deleteEntries.");
            if (zzpf != null) {
                String format = String.format("%s in (%s)", new Object[]{"ID", TextUtils.join(Table.COMMA_SEP, Collections.nCopies(strArr.length, "?"))});
                try {
                    String str = "datalayer";
                    if (zzpf instanceof SQLiteDatabase) {
                        SQLiteInstrumentation.delete(zzpf, str, format, strArr);
                    } else {
                        zzpf.delete(str, format, strArr);
                    }
                } catch (SQLiteException e) {
                    format = "Error deleting entries ";
                    String valueOf = String.valueOf(Arrays.toString(strArr));
                    zzbo.zzdi(valueOf.length() != 0 ? format.concat(valueOf) : new String(format));
                }
            }
        }
    }

    private void zzpe(String str) {
        SQLiteDatabase zzpf = zzpf("Error opening database for clearKeysWithPrefix.");
        if (zzpf != null) {
            try {
                String str2 = "datalayer";
                String str3 = "key = ? OR key LIKE ?";
                String[] strArr = new String[]{str, String.valueOf(str).concat(".%")};
                zzbo.v("Cleared " + (!(zzpf instanceof SQLiteDatabase) ? zzpf.delete(str2, str3, strArr) : SQLiteInstrumentation.delete(zzpf, str2, str3, strArr)) + " items");
            } catch (SQLiteException e) {
                String valueOf = String.valueOf(e);
                zzbo.zzdi(new StringBuilder((String.valueOf(str).length() + 44) + String.valueOf(valueOf).length()).append("Error deleting entries with key prefix: ").append(str).append(" (").append(valueOf).append(").").toString());
            } finally {
                zzcex();
            }
        }
    }

    private SQLiteDatabase zzpf(String str) {
        try {
            return this.aER.getWritableDatabase();
        } catch (SQLiteException e) {
            zzbo.zzdi(str);
            return null;
        }
    }

    public void zza(final com.google.android.gms.tagmanager.DataLayer.zzc.zza com_google_android_gms_tagmanager_DataLayer_zzc_zza) {
        this.aEQ.execute(new Runnable(this) {
            final /* synthetic */ zzx aEV;

            public void run() {
                com_google_android_gms_tagmanager_DataLayer_zzc_zza.zzai(this.aEV.zzceu());
            }
        });
    }

    public void zza(List<zza> list, final long j) {
        final List zzak = zzak((List) list);
        this.aEQ.execute(new Runnable(this) {
            final /* synthetic */ zzx aEV;

            public void run() {
                this.aEV.zzb(zzak, j);
            }
        });
    }

    public void zzpd(final String str) {
        this.aEQ.execute(new Runnable(this) {
            final /* synthetic */ zzx aEV;

            public void run() {
                this.aEV.zzpe(str);
            }
        });
    }
}
