package com.zendesk.sdk.model.access;

public class AuthenticationResponse {
    AccessToken authentication;

    public AccessToken getAuthentication() {
        return this.authentication;
    }
}
