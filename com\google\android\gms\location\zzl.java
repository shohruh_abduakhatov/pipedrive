package com.google.android.gms.location;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.List;

public class zzl implements Creator<LocationResult> {
    static void zza(LocationResult locationResult, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zzc(parcel, 1, locationResult.getLocations(), false);
        zzb.zzc(parcel, 1000, locationResult.getVersionCode());
        zzb.zzaj(parcel, zzcs);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zznx(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzut(i);
    }

    public LocationResult zznx(Parcel parcel) {
        int zzcr = zza.zzcr(parcel);
        int i = 0;
        List list = LocationResult.akr;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    list = zza.zzc(parcel, zzcq, Location.CREATOR);
                    break;
                case 1000:
                    i = zza.zzg(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new LocationResult(i, list);
        }
        throw new zza.zza("Overread allowed size end=" + zzcr, parcel);
    }

    public LocationResult[] zzut(int i) {
        return new LocationResult[i];
    }
}
