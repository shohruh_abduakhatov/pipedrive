package com.pipedrive.util.clone;

import android.os.Parcelable;

abstract class CloneableParcel<T> implements Parcelable {
    public abstract T getDeepCopy();

    CloneableParcel() {
    }

    public boolean isEqual(CloneableParcel parcelable) {
        return Cloner.isEqual(this, parcelable);
    }
}
