package com.pipedrive.nearby.toolbar;

import android.support.annotation.NonNull;
import com.pipedrive.nearby.util.Irrelevant;
import rx.Observable;

public interface HomeButtonClickProvider {
    @NonNull
    Observable<Irrelevant> homeButtonClicks();
}
