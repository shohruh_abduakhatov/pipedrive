package com.zendesk.sdk.deeplinking;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.deeplinking.targets.DeepLinkingTarget;
import com.zendesk.sdk.deeplinking.targets.DeepLinkingTargetArticle;
import com.zendesk.sdk.deeplinking.targets.DeepLinkingTargetRequest;

public class ZendeskDeepLinkingBroadcastReceiver extends BroadcastReceiver {
    private static String LOG_TAG = ZendeskDeepLinkingBroadcastReceiver.class.getSimpleName();

    public void onReceive(Context context, Intent intent) {
        switch (DeepLinkingTarget.getDeepLinkType(intent)) {
            case Request:
                new DeepLinkingTargetRequest().execute(context, intent);
                return;
            case Article:
                new DeepLinkingTargetArticle().execute(context, intent);
                return;
            case Unknown:
                Logger.e(LOG_TAG, "Unknown intent", new Object[0]);
                return;
            default:
                return;
        }
    }
}
