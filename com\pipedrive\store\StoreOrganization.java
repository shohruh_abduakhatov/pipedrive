package com.pipedrive.store;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.changes.OrganizationChanger;
import com.pipedrive.model.Organization;
import com.pipedrive.util.StringUtils;

public class StoreOrganization extends Store<Organization> {
    public StoreOrganization(@NonNull Session session) {
        super(session);
    }

    void cleanupBeforeCreate(@NonNull Organization newOrganizationBeingStored) {
        processOrgBeforeStoringShared(newOrganizationBeingStored);
        newOrganizationBeingStored.setIsActive(true);
    }

    boolean implementCreate(@NonNull Organization newOrganization) {
        return new OrganizationChanger(getSession()).create(newOrganization);
    }

    void cleanupBeforeUpdate(@NonNull Organization updatedEntityBeingStored) {
        processOrgBeforeStoringShared(updatedEntityBeingStored);
    }

    boolean implementUpdate(@NonNull Organization updatedOrganization) {
        return new OrganizationChanger(getSession()).update(updatedOrganization);
    }

    private void processOrgBeforeStoringShared(Organization orgBeingSaved) {
        Session session = getSession();
        if (orgBeingSaved.getOwnerPipedriveId() <= 0) {
            orgBeingSaved.setOwnerPipedriveId(Long.valueOf(session.getAuthenticatedUserID()).intValue());
        }
        if (StringUtils.isTrimmedAndEmpty(orgBeingSaved.getOwnerName()) && ((long) orgBeingSaved.getOwnerPipedriveId()) == session.getAuthenticatedUserID()) {
            orgBeingSaved.setOwnerName(session.getUserSettingsName(null));
        }
    }
}
