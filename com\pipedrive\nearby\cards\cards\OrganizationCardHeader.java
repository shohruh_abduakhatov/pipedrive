package com.pipedrive.nearby.cards.cards;

import android.support.annotation.NonNull;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.model.Organization;
import com.pipedrive.nearby.model.OrganizationNearbyItem;

public class OrganizationCardHeader extends CardHeader<OrganizationNearbyItem, Organization> {
    @NonNull
    String getSubTitle(@NonNull Session session, @NonNull OrganizationNearbyItem nearbyItem, @NonNull Organization entity) {
        Integer openDealCount = (Integer) nearbyItem.getOpenDealCount(session).toBlocking().first();
        if (openDealCount.intValue() == 0) {
            return "";
        }
        return this.context.getResources().getQuantityString(R.plurals.cards_open_deals, openDealCount.intValue(), new Object[]{openDealCount});
    }

    @NonNull
    protected String getTitle(@NonNull Organization entity) {
        return entity.getName();
    }

    public void propagateClickEvents(@NonNull Long itemSqlId) {
        CardEventBus.INSTANCE.onOrganizationDetailsClicked(itemSqlId);
    }
}
