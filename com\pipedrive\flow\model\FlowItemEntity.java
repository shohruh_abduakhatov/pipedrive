package com.pipedrive.flow.model;

import android.support.annotation.NonNull;
import com.pipedrive.datetime.PipedriveDateTime;

public class FlowItemEntity {
    private String mDealName;
    private Boolean mExtraBoolean;
    private Long mExtraNumber;
    private String mExtraString;
    private String mExtraString2;
    private String mOrgName;
    private String mPersonName;
    private Long mPipedriveId;
    private Long mSqlId;
    private PipedriveDateTime mTimestamp;
    private String mTitle;
    private String mType;

    public FlowItemEntity(@NonNull String title) {
        this(null, null, null, null, title, null, null, null, null, null, null, null);
    }

    FlowItemEntity(Long sqlId, Long pipedriveId, String type, PipedriveDateTime timestamp, String title, String dealName, String personName, String orgName, Boolean extraBoolean, Long extraNumber, String extraString, String extraString2) {
        this.mSqlId = sqlId;
        this.mPipedriveId = pipedriveId;
        this.mType = type;
        this.mTimestamp = timestamp;
        this.mTitle = title;
        this.mDealName = dealName;
        this.mPersonName = personName;
        this.mOrgName = orgName;
        this.mExtraBoolean = extraBoolean;
        this.mExtraNumber = extraNumber;
        this.mExtraString = extraString;
        this.mExtraString2 = extraString2;
    }

    public Long getSqlId() {
        return this.mSqlId;
    }

    public Long getPipedriveId() {
        return this.mPipedriveId;
    }

    public String getType() {
        return this.mType;
    }

    public PipedriveDateTime getTimestamp() {
        return this.mTimestamp;
    }

    public String getTitle() {
        return this.mTitle;
    }

    public String getDealName() {
        return this.mDealName;
    }

    public String getPersonName() {
        return this.mPersonName;
    }

    public String getOrgName() {
        return this.mOrgName;
    }

    public Boolean getExtraBoolean() {
        return this.mExtraBoolean;
    }

    public Long getExtraNumber() {
        return this.mExtraNumber;
    }

    public String getExtraString() {
        return this.mExtraString;
    }

    public String getExtraString2() {
        return this.mExtraString2;
    }
}
