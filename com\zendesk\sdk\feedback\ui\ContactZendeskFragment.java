package com.zendesk.sdk.feedback.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.background.ApplicationStateMonitor;
import com.newrelic.agent.android.instrumentation.Instrumented;
import com.newrelic.agent.android.tracing.TraceMachine;
import com.zendesk.belvedere.BelvedereCallback;
import com.zendesk.belvedere.BelvedereResult;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.sdk.attachment.AttachmentHelper;
import com.zendesk.sdk.attachment.ImageUploadHelper;
import com.zendesk.sdk.attachment.ImageUploadHelper.ImageUploadProgressListener;
import com.zendesk.sdk.attachment.ZendeskBelvedereProvider;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConnector;
import com.zendesk.sdk.feedback.ui.AttachmentContainerHost.AttachmentContainerListener;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.model.access.AnonymousIdentity.Builder;
import com.zendesk.sdk.model.access.Identity;
import com.zendesk.sdk.model.request.CreateRequest;
import com.zendesk.sdk.model.request.UploadResponse;
import com.zendesk.sdk.model.settings.SafeMobileSettings;
import com.zendesk.sdk.network.Retryable;
import com.zendesk.sdk.network.SettingsHelper;
import com.zendesk.sdk.network.SubmissionListener;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.ui.EmailAddressAutoCompleteTextView;
import com.zendesk.sdk.ui.TextWatcherAdapter;
import com.zendesk.sdk.util.NetworkUtils;
import com.zendesk.service.ErrorResponse;
import com.zendesk.service.ErrorResponseAdapter;
import com.zendesk.service.SafeZendeskCallback;
import com.zendesk.service.ZendeskCallback;
import com.zendesk.util.StringUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Instrumented
public class ContactZendeskFragment extends Fragment implements TraceFieldInterface {
    public static final String EXTRA_CONFIGURATION = "EXTRA_CONFIGURATION";
    public static final String EXTRA_CONFIGURATION_ADDITIONAL_INFO = "EXTRA_CONFIGURATION_ADDITIONAL";
    public static final String EXTRA_CONFIGURATION_REQUEST_SUBJECT = "EXTRA_CONFIGURATION_SUBJECT";
    public static final String EXTRA_CONFIGURATION_TAGS = "EXTRA_CONFIGURATION_TAGS";
    private static final int FOURTY_PERCENT_OPACITY = 102;
    private static final int FULL_OPACTITY = 255;
    private static final String LOG_TAG = ContactZendeskFragment.class.getSimpleName();
    private AttachmentContainerHost attachmentContainerHost;
    private AttachmentContainerListener attachmentContainerListener;
    private MenuItem attachmentItem;
    private ContactZendeskFeedbackConfiguration contactZendeskFeedbackConfiguration;
    private ViewGroup container;
    private EditText descriptionEditText;
    private MenuItem doneMenuItem;
    private EmailAddressAutoCompleteTextView emailEditText;
    private SubmissionListener feedbackListener;
    private ImageUploadProgressListener imageUploadProgressListener;
    private ImageUploadHelper imageUploadService;
    private BelvedereCallback<List<BelvedereResult>> imagesExtracted;
    private boolean isEmailFieldVisible;
    private SafeMobileSettings mobileSettings;
    private ProgressBar progressBar;
    private SafeZendeskCallback<CreateRequest> requestCallback;
    private Retryable retryable;
    private SafeZendeskCallback<SafeMobileSettings> settingsCallback;
    private SettingsHelper settingsHelper;

    private class ContactZendeskFeedbackConfiguration implements ZendeskFeedbackConfiguration {
        private final ZendeskFeedbackConfiguration mConfiguration;

        public ContactZendeskFeedbackConfiguration(ZendeskFeedbackConfiguration configuration) {
            this.mConfiguration = configuration;
        }

        public List<String> getTags() {
            return this.mConfiguration.getTags();
        }

        public String getAdditionalInfo() {
            return this.mConfiguration.getAdditionalInfo();
        }

        public String getRequestSubject() {
            return this.mConfiguration.getRequestSubject();
        }
    }

    class RequestCallback extends ZendeskCallback<CreateRequest> {
        RequestCallback() {
        }

        public void onSuccess(CreateRequest result) {
            if (result == null || result.getId() == null) {
                Logger.e(ContactZendeskFragment.LOG_TAG, "Attempted to store a null request in callback.", new Object[0]);
            } else {
                ZendeskConfig.INSTANCE.storage().requestStorage().setCommentCount(result.getId(), 1);
            }
            if (ContactZendeskFragment.this.feedbackListener != null) {
                ContactZendeskFragment.this.feedbackListener.onSubmissionCompleted();
            }
            if (ContactZendeskFragment.this.isAdded() && ContactZendeskFragment.this.getActivity() != null) {
                ContactZendeskFragment.this.reinitializeFragment();
            }
        }

        public void onError(ErrorResponse error) {
            ContactZendeskFragment.this.enableSendButton();
            if (ContactZendeskFragment.this.feedbackListener != null) {
                ContactZendeskFragment.this.feedbackListener.onSubmissionError(error);
            }
            ContactZendeskFragment.this.progressBar.setVisibility(8);
            ContactZendeskFragment.this.descriptionEditText.setEnabled(true);
            ContactZendeskFragment.this.emailEditText.setEnabled(true);
            if (ContactZendeskFragment.this.doneMenuItem != null) {
                ContactZendeskFragment.this.doneMenuItem.getIcon().setAlpha(ContactZendeskFragment.this.descriptionEditText.getText().length() == 0 ? 102 : 255);
            }
            if (ContactZendeskFragment.this.retryable != null) {
                ContactZendeskFragment.this.retryable.onRetryAvailable(ContactZendeskFragment.this.getString(R.string.rate_my_app_dialog_feedback_send_error_toast), new OnClickListener() {
                    public void onClick(View v) {
                        ContactZendeskFragment.this.sendFeedback();
                    }
                });
            }
        }
    }

    protected void onStart() {
        super.onStart();
        ApplicationStateMonitor.getInstance().activityStarted();
    }

    protected void onStop() {
        super.onStop();
        ApplicationStateMonitor.getInstance().activityStopped();
    }

    public static ContactZendeskFragment newInstance(ZendeskFeedbackConfiguration configuration) {
        Bundle fragmentArgs = new Bundle();
        if (configuration == null) {
            Logger.e(LOG_TAG, "Cannot instantiate a ContactZendeskFragment with no configuration", new Object[0]);
            return null;
        }
        fragmentArgs.putBundle(EXTRA_CONFIGURATION, configurationToBundle(configuration.getTags(), configuration.getRequestSubject(), configuration.getAdditionalInfo()));
        ContactZendeskFragment contactZendeskFragment = new ContactZendeskFragment();
        contactZendeskFragment.setArguments(fragmentArgs);
        contactZendeskFragment.setRetainInstance(true);
        return contactZendeskFragment;
    }

    private static Bundle configurationToBundle(List<String> tags, String subject, String additionalInfo) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_CONFIGURATION_REQUEST_SUBJECT, subject);
        bundle.putString(EXTRA_CONFIGURATION_ADDITIONAL_INFO, additionalInfo);
        bundle.putStringArrayList(EXTRA_CONFIGURATION_TAGS, tags == null ? null : new ArrayList(tags));
        return bundle;
    }

    private static ZendeskFeedbackConfiguration bundleToZendeskFeedbackConfiguration(final Bundle bundle) {
        return new ZendeskFeedbackConfiguration() {
            public List<String> getTags() {
                return bundle.getStringArrayList(ContactZendeskFragment.EXTRA_CONFIGURATION_TAGS);
            }

            public String getAdditionalInfo() {
                return bundle.getString(ContactZendeskFragment.EXTRA_CONFIGURATION_ADDITIONAL_INFO);
            }

            public String getRequestSubject() {
                return bundle.getString(ContactZendeskFragment.EXTRA_CONFIGURATION_REQUEST_SUBJECT);
            }
        };
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Retryable) {
            this.retryable = (Retryable) context;
        }
        setUpCallbacks();
    }

    public void onCreate(Bundle bundle) {
        TraceMachine.startTracing("ContactZendeskFragment");
        try {
            TraceMachine.enterMethod(this._nr_trace, "ContactZendeskFragment#onCreate", null);
        } catch (NoSuchFieldError e) {
            while (true) {
                TraceMachine.enterMethod(null, "ContactZendeskFragment#onCreate", null);
            }
        }
        super.onCreate(bundle);
        setHasOptionsMenu(true);
        this.settingsHelper = ZendeskConfig.INSTANCE.provider().uiSettingsHelper();
        if (getArguments() == null || !getArguments().containsKey(EXTRA_CONFIGURATION)) {
            Logger.w(LOG_TAG, "No configuration passed to the fragment, this will result in no feedback being sent", new Object[0]);
        } else {
            this.contactZendeskFeedbackConfiguration = new ContactZendeskFeedbackConfiguration(bundleToZendeskFeedbackConfiguration(getArguments().getBundle(EXTRA_CONFIGURATION)));
        }
        if (this.imageUploadService == null) {
            this.imageUploadService = new ImageUploadHelper(this.imageUploadProgressListener, ZendeskConfig.INSTANCE.provider().uploadProvider());
        } else {
            this.imageUploadService.setImageUploadProgressListener(this.imageUploadProgressListener);
        }
        TraceMachine.exitMethod();
    }

    @SuppressLint({"SetTextI18n"})
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View fragmentView;
        try {
            TraceMachine.enterMethod(this._nr_trace, "ContactZendeskFragment#onCreateView", null);
        } catch (NoSuchFieldError e) {
            while (true) {
                TraceMachine.enterMethod(null, "ContactZendeskFragment#onCreateView", null);
            }
        }
        if (ZendeskConfig.INSTANCE.storage().identityStorage().getIdentity() != null) {
            fragmentView = layoutInflater.inflate(R.layout.fragment_contact_zendesk, viewGroup, false);
            this.attachmentContainerHost = (AttachmentContainerHost) fragmentView.findViewById(R.id.contact_fragment_attachments);
            this.attachmentContainerHost.setState(this.imageUploadService);
            this.attachmentContainerHost.setAttachmentContainerListener(this.attachmentContainerListener);
            this.emailEditText = (EmailAddressAutoCompleteTextView) fragmentView.findViewById(R.id.contact_fragment_email);
            this.emailEditText.addTextChangedListener(new TextWatcherAdapter() {
                public void afterTextChanged(Editable s) {
                    ContactZendeskFragment.this.checkSendButtonState();
                }
            });
            this.descriptionEditText = (EditText) fragmentView.findViewById(R.id.contact_fragment_description);
            this.descriptionEditText.addTextChangedListener(new TextWatcherAdapter() {
                public void afterTextChanged(Editable editable) {
                    ContactZendeskFragment.this.checkSendButtonState();
                }
            });
            this.progressBar = (ProgressBar) fragmentView.findViewById(R.id.contact_fragment_progress);
            this.container = (ViewGroup) fragmentView.findViewById(R.id.contact_fragment_container);
            preloadSettingsAndInit();
        } else {
            View errorView = new TextView(layoutInflater.getContext());
            errorView.setText("Error, please check that an identity or email address has been set");
            fragmentView = errorView;
        }
        TraceMachine.exitMethod();
        return fragmentView;
    }

    private void preloadSettingsAndInit() {
        this.progressBar.setVisibility(0);
        this.settingsHelper.loadSetting(this.settingsCallback);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_contact_zendesk_menu, menu);
        this.doneMenuItem = menu.findItem(R.id.fragment_contact_zendesk_menu_done);
        this.attachmentItem = menu.findItem(R.id.fragment_contact_zendesk_attachment);
        if (this.mobileSettings == null || !NetworkUtils.isConnected(getActivity())) {
            disableSendButton();
            disableAttachmentButton();
        }
    }

    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        checkSendButtonState();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.fragment_contact_zendesk_menu_done) {
            sendFeedback();
            return true;
        } else if (item.getItemId() != R.id.fragment_contact_zendesk_attachment) {
            return false;
        } else {
            ZendeskBelvedereProvider.INSTANCE.getBelvedere(getContext()).showDialog(getChildFragmentManager());
            return true;
        }
    }

    public void onDetach() {
        super.onDetach();
        this.retryable = null;
        tearDownCallbacks();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        disableSendButton();
        ZendeskBelvedereProvider.INSTANCE.getBelvedere(getContext()).getFilesFromActivityOnResult(requestCode, resultCode, data, this.imagesExtracted);
    }

    public void deleteUnusedAttachmentsBeforeShutdown() {
        if (this.imageUploadService != null) {
            this.imageUploadService.deleteAllAttachmentsBeforeShutdown();
        }
    }

    private void checkSendButtonState() {
        boolean descriptionPresent;
        if (this.descriptionEditText == null || !StringUtils.hasLength(this.descriptionEditText.getText().toString())) {
            descriptionPresent = false;
        } else {
            descriptionPresent = true;
        }
        boolean imagesUploaded = this.imageUploadService.isImageUploadCompleted();
        boolean emailFieldValidOrHidden;
        if (!this.isEmailFieldVisible) {
            emailFieldValidOrHidden = true;
        } else if (this.emailEditText == null || !this.emailEditText.isInputValid()) {
            emailFieldValidOrHidden = false;
        } else {
            emailFieldValidOrHidden = true;
        }
        if (descriptionPresent && imagesUploaded && emailFieldValidOrHidden) {
            enableSendButton();
        } else {
            disableSendButton();
        }
    }

    private void disableAttachmentButton() {
        if (this.attachmentItem != null) {
            this.attachmentItem.setEnabled(false);
            this.attachmentItem.getIcon().setAlpha(102);
        }
    }

    private void displayAttachmentButton() {
        if (NetworkUtils.isConnected(getActivity())) {
            SafeMobileSettings storedSettings = ZendeskConfig.INSTANCE.getMobileSettings();
            if (storedSettings == null) {
                return;
            }
            if (!AttachmentHelper.isAttachmentSupportEnabled(storedSettings)) {
                Logger.d(LOG_TAG, "Ignoring displayAttachmentButton() because attachment support is disabled", new Object[0]);
                return;
            } else if (!ZendeskBelvedereProvider.INSTANCE.getBelvedere(getContext()).oneOrMoreSourceAvailable()) {
                Logger.d(LOG_TAG, "Ignoring displayAttachmentButton() because we don't have permissions for the camera or gallery", new Object[0]);
                return;
            } else if (this.attachmentItem != null) {
                this.attachmentItem.setEnabled(true);
                this.attachmentItem.setVisible(true);
                this.attachmentItem.getIcon().setAlpha(255);
                return;
            } else {
                return;
            }
        }
        Logger.d(LOG_TAG, "Ignoring displayAttachmentButton() because there is no network connection", new Object[0]);
    }

    public void setFeedbackListener(SubmissionListener feedbackListener) {
        this.feedbackListener = feedbackListener;
    }

    private void sendFeedback() {
        if (this.feedbackListener != null) {
            this.feedbackListener.onSubmissionStarted();
        }
        if (this.isEmailFieldVisible) {
            updateIdentity();
        }
        if (this.contactZendeskFeedbackConfiguration == null) {
            Logger.e(LOG_TAG, "Configuration is null, cannot send feedback...", new Object[0]);
            if (this.feedbackListener != null) {
                this.feedbackListener.onSubmissionError(new ErrorResponseAdapter("Configuration is null, cannot send feedback..."));
                return;
            }
            return;
        }
        this.progressBar.setVisibility(0);
        this.descriptionEditText.setEnabled(false);
        this.emailEditText.setEnabled(false);
        disableSendButton();
        this.attachmentContainerHost.setAttachmentsDeletable(false);
        ZendeskFeedbackConnector.defaultConnector(getActivity(), this.contactZendeskFeedbackConfiguration, ZendeskConfig.INSTANCE.getMobileSettings().getContactZendeskTags()).sendFeedback(this.descriptionEditText.getText().toString(), this.imageUploadService.getUploadTokens(), this.requestCallback);
    }

    private void updateIdentity() {
        Logger.d(LOG_TAG, "Updating existing anonymous identity with an email address", new Object[0]);
        Identity identity = ZendeskConfig.INSTANCE.storage().identityStorage().getIdentity();
        String emailAddress = this.emailEditText.getText().toString();
        if (identity == null || !(identity instanceof AnonymousIdentity)) {
            Logger.d(LOG_TAG, "No valid identity found ", new Object[0]);
            return;
        }
        AnonymousIdentity anonymousIdentity = (AnonymousIdentity) identity;
        Builder newIdentity = new Builder();
        newIdentity.withEmailIdentifier(emailAddress);
        if (StringUtils.hasLength(anonymousIdentity.getName())) {
            newIdentity.withNameIdentifier(anonymousIdentity.getName());
        }
        if (StringUtils.hasLength(anonymousIdentity.getExternalId())) {
            newIdentity.withExternalIdentifier(anonymousIdentity.getExternalId());
        }
        ZendeskConfig.INSTANCE.setIdentity(newIdentity.build());
    }

    private void disableSendButton() {
        if (this.doneMenuItem != null) {
            this.doneMenuItem.setEnabled(false);
            this.doneMenuItem.getIcon().setAlpha(102);
        }
    }

    private void enableSendButton() {
        if (!NetworkUtils.isConnected(getActivity())) {
            Logger.d(LOG_TAG, "Ignoring enableSendButton() because there is no network connection", new Object[0]);
        } else if (this.doneMenuItem != null) {
            this.doneMenuItem.setEnabled(true);
            this.doneMenuItem.getIcon().setAlpha(255);
        }
    }

    void networkNotAvailable() {
        FragmentActivity activity = getActivity();
        if (activity != null && (activity instanceof ContactZendeskActivity)) {
            ((ContactZendeskActivity) activity).onNetworkUnavailable();
        }
    }

    void onNetworkAvailable() {
        checkSendButtonState();
        displayAttachmentButton();
        if (this.mobileSettings == null) {
            preloadSettingsAndInit();
        }
    }

    void onNetworkUnavailable() {
        disableAttachmentButton();
        disableSendButton();
    }

    private void tearDownCallbacks() {
        this.requestCallback.cancel();
        this.requestCallback = null;
        this.imagesExtracted.cancel();
        this.imagesExtracted = null;
        if (this.attachmentContainerHost != null) {
            this.attachmentContainerHost.setAttachmentContainerListener(null);
        }
        this.settingsCallback.cancel();
        this.settingsCallback = null;
    }

    private void setUpCallbacks() {
        this.requestCallback = SafeZendeskCallback.from(new RequestCallback());
        this.imagesExtracted = new BelvedereCallback<List<BelvedereResult>>() {
            public void success(List<BelvedereResult> result) {
                SafeMobileSettings storedSettings = ZendeskConfig.INSTANCE.getMobileSettings();
                if (storedSettings != null) {
                    AttachmentHelper.processAndUploadSelectedFiles(result, ContactZendeskFragment.this.imageUploadService, ContactZendeskFragment.this.getActivity(), ContactZendeskFragment.this.attachmentContainerHost, storedSettings);
                }
                ContactZendeskFragment.this.checkSendButtonState();
            }
        };
        this.attachmentContainerListener = new AttachmentContainerListener() {
            public void attachmentRemoved(File file) {
                ContactZendeskFragment.this.imageUploadService.removeImage(file);
            }
        };
        this.imageUploadProgressListener = new ImageUploadProgressListener() {
            public void allImagesUploaded(Map<File, UploadResponse> map) {
                ContactZendeskFragment.this.checkSendButtonState();
            }

            public void imageUploaded(UploadResponse uploadResponse, BelvedereResult file) {
                ContactZendeskFragment.this.attachmentContainerHost.setAttachmentUploaded(file.getFile());
            }

            public void imageUploadError(ErrorResponse errorResponse, BelvedereResult file) {
                Logger.e(ContactZendeskFragment.LOG_TAG, String.format(Locale.US, "Image upload error; Reason: %s, Status: %s, isNetworkError: %s", new Object[]{errorResponse.getReason(), Integer.valueOf(errorResponse.getStatus()), Boolean.valueOf(errorResponse.isNetworkError())}), new Object[0]);
                AttachmentHelper.showAttachmentTryAgainDialog(ContactZendeskFragment.this.getActivity(), file, errorResponse, ContactZendeskFragment.this.imageUploadService, ContactZendeskFragment.this.attachmentContainerHost);
            }
        };
        this.settingsCallback = SafeZendeskCallback.from(new ZendeskCallback<SafeMobileSettings>() {
            public void onSuccess(SafeMobileSettings mobileSettings) {
                ContactZendeskFragment.this.mobileSettings = mobileSettings;
                ContactZendeskFragment.this.isEmailFieldVisible = ContactZendeskFragment.this.isEmailFieldEnabled(mobileSettings);
                ContactZendeskFragment.this.emailEditText.setVisibility(ContactZendeskFragment.this.isEmailFieldVisible ? 0 : 8);
                ContactZendeskFragment.this.progressBar.setVisibility(8);
                ContactZendeskFragment.this.container.setVisibility(0);
                if (!(ContactZendeskFragment.this.doneMenuItem == null || ContactZendeskFragment.this.attachmentItem == null)) {
                    ContactZendeskFragment.this.disableSendButton();
                    ContactZendeskFragment.this.displayAttachmentButton();
                }
                if (!NetworkUtils.isConnected(ContactZendeskFragment.this.getContext())) {
                    ContactZendeskFragment.this.networkNotAvailable();
                    Logger.d(ContactZendeskFragment.LOG_TAG, "Preload settings: Network not available.", new Object[0]);
                }
            }

            public void onError(ErrorResponse errorResponse) {
                ContactZendeskFragment.this.progressBar.setVisibility(8);
                if (!NetworkUtils.isConnected(ContactZendeskFragment.this.getContext())) {
                    ContactZendeskFragment.this.networkNotAvailable();
                    Logger.d(ContactZendeskFragment.LOG_TAG, "Preload settings: Network not available.", new Object[0]);
                } else if (ContactZendeskFragment.this.retryable != null) {
                    ContactZendeskFragment.this.retryable.onRetryAvailable(ContactZendeskFragment.this.getString(R.string.rate_my_app_dialog_feedback_send_error_toast), new OnClickListener() {
                        public void onClick(View v) {
                            ContactZendeskFragment.this.preloadSettingsAndInit();
                            Activity parentActivity = ContactZendeskFragment.this.getActivity();
                            if (parentActivity instanceof Retryable) {
                                ((Retryable) parentActivity).onRetryUnavailable();
                            }
                        }
                    });
                }
            }
        });
    }

    public void onDestroy() {
        super.onDestroy();
        this.imageUploadService.setImageUploadProgressListener(null);
    }

    boolean isEmailFieldEnabled(SafeMobileSettings safeSettings) {
        if (safeSettings == null || safeSettings.isConversationsEnabled()) {
            return false;
        }
        Identity identity = ZendeskConfig.INSTANCE.storage().identityStorage().getIdentity();
        if (identity == null || !(identity instanceof AnonymousIdentity) || StringUtils.hasLength(((AnonymousIdentity) identity).getEmail())) {
            return false;
        }
        if (!ZendeskConfig.INSTANCE.isCoppaEnabled()) {
            return true;
        }
        Logger.w(LOG_TAG, "This ticket will not be viewable by the user after submission. Conversations are not supported and COPPA removes any personal information associated with the ticket", new Object[0]);
        return false;
    }

    private void reinitializeFragment() {
        this.progressBar.setVisibility(8);
        this.attachmentContainerHost.reset();
        this.imageUploadService.reset();
        this.isEmailFieldVisible = isEmailFieldEnabled(this.mobileSettings);
        this.descriptionEditText.setEnabled(true);
        this.descriptionEditText.setText("");
        this.emailEditText.setEnabled(true);
        this.emailEditText.setText("");
        checkSendButtonState();
        displayAttachmentButton();
    }
}
