package com.zendesk.sdk.model.helpcenter;

import android.support.annotation.Nullable;
import java.io.Serializable;
import java.util.Date;

public class Category implements Serializable {
    private Date createdAt;
    private String description;
    private String htmlUrl;
    private Long id;
    private String locale;
    private String name;
    private boolean outdated;
    private int position;
    private String sourceLocale;
    private Date updatedAt;
    private String url;

    @Nullable
    public Long getId() {
        return this.id;
    }

    @Nullable
    public String getUrl() {
        return this.url;
    }

    @Nullable
    public String getHtmlUrl() {
        return this.htmlUrl;
    }

    public int getPosition() {
        return this.position;
    }

    @Nullable
    public Date getCreatedAt() {
        return this.createdAt == null ? null : new Date(this.createdAt.getTime());
    }

    @Nullable
    public Date getUpdatedAt() {
        return this.updatedAt == null ? null : new Date(this.updatedAt.getTime());
    }

    @Nullable
    public String getName() {
        return this.name;
    }

    @Nullable
    public String getDescription() {
        return this.description;
    }

    @Nullable
    public String getLocale() {
        return this.locale;
    }

    @Nullable
    public String getSourceLocale() {
        return this.sourceLocale;
    }

    public boolean isOutdated() {
        return this.outdated;
    }
}
