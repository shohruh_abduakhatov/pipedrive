package com.pipedrive.call;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.pipedrive.util.time.TimeManager;

public abstract class CallReceiver extends BroadcastReceiver {
    protected static volatile long sCallDurationInMillis = 0;
    private static volatile String sPreviousState;

    abstract void callFinished(@NonNull Context context, @Nullable Long l, @Nullable Long l2);

    protected static void enableCallReceiver(@NonNull Context context, @NonNull Class receiverImplementation, boolean enable) {
        Context applicationContext = context.getApplicationContext();
        applicationContext.getPackageManager().setComponentEnabledSetting(new ComponentName(applicationContext, receiverImplementation), enable ? 1 : 2, 1);
    }

    public final void onReceive(@NonNull Context context, @NonNull Intent intent) {
        String actionPhoneState = intent.getStringExtra("state");
        boolean isCallInitiated = TextUtils.equals(actionPhoneState, TelephonyManager.EXTRA_STATE_OFFHOOK);
        boolean isCallEnded = TextUtils.equals(actionPhoneState, TelephonyManager.EXTRA_STATE_IDLE) && TextUtils.equals(sPreviousState, TelephonyManager.EXTRA_STATE_OFFHOOK);
        sPreviousState = actionPhoneState;
        if (isCallInitiated) {
            sCallDurationInMillis = TimeManager.getInstance().currentTimeMillis().longValue();
        } else if (isCallEnded) {
            long callStartTimeInMillis = sCallDurationInMillis;
            if (sCallDurationInMillis > 0) {
                sCallDurationInMillis = TimeManager.getInstance().currentTimeMillis().longValue() - sCallDurationInMillis;
            }
            callFinished(context, sCallDurationInMillis > 0 ? Long.valueOf(sCallDurationInMillis) : null, callStartTimeInMillis > 0 ? Long.valueOf(callStartTimeInMillis) : null);
        }
    }
}
