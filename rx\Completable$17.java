package rx;

import rx.functions.Action1;

class Completable$17 implements Action1<Throwable> {
    final /* synthetic */ Completable this$0;
    final /* synthetic */ Action1 val$onNotification;

    Completable$17(Completable completable, Action1 action1) {
        this.this$0 = completable;
        this.val$onNotification = action1;
    }

    public void call(Throwable throwable) {
        this.val$onNotification.call(Notification.createOnError(throwable));
    }
}
