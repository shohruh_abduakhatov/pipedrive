package com.pipedrive.products.edit;

import android.support.annotation.Nullable;
import com.pipedrive.model.products.DealProduct;

interface DealProductEditView {
    void onDealProductCreatedOrUpdated();

    void onDealProductDeleted();

    void onDealProductRequested(@Nullable DealProduct dealProduct);
}
