package com.newrelic.agent.android.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import com.newrelic.agent.android.analytics.AnalyticAttribute;
import com.newrelic.agent.android.analytics.AnalyticAttributeStore;
import com.newrelic.agent.android.logging.AgentLog;
import com.newrelic.agent.android.logging.AgentLogManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class SharedPrefsAnalyticAttributeStore implements AnalyticAttributeStore {
    private static final String STORE_FILE = "NRAnalyticAttributeStore";
    private static final AgentLog log = AgentLogManager.getAgentLog();
    private final Context context;

    public SharedPrefsAnalyticAttributeStore(Context context) {
        this.context = context;
    }

    public boolean store(AnalyticAttribute attribute) {
        boolean z = false;
        synchronized (this) {
            if (attribute.isPersistent()) {
                Editor editor = this.context.getSharedPreferences(STORE_FILE, 0).edit();
                switch (attribute.getAttributeDataType()) {
                    case STRING:
                        log.verbose("SharedPrefsAnalyticAttributeStore.store - storing analytic attribute " + attribute.getName() + "=" + attribute.getStringValue());
                        editor.putString(attribute.getName(), attribute.getStringValue());
                        break;
                    case FLOAT:
                        log.verbose("SharedPrefsAnalyticAttributeStore.store - storing analytic attribute " + attribute.getName() + "=" + attribute.getFloatValue());
                        editor.putFloat(attribute.getName(), attribute.getFloatValue());
                        break;
                    case BOOLEAN:
                        log.verbose("SharedPrefsAnalyticAttributeStore.store - storing analytic attribute " + attribute.getName() + "=" + attribute.getBooleanValue());
                        editor.putBoolean(attribute.getName(), attribute.getBooleanValue());
                        break;
                    default:
                        log.error("SharedPrefsAnalyticAttributeStore.store - unsupported analytic attribute data type" + attribute.getName());
                        break;
                }
                if (VERSION.SDK_INT < 9) {
                    z = editor.commit();
                } else {
                    editor.apply();
                    z = true;
                }
            }
        }
        return z;
    }

    public List<AnalyticAttribute> fetchAll() {
        log.verbose("SharedPrefsAnalyticAttributeStore.fetchAll invoked.");
        SharedPreferences preferences = this.context.getSharedPreferences(STORE_FILE, 0);
        ArrayList<AnalyticAttribute> analyticAttributeArrayList = new ArrayList();
        synchronized (this) {
            Map<String, ?> storedAttributes = preferences.getAll();
        }
        for (Entry entry : storedAttributes.entrySet()) {
            log.debug("SharedPrefsAnalyticAttributeStore.fetchAll - found analytic attribute " + entry.getKey() + "=" + entry.getValue());
            if (entry.getValue() instanceof String) {
                analyticAttributeArrayList.add(new AnalyticAttribute(entry.getKey().toString(), entry.getValue().toString(), true));
            } else if (entry.getValue() instanceof Float) {
                analyticAttributeArrayList.add(new AnalyticAttribute(entry.getKey().toString(), Float.valueOf(entry.getValue().toString()).floatValue(), true));
            } else if (entry.getValue() instanceof Boolean) {
                analyticAttributeArrayList.add(new AnalyticAttribute(entry.getKey().toString(), Boolean.valueOf(entry.getValue().toString()).booleanValue(), true));
            } else {
                log.error("SharedPrefsAnalyticAttributeStore.fetchAll - unsupported analytic attribute " + entry.getKey() + "=" + entry.getValue());
            }
        }
        return analyticAttributeArrayList;
    }

    public int count() {
        int size = this.context.getSharedPreferences(STORE_FILE, 0).getAll().size();
        log.verbose("SharedPrefsAnalyticAttributeStore.count - returning " + size);
        return size;
    }

    public void clear() {
        log.verbose("SharedPrefsAnalyticAttributeStore.clear - flushing stored attributes");
        synchronized (this) {
            this.context.getSharedPreferences(STORE_FILE, 0).edit().clear().apply();
        }
    }

    @SuppressLint({"CommitPrefEdits"})
    public void delete(AnalyticAttribute attribute) {
        synchronized (this) {
            log.verbose("SharedPrefsAnalyticAttributeStore.delete - deleting attribute " + attribute.getName());
            SharedPreferences preferences = this.context.getSharedPreferences(STORE_FILE, 0);
            if (VERSION.SDK_INT < 9) {
                preferences.edit().remove(attribute.getName()).commit();
            } else {
                preferences.edit().remove(attribute.getName()).apply();
            }
        }
    }
}
