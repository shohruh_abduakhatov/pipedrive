package com.pipedrive.model;

import android.os.Parcel;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class BaseDatasourceEntity {
    private boolean isShadow = false;
    @Nullable
    private Long pipedriveId;
    @Nullable
    private Long sqlId;

    public boolean isExisting() {
        return this.pipedriveId != null && this.pipedriveId.longValue() > 0;
    }

    public boolean isStored() {
        return this.sqlId != null && this.sqlId.longValue() > 0;
    }

    @Deprecated
    public long getSqlId() {
        return this.sqlId == null ? 0 : this.sqlId.longValue();
    }

    @Nullable
    public Long getSqlIdOrNull() {
        return this.sqlId;
    }

    @Deprecated
    public void setSqlId(long sqlId) {
        this.sqlId = Long.valueOf(sqlId);
    }

    public void setSqlIdOrNull(@IntRange(from = 1) @NonNull Long sqlId) {
        if (sqlId.longValue() <= 0) {
            sqlId = null;
        }
        this.sqlId = sqlId;
    }

    public boolean isShadow() {
        return this.isShadow;
    }

    void setShadow(boolean shadow) {
        this.isShadow = shadow;
    }

    @Deprecated
    public int getPipedriveId() {
        return this.pipedriveId == null ? 0 : this.pipedriveId.intValue();
    }

    @Nullable
    public Long getPipedriveIdOrNull() {
        return this.pipedriveId;
    }

    @Deprecated
    public void setPipedriveId(int pipedriveId) {
        this.pipedriveId = pipedriveId <= 0 ? null : Long.valueOf((long) pipedriveId);
    }

    public void setPipedriveId(@IntRange(from = 1) @NonNull Long pipedriveId) {
        if (pipedriveId.longValue() <= 0) {
            pipedriveId = null;
        }
        this.pipedriveId = pipedriveId;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.sqlId);
        dest.writeSerializable(this.pipedriveId);
        dest.writeInt(this.isShadow ? 1 : 0);
    }

    protected void readFromParcel(Parcel in) {
        this.sqlId = (Long) in.readSerializable();
        this.pipedriveId = (Long) in.readSerializable();
        this.isShadow = in.readInt() == 1;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BaseDatasourceEntity that = (BaseDatasourceEntity) o;
        if (this.isShadow == that.isShadow) {
            if (this.sqlId != null) {
                if (this.sqlId.equals(that.sqlId)) {
                    return true;
                }
            } else if (that.sqlId == null) {
                if (this.pipedriveId != null) {
                    if (this.pipedriveId.equals(that.pipedriveId)) {
                        return true;
                    }
                } else if (that.pipedriveId == null) {
                    return true;
                }
            }
        }
        return false;
    }

    public int hashCode() {
        int result;
        int hashCode;
        int i = 0;
        if (this.sqlId != null) {
            result = this.sqlId.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.pipedriveId != null) {
            hashCode = this.pipedriveId.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (i2 + hashCode) * 31;
        if (this.isShadow) {
            i = 1;
        }
        return hashCode + i;
    }

    public String toString() {
        return "BaseDatasourceEntity{sqlId=" + this.sqlId + ", pipedriveId=" + this.pipedriveId + ", isShadow=" + this.isShadow + '}';
    }
}
