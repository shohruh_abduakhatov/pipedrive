package com.zendesk.sdk.model.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.annotations.SerializedName;
import com.zendesk.util.CollectionUtils;
import java.util.Date;
import java.util.List;

public class CommentResponse {
    private List<Attachment> attachments;
    private Long authorId;
    private String body;
    private Date createdAt;
    private String htmlBody;
    private Long id;
    @SerializedName("public")
    private boolean isPublic = true;
    private String requestId;
    private String url;

    @Nullable
    public String getUrl() {
        return this.url;
    }

    @Nullable
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Nullable
    public String getRequestId() {
        return this.requestId;
    }

    @Nullable
    public String getBody() {
        return this.body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Nullable
    public String getHtmlBody() {
        return this.htmlBody;
    }

    public boolean isPublic() {
        return this.isPublic;
    }

    @Nullable
    public Long getAuthorId() {
        return this.authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    @NonNull
    public List<Attachment> getAttachments() {
        return CollectionUtils.copyOf(this.attachments);
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    @Nullable
    public Date getCreatedAt() {
        return this.createdAt == null ? null : new Date(this.createdAt.getTime());
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
