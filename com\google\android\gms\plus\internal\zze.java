package com.google.android.gms.plus.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.common.internal.zzq;
import com.google.android.gms.plus.People.LoadPeopleResult;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.internal.model.people.PersonEntity;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

public class zze extends zzj<zzd> {
    private Person aAV;
    private final PlusSession aAW;

    static final class zza implements LoadPeopleResult {
        private final String aAX;
        private final PersonBuffer aAY;
        private final Status hv;

        public zza(Status status, DataHolder dataHolder, String str) {
            this.hv = status;
            this.aAX = str;
            this.aAY = dataHolder != null ? new PersonBuffer(dataHolder) : null;
        }

        public String getNextPageToken() {
            return this.aAX;
        }

        public PersonBuffer getPersonBuffer() {
            return this.aAY;
        }

        public Status getStatus() {
            return this.hv;
        }

        public void release() {
            if (this.aAY != null) {
                this.aAY.release();
            }
        }
    }

    static final class zzb extends zza {
        private final com.google.android.gms.internal.zzqo.zzb<LoadPeopleResult> alb;

        public zzb(com.google.android.gms.internal.zzqo.zzb<LoadPeopleResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_plus_People_LoadPeopleResult) {
            this.alb = com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_plus_People_LoadPeopleResult;
        }

        public void zza(DataHolder dataHolder, String str) {
            Status status = new Status(dataHolder.getStatusCode(), null, dataHolder.zzaui() != null ? (PendingIntent) dataHolder.zzaui().getParcelable("pendingIntent") : null);
            if (!(status.isSuccess() || dataHolder == null)) {
                if (!dataHolder.isClosed()) {
                    dataHolder.close();
                }
                dataHolder = null;
            }
            this.alb.setResult(new zza(status, dataHolder, str));
        }
    }

    static final class zzc extends zza {
        private final com.google.android.gms.internal.zzqo.zzb<Status> alb;

        public zzc(com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status) {
            this.alb = com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status;
        }

        public void zzk(int i, Bundle bundle) {
            this.alb.setResult(new Status(i, null, bundle != null ? (PendingIntent) bundle.getParcelable("pendingIntent") : null));
        }
    }

    public zze(Context context, Looper looper, zzf com_google_android_gms_common_internal_zzf, PlusSession plusSession, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 2, com_google_android_gms_common_internal_zzf, connectionCallbacks, onConnectionFailedListener);
        this.aAW = plusSession;
    }

    public static boolean zze(Set<Scope> set) {
        return (set == null || set.isEmpty()) ? false : (set.size() == 1 && set.contains(new Scope("plus_one_placeholder_scope"))) ? false : true;
    }

    public String getAccountName() {
        zzavf();
        try {
            return ((zzd) zzavg()).getAccountName();
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    public zzq zza(com.google.android.gms.internal.zzqo.zzb<LoadPeopleResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_plus_People_LoadPeopleResult, int i, String str) {
        zzavf();
        Object com_google_android_gms_plus_internal_zze_zzb = new zzb(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_plus_People_LoadPeopleResult);
        try {
            return ((zzd) zzavg()).zza(com_google_android_gms_plus_internal_zze_zzb, 1, i, -1, str);
        } catch (RemoteException e) {
            com_google_android_gms_plus_internal_zze_zzb.zza(DataHolder.zzgb(8), null);
            return null;
        }
    }

    protected void zza(int i, IBinder iBinder, Bundle bundle, int i2) {
        if (i == 0 && bundle != null && bundle.containsKey("loaded_person")) {
            this.aAV = PersonEntity.zzaf(bundle.getByteArray("loaded_person"));
        }
        super.zza(i, iBinder, bundle, i2);
    }

    public void zza(com.google.android.gms.internal.zzqo.zzb<LoadPeopleResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_plus_People_LoadPeopleResult, Collection<String> collection) {
        zzavf();
        zzb com_google_android_gms_plus_internal_zze_zzb = new zzb(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_plus_People_LoadPeopleResult);
        try {
            ((zzd) zzavg()).zza(com_google_android_gms_plus_internal_zze_zzb, new ArrayList(collection));
        } catch (RemoteException e) {
            com_google_android_gms_plus_internal_zze_zzb.zza(DataHolder.zzgb(8), null);
        }
    }

    protected Bundle zzahv() {
        Bundle zzccc = this.aAW.zzccc();
        zzccc.putStringArray("request_visible_actions", this.aAW.zzcbw());
        zzccc.putString("auth_package", this.aAW.zzcby());
        return zzccc;
    }

    public boolean zzain() {
        return zze(zzawb().zzc(Plus.API));
    }

    public void zzcbq() {
        zzavf();
        try {
            this.aAV = null;
            ((zzd) zzavg()).zzcbq();
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    public Person zzcbs() {
        zzavf();
        return this.aAV;
    }

    public void zzd(com.google.android.gms.internal.zzqo.zzb<LoadPeopleResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_plus_People_LoadPeopleResult, String[] strArr) {
        zza(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_plus_People_LoadPeopleResult, Arrays.asList(strArr));
    }

    protected /* synthetic */ IInterface zzh(IBinder iBinder) {
        return zzkn(iBinder);
    }

    protected String zzjx() {
        return "com.google.android.gms.plus.service.START";
    }

    protected String zzjy() {
        return "com.google.android.gms.plus.internal.IPlusService";
    }

    protected zzd zzkn(IBinder iBinder) {
        return com.google.android.gms.plus.internal.zzd.zza.zzkm(iBinder);
    }

    public zzq zzu(com.google.android.gms.internal.zzqo.zzb<LoadPeopleResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_plus_People_LoadPeopleResult, String str) {
        return zza(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_plus_People_LoadPeopleResult, 0, str);
    }

    public void zzv(com.google.android.gms.internal.zzqo.zzb<LoadPeopleResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_plus_People_LoadPeopleResult) {
        zzavf();
        Object com_google_android_gms_plus_internal_zze_zzb = new zzb(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_plus_People_LoadPeopleResult);
        try {
            ((zzd) zzavg()).zza(com_google_android_gms_plus_internal_zze_zzb, 2, 1, -1, null);
        } catch (RemoteException e) {
            com_google_android_gms_plus_internal_zze_zzb.zza(DataHolder.zzgb(8), null);
        }
    }

    public void zzw(com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status) {
        zzavf();
        zzcbq();
        Object com_google_android_gms_plus_internal_zze_zzc = new zzc(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status);
        try {
            ((zzd) zzavg()).zzb(com_google_android_gms_plus_internal_zze_zzc);
        } catch (RemoteException e) {
            com_google_android_gms_plus_internal_zze_zzc.zzk(8, null);
        }
    }
}
