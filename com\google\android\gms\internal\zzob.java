package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.auth.api.proxy.ProxyApi.ProxyResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzqo.zza;

abstract class zzob extends zza<ProxyResult, zzny> {
    public zzob(GoogleApiClient googleApiClient) {
        super(com.google.android.gms.auth.api.zza.API, googleApiClient);
    }

    protected abstract void zza(Context context, zzoa com_google_android_gms_internal_zzoa) throws RemoteException;

    protected final void zza(zzny com_google_android_gms_internal_zzny) throws RemoteException {
        zza(com_google_android_gms_internal_zzny.getContext(), (zzoa) com_google_android_gms_internal_zzny.zzavg());
    }

    protected /* synthetic */ Result zzc(Status status) {
        return zzk(status);
    }

    protected ProxyResult zzk(Status status) {
        return new zzod(status);
    }
}
