package com.pipedrive.organization.edit;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.model.Organization;
import com.pipedrive.presenter.Presenter;

@MainThread
abstract class OrganizationEditPresenter extends Presenter<OrganizationEditView> {
    abstract void createOrUpdateOrganization();

    abstract void deleteOrganization();

    @Nullable
    public abstract Boolean enableOrganizationAddressChange();

    @Nullable
    public abstract Organization getOrganization();

    abstract boolean isModified();

    abstract void requestNewOrganization();

    abstract void requestNewOrganizationWithName(@Nullable String str);

    abstract void requestOrganization(@NonNull Long l);

    public OrganizationEditPresenter(@NonNull Session session) {
        super(session);
    }
}
