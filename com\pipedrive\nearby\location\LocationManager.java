package com.pipedrive.nearby.location;

import com.pipedrive.nearby.map.LocationProvider;
import com.pipedrive.nearby.map.LocationSourceProvider;

public interface LocationManager extends LocationProvider, LocationSourceProvider {
}
