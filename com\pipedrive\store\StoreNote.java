package com.pipedrive.store;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.changes.NoteChanger;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.model.notes.Note;

public class StoreNote extends Store<Note> {
    public StoreNote(@NonNull Session session) {
        super(session);
    }

    void cleanupBeforeCreate(@NonNull Note newNoteBeingStored) {
        if (newNoteBeingStored.getOrganization() != null) {
            new StoreOrganization(getSession()).cleanupBeforeCreate(newNoteBeingStored.getOrganization());
        }
        if (newNoteBeingStored.getPerson() != null) {
            new StorePerson(getSession()).cleanupBeforeCreate(newNoteBeingStored.getPerson());
        }
        if (newNoteBeingStored.getDeal() != null) {
            new StoreDeal(getSession()).cleanupBeforeCreate(newNoteBeingStored.getDeal());
        }
        processNoteBeforeStoringShared(newNoteBeingStored);
        newNoteBeingStored.setActiveFlag(true);
    }

    boolean implementCreate(@NonNull Note newNote) {
        return new NoteChanger(getSession()).create(newNote);
    }

    void cleanupBeforeUpdate(@NonNull Note updatedNoteBeingStored) {
        if (updatedNoteBeingStored.getOrganization() != null) {
            new StoreOrganization(getSession()).cleanupBeforeUpdate(updatedNoteBeingStored.getOrganization());
        }
        if (updatedNoteBeingStored.getPerson() != null) {
            new StorePerson(getSession()).cleanupBeforeUpdate(updatedNoteBeingStored.getPerson());
        }
        if (updatedNoteBeingStored.getDeal() != null) {
            new StoreDeal(getSession()).cleanupBeforeUpdate(updatedNoteBeingStored.getDeal());
        }
        processNoteBeforeStoringShared(updatedNoteBeingStored);
    }

    boolean implementUpdate(@NonNull Note updatedNote) {
        return new NoteChanger(getSession()).update(updatedNote);
    }

    private void processNoteBeforeStoringShared(Note noteBeingSaved) {
        if (noteBeingSaved.getAddTime() == null) {
            noteBeingSaved.setAddTime(PipedriveDateTime.instanceWithCurrentDateTime());
        }
    }
}
