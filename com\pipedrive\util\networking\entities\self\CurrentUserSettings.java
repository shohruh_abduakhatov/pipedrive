package com.pipedrive.util.networking.entities.self;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pipedrive.model.UserSettingsPipelineFilter;
import java.util.ArrayList;
import java.util.List;

class CurrentUserSettings {
    static final Boolean CAN_CHANGE_VISIBILITY_OF_ITEMS_DEFAULT_VALUE = Boolean.valueOf(false);
    static final Boolean CAN_DELETE_DEALS_DEFAULT_VALUE = Boolean.valueOf(false);
    @SerializedName("can_change_visibility_of_items")
    @Expose
    @NonNull
    private Boolean canChangeVisibilityOfItems = CAN_CHANGE_VISIBILITY_OF_ITEMS_DEFAULT_VALUE;
    @SerializedName("can_delete_deals")
    @Expose
    @NonNull
    private Boolean canDeleteDeals = CAN_DELETE_DEALS_DEFAULT_VALUE;
    @Nullable
    @SerializedName("current_pipeline_id")
    @Expose
    private Long currentPipelineId;
    @Nullable
    @SerializedName("deal_default_visibility")
    @Expose
    private Integer defaultVisibilityDeal;
    @Nullable
    @SerializedName("org_default_visibility")
    @Expose
    private Integer defaultVisibilityOrganization;
    @Nullable
    @SerializedName("person_default_visibility")
    @Expose
    private Integer defaultVisibilityPerson;
    @Nullable
    @SerializedName("product_default_visibility")
    @Expose
    private Integer defaultVisibilityProduct;
    @NonNull
    private List<UserSettingsPipelineFilter> pipelineFilters = new ArrayList();

    CurrentUserSettings() {
    }

    @Nullable
    Long getCurrentPipelineId() {
        return this.currentPipelineId;
    }

    @Nullable
    Integer getDefaultVisibilityOrganization() {
        return this.defaultVisibilityOrganization;
    }

    @Nullable
    Integer getDefaultVisibilityPerson() {
        return this.defaultVisibilityPerson;
    }

    @Nullable
    Integer getDefaultVisibilityDeal() {
        return this.defaultVisibilityDeal;
    }

    @Nullable
    Integer getDefaultVisibilityProduct() {
        return this.defaultVisibilityProduct;
    }

    @NonNull
    Boolean canChangeVisibilityOfItems() {
        return this.canChangeVisibilityOfItems;
    }

    @NonNull
    Boolean canDeleteDeals() {
        return this.canDeleteDeals;
    }

    @NonNull
    List<UserSettingsPipelineFilter> getPipelineFilters() {
        return this.pipelineFilters;
    }

    void addPipelineFilter(@NonNull UserSettingsPipelineFilter filter) {
        this.pipelineFilters.add(filter);
    }
}
