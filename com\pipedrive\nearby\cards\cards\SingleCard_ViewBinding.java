package com.pipedrive.nearby.cards.cards;

import android.support.annotation.UiThread;
import android.view.View;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class SingleCard_ViewBinding extends Card_ViewBinding {
    private SingleCard target;
    private View view2131821167;

    @UiThread
    public SingleCard_ViewBinding(final SingleCard target, View source) {
        super(target, source);
        this.target = target;
        target.lastActivityView = (ActivitySummary) Utils.findRequiredViewAsType(source, R.id.nearbyCard.lastActivity, "field 'lastActivityView'", ActivitySummary.class);
        target.nextActivityView = (ActivitySummary) Utils.findRequiredViewAsType(source, R.id.nearbyCard.nextActivity, "field 'nextActivityView'", ActivitySummary.class);
        View view = Utils.findRequiredView(source, R.id.nearbyCard.detailsButton, "method 'onDetailsButtonClicked'");
        this.view2131821167 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onDetailsButtonClicked();
            }
        });
    }

    public void unbind() {
        SingleCard target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.lastActivityView = null;
        target.nextActivityView = null;
        this.view2131821167.setOnClickListener(null);
        this.view2131821167 = null;
        super.unbind();
    }
}
