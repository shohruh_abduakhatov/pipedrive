package com.zendesk.sdk.feedback;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseZendeskFeedbackConfiguration implements ZendeskFeedbackConfiguration, Serializable {
    public List<String> getTags() {
        return new ArrayList();
    }

    public String getAdditionalInfo() {
        return "";
    }
}
