package com.pipedrive.model.email;

import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.util.networking.entities.EmailThreadEntity;
import java.util.List;

public class EmailThread extends BaseDatasourceEntity {
    private Deal mDeal;
    private List<Organization> mOrganizations;

    public Deal getDeal() {
        return this.mDeal;
    }

    public void setDeal(Deal deal) {
        this.mDeal = deal;
    }

    public List<Organization> getOrganizations() {
        return this.mOrganizations;
    }

    public void setOrganizations(List<Organization> organizations) {
        this.mOrganizations = organizations;
    }

    public static EmailThread from(EmailThreadEntity entity) {
        EmailThread emailThread = new EmailThread();
        if (entity == null) {
            return null;
        }
        if (entity.getId() == null) {
            return emailThread;
        }
        emailThread.setPipedriveId(entity.getId().intValue());
        return emailThread;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EmailThread that = (EmailThread) o;
        if (this.mDeal == null ? that.mDeal != null : !this.mDeal.equals(that.mDeal)) {
            if (this.mOrganizations != null) {
                if (this.mOrganizations.equals(that.mOrganizations)) {
                    return true;
                }
            } else if (that.mOrganizations == null) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        int result;
        int i = 0;
        if (this.mDeal != null) {
            result = this.mDeal.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.mOrganizations != null) {
            i = this.mOrganizations.hashCode();
        }
        return i2 + i;
    }
}
