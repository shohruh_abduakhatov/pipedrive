package com.pipedrive.sync;

public enum SyncManager$StateOfDownloadAll {
    IDLE,
    REGISTERED,
    DOWNLOADING
}
