package com.pipedrive.views.calendar;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import java.util.Calendar;
import java.util.Locale;

class WeekViewHolder extends ViewHolder {
    public WeekViewHolder(@NonNull WeekView weekView) {
        super(weekView);
    }

    void bind(@NonNull Locale locale, int year, int month, @Nullable Calendar selectedDate, int weekOfMonth, int weekOfYear) {
        ((WeekView) this.itemView).setup(locale, selectedDate, year, month, weekOfMonth, weekOfYear);
    }
}
