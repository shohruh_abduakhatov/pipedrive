package com.pipedrive.nearby.toolbar;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.OnTabSelectedListener;
import android.support.design.widget.TabLayout.Tab;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.pipedrive.R;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.cards.NearbyRequestEventBus;
import com.pipedrive.nearby.model.NearbyItemType;
import com.pipedrive.nearby.util.Irrelevant;
import rx.Emitter;
import rx.Emitter.BackpressureMode;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Cancellable;
import rx.subjects.BehaviorSubject;
import rx.subjects.Subject;

public class NearbyToolbarView extends AppBarLayout implements NearbyToolbar {
    private static final NearbyItemType DEFAULT_TYPE = NearbyItemType.DEALS;
    private static final String SCREEN_NEARBY_TYPE_FORMAT = "Nearby: %s view";
    @NonNull
    private final Subject<NearbyItemType, NearbyItemType> nearbyItemTypeChanges = BehaviorSubject.create().toSerialized();
    @Nullable
    private Session session;
    @BindView(2131820987)
    TabLayout tabLayout;
    @Nullable
    private OnTabSelectedListener tabSelectedListener;
    @BindView(2131820700)
    Toolbar toolbar;

    @NonNull
    Toolbar getToolbar() {
        return this.toolbar;
    }

    public NearbyToolbarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        ButterKnife.bind((Object) this, ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate(R.layout.layout_nearby_toolbar, this, true));
        this.toolbar.setNavigationIcon(ContextCompat.getDrawable(getContext(), R.drawable.icon_back));
    }

    public void setup(@NonNull final Session session) {
        this.session = session;
        this.tabSelectedListener = new OnTabSelectedListener() {
            public void onTabSelected(Tab tab) {
                NearbyToolbarView.this.emitTypeValue(NearbyItemType.values()[tab.getPosition()]);
                NearbyRequestEventBus.INSTANCE.onRequestStarted();
                session.setLastSelectedNearbyToolbarTabIndex(tab.getPosition());
            }

            public void onTabUnselected(Tab tab) {
            }

            public void onTabReselected(Tab tab) {
            }
        };
    }

    private void emitDefaultTabSelected(@NonNull Session session) {
        Integer lastSelectedNearbyToolbarTabIndex = session.getLastSelectedNearbyToolbarTabIndex();
        NearbyItemType itemTypeToSelect = lastSelectedNearbyToolbarTabIndex == null ? DEFAULT_TYPE : NearbyItemType.values()[lastSelectedNearbyToolbarTabIndex.intValue()];
        Tab tabToSelect = this.tabLayout.getTabAt(itemTypeToSelect.ordinal());
        if (tabToSelect != null) {
            if (tabToSelect.isSelected()) {
                emitTypeValue(itemTypeToSelect);
                NearbyRequestEventBus.INSTANCE.onRequestStarted();
                return;
            }
            tabToSelect.select();
        }
    }

    private void emitTypeValue(NearbyItemType itemTypeToSelect) {
        Analytics.hitScreen(String.format(SCREEN_NEARBY_TYPE_FORMAT, new Object[]{itemTypeToSelect.name().toLowerCase()}), getContext());
        this.nearbyItemTypeChanges.onNext(itemTypeToSelect);
    }

    @NonNull
    public Observable<Irrelevant> homeButtonClicks() {
        return Observable.create(new Action1<Emitter<Irrelevant>>() {
            public void call(final Emitter<Irrelevant> emitter) {
                NearbyToolbarView.this.toolbar.setNavigationOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        emitter.onNext(Irrelevant.INSTANCE);
                    }
                });
                emitter.setCancellation(new Cancellable() {
                    public void cancel() throws Exception {
                        NearbyToolbarView.this.toolbar.setNavigationOnClickListener(null);
                    }
                });
            }
        }, BackpressureMode.LATEST);
    }

    public void releaseResources() {
        if (this.tabSelectedListener != null) {
            this.tabLayout.removeOnTabSelectedListener(this.tabSelectedListener);
        }
    }

    public void bind() {
        if (this.tabSelectedListener != null) {
            this.tabLayout.addOnTabSelectedListener(this.tabSelectedListener);
        }
        if (this.session != null) {
            emitDefaultTabSelected(this.session);
        }
    }

    @NonNull
    public Observable<NearbyItemType> nearbyItemTypeChanges(@NonNull Session session) {
        return this.nearbyItemTypeChanges;
    }
}
