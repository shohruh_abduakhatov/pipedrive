package com.pipedrive.util.devices;

import com.zendesk.service.HttpConstants;
import kotlin.Metadata;
import retrofit2.HttpException;
import rx.Completable;
import rx.functions.Func1;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u000e\u0010\u0003\u001a\n \u0002*\u0004\u0018\u00010\u00040\u0004H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "Lrx/Completable;", "kotlin.jvm.PlatformType", "error", "", "call"}, k = 3, mv = {1, 1, 6})
/* compiled from: MobileDevices.kt */
final class MobileDevices$registerDevice$2<T, R> implements Func1<Throwable, Completable> {
    public static final MobileDevices$registerDevice$2 INSTANCE = new MobileDevices$registerDevice$2();

    MobileDevices$registerDevice$2() {
    }

    public final Completable call(Throwable error) {
        if ((error instanceof HttpException) && ((HttpException) error).code() == HttpConstants.HTTP_FORBIDDEN) {
            return Completable.error(error);
        }
        return Completable.complete();
    }
}
