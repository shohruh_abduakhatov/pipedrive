package com.pipedrive.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.pipedrive.LoadingModalActivity;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.sync.SyncManager;
import com.pipedrive.tasks.authorization.OnEnoughDataToShowUI;

public class SynchronizeDataActivity extends LoadingModalActivity {
    public static void startActivity(@NonNull Context context) {
        context.startActivity(new Intent(context, SynchronizeDataActivity.class).setFlags(8388608));
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SyncManager.getInstance().requestSettingsRefresh(getSession(), new OnEnoughDataToShowUI() {
            public void readyToShowUI(@NonNull Session session) {
                SynchronizeDataActivity.this.finish();
            }
        });
    }

    protected int getLoadingMessage() {
        return R.string.loading;
    }
}
