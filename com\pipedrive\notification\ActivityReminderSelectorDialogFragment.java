package com.pipedrive.notification;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog.Builder;
import com.pipedrive.R;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.AnalyticsEvent;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;

public class ActivityReminderSelectorDialogFragment extends DialogFragment {
    private static final String KEY_SESSION_ID = "SESSION_ID";
    private static final String TAG = ActivityReminderSelectorDialogFragment.class.getSimpleName();
    @Nullable
    private Callback callback;
    @Nullable
    private Session session;

    public interface Callback {
        void onActivityReminderOptionChanged();
    }

    public static void show(@NonNull Session session, @NonNull FragmentManager fragmentManager) {
        Bundle args = new Bundle();
        args.putString(KEY_SESSION_ID, session.getSessionID());
        ActivityReminderSelectorDialogFragment activityReminderSelector = new ActivityReminderSelectorDialogFragment();
        activityReminderSelector.setArguments(args);
        activityReminderSelector.show(fragmentManager, TAG);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.session = PipedriveApp.getSessionManager().findActiveSession(getArguments().getString(KEY_SESSION_ID));
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (this.session == null) {
            return null;
        }
        Analytics.hitFragment((Fragment) this);
        int selectedItem = this.session.getSharedSession().getActivityReminderOption().ordinal();
        CharSequence[] items = new CharSequence[ActivityReminderOption.values().length];
        for (int i = 0; i < ActivityReminderOption.values().length; i++) {
            items[i] = ActivityReminderOption.values()[i].getTitle(getActivity());
        }
        return new Builder(getActivity(), R.style.Theme.Pipedrive.Dialog.Alert).setSingleChoiceItems(items, selectedItem, new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ActivityReminderOption newValue = ActivityReminderOption.values()[which];
                if (newValue != ActivityReminderSelectorDialogFragment.this.session.getSharedSession().getActivityReminderOption()) {
                    ActivityReminderSelectorDialogFragment.this.session.getSharedSession().setActivityReminderOption(newValue);
                    Analytics.sendEvent(ActivityReminderSelectorDialogFragment.this.getActivity(), AnalyticsEvent.ACTIVITY_REMINDER_OPTION_CHANGED);
                    if (ActivityReminderSelectorDialogFragment.this.callback != null) {
                        ActivityReminderSelectorDialogFragment.this.callback.onActivityReminderOptionChanged();
                    }
                }
                dialog.dismiss();
            }
        }).setTitle((int) R.string.pref_activity_reminder_title).create();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.callback = (Callback) activity;
        } catch (ClassCastException e) {
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.callback = (Callback) context;
        } catch (ClassCastException e) {
        }
    }
}
