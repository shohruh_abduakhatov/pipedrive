package rx;

import rx.functions.Action1;

public interface Single$OnSubscribe<T> extends Action1<SingleSubscriber<? super T>> {
}
