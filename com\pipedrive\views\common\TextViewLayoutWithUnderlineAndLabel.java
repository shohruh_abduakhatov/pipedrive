package com.pipedrive.views.common;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

public abstract class TextViewLayoutWithUnderlineAndLabel<V extends TextView> extends LayoutWithUnderlineAndLabel<V> {
    public TextViewLayoutWithUnderlineAndLabel(Context context) {
        super(context, null);
    }

    public TextViewLayoutWithUnderlineAndLabel(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
    }

    public TextViewLayoutWithUnderlineAndLabel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public final void setText(@Nullable CharSequence text) {
        ((TextView) getMainView()).setText(text);
    }

    public final CharSequence getText() {
        return ((TextView) getMainView()).getText();
    }
}
