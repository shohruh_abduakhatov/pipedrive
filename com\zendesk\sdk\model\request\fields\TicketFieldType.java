package com.zendesk.sdk.model.request.fields;

public enum TicketFieldType {
    Checkbox,
    Date,
    Decimal,
    Description,
    Integer,
    PartialCreditCard,
    Regexp,
    Subject,
    Tagger,
    Text,
    TextArea,
    Unknown
}
