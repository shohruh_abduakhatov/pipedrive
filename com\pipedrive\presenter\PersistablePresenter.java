package com.pipedrive.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;

public abstract class PersistablePresenter<T> extends Presenter<T> {
    protected abstract String[] getKeysNeededForRestore();

    protected abstract void restoreFromBundle(@NonNull Bundle bundle);

    protected abstract void saveToBundle(@NonNull Bundle bundle);

    public PersistablePresenter(@NonNull Session session, @Nullable Bundle savedState) {
        super(session);
        onRestoreInstanceState(savedState);
    }

    public final void onSaveInstanceState(@NonNull Bundle outState) {
        saveToBundle(outState);
    }

    private void onRestoreInstanceState(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            String[] keysNeededForRestore = getKeysNeededForRestore();
            int length = keysNeededForRestore.length;
            int i = 0;
            while (i < length) {
                if (savedInstanceState.containsKey(keysNeededForRestore[i])) {
                    i++;
                } else {
                    return;
                }
            }
            restoreFromBundle(savedInstanceState);
        }
    }
}
