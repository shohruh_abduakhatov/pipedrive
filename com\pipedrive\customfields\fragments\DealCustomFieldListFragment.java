package com.pipedrive.customfields.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.customfields.views.CustomFieldListView;
import com.pipedrive.customfields.views.DealCustomFieldListView;

public final class DealCustomFieldListFragment extends CustomFieldListFragment {
    private static final String KEY_DEAL_SQL_ID = "DEAL_SQL_ID";

    public static DealCustomFieldListFragment newInstance(@NonNull Long dealSqlId) {
        Bundle args = new Bundle();
        args.putSerializable("DEAL_SQL_ID", dealSqlId);
        DealCustomFieldListFragment fragment = new DealCustomFieldListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    protected CustomFieldListView createCustomFieldListView(@NonNull Context context) {
        return new DealCustomFieldListView(context);
    }

    @Nullable
    protected Long getSqlId() {
        return (Long) getArguments().getSerializable("DEAL_SQL_ID");
    }
}
