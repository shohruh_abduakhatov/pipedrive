package com.zendesk.sdk.network;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.sdk.model.request.Comment;
import com.zendesk.sdk.model.request.CommentsResponse;
import com.zendesk.sdk.model.request.CreateRequest;
import com.zendesk.sdk.model.request.EndUserComment;
import com.zendesk.sdk.model.request.Request;
import com.zendesk.sdk.model.request.fields.TicketForm;
import com.zendesk.service.ZendeskCallback;
import java.util.List;

public interface RequestProvider {
    void addComment(@NonNull String str, @NonNull EndUserComment endUserComment, @Nullable ZendeskCallback<Comment> zendeskCallback);

    void createRequest(@NonNull CreateRequest createRequest, @Nullable ZendeskCallback<CreateRequest> zendeskCallback);

    void getAllRequests(@Nullable ZendeskCallback<List<Request>> zendeskCallback);

    void getComments(@NonNull String str, @Nullable ZendeskCallback<CommentsResponse> zendeskCallback);

    void getRequest(@NonNull String str, @Nullable ZendeskCallback<Request> zendeskCallback);

    void getRequests(@NonNull String str, @Nullable ZendeskCallback<List<Request>> zendeskCallback);

    void getTicketFormsById(@NonNull List<Long> list, @Nullable ZendeskCallback<List<TicketForm>> zendeskCallback);
}
