package com.pipedrive.fcm;

import com.google.firebase.iid.FirebaseInstanceIdService;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.util.devices.MobileDevices;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.Nullable;
import rx.functions.Action0;
import rx.functions.Action1;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0005B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016¨\u0006\u0006"}, d2 = {"Lcom/pipedrive/fcm/PipedriveFirebaseInstanceIDService;", "Lcom/google/firebase/iid/FirebaseInstanceIdService;", "()V", "onTokenRefresh", "", "emptyAction", "pipedrive_prodRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: PipedriveFirebaseInstanceIDService.kt */
public final class PipedriveFirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\bÂ\u0002\u0018\u00002\u00020\u00012\b\u0012\u0004\u0012\u00020\u00030\u0002B\u0007\b\u0002¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0006H\u0016J\u0012\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\u0003H\u0016¨\u0006\b"}, d2 = {"Lcom/pipedrive/fcm/PipedriveFirebaseInstanceIDService$emptyAction;", "Lrx/functions/Action0;", "Lrx/functions/Action1;", "", "()V", "call", "", "t", "pipedrive_prodRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: PipedriveFirebaseInstanceIDService.kt */
    private static final class emptyAction implements Action0, Action1<Object> {
        public static final emptyAction INSTANCE = null;

        static {
            emptyAction com_pipedrive_fcm_PipedriveFirebaseInstanceIDService_emptyAction = new emptyAction();
        }

        private emptyAction() {
            INSTANCE = this;
        }

        public void call(@Nullable Object t) {
        }

        public void call() {
        }
    }

    public void onTokenRefresh() {
        LogJourno.reportEvent(EVENT.MOBILE_DEVICES_FCM_TOKEN_REFRESHED);
        Session activeSession = PipedriveApp.getSessionManager().getActiveSession();
        if (activeSession != null) {
            Intrinsics.checkExpressionValueIsNotNull(activeSession, "activeSession");
            new MobileDevices(activeSession, null, 2, null).updateDevice().subscribe(emptyAction.INSTANCE, emptyAction.INSTANCE);
        }
    }
}
