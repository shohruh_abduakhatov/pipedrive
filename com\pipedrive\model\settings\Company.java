package com.pipedrive.model.settings;

import android.support.annotation.Nullable;
import com.pipedrive.model.BaseDatasourceEntity;

public class Company extends BaseDatasourceEntity {
    @Nullable
    private String mApiToken;
    @Nullable
    private String mDomain;
    @Nullable
    private String mName;
    @Nullable
    private Long mUserPipedriveId;

    @Nullable
    public String getApiToken() {
        return this.mApiToken;
    }

    public void setApiToken(@Nullable String apiToken) {
        this.mApiToken = apiToken;
    }

    @Nullable
    public String getName() {
        return this.mName;
    }

    public void setName(@Nullable String name) {
        this.mName = name;
    }

    @Nullable
    public Long getUserPipedriveId() {
        return this.mUserPipedriveId;
    }

    public void setUserPipedriveId(@Nullable Long pipedriveId) {
        this.mUserPipedriveId = pipedriveId;
    }

    @Nullable
    public String getDomain() {
        return this.mDomain;
    }

    public void setDomain(@Nullable String domain) {
        this.mDomain = domain;
    }

    public String toString() {
        return "Company{mApiToken='" + this.mApiToken + '\'' + ", mName='" + this.mName + '\'' + ", mUserPipedriveId=" + this.mUserPipedriveId + ", mPipedriveId=" + super.getPipedriveId() + ", mDomain='" + this.mDomain + '\'' + '}';
    }
}
