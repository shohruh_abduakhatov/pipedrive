package com.pipedrive.views.calendar.recyclerview;

import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import java.util.Calendar;

public abstract class CalendarRecyclerViewAdapter<VH extends ViewHolder> extends Adapter<VH> {
    public abstract Calendar getItem(int i);
}
