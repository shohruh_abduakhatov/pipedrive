package com.pipedrive.views.profilepicture;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.BitmapDisplayer;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.pipedrive.R;
import com.pipedrive.util.StringUtils;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

class ProfilePicture extends FrameLayout {
    private static final List<String> mDisplayedImages = Collections.synchronizedList(new LinkedList());
    @Nullable
    private DisplayImageOptions mDisplayImageOptions;
    @NonNull
    private final TextView mInitialsText;
    @NonNull
    private final ImageView mProfileImage;
    private final int mProfileImageInitialVisibilityState;

    class AnonymousClass1PeopleViewHolderSimpleImageLoadingListener extends SimpleImageLoadingListener {
        @NonNull
        private ImageView mProfileImageForLaterProcessing;

        AnonymousClass1PeopleViewHolderSimpleImageLoadingListener(@NonNull ImageView profileImageForLaterProcessing) {
            this.mProfileImageForLaterProcessing = profileImageForLaterProcessing;
        }

        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            boolean loadedImageBitmapExists;
            boolean firstDisplay = true;
            if (loadedImage != null) {
                loadedImageBitmapExists = true;
            } else {
                loadedImageBitmapExists = false;
            }
            if (loadedImageBitmapExists) {
                this.mProfileImageForLaterProcessing.setVisibility(0);
                if (view instanceof ImageView) {
                    if (ProfilePicture.mDisplayedImages.contains(imageUri)) {
                        firstDisplay = false;
                    }
                    if (firstDisplay) {
                        ProfilePicture.mDisplayedImages.add(imageUri);
                        FadeInBitmapDisplayer.animate(view, view.getResources().getInteger(R.integer.imageloader_loaded_image_fade_duration_ms));
                    }
                }
            }
        }
    }

    public ProfilePicture(Context context) {
        this(context, null, 0);
    }

    public ProfilePicture(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProfilePicture(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setUpLayout(context);
        this.mInitialsText = (TextView) ButterKnife.findById(this, R.id.initialsText);
        this.mProfileImage = (ImageView) ButterKnife.findById(this, R.id.profileImage);
        this.mProfileImageInitialVisibilityState = this.mProfileImage.getVisibility();
    }

    protected void setUpLayout(Context context) {
        LayoutInflater.from(context).inflate(R.layout.view_profilepicture, this);
    }

    @Nullable
    protected BitmapDisplayer getBitmapDisplayer() {
        return null;
    }

    private DisplayImageOptions getDisplayImageOptions() {
        if (this.mDisplayImageOptions == null) {
            BitmapDisplayer bitmapDisplayer = getBitmapDisplayer();
            Builder displayImageOptionsBuilder = new Builder().cacheInMemory(true).cacheOnDisk(true).considerExifParams(true).resetViewBeforeLoading(true);
            if (bitmapDisplayer != null) {
                displayImageOptionsBuilder.displayer(bitmapDisplayer);
            }
            this.mDisplayImageOptions = displayImageOptionsBuilder.build();
        }
        return this.mDisplayImageOptions;
    }

    public void loadPicture(@Nullable String text, @Nullable String imageUrl) {
        setInitialsText(text);
        loadProfileImage(imageUrl, getDisplayImageOptions());
    }

    private void setInitialsText(@Nullable String text) {
        boolean doNotShowInitials;
        if (text == null) {
            doNotShowInitials = true;
        } else {
            doNotShowInitials = false;
        }
        if (doNotShowInitials) {
            this.mInitialsText.setVisibility(4);
            return;
        }
        this.mInitialsText.setVisibility(0);
        this.mInitialsText.setText(text);
    }

    private void loadProfileImage(@Nullable String profileImageUrl, @NonNull DisplayImageOptions displayImageOptions) {
        this.mProfileImage.setVisibility(this.mProfileImageInitialVisibilityState);
        if (!StringUtils.isTrimmedAndEmpty(profileImageUrl)) {
            ImageLoader.getInstance().displayImage(profileImageUrl, this.mProfileImage, displayImageOptions, new AnonymousClass1PeopleViewHolderSimpleImageLoadingListener(this.mProfileImage));
        }
    }

    public void setTextSize(float textSize) {
        this.mInitialsText.setTextSize(textSize);
    }
}
