package com.zendesk.sdk.deeplinking.targets;

import android.content.Intent;
import com.zendesk.sdk.model.helpcenter.Article;
import com.zendesk.sdk.model.helpcenter.SimpleArticle;
import java.util.ArrayList;

public class ArticleConfiguration extends TargetConfiguration {
    private Article mArticle;
    private SimpleArticle mSimpleArticle;

    public /* bridge */ /* synthetic */ ArrayList getBackStackActivities() {
        return super.getBackStackActivities();
    }

    public /* bridge */ /* synthetic */ DeepLinkType getDeepLinkType() {
        return super.getDeepLinkType();
    }

    public /* bridge */ /* synthetic */ Intent getFallbackActivity() {
        return super.getFallbackActivity();
    }

    public ArticleConfiguration(Article article, ArrayList<Intent> backStackActivities, Intent fallBackActivity) {
        super(DeepLinkType.Article, backStackActivities, fallBackActivity);
        this.mArticle = article;
    }

    public ArticleConfiguration(SimpleArticle article, ArrayList<Intent> backStackActivities, Intent fallBackActivity) {
        super(DeepLinkType.Article, backStackActivities, fallBackActivity);
        this.mSimpleArticle = article;
    }

    public Article getArticle() {
        return this.mArticle;
    }

    public SimpleArticle getSimpleArticle() {
        return this.mSimpleArticle;
    }
}
