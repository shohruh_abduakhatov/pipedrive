package com.pipedrive.nearby.filter;

import android.support.annotation.NonNull;
import com.pipedrive.nearby.util.Irrelevant;
import rx.Observable;
import rx.subjects.Subject;

public enum FilterEventBus {
    INSTANCE;
    
    @NonNull
    private final Subject<Irrelevant, Irrelevant> filterReady;

    @NonNull
    public Observable<Irrelevant> getFilterReady() {
        return this.filterReady;
    }

    public void onFilterReady() {
        this.filterReady.onNext(Irrelevant.INSTANCE);
    }
}
