package com.zendesk.sdk.storage;

import android.support.annotation.NonNull;
import com.zendesk.sdk.model.helpcenter.LastSearch;

class ZendeskHelpCenterSessionCache implements HelpCenterSessionCache {
    public static final LastSearch DEFAULT_SEARCH = new LastSearch("", 0);
    private LastSearch lastSearch;
    private boolean uniqueSearchResultClick = false;

    ZendeskHelpCenterSessionCache() {
    }

    @NonNull
    public LastSearch getLastSearch() {
        return this.lastSearch != null ? this.lastSearch : DEFAULT_SEARCH;
    }

    public void setLastSearch(String query, int resultCount) {
        this.lastSearch = new LastSearch(query, resultCount);
        this.uniqueSearchResultClick = true;
    }

    public void unsetUniqueSearchResultClick() {
        this.uniqueSearchResultClick = false;
    }

    public boolean isUniqueSearchResultClick() {
        return this.uniqueSearchResultClick;
    }

    void reset() {
        this.lastSearch = null;
        this.uniqueSearchResultClick = false;
    }
}
