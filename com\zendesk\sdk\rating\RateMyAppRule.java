package com.zendesk.sdk.rating;

public interface RateMyAppRule {
    boolean permitsShowOfDialog();
}
