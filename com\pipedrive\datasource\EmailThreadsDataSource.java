package com.pipedrive.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.email.EmailThread;
import com.pipedrive.util.CursorHelper;
import java.util.ArrayList;
import java.util.List;

public class EmailThreadsDataSource extends BaseDataSource<EmailThread> {
    private DealsDataSource mDealsDataSource;
    private OrganizationsDataSource mOrganizationsDataSource;

    public EmailThreadsDataSource(SQLiteDatabase dbconn) {
        super(dbconn);
    }

    private OrganizationsDataSource getOrganizationsDataSource() {
        if (this.mOrganizationsDataSource != null) {
            return this.mOrganizationsDataSource;
        }
        OrganizationsDataSource organizationsDataSource = new OrganizationsDataSource(getTransactionalDBConnection());
        this.mOrganizationsDataSource = organizationsDataSource;
        return organizationsDataSource;
    }

    private DealsDataSource getDealsDataSource() {
        if (this.mDealsDataSource != null) {
            return this.mDealsDataSource;
        }
        DealsDataSource dealsDataSource = new DealsDataSource(getTransactionalDBConnection());
        this.mDealsDataSource = dealsDataSource;
        return dealsDataSource;
    }

    @NonNull
    protected String[] getAllColumns() {
        return new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_EMAIL_THREAD_PIPEDRIVE_ID, PipeSQLiteHelper.COLUMN_EMAIL_THREAD_DEAL_ID};
    }

    @NonNull
    protected String getColumnNameForPipedriveId() {
        return PipeSQLiteHelper.COLUMN_EMAIL_THREAD_PIPEDRIVE_ID;
    }

    @NonNull
    protected String getColumnNameForSqlId() {
        return PipeSQLiteHelper.COLUMN_ID;
    }

    @NonNull
    protected String getTableName() {
        return PipeSQLiteHelper.TABLE_EMAIL_THREADS;
    }

    @NonNull
    protected ContentValues getContentValues(@NonNull EmailThread emailThread) {
        ContentValues contentValues = super.getContentValues(emailThread);
        if (emailThread.getDeal() != null && emailThread.getDeal().isStored()) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_THREAD_DEAL_ID, Long.valueOf(emailThread.getDeal().getSqlId()));
        }
        return contentValues;
    }

    @Nullable
    protected EmailThread deflateCursor(@NonNull Cursor cursor) {
        EmailThread emailThread = new EmailThread();
        Long sqlId = CursorHelper.getLong(cursor, getColumnNameForSqlId());
        if (sqlId != null) {
            emailThread.setSqlId(sqlId.longValue());
        }
        Integer pipedriveId = CursorHelper.getInteger(cursor, getColumnNameForPipedriveId());
        if (pipedriveId != null) {
            emailThread.setPipedriveId(pipedriveId.intValue());
        }
        Long dealId = CursorHelper.getLong(cursor, PipeSQLiteHelper.COLUMN_EMAIL_THREAD_DEAL_ID);
        if (dealId != null) {
            emailThread.setDeal((Deal) getDealsDataSource().findBySqlId(dealId.longValue()));
        }
        readOrganizations(emailThread);
        return emailThread;
    }

    public long createOrUpdate(EmailThread emailThread) {
        long sqlId = super.createOrUpdate((BaseDatasourceEntity) emailThread);
        if (!(emailThread == null || !emailThread.isStored() || emailThread.getOrganizations() == null || emailThread.getOrganizations().isEmpty())) {
            List<Organization> orgs = emailThread.getOrganizations();
            beginTransactionNonExclusive();
            for (Organization org : orgs) {
                if (org != null && org.isStored()) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_THREAD_TO_ORG_ORG_ID, Long.valueOf(org.getSqlId()));
                    contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_THREAD_TO_ORG_THREAD_ID, Long.valueOf(emailThread.getSqlId()));
                    SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
                    String str = PipeSQLiteHelper.TABLE_EMAIL_THREADS_TO_ORGS;
                    if (transactionalDBConnection instanceof SQLiteDatabase) {
                        SQLiteInstrumentation.insert(transactionalDBConnection, str, null, contentValues);
                    } else {
                        transactionalDBConnection.insert(str, null, contentValues);
                    }
                }
            }
            commit();
        }
        return sqlId;
    }

    private void readOrganizations(EmailThread emailThread) {
        if (emailThread.isStored()) {
            Cursor cursor;
            ArrayList<Organization> organizations = new ArrayList();
            SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
            String str = PipeSQLiteHelper.TABLE_EMAIL_THREADS_TO_ORGS;
            String[] strArr = new String[]{PipeSQLiteHelper.COLUMN_EMAIL_THREAD_TO_ORG_ORG_ID};
            String str2 = "email_threads_to_orgs_thread_id=?";
            String[] strArr2 = new String[]{String.valueOf(emailThread.getSqlId())};
            if (transactionalDBConnection instanceof SQLiteDatabase) {
                cursor = SQLiteInstrumentation.query(transactionalDBConnection, str, strArr, str2, strArr2, null, null, null);
            } else {
                cursor = transactionalDBConnection.query(str, strArr, str2, strArr2, null, null, null);
            }
            try {
                cursor.moveToPosition(-1);
                while (cursor.moveToNext()) {
                    Organization organization = (Organization) getOrganizationsDataSource().findBySqlId(cursor.getLong(0));
                    if (organization != null) {
                        organizations.add(organization);
                    }
                }
                emailThread.setOrganizations(organizations);
            } finally {
                cursor.close();
            }
        }
    }
}
