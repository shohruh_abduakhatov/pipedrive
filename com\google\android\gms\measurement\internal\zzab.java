package com.google.android.gms.measurement.internal;

import android.content.Context;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.common.util.zzh;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.firebase.analytics.FirebaseAnalytics;

public class zzab {
    final Context mContext;

    public zzab(Context context) {
        zzaa.zzy(context);
        Context applicationContext = context.getApplicationContext();
        zzaa.zzy(applicationContext);
        this.mContext = applicationContext;
    }

    zzd zza(zzx com_google_android_gms_measurement_internal_zzx) {
        return new zzd(com_google_android_gms_measurement_internal_zzx);
    }

    zzt zzb(zzx com_google_android_gms_measurement_internal_zzx) {
        return new zzt(com_google_android_gms_measurement_internal_zzx);
    }

    public zzx zzbyo() {
        return new zzx(this);
    }

    zzq zzc(zzx com_google_android_gms_measurement_internal_zzx) {
        return new zzq(com_google_android_gms_measurement_internal_zzx);
    }

    zzw zzd(zzx com_google_android_gms_measurement_internal_zzx) {
        return new zzw(com_google_android_gms_measurement_internal_zzx);
    }

    zzag zze(zzx com_google_android_gms_measurement_internal_zzx) {
        return new zzag(com_google_android_gms_measurement_internal_zzx);
    }

    zzv zzf(zzx com_google_android_gms_measurement_internal_zzx) {
        return new zzv(com_google_android_gms_measurement_internal_zzx);
    }

    FirebaseAnalytics zzg(zzx com_google_android_gms_measurement_internal_zzx) {
        return new FirebaseAnalytics(com_google_android_gms_measurement_internal_zzx);
    }

    AppMeasurement zzh(zzx com_google_android_gms_measurement_internal_zzx) {
        return new AppMeasurement(com_google_android_gms_measurement_internal_zzx);
    }

    zzac zzi(zzx com_google_android_gms_measurement_internal_zzx) {
        return new zzac(com_google_android_gms_measurement_internal_zzx);
    }

    zzal zzj(zzx com_google_android_gms_measurement_internal_zzx) {
        return new zzal(com_google_android_gms_measurement_internal_zzx);
    }

    zze zzk(zzx com_google_android_gms_measurement_internal_zzx) {
        return new zze(com_google_android_gms_measurement_internal_zzx);
    }

    zzo zzl(zzx com_google_android_gms_measurement_internal_zzx) {
        return new zzo(com_google_android_gms_measurement_internal_zzx);
    }

    zzr zzm(zzx com_google_android_gms_measurement_internal_zzx) {
        return new zzr(com_google_android_gms_measurement_internal_zzx);
    }

    zze zzn(zzx com_google_android_gms_measurement_internal_zzx) {
        return zzh.zzayl();
    }

    zzad zzo(zzx com_google_android_gms_measurement_internal_zzx) {
        return new zzad(com_google_android_gms_measurement_internal_zzx);
    }

    zzae zzp(zzx com_google_android_gms_measurement_internal_zzx) {
        return new zzae(com_google_android_gms_measurement_internal_zzx);
    }

    zzg zzq(zzx com_google_android_gms_measurement_internal_zzx) {
        return new zzg(com_google_android_gms_measurement_internal_zzx);
    }

    zzn zzr(zzx com_google_android_gms_measurement_internal_zzx) {
        return new zzn(com_google_android_gms_measurement_internal_zzx);
    }

    zzs zzs(zzx com_google_android_gms_measurement_internal_zzx) {
        return new zzs(com_google_android_gms_measurement_internal_zzx);
    }

    zzai zzt(zzx com_google_android_gms_measurement_internal_zzx) {
        return new zzai(com_google_android_gms_measurement_internal_zzx);
    }

    zzc zzu(zzx com_google_android_gms_measurement_internal_zzx) {
        return new zzc(com_google_android_gms_measurement_internal_zzx);
    }
}
