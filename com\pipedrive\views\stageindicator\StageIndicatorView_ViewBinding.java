package com.pipedrive.views.stageindicator;

import android.content.Context;
import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.content.ContextCompat;
import android.view.View;
import butterknife.Unbinder;
import com.pipedrive.R;

public class StageIndicatorView_ViewBinding implements Unbinder {
    @UiThread
    public StageIndicatorView_ViewBinding(StageIndicatorView target) {
        this(target, target.getContext());
    }

    @UiThread
    @Deprecated
    public StageIndicatorView_ViewBinding(StageIndicatorView target, View source) {
        this(target, source.getContext());
    }

    @UiThread
    public StageIndicatorView_ViewBinding(StageIndicatorView target, Context context) {
        target.colorActive = ContextCompat.getColor(context, R.color.mid);
        target.colorInactive = ContextCompat.getColor(context, R.color.light);
    }

    @CallSuper
    public void unbind() {
    }
}
