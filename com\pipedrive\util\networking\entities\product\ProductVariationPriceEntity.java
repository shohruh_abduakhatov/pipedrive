package com.pipedrive.util.networking.entities.product;

import android.support.annotation.Nullable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.math.BigDecimal;

public class ProductVariationPriceEntity {
    @Nullable
    @SerializedName("currency")
    @Expose
    private String currency;
    @Nullable
    @SerializedName("id")
    @Expose
    private Long pipedriveId;
    @Nullable
    @SerializedName("price")
    @Expose
    private BigDecimal price;

    @Nullable
    public Long getPipedriveId() {
        return this.pipedriveId;
    }

    @Nullable
    public String getCurrency() {
        return this.currency;
    }

    @Nullable
    public BigDecimal getPrice() {
        return this.price;
    }

    public String toString() {
        return "ProductVariationPriceEntity{pipedriveId=" + getPipedriveId() + ", currency='" + getCurrency() + '\'' + ", price=" + getPrice() + '}';
    }
}
