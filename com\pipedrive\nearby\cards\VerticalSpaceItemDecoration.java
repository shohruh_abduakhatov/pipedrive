package com.pipedrive.nearby.cards;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;

class VerticalSpaceItemDecoration extends ItemDecoration {
    private final int largeVerticalOffset;
    private final int normalVerticalOffset;

    VerticalSpaceItemDecoration(int normalVerticalOffset, int largeVerticalOffset) {
        this.normalVerticalOffset = normalVerticalOffset;
        this.largeVerticalOffset = largeVerticalOffset;
    }

    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, State state) {
        boolean itemIsFirst;
        boolean itemIsLast;
        if (parent.getChildAdapterPosition(view) == 0) {
            itemIsFirst = true;
        } else {
            itemIsFirst = false;
        }
        if (parent.getChildAdapterPosition(view) == parent.getAdapter().getItemCount() - 1) {
            itemIsLast = true;
        } else {
            itemIsLast = false;
        }
        outRect.left = itemIsFirst ? this.largeVerticalOffset : this.normalVerticalOffset;
        outRect.right = itemIsLast ? this.largeVerticalOffset : this.normalVerticalOffset;
    }
}
