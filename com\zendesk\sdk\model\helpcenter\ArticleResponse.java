package com.zendesk.sdk.model.helpcenter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.util.CollectionUtils;
import java.util.List;

public class ArticleResponse {
    private Article article;
    private List<User> users;

    @Nullable
    public Article getArticle() {
        return this.article;
    }

    @NonNull
    public List<User> getUsers() {
        return CollectionUtils.copyOf(this.users);
    }
}
