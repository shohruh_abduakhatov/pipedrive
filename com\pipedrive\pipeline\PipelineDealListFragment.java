package com.pipedrive.pipeline;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import butterknife.Unbinder;
import com.pipedrive.R;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.contentproviders.DealsContentProvider;
import com.pipedrive.deal.view.DealDetailsActivity;
import com.pipedrive.fragments.BaseFragment;
import com.pipedrive.logging.Log;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.views.CustomSwipeRefreshLayout;

public class PipelineDealListFragment extends BaseFragment implements OnRefreshListener {
    public static final String ARG_POSITION = "position";
    public static final String ARG_STAGE_ID = "stageId";
    public static final String ARG_STAGE_NAME = "stageName";
    private PipelineDealListAdapter mAdapter;
    private ContentObserver mContentObserver = new ContentObserver(new Handler()) {
        public void onChange(boolean selfChange, Uri uri) {
            super.onChange(selfChange, uri);
            if (PipelineDealListFragment.this.mInterface != null) {
                PipelineDealListFragment.this.mInterface.requery(Integer.valueOf(PipelineDealListFragment.this.mStageId));
            }
        }
    };
    @BindView(2131820884)
    TextView mEmpty;
    @BindView(2131820921)
    PipelineDealListHeaderView mHeaderView;
    private PipelineDealListFragmentInterface mInterface;
    @BindView(2131820825)
    ListView mListView;
    private boolean mLoading = true;
    private int mPosition;
    private Session mSession;
    @Nullable
    private StageData mStageData;
    private int mStageId;
    private String mStageName;
    @BindView(2131820883)
    CustomSwipeRefreshLayout mSwipeRefreshLayout;
    @Nullable
    private Unbinder mUnbinder;

    public static PipelineDealListFragment newInstance(int stageId, String stageName, int position) {
        Bundle args = new Bundle();
        args.putInt(ARG_STAGE_ID, stageId);
        args.putInt(ARG_POSITION, position);
        args.putString(ARG_STAGE_NAME, stageName);
        PipelineDealListFragment fragment = new PipelineDealListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.mInterface = (PipelineDealListFragmentInterface) context;
        } catch (ClassCastException e) {
            Log.e(new RuntimeException(context.toString() + " has to implement " + PipelineDealListFragmentInterface.class.getSimpleName()));
        }
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mStageId = getArguments().getInt(ARG_STAGE_ID);
        this.mPosition = getArguments().getInt(ARG_POSITION);
        this.mStageName = getArguments().getString(ARG_STAGE_NAME);
        this.mSession = PipedriveApp.getActiveSession();
    }

    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.mInterface.attach(this.mStageId, this.mPosition, this);
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pipeline_deals_list, container, false);
    }

    public void onResume() {
        super.onResume();
        this.mHeaderView.init(this.mSession, this.mStageName, this.mStageData);
        this.mSwipeRefreshLayout.setOnRefreshListener(this);
        updateListView();
        this.mSession.getApplicationContext().getContentResolver().registerContentObserver(new DealsContentProvider(this.mSession).createAllDealsUri(), false, this.mContentObserver);
    }

    public void onPause() {
        this.mSession.getApplicationContext().getContentResolver().unregisterContentObserver(this.mContentObserver);
        super.onPause();
    }

    public void onDestroyView() {
        if (this.mUnbinder != null) {
            this.mUnbinder.unbind();
        }
        super.onDestroyView();
    }

    public void onDetach() {
        this.mInterface.detach(this.mStageId, this.mPosition, this);
        this.mInterface = null;
        super.onDetach();
    }

    public void setData(@Nullable StageData stageData) {
        this.mStageData = stageData;
    }

    public void setLoading(boolean loading) {
        this.mLoading = loading;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        this.mUnbinder = ButterKnife.bind(this, view);
        this.mListView.addFooterView(ViewUtil.getFooterForListWithFloatingButton(getActivity()), null, true);
        this.mListView.setEmptyView(this.mEmpty);
    }

    @OnItemClick({2131820825})
    void onDealClicked(AdapterView<?> adapterView, View view, int position, long id) {
        long dealId = this.mAdapter.getItemId(position);
        if (dealId > 0) {
            DealDetailsActivity.start(getActivity(), Long.valueOf(dealId));
        }
    }

    public void updateDataView() {
        if (isVisible()) {
            int emptyTextResId;
            if (this.mStageData != null) {
                this.mHeaderView.setSummaryData(this.mStageData.getDealSumValue(), this.mStageData.getNumberOfDealsCountedForSumValue());
                emptyTextResId = R.string.label_empty_header_pipeline_deals_list;
            } else {
                this.mHeaderView.clearSummaryData();
                emptyTextResId = R.string.something_went_wrong_while_downloading_deals_pull_to_refresh_the_view_to_update_the_view;
            }
            this.mEmpty.setText(emptyTextResId);
            updateListView();
        }
    }

    private void updateListView() {
        Cursor cursor = null;
        if (this.mAdapter == null) {
            Session session = this.mSession;
            if (this.mStageData != null) {
                cursor = this.mStageData.getDealsCursor();
            }
            this.mAdapter = new PipelineDealListAdapter(session, cursor);
        } else {
            PipelineDealListAdapter pipelineDealListAdapter = this.mAdapter;
            if (this.mStageData != null) {
                cursor = this.mStageData.getDealsCursor();
            }
            pipelineDealListAdapter.changeCursor(cursor);
        }
        if (this.mListView.getAdapter() == null) {
            this.mListView.setAdapter(this.mAdapter);
        }
    }

    public void updateLoadingView() {
        if (isVisible()) {
            this.mHeaderView.setLoading(this.mLoading);
            if (!this.mLoading) {
                this.mSwipeRefreshLayout.setRefreshing(false);
            }
        }
    }

    public void onRefresh() {
        if (this.mInterface != null) {
            this.mInterface.refresh(this.mStageId);
        }
    }
}
