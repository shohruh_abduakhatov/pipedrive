package com.pipedrive.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.pipedrive.R;
import com.pipedrive.adapter.filter.FilterContentPipelineAdapter.FilterRow;
import com.pipedrive.adapter.filter.FilterContentPipelineAdapter.Header;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.FiltersDataSource;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.model.Filter;
import com.pipedrive.model.User;
import com.pipedrive.model.UserSettingsPipelineFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public enum FiltersUtil {
    ;

    @Nullable
    public static Filter readFilter(JsonReader reader) throws IOException {
        Integer pipedriveId = null;
        String name = null;
        String type = null;
        Boolean isActive = null;
        Boolean isPrivate = null;
        reader.beginObject();
        while (reader.hasNext()) {
            String contactField = reader.nextName();
            if (PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID.equals(contactField)) {
                pipedriveId = Integer.valueOf(reader.nextInt());
            } else if ("name".equals(contactField) && reader.peek() != JsonToken.NULL) {
                name = reader.nextString();
            } else if ("type".equals(contactField) && reader.peek() != JsonToken.NULL) {
                type = reader.nextString();
            } else if ("active_flag".equals(contactField) && reader.peek() != JsonToken.NULL) {
                isActive = Boolean.valueOf(reader.nextBoolean());
            } else if (!PipeSQLiteHelper.COLUMN_VISIBLE_TO.equals(contactField) || reader.peek() == JsonToken.NULL) {
                reader.skipValue();
            } else {
                isPrivate = Boolean.valueOf("1".equals(reader.nextString()));
            }
        }
        reader.endObject();
        if (pipedriveId == null || name == null || type == null || isActive == null || isPrivate == null) {
            return null;
        }
        return Filter.create(pipedriveId, name, type, isActive, isPrivate);
    }

    @Nullable
    public static String getSelectedFilterName(@NonNull Session session) {
        int selectedFilterIdx = getPipelineFilterSpinnerIdx(session);
        List<FilterRow> filterLabels = getPipelineFilterContent(session);
        boolean selectedFilterIdxHasLabel = selectedFilterIdx >= 0 && filterLabels.size() > selectedFilterIdx;
        if (selectedFilterIdxHasLabel) {
            return ((FilterRow) filterLabels.get(selectedFilterIdx)).getLabel();
        }
        return null;
    }

    public static int getSelectedPipelineFilterId(Session session, int returnIfNotFound) {
        int selectedPipelineId = (int) session.getPipelineSelectedPipelineId(-2147483648L);
        if (selectedPipelineId == Integer.MIN_VALUE) {
            return returnIfNotFound;
        }
        int selectedPipelineFilterId = returnIfNotFound;
        for (UserSettingsPipelineFilter userSettingsPipelineFilter : UsersUtil.getUserSettingsFilters(session)) {
            if (userSettingsPipelineFilter.getPipelineId() == ((long) selectedPipelineId)) {
                selectedPipelineFilterId = (int) userSettingsPipelineFilter.getFilterId();
                break;
            }
        }
        return selectedPipelineFilterId;
    }

    public static boolean isFilterSet(@NonNull Session session) {
        return session.getPipelineFilter(-1) > 0 || session.getPipelineUser(-1) > 0;
    }

    @NonNull
    public static List<FilterRow> getPipelineFilterContent(@NonNull Session session) {
        List<FilterRow> pipelineFilters = getPipelineFiltersWithHeader(session, "deals");
        List<FilterRow> companyUsers = getPipelineCompanyUsersWithHeader(session);
        renameCurrentlyAuthenticatedUserInPipelineFilter(session, companyUsers);
        List<FilterRow> pipelineFilterContent = new ArrayList();
        pipelineFilterContent.addAll(companyUsers);
        pipelineFilterContent.addAll(pipelineFilters);
        return pipelineFilterContent;
    }

    private static void renameCurrentlyAuthenticatedUserInPipelineFilter(@NonNull Session session, @NonNull List<FilterRow> filterRows) {
        long authenticatedUserPipedriveId = session.getAuthenticatedUserID();
        for (FilterRow filterRow : filterRows) {
            boolean filterRowIsCurrentlyAuthenticatedUser;
            User userInFilterRow = filterRow.getUser();
            if (userInFilterRow == null || userInFilterRow.getPipedriveIdOrNull() == null || userInFilterRow.getPipedriveIdOrNull().longValue() != authenticatedUserPipedriveId) {
                filterRowIsCurrentlyAuthenticatedUser = false;
                continue;
            } else {
                filterRowIsCurrentlyAuthenticatedUser = true;
                continue;
            }
            if (filterRowIsCurrentlyAuthenticatedUser) {
                userInFilterRow.setName(session.getApplicationContext().getString(R.string.you, new Object[]{userInFilterRow.getName()}));
                return;
            }
        }
    }

    @NonNull
    public static List<FilterRow> getPipelineCompanyUsersWithHeader(@NonNull Session session) {
        List<User> companyUsers = UsersUtil.getActiveCompanyUsersWithCurrentlyAuthenticatedUserAsFirst(session);
        List<FilterRow> users = new ArrayList();
        users.add(new Header(session.getApplicationContext().getString(R.string.special_user_name_header_title)));
        users.add(new User(null, session.getApplicationContext().getString(R.string.special_user_name_everyone)));
        users.addAll(companyUsers);
        return users;
    }

    @NonNull
    public static List<FilterRow> getPipelineFiltersWithHeader(@NonNull Session session, @Nullable String type) {
        FiltersDataSource filtersDS = new FiltersDataSource(session);
        List<Filter> pipelineFilters = TextUtils.isEmpty(type) ? filtersDS.findAllActive() : filtersDS.findAllActiveByType(type);
        List<FilterRow> filters = new ArrayList();
        filters.add(new Header(session.getApplicationContext().getString(R.string.special_user_name_header_title_filter)));
        filters.addAll(pipelineFilters);
        return filters;
    }

    public static int getPipelineFilterSpinnerIdx(@NonNull Session session) {
        long userId = session.getPipelineUser(0);
        long filterId = session.getPipelineFilter(0);
        List<FilterRow> rows = getPipelineFilterContent(session);
        int i;
        if (userId > 0) {
            for (i = 0; i < rows.size(); i++) {
                User user = ((FilterRow) rows.get(i)).getUser();
                if (user != null && user.getPipedriveIdOrNull() != null && user.getPipedriveIdOrNull().longValue() == userId) {
                    return i;
                }
            }
        } else if (filterId > 0) {
            for (i = 0; i < rows.size(); i++) {
                Filter filter = ((FilterRow) rows.get(i)).getFilter();
                if (filter != null && filter.getPipedriveIdOrNull() != null && filter.getPipedriveIdOrNull().longValue() == filterId) {
                    return i;
                }
            }
        }
        return getFilterSpinnerIdxAllUsers();
    }

    public static int getFilterSpinnerIdxAllUsers() {
        return 1;
    }
}
