package com.zendesk.sdk.requests;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.background.ApplicationStateMonitor;
import com.newrelic.agent.android.instrumentation.Instrumented;
import com.newrelic.agent.android.tracing.TraceMachine;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.sdk.model.request.Request;
import com.zendesk.sdk.network.NetworkAware;
import com.zendesk.sdk.network.RequestLoadingListener;
import com.zendesk.sdk.network.Retryable;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.ui.LoadingState;
import com.zendesk.sdk.ui.NetworkAwareActionbarActivity;
import com.zendesk.sdk.util.NetworkUtils;
import com.zendesk.sdk.util.UiUtils;
import com.zendesk.service.ErrorResponse;
import com.zendesk.service.ErrorResponseAdapter;
import com.zendesk.service.SafeZendeskCallback;
import com.zendesk.service.ZendeskCallback;
import com.zendesk.util.CollectionUtils;
import com.zendesk.util.StringUtils;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Instrumented
public class RequestListFragment extends ListFragment implements NetworkAware, TraceFieldInterface {
    private static final String FILTER_BY_STATUSES = "new,open,pending,hold,solved";
    private static final String LOG_TAG = RequestListFragment.class.getSimpleName();
    private View mEmptyView;
    private AtomicBoolean mIsLoading = new AtomicBoolean(false);
    private RequestLoadingListener mListener;
    private View mProgressView;
    private SafeZendeskCallback<List<Request>> mRequestsCallback;
    private Retryable mRetryable;

    class RequestsCallback extends ZendeskCallback<List<Request>> {
        RequestsCallback() {
        }

        public void onSuccess(List<Request> requests) {
            RequestListFragment.this.mIsLoading.set(false);
            RequestListFragment.this.setLoadingState(LoadingState.DISPLAYING);
            if (RequestListFragment.this.mListener != null) {
                int numberOfRequests = 0;
                if (!CollectionUtils.isEmpty(requests)) {
                    numberOfRequests = requests.size();
                }
                RequestListFragment.this.mListener.onLoadFinished(numberOfRequests);
            }
            final ListView listView = RequestListFragment.this.getListView();
            if (listView != null && RequestListFragment.this.getActivity() != null) {
                RequestListFragment.this.setListAdapter(new RequestsAdapter(RequestListFragment.this.getActivity(), requests));
                listView.setOnItemClickListener(new OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                        if (NetworkUtils.isConnected(RequestListFragment.this.getActivity())) {
                            Request request = (Request) listView.getAdapter().getItem(position);
                            Intent intent = new Intent(RequestListFragment.this.getActivity(), ViewRequestActivity.class);
                            intent.putExtra("requestId", request.getId());
                            if (StringUtils.hasLength(request.getSubject())) {
                                intent.putExtra("subject", request.getSubject());
                            }
                            RequestListFragment.this.startActivity(intent);
                            return;
                        }
                        Logger.d(RequestListFragment.LOG_TAG, "Ignoring conversation selection because there is no network connection", new Object[0]);
                    }
                });
            }
        }

        public void onError(ErrorResponse error) {
            RequestListFragment.this.mIsLoading.set(false);
            Logger.w(RequestListFragment.LOG_TAG, "Failed to fetch requests: " + error.getReason() + " status " + error.getStatus(), new Object[0]);
            RequestListFragment.this.setLoadingState(LoadingState.ERRORED);
            if (RequestListFragment.this.mListener != null) {
                RequestListFragment.this.mListener.onLoadError(error);
            }
        }
    }

    protected void onStart() {
        super.onStart();
        ApplicationStateMonitor.getInstance().activityStarted();
    }

    protected void onStop() {
        super.onStop();
        ApplicationStateMonitor.getInstance().activityStopped();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        try {
            TraceMachine.enterMethod(this._nr_trace, "RequestListFragment#onCreateView", null);
        } catch (NoSuchFieldError e) {
            while (true) {
                TraceMachine.enterMethod(null, "RequestListFragment#onCreateView", null);
            }
        }
        View fragmentView = layoutInflater.inflate(R.layout.fragment_request_list, viewGroup, false);
        this.mProgressView = fragmentView.findViewById(R.id.request_list_fragment_progress);
        this.mEmptyView = fragmentView.findViewById(16908292);
        TraceMachine.exitMethod();
        return fragmentView;
    }

    public void onResume() {
        super.onResume();
        setupCallback();
        refreshRequests();
    }

    public void onPause() {
        super.onPause();
        teardownCallback();
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Retryable) {
            this.mRetryable = (Retryable) context;
        }
    }

    public void onDetach() {
        super.onDetach();
        this.mRetryable = null;
    }

    public void refreshRequests() {
        if (!this.mIsLoading.compareAndSet(false, true)) {
            Logger.d(LOG_TAG, "Already loading requests, skipping refreshRequests()", new Object[0]);
        } else if (!ZendeskConfig.INSTANCE.isAuthenticationAvailable()) {
            String error = "Cannot load requests because no identity has been set up. See ZendeskConfig.INSTANCE.setIdentity(Identity identity)";
            Logger.e(LOG_TAG, "Cannot load requests because no identity has been set up. See ZendeskConfig.INSTANCE.setIdentity(Identity identity)", new Object[0]);
            if (this.mListener != null) {
                this.mListener.onLoadError(new ErrorResponseAdapter("Cannot load requests because no identity has been set up. See ZendeskConfig.INSTANCE.setIdentity(Identity identity)"));
            }
        } else if (NetworkUtils.isConnected(getContext())) {
            Logger.d(LOG_TAG, "Requesting requests from the network", new Object[0]);
            setLoadingState(LoadingState.LOADING);
            if (this.mListener != null) {
                this.mListener.onLoadStarted();
            }
            try {
                ZendeskConfig.INSTANCE.provider().requestProvider().getRequests(FILTER_BY_STATUSES, this.mRequestsCallback);
            } catch (Exception e) {
                if (getActivity() != null) {
                    getActivity().finish();
                }
            }
        } else {
            Logger.d(LOG_TAG, "No network connect", new Object[0]);
            setLoadingState(LoadingState.ERRORED);
            this.mIsLoading.set(false);
            FragmentActivity activity = getActivity();
            if (activity != null && (activity instanceof NetworkAwareActionbarActivity)) {
                ((NetworkAwareActionbarActivity) activity).onNetworkUnavailable();
            }
        }
    }

    public void setRequestLoadingListener(RequestLoadingListener listener) {
        this.mListener = listener;
    }

    private void setLoadingState(LoadingState loadingState) {
        if (loadingState == null) {
            Logger.w(LOG_TAG, "LoadingState was null, nothing to do", new Object[0]);
        } else if (isAdded()) {
            switch (loadingState) {
                case LOADING:
                    UiUtils.setVisibility(getListView(), 8);
                    UiUtils.setVisibility(this.mProgressView, 0);
                    UiUtils.setVisibility(this.mEmptyView, 8);
                    if (this.mRetryable != null) {
                        this.mRetryable.onRetryUnavailable();
                        return;
                    }
                    return;
                case DISPLAYING:
                    UiUtils.setVisibility(getListView(), 0);
                    UiUtils.setVisibility(this.mProgressView, 8);
                    UiUtils.setVisibility(this.mEmptyView, 8);
                    if (this.mRetryable != null) {
                        this.mRetryable.onRetryUnavailable();
                        return;
                    }
                    return;
                case ERRORED:
                    UiUtils.setVisibility(getListView(), 0);
                    UiUtils.setVisibility(this.mProgressView, 8);
                    UiUtils.setVisibility(this.mEmptyView, 8);
                    if (this.mRetryable != null) {
                        this.mRetryable.onRetryAvailable(getString(R.string.request_list_fragment_error_message), new OnClickListener() {
                            public void onClick(View v) {
                                RequestListFragment.this.refreshRequests();
                            }
                        });
                        return;
                    }
                    return;
                default:
                    return;
            }
        } else {
            Logger.w(LOG_TAG, "Activity is not attached, nothing to do", new Object[0]);
        }
    }

    private void setupCallback() {
        this.mRequestsCallback = SafeZendeskCallback.from(new RequestsCallback());
    }

    private void teardownCallback() {
        this.mRequestsCallback.cancel();
        this.mRequestsCallback = null;
        this.mIsLoading.set(false);
    }

    public void onNetworkAvailable() {
        Logger.d(LOG_TAG, "RequestListFragment received onNetworkAvailable", new Object[0]);
        refreshRequests();
    }

    public void onNetworkUnavailable() {
        Logger.d(LOG_TAG, "RequestListFragment received onNetworkUnavailable", new Object[0]);
    }
}
