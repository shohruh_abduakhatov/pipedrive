package com.pipedrive.person.edit;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.model.Person;
import com.pipedrive.tasks.AsyncTask;

class ReadPersonTasks extends AsyncTask<Long, Void, Person> {
    @Nullable
    private OnTaskFinished mOnTaskFinished;

    @MainThread
    interface OnTaskFinished {
        void onPersonRead(@Nullable Person person);
    }

    public ReadPersonTasks(@NonNull Session session, @Nullable OnTaskFinished onTaskFinished) {
        super(session);
        this.mOnTaskFinished = onTaskFinished;
    }

    protected Person doInBackground(Long... params) {
        return (Person) new PersonsDataSource(getSession().getDatabase()).findBySqlId(params[0].longValue());
    }

    protected void onPostExecute(Person person) {
        if (this.mOnTaskFinished != null) {
            this.mOnTaskFinished.onPersonRead(person);
        }
    }
}
