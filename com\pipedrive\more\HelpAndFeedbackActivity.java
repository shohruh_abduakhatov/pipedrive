package com.pipedrive.more;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.pipedrive.BaseActivity;
import com.pipedrive.R;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.AnalyticsEvent;
import com.pipedrive.application.PipedriveApp;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.requests.RequestActivity;
import com.zendesk.sdk.support.SupportActivity.Builder;

public class HelpAndFeedbackActivity extends BaseActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_help_and_feedback);
        ButterKnife.bind((Activity) this);
    }

    public static void startActivity(@NonNull Activity activity) {
        ContextCompat.startActivity(activity, new Intent(activity, HelpAndFeedbackActivity.class), ActivityOptionsCompat.makeBasic().toBundle());
    }

    @OnClick({2131820774})
    void onTipsAndTricksClicked() {
        setIdentity();
        ZendeskFeedbackConfiguration zendeskConfiguration = PipedriveApp.getZendeskConfiguration(getSession());
        Analytics.sendEvent(this, AnalyticsEvent.TIPS_AND_TRICKS_PRESSED);
        new Builder().withArticlesForSectionIds(new long[]{201493829}).withContactConfiguration(zendeskConfiguration).show(this);
    }

    @OnClick({2131820775})
    void onMyFeedbackClicked() {
        setIdentity();
        Analytics.sendEvent(this, AnalyticsEvent.MY_FEEDBACK_PRESSED);
        RequestActivity.startActivity(this, PipedriveApp.getZendeskConfiguration(getSession()));
    }

    private void setIdentity() {
        String userProfileName = getSession().getUserSettingsName("");
        String userSettingsEmail = getSession().getUserSettingsEmail("");
        ZendeskConfig.INSTANCE.setIdentity(new AnonymousIdentity.Builder().withNameIdentifier(userProfileName).withEmailIdentifier(userSettingsEmail).withExternalIdentifier(String.valueOf(getSession().getAuthenticatedUserID())).build());
    }
}
