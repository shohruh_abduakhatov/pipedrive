package com.pipedrive.activity;

import android.support.annotation.MainThread;
import com.pipedrive.application.Session;
import com.pipedrive.contentproviders.FlowContentProvider;
import com.pipedrive.model.Activity;
import com.pipedrive.store.StoreActivity;
import com.pipedrive.tasks.AsyncTask;

public class UpdateActivityTask extends AsyncTask<Activity, Void, Boolean> {
    private final OnTaskFinished mOnTaskFinished;

    @MainThread
    public interface OnTaskFinished {
        void onActivityUpdated(boolean z);
    }

    public UpdateActivityTask(Session session, OnTaskFinished onTaskFinished) {
        super(session);
        this.mOnTaskFinished = onTaskFinished;
    }

    protected Boolean doInBackground(Activity... params) {
        return Boolean.valueOf(new StoreActivity(getSession()).update(params[0]));
    }

    protected void onPostExecute(Boolean success) {
        if (success.booleanValue()) {
            FlowContentProvider.notifyChangesMadeInFlow(getSession().getApplicationContext());
        }
        if (this.mOnTaskFinished != null) {
            this.mOnTaskFinished.onActivityUpdated(success.booleanValue());
        }
    }
}
