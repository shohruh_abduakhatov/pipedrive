package com.pipedrive.views.association;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.pipedrive.PersonDetailActivity;
import com.pipedrive.R;
import com.pipedrive.linking.PersonLinkingActivity;
import com.pipedrive.model.Person;
import com.pipedrive.views.association.AssociationView.AssociationListener;

public class PersonAssociationView extends AssociationView<Person> {
    public /* bridge */ /* synthetic */ void enableAssociationDetailViewOpening() {
        super.enableAssociationDetailViewOpening();
    }

    public /* bridge */ /* synthetic */ void setAssociationListener(@Nullable AssociationListener associationListener) {
        super.setAssociationListener(associationListener);
    }

    public PersonAssociationView(Context context) {
        super(context);
    }

    public PersonAssociationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PersonAssociationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @NonNull
    String getAssociationName(@NonNull Person association) {
        return association.getName();
    }

    boolean canAssociationBeViewed(@NonNull Person association) {
        return association.isStored();
    }

    void openAssociationDetailView(@NonNull Person association) {
        PersonDetailActivity.startActivity(getContext(), Long.valueOf(association.getSqlId()));
    }

    int getAssociationNameHint() {
        return R.string.lbl_choose_contact;
    }

    @Nullable
    Integer getAssociationTypeIconImageLevel() {
        return Integer.valueOf(0);
    }

    void requestNewAssociation() {
        Context context = getContext();
        if (context != null && (context instanceof Activity) && getRequestCode() != null) {
            PersonLinkingActivity.startActivityForResult((Activity) getContext(), getRequestCode().intValue());
        }
    }
}
