package com.pipedrive.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Spinner;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;
import com.pipedrive.views.assignment.AssignedToView;
import com.pipedrive.views.association.DealAssociationView;
import com.pipedrive.views.association.OrganizationAssociationView;
import com.pipedrive.views.association.PersonAssociationView;
import com.pipedrive.views.edit.activity.ActivitySubjectEditText;
import com.pipedrive.views.edit.activity.DueDateView;
import com.pipedrive.views.edit.activity.DueTimeView;
import com.pipedrive.views.edit.activity.DurationView;
import com.pipedrive.views.edit.activity.NoteView;

public class ActivityDetailActivity_ViewBinding implements Unbinder {
    private ActivityDetailActivity target;
    private View view2131820708;

    @UiThread
    public ActivityDetailActivity_ViewBinding(ActivityDetailActivity target) {
        this(target, target.getWindow().getDecorView());
    }

    @UiThread
    public ActivityDetailActivity_ViewBinding(final ActivityDetailActivity target, View source) {
        this.target = target;
        target.mSubject = (ActivitySubjectEditText) Utils.findRequiredViewAsType(source, R.id.subject, "field 'mSubject'", ActivitySubjectEditText.class);
        target.mCheckboxDone = (CheckBox) Utils.findRequiredViewAsType(source, R.id.checkDone, "field 'mCheckboxDone'", CheckBox.class);
        target.mActivityDueDateView = (DueDateView) Utils.findRequiredViewAsType(source, R.id.activityDueDate, "field 'mActivityDueDateView'", DueDateView.class);
        target.mActivityDueTime = (DueTimeView) Utils.findRequiredViewAsType(source, R.id.activityDueTime, "field 'mActivityDueTime'", DueTimeView.class);
        target.mActivityDuration = (DurationView) Utils.findRequiredViewAsType(source, R.id.activityDuration, "field 'mActivityDuration'", DurationView.class);
        target.mSpinnerActivityType = (Spinner) Utils.findRequiredViewAsType(source, R.id.spinnerActivityType, "field 'mSpinnerActivityType'", Spinner.class);
        target.mAssignedToView = (AssignedToView) Utils.findRequiredViewAsType(source, R.id.assignedToView, "field 'mAssignedToView'", AssignedToView.class);
        target.mActivityNoteTextView = (NoteView) Utils.findRequiredViewAsType(source, R.id.noteTextView, "field 'mActivityNoteTextView'", NoteView.class);
        target.mPersonAssociationView = (PersonAssociationView) Utils.findRequiredViewAsType(source, R.id.personAssociationView, "field 'mPersonAssociationView'", PersonAssociationView.class);
        target.mOrganizationAssociationView = (OrganizationAssociationView) Utils.findRequiredViewAsType(source, R.id.organizationAssociationView, "field 'mOrganizationAssociationView'", OrganizationAssociationView.class);
        target.mDealAssociationView = (DealAssociationView) Utils.findRequiredViewAsType(source, R.id.dealAssociationView, "field 'mDealAssociationView'", DealAssociationView.class);
        View view = Utils.findRequiredView(source, R.id.noteTextViewContainer, "method 'onNoteClicked'");
        this.view2131820708 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onNoteClicked();
            }
        });
    }

    @CallSuper
    public void unbind() {
        ActivityDetailActivity target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mSubject = null;
        target.mCheckboxDone = null;
        target.mActivityDueDateView = null;
        target.mActivityDueTime = null;
        target.mActivityDuration = null;
        target.mSpinnerActivityType = null;
        target.mAssignedToView = null;
        target.mActivityNoteTextView = null;
        target.mPersonAssociationView = null;
        target.mOrganizationAssociationView = null;
        target.mDealAssociationView = null;
        this.view2131820708.setOnClickListener(null);
        this.view2131820708 = null;
    }
}
