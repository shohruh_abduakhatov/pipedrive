package com.pipedrive.util.networking;

import com.pipedrive.BuildConfig;
import com.pipedrive.application.Session;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient.Builder;
import org.jetbrains.annotations.NotNull;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u001a\u000e\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u001a\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0002\u001a\u00020\u0003H\u0002¨\u0006\u0006"}, d2 = {"createRetrofitFromSession", "Lretrofit2/Retrofit;", "session", "Lcom/pipedrive/application/Session;", "getHostnameFromSession", "", "pipedrive_prodRelease"}, k = 2, mv = {1, 1, 6})
/* compiled from: RetrofitUtils.kt */
public final class RetrofitUtilsKt {
    @NotNull
    public static final Retrofit createRetrofitFromSession(@NotNull Session session) {
        Object obj = null;
        Intrinsics.checkParameterIsNotNull(session, SettingsJsonConstants.SESSION_KEY);
        Builder httpClientBuilder = HttpClientManager.INSTANCE.getHttpClient().newBuilder();
        for (Interceptor it : httpClientBuilder.networkInterceptors()) {
            if (it instanceof ApiTokenInterceptor) {
                Object obj2 = null;
                break;
            }
        }
        int i = 1;
        if (obj2 != null) {
            httpClientBuilder.networkInterceptors().add(new ApiTokenInterceptor(session));
        }
        for (Interceptor it2 : httpClientBuilder.networkInterceptors()) {
            if (it2 instanceof StrictModeInterceptor) {
                break;
            }
        }
        int i2 = 1;
        if (obj != null) {
            httpClientBuilder.networkInterceptors().add(new StrictModeInterceptor());
        }
        Retrofit build = new Retrofit.Builder().baseUrl("https://" + getHostnameFromSession(session) + "/v1/").client(httpClientBuilder.build()).addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io())).addConverterFactory(GsonConverterFactory.create()).build();
        Intrinsics.checkExpressionValueIsNotNull(build, "Retrofit.Builder()\n     …reate())\n        .build()");
        return build;
    }

    private static final String getHostnameFromSession(Session session) {
        String str = BuildConfig.OVERRIDE_API_HOSTNAME;
        return str != null ? str : session.getDomain(SettingsJsonConstants.APP_KEY) + ".pipedrive.com";
    }
}
