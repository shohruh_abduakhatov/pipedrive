package com.pipedrive.store;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.changes.FlowFileChanger;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.model.FlowFile;

public class StoreFlowFile extends Store<FlowFile> {
    public StoreFlowFile(@NonNull Session session) {
        super(session);
    }

    void cleanupBeforeCreate(@NonNull FlowFile newFlowFileBeingStored) {
        boolean newFlowFileHasAssociatedOrganization;
        boolean newFlowFileHasAssociatedPerson;
        boolean newFlowFileHasAssociatedDeal = true;
        if (newFlowFileBeingStored.getOrganization() != null) {
            newFlowFileHasAssociatedOrganization = true;
        } else {
            newFlowFileHasAssociatedOrganization = false;
        }
        if (newFlowFileHasAssociatedOrganization) {
            new StoreOrganization(getSession()).cleanupBeforeCreate(newFlowFileBeingStored.getOrganization());
        }
        if (newFlowFileBeingStored.getPerson() != null) {
            newFlowFileHasAssociatedPerson = true;
        } else {
            newFlowFileHasAssociatedPerson = false;
        }
        if (newFlowFileHasAssociatedPerson) {
            new StorePerson(getSession()).cleanupBeforeCreate(newFlowFileBeingStored.getPerson());
        }
        if (newFlowFileBeingStored.getDeal() == null) {
            newFlowFileHasAssociatedDeal = false;
        }
        if (newFlowFileHasAssociatedDeal) {
            new StoreDeal(getSession()).cleanupBeforeCreate(newFlowFileBeingStored.getDeal());
        }
        processCreateBeforeStoring(newFlowFileBeingStored);
        processBeforeStoringShared(newFlowFileBeingStored);
    }

    boolean implementCreate(@NonNull FlowFile flowFileToCreate) {
        return new FlowFileChanger(getSession()).create(flowFileToCreate);
    }

    boolean implementUpdate(@NonNull FlowFile flowFileToUpdate) {
        throw new RuntimeException("Update is not implemented as it is not used.");
    }

    private void processBeforeStoringShared(@NonNull FlowFile flowFileBeingStored) {
        boolean byDefaultWeSetFilesToBeActive;
        boolean flowFileUserIsNotSet;
        boolean needAddTimeForProperFlowPositioning;
        boolean needUpdateTimeForProperFlowPositioning = true;
        if (flowFileBeingStored.isActive() == null) {
            byDefaultWeSetFilesToBeActive = true;
        } else {
            byDefaultWeSetFilesToBeActive = false;
        }
        if (byDefaultWeSetFilesToBeActive) {
            flowFileBeingStored.setActive(Boolean.valueOf(true));
        }
        if (flowFileBeingStored.getUserId() == null || flowFileBeingStored.getUserId().intValue() <= 0) {
            flowFileUserIsNotSet = true;
        } else {
            flowFileUserIsNotSet = false;
        }
        if (flowFileUserIsNotSet) {
            flowFileBeingStored.setUserId(Integer.valueOf((int) getSession().getAuthenticatedUserID()));
        }
        if (flowFileBeingStored.getAddTime() == null) {
            needAddTimeForProperFlowPositioning = true;
        } else {
            needAddTimeForProperFlowPositioning = false;
        }
        if (needAddTimeForProperFlowPositioning) {
            flowFileBeingStored.setAddTime(PipedriveDateTime.instanceWithCurrentDateTime());
        }
        if (flowFileBeingStored.getUpdateTime() != null) {
            needUpdateTimeForProperFlowPositioning = false;
        }
        if (needUpdateTimeForProperFlowPositioning) {
            flowFileBeingStored.setUpdateTime(PipedriveDateTime.instanceWithCurrentDateTime());
        }
    }

    private void processCreateBeforeStoring(@NonNull FlowFile newFlowFileBeingStored) {
        boolean fileUploadStatusIsNotSet;
        if (newFlowFileBeingStored.getFileUploadStatus() == null) {
            fileUploadStatusIsNotSet = true;
        } else {
            fileUploadStatusIsNotSet = false;
        }
        if (fileUploadStatusIsNotSet) {
            newFlowFileBeingStored.setFileUploadStatus(Integer.valueOf(0));
        }
    }
}
