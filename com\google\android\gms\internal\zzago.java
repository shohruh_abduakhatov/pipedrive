package com.google.android.gms.internal;

import java.io.IOException;
import java.util.Arrays;

public final class zzago extends zzaru<zzago> {
    public zza[] aUN;

    public static final class zza extends zzaru<zza> {
        private static volatile zza[] aUO;
        public zza aUP;
        public String name;

        public static final class zza extends zzaru<zza> {
            private static volatile zza[] aUQ;
            public zza aUR;
            public int type;

            public static final class zza extends zzaru<zza> {
                public byte[] aUS;
                public String aUT;
                public double aUU;
                public float aUV;
                public long aUW;
                public int aUX;
                public int aUY;
                public boolean aUZ;
                public zza[] aVa;
                public zza[] aVb;
                public String[] aVc;
                public long[] aVd;
                public float[] aVe;
                public long aVf;

                public zza() {
                    zzcnf();
                }

                public boolean equals(Object obj) {
                    if (obj == this) {
                        return true;
                    }
                    if (!(obj instanceof zza)) {
                        return false;
                    }
                    zza com_google_android_gms_internal_zzago_zza_zza_zza = (zza) obj;
                    if (!Arrays.equals(this.aUS, com_google_android_gms_internal_zzago_zza_zza_zza.aUS)) {
                        return false;
                    }
                    if (this.aUT == null) {
                        if (com_google_android_gms_internal_zzago_zza_zza_zza.aUT != null) {
                            return false;
                        }
                    } else if (!this.aUT.equals(com_google_android_gms_internal_zzago_zza_zza_zza.aUT)) {
                        return false;
                    }
                    return (Double.doubleToLongBits(this.aUU) == Double.doubleToLongBits(com_google_android_gms_internal_zzago_zza_zza_zza.aUU) && Float.floatToIntBits(this.aUV) == Float.floatToIntBits(com_google_android_gms_internal_zzago_zza_zza_zza.aUV) && this.aUW == com_google_android_gms_internal_zzago_zza_zza_zza.aUW && this.aUX == com_google_android_gms_internal_zzago_zza_zza_zza.aUX && this.aUY == com_google_android_gms_internal_zzago_zza_zza_zza.aUY && this.aUZ == com_google_android_gms_internal_zzago_zza_zza_zza.aUZ && zzary.equals(this.aVa, com_google_android_gms_internal_zzago_zza_zza_zza.aVa) && zzary.equals(this.aVb, com_google_android_gms_internal_zzago_zza_zza_zza.aVb) && zzary.equals(this.aVc, com_google_android_gms_internal_zzago_zza_zza_zza.aVc) && zzary.equals(this.aVd, com_google_android_gms_internal_zzago_zza_zza_zza.aVd) && zzary.equals(this.aVe, com_google_android_gms_internal_zzago_zza_zza_zza.aVe) && this.aVf == com_google_android_gms_internal_zzago_zza_zza_zza.aVf) ? (this.btG == null || this.btG.isEmpty()) ? com_google_android_gms_internal_zzago_zza_zza_zza.btG == null || com_google_android_gms_internal_zzago_zza_zza_zza.btG.isEmpty() : this.btG.equals(com_google_android_gms_internal_zzago_zza_zza_zza.btG) : false;
                }

                public int hashCode() {
                    int i = 0;
                    int hashCode = (this.aUT == null ? 0 : this.aUT.hashCode()) + ((((getClass().getName().hashCode() + 527) * 31) + Arrays.hashCode(this.aUS)) * 31);
                    long doubleToLongBits = Double.doubleToLongBits(this.aUU);
                    hashCode = ((((((((((((((this.aUZ ? 1231 : 1237) + (((((((((((hashCode * 31) + ((int) (doubleToLongBits ^ (doubleToLongBits >>> 32)))) * 31) + Float.floatToIntBits(this.aUV)) * 31) + ((int) (this.aUW ^ (this.aUW >>> 32)))) * 31) + this.aUX) * 31) + this.aUY) * 31)) * 31) + zzary.hashCode(this.aVa)) * 31) + zzary.hashCode(this.aVb)) * 31) + zzary.hashCode(this.aVc)) * 31) + zzary.hashCode(this.aVd)) * 31) + zzary.hashCode(this.aVe)) * 31) + ((int) (this.aVf ^ (this.aVf >>> 32)))) * 31;
                    if (!(this.btG == null || this.btG.isEmpty())) {
                        i = this.btG.hashCode();
                    }
                    return hashCode + i;
                }

                public void zza(zzart com_google_android_gms_internal_zzart) throws IOException {
                    int i = 0;
                    if (!Arrays.equals(this.aUS, zzasd.btY)) {
                        com_google_android_gms_internal_zzart.zzb(1, this.aUS);
                    }
                    if (!(this.aUT == null || this.aUT.equals(""))) {
                        com_google_android_gms_internal_zzart.zzq(2, this.aUT);
                    }
                    if (Double.doubleToLongBits(this.aUU) != Double.doubleToLongBits(0.0d)) {
                        com_google_android_gms_internal_zzart.zza(3, this.aUU);
                    }
                    if (Float.floatToIntBits(this.aUV) != Float.floatToIntBits(0.0f)) {
                        com_google_android_gms_internal_zzart.zzc(4, this.aUV);
                    }
                    if (this.aUW != 0) {
                        com_google_android_gms_internal_zzart.zzb(5, this.aUW);
                    }
                    if (this.aUX != 0) {
                        com_google_android_gms_internal_zzart.zzaf(6, this.aUX);
                    }
                    if (this.aUY != 0) {
                        com_google_android_gms_internal_zzart.zzag(7, this.aUY);
                    }
                    if (this.aUZ) {
                        com_google_android_gms_internal_zzart.zzg(8, this.aUZ);
                    }
                    if (this.aVa != null && this.aVa.length > 0) {
                        for (zzasa com_google_android_gms_internal_zzasa : this.aVa) {
                            if (com_google_android_gms_internal_zzasa != null) {
                                com_google_android_gms_internal_zzart.zza(9, com_google_android_gms_internal_zzasa);
                            }
                        }
                    }
                    if (this.aVb != null && this.aVb.length > 0) {
                        for (zzasa com_google_android_gms_internal_zzasa2 : this.aVb) {
                            if (com_google_android_gms_internal_zzasa2 != null) {
                                com_google_android_gms_internal_zzart.zza(10, com_google_android_gms_internal_zzasa2);
                            }
                        }
                    }
                    if (this.aVc != null && this.aVc.length > 0) {
                        for (String str : this.aVc) {
                            if (str != null) {
                                com_google_android_gms_internal_zzart.zzq(11, str);
                            }
                        }
                    }
                    if (this.aVd != null && this.aVd.length > 0) {
                        for (long zzb : this.aVd) {
                            com_google_android_gms_internal_zzart.zzb(12, zzb);
                        }
                    }
                    if (this.aVf != 0) {
                        com_google_android_gms_internal_zzart.zzb(13, this.aVf);
                    }
                    if (this.aVe != null && this.aVe.length > 0) {
                        while (i < this.aVe.length) {
                            com_google_android_gms_internal_zzart.zzc(14, this.aVe[i]);
                            i++;
                        }
                    }
                    super.zza(com_google_android_gms_internal_zzart);
                }

                public /* synthetic */ zzasa zzb(zzars com_google_android_gms_internal_zzars) throws IOException {
                    return zzba(com_google_android_gms_internal_zzars);
                }

                public zza zzba(zzars com_google_android_gms_internal_zzars) throws IOException {
                    while (true) {
                        int bU = com_google_android_gms_internal_zzars.bU();
                        int zzc;
                        Object obj;
                        int zzagt;
                        switch (bU) {
                            case 0:
                                break;
                            case 10:
                                this.aUS = com_google_android_gms_internal_zzars.readBytes();
                                continue;
                            case 18:
                                this.aUT = com_google_android_gms_internal_zzars.readString();
                                continue;
                            case 25:
                                this.aUU = com_google_android_gms_internal_zzars.readDouble();
                                continue;
                            case 37:
                                this.aUV = com_google_android_gms_internal_zzars.readFloat();
                                continue;
                            case 40:
                                this.aUW = com_google_android_gms_internal_zzars.bX();
                                continue;
                            case 48:
                                this.aUX = com_google_android_gms_internal_zzars.bY();
                                continue;
                            case 56:
                                this.aUY = com_google_android_gms_internal_zzars.cb();
                                continue;
                            case 64:
                                this.aUZ = com_google_android_gms_internal_zzars.ca();
                                continue;
                            case 74:
                                zzc = zzasd.zzc(com_google_android_gms_internal_zzars, 74);
                                bU = this.aVa == null ? 0 : this.aVa.length;
                                obj = new zza[(zzc + bU)];
                                if (bU != 0) {
                                    System.arraycopy(this.aVa, 0, obj, 0, bU);
                                }
                                while (bU < obj.length - 1) {
                                    obj[bU] = new zza();
                                    com_google_android_gms_internal_zzars.zza(obj[bU]);
                                    com_google_android_gms_internal_zzars.bU();
                                    bU++;
                                }
                                obj[bU] = new zza();
                                com_google_android_gms_internal_zzars.zza(obj[bU]);
                                this.aVa = obj;
                                continue;
                            case 82:
                                zzc = zzasd.zzc(com_google_android_gms_internal_zzars, 82);
                                bU = this.aVb == null ? 0 : this.aVb.length;
                                obj = new zza[(zzc + bU)];
                                if (bU != 0) {
                                    System.arraycopy(this.aVb, 0, obj, 0, bU);
                                }
                                while (bU < obj.length - 1) {
                                    obj[bU] = new zza();
                                    com_google_android_gms_internal_zzars.zza(obj[bU]);
                                    com_google_android_gms_internal_zzars.bU();
                                    bU++;
                                }
                                obj[bU] = new zza();
                                com_google_android_gms_internal_zzars.zza(obj[bU]);
                                this.aVb = obj;
                                continue;
                            case 90:
                                zzc = zzasd.zzc(com_google_android_gms_internal_zzars, 90);
                                bU = this.aVc == null ? 0 : this.aVc.length;
                                obj = new String[(zzc + bU)];
                                if (bU != 0) {
                                    System.arraycopy(this.aVc, 0, obj, 0, bU);
                                }
                                while (bU < obj.length - 1) {
                                    obj[bU] = com_google_android_gms_internal_zzars.readString();
                                    com_google_android_gms_internal_zzars.bU();
                                    bU++;
                                }
                                obj[bU] = com_google_android_gms_internal_zzars.readString();
                                this.aVc = obj;
                                continue;
                            case 96:
                                zzc = zzasd.zzc(com_google_android_gms_internal_zzars, 96);
                                bU = this.aVd == null ? 0 : this.aVd.length;
                                obj = new long[(zzc + bU)];
                                if (bU != 0) {
                                    System.arraycopy(this.aVd, 0, obj, 0, bU);
                                }
                                while (bU < obj.length - 1) {
                                    obj[bU] = com_google_android_gms_internal_zzars.bX();
                                    com_google_android_gms_internal_zzars.bU();
                                    bU++;
                                }
                                obj[bU] = com_google_android_gms_internal_zzars.bX();
                                this.aVd = obj;
                                continue;
                            case 98:
                                zzagt = com_google_android_gms_internal_zzars.zzagt(com_google_android_gms_internal_zzars.cd());
                                zzc = com_google_android_gms_internal_zzars.getPosition();
                                bU = 0;
                                while (com_google_android_gms_internal_zzars.ci() > 0) {
                                    com_google_android_gms_internal_zzars.bX();
                                    bU++;
                                }
                                com_google_android_gms_internal_zzars.zzagv(zzc);
                                zzc = this.aVd == null ? 0 : this.aVd.length;
                                Object obj2 = new long[(bU + zzc)];
                                if (zzc != 0) {
                                    System.arraycopy(this.aVd, 0, obj2, 0, zzc);
                                }
                                while (zzc < obj2.length) {
                                    obj2[zzc] = com_google_android_gms_internal_zzars.bX();
                                    zzc++;
                                }
                                this.aVd = obj2;
                                com_google_android_gms_internal_zzars.zzagu(zzagt);
                                continue;
                            case 104:
                                this.aVf = com_google_android_gms_internal_zzars.bX();
                                continue;
                            case 114:
                                bU = com_google_android_gms_internal_zzars.cd();
                                zzc = com_google_android_gms_internal_zzars.zzagt(bU);
                                zzagt = bU / 4;
                                bU = this.aVe == null ? 0 : this.aVe.length;
                                Object obj3 = new float[(zzagt + bU)];
                                if (bU != 0) {
                                    System.arraycopy(this.aVe, 0, obj3, 0, bU);
                                }
                                while (bU < obj3.length) {
                                    obj3[bU] = com_google_android_gms_internal_zzars.readFloat();
                                    bU++;
                                }
                                this.aVe = obj3;
                                com_google_android_gms_internal_zzars.zzagu(zzc);
                                continue;
                            case 117:
                                zzc = zzasd.zzc(com_google_android_gms_internal_zzars, 117);
                                bU = this.aVe == null ? 0 : this.aVe.length;
                                obj = new float[(zzc + bU)];
                                if (bU != 0) {
                                    System.arraycopy(this.aVe, 0, obj, 0, bU);
                                }
                                while (bU < obj.length - 1) {
                                    obj[bU] = com_google_android_gms_internal_zzars.readFloat();
                                    com_google_android_gms_internal_zzars.bU();
                                    bU++;
                                }
                                obj[bU] = com_google_android_gms_internal_zzars.readFloat();
                                this.aVe = obj;
                                continue;
                            default:
                                if (!super.zza(com_google_android_gms_internal_zzars, bU)) {
                                    break;
                                }
                                continue;
                        }
                        return this;
                    }
                }

                public zza zzcnf() {
                    this.aUS = zzasd.btY;
                    this.aUT = "";
                    this.aUU = 0.0d;
                    this.aUV = 0.0f;
                    this.aUW = 0;
                    this.aUX = 0;
                    this.aUY = 0;
                    this.aUZ = false;
                    this.aVa = zza.zzcnb();
                    this.aVb = zza.zzcnd();
                    this.aVc = zzasd.btW;
                    this.aVd = zzasd.btS;
                    this.aVe = zzasd.btT;
                    this.aVf = 0;
                    this.btG = null;
                    this.btP = -1;
                    return this;
                }

                protected int zzx() {
                    int i;
                    int i2 = 0;
                    int zzx = super.zzx();
                    if (!Arrays.equals(this.aUS, zzasd.btY)) {
                        zzx += zzart.zzc(1, this.aUS);
                    }
                    if (!(this.aUT == null || this.aUT.equals(""))) {
                        zzx += zzart.zzr(2, this.aUT);
                    }
                    if (Double.doubleToLongBits(this.aUU) != Double.doubleToLongBits(0.0d)) {
                        zzx += zzart.zzb(3, this.aUU);
                    }
                    if (Float.floatToIntBits(this.aUV) != Float.floatToIntBits(0.0f)) {
                        zzx += zzart.zzd(4, this.aUV);
                    }
                    if (this.aUW != 0) {
                        zzx += zzart.zzf(5, this.aUW);
                    }
                    if (this.aUX != 0) {
                        zzx += zzart.zzah(6, this.aUX);
                    }
                    if (this.aUY != 0) {
                        zzx += zzart.zzai(7, this.aUY);
                    }
                    if (this.aUZ) {
                        zzx += zzart.zzh(8, this.aUZ);
                    }
                    if (this.aVa != null && this.aVa.length > 0) {
                        i = zzx;
                        for (zzasa com_google_android_gms_internal_zzasa : this.aVa) {
                            if (com_google_android_gms_internal_zzasa != null) {
                                i += zzart.zzc(9, com_google_android_gms_internal_zzasa);
                            }
                        }
                        zzx = i;
                    }
                    if (this.aVb != null && this.aVb.length > 0) {
                        i = zzx;
                        for (zzasa com_google_android_gms_internal_zzasa2 : this.aVb) {
                            if (com_google_android_gms_internal_zzasa2 != null) {
                                i += zzart.zzc(10, com_google_android_gms_internal_zzasa2);
                            }
                        }
                        zzx = i;
                    }
                    if (this.aVc != null && this.aVc.length > 0) {
                        int i3 = 0;
                        int i4 = 0;
                        for (String str : this.aVc) {
                            if (str != null) {
                                i4++;
                                i3 += zzart.zzuy(str);
                            }
                        }
                        zzx = (zzx + i3) + (i4 * 1);
                    }
                    if (this.aVd != null && this.aVd.length > 0) {
                        i = 0;
                        while (i2 < this.aVd.length) {
                            i += zzart.zzcz(this.aVd[i2]);
                            i2++;
                        }
                        zzx = (zzx + i) + (this.aVd.length * 1);
                    }
                    if (this.aVf != 0) {
                        zzx += zzart.zzf(13, this.aVf);
                    }
                    return (this.aVe == null || this.aVe.length <= 0) ? zzx : (zzx + (this.aVe.length * 4)) + (this.aVe.length * 1);
                }
            }

            public zza() {
                zzcne();
            }

            public static zza[] zzcnd() {
                if (aUQ == null) {
                    synchronized (zzary.btO) {
                        if (aUQ == null) {
                            aUQ = new zza[0];
                        }
                    }
                }
                return aUQ;
            }

            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof zza)) {
                    return false;
                }
                zza com_google_android_gms_internal_zzago_zza_zza = (zza) obj;
                if (this.type != com_google_android_gms_internal_zzago_zza_zza.type) {
                    return false;
                }
                if (this.aUR == null) {
                    if (com_google_android_gms_internal_zzago_zza_zza.aUR != null) {
                        return false;
                    }
                } else if (!this.aUR.equals(com_google_android_gms_internal_zzago_zza_zza.aUR)) {
                    return false;
                }
                return (this.btG == null || this.btG.isEmpty()) ? com_google_android_gms_internal_zzago_zza_zza.btG == null || com_google_android_gms_internal_zzago_zza_zza.btG.isEmpty() : this.btG.equals(com_google_android_gms_internal_zzago_zza_zza.btG);
            }

            public int hashCode() {
                int i = 0;
                int hashCode = ((this.aUR == null ? 0 : this.aUR.hashCode()) + ((((getClass().getName().hashCode() + 527) * 31) + this.type) * 31)) * 31;
                if (!(this.btG == null || this.btG.isEmpty())) {
                    i = this.btG.hashCode();
                }
                return hashCode + i;
            }

            public void zza(zzart com_google_android_gms_internal_zzart) throws IOException {
                com_google_android_gms_internal_zzart.zzaf(1, this.type);
                if (this.aUR != null) {
                    com_google_android_gms_internal_zzart.zza(2, this.aUR);
                }
                super.zza(com_google_android_gms_internal_zzart);
            }

            public zza zzaz(zzars com_google_android_gms_internal_zzars) throws IOException {
                while (true) {
                    int bU = com_google_android_gms_internal_zzars.bU();
                    switch (bU) {
                        case 0:
                            break;
                        case 8:
                            bU = com_google_android_gms_internal_zzars.bY();
                            switch (bU) {
                                case 1:
                                case 2:
                                case 3:
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                case 9:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                    this.type = bU;
                                    break;
                                default:
                                    continue;
                            }
                        case 18:
                            if (this.aUR == null) {
                                this.aUR = new zza();
                            }
                            com_google_android_gms_internal_zzars.zza(this.aUR);
                            continue;
                        default:
                            if (!super.zza(com_google_android_gms_internal_zzars, bU)) {
                                break;
                            }
                            continue;
                    }
                    return this;
                }
            }

            public /* synthetic */ zzasa zzb(zzars com_google_android_gms_internal_zzars) throws IOException {
                return zzaz(com_google_android_gms_internal_zzars);
            }

            public zza zzcne() {
                this.type = 1;
                this.aUR = null;
                this.btG = null;
                this.btP = -1;
                return this;
            }

            protected int zzx() {
                int zzx = super.zzx() + zzart.zzah(1, this.type);
                return this.aUR != null ? zzx + zzart.zzc(2, this.aUR) : zzx;
            }
        }

        public zza() {
            zzcnc();
        }

        public static zza[] zzcnb() {
            if (aUO == null) {
                synchronized (zzary.btO) {
                    if (aUO == null) {
                        aUO = new zza[0];
                    }
                }
            }
            return aUO;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zza)) {
                return false;
            }
            zza com_google_android_gms_internal_zzago_zza = (zza) obj;
            if (this.name == null) {
                if (com_google_android_gms_internal_zzago_zza.name != null) {
                    return false;
                }
            } else if (!this.name.equals(com_google_android_gms_internal_zzago_zza.name)) {
                return false;
            }
            if (this.aUP == null) {
                if (com_google_android_gms_internal_zzago_zza.aUP != null) {
                    return false;
                }
            } else if (!this.aUP.equals(com_google_android_gms_internal_zzago_zza.aUP)) {
                return false;
            }
            return (this.btG == null || this.btG.isEmpty()) ? com_google_android_gms_internal_zzago_zza.btG == null || com_google_android_gms_internal_zzago_zza.btG.isEmpty() : this.btG.equals(com_google_android_gms_internal_zzago_zza.btG);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.aUP == null ? 0 : this.aUP.hashCode()) + (((this.name == null ? 0 : this.name.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31;
            if (!(this.btG == null || this.btG.isEmpty())) {
                i = this.btG.hashCode();
            }
            return hashCode + i;
        }

        public void zza(zzart com_google_android_gms_internal_zzart) throws IOException {
            com_google_android_gms_internal_zzart.zzq(1, this.name);
            if (this.aUP != null) {
                com_google_android_gms_internal_zzart.zza(2, this.aUP);
            }
            super.zza(com_google_android_gms_internal_zzart);
        }

        public zza zzay(zzars com_google_android_gms_internal_zzars) throws IOException {
            while (true) {
                int bU = com_google_android_gms_internal_zzars.bU();
                switch (bU) {
                    case 0:
                        break;
                    case 10:
                        this.name = com_google_android_gms_internal_zzars.readString();
                        continue;
                    case 18:
                        if (this.aUP == null) {
                            this.aUP = new zza();
                        }
                        com_google_android_gms_internal_zzars.zza(this.aUP);
                        continue;
                    default:
                        if (!super.zza(com_google_android_gms_internal_zzars, bU)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }

        public /* synthetic */ zzasa zzb(zzars com_google_android_gms_internal_zzars) throws IOException {
            return zzay(com_google_android_gms_internal_zzars);
        }

        public zza zzcnc() {
            this.name = "";
            this.aUP = null;
            this.btG = null;
            this.btP = -1;
            return this;
        }

        protected int zzx() {
            int zzx = super.zzx() + zzart.zzr(1, this.name);
            return this.aUP != null ? zzx + zzart.zzc(2, this.aUP) : zzx;
        }
    }

    public zzago() {
        zzcna();
    }

    public static zzago zzar(byte[] bArr) throws zzarz {
        return (zzago) zzasa.zza(new zzago(), bArr);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzago)) {
            return false;
        }
        zzago com_google_android_gms_internal_zzago = (zzago) obj;
        return zzary.equals(this.aUN, com_google_android_gms_internal_zzago.aUN) ? (this.btG == null || this.btG.isEmpty()) ? com_google_android_gms_internal_zzago.btG == null || com_google_android_gms_internal_zzago.btG.isEmpty() : this.btG.equals(com_google_android_gms_internal_zzago.btG) : false;
    }

    public int hashCode() {
        int hashCode = (((getClass().getName().hashCode() + 527) * 31) + zzary.hashCode(this.aUN)) * 31;
        int hashCode2 = (this.btG == null || this.btG.isEmpty()) ? 0 : this.btG.hashCode();
        return hashCode2 + hashCode;
    }

    public void zza(zzart com_google_android_gms_internal_zzart) throws IOException {
        if (this.aUN != null && this.aUN.length > 0) {
            for (zzasa com_google_android_gms_internal_zzasa : this.aUN) {
                if (com_google_android_gms_internal_zzasa != null) {
                    com_google_android_gms_internal_zzart.zza(1, com_google_android_gms_internal_zzasa);
                }
            }
        }
        super.zza(com_google_android_gms_internal_zzart);
    }

    public zzago zzax(zzars com_google_android_gms_internal_zzars) throws IOException {
        while (true) {
            int bU = com_google_android_gms_internal_zzars.bU();
            switch (bU) {
                case 0:
                    break;
                case 10:
                    int zzc = zzasd.zzc(com_google_android_gms_internal_zzars, 10);
                    bU = this.aUN == null ? 0 : this.aUN.length;
                    Object obj = new zza[(zzc + bU)];
                    if (bU != 0) {
                        System.arraycopy(this.aUN, 0, obj, 0, bU);
                    }
                    while (bU < obj.length - 1) {
                        obj[bU] = new zza();
                        com_google_android_gms_internal_zzars.zza(obj[bU]);
                        com_google_android_gms_internal_zzars.bU();
                        bU++;
                    }
                    obj[bU] = new zza();
                    com_google_android_gms_internal_zzars.zza(obj[bU]);
                    this.aUN = obj;
                    continue;
                default:
                    if (!super.zza(com_google_android_gms_internal_zzars, bU)) {
                        break;
                    }
                    continue;
            }
            return this;
        }
    }

    public /* synthetic */ zzasa zzb(zzars com_google_android_gms_internal_zzars) throws IOException {
        return zzax(com_google_android_gms_internal_zzars);
    }

    public zzago zzcna() {
        this.aUN = zza.zzcnb();
        this.btG = null;
        this.btP = -1;
        return this;
    }

    protected int zzx() {
        int zzx = super.zzx();
        if (this.aUN != null && this.aUN.length > 0) {
            for (zzasa com_google_android_gms_internal_zzasa : this.aUN) {
                if (com_google_android_gms_internal_zzasa != null) {
                    zzx += zzart.zzc(1, com_google_android_gms_internal_zzasa);
                }
            }
        }
        return zzx;
    }
}
