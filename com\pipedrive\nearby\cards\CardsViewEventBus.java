package com.pipedrive.nearby.cards;

import android.support.annotation.NonNull;
import com.pipedrive.nearby.util.Irrelevant;
import rx.Observable;
import rx.subjects.Subject;

public enum CardsViewEventBus {
    INSTANCE;
    
    @NonNull
    private final Subject<Irrelevant, Irrelevant> hideEvents;
    @NonNull
    private Boolean isVisible;
    @NonNull
    private final Subject<Irrelevant, Irrelevant> showEvents;

    @NonNull
    public Observable<Irrelevant> showEvents() {
        return this.showEvents;
    }

    @NonNull
    public Observable<Irrelevant> hideEvents() {
        return this.hideEvents;
    }

    @NonNull
    public Boolean isVisible() {
        return this.isVisible;
    }

    public void onCardsViewHideCalled() {
        this.hideEvents.onNext(Irrelevant.INSTANCE);
        this.isVisible = Boolean.valueOf(false);
    }

    public void onCardsViewShowCalled() {
        this.showEvents.onNext(Irrelevant.INSTANCE);
        this.isVisible = Boolean.valueOf(true);
    }
}
