package com.pipedrive.sync;

final class SyncManager$Lazy {
    static final SyncManager instance = new SyncManager(null);

    private SyncManager$Lazy() {
    }
}
