package com.pipedrive.views.email.detail;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.pipedrive.FileOpenActivity;
import com.pipedrive.R;
import com.pipedrive.model.email.EmailAttachment;
import com.pipedrive.model.email.EmailMessage;
import java.util.List;

public class AttachmentListView extends ListView implements OnItemClickListener {
    private AttachmentListAdapter mAdapter;
    private EmailMessage mEmailMessage;
    private LayoutParams mLayoutParams;
    private int mOldItemCount;

    private static class AttachmentListAdapter extends BaseAdapter {
        private List<EmailAttachment> mItems;

        private static class ViewHolder {
            private TextView mName;
            private TextView mSize;

            public ViewHolder(View view) {
                this.mName = (TextView) view.findViewById(R.id.attachment_name);
                this.mSize = (TextView) view.findViewById(R.id.attachment_size);
                view.setTag(this);
            }
        }

        private AttachmentListAdapter(@NonNull List<EmailAttachment> items) {
            this.mItems = items;
        }

        public int getCount() {
            return this.mItems.size();
        }

        public EmailAttachment getItem(int position) {
            return (EmailAttachment) this.mItems.get(position);
        }

        public long getItemId(int position) {
            return Integer.valueOf(position).longValue();
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.attachment_list_item, null);
                holder = new ViewHolder(convertView);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            EmailAttachment attachment = getItem(position);
            holder.mName.setText(attachment.getCleanName());
            holder.mSize.setText(attachment.getHumanReadableFileSize());
            return convertView;
        }
    }

    public AttachmentListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void setEmailMessage(EmailMessage emailMessage) {
        this.mEmailMessage = emailMessage;
        init();
    }

    private void init() {
        if (!isInEditMode() && this.mEmailMessage != null && this.mEmailMessage.getAttachments() != null) {
            setDivider(new ColorDrawable(0));
            setDividerHeight((int) TypedValue.applyDimension(1, 4.0f, getResources().getDisplayMetrics()));
            this.mAdapter = new AttachmentListAdapter(this.mEmailMessage.getAttachments());
            setAdapter(this.mAdapter);
            setOnItemClickListener(this);
        }
    }

    protected void onDraw(Canvas canvas) {
        int i = 0;
        if (getCount() != this.mOldItemCount) {
            this.mOldItemCount = getCount();
            this.mLayoutParams = getLayoutParams();
            LayoutParams layoutParams = this.mLayoutParams;
            int count = getCount();
            if (this.mOldItemCount > 0) {
                i = getChildAt(0).getHeight();
            }
            layoutParams.height = (i * count) + (getDividerHeight() * (getCount() - 1));
            setLayoutParams(this.mLayoutParams);
        }
        super.onDraw(canvas);
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        FileOpenActivity.startActivityForEmailAttachment(getContext(), ((EmailAttachment) parent.getAdapter().getItem(position)).getSqlId());
    }
}
