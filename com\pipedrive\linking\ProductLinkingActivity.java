package com.pipedrive.linking;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import com.pipedrive.R;
import com.pipedrive.adapter.BaseSectionedListAdapter;
import com.pipedrive.adapter.ProductListAdapter;
import com.pipedrive.datasource.CurrenciesDataSource;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.products.ProductsDataSource;
import com.pipedrive.model.Deal;
import com.pipedrive.products.edit.DealProductCreateActivity;

public class ProductLinkingActivity extends BaseLinkingActivity {
    private static final String ARG_DEAL_SQL_ID = "ARG_DEAL_SQL_ID";
    private static final int REQUEST_CODE_CREATE_DEAL_PRODUCT = 0;

    public static void startActivity(@NonNull Activity context, @NonNull Long dealSqlId) {
        startActivity(context, dealSqlId, null);
    }

    public static void startActivity(@NonNull Activity activity, @NonNull Long dealSqlId, @Nullable Integer requestCode) {
        Intent intent = new Intent(activity, ProductLinkingActivity.class).putExtra(ARG_DEAL_SQL_ID, dealSqlId);
        if (requestCode == null) {
            ContextCompat.startActivity(activity, intent, null);
        } else {
            ActivityCompat.startActivityForResult(activity, intent, requestCode.intValue(), null);
        }
    }

    void onAddClicked() {
    }

    public int getSearchHint() {
        return R.string.search_products;
    }

    @Nullable
    public Drawable getFabIcon() {
        return null;
    }

    @Nullable
    public BaseSectionedListAdapter getAdapter() {
        Deal deal = (Deal) new DealsDataSource(getSession().getDatabase()).findBySqlId(Long.valueOf(getIntent().getExtras().getLong(ARG_DEAL_SQL_ID)).longValue());
        if (deal == null) {
            return null;
        }
        return new ProductListAdapter(this, getCursor(), getSession(), new CurrenciesDataSource(getSession().getDatabase()).getCurrencyByCode(deal.getCurrencyCode()));
    }

    @Nullable
    public Cursor getCursor() {
        return new ProductsDataSource(getSession().getDatabase()).getSearchCursor(this.mSearchConstraint);
    }

    @Nullable
    public String getKey() {
        return null;
    }

    void setSelectedItemSqlId(long selectedItemSqlId) {
        DealProductCreateActivity.startActivity(this, Long.valueOf(selectedItemSqlId), Long.valueOf(getIntent().getExtras().getLong(ARG_DEAL_SQL_ID)), Integer.valueOf(0));
    }

    protected boolean isFabVisible() {
        return false;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        setResult(resultCode);
        finish();
    }
}
