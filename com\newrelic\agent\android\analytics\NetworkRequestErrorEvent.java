package com.newrelic.agent.android.analytics;

import java.util.Set;

public class NetworkRequestErrorEvent extends AnalyticsEvent {
    public NetworkRequestErrorEvent() {
        super(null, AnalyticsEventCategory.RequestError);
    }

    public NetworkRequestErrorEvent(Set<AnalyticAttribute> attributeSet) {
        super(null, AnalyticsEventCategory.RequestError, AnalyticAttribute.EVENT_TYPE_ATTRIBUTE_MOBILE_REQUEST_ERROR, attributeSet);
    }
}
