package com.pipedrive.tasks;

import android.os.AsyncTask.Status;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.instrumentation.AsyncTaskInstrumentation;
import com.newrelic.agent.android.tracing.Trace;
import com.newrelic.agent.android.tracing.TraceMachine;
import com.pipedrive.application.Session;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

public abstract class AsyncTask<Params, Progress, Result> {
    private AsyncTaskAdapter mAsyncTask = null;
    @NonNull
    private Session mSession;

    public interface AsyncTaskExecutor {
        @MainThread
        @NonNull
        ExecutorService getAsyncTaskExecutor();

        @MainThread
        void registerAsyncTask(@NonNull AsyncTask asyncTask);
    }

    private class AsyncTaskAdapter extends android.os.AsyncTask<Params, Progress, Result> implements TraceFieldInterface {
        public Trace _nr_trace;

        public void _nr_setTrace(Trace trace) {
            try {
                this._nr_trace = trace;
            } catch (Exception e) {
            }
        }

        private AsyncTaskAdapter() {
        }

        protected Result doInBackground(Params... paramsArr) {
            try {
                TraceMachine.enterMethod(this._nr_trace, "AsyncTask$AsyncTaskAdapter#doInBackground", null);
            } catch (NoSuchFieldError e) {
                while (true) {
                    TraceMachine.enterMethod(null, "AsyncTask$AsyncTaskAdapter#doInBackground", null);
                }
            }
            if (AsyncTask.this.isSessionValid()) {
                Result doInBackground = AsyncTask.this.doInBackground(paramsArr);
                TraceMachine.exitMethod();
                TraceMachine.unloadTraceContext(this);
                return doInBackground;
            }
            cancel(true);
            TraceMachine.exitMethod();
            TraceMachine.unloadTraceContext(this);
            return null;
        }

        protected void onPostExecute(Result result) {
            try {
                TraceMachine.enterMethod(this._nr_trace, "AsyncTask$AsyncTaskAdapter#onPostExecute", null);
            } catch (NoSuchFieldError e) {
                while (true) {
                    TraceMachine.enterMethod(null, "AsyncTask$AsyncTaskAdapter#onPostExecute", null);
                }
            }
            AsyncTask.this.unregisterTask();
            if (AsyncTask.this.isSessionValid()) {
                AsyncTask.this.onPostExecute(result);
            }
            AsyncTask.this.releaseResources();
            TraceMachine.exitMethod();
        }

        protected void onProgressUpdate(Progress... values) {
            if (AsyncTask.this.isSessionValid()) {
                AsyncTask.this.onProgressUpdate(values);
            }
        }

        final void publishProgress_(Progress... values) {
            if (AsyncTask.this.isSessionValid()) {
                publishProgress(values);
            }
        }

        protected void onCancelled() {
            if (AsyncTask.this.isSessionValid()) {
                AsyncTask.this.onCancelled();
            }
            AsyncTask.this.unregisterTask();
            AsyncTask.this.releaseResources();
        }

        protected void onCancelled(Result result) {
            if (AsyncTask.this.isSessionValid()) {
                AsyncTask.this.onCancelled(result);
            }
            AsyncTask.this.unregisterTask();
            AsyncTask.this.releaseResources();
        }
    }

    protected abstract Result doInBackground(Params... paramsArr);

    public AsyncTask(@NonNull Session session) {
        this.mSession = session;
    }

    @NonNull
    public final Session getSession() {
        return this.mSession;
    }

    public final AsyncTask executeOnAsyncTaskExecutor(@NonNull AsyncTaskExecutor asyncTaskExecutor, Params... params) {
        AsyncTask asyncTask = executeOnExecutor(asyncTaskExecutor.getAsyncTaskExecutor(), params);
        asyncTaskExecutor.registerAsyncTask(asyncTask);
        return asyncTask;
    }

    public final AsyncTask executeOnExecutor(@Nullable Executor exec, Params... params) {
        boolean executorIsShutDown = exec != null && (exec instanceof ExecutorService) && ((ExecutorService) exec).isShutdown();
        if (!(executorIsShutDown || getSession().getRuntimeCache() == null)) {
            String taskID = taskID(getClass());
            ConcurrentMap<String, Integer> register = getSession().getRuntimeCache().runningAsyncTaskRegister;
            if (register.containsKey(taskID)) {
                register.replace(taskID, Integer.valueOf(((Integer) register.get(taskID)).intValue() + 1));
            } else {
                register.put(taskID, Integer.valueOf(1));
            }
            if (isSessionValid()) {
                onPreExecute();
                onPreExecute(params);
                this.mAsyncTask = new AsyncTaskAdapter();
                AsyncTaskAdapter asyncTaskAdapter;
                if (exec != null) {
                    asyncTaskAdapter = this.mAsyncTask;
                    if (asyncTaskAdapter instanceof android.os.AsyncTask) {
                        AsyncTaskInstrumentation.executeOnExecutor(asyncTaskAdapter, exec, params);
                    } else {
                        asyncTaskAdapter.executeOnExecutor(exec, params);
                    }
                } else {
                    asyncTaskAdapter = this.mAsyncTask;
                    if (asyncTaskAdapter instanceof android.os.AsyncTask) {
                        AsyncTaskInstrumentation.execute(asyncTaskAdapter, params);
                    } else {
                        asyncTaskAdapter.execute(params);
                    }
                }
            }
        }
        return this;
    }

    public AsyncTask execute(Params... params) {
        if (isSessionValid()) {
            executeOnExecutor(null, params);
        }
        return this;
    }

    public AsyncTask executeIfNotExecuting(Params... params) {
        return isExecuting() ? this : execute(params);
    }

    protected void onPreExecute(Params... paramsArr) {
    }

    protected void onPreExecute() {
    }

    protected void onPostExecute(Result result) {
    }

    protected void onProgressUpdate(Progress... progressArr) {
    }

    protected void onCancelled(Result result) {
        onCancelled();
    }

    protected void onCancelled() {
    }

    public final boolean isCancelled() {
        return this.mAsyncTask != null && this.mAsyncTask.isCancelled();
    }

    protected final void publishProgress(Progress... values) {
        if (this.mAsyncTask != null) {
            this.mAsyncTask.publishProgress_(values);
        }
    }

    public final boolean isExecuting() {
        return isExecuting(getSession(), getClass());
    }

    public final Status getStatus() {
        return this.mAsyncTask == null ? Status.PENDING : this.mAsyncTask.getStatus();
    }

    public static boolean isExecuting(Session session, Class<? extends AsyncTask> asyncTask) {
        if (session == null || session.getRuntimeCache() == null) {
            return false;
        }
        String taskID = taskID(asyncTask);
        ConcurrentMap<String, Integer> register = session.getRuntimeCache().runningAsyncTaskRegister;
        if (!register.containsKey(taskID)) {
            return false;
        }
        return ((Integer) register.get(taskID)).intValue() > 0;
    }

    private static String taskID(Class<? extends AsyncTask> asyncTask) {
        return asyncTask.getName();
    }

    private void releaseResources() {
        this.mSession = null;
    }

    private void unregisterTask() {
        String taskID = taskID(getClass());
        if (getSession().getRuntimeCache() != null) {
            ConcurrentMap<String, Integer> register = getSession().getRuntimeCache().runningAsyncTaskRegister;
            int counter = ((Integer) register.get(taskID)).intValue();
            if (counter > 0) {
                counter--;
            }
            register.replace(taskID, Integer.valueOf(counter));
        }
    }

    protected boolean isSessionValid() {
        return this.mSession.isValid();
    }

    public boolean cancel(boolean mayInterruptIfRunning) {
        return this.mAsyncTask != null && this.mAsyncTask.cancel(mayInterruptIfRunning);
    }
}
