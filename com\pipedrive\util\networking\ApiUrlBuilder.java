package com.pipedrive.util.networking;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.pipedrive.application.Session;
import com.pipedrive.model.pagination.Pagination;
import com.pipedrive.nearby.model.NearbyItemType;
import com.pipedrive.util.StringUtils;

public class ApiUrlBuilder extends ApiUrlBuilderWithoutSession {
    public static final String ENDPOINT_ACTIVITIES = "activities";
    public static final String ENDPOINT_ACTIVITY_TYPES = "activityTypes";
    static final String ENDPOINT_AGGREGATED_REQUESTS = "aggregatedRequests";
    public static final String ENDPOINT_CURRENCIES = "currencies";
    public static final String ENDPOINT_CURRENCIES_EXCHANGE_RATES = "currencies/exchangeRates";
    public static final String ENDPOINT_DEALS = "deals";
    public static final String ENDPOINT_DEAL_FIELDS = "dealFields";
    public static final String ENDPOINT_FILTERS = "filters";
    private static final String ENDPOINT_FLOW = "flow";
    public static final String ENDPOINT_NOTES = "notes";
    public static final String ENDPOINT_ORGANIZATIONS = "organizations";
    public static final String ENDPOINT_ORGANIZATION_FIELDS = "organizationFields";
    public static final String ENDPOINT_PERSONS = "persons";
    public static final String ENDPOINT_PERSON_FIELDS = "personFields";
    public static final String ENDPOINT_PIPELINES = "pipelines";
    public static final String ENDPOINT_PIPELINE_STAGES = "stages";
    public static final String ENDPOINT_PRODUCTS = "products";
    public static final String ENDPOINT_RECENTS = "recents";
    public static final String ENDPOINT_SEARCH_RESULTS = "searchResults";
    public static final String ENDPOINT_SELF = "users/self";
    public static final String ENDPOINT_USERS = "users";
    private static final String PARAM_API_TOKEN = "api_token";
    @NonNull
    private final Session mSession;

    public enum UrlTemplates {
        ;

        @NonNull
        public static ApiUrlBuilder createDealProduct(@NonNull Session session, @NonNull Long dealPipedriveId) {
            ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder(session, "deals");
            apiUrlBuilder.mBuilder.appendPath(String.valueOf(dealPipedriveId));
            apiUrlBuilder.mBuilder.appendPath("products");
            return apiUrlBuilder;
        }

        @NonNull
        public static ApiUrlBuilder updateDealProduct(@NonNull Session session, @NonNull Long dealPipedriveId, @NonNull Long dealProductPipedriveId) {
            ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder(session, "deals");
            apiUrlBuilder.mBuilder.appendPath(String.valueOf(dealPipedriveId));
            apiUrlBuilder.mBuilder.appendPath("products");
            apiUrlBuilder.mBuilder.appendPath(String.valueOf(dealProductPipedriveId));
            return apiUrlBuilder;
        }

        @NonNull
        public static ApiUrlBuilder deleteDealProduct(@NonNull Session session, @NonNull Long dealPipedriveId, @NonNull Long dealProductPipedriveId) {
            return updateDealProduct(session, dealPipedriveId, dealProductPipedriveId);
        }

        @NonNull
        public static ApiUrlBuilder listDealProducts(@NonNull Session session, @NonNull Long dealPipedriveId, @Nullable Long start, @Nullable Long limit) {
            ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder(session, "deals");
            apiUrlBuilder.mBuilder.appendPath(String.valueOf(dealPipedriveId));
            apiUrlBuilder.mBuilder.appendEncodedPath("products:(id,order_nr,product_id,product_variation_id,item_price,discount_percentage,duration,currency,active_flag,name,quantity,product:(id,name,active_flag,selectable,prices,product_variations))");
            apiUrlBuilder.pagination(start, limit);
            apiUrlBuilder.appendQueryParameter("include_product_data", "1");
            return apiUrlBuilder;
        }

        @NonNull
        private static ApiUrlBuilder getApiUrlBuilderForNearbyItemsRequest(@NonNull Session session, @NonNull NearbyItemType nearbyItemType) {
            String endpoint;
            if (nearbyItemType == NearbyItemType.DEALS) {
                endpoint = "deals";
            } else if (nearbyItemType == NearbyItemType.PERSONS) {
                endpoint = "persons";
            } else {
                endpoint = "organizations";
            }
            return new ApiUrlBuilder(session, endpoint).appendEncodedPath("nearby");
        }

        @NonNull
        public static ApiUrlBuilder listNearbyItems(@NonNull Session session, @NonNull NearbyItemType nearbyItemType, @NonNull LatLng currentLocation, @NonNull LatLngBounds viewport) {
            return getApiUrlBuilderForNearbyItemsRequest(session, nearbyItemType).param("lat_south", String.valueOf(viewport.southwest.latitude)).param("lat_north", String.valueOf(viewport.northeast.latitude)).param("long_west", String.valueOf(viewport.southwest.longitude)).param("long_east", String.valueOf(viewport.northeast.longitude)).param("lat", String.valueOf(currentLocation.latitude)).param("long", String.valueOf(currentLocation.longitude));
        }

        @NonNull
        public static ApiUrlBuilder requestStagesForPipelineId(@NonNull Session session, @NonNull Long pipelineId) {
            ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder(session, ApiUrlBuilder.ENDPOINT_PIPELINE_STAGES);
            apiUrlBuilder.param("pipeline_id", Long.toString(pipelineId.longValue()));
            return apiUrlBuilder;
        }
    }

    @NonNull
    public /* bridge */ /* synthetic */ String build() {
        return super.build();
    }

    public ApiUrlBuilder(@NonNull ApiUrlBuilder cloneFromApiUrlBuilder) {
        super((ApiUrlBuilderWithoutSession) cloneFromApiUrlBuilder);
        this.mSession = cloneFromApiUrlBuilder.getSession();
    }

    public ApiUrlBuilder(@NonNull Session session, @NonNull String requestApiEndpoint) {
        super(requestApiEndpoint);
        this.mSession = session;
        setApiToken(session.getApiToken());
    }

    private void setApiToken(String apiToken) {
        appendQueryParameterIfNotPresent(PARAM_API_TOKEN, apiToken);
    }

    @Nullable
    final String getApiUrlDomainLabel() {
        return this.mSession.getDomain(null);
    }

    @NonNull
    public Session getSession() {
        return this.mSession;
    }

    @NonNull
    public ApiUrlBuilder param(@Nullable String name, @Nullable Integer value) {
        if (!(name == null || TextUtils.isEmpty(name) || value == null)) {
            appendQueryParameter(name, value.toString());
        }
        return this;
    }

    @NonNull
    public ApiUrlBuilder param(@Nullable String name, @Nullable String value) {
        boolean weDoNotSupportEmptyParams = StringUtils.isTrimmedAndEmpty(name) || StringUtils.isTrimmedAndEmpty(value);
        if (!(weDoNotSupportEmptyParams || name == null)) {
            appendQueryParameterIfNotPresent(name, value);
        }
        return this;
    }

    @NonNull
    public ApiUrlBuilder pagination(@Nullable Long start, @Nullable Long limit) {
        setPaginationStart(start);
        setPaginationLimit(limit);
        return this;
    }

    private void setPaginationStart(@Nullable Long start) {
        if (start != null && start.longValue() > 0) {
            appendQueryParameterIfNotPresent(Pagination.JSON_PARAM_PAGINATION_START, String.valueOf(start));
        }
    }

    private void setPaginationLimit(@Nullable Long limit) {
        if (limit != null && limit.longValue() > 0) {
            appendQueryParameterIfNotPresent(Pagination.JSON_PARAM_PAGINATION_LIMIT, String.valueOf(limit));
        }
    }

    void updateUrlPaginationParametersToQueryNextPage(@NonNull Pagination pagination) {
        removeQueryParameter(Pagination.JSON_PARAM_PAGINATION_START);
        setPaginationStart(Long.valueOf((long) pagination.nextStart));
        removeQueryParameter(Pagination.JSON_PARAM_PAGINATION_LIMIT);
        setPaginationLimit(Long.valueOf((long) pagination.limit));
    }

    @NonNull
    public ApiUrlBuilder flow(Long id, Long start, Long limit) {
        if (!(id == null || id.longValue() <= 0 || this.mBuilder.build().getPath().contains(ENDPOINT_FLOW))) {
            this.mBuilder.appendPath(String.valueOf(id));
            this.mBuilder.appendPath(ENDPOINT_FLOW);
            pagination(start, limit);
        }
        return this;
    }

    @NonNull
    public ApiUrlBuilder dealsForStage(Long id, Long start, Long limit) {
        if (!(id == null || id.longValue() <= 0 || this.mBuilder.build().getPath().contains("deals"))) {
            this.mBuilder.appendPath(String.valueOf(id));
            this.mBuilder.appendPath("deals");
            pagination(start, limit);
        }
        return this;
    }

    @NonNull
    public ApiUrlBuilder appendEncodedPath(@NonNull String path) {
        this.mBuilder.appendEncodedPath(path);
        return this;
    }
}
