package com.pipedrive.tasks;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished.TaskResult;

public abstract class StoreAwareAsyncTaskWithCallback<Params, Progress> extends AsyncTaskWithCallback<Params, Progress> {
    protected abstract TaskResult doInBackgroundAfterStoreSyncWithCallback(Params... paramsArr);

    public StoreAwareAsyncTaskWithCallback(@NonNull Session session, @Nullable OnTaskFinished<Params> onTaskFinished) {
        super(session, onTaskFinished);
    }

    @SafeVarargs
    protected final TaskResult doInBackgroundWithCallback(Params... params) {
        try {
            getSession().getStoreSynchronizeManager().waitForOfflineChangesToSync();
            return doInBackgroundAfterStoreSyncWithCallback(params);
        } catch (Exception e) {
            return null;
        }
    }
}
