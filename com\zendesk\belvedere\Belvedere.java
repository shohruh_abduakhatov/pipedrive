package com.zendesk.belvedere;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import java.io.File;
import java.util.List;
import java.util.Locale;

public class Belvedere {
    private static final String LOG_TAG = "Belvedere";
    private final BelvedereStorage belvedereStorage;
    private final Context context;
    private final BelvedereImagePicker imagePicker;
    private final BelvedereLogger log;

    Belvedere(Context context, BelvedereConfig belvedereConfig) {
        this.context = context;
        this.belvedereStorage = new BelvedereStorage(belvedereConfig);
        this.imagePicker = new BelvedereImagePicker(belvedereConfig, this.belvedereStorage);
        this.log = belvedereConfig.getBelvedereLogger();
        this.log.d(LOG_TAG, "Belvedere initialized");
    }

    @NonNull
    public static BelvedereConfig$Builder from(@NonNull Context context) {
        if (context != null && context.getApplicationContext() != null) {
            return new BelvedereConfig$Builder(context.getApplicationContext());
        }
        throw new IllegalArgumentException("Invalid context provided");
    }

    @NonNull
    public List<BelvedereIntent> getBelvedereIntents() {
        return this.imagePicker.getBelvedereIntents(this.context);
    }

    public void showDialog(@NonNull FragmentManager fragmentManager) {
        BelvedereDialog.showDialog(fragmentManager, getBelvedereIntents());
    }

    public void getFilesFromActivityOnResult(int requestCode, int resultCode, Intent data, @NonNull BelvedereCallback<List<BelvedereResult>> callback) {
        this.imagePicker.getFilesFromActivityOnResult(this.context, requestCode, resultCode, data, callback);
    }

    @Nullable
    public BelvedereResult getFileRepresentation(@NonNull String fileName) {
        File file = this.belvedereStorage.getTempFileForRequestAttachment(this.context, fileName);
        this.log.d(LOG_TAG, String.format(Locale.US, "Get internal File: %s", new Object[]{file}));
        if (file != null) {
            Uri uri = this.belvedereStorage.getFileProviderUri(this.context, file);
            if (uri != null) {
                return new BelvedereResult(file, uri);
            }
        }
        return null;
    }

    public void grantPermissionsForUri(Intent intent, Uri uri) {
        this.log.d(LOG_TAG, String.format(Locale.US, "Grant Permission - Intent: %s - Uri: %s", new Object[]{intent, uri}));
        this.belvedereStorage.grantPermissionsForUri(this.context, intent, uri, 3);
    }

    public void revokePermissionsForUri(Uri uri) {
        this.log.d(LOG_TAG, String.format(Locale.US, "Revoke Permission - Uri: %s", new Object[]{uri}));
        this.belvedereStorage.revokePermissionsFromUri(this.context, uri, 3);
    }

    public boolean oneOrMoreSourceAvailable() {
        return this.imagePicker.oneOrMoreSourceAvailable(this.context);
    }

    public boolean isFunctionalityAvailable(@NonNull BelvedereSource source) {
        return this.imagePicker.isFunctionalityAvailable(source, this.context);
    }

    public void clear() {
        this.log.d(LOG_TAG, "Clear Belvedere cache");
        this.belvedereStorage.clearStorage(this.context);
    }
}
