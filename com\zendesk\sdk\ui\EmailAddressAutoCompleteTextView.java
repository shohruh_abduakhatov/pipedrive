package com.zendesk.sdk.ui;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Patterns;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.util.StringUtils;
import java.util.ArrayList;
import java.util.List;

public class EmailAddressAutoCompleteTextView extends AutoCompleteTextView {
    public EmailAddressAutoCompleteTextView(Context context) {
        this(context, null);
    }

    public EmailAddressAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialise();
    }

    public EmailAddressAutoCompleteTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initialise();
    }

    private void initialise() {
        int i = 0;
        if (-1 == getContext().checkCallingOrSelfPermission("android.permission.GET_ACCOUNTS") || isInEditMode()) {
            Logger.w(EmailAddressAutoCompleteTextView.class.getCanonicalName(), "You must declare the GET_ACCOUNTS permission to use the auto-complete feature", new Object[0]);
        } else {
            AccountManager accountManager = AccountManager.get(getContext());
            List<String> emailAddresses = new ArrayList();
            Account[] accounts = accountManager.getAccounts();
            int length = accounts.length;
            while (i < length) {
                Account account = accounts[i];
                if (isEmailValid(account.name) && !emailAddresses.contains(account.name)) {
                    emailAddresses.add(account.name);
                }
                i++;
            }
            if (emailAddresses.size() > 0) {
                setAdapter(new ArrayAdapter(getContext(), 17367050, emailAddresses));
            }
        }
        addTextChangedListener(new TextWatcherAdapter() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s == null) {
                    return;
                }
                if (EmailAddressAutoCompleteTextView.this.isEmailValid(s)) {
                    EmailAddressAutoCompleteTextView.this.setError(null);
                } else {
                    EmailAddressAutoCompleteTextView.this.setError(EmailAddressAutoCompleteTextView.this.getContext().getString(R.string.contact_fragment_email_validation_error));
                }
            }
        });
    }

    public boolean isInputValid() {
        String input = getText().toString();
        return StringUtils.hasLength(input) && isEmailValid(input);
    }

    private boolean isEmailValid(CharSequence email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
