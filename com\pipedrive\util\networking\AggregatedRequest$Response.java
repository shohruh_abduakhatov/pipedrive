package com.pipedrive.util.networking;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

public class AggregatedRequest$Response {
    @SerializedName("request")
    Request mRequest;
    @SerializedName("response")
    ResponseData mResponse;

    protected class Request {
        @SerializedName("method")
        String mMethod;
        @SerializedName("url")
        String mUrl;

        protected Request() {
        }

        public String toString() {
            return String.format("[url:%s; method:%s]", new Object[]{this.mUrl, this.mMethod});
        }
    }

    class ResponseData {
        @SerializedName("body")
        JsonObject mDataJSON;
        @SerializedName("statusCode")
        int mStatusCode;

        ResponseData() {
        }

        public String toString() {
            return String.format("[statusCode:%s; data:%s...]", new Object[]{Integer.valueOf(this.mStatusCode), this.mDataJSON.toString().substring(0, this.mDataJSON.toString().length() / 10)});
        }
    }

    int getResponseStatusCode() {
        return this.mResponse.mStatusCode;
    }

    JsonObject getResponseJsonData() {
        return this.mResponse.mDataJSON;
    }

    boolean hasResponseJsonData() {
        return getResponseJsonData() != null;
    }

    public String toString() {
        return String.format("[request:%s; response:%s]", new Object[]{this.mRequest, this.mResponse});
    }
}
