package com.pipedrive.model.contact;

import org.json.JSONObject;

public class Phone extends CommunicationMedium {
    public Phone(String phone, String label) {
        this(phone, label, false);
    }

    public Phone(String phone, String label, boolean isPrimary) {
        super(phone, label, isPrimary);
    }

    public Phone(JSONObject jsonObject) {
        super(jsonObject);
    }

    public final String getPhone() {
        return getValue();
    }
}
