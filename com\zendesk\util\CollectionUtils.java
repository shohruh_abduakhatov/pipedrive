package com.zendesk.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class CollectionUtils {
    private CollectionUtils() {
    }

    public static <T> boolean isEmpty(T[] array) {
        return array == null || array.length == 0;
    }

    public static <T> boolean isNotEmpty(T[] array) {
        return !isEmpty((Object[]) array);
    }

    public static <T> boolean isEmpty(Collection<T> collection) {
        return collection == null || collection.isEmpty();
    }

    public static <T> boolean isNotEmpty(Collection<T> collection) {
        return !isEmpty((Collection) collection);
    }

    public static <T> List<T> ensureEmpty(List<T> list) {
        if (isEmpty((Collection) list)) {
            return new ArrayList();
        }
        return list;
    }

    public static <T> List<T> unmodifiableList(List<T> list) {
        return Collections.unmodifiableList(ensureEmpty(list));
    }

    @SafeVarargs
    public static <T> List<T> combineLists(List<T>... lists) {
        if (lists == null || lists.length == 0) {
            return new ArrayList();
        }
        List<T> combinedLists = new ArrayList();
        Iterator it = new CopyOnWriteArrayList(lists).iterator();
        while (it.hasNext()) {
            Collection currentList = (List) it.next();
            if (isNotEmpty(currentList)) {
                Iterator i$ = new CopyOnWriteArrayList(currentList).iterator();
                while (i$.hasNext()) {
                    combinedLists.add(i$.next());
                }
            }
        }
        return combinedLists;
    }

    public static <T> List<T> copyOf(List<T> list) {
        if (list == null) {
            return new ArrayList();
        }
        CopyOnWriteArrayList<T> copyOnWriteArrayList = new CopyOnWriteArrayList(list);
        List<T> copiedList = new ArrayList(copyOnWriteArrayList.size());
        Iterator i$ = copyOnWriteArrayList.iterator();
        while (i$.hasNext()) {
            copiedList.add(i$.next());
        }
        return copiedList;
    }

    public static <K, V> Map<K, V> copyOf(Map<K, V> map) {
        if (map == null) {
            return new HashMap();
        }
        Map<K, V> sourceMap = Collections.synchronizedMap(map);
        Map<K, V> destinationMap = new HashMap();
        destinationMap.putAll(sourceMap);
        return destinationMap;
    }
}
