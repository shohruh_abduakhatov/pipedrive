package com.google.android.gms.internal;

import android.support.v4.media.TransportMediator;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

public final class zzagn {

    public static class zza {
        public final zzago aUL;
        public final List<Asset> aUM;

        public zza(zzago com_google_android_gms_internal_zzago, List<Asset> list) {
            this.aUL = com_google_android_gms_internal_zzago;
            this.aUM = list;
        }
    }

    private static int zza(String str, com.google.android.gms.internal.zzago.zza.zza[] com_google_android_gms_internal_zzago_zza_zzaArr) {
        int i = 14;
        for (com.google.android.gms.internal.zzago.zza.zza com_google_android_gms_internal_zzago_zza_zza : com_google_android_gms_internal_zzago_zza_zzaArr) {
            if (i == 14) {
                if (com_google_android_gms_internal_zzago_zza_zza.type == 9 || com_google_android_gms_internal_zzago_zza_zza.type == 2 || com_google_android_gms_internal_zzago_zza_zza.type == 6) {
                    i = com_google_android_gms_internal_zzago_zza_zza.type;
                } else if (com_google_android_gms_internal_zzago_zza_zza.type != 14) {
                    throw new IllegalArgumentException(new StringBuilder(String.valueOf(str).length() + 48).append("Unexpected TypedValue type: ").append(com_google_android_gms_internal_zzago_zza_zza.type).append(" for key ").append(str).toString());
                }
            } else if (com_google_android_gms_internal_zzago_zza_zza.type != i) {
                throw new IllegalArgumentException(new StringBuilder(String.valueOf(str).length() + TransportMediator.KEYCODE_MEDIA_PLAY).append("The ArrayList elements should all be the same type, but ArrayList with key ").append(str).append(" contains items of type ").append(i).append(" and ").append(com_google_android_gms_internal_zzago_zza_zza.type).toString());
            }
        }
        return i;
    }

    static int zza(List<Asset> list, Asset asset) {
        list.add(asset);
        return list.size() - 1;
    }

    public static zza zza(DataMap dataMap) {
        zzago com_google_android_gms_internal_zzago = new zzago();
        List arrayList = new ArrayList();
        com_google_android_gms_internal_zzago.aUN = zza(dataMap, arrayList);
        return new zza(com_google_android_gms_internal_zzago, arrayList);
    }

    private static com.google.android.gms.internal.zzago.zza.zza zza(List<Asset> list, Object obj) {
        com.google.android.gms.internal.zzago.zza.zza com_google_android_gms_internal_zzago_zza_zza = new com.google.android.gms.internal.zzago.zza.zza();
        if (obj == null) {
            com_google_android_gms_internal_zzago_zza_zza.type = 14;
            return com_google_android_gms_internal_zzago_zza_zza;
        }
        com_google_android_gms_internal_zzago_zza_zza.aUR = new com.google.android.gms.internal.zzago.zza.zza.zza();
        if (obj instanceof String) {
            com_google_android_gms_internal_zzago_zza_zza.type = 2;
            com_google_android_gms_internal_zzago_zza_zza.aUR.aUT = (String) obj;
        } else if (obj instanceof Integer) {
            com_google_android_gms_internal_zzago_zza_zza.type = 6;
            com_google_android_gms_internal_zzago_zza_zza.aUR.aUX = ((Integer) obj).intValue();
        } else if (obj instanceof Long) {
            com_google_android_gms_internal_zzago_zza_zza.type = 5;
            com_google_android_gms_internal_zzago_zza_zza.aUR.aUW = ((Long) obj).longValue();
        } else if (obj instanceof Double) {
            com_google_android_gms_internal_zzago_zza_zza.type = 3;
            com_google_android_gms_internal_zzago_zza_zza.aUR.aUU = ((Double) obj).doubleValue();
        } else if (obj instanceof Float) {
            com_google_android_gms_internal_zzago_zza_zza.type = 4;
            com_google_android_gms_internal_zzago_zza_zza.aUR.aUV = ((Float) obj).floatValue();
        } else if (obj instanceof Boolean) {
            com_google_android_gms_internal_zzago_zza_zza.type = 8;
            com_google_android_gms_internal_zzago_zza_zza.aUR.aUZ = ((Boolean) obj).booleanValue();
        } else if (obj instanceof Byte) {
            com_google_android_gms_internal_zzago_zza_zza.type = 7;
            com_google_android_gms_internal_zzago_zza_zza.aUR.aUY = ((Byte) obj).byteValue();
        } else if (obj instanceof byte[]) {
            com_google_android_gms_internal_zzago_zza_zza.type = 1;
            com_google_android_gms_internal_zzago_zza_zza.aUR.aUS = (byte[]) obj;
        } else if (obj instanceof String[]) {
            com_google_android_gms_internal_zzago_zza_zza.type = 11;
            com_google_android_gms_internal_zzago_zza_zza.aUR.aVc = (String[]) obj;
        } else if (obj instanceof long[]) {
            com_google_android_gms_internal_zzago_zza_zza.type = 12;
            com_google_android_gms_internal_zzago_zza_zza.aUR.aVd = (long[]) obj;
        } else if (obj instanceof float[]) {
            com_google_android_gms_internal_zzago_zza_zza.type = 15;
            com_google_android_gms_internal_zzago_zza_zza.aUR.aVe = (float[]) obj;
        } else if (obj instanceof Asset) {
            com_google_android_gms_internal_zzago_zza_zza.type = 13;
            com_google_android_gms_internal_zzago_zza_zza.aUR.aVf = (long) zza((List) list, (Asset) obj);
        } else if (obj instanceof DataMap) {
            com_google_android_gms_internal_zzago_zza_zza.type = 9;
            DataMap dataMap = (DataMap) obj;
            TreeSet treeSet = new TreeSet(dataMap.keySet());
            com.google.android.gms.internal.zzago.zza[] com_google_android_gms_internal_zzago_zzaArr = new com.google.android.gms.internal.zzago.zza[treeSet.size()];
            Iterator it = treeSet.iterator();
            r1 = 0;
            while (it.hasNext()) {
                r0 = (String) it.next();
                com_google_android_gms_internal_zzago_zzaArr[r1] = new com.google.android.gms.internal.zzago.zza();
                com_google_android_gms_internal_zzago_zzaArr[r1].name = r0;
                com_google_android_gms_internal_zzago_zzaArr[r1].aUP = zza((List) list, dataMap.get(r0));
                r1++;
            }
            com_google_android_gms_internal_zzago_zza_zza.aUR.aVa = com_google_android_gms_internal_zzago_zzaArr;
        } else if (obj instanceof ArrayList) {
            com_google_android_gms_internal_zzago_zza_zza.type = 10;
            ArrayList arrayList = (ArrayList) obj;
            com.google.android.gms.internal.zzago.zza.zza[] com_google_android_gms_internal_zzago_zza_zzaArr = new com.google.android.gms.internal.zzago.zza.zza[arrayList.size()];
            Object obj2 = null;
            int size = arrayList.size();
            int i = 0;
            int i2 = 14;
            while (i < size) {
                Object obj3 = arrayList.get(i);
                com.google.android.gms.internal.zzago.zza.zza zza = zza((List) list, obj3);
                if (zza.type == 14 || zza.type == 2 || zza.type == 6 || zza.type == 9) {
                    if (i2 == 14 && zza.type != 14) {
                        r1 = zza.type;
                    } else if (zza.type != i2) {
                        String valueOf = String.valueOf(obj2.getClass());
                        r0 = String.valueOf(obj3.getClass());
                        throw new IllegalArgumentException(new StringBuilder((String.valueOf(valueOf).length() + 80) + String.valueOf(r0).length()).append("ArrayList elements must all be of the sameclass, but this one contains a ").append(valueOf).append(" and a ").append(r0).toString());
                    } else {
                        obj3 = obj2;
                        r1 = i2;
                    }
                    com_google_android_gms_internal_zzago_zza_zzaArr[i] = zza;
                    i++;
                    i2 = r1;
                    obj2 = obj3;
                } else {
                    r0 = String.valueOf(obj3.getClass());
                    throw new IllegalArgumentException(new StringBuilder(String.valueOf(r0).length() + TransportMediator.KEYCODE_MEDIA_RECORD).append("The only ArrayList element types supported by DataBundleUtil are String, Integer, Bundle, and null, but this ArrayList contains a ").append(r0).toString());
                }
            }
            com_google_android_gms_internal_zzago_zza_zza.aUR.aVb = com_google_android_gms_internal_zzago_zza_zzaArr;
        } else {
            String str = "newFieldValueFromValue: unexpected value ";
            r0 = String.valueOf(obj.getClass().getSimpleName());
            throw new RuntimeException(r0.length() != 0 ? str.concat(r0) : new String(str));
        }
        return com_google_android_gms_internal_zzago_zza_zza;
    }

    public static DataMap zza(zza com_google_android_gms_internal_zzagn_zza) {
        DataMap dataMap = new DataMap();
        for (com.google.android.gms.internal.zzago.zza com_google_android_gms_internal_zzago_zza : com_google_android_gms_internal_zzagn_zza.aUL.aUN) {
            zza(com_google_android_gms_internal_zzagn_zza.aUM, dataMap, com_google_android_gms_internal_zzago_zza.name, com_google_android_gms_internal_zzago_zza.aUP);
        }
        return dataMap;
    }

    private static ArrayList zza(List<Asset> list, com.google.android.gms.internal.zzago.zza.zza.zza com_google_android_gms_internal_zzago_zza_zza_zza, int i) {
        ArrayList arrayList = new ArrayList(com_google_android_gms_internal_zzago_zza_zza_zza.aVb.length);
        for (com.google.android.gms.internal.zzago.zza.zza com_google_android_gms_internal_zzago_zza_zza : com_google_android_gms_internal_zzago_zza_zza_zza.aVb) {
            if (com_google_android_gms_internal_zzago_zza_zza.type == 14) {
                arrayList.add(null);
            } else if (i == 9) {
                DataMap dataMap = new DataMap();
                for (com.google.android.gms.internal.zzago.zza com_google_android_gms_internal_zzago_zza : com_google_android_gms_internal_zzago_zza_zza.aUR.aVa) {
                    zza(list, dataMap, com_google_android_gms_internal_zzago_zza.name, com_google_android_gms_internal_zzago_zza.aUP);
                }
                arrayList.add(dataMap);
            } else if (i == 2) {
                arrayList.add(com_google_android_gms_internal_zzago_zza_zza.aUR.aUT);
            } else if (i == 6) {
                arrayList.add(Integer.valueOf(com_google_android_gms_internal_zzago_zza_zza.aUR.aUX));
            } else {
                throw new IllegalArgumentException("Unexpected typeOfArrayList: " + i);
            }
        }
        return arrayList;
    }

    private static void zza(List<Asset> list, DataMap dataMap, String str, com.google.android.gms.internal.zzago.zza.zza com_google_android_gms_internal_zzago_zza_zza) {
        int i = com_google_android_gms_internal_zzago_zza_zza.type;
        if (i == 14) {
            dataMap.putString(str, null);
            return;
        }
        com.google.android.gms.internal.zzago.zza.zza.zza com_google_android_gms_internal_zzago_zza_zza_zza = com_google_android_gms_internal_zzago_zza_zza.aUR;
        if (i == 1) {
            dataMap.putByteArray(str, com_google_android_gms_internal_zzago_zza_zza_zza.aUS);
        } else if (i == 11) {
            dataMap.putStringArray(str, com_google_android_gms_internal_zzago_zza_zza_zza.aVc);
        } else if (i == 12) {
            dataMap.putLongArray(str, com_google_android_gms_internal_zzago_zza_zza_zza.aVd);
        } else if (i == 15) {
            dataMap.putFloatArray(str, com_google_android_gms_internal_zzago_zza_zza_zza.aVe);
        } else if (i == 2) {
            dataMap.putString(str, com_google_android_gms_internal_zzago_zza_zza_zza.aUT);
        } else if (i == 3) {
            dataMap.putDouble(str, com_google_android_gms_internal_zzago_zza_zza_zza.aUU);
        } else if (i == 4) {
            dataMap.putFloat(str, com_google_android_gms_internal_zzago_zza_zza_zza.aUV);
        } else if (i == 5) {
            dataMap.putLong(str, com_google_android_gms_internal_zzago_zza_zza_zza.aUW);
        } else if (i == 6) {
            dataMap.putInt(str, com_google_android_gms_internal_zzago_zza_zza_zza.aUX);
        } else if (i == 7) {
            dataMap.putByte(str, (byte) com_google_android_gms_internal_zzago_zza_zza_zza.aUY);
        } else if (i == 8) {
            dataMap.putBoolean(str, com_google_android_gms_internal_zzago_zza_zza_zza.aUZ);
        } else if (i == 13) {
            if (list == null) {
                String str2 = "populateBundle: unexpected type for: ";
                String valueOf = String.valueOf(str);
                throw new RuntimeException(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            }
            dataMap.putAsset(str, (Asset) list.get((int) com_google_android_gms_internal_zzago_zza_zza_zza.aVf));
        } else if (i == 9) {
            DataMap dataMap2 = new DataMap();
            for (com.google.android.gms.internal.zzago.zza com_google_android_gms_internal_zzago_zza : com_google_android_gms_internal_zzago_zza_zza_zza.aVa) {
                zza(list, dataMap2, com_google_android_gms_internal_zzago_zza.name, com_google_android_gms_internal_zzago_zza.aUP);
            }
            dataMap.putDataMap(str, dataMap2);
        } else if (i == 10) {
            i = zza(str, com_google_android_gms_internal_zzago_zza_zza_zza.aVb);
            ArrayList zza = zza(list, com_google_android_gms_internal_zzago_zza_zza_zza, i);
            if (i == 14) {
                dataMap.putStringArrayList(str, zza);
            } else if (i == 9) {
                dataMap.putDataMapArrayList(str, zza);
            } else if (i == 2) {
                dataMap.putStringArrayList(str, zza);
            } else if (i == 6) {
                dataMap.putIntegerArrayList(str, zza);
            } else {
                throw new IllegalStateException("Unexpected typeOfArrayList: " + i);
            }
        } else {
            throw new RuntimeException("populateBundle: unexpected type " + i);
        }
    }

    private static com.google.android.gms.internal.zzago.zza[] zza(DataMap dataMap, List<Asset> list) {
        TreeSet treeSet = new TreeSet(dataMap.keySet());
        com.google.android.gms.internal.zzago.zza[] com_google_android_gms_internal_zzago_zzaArr = new com.google.android.gms.internal.zzago.zza[treeSet.size()];
        Iterator it = treeSet.iterator();
        int i = 0;
        while (it.hasNext()) {
            String str = (String) it.next();
            Object obj = dataMap.get(str);
            com_google_android_gms_internal_zzago_zzaArr[i] = new com.google.android.gms.internal.zzago.zza();
            com_google_android_gms_internal_zzago_zzaArr[i].name = str;
            com_google_android_gms_internal_zzago_zzaArr[i].aUP = zza((List) list, obj);
            i++;
        }
        return com_google_android_gms_internal_zzago_zzaArr;
    }
}
