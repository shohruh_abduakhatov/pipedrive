package com.zendesk.sdk.requests;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.sdk.model.request.Request;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.ui.ListRowView;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

class RequestsAdapter extends ArrayAdapter<Request> {

    private static class RequestRow extends RelativeLayout implements ListRowView<Request> {
        private static final String CREATED_AT_DATE_FORMAT = "dd MMMM yyy";
        private static final String LOG_TAG = "RequestRow";
        private final Context context;
        private TextView date;
        private TextView description;
        private ImageView unreadIndicator;

        public RequestRow(Context context) {
            super(context);
            this.context = context;
            initialise();
        }

        private void initialise() {
            View container = LayoutInflater.from(this.context).inflate(R.layout.row_request, this);
            if (container != null) {
                this.description = (TextView) container.findViewById(R.id.row_request_description);
                this.date = (TextView) container.findViewById(R.id.row_request_date);
                this.unreadIndicator = (ImageView) container.findViewById(R.id.row_request_unread_indicator);
            }
        }

        public void bind(Request request) {
            if (request == null) {
                Logger.w(LOG_TAG, "request is null, nothing to bind.", new Object[0]);
                return;
            }
            if (this.description != null) {
                this.description.setText(request.getDescription());
            }
            if (!(this.date == null || request.getCreatedAt() == null)) {
                this.date.setText(new SimpleDateFormat(CREATED_AT_DATE_FORMAT, Locale.getDefault()).format(request.getUpdatedAt()));
            }
            if (this.unreadIndicator != null) {
                Integer storedCommentCount;
                if (request.getId() == null) {
                    storedCommentCount = null;
                } else {
                    storedCommentCount = ZendeskConfig.INSTANCE.storage().requestStorage().getCommentCount(request.getId());
                }
                if (storedCommentCount == null || request.getCommentCount() == null) {
                    setUnreadIndicator(true);
                } else if (storedCommentCount.intValue() < request.getCommentCount().intValue()) {
                    setUnreadIndicator(true);
                } else {
                    setUnreadIndicator(false);
                }
            }
        }

        public View getView() {
            return this;
        }

        public void setUnreadIndicator(boolean isUnread) {
            if (this.unreadIndicator == null) {
                return;
            }
            if (isUnread) {
                this.unreadIndicator.setVisibility(0);
            } else {
                this.unreadIndicator.setVisibility(4);
            }
        }
    }

    public RequestsAdapter(Context context, List<Request> requests) {
        super(context, R.layout.row_request, requests);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        RequestRow requestRowView;
        if (convertView instanceof RequestRow) {
            requestRowView = (RequestRow) convertView;
        } else {
            requestRowView = new RequestRow(getContext());
        }
        requestRowView.bind((Request) getItem(position));
        return requestRowView.getView();
    }
}
