package com.zendesk.sdk.model.request;

public class CustomField {
    private Long id;
    private String value;

    public CustomField(Long id, String value) {
        this.id = id;
        this.value = value;
    }
}
