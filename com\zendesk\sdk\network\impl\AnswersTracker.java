package com.zendesk.sdk.network.impl;

import com.crashlytics.android.answers.shim.AnswersOptionalLogger;
import com.crashlytics.android.answers.shim.KitEvent;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.network.ZendeskTracker;
import com.zendesk.util.StringUtils;

class AnswersTracker implements ZendeskTracker {
    private static final String LOG_TAG = "AnswersTracker";

    AnswersTracker() {
    }

    public void helpCenterLoaded() {
        Logger.d(LOG_TAG, "helpCenterLoaded", new Object[0]);
        AnswersOptionalLogger.get().logKitEvent(new KitEvent("help-center-fetched"));
    }

    public void helpCenterSearched(String query) {
        Logger.d(LOG_TAG, "helpCenterSearched", new Object[0]);
        KitEvent event = new KitEvent("help-center-search");
        String str = "search-term";
        if (StringUtils.isEmpty(query)) {
            query = "";
        }
        event.putAttribute(str, query);
        AnswersOptionalLogger.get().logKitEvent(event);
    }

    public void helpCenterArticleViewed() {
        Logger.d(LOG_TAG, "helpCenterArticleViewed", new Object[0]);
        AnswersOptionalLogger.get().logKitEvent(new KitEvent("help-center-article-viewed"));
    }

    public void requestCreated() {
        Logger.d(LOG_TAG, "requestCreated", new Object[0]);
        AnswersOptionalLogger.get().logKitEvent(new KitEvent("request-created"));
    }

    public void requestUpdated() {
        Logger.d(LOG_TAG, "requestUpdated", new Object[0]);
        AnswersOptionalLogger.get().logKitEvent(new KitEvent("request-updated"));
    }

    public void rateMyAppRated() {
        Logger.d(LOG_TAG, "rateMyAppRated", new Object[0]);
        AnswersOptionalLogger.get().logKitEvent(new KitEvent("rma-rate-app"));
    }

    public void rateMyAppFeedbackSent() {
        Logger.d(LOG_TAG, "rateMyAppFeedbackSent", new Object[0]);
        AnswersOptionalLogger.get().logKitEvent(new KitEvent("rma-feedback-sent"));
    }
}
