package com.zendesk.sdk.model.settings;

import android.support.annotation.Nullable;

public class AccountSettings {
    private AttachmentSettings attachments;

    @Nullable
    public AttachmentSettings getAttachmentSettings() {
        return this.attachments;
    }
}
