package com.zendesk.sdk.model.request;

import android.support.annotation.Nullable;

public class UpdateRequestWrapper {
    private Request request;

    public void setRequest(Request request) {
        this.request = request;
    }

    @Nullable
    public Request getRequest() {
        return this.request;
    }
}
