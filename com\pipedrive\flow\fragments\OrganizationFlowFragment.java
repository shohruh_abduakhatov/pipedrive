package com.pipedrive.flow.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.pipedrive.application.Session;
import com.pipedrive.flow.views.FlowView;
import com.pipedrive.flow.views.OrganizationFlowView;

public class OrganizationFlowFragment extends FlowFragment {
    private static final String KEY_ORGANIZATION_SQL_ID = "ORGANIZATION_SQL_ID";

    public /* bridge */ /* synthetic */ void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    public /* bridge */ /* synthetic */ View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return super.onCreateView(layoutInflater, viewGroup, bundle);
    }

    public /* bridge */ /* synthetic */ void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
    }

    public /* bridge */ /* synthetic */ void refreshContent(@NonNull Session session) {
        super.refreshContent(session);
    }

    public static OrganizationFlowFragment newInstance(@NonNull Long organizationSqlId) {
        Bundle args = new Bundle();
        args.putSerializable("ORGANIZATION_SQL_ID", organizationSqlId);
        OrganizationFlowFragment fragment = new OrganizationFlowFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    protected Long getSqlId() {
        return (Long) getArguments().getSerializable("ORGANIZATION_SQL_ID");
    }

    @NonNull
    protected FlowView createFlowView(@NonNull Context context) {
        return new OrganizationFlowView(context);
    }
}
