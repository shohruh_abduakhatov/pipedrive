package com.pipedrive.note;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.pipedrive.BaseActivity;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.model.HtmlContent;
import com.pipedrive.navigator.Navigable;
import com.pipedrive.navigator.Navigator;
import com.pipedrive.util.StringUtils;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.util.ViewUtil.OnKeyboardVisibilityListener;

public abstract class HtmlEditorActivity<PRESENTER extends HtmlEditorPresenter> extends BaseActivity implements HtmlEditorView, Navigable {
    @BindView(2131820718)
    TextView mMetaInfoView;
    @BindColor(2131755182)
    int mNoteColor;
    @BindView(2131820717)
    NoteEditView mNoteEditView;
    @NonNull
    protected PRESENTER mPresenter;
    @BindView(2131820716)
    ViewGroup mRootView;
    @BindView(2131820700)
    Toolbar mToolbar;

    protected abstract PRESENTER instantiatePresenter(@NonNull Session session, @Nullable Bundle bundle);

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_add_edit_note);
        ButterKnife.bind((Activity) this);
        final GestureDetector gestureDetector = new GestureDetector(this, new SimpleOnGestureListener() {
            public boolean onSingleTapConfirmed(MotionEvent e) {
                if (HtmlEditorActivity.this.mNoteEditView.isViewable().booleanValue()) {
                    HtmlEditorActivity.this.openKeyboard();
                }
                return true;
            }
        });
        this.mNoteEditView.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });
        setSupportActionBar(this.mToolbar);
        this.mPresenter = instantiatePresenter(getSession(), savedInstanceState);
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.mPresenter.setHtmlContentForStorage(getNoteContentAsHtml());
        this.mPresenter.onSaveInstanceState(outState);
    }

    public void onResume() {
        super.onResume();
        this.mPresenter.bindView(this);
    }

    protected void onPause() {
        super.onPause();
        this.mPresenter.unbindView();
    }

    public void onDataRequested(@Nullable HtmlContent htmlContent) {
        if (htmlContent != null) {
            fillContent(htmlContent);
            fillNoteMetaInfo(htmlContent);
        }
    }

    @Nullable
    protected Navigator getNavigatorImplementation() {
        return new Navigator(this, this.mToolbar);
    }

    public boolean isChangesMadeToFieldsAfterInitialLoad() {
        return !this.mPresenter.hashCodeEquals(this.mNoteEditView.getNoteHashCode()).booleanValue();
    }

    public boolean isAllRequiredFieldsSet() {
        return true;
    }

    private void fillNoteMetaInfo(@NonNull HtmlContent htmlContent) {
        this.mMetaInfoView.setText(htmlContent.getMetaInfo(getSession(), getString(R.string.subtitle_separator)));
    }

    private void fillContent(@NonNull HtmlContent htmlContent) {
        this.mNoteEditView.setTextAsHtmlContent(htmlContent);
        this.mPresenter.setOriginalNoteHashCode(this.mNoteEditView.getNoteHashCode());
        if (StringUtils.isTrimmedAndEmpty(htmlContent.getHtmlContent())) {
            openKeyboard();
        } else {
            enableSeparatedViewAndEditModes();
        }
    }

    private void enableSeparatedViewAndEditModes() {
        this.mNoteEditView.setViewable();
        ViewUtil.setKeyboardVisibilityListener(this, this.mRootView, new OnKeyboardVisibilityListener() {
            public void onVisibilityChanged(int visibility) {
                switch (visibility) {
                    case 1:
                        HtmlEditorActivity.this.mNoteEditView.setChangeable();
                        return;
                    case 2:
                        HtmlEditorActivity.this.mNoteEditView.setViewable();
                        return;
                    default:
                        return;
                }
            }
        });
    }

    private void openKeyboard() {
        this.mNoteEditView.setChangeable();
        ((InputMethodManager) getSystemService("input_method")).showSoftInput(this.mNoteEditView, 0);
    }

    @NonNull
    final String getNoteContentAsHtml() {
        return Html.toHtml(this.mNoteEditView.getTextAsSpanned()).replaceAll("\\r?\\n", "");
    }
}
