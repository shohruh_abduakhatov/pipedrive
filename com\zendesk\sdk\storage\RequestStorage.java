package com.zendesk.sdk.storage;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.sdk.storage.SdkStorage.UserStorage;
import java.util.List;

public interface RequestStorage extends UserStorage {
    @Nullable
    Integer getCommentCount(@NonNull String str);

    @NonNull
    List<String> getStoredRequestIds();

    void setCommentCount(@NonNull String str, int i);

    void storeRequestId(@NonNull String str);
}
