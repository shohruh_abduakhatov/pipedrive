package com.zendesk.sdk.network.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.request.CustomField;
import com.zendesk.sdk.network.SdkOptions;
import com.zendesk.sdk.storage.StorageModule;
import com.zendesk.sdk.util.LibraryModule;
import com.zendesk.sdk.util.ScopeCache;
import com.zendesk.sdk.util.StageDetectionUtil;
import com.zendesk.util.CollectionUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ApplicationScope {
    private static final String LOG_TAG = "ApplicationScope";
    private final String appId;
    private final Context applicationContext;
    private final boolean coppaEnabled;
    private final List<CustomField> customFields;
    private final boolean developmentMode;
    private ScopeCache<LibraryModule> libraryModuleCache;
    private final Locale locale;
    private final String oAuthToken;
    private ScopeCache<RestAdapterModule> restAdapterCache;
    private final SdkOptions sdkOptions;
    private ScopeCache<StorageModule> storageModuleCache;
    private final Long ticketFormId;
    private final String url;
    private final String userAgentHeader;

    static class Builder {
        private final String appId;
        private final Context applicationContext;
        private boolean coppaEnabled;
        private List<CustomField> customFields;
        private final boolean developmentMode;
        private Locale locale;
        private final String oAuthToken;
        private SdkOptions sdkOptions;
        private Long ticketFormId;
        private final String url;
        private String userAgentHeader;

        Builder(Context context, String url, String appId, String oAuth) {
            this.applicationContext = context;
            this.url = url;
            this.appId = appId;
            this.oAuthToken = oAuth;
            boolean devMode = true;
            try {
                devMode = StageDetectionUtil.isDebug(this.applicationContext);
            } catch (Exception e) {
                Logger.d(ApplicationScope.LOG_TAG, "Unable to detect stage", new Object[0]);
            }
            this.developmentMode = devMode;
            this.locale = Locale.getDefault();
            this.coppaEnabled = false;
            this.sdkOptions = new DefaultSdkOptions();
            this.ticketFormId = null;
            this.customFields = new ArrayList();
            this.userAgentHeader = "";
        }

        Builder(ApplicationScope applicationScope) {
            this.applicationContext = applicationScope.getApplicationContext();
            this.url = applicationScope.getUrl();
            this.appId = applicationScope.getAppId();
            this.oAuthToken = applicationScope.getOAuthToken();
            this.developmentMode = applicationScope.isDevelopmentMode();
            this.locale = applicationScope.getLocale();
            this.coppaEnabled = applicationScope.coppaEnabled;
            this.sdkOptions = applicationScope.getSdkOptions();
            this.ticketFormId = applicationScope.getTicketFormId();
            this.customFields = applicationScope.getCustomFields();
            this.userAgentHeader = applicationScope.getUserAgentHeader();
        }

        @NonNull
        Builder ticketFormId(@NonNull Long ticketFormId) {
            this.ticketFormId = ticketFormId;
            return this;
        }

        @NonNull
        Builder customFields(@NonNull List<CustomField> customFields) {
            this.customFields = CollectionUtils.ensureEmpty(customFields);
            return this;
        }

        @NonNull
        Builder locale(@NonNull Locale locale) {
            this.locale = locale;
            return this;
        }

        @NonNull
        Builder coppa(boolean coppaEnabled) {
            this.coppaEnabled = coppaEnabled;
            return this;
        }

        @NonNull
        Builder sdkOptions(@NonNull SdkOptions sdkOptions) {
            this.sdkOptions = sdkOptions;
            return this;
        }

        @NonNull
        Builder userAgentHeader(@NonNull String userAgentHeader) {
            this.userAgentHeader = userAgentHeader;
            return this;
        }

        @NonNull
        ApplicationScope build() {
            return new ApplicationScope();
        }
    }

    private ApplicationScope(Builder builder) {
        this.libraryModuleCache = new ScopeCache();
        this.restAdapterCache = new ScopeCache();
        this.storageModuleCache = new ScopeCache();
        this.applicationContext = builder.applicationContext;
        this.url = builder.url;
        this.appId = builder.appId;
        this.oAuthToken = builder.oAuthToken;
        this.locale = builder.locale;
        this.coppaEnabled = builder.coppaEnabled;
        this.developmentMode = builder.developmentMode;
        this.sdkOptions = builder.sdkOptions;
        this.ticketFormId = builder.ticketFormId;
        this.customFields = builder.customFields;
        this.userAgentHeader = builder.userAgentHeader;
    }

    @NonNull
    public Context getApplicationContext() {
        return this.applicationContext;
    }

    @NonNull
    public String getUrl() {
        return this.url;
    }

    @NonNull
    public String getAppId() {
        return this.appId;
    }

    @NonNull
    public String getOAuthToken() {
        return this.oAuthToken;
    }

    @NonNull
    public Locale getLocale() {
        return this.locale;
    }

    public boolean isCoppaEnabled() {
        return this.coppaEnabled;
    }

    public boolean isDevelopmentMode() {
        return this.developmentMode;
    }

    @NonNull
    public SdkOptions getSdkOptions() {
        return this.sdkOptions;
    }

    @Nullable
    public Long getTicketFormId() {
        return this.ticketFormId;
    }

    @NonNull
    public List<CustomField> getCustomFields() {
        return CollectionUtils.copyOf(this.customFields);
    }

    @NonNull
    public String getUserAgentHeader() {
        return this.userAgentHeader;
    }

    @NonNull
    public Builder newBuilder() {
        return new Builder(this);
    }

    public ScopeCache<LibraryModule> getLibraryModuleCache() {
        return this.libraryModuleCache;
    }

    public ScopeCache<RestAdapterModule> getRestAdapterCache() {
        return this.restAdapterCache;
    }

    public ScopeCache<StorageModule> getStorageModuleCache() {
        return this.storageModuleCache;
    }
}
