package com.pipedrive.changes;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.changes.Change.ChangeOperationType;
import com.pipedrive.datasource.NotesDataSource;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.notes.Note;

public class NoteChanger extends Changer<Note> {
    public NoteChanger(Session session) {
        super(session, 50, null);
    }

    protected NoteChanger(Changer parentChanger) {
        super(parentChanger.getSession(), 50, parentChanger);
    }

    @Nullable
    protected Long getChangeMetaData(@NonNull Note note, @NonNull ChangeOperationType changeOperationType) {
        return Long.valueOf(note.getSqlId());
    }

    @NonNull
    protected ChangeResult createChange(@NonNull Note newNote) {
        if (newNote.isExisting() || newNote.isStored()) {
            LogJourno.reportEvent(EVENT.ChangesChanger_createChangeRequestedWithExistingChange, NoteChanger.class.getSimpleName());
            Log.e(new Throwable("I can only handle new Notes!"));
            return ChangeResult.FAILED;
        }
        createAndRelateNoteAssociations(newNote);
        long newNoteSqlId = new NotesDataSource(getSession().getDatabase()).createOrUpdate((BaseDatasourceEntity) newNote);
        if (newNoteSqlId == -1) {
            return ChangeResult.FAILED;
        }
        newNote.setSqlId(newNoteSqlId);
        return ChangeResult.SUCCESSFUL;
    }

    @NonNull
    protected ChangeResult updateChange(@NonNull Note existingNote) {
        NotesDataSource notesDataSource = new NotesDataSource(getSession().getDatabase());
        if (!existingNote.isStored() || notesDataSource.findBySqlId(existingNote.getSqlId()) == null) {
            LogJourno.reportEvent(EVENT.ChangesChanger_updateChangeRequestedWithNewChange, NoteChanger.class.getSimpleName());
            Log.e(new Throwable("I can only handle existing Notes!"));
            return ChangeResult.FAILED;
        }
        createAndRelateNoteAssociations(existingNote);
        return new NotesDataSource(getSession().getDatabase()).createOrUpdate((BaseDatasourceEntity) existingNote) == -1 ? ChangeResult.FAILED : ChangeResult.SUCCESSFUL;
    }

    private void createAndRelateNoteAssociations(Note note) {
        if (note != null) {
            if (!(note.getDeal() == null || note.getDeal().isStored() || new DealChanger((Changer) this).create(note.getDeal()))) {
                note.setDeal(null);
            }
            if (!(note.getPerson() == null || note.getPerson().isStored() || new PersonChanger((Changer) this).create(note.getPerson()))) {
                note.setPerson(null);
            }
            if (note.getOrganization() != null && !note.getOrganization().isStored() && !new OrganizationChanger((Changer) this).create(note.getOrganization())) {
                note.setOrganization(null);
            }
        }
    }
}
