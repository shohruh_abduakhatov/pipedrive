package com.zendesk.sdk.rating.impl;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import com.zendesk.sdk.R;
import com.zendesk.sdk.storage.RateMyAppStorage;

public class RateMyAppDontAskAgainButton extends BaseRateMyAppButton {
    private final String mLabel;

    public RateMyAppDontAskAgainButton(Context context) {
        this.mLabel = context.getString(R.string.rate_my_app_dialog_dismiss_action_label);
    }

    public String getLabel() {
        return this.mLabel;
    }

    public OnClickListener getOnClickListener() {
        return new OnClickListener() {
            public void onClick(View view) {
                new RateMyAppStorage(view.getContext()).setDontShowAgain();
            }
        };
    }

    public int getId() {
        return R.id.rma_dont_ask_again;
    }
}
