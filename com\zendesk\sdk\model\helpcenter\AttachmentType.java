package com.zendesk.sdk.model.helpcenter;

public enum AttachmentType {
    INLINE("inline"),
    BLOCK("block");
    
    private String attachmentType;

    private AttachmentType(String attachmentType) {
        this.attachmentType = attachmentType;
    }

    public String getAttachmentType() {
        return this.attachmentType;
    }
}
