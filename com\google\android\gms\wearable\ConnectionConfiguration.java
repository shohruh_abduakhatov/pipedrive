package com.google.android.gms.wearable;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;

public class ConnectionConfiguration extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Creator<ConnectionConfiguration> CREATOR = new zzg();
    private final int JJ;
    private final String QI;
    private final boolean aSh;
    private String aSi;
    private boolean aSj;
    private String aSk;
    private boolean fz;
    private final String mName;
    final int mVersionCode;
    private final int nV;

    ConnectionConfiguration(int i, String str, String str2, int i2, int i3, boolean z, boolean z2, String str3, boolean z3, String str4) {
        this.mVersionCode = i;
        this.mName = str;
        this.QI = str2;
        this.nV = i2;
        this.JJ = i3;
        this.aSh = z;
        this.fz = z2;
        this.aSi = str3;
        this.aSj = z3;
        this.aSk = str4;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ConnectionConfiguration)) {
            return false;
        }
        ConnectionConfiguration connectionConfiguration = (ConnectionConfiguration) obj;
        return zzz.equal(Integer.valueOf(this.mVersionCode), Integer.valueOf(connectionConfiguration.mVersionCode)) && zzz.equal(this.mName, connectionConfiguration.mName) && zzz.equal(this.QI, connectionConfiguration.QI) && zzz.equal(Integer.valueOf(this.nV), Integer.valueOf(connectionConfiguration.nV)) && zzz.equal(Integer.valueOf(this.JJ), Integer.valueOf(connectionConfiguration.JJ)) && zzz.equal(Boolean.valueOf(this.aSh), Boolean.valueOf(connectionConfiguration.aSh)) && zzz.equal(Boolean.valueOf(this.aSj), Boolean.valueOf(connectionConfiguration.aSj));
    }

    public String getAddress() {
        return this.QI;
    }

    public String getName() {
        return this.mName;
    }

    public String getNodeId() {
        return this.aSk;
    }

    public int getRole() {
        return this.JJ;
    }

    public int getType() {
        return this.nV;
    }

    public int hashCode() {
        return zzz.hashCode(Integer.valueOf(this.mVersionCode), this.mName, this.QI, Integer.valueOf(this.nV), Integer.valueOf(this.JJ), Boolean.valueOf(this.aSh), Boolean.valueOf(this.aSj));
    }

    public boolean isConnected() {
        return this.fz;
    }

    public boolean isEnabled() {
        return this.aSh;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("ConnectionConfiguration[ ");
        String str = "mName=";
        String valueOf = String.valueOf(this.mName);
        stringBuilder.append(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        str = ", mAddress=";
        valueOf = String.valueOf(this.QI);
        stringBuilder.append(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        stringBuilder.append(", mType=" + this.nV);
        stringBuilder.append(", mRole=" + this.JJ);
        stringBuilder.append(", mEnabled=" + this.aSh);
        stringBuilder.append(", mIsConnected=" + this.fz);
        str = ", mPeerNodeId=";
        valueOf = String.valueOf(this.aSi);
        stringBuilder.append(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        stringBuilder.append(", mBtlePriority=" + this.aSj);
        str = ", mNodeId=";
        valueOf = String.valueOf(this.aSk);
        stringBuilder.append(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzg.zza(this, parcel, i);
    }

    public String zzcme() {
        return this.aSi;
    }

    public boolean zzcmf() {
        return this.aSj;
    }
}
