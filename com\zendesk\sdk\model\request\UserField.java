package com.zendesk.sdk.model.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.annotations.SerializedName;
import com.zendesk.util.CollectionUtils;
import java.util.Date;
import java.util.List;

public class UserField {
    private Boolean active;
    private Date createdAt;
    private List<UserFieldOption> customFieldOptions;
    private String description;
    private Long id;
    private String key;
    private Long position;
    private String rawDescription;
    private String rawTitle;
    private String regexpForValidation;
    private Boolean system;
    private String title;
    private Date updatedAt;
    private String url;
    @SerializedName("type")
    private UserFieldType userFieldType;

    enum UserFieldType {
        Integer,
        Decimal,
        Checkbox,
        Date,
        Text,
        Textarea,
        Dropdown,
        Regexp
    }

    @Nullable
    public Long getId() {
        return this.id;
    }

    @Nullable
    public String getUrl() {
        return this.url;
    }

    @Nullable
    public UserFieldType getUserFieldType() {
        return this.userFieldType;
    }

    @Nullable
    public String getKey() {
        return this.key;
    }

    @Nullable
    public String getTitle() {
        return this.title;
    }

    @Nullable
    public String getDescription() {
        return this.description;
    }

    @Nullable
    public String getRawDescription() {
        return this.rawDescription;
    }

    @Nullable
    public String getRawTitle() {
        return this.rawTitle;
    }

    @Nullable
    public Long getPosition() {
        return this.position;
    }

    public boolean isActive() {
        return this.active != null && this.active.booleanValue();
    }

    public boolean isSystem() {
        return this.system != null && this.system.booleanValue();
    }

    @Nullable
    public String getRegexpForValidation() {
        return this.regexpForValidation;
    }

    @Nullable
    public Date getCreatedAt() {
        return this.createdAt == null ? null : new Date(this.createdAt.getTime());
    }

    @Nullable
    public Date getUpdatedAt() {
        return this.updatedAt == null ? null : new Date(this.updatedAt.getTime());
    }

    @NonNull
    public List<UserFieldOption> getUserFieldOptions() {
        return CollectionUtils.copyOf(this.customFieldOptions);
    }
}
