package com.pipedrive.nearby.map;

import android.support.annotation.NonNull;
import com.pipedrive.nearby.model.NearbyItem;
import rx.Observable;

public interface MarkerSelectionProvider {
    @NonNull
    Observable<NearbyItem> markerSelection();
}
