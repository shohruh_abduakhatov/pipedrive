package com.pipedrive.contentproviders;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.model.Organization;
import com.pipedrive.model.customfields.CustomField;
import java.util.Arrays;
import java.util.HashSet;

public class OrganizationContentProvider extends BaseContentProvider {
    private static final String AUTHORITY = "com.pipedrive.contentproviders.organizationcontentprovider";
    private static final String BASE_PATH = "organizations";
    private static final int NOT_DEFINED = 0;
    private static final int ORGANIZATIONS = 10;
    private static final int ORG_PIPEDRIVE_ID = 30;
    private static final int ORG_SQL_ID = 20;
    private static final String PIPEDRIVE_ID_PATH = "pipedrive_id";
    private static final UriMatcher URI_MATCHER = new UriMatcher(-1);

    @Nullable
    @Deprecated
    public /* bridge */ /* synthetic */ Uri attachSessionIdToUri_hackMethodToEnableCPUriBuildOutsideCP_BAD_DESIGN_AND_USAGE(Uri uri) {
        return super.attachSessionIdToUri_hackMethodToEnableCPUriBuildOutsideCP_BAD_DESIGN_AND_USAGE(uri);
    }

    public /* bridge */ /* synthetic */ int delete(@NonNull Uri uri, String str, String[] strArr, @NonNull SQLiteDatabase sQLiteDatabase) {
        return super.delete(uri, str, strArr, sQLiteDatabase);
    }

    @Nullable
    public /* bridge */ /* synthetic */ String getType(@NonNull Uri uri) {
        return super.getType(uri);
    }

    @Nullable
    public /* bridge */ /* synthetic */ Uri insert(@NonNull Uri uri, ContentValues contentValues, @NonNull SQLiteDatabase sQLiteDatabase) {
        return super.insert(uri, contentValues, sQLiteDatabase);
    }

    public /* bridge */ /* synthetic */ boolean onCreate() {
        return super.onCreate();
    }

    public /* bridge */ /* synthetic */ int update(@NonNull Uri uri, ContentValues contentValues, String str, String[] strArr, @NonNull SQLiteDatabase sQLiteDatabase) {
        return super.update(uri, contentValues, str, strArr, sQLiteDatabase);
    }

    static {
        URI_MATCHER.addURI("com.pipedrive.contentproviders.organizationcontentprovider", "organizations", 10);
        URI_MATCHER.addURI("com.pipedrive.contentproviders.organizationcontentprovider", "organizations/#", 20);
        URI_MATCHER.addURI("com.pipedrive.contentproviders.organizationcontentprovider", "organizations/pipedrive_id/#", 30);
    }

    public OrganizationContentProvider(@NonNull Session session) {
        super(session);
    }

    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder, @NonNull SQLiteDatabase sqLiteDatabase) {
        OrganizationsDataSource dataSource = new OrganizationsDataSource(sqLiteDatabase);
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        checkColumns(sqLiteDatabase, projection);
        queryBuilder.setTables("organizations");
        Cursor cursor = null;
        switch (URI_MATCHER.match(uri)) {
            case 10:
                break;
            case 20:
                cursor = dataSource.findBySqlIdCursor(ContentUris.parseId(uri));
                break;
            case 30:
                queryBuilder.appendWhere("id=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        if (cursor == null) {
            cursor = queryBuilder.query(sqLiteDatabase, projection, selection, selectionArgs, null, null, sortOrder);
        }
        if (!(cursor == null || getContext() == null)) {
            cursor.setNotificationUri(getContext().getContentResolver(), contentChangedUri());
        }
        return cursor;
    }

    @Deprecated
    private void checkColumns(@NonNull SQLiteDatabase sqLiteDatabase, String[] projection) {
        OrganizationsDataSource organizationsDataSource = new OrganizationsDataSource(sqLiteDatabase);
        if (projection != null) {
            if (!new HashSet(Arrays.asList(organizationsDataSource.getAllColumns())).containsAll(new HashSet(Arrays.asList(projection)))) {
                throw new IllegalArgumentException("Unknown columns in projection");
            }
        }
    }

    @Nullable
    public Uri createGetOrganizationUriFromPipedriveId(int pipedriveId) {
        if (pipedriveId == 0) {
            return null;
        }
        Builder uriBuilder = new Builder();
        uriBuilder.scheme("content").authority("com.pipedrive.contentproviders.organizationcontentprovider").appendPath("organizations");
        uriBuilder.appendPath(PIPEDRIVE_ID_PATH);
        ContentUris.appendId(uriBuilder, (long) pipedriveId);
        return attachSessionIdToUri(uriBuilder.build());
    }

    @Nullable
    public Uri createGetOrganizationUriFromDatabaseID(long dbId) {
        if (dbId <= 0) {
            return null;
        }
        Builder uriBuilder = new Builder();
        uriBuilder.scheme("content").authority("com.pipedrive.contentproviders.organizationcontentprovider").appendPath("organizations");
        ContentUris.appendId(uriBuilder, dbId);
        return attachSessionIdToUri(uriBuilder.build());
    }

    @Nullable
    public static Organization getOrganizationFromUri(ContentResolver resolver, @Nullable Uri uri) {
        if (uri == null) {
            return null;
        }
        Session session = PipedriveApp.getActiveSession();
        if (session == null) {
            return null;
        }
        OrganizationsDataSource organizationsDataSource = new OrganizationsDataSource(session.getDatabase());
        Organization organization = null;
        Cursor cursor = resolver.query(uri, organizationsDataSource.getAllColumns(), null, null, null);
        if (cursor != null) {
            try {
                cursor.moveToFirst();
                if (!cursor.isAfterLast()) {
                    organization = organizationsDataSource.deflateCursor(cursor);
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        if (cursor != null) {
            cursor.close();
        }
        return organization;
    }

    public static void notifyChangesForOrganizationContentProvider(Context context) {
        if (!(context == null)) {
            context.getContentResolver().notifyChange(contentChangedUri(), null);
        }
    }

    @NonNull
    private static Uri contentChangedUri() {
        Builder builder = new Builder();
        builder.scheme("content").authority("com.pipedrive.contentproviders.organizationcontentprovider").appendPath("/orgdatachanged");
        return builder.build();
    }

    public static Uri getOrganizationUriFromCustomField(CustomField field, Session session, int id) {
        if (field.getTempValue().startsWith(CustomField.LOCAL_DATABASE_ID_PREFIX)) {
            return new OrganizationContentProvider(session).createGetOrganizationUriFromDatabaseID((long) id);
        }
        return new OrganizationContentProvider(session).createGetOrganizationUriFromPipedriveId(id);
    }
}
