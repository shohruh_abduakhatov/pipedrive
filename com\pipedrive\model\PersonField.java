package com.pipedrive.model;

import java.util.ArrayList;
import java.util.List;

public class PersonField {
    private int id;
    private String key;
    private String name;
    private List<PersonFieldOption> options;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PersonFieldOption> getOptions() {
        if (this.options == null) {
            this.options = new ArrayList();
        }
        return this.options;
    }

    public void setOptions(List<PersonFieldOption> options) {
        this.options = options;
    }

    public void addOption(PersonFieldOption option) {
        getOptions().add(option);
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
