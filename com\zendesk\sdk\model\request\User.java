package com.zendesk.sdk.model.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.util.CollectionUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class User {
    private boolean agent = false;
    private Long id = Long.valueOf(-1);
    private String name = "";
    private Long organizationId = Long.valueOf(-1);
    private Attachment photo = null;
    private List<String> tags = new ArrayList();
    private Map<String, String> userFields = new HashMap();

    @Nullable
    public Long getId() {
        return this.id;
    }

    @Nullable
    public String getName() {
        return this.name;
    }

    @Nullable
    public Attachment getPhoto() {
        return this.photo;
    }

    public boolean isAgent() {
        return this.agent;
    }

    @Nullable
    public Long getOrganizationId() {
        return this.organizationId;
    }

    @NonNull
    public List<String> getTags() {
        return CollectionUtils.copyOf(this.tags);
    }

    @NonNull
    public Map<String, String> getUserFields() {
        return CollectionUtils.copyOf(this.userFields);
    }
}
