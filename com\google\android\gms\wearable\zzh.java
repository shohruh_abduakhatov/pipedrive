package com.google.android.gms.wearable;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzh implements Creator<PutDataRequest> {
    static void zza(PutDataRequest putDataRequest, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zzc(parcel, 1, putDataRequest.mVersionCode);
        zzb.zza(parcel, 2, putDataRequest.getUri(), i, false);
        zzb.zza(parcel, 4, putDataRequest.zzcmg(), false);
        zzb.zza(parcel, 5, putDataRequest.getData(), false);
        zzb.zza(parcel, 6, putDataRequest.zzcmh());
        zzb.zzaj(parcel, zzcs);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzur(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzadl(i);
    }

    public PutDataRequest[] zzadl(int i) {
        return new PutDataRequest[i];
    }

    public PutDataRequest zzur(Parcel parcel) {
        byte[] bArr = null;
        int zzcr = zza.zzcr(parcel);
        int i = 0;
        long j = 0;
        Bundle bundle = null;
        Uri uri = null;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    i = zza.zzg(parcel, zzcq);
                    break;
                case 2:
                    uri = (Uri) zza.zza(parcel, zzcq, Uri.CREATOR);
                    break;
                case 4:
                    bundle = zza.zzs(parcel, zzcq);
                    break;
                case 5:
                    bArr = zza.zzt(parcel, zzcq);
                    break;
                case 6:
                    j = zza.zzi(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new PutDataRequest(i, uri, bundle, bArr, j);
        }
        throw new zza.zza("Overread allowed size end=" + zzcr, parcel);
    }
}
