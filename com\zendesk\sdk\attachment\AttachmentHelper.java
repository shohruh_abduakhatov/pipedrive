package com.zendesk.sdk.attachment;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.net.Uri;
import android.widget.Toast;
import com.zendesk.belvedere.BelvedereResult;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.sdk.feedback.ui.AttachmentContainerHost;
import com.zendesk.sdk.feedback.ui.AttachmentContainerHost.AttachmentState;
import com.zendesk.sdk.model.settings.SafeMobileSettings;
import com.zendesk.service.ErrorResponse;
import com.zendesk.util.FileUtils;
import java.io.File;
import java.util.List;
import java.util.Locale;

public class AttachmentHelper {
    private static final String DEFAULT_MIMETYPE = "application/octet-stream";
    private static final String LOG_TAG = AttachmentHelper.class.getSimpleName();

    public static boolean isAttachmentSupportEnabled(SafeMobileSettings storedSettings) {
        return storedSettings != null && storedSettings.isAttachmentsEnabled();
    }

    public static boolean isFileEligibleForUpload(File file, SafeMobileSettings storedSettings) {
        if (file == null || !file.exists() || storedSettings == null || file.length() > storedSettings.getMaxAttachmentSize()) {
            return false;
        }
        return true;
    }

    public static void showAttachmentTryAgainDialog(final Context context, final BelvedereResult file, ErrorResponse errorResponse, final ImageUploadHelper imageUploadHelper, final AttachmentContainerHost attachmentContainerHost) {
        Logger.e(LOG_TAG, "Attachment failed to upload: %s", file.getFile().getName());
        Builder builder = new Builder(context);
        builder.setMessage(context.getString(R.string.attachment_upload_error_upload_failed));
        builder.setCancelable(false);
        builder.setNegativeButton(context.getString(R.string.attachment_upload_error_cancel), new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                attachmentContainerHost.removeAttachment(file.getFile());
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(context.getString(R.string.attachment_upload_error_try_again), new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                imageUploadHelper.uploadImage(file, AttachmentHelper.getMimeType(context, file.getUri()));
                attachmentContainerHost.setAttachmentState(file.getFile(), AttachmentState.UPLOADING);
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public static void processAndUploadSelectedFiles(List<BelvedereResult> selectedFiles, ImageUploadHelper imageUploadHelper, Context context, AttachmentContainerHost attachmentContainerHost, SafeMobileSettings storedSettings) {
        List<BelvedereResult> uniqueFiles = imageUploadHelper.removeDuplicateFilesFromList(selectedFiles);
        if (uniqueFiles.size() != selectedFiles.size()) {
            Toast.makeText(context, context.getString(R.string.attachment_upload_error_file_already_added), 1).show();
            Logger.e(LOG_TAG, "Files already added", new Object[0]);
        }
        for (BelvedereResult file : uniqueFiles) {
            if (file == null || !file.getFile().exists()) {
                Toast.makeText(context, context.getString(R.string.attachment_upload_error_file_not_found), 1).show();
                Logger.e(LOG_TAG, "File not found: " + (file == null ? "no filename" : file.getFile().getName()), new Object[0]);
            } else if (isFileEligibleForUpload(file.getFile(), storedSettings)) {
                imageUploadHelper.uploadImage(file, getMimeType(context, file.getUri()));
                attachmentContainerHost.addAttachment(file.getFile());
            } else {
                String fileSize = "";
                if (storedSettings != null) {
                    fileSize = FileUtils.humanReadableFileSize(Long.valueOf(storedSettings.getMaxAttachmentSize()));
                }
                Toast.makeText(context, String.format(Locale.US, context.getString(R.string.attachment_upload_error_file_too_big), new Object[]{fileSize}), 1).show();
                Logger.e(LOG_TAG, "File is too big: " + file.getFile().getName(), new Object[0]);
            }
        }
    }

    private static String getMimeType(Context context, Uri file) {
        return file != null ? context.getContentResolver().getType(file) : DEFAULT_MIMETYPE;
    }
}
