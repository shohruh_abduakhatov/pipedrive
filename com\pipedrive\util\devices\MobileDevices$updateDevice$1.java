package com.pipedrive.util.devices;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import rx.functions.Func1;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "Lcom/pipedrive/util/devices/DeviceEntity;", "deviceResponse", "Lcom/pipedrive/util/devices/DeviceResponse;", "kotlin.jvm.PlatformType", "call"}, k = 3, mv = {1, 1, 6})
/* compiled from: MobileDevices.kt */
final class MobileDevices$updateDevice$1<T, R> implements Func1<T, R> {
    public static final MobileDevices$updateDevice$1 INSTANCE = new MobileDevices$updateDevice$1();

    MobileDevices$updateDevice$1() {
    }

    @NotNull
    public final DeviceEntity call(DeviceResponse deviceResponse) {
        DeviceEntity deviceEntity = deviceResponse.getDeviceEntity();
        if (deviceEntity == null) {
            Intrinsics.throwNpe();
        }
        return deviceEntity;
    }
}
