package com.pipedrive.views.edit.products;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.TextViewCompat;
import android.util.AttributeSet;
import android.widget.TextView;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.views.common.DecimalEditText;
import com.pipedrive.views.common.DecimalEditText.OnTextOrFocusChangeListener;
import com.pipedrive.views.common.TextViewLayoutWithUnderlineAndLabel;

public abstract class DecimalEditTextWithUnderlineAndLabel extends TextViewLayoutWithUnderlineAndLabel<DecimalEditText> implements OnTextOrFocusChangeListener {
    private boolean mIsFormattingApplied;
    @Nullable
    private Session mSession;
    private double mValue;

    @Nullable
    protected abstract String getFormattedValue();

    protected abstract void onValueChanged(double d);

    public DecimalEditTextWithUnderlineAndLabel(Context context) {
        this(context, null);
    }

    public DecimalEditTextWithUnderlineAndLabel(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DecimalEditTextWithUnderlineAndLabel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mIsFormattingApplied = true;
    }

    public double getValue() {
        return this.mValue;
    }

    @Nullable
    protected Session getSession() {
        return this.mSession;
    }

    @NonNull
    public DecimalEditText createMainView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        return new DecimalEditText(context, attrs, defStyle);
    }

    protected void setupMainViewWithZeroValueHidden(@NonNull Session session, double value) {
        init(session, value);
        if (this.mValue != 0.0d) {
            displayInitialValue();
            return;
        }
        this.mIsFormattingApplied = false;
        ((DecimalEditText) this.mMainView).getText().clear();
        hideLabel();
        ((DecimalEditText) this.mMainView).setHint(getLabelTextResourceId());
    }

    protected void setupMainViewWithZeroValueDisplayed(@NonNull Session session, double value) {
        init(session, value);
        displayInitialValue();
    }

    private void init(@NonNull Session session, double value) {
        this.mSession = session;
        this.mValue = value;
        ((DecimalEditText) this.mMainView).init(session, this);
        ((DecimalEditText) this.mMainView).setBackgroundColor(0);
        TextViewCompat.setTextAppearance((TextView) this.mMainView, R.style.TextAppearance.Title);
    }

    private void displayInitialValue() {
        if (((DecimalEditText) this.mMainView).hasFocus()) {
            ((DecimalEditText) this.mMainView).setDecimalValueWithoutTailingZeroes(this.mValue);
        } else {
            ((DecimalEditText) this.mMainView).setText(getFormattedValue());
        }
    }

    public void onFocusGained() {
        showLabel();
        this.mIsFormattingApplied = false;
        if (((DecimalEditText) this.mMainView).nothingIsEntered()) {
            ((DecimalEditText) this.mMainView).setHint("");
        } else {
            ((DecimalEditText) this.mMainView).setDecimalValueWithoutTailingZeroes(this.mValue);
        }
    }

    public void onFocusLost() {
        this.mIsFormattingApplied = true;
        if (((DecimalEditText) this.mMainView).nothingIsEntered()) {
            hideLabel();
            ((DecimalEditText) this.mMainView).setHint(getLabelTextResourceId());
            return;
        }
        ((DecimalEditText) this.mMainView).setText(getFormattedValue());
    }

    public void onAfterTextChanged(double newValue) {
        if (!this.mIsFormattingApplied) {
            this.mValue = newValue;
            onValueChanged(newValue);
        }
    }
}
