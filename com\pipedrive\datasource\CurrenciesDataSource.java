package com.pipedrive.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.model.Currency;
import com.pipedrive.util.CursorHelper;
import java.util.List;

public class CurrenciesDataSource extends BaseDataSource<Currency> {
    private final String[] ALL_COLUMNS = new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, "name", PipeSQLiteHelper.COLUMN_CODE, PipeSQLiteHelper.COLUMN_SYMBOL, PipeSQLiteHelper.COLUMN_DECIMAL_POINTS, PipeSQLiteHelper.COLUMN_RATE_TO_DEFAULT, PipeSQLiteHelper.COLUMN_ACTIVE, PipeSQLiteHelper.COLUMN_IS_CUSTOM};

    public CurrenciesDataSource(SQLiteDatabase dbconn) {
        super(dbconn);
    }

    @Nullable
    public Currency getCurrencyByCode(@Nullable String code) {
        if (code == null) {
            return null;
        }
        Cursor cursor = null;
        try {
            String tableName = getTableName();
            String[] allColumns = getAllColumns();
            String str = "upper(cur_code) = ?";
            String[] strArr = new String[]{code.toUpperCase()};
            cursor = !(this instanceof SQLiteDatabase) ? query(tableName, allColumns, str, strArr, null, null, null) : SQLiteInstrumentation.query((SQLiteDatabase) this, tableName, allColumns, str, strArr, null, null, null);
            if (cursor.getCount() <= 0) {
                return null;
            }
            cursor.moveToFirst();
            Currency currency = deflateCursor(cursor);
            if (cursor == null) {
                return currency;
            }
            cursor.close();
            return currency;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @NonNull
    public List<Currency> getCurrencies() {
        String tableName = getTableName();
        String[] allColumns = getAllColumns();
        String str = "active = ?";
        String[] strArr = new String[]{String.valueOf(1)};
        String str2 = PipeSQLiteHelper.COLUMN_ID;
        return deflateCursorToList(!(this instanceof SQLiteDatabase) ? query(tableName, allColumns, str, strArr, null, null, str2) : SQLiteInstrumentation.query((SQLiteDatabase) this, tableName, allColumns, str, strArr, null, null, str2));
    }

    @NonNull
    protected String getColumnNameForPipedriveId() {
        return PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID;
    }

    @NonNull
    protected String getColumnNameForSqlId() {
        return PipeSQLiteHelper.COLUMN_ID;
    }

    @NonNull
    protected String getTableName() {
        return "currencies";
    }

    @NonNull
    protected ContentValues getContentValues(@NonNull Currency currency) {
        ContentValues contentValues = super.getContentValues(currency);
        CursorHelper.put(currency.getName(), contentValues, "name");
        CursorHelper.put(currency.getCode(), contentValues, PipeSQLiteHelper.COLUMN_CODE);
        CursorHelper.put(currency.getSymbol(), contentValues, PipeSQLiteHelper.COLUMN_SYMBOL);
        CursorHelper.put(Integer.valueOf(currency.getDecimalPoints()), contentValues, PipeSQLiteHelper.COLUMN_DECIMAL_POINTS);
        CursorHelper.put(Double.valueOf(currency.getRateToDefault()), contentValues, PipeSQLiteHelper.COLUMN_RATE_TO_DEFAULT);
        CursorHelper.put(Boolean.valueOf(currency.isActive()), contentValues, PipeSQLiteHelper.COLUMN_ACTIVE);
        CursorHelper.put(Boolean.valueOf(currency.isCustom()), contentValues, PipeSQLiteHelper.COLUMN_IS_CUSTOM);
        return contentValues;
    }

    @NonNull
    protected String[] getAllColumns() {
        return this.ALL_COLUMNS;
    }

    @Nullable
    protected Currency deflateCursor(@NonNull Cursor cursor) {
        boolean z = true;
        Currency currency = new Currency();
        currency.setSqlId(cursor.getLong(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_ID)));
        currency.setPipedriveId(cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID)));
        currency.setActive(cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_ACTIVE)) == 1);
        if (cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_IS_CUSTOM)) != 1) {
            z = false;
        }
        currency.setCustom(z);
        currency.setName(cursor.getString(cursor.getColumnIndex("name")));
        currency.setRateToDefault(cursor.getDouble(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_RATE_TO_DEFAULT)));
        currency.setDecimalPoints(cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DECIMAL_POINTS)));
        currency.setCode(cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_CODE)));
        currency.setSymbol(cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_SYMBOL)));
        return currency;
    }
}
