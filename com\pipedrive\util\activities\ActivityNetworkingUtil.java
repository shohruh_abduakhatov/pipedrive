package com.pipedrive.util.activities;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.model.Activity;
import com.pipedrive.model.ActivityType;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.notification.UINotificationManager;
import com.pipedrive.util.networking.entities.ActivityEntity;
import com.pipedrive.util.organizations.OrganizationsNetworkingUtil;
import com.pipedrive.util.persons.PersonNetworkingUtil;
import java.util.ArrayList;
import java.util.List;

public enum ActivityNetworkingUtil {
    ;
    
    public static final int PREFS_ACTIVITY_FILTER_ACTIVITY_TYPE_ID_VALUE_UNDEFINED = -1;
    public static final int PREFS_ACTIVITY_FILTER_ASSIGNEE_ID_VALUE_UNDEFINED = -1;
    public static final int PREFS_ACTIVITY_FILTER_TIME_VALUE_UNDEFINED = -1;

    public static List<ActivityType> loadAllActivityTypesList(Session session) {
        return loadActivityTypesList(session, false, 0);
    }

    @NonNull
    public static List<ActivityType> loadActivityTypesList(@NonNull Session session, boolean loadOnlyActive) {
        return loadActivityTypesList(session, loadOnlyActive, 0);
    }

    public static List<ActivityType> loadActivityTypesList(Session session, boolean loadOnlyActive, int currentActivityTypeID) {
        List<ActivityType> activityTypes = ActivityType.readActivityTypes(session);
        List<ActivityType> activityTypesToReturn = new ArrayList();
        for (ActivityType activityType : activityTypes) {
            if (!loadOnlyActive || activityType.isActiveFlag() || currentActivityTypeID == activityType.getId()) {
                activityTypesToReturn.add(activityType);
            }
        }
        return activityTypesToReturn;
    }

    @Nullable
    public static Activity createOrUpdateActivityIntoDBWithRelations(@NonNull Session session, @Nullable ActivityEntity activityEntity) {
        Activity createdActivity = createOrUpdateActivityIntoDBWithRelations(session, activityEntity, null);
        UINotificationManager.getInstance().processNotificationForActivity(session, createdActivity);
        return createdActivity;
    }

    @Nullable
    public static Activity createOrUpdateActivityIntoDBWithRelations(@NonNull Session session, @Nullable ActivityEntity activityEntity, @Nullable Long activitySqlId) {
        Activity activity = Activity.fromEntityReturnedWithoutAssociations(session, activityEntity);
        if (activity == null) {
            return null;
        }
        if (activitySqlId != null) {
            activity.setSqlId(activitySqlId.longValue());
        }
        Person activityAssociatedPerson = null;
        if (activityEntity.personId != null && activityEntity.personId.longValue() > 0) {
            activityAssociatedPerson = new PersonsDataSource(session.getDatabase()).findPersonByPipedriveId(activityEntity.personId.longValue());
            if (activityAssociatedPerson == null) {
                activityAssociatedPerson = PersonNetworkingUtil.downloadPerson(session, activityEntity.personId.longValue());
                if (activityAssociatedPerson != null) {
                    activityAssociatedPerson = PersonNetworkingUtil.createOrUpdatePersonIntoDBWithRelations(session, activityAssociatedPerson);
                }
            }
        }
        activity.setPerson(activityAssociatedPerson);
        Organization activityAssociatedOrganization = null;
        if (activityEntity.orgId != null && activityEntity.orgId.longValue() > 0) {
            activityAssociatedOrganization = new OrganizationsDataSource(session.getDatabase()).findOrganizationByPipedriveId(activityEntity.orgId.longValue());
            if (activityAssociatedOrganization == null) {
                activityAssociatedOrganization = OrganizationsNetworkingUtil.downloadOrganization(session, activityEntity.orgId.longValue());
                if (activityAssociatedOrganization != null) {
                    activityAssociatedOrganization = OrganizationsNetworkingUtil.createOrUpdateOrganizationIntoDBWithRelations(session, activityAssociatedOrganization);
                }
            }
        }
        activity.setOrganization(activityAssociatedOrganization);
        Deal activityAssociatedDeal = null;
        if (activityEntity.dealId != null && activityEntity.dealId.longValue() > 0) {
            activityAssociatedDeal = new DealsDataSource(session.getDatabase()).findDealByDealId(activityEntity.dealId.longValue());
            if (activityAssociatedDeal == null) {
                activityAssociatedDeal = DealsNetworkingUtil.downloadDeal(session, activityEntity.dealId.longValue());
                if (activityAssociatedDeal != null) {
                    activityAssociatedDeal = DealsNetworkingUtil.createOrUpdateDealIntoDBWithRelations(session, activityAssociatedDeal);
                }
            }
        }
        activity.setDeal(activityAssociatedDeal);
        long createdActivitySqlId = new ActivitiesDataSource(session).createOrUpdate(activity);
        if (createdActivitySqlId == -1) {
            return null;
        }
        activity.setSqlId(createdActivitySqlId);
        return activity;
    }

    @NonNull
    public static String[] getCallActivityTypeNames(@NonNull Session session) {
        List<String> callTypes = new ArrayList();
        for (ActivityType activityType : loadActivityTypesList(session, true)) {
            if (activityType.getImageResourceId() == R.drawable.activity_call) {
                callTypes.add(activityType.getName());
            }
        }
        return (String[]) callTypes.toArray(new String[callTypes.size()]);
    }
}
