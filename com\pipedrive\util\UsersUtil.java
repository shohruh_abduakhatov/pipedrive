package com.pipedrive.util;

import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.UsersDataSource;
import com.pipedrive.logging.Log;
import com.pipedrive.model.User;
import com.pipedrive.model.UserSettingsPipelineFilter;
import java.util.List;

public class UsersUtil {
    private static final String TAG = UsersUtil.class.getSimpleName();

    private UsersUtil() {
    }

    public static void updateUserSettingsSelectedFilterForUser(@NonNull Session session, @IntRange(from = -1) long pipelineId, @Nullable Long userId, boolean allUsers) {
        updateUserSettingsSelectedFilter(session, pipelineId, null, userId, allUsers);
    }

    public static void updateUserSettingsSelectedFilterForFilter(@NonNull Session session, @IntRange(from = -1) long pipelineId, @Nullable Long filterId, boolean allUsers) {
        updateUserSettingsSelectedFilter(session, pipelineId, filterId, null, allUsers);
    }

    private static void updateUserSettingsSelectedFilter(@NonNull Session session, @IntRange(from = -1) long pipelineId, @Nullable Long filterId, @Nullable Long userId, boolean allUsers) {
        String tag = TAG + ".updateUserSettings()";
        List<UserSettingsPipelineFilter> userSettingsPipelineFilters = getUserSettingsFilters(session);
        boolean filterFound = false;
        String filterName = allUsers ? "all_users" : filterId != null ? "filter_" + filterId : userId != null ? "user_" + userId : null;
        Log.d(tag, String.format("User settings filter name will be set to %s", new Object[]{filterName}));
        if (filterName != null) {
            for (UserSettingsPipelineFilter userSettingsPipelineFilter : userSettingsPipelineFilters) {
                if (userSettingsPipelineFilter.getPipelineId() == pipelineId) {
                    userSettingsPipelineFilter.setFilterName(filterName);
                    filterFound = true;
                    break;
                }
            }
            if (!filterFound) {
                userSettingsPipelineFilters.add(new UserSettingsPipelineFilter(pipelineId, filterName));
            }
            saveUserSettingsFilters(session, userSettingsPipelineFilters);
            session.setPipelineFilter(filterId);
            session.setPipelineUser(userId);
        }
    }

    public static void saveUserSettingsFilters(Session session, List<UserSettingsPipelineFilter> filters) {
        session.setUserSettingsFilters(UserSettingsPipelineFilter.getJSON(filters));
    }

    static List<UserSettingsPipelineFilter> getUserSettingsFilters(Session session) {
        String savedUserFiltersJSON = session.getUserSettingsFilters();
        if (savedUserFiltersJSON.equalsIgnoreCase(Session.PREFS_NOT_SET_STRING)) {
            savedUserFiltersJSON = null;
        }
        return UserSettingsPipelineFilter.getFromJSON(savedUserFiltersJSON);
    }

    @NonNull
    public static List<User> getActiveCompanyUsers(@NonNull Session session) {
        return new UsersDataSource(session.getDatabase()).getActiveUsersOrderedByName();
    }

    @NonNull
    public static List<User> getActiveCompanyUsersWithCurrentlyAuthenticatedUserAsFirst(@NonNull Session session) {
        List<User> activeCompanyUsers = getActiveCompanyUsers(session);
        long authenticatedUserPipedriveId = session.getAuthenticatedUserID();
        for (int i = 0; i < activeCompanyUsers.size(); i++) {
            boolean authenticatedUserFound;
            User user = (User) activeCompanyUsers.get(i);
            if (user == null || user.getPipedriveIdOrNull() == null || user.getPipedriveIdOrNull().longValue() != authenticatedUserPipedriveId) {
                authenticatedUserFound = false;
            } else {
                authenticatedUserFound = true;
            }
            if (authenticatedUserFound) {
                activeCompanyUsers.remove(user);
                activeCompanyUsers.add(0, user);
                break;
            }
        }
        return activeCompanyUsers;
    }
}
