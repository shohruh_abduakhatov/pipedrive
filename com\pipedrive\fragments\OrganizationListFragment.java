package com.pipedrive.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import com.pipedrive.OrganizationDetailActivity;
import com.pipedrive.R;
import com.pipedrive.adapter.OrganizationListAdapter;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.util.ViewUtil;

public class OrganizationListFragment extends BaseListFragment {
    private OrganizationListAdapter mAdapter;
    @NonNull
    private SearchConstraint mSearchConstraint = SearchConstraint.EMPTY;
    private Session mSession;

    public static OrganizationListFragment newInstance() {
        return new OrganizationListFragment();
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mSession = PipedriveApp.getActiveSession();
    }

    public void onStart() {
        super.onStart();
        Cursor cursor = new OrganizationsDataSource(this.mSession.getDatabase()).getSearchCursor(this.mSearchConstraint);
        if (this.mAdapter == null) {
            this.mAdapter = new OrganizationListAdapter(getActivity(), cursor, this.mSession);
            getListView().addFooterView(ViewUtil.getFooterForListWithFloatingButton(getActivity()), null, false);
            getListView().setAdapter(this.mAdapter);
            getListView().setFastScrollEnabled(true);
            setEmptyText(R.string.lbl_no_results_found);
            return;
        }
        this.mAdapter.changeCursor(cursor);
    }

    public void listFilter(@NonNull SearchConstraint searchConstraint) {
        this.mSearchConstraint = searchConstraint;
        this.mAdapter.setSearchConstraint(searchConstraint);
    }

    public void onItemClick(@NonNull AdapterView<?> adapterView, @NonNull View view, int position, long id) {
        OrganizationDetailActivity.startActivity(getActivity(), Long.valueOf(((Cursor) this.mAdapter.getItem(position)).getLong(0)));
    }
}
