package com.pipedrive;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.view.MotionEvent;
import android.widget.TextView;
import butterknife.ButterKnife;

public abstract class LoadingModalActivity extends BaseActivity {
    @StringRes
    protected abstract int getLoadingMessage();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.layout_loading_modal);
        TextView loadingMessage = (TextView) ButterKnife.findById((Activity) this, (int) R.id.loading_message);
        if (loadingMessage != null) {
            loadingMessage.setText(getLoadingMessage());
        }
        getWindow().setFlags(32, 32);
        getWindow().setFlags(262144, 262144);
    }

    @SuppressLint({"MissingSuperCall"})
    public void onBackPressed() {
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (4 == event.getAction()) {
            return true;
        }
        return super.onTouchEvent(event);
    }
}
