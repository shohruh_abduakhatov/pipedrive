package com.pipedrive.pipeline;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class PipelineDealListHeaderView_ViewBinding implements Unbinder {
    private PipelineDealListHeaderView target;

    @UiThread
    public PipelineDealListHeaderView_ViewBinding(PipelineDealListHeaderView target) {
        this(target, target);
    }

    @UiThread
    public PipelineDealListHeaderView_ViewBinding(PipelineDealListHeaderView target, View source) {
        this.target = target;
        target.mTitle = (TextView) Utils.findRequiredViewAsType(source, R.id.stage_title, "field 'mTitle'", TextView.class);
        target.mSummary = (TextView) Utils.findRequiredViewAsType(source, R.id.stage_summary, "field 'mSummary'", TextView.class);
        target.mProgressBar = (ProgressBar) Utils.findRequiredViewAsType(source, R.id.loading_progress, "field 'mProgressBar'", ProgressBar.class);
    }

    @CallSuper
    public void unbind() {
        PipelineDealListHeaderView target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mTitle = null;
        target.mSummary = null;
        target.mProgressBar = null;
    }
}
