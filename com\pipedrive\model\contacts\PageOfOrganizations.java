package com.pipedrive.model.contacts;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.gson.stream.JsonReader;
import com.pipedrive.application.Session;
import com.pipedrive.contentproviders.OrganizationContentProvider;
import com.pipedrive.datasource.SQLTransactionManager;
import com.pipedrive.logging.Log;
import com.pipedrive.model.Contact;
import com.pipedrive.model.Organization;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.organizations.OrganizationsNetworkingUtil;
import com.pipedrive.util.time.TimeManager;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class PageOfOrganizations extends PageOfContacts {
    public static final String ACTION_ORGANIZATIONS_DOWNLOADED = ("com.pipedrive." + TAG + ".ACTION_ORGANIZATIONS_DOWNLOADED");
    public static final String ACTION_ORGANIZATIONS_DOWNLOADED_INTENT_MORE_COMING = ("com.pipedrive." + TAG + ".ACTION_ORGANIZATIONS_DOWNLOADED_INTENT__MORE_COMING");
    static final String TAG = PageOfOrganizations.class.getSimpleName();
    private List<Organization> contacts = new ArrayList();

    public List<Contact> getAllContacts() {
        List<Contact> c = new ArrayList(this.contacts.size());
        for (Organization organization : this.contacts) {
            c.add(organization);
        }
        return c;
    }

    public void deleteAllContacts() {
        this.contacts.clear();
    }

    @Nullable
    public InputStream getPageOfContactsInputStream(@NonNull Session session, int start, int limit, boolean firstStart) {
        return getOrganizationsListStream(session, start, limit, firstStart);
    }

    public void readAndSaveOneContact(Session session, JsonReader reader) throws IOException {
        Organization org = OrganizationsNetworkingUtil.readOrg(reader, session.getOrgAddressField(null));
        if (!TextUtils.isEmpty(org.getName())) {
            this.contacts.add(org);
        }
    }

    public void persistAllContacts(Session session) {
        String tag = TAG + ".saveAllContacts()";
        long time = TimeManager.getInstance().currentTimeMillis().longValue();
        Log.d(tag, "Downloaded " + this.contacts.size() + " organizations. Starting to save them in DB under transaction.");
        SQLTransactionManager sqlTransactionManager = new SQLTransactionManager(session.getDatabase());
        sqlTransactionManager.beginTransactionNonExclusive();
        for (Organization organization : this.contacts) {
            OrganizationsNetworkingUtil.createOrUpdateOrganizationIntoDBWithRelations(session, organization);
        }
        sqlTransactionManager.commit();
        Log.d(tag, "Added " + this.contacts.size() + " organizations into database. Time: " + (TimeManager.getInstance().currentTimeMillis().longValue() - time) + "ms");
    }

    public void pageOfContactsHaveBeenLoaded(Session session, boolean moreItemsInDB) {
        Context context = session.getApplicationContext();
        OrganizationContentProvider.notifyChangesForOrganizationContentProvider(context);
        Intent intent = new Intent();
        intent.setAction(ACTION_ORGANIZATIONS_DOWNLOADED);
        intent.putExtra(ACTION_ORGANIZATIONS_DOWNLOADED_INTENT_MORE_COMING, moreItemsInDB);
        context.sendBroadcast(intent);
    }

    public long getNewestUpdateTimeReadFromContactsQuery(Session session, long returnIfNotSet) {
        return session.getOrganizationsListUpdateTime(returnIfNotSet);
    }

    public void saveNewestUpdateTimeReadFromContactsQuery(Session session, long time) {
        session.setOrganizationsListUpdateTime(time);
    }

    @Nullable
    private static InputStream getOrganizationsListStream(@NonNull Session session, int start, int limit, boolean asc) {
        ApiUrlBuilder uriBuilder = new ApiUrlBuilder(session, "organizations");
        uriBuilder.pagination(Long.valueOf(Integer.valueOf(start).longValue()), Long.valueOf(Integer.valueOf(limit).longValue()));
        uriBuilder.param("sort_by", "update_time");
        uriBuilder.param("sort_mode", asc ? "asc" : "desc");
        return ConnectionUtil.requestGet(uriBuilder);
    }
}
