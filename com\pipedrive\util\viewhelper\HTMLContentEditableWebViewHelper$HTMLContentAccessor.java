package com.pipedrive.util.viewhelper;

import android.webkit.JavascriptInterface;
import com.pipedrive.util.viewhelper.HTMLContentEditableWebViewHelper.OnWebViewContentAcquired;

class HTMLContentEditableWebViewHelper$HTMLContentAccessor {
    private OnWebViewContentAcquired mWebViewAcquirer;
    final /* synthetic */ HTMLContentEditableWebViewHelper this$0;

    private HTMLContentEditableWebViewHelper$HTMLContentAccessor(HTMLContentEditableWebViewHelper hTMLContentEditableWebViewHelper) {
        this.this$0 = hTMLContentEditableWebViewHelper;
        this.mWebViewAcquirer = null;
    }

    public void setOnWebViewContentAcquired(OnWebViewContentAcquired webViewAcquirer) {
        this.mWebViewAcquirer = webViewAcquirer;
    }

    @JavascriptInterface
    public void contentInHTML(String contentInHTML) {
        if (this.mWebViewAcquirer != null) {
            OnWebViewContentAcquired onWebViewContentAcquired = this.mWebViewAcquirer;
            if (contentInHTML == null) {
                contentInHTML = "";
            }
            onWebViewContentAcquired.webViewContent(contentInHTML);
        }
    }
}
