package com.pipedrive.util.networking;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.pipedrive.application.Session;

public enum ApiUrlBuilder$Tools {
    ;

    @Deprecated
    @NonNull
    public static String appendAPITokenToURLIfMissing(@NonNull String url, @NonNull Session session) {
        boolean apiTokenNotPresentInUrl;
        if (url.contains("api_token")) {
            apiTokenNotPresentInUrl = false;
        } else {
            apiTokenNotPresentInUrl = true;
        }
        if (!apiTokenNotPresentInUrl) {
            return url;
        }
        return String.format(url.contains("?") ? "%s&api_token=%s" : "%s?api_token=%s", new Object[]{url, session.getApiToken()});
    }

    @NonNull
    public static String stripAPIKeyFromURL(@Nullable String url) {
        if (url == null || TextUtils.isEmpty(url)) {
            return "";
        }
        return url.replaceAll("api_token=[^&]*&?", "");
    }
}
