package com.zendesk.sdk.network.impl;

class ConfigurationRuntimeException extends RuntimeException {
    private static final String MESSAGE = "Zendesk configuration error occurred";

    ConfigurationRuntimeException() {
        super(MESSAGE);
    }

    ConfigurationRuntimeException(String message) {
        super(message);
    }
}
