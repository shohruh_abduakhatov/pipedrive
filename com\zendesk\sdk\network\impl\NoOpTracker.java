package com.zendesk.sdk.network.impl;

import com.zendesk.sdk.network.ZendeskTracker;

class NoOpTracker implements ZendeskTracker {
    NoOpTracker() {
    }

    public void helpCenterLoaded() {
    }

    public void helpCenterSearched(String query) {
    }

    public void helpCenterArticleViewed() {
    }

    public void requestCreated() {
    }

    public void requestUpdated() {
    }

    public void rateMyAppRated() {
    }

    public void rateMyAppFeedbackSent() {
    }
}
