package com.google.android.gms.location.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.location.Location;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationStatusCodes;
import java.util.List;

public class zzl extends zzb {
    private final zzk ala;

    private static final class zza extends com.google.android.gms.location.internal.zzh.zza {
        private com.google.android.gms.internal.zzqo.zzb<Status> alb;

        public zza(com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status) {
            this.alb = com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status;
        }

        public void zza(int i, PendingIntent pendingIntent) {
            Log.wtf("LocationClientImpl", "Unexpected call to onRemoveGeofencesByPendingIntentResult");
        }

        public void zza(int i, String[] strArr) {
            if (this.alb == null) {
                Log.wtf("LocationClientImpl", "onAddGeofenceResult called multiple times");
                return;
            }
            this.alb.setResult(LocationStatusCodes.zzuy(LocationStatusCodes.zzux(i)));
            this.alb = null;
        }

        public void zzb(int i, String[] strArr) {
            Log.wtf("LocationClientImpl", "Unexpected call to onRemoveGeofencesByRequestIdsResult");
        }
    }

    private static final class zzb extends com.google.android.gms.location.internal.zzh.zza {
        private com.google.android.gms.internal.zzqo.zzb<Status> alb;

        public zzb(com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status) {
            this.alb = com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status;
        }

        private void zzvb(int i) {
            if (this.alb == null) {
                Log.wtf("LocationClientImpl", "onRemoveGeofencesResult called multiple times");
                return;
            }
            this.alb.setResult(LocationStatusCodes.zzuy(LocationStatusCodes.zzux(i)));
            this.alb = null;
        }

        public void zza(int i, PendingIntent pendingIntent) {
            zzvb(i);
        }

        public void zza(int i, String[] strArr) {
            Log.wtf("LocationClientImpl", "Unexpected call to onAddGeofencesResult");
        }

        public void zzb(int i, String[] strArr) {
            zzvb(i);
        }
    }

    private static final class zzc extends com.google.android.gms.location.internal.zzj.zza {
        private com.google.android.gms.internal.zzqo.zzb<LocationSettingsResult> alb;

        public zzc(com.google.android.gms.internal.zzqo.zzb<LocationSettingsResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_location_LocationSettingsResult) {
            zzaa.zzb(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_location_LocationSettingsResult != null, (Object) "listener can't be null.");
            this.alb = com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_location_LocationSettingsResult;
        }

        public void zza(LocationSettingsResult locationSettingsResult) throws RemoteException {
            this.alb.setResult(locationSettingsResult);
            this.alb = null;
        }
    }

    public zzl(Context context, Looper looper, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, String str) {
        this(context, looper, connectionCallbacks, onConnectionFailedListener, str, zzf.zzca(context));
    }

    public zzl(Context context, Looper looper, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, String str, zzf com_google_android_gms_common_internal_zzf) {
        super(context, looper, connectionCallbacks, onConnectionFailedListener, str, com_google_android_gms_common_internal_zzf);
        this.ala = new zzk(context, this.akH);
    }

    public void disconnect() {
        synchronized (this.ala) {
            if (isConnected()) {
                try {
                    this.ala.removeAllListeners();
                    this.ala.zzbqh();
                } catch (Throwable e) {
                    Log.e("LocationClientImpl", "Client disconnected before listeners could be cleaned up", e);
                }
            }
            super.disconnect();
        }
    }

    public Location getLastLocation() {
        return this.ala.getLastLocation();
    }

    public void zza(long j, PendingIntent pendingIntent) throws RemoteException {
        zzavf();
        zzaa.zzy(pendingIntent);
        zzaa.zzb(j >= 0, (Object) "detectionIntervalMillis must be >= 0");
        ((zzi) zzavg()).zza(j, true, pendingIntent);
    }

    public void zza(PendingIntent pendingIntent, com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status) throws RemoteException {
        zzavf();
        zzaa.zzb((Object) pendingIntent, (Object) "PendingIntent must be specified.");
        zzaa.zzb((Object) com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, (Object) "ResultHolder not provided.");
        ((zzi) zzavg()).zza(pendingIntent, new zzb(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status), getContext().getPackageName());
    }

    public void zza(PendingIntent pendingIntent, zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        this.ala.zza(pendingIntent, com_google_android_gms_location_internal_zzg);
    }

    public void zza(GeofencingRequest geofencingRequest, PendingIntent pendingIntent, com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status) throws RemoteException {
        zzavf();
        zzaa.zzb((Object) geofencingRequest, (Object) "geofencingRequest can't be null.");
        zzaa.zzb((Object) pendingIntent, (Object) "PendingIntent must be specified.");
        zzaa.zzb((Object) com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, (Object) "ResultHolder not provided.");
        ((zzi) zzavg()).zza(geofencingRequest, pendingIntent, new zza(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status));
    }

    public void zza(LocationCallback locationCallback, zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        this.ala.zza(locationCallback, com_google_android_gms_location_internal_zzg);
    }

    public void zza(LocationListener locationListener, zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        this.ala.zza(locationListener, com_google_android_gms_location_internal_zzg);
    }

    public void zza(LocationRequest locationRequest, PendingIntent pendingIntent, zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        this.ala.zza(locationRequest, pendingIntent, com_google_android_gms_location_internal_zzg);
    }

    public void zza(LocationRequest locationRequest, LocationListener locationListener, Looper looper, zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        synchronized (this.ala) {
            this.ala.zza(locationRequest, locationListener, looper, com_google_android_gms_location_internal_zzg);
        }
    }

    public void zza(LocationSettingsRequest locationSettingsRequest, com.google.android.gms.internal.zzqo.zzb<LocationSettingsResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_location_LocationSettingsResult, String str) throws RemoteException {
        boolean z = true;
        zzavf();
        zzaa.zzb(locationSettingsRequest != null, (Object) "locationSettingsRequest can't be null nor empty.");
        if (com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_location_LocationSettingsResult == null) {
            z = false;
        }
        zzaa.zzb(z, (Object) "listener can't be null.");
        ((zzi) zzavg()).zza(locationSettingsRequest, new zzc(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_location_LocationSettingsResult), str);
    }

    public void zza(LocationRequestInternal locationRequestInternal, LocationCallback locationCallback, Looper looper, zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        synchronized (this.ala) {
            this.ala.zza(locationRequestInternal, locationCallback, looper, com_google_android_gms_location_internal_zzg);
        }
    }

    public void zza(zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        this.ala.zza(com_google_android_gms_location_internal_zzg);
    }

    public void zza(List<String> list, com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status) throws RemoteException {
        zzavf();
        boolean z = list != null && list.size() > 0;
        zzaa.zzb(z, (Object) "geofenceRequestIds can't be null nor empty.");
        zzaa.zzb((Object) com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, (Object) "ResultHolder not provided.");
        ((zzi) zzavg()).zza((String[]) list.toArray(new String[0]), new zzb(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status), getContext().getPackageName());
    }

    public void zzb(PendingIntent pendingIntent) throws RemoteException {
        zzavf();
        zzaa.zzy(pendingIntent);
        ((zzi) zzavg()).zzb(pendingIntent);
    }

    public LocationAvailability zzbqg() {
        return this.ala.zzbqg();
    }

    public void zzcd(boolean z) throws RemoteException {
        this.ala.zzcd(z);
    }

    public void zzd(Location location) throws RemoteException {
        this.ala.zzd(location);
    }
}
