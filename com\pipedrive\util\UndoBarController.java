package com.pipedrive.util;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import com.pipedrive.R;
import com.pipedrive.logging.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class UndoBarController<T> {
    static final String TAG = UndoBarController.class.getSimpleName();
    private Handler mHideHandler = new Handler();
    private List<UndoBarRequest> mUndoBarRequests = Collections.synchronizedList(new ArrayList());
    private ViewGroup mUndoViewAnchor;

    public interface NoUndoListener<T> {
        void onNoUndo(T t);
    }

    public interface UndoListener<T> {
        void onUndo(T t, int i);
    }

    public UndoBarController<T> setUndoBarViewHolder(View viewIncludingUndobarLayout) {
        String tag = TAG + ".setUndoBarViewHolder()";
        if (viewIncludingUndobarLayout == null) {
            Log.w(tag, "View is null! Cannot create undo views! Expected behaviour?");
        } else {
            View undoViewAnchor = viewIncludingUndobarLayout.findViewById(R.id.undobarAnchor);
            if (undoViewAnchor == null) {
                Log.w(tag, "Layout does not include the required undo layout 'layout_undobar.xml'! Will not create undo views! Expected behaviour?");
            } else if (!(undoViewAnchor instanceof ViewGroup)) {
                Log.w(tag, "Undobar layout 'layout_undobar.xml' does not include required viewgroup for adding done views! Cannot create undo views! Expected behaviour?");
            }
            this.mUndoViewAnchor = (ViewGroup) undoViewAnchor;
        }
        return this;
    }

    public void showUndoBar(CharSequence message, T undoToken, UndoListener<T> undoListener, Context context) {
        showUndoBar(message, undoToken, undoListener, null, context);
    }

    public void showUndoBar(CharSequence message, T undoToken, UndoListener<T> undoListener, NoUndoListener<T> noUndoListener, Context context) {
        showUndoBar(false, message, (Object) undoToken, (UndoListener) undoListener, (NoUndoListener) noUndoListener, context);
    }

    public void showUndoBar(boolean immediate, CharSequence message, T undoToken, UndoListener<T> undoListener, NoUndoListener<T> noUndoListener, Context context) {
        showUndoBar(immediate, message, (Object) undoToken, 0, (UndoListener) undoListener, (NoUndoListener) noUndoListener, context);
    }

    public void showUndoBar(boolean immediate, CharSequence message, T undoToken, int tokenInt, UndoListener<T> undoListener, Context context) {
        showUndoBar(immediate, message, (Object) undoToken, tokenInt, (UndoListener) undoListener, null, context);
    }

    public void showUndoBar(boolean immediate, CharSequence message, T undoToken, int tokenInt, UndoListener<T> undoListener, NoUndoListener<T> noUndoListener, Context context) {
        showUndoBar(immediate ? 0 : context.getResources().getInteger(R.integer.undobar_hide_delay), message, (Object) undoToken, tokenInt, (UndoListener) undoListener, (NoUndoListener) noUndoListener, context);
    }

    private void showUndoBar(int delayInMillis, CharSequence message, T undoToken, int tokenInt, UndoListener<T> undoListener, NoUndoListener<T> noUndoListener, Context context) {
        String tag = TAG + ".showUndoBar()";
        if (context == null) {
            Log.w(tag, "Context is null! I need Context! Cannot show undo views! Expected behaviour?");
            return;
        }
        if (this.mUndoViewAnchor == null) {
            Log.w(tag, "Anchor for undo views was not correcty passed to the constructor! Will not show undo bar! Expected behaviour?");
        }
        Log.d(tag, String.format("Undo bar requested for undo token: %s with dealay in millis: %s", new Object[]{undoToken, Integer.valueOf(delayInMillis)}));
        synchronized (this.mUndoBarRequests) {
            Iterator<UndoBarRequest> iter = this.mUndoBarRequests.iterator();
            while (iter.hasNext()) {
                if (((UndoBarRequest) iter.next()).isProcessed()) {
                    iter.remove();
                }
            }
            this.mUndoBarRequests.add(new UndoBarRequest(this, delayInMillis, message, undoToken, tokenInt, undoListener, noUndoListener, this.mUndoViewAnchor, context));
        }
    }

    public void processAllPendingUndoBarsAsNoundos() {
        synchronized (this.mUndoBarRequests) {
            Iterator<UndoBarRequest> iter = this.mUndoBarRequests.iterator();
            while (iter.hasNext()) {
                UndoBarRequest undoBarRequest = (UndoBarRequest) iter.next();
                if (undoBarRequest.isProcessed()) {
                    iter.remove();
                } else {
                    undoBarRequest.processAtOnce();
                }
            }
        }
    }
}
