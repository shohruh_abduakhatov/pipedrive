package com.zendesk.sdk.model.helpcenter;

import android.support.annotation.NonNull;
import com.zendesk.util.CollectionUtils;
import java.util.List;

public class CategoriesResponse {
    private List<Category> categories;

    @NonNull
    public List<Category> getCategories() {
        return CollectionUtils.copyOf(this.categories);
    }
}
