package com.pipedrive.pipeline;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import com.pipedrive.NavigationActivity;
import com.pipedrive.R;
import com.pipedrive.adapter.filter.FilterContentPipelineAdapter;
import com.pipedrive.adapter.filter.FilterContentPipelineAdapter.FilterRow;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.ScreensMapper;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.deal.DealEditActivity;
import com.pipedrive.model.Filter;
import com.pipedrive.model.User;
import com.pipedrive.util.CursorHelper;
import com.pipedrive.util.FiltersUtil;
import com.pipedrive.util.UsersUtil;
import com.pipedrive.views.filter.FilterDrawerLayout;
import com.pipedrive.views.filter.FilterDrawerLayout.DrawerLayoutListener;
import com.pipedrive.views.filter.SelectedFilterLabel;
import com.pipedrive.views.stageindicator.StageIndicatorView;
import java.util.List;

public class PipelineActivity extends NavigationActivity implements PipelineView, PipelineDealListFragmentInterface {
    public static final String DEVICE_TOKEN = "deviceToken";
    @BindView(2131820590)
    View mAddButton;
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(@NonNull Context context, @NonNull Intent intent) {
            PipelineActivity.this.mPresenter.requestPipelines();
        }
    };
    @NonNull
    private FilterDrawerLayout mFilterDrawer;
    private boolean mFilterIsLoading = true;
    private MenuItem mFilterMenuItem;
    private final Handler mHandler = new Handler();
    @BindView(2131820806)
    StageIndicatorView mLineStageIndicator;
    @BindView(2131820809)
    TextView mMessage;
    private OnPageChangeListener mOnPageChangeListener = new OnPageChangeListener() {

        class UpdateRunnable implements Runnable {
            private final int mPosition;

            UpdateRunnable(int position) {
                this.mPosition = position;
            }

            public void run() {
                Fragment item = (Fragment) PipelineActivity.this.mPositionFragments.get(this.mPosition);
                if (item != null) {
                    ((PipelineDealListFragment) item).updateDataView();
                    ((PipelineDealListFragment) item).updateLoadingView();
                }
            }
        }

        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        public void onPageSelected(int position) {
        }

        public void onPageScrollStateChanged(int state) {
            PipelineActivity.this.mScrolling = state != 0;
            if (!PipelineActivity.this.mScrolling) {
                UpdateRunnable updateRunnable = new UpdateRunnable(PipelineActivity.this.mPager.getCurrentItem());
                PipelineActivity.this.mHandler.removeCallbacks(updateRunnable);
                PipelineActivity.this.mHandler.post(updateRunnable);
                PipelineActivity.this.getSession().setPipelineSelectedStagePosition(PipelineActivity.this.mPager.getCurrentItem());
            }
        }
    };
    @BindView(2131820736)
    ViewPager mPager;
    private PipelinePagerAdapter mPagerAdapter;
    @BindView(2131820802)
    View mPipelineProgress;
    @BindView(2131820803)
    Spinner mPipelineSpinner;
    private PipelineSpinnerAdapter mPipelineSpinnerAdapter;
    private SparseArray<PipelineDealListFragment> mPositionFragments = new SparseArray();
    private PipelinePresenter mPresenter;
    private boolean mScrolling = false;
    @BindView(2131820804)
    SelectedFilterLabel mSelectedFilterLabel;
    @BindView(2131820807)
    View mSeparator;
    private SparseArray<PipelineDealListFragment> mStageFragments = new SparseArray();
    @BindView(2131820805)
    View mStagesContent;
    @BindView(2131820808)
    View mStagesProgress;

    public static void startActivity(Activity activity) {
        ActivityCompat.startActivity(activity, getStartIntent(activity), null);
    }

    public static Intent getStartIntent(@NonNull Context context) {
        return new Intent(context, PipelineActivity.class).addFlags(67108864);
    }

    public void onResume() {
        super.onResume();
        this.mPresenter.bindView(this);
        this.mPresenter.requestPipelines();
        LocalBroadcastManager.getInstance(this).registerReceiver(this.mBroadcastReceiver, new IntentFilter(InvalidatePipelinesTask.ACTION_INVALIDATE_PIPELINE));
        setFilters();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mPresenter = new PipelinePresenterImpl(getSession());
        setContentView(R.layout.activity_pipeline);
        ButterKnife.bind(this);
        setupPipelineSpinner();
        setupViewPager();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.pipeline_menu, menu);
        this.mFilterMenuItem = menu.findItem(R.id.menu_filter);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_filter:
                openFilter();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openFilter() {
        this.mFilterDrawer.openDrawer();
        Analytics.hitScreen(ScreensMapper.SCREEN_NAME_PIPELINE_LIST_FILTER, this);
    }

    public void setContentView(int layoutResID) {
        this.mFilterDrawer = new FilterDrawerLayout(this, getLayoutInflater(), getLayoutInflater().inflate(layoutResID, new FrameLayout(this)), R.layout.layout_pipeline_filter, false);
        super.setContentView(this.mFilterDrawer);
    }

    private void setupPipelineSpinner() {
        this.mPipelineSpinnerAdapter = new PipelineSpinnerAdapter(this, null, true);
        this.mPipelineSpinner.setAdapter(this.mPipelineSpinnerAdapter);
    }

    private void setupViewPager() {
        this.mPagerAdapter = new PipelinePagerAdapter(getSupportFragmentManager(), null);
        this.mPager.setAdapter(this.mPagerAdapter);
        this.mPager.addOnPageChangeListener(this.mOnPageChangeListener);
        this.mLineStageIndicator.setViewPager(this.mPager);
    }

    protected void onPause() {
        this.mPresenter.unbindView();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(this.mBroadcastReceiver);
        super.onPause();
    }

    @OnItemSelected({2131820803})
    void onPipelineSelected(AdapterView<?> parent, View view, int position, long id) {
        Integer pipelineId = CursorHelper.getInteger((Cursor) parent.getItemAtPosition(position), PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID);
        if (pipelineId != null) {
            onPipelineChanged(pipelineId.intValue());
        }
    }

    private void onPipelineChanged(int pipelineId) {
        boolean pipelineWasChanged;
        if (((long) pipelineId) != getSession().getPipelineSelectedPipelineId(0)) {
            pipelineWasChanged = true;
        } else {
            pipelineWasChanged = false;
        }
        if (pipelineWasChanged) {
            getSession().setPipelineSelectedStagePosition(0);
        }
        this.mPresenter.requestAllPipelineStages(pipelineId);
    }

    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowHomeEnabled(false);
            supportActionBar.setDisplayShowTitleEnabled(false);
        }
    }

    public void onBackPressed() {
        boolean closeFilterOnly = this.mFilterDrawer.isDrawerOpen() && PipedriveApp.getSessionManager().hasActiveSession();
        if (closeFilterOnly) {
            this.mFilterDrawer.closeDrawer();
            setFilterSelection(this.mFilterDrawer);
            return;
        }
        super.onBackPressed();
    }

    public void setPipelines(Cursor pipelineCursor, int selection) {
        this.mPipelineSpinnerAdapter.changeCursor(pipelineCursor);
        this.mPipelineSpinner.setSelection(selection, false);
        this.mPipelineProgress.setVisibility(8);
        this.mPipelineSpinner.setVisibility(0);
        this.mMessage.setVisibility(8);
    }

    public void setStages(Session session, Cursor stagesCursor) {
        int i;
        removeCachedFragments();
        this.mPagerAdapter = new PipelinePagerAdapter(getSupportFragmentManager(), stagesCursor);
        this.mPager.setAdapter(this.mPagerAdapter);
        if (this.mPagerAdapter.getCount() > 0) {
            this.mPager.setCurrentItem(getSession().getPipelineSelectedStagePosition(0), true);
        }
        for (int i2 = 0; i2 < this.mStageFragments.size(); i2++) {
            this.mPresenter.requestDeals(this.mStageFragments.keyAt(i2));
        }
        this.mPager.setVisibility(this.mPagerAdapter.getCount() > 0 ? 0 : 4);
        View view = this.mSeparator;
        if (this.mPagerAdapter.getCount() == 1) {
            i = 0;
        } else {
            i = 8;
        }
        view.setVisibility(i);
        this.mAddButton.setVisibility(this.mPager.getVisibility());
        this.mStagesContent.setVisibility(0);
        this.mStagesProgress.setVisibility(8);
        this.mMessage.setVisibility(8);
    }

    private void removeCachedFragments() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        for (int i = 0; i < this.mStageFragments.size(); i++) {
            fragmentTransaction.remove((PipelineDealListFragment) this.mStageFragments.valueAt(i));
        }
        this.mStageFragments.clear();
        fragmentTransaction.commit();
    }

    public void setDeals(int stageId, @Nullable StageData stageData) {
        PipelineDealListFragment stageView = getStageView(stageId);
        if (stageView != null) {
            stageView.setData(stageData);
            if (!this.mScrolling) {
                stageView.updateDataView();
            }
        }
    }

    public void setDealsLoading(int stageId, boolean isLoading) {
        PipelineDealListFragment stageView = getStageView(stageId);
        if (stageView != null) {
            stageView.setLoading(isLoading);
            if (!this.mScrolling) {
                stageView.updateLoadingView();
            }
        }
    }

    public void setDealsFailedToDownload(int stageId) {
        setDeals(stageId, null);
    }

    public void setFilters() {
        setupFilterDrawer(this.mFilterDrawer);
        setupFilterLabel();
    }

    public void setFiltersLoading(boolean isLoading) {
        this.mFilterIsLoading = isLoading;
        invalidateOptionsMenu();
    }

    public void setPipelinesFailedToDownload() {
        this.mPipelineProgress.setVisibility(8);
        this.mPipelineSpinner.setVisibility(8);
        this.mStagesProgress.setVisibility(8);
        this.mStagesContent.setVisibility(8);
        this.mMessage.setText(R.string.something_went_wrong_while_downloading_your_pipelines_please_go_to_settings_view_and_trigger_the_manual_sync);
        this.mMessage.setVisibility(0);
    }

    public void setStagesFailedToDownload() {
        this.mStagesProgress.setVisibility(8);
        this.mStagesContent.setVisibility(8);
        this.mMessage.setText(R.string.no_stages_in_this_pipeline_you_can_edit_the_stages_for_this_pipeline_on_the_web_app);
        this.mMessage.setVisibility(0);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean menuItemVisible = !this.mFilterIsLoading;
        if (this.mFilterMenuItem != null) {
            this.mFilterMenuItem.setVisible(menuItemVisible);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Nullable
    private PipelineDealListFragment getStageView(int stageId) {
        return (PipelineDealListFragment) this.mStageFragments.get(stageId);
    }

    private void setupFilterDrawer(@NonNull final FilterDrawerLayout filterDrawer) {
        final ListView pipelineFilterListView = setupFilterContent(filterDrawer);
        if (pipelineFilterListView != null) {
            filterDrawer.setDrawerLayoutListener(new DrawerLayoutListener() {
                public boolean onClear() {
                    return PipelineActivity.this.filterCleared(pipelineFilterListView, filterDrawer);
                }

                public void onClose() {
                    PipelineActivity.this.onBackPressed();
                }
            });
        }
    }

    private void setupFilterLabel() {
        String selectedFilterName = FiltersUtil.getSelectedFilterName(getSession());
        if (FiltersUtil.isFilterSet(getSession())) {
            this.mSelectedFilterLabel.setLabel(selectedFilterName);
        } else {
            this.mSelectedFilterLabel.disableLabel();
        }
    }

    @Nullable
    private ListView setupFilterContent(@NonNull final FilterDrawerLayout filterDrawer) {
        if (!(filterDrawer.getDrawerContentView() instanceof ListView)) {
            return null;
        }
        final ListView filterContent = (ListView) filterDrawer.getDrawerContentView();
        filterContent.setAdapter(new FilterContentPipelineAdapter(this, FiltersUtil.getPipelineFilterContent(getSession())));
        setFilterSelection(filterDrawer);
        filterContent.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(@NonNull AdapterView<?> parent, @NonNull View view, int position, long id) {
                if (parent.getAdapter() instanceof FilterContentPipelineAdapter) {
                    PipelineActivity.this.filterApplied(filterContent, filterDrawer);
                }
            }
        });
        return filterContent;
    }

    private void setFilterSelection(@NonNull FilterDrawerLayout filterDrawer) {
        int currentFilterSelectionInSession = FiltersUtil.getPipelineFilterSpinnerIdx(getSession());
        getSession().getRuntimeCache().lastSelectedPipelineFilter = currentFilterSelectionInSession;
        if (filterDrawer.getDrawerContentView() instanceof ListView) {
            ListView filterContentListView = (ListView) filterDrawer.getDrawerContentView();
            filterContentListView.setItemChecked(currentFilterSelectionInSession, true);
            filterContentListView.smoothScrollToPosition(currentFilterSelectionInSession);
        }
        supportInvalidateOptionsMenu();
    }

    private boolean filterApplied(@NonNull ListView pipelineFilterListView, @NonNull FilterDrawerLayout filterDrawer) {
        boolean listViewSelectionIsValid;
        if (pipelineFilterListView.getCheckedItemPosition() != -1) {
            listViewSelectionIsValid = true;
        } else {
            listViewSelectionIsValid = false;
        }
        if (listViewSelectionIsValid) {
            try {
                filterItemSelected(pipelineFilterListView.getCheckedItemPosition(), false);
                return true;
            } finally {
                filterDrawer.closeDrawer();
            }
        } else {
            filterDrawer.closeDrawer();
            return false;
        }
    }

    private boolean filterCleared(@NonNull ListView pipelineFilterListView, @NonNull FilterDrawerLayout filterDrawer) {
        pipelineFilterListView.setItemChecked(FiltersUtil.getFilterSpinnerIdxAllUsers(), true);
        return filterApplied(pipelineFilterListView, filterDrawer);
    }

    private boolean filterItemSelected(int itemPositionFromAdapter, boolean forcedItemPositionRefresh) {
        boolean alreadyChosenItem = !forcedItemPositionRefresh && getSession().getRuntimeCache().lastSelectedPipelineFilter == itemPositionFromAdapter;
        if (alreadyChosenItem) {
            return true;
        }
        long pipelineId = getSession().getPipelineSelectedPipelineId(-1);
        List<FilterRow> pipelineFilterContent = FiltersUtil.getPipelineFilterContent(getSession());
        boolean selectionWithinFilterContent = itemPositionFromAdapter >= 0 && itemPositionFromAdapter < pipelineFilterContent.size();
        if (selectionWithinFilterContent) {
            FilterRow filterRow = (FilterRow) pipelineFilterContent.get(itemPositionFromAdapter);
            User user = filterRow.getUser();
            boolean isUserSelection = user != null;
            Filter filter = filterRow.getFilter();
            boolean isFilterSelection = filter != null;
            if (isUserSelection || isFilterSelection) {
                if (isUserSelection) {
                    UsersUtil.updateUserSettingsSelectedFilterForUser(getSession(), pipelineId, user.getPipedriveIdOrNull(), user.getPipedriveIdOrNull() == null);
                } else {
                    UsersUtil.updateUserSettingsSelectedFilterForFilter(getSession(), pipelineId, filter.getPipedriveIdOrNull(), false);
                }
                getSession().getRuntimeCache().lastSelectedPipelineFilter = itemPositionFromAdapter;
                this.mPresenter.onFilterChanged();
                invalidateOptionsMenu();
                setupFilterLabel();
                return true;
            }
        }
        invalidateOptionsMenu();
        return false;
    }

    @OnClick({2131820590})
    void onAddClicked() {
        DealEditActivity.startActivityForAddingDealToStage(this, this.mPagerAdapter.getStageIdForPosition(this.mPager.getCurrentItem()));
    }

    public void attach(int stageId, int position, PipelineDealListFragment pipelineDealListFragment) {
        this.mStageFragments.put(stageId, pipelineDealListFragment);
        this.mPositionFragments.put(position, pipelineDealListFragment);
        this.mPresenter.requestDeals(stageId);
    }

    public void detach(int stageId, int position, PipelineDealListFragment pipelineDealListFragment) {
        this.mStageFragments.remove(stageId);
        this.mPositionFragments.remove(position);
    }

    public void refresh(int stageId) {
        this.mPresenter.onStageRefresh(stageId);
        int size = this.mStageFragments.size();
        for (int i = 0; i < size; i++) {
            this.mPresenter.requestDeals(this.mStageFragments.keyAt(i));
        }
    }

    public void requery(@NonNull Integer stageId) {
        this.mPresenter.requery(stageId);
    }
}
