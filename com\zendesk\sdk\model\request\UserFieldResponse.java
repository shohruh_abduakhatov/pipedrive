package com.zendesk.sdk.model.request;

import android.support.annotation.NonNull;
import com.zendesk.util.CollectionUtils;
import java.util.List;

public class UserFieldResponse {
    private List<UserField> userFields;

    @NonNull
    public List<UserField> getUserFields() {
        return CollectionUtils.copyOf(this.userFields);
    }
}
