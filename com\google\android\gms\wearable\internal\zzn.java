package com.google.android.gms.wearable.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzn implements Creator<ChannelEventParcelable> {
    static void zza(ChannelEventParcelable channelEventParcelable, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zzc(parcel, 1, channelEventParcelable.mVersionCode);
        zzb.zza(parcel, 2, channelEventParcelable.aTp, i, false);
        zzb.zzc(parcel, 3, channelEventParcelable.type);
        zzb.zzc(parcel, 4, channelEventParcelable.aTn);
        zzb.zzc(parcel, 5, channelEventParcelable.aTo);
        zzb.zzaj(parcel, zzcs);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzux(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzadt(i);
    }

    public ChannelEventParcelable[] zzadt(int i) {
        return new ChannelEventParcelable[i];
    }

    public ChannelEventParcelable zzux(Parcel parcel) {
        int i = 0;
        int zzcr = zza.zzcr(parcel);
        ChannelImpl channelImpl = null;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    i4 = zza.zzg(parcel, zzcq);
                    break;
                case 2:
                    channelImpl = (ChannelImpl) zza.zza(parcel, zzcq, ChannelImpl.CREATOR);
                    break;
                case 3:
                    i3 = zza.zzg(parcel, zzcq);
                    break;
                case 4:
                    i2 = zza.zzg(parcel, zzcq);
                    break;
                case 5:
                    i = zza.zzg(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new ChannelEventParcelable(i4, channelImpl, i3, i2, i);
        }
        throw new zza.zza("Overread allowed size end=" + zzcr, parcel);
    }
}
