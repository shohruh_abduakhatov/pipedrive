package com.zendesk.sdk.model.push;

public class AnonymousPushRegistrationRequest extends PushRegistrationRequest {
    private String sdkGuid;

    public void setSdkGuid(String sdkGuid) {
        this.sdkGuid = sdkGuid;
    }

    public String getSdkGuid() {
        return this.sdkGuid;
    }
}
