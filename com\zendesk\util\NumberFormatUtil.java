package com.zendesk.util;

import com.google.maps.android.heatmaps.WeightedLatLng;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.TreeMap;

public class NumberFormatUtil {
    private static long MILLION_PRECISION = 1000000;
    private static final NavigableMap<Long, NumberSuffix> SUFFIXES = new TreeMap();

    public enum NumberSuffix {
        NONE(""),
        KILO("k"),
        MEGA("M"),
        GIGA("G"),
        TERA("T"),
        PETA("P"),
        EXA("E");
        
        private String suffix;

        private NumberSuffix(String suffix) {
            this.suffix = suffix;
        }

        public String getSuffix() {
            return this.suffix;
        }
    }

    public interface SuffixFormatDelegate {
        String getSuffix(NumberSuffix numberSuffix);
    }

    private NumberFormatUtil() {
    }

    static {
        SUFFIXES.put(Long.valueOf(1000), NumberSuffix.KILO);
        SUFFIXES.put(Long.valueOf(1000000), NumberSuffix.MEGA);
        SUFFIXES.put(Long.valueOf(1000000000), NumberSuffix.GIGA);
        SUFFIXES.put(Long.valueOf(1000000000000L), NumberSuffix.TERA);
        SUFFIXES.put(Long.valueOf(1000000000000000L), NumberSuffix.PETA);
        SUFFIXES.put(Long.valueOf(1000000000000000000L), NumberSuffix.EXA);
    }

    public static String format(long value) {
        return processValue(value, null);
    }

    public static String format(long value, SuffixFormatDelegate suffixFormatDelegate) {
        return processValue(value, suffixFormatDelegate);
    }

    private static String processValue(long value, SuffixFormatDelegate suffixFormatDelegate) {
        if (value == Long.MIN_VALUE) {
            return processValue(-9223372036854775807L, suffixFormatDelegate);
        }
        boolean isNegative = value < 0;
        if (isNegative) {
            value = -value;
        }
        if (value < 1000) {
            return formatValue(stringValue((double) value), NumberSuffix.NONE, suffixFormatDelegate);
        }
        long truncated;
        Entry<Long, NumberSuffix> e = SUFFIXES.floorEntry(Long.valueOf(value));
        Long divideBy = (Long) e.getKey();
        NumberSuffix suffix = (NumberSuffix) e.getValue();
        if (divideBy.longValue() <= MILLION_PRECISION) {
            truncated = (long) Math.ceil(((double) value) / (((double) divideBy.longValue()) / 10.0d));
        } else {
            truncated = value / (divideBy.longValue() / 10);
        }
        boolean hasDecimal = truncated < 100 && ((double) truncated) / 10.0d != ((double) (truncated / 10));
        double finalValue = hasDecimal ? ((double) truncated) / 10.0d : (double) (truncated / 10);
        if (isNegative) {
            finalValue = -finalValue;
        }
        return formatValue(stringValue(finalValue), suffix, suffixFormatDelegate);
    }

    private static String formatValue(String value, NumberSuffix numberSuffix, SuffixFormatDelegate suffixFormatDelegate) {
        String suffix = numberSuffix.getSuffix();
        if (suffixFormatDelegate != null) {
            suffix = suffixFormatDelegate.getSuffix(numberSuffix);
        }
        return String.format(Locale.US, "%1$s%2$s", new Object[]{value, suffix});
    }

    private static String stringValue(double value) {
        if (value % WeightedLatLng.DEFAULT_INTENSITY == 0.0d) {
            return String.format(Locale.US, "%1.0f", new Object[]{Double.valueOf(value)});
        }
        return String.format(Locale.US, "%.1f", new Object[]{Double.valueOf(value)});
    }
}
