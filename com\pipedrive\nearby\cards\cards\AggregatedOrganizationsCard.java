package com.pipedrive.nearby.cards.cards;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.ViewGroup;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.cards.adapter.CardAdapterForAggregatedItemsList;
import com.pipedrive.nearby.cards.adapter.OrganizationCardAdapterForAggregatedItemsList;
import com.pipedrive.nearby.model.NearbyItem;
import com.pipedrive.nearby.model.OrganizationNearbyItem;
import java.util.List;
import rx.Observable;
import rx.functions.Func1;

public class AggregatedOrganizationsCard extends AggregatedCard<OrganizationNearbyItem> {
    public AggregatedOrganizationsCard(@NonNull ViewGroup parent, @LayoutRes @NonNull Integer layoutResId) {
        super(parent, layoutResId);
    }

    Observable<List<OrganizationNearbyItem>> getSingleItemsList(@NonNull final OrganizationNearbyItem nearbyItem) {
        return nearbyItem.getItemSqlIds().map(new Func1<Long, OrganizationNearbyItem>() {
            public OrganizationNearbyItem call(Long aLong) {
                return new OrganizationNearbyItem(nearbyItem.getLatitude(), nearbyItem.getLongitude(), nearbyItem.getAddress(), aLong);
            }
        }).toList();
    }

    @NonNull
    Observable<String> getAggregatedInformation(@NonNull Session session, @NonNull OrganizationNearbyItem nearbyItem) {
        return nearbyItem.getOpenDealCount(session).map(new Func1<Integer, String>() {
            public String call(Integer openDealCount) {
                return AggregatedOrganizationsCard.this.getResources().getQuantityString(R.plurals.cards_open_deals, openDealCount.intValue(), new Object[]{openDealCount});
            }
        });
    }

    @NonNull
    Integer getQuantityStringResId() {
        return Integer.valueOf(R.plurals.organizations);
    }

    @NonNull
    CardAdapterForAggregatedItemsList getAdapter(@NonNull List<? extends NearbyItem> nearbyItems) {
        return new OrganizationCardAdapterForAggregatedItemsList(this.session, nearbyItems);
    }
}
