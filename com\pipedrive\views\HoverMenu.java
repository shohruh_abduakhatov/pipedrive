package com.pipedrive.views;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import com.pipedrive.R;
import java.security.InvalidParameterException;

public class HoverMenu {
    private static final int SHADOW_PADDING_DIP = 8;
    private Context mContext = null;
    private PopupWindow mPopupWindow = null;
    private boolean mUseShadowEdges = false;

    public HoverMenu(Context context) {
        if (context == null) {
            throw new InvalidParameterException("Context cannot be null! I need context!");
        }
        this.mContext = context;
        this.mPopupWindow = new PopupWindow(this.mContext);
    }

    public final HoverMenu createMenu(ArrayAdapter<String> adapter, OnItemClickListener listener, View viewWidth, int listViewResource, boolean useShadowEdges) {
        int wpx = viewWidth.getLayoutParams().width;
        if (wpx == -2) {
            wpx = -1;
        }
        int wdp = wpx;
        if (wpx != -1 && wpx >= 0) {
            wdp = (int) (((float) wpx) / this.mContext.getResources().getDisplayMetrics().density);
        }
        return createMenu((ArrayAdapter) adapter, listener, wdp, listViewResource, useShadowEdges);
    }

    public final HoverMenu createMenu(ArrayAdapter<String> adapter, OnItemClickListener listener, int menuWidthInDIP, int listViewResource, boolean useShadowEdges) {
        ListView lv = (ListView) ((LayoutInflater) this.mContext.getSystemService("layout_inflater")).inflate(listViewResource, null);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(listener);
        this.mUseShadowEdges = useShadowEdges;
        this.mPopupWindow.setBackgroundDrawable(this.mContext.getResources().getDrawable(useShadowEdges ? R.drawable.abc_menu_dropdown_panel_holo_light : 17170445));
        this.mPopupWindow.setFocusable(true);
        this.mPopupWindow.setWidth(-1);
        if (menuWidthInDIP >= 0) {
            if (useShadowEdges) {
                menuWidthInDIP += 16;
            }
            int menuWidthInPX = (int) TypedValue.applyDimension(1, (float) menuWidthInDIP, this.mContext.getResources().getDisplayMetrics());
            if (menuWidthInPX >= this.mContext.getResources().getDisplayMetrics().widthPixels) {
                this.mPopupWindow.setWidth(-1);
            } else {
                this.mPopupWindow.setWidth(menuWidthInPX);
            }
        }
        this.mPopupWindow.setHeight(-2);
        this.mPopupWindow.setContentView(lv);
        return this;
    }

    public final HoverMenu showAsDropDown(View anchor) {
        if (this.mUseShadowEdges) {
            int px = (int) TypedValue.applyDimension(1, 8.0f, this.mContext.getResources().getDisplayMetrics());
            this.mPopupWindow.showAsDropDown(anchor, -px, -px);
        } else {
            this.mPopupWindow.showAsDropDown(anchor);
        }
        return this;
    }

    public final HoverMenu dismiss() {
        if (this.mPopupWindow != null) {
            this.mPopupWindow.dismiss();
        }
        return this;
    }
}
