package com.pipedrive.views.profilepicture;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import com.pipedrive.R;
import com.pipedrive.model.User;
import com.pipedrive.util.StringUtils;

public class ProfilePictureRoundUser extends ProfilePictureRound {
    public /* bridge */ /* synthetic */ void loadPicture(@Nullable String str, @Nullable String str2) {
        super.loadPicture(str, str2);
    }

    public /* bridge */ /* synthetic */ void setTextSize(float f) {
        super.setTextSize(f);
    }

    public ProfilePictureRoundUser(Context context) {
        super(context);
    }

    public ProfilePictureRoundUser(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProfilePictureRoundUser(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected void setUpLayout(Context context) {
        LayoutInflater.from(context).inflate(R.layout.view_profilepicture_blue, this);
    }

    public boolean loadPicture(@Nullable User user) {
        boolean canLoadPicture;
        if (user != null) {
            canLoadPicture = true;
        } else {
            canLoadPicture = false;
        }
        if (!canLoadPicture) {
            return false;
        }
        super.loadPicture(StringUtils.getInitials(user), user.getIconUrl());
        return true;
    }
}
