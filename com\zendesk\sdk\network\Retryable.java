package com.zendesk.sdk.network;

import android.view.View.OnClickListener;

public interface Retryable {
    void onRetryAvailable(String str, OnClickListener onClickListener);

    void onRetryUnavailable();
}
