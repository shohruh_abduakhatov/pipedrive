package com.google.android.gms.internal;

import com.newrelic.agent.android.instrumentation.HttpInstrumentation;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.zendesk.service.HttpConstants;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

class zzafy implements zzafz {
    private HttpURLConnection aLF;

    zzafy() {
    }

    private InputStream zzd(HttpURLConnection httpURLConnection) throws IOException {
        int responseCode = httpURLConnection.getResponseCode();
        if (responseCode == 200) {
            return httpURLConnection.getInputStream();
        }
        String str = "Bad response: " + responseCode;
        if (responseCode == HttpConstants.HTTP_NOT_FOUND) {
            throw new FileNotFoundException(str);
        } else if (responseCode == HttpConstants.HTTP_UNAVAILABLE) {
            throw new zzagb(str);
        } else {
            throw new IOException(str);
        }
    }

    private void zze(HttpURLConnection httpURLConnection) {
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
    }

    public void close() {
        zze(this.aLF);
    }

    public InputStream zzqz(String str) throws IOException {
        this.aLF = zzra(str);
        return zzd(this.aLF);
    }

    HttpURLConnection zzra(String str) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) HttpInstrumentation.openConnection(new URL(str).openConnection());
        httpURLConnection.setReadTimeout(BaseImageDownloader.DEFAULT_HTTP_READ_TIMEOUT);
        httpURLConnection.setConnectTimeout(BaseImageDownloader.DEFAULT_HTTP_READ_TIMEOUT);
        return httpURLConnection;
    }
}
