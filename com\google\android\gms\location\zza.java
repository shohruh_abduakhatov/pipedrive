package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.WorkSource;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zza implements Creator<ActivityRecognitionRequest> {
    static void zza(ActivityRecognitionRequest activityRecognitionRequest, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zza(parcel, 1, activityRecognitionRequest.getIntervalMillis());
        zzb.zza(parcel, 2, activityRecognitionRequest.zzbps());
        zzb.zza(parcel, 3, activityRecognitionRequest.zzbpt(), i, false);
        zzb.zza(parcel, 4, activityRecognitionRequest.getTag(), false);
        zzb.zza(parcel, 5, activityRecognitionRequest.zzbpu(), false);
        zzb.zza(parcel, 6, activityRecognitionRequest.zzbpv());
        zzb.zza(parcel, 7, activityRecognitionRequest.getAccountName(), false);
        zzb.zzc(parcel, 1000, activityRecognitionRequest.getVersionCode());
        zzb.zza(parcel, 8, activityRecognitionRequest.zzbpw());
        zzb.zzaj(parcel, zzcs);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzno(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzuf(i);
    }

    public ActivityRecognitionRequest zzno(Parcel parcel) {
        long j = 0;
        boolean z = false;
        String str = null;
        int zzcr = com.google.android.gms.common.internal.safeparcel.zza.zzcr(parcel);
        int[] iArr = null;
        String str2 = null;
        WorkSource workSource = null;
        boolean z2 = false;
        long j2 = 0;
        int i = 0;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = com.google.android.gms.common.internal.safeparcel.zza.zzcq(parcel);
            switch (com.google.android.gms.common.internal.safeparcel.zza.zzgu(zzcq)) {
                case 1:
                    j2 = com.google.android.gms.common.internal.safeparcel.zza.zzi(parcel, zzcq);
                    break;
                case 2:
                    z2 = com.google.android.gms.common.internal.safeparcel.zza.zzc(parcel, zzcq);
                    break;
                case 3:
                    workSource = (WorkSource) com.google.android.gms.common.internal.safeparcel.zza.zza(parcel, zzcq, WorkSource.CREATOR);
                    break;
                case 4:
                    str2 = com.google.android.gms.common.internal.safeparcel.zza.zzq(parcel, zzcq);
                    break;
                case 5:
                    iArr = com.google.android.gms.common.internal.safeparcel.zza.zzw(parcel, zzcq);
                    break;
                case 6:
                    z = com.google.android.gms.common.internal.safeparcel.zza.zzc(parcel, zzcq);
                    break;
                case 7:
                    str = com.google.android.gms.common.internal.safeparcel.zza.zzq(parcel, zzcq);
                    break;
                case 8:
                    j = com.google.android.gms.common.internal.safeparcel.zza.zzi(parcel, zzcq);
                    break;
                case 1000:
                    i = com.google.android.gms.common.internal.safeparcel.zza.zzg(parcel, zzcq);
                    break;
                default:
                    com.google.android.gms.common.internal.safeparcel.zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new ActivityRecognitionRequest(i, j2, z2, workSource, str2, iArr, z, str, j);
        }
        throw new com.google.android.gms.common.internal.safeparcel.zza.zza("Overread allowed size end=" + zzcr, parcel);
    }

    public ActivityRecognitionRequest[] zzuf(int i) {
        return new ActivityRecognitionRequest[i];
    }
}
