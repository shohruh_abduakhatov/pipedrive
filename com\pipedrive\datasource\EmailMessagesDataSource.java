package com.pipedrive.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.email.EmailMessage;
import com.pipedrive.model.email.EmailParticipant;
import com.pipedrive.model.email.EmailPosition;
import com.pipedrive.model.email.EmailThread;
import com.pipedrive.util.CursorHelper;
import java.util.ArrayList;
import java.util.List;

public class EmailMessagesDataSource extends BaseDataSource<EmailMessage> {
    private EmailAttachmentsDataSource mEmailAttachmentsDataSource;
    private EmailParticipantsDataSource mEmailParticipantsDataSource;
    private EmailThreadsDataSource mEmailThreadsDataSource;

    public EmailMessagesDataSource(SQLiteDatabase dbconn) {
        super(dbconn);
    }

    private EmailParticipantsDataSource getEmailParticipantsDataSource() {
        if (this.mEmailParticipantsDataSource != null) {
            return this.mEmailParticipantsDataSource;
        }
        EmailParticipantsDataSource emailParticipantsDataSource = new EmailParticipantsDataSource(getTransactionalDBConnection());
        this.mEmailParticipantsDataSource = emailParticipantsDataSource;
        return emailParticipantsDataSource;
    }

    private EmailThreadsDataSource getEmailThreadsDataSource() {
        if (this.mEmailThreadsDataSource != null) {
            return this.mEmailThreadsDataSource;
        }
        EmailThreadsDataSource emailThreadsDataSource = new EmailThreadsDataSource(getTransactionalDBConnection());
        this.mEmailThreadsDataSource = emailThreadsDataSource;
        return emailThreadsDataSource;
    }

    private EmailAttachmentsDataSource getEmailAttachmentsDataSource() {
        if (this.mEmailAttachmentsDataSource != null) {
            return this.mEmailAttachmentsDataSource;
        }
        EmailAttachmentsDataSource emailAttachmentsDataSource = new EmailAttachmentsDataSource(getTransactionalDBConnection());
        this.mEmailAttachmentsDataSource = emailAttachmentsDataSource;
        return emailAttachmentsDataSource;
    }

    @NonNull
    protected String[] getAllColumns() {
        return new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_PIPEDRIVE_ID, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_THREAD_ID, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_SUBJECT, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_SUMMARY, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_BODY, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_MESSAGE_TIME, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_ADD_TIME, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_UPDATE_TIME, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_ACTIVE, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_DRAFT, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_SENDERS_DISPLAY_STRING, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_RECEPIENTS_DISPLAY_STRING, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_ATTACHMENT_COUNT};
    }

    @NonNull
    protected String getColumnNameForPipedriveId() {
        return PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_PIPEDRIVE_ID;
    }

    @NonNull
    protected String getColumnNameForSqlId() {
        return PipeSQLiteHelper.COLUMN_ID;
    }

    @NonNull
    protected String getTableName() {
        return PipeSQLiteHelper.TABLE_EMAIL_MESSAGES;
    }

    @NonNull
    protected ContentValues getContentValues(@NonNull EmailMessage emailMessage) {
        ContentValues contentValues = super.getContentValues(emailMessage);
        if (emailMessage.getThread() != null && emailMessage.getThread().isStored()) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_THREAD_ID, Long.valueOf(emailMessage.getThread().getSqlId()));
        }
        if (emailMessage.getSubject() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_SUBJECT, emailMessage.getSubject());
        }
        if (emailMessage.getSummary() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_SUMMARY, emailMessage.getSummary());
        }
        if (emailMessage.getBody() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_BODY, emailMessage.getBody());
        }
        if (emailMessage.getMessageTime() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_MESSAGE_TIME, Long.valueOf(emailMessage.getMessageTime().getRepresentationInUnixTime()));
        }
        if (emailMessage.getAddTime() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_ADD_TIME, Long.valueOf(emailMessage.getAddTime().getRepresentationInUnixTime()));
        }
        if (emailMessage.getUpdateTime() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_UPDATE_TIME, Long.valueOf(emailMessage.getUpdateTime().getRepresentationInUnixTime()));
        }
        if (emailMessage.getActive() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_ACTIVE, emailMessage.getActive());
        }
        if (emailMessage.getDraft() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_DRAFT, emailMessage.getDraft());
        }
        if (emailMessage.getSendersDisplayString() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_SENDERS_DISPLAY_STRING, emailMessage.getSendersDisplayString());
        }
        if (emailMessage.getRecepientsDisplayString() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_RECEPIENTS_DISPLAY_STRING, emailMessage.getRecepientsDisplayString());
        }
        if (emailMessage.getAttachmentCount() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_ATTACHMENT_COUNT, emailMessage.getAttachmentCount());
        }
        return contentValues;
    }

    public long createOrUpdate(EmailMessage emailMessage) {
        long sqlId = super.createOrUpdate((BaseDatasourceEntity) emailMessage);
        if (emailMessage.isStored()) {
            storeParticipants(emailMessage);
            storeAttachments(emailMessage);
        }
        return sqlId;
    }

    private void storeAttachments(EmailMessage emailMessage) {
        if (emailMessage != null && emailMessage.isStored()) {
            getEmailAttachmentsDataSource().createOrUpdateAttachments(emailMessage);
        }
    }

    private void storeParticipants(EmailMessage emailMessage) {
        storeParticipants(Long.valueOf(emailMessage.getSqlId()), emailMessage.getFrom(), EmailPosition.FROM);
        storeParticipants(Long.valueOf(emailMessage.getSqlId()), emailMessage.getTo(), EmailPosition.TO);
        storeParticipants(Long.valueOf(emailMessage.getSqlId()), emailMessage.getCc(), EmailPosition.CC);
        storeParticipants(Long.valueOf(emailMessage.getSqlId()), emailMessage.getBcc(), EmailPosition.BCC);
    }

    private void storeParticipants(@Nullable Long messageSqlId, @Nullable List<EmailParticipant> participantList, @Nullable EmailPosition emailPosition) {
        if (messageSqlId != null && participantList != null && !participantList.isEmpty() && emailPosition != null) {
            beginTransactionNonExclusive();
            for (EmailParticipant participant : participantList) {
                if (participant != null) {
                    getEmailParticipantsDataSource().createOrUpdate(participant);
                    if (participant.isStored()) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_PARTICIPANTS_TO_MESSAGE_EMAIL_MESSAGE_ID, messageSqlId);
                        contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_PARTICIPANTS_TO_MESSAGE_PARTICIPANT_ID, Long.valueOf(participant.getSqlId()));
                        contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_PARTICIPANTS_TO_MESSAGE_POSITION, emailPosition.toString());
                        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
                        String str = PipeSQLiteHelper.TABLE_EMAIL_PARTICIPANTS_TO_MESSAGES;
                        String str2 = "email_participants_to_messages_email_message_id=? and email_participants_to_messages_participant_id=? and email_participants_to_messages_email_position=?";
                        String[] strArr = new String[]{String.valueOf(messageSqlId), String.valueOf(participant.getSqlId()), emailPosition.toString()};
                        if ((!(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.update(str, contentValues, str2, strArr) : SQLiteInstrumentation.update(transactionalDBConnection, str, contentValues, str2, strArr)) < 1) {
                            transactionalDBConnection = getTransactionalDBConnection();
                            str = PipeSQLiteHelper.TABLE_EMAIL_PARTICIPANTS_TO_MESSAGES;
                            if (transactionalDBConnection instanceof SQLiteDatabase) {
                                SQLiteInstrumentation.insert(transactionalDBConnection, str, null, contentValues);
                            } else {
                                transactionalDBConnection.insert(str, null, contentValues);
                            }
                        }
                    }
                }
            }
            commit();
        }
    }

    @Nullable
    protected EmailMessage deflateCursor(@NonNull Cursor cursor) {
        EmailMessage emailMessage = new EmailMessage();
        Long sqlId = CursorHelper.getLong(cursor, getColumnNameForSqlId());
        if (sqlId != null) {
            emailMessage.setSqlId(sqlId.longValue());
        }
        Integer pipedriveId = CursorHelper.getInteger(cursor, getColumnNameForPipedriveId());
        if (pipedriveId != null) {
            emailMessage.setPipedriveId(pipedriveId.intValue());
        }
        Long threadSqlId = CursorHelper.getLong(cursor, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_THREAD_ID);
        if (threadSqlId != null) {
            emailMessage.setThread((EmailThread) getEmailThreadsDataSource().findBySqlId(threadSqlId.longValue()));
        }
        emailMessage.setSubject(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_SUBJECT));
        emailMessage.setSummary(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_SUMMARY));
        emailMessage.setBody(CursorHelper.getBlob(cursor, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_BODY));
        emailMessage.setMessageTime(CursorHelper.getPipedriveDateTimeFromColumnHeldInSeconds(cursor, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_MESSAGE_TIME));
        emailMessage.setAddTime(CursorHelper.getPipedriveDateTimeFromColumnHeldInSeconds(cursor, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_ADD_TIME));
        emailMessage.setUpdateTime(CursorHelper.getPipedriveDateTimeFromColumnHeldInSeconds(cursor, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_UPDATE_TIME));
        emailMessage.setActive(CursorHelper.getBoolean(cursor, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_ACTIVE));
        emailMessage.setDraft(CursorHelper.getBoolean(cursor, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_DRAFT));
        emailMessage.setAttachmentCount(CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_EMAIL_MESSAGES_ATTACHMENT_COUNT));
        readParticipants(emailMessage);
        readAttachments(emailMessage);
        return emailMessage;
    }

    private void readAttachments(@NonNull EmailMessage emailMessage) {
        if (emailMessage.isStored()) {
            emailMessage.setAttachments(getEmailAttachmentsDataSource().getAttachmentsForMessage(emailMessage.getSqlId()));
        }
    }

    private void readParticipants(@NonNull EmailMessage emailMessage) {
        if (emailMessage.isStored()) {
            emailMessage.setFrom(getParticipants(Long.valueOf(emailMessage.getSqlId()), EmailPosition.FROM));
            emailMessage.setTo(getParticipants(Long.valueOf(emailMessage.getSqlId()), EmailPosition.TO));
            emailMessage.setCc(getParticipants(Long.valueOf(emailMessage.getSqlId()), EmailPosition.CC));
            emailMessage.setBcc(getParticipants(Long.valueOf(emailMessage.getSqlId()), EmailPosition.BCC));
        }
    }

    private List<EmailParticipant> getParticipants(@NonNull Long messageSqlId, @NonNull EmailPosition position) {
        Cursor cursor;
        List<EmailParticipant> list = new ArrayList();
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String str = PipeSQLiteHelper.TABLE_EMAIL_PARTICIPANTS_TO_MESSAGES;
        String[] strArr = new String[]{PipeSQLiteHelper.COLUMN_EMAIL_PARTICIPANTS_TO_MESSAGE_PARTICIPANT_ID};
        String str2 = "email_participants_to_messages_email_message_id=? and email_participants_to_messages_email_position=?";
        String[] strArr2 = new String[]{String.valueOf(messageSqlId), position.toString()};
        if (transactionalDBConnection instanceof SQLiteDatabase) {
            cursor = SQLiteInstrumentation.query(transactionalDBConnection, str, strArr, str2, strArr2, null, null, null);
        } else {
            cursor = transactionalDBConnection.query(str, strArr, str2, strArr2, null, null, null);
        }
        try {
            cursor.moveToPosition(-1);
            while (cursor.moveToNext()) {
                EmailParticipant participant = (EmailParticipant) getEmailParticipantsDataSource().findBySqlId(cursor.getLong(0));
                if (participant != null) {
                    list.add(participant);
                }
            }
            return list;
        } finally {
            cursor.close();
        }
    }
}
