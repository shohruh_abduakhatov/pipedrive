package com.google.android.gms.location.internal;

import android.app.PendingIntent;
import android.content.ContentProviderClient;
import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.zzh;
import com.google.android.gms.location.zzi;
import java.util.HashMap;
import java.util.Map;

public class zzk {
    private Map<LocationListener, zzc> VB = new HashMap();
    private final zzp<zzi> akH;
    private ContentProviderClient akU = null;
    private boolean akV = false;
    private Map<LocationCallback, zza> akW = new HashMap();
    private final Context mContext;

    private static class zza extends com.google.android.gms.location.zzh.zza {
        private Handler akX;

        zza(final LocationCallback locationCallback, Looper looper) {
            if (looper == null) {
                looper = Looper.myLooper();
                zzaa.zza(looper != null, (Object) "Can't create handler inside thread that has not called Looper.prepare()");
            }
            this.akX = new Handler(this, looper) {
                final /* synthetic */ zza akY;

                public void handleMessage(Message message) {
                    switch (message.what) {
                        case 0:
                            locationCallback.onLocationResult((LocationResult) message.obj);
                            return;
                        case 1:
                            locationCallback.onLocationAvailability((LocationAvailability) message.obj);
                            return;
                        default:
                            return;
                    }
                }
            };
        }

        private synchronized void zzb(int i, Object obj) {
            if (this.akX == null) {
                Log.e("LocationClientHelper", "Received a data in client after calling removeLocationUpdates.");
            } else {
                Message obtain = Message.obtain();
                obtain.what = i;
                obtain.obj = obj;
                this.akX.sendMessage(obtain);
            }
        }

        public void onLocationAvailability(LocationAvailability locationAvailability) {
            zzb(1, locationAvailability);
        }

        public void onLocationResult(LocationResult locationResult) {
            zzb(0, locationResult);
        }

        public synchronized void release() {
            this.akX = null;
        }
    }

    private static class zzb extends Handler {
        private final LocationListener akZ;

        public zzb(LocationListener locationListener) {
            this.akZ = locationListener;
        }

        public zzb(LocationListener locationListener, Looper looper) {
            super(looper);
            this.akZ = locationListener;
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    this.akZ.onLocationChanged(new Location((Location) message.obj));
                    return;
                default:
                    Log.e("LocationClientHelper", "unknown message in LocationHandler.handleMessage");
                    return;
            }
        }
    }

    private static class zzc extends com.google.android.gms.location.zzi.zza {
        private Handler akX;

        zzc(LocationListener locationListener, Looper looper) {
            if (looper == null) {
                zzaa.zza(Looper.myLooper() != null, (Object) "Can't create handler inside thread that has not called Looper.prepare()");
            }
            this.akX = looper == null ? new zzb(locationListener) : new zzb(locationListener, looper);
        }

        public synchronized void onLocationChanged(Location location) {
            if (this.akX == null) {
                Log.e("LocationClientHelper", "Received a location in client after calling removeLocationUpdates.");
            } else {
                Message obtain = Message.obtain();
                obtain.what = 1;
                obtain.obj = location;
                this.akX.sendMessage(obtain);
            }
        }

        public synchronized void release() {
            this.akX = null;
        }
    }

    public zzk(Context context, zzp<zzi> com_google_android_gms_location_internal_zzp_com_google_android_gms_location_internal_zzi) {
        this.mContext = context;
        this.akH = com_google_android_gms_location_internal_zzp_com_google_android_gms_location_internal_zzi;
    }

    private zza zza(LocationCallback locationCallback, Looper looper) {
        zza com_google_android_gms_location_internal_zzk_zza;
        synchronized (this.akW) {
            com_google_android_gms_location_internal_zzk_zza = (zza) this.akW.get(locationCallback);
            if (com_google_android_gms_location_internal_zzk_zza == null) {
                com_google_android_gms_location_internal_zzk_zza = new zza(locationCallback, looper);
            }
            this.akW.put(locationCallback, com_google_android_gms_location_internal_zzk_zza);
        }
        return com_google_android_gms_location_internal_zzk_zza;
    }

    private zzc zza(LocationListener locationListener, Looper looper) {
        zzc com_google_android_gms_location_internal_zzk_zzc;
        synchronized (this.VB) {
            com_google_android_gms_location_internal_zzk_zzc = (zzc) this.VB.get(locationListener);
            if (com_google_android_gms_location_internal_zzk_zzc == null) {
                com_google_android_gms_location_internal_zzk_zzc = new zzc(locationListener, looper);
            }
            this.VB.put(locationListener, com_google_android_gms_location_internal_zzk_zzc);
        }
        return com_google_android_gms_location_internal_zzk_zzc;
    }

    public Location getLastLocation() {
        this.akH.zzavf();
        try {
            return ((zzi) this.akH.zzavg()).zzkx(this.mContext.getPackageName());
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    public void removeAllListeners() {
        try {
            synchronized (this.VB) {
                for (zzi com_google_android_gms_location_zzi : this.VB.values()) {
                    if (com_google_android_gms_location_zzi != null) {
                        ((zzi) this.akH.zzavg()).zza(LocationRequestUpdateData.zza(com_google_android_gms_location_zzi, null));
                    }
                }
                this.VB.clear();
            }
            synchronized (this.akW) {
                for (zzh com_google_android_gms_location_zzh : this.akW.values()) {
                    if (com_google_android_gms_location_zzh != null) {
                        ((zzi) this.akH.zzavg()).zza(LocationRequestUpdateData.zza(com_google_android_gms_location_zzh, null));
                    }
                }
                this.akW.clear();
            }
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    public void zza(PendingIntent pendingIntent, zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        this.akH.zzavf();
        ((zzi) this.akH.zzavg()).zza(LocationRequestUpdateData.zzb(pendingIntent, com_google_android_gms_location_internal_zzg));
    }

    public void zza(LocationCallback locationCallback, zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        this.akH.zzavf();
        zzaa.zzb((Object) locationCallback, (Object) "Invalid null callback");
        synchronized (this.akW) {
            zzh com_google_android_gms_location_zzh = (zza) this.akW.remove(locationCallback);
            if (com_google_android_gms_location_zzh != null) {
                com_google_android_gms_location_zzh.release();
                ((zzi) this.akH.zzavg()).zza(LocationRequestUpdateData.zza(com_google_android_gms_location_zzh, com_google_android_gms_location_internal_zzg));
            }
        }
    }

    public void zza(LocationListener locationListener, zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        this.akH.zzavf();
        zzaa.zzb((Object) locationListener, (Object) "Invalid null listener");
        synchronized (this.VB) {
            zzi com_google_android_gms_location_zzi = (zzc) this.VB.remove(locationListener);
            if (com_google_android_gms_location_zzi != null) {
                com_google_android_gms_location_zzi.release();
                ((zzi) this.akH.zzavg()).zza(LocationRequestUpdateData.zza(com_google_android_gms_location_zzi, com_google_android_gms_location_internal_zzg));
            }
        }
    }

    public void zza(LocationRequest locationRequest, PendingIntent pendingIntent, zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        this.akH.zzavf();
        ((zzi) this.akH.zzavg()).zza(LocationRequestUpdateData.zza(LocationRequestInternal.zzb(locationRequest), pendingIntent, com_google_android_gms_location_internal_zzg));
    }

    public void zza(LocationRequest locationRequest, LocationListener locationListener, Looper looper, zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        this.akH.zzavf();
        ((zzi) this.akH.zzavg()).zza(LocationRequestUpdateData.zza(LocationRequestInternal.zzb(locationRequest), zza(locationListener, looper), com_google_android_gms_location_internal_zzg));
    }

    public void zza(LocationRequestInternal locationRequestInternal, LocationCallback locationCallback, Looper looper, zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        this.akH.zzavf();
        ((zzi) this.akH.zzavg()).zza(LocationRequestUpdateData.zza(locationRequestInternal, zza(locationCallback, looper), com_google_android_gms_location_internal_zzg));
    }

    public void zza(zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        this.akH.zzavf();
        ((zzi) this.akH.zzavg()).zza(com_google_android_gms_location_internal_zzg);
    }

    public LocationAvailability zzbqg() {
        this.akH.zzavf();
        try {
            return ((zzi) this.akH.zzavg()).zzky(this.mContext.getPackageName());
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    public void zzbqh() {
        if (this.akV) {
            try {
                zzcd(false);
            } catch (Throwable e) {
                throw new IllegalStateException(e);
            }
        }
    }

    public void zzcd(boolean z) throws RemoteException {
        this.akH.zzavf();
        ((zzi) this.akH.zzavg()).zzcd(z);
        this.akV = z;
    }

    public void zzd(Location location) throws RemoteException {
        this.akH.zzavf();
        ((zzi) this.akH.zzavg()).zzd(location);
    }
}
