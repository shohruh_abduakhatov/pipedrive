package com.pipedrive.gson;

import android.support.annotation.NonNull;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.stream.JsonReader;
import com.newrelic.agent.android.instrumentation.GsonInstrumentation;
import com.pipedrive.datetime.DateFormatHelper;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.Date;

public enum GsonHelper {
    ;
    
    private static Gson gsonInstance;

    static {
        gsonInstance = null;
    }

    public static JsonReader getJSONReader(InputStream inputStream) throws UnsupportedEncodingException {
        JsonReader jsonReader = new JsonReader(new InputStreamReader(inputStream, HttpRequest.CHARSET_UTF8));
        jsonReader.setLenient(true);
        return jsonReader;
    }

    public static <T> T fromJSON(JsonReader jsonReader, Type typeOfT) throws IOException {
        Gson gson = getGson();
        return !(gson instanceof Gson) ? gson.fromJson(jsonReader, typeOfT) : GsonInstrumentation.fromJson(gson, jsonReader, typeOfT);
    }

    @NonNull
    public static <T> T fromJSON(@NonNull JsonElement jsonElement, @NonNull Type typeOfT) {
        Gson gson = getGson();
        return !(gson instanceof Gson) ? gson.fromJson(jsonElement, typeOfT) : GsonInstrumentation.fromJson(gson, jsonElement, typeOfT);
    }

    @NonNull
    public static <T> T fromJSON(@NonNull String string, @NonNull Type typeOfT) {
        Gson gson = getGson();
        return !(gson instanceof Gson) ? gson.fromJson(string, typeOfT) : GsonInstrumentation.fromJson(gson, string, typeOfT);
    }

    @NonNull
    public static <T> String toJSONString(@NonNull T t) {
        Gson gson = getGson();
        return !(gson instanceof Gson) ? gson.toJson((Object) t) : GsonInstrumentation.toJson(gson, (Object) t);
    }

    @NonNull
    private static synchronized Gson getGson() {
        Gson gson;
        synchronized (GsonHelper.class) {
            if (gsonInstance == null || !gsonInstance.serializeNulls()) {
                GsonBuilder gsonBuilder = new GsonBuilder().registerTypeAdapter(Date.class, new UtcDateTypeAdapter()).excludeFieldsWithoutExposeAnnotation().setDateFormat(DateFormatHelper.DATE_FORMAT_LONG);
                gsonBuilder.serializeNulls();
                gsonInstance = gsonBuilder.create();
            }
            gson = gsonInstance;
        }
        return gson;
    }
}
