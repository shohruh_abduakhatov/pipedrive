package com.zendesk.sdk.model.settings;

import android.support.annotation.Nullable;

public class HelpCenterSettings {
    private boolean enabled;
    private String locale;

    public boolean isEnabled() {
        return this.enabled;
    }

    @Nullable
    public String getLocale() {
        return this.locale;
    }
}
