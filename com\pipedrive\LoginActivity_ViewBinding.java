package com.pipedrive;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class LoginActivity_ViewBinding implements Unbinder {
    private LoginActivity target;
    private View view2131820960;
    private View view2131821159;
    private View view2131821160;
    private View view2131821161;

    @UiThread
    public LoginActivity_ViewBinding(LoginActivity target) {
        this(target, target.getWindow().getDecorView());
    }

    @UiThread
    public LoginActivity_ViewBinding(final LoginActivity target, View source) {
        this.target = target;
        target.mEmailView = (EditText) Utils.findRequiredViewAsType(source, R.id.email, "field 'mEmailView'", EditText.class);
        View view = Utils.findRequiredView(source, R.id.password, "field 'mPasswordView' and method 'onPasswordEditorAction'");
        target.mPasswordView = (EditText) Utils.castView(view, R.id.password, "field 'mPasswordView'", EditText.class);
        this.view2131821159 = view;
        ((TextView) view).setOnEditorActionListener(new OnEditorActionListener() {
            public boolean onEditorAction(TextView p0, int p1, KeyEvent p2) {
                return target.onPasswordEditorAction();
            }
        });
        target.mRootView = (ViewGroup) Utils.findRequiredViewAsType(source, R.id.root, "field 'mRootView'", ViewGroup.class);
        target.mTopLayout = Utils.findRequiredView(source, R.id.top_layout, "field 'mTopLayout'");
        target.mCenterLayout = Utils.findRequiredView(source, R.id.center_layout, "field 'mCenterLayout'");
        view = Utils.findRequiredView(source, R.id.google_sign_in, "field 'mConnectWithGoogleButton' and method 'onGoogleSignInClicked'");
        target.mConnectWithGoogleButton = view;
        this.view2131820960 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onGoogleSignInClicked();
            }
        });
        target.mProgress = Utils.findRequiredView(source, R.id.progress, "field 'mProgress'");
        view = Utils.findRequiredView(source, R.id.sign_in, "method 'onSignInClicked'");
        this.view2131821160 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onSignInClicked();
            }
        });
        view = Utils.findRequiredView(source, R.id.sign_up, "method 'onSignUpClicked'");
        this.view2131821161 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onSignUpClicked();
            }
        });
    }

    @CallSuper
    public void unbind() {
        LoginActivity target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mEmailView = null;
        target.mPasswordView = null;
        target.mRootView = null;
        target.mTopLayout = null;
        target.mCenterLayout = null;
        target.mConnectWithGoogleButton = null;
        target.mProgress = null;
        ((TextView) this.view2131821159).setOnEditorActionListener(null);
        this.view2131821159 = null;
        this.view2131820960.setOnClickListener(null);
        this.view2131820960 = null;
        this.view2131821160.setOnClickListener(null);
        this.view2131821160 = null;
        this.view2131821161.setOnClickListener(null);
        this.view2131821161 = null;
    }
}
