package com.pipedrive.views.fab;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.view.ViewGroup;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class FabWithOverlayMenu_ViewBinding implements Unbinder {
    private FabWithOverlayMenu target;
    private View view2131820590;
    private View view2131820973;
    private View view2131820974;

    @UiThread
    public FabWithOverlayMenu_ViewBinding(FabWithOverlayMenu target) {
        this(target, target);
    }

    @UiThread
    public FabWithOverlayMenu_ViewBinding(final FabWithOverlayMenu target, View source) {
        this.target = target;
        View view = Utils.findRequiredView(source, R.id.add, "field 'mAddButton' and method 'onAddClicked'");
        target.mAddButton = view;
        this.view2131820590 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onAddClicked();
            }
        });
        target.mItemContainer = (ViewGroup) Utils.findRequiredViewAsType(source, R.id.item_container, "field 'mItemContainer'", ViewGroup.class);
        view = Utils.findRequiredView(source, R.id.close, "field 'mCloseButton' and method 'onCloseClicked'");
        target.mCloseButton = view;
        this.view2131820974 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onCloseClicked();
            }
        });
        view = Utils.findRequiredView(source, R.id.background, "field 'mBackground' and method 'onCloseClicked'");
        target.mBackground = view;
        this.view2131820973 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onCloseClicked();
            }
        });
    }

    @CallSuper
    public void unbind() {
        FabWithOverlayMenu target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mAddButton = null;
        target.mItemContainer = null;
        target.mCloseButton = null;
        target.mBackground = null;
        this.view2131820590.setOnClickListener(null);
        this.view2131820590 = null;
        this.view2131820974.setOnClickListener(null);
        this.view2131820974 = null;
        this.view2131820973.setOnClickListener(null);
        this.view2131820973 = null;
    }
}
