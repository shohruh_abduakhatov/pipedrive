package com.pipedrive.views.common;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class LayoutWithUnderlineAndLabel_ViewBinding implements Unbinder {
    private LayoutWithUnderlineAndLabel target;

    @UiThread
    public LayoutWithUnderlineAndLabel_ViewBinding(LayoutWithUnderlineAndLabel target) {
        this(target, target);
    }

    @UiThread
    public LayoutWithUnderlineAndLabel_ViewBinding(LayoutWithUnderlineAndLabel target, View source) {
        this.target = target;
        target.mLabelView = (TextView) Utils.findRequiredViewAsType(source, R.id.label, "field 'mLabelView'", TextView.class);
        target.mContentLayout = (ViewGroup) Utils.findRequiredViewAsType(source, R.id.contentLayout, "field 'mContentLayout'", ViewGroup.class);
        target.mUnderlineView = Utils.findRequiredView(source, R.id.underline, "field 'mUnderlineView'");
    }

    @CallSuper
    public void unbind() {
        LayoutWithUnderlineAndLabel target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mLabelView = null;
        target.mContentLayout = null;
        target.mUnderlineView = null;
    }
}
