package com.zendesk.sdk.model.request;

import java.util.HashMap;
import java.util.Map;

public class UserFieldRequest {
    private Map<String, Map<String, String>> user;

    public UserFieldRequest(Map<String, String> user) {
        Map<String, Map<String, String>> maps = new HashMap();
        maps.put("user_fields", user);
        this.user = maps;
    }
}
