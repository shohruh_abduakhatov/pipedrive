package com.pipedrive.views.common;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.CurrenciesDataSource;
import com.pipedrive.model.Currency;
import com.pipedrive.views.Spinner.LabelBuilder;
import com.pipedrive.views.Spinner.OnItemSelectedListener;

public class CurrencySpinner extends SpinnerWithUnderlineAndLabel {

    public interface OnCurrencySelectedListener {
        void onCurrencySelected(@NonNull Currency currency);
    }

    public CurrencySpinner(Context context) {
        this(context, null);
    }

    public CurrencySpinner(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CurrencySpinner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected int getLabelTextResourceId() {
        return R.string.currency;
    }

    public void setup(@NonNull Session session, @Nullable final String currentlySelectedCurrencyCode, @Nullable final OnCurrencySelectedListener onCurrencySelectedListener) {
        super.setup(new CurrenciesDataSource(session.getDatabase()).getCurrencies(), new LabelBuilder<Currency>() {
            public String getLabel(Currency item) {
                String str = "%s (%s)";
                Object[] objArr = new Object[2];
                objArr[0] = item.getName();
                objArr[1] = item.isCustom() ? item.getSymbol() : item.getCode();
                return String.format(str, objArr);
            }

            public boolean isSelected(Currency item) {
                return item.getCode().equalsIgnoreCase(currentlySelectedCurrencyCode);
            }
        }, new OnItemSelectedListener<Currency>() {
            public void onItemSelected(Currency item) {
                if (onCurrencySelectedListener != null) {
                    onCurrencySelectedListener.onCurrencySelected(item);
                }
            }
        });
    }
}
