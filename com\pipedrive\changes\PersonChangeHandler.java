package com.pipedrive.changes;

import android.support.annotation.NonNull;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.newrelic.agent.android.instrumentation.JSONObjectInstrumentation;
import com.pipedrive.application.Session;
import com.pipedrive.changes.Change.ChangeOperationType;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.Person;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.ConnectionUtil.JsonReaderInterceptor;
import com.pipedrive.util.networking.Response;
import com.pipedrive.util.persons.PersonNetworkingUtil;
import java.io.IOException;
import org.json.JSONObject;

class PersonChangeHandler extends ChangeHandler {
    PersonChangeHandler() {
    }

    @NonNull
    public ChangeHandled handleChange(@NonNull Session session, @NonNull Change personChange) {
        if (personChange.getChangeOperationType() != ChangeOperationType.OPERATION_CREATE && personChange.getChangeOperationType() != ChangeOperationType.OPERATION_UPDATE) {
            LogJourno.reportEvent(EVENT.ChangesHandler_corruptedChangeFound, String.format("Change:[%s]", new Object[]{personChange}));
            Log.e(new Throwable(String.format("Unknown change type requested for person: %s", new Object[]{personChange})));
            return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
        } else if (!ConnectionUtil.isConnected(session.getApplicationContext())) {
            return ChangeHandled.NOT_PROCESSED_PLEASE_RETRY;
        } else {
            boolean isUpdatePersonOperation;
            if (personChange.getChangeOperationType() == ChangeOperationType.OPERATION_UPDATE) {
                isUpdatePersonOperation = true;
            } else {
                isUpdatePersonOperation = false;
            }
            JsonReaderInterceptor<AnonymousClass1PersonResponse> personResponseParser = new JsonReaderInterceptor<AnonymousClass1PersonResponse>() {
                public AnonymousClass1PersonResponse interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull AnonymousClass1PersonResponse responseTemplate) throws IOException {
                    if (jsonReader.peek() == JsonToken.NULL) {
                        jsonReader.skipValue();
                    } else {
                        Person personFromResponse = PersonNetworkingUtil.readContact(jsonReader);
                        if (personFromResponse == null) {
                            LogJourno.reportEvent(EVENT.ChangesHandler_changeDataResponseToJsonParsingFailed, String.format("PersonResponseTemplate:[%s]", new Object[]{responseTemplate}));
                            Log.e(new Throwable(String.format("Parsing of Person from response failed! Response: %s", new Object[]{responseTemplate})));
                        } else if (responseTemplate.personUnderChangeSqlId <= 0 || !personFromResponse.isExisting()) {
                            LogJourno.reportEvent(EVENT.ChangesHandler_cannotUpdatePipedriveIdFromChangeDataResponse, String.format("PersonResponseTemplate:[%s] PersonFromResponse:[%s]", new Object[]{responseTemplate, personFromResponse}));
                            Log.e(new Throwable(String.format("Cannot update PD ID! SQL ID: %s; PD ID: %s", new Object[]{Long.valueOf(responseTemplate.personUnderChangeSqlId), Integer.valueOf(personFromResponse.getPipedriveId())})));
                        } else {
                            new PersonsDataSource(session.getDatabase()).updatePipedriveIdBySqlId(responseTemplate.personUnderChangeSqlId, personFromResponse.getPipedriveId());
                        }
                    }
                    return responseTemplate;
                }
            };
            try {
                Long personSqlId = personChange.getMetaData();
                if (personSqlId == null) {
                    LogJourno.reportEvent(EVENT.ChangesHandler_changeMetadataNotFound, String.format("Change:[%s]", new Object[]{personChange}));
                    Log.e(new Throwable("Change metadata is missing! Cannot find the Person to change!"));
                    return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
                }
                Person personUnderChange = (Person) new PersonsDataSource(session.getDatabase()).findBySqlId(personSqlId.longValue());
                if (personUnderChange == null) {
                    LogJourno.reportEvent(EVENT.ChangesHandler_changeDataNotFound, String.format("Change:[%s] SQL id:[%s]", new Object[]{personChange, personSqlId}));
                    Log.e(new Throwable(String.format("Change metadata is present (%s) but unable to find Person that was registered for the change!", new Object[]{personSqlId})));
                    return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
                }
                AnonymousClass1PersonResponse result;
                if (!personUnderChange.isAllAssociationsExisting()) {
                    LogJourno.reportEvent(EVENT.ChangesHandler_notAllChangePipedriveAssociationsExist, String.format("Change:[%s]", new Object[]{personChange}));
                    Log.e(new Throwable(String.format("Person associations are not all existing! Change: %s", new Object[]{personChange})));
                }
                ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder(session, "persons");
                if (isUpdatePersonOperation) {
                    apiUrlBuilder.appendEncodedPath(Integer.toString(personUnderChange.getPipedriveId()));
                }
                AnonymousClass1PersonResponse personResponse = new Response() {
                    long personUnderChangeSqlId;

                    public String toString() {
                        return "PersonResponse:[personUnderChangeSqlId=" + this.personUnderChangeSqlId + "][super.toString:[" + super.toString() + "]]";
                    }
                };
                personResponse.personUnderChangeSqlId = personSqlId.longValue();
                JSONObject json;
                if (isUpdatePersonOperation) {
                    json = personUnderChange.getJSON();
                    result = (AnonymousClass1PersonResponse) ConnectionUtil.requestPut(apiUrlBuilder, !(json instanceof JSONObject) ? json.toString() : JSONObjectInstrumentation.toString(json), personResponseParser, personResponse);
                } else {
                    json = personUnderChange.getJSON();
                    result = (AnonymousClass1PersonResponse) ConnectionUtil.requestPost(apiUrlBuilder, !(json instanceof JSONObject) ? json.toString() : JSONObjectInstrumentation.toString(json), personResponseParser, personResponse);
                }
                return parseResponse(result, this, personChange);
            } catch (Exception e) {
                LogJourno.reportEvent(EVENT.ChangesHandler_changeDataRequestToJsonParsingFailed, String.format("Change:[%s]", new Object[]{personChange}), e);
                Log.e(new Throwable("Person change failed", e));
                return ChangeHandled.CHANGE_FAILED;
            }
        }
    }
}
