package com.pipedrive.util.recents;

import com.pipedrive.model.pagination.Pagination;

public class RecentsResponse extends Pagination {
    protected static final String ITEM_TYPE_MARKER_ACTIVITY = "activity";
    protected static final String ITEM_TYPE_MARKER_ACTIVITY_TYPE = "activityType";
    protected static final String ITEM_TYPE_MARKER_CONTACT = "person";
    protected static final String ITEM_TYPE_MARKER_DEAL = "deal";
    protected static final String ITEM_TYPE_MARKER_DEAL_STAGE = "stage";
    protected static final String ITEM_TYPE_MARKER_EMAIL_MESSAGE = "emailMessage";
    protected static final String ITEM_TYPE_MARKER_EMAIL_THREAD = "emailThread";
    protected static final String ITEM_TYPE_MARKER_FILE = "file";
    protected static final String ITEM_TYPE_MARKER_FILTER = "filter";
    protected static final String ITEM_TYPE_MARKER_NOTE = "note";
    protected static final String ITEM_TYPE_MARKER_ORGANIZATION = "organization";
    protected static final String ITEM_TYPE_MARKER_PIPELINE = "pipeline";
    protected static final String ITEM_TYPE_MARKER_PRODUCT = "product";
    protected static final String ITEM_TYPE_MARKER_USER = "user";
    protected boolean pipelineReloadFilter = false;
    protected boolean pipelineReloadToDefaultFilter = false;
    protected boolean pipelineReloadToDefaultFilterAndPipeline = false;
}
