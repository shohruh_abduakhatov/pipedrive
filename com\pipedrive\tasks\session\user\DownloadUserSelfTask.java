package com.pipedrive.tasks.session.user;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.CompaniesDataSource;
import com.pipedrive.fragments.LocaleHelper;
import com.pipedrive.gson.GsonHelper;
import com.pipedrive.tasks.AsyncTask;
import com.pipedrive.util.UsersUtil;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.ConnectionUtil$JsonReaderInterceptor;
import com.pipedrive.util.networking.Response;
import com.pipedrive.util.networking.entities.self.CompanyEntity;
import com.pipedrive.util.networking.entities.self.CurrentCompanyFeaturesEntity;
import com.pipedrive.util.networking.entities.self.SelfEntity;
import java.io.IOException;
import java.util.List;

public class DownloadUserSelfTask extends AsyncTask<Void, Void, Boolean> {
    public DownloadUserSelfTask(@NonNull Session session) {
        super(session);
    }

    protected Boolean doInBackground(Void... params) {
        return Boolean.valueOf(downloadAndStoreBlocking());
    }

    public boolean downloadAndStoreBlocking() {
        boolean selfRetrievalSuccessful;
        boolean selfCompaniesRetrieved = true;
        SelfEntity self = downloadAndStoreSelfBlocking();
        if (self != null) {
            selfRetrievalSuccessful = true;
        } else {
            selfRetrievalSuccessful = false;
        }
        if (selfRetrievalSuccessful) {
            List<CompanyEntity> companies = self.getCompanies();
            if (companies == null) {
                selfCompaniesRetrieved = false;
            }
            if (selfCompaniesRetrieved) {
                new CompaniesDataSource(getSession()).updateExistingCompanies(companies);
            }
        }
        return selfRetrievalSuccessful;
    }

    @Nullable
    private SelfEntity downloadAndStoreSelfBlocking() {
        SelfEntity self = downloadSelfBlocking();
        if (!(self != null)) {
            return null;
        }
        setupAppAndStoreSelf(self);
        return self;
    }

    @Nullable
    public SelfEntity downloadSelfBlocking() {
        String selfUrlFilter = SelfEntity.URL_NESTED_PARAM_LIST;
        ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder(getSession(), ApiUrlBuilder.ENDPOINT_SELF);
        apiUrlBuilder.appendEncodedPath(SelfEntity.URL_NESTED_PARAM_LIST);
        return ((AnonymousClass1SelfResponse) ConnectionUtil.requestGet(apiUrlBuilder, new ConnectionUtil$JsonReaderInterceptor<AnonymousClass1SelfResponse>() {
            public AnonymousClass1SelfResponse interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull AnonymousClass1SelfResponse selfResponse) throws IOException {
                JsonElement data = new JsonParser().parse(jsonReader);
                if (data != null && data.isJsonObject()) {
                    selfResponse.selfEntity = DownloadUserSelfTask.this.parseResponse(data);
                }
                return selfResponse;
            }
        }, new Response() {
            SelfEntity selfEntity = null;
        })).selfEntity;
    }

    @NonNull
    private SelfEntity parseResponse(@NonNull JsonElement jsonElement) {
        return (SelfEntity) GsonHelper.fromJSON(jsonElement, SelfEntity.class);
    }

    void setupAppAndStoreSelf(@NonNull SelfEntity selfEntity) {
        setUserSelectedFiltersFromSelf(selfEntity);
        setLocalSetInSelfIfToBeChanged(selfEntity);
        setCompanyFeatures(selfEntity.getCurrentCompanyFeaturesEntity());
        saveSelfItemsToSession(selfEntity);
    }

    private void setUserSelectedFiltersFromSelf(@NonNull SelfEntity selfEntity) {
        UsersUtil.saveUserSettingsFilters(getSession(), selfEntity.getPipelineFilters());
    }

    private void setLocalSetInSelfIfToBeChanged(@NonNull SelfEntity selfEntity) {
        LocaleHelper.INSTANCE.setLanguage(getSession().getApplicationContext(), selfEntity.getDefaultLanguageCode(), selfEntity.getDefaultLanguageCountry(), false);
    }

    private void setCompanyFeatures(@Nullable CurrentCompanyFeaturesEntity companyFeatures) {
        Boolean bool = null;
        Session session = getSession();
        session.setCompanyFeatureProducts(companyFeatures == null ? null : companyFeatures.isProductsEnabled);
        session.setCompanyFeatureProductPriceVariations(companyFeatures == null ? null : companyFeatures.isProductsPriceVariationsEnabled);
        if (companyFeatures != null) {
            bool = companyFeatures.isProductsDurationsEnabled;
        }
        session.setCompanyFeatureProductDurations(bool);
    }

    private void saveSelfItemsToSession(@NonNull SelfEntity selfEntity) {
        Session session = getSession();
        session.setUserSettingsDefaultCurrencyCode(selfEntity.getDefaultCurrency());
        session.setUserSettingsDefaultVisibilityForOrganization(selfEntity.getDefaultVisibilityOrganization());
        session.setUserSettingsDefaultVisibilityForPerson(selfEntity.getDefaultVisibilityPerson());
        session.setUserSettingsDefaultVisibilityForDeal(selfEntity.getDefaultVisibilityDeal());
        session.setUserSettingsCanChangeVisivilityOfItems(selfEntity.canChangeVisibilityOfItems().booleanValue());
        session.setUserSettingsCanDeleteDeal(selfEntity.canDeleteDeals().booleanValue());
        session.setUserSettingsDefaultLanguageCountry(selfEntity.getDefaultLanguageCountry());
        session.setUserSettingsDefaultLocale(selfEntity.getDefaultLocale());
        session.setUserSettingsIsAdmin(selfEntity.isAdmin().booleanValue());
        session.setUserSettingsName(selfEntity.getName());
        session.setUserSettingsEmail(selfEntity.getEmail());
        session.setPipelineSelectedPipelineId(selfEntity.getCurrentPipelineId());
    }
}
