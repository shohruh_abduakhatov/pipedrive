package com.pipedrive.application;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi.GetConnectedNodesResult;
import com.google.android.gms.wearable.Wearable;
import com.newrelic.agent.android.NewRelic;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.pipedrive.R;
import com.pipedrive.fragments.LocaleHelper;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.util.flowfiles.FileHelper;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import io.fabric.sdk.android.Fabric;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PipedriveApp extends PipedriveAppBase {
    private static volatile Context mApplicationContext = null;
    private static volatile SessionManager mSessionManager = null;
    private static volatile Tracker mTrackers = null;
    protected boolean useWear = true;

    public void onCreate() {
        boolean noActiveSession;
        boolean enableNewRelic;
        super.onCreate();
        if (mApplicationContext == null) {
            mApplicationContext = getApplicationContext();
        }
        if (getSessionManager().hasActiveSession()) {
            noActiveSession = false;
        } else {
            noActiveSession = true;
        }
        if (noActiveSession || getActiveSession().getSharedSession().isAnalyticsEnabled()) {
            enableNewRelic = true;
        } else {
            enableNewRelic = false;
        }
        if (enableNewRelic) {
            NewRelic.withApplicationToken("AA20eac9327a445f4225b891a4a0a6d0872d21cab6").start(mApplicationContext);
        }
        Fabric.with(this, new Crashlytics());
        initZendesk();
        initImageLoader(mApplicationContext);
        LocaleHelper.INSTANCE.init(this);
        FileHelper.initialize();
        checkWearConnectionForStatistics(mApplicationContext);
    }

    private void checkWearConnectionForStatistics(@NonNull Context context) {
        if (this.useWear) {
            final GoogleApiClient googleApiClient = new Builder(context).addApi(Wearable.API).build();
            new Thread(new Runnable() {
                public void run() {
                    try {
                        googleApiClient.blockingConnect(5, TimeUnit.SECONDS);
                        for (Node wearableNode : ((GetConnectedNodesResult) Wearable.NodeApi.getConnectedNodes(googleApiClient).await()).getNodes()) {
                            LogJourno.reportEvent(EVENT.NotSpecified_wearableNodeConnectionFound, wearableNode.getDisplayName());
                        }
                        googleApiClient.disconnect();
                    } catch (Exception e) {
                    }
                }
            }).start();
        }
    }

    private void initZendesk() {
        ZendeskConfig.INSTANCE.init(this, "https://pipedrive.zendesk.com", "6eb4ece02b2c38b01de16d5c8ba4a26f8500efb501ebb70b", "mobile_sdk_client_5507990d79634f6b9c43");
    }

    public static ZendeskFeedbackConfiguration getZendeskConfiguration(@NonNull Session session) {
        String delimiter = "; ";
        final Resources resources = session.getApplicationContext().getResources();
        final String metadata = "App Version: 5.2 (251); Company ID: " + String.valueOf(session.getSelectedCompanyID()) + "; " + "User ID: " + String.valueOf(session.getAuthenticatedUserID()) + "; " + "Android Version: " + VERSION.RELEASE + "; " + "Device: " + Build.MODEL;
        return new ZendeskFeedbackConfiguration() {
            public String getRequestSubject() {
                return resources.getString(R.string.android_app_ticket);
            }

            public List<String> getTags() {
                return Arrays.asList(resources.getStringArray(R.array.android_app_ticket_tags));
            }

            public String getAdditionalInfo() {
                return metadata;
            }
        };
    }

    @Nullable
    public static synchronized Tracker getTracker(@Nullable Context ctx) {
        Tracker tracker;
        synchronized (PipedriveApp.class) {
            if (mTrackers == null && ctx != null) {
                mTrackers = GoogleAnalytics.getInstance(ctx).newTracker((int) R.xml.ga_app_tracker);
            }
            tracker = mTrackers;
        }
        return tracker;
    }

    @NonNull
    public static SessionManager getSessionManager() {
        if (mSessionManager == null) {
            mSessionManager = new SessionManager(mApplicationContext);
        }
        return mSessionManager;
    }

    @Nullable
    public static Session getActiveSession() {
        return getSessionManager().getActiveSession();
    }

    protected static SharedSession getSharedSession() {
        return getSessionManager().getSharedSession();
    }

    private static void initImageLoader(Context context) {
        ImageLoaderConfiguration.Builder imageLoaderConfig = new ImageLoaderConfiguration.Builder(context);
        imageLoaderConfig.denyCacheImageMultipleSizesInMemory();
        imageLoaderConfig.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        imageLoaderConfig.diskCacheSize(52428800);
        imageLoaderConfig.writeDebugLogs();
        ImageLoader.getInstance().init(imageLoaderConfig.build());
    }
}
