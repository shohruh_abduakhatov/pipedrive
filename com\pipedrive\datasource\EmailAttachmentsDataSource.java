package com.pipedrive.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.email.EmailAttachment;
import com.pipedrive.model.email.EmailMessage;
import com.pipedrive.util.CursorHelper;
import java.util.ArrayList;
import java.util.List;

public class EmailAttachmentsDataSource extends BaseDataSource<EmailAttachment> {
    public EmailAttachmentsDataSource(SQLiteDatabase dbconn) {
        super(dbconn);
    }

    @NonNull
    protected String[] getAllColumns() {
        return new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_PIPEDRIVE_ID, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_MESSAGE_ID, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_COMPANY_ID, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_ADD_TIME, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_FILE_NAME, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_CLEAN_NAME, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_FILE_TYPE, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_FILE_SIZE, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_ACTIVE, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_INLINE, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_COMMENT, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_URL};
    }

    @NonNull
    protected String getColumnNameForPipedriveId() {
        return PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_PIPEDRIVE_ID;
    }

    @NonNull
    protected String getColumnNameForSqlId() {
        return PipeSQLiteHelper.COLUMN_ID;
    }

    @NonNull
    protected String getTableName() {
        return PipeSQLiteHelper.TABLE_EMAIL_ATTACHMENTS;
    }

    @NonNull
    protected ContentValues getContentValues(@NonNull EmailAttachment attachment) {
        ContentValues contentValues = super.getContentValues(attachment);
        if (attachment.getMessageId() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_MESSAGE_ID, attachment.getMessageId());
        }
        if (attachment.getCompanyId() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_COMPANY_ID, attachment.getCompanyId());
        }
        if (attachment.getAddTime() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_ADD_TIME, Long.valueOf(attachment.getAddTime().getRepresentationInUnixTime()));
        }
        if (attachment.getFileName() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_FILE_NAME, attachment.getFileName());
        }
        if (attachment.getCleanName() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_CLEAN_NAME, attachment.getCleanName());
        }
        if (attachment.getFileType() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_FILE_TYPE, attachment.getFileType());
        }
        if (attachment.getFileSize() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_FILE_SIZE, attachment.getFileSize());
        }
        if (attachment.getActive() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_ACTIVE, attachment.getActive());
        }
        if (attachment.getInline() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_INLINE, attachment.getInline());
        }
        if (attachment.getComment() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_COMMENT, attachment.getComment());
        }
        if (attachment.getUrl() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_URL, attachment.getUrl());
        }
        return contentValues;
    }

    @Nullable
    protected EmailAttachment deflateCursor(@NonNull Cursor cursor) {
        EmailAttachment attachment = new EmailAttachment();
        Long sqlId = CursorHelper.getLong(cursor, getColumnNameForSqlId());
        if (sqlId != null) {
            attachment.setSqlId(sqlId.longValue());
        }
        Integer pipedriveId = CursorHelper.getInteger(cursor, getColumnNameForPipedriveId());
        if (pipedriveId != null) {
            attachment.setPipedriveId(pipedriveId.intValue());
        }
        Long messageId = CursorHelper.getLong(cursor, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_MESSAGE_ID);
        if (messageId != null) {
            attachment.setMessageId(messageId);
        }
        attachment.setActive(CursorHelper.getBoolean(cursor, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_ACTIVE));
        attachment.setAddTime(CursorHelper.getPipedriveDateTimeFromColumnHeldInSeconds(cursor, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_ADD_TIME));
        attachment.setCleanName(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_CLEAN_NAME));
        attachment.setComment(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_COMMENT));
        attachment.setCompanyId(CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_COMPANY_ID));
        attachment.setFileName(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_FILE_NAME));
        attachment.setFileSize(CursorHelper.getLong(cursor, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_FILE_SIZE));
        attachment.setFileType(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_FILE_TYPE));
        attachment.setInline(CursorHelper.getBoolean(cursor, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_INLINE));
        attachment.setUrl(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_EMAIL_ATTACHMENT_URL));
        return attachment;
    }

    void createOrUpdateAttachments(@NonNull EmailMessage emailMessage) {
        if (emailMessage.getAttachments() != null) {
            beginTransactionNonExclusive();
            for (EmailAttachment attachment : emailMessage.getAttachments()) {
                attachment.setMessageId(Long.valueOf(emailMessage.getSqlId()));
                createOrUpdate((BaseDatasourceEntity) attachment);
            }
            commit();
        }
    }

    List<EmailAttachment> getAttachmentsForMessage(long emailMessageSqlId) {
        List<EmailAttachment> result = new ArrayList();
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String tableName = getTableName();
        String[] allColumns = getAllColumns();
        String str = "email_attachments_message_id=?";
        String[] strArr = new String[]{String.valueOf(emailMessageSqlId)};
        Cursor cursor = !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.query(tableName, allColumns, str, strArr, null, null, null) : SQLiteInstrumentation.query(transactionalDBConnection, tableName, allColumns, str, strArr, null, null, null);
        try {
            cursor.moveToPosition(-1);
            while (cursor.moveToNext()) {
                result.add(deflateCursor(cursor));
            }
            return result;
        } finally {
            cursor.close();
        }
    }
}
