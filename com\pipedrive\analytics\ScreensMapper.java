package com.pipedrive.analytics;

import android.app.Activity;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.pipedrive.ActivityListActivity;
import com.pipedrive.CalendarActivity;
import com.pipedrive.ContactsActivity;
import com.pipedrive.CustomFieldEditActivity;
import com.pipedrive.EmailDetailViewActivity;
import com.pipedrive.FileOpenActivity;
import com.pipedrive.GlobalSearchActivity;
import com.pipedrive.LoginActivity;
import com.pipedrive.OrganizationDetailActivity;
import com.pipedrive.PersonDetailActivity;
import com.pipedrive.TrialExpiredActivity;
import com.pipedrive.activity.ActivityDetailActivity;
import com.pipedrive.call.CallSummaryActivity;
import com.pipedrive.call.RelatedActivityListActivity;
import com.pipedrive.customfields.fragments.DealCustomFieldListFragment;
import com.pipedrive.customfields.fragments.PersonCustomFieldListFragment;
import com.pipedrive.deal.DealEditActivity;
import com.pipedrive.deal.view.DealDetailsActivity;
import com.pipedrive.dialogs.ChangeCompanyDialogFragment;
import com.pipedrive.dialogs.communicationmediumlist.CommunicationMediumListDialogFragment;
import com.pipedrive.dialogs.login.DeviceDisabledDialog;
import com.pipedrive.dialogs.login.EmailPasswordLoginFailedDialogFragment;
import com.pipedrive.dialogs.login.GoogleLoginFailedDialogFragment;
import com.pipedrive.flow.fragments.DealFlowFragment;
import com.pipedrive.flow.fragments.OrganizationFlowFragment;
import com.pipedrive.flow.fragments.PersonFlowFragment;
import com.pipedrive.fragments.ActivityListFragment;
import com.pipedrive.fragments.DealsListFragment;
import com.pipedrive.fragments.GlobalSearchFragment;
import com.pipedrive.fragments.LanguageSelectorDialogFragment;
import com.pipedrive.fragments.OrganizationDetailFragment;
import com.pipedrive.fragments.OrganizationListFragment;
import com.pipedrive.fragments.OrganizationPeopleListFragment;
import com.pipedrive.fragments.PeopleListFragment;
import com.pipedrive.fragments.PersonDetailFragment;
import com.pipedrive.fragments.customfields.CustomFieldDoubleFragment;
import com.pipedrive.fragments.customfields.CustomFieldEditTextFragment;
import com.pipedrive.linking.ProductLinkingActivity;
import com.pipedrive.more.AboutPipedriveActivity;
import com.pipedrive.more.HelpAndFeedbackActivity;
import com.pipedrive.more.MoreActivity;
import com.pipedrive.more.PreferencesActivity;
import com.pipedrive.note.NoteCreateActivity;
import com.pipedrive.note.NoteUpdateActivity;
import com.pipedrive.notification.ActivityReminderSelectorDialogFragment;
import com.pipedrive.organization.edit.OrganizationCreateActivity;
import com.pipedrive.organization.edit.OrganizationUpdateActivity;
import com.pipedrive.person.edit.PersonCreateActivity;
import com.pipedrive.person.edit.PersonUpdateActivity;
import com.pipedrive.pipeline.PipelineActivity;
import com.pipedrive.pipeline.PipelineDealListFragment;
import com.pipedrive.products.DealProductListActivity;
import com.pipedrive.products.VariationSelectionDialogFragment;
import com.pipedrive.products.edit.DealProductCreateActivity;
import com.pipedrive.products.edit.DealProductEditActivity;
import com.pipedrive.settings.SettingsActivity;
import com.pipedrive.whatsnew.WhatsNewActivity;

public enum ScreensMapper {
    ;
    
    public static final String SCREEN_NAME_ABOUT = "About";
    public static final String SCREEN_NAME_ACTIVITY_DATE_DIALOG = "Activity Date Dialog";
    public static final String SCREEN_NAME_ACTIVITY_LIST_FILTER = "Activity Filters List";
    public static final String SCREEN_NAME_ACTIVITY_TIME_DIALOG = "Activity Time Dialog";
    public static final String SCREEN_NAME_CALENDAR_VIEW_MONTH_EXTENDED = "Calendar view: Month extended";
    public static final String SCREEN_NAME_CHANGELOG = "Changelog";
    public static final String SCREEN_NAME_DEVICE_DISABLED = "Device disabled modal";
    public static final String SCREEN_NAME_EMAIL_PASSWORD_LOGIN_FAILED = "Email/password login failure modal ";
    public static final String SCREEN_NAME_GOOGLE_LOGIN_FAILED = "Google login failure modal ";
    public static final String SCREEN_NAME_HELP = "Help and Feedback";
    public static final String SCREEN_NAME_OPEN_SOURCE_LICENSES = "Open Source Licences";
    public static final String SCREEN_NAME_PIPELINE_LIST_FILTER = "Pipeline Filters List";
    public static final String SCREEN_NAME_PRIVACY_POLICY = "Privacy Policy";
    public static final String SCREEN_NAME_TERMS_OF_SERVICE = "Terms of Service";

    @Nullable
    static String getScreenName(@Nullable Activity activity) {
        if (activity == null) {
            return null;
        }
        String label = activity.getClass().getCanonicalName();
        if (TextUtils.equals(label, DealDetailsActivity.class.getCanonicalName())) {
            return "Deal Details";
        }
        if (TextUtils.equals(label, PipelineActivity.class.getCanonicalName())) {
            return "Pipeline Deals List";
        }
        if (TextUtils.equals(label, SettingsActivity.class.getCanonicalName())) {
            return "Settings";
        }
        if (TextUtils.equals(label, MoreActivity.class.getCanonicalName())) {
            return "More Activity";
        }
        if (TextUtils.equals(label, PreferencesActivity.class.getCanonicalName())) {
            return "Preferences";
        }
        if (TextUtils.equals(label, AboutPipedriveActivity.class.getCanonicalName())) {
            return "About Pipedrive";
        }
        if (TextUtils.equals(label, HelpAndFeedbackActivity.class.getCanonicalName())) {
            return "Help And Feedback";
        }
        if (TextUtils.equals(label, ActivityListActivity.class.getCanonicalName())) {
            return "Activities Main List";
        }
        if (TextUtils.equals(label, ActivityDetailActivity.class.getCanonicalName())) {
            return "Add Edit Activity";
        }
        if (TextUtils.equals(label, PersonCreateActivity.class.getCanonicalName())) {
            return "Add New Person";
        }
        if (TextUtils.equals(label, PersonUpdateActivity.class.getCanonicalName())) {
            return "Edit Person";
        }
        if (TextUtils.equals(label, LoginActivity.class.getCanonicalName())) {
            return "Login";
        }
        if (TextUtils.equals(label, NoteCreateActivity.class.getCanonicalName())) {
            return "New Note";
        }
        if (TextUtils.equals(label, NoteUpdateActivity.class.getCanonicalName())) {
            return "Edit Note";
        }
        if (TextUtils.equals(label, GlobalSearchActivity.class.getCanonicalName())) {
            return null;
        }
        if (TextUtils.equals(label, CustomFieldEditActivity.class.getCanonicalName())) {
            return null;
        }
        if (TextUtils.equals(label, FileOpenActivity.class.getCanonicalName())) {
            return "File Opening";
        }
        if (TextUtils.equals(label, TrialExpiredActivity.class.getCanonicalName())) {
            return "Trial end";
        }
        if (TextUtils.equals(label, EmailDetailViewActivity.class.getCanonicalName())) {
            return "Email";
        }
        if (TextUtils.equals(label, OrganizationDetailActivity.class.getCanonicalName())) {
            return "Organization General";
        }
        if (TextUtils.equals(label, PersonDetailActivity.class.getCanonicalName())) {
            return "Person General";
        }
        if (TextUtils.equals(label, OrganizationCreateActivity.class.getCanonicalName())) {
            return "Add New Organization";
        }
        if (TextUtils.equals(label, OrganizationUpdateActivity.class.getCanonicalName())) {
            return "Edit Organization";
        }
        if (TextUtils.equals(label, ContactsActivity.class.getCanonicalName())) {
            return null;
        }
        if (TextUtils.equals(label, DealEditActivity.class.getCanonicalName())) {
            return "Add Edit Deal";
        }
        if (TextUtils.equals(label, WhatsNewActivity.class.getCanonicalName())) {
            return null;
        }
        if (TextUtils.equals(label, CallSummaryActivity.class.getCanonicalName())) {
            return "Call Summary";
        }
        if (TextUtils.equals(label, RelatedActivityListActivity.class.getCanonicalName())) {
            return "Call Summary: Related Activities";
        }
        if (TextUtils.equals(label, CalendarActivity.class.getCanonicalName())) {
            return "Calendar view";
        }
        if (TextUtils.equals(label, DealProductListActivity.class.getCanonicalName())) {
            return "Deal Products List";
        }
        if (TextUtils.equals(label, ProductLinkingActivity.class.getCanonicalName())) {
            return "Company's Products List";
        }
        if (TextUtils.equals(label, DealProductEditActivity.class.getCanonicalName())) {
            return "Edit Deal Product";
        }
        if (TextUtils.equals(label, DealProductCreateActivity.class.getCanonicalName())) {
            return "Add Deal Product";
        }
        return label;
    }

    @Nullable
    static String getScreenNameForNativeFragment(@Nullable Fragment fragment) {
        if (fragment == null) {
            return null;
        }
        String label = fragment.getClass().getCanonicalName();
        if (TextUtils.equals(label, LanguageSelectorDialogFragment.class.getCanonicalName())) {
            return "Language selection";
        }
        if (TextUtils.equals(label, ActivityReminderSelectorDialogFragment.class.getCanonicalName())) {
            return "Activity Reminder";
        }
        if (TextUtils.equals(label, VariationSelectionDialogFragment.class.getCanonicalName())) {
            return "Product Variations Modal";
        }
        return label;
    }

    @Nullable
    static String getScreenNameForSupportFragment(@Nullable android.support.v4.app.Fragment fragment) {
        if (fragment == null) {
            return null;
        }
        String label = fragment.getClass().getCanonicalName();
        if (TextUtils.equals(label, DealsListFragment.class.getCanonicalName())) {
            return null;
        }
        if (TextUtils.equals(label, OrganizationPeopleListFragment.class.getCanonicalName())) {
            return null;
        }
        if (TextUtils.equals(label, DealFlowFragment.class.getCanonicalName())) {
            return null;
        }
        if (TextUtils.equals(label, PersonFlowFragment.class.getCanonicalName())) {
            return null;
        }
        if (TextUtils.equals(label, OrganizationFlowFragment.class.getCanonicalName())) {
            return null;
        }
        if (TextUtils.equals(label, GlobalSearchFragment.class.getCanonicalName())) {
            return "Global Search";
        }
        if (TextUtils.equals(label, DealCustomFieldListFragment.class.getCanonicalName())) {
            return null;
        }
        if (TextUtils.equals(label, CustomFieldEditTextFragment.class.getCanonicalName())) {
            return null;
        }
        if (TextUtils.equals(label, CustomFieldDoubleFragment.class.getCanonicalName())) {
            return null;
        }
        if (TextUtils.equals(label, LanguageSelectorDialogFragment.class.getCanonicalName())) {
            return "Language selection";
        }
        if (TextUtils.equals(label, ChangeCompanyDialogFragment.class.getCanonicalName())) {
            return "Company switch on trial";
        }
        if (TextUtils.equals(label, ActivityListFragment.class.getCanonicalName())) {
            return null;
        }
        if (TextUtils.equals(label, PipelineDealListFragment.class.getCanonicalName())) {
            return null;
        }
        if (TextUtils.equals(label, CommunicationMediumListDialogFragment.class.getCanonicalName())) {
            return null;
        }
        if (TextUtils.equals(label, OrganizationListFragment.class.getCanonicalName())) {
            return "Organizations List";
        }
        if (TextUtils.equals(label, PeopleListFragment.class.getCanonicalName())) {
            return "Persons List";
        }
        if (TextUtils.equals(label, PersonCustomFieldListFragment.class.getCanonicalName())) {
            return null;
        }
        if (TextUtils.equals(label, OrganizationDetailFragment.class.getCanonicalName())) {
            return null;
        }
        if (TextUtils.equals(label, PersonDetailFragment.class.getCanonicalName())) {
            return null;
        }
        if (TextUtils.equals(label, EmailPasswordLoginFailedDialogFragment.class.getCanonicalName())) {
            return SCREEN_NAME_EMAIL_PASSWORD_LOGIN_FAILED;
        }
        if (TextUtils.equals(label, GoogleLoginFailedDialogFragment.class.getCanonicalName())) {
            return SCREEN_NAME_GOOGLE_LOGIN_FAILED;
        }
        if (TextUtils.equals(label, DeviceDisabledDialog.class.getCanonicalName())) {
            return SCREEN_NAME_DEVICE_DISABLED;
        }
        return label;
    }
}
