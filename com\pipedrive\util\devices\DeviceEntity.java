package com.pipedrive.util.devices;

import android.os.Build;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pipedrive.application.Session;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B5\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0003\u0010\b\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0003\u0010\t\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\nJ\u000b\u0010\u000f\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0006HÂ\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0006HÂ\u0003J\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0006HÂ\u0003J9\u0010\u0013\u001a\u00020\u00002\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\b\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u00062\n\b\u0003\u0010\b\u001a\u0004\u0018\u00010\u00062\n\b\u0003\u0010\t\u001a\u0004\u0018\u00010\u0006HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0006HÖ\u0001R\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00068\u0002X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\t\u001a\u0004\u0018\u00010\u00068\u0002X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\b\u001a\u0004\u0018\u00010\u00068\u0002X\u0004¢\u0006\u0002\n\u0000R \u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e¨\u0006\u001a"}, d2 = {"Lcom/pipedrive/util/devices/DeviceEntity;", "", "session", "Lcom/pipedrive/application/Session;", "(Lcom/pipedrive/application/Session;)V", "pipedriveId", "", "androidId", "fcmToken", "deviceInfo", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getPipedriveId", "()Ljava/lang/String;", "setPipedriveId", "(Ljava/lang/String;)V", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "pipedrive_prodRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: DeviceEntity.kt */
public final class DeviceEntity {
    @SerializedName("device_id")
    @Expose
    private final String androidId;
    @SerializedName("device_info")
    @Expose
    private final String deviceInfo;
    @SerializedName("device_token")
    @Expose
    private final String fcmToken;
    @SerializedName("id")
    @Nullable
    @Expose(serialize = false)
    private String pipedriveId;

    public DeviceEntity() {
        this(null, null, null, null, 15, null);
    }

    private final String component2() {
        return this.androidId;
    }

    private final String component3() {
        return this.fcmToken;
    }

    private final String component4() {
        return this.deviceInfo;
    }

    @NotNull
    public static /* bridge */ /* synthetic */ DeviceEntity copy$default(DeviceEntity deviceEntity, String str, String str2, String str3, String str4, int i, Object obj) {
        if ((i & 1) != 0) {
            str = deviceEntity.pipedriveId;
        }
        if ((i & 2) != 0) {
            str2 = deviceEntity.androidId;
        }
        if ((i & 4) != 0) {
            str3 = deviceEntity.fcmToken;
        }
        if ((i & 8) != 0) {
            str4 = deviceEntity.deviceInfo;
        }
        return deviceEntity.copy(str, str2, str3, str4);
    }

    @Nullable
    public final String component1() {
        return this.pipedriveId;
    }

    @NotNull
    public final DeviceEntity copy(@Nullable String pipedriveId, @Nullable String androidId, @Nullable String fcmToken, @Nullable String deviceInfo) {
        return new DeviceEntity(pipedriveId, androidId, fcmToken, deviceInfo);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DeviceEntity) {
                DeviceEntity deviceEntity = (DeviceEntity) obj;
                if (Intrinsics.areEqual(this.pipedriveId, deviceEntity.pipedriveId)) {
                    if (Intrinsics.areEqual(this.androidId, deviceEntity.androidId)) {
                        if (Intrinsics.areEqual(this.fcmToken, deviceEntity.fcmToken)) {
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        String str = this.pipedriveId;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        str = this.androidId;
        hashCode = ((str != null ? str.hashCode() : 0) + hashCode) * 31;
        str = this.fcmToken;
        int hashCode2 = ((str != null ? str.hashCode() : 0) + hashCode) * 31;
        String str2 = this.deviceInfo;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        return "DeviceEntity(pipedriveId=" + this.pipedriveId + ", androidId=" + this.androidId + ", fcmToken=" + this.fcmToken + ", deviceInfo=" + this.deviceInfo + ")";
    }

    public DeviceEntity(@Nullable String pipedriveId, @Nullable String androidId, @Nullable String fcmToken, @Nullable String deviceInfo) {
        this.pipedriveId = pipedriveId;
        this.androidId = androidId;
        this.fcmToken = fcmToken;
        this.deviceInfo = deviceInfo;
    }

    public /* synthetic */ DeviceEntity(String str, String str2, String str3, String str4, int i, DefaultConstructorMarker defaultConstructorMarker) {
        String str5;
        String str6;
        if ((i & 1) != 0) {
            str5 = (String) null;
        } else {
            str5 = str;
        }
        if ((i & 2) != 0) {
            str6 = (String) null;
        } else {
            str6 = str2;
        }
        if ((i & 4) != 0) {
            str3 = FirebaseInstanceId.getInstance().getToken();
        }
        if ((i & 8) != 0) {
            str4 = Build.MODEL;
        }
        this(str5, str6, str3, str4);
    }

    @Nullable
    public final String getPipedriveId() {
        return this.pipedriveId;
    }

    public final void setPipedriveId(@Nullable String <set-?>) {
        this.pipedriveId = <set-?>;
    }

    public DeviceEntity(@NotNull Session session) {
        Intrinsics.checkParameterIsNotNull(session, SettingsJsonConstants.SESSION_KEY);
        this(null, session.getAndroidId(), null, null, 13, null);
    }
}
