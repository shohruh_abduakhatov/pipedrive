package com.pipedrive.views.viewholder.deal;

import android.content.Context;
import android.support.annotation.NonNull;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.model.Deal;

public class DealRowViewHolderForDealsList extends DealRowViewHolder {
    public /* bridge */ /* synthetic */ void fill(@NonNull Session session, @NonNull Context context, @NonNull Deal deal) {
        super.fill(session, context, deal);
    }

    public int getLayoutResourceId() {
        return R.layout.row_deal_deals_list;
    }
}
