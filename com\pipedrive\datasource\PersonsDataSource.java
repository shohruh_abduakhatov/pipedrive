package com.pipedrive.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.newrelic.agent.android.instrumentation.JSONArrayInstrumentation;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.contact.CommunicationMedium;
import com.pipedrive.util.CursorHelper;
import com.pipedrive.util.StringUtils;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;

public class PersonsDataSource extends BaseDataSource<Person> {
    private static final String[] ALL_COLUMNS = new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_PERSONS_PIPEDRIVE_ID, PipeSQLiteHelper.COLUMN_PERSONS_ORGANIZATION_SQL_ID, "c_name", PipeSQLiteHelper.COLUMN_PERSONS_PHONES, PipeSQLiteHelper.COLUMN_PERSONS_EMAILS, PipeSQLiteHelper.COLUMN_PERSONS_VISIBLE_TO, PipeSQLiteHelper.COLUMN_PERSONS_OWNER_ID, PipeSQLiteHelper.COLUMN_PERSONS_DROP_BOX_ADDRESS, PipeSQLiteHelper.COLUMN_PERSONS_IS_ACTIVE, PipeSQLiteHelper.COLUMN_PERSONS_OWNER_NAME, PipeSQLiteHelper.COLUMN_PERSONS_IMS, PipeSQLiteHelper.COLUMN_PERSONS_POSTAL_ADDRESS, PipeSQLiteHelper.COLUMN_PERSONS_PICTURE_ID, PipeSQLiteHelper.COLUMN_PERSONS_NAME_SEARCH_FIELD, PipeSQLiteHelper.COLUMN_PERSONS_OTHER_FIELDS, PipeSQLiteHelper.COLUMN_PERSONS_PHONES_SEARCH_FIELD, PipeSQLiteHelper.COLUMN_PERSONS_IS_SHADOW, "persons._id AS c_person_sql_id"};
    private static final String COLUMN_PERSON_SQLID_ALIAS = "c_person_sql_id";
    @NonNull
    private final OrganizationsDataSource organizationsDataSource;
    @Nullable
    private com.pipedrive.datasource.OrganizationsDataSource.ForJoin organizationsDataSourceForJoin;
    @Nullable
    private ForJoin personsDataSourceForJoin;
    @NonNull
    private final SearchHelper searchHelper = new SearchHelper("persons.c_name_search_field", "persons.c_phones_search_field", "organizations.org_name_search_field", "organizations.address");

    public static class ForJoin extends PersonsDataSource {
        @Nullable
        public /* bridge */ /* synthetic */ BaseDatasourceEntity deflateCursor(@NonNull Cursor cursor) {
            return super.deflateCursor(cursor);
        }

        @NonNull
        protected /* bridge */ /* synthetic */ ContentValues getContentValues(@NonNull BaseDatasourceEntity baseDatasourceEntity) {
            return super.getContentValues((Person) baseDatasourceEntity);
        }

        public ForJoin(SQLiteDatabase dbconn) {
            super(dbconn);
        }

        @NonNull
        public String[] getAllColumns() {
            return (String[]) Arrays.copyOfRange(PersonsDataSource.ALL_COLUMNS, 1, PersonsDataSource.ALL_COLUMNS.length);
        }
    }

    public PersonsDataSource(SQLiteDatabase dbconn) {
        super(dbconn);
        this.organizationsDataSource = new OrganizationsDataSource(dbconn);
    }

    @NonNull
    private synchronized ForJoin getPersonsDataSourceForJoinLazy() {
        if (this.personsDataSourceForJoin == null) {
            this.personsDataSourceForJoin = new ForJoin(getTransactionalDBConnection());
        }
        return this.personsDataSourceForJoin;
    }

    @NonNull
    private synchronized com.pipedrive.datasource.OrganizationsDataSource.ForJoin getOrganizationsDataSourceForJoinLazy() {
        if (this.organizationsDataSourceForJoin == null) {
            this.organizationsDataSourceForJoin = new com.pipedrive.datasource.OrganizationsDataSource.ForJoin(getTransactionalDBConnection());
        }
        return this.organizationsDataSourceForJoin;
    }

    public void deletePerson(int pipedriveId) {
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String str = "persons";
        String str2 = "c_pd_id = " + pipedriveId;
        if (transactionalDBConnection instanceof SQLiteDatabase) {
            SQLiteInstrumentation.delete(transactionalDBConnection, str, str2, null);
        } else {
            transactionalDBConnection.delete(str, str2, null);
        }
    }

    @NonNull
    public List<Person> findPersonsByName(@NonNull SearchConstraint searchConstraint) {
        return deflateCursorToList(getSearchCursor(searchConstraint));
    }

    public Person findPersonByPipedriveId(long pipedriveId) {
        return (Person) findByPipedriveId(pipedriveId);
    }

    public Cursor findPersonByPipedriveIdCursor(long pipedriveId) {
        return findByPipedriveIdCursor(pipedriveId);
    }

    @NonNull
    public Cursor getSearchCursor(@NonNull SearchConstraint searchConstraint) {
        return query("persons LEFT JOIN organizations ON organizations._id = persons.c_org_id_sql", new String[]{"persons._id", TextUtils.join(Table.COMMA_SEP, getPersonsDataSourceForJoinLazy().getAllColumns()), TextUtils.join(Table.COMMA_SEP, getOrganizationsDataSourceForJoinLazy().getAllColumns())}, getSelectionForSearch(searchConstraint), "persons.c_name_search_field");
    }

    @NonNull
    public String getAlphabetString(@NonNull SearchConstraint searchConstraint) {
        Cursor firstLetters;
        SelectionHolder selectionHolder = getSelectionForSearch(searchConstraint);
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String str = "persons LEFT JOIN organizations ON organizations._id = persons.c_org_id_sql";
        String[] strArr = new String[]{"SUBSTR(c_name_search_field, 1, 1)", PipeSQLiteHelper.COLUMN_ORG_NAME_SEARCH_FIELD};
        String selection = selectionHolder.getSelection();
        String[] selectionArgs = selectionHolder.getSelectionArgs();
        String str2 = PipeSQLiteHelper.COLUMN_PERSONS_NAME_SEARCH_FIELD;
        if (transactionalDBConnection instanceof SQLiteDatabase) {
            firstLetters = SQLiteInstrumentation.query(transactionalDBConnection, true, str, strArr, selection, selectionArgs, null, null, str2, null);
        } else {
            firstLetters = transactionalDBConnection.query(true, str, strArr, selection, selectionArgs, null, null, str2, null);
        }
        String emptyString = "";
        return ((String) CursorHelper.observeAllRows(firstLetters).map(new Func1<Cursor, String>() {
            public String call(Cursor cursor) {
                return cursor.getString(0);
            }
        }).switchIfEmpty(Observable.just("")).distinct().reduce(new Func2<String, String, String>() {
            public String call(String s, String s2) {
                return s.concat(s2);
            }
        }).toBlocking().single()).toUpperCase();
    }

    public Cursor getOrganizationPersonsCursor(long orgSqlId) {
        String str = "persons LEFT JOIN organizations ON organizations._id = persons.c_org_id_sql";
        String[] strArr = new String[]{"persons._id", TextUtils.join(Table.COMMA_SEP, getPersonsDataSourceForJoinLazy().getAllColumns()), TextUtils.join(Table.COMMA_SEP, getOrganizationsDataSourceForJoinLazy().getAllColumns())};
        String str2 = "c_org_id_sql = ? AND c_is_active=?";
        String[] strArr2 = new String[]{Long.toString(orgSqlId), String.valueOf(1)};
        String str3 = "persons.c_name_search_field";
        return !(this instanceof SQLiteDatabase) ? query(str, strArr, str2, strArr2, null, null, str3) : SQLiteInstrumentation.query((SQLiteDatabase) this, str, strArr, str2, strArr2, null, null, str3);
    }

    @NonNull
    protected String[] getAllColumns() {
        return ALL_COLUMNS;
    }

    @Nullable
    public Person deflateCursor(@NonNull Cursor cursor) {
        boolean z = true;
        Person person = new Person();
        person.setSqlId(cursor.getLong(cursor.getColumnIndex(COLUMN_PERSON_SQLID_ALIAS)));
        person.setPipedriveId(cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_PERSONS_PIPEDRIVE_ID)));
        person.setName(cursor.getString(cursor.getColumnIndex("c_name")));
        person.setPhones(cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_PERSONS_PHONES)));
        person.setEmails(cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_PERSONS_EMAILS)));
        person.setVisibleTo(cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_PERSONS_VISIBLE_TO)));
        person.setOwnerPipedriveId(cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_PERSONS_OWNER_ID)));
        person.setCustomFields(cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_PERSONS_OTHER_FIELDS)));
        person.setDropBoxAddress(cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_PERSONS_DROP_BOX_ADDRESS)));
        if (cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_PERSONS_IS_ACTIVE)) != 1) {
            z = false;
        }
        person.setIsActive(z);
        Long orgSqlId = CursorHelper.getLong(cursor, PipeSQLiteHelper.COLUMN_PERSONS_ORGANIZATION_SQL_ID);
        if (orgSqlId != null) {
            person.setCompany((Organization) this.organizationsDataSource.findBySqlId(orgSqlId.longValue()));
        }
        person.setOwnerName(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_PERSONS_OWNER_NAME));
        person.setIms(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_PERSONS_IMS));
        person.setPostalAddress(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_PERSONS_POSTAL_ADDRESS));
        person.setPictureId(CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_PERSONS_PICTURE_ID));
        return person;
    }

    @NonNull
    protected String getColumnNameForPipedriveId() {
        return PipeSQLiteHelper.COLUMN_PERSONS_PIPEDRIVE_ID;
    }

    @NonNull
    protected String getColumnNameForSqlId() {
        return PipeSQLiteHelper.COLUMN_ID;
    }

    @NonNull
    protected String getTableName() {
        return "persons";
    }

    @NonNull
    protected ContentValues getContentValues(@NonNull Person person) {
        ContentValues values = new ContentValues();
        if (person.getSqlId() != 0) {
            values.put(PipeSQLiteHelper.COLUMN_ID, Long.valueOf(person.getSqlId()));
        }
        if (person.getCompany() == null || person.getCompany().getSqlId() <= 0) {
            values.putNull(PipeSQLiteHelper.COLUMN_PERSONS_ORGANIZATION_SQL_ID);
        } else {
            values.put(PipeSQLiteHelper.COLUMN_PERSONS_ORGANIZATION_SQL_ID, Long.valueOf(person.getCompany().getSqlId()));
        }
        if (person.isExisting()) {
            values.put(PipeSQLiteHelper.COLUMN_PERSONS_PIPEDRIVE_ID, Integer.valueOf(person.getPipedriveId()));
        }
        values.put("c_name", person.getName());
        values.put(PipeSQLiteHelper.COLUMN_PERSONS_NAME_SEARCH_FIELD, StringUtils.getNormalizedPersonName(person));
        values.put(PipeSQLiteHelper.COLUMN_PERSONS_PHONES, person.getPhonesJsonString());
        values.put(PipeSQLiteHelper.COLUMN_PERSONS_EMAILS, person.getEmailsJsonString());
        values.put(PipeSQLiteHelper.COLUMN_PERSONS_OWNER_ID, Integer.valueOf(person.getOwnerPipedriveId()));
        values.put(PipeSQLiteHelper.COLUMN_PERSONS_VISIBLE_TO, Integer.valueOf(person.getVisibleTo()));
        if (person.getCustomFields() != null) {
            String jSONArrayInstrumentation;
            String str = PipeSQLiteHelper.COLUMN_PERSONS_OTHER_FIELDS;
            JSONArray customFields = person.getCustomFields();
            if (customFields instanceof JSONArray) {
                jSONArrayInstrumentation = JSONArrayInstrumentation.toString(customFields);
            } else {
                jSONArrayInstrumentation = customFields.toString();
            }
            values.put(str, jSONArrayInstrumentation);
        }
        if (person.getDropBoxAddress() != null) {
            values.put(PipeSQLiteHelper.COLUMN_PERSONS_DROP_BOX_ADDRESS, person.getDropBoxAddress());
        }
        values.put(PipeSQLiteHelper.COLUMN_PERSONS_IS_ACTIVE, Boolean.valueOf(person.isActive()));
        values.put(PipeSQLiteHelper.COLUMN_PERSONS_OWNER_NAME, person.getOwnerName());
        if (!TextUtils.isEmpty(person.getImsJsonString())) {
            values.put(PipeSQLiteHelper.COLUMN_PERSONS_IMS, person.getImsJsonString());
        }
        if (!TextUtils.isEmpty(person.getPostalAddress())) {
            values.put(PipeSQLiteHelper.COLUMN_PERSONS_POSTAL_ADDRESS, person.getPostalAddress());
        }
        if (person.getPictureId() != null) {
            values.put(PipeSQLiteHelper.COLUMN_PERSONS_PICTURE_ID, person.getPictureId());
        } else {
            values.putNull(PipeSQLiteHelper.COLUMN_PERSONS_PICTURE_ID);
        }
        values.put(PipeSQLiteHelper.COLUMN_PERSONS_PHONES_SEARCH_FIELD, CommunicationMedium.getNormalizedString(person.getPhones()));
        values.put(PipeSQLiteHelper.COLUMN_PERSONS_IS_SHADOW, Boolean.valueOf(person.isShadow()));
        return values;
    }

    @NonNull
    public SelectionHolder getSelectionForSearch(@NonNull SearchConstraint searchConstraint) {
        return new SelectionHolder(getTableName() + '.' + PipeSQLiteHelper.COLUMN_PERSONS_IS_ACTIVE + " =?", new String[]{String.valueOf(1)}).add(this.searchHelper.getSelectionHolderForSearchConstraint(searchConstraint));
    }
}
