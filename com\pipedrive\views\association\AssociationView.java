package com.pipedrive.views.association;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.pipedrive.R;

abstract class AssociationView<T> extends LinearLayout {
    private static final int ASSOCIATION_ADD_CLEAR_DRAWABLE_LEVEL_ADD = 0;
    private static final int ASSOCIATION_ADD_CLEAR_DRAWABLE_LEVEL_CLEAR = 1;
    @NonNull
    private TextView mAssociation;
    @NonNull
    private ImageView mAssociationAddClear;
    @Nullable
    private AssociationListener<T> mAssociationListener;
    @Nullable
    private T mCurrentlyHandledAssociation;
    @Nullable
    private Integer mLinkPersonRequestCode;
    private boolean mShouldOpenAssociationDetailViewOnClick;

    public interface AssociationListener<T> {
        void onAssociationChanged(@Nullable T t);
    }

    abstract boolean canAssociationBeViewed(@NonNull T t);

    @NonNull
    abstract String getAssociationName(@NonNull T t);

    @StringRes
    abstract int getAssociationNameHint();

    @Nullable
    abstract Integer getAssociationTypeIconImageLevel();

    abstract void openAssociationDetailView(@NonNull T t);

    abstract void requestNewAssociation();

    public AssociationView(Context context) {
        this(context, null);
    }

    public AssociationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AssociationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Nullable
    Integer getRequestCode() {
        return this.mLinkPersonRequestCode;
    }

    private void init() {
        boolean associationTypeIconImageLevelExists;
        LayoutInflater.from(getContext()).inflate(R.layout.layout_association_view, this, true);
        this.mAssociation = (TextView) ButterKnife.findById(this, R.id.association);
        this.mAssociation.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                AssociationView.this.onClickAssociation();
            }
        });
        this.mAssociation.setHint(getAssociationNameHint());
        this.mAssociationAddClear = (ImageView) ButterKnife.findById(this, R.id.associationAddClear);
        this.mAssociationAddClear.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (v instanceof ImageView) {
                    AssociationView.this.onClickAssociationImage((ImageView) v);
                }
            }
        });
        ImageView associationImage = (ImageView) ButterKnife.findById(this, R.id.associationImage);
        if (getAssociationTypeIconImageLevel() != null) {
            associationTypeIconImageLevelExists = true;
        } else {
            associationTypeIconImageLevelExists = false;
        }
        associationImage.setVisibility(associationTypeIconImageLevelExists ? 0 : 4);
        if (associationTypeIconImageLevelExists) {
            associationImage.setImageLevel(getAssociationTypeIconImageLevel().intValue());
        }
        setClickable(true);
    }

    public void setAssociationListener(@Nullable AssociationListener<T> associationListener) {
        this.mAssociationListener = associationListener;
    }

    public void setAssociation(@Nullable T association, @Nullable Integer requestCode) {
        boolean associationExists;
        int i = 1;
        this.mCurrentlyHandledAssociation = association;
        if (this.mCurrentlyHandledAssociation != null) {
            associationExists = true;
        } else {
            associationExists = false;
        }
        this.mAssociation.setText(associationExists ? getAssociationName(association) : "");
        ImageView imageView = this.mAssociationAddClear;
        if (!associationExists) {
            i = 0;
        }
        imageView.setImageLevel(i);
        this.mLinkPersonRequestCode = requestCode;
    }

    public void setAssociation(@Nullable T association) {
        setAssociation(association, null);
    }

    public void enableAssociationDetailViewOpening() {
        this.mShouldOpenAssociationDetailViewOnClick = true;
    }

    @Nullable
    public T getAssociation() {
        return this.mCurrentlyHandledAssociation;
    }

    private void onClickAssociation() {
        if (isClickable()) {
            boolean associationDoesNotExist = getAssociation() == null;
            if (!this.mShouldOpenAssociationDetailViewOnClick || associationDoesNotExist) {
                requestNewAssociation();
            } else {
                requestOpenAssociationDetailView(getAssociation());
            }
        }
    }

    private void requestOpenAssociationDetailView(@NonNull T association) {
        if (canAssociationBeViewed(association)) {
            openAssociationDetailView(association);
        }
    }

    private void onClickAssociationImage(@NonNull ImageView imageView) {
        boolean clearAssociationIsRequested;
        boolean associationAddClearSelected;
        if (imageView.getDrawable().getLevel() == 1) {
            clearAssociationIsRequested = true;
        } else {
            clearAssociationIsRequested = false;
        }
        if (imageView.getId() == R.id.associationAddClear) {
            associationAddClearSelected = true;
        } else {
            associationAddClearSelected = false;
        }
        if (!associationAddClearSelected) {
            return;
        }
        if (clearAssociationIsRequested) {
            setAssociation(null);
            if (this.mAssociationListener != null) {
                this.mAssociationListener.onAssociationChanged(null);
                return;
            }
            return;
        }
        onClickAssociation();
    }
}
