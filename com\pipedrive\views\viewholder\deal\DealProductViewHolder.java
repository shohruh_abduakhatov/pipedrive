package com.pipedrive.views.viewholder.deal;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.pipedrive.application.Session;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.util.formatter.DecimalFormatter;
import com.pipedrive.util.formatter.MonetaryFormatter;
import com.pipedrive.util.formatter.PercentFormatter;

public class DealProductViewHolder extends ViewHolder {
    @BindView(2131821073)
    TextView mProductFormula;
    @BindView(2131821072)
    TextView mProductTitle;
    @BindView(2131821074)
    TextView mProductTotalPrice;

    public DealProductViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void fill(@NonNull Session session, @NonNull DealProduct dealProduct) {
        this.mProductTitle.setText(dealProduct.getProduct().getName());
        this.mProductFormula.setText(getProductFormula(session, dealProduct));
        this.mProductTotalPrice.setText(new MonetaryFormatter(session).formatDealProductSum(dealProduct));
    }

    @NonNull
    private String getProductFormula(@NonNull Session session, @NonNull DealProduct dealProduct) {
        MonetaryFormatter monetaryFormatter = new MonetaryFormatter(session);
        DecimalFormatter decimalFormatter = new DecimalFormatter(session);
        PercentFormatter percentFormatter = new PercentFormatter(session);
        StringBuilder sb = new StringBuilder();
        sb.append(monetaryFormatter.formatDealProductPrice(dealProduct));
        sb.append(" × ");
        sb.append(decimalFormatter.format(dealProduct.getQuantity()));
        if (session.isCompanyFeatureProductDurationsEnabled(false)) {
            sb.append(" × ");
            sb.append(decimalFormatter.format(dealProduct.getDuration()));
        }
        if (dealProduct.getDiscountPercentage().doubleValue() != 0.0d) {
            sb.append(" -");
            sb.append(percentFormatter.formatDealProductDiscount(dealProduct.getDiscountPercentage()));
        }
        return sb.toString();
    }
}
