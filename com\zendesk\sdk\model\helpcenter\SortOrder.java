package com.zendesk.sdk.model.helpcenter;

public enum SortOrder {
    ASCENDING("asc"),
    DESCENDING("desc");
    
    private final String apiValue;

    private SortOrder(String apiValue) {
        this.apiValue = apiValue;
    }

    public String getApiValue() {
        return this.apiValue;
    }
}
