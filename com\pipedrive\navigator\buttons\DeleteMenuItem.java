package com.pipedrive.navigator.buttons;

import com.pipedrive.R;

public abstract class DeleteMenuItem implements NavigatorMenuItem {
    public int getMenuItemId() {
        return R.id.menu_delete;
    }
}
