package com.pipedrive.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AlphabetIndexer;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.views.viewholder.ViewHolderBuilder;
import com.pipedrive.views.viewholder.contact.PersonRowViewHolder;
import com.pipedrive.views.viewholder.contact.PersonRowViewHolder.OnCallButtonClickListener;

public class PeopleListAdapter extends BaseSectionedListAdapter {
    final PersonsDataSource mDataSource;
    private OnCallButtonClickListener mOnCallButtonClickListener;
    @NonNull
    SearchConstraint mSearchConstraint = SearchConstraint.EMPTY;
    final Session mSession;

    public PeopleListAdapter(Context context, Cursor c, Session session) {
        super(context, c, true);
        this.mSession = session;
        this.mDataSource = new PersonsDataSource(session.getDatabase());
        this.mAlphabetIndexer = getCount() <= 0 ? null : new AlphabetIndexer(getCursor(), getCursor().getColumnIndex(PipeSQLiteHelper.COLUMN_PERSONS_NAME_SEARCH_FIELD), this.mDataSource.getAlphabetString(this.mSearchConstraint));
    }

    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return ViewHolderBuilder.inflateViewAndTagWithViewHolder(context, parent, false, new PersonRowViewHolder());
    }

    public void bindView(View view, Context context, Cursor cursor) {
        PersonRowViewHolder personViewHolder = (PersonRowViewHolder) view.getTag();
        personViewHolder.setOnCallButtonClickListener(this.mOnCallButtonClickListener);
        personViewHolder.fill(this.mSession, this.mDataSource.deflateCursor(cursor), this.mSearchConstraint);
    }

    public void setSearchConstraint(@NonNull SearchConstraint searchConstraint) {
        this.mSearchConstraint = searchConstraint;
        changeCursor(this.mDataSource.getSearchCursor(searchConstraint));
        this.mAlphabetIndexer = getCount() <= 0 ? null : new AlphabetIndexer(getCursor(), getCursor().getColumnIndex(PipeSQLiteHelper.COLUMN_PERSONS_NAME_SEARCH_FIELD), this.mDataSource.getAlphabetString(this.mSearchConstraint));
    }

    public void setOnCallButtonClickListener(OnCallButtonClickListener onCallButtonClickListener) {
        this.mOnCallButtonClickListener = onCallButtonClickListener;
    }

    public void changeCursor(Cursor cursor) {
        super.changeCursor(cursor);
        this.mAlphabetIndexer = getCount() <= 0 ? null : new AlphabetIndexer(getCursor(), getCursor().getColumnIndex(PipeSQLiteHelper.COLUMN_PERSONS_NAME_SEARCH_FIELD), this.mDataSource.getAlphabetString(this.mSearchConstraint));
    }
}
