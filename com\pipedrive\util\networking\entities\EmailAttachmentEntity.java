package com.pipedrive.util.networking.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.Date;

public class EmailAttachmentEntity {
    @SerializedName("active_flag")
    @Expose
    private Boolean mActive;
    @SerializedName("add_time")
    @Expose
    private Date mAddTime;
    @SerializedName("clean_name")
    @Expose
    private String mCleanName;
    @SerializedName("comment")
    @Expose
    private String mComment;
    @SerializedName("company_id")
    @Expose
    private Integer mCompanyId;
    @SerializedName("file_name")
    @Expose
    private String mFileName;
    @SerializedName("file_size")
    @Expose
    private Long mFileSize;
    @SerializedName("file_type")
    @Expose
    private String mFileType;
    @SerializedName("id")
    @Expose
    private Integer mId;
    @SerializedName("inline_flag")
    @Expose
    private Boolean mInline;
    @SerializedName("url")
    @Expose
    private String mUrl;

    public Integer getId() {
        return this.mId;
    }

    public void setId(Integer id) {
        this.mId = id;
    }

    public Integer getCompanyId() {
        return this.mCompanyId;
    }

    public void setCompanyId(Integer companyId) {
        this.mCompanyId = companyId;
    }

    public Date getAddTime() {
        return this.mAddTime;
    }

    public void setAddTime(Date addTime) {
        this.mAddTime = addTime;
    }

    public String getFileName() {
        return this.mFileName;
    }

    public void setFileName(String fileName) {
        this.mFileName = fileName;
    }

    public String getCleanName() {
        return this.mCleanName;
    }

    public void setCleanName(String cleanName) {
        this.mCleanName = cleanName;
    }

    public String getFileType() {
        return this.mFileType;
    }

    public void setFileType(String fileType) {
        this.mFileType = fileType;
    }

    public Long getFileSize() {
        return this.mFileSize;
    }

    public void setFileSize(Long fileSize) {
        this.mFileSize = fileSize;
    }

    public Boolean getActive() {
        return this.mActive;
    }

    public void setActive(Boolean active) {
        this.mActive = active;
    }

    public Boolean getInline() {
        return this.mInline;
    }

    public void setInline(Boolean inline) {
        this.mInline = inline;
    }

    public String getComment() {
        return this.mComment;
    }

    public void setComment(String comment) {
        this.mComment = comment;
    }

    public String getUrl() {
        return this.mUrl;
    }

    public void setUrl(String url) {
        this.mUrl = url;
    }
}
