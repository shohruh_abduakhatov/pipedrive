package com.pipedrive.more;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class HelpAndFeedbackActivity_ViewBinding implements Unbinder {
    private HelpAndFeedbackActivity target;
    private View view2131820774;
    private View view2131820775;

    @UiThread
    public HelpAndFeedbackActivity_ViewBinding(HelpAndFeedbackActivity target) {
        this(target, target.getWindow().getDecorView());
    }

    @UiThread
    public HelpAndFeedbackActivity_ViewBinding(final HelpAndFeedbackActivity target, View source) {
        this.target = target;
        View view = Utils.findRequiredView(source, R.id.tipsAndTricks, "method 'onTipsAndTricksClicked'");
        this.view2131820774 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onTipsAndTricksClicked();
            }
        });
        view = Utils.findRequiredView(source, R.id.myFeedback, "method 'onMyFeedbackClicked'");
        this.view2131820775 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onMyFeedbackClicked();
            }
        });
    }

    @CallSuper
    public void unbind() {
        if (this.target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        this.view2131820774.setOnClickListener(null);
        this.view2131820774 = null;
        this.view2131820775.setOnClickListener(null);
        this.view2131820775 = null;
    }
}
