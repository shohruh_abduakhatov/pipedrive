package com.pipedrive.nearby.searchbutton;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;

public final class SearchButtonView_ViewBinding implements Unbinder {
    private SearchButtonView target;
    private View view2131821173;

    @UiThread
    public SearchButtonView_ViewBinding(SearchButtonView target) {
        this(target, target);
    }

    @UiThread
    public SearchButtonView_ViewBinding(final SearchButtonView target, View source) {
        this.target = target;
        View view = Utils.findRequiredView(source, R.id.searchButton, "field 'searchButton' and method 'onSearchButtonClicked'");
        target.searchButton = view;
        this.view2131821173 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onSearchButtonClicked();
            }
        });
        target.animatedVectorContainer = (ImageView) Utils.findRequiredViewAsType(source, R.id.animatedVectorContainer, "field 'animatedVectorContainer'", ImageView.class);
    }

    public void unbind() {
        SearchButtonView target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.searchButton = null;
        target.animatedVectorContainer = null;
        this.view2131821173.setOnClickListener(null);
        this.view2131821173 = null;
    }
}
