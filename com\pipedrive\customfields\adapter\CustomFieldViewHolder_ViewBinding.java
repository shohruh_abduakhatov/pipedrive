package com.pipedrive.customfields.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class CustomFieldViewHolder_ViewBinding implements Unbinder {
    private CustomFieldViewHolder target;

    @UiThread
    public CustomFieldViewHolder_ViewBinding(CustomFieldViewHolder target, View source) {
        this.target = target;
        target.mTitle = (TextView) Utils.findRequiredViewAsType(source, R.id.customFieldRow_title, "field 'mTitle'", TextView.class);
        target.mValue = (TextView) Utils.findOptionalViewAsType(source, R.id.customFieldRow_value, "field 'mValue'", TextView.class);
        target.mIcon = (ImageView) Utils.findOptionalViewAsType(source, R.id.customFieldRow_icon, "field 'mIcon'", ImageView.class);
    }

    @CallSuper
    public void unbind() {
        CustomFieldViewHolder target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mTitle = null;
        target.mValue = null;
        target.mIcon = null;
    }
}
