package com.pipedrive.tasks.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.pipedrive.application.Session;
import com.pipedrive.gson.GsonHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.tasks.AsyncTask;
import com.pipedrive.util.activities.ActivityNetworkingUtil;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.ConnectionUtil$JsonReaderInterceptor;
import com.pipedrive.util.networking.Response;
import com.pipedrive.util.networking.entities.ActivityEntity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ActivitiesListTask extends AsyncTask<String, Integer, Void> {
    public static final String ACTION_ACTIVITIES_DOWNLOADED = "com.pipedrive.ActivitiesListTask.ACTION_ACTIVITIES_DOWNLOADED";
    private static final String PARAM_USER_ID = "user_Id";
    private static final String TAG = ActivitiesListTask.class.getSimpleName();
    private static final Integer VALUE_USER_ID_EVERYONE = Integer.valueOf(0);

    public ActivitiesListTask(@NonNull Session session) {
        super(session);
    }

    protected Void doInBackground(String... params) {
        String tag = TAG + ".doInBackground()";
        Log.d(tag, "Start.");
        Log.d(tag, "Requesting to download undone activities.");
        downloadActivitiesWithRelations(true);
        Log.d(tag, "Requesting to download done activities.");
        downloadActivitiesWithRelations(false);
        Log.d(tag, "Done.");
        return null;
    }

    protected void onPostExecute(Void result) {
        String tag = TAG + ".onPostExecute()";
        Log.d(tag, "Broadcasting done event:com.pipedrive.ActivitiesListTask.ACTION_ACTIVITIES_DOWNLOADED");
        getSession().getApplicationContext().sendBroadcast(new Intent(ACTION_ACTIVITIES_DOWNLOADED));
        Log.d(tag, "Done.");
    }

    @NonNull
    private ApiUrlBuilder getUriForDownloadingAllActivities(boolean requestUndoneActivities) {
        ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder(getSession(), "activities").param(PARAM_USER_ID, VALUE_USER_ID_EVERYONE);
        apiUrlBuilder.param("done", requestUndoneActivities ? "0" : "1");
        return apiUrlBuilder;
    }

    private void downloadActivitiesWithRelations(boolean requestUndoneActivities) {
        ConnectionUtil.requestGetWithAutomaticPaging(getUriForDownloadingAllActivities(requestUndoneActivities), new ConnectionUtil$JsonReaderInterceptor<Response>() {
            public Response interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull Response responseTemplate) throws IOException {
                if (jsonReader.peek() == JsonToken.NULL) {
                    jsonReader.skipValue();
                } else {
                    List<ActivityEntity> downloadedActivityEntities = new ArrayList();
                    if (jsonReader.peek() == JsonToken.BEGIN_ARRAY) {
                        jsonReader.beginArray();
                        while (jsonReader.hasNext()) {
                            downloadedActivityEntities.add((ActivityEntity) GsonHelper.fromJSON(jsonReader, ActivityEntity.class));
                        }
                        jsonReader.endArray();
                    }
                    for (ActivityEntity activityEntity : downloadedActivityEntities) {
                        ActivityNetworkingUtil.createOrUpdateActivityIntoDBWithRelations(session, activityEntity);
                    }
                }
                return responseTemplate;
            }
        });
    }
}
