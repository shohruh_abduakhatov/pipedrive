package com.pipedrive.nearby.toolbar;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;

public interface NearbyToolbar extends HomeButtonClickProvider, NearbyItemTypeChangesProvider {
    void bind();

    void releaseResources();

    void setup(@NonNull Session session);
}
