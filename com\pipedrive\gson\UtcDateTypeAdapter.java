package com.pipedrive.gson;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.pipedrive.datetime.DateFormatHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.util.StringUtils;
import com.pipedrive.util.time.TimeManager;
import java.io.IOException;
import java.text.ParsePosition;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import kotlin.text.Typography;

public class UtcDateTypeAdapter extends TypeAdapter<Date> {
    private static final String GMT_ID = "GMT";
    private static final char OFFSET_TIME = ' ';
    private static final String PATTERN_00_59 = "[0-5]\\d";
    private static final String PATTERN_00_999 = "\\d{2,3}";
    private static final String PATTERN_HHH_MM = "^(\\d{2,3}):([0-5]\\d)$";
    private static final String PATTERN_HHH_MM_SS = "^(\\d{2,3}):([0-5]\\d):([0-5]\\d)$";
    private static final String PATTERN_NEGATIVE_00_00 = "^\\-00:00$";
    private static final String PATTERN_NEGATIVE_00_00_00 = "^\\-00:00:00$";
    private static final String PATTERN_YYYY_MM_DD = "((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])";

    private enum StringType {
        TIME,
        DATE,
        UNKNOWN
    }

    public void write(JsonWriter out, Date date) throws IOException {
        if (date == null) {
            out.nullValue();
        } else {
            out.value(format(date, true, TimeManager.getInstance().getUtcTimeZone()));
        }
    }

    public Date read(JsonReader in) throws IOException {
        switch (in.peek()) {
            case NULL:
                in.nextNull();
                return null;
            default:
                return parse(in.nextString(), new ParsePosition(0));
        }
    }

    @Nullable
    public static Date parseTime(@NonNull String time) {
        boolean willBeParsingTime;
        if (getTypeOf(time) == StringType.TIME) {
            willBeParsingTime = true;
        } else {
            willBeParsingTime = false;
        }
        return willBeParsingTime ? parse(time, new ParsePosition(0)) : null;
    }

    public static Date parseDate(@NonNull String date) {
        boolean willBeParsingDate;
        if (getTypeOf(date) == StringType.DATE) {
            willBeParsingDate = true;
        } else {
            willBeParsingDate = false;
        }
        return willBeParsingDate ? parse(date, new ParsePosition(0)) : null;
    }

    public static Date parseDateTime(@NonNull String dateTime) {
        boolean itsNotTimeNorDateLetsTryDateTime;
        if (getTypeOf(dateTime) == StringType.UNKNOWN) {
            itsNotTimeNorDateLetsTryDateTime = true;
        } else {
            itsNotTimeNorDateLetsTryDateTime = false;
        }
        return itsNotTimeNorDateLetsTryDateTime ? parse(dateTime, new ParsePosition(0)) : null;
    }

    private static StringType getTypeOf(@NonNull String stringToParse) {
        boolean isTimeType = stringToParse.matches(PATTERN_HHH_MM_SS) || stringToParse.matches(PATTERN_HHH_MM) || stringToParse.matches(PATTERN_NEGATIVE_00_00) || stringToParse.matches(PATTERN_NEGATIVE_00_00_00);
        if (isTimeType) {
            return StringType.TIME;
        }
        if (stringToParse.matches(PATTERN_YYYY_MM_DD)) {
            return StringType.DATE;
        }
        return StringType.UNKNOWN;
    }

    private static String format(Date date, boolean millis, TimeZone tz) {
        int length;
        Calendar calendar = new GregorianCalendar(tz, Locale.US);
        calendar.setTime(date);
        int capacity = DateFormatHelper.DATE_FORMAT_LONG.length() + (millis ? ".sss".length() : 0);
        if (tz.getRawOffset() == 0) {
            length = "Z".length();
        } else {
            length = "+hh:mm".length();
        }
        StringBuilder formatted = new StringBuilder(capacity + length);
        padInt(formatted, calendar.get(1), "yyyy".length());
        formatted.append('-');
        padInt(formatted, calendar.get(2) + 1, "MM".length());
        formatted.append('-');
        padInt(formatted, calendar.get(5), "dd".length());
        formatted.append(' ');
        padInt(formatted, calendar.get(11), "hh".length());
        formatted.append(':');
        padInt(formatted, calendar.get(12), "mm".length());
        formatted.append(':');
        padInt(formatted, calendar.get(13), "ss".length());
        if (millis) {
            formatted.append('.');
            padInt(formatted, calendar.get(14), "sss".length());
        }
        int offset = tz.getOffset(calendar.getTimeInMillis());
        if (offset != 0) {
            int hours = Math.abs((offset / 60000) / 60);
            int minutes = Math.abs((offset / 60000) % 60);
            formatted.append(offset < 0 ? '-' : '+');
            padInt(formatted, hours, "hh".length());
            formatted.append(':');
            padInt(formatted, minutes, "mm".length());
        } else {
            formatted.append('Z');
        }
        return formatted.toString();
    }

    private static void padInt(StringBuilder buffer, int value, int length) {
        String strValue = Integer.toString(value);
        for (int i = length - strValue.length(); i > 0; i--) {
            buffer.append('0');
        }
        buffer.append(strValue);
    }

    private static Date parse(String date, ParsePosition pos) {
        if (StringUtils.isTrimmedAndEmpty(date)) {
            return null;
        }
        try {
            int i;
            int offset = pos.getIndex();
            boolean removeMinusFromTheBeginningOfDate = date.matches(PATTERN_NEGATIVE_00_00) || date.matches(PATTERN_NEGATIVE_00_00_00);
            if (removeMinusFromTheBeginningOfDate) {
                date = date.substring(1);
            }
            int year = 1970;
            int month = 1;
            int day = 1;
            boolean dateIncludesDateInfo = (date.matches(PATTERN_HHH_MM_SS) || date.matches(PATTERN_HHH_MM)) ? false : true;
            if (dateIncludesDateInfo) {
                i = offset + 4;
                year = parseInt(date, offset, i);
                if (checkOffset(date, i, '-')) {
                    i++;
                }
                offset = i + 2;
                month = parseInt(date, i, offset);
                if (checkOffset(date, offset, '-')) {
                    i = offset + 1;
                } else {
                    i = offset;
                }
                offset = i + 2;
                day = parseInt(date, i, offset);
            } else {
                date = ' ' + date;
            }
            int hour = 0;
            int minutes = 0;
            int seconds = 0;
            int milliseconds = 0;
            if (checkOffset(date, offset, ' ')) {
                if (checkOffset(date, offset + 4, ':')) {
                    offset++;
                    i = offset + 3;
                    hour = parseInt(date, offset, i);
                    offset = i;
                } else {
                    offset++;
                    i = offset + 2;
                    hour = parseInt(date, offset, i);
                    offset = i;
                }
                if (checkOffset(date, offset, ':')) {
                    i = offset + 1;
                } else {
                    i = offset;
                }
                offset = i + 2;
                minutes = parseInt(date, i, offset);
                if (checkOffset(date, offset, ':')) {
                    i = offset + 1;
                } else {
                    i = offset;
                }
                if (date.length() > i) {
                    char c = date.charAt(i);
                    if (!(c == 'Z' || c == '+' || c == '-')) {
                        offset = i + 2;
                        seconds = parseInt(date, i, offset);
                        if (checkOffset(date, offset, '.')) {
                            offset++;
                            i = offset + 3;
                            milliseconds = parseInt(date, offset, i);
                            offset = i;
                        }
                    }
                }
                offset = i;
            }
            String timezoneId = GMT_ID;
            TimeZone timezone = TimeZone.getTimeZone(timezoneId);
            if (timezone.getID().equals(timezoneId)) {
                Calendar calendar = new GregorianCalendar(timezone);
                calendar.setLenient(false);
                calendar.set(1, year);
                calendar.set(2, month - 1);
                calendar.set(5, day);
                calendar.set(11, 0);
                calendar.add(11, hour);
                calendar.set(12, minutes);
                calendar.set(13, seconds);
                calendar.set(14, milliseconds);
                pos.setIndex(offset);
                return calendar.getTime();
            }
            throw new IndexOutOfBoundsException();
        } catch (Exception e) {
            Log.e(e);
            LogJourno.reportEvent(EVENT.JsonParser_UtcDateTypeAdapterFailed, "i:" + (date == null ? null : Typography.quote + date + "'"), e);
            return null;
        }
    }

    private static boolean checkOffset(String value, int offset, char expected) {
        return offset < value.length() && value.charAt(offset) == expected;
    }

    private static int parseInt(String value, int beginIndex, int endIndex) throws NumberFormatException {
        if (beginIndex < 0 || endIndex > value.length() || beginIndex > endIndex) {
            throw new NumberFormatException(value);
        }
        int i;
        int digit;
        int i2 = beginIndex;
        int result = 0;
        if (i2 < endIndex) {
            i = i2 + 1;
            digit = Character.digit(value.charAt(i2), 10);
            if (digit < 0) {
                throw new NumberFormatException("Invalid number: " + value);
            }
            result = -digit;
        } else {
            i = i2;
        }
        while (i < endIndex) {
            i2 = i + 1;
            digit = Character.digit(value.charAt(i), 10);
            if (digit < 0) {
                throw new NumberFormatException("Invalid number: " + value);
            }
            result = (result * 10) - digit;
            i = i2;
        }
        return -result;
    }
}
