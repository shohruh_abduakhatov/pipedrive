package com.pipedrive.nearby.cards.cards;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.ViewGroup;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.AnalyticsEvent;
import com.pipedrive.model.Person;
import com.pipedrive.model.contact.CommunicationMedium;
import com.pipedrive.nearby.model.PersonNearbyItem;

public class SinglePersonCard extends SingleCardWithCommunicationButtons<PersonNearbyItem, Person> {
    public SinglePersonCard(@NonNull ViewGroup parent, @LayoutRes @NonNull Integer layoutResId) {
        super(parent, layoutResId);
    }

    @NonNull
    CardHeader<PersonNearbyItem, Person> getHeader() {
        return new PersonCardHeader();
    }

    void onDetailsRequested(@NonNull Long sqlId) {
        CardEventBus.INSTANCE.onPersonDetailsClicked(sqlId);
    }

    @Nullable
    public Person getEntity() {
        return (Person) this.entity;
    }

    public void onCallRequested(@NonNull CommunicationMedium medium, @NonNull Person person) {
        super.onCallRequested(medium, person);
        Analytics.sendEvent(AnalyticsEvent.CALL_PERSON);
    }

    public void onSmsRequested(@NonNull CommunicationMedium medium, @NonNull Person person) {
        super.onSmsRequested(medium, person);
        Analytics.sendEvent(AnalyticsEvent.TEXT_MESSAGE_PERSON);
    }

    public void onEmailRequested(@NonNull CommunicationMedium medium, @NonNull Person person) {
        Analytics.sendEvent(AnalyticsEvent.EMAIL_PERSON);
        super.onEmailRequested(medium, person);
    }
}
