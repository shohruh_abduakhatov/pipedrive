package com.zendesk.logger;

import android.os.Build.VERSION;
import android.util.Log;
import com.zendesk.service.ErrorResponse;
import com.zendesk.util.StringUtils;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Logger {
    private static final String ISO_8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    private static final TimeZone UTC_TIMEZONE = TimeZone.getTimeZone("UTC");
    private static boolean sLoggable = false;
    private static Platform sPlatform;

    interface Platform {
        boolean appendUtcInLogs(String str);

        void log(Priority priority, String str, String str2, Throwable th);
    }

    static class Android implements Platform {
        private static final int MAX_LINE_LENGTH = 4000;

        Android() {
        }

        public boolean appendUtcInLogs(String tag) {
            return StringUtils.hasLength(tag) && (tag.endsWith("Provider") || tag.endsWith("Service"));
        }

        public void log(Priority priority, String tag, String message, Throwable throwable) {
            if (Logger.sLoggable) {
                String androidTag = LoggerHelper.getAndroidTag(tag);
                if (appendUtcInLogs(tag) && Priority.ERROR == priority) {
                    SimpleDateFormat utcFormat = new SimpleDateFormat(Logger.ISO_8601_FORMAT, Locale.US);
                    utcFormat.setTimeZone(Logger.UTC_TIMEZONE);
                    Log.println(Priority.ERROR.priority, androidTag, "Time in UTC: " + utcFormat.format(new Date()));
                }
                if (throwable != null) {
                    message = message + StringUtils.LINE_SEPARATOR + Log.getStackTraceString(throwable);
                }
                for (String line : LoggerHelper.splitLogMessage(message, 4000)) {
                    Log.println(priority == null ? Priority.INFO.priority : priority.priority, androidTag, line);
                }
            }
        }
    }

    static class Java implements Platform {
        Java() {
        }

        public boolean appendUtcInLogs(String tag) {
            return false;
        }

        public void log(Priority priority, String tag, String message, Throwable throwable) {
            if (Logger.sLoggable) {
                StringBuilder logBuilder = new StringBuilder(100);
                StringBuilder append = logBuilder.append("[").append(new SimpleDateFormat(Logger.ISO_8601_FORMAT, Locale.US).format(new Date())).append("]").append(" ").append(priority == null ? LoggerHelper.getLevelFromPriority(Priority.INFO.priority) : LoggerHelper.getLevelFromPriority(priority.priority)).append("/");
                if (!StringUtils.hasLength(tag)) {
                    tag = "UNKNOWN";
                }
                append.append(tag).append(": ").append(message);
                System.out.println(logBuilder.toString());
                if (throwable != null) {
                    throwable.printStackTrace(System.out);
                }
            }
        }
    }

    enum Priority {
        VERBOSE(2),
        DEBUG(3),
        INFO(4),
        WARN(5),
        ERROR(6);
        
        private final int priority;

        private Priority(int priority) {
            this.priority = priority;
        }
    }

    static {
        try {
            Class.forName("android.os.Build");
            if (VERSION.SDK_INT != 0) {
                sPlatform = new Android();
            }
            if (sPlatform == null) {
                sPlatform = new Java();
            }
        } catch (ClassNotFoundException e) {
            if (sPlatform == null) {
                sPlatform = new Java();
            }
        } catch (Throwable th) {
            if (sPlatform == null) {
                sPlatform = new Java();
            }
        }
    }

    private Logger() {
    }

    public static boolean isLoggable() {
        return sLoggable;
    }

    public static void setLoggable(boolean loggable) {
        sLoggable = loggable;
    }

    public static void w(String tag, String message, Object... args) {
        logInternal(Priority.WARN, tag, message, null, args);
    }

    public static void w(String tag, String message, Throwable throwable, Object... args) {
        logInternal(Priority.WARN, tag, message, throwable, args);
    }

    public static void e(String tag, String message, Object... args) {
        logInternal(Priority.ERROR, tag, message, null, args);
    }

    public static void e(String tag, String message, Throwable throwable, Object... args) {
        logInternal(Priority.ERROR, tag, message, throwable, args);
    }

    public static void v(String tag, String message, Object... args) {
        logInternal(Priority.VERBOSE, tag, message, null, args);
    }

    public static void v(String tag, String message, Throwable throwable, Object... args) {
        logInternal(Priority.VERBOSE, tag, message, throwable, args);
    }

    public static void i(String tag, String message, Object... args) {
        logInternal(Priority.INFO, tag, message, null, args);
    }

    public static void i(String tag, String message, Throwable throwable, Object... args) {
        logInternal(Priority.INFO, tag, message, throwable, args);
    }

    public static void d(String tag, String message, Object... args) {
        logInternal(Priority.DEBUG, tag, message, null, args);
    }

    public static void d(String tag, String message, Throwable throwable, Object... args) {
        logInternal(Priority.DEBUG, tag, message, throwable, args);
    }

    public static void e(String tag, ErrorResponse error) {
        StringBuilder messageBuilder = new StringBuilder();
        if (error != null) {
            messageBuilder.append("Network Error: ").append(error.isNetworkError());
            messageBuilder.append(", Status Code: ").append(error.getStatus());
            if (StringUtils.hasLength(error.getReason())) {
                messageBuilder.append(", Reason: ").append(error.getReason());
            }
        }
        String message = messageBuilder.toString();
        Priority priority = Priority.ERROR;
        if (!StringUtils.hasLength(message)) {
            message = "Unknown error";
        }
        logInternal(priority, tag, message, null, new Object[0]);
    }

    private static void logInternal(Priority priority, String tag, String message, Throwable throwable, Object... args) {
        if (args != null && args.length > 0) {
            message = String.format(Locale.US, message, args);
        }
        sPlatform.log(priority, tag, message, throwable);
    }
}
