package com.pipedrive.changes;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class Change {
    private Long mChangeMetaData = null;
    @NonNull
    private ChangeOperationType mChangeOperationType = ChangeOperationType.OPERATION_UNDEFINED;
    protected int mChangeTypeIdentifier = 0;

    public enum ChangeOperationType {
        OPERATION_UNDEFINED(0),
        OPERATION_CREATE(1),
        OPERATION_UPDATE(2),
        OPERATION_DELETE(3);
        
        private int mOpId;

        private ChangeOperationType(int opId) {
            this.mOpId = 0;
            this.mOpId = opId;
        }

        public int getUniqueId() {
            return this.mOpId;
        }

        @NonNull
        public static ChangeOperationType getChangeOperationTypeById(int opId) {
            switch (opId) {
                case 1:
                    return OPERATION_CREATE;
                case 2:
                    return OPERATION_UPDATE;
                case 3:
                    return OPERATION_DELETE;
                default:
                    return OPERATION_UNDEFINED;
            }
        }
    }

    protected Change() {
    }

    protected void setChangeOperationType(@NonNull ChangeOperationType changeOperationType) {
        this.mChangeOperationType = changeOperationType;
    }

    @NonNull
    public ChangeOperationType getChangeOperationType() {
        return this.mChangeOperationType;
    }

    protected void setChangeMetaData(@Nullable Long changeMetaData) {
        this.mChangeMetaData = changeMetaData;
    }

    public final int getTypeIdentifier() {
        return this.mChangeTypeIdentifier;
    }

    @Nullable
    public final Long getMetaData() {
        return this.mChangeMetaData;
    }

    public String toString() {
        String str = " Change: [operation type:%s][operation identifier: %s][operation meta data: %s][class name: %s]";
        Object[] objArr = new Object[4];
        objArr[0] = this.mChangeOperationType;
        objArr[1] = Integer.valueOf(this.mChangeTypeIdentifier);
        objArr[2] = this.mChangeMetaData != null ? Long.valueOf(this.mChangeMetaData.longValue()) : "NULL";
        objArr[3] = getClass().getSimpleName();
        return String.format(str, objArr);
    }

    @NonNull
    public static Change restoreChange(int changeOperationTypeId, int changeTypeIdentifier, @Nullable Long operationMetaData) {
        Change change = new Change();
        change.setChangeOperationType(ChangeOperationType.getChangeOperationTypeById(changeOperationTypeId));
        change.mChangeTypeIdentifier = changeTypeIdentifier;
        change.mChangeMetaData = operationMetaData;
        return change;
    }
}
