package com.pipedrive.linking;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import com.pipedrive.R;
import com.pipedrive.adapter.BaseSectionedListAdapter;
import com.pipedrive.adapter.DealListAdapter;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.deal.DealEditActivity;

public class DealLinkingActivity extends BaseLinkingActivity {
    private static final String ARG_ORG_SQL_ID = "org_sql_id";
    private static final String ARG_PERSON_SQL_ID = "person_sql_id";
    private static final int GET_NEW_DEAL_REQUEST_CODE = 1;
    public static final String KEY_DEAL_SQL_ID = "DEAL_SQL_ID";

    public static void startActivityForResult(@NonNull Activity context, int requestCode, @Nullable Long personSqlId, @Nullable Long organizationSqlId) {
        Intent intent = new Intent(context, DealLinkingActivity.class);
        if (personSqlId != null) {
            intent.putExtra(ARG_PERSON_SQL_ID, personSqlId);
        }
        if (organizationSqlId != null) {
            intent.putExtra(ARG_ORG_SQL_ID, organizationSqlId);
        }
        ActivityCompat.startActivityForResult(context, intent, requestCode, null);
    }

    void onAddClicked() {
        Bundle intentExtras = getIntent().getExtras();
        Long personSqlId = null;
        Long orgSqlId = null;
        if (intentExtras != null) {
            if (intentExtras.containsKey(ARG_PERSON_SQL_ID)) {
                personSqlId = Long.valueOf(intentExtras.getLong(ARG_PERSON_SQL_ID));
            } else {
                personSqlId = null;
            }
            if (intentExtras.containsKey(ARG_ORG_SQL_ID)) {
                orgSqlId = Long.valueOf(intentExtras.getLong(ARG_ORG_SQL_ID));
            } else {
                orgSqlId = null;
            }
        }
        DealEditActivity.startActivityForResult(this, this.mSearchConstraint.toString(), 1, personSqlId, orgSqlId);
    }

    public int getSearchHint() {
        return R.string.search_deals;
    }

    @Nullable
    public Drawable getFabIcon() {
        return ContextCompat.getDrawable(this, R.drawable.icon_add);
    }

    @NonNull
    public BaseSectionedListAdapter getAdapter() {
        return new DealListAdapter(this, getCursor(), getSession());
    }

    @Nullable
    public Cursor getCursor() {
        return new DealsDataSource(getSession().getDatabase()).getSearchCursor(this.mSearchConstraint);
    }

    @NonNull
    public String getKey() {
        return "DEAL_SQL_ID";
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == -1) {
            setSelectedItemSqlId(data.getLongExtra("DEAL_SQL_ID", 0));
        }
    }
}
