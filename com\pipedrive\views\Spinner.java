package com.pipedrive.views;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatSpinner;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import com.pipedrive.R;
import java.util.List;

public class Spinner<T> extends AppCompatSpinner {
    public static final int DEFAULT_LAYOUT_DROPDOWN_ITEM = 2130903291;
    public static final int DEFAULT_LAYOUT_SELECTED_ITEM_NEW = 2130903293;
    static final String TAG = Spinner.class.getSimpleName();

    public interface LabelBuilder<T> {
        String getLabel(T t);

        boolean isSelected(T t);
    }

    public interface OnItemSelectedListener<T> {
        void onItemSelected(T t);
    }

    private static class SpinnerDataHolder<T> {
        T data;
        String label;

        private SpinnerDataHolder() {
            this.data = null;
            this.label = "";
        }

        public String toString() {
            return this.label;
        }
    }

    public Spinner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public Spinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Spinner(Context context) {
        super(context);
    }

    public void load(@NonNull List<T> spinnerData, @NonNull LabelBuilder<T> spinnerBuilder) {
        loadAndInit(spinnerData, spinnerBuilder, null, R.layout.row_simple_spinner_selected_item_new, R.layout.row_simple_spinner_dropdown_item);
    }

    public void loadAndInit(@NonNull List<T> spinnerData, @NonNull LabelBuilder<T> spinnerBuilder, @NonNull OnItemSelectedListener<T> onItemSelectedListener) {
        loadAndInit(spinnerData, spinnerBuilder, onItemSelectedListener, R.layout.row_simple_spinner_selected_item_new, R.layout.row_simple_spinner_dropdown_item);
    }

    public void load(@NonNull List<T> spinnerData, @NonNull LabelBuilder<T> spinnerBuilder, @LayoutRes int selectedItemLayoutResId, @LayoutRes int dropdownItemLayoutResId) {
        loadAndInit(spinnerData, spinnerBuilder, null, selectedItemLayoutResId, dropdownItemLayoutResId);
    }

    public void loadAndInit(@NonNull List<T> spinnerData, @NonNull LabelBuilder<T> spinnerBuilder, @Nullable OnItemSelectedListener<T> onItemSelectedListener, @LayoutRes int selectedItemLayoutResId, @LayoutRes int dropdownItemLayoutResId) {
        int currentSelectedIdx = Integer.MIN_VALUE;
        SpinnerDataHolder[] spinnerDataHolderObjects = new SpinnerDataHolder[spinnerData.size()];
        for (int i = 0; i < spinnerData.size(); i++) {
            SpinnerDataHolder<T> spinnerDataHolder = new SpinnerDataHolder();
            T spinnerDataItem = spinnerData.get(i);
            spinnerDataHolder.data = spinnerDataItem;
            spinnerDataHolder.label = spinnerBuilder.getLabel(spinnerDataItem);
            if (spinnerDataHolder.label == null) {
                spinnerDataHolder.label = null;
            }
            if (spinnerBuilder.isSelected(spinnerDataItem)) {
                currentSelectedIdx = i;
            }
            spinnerDataHolderObjects[i] = spinnerDataHolder;
        }
        ArrayAdapter<SpinnerDataHolder> adapter = new ArrayAdapter<SpinnerDataHolder>(getContext(), selectedItemLayoutResId, spinnerDataHolderObjects) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                if (convertView == null) {
                    view.setPadding(0, 0, 0, 0);
                }
                return view;
            }
        };
        adapter.setDropDownViewResource(dropdownItemLayoutResId);
        setAdapter(adapter);
        if (currentSelectedIdx != Integer.MIN_VALUE) {
            setSelection(currentSelectedIdx);
        }
        setOnItemSelectedListener(onItemSelectedListener);
    }

    public void setOnItemSelectedListener(@Nullable final OnItemSelectedListener<T> onItemSelectedListener) {
        if (onItemSelectedListener != null) {
            super.setOnItemSelectedListener(new android.widget.AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    SpinnerDataHolder<T> item = parent.getItemAtPosition(position);
                    if (item instanceof SpinnerDataHolder) {
                        onItemSelectedListener.onItemSelected(item.data);
                    }
                }

                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
        } else {
            super.setOnItemSelectedListener(null);
        }
    }

    public T getSelectedTypedItem() {
        SpinnerDataHolder<T> aauh = (SpinnerDataHolder) super.getSelectedItem();
        if (aauh == null) {
            return null;
        }
        return aauh.data;
    }

    public Object getSelectedItem() {
        throw new RuntimeException("Do not use! Use getSelectedTypedItem() instead!");
    }
}
