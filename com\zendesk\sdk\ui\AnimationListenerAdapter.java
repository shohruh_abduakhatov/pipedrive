package com.zendesk.sdk.ui;

import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

class AnimationListenerAdapter implements AnimationListener {
    AnimationListenerAdapter() {
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }
}
