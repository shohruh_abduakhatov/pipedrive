package com.pipedrive.deal;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.model.Deal;
import com.pipedrive.tasks.AsyncTask;

class ReadDealTask extends AsyncTask<Long, Void, Deal> {
    @Nullable
    private final Callback callback;

    interface Callback {
        void onTaskFinished(@Nullable Deal deal);
    }

    ReadDealTask(@NonNull Session session, @Nullable Callback callback) {
        super(session);
        this.callback = callback;
    }

    protected Deal doInBackground(Long... params) {
        if (params == null || params.length == 0) {
            return null;
        }
        return (Deal) new DealsDataSource(getSession().getDatabase()).findBySqlId(params[0].longValue());
    }

    protected void onPostExecute(@Nullable Deal deal) {
        if (this.callback != null) {
            this.callback.onTaskFinished(deal);
        }
    }
}
