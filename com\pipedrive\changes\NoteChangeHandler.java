package com.pipedrive.changes;

import android.support.annotation.NonNull;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.pipedrive.application.Session;
import com.pipedrive.changes.Change.ChangeOperationType;
import com.pipedrive.datasource.NotesDataSource;
import com.pipedrive.gson.GsonHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.notes.Note;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.ConnectionUtil.JsonReaderInterceptor;
import com.pipedrive.util.networking.Response;
import com.pipedrive.util.networking.entities.NoteEntity;
import java.io.IOException;
import java.lang.reflect.Type;

class NoteChangeHandler extends ChangeHandler {
    NoteChangeHandler() {
    }

    @NonNull
    public ChangeHandled handleChange(@NonNull Session session, @NonNull Change noteChange) {
        if (noteChange.getChangeOperationType() != ChangeOperationType.OPERATION_CREATE && noteChange.getChangeOperationType() != ChangeOperationType.OPERATION_UPDATE) {
            LogJourno.reportEvent(EVENT.ChangesHandler_corruptedChangeFound, String.format("Change:[%s]", new Object[]{noteChange}));
            Log.e(new Throwable(String.format("Unknown change type requested for note: %s", new Object[]{noteChange})));
            return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
        } else if (!ConnectionUtil.isConnected(session.getApplicationContext())) {
            return ChangeHandled.NOT_PROCESSED_PLEASE_RETRY;
        } else {
            boolean isUpdateNoteOperation;
            if (noteChange.getChangeOperationType() == ChangeOperationType.OPERATION_UPDATE) {
                isUpdateNoteOperation = true;
            } else {
                isUpdateNoteOperation = false;
            }
            JsonReaderInterceptor<AnonymousClass1NoteResponse> noteResponseParser = new JsonReaderInterceptor<AnonymousClass1NoteResponse>() {
                public AnonymousClass1NoteResponse interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull AnonymousClass1NoteResponse responseTemplate) throws IOException {
                    if (jsonReader.peek() == JsonToken.NULL) {
                        jsonReader.skipValue();
                    } else {
                        NoteEntity noteEntity = (NoteEntity) GsonHelper.fromJSON(jsonReader, (Type) NoteEntity.class);
                        if (noteEntity == null) {
                            LogJourno.reportEvent(EVENT.ChangesHandler_changeDataResponseToJsonParsingFailed, String.format("NoteResponseTemplate:[%s]", new Object[]{responseTemplate}));
                            Log.e(new Throwable(String.format("Parsing of Note from response failed! Response: %s", new Object[]{responseTemplate})));
                        } else if (responseTemplate.noteUnderChangeSqlId <= 0 || noteEntity.pipedriveId <= 0) {
                            LogJourno.reportEvent(EVENT.ChangesHandler_cannotUpdatePipedriveIdFromChangeDataResponse, String.format("NoteResponseTemplate:[%s] NoteFromResponse:[%s]", new Object[]{responseTemplate, noteEntity}));
                            Log.e(new Throwable(String.format("Cannot update PD ID! SQL ID: %s; PD ID: %s", new Object[]{Long.valueOf(responseTemplate.noteUnderChangeSqlId), Integer.valueOf(noteEntity.pipedriveId)})));
                        } else {
                            new NotesDataSource(session.getDatabase()).updatePipedriveIdBySqlId(responseTemplate.noteUnderChangeSqlId, noteEntity.pipedriveId);
                        }
                    }
                    return responseTemplate;
                }
            };
            Long noteSqlId = noteChange.getMetaData();
            if (noteSqlId == null) {
                LogJourno.reportEvent(EVENT.ChangesHandler_changeMetadataNotFound, String.format("Change:[%s]", new Object[]{noteChange}));
                Log.e(new Throwable("Change metadata is missing! Cannot find the Note to change!"));
                return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
            }
            Note noteUnderChange = (Note) new NotesDataSource(session.getDatabase()).findBySqlId(noteSqlId.longValue());
            if (noteUnderChange == null) {
                LogJourno.reportEvent(EVENT.ChangesHandler_changeDataNotFound, String.format("Change:[%s] SQL id:[%s]", new Object[]{noteChange, noteSqlId}));
                Log.e(new Throwable(String.format("Change metadata is present (%s) but unable to find Note that was registered for the change!", new Object[]{noteSqlId})));
                return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
            }
            Response result;
            if (!noteUnderChange.isAllAssociationsExisting()) {
                LogJourno.reportEvent(EVENT.ChangesHandler_notAllChangePipedriveAssociationsExist, String.format("Change:[%s]", new Object[]{noteChange}));
                Log.e(new Throwable(String.format("Note associations are not all existing! Change: %s", new Object[]{noteChange})));
            }
            ApiUrlBuilder apiUriBuilder = new ApiUrlBuilder(session, "notes");
            if (isUpdateNoteOperation) {
                apiUriBuilder.appendEncodedPath(Integer.toString(noteUnderChange.getPipedriveId()));
            }
            AnonymousClass1NoteResponse noteResponse = new Response() {
                long noteUnderChangeSqlId;

                public String toString() {
                    return "NoteResponse:[noteUnderChangeSqlId=" + this.noteUnderChangeSqlId + "][super.toString:[" + super.toString() + "]]";
                }
            };
            noteResponse.noteUnderChangeSqlId = noteSqlId.longValue();
            if (isUpdateNoteOperation) {
                result = (AnonymousClass1NoteResponse) ConnectionUtil.requestPut(apiUriBuilder, NoteEntity.getJSON(noteUnderChange).toString(), noteResponseParser, noteResponse);
            } else {
                result = (AnonymousClass1NoteResponse) ConnectionUtil.requestPost(apiUriBuilder, NoteEntity.getJSON(noteUnderChange).toString(), noteResponseParser, noteResponse);
            }
            return parseResponse(result, this, noteChange);
        }
    }
}
