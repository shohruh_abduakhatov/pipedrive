package com.zendesk.sdk.util;

import android.support.annotation.NonNull;
import com.zendesk.sdk.network.impl.ApplicationScope;
import com.zendesk.sdk.network.impl.RestAdapterInjector;
import com.zendesk.sdk.network.impl.RestAdapterModule;

public class ModuleInjector {
    public static LibraryModule injectCachedLibraryModule(final ApplicationScope applicationScope) {
        return (LibraryModule) injectLibraryModuleCache(applicationScope).get(new DependencyProvider<LibraryModule>() {
            @NonNull
            public LibraryModule provideDependency() {
                return ModuleInjector.injectLibraryModule(applicationScope);
            }
        });
    }

    public static RestAdapterModule injectCachedRestAdapterModule(final ApplicationScope applicationScope) {
        return (RestAdapterModule) injectRestAdapterModuleCache(applicationScope).get(new DependencyProvider<RestAdapterModule>() {
            @NonNull
            public RestAdapterModule provideDependency() {
                return ModuleInjector.injectRestAdapterModule(applicationScope);
            }
        });
    }

    private static ScopeCache<LibraryModule> injectLibraryModuleCache(ApplicationScope applicationScope) {
        return applicationScope.getLibraryModuleCache();
    }

    private static ScopeCache<RestAdapterModule> injectRestAdapterModuleCache(ApplicationScope applicationScope) {
        return applicationScope.getRestAdapterCache();
    }

    private static RestAdapterModule injectRestAdapterModule(ApplicationScope applicationScope) {
        return new RestAdapterModule(RestAdapterInjector.injectRestAdapter(applicationScope));
    }

    private static LibraryModule injectLibraryModule(ApplicationScope applicationScope) {
        return new LibraryModule(LibraryInjector.injectGson(applicationScope), LibraryInjector.injectOkHttpClient(applicationScope));
    }
}
