package com.zendesk.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

public class DigestUtils {
    private static final String MD5 = "MD5";
    private static final String SHA1 = "SHA-1";
    private static final String SHA256 = "SHA-256";
    private static final String SHA384 = "SHA-384";
    private static final String SHA512 = "SHA-512";

    public static String md5(String input) {
        if (StringUtils.hasLength(input)) {
            return hexString(digest("MD5", input));
        }
        return "";
    }

    public static String sha1(String input) {
        if (StringUtils.hasLength(input)) {
            return hexString(digest("SHA-1", input));
        }
        return "";
    }

    public static String sha256(String input) {
        if (StringUtils.hasLength(input)) {
            return hexString(digest(SHA256, input));
        }
        return "";
    }

    public static String sha384(String input) {
        if (StringUtils.hasLength(input)) {
            return hexString(digest(SHA384, input));
        }
        return "";
    }

    public static String sha512(String input) {
        if (StringUtils.hasLength(input)) {
            return hexString(digest(SHA512, input));
        }
        return "";
    }

    private static byte[] digest(String hashType, String input) {
        if (!StringUtils.hasLength(hashType) || !StringUtils.hasLength(input)) {
            return new byte[0];
        }
        try {
            MessageDigest digest = MessageDigest.getInstance(hashType);
            digest.update(input.getBytes());
            return digest.digest();
        } catch (NoSuchAlgorithmException e) {
            return new byte[0];
        }
    }

    private static String hexString(byte[] input) {
        StringBuilder sb = new StringBuilder(input.length * 2);
        for (byte b : input) {
            sb.append(String.format(Locale.US, "%02x", new Object[]{Integer.valueOf(b & 255)}));
        }
        return sb.toString();
    }
}
