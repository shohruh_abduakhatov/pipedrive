package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.location.internal.ClientIdentity;
import java.util.List;

public class zzd implements Creator<ActivityTransitionRequest> {
    static void zza(ActivityTransitionRequest activityTransitionRequest, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zzc(parcel, 1, activityTransitionRequest.zzbpy(), false);
        zzb.zza(parcel, 2, activityTransitionRequest.getTag(), false);
        zzb.zzc(parcel, 3, activityTransitionRequest.zzbpz(), false);
        zzb.zzc(parcel, 1000, activityTransitionRequest.getVersionCode());
        zzb.zzaj(parcel, zzcs);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zznr(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzui(i);
    }

    public ActivityTransitionRequest zznr(Parcel parcel) {
        List list = null;
        int zzcr = zza.zzcr(parcel);
        int i = 0;
        String str = null;
        List list2 = null;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    list2 = zza.zzc(parcel, zzcq, ActivityTransition.CREATOR);
                    break;
                case 2:
                    str = zza.zzq(parcel, zzcq);
                    break;
                case 3:
                    list = zza.zzc(parcel, zzcq, ClientIdentity.CREATOR);
                    break;
                case 1000:
                    i = zza.zzg(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new ActivityTransitionRequest(i, list2, str, list);
        }
        throw new zza.zza("Overread allowed size end=" + zzcr, parcel);
    }

    public ActivityTransitionRequest[] zzui(int i) {
        return new ActivityTransitionRequest[i];
    }
}
