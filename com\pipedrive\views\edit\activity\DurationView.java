package com.pipedrive.views.edit.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog.Builder;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.pipedrive.R;
import com.pipedrive.datetime.PipedriveTime;
import com.pipedrive.util.StringUtils;
import com.pipedrive.views.common.ClickableTextViewWithUnderlineAndLabel;
import java.util.Arrays;

public class DurationView extends ClickableTextViewWithUnderlineAndLabel {

    public interface OnDurationChangedListener {
        void onDurationChanged(long j);
    }

    public interface OnDurationClearedListener {
        void onDurationCleared();
    }

    public DurationView(Context context) {
        this(context, null);
    }

    public DurationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DurationView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected int getLabelTextResourceId() {
        return R.string.duration;
    }

    public void setOnDurationChangedListeners(@Nullable final OnDurationChangedListener onDurationChangedListener, @Nullable final OnDurationClearedListener onDurationClearedListener) {
        setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String activityDuration = ((TextView) DurationView.this.mMainView).getText().toString();
                final String[] activityDurationChoices = DurationView.this.getResources().getStringArray(R.array.activity_duration);
                boolean activityDurationIsSet = !StringUtils.isTrimmedAndEmpty(activityDuration);
                int selectedActivityDurationChoice = activityDurationIsSet ? Arrays.binarySearch(activityDurationChoices, activityDuration) : -1;
                Builder builder = new Builder(v.getContext(), R.style.Theme.Pipedrive.Dialog.Alert);
                builder.setTitle(R.string.duration).setSingleChoiceItems(R.array.activity_duration, selectedActivityDurationChoice, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (onDurationChangedListener != null) {
                            String newSelectedDurationAsHHMM = activityDurationChoices[which];
                            DurationView.this.setText(newSelectedDurationAsHHMM);
                            PipedriveTime duration = PipedriveTime.instanceFormApiShortRepresentation(newSelectedDurationAsHHMM);
                            if (duration != null) {
                                onDurationChangedListener.onDurationChanged(duration.getRepresentationInUnixTime());
                            }
                        }
                        dialog.dismiss();
                    }
                }).setNegativeButton(17039360, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
                if (activityDurationIsSet) {
                    builder.setNeutralButton(R.string.clear, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (onDurationClearedListener != null) {
                                DurationView.this.setText(null);
                                onDurationClearedListener.onDurationCleared();
                            }
                        }
                    });
                }
                builder.create().show();
            }
        });
    }
}
