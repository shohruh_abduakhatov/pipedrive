package com.google.android.gms.auth.api;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.zzf;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.internal.zzny;

public final class zza {
    public static final Api<zzb> API = new Api("Auth.PROXY_API", ij, ii);
    public static final zzf<zzny> ii = new zzf();
    private static final com.google.android.gms.common.api.Api.zza<zzny, zzb> ij = new com.google.android.gms.common.api.Api.zza<zzny, zzb>() {
        public zzny zza(Context context, Looper looper, com.google.android.gms.common.internal.zzf com_google_android_gms_common_internal_zzf, zzb com_google_android_gms_auth_api_zzb, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            return new zzny(context, looper, com_google_android_gms_common_internal_zzf, com_google_android_gms_auth_api_zzb, connectionCallbacks, onConnectionFailedListener);
        }
    };
}
