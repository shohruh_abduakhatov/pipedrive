package com.pipedrive.fragments;

import android.support.annotation.UiThread;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;
import com.pipedrive.views.fab.FabWithOverlayMenu;

public final class OrganizationDetailFragment_ViewBinding implements Unbinder {
    private OrganizationDetailFragment target;
    private View view2131820923;

    @UiThread
    public OrganizationDetailFragment_ViewBinding(final OrganizationDetailFragment target, View source) {
        this.target = target;
        target.mNameTextView = (TextView) Utils.findRequiredViewAsType(source, R.id.name, "field 'mNameTextView'", TextView.class);
        View view = Utils.findRequiredView(source, R.id.address, "field 'mAddressTextView' and method 'onAddressClicked'");
        target.mAddressTextView = (TextView) Utils.castView(view, R.id.address, "field 'mAddressTextView'", TextView.class);
        this.view2131820923 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onAddressClicked();
            }
        });
        target.mViewPager = (ViewPager) Utils.findRequiredViewAsType(source, R.id.pager, "field 'mViewPager'", ViewPager.class);
        target.mTabLayout = (TabLayout) Utils.findRequiredViewAsType(source, R.id.tab_layout, "field 'mTabLayout'", TabLayout.class);
        target.mFabWithOverlayMenu = (FabWithOverlayMenu) Utils.findRequiredViewAsType(source, R.id.fab, "field 'mFabWithOverlayMenu'", FabWithOverlayMenu.class);
    }

    public void unbind() {
        OrganizationDetailFragment target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mNameTextView = null;
        target.mAddressTextView = null;
        target.mViewPager = null;
        target.mTabLayout = null;
        target.mFabWithOverlayMenu = null;
        this.view2131820923.setOnClickListener(null);
        this.view2131820923 = null;
    }
}
