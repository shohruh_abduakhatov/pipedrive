package com.pipedrive.note;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.model.Activity;
import com.pipedrive.store.StoreActivity;
import com.pipedrive.tasks.AsyncTask;

public class UpdateActivityNoteTask extends AsyncTask<Activity, Void, Boolean> {
    @Nullable
    private OnTaskFinished mOnTaskFinished;

    interface OnTaskFinished {
        void onActivityNoteUpdated(boolean z);
    }

    public UpdateActivityNoteTask(@NonNull Session session, @Nullable OnTaskFinished onTaskFinished) {
        super(session);
        this.mOnTaskFinished = onTaskFinished;
    }

    protected Boolean doInBackground(Activity... params) {
        Activity activity = params[0];
        if (activity.isStored()) {
            return Boolean.valueOf(new StoreActivity(getSession()).update(activity));
        }
        return Boolean.valueOf(true);
    }

    protected void onPostExecute(Boolean aBoolean) {
        if (this.mOnTaskFinished != null) {
            this.mOnTaskFinished.onActivityNoteUpdated(aBoolean.booleanValue());
        }
    }
}
