package com.pipedrive.util.networking.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmailThreadEntity {
    @SerializedName("deal_id")
    @Expose
    private Integer mDealId;
    @SerializedName("id")
    @Expose
    private Integer mId;
    @SerializedName("org_ids")
    @Expose
    private Integer[] mOrgIds;

    public Integer getId() {
        return this.mId;
    }

    public void setId(Integer id) {
        this.mId = id;
    }

    public Integer getDealId() {
        return this.mDealId;
    }

    public void setDealId(Integer dealId) {
        this.mDealId = dealId;
    }

    public Integer[] getOrgIds() {
        return this.mOrgIds;
    }

    public void setOrgIds(Integer[] orgIds) {
        this.mOrgIds = orgIds;
    }
}
