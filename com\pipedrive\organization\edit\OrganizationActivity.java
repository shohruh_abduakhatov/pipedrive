package com.pipedrive.organization.edit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.ButterKnife;
import com.pipedrive.BaseActivity;
import com.pipedrive.R;
import com.pipedrive.dialogs.MessageDialog;
import com.pipedrive.model.Contact;
import com.pipedrive.model.Organization;
import com.pipedrive.model.PersonFieldOption;
import com.pipedrive.model.User;
import com.pipedrive.navigator.Navigable;
import com.pipedrive.navigator.Navigator;
import com.pipedrive.navigator.buttons.DeleteMenuItem;
import com.pipedrive.navigator.buttons.DiscardMenuItem;
import com.pipedrive.navigator.buttons.NavigatorMenuItem;
import com.pipedrive.util.StringUtils;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.util.snackbar.SnackBarManager;
import com.pipedrive.views.ColoredImageView;
import com.pipedrive.views.assignment.OwnerView;
import com.pipedrive.views.assignment.SpinnerWithUserProfilePicture.OnItemSelectedListener;
import com.pipedrive.views.common.EditTextWithUnderlineAndLabel.OnValueChangedListener;
import com.pipedrive.views.common.TitleEditText;
import com.pipedrive.views.common.VisibilitySpinner;
import com.pipedrive.views.common.VisibilitySpinner.OnVisibilityChangeListener;
import com.pipedrive.views.edit.org.OrganizationAddressEditText;
import java.util.Arrays;
import java.util.List;

abstract class OrganizationActivity extends BaseActivity implements OrganizationEditView, Navigable {
    protected static final String KEY_NEW_ORGANIZATION_NAME = "NEW_ORGANIZATION_NAME";
    public static final String KEY_ORGANIZATION_SQL_ID = "ORGANIZATION_SQL_ID";
    @NonNull
    private OrganizationAddressEditText mAddress;
    @NonNull
    private View mAddressContainer;
    @NonNull
    private TitleEditText mName;
    @NonNull
    private OwnerView mOwnerView;
    @NonNull
    private OrganizationEditPresenter mPresenter;
    @NonNull
    private VisibilitySpinner mVisibilitySpinner;

    OrganizationActivity() {
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupLayout();
        this.mPresenter = new OrganizationEditPresenterImpl(getSession());
    }

    public void onResume() {
        super.onResume();
        this.mPresenter.bindView(this);
        Organization organization = this.mPresenter.getOrganization();
        if (organization != null) {
            onRequestOrganization(organization);
        } else if (!requestOrganization(getIntent().getExtras())) {
            finish();
        }
    }

    @Nullable
    protected Navigator getNavigatorImplementation() {
        return new Navigator(this, (Toolbar) findViewById(R.id.toolbar));
    }

    int getLayoutId() {
        return R.layout.activity_organization_edit;
    }

    private void setupLayout() {
        setContentView(getLayoutId());
        setSupportActionBar((Toolbar) ButterKnife.findById((Activity) this, (int) R.id.toolbar));
        this.mName = (TitleEditText) ButterKnife.findById((Activity) this, (int) R.id.name);
        this.mAddress = (OrganizationAddressEditText) ButterKnife.findById((Activity) this, (int) R.id.address);
        this.mAddressContainer = ButterKnife.findById((Activity) this, (int) R.id.addressContainer);
        this.mOwnerView = (OwnerView) ButterKnife.findById((Activity) this, (int) R.id.ownerView);
        this.mVisibilitySpinner = (VisibilitySpinner) ButterKnife.findById((Activity) this, (int) R.id.visibleTo);
        ((ColoredImageView) ButterKnife.findById((Activity) this, (int) R.id.image)).setImageResource(R.drawable.deal_org);
    }

    private boolean requestOrganization(@Nullable Bundle intentExtras) {
        boolean newOrganizationRequested;
        if (intentExtras == null) {
            newOrganizationRequested = true;
        } else {
            newOrganizationRequested = false;
        }
        if (newOrganizationRequested) {
            this.mPresenter.requestNewOrganization();
            return true;
        } else if (intentExtras.containsKey(KEY_ORGANIZATION_SQL_ID)) {
            this.mPresenter.requestOrganization(Long.valueOf(intentExtras.getLong(KEY_ORGANIZATION_SQL_ID)));
            return true;
        } else if (!intentExtras.containsKey(KEY_NEW_ORGANIZATION_NAME)) {
            return false;
        } else {
            this.mPresenter.requestNewOrganizationWithName(intentExtras.getString(KEY_NEW_ORGANIZATION_NAME));
            return true;
        }
    }

    public void onRequestOrganization(@Nullable Organization organization) {
        if (organization == null) {
            finish();
            return;
        }
        setupActionBar(organization);
        setupNameView(organization);
        setupAddressView(organization);
        setupOwnerView(organization);
        setupVisibleToView(organization);
    }

    private void setupActionBar(@NonNull Contact contact) {
        setTitle(!contact.isStored() ? R.string.add_organization : R.string.edit_organization);
    }

    private void setupNameView(@NonNull final Organization organization) {
        this.mName.setup(organization, new OnValueChangedListener() {
            public void onValueChanged(@NonNull String value) {
                organization.setName(value);
            }
        });
    }

    private void setupAddressView(@NonNull final Organization organization) {
        boolean enableAddressChange;
        int i = 0;
        this.mAddress.setup(organization, new OnValueChangedListener() {
            public void onValueChanged(@NonNull String value) {
                organization.setAddress(value);
            }
        });
        Boolean enableOrganizationAddressChange = this.mPresenter.enableOrganizationAddressChange();
        if (enableOrganizationAddressChange == null || !enableOrganizationAddressChange.booleanValue()) {
            enableAddressChange = false;
        } else {
            enableAddressChange = true;
        }
        View view = this.mAddressContainer;
        if (!enableAddressChange) {
            i = 8;
        }
        view.setVisibility(i);
    }

    private void setupOwnerView(@NonNull final Organization organization) {
        this.mOwnerView.loadOwnership(getSession(), organization, new OnItemSelectedListener() {
            public void onItemSelected(@NonNull User user) {
                organization.setOwnerPipedriveId(user.getPipedriveId());
                organization.setOwnerName(user.getName());
            }
        });
    }

    private void setupVisibleToView(@NonNull final Organization organization) {
        this.mVisibilitySpinner.setup(getSession(), organization, new OnVisibilityChangeListener() {
            public void onVisibilityChanged(@NonNull PersonFieldOption personFieldOption) {
                organization.setVisibleTo(personFieldOption.getId());
            }
        });
    }

    public void onOrganizationCreated(boolean success) {
        if (success) {
            if (this.mPresenter.getOrganization() != null) {
                setResult(-1, new Intent().putExtra(KEY_ORGANIZATION_SQL_ID, this.mPresenter.getOrganization().getSqlId()));
            }
            SnackBarManager.INSTANCE.setDelayedSnackBar(R.string.organization_added);
        }
        processOrganizationCrudResponse(success);
    }

    public void onOrganizationUpdated(boolean success) {
        processOrganizationCrudResponse(success);
        if (success) {
            SnackBarManager.INSTANCE.setDelayedSnackBar(R.string.organization_updated);
        }
    }

    public void onOrganizationDeleted(boolean success) {
        if (success) {
            setResult(2);
            SnackBarManager.INSTANCE.setDelayedSnackBar(R.string.organization_deleted);
        }
        processOrganizationCrudResponse(success);
    }

    private void processOrganizationCrudResponse(boolean success) {
        if (success) {
            finish();
        } else {
            ViewUtil.showErrorToast(this, R.string.error_organization_save_failed);
        }
    }

    protected void onPause() {
        this.mPresenter.unbindView();
        super.onPause();
    }

    public boolean isChangesMadeToFieldsAfterInitialLoad() {
        return this.mPresenter.isModified();
    }

    public boolean isAllRequiredFieldsSet() {
        Organization currentlyManagedOrganization = this.mPresenter.getOrganization();
        return (currentlyManagedOrganization == null || StringUtils.isTrimmedAndEmpty(currentlyManagedOrganization.getName())) ? false : true;
    }

    public void onCreateOrUpdate() {
        this.mPresenter.createOrUpdateOrganization();
    }

    public void createOrUpdateDiscardedAsRequiredFieldsAreNotSet() {
        MessageDialog.showMessage(getFragmentManager(), R.string.please_add_a_name_to_your_contact_organization);
    }

    private void onDelete() {
        ViewUtil.showDeleteConfirmationDialog(this, getResources().getString(R.string.dialog_delete_org), new Runnable() {
            public void run() {
                OrganizationActivity.this.mPresenter.deleteOrganization();
            }
        });
    }

    public void onCancel() {
        finish();
    }

    @Nullable
    public List<? extends NavigatorMenuItem> getAdditionalButtons() {
        return Arrays.asList(new NavigatorMenuItem[]{new DeleteMenuItem() {
            public boolean isEnabled() {
                return OrganizationActivity.this.mPresenter.getOrganization() != null && OrganizationActivity.this.mPresenter.getOrganization().isStored() && OrganizationActivity.this.getSession().canUserDeleteOrg();
            }

            public void onClick() {
                OrganizationActivity.this.onDelete();
            }
        }, new DiscardMenuItem() {
            public void onClick() {
                OrganizationActivity.this.onCancel();
            }
        }});
    }
}
