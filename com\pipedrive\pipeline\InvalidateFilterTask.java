package com.pipedrive.pipeline;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.pipedrive.adapter.filter.FilterContentPipelineAdapter.FilterRow;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.FiltersDataSource;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Filter;
import com.pipedrive.tasks.StoreAwareAsyncTask;
import com.pipedrive.tasks.users.DownloadUsersTask;
import com.pipedrive.util.FiltersUtil;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.Response;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

class InvalidateFilterTask extends StoreAwareAsyncTask<Void, Void, Boolean> {
    @Nullable
    private final OnTaskFinished mOnTaskFinished;
    private boolean mSelectedFilterDoesNotExist = false;
    private boolean mSelectedUserDoesNotExist = false;

    public interface OnTaskFinished {
        @MainThread
        void allFiltersDownloaded(@NonNull Session session);

        @MainThread
        void selectedFilterDoesNotExist(@NonNull Session session);

        @MainThread
        void selectedUserDoesNotExist(@NonNull Session session);

        @MainThread
        void taskNotExecutedDueToOfflineMode(@NonNull Session session);
    }

    InvalidateFilterTask(@NonNull Session session, @Nullable OnTaskFinished onTaskFinished) {
        super(session);
        this.mOnTaskFinished = onTaskFinished;
    }

    protected Boolean doInBackgroundAfterStoreSync(Void... params) {
        if (!ConnectionUtil.isConnected(getSession().getApplicationContext())) {
            return Boolean.valueOf(true);
        }
        downloadFilters(getSession(), "deals");
        downloadFilters(getSession(), Filter.TYPE_PEOPLE);
        downloadFilters(getSession(), Filter.TYPE_ORGANIZATIONS);
        downloadFilters(getSession(), "products");
        DownloadUsersTask.downloadUsers(getSession());
        long userId = getSession().getPipelineUser(Long.MIN_VALUE);
        long filterId = getSession().getPipelineFilter(Long.MIN_VALUE);
        if (userId > 0) {
            for (FilterRow filterRow : FiltersUtil.getPipelineCompanyUsersWithHeader(getSession())) {
                if (filterRow.getUser() != null && filterRow.getUser().getPipedriveIdOrNull() != null && filterRow.getUser().getPipedriveIdOrNull().longValue() == userId) {
                    return Boolean.valueOf(false);
                }
            }
            this.mSelectedUserDoesNotExist = true;
        } else if (filterId > 0) {
            for (FilterRow filterRow2 : getPipelineFiltersWithHeader(getSession())) {
                if (filterRow2.getFilter() != null && filterRow2.getFilter().getPipedriveIdOrNull() != null && filterRow2.getFilter().getPipedriveIdOrNull().longValue() == filterId) {
                    return Boolean.valueOf(false);
                }
            }
            this.mSelectedFilterDoesNotExist = true;
        }
        return Boolean.valueOf(false);
    }

    protected void onPostExecute(@NonNull Boolean taskNotExecutedDueToOfflineMode) {
        if (!(this.mOnTaskFinished != null)) {
            return;
        }
        if (taskNotExecutedDueToOfflineMode.booleanValue()) {
            this.mOnTaskFinished.taskNotExecutedDueToOfflineMode(getSession());
        } else if (this.mSelectedFilterDoesNotExist) {
            this.mOnTaskFinished.selectedFilterDoesNotExist(getSession());
        } else if (this.mSelectedUserDoesNotExist) {
            this.mOnTaskFinished.selectedUserDoesNotExist(getSession());
        } else {
            this.mOnTaskFinished.allFiltersDownloaded(getSession());
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void downloadFilters(@NonNull Session session, @NonNull String type) {
        List<Filter> filters = new ArrayList();
        InputStream in = getFiltersListStream(session, type);
        if (in != null) {
            JsonReader reader = new JsonReader(new InputStreamReader(in, HttpRequest.CHARSET_UTF8));
            reader.beginObject();
            while (reader.hasNext()) {
                String contactsListField = reader.nextName();
                if (!Response.JSON_PARAM_SUCCESS.equals(contactsListField)) {
                    try {
                        if (Response.JSON_PARAM_DATA.equals(contactsListField) && reader.peek() == JsonToken.BEGIN_ARRAY) {
                            reader.beginArray();
                            while (reader.hasNext()) {
                                filters.add(FiltersUtil.readFilter(reader));
                            }
                            reader.endArray();
                        } else {
                            reader.skipValue();
                        }
                    } catch (Throwable e) {
                        LogJourno.reportEvent(EVENT.Networking_downloadFiltersFailed, e);
                        Log.e(e);
                    } catch (Throwable th) {
                        try {
                            in.close();
                        } catch (IOException e2) {
                        }
                    }
                } else if (!reader.nextBoolean()) {
                    reader.close();
                }
            }
            reader.endObject();
            reader.close();
            try {
                in.close();
            } catch (IOException e3) {
            }
            FiltersDataSource filtersDataSource = new FiltersDataSource(session);
            filtersDataSource.beginTransaction();
            filtersDataSource.deleteAllByType(type);
            for (Filter filter : filters) {
                filtersDataSource.createOrUpdate((BaseDatasourceEntity) filter);
            }
            filtersDataSource.commit();
        }
    }

    @Nullable
    private InputStream getFiltersListStream(@NonNull Session session, @NonNull String type) {
        ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder(session, "filters");
        apiUrlBuilder.param("type", type);
        return ConnectionUtil.requestGet(apiUrlBuilder);
    }

    @NonNull
    private List<FilterRow> getPipelineFiltersWithHeader(@NonNull Session session) {
        return FiltersUtil.getPipelineFiltersWithHeader(session, null);
    }
}
