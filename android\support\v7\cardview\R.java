package android.support.v7.cardview;

public final class R {

    public static final class attr {
        public static final int cardBackgroundColor = 2130772151;
        public static final int cardCornerRadius = 2130772152;
        public static final int cardElevation = 2130772153;
        public static final int cardMaxElevation = 2130772154;
        public static final int cardPreventCornerOverlap = 2130772156;
        public static final int cardUseCompatPadding = 2130772155;
        public static final int contentPadding = 2130772157;
        public static final int contentPaddingBottom = 2130772161;
        public static final int contentPaddingLeft = 2130772158;
        public static final int contentPaddingRight = 2130772159;
        public static final int contentPaddingTop = 2130772160;
    }

    public static final class color {
        public static final int cardview_dark_background = 2131755060;
        public static final int cardview_light_background = 2131755061;
        public static final int cardview_shadow_end_color = 2131755062;
        public static final int cardview_shadow_start_color = 2131755063;
    }

    public static final class dimen {
        public static final int cardview_compat_inset_shadow = 2131493010;
        public static final int cardview_default_elevation = 2131493011;
        public static final int cardview_default_radius = 2131493012;
    }

    public static final class style {
        public static final int Base_CardView = 2131558582;
        public static final int CardView = 2131558567;
        public static final int CardView_Dark = 2131558630;
        public static final int CardView_Light = 2131558631;
    }

    public static final class styleable {
        public static final int[] CardView = new int[]{16843071, 16843072, com.pipedrive.R.attr.cardBackgroundColor, com.pipedrive.R.attr.cardCornerRadius, com.pipedrive.R.attr.cardElevation, com.pipedrive.R.attr.cardMaxElevation, com.pipedrive.R.attr.cardUseCompatPadding, com.pipedrive.R.attr.cardPreventCornerOverlap, com.pipedrive.R.attr.contentPadding, com.pipedrive.R.attr.contentPaddingLeft, com.pipedrive.R.attr.contentPaddingRight, com.pipedrive.R.attr.contentPaddingTop, com.pipedrive.R.attr.contentPaddingBottom};
        public static final int CardView_android_minHeight = 1;
        public static final int CardView_android_minWidth = 0;
        public static final int CardView_cardBackgroundColor = 2;
        public static final int CardView_cardCornerRadius = 3;
        public static final int CardView_cardElevation = 4;
        public static final int CardView_cardMaxElevation = 5;
        public static final int CardView_cardPreventCornerOverlap = 7;
        public static final int CardView_cardUseCompatPadding = 6;
        public static final int CardView_contentPadding = 8;
        public static final int CardView_contentPaddingBottom = 12;
        public static final int CardView_contentPaddingLeft = 9;
        public static final int CardView_contentPaddingRight = 10;
        public static final int CardView_contentPaddingTop = 11;
    }
}
