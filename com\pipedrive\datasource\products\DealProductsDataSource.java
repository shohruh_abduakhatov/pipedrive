package com.pipedrive.datasource.products;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.datasource.BaseChildDataSource;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.datasource.SelectionHolder;
import com.pipedrive.model.ChildDataSourceEntity;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.model.products.Price;
import com.pipedrive.model.products.Product;
import com.pipedrive.model.products.ProductVariation;
import com.pipedrive.util.CursorHelper;
import java.util.List;

public class DealProductsDataSource extends BaseChildDataSource<DealProduct> {
    private static final String[] ALL_COLUMNS = new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_PIPEDRIVE_ID, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_DEAL_SQL_ID, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_PRODUCT_SQL_ID, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_PRODUCT_VARIATION_SQL_ID, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_PRICE, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_CURRENCY_SQL_ID, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_QUANTITY, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_DURATION, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_DISCOUNT_PERCENTAGE, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_IS_ACTIVE, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_ORDER};

    public DealProductsDataSource(SQLiteDatabase dbconn) {
        super(dbconn);
    }

    @NonNull
    protected String getColumnNameForParentSqlId() {
        return PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_DEAL_SQL_ID;
    }

    @NonNull
    protected ContentValues getContentValues(@NonNull DealProduct dealProduct) {
        ContentValues contentValues = super.getContentValues((ChildDataSourceEntity) dealProduct);
        CursorHelper.put(dealProduct.getProduct().getSqlIdOrNull(), contentValues, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_PRODUCT_SQL_ID);
        CursorHelper.put(dealProduct.getPrice().getValue(), contentValues, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_PRICE);
        CursorHelper.put(dealProduct.getPrice().getCurrency().getSqlIdOrNull(), contentValues, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_CURRENCY_SQL_ID);
        CursorHelper.put(dealProduct.getQuantity(), contentValues, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_QUANTITY);
        CursorHelper.put(dealProduct.getDuration(), contentValues, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_DURATION);
        CursorHelper.put(dealProduct.getDiscountPercentage(), contentValues, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_DISCOUNT_PERCENTAGE);
        CursorHelper.put(dealProduct.isActive(), contentValues, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_IS_ACTIVE);
        CursorHelper.put(dealProduct.getOrder(), contentValues, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_ORDER);
        CursorHelper.put(dealProduct.getProductVariation() == null ? null : dealProduct.getProductVariation().getSqlIdOrNull(), contentValues, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_PRODUCT_VARIATION_SQL_ID);
        return contentValues;
    }

    @Nullable
    protected DealProduct deflateCursor(@NonNull Cursor cursor, @Nullable Long sqlId, @Nullable Long pipedriveId, @Nullable Long parentSqlId) {
        Price price = CursorHelper.getPrice(cursor, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_PRICE, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_CURRENCY_SQL_ID, getTransactionalDBConnection());
        Double quantity = CursorHelper.getDouble(cursor, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_QUANTITY);
        Double duration = CursorHelper.getDouble(cursor, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_DURATION);
        Double discountPercentage = CursorHelper.getDouble(cursor, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_DISCOUNT_PERCENTAGE);
        Boolean isEnabled = CursorHelper.getBoolean(cursor, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_IS_ACTIVE);
        Integer order = CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_ORDER);
        Product product = deflateProduct(cursor);
        if (price == null || quantity == null || discountPercentage == null || isEnabled == null || product == null) {
            return null;
        }
        return DealProduct.create(sqlId, pipedriveId, parentSqlId, product, deflateProductVariation(cursor), price, quantity, duration, discountPercentage, isEnabled, order);
    }

    @Nullable
    private Product deflateProduct(@NonNull Cursor cursor) {
        Long productSqlId = CursorHelper.getLong(cursor, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_PRODUCT_SQL_ID);
        if (productSqlId == null) {
            return null;
        }
        return (Product) new ProductsDataSource(getTransactionalDBConnection()).findBySqlId(productSqlId.longValue());
    }

    @Nullable
    private ProductVariation deflateProductVariation(@NonNull Cursor cursor) {
        Long productVariationSqlId = CursorHelper.getLong(cursor, PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_PRODUCT_VARIATION_SQL_ID);
        if (productVariationSqlId == null) {
            return null;
        }
        return (ProductVariation) new ProductVariationDataSource(getTransactionalDBConnection()).findBySqlId(productVariationSqlId.longValue());
    }

    @NonNull
    protected String getColumnNameForPipedriveId() {
        return PipeSQLiteHelper.COLUMN_DEAL_PRODUCTS_PIPEDRIVE_ID;
    }

    @NonNull
    protected String getColumnNameForSqlId() {
        return PipeSQLiteHelper.COLUMN_ID;
    }

    @NonNull
    protected String getTableName() {
        return PipeSQLiteHelper.TABLE_DEAL_PRODUCTS;
    }

    @NonNull
    protected String[] getAllColumns() {
        return ALL_COLUMNS;
    }

    @NonNull
    public List<DealProduct> findAllActiveDealProductsRelatedToDealSqlId(@NonNull Long dealSqlId) {
        return deflateCursorToList(query(getTableName(), getAllColumns(), getSelectionHolderForActiveDealProducts(dealSqlId), null));
    }

    @NonNull
    private SelectionHolder getSelectionHolderForActiveDealProducts(@NonNull Long dealSqlId) {
        String selection = "deal_products_is_active =? AND deal_products_deal_sql_id =? ";
        return new SelectionHolder("deal_products_is_active =? AND deal_products_deal_sql_id =? ", new String[]{String.valueOf(1), String.valueOf(dealSqlId)});
    }

    public int getDealProductCount(@NonNull Long dealSqlId) {
        String[] columns = new String[]{"COUNT(*)"};
        SelectionHolder selectionHolder = getSelectionHolderForActiveDealProducts(dealSqlId);
        String tableName = getTableName();
        String selection = selectionHolder.getSelection();
        String[] selectionArgs = selectionHolder.getSelectionArgs();
        Cursor cursor = !(this instanceof SQLiteDatabase) ? query(tableName, columns, selection, selectionArgs, null, null, null) : SQLiteInstrumentation.query((SQLiteDatabase) this, tableName, columns, selection, selectionArgs, null, null, null);
        int i = 0;
        try {
            cursor.moveToFirst();
            i = cursor.getInt(0);
            return i;
        } finally {
            cursor.close();
        }
    }
}
