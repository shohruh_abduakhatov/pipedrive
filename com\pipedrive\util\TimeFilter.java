package com.pipedrive.util;

import android.content.Context;
import com.pipedrive.R;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.datetime.PipedriveDate;
import com.pipedrive.model.Activity.ActivityBuilderForDataSource.ActivityStartDateTimeType;
import com.pipedrive.util.time.TimeManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public enum TimeFilter {
    ;
    
    public static final int TIME_ALL = -1;
    public static final int TIME_FUTURE = 1000;
    public static final int[] TIME_IDS = null;
    public static final int TIME_NEXT_WEEK = 5;
    public static final int TIME_OVERDUE = 1;
    public static final int TIME_THIS_WEEK = 4;
    public static final int TIME_TODAY = 2;
    public static final int TIME_TOMORROW = 3;

    static {
        TIME_IDS = new int[]{-1, 1, 2, 3, 4, 5};
    }

    public static String getLabelById(Context context, int filterTimeId) {
        String label = "";
        int i = 0;
        while (i < TIME_IDS.length) {
            if (TIME_IDS[i] == filterTimeId && context.getResources().getStringArray(R.array.time_filter_titles).length > i) {
                return context.getResources().getStringArray(R.array.time_filter_titles)[i];
            }
            i++;
        }
        return label;
    }

    public static String getSelectionForActivityListFilter(int timeFilter) {
        switch (timeFilter) {
            case 1:
                return " case when ac_start_type = " + ActivityStartDateTimeType.Date.getSqlValue() + " then date(" + PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME + ", 'unixepoch') " + " else datetime(" + PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME + ", 'unixepoch', 'localtime') " + " end" + " < " + " case when " + PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME_TYPE + " = " + ActivityStartDateTimeType.Date.getSqlValue() + " then date('now') " + " else datetime('now', 'localtime') " + " end";
            case 2:
                return " case when ac_start_type = " + ActivityStartDateTimeType.Date.getSqlValue() + " then date(" + PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME + ", 'unixepoch') " + " else date(" + PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME + ", 'unixepoch', 'localtime') " + " end " + " = date('now', 'localtime')";
            case 3:
                return " case when ac_start_type = " + ActivityStartDateTimeType.Date.getSqlValue() + " then date(" + PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME + ", 'unixepoch') " + " else date(" + PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME + ", 'unixepoch', 'localtime') " + " end " + " = date('now', '+1 day', 'localtime')";
            case 4:
            case 5:
                return " case when ac_start_type = " + ActivityStartDateTimeType.Date.getSqlValue() + " then date(" + PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME + ", 'unixepoch') " + " else date(" + PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME + ", 'unixepoch', 'localtime') " + " end " + " between date(?, 'unixepoch') and date(?, 'unixepoch')";
            case 1000:
                return " case when ac_start_type = " + ActivityStartDateTimeType.Date.getSqlValue() + " then date(" + PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME + ", 'unixepoch') " + " else datetime(" + PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME + ", 'unixepoch', 'localtime') " + " end" + " >= " + " case when " + PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME_TYPE + " = " + ActivityStartDateTimeType.Date.getSqlValue() + " then date('now') " + " else datetime('now', 'localtime') " + " end";
            default:
                return "";
        }
    }

    public static List<String> getSelectionArgsForActivityListFilter(int timeFilter) {
        Date now = new Date(TimeManager.getInstance().currentTimeMillis().longValue());
        Calendar cal = TimeManager.getInstance().getLocalCalendar();
        cal.setFirstDayOfWeek(2);
        cal.setTime(now);
        cal.set(11, 0);
        cal.clear(12);
        cal.clear(13);
        cal.clear(14);
        switch (timeFilter) {
            case 1:
            case 2:
            case 3:
            case 1000:
                return Collections.emptyList();
            case 4:
                List<String> argsThisWeek = new ArrayList();
                cal.set(7, 2);
                if (now.getTime() - cal.getTime().getTime() > TimeUnit.DAYS.toMillis(7)) {
                    cal.add(3, 1);
                }
                argsThisWeek.add(String.valueOf(PipedriveDate.instanceFrom(cal).getRepresentationInUnixTime()));
                cal.add(5, 6);
                argsThisWeek.add(String.valueOf(PipedriveDate.instanceFrom(cal).getRepresentationInUnixTime()));
                return argsThisWeek;
            case 5:
                cal.add(3, 1);
                ArrayList<String> argsNextWeek = new ArrayList();
                cal.set(7, 2);
                argsNextWeek.add(String.valueOf(PipedriveDate.instanceFrom(cal).getRepresentationInUnixTime()));
                cal.add(5, 6);
                argsNextWeek.add(String.valueOf(PipedriveDate.instanceFrom(cal).getRepresentationInUnixTime()));
                return argsNextWeek;
            default:
                return Collections.emptyList();
        }
    }
}
