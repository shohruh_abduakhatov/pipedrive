package com.zendesk.sdk.rating.impl;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import com.zendesk.sdk.R;
import com.zendesk.sdk.feedback.FeedbackConnector;
import com.zendesk.sdk.network.SubmissionListener;
import com.zendesk.sdk.rating.ui.FeedbackDialog;
import com.zendesk.sdk.rating.ui.RateMyAppDialog;

public class RateMyAppSendFeedbackButton extends BaseRateMyAppButton {
    public static final String FEEDBACK_DIALOG_TAG = "feedback";
    private final transient FeedbackConnector mConnector;
    private transient SubmissionListener mFeedbackListener;
    private final transient FragmentActivity mFragmentActivity;
    private final String mLabel;

    public RateMyAppSendFeedbackButton(FragmentActivity fragmentActivity, FeedbackConnector connector) {
        this.mLabel = fragmentActivity.getString(R.string.rate_my_app_dialog_negative_action_label);
        this.mFragmentActivity = fragmentActivity;
        this.mConnector = connector;
    }

    public OnClickListener getOnClickListener() {
        return new OnClickListener() {
            public void onClick(View view) {
                FragmentManager fragmentManager = RateMyAppSendFeedbackButton.this.mFragmentActivity.getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment rmaDialog = fragmentManager.findFragmentByTag(RateMyAppDialog.RMA_DIALOG_TAG);
                if (rmaDialog != null) {
                    fragmentTransaction.remove(rmaDialog);
                }
                fragmentTransaction.commit();
                fragmentManager.popBackStackImmediate();
                fragmentTransaction = fragmentManager.beginTransaction();
                Fragment feedbackDialogFragment = fragmentManager.findFragmentByTag(RateMyAppSendFeedbackButton.FEEDBACK_DIALOG_TAG);
                if (feedbackDialogFragment != null) {
                    fragmentTransaction.remove(feedbackDialogFragment);
                }
                fragmentTransaction.addToBackStack(null);
                FeedbackDialog feedbackDialog = FeedbackDialog.newInstance(RateMyAppSendFeedbackButton.this.mConnector);
                feedbackDialog.setFeedbackListener(RateMyAppSendFeedbackButton.this.mFeedbackListener);
                feedbackDialog.show(fragmentTransaction, RateMyAppSendFeedbackButton.FEEDBACK_DIALOG_TAG);
            }
        };
    }

    public String getLabel() {
        return this.mLabel;
    }

    public boolean shouldDismissDialog() {
        return false;
    }

    public void setFeedbackListener(SubmissionListener feedbackListener) {
        this.mFeedbackListener = feedbackListener;
    }

    public int getId() {
        return R.id.rma_feedback_button;
    }
}
