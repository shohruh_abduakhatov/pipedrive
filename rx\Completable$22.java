package rx;

import java.util.concurrent.CountDownLatch;

class Completable$22 implements CompletableSubscriber {
    final /* synthetic */ Completable this$0;
    final /* synthetic */ CountDownLatch val$cdl;
    final /* synthetic */ Throwable[] val$err;

    Completable$22(Completable completable, CountDownLatch countDownLatch, Throwable[] thArr) {
        this.this$0 = completable;
        this.val$cdl = countDownLatch;
        this.val$err = thArr;
    }

    public void onCompleted() {
        this.val$cdl.countDown();
    }

    public void onError(Throwable e) {
        this.val$err[0] = e;
        this.val$cdl.countDown();
    }

    public void onSubscribe(Subscription d) {
    }
}
