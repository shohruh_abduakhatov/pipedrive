package com.pipedrive.more;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog.Builder;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.pipedrive.BuildConfig;
import com.pipedrive.LoginActivity;
import com.pipedrive.NavigationActivity;
import com.pipedrive.R;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.AnalyticsEvent;
import com.pipedrive.analytics.ScreensMapper;
import com.pipedrive.datasource.CompaniesDataSource;
import com.pipedrive.datasource.UsersDataSource;
import com.pipedrive.datetime.DateFormatHelper;
import com.pipedrive.dialogs.ChangeCompanyDialogFragment;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.User;
import com.pipedrive.model.settings.Company;
import com.pipedrive.nearby.NearbyActivity;
import com.pipedrive.notification.UINotificationManager;
import com.pipedrive.settings.SynchronizeDataActivity;
import com.pipedrive.sync.SyncManager;
import com.pipedrive.sync.SyncManager.StateOfDownloadAll;
import com.pipedrive.util.devices.MobileDevices;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.snackbar.SnackBarManager;
import com.pipedrive.views.profilepicture.ProfilePictureRoundUserGrey;
import java.util.Date;
import java.util.List;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Actions;

public class MoreActivity extends NavigationActivity {
    private static final String TAG = MoreActivity.class.getSimpleName();
    @BindView(2131820790)
    MoreButton aboutButton;
    @BindView(2131820721)
    View arrow;
    @BindView(2131820784)
    View companySwitchContainer;
    @BindView(2131820782)
    ProfilePictureRoundUserGrey profilePicture;
    @BindView(2131820785)
    TextView selectedCompanyName;
    @Nullable
    private Subscription subscription;
    @BindView(2131820787)
    MoreButton synchronizeNowButton;
    @BindView(2131820783)
    TextView userName;

    public static void startActivity(@NonNull Activity activity) {
        ContextCompat.startActivity(activity, new Intent(activity, MoreActivity.class), ActivityOptionsCompat.makeBasic().toBundle());
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_more);
        ButterKnife.bind((Activity) this);
    }

    public void onResume() {
        super.onResume();
        updateLastSyncTime();
        updateAboutButton();
        setupHeaderView();
        this.subscription = SyncManager.getInstance().getStateOfDownloadAll(getSession()).subscribeOn(AndroidSchedulers.mainThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<StateOfDownloadAll>() {
            public void call(StateOfDownloadAll stateOfDownloadAll) {
                boolean enableManualRefresh = stateOfDownloadAll == StateOfDownloadAll.IDLE;
                MoreActivity.this.synchronizeNowButton.setEnabled(enableManualRefresh);
                MoreActivity.this.updateSynchronizeButtonTitle(enableManualRefresh);
            }
        });
    }

    private void setupHeaderView() {
        String userProfileName = getSession().getUserSettingsName(null);
        if (userProfileName != null) {
            this.userName.setText(userProfileName);
        }
        this.companySwitchContainer.setClickable(false);
        this.arrow.setVisibility(8);
        this.profilePicture.loadPicture((User) new UsersDataSource(getSession().getDatabase()).findByPipedriveId(getSession().getAuthenticatedUserID()));
        setupCompanySwitch();
    }

    private void updateAboutButton() {
        try {
            CharSequence applicationLabel = getPackageManager().getApplicationLabel(getPackageManager().getApplicationInfo(getPackageName(), 0));
            this.aboutButton.setTitle(getString(R.string.pref_about_title, new Object[]{applicationLabel}));
        } catch (NameNotFoundException e) {
            Log.e(e);
            LogJourno.reportEvent(EVENT.NotSpecified_applicationLabelCannotBeRetrieved);
        }
        this.aboutButton.setSubtitle(getString(R.string.pref_about_summary, new Object[]{BuildConfig.APP_VERSION_NAME}));
    }

    protected void onPause() {
        super.onPause();
        if (this.subscription != null) {
            this.subscription.unsubscribe();
        }
    }

    private void updateLastSyncTime() {
        String lastRefreshTimeAsString = "";
        long lastRefreshTimeInMillis = getSession().getLastGetRequestTimeInMillis().longValue();
        if (lastRefreshTimeInMillis != Long.MIN_VALUE) {
            lastRefreshTimeAsString = DateFormatHelper.getTimePassedInHumanReadableForm(getSession().getApplicationContext(), R.string.pref_manual_refresh_subtitle, new Date(lastRefreshTimeInMillis));
        }
        this.synchronizeNowButton.setSubtitle(lastRefreshTimeAsString);
    }

    private void updateSynchronizeButtonTitle(boolean enabled) {
        String defaultTitleText = getResources().getString(R.string.pref_manual_refresh_title);
        String disabledTitleText = getResources().getString(R.string.syncing_data_in_progress);
        MoreButton moreButton = this.synchronizeNowButton;
        if (!enabled) {
            defaultTitleText = disabledTitleText;
        }
        moreButton.setTitle(defaultTitleText);
    }

    @OnClick({2131820786})
    void onNearbyClicked() {
        if (ConnectionUtil.isConnected(this)) {
            NearbyActivity.startActivity(this);
        } else {
            SnackBarManager.INSTANCE.showSnackBar(R.string.cant_access_nearby_when_offline);
        }
    }

    @OnClick({2131820787})
    void onSynchronizeNowClicked() {
        this.synchronizeNowButton.setEnabled(false);
        updateSynchronizeButtonTitle(false);
        SynchronizeDataActivity.startActivity(this);
        Analytics.sendEvent(this, AnalyticsEvent.SYNC_DATA_PRESSED);
    }

    @OnClick({2131820788})
    void onPreferencesClicked() {
        PreferencesActivity.startActivity(this);
    }

    @OnClick({2131820789})
    void onHelpAndFeedbackClicked() {
        HelpAndFeedbackActivity.startActivity(this);
        Analytics.hitScreen(ScreensMapper.SCREEN_NAME_HELP, this);
    }

    @OnClick({2131820790})
    void onAboutClicked() {
        AboutPipedriveActivity.startActivity(this);
        Analytics.hitScreen(ScreensMapper.SCREEN_NAME_ABOUT, this);
    }

    @OnClick({2131820791})
    void onSignOutClicked() {
        CharSequence positiveButtonText = getResources().getString(R.string.pref_logout_dialog_answer_yes);
        new Builder(this, R.style.Theme.Pipedrive.Dialog.Alert).setTitle(getResources().getString(R.string.pref_logout_dialog_message)).setPositiveButton(positiveButtonText, new OnClickListener() {
            public void onClick(@NonNull DialogInterface dialog, int which) {
                new MobileDevices(MoreActivity.this.getSession()).logoutDevice().subscribe(Actions.empty(), Actions.empty());
                Analytics.sendEvent(MoreActivity.this, AnalyticsEvent.LOG_OUT_PRESSED);
                UINotificationManager.getInstance().cancelAllActivityAlarmsAndNotifications(MoreActivity.this.getSession());
                LoginActivity.logOut(MoreActivity.this, true);
                MoreActivity.this.finish();
                dialog.cancel();
            }
        }).setNegativeButton(getResources().getString(R.string.pref_logout_dialog_answer_no), null).show();
    }

    private void setupCompanySwitch() {
        List<Company> availableCompanies = new CompaniesDataSource(getSession()).findAll();
        if (availableCompanies.size() > 1) {
            this.companySwitchContainer.setClickable(true);
            displayDropDownArrowIfOnline();
        }
        for (Company company : availableCompanies) {
            long loginDataCompanyId = getSession().getSelectedCompanyID();
            Long companyPipedriveId = company.getPipedriveIdOrNull();
            if (companyPipedriveId != null) {
                boolean currentlyLoggedInCompanyFound;
                if (companyPipedriveId.longValue() == loginDataCompanyId) {
                    currentlyLoggedInCompanyFound = true;
                } else {
                    currentlyLoggedInCompanyFound = false;
                }
                if (currentlyLoggedInCompanyFound) {
                    Log.d(TAG, String.format("Currently logged in company ID: %s", new Object[]{Long.valueOf(loginDataCompanyId)}));
                    String summaryText = company.getName();
                    Log.d(TAG, String.format("Summary text for company settings: %s", new Object[]{summaryText}));
                    this.selectedCompanyName.setText(summaryText);
                }
            }
        }
    }

    private void displayDropDownArrowIfOnline() {
        if (!ConnectionUtil.isNotConnected(this)) {
            this.arrow.setVisibility(0);
        }
    }

    @OnClick({2131820784})
    void companySwitchClicked() {
        if (ConnectionUtil.isNotConnected(this)) {
            SnackBarManager.INSTANCE.showSnackBar(R.string.cant_switch_company_when_offline);
        } else {
            ChangeCompanyDialogFragment.show(getSupportFragmentManager());
        }
    }

    @NonNull
    protected View getSnackBarContainer() {
        return findViewById(R.id.snackBarContainer);
    }
}
