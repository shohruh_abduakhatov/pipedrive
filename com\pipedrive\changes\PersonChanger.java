package com.pipedrive.changes;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.changes.Change.ChangeOperationType;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Person;

public class PersonChanger extends Changer<Person> {
    public PersonChanger(Session session) {
        super(session, 20, null);
    }

    protected PersonChanger(Changer parentChanger) {
        super(parentChanger.getSession(), 20, parentChanger);
    }

    @Nullable
    protected Long getChangeMetaData(@NonNull Person person, @NonNull ChangeOperationType changeOperationType) {
        return Long.valueOf(person.getSqlId());
    }

    @NonNull
    protected ChangeResult createChange(@NonNull Person newPerson) {
        if (newPerson.isExisting() || newPerson.isStored()) {
            LogJourno.reportEvent(EVENT.ChangesChanger_createChangeRequestedWithExistingChange, PersonChanger.class.getSimpleName());
            Log.e(new Throwable("I can only handle new Persons!"));
            return ChangeResult.FAILED;
        }
        createAndRelateNewOrgChangeIfNewOrgExists(newPerson);
        long newContactSqlId = new PersonsDataSource(getSession().getDatabase()).createOrUpdate((BaseDatasourceEntity) newPerson);
        if (newContactSqlId == -1) {
            return ChangeResult.FAILED;
        }
        newPerson.setSqlId(newContactSqlId);
        return ChangeResult.SUCCESSFUL;
    }

    @NonNull
    protected ChangeResult updateChange(@NonNull Person existingPerson) {
        PersonsDataSource personsDataSource = new PersonsDataSource(getSession().getDatabase());
        if (!existingPerson.isStored() || personsDataSource.findBySqlId(existingPerson.getSqlId()) == null) {
            LogJourno.reportEvent(EVENT.ChangesChanger_updateChangeRequestedWithNewChange, PersonChanger.class.getSimpleName());
            Log.e(new Throwable("I can only handle existing Persons!"));
            return ChangeResult.FAILED;
        }
        createAndRelateNewOrgChangeIfNewOrgExists(existingPerson);
        return personsDataSource.createOrUpdate((BaseDatasourceEntity) existingPerson) == -1 ? ChangeResult.FAILED : ChangeResult.SUCCESSFUL;
    }

    private void createAndRelateNewOrgChangeIfNewOrgExists(Person person) {
        if (person.getCompany() != null && !person.getCompany().isStored() && !new OrganizationChanger((Changer) this).create(person.getCompany())) {
            person.setCompany(null);
        }
    }
}
