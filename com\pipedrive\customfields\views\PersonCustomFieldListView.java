package com.pipedrive.customfields.views;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Pair;
import com.pipedrive.application.Session;
import com.pipedrive.customfields.views.CustomFieldListView.OnItemClickListener;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.model.Person;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.util.CustomFieldsUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PersonCustomFieldListView extends CustomFieldListView {

    public interface OnPersonCustomFieldListItemClickListener extends OnItemClickListener {
        void onEmailClicked(@NonNull String str, @NonNull String str2);
    }

    public PersonCustomFieldListView(Context context) {
        this(context, null);
    }

    public PersonCustomFieldListView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PersonCustomFieldListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @NonNull
    protected String getSpecialFieldsType() {
        return "person";
    }

    @NonNull
    protected List<CustomField> getCustomFields(@NonNull Session session) {
        Person person = (Person) new PersonsDataSource(session.getDatabase()).findBySqlId(this.mSqlId.longValue());
        if (person == null) {
            return Collections.emptyList();
        }
        Pair<ArrayList<CustomField>, Integer> strangeCustomFieldArray = CustomFieldsUtil.populateCustomAndSpecialFields("person", person.getCustomFields(), null);
        return ((ArrayList) strangeCustomFieldArray.first).subList(((Integer) strangeCustomFieldArray.second).intValue(), ((ArrayList) strangeCustomFieldArray.first).size());
    }
}
