package com.zendesk.belvedere;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import java.io.File;

public class BelvedereResult implements Parcelable {
    public static final Creator<BelvedereResult> CREATOR = new Creator<BelvedereResult>() {
        public BelvedereResult createFromParcel(@NonNull Parcel in) {
            return new BelvedereResult(in);
        }

        @NonNull
        public BelvedereResult[] newArray(int size) {
            return new BelvedereResult[size];
        }
    };
    private final File file;
    private final Uri uri;

    public BelvedereResult(@NonNull File file, @NonNull Uri uri) {
        this.file = file;
        this.uri = uri;
    }

    @NonNull
    public File getFile() {
        return this.file;
    }

    @NonNull
    public Uri getUri() {
        return this.uri;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeSerializable(this.file);
        dest.writeParcelable(this.uri, flags);
    }

    private BelvedereResult(Parcel in) {
        this.file = (File) in.readSerializable();
        this.uri = (Uri) in.readParcelable(BelvedereResult.class.getClassLoader());
    }
}
