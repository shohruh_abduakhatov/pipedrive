package com.pipedrive.sync.recents;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class RecentsSyncService extends Service {
    public static final String RECENTS_AUTHORITY = "com.pipedrive.contentproviders.recentsprovider";
    private static final Object SYNC_ADAPTER_LOCK = new Object();
    private static RecentsSyncAdapter sSyncAdapter = null;

    public void onCreate() {
        synchronized (SYNC_ADAPTER_LOCK) {
            if (sSyncAdapter == null) {
                sSyncAdapter = new RecentsSyncAdapter(getApplicationContext(), true);
            }
        }
    }

    public IBinder onBind(Intent intent) {
        return sSyncAdapter.getSyncAdapterBinder();
    }
}
