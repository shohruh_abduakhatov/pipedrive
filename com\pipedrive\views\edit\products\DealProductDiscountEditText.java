package com.pipedrive.views.edit.products;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.pipedrive.R;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.util.formatter.PercentFormatter;

public class DealProductDiscountEditText extends DealProductNumericEditTextWithFormatting {
    public DealProductDiscountEditText(Context context) {
        super(context);
    }

    public DealProductDiscountEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DealProductDiscountEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected int getLabelTextResourceId() {
        return R.string.lbl_discount;
    }

    @Nullable
    protected String getFormattedValue() {
        if (getSession() == null) {
            return null;
        }
        return new PercentFormatter(getSession()).formatDealProductDiscount(Double.valueOf(getValue()));
    }

    double getInitialValue(@NonNull DealProduct dealProduct) {
        return dealProduct.getDiscountPercentage().doubleValue();
    }
}
