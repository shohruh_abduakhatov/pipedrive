package com.pipedrive.util;

import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.widget.TextView;

public class CompatUtil {
    private CompatUtil() {
    }

    public static void setTextAppearance(@NonNull TextView textView, @StyleRes int resId) {
        if (VERSION.SDK_INT < 23) {
            textView.setTextAppearance(textView.getContext(), resId);
        } else {
            textView.setTextAppearance(resId);
        }
    }
}
