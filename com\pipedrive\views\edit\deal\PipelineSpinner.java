package com.pipedrive.views.edit.deal;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.PipelineDataSource;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Pipeline;
import com.pipedrive.views.Spinner.LabelBuilder;
import com.pipedrive.views.Spinner.OnItemSelectedListener;
import com.pipedrive.views.common.SpinnerWithUnderlineAndLabel;

public class PipelineSpinner extends SpinnerWithUnderlineAndLabel {

    public interface OnPipelineSelectedListener {
        void onPipelineSelected(@NonNull Pipeline pipeline);
    }

    public PipelineSpinner(Context context) {
        this(context, null);
    }

    public PipelineSpinner(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PipelineSpinner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected int getLabelTextResourceId() {
        return R.string.pipeline;
    }

    public void setup(@NonNull Session session, @NonNull final Deal deal, @Nullable final OnPipelineSelectedListener onPipelineSelectedListener) {
        super.setup(new PipelineDataSource(session.getDatabase()).getPipelines(100), new LabelBuilder<Pipeline>() {
            public String getLabel(Pipeline item) {
                return item.getName();
            }

            public boolean isSelected(Pipeline item) {
                return deal.getPipelineId() == item.getPipedriveId();
            }
        }, new OnItemSelectedListener<Pipeline>() {
            public void onItemSelected(Pipeline item) {
                if (onPipelineSelectedListener != null) {
                    onPipelineSelectedListener.onPipelineSelected(item);
                }
            }
        });
    }
}
