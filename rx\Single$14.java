package rx;

import rx.functions.Action1;

class Single$14 implements Action1<Throwable> {
    final /* synthetic */ Single this$0;
    final /* synthetic */ Action1 val$onError;

    Single$14(Single single, Action1 action1) {
        this.this$0 = single;
        this.val$onError = action1;
    }

    public void call(Throwable throwable) {
        this.val$onError.call(throwable);
    }
}
