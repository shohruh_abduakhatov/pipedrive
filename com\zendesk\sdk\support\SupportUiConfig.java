package com.zendesk.sdk.support;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import com.zendesk.util.CollectionUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SupportUiConfig implements Parcelable {
    public static final Creator<SupportUiConfig> CREATOR = new Creator<SupportUiConfig>() {
        public SupportUiConfig createFromParcel(Parcel in) {
            return new SupportUiConfig(in);
        }

        public SupportUiConfig[] newArray(int size) {
            return new SupportUiConfig[size];
        }
    };
    private static final String LOG_TAG = "SupportUiConfig";
    private boolean addListPaddingBottom;
    private List<Long> categoryIds;
    private boolean collapseCategories;
    private String[] labelNames;
    private List<Long> sectionIds;
    private boolean showContactUsButton;
    private boolean showConversationsMenuButton;

    static class Builder {
        private boolean addListPaddingBottom = this.showContactUsButton;
        private List<Long> categoryIds = Collections.emptyList();
        private boolean collapseCategories;
        private String[] labelNames = new String[0];
        private List<Long> sectionIds = Collections.emptyList();
        private boolean showContactUsButton = true;
        private boolean showConversationsMenuButton = true;

        Builder() {
        }

        Builder withCategoryIds(List<Long> categoryIds) {
            this.categoryIds = CollectionUtils.copyOf(categoryIds);
            return this;
        }

        Builder withSectionIds(List<Long> sectionIds) {
            this.sectionIds = CollectionUtils.copyOf(sectionIds);
            return this;
        }

        Builder withLabelNames(String[] labelNames) {
            if (labelNames != null) {
                this.labelNames = (String[]) labelNames.clone();
            }
            return this;
        }

        Builder withShowContactUsButton(boolean showContactUsButton) {
            this.showContactUsButton = showContactUsButton;
            return this;
        }

        Builder withAddListPaddingBottom(boolean addListPaddingBottom) {
            this.addListPaddingBottom = addListPaddingBottom;
            return this;
        }

        Builder withCollapseCategories(boolean collapseCategories) {
            this.collapseCategories = collapseCategories;
            return this;
        }

        Builder withShowConversationsMenuButton(boolean showConversationsMenuButton) {
            this.showConversationsMenuButton = showConversationsMenuButton;
            return this;
        }

        SupportUiConfig build() {
            return new SupportUiConfig();
        }
    }

    public List<Long> getCategoryIds() {
        return this.categoryIds;
    }

    public List<Long> getSectionIds() {
        return this.sectionIds;
    }

    public String[] getLabelNames() {
        return this.labelNames;
    }

    boolean isShowContactUsButton() {
        return this.showContactUsButton;
    }

    public boolean isAddListPaddingBottom() {
        return this.addListPaddingBottom;
    }

    public boolean isCollapseCategories() {
        return this.collapseCategories;
    }

    public boolean isShowConversationsMenuButton() {
        return this.showConversationsMenuButton;
    }

    private SupportUiConfig(Builder builder) {
        this.categoryIds = builder.categoryIds;
        this.sectionIds = builder.sectionIds;
        this.labelNames = builder.labelNames;
        this.showContactUsButton = builder.showContactUsButton;
        this.addListPaddingBottom = builder.addListPaddingBottom;
        this.collapseCategories = builder.collapseCategories;
        this.showConversationsMenuButton = builder.showConversationsMenuButton;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected SupportUiConfig(Parcel in) {
        this.categoryIds = new ArrayList();
        in.readList(this.categoryIds, null);
        this.sectionIds = new ArrayList();
        in.readList(this.sectionIds, null);
        this.labelNames = in.createStringArray();
        boolean[] parcelledBools = in.createBooleanArray();
        switch (parcelledBools.length) {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                this.showConversationsMenuButton = parcelledBools[3];
                break;
            default:
                return;
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeList(this.categoryIds);
        dest.writeList(this.sectionIds);
        dest.writeStringArray(this.labelNames);
        dest.writeBooleanArray(new boolean[]{this.showContactUsButton, this.addListPaddingBottom, this.collapseCategories, this.showConversationsMenuButton});
    }
}
