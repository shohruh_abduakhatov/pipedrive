package android.support.v7.gridlayout;

public final class R {

    public static final class attr {
        public static final int alignmentMode = 2130772219;
        public static final int columnCount = 2130772217;
        public static final int columnOrderPreserved = 2130772221;
        public static final int layout_column = 2130772225;
        public static final int layout_columnSpan = 2130772226;
        public static final int layout_columnWeight = 2130772227;
        public static final int layout_gravity = 2130772228;
        public static final int layout_row = 2130772222;
        public static final int layout_rowSpan = 2130772223;
        public static final int layout_rowWeight = 2130772224;
        public static final int orientation = 2130772215;
        public static final int rowCount = 2130772216;
        public static final int rowOrderPreserved = 2130772220;
        public static final int useDefaultMargins = 2130772218;
    }

    public static final class dimen {
        public static final int default_gap = 2131493022;
    }

    public static final class id {
        public static final int alignBounds = 2131820618;
        public static final int alignMargins = 2131820619;
        public static final int bottom = 2131820598;
        public static final int center = 2131820599;
        public static final int center_horizontal = 2131820600;
        public static final int center_vertical = 2131820601;
        public static final int clip_horizontal = 2131820610;
        public static final int clip_vertical = 2131820611;
        public static final int end = 2131820602;
        public static final int fill = 2131820612;
        public static final int fill_horizontal = 2131820613;
        public static final int fill_vertical = 2131820603;
        public static final int horizontal = 2131820616;
        public static final int left = 2131820604;
        public static final int right = 2131820605;
        public static final int start = 2131820606;
        public static final int top = 2131820607;
        public static final int vertical = 2131820617;
    }

    public static final class styleable {
        public static final int[] GridLayout = new int[]{com.pipedrive.R.attr.orientation, com.pipedrive.R.attr.rowCount, com.pipedrive.R.attr.columnCount, com.pipedrive.R.attr.useDefaultMargins, com.pipedrive.R.attr.alignmentMode, com.pipedrive.R.attr.rowOrderPreserved, com.pipedrive.R.attr.columnOrderPreserved};
        public static final int[] GridLayout_Layout = new int[]{16842996, 16842997, 16842998, 16842999, 16843000, 16843001, 16843002, com.pipedrive.R.attr.layout_row, com.pipedrive.R.attr.layout_rowSpan, com.pipedrive.R.attr.layout_rowWeight, com.pipedrive.R.attr.layout_column, com.pipedrive.R.attr.layout_columnSpan, com.pipedrive.R.attr.layout_columnWeight, com.pipedrive.R.attr.layout_gravity};
        public static final int GridLayout_Layout_android_layout_height = 1;
        public static final int GridLayout_Layout_android_layout_margin = 2;
        public static final int GridLayout_Layout_android_layout_marginBottom = 6;
        public static final int GridLayout_Layout_android_layout_marginLeft = 3;
        public static final int GridLayout_Layout_android_layout_marginRight = 5;
        public static final int GridLayout_Layout_android_layout_marginTop = 4;
        public static final int GridLayout_Layout_android_layout_width = 0;
        public static final int GridLayout_Layout_layout_column = 10;
        public static final int GridLayout_Layout_layout_columnSpan = 11;
        public static final int GridLayout_Layout_layout_columnWeight = 12;
        public static final int GridLayout_Layout_layout_gravity = 13;
        public static final int GridLayout_Layout_layout_row = 7;
        public static final int GridLayout_Layout_layout_rowSpan = 8;
        public static final int GridLayout_Layout_layout_rowWeight = 9;
        public static final int GridLayout_alignmentMode = 4;
        public static final int GridLayout_columnCount = 2;
        public static final int GridLayout_columnOrderPreserved = 6;
        public static final int GridLayout_orientation = 0;
        public static final int GridLayout_rowCount = 1;
        public static final int GridLayout_rowOrderPreserved = 5;
        public static final int GridLayout_useDefaultMargins = 3;
    }
}
