package com.pipedrive.application;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.notification.ActivityReminderOption;
import com.pipedrive.notification.AllDayActivityReminderOption;
import java.util.Locale;

public final class SharedSession {
    private static final String PREFS_ACTIVITY_REMINDER_OPTION = "Session.PREFS_ACTIVITY_REMINDER_OPTION";
    private static final String PREFS_ALL_DAY_ACTIVITY_REMINDER_OPTION = "Session.PREFS_ALL_DAY_ACTIVITY_REMINDER_OPTION";
    private static final String PREFS_DISPLAY_WHATS_NEW_SCREEN = "SharedSession.PREFS__DISPLAY_WHATS_NEW_SCREEN";
    private static final String PREFS_DO_NOT_SHOW_INLINE_COMPONENT_WITH_ID = "Session.PREFS__DO_NOT_SHOW_INLINE_COMPONENT_WITH_ID";
    private static final String PREFS_ENABLE_ANALYTICS = "Session.PREFS__ENABLE_ANALYTICS";
    private static final String PREFS_ENABLE_CALL_SUMMARY = "Session.PREFS__ENABLE_CALL_SUMMARY";
    private static final String PREFS_ENABLE_OFFLINE_MODE = "Session.PREFS__ENABLE_OFFLINE_MODE";
    private static final int PREFS_INT_BOOLEAN_FALSE = 0;
    private static final int PREFS_INT_BOOLEAN_TRUE = 1;
    @NonNull
    private final Context mApplicationContext;
    volatile DBConnectorForSharedSession mSharedDBConnector = null;

    protected SharedSession(@NonNull Context context) {
        this.mApplicationContext = context.getApplicationContext();
        Analytics.setAppOptOut(this.mApplicationContext, !isAnalyticsEnabled());
    }

    @NonNull
    public final Context getApplicationContext() {
        return this.mApplicationContext;
    }

    public final SQLiteDatabase getDatabase() {
        if (this.mSharedDBConnector == null) {
            this.mSharedDBConnector = new DBConnectorForSharedSession(this.mApplicationContext);
        }
        return this.mSharedDBConnector.getDatabase();
    }

    public final void enableAnalytics(boolean enableAnalytics) {
        boolean z;
        int i = 1;
        Context context = this.mApplicationContext;
        if (enableAnalytics) {
            z = false;
        } else {
            z = true;
        }
        Analytics.setAppOptOut(context, z);
        Context context2 = this.mApplicationContext;
        String str = PREFS_ENABLE_ANALYTICS;
        if (!enableAnalytics) {
            i = 0;
        }
        com.pipedrive.application.SessionStoreHelper.SharedSession.setSessionPrefsInt(context2, str, Integer.valueOf(i));
    }

    public final boolean isAnalyticsEnabled() {
        if (com.pipedrive.application.SessionStoreHelper.SharedSession.getSessionPrefsInt(this.mApplicationContext, PREFS_ENABLE_ANALYTICS, Integer.valueOf(1)).intValue() == 1) {
            return true;
        }
        return false;
    }

    public final void enableOfflineSupport(boolean enableOfflineSupport) {
        com.pipedrive.application.SessionStoreHelper.SharedSession.setSessionPrefsInt(this.mApplicationContext, PREFS_ENABLE_OFFLINE_MODE, Integer.valueOf(enableOfflineSupport ? 1 : 0));
    }

    public final boolean isOfflineSupportEnabled() {
        return true;
    }

    public final boolean displayWhatsNewScreens() {
        boolean newUser;
        boolean whatsNewScreensAreNotSeenYet = true;
        int displayWhatsNewScreenPrefs = com.pipedrive.application.SessionStoreHelper.SharedSession.getSessionPrefsInt(this.mApplicationContext, PREFS_DISPLAY_WHATS_NEW_SCREEN, Integer.valueOf(0)).intValue();
        if (displayWhatsNewScreenPrefs == 0) {
            newUser = true;
        } else {
            newUser = false;
        }
        if (newUser) {
            disableDisplayingWhatsNewScreensForThisVersionCode();
            return false;
        }
        if (displayWhatsNewScreenPrefs == 6) {
            whatsNewScreensAreNotSeenYet = false;
        }
        return whatsNewScreensAreNotSeenYet;
    }

    public final void disableDisplayingWhatsNewScreensForThisVersionCode() {
        com.pipedrive.application.SessionStoreHelper.SharedSession.setSessionPrefsInt(this.mApplicationContext, PREFS_DISPLAY_WHATS_NEW_SCREEN, Integer.valueOf(6));
    }

    public final void enableCallLogging(boolean enableCallLogging) {
        com.pipedrive.application.SessionStoreHelper.SharedSession.setSessionPrefsInt(this.mApplicationContext, PREFS_ENABLE_CALL_SUMMARY, Integer.valueOf(enableCallLogging ? 1 : 0));
    }

    public final boolean isCallLoggingEnabled() {
        if (com.pipedrive.application.SessionStoreHelper.SharedSession.getSessionPrefsInt(this.mApplicationContext, PREFS_ENABLE_CALL_SUMMARY, Integer.valueOf(1)).intValue() == 1) {
            return true;
        }
        return false;
    }

    public boolean shouldNotShowInlineIntroComponentWithId(int inlineIntroId) {
        if (com.pipedrive.application.SessionStoreHelper.SharedSession.getSessionPrefsInt(this.mApplicationContext, String.format(Locale.getDefault(), "%s_%d", new Object[]{PREFS_DO_NOT_SHOW_INLINE_COMPONENT_WITH_ID, Integer.valueOf(inlineIntroId)}), Integer.valueOf(0)).intValue() == 1) {
            return true;
        }
        return false;
    }

    public void setDoNotShowInlineComponentWithId(int inlineIntroId) {
        com.pipedrive.application.SessionStoreHelper.SharedSession.setSessionPrefsInt(this.mApplicationContext, String.format(Locale.getDefault(), "%s_%d", new Object[]{PREFS_DO_NOT_SHOW_INLINE_COMPONENT_WITH_ID, Integer.valueOf(inlineIntroId)}), Integer.valueOf(1));
    }

    @VisibleForTesting
    public void clearDoNotShowInlineComponentWithId(int inlineIntroId) {
        com.pipedrive.application.SessionStoreHelper.SharedSession.setSessionPrefsInt(this.mApplicationContext, String.format(Locale.getDefault(), "%s_%d", new Object[]{PREFS_DO_NOT_SHOW_INLINE_COMPONENT_WITH_ID, Integer.valueOf(inlineIntroId)}), Integer.valueOf(0));
    }

    public ActivityReminderOption getActivityReminderOption() {
        return ActivityReminderOption.values()[com.pipedrive.application.SessionStoreHelper.SharedSession.getSessionPrefsInt(this.mApplicationContext, PREFS_ACTIVITY_REMINDER_OPTION, Integer.valueOf(ActivityReminderOption.DEFAULT.ordinal())).intValue()];
    }

    public void setActivityReminderOption(@NonNull ActivityReminderOption value) {
        com.pipedrive.application.SessionStoreHelper.SharedSession.setSessionPrefsInt(this.mApplicationContext, PREFS_ACTIVITY_REMINDER_OPTION, Integer.valueOf(value.ordinal()));
    }

    public AllDayActivityReminderOption getAllDayActivityReminderOption() {
        return AllDayActivityReminderOption.values()[com.pipedrive.application.SessionStoreHelper.SharedSession.getSessionPrefsInt(this.mApplicationContext, PREFS_ALL_DAY_ACTIVITY_REMINDER_OPTION, Integer.valueOf(AllDayActivityReminderOption.DEFAULT.ordinal())).intValue()];
    }

    public void setAllDayActivityReminderOption(@NonNull AllDayActivityReminderOption value) {
        com.pipedrive.application.SessionStoreHelper.SharedSession.setSessionPrefsInt(this.mApplicationContext, PREFS_ALL_DAY_ACTIVITY_REMINDER_OPTION, Integer.valueOf(value.ordinal()));
    }
}
