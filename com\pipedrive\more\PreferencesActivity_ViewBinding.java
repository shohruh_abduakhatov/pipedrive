package com.pipedrive.more;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class PreferencesActivity_ViewBinding implements Unbinder {
    private PreferencesActivity target;
    private View view2131820811;
    private View view2131820814;
    private View view2131820815;

    @UiThread
    public PreferencesActivity_ViewBinding(PreferencesActivity target) {
        this(target, target.getWindow().getDecorView());
    }

    @UiThread
    public PreferencesActivity_ViewBinding(final PreferencesActivity target, View source) {
        this.target = target;
        View view = Utils.findRequiredView(source, R.id.languagePreference, "field 'languagePreference' and method 'onLanguageClicked'");
        target.languagePreference = (MoreButton) Utils.castView(view, R.id.languagePreference, "field 'languagePreference'", MoreButton.class);
        this.view2131820811 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onLanguageClicked();
            }
        });
        view = Utils.findRequiredView(source, R.id.allDayActivityReminder, "field 'allDayActivityReminderPreference' and method 'onAllDayActivityReminderClicked'");
        target.allDayActivityReminderPreference = (MoreButton) Utils.castView(view, R.id.allDayActivityReminder, "field 'allDayActivityReminderPreference'", MoreButton.class);
        this.view2131820815 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onAllDayActivityReminderClicked();
            }
        });
        view = Utils.findRequiredView(source, R.id.activityReminder, "field 'activityReminderPreference' and method 'onActivityReminderClicked'");
        target.activityReminderPreference = (MoreButton) Utils.castView(view, R.id.activityReminder, "field 'activityReminderPreference'", MoreButton.class);
        this.view2131820814 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onActivityReminderClicked();
            }
        });
        target.analyticsPreference = (MoreButton) Utils.findRequiredViewAsType(source, R.id.analyticsPreference, "field 'analyticsPreference'", MoreButton.class);
        target.callLoggingPreference = (MoreButton) Utils.findRequiredViewAsType(source, R.id.logCalls, "field 'callLoggingPreference'", MoreButton.class);
    }

    @CallSuper
    public void unbind() {
        PreferencesActivity target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.languagePreference = null;
        target.allDayActivityReminderPreference = null;
        target.activityReminderPreference = null;
        target.analyticsPreference = null;
        target.callLoggingPreference = null;
        this.view2131820811.setOnClickListener(null);
        this.view2131820811 = null;
        this.view2131820815.setOnClickListener(null);
        this.view2131820815 = null;
        this.view2131820814.setOnClickListener(null);
        this.view2131820814 = null;
    }
}
