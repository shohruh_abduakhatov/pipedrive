package okhttp3;

import okhttp3.Headers.Builder;

public class Response$Builder {
    ResponseBody body;
    Response cacheResponse;
    int code;
    Handshake handshake;
    Builder headers;
    String message;
    Response networkResponse;
    Response priorResponse;
    Protocol protocol;
    long receivedResponseAtMillis;
    Request request;
    long sentRequestAtMillis;

    public Response$Builder() {
        this.code = -1;
        this.headers = new Builder();
    }

    Response$Builder(Response response) {
        this.code = -1;
        this.request = response.request;
        this.protocol = response.protocol;
        this.code = response.code;
        this.message = response.message;
        this.handshake = response.handshake;
        this.headers = response.headers.newBuilder();
        this.body = response.body;
        this.networkResponse = response.networkResponse;
        this.cacheResponse = response.cacheResponse;
        this.priorResponse = response.priorResponse;
        this.sentRequestAtMillis = response.sentRequestAtMillis;
        this.receivedResponseAtMillis = response.receivedResponseAtMillis;
    }

    public Response$Builder request(Request request) {
        this.request = request;
        return this;
    }

    public Response$Builder protocol(Protocol protocol) {
        this.protocol = protocol;
        return this;
    }

    public Response$Builder code(int code) {
        this.code = code;
        return this;
    }

    public Response$Builder message(String message) {
        this.message = message;
        return this;
    }

    public Response$Builder handshake(Handshake handshake) {
        this.handshake = handshake;
        return this;
    }

    public Response$Builder header(String name, String value) {
        this.headers.set(name, value);
        return this;
    }

    public Response$Builder addHeader(String name, String value) {
        this.headers.add(name, value);
        return this;
    }

    public Response$Builder removeHeader(String name) {
        this.headers.removeAll(name);
        return this;
    }

    public Response$Builder headers(Headers headers) {
        this.headers = headers.newBuilder();
        return this;
    }

    public Response$Builder body(ResponseBody body) {
        this.body = body;
        return this;
    }

    public Response$Builder networkResponse(Response networkResponse) {
        if (networkResponse != null) {
            checkSupportResponse("networkResponse", networkResponse);
        }
        this.networkResponse = networkResponse;
        return this;
    }

    public Response$Builder cacheResponse(Response cacheResponse) {
        if (cacheResponse != null) {
            checkSupportResponse("cacheResponse", cacheResponse);
        }
        this.cacheResponse = cacheResponse;
        return this;
    }

    private void checkSupportResponse(String name, Response response) {
        if (response.body != null) {
            throw new IllegalArgumentException(name + ".body != null");
        } else if (response.networkResponse != null) {
            throw new IllegalArgumentException(name + ".networkResponse != null");
        } else if (response.cacheResponse != null) {
            throw new IllegalArgumentException(name + ".cacheResponse != null");
        } else if (response.priorResponse != null) {
            throw new IllegalArgumentException(name + ".priorResponse != null");
        }
    }

    public Response$Builder priorResponse(Response priorResponse) {
        if (priorResponse != null) {
            checkPriorResponse(priorResponse);
        }
        this.priorResponse = priorResponse;
        return this;
    }

    private void checkPriorResponse(Response response) {
        if (response.body != null) {
            throw new IllegalArgumentException("priorResponse.body != null");
        }
    }

    public Response$Builder sentRequestAtMillis(long sentRequestAtMillis) {
        this.sentRequestAtMillis = sentRequestAtMillis;
        return this;
    }

    public Response$Builder receivedResponseAtMillis(long receivedResponseAtMillis) {
        this.receivedResponseAtMillis = receivedResponseAtMillis;
        return this;
    }

    public Response build() {
        if (this.request == null) {
            throw new IllegalStateException("request == null");
        } else if (this.protocol == null) {
            throw new IllegalStateException("protocol == null");
        } else if (this.code >= 0) {
            return new Response(this);
        } else {
            throw new IllegalStateException("code < 0: " + this.code);
        }
    }
}
