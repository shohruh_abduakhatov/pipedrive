package com.pipedrive.customfields.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.customfields.views.CustomFieldListView.OnItemClickAdapter;
import com.pipedrive.customfields.views.CustomFieldListView.OnItemClickListener;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.model.Deal;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.util.CustomFieldsUtil;
import java.util.ArrayList;

class DealSpecialCustomFieldViewHolder extends SpecialCustomFieldViewHolder {
    public DealSpecialCustomFieldViewHolder(@NonNull Context context, @NonNull Long itemSqlId) {
        super(context, itemSqlId);
    }

    void bind(@NonNull Session session, @Nullable OnItemClickListener onItemClickListener) {
        Deal deal = (Deal) new DealsDataSource(session.getDatabase()).findBySqlId(this.itemSqlId.longValue());
        if (deal != null) {
            LinearLayout container = new LinearLayout(this.itemView.getContext());
            container.setOrientation(1);
            Pair<ArrayList<CustomField>, Integer> strangeCustomFieldsStructure = CustomFieldsUtil.populateCustomAndSpecialFields("deal", deal.getCustomFields(), deal);
            for (CustomField customField : ((ArrayList) strangeCustomFieldsStructure.first).subList(0, ((Integer) strangeCustomFieldsStructure.second).intValue())) {
                if (container.getChildCount() > 0) {
                    View divider = new View(this.itemView.getContext());
                    divider.setBackgroundResource(R.drawable.divider);
                    container.addView(divider, new LayoutParams(-1, -2));
                }
                boolean customFieldIsEmpty = customField.isEmpty();
                View view = LayoutInflater.from(this.itemView.getContext()).inflate(customFieldIsEmpty ? R.layout.row_custom_field_empty : R.layout.row_custom_field, container, false);
                CustomFieldViewHolder customFieldViewHolder = new CustomFieldViewHolder(view);
                final OnItemClickListener onItemClickListener2 = onItemClickListener;
                OnItemClickAdapter onClickAdapter = new OnItemClickAdapter() {
                    public void onCustomFieldClicked(@NonNull CustomField customField) {
                        if (Deal.EXPECTED_CLOSE_DATE.equalsIgnoreCase(customField.getKey()) && onItemClickListener2 != null) {
                            onItemClickListener2.onCustomFieldClicked(customField);
                        }
                    }

                    public void onCustomFieldLongClicked(@NonNull String value) {
                        if (onItemClickListener2 != null) {
                            onItemClickListener2.onCustomFieldLongClicked(value);
                        }
                    }
                };
                if (customFieldIsEmpty) {
                    customFieldViewHolder.bindEmpty(customField, onClickAdapter);
                } else {
                    customFieldViewHolder.bind(session, customField, onClickAdapter);
                }
                container.addView(view, new LayoutParams(-1, view.getLayoutParams().height));
                this.rootContainer.removeAllViews();
                this.rootContainer.addView(container, new LayoutParams(-1, -1));
            }
        }
    }
}
