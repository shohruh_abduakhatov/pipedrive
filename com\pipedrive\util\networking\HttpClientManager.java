package com.pipedrive.util.networking;

import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.newrelic.agent.android.instrumentation.JSONObjectInstrumentation;
import com.newrelic.agent.android.instrumentation.okhttp3.OkHttp3Instrumentation;
import com.pipedrive.BuildConfig;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONException;
import org.json.JSONObject;

public enum HttpClientManager {
    INSTANCE;
    
    static final int CONNECT_TIMEOUT_SECONDS = 60;
    static final String HEADER_CONTENT_TYPE = "Content-Type";
    static final String HEADER_USER_AGENT = "User-Agent";
    static final int RETRY_DELAY_SECONDS = 5;
    static final int RETRY_DELAY_SECONDS_LONG = 20;
    private static final String TAG = null;
    static final String TYPE_APPLICATION_JSON = "application/json";
    private OkHttpClient mHttpClient;
    private String mUserAgentCached;

    static {
        TAG = HttpClientManager.class.getSimpleName();
    }

    @NonNull
    public OkHttpClient getHttpClient() {
        return this.mHttpClient;
    }

    @Nullable
    private Response post(@NonNull Context context, @NonNull String url, @NonNull String jsonData) {
        Log.d(TAG, "We'll be opening connection to URL: " + url);
        Log.d(TAG, "Request type is POST; UA=" + getUserAgent());
        Response response = null;
        try {
            Builder post = new Builder().url(url.replace(" ", "%20")).header("Content-Type", "application/json").header("User-Agent", getUserAgent()).post(RequestBody.create(MediaType.parse("application/json"), jsonData));
            Request request = !(post instanceof Builder) ? post.build() : OkHttp3Instrumentation.build(post);
            OkHttpClient okHttpClient = this.mHttpClient;
            response = (!(okHttpClient instanceof OkHttpClient) ? okHttpClient.newCall(request) : OkHttp3Instrumentation.newCall(okHttpClient, request)).execute();
        } catch (Exception e) {
            LogJourno.reportEvent(EVENT.Networking_httpClientManagerPostNewCallException, String.format("Url:[%s]", new Object[]{ApiUrlBuilder$Tools.stripAPIKeyFromURL(url)}), e);
            Log.e(new Throwable("Exception caught while POST: " + urlToLog, e));
        }
        Analytics.dispatchData(context);
        return response;
    }

    @NonNull
    public String getUserAgent() {
        if (this.mUserAgentCached != null) {
            return this.mUserAgentCached;
        }
        String format = String.format("PipedriveAndroid/%s (%s %s/%s; %s)", new Object[]{BuildConfig.APP_VERSION_NAME, Build.BRAND, Build.MANUFACTURER, Build.MODEL, Integer.valueOf(VERSION.SDK_INT)});
        this.mUserAgentCached = format;
        return format;
    }

    @Nullable
    public final Response authorization(@NonNull Context context, @Nullable String email, @Nullable String password) {
        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            return null;
        }
        try {
            JSONObject putOpt = new JSONObject().putOpt("email", email).putOpt("password", password);
            return INSTANCE.post(context, new ApiUrlBuilderWithoutSession("authorizations").build(), !(putOpt instanceof JSONObject) ? putOpt.toString() : JSONObjectInstrumentation.toString(putOpt));
        } catch (JSONException e) {
            LogJourno.reportEvent(EVENT.Authorization_pipedriveAuthJsonResponseException, e.getMessage());
            Log.e(new Throwable("Login JSON request composition failed! Will not log user in!", e));
            return null;
        }
    }

    @Nullable
    public Response authorizationGoogleAuth(@NonNull Context context, @Nullable String googleAuthToken) {
        if (TextUtils.isEmpty(googleAuthToken)) {
            return null;
        }
        try {
            JSONObject googlePlusLoginJSON = new JSONObject();
            googlePlusLoginJSON.put("token", googleAuthToken);
            googlePlusLoginJSON.put("client", AbstractSpiCall.ANDROID_CLIENT_TYPE);
            return INSTANCE.post(context, new ApiUrlBuilderWithoutSession("authorizations/googleOAuth2").build(), !(googlePlusLoginJSON instanceof JSONObject) ? googlePlusLoginJSON.toString() : JSONObjectInstrumentation.toString(googlePlusLoginJSON));
        } catch (JSONException e) {
            LogJourno.reportEvent(EVENT.Authorization_pipedriveAuthJsonResponseException, e.getMessage());
            Log.e(new Throwable("Login JSON request composition failed! Will not log user in!", e));
            return null;
        }
    }
}
