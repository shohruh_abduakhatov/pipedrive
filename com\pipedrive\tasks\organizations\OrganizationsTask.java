package com.pipedrive.tasks.organizations;

import com.pipedrive.application.Session;
import com.pipedrive.tasks.AsyncTask;
import com.pipedrive.util.ContactsUtil;

public class OrganizationsTask extends AsyncTask<String, Integer, Void> {
    public OrganizationsTask(Session session) {
        super(session);
    }

    protected Void doInBackground(String... params) {
        ContactsUtil.downloadOrganizations(getSession());
        return null;
    }
}
