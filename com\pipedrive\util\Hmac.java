package com.pipedrive.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public enum Hmac {
    ;
    
    private static final String ALGORITHM = "HMACMD5";

    @Nullable
    public static String md5(@NonNull String key, @NonNull String message) {
        String digest = null;
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(HttpRequest.CHARSET_UTF8), ALGORITHM);
            Mac mac = Mac.getInstance(ALGORITHM);
            mac.init(secretKeySpec);
            byte[] bytes = mac.doFinal(message.getBytes("ASCII"));
            StringBuilder hash = new StringBuilder();
            for (byte aByte : bytes) {
                String hex = Integer.toHexString(aByte & 255);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (UnsupportedEncodingException e) {
        } catch (NoSuchAlgorithmException e2) {
        } catch (InvalidKeyException e3) {
        }
        return digest;
    }
}
