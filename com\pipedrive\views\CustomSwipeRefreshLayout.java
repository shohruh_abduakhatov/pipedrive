package com.pipedrive.views;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

public class CustomSwipeRefreshLayout extends SwipeRefreshLayout {
    public CustomSwipeRefreshLayout(Context context) {
        super(context);
    }

    public CustomSwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean canChildScrollUp() {
        ViewGroup target = getChildAt(0) instanceof ViewGroup ? (ViewGroup) getChildAt(0) : (ViewGroup) getChildAt(1);
        View scrollableView = target.getChildAt(1);
        if (scrollableView.getVisibility() == 8) {
            scrollableView = target.getChildAt(0);
        }
        return ViewCompat.canScrollVertically(scrollableView, -1);
    }
}
