package com.pipedrive.nearby;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.FrameLayout;
import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.pipedrive.BaseActivity;
import com.pipedrive.OrganizationDetailActivity;
import com.pipedrive.PersonDetailActivity;
import com.pipedrive.R;
import com.pipedrive.deal.view.DealDetailsActivity;
import com.pipedrive.logging.Log;
import com.pipedrive.nearby.cards.Cards;
import com.pipedrive.nearby.cards.NearbyRequestEventBus;
import com.pipedrive.nearby.cards.cards.CardEventBus;
import com.pipedrive.nearby.cards.cards.EmptyCard;
import com.pipedrive.nearby.filter.views.FilterButton;
import com.pipedrive.nearby.filter.views.NearbyFilter;
import com.pipedrive.nearby.location.LocationServicesConnectionManager;
import com.pipedrive.nearby.map.PDMapFragment;
import com.pipedrive.nearby.model.NearbyItem;
import com.pipedrive.nearby.searchbutton.SearchButton;
import com.pipedrive.nearby.toolbar.NearbyToolbar;
import com.pipedrive.nearby.util.Irrelevant;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class NearbyActivity extends BaseActivity {
    @NonNull
    private final CompositeSubscription allSubscriptions = new CompositeSubscription();
    @BindView(2131820798)
    Cards cards;
    @BindView(2131820799)
    EmptyCard emptyCard;
    @BindView(2131820797)
    FilterButton filterButton;
    @NonNull
    private NearbyPresenter mPresenter;
    @NonNull
    private NearbyFilter nearbyFilter;
    @BindView(2131820795)
    NearbyToolbar nearbyToolbar;
    @BindView(2131820794)
    View progress;
    @BindDimen(2131493165)
    int progressTranslationY;
    @BindView(2131820796)
    SearchButton searchButton;

    public static void startActivity(@NonNull Activity activity) {
        ContextCompat.startActivity(activity, new Intent(activity, NearbyActivity.class), ActivityOptionsCompat.makeBasic().toBundle());
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.nearbyFilter = new NearbyFilter(this, getLayoutInflater(), getLayoutInflater().inflate(R.layout.activity_nearby, new FrameLayout(this)));
        setContentView(this.nearbyFilter);
        ButterKnife.bind((Activity) this);
        PDMapFragment mapFragment = (PDMapFragment) getSupportFragmentManager().findFragmentByTag(PDMapFragment.TAG);
        if (mapFragment == null) {
            mapFragment = new PDMapFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, mapFragment, PDMapFragment.TAG).commit();
        }
        Bundle bundle = savedInstanceState;
        this.mPresenter = new NearbyPresenter(bundle, getSession(), this.nearbyToolbar, mapFragment, this.cards, this.searchButton, new LocationServicesConnectionManager(this), this.emptyCard, this.nearbyFilter, this.filterButton);
    }

    private void setNearbyRequestStateProvider() {
        final ObjectAnimator progressTranslationAnimator = NearbyAnimation.getTranslationYObjectAnimator(this.progress, 0.0f, (float) this.progressTranslationY);
        this.allSubscriptions.add(NearbyRequestEventBus.INSTANCE.requestStartedEvents().observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Irrelevant>() {
            public void call(Irrelevant irrelevant) {
                if (NearbyActivity.this.searchButton.isHidden()) {
                    progressTranslationAnimator.removeAllListeners();
                    NearbyActivity.this.progress.setVisibility(0);
                    progressTranslationAnimator.start();
                    return;
                }
                NearbyActivity.this.progress.setTranslationY((float) NearbyActivity.this.progressTranslationY);
                NearbyActivity.this.progress.setVisibility(0);
            }
        }, onError()));
        this.allSubscriptions.add(NearbyRequestEventBus.INSTANCE.requestCompletedEvents().observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Irrelevant>() {
            public void call(Irrelevant irrelevant) {
                progressTranslationAnimator.removeAllListeners();
                progressTranslationAnimator.addListener(new AnimatorListenerAdapter() {
                    public void onAnimationEnd(Animator animation) {
                        NearbyActivity.this.progress.setVisibility(8);
                    }
                });
                progressTranslationAnimator.reverse();
            }
        }, onError()));
    }

    @NonNull
    private Action1<Throwable> onError() {
        return new Action1<Throwable>() {
            public void call(Throwable throwable) {
                Log.e(throwable);
            }
        };
    }

    public void onResume() {
        super.onResume();
        this.mPresenter.bindView(this);
        this.allSubscriptions.add(this.nearbyToolbar.homeButtonClicks().subscribe(new Action1<Irrelevant>() {
            public void call(Irrelevant irrelevant) {
                NearbyActivity.this.finish();
            }
        }, onError()));
        this.allSubscriptions.add(CardEventBus.INSTANCE.dealDetailsClicks().observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Long>() {
            public void call(Long dealSqlId) {
                DealDetailsActivity.start(NearbyActivity.this, dealSqlId);
            }
        }, onError()));
        this.allSubscriptions.add(CardEventBus.INSTANCE.personDetailsClicks().observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Long>() {
            public void call(Long personSqlId) {
                PersonDetailActivity.startActivity(NearbyActivity.this, personSqlId);
            }
        }, onError()));
        this.allSubscriptions.add(CardEventBus.INSTANCE.organizationDetailsClicks().observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Long>() {
            public void call(Long organizationSqlId) {
                OrganizationDetailActivity.startActivity(NearbyActivity.this, organizationSqlId);
            }
        }, onError()));
        this.allSubscriptions.add(CardEventBus.INSTANCE.directionsFabClicks().observeOn(AndroidSchedulers.mainThread()).subscribe(openDirectionsApp(), onError()));
        this.allSubscriptions.add(CardEventBus.INSTANCE.directionsButtonClicks().observeOn(AndroidSchedulers.mainThread()).subscribe(openDirectionsApp(), onError()));
        setNearbyRequestStateProvider();
    }

    @NonNull
    private Action1<NearbyItem> openDirectionsApp() {
        return new Action1<NearbyItem>() {
            public void call(NearbyItem nearbyItem) {
                NearbyActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("geo:?q=" + nearbyItem.getLatitude() + Table.COMMA_SEP + nearbyItem.getLongitude())));
            }
        };
    }

    protected void onPause() {
        this.allSubscriptions.clear();
        this.mPresenter.unbindView();
        super.onPause();
    }

    protected void onSaveInstanceState(Bundle outState) {
        this.mPresenter.saveState(outState);
        super.onSaveInstanceState(outState);
    }

    public void onBackPressed() {
        if (this.nearbyFilter.isDrawerOpen()) {
            this.nearbyFilter.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }
}
