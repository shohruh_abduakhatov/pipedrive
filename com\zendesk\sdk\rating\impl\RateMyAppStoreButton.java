package com.zendesk.sdk.rating.impl;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.View.OnClickListener;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.sdk.model.settings.SafeMobileSettings;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.storage.RateMyAppStorage;
import com.zendesk.util.StringUtils;

public class RateMyAppStoreButton extends BaseRateMyAppButton {
    private static final String LOG_TAG = RateMyAppStoreButton.class.getSimpleName();
    private final String mButtonLabel;

    public RateMyAppStoreButton(@NonNull Context context) {
        this.mButtonLabel = context.getString(R.string.rate_my_app_dialog_positive_action_label);
    }

    public String getLabel() {
        return this.mButtonLabel;
    }

    public OnClickListener getOnClickListener() {
        final SafeMobileSettings safeMobileSettings = ZendeskConfig.INSTANCE.getMobileSettings();
        if (!StringUtils.isEmpty(safeMobileSettings.getRateMyAppStoreUrl())) {
            return new OnClickListener() {
                public void onClick(View view) {
                    Context context = view.getContext();
                    try {
                        String storeDetailsUrl = safeMobileSettings.getRateMyAppStoreUrl();
                        Intent storeIntent = new Intent("android.intent.action.VIEW", Uri.parse(storeDetailsUrl));
                        Logger.i(RateMyAppStoreButton.LOG_TAG, "Using store URL: " + storeDetailsUrl, new Object[0]);
                        context.startActivity(storeIntent);
                        new RateMyAppStorage(context).setRatedForCurrentVersion();
                        ZendeskConfig.INSTANCE.getTracker().rateMyAppRated();
                    } catch (Exception e) {
                        Logger.e(RateMyAppStoreButton.LOG_TAG, e.getMessage(), e, new Object[0]);
                    }
                }
            };
        }
        Logger.e(LOG_TAG, "Rate my app settings and / or store url is null, disabling button", new Object[0]);
        return null;
    }

    public int getId() {
        return R.id.rma_store_button;
    }
}
