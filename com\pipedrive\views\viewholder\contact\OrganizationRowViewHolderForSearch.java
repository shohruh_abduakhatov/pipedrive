package com.pipedrive.views.viewholder.contact;

import android.support.annotation.NonNull;
import com.pipedrive.R;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.interfaces.ContactOrgInterface;

public class OrganizationRowViewHolderForSearch extends OrganizationRowViewHolder {
    public void fill(@NonNull ContactOrgInterface contact, @NonNull SearchConstraint searchConstraint) {
        super.fill(searchConstraint, contact.getName(), getContactOrganizationName(contact));
    }

    public int getLayoutResourceId() {
        return R.layout.row_organization_search;
    }
}
