package com.pipedrive.views.common;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;
import com.pipedrive.R;

public abstract class ClickableTextViewWithUnderlineAndLabel extends TextViewLayoutWithUnderlineAndLabel<TextView> {
    public ClickableTextViewWithUnderlineAndLabel(Context context) {
        this(context, null);
    }

    public ClickableTextViewWithUnderlineAndLabel(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ClickableTextViewWithUnderlineAndLabel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypedValue backgroundValue = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.selectableItemBackgroundBorderless, backgroundValue, true);
        setBackgroundResource(backgroundValue.resourceId);
    }

    @NonNull
    protected TextView createMainView(@NonNull Context context, AttributeSet attrs, int defStyle) {
        TextView textView = new TextView(getContext());
        textView.setTextAppearance(getContext(), R.style.TextAppearance.Title);
        return textView;
    }
}
