package com.pipedrive.util.networking.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserEntity {
    private static final String JSON_PARAM_ACTIVE_FLAG = "active_flag";
    private static final String JSON_PARAM_ICON_URL = "icon_url";
    private static final String JSON_PARAM_ID = "id";
    private static final String JSON_PARAM_NAME = "name";
    @SerializedName("active_flag")
    @Expose
    public Boolean activeFlag;
    @SerializedName("icon_url")
    @Expose
    public String iconUrl;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("id")
    @Expose
    public int pipedriveId;

    public String toString() {
        return "UserEntity{pipedriveId=" + this.pipedriveId + ", name='" + this.name + '\'' + ", activeFlag=" + this.activeFlag + ", iconUrl='" + this.iconUrl + '\'' + '}';
    }
}
