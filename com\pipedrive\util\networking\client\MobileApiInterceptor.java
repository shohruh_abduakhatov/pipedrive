package com.pipedrive.util.networking.client;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.io.IOException;
import okhttp3.Interceptor.Chain;
import okhttp3.Request;
import okhttp3.Response;

public class MobileApiInterceptor extends HeaderInterceptor {
    public /* bridge */ /* synthetic */ Response intercept(Chain chain) throws IOException {
        return super.intercept(chain);
    }

    @NonNull
    String getHeaderName() {
        return "X-Pipedrive-Mobile-Api";
    }

    @Nullable
    String getHeaderValue(@NonNull Request request) {
        return "v1";
    }
}
