package com.pipedrive.views.viewholder.product;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class ProductViewHolder_ViewBinding implements Unbinder {
    private ProductViewHolder target;

    @UiThread
    public ProductViewHolder_ViewBinding(ProductViewHolder target, View source) {
        this.target = target;
        target.titleView = (TextView) Utils.findRequiredViewAsType(source, R.id.title, "field 'titleView'", TextView.class);
        target.priceView = (TextView) Utils.findRequiredViewAsType(source, R.id.price, "field 'priceView'", TextView.class);
    }

    @CallSuper
    public void unbind() {
        ProductViewHolder target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.titleView = null;
        target.priceView = null;
    }
}
