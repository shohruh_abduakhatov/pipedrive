package rx;

class Completable$30 implements CompletableSubscriber {
    final /* synthetic */ Completable this$0;
    final /* synthetic */ Subscriber val$s;

    Completable$30(Completable completable, Subscriber subscriber) {
        this.this$0 = completable;
        this.val$s = subscriber;
    }

    public void onCompleted() {
        this.val$s.onCompleted();
    }

    public void onError(Throwable e) {
        this.val$s.onError(e);
    }

    public void onSubscribe(Subscription d) {
        this.val$s.add(d);
    }
}
