package com.pipedrive.nearby.filter.filterlist;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class NearbyFilterListViewHolder_ViewBinding implements Unbinder {
    private NearbyFilterListViewHolder target;

    @UiThread
    public NearbyFilterListViewHolder_ViewBinding(NearbyFilterListViewHolder target, View source) {
        this.target = target;
        target.recyclerView = (RecyclerView) Utils.findRequiredViewAsType(source, R.id.filterItemRecyclerView, "field 'recyclerView'", RecyclerView.class);
        target.label = (TextView) Utils.findRequiredViewAsType(source, R.id.filterLabel, "field 'label'", TextView.class);
    }

    @CallSuper
    public void unbind() {
        NearbyFilterListViewHolder target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.recyclerView = null;
        target.label = null;
    }
}
