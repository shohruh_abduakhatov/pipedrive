package com.pipedrive.navigator.buttons;

import android.support.annotation.IdRes;
import android.support.annotation.MainThread;

@MainThread
public interface NavigatorMenuItem {
    @IdRes
    int getMenuItemId();

    boolean isEnabled();

    void onClick();
}
