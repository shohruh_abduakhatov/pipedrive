package com.pipedrive.model;

import com.pipedrive.model.pagination.Pagination;

public class PageOfDeals extends Pagination {
    public boolean isSuccessful;

    public String toString() {
        return "PageOfDeals{, isSuccessful=" + this.isSuccessful + ", super.toString=" + super.toString() + "}";
    }
}
