package com.google.android.gms.auth.api.credentials.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.Auth.AuthCredentialsOptions;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.CredentialRequest;
import com.google.android.gms.auth.api.credentials.CredentialRequestResult;
import com.google.android.gms.auth.api.credentials.CredentialsApi;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.credentials.PasswordSpecification;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzqo.zzb;

public final class zze implements CredentialsApi {

    private static class zza extends zza {
        private zzb<Status> iU;

        zza(zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status) {
            this.iU = com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status;
        }

        public void zzh(Status status) {
            this.iU.setResult(status);
        }
    }

    private PasswordSpecification zza(GoogleApiClient googleApiClient) {
        AuthCredentialsOptions zzaim = ((zzg) googleApiClient.zza(Auth.hX)).zzaim();
        return (zzaim == null || zzaim.zzaid() == null) ? PasswordSpecification.iG : zzaim.zzaid();
    }

    public PendingResult<Status> delete(GoogleApiClient googleApiClient, final Credential credential) {
        return googleApiClient.zzb(new zzf<Status>(this, googleApiClient) {
            final /* synthetic */ zze iR;

            protected void zza(Context context, zzk com_google_android_gms_auth_api_credentials_internal_zzk) throws RemoteException {
                com_google_android_gms_auth_api_credentials_internal_zzk.zza(new zza(this), new DeleteRequest(credential));
            }

            protected Status zzb(Status status) {
                return status;
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzb(status);
            }
        });
    }

    public PendingResult<Status> disableAutoSignIn(GoogleApiClient googleApiClient) {
        return googleApiClient.zzb(new zzf<Status>(this, googleApiClient) {
            final /* synthetic */ zze iR;

            protected void zza(Context context, zzk com_google_android_gms_auth_api_credentials_internal_zzk) throws RemoteException {
                com_google_android_gms_auth_api_credentials_internal_zzk.zza(new zza(this));
            }

            protected Status zzb(Status status) {
                return status;
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzb(status);
            }
        });
    }

    public PendingIntent getHintPickerIntent(GoogleApiClient googleApiClient, HintRequest hintRequest) {
        zzaa.zzb((Object) googleApiClient, (Object) "client must not be null");
        zzaa.zzb((Object) hintRequest, (Object) "request must not be null");
        zzaa.zzb(googleApiClient.zza(Auth.CREDENTIALS_API), (Object) "Auth.CREDENTIALS_API must be added to GoogleApiClient to use this API");
        return PendingIntent.getActivity(googleApiClient.getContext(), 2000, zzc.zza(googleApiClient.getContext(), hintRequest, zza(googleApiClient)), 268435456);
    }

    public PendingResult<CredentialRequestResult> request(GoogleApiClient googleApiClient, final CredentialRequest credentialRequest) {
        return googleApiClient.zza(new zzf<CredentialRequestResult>(this, googleApiClient) {
            final /* synthetic */ zze iR;

            protected void zza(Context context, zzk com_google_android_gms_auth_api_credentials_internal_zzk) throws RemoteException {
                com_google_android_gms_auth_api_credentials_internal_zzk.zza(new zza(this) {
                    final /* synthetic */ AnonymousClass1 iS;

                    {
                        this.iS = r1;
                    }

                    public void zza(Status status, Credential credential) {
                        this.iS.zzc(new zzd(status, credential));
                    }

                    public void zzh(Status status) {
                        this.iS.zzc(zzd.zzi(status));
                    }
                }, credentialRequest);
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzj(status);
            }

            protected CredentialRequestResult zzj(Status status) {
                return zzd.zzi(status);
            }
        });
    }

    public PendingResult<Status> save(GoogleApiClient googleApiClient, final Credential credential) {
        return googleApiClient.zzb(new zzf<Status>(this, googleApiClient) {
            final /* synthetic */ zze iR;

            protected void zza(Context context, zzk com_google_android_gms_auth_api_credentials_internal_zzk) throws RemoteException {
                com_google_android_gms_auth_api_credentials_internal_zzk.zza(new zza(this), new SaveRequest(credential));
            }

            protected Status zzb(Status status) {
                return status;
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzb(status);
            }
        });
    }
}
