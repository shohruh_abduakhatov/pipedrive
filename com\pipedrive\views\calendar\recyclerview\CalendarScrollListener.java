package com.pipedrive.views.calendar.recyclerview;

import java.util.Calendar;

public interface CalendarScrollListener {
    void onScrollToDate(Calendar calendar);

    void setupForDate(Calendar calendar);
}
