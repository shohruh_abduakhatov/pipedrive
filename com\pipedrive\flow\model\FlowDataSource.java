package com.pipedrive.flow.model;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.datasource.SQLTransactionManager;
import com.pipedrive.util.CursorHelper;

public class FlowDataSource extends SQLTransactionManager {
    private static final String COLUMNS_ACTIVITIES = "activities._id AS _id,activities.ac_pd_id AS flow_item_pipedrive_id,'activity' AS flow_item_type,activities.ac_start AS flow_item_timestamp,activities.ac_subject AS flow_item_title,deals.d_title AS flow_item_deal_name,persons.c_name AS flow_item_person_name,organizations.name AS flow_item_org_name,activities.ac_done AS flow_item_extra_boolean,activities.ac_start_type AS flow_item_extra_number,activities.ac_type AS flow_item_extra_string,activities.ac_note AS flow_item_extra_string_2";
    private static final String COLUMNS_EMAILS = "email_messages._id AS _id,email_messages.email_messages_pipedrive_id AS flow_item_pipedrive_id,'emailMessage' AS flow_item_type,email_messages.email_messages_message_time AS flow_item_timestamp,email_messages.email_messages_subject AS flow_item_title,NULL AS flow_item_deal_name,email_messages.email_messages_senders_display_string AS flow_item_person_name,email_messages.email_messages_recepients_display_string AS flow_item_org_name,NULL AS flow_item_extra_boolean,email_messages.email_messages_attachment_count AS flow_item_extra_number,email_messages.email_messages_summary AS flow_item_extra_string,NULL AS flow_item_extra_string_2";
    private static final String COLUMNS_FILES = "files._id AS _id,files.files_id AS flow_item_pipedrive_id,'file' AS flow_item_type,files.files_add_time AS flow_item_timestamp,files.files_name AS flow_item_title,deals.d_title AS flow_item_deal_name,persons.c_name AS flow_item_person_name,organizations.name AS flow_item_org_name,NULL AS flow_item_extra_boolean,files.files_upload_status AS flow_item_extra_number,NULL AS flow_item_extra_string,NULL AS flow_item_extra_string_2";
    private static final String COLUMNS_NOTES = "notes._id AS _id,notes.n_pd_id AS flow_item_pipedrive_id,'note' AS flow_item_type,notes.n_add_time AS flow_item_timestamp,notes.n_content AS flow_item_title,deals.d_title AS flow_item_deal_name,persons.c_name AS flow_item_person_name,organizations.name AS flow_item_org_name,NULL AS flow_item_extra_boolean,NULL AS flow_item_extra_number,NULL AS flow_item_extra_string,NULL AS flow_item_extra_string_2";
    private static final String[] COLUMNS_PLANNED = new String[]{"activities._id AS _id", "activities.ac_pd_id AS flow_item_pipedrive_id", "'activity' AS flow_item_type", "activities.ac_start AS flow_item_timestamp", "activities.ac_subject AS flow_item_title", "deals.d_title AS flow_item_deal_name", "persons.c_name AS flow_item_person_name", "organizations.name AS flow_item_org_name", "activities.ac_done AS flow_item_extra_boolean", "activities.ac_start_type AS flow_item_extra_number", "activities.ac_type AS flow_item_extra_string", "activities.ac_note AS flow_item_extra_string_2"};
    private static final String COLUMN_FLOW_ITEM_DEAL_NAME = "flow_item_deal_name";
    private static final String COLUMN_FLOW_ITEM_EXTRA_BOOLEAN = "flow_item_extra_boolean";
    private static final String COLUMN_FLOW_ITEM_EXTRA_NUMBER = "flow_item_extra_number";
    private static final String COLUMN_FLOW_ITEM_EXTRA_STRING = "flow_item_extra_string";
    private static final String COLUMN_FLOW_ITEM_EXTRA_STRING_2 = "flow_item_extra_string_2";
    private static final String COLUMN_FLOW_ITEM_ID = "_id";
    private static final String COLUMN_FLOW_ITEM_ORG_NAME = "flow_item_org_name";
    private static final String COLUMN_FLOW_ITEM_PERSON_NAME = "flow_item_person_name";
    private static final String COLUMN_FLOW_ITEM_PIPEDRIVE_ID = "flow_item_pipedrive_id";
    private static final String COLUMN_FLOW_ITEM_TIMESTAMP = "flow_item_timestamp";
    private static final String COLUMN_FLOW_ITEM_TITLE = "flow_item_title";
    private static final String COLUMN_FLOW_ITEM_TYPE = "flow_item_type";
    private static final String SELECT_FILES = "SELECT files._id AS _id,files.files_id AS flow_item_pipedrive_id,'file' AS flow_item_type,files.files_add_time AS flow_item_timestamp,files.files_name AS flow_item_title,deals.d_title AS flow_item_deal_name,persons.c_name AS flow_item_person_name,organizations.name AS flow_item_org_name,NULL AS flow_item_extra_boolean,files.files_upload_status AS flow_item_extra_number,NULL AS flow_item_extra_string,NULL AS flow_item_extra_string_2 FROM files LEFT JOIN deals ON deals._id=files.files_d_pd_id LEFT JOIN persons ON persons._id=files.files_person_id LEFT JOIN organizations ON organizations._id=files.files_org_id WHERE files.files_active=1";
    private static final String SELECT_NOTES = "SELECT notes._id AS _id,notes.n_pd_id AS flow_item_pipedrive_id,'note' AS flow_item_type,notes.n_add_time AS flow_item_timestamp,notes.n_content AS flow_item_title,deals.d_title AS flow_item_deal_name,persons.c_name AS flow_item_person_name,organizations.name AS flow_item_org_name,NULL AS flow_item_extra_boolean,NULL AS flow_item_extra_number,NULL AS flow_item_extra_string,NULL AS flow_item_extra_string_2 FROM notes LEFT JOIN deals ON deals._id=notes.n_deal_sql_id LEFT JOIN persons ON persons._id=notes.n_person_sql_id LEFT JOIN organizations ON organizations._id=notes.n_org_sql_id WHERE notes.n_is_active=1";
    private static final String SELECT_PAST_ACTIVITIES = "SELECT activities._id AS _id,activities.ac_pd_id AS flow_item_pipedrive_id,'activity' AS flow_item_type,activities.ac_start AS flow_item_timestamp,activities.ac_subject AS flow_item_title,deals.d_title AS flow_item_deal_name,persons.c_name AS flow_item_person_name,organizations.name AS flow_item_org_name,activities.ac_done AS flow_item_extra_boolean,activities.ac_start_type AS flow_item_extra_number,activities.ac_type AS flow_item_extra_string,activities.ac_note AS flow_item_extra_string_2 FROM activities LEFT JOIN deals ON deals._id=activities.ac_deal_id_sql LEFT JOIN persons ON persons._id=activities.ac_person_id_sql LEFT JOIN organizations ON organizations._id=activities.ac_org_id_sql WHERE activities.ac_is_active=1 AND activities.ac_done=1";
    private static final String TABLES_EMAIL_MESSAGES_JOIN_THREADS = "email_messages LEFT JOIN email_threads ON email_messages.email_messages_thread_id=email_threads._id";
    private static final String TABLE_ACTIVITIES_JOIN_DEALS_PERSONS_AND_ORGS = "activities LEFT JOIN deals ON deals._id=activities.ac_deal_id_sql LEFT JOIN persons ON persons._id=activities.ac_person_id_sql LEFT JOIN organizations ON organizations._id=activities.ac_org_id_sql";
    private static final String TABLE_FILES_JOIN_DEALS_PERSONS_AND_ORGS = "files LEFT JOIN deals ON deals._id=files.files_d_pd_id LEFT JOIN persons ON persons._id=files.files_person_id LEFT JOIN organizations ON organizations._id=files.files_org_id";
    private static final String TABLE_NOTES_JOIN_DEALS_PERSONS_AND_ORGS = "notes LEFT JOIN deals ON deals._id=notes.n_deal_sql_id LEFT JOIN persons ON persons._id=notes.n_person_sql_id LEFT JOIN organizations ON organizations._id=notes.n_org_sql_id";
    private static final String WHERE_ACTIVITY_IS_ACTIVE_AND_DONE = "activities.ac_is_active=1 AND activities.ac_done=1";
    private static final String WHERE_ACTIVITY_IS_ACTIVE_AND_DONE_TEMPLATE = "activities.ac_is_active=? AND activities.ac_done=?";
    private static final String WHERE_EMAIL_IS_ACTIVE_AND_NOT_DRAFT = "email_messages.email_messages_active=1 AND email_messages.email_messages_draft=0";
    private static final String WHERE_FILE_IS_ACTIVE = "files.files_active=1";
    private static final String WHERE_NOTE_IS_ACTIVE = "notes.n_is_active=1";

    public FlowDataSource(@NonNull Session session) {
        super(session.getDatabase());
    }

    public FlowItemEntity deflateCursor(@NonNull Cursor cursor) {
        return new FlowItemEntity(CursorHelper.getLong(cursor, "_id"), CursorHelper.getLong(cursor, COLUMN_FLOW_ITEM_PIPEDRIVE_ID), CursorHelper.getString(cursor, COLUMN_FLOW_ITEM_TYPE), CursorHelper.getPipedriveDateTimeFromColumnHeldInSeconds(cursor, COLUMN_FLOW_ITEM_TIMESTAMP), CursorHelper.getString(cursor, COLUMN_FLOW_ITEM_TITLE), CursorHelper.getString(cursor, COLUMN_FLOW_ITEM_DEAL_NAME), CursorHelper.getString(cursor, COLUMN_FLOW_ITEM_PERSON_NAME), CursorHelper.getString(cursor, COLUMN_FLOW_ITEM_ORG_NAME), CursorHelper.getBoolean(cursor, COLUMN_FLOW_ITEM_EXTRA_BOOLEAN), CursorHelper.getLong(cursor, COLUMN_FLOW_ITEM_EXTRA_NUMBER), CursorHelper.getString(cursor, COLUMN_FLOW_ITEM_EXTRA_STRING), CursorHelper.getString(cursor, COLUMN_FLOW_ITEM_EXTRA_STRING_2));
    }

    @NonNull
    private String getDealEmailsQuery(@NonNull Long dealId) {
        return "SELECT email_messages._id AS _id,email_messages.email_messages_pipedrive_id AS flow_item_pipedrive_id,'emailMessage' AS flow_item_type,email_messages.email_messages_message_time AS flow_item_timestamp,email_messages.email_messages_subject AS flow_item_title,NULL AS flow_item_deal_name,email_messages.email_messages_senders_display_string AS flow_item_person_name,email_messages.email_messages_recepients_display_string AS flow_item_org_name,NULL AS flow_item_extra_boolean,email_messages.email_messages_attachment_count AS flow_item_extra_number,email_messages.email_messages_summary AS flow_item_extra_string,NULL AS flow_item_extra_string_2 FROM email_messages LEFT JOIN email_threads ON email_messages.email_messages_thread_id=email_threads._id WHERE email_messages.email_messages_active=1 AND email_messages.email_messages_draft=0 AND email_threads.email_threads_deal_id=" + dealId;
    }

    @NonNull
    private String getPersonEmailsQuery(@NonNull Long personSqlId) {
        return "SELECT email_messages._id AS _id,email_messages.email_messages_pipedrive_id AS flow_item_pipedrive_id,'emailMessage' AS flow_item_type,email_messages.email_messages_message_time AS flow_item_timestamp,email_messages.email_messages_subject AS flow_item_title,NULL AS flow_item_deal_name,email_messages.email_messages_senders_display_string AS flow_item_person_name,email_messages.email_messages_recepients_display_string AS flow_item_org_name,NULL AS flow_item_extra_boolean,email_messages.email_messages_attachment_count AS flow_item_extra_number,email_messages.email_messages_summary AS flow_item_extra_string,NULL AS flow_item_extra_string_2 FROM email_messages LEFT JOIN email_threads ON email_messages.email_messages_thread_id=email_threads._id LEFT JOIN email_participants_to_messages ON  email_messages._id = email_participants_to_messages.email_participants_to_messages_email_message_id LEFT JOIN email_participants ON email_participants_to_messages.email_participants_to_messages_participant_id = email_participants._id WHERE email_messages.email_messages_active=1 AND email_messages.email_messages_draft=0 AND email_participants.email_participants_person_id=" + personSqlId;
    }

    @NonNull
    private String getOrgEmailsQuery(@NonNull Long orgId) {
        return "SELECT email_messages._id AS _id,email_messages.email_messages_pipedrive_id AS flow_item_pipedrive_id,'emailMessage' AS flow_item_type,email_messages.email_messages_message_time AS flow_item_timestamp,email_messages.email_messages_subject AS flow_item_title,NULL AS flow_item_deal_name,email_messages.email_messages_senders_display_string AS flow_item_person_name,email_messages.email_messages_recepients_display_string AS flow_item_org_name,NULL AS flow_item_extra_boolean,email_messages.email_messages_attachment_count AS flow_item_extra_number,email_messages.email_messages_summary AS flow_item_extra_string,NULL AS flow_item_extra_string_2 FROM email_messages LEFT JOIN email_threads ON email_messages.email_messages_thread_id=email_threads._id LEFT JOIN email_threads_to_orgs ON email_messages.email_messages_thread_id=email_threads_to_orgs.email_threads_to_orgs_thread_id WHERE email_messages.email_messages_active=1 AND email_messages.email_messages_draft=0 AND email_threads_to_orgs.email_threads_to_orgs_org_id=" + orgId;
    }

    @NonNull
    public Cursor getDealPastFlowCursor(@NonNull Long dealId) {
        String query = "SELECT activities._id AS _id,activities.ac_pd_id AS flow_item_pipedrive_id,'activity' AS flow_item_type,activities.ac_start AS flow_item_timestamp,activities.ac_subject AS flow_item_title,deals.d_title AS flow_item_deal_name,persons.c_name AS flow_item_person_name,organizations.name AS flow_item_org_name,activities.ac_done AS flow_item_extra_boolean,activities.ac_start_type AS flow_item_extra_number,activities.ac_type AS flow_item_extra_string,activities.ac_note AS flow_item_extra_string_2 FROM activities LEFT JOIN deals ON deals._id=activities.ac_deal_id_sql LEFT JOIN persons ON persons._id=activities.ac_person_id_sql LEFT JOIN organizations ON organizations._id=activities.ac_org_id_sql WHERE activities.ac_is_active=1 AND activities.ac_done=1 AND activities.ac_deal_id_sql=" + dealId + " UNION " + SELECT_NOTES + " AND " + "notes" + '.' + PipeSQLiteHelper.COLUMN_NOTES_DEAL_SQL_ID + '=' + dealId + " UNION " + SELECT_FILES + " AND " + "files" + '.' + PipeSQLiteHelper.COLUMN_FILES_DEAL_ID + '=' + dealId + " UNION " + getDealEmailsQuery(dealId) + " ORDER BY " + COLUMN_FLOW_ITEM_TIMESTAMP + " DESC";
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        return !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.rawQuery(query, null) : SQLiteInstrumentation.rawQuery(transactionalDBConnection, query, null);
    }

    @NonNull
    public Cursor getPersonPastFlowCursor(@NonNull Long personSqlId) {
        String query = "SELECT activities._id AS _id,activities.ac_pd_id AS flow_item_pipedrive_id,'activity' AS flow_item_type,activities.ac_start AS flow_item_timestamp,activities.ac_subject AS flow_item_title,deals.d_title AS flow_item_deal_name,persons.c_name AS flow_item_person_name,organizations.name AS flow_item_org_name,activities.ac_done AS flow_item_extra_boolean,activities.ac_start_type AS flow_item_extra_number,activities.ac_type AS flow_item_extra_string,activities.ac_note AS flow_item_extra_string_2 FROM activities LEFT JOIN deals ON deals._id=activities.ac_deal_id_sql LEFT JOIN persons ON persons._id=activities.ac_person_id_sql LEFT JOIN organizations ON organizations._id=activities.ac_org_id_sql WHERE activities.ac_is_active=1 AND activities.ac_done=1 AND activities.ac_person_id_sql=" + personSqlId + " UNION " + SELECT_NOTES + " AND " + "notes" + '.' + PipeSQLiteHelper.COLUMN_NOTES_PERSON_SQL_ID + '=' + personSqlId + " UNION " + SELECT_FILES + " AND " + "files" + '.' + PipeSQLiteHelper.COLUMN_FILES_PERSON_ID + '=' + personSqlId + " UNION " + getPersonEmailsQuery(personSqlId) + " ORDER BY " + COLUMN_FLOW_ITEM_TIMESTAMP + " DESC";
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        return !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.rawQuery(query, null) : SQLiteInstrumentation.rawQuery(transactionalDBConnection, query, null);
    }

    @NonNull
    public Cursor getOrgPastFlowCursor(@NonNull Long organizationId) {
        String query = "SELECT activities._id AS _id,activities.ac_pd_id AS flow_item_pipedrive_id,'activity' AS flow_item_type,activities.ac_start AS flow_item_timestamp,activities.ac_subject AS flow_item_title,deals.d_title AS flow_item_deal_name,persons.c_name AS flow_item_person_name,organizations.name AS flow_item_org_name,activities.ac_done AS flow_item_extra_boolean,activities.ac_start_type AS flow_item_extra_number,activities.ac_type AS flow_item_extra_string,activities.ac_note AS flow_item_extra_string_2 FROM activities LEFT JOIN deals ON deals._id=activities.ac_deal_id_sql LEFT JOIN persons ON persons._id=activities.ac_person_id_sql LEFT JOIN organizations ON organizations._id=activities.ac_org_id_sql WHERE activities.ac_is_active=1 AND activities.ac_done=1 AND activities.ac_org_id_sql=" + organizationId + " UNION " + SELECT_NOTES + " AND " + "notes" + '.' + PipeSQLiteHelper.COLUMN_NOTES_ORGANIZATION_SQL_ID + '=' + organizationId + " UNION " + SELECT_FILES + " AND " + "files" + '.' + PipeSQLiteHelper.COLUMN_FILES_ORG_ID + '=' + organizationId + " UNION " + getOrgEmailsQuery(organizationId) + " ORDER BY " + COLUMN_FLOW_ITEM_TIMESTAMP + " DESC";
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        return !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.rawQuery(query, null) : SQLiteInstrumentation.rawQuery(transactionalDBConnection, query, null);
    }

    @NonNull
    public Cursor getDealPlannedFlowCursor(@NonNull Long dealId) {
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String str = TABLE_ACTIVITIES_JOIN_DEALS_PERSONS_AND_ORGS;
        String[] strArr = COLUMNS_PLANNED;
        String str2 = "activities.ac_is_active=? AND activities.ac_done=? AND activities.ac_deal_id_sql=?";
        String[] strArr2 = new String[]{String.valueOf(1), String.valueOf(0), dealId.toString()};
        String str3 = COLUMN_FLOW_ITEM_TIMESTAMP;
        return !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.query(str, strArr, str2, strArr2, null, null, str3) : SQLiteInstrumentation.query(transactionalDBConnection, str, strArr, str2, strArr2, null, null, str3);
    }

    @NonNull
    public Cursor getPersonPlannedFlowCursor(@NonNull Long personId) {
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String str = TABLE_ACTIVITIES_JOIN_DEALS_PERSONS_AND_ORGS;
        String[] strArr = COLUMNS_PLANNED;
        String str2 = "activities.ac_is_active=? AND activities.ac_done=? AND activities.ac_person_id_sql=?";
        String[] strArr2 = new String[]{String.valueOf(1), String.valueOf(0), personId.toString()};
        String str3 = COLUMN_FLOW_ITEM_TIMESTAMP;
        return !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.query(str, strArr, str2, strArr2, null, null, str3) : SQLiteInstrumentation.query(transactionalDBConnection, str, strArr, str2, strArr2, null, null, str3);
    }

    @NonNull
    public Cursor getOrgPlannedFlowCursor(@NonNull Long organizationId) {
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String str = TABLE_ACTIVITIES_JOIN_DEALS_PERSONS_AND_ORGS;
        String[] strArr = COLUMNS_PLANNED;
        String str2 = "activities.ac_is_active=? AND activities.ac_done=? AND activities.ac_org_id_sql=?";
        String[] strArr2 = new String[]{String.valueOf(1), String.valueOf(0), organizationId.toString()};
        String str3 = COLUMN_FLOW_ITEM_TIMESTAMP;
        return !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.query(str, strArr, str2, strArr2, null, null, str3) : SQLiteInstrumentation.query(transactionalDBConnection, str, strArr, str2, strArr2, null, null, str3);
    }

    @Nullable
    public String getTypeString(@NonNull Cursor cursor) {
        return CursorHelper.getString(cursor, COLUMN_FLOW_ITEM_TYPE);
    }
}
