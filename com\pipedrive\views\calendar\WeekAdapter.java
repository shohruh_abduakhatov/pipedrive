package com.pipedrive.views.calendar;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.view.ViewGroup;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class WeekAdapter extends CalendarViewAdapter<WeekViewHolder> {
    @Nullable
    private OnDateChangedInternalListener internalListener;
    @NonNull
    final List<Calendar> items;
    private final Locale locale;
    @Nullable
    private Calendar selectedDate;

    public WeekAdapter(@NonNull Locale locale, @NonNull List<Calendar> items, @Nullable Calendar selectedDate) {
        this.locale = locale;
        this.items = items;
        this.selectedDate = selectedDate;
    }

    public void setInternalListener(@Nullable OnDateChangedInternalListener internalListener) {
        this.internalListener = internalListener;
    }

    public WeekViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        WeekView weekView = new WeekView(parent.getContext());
        weekView.setLayoutParams(new LayoutParams(parent.getMeasuredWidth(), -2));
        weekView.setOnDateChangedInternalListener(this.internalListener);
        return new WeekViewHolder(weekView);
    }

    public void onBindViewHolder(WeekViewHolder holder, int position) {
        Calendar item = (Calendar) this.items.get(position);
        WeekViewHolder weekViewHolder = holder;
        weekViewHolder.bind(this.locale, item.get(1), item.get(2), this.selectedDate, item.get(4), item.get(3));
    }

    public int getItemCount() {
        return this.items.size();
    }

    public void setSelectedDate(@Nullable Calendar selectedDate) {
        this.selectedDate = selectedDate;
        notifyDataSetChanged();
    }

    public Calendar getItem(int position) {
        return (Calendar) this.items.get(position);
    }
}
