package com.zendesk.service;

import com.zendesk.util.StringUtils;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import okhttp3.Headers;
import retrofit2.Response;

public class RetrofitErrorResponse implements ErrorResponse {
    private static final String LOG_TAG = "RetrofitErrorResponse";
    private Response mResponse;
    private Throwable mThrowable;

    public static RetrofitErrorResponse throwable(Throwable throwable) {
        return new RetrofitErrorResponse(throwable);
    }

    private RetrofitErrorResponse(Throwable throwable) {
        this.mThrowable = throwable;
    }

    public static RetrofitErrorResponse response(Response response) {
        return new RetrofitErrorResponse(response);
    }

    private RetrofitErrorResponse(Response response) {
        this.mResponse = response;
    }

    public boolean isNetworkError() {
        return this.mThrowable != null && (this.mThrowable instanceof IOException);
    }

    public boolean isConversionError() {
        return isNetworkError();
    }

    public boolean isHTTPError() {
        return (this.mThrowable != null || this.mResponse == null || this.mResponse.isSuccessful()) ? false : true;
    }

    public String getReason() {
        if (this.mThrowable != null) {
            return this.mThrowable.getMessage();
        }
        StringBuilder reasonBuilder = new StringBuilder();
        if (this.mResponse != null) {
            if (StringUtils.hasLength(this.mResponse.message())) {
                reasonBuilder.append(this.mResponse.message());
            } else {
                reasonBuilder.append(this.mResponse.code());
            }
        }
        return reasonBuilder.toString();
    }

    public int getStatus() {
        if (this.mResponse != null) {
            return this.mResponse.code();
        }
        return -1;
    }

    public String getUrl() {
        return (this.mResponse == null || this.mResponse.raw().request() == null || this.mResponse.raw().request().url() == null) ? "" : this.mResponse.raw().request().url().toString();
    }

    public String getResponseBody() {
        String responseBody = "";
        if (this.mResponse == null || this.mResponse.errorBody() == null) {
            return responseBody;
        }
        try {
            return new String(this.mResponse.errorBody().bytes(), HttpRequest.CHARSET_UTF8);
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError("UTF-8 must be supported");
        } catch (IOException e2) {
            return responseBody;
        }
    }

    public String getResponseBodyType() {
        if (this.mResponse == null || this.mResponse.errorBody() == null) {
            return "";
        }
        return this.mResponse.errorBody().contentType().toString();
    }

    public List<Header> getResponseHeaders() {
        if (this.mThrowable != null) {
            return new ArrayList();
        }
        List<Header> headers = new ArrayList();
        if (this.mResponse == null || this.mResponse.headers() == null || this.mResponse.headers().size() <= 0) {
            return headers;
        }
        Headers responseHeaders = this.mResponse.headers();
        for (String headerName : responseHeaders.names()) {
            headers.add(new Header(headerName, responseHeaders.get(headerName)));
        }
        return headers;
    }
}
