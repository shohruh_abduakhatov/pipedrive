package com.zendesk.sdk.model.push;

import android.support.annotation.Nullable;
import com.google.gson.annotations.SerializedName;

public class PushRegistrationResponseWrapper {
    @SerializedName("push_notification_device")
    private PushRegistrationResponse registrationResponse;

    @Nullable
    public PushRegistrationResponse getRegistrationResponse() {
        return this.registrationResponse;
    }
}
