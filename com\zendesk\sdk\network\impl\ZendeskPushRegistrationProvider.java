package com.zendesk.sdk.network.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.SdkConfiguration;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.model.access.AuthenticationType;
import com.zendesk.sdk.model.push.AnonymousPushRegistrationRequest;
import com.zendesk.sdk.model.push.JwtPushRegistrationRequest;
import com.zendesk.sdk.model.push.PushRegistrationRequest;
import com.zendesk.sdk.model.push.PushRegistrationRequestWrapper;
import com.zendesk.sdk.model.push.PushRegistrationResponse;
import com.zendesk.sdk.model.push.PushRegistrationResponseWrapper;
import com.zendesk.sdk.network.BaseProvider;
import com.zendesk.sdk.network.PushRegistrationProvider;
import com.zendesk.sdk.storage.IdentityStorage;
import com.zendesk.sdk.storage.PushRegistrationResponseStorage;
import com.zendesk.service.ErrorResponseAdapter;
import com.zendesk.service.ZendeskCallback;
import com.zendesk.util.LocaleUtil;
import java.util.Locale;

class ZendeskPushRegistrationProvider implements PushRegistrationProvider {
    private static final String LOG_TAG = "PushRegistrationProvide";
    private final BaseProvider baseProvider;
    private final IdentityStorage identityStorage;
    private final PushRegistrationResponseStorage pushIdStorage;
    private final ZendeskPushRegistrationService pushService;

    enum TokenType {
        Identifier(null),
        UrbanAirshipChannelId("urban_airship_channel_id");
        
        final String name;

        private TokenType(String name) {
            this.name = name;
        }

        String getName() {
            return this.name;
        }
    }

    ZendeskPushRegistrationProvider(BaseProvider baseProvider, ZendeskPushRegistrationService pushService, IdentityStorage identityStorage, PushRegistrationResponseStorage pushIdStorage) {
        this.baseProvider = baseProvider;
        this.pushService = pushService;
        this.identityStorage = identityStorage;
        this.pushIdStorage = pushIdStorage;
    }

    public void registerDeviceWithIdentifier(@NonNull String identifier, @NonNull Locale locale, @Nullable ZendeskCallback<PushRegistrationResponse> callback) {
        if (!checkForStoredPushRegistration(identifier, callback)) {
            final ZendeskCallback<PushRegistrationResponse> zendeskCallback = callback;
            final String str = identifier;
            final Locale locale2 = locale;
            this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
                public void onSuccess(SdkConfiguration config) {
                    AuthenticationType authenticationType = config.getMobileSettings().getAuthenticationType();
                    if (authenticationType != null) {
                        ZendeskPushRegistrationProvider.this.internalRegister(config.getBearerAuthorizationHeader(), ZendeskPushRegistrationProvider.this.getPushRegistrationRequest(str, locale2, authenticationType, TokenType.Identifier), zendeskCallback);
                    } else if (zendeskCallback != null) {
                        zendeskCallback.onError(new ErrorResponseAdapter("Authentication type is null."));
                    }
                }
            });
        }
    }

    public void registerDeviceWithUAChannelId(@NonNull String urbanAirshipChannelId, @NonNull Locale locale, @Nullable ZendeskCallback<PushRegistrationResponse> callback) {
        if (!checkForStoredPushRegistration(urbanAirshipChannelId, callback)) {
            final ZendeskCallback<PushRegistrationResponse> zendeskCallback = callback;
            final String str = urbanAirshipChannelId;
            final Locale locale2 = locale;
            this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
                public void onSuccess(SdkConfiguration config) {
                    AuthenticationType authenticationType = config.getMobileSettings().getAuthenticationType();
                    if (authenticationType != null) {
                        ZendeskPushRegistrationProvider.this.internalRegister(config.getBearerAuthorizationHeader(), ZendeskPushRegistrationProvider.this.getPushRegistrationRequest(str, locale2, authenticationType, TokenType.UrbanAirshipChannelId), zendeskCallback);
                    } else if (zendeskCallback != null) {
                        zendeskCallback.onError(new ErrorResponseAdapter("Authentication type is null."));
                    }
                }
            });
        }
    }

    public void unregisterDevice(@NonNull final String identifier, @Nullable final ZendeskCallback<Void> callback) {
        this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
            public void onSuccess(SdkConfiguration config) {
                ZendeskPushRegistrationProvider.this.pushService.unregisterDevice(config.getBearerAuthorizationHeader(), identifier, callback);
                ZendeskPushRegistrationProvider.this.pushIdStorage.removePushRegistrationResponse();
            }
        });
    }

    private boolean checkForStoredPushRegistration(@NonNull String identifier, @Nullable ZendeskCallback<PushRegistrationResponse> callback) {
        if (!this.pushIdStorage.hasStoredPushRegistrationResponse()) {
            return false;
        }
        PushRegistrationResponse storedResponse = this.pushIdStorage.getPushRegistrationResponse();
        if (storedResponse == null || !identifier.equals(storedResponse.getIdentifier())) {
            Logger.d(LOG_TAG, "Removing stored push registration response because a new one has been provided.", new Object[0]);
            this.pushIdStorage.removePushRegistrationResponse();
            return false;
        }
        Logger.d(LOG_TAG, "Skipping registration because device is already registered with this ID.", new Object[0]);
        if (callback != null) {
            callback.onSuccess(storedResponse);
        }
        return true;
    }

    void internalRegister(@NonNull String header, @NonNull PushRegistrationRequest pushRegistrationRequest, @Nullable final ZendeskCallback<PushRegistrationResponse> callback) {
        PushRegistrationRequestWrapper pushRegistrationRequestWrapper = new PushRegistrationRequestWrapper();
        pushRegistrationRequestWrapper.setPushRegistrationRequest(pushRegistrationRequest);
        this.pushService.registerDevice(header, pushRegistrationRequestWrapper, new PassThroughErrorZendeskCallback<PushRegistrationResponseWrapper>(callback) {
            public void onSuccess(PushRegistrationResponseWrapper result) {
                if (!(result == null || result.getRegistrationResponse() == null)) {
                    Logger.d(ZendeskPushRegistrationProvider.LOG_TAG, "Storing push registration response.", new Object[0]);
                    ZendeskPushRegistrationProvider.this.pushIdStorage.storePushRegistrationResponse(result.getRegistrationResponse());
                }
                if (callback == null) {
                    return;
                }
                if (result == null) {
                    callback.onError(new ErrorResponseAdapter("Could not read push registration response: response object was null."));
                } else {
                    callback.onSuccess(result.getRegistrationResponse());
                }
            }
        });
    }

    PushRegistrationRequest getPushRegistrationRequest(@NonNull String identifier, @NonNull Locale locale, @NonNull AuthenticationType authenticationType, @NonNull TokenType tokenType) {
        switch (authenticationType) {
            case JWT:
                return decoratePushRegistrationRequest(identifier, locale, tokenType, new JwtPushRegistrationRequest());
            case ANONYMOUS:
                AnonymousPushRegistrationRequest anonymousPushRegistrationRequest = (AnonymousPushRegistrationRequest) decoratePushRegistrationRequest(identifier, locale, tokenType, new AnonymousPushRegistrationRequest());
                AnonymousIdentity anonymousIdentity = (AnonymousIdentity) this.identityStorage.getIdentity();
                if (anonymousIdentity == null) {
                    return anonymousPushRegistrationRequest;
                }
                anonymousPushRegistrationRequest.setSdkGuid(anonymousIdentity.getSdkGuid());
                return anonymousPushRegistrationRequest;
            default:
                return null;
        }
    }

    private <E extends PushRegistrationRequest> E decoratePushRegistrationRequest(@NonNull String identifier, @NonNull Locale locale, @NonNull TokenType tokenType, @NonNull E request) {
        request.setIdentifier(identifier);
        request.setLocale(LocaleUtil.toLanguageTag(locale));
        if (tokenType == TokenType.UrbanAirshipChannelId) {
            request.setTokenType(tokenType.name);
        }
        return request;
    }
}
