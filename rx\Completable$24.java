package rx;

import rx.Scheduler.Worker;
import rx.functions.Action0;
import rx.internal.util.SubscriptionList;

class Completable$24 implements Completable$OnSubscribe {
    final /* synthetic */ Completable this$0;
    final /* synthetic */ Scheduler val$scheduler;

    Completable$24(Completable completable, Scheduler scheduler) {
        this.this$0 = completable;
        this.val$scheduler = scheduler;
    }

    public void call(final CompletableSubscriber s) {
        final SubscriptionList ad = new SubscriptionList();
        final Worker w = this.val$scheduler.createWorker();
        ad.add(w);
        s.onSubscribe(ad);
        this.this$0.unsafeSubscribe(new CompletableSubscriber() {
            public void onCompleted() {
                w.schedule(new Action0() {
                    public void call() {
                        try {
                            s.onCompleted();
                        } finally {
                            ad.unsubscribe();
                        }
                    }
                });
            }

            public void onError(final Throwable e) {
                w.schedule(new Action0() {
                    public void call() {
                        try {
                            s.onError(e);
                        } finally {
                            ad.unsubscribe();
                        }
                    }
                });
            }

            public void onSubscribe(Subscription d) {
                ad.add(d);
            }
        });
    }
}
