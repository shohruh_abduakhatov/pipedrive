package com.pipedrive.person.edit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import butterknife.ButterKnife;
import com.pipedrive.BaseActivity;
import com.pipedrive.R;
import com.pipedrive.dialogs.MessageDialog;
import com.pipedrive.linking.OrganizationLinkingActivity;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.PersonFieldOption;
import com.pipedrive.model.User;
import com.pipedrive.model.contact.CommunicationMedium;
import com.pipedrive.model.contact.Email;
import com.pipedrive.model.contact.Phone;
import com.pipedrive.navigator.Navigable;
import com.pipedrive.navigator.Navigator;
import com.pipedrive.navigator.buttons.DeleteMenuItem;
import com.pipedrive.navigator.buttons.DiscardMenuItem;
import com.pipedrive.navigator.buttons.NavigatorMenuItem;
import com.pipedrive.util.StringUtils;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.util.snackbar.SnackBarManager;
import com.pipedrive.views.ColoredImageView;
import com.pipedrive.views.assignment.OwnerView;
import com.pipedrive.views.assignment.SpinnerWithUserProfilePicture.OnItemSelectedListener;
import com.pipedrive.views.association.AssociationView.AssociationListener;
import com.pipedrive.views.association.OrganizationAssociationView;
import com.pipedrive.views.common.EditTextWithUnderlineAndLabel.OnValueChangedListener;
import com.pipedrive.views.common.TitleEditText;
import com.pipedrive.views.common.VisibilitySpinner;
import com.pipedrive.views.common.VisibilitySpinner.OnVisibilityChangeListener;
import com.pipedrive.views.edit.person.CommunicationMediumEditView;
import com.pipedrive.views.edit.person.CommunicationMediumEditView.OnAddAnotherClickListener;
import com.pipedrive.views.edit.person.CommunicationMediumEditView.OnClearClickListener;
import java.util.Arrays;
import java.util.List;

abstract class PersonActivity extends BaseActivity implements PersonEditView, Navigable {
    protected static final String KEY_NEW_PERSON_NAME = "NEW_PERSON_NAME";
    public static final String KEY_PERSON_SQL_ID = "PERSON_SQL_ID";
    private static final int LINK_ORGANIZATION_REQUEST_CODE = 1;
    private final DeleteMenuItem mDeleteMenuItem = new DeleteMenuItem() {
        public boolean isEnabled() {
            return PersonActivity.this.mPresenter.getPerson() != null && PersonActivity.this.mPresenter.getPerson().isStored() && PersonActivity.this.getSession().canUserDeletePerson();
        }

        public void onClick() {
            PersonActivity.this.onDelete();
        }
    };
    private final DiscardMenuItem mDiscardMenuItem = new DiscardMenuItem() {
        public void onClick() {
            PersonActivity.this.onCancel();
        }
    };
    @NonNull
    private CommunicationMediumEditView mEmails;
    @NonNull
    private TitleEditText mName;
    @NonNull
    private OrganizationAssociationView mOrganizationAssociationView;
    @NonNull
    private OwnerView mOwnerView;
    @NonNull
    private CommunicationMediumEditView mPhones;
    @NonNull
    private PersonEditPresenter mPresenter;
    @NonNull
    private VisibilitySpinner mVisibilitySpinner;

    PersonActivity() {
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupLayout();
        this.mPresenter = new PersonEditPresenterImpl(getSession(), savedInstanceState);
    }

    int getLayoutId() {
        return R.layout.activity_person_edit;
    }

    private void setupLayout() {
        setContentView(getLayoutId());
        setSupportActionBar((Toolbar) ButterKnife.findById((Activity) this, (int) R.id.toolbar));
        this.mName = (TitleEditText) ButterKnife.findById((Activity) this, (int) R.id.name);
        this.mOrganizationAssociationView = (OrganizationAssociationView) ButterKnife.findById((Activity) this, (int) R.id.organizationAssociationView);
        this.mPhones = (CommunicationMediumEditView) ButterKnife.findById((Activity) this, (int) R.id.phones);
        this.mEmails = (CommunicationMediumEditView) ButterKnife.findById((Activity) this, (int) R.id.emails);
        this.mOwnerView = (OwnerView) ButterKnife.findById((Activity) this, (int) R.id.ownerView);
        this.mVisibilitySpinner = (VisibilitySpinner) ButterKnife.findById((Activity) this, (int) R.id.visibleTo);
        ((ColoredImageView) ButterKnife.findById((Activity) this, (int) R.id.image)).setImageResource(R.drawable.main_contacts);
    }

    public void onResume() {
        super.onResume();
        this.mPresenter.bindView(this);
        Person person = this.mPresenter.getPerson();
        if (person != null) {
            onRequestPerson(person);
        } else if (!requestPerson(getIntent().getExtras())) {
            finish();
        }
    }

    @Nullable
    protected Navigator getNavigatorImplementation() {
        return new Navigator(this, (Toolbar) findViewById(R.id.toolbar));
    }

    public void onRequestPerson(@Nullable Person person) {
        if (person == null) {
            finish();
            return;
        }
        setupActionBar(person);
        setupNameView(person);
        setupAssociateOrganizationView(person.getCompany());
        setupAssociateOrganizationViewActions();
        setupEmailsView(person.getEmails());
        setupPhonesView(person.getPhones());
        setupOwnerView(person);
        setupVisibleToView(person);
    }

    private boolean requestPerson(Bundle intentExtras) {
        boolean newPersonRequested;
        if (intentExtras == null) {
            newPersonRequested = true;
        } else {
            newPersonRequested = false;
        }
        if (newPersonRequested) {
            this.mPresenter.requestOrRestoreNewPerson();
            return true;
        } else if (intentExtras.containsKey("PERSON_SQL_ID")) {
            this.mPresenter.requestOrRestorePerson(Long.valueOf(intentExtras.getLong("PERSON_SQL_ID")));
            return true;
        } else if (!intentExtras.containsKey(KEY_NEW_PERSON_NAME)) {
            return false;
        } else {
            this.mPresenter.requestOrRestoreNewPersonWithName(intentExtras.getString(KEY_NEW_PERSON_NAME));
            return true;
        }
    }

    private void setupActionBar(@NonNull Person person) {
        setTitle(!person.isStored() ? R.string.add_person : R.string.edit_person);
    }

    private void setupNameView(@NonNull final Person person) {
        this.mName.setup(person, new OnValueChangedListener() {
            public void onValueChanged(@NonNull String value) {
                person.setName(value);
            }
        });
    }

    private void setupAssociateOrganizationView(@Nullable Organization organization) {
        this.mOrganizationAssociationView.setAssociation(organization, Integer.valueOf(1));
    }

    private void setupAssociateOrganizationViewActions() {
        this.mOrganizationAssociationView.setAssociationListener(new AssociationListener<Organization>() {
            public void onAssociationChanged(@Nullable Organization association) {
                PersonActivity.this.mPresenter.associateOrganization(association);
            }
        });
    }

    private void setupEmailsView(@NonNull List<Email> emails) {
        this.mEmails.init(emails, new OnAddAnotherClickListener() {
            public void onAddAnotherClicked() {
                PersonActivity.this.mPresenter.addEmail();
            }
        }, new OnClearClickListener() {
            public void onClearClicked(@NonNull CommunicationMedium communicationMedium) {
                PersonActivity.this.mPresenter.removeEmail(communicationMedium);
            }
        });
    }

    private void setupPhonesView(@NonNull List<Phone> phones) {
        this.mPhones.init(phones, new OnAddAnotherClickListener() {
            public void onAddAnotherClicked() {
                PersonActivity.this.mPresenter.addPhone();
            }
        }, new OnClearClickListener() {
            public void onClearClicked(@NonNull CommunicationMedium communicationMedium) {
                PersonActivity.this.mPresenter.removePhone(communicationMedium);
            }
        });
    }

    private void setupOwnerView(@NonNull final Person person) {
        this.mOwnerView.loadOwnership(getSession(), person, new OnItemSelectedListener() {
            public void onItemSelected(@NonNull User user) {
                person.setOwnerPipedriveId(user.getPipedriveId());
                person.setOwnerName(user.getName());
            }
        });
    }

    private void setupVisibleToView(@NonNull final Person person) {
        this.mVisibilitySpinner.setup(getSession(), person, new OnVisibilityChangeListener() {
            public void onVisibilityChanged(@NonNull PersonFieldOption personFieldOption) {
                person.setVisibleTo(personFieldOption.getId());
            }
        });
    }

    public void onPersonCreated(boolean success) {
        if (success) {
            if (this.mPresenter.getPerson() != null) {
                setResult(-1, new Intent().putExtra("PERSON_SQL_ID", this.mPresenter.getPerson().getSqlId()));
            }
            SnackBarManager.INSTANCE.setDelayedSnackBar(R.string.person_added);
        }
        processPersonCrudResponse(success);
    }

    public void onPersonUpdated(boolean success) {
        processPersonCrudResponse(success);
        if (success) {
            SnackBarManager.INSTANCE.setDelayedSnackBar(R.string.person_updated);
        }
    }

    public void onPersonDeleted(boolean success) {
        if (success) {
            setResult(2);
            SnackBarManager.INSTANCE.setDelayedSnackBar(R.string.person_deleted);
        }
        processPersonCrudResponse(success);
    }

    private void processPersonCrudResponse(boolean success) {
        if (success) {
            finish();
        } else {
            ViewUtil.showErrorToast(this, R.string.error_contact_save_failed);
        }
    }

    public void onOrganizationAssociationUpdated(@NonNull Person person) {
        setupAssociateOrganizationView(person.getCompany());
    }

    public void onPhonesUpdated(@NonNull Person person) {
        setupPhonesView(person.getPhones());
    }

    public void onEmailsUpdated(@NonNull Person person) {
        setupEmailsView(person.getEmails());
    }

    protected void onPause() {
        this.mPresenter.unbindView();
        super.onPause();
    }

    public boolean isChangesMadeToFieldsAfterInitialLoad() {
        return this.mPresenter.personWasModified();
    }

    public boolean isAllRequiredFieldsSet() {
        Person currentlyManagedPerson = this.mPresenter.getPerson();
        return (currentlyManagedPerson == null || StringUtils.isTrimmedAndEmpty(currentlyManagedPerson.getName())) ? false : true;
    }

    public void onCreateOrUpdate() {
        this.mPresenter.changePerson();
    }

    public void createOrUpdateDiscardedAsRequiredFieldsAreNotSet() {
        MessageDialog.showMessage(getFragmentManager(), R.string.please_add_a_name_to_your_contact_person);
    }

    private void onDelete() {
        ViewUtil.showDeleteConfirmationDialog(this, getResources().getString(R.string.dialog_delete_person), new Runnable() {
            public void run() {
                PersonActivity.this.mPresenter.deletePerson();
            }
        });
    }

    public void onCancel() {
        finish();
    }

    @Nullable
    public List<? extends NavigatorMenuItem> getAdditionalButtons() {
        return Arrays.asList(new NavigatorMenuItem[]{this.mDeleteMenuItem, this.mDiscardMenuItem});
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1 && requestCode == 1) {
            long orgSqlId = data.getLongExtra(OrganizationLinkingActivity.KEY_ORG_SQL_ID, 0);
            if (orgSqlId != 0) {
                this.mPresenter.associateOrganizationBySqlId(Long.valueOf(orgSqlId));
            }
        }
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.mPresenter.onSaveInstanceState(outState);
    }
}
