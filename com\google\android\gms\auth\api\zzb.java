package com.google.android.gms.auth.api;

import android.os.Bundle;
import com.google.android.gms.common.api.Api.ApiOptions.Optional;

public class zzb implements Optional {
    private final Bundle ik;

    public Bundle zzaie() {
        return new Bundle(this.ik);
    }
}
