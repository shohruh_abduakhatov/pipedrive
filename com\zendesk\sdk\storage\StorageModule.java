package com.zendesk.sdk.storage;

public class StorageModule {
    private final HelpCenterSessionCache helpCenterSessionCache;
    private final IdentityStorage identityStorage;
    private final PushRegistrationResponseStorage pushRegistrationResponseStorage;
    private final RequestSessionCache requestSessionCache;
    private final RequestStorage requestStorage;
    private final SdkSettingsStorage sdkSettingsStorage;
    private final SdkStorage sdkStorage;

    StorageModule(SdkStorage sdkStorage, IdentityStorage identityStorage, RequestStorage requestStorage, SdkSettingsStorage sdkSettingsStorage, HelpCenterSessionCache sessionCache, RequestSessionCache requestSessionCache, PushRegistrationResponseStorage pushRegistrationResponseStorage) {
        this.sdkStorage = sdkStorage;
        this.identityStorage = identityStorage;
        this.requestStorage = requestStorage;
        this.sdkSettingsStorage = sdkSettingsStorage;
        this.helpCenterSessionCache = sessionCache;
        this.requestSessionCache = requestSessionCache;
        this.pushRegistrationResponseStorage = pushRegistrationResponseStorage;
    }

    public SdkStorage getSdkStorage() {
        return this.sdkStorage;
    }

    public IdentityStorage getIdentityStorage() {
        return this.identityStorage;
    }

    public RequestStorage getRequestStorage() {
        return this.requestStorage;
    }

    public SdkSettingsStorage getSdkSettingsStorage() {
        return this.sdkSettingsStorage;
    }

    public HelpCenterSessionCache getHelpCenterSessionCache() {
        return this.helpCenterSessionCache;
    }

    public RequestSessionCache getRequestSessionCache() {
        return this.requestSessionCache;
    }

    public PushRegistrationResponseStorage getPushRegistrationResponseStorage() {
        return this.pushRegistrationResponseStorage;
    }
}
