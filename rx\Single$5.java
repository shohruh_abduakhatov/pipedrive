package rx;

import rx.functions.Func4;
import rx.functions.FuncN;

class Single$5 implements FuncN<R> {
    final /* synthetic */ Func4 val$zipFunction;

    Single$5(Func4 func4) {
        this.val$zipFunction = func4;
    }

    public R call(Object... args) {
        return this.val$zipFunction.call(args[0], args[1], args[2], args[3]);
    }
}
