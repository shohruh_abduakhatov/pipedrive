package com.pipedrive.application;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.provider.Settings.Secure;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.pipedrive.datetime.DateFormatHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.model.ActivityType;
import com.pipedrive.store.StoreSynchronizeManager;
import com.pipedrive.util.time.TimeManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

public final class Session {
    private static final String PREFS_ACTIVITY_FILTER_ACTIVITY_TYPE_ID = "Session.PREFS_ACTIVITY_FILTER_ACTIVITY_TYPE_ID";
    private static final String PREFS_ACTIVITY_FILTER_ASSIGNEE_ID = "Session.PREFS__ACTIVITY_FILTER_ASSIGNEE_ID";
    private static final String PREFS_ACTIVITY_FILTER_TIME = "Session.PREFS__ACTIVITY_FILTER_TIME";
    private static final String PREFS_ACTIVITY_TYPES = "Session.PREFS__ACTIVITY_TYPES";
    private static final String PREFS_API_TOKEN = "Session.PREFS__API_TOKEN";
    private static final String PREFS_AUTHENTICATED_USER_ID = "Session.PREFS__AUTHENTICATED_USER_ID";
    private static final String PREFS_CALENDAR_LAST_SELECTED_DATE = "Session.PREFS__LAST_SELECTED_CALENDAR_DATE";
    private static final String PREFS_CALENDAR_LAST_SELECTED_PERIOD = "Session.PREFS__LAST_SELECTED_CALENDAR_PERIOD";
    private static final String PREFS_COMPANY_FEATURE_PRODUCTS_ENABLED = "Session.PREFS_COMPANY_FEATURE_PRODUCTS_ENABLED";
    private static final String PREFS_COMPANY_FEATURE_PRODUCT_DURATIONS = "Session.PREFS_COMPANY_FEATURE_PRODUCT_DURATIONS";
    private static final String PREFS_COMPANY_FEATURE_PRODUCT_PRICE_VARIATIONS_ENABLED = "Session.PREFS_COMPANY_FEATURE_PRODUCT_PRICE_VARIATIONS_ENABLED";
    private static final String PREFS_CONTACTS_LIST_UPDATE_TIME = "Session.PREFS__CONTACTS_LIST_UPDATE_TIME";
    private static final String PREFS_CONTACTS_VISIBILITY_FIELDS = "Session.PREFS__CONTACTS_VISIBILITY_FIELDS";
    private static final String PREFS_DOMAIN = "Session.PREFS__DOMAIN";
    private static final long PREFS_FALSE_LONG = -9223372036854775807L;
    private static final String PREFS_IS_CALENDAR_SCREEN_OPEN = "Session.PREFS__IS_CALENDAR_SCREEN_OPEN";
    private static final String PREFS_LAST_APP_START_TIME = "Session.PREFS__LAST_APP_START_TIME";
    private static final String PREFS_LAST_GET_REQUEST_TIME = "Session.PREFS_LAST_GET_REQUEST_TIME";
    private static final String PREFS_LAST_PROCESS_CHANGES_AND_RECENTS_TIME = "Session.PREFS_LAST_PROCESS_CHANGES_AND_RECENTS_TIME";
    private static final String PREFS_LAST_SELECTED_NEARBY_TOOLBAR_TAB_INDEX = "Session.PREFS_LAST_SELECTED_NEARBY_TOOLBAR_TAB_INDEX";
    private static final String PREFS_LAST_SYNC_TIME = "Session.PREFS__LAST_SYNC_TIME";
    public static final int PREFS_NOT_SET_INT = Integer.MIN_VALUE;
    public static final long PREFS_NOT_SET_LONG = Long.MIN_VALUE;
    public static final String PREFS_NOT_SET_STRING = "Session.PREFS__NOT_SET__STRING";
    private static final String PREFS_ORGANIZATION_LIST_UPDATE_TIME = "Session.PREFS__ORGANIZATION_LIST_UPDATE_TIME";
    private static final String PREFS_ORG_ADDRESS_FIELD = "Session.PREFS__ORG_ADDRESS_FIELD";
    private static final String PREFS_PIPELINE_PIPELINE_FILTER = "Session.PREFS__PIPELINE_PIPELINE_FILTER";
    private static final String PREFS_PIPELINE_PIPELINE_USER = "Session.PREFS__PIPELINE_PIPELINE_USER";
    private static final String PREFS_PIPELINE_SELECTED_PIPELINE_ID = "Session.PREFS__PIPELINE_SELECTED_PIPELINE_ID";
    private static final String PREFS_PIPELINE_SELECTED_STAGE_POSITION = "Session.PREFS__PIPELINE_SELECTED_STAGE_POSITION";
    private static final String PREFS_SELECTED_COMPANY_ID = "Session.PREFS__SELECTED_COMPANY_ID";
    private static final String PREFS_USER_SETTINGS_CAN_CHANGE_VISIBILITY_OF_ITEMS = "Session.PREFS__USER_SETTINGS_CAN_CHANGE_VISIBILITY_OF_ITEMS";
    private static final String PREFS_USER_SETTINGS_CAN_DELETE_DEAL = "Session.PREFS__USER_SETTINGS_DELETE_DEAL";
    private static final String PREFS_USER_SETTINGS_DEFAULT_CURRENCY_CODE = "Session.PREFS__USER_SETTINGS_DEFAULT_CURRENCY";
    private static final String PREFS_USER_SETTINGS_DEFAULT_LANGUAGE_COUNTRY = "Session.PREFS__USER_SETTINGS_DEFAULT_LANGUAGE_COUNTRY";
    private static final String PREFS_USER_SETTINGS_DEFAULT_LOCALE = "Session.PREFS__USER_SETTINGS_DEFAULT_LOCALE";
    private static final String PREFS_USER_SETTINGS_DEFAULT_VISIBILITY_DEAL = "Session.PREFS__USER_SETTINGS_DEFAULT_VISIBILITY_DEAL";
    private static final String PREFS_USER_SETTINGS_DEFAULT_VISIBILITY_ORG = "Session.PREFS__USER_SETTINGS_DEFAULT_VISIBILITY_ORG";
    private static final String PREFS_USER_SETTINGS_DEFAULT_VISIBILITY_PERSON = "Session.PREFS__USER_SETTINGS_DEFAULT_VISIBILITY_PERSON";
    private static final String PREFS_USER_SETTINGS_EMAIL = "Session.PREFS_USER_SETTINGS_EMAIL";
    private static final String PREFS_USER_SETTINGS_FILTERS = "Session.PREFS__USER_SETTINGS_FILTERS";
    private static final String PREFS_USER_SETTINGS_IS_ADMIN = "Session.PREFS__USER_SETTINGS_IS_ADMIN";
    private static final String PREFS_USER_SETTINGS_NAME = "Session.PREFS_USER_SETTINGS_NAME";
    @NonNull
    private final Context mApplicationContext;
    private volatile DBConnectorForSession mDBConnector = null;
    private final SessionRuntimeCache mSessionCache = new SessionRuntimeCache();
    private String mSessionID = null;
    private final StoreSynchronizeManager storeSynchronizeManager = new StoreSynchronizeManager();

    protected Session(String sessionID, Context context) {
        this.mSessionID = sessionID;
        this.mApplicationContext = context.getApplicationContext();
        getDatabase();
        this.mSessionCache.valid = true;
    }

    @SuppressLint({"SdCardPath"})
    public String dumpDatabaseIntoExternal() {
        return "";
    }

    private String dumpDb(@NonNull String dbName, @NonNull String toFilename) {
        File dbFileExternalLocation = new File(getApplicationContext().getExternalFilesDir(null), toFilename);
        dbFileExternalLocation.delete();
        try {
            FileInputStream dbFileIS = new FileInputStream(getApplicationContext().getDatabasePath(dbName));
            OutputStream dbFileOS = new FileOutputStream(dbFileExternalLocation);
            byte[] bucket = new byte[dbFileIS.available()];
            dbFileIS.read(bucket);
            dbFileOS.write(bucket);
            dbFileIS.close();
            dbFileOS.close();
        } catch (IOException e) {
            Log.e(new Throwable(String.format("Error making copy of DB file: %s", new Object[]{dbFileExternalLocation})));
        }
        return "/sdcard/Android/data/com.pipedrive/files/" + toFilename;
    }

    public final SessionRuntimeCache getRuntimeCache() {
        return this.mSessionCache;
    }

    @NonNull
    public final Context getApplicationContext() {
        return this.mApplicationContext;
    }

    public final SQLiteDatabase getDatabase() {
        if (this.mDBConnector == null) {
            this.mDBConnector = new DBConnectorForSession(this);
        }
        return this.mDBConnector.getDatabase();
    }

    private void closeDatabaseIfOpen() {
        if (this.mDBConnector != null) {
            this.mDBConnector.closeDatabase();
            this.mDBConnector = null;
        }
    }

    public final SharedSession getSharedSession() {
        return PipedriveApp.getSharedSession();
    }

    public final String getSessionID() {
        return this.mSessionID;
    }

    private void setSessionPrefsString(@NonNull String prefsKey, @Nullable String prefsValue) {
        com.pipedrive.application.SessionStoreHelper.Session.setSessionPrefsString(this, prefsKey, prefsValue);
    }

    private String getSessionPrefsString(@NonNull String prefsKey, @Nullable String returnIfNotFound) {
        return com.pipedrive.application.SessionStoreHelper.Session.getSessionPrefsString(this, prefsKey, returnIfNotFound);
    }

    private void setSessionPrefsLong(@NonNull String prefsKey, @Nullable Long prefsValue) {
        com.pipedrive.application.SessionStoreHelper.Session.setSessionPrefsLong(this, prefsKey, prefsValue);
    }

    private Long getSessionPrefsLong(@NonNull String prefsKey, @Nullable Long returnIfNotFound) {
        return com.pipedrive.application.SessionStoreHelper.Session.getSessionPrefsLong(this, prefsKey, returnIfNotFound);
    }

    private void setSessionPrefsInt(@NonNull String prefsKey, @Nullable Integer prefsValue) {
        com.pipedrive.application.SessionStoreHelper.Session.setSessionPrefsInt(this, prefsKey, prefsValue);
    }

    private Integer getSessionPrefsInt(@NonNull String prefsKey, @Nullable Integer returnIfNotFound) {
        return com.pipedrive.application.SessionStoreHelper.Session.getSessionPrefsInt(this, prefsKey, returnIfNotFound);
    }

    public String toString() {
        return String.format("Session: [ID: %s]", new Object[]{getSessionID()});
    }

    @Nullable
    public final String getApiToken() {
        return getSessionPrefsString(PREFS_API_TOKEN, PREFS_NOT_SET_STRING);
    }

    final void setApiToken(@Nullable String apiToken) {
        setSessionPrefsString(PREFS_API_TOKEN, apiToken);
    }

    @Nullable
    public final String getDomain(@Nullable String returnIfNotSet) {
        String domain = getSessionPrefsString(PREFS_DOMAIN, PREFS_NOT_SET_STRING);
        return TextUtils.equals(domain, PREFS_NOT_SET_STRING) ? returnIfNotSet : domain;
    }

    final void setDomain(@Nullable String domain) {
        setSessionPrefsString(PREFS_DOMAIN, domain);
    }

    public final long getAuthenticatedUserID() {
        return getSessionPrefsLong(PREFS_AUTHENTICATED_USER_ID, Long.valueOf(Long.MIN_VALUE)).longValue();
    }

    final void setAuthenticatedUserID(long id) {
        setSessionPrefsLong(PREFS_AUTHENTICATED_USER_ID, Long.valueOf(id));
    }

    public final long getSelectedCompanyID() {
        return getSessionPrefsLong(PREFS_SELECTED_COMPANY_ID, Long.valueOf(Long.MIN_VALUE)).longValue();
    }

    final void setSelectedCompanyID(long id) {
        setSessionPrefsLong(PREFS_SELECTED_COMPANY_ID, Long.valueOf(id));
    }

    public final String getContactsVisibilityFields() {
        return getSessionPrefsString(PREFS_CONTACTS_VISIBILITY_FIELDS, PREFS_NOT_SET_STRING);
    }

    public final void setContactsVisibilityFields(String contactsVisibilityFields) {
        setSessionPrefsString(PREFS_CONTACTS_VISIBILITY_FIELDS, contactsVisibilityFields);
    }

    public final long getContactsListUpdateTime(long returnIfNotSet) {
        long contactsListUpdateTime = getSessionPrefsLong(PREFS_CONTACTS_LIST_UPDATE_TIME, Long.valueOf(Long.MIN_VALUE)).longValue();
        return contactsListUpdateTime == Long.MIN_VALUE ? returnIfNotSet : contactsListUpdateTime;
    }

    public final void setContactsListUpdateTime(long time) {
        setSessionPrefsLong(PREFS_CONTACTS_LIST_UPDATE_TIME, Long.valueOf(time));
    }

    public final long getOrganizationsListUpdateTime(long returnIfNotSet) {
        long organizationsListUpdateTime = getSessionPrefsLong(PREFS_ORGANIZATION_LIST_UPDATE_TIME, Long.valueOf(Long.MIN_VALUE)).longValue();
        return organizationsListUpdateTime == Long.MIN_VALUE ? returnIfNotSet : organizationsListUpdateTime;
    }

    public final void setOrganizationsListUpdateTime(long time) {
        setSessionPrefsLong(PREFS_ORGANIZATION_LIST_UPDATE_TIME, Long.valueOf(time));
    }

    public final long getLastProcessChangesAndRecentsTimeInMillis(long returnIfNotSet) {
        long lastProcessChangesTime = getSessionPrefsLong(PREFS_LAST_PROCESS_CHANGES_AND_RECENTS_TIME, Long.valueOf(Long.MIN_VALUE)).longValue();
        return (lastProcessChangesTime == Long.MIN_VALUE || lastProcessChangesTime == PREFS_FALSE_LONG) ? returnIfNotSet : Math.abs(lastProcessChangesTime);
    }

    public final void setLastProcessChangesAndRecentsTimeInMillis(long timeInMillis) {
        setSessionPrefsLong(PREFS_LAST_PROCESS_CHANGES_AND_RECENTS_TIME, Long.valueOf(timeInMillis));
    }

    public final boolean isLastProcessChangesAndRecentsEnabled() {
        long lastProcessChangesTime = getSessionPrefsLong(PREFS_LAST_PROCESS_CHANGES_AND_RECENTS_TIME, Long.valueOf(Long.MIN_VALUE)).longValue();
        return lastProcessChangesTime == Long.MIN_VALUE || lastProcessChangesTime > 0;
    }

    public final void enableLastProcessChangesAndRecents(boolean enable) {
        long newLastProcessChangesTime = Long.MIN_VALUE;
        long lastProcessChangesTime = getSessionPrefsLong(PREFS_LAST_PROCESS_CHANGES_AND_RECENTS_TIME, Long.valueOf(Long.MIN_VALUE)).longValue();
        if (lastProcessChangesTime == Long.MIN_VALUE) {
            if (!enable) {
                newLastProcessChangesTime = PREFS_FALSE_LONG;
            }
        } else if (lastProcessChangesTime != PREFS_FALSE_LONG) {
            newLastProcessChangesTime = enable ? Math.abs(lastProcessChangesTime) : lastProcessChangesTime * -1;
        } else if (!enable) {
            newLastProcessChangesTime = PREFS_FALSE_LONG;
        }
        setSessionPrefsLong(PREFS_LAST_PROCESS_CHANGES_AND_RECENTS_TIME, Long.valueOf(newLastProcessChangesTime));
    }

    public final String getLastSyncTime(String returnIfNotSet) {
        String lastSyncTime = getSessionPrefsString(PREFS_LAST_SYNC_TIME, PREFS_NOT_SET_STRING);
        return lastSyncTime.equalsIgnoreCase(PREFS_NOT_SET_STRING) ? returnIfNotSet : lastSyncTime;
    }

    public final void setLastSyncTime(String lastSyncTime) {
        setSessionPrefsString(PREFS_LAST_SYNC_TIME, lastSyncTime);
    }

    public void setLastSyncTimeToNow() {
        setLastSyncTime(DateFormatHelper.fullDateFormat2UTC().format(new Date(TimeManager.getInstance().currentTimeMillis().longValue())));
    }

    public final String getUserSettingsFilters() {
        return getSessionPrefsString(PREFS_USER_SETTINGS_FILTERS, PREFS_NOT_SET_STRING);
    }

    public final void setUserSettingsFilters(String settings) {
        setSessionPrefsString(PREFS_USER_SETTINGS_FILTERS, settings);
    }

    public final String getUserSettingsDefaultLanguageCountry() {
        return getSessionPrefsString(PREFS_USER_SETTINGS_DEFAULT_LANGUAGE_COUNTRY, PREFS_NOT_SET_STRING);
    }

    public final void setUserSettingsDefaultLanguageCountry(String langCountry) {
        setSessionPrefsString(PREFS_USER_SETTINGS_DEFAULT_LANGUAGE_COUNTRY, langCountry);
    }

    public final String getUserSettingsDefaultLocale() {
        return getSessionPrefsString(PREFS_USER_SETTINGS_DEFAULT_LOCALE, PREFS_NOT_SET_STRING);
    }

    public final void setUserSettingsDefaultLocale(String locale) {
        setSessionPrefsString(PREFS_USER_SETTINGS_DEFAULT_LOCALE, locale);
    }

    public final String getUserSettingsDefaultCurrencyCode() {
        return getSessionPrefsString(PREFS_USER_SETTINGS_DEFAULT_CURRENCY_CODE, PREFS_NOT_SET_STRING);
    }

    public final void setUserSettingsDefaultCurrencyCode(String currencyCode) {
        setSessionPrefsString(PREFS_USER_SETTINGS_DEFAULT_CURRENCY_CODE, currencyCode);
    }

    public final void setUserSettingsDefaultVisibilityForOrganization(@Nullable Integer visibleTo) {
        setSessionPrefsInt(PREFS_USER_SETTINGS_DEFAULT_VISIBILITY_ORG, Integer.valueOf(visibleTo == null ? Integer.MIN_VALUE : visibleTo.intValue()));
    }

    public final int getUserSettingsDefaultVisibilityForOrganization(int returnIfNotSet) {
        int prefsInt = getSessionPrefsInt(PREFS_USER_SETTINGS_DEFAULT_VISIBILITY_ORG, Integer.valueOf(Integer.MIN_VALUE)).intValue();
        return prefsInt == Integer.MIN_VALUE ? returnIfNotSet : prefsInt;
    }

    public final void setUserSettingsDefaultVisibilityForPerson(@Nullable Integer visibleTo) {
        setSessionPrefsInt(PREFS_USER_SETTINGS_DEFAULT_VISIBILITY_PERSON, Integer.valueOf(visibleTo == null ? Integer.MIN_VALUE : visibleTo.intValue()));
    }

    public final int getUserSettingsDefaultVisibilityForPerson(int returnIfNotSet) {
        int prefsInt = getSessionPrefsInt(PREFS_USER_SETTINGS_DEFAULT_VISIBILITY_PERSON, Integer.valueOf(Integer.MIN_VALUE)).intValue();
        return prefsInt == Integer.MIN_VALUE ? returnIfNotSet : prefsInt;
    }

    public final void setUserSettingsDefaultVisibilityForDeal(@Nullable Integer visibleTo) {
        setSessionPrefsInt(PREFS_USER_SETTINGS_DEFAULT_VISIBILITY_DEAL, Integer.valueOf(visibleTo == null ? Integer.MIN_VALUE : visibleTo.intValue()));
    }

    public final int getUserSettingsDefaultVisibilityForDeal(int returnIfNotSet) {
        int prefsInt = getSessionPrefsInt(PREFS_USER_SETTINGS_DEFAULT_VISIBILITY_DEAL, Integer.valueOf(Integer.MIN_VALUE)).intValue();
        return prefsInt == Integer.MIN_VALUE ? returnIfNotSet : prefsInt;
    }

    public void setUserSettingsCanChangeVisivilityOfItems(boolean canChangeVisibilityOfItems) {
        setSessionPrefsInt(PREFS_USER_SETTINGS_CAN_CHANGE_VISIBILITY_OF_ITEMS, Integer.valueOf(canChangeVisibilityOfItems ? 1 : 0));
    }

    public final boolean getUserSettingsCanChangeVisibilityOfItems() {
        return getSessionPrefsInt(PREFS_USER_SETTINGS_CAN_CHANGE_VISIBILITY_OF_ITEMS, Integer.valueOf(Integer.MIN_VALUE)).intValue() == 1;
    }

    public void setUserSettingsCanDeleteDeal(boolean canDeleteDeal) {
        setSessionPrefsInt(PREFS_USER_SETTINGS_CAN_DELETE_DEAL, Integer.valueOf(canDeleteDeal ? 1 : 0));
    }

    public final boolean getUserSettingsCanDeleteDeal() {
        int canUserDeleteDealSessionSetting = getSessionPrefsInt(PREFS_USER_SETTINGS_CAN_DELETE_DEAL, Integer.valueOf(Integer.MIN_VALUE)).intValue();
        if (canUserDeleteDealSessionSetting == Integer.MIN_VALUE || canUserDeleteDealSessionSetting != 1) {
            return false;
        }
        return true;
    }

    public void setUserSettingsIsAdmin(boolean isAdmin) {
        setSessionPrefsInt(PREFS_USER_SETTINGS_IS_ADMIN, Integer.valueOf(isAdmin ? 1 : 0));
    }

    public boolean isUserAdmin() {
        return getSessionPrefsInt(PREFS_USER_SETTINGS_IS_ADMIN, Integer.valueOf(Integer.MIN_VALUE)).intValue() == 1;
    }

    public void setUserSettingsName(@Nullable String name) {
        String str = PREFS_USER_SETTINGS_NAME;
        if (name == null) {
            name = PREFS_NOT_SET_STRING;
        }
        setSessionPrefsString(str, name);
    }

    public final String getUserSettingsName(@Nullable String returnIfNotSet) {
        String name = getSessionPrefsString(PREFS_USER_SETTINGS_NAME, PREFS_NOT_SET_STRING);
        return name.equalsIgnoreCase(PREFS_NOT_SET_STRING) ? returnIfNotSet : name;
    }

    public void setUserSettingsEmail(@Nullable String email) {
        String str = PREFS_USER_SETTINGS_EMAIL;
        if (email == null) {
            email = PREFS_NOT_SET_STRING;
        }
        setSessionPrefsString(str, email);
    }

    public final String getUserSettingsEmail(@Nullable String returnIfNotSet) {
        String name = getSessionPrefsString(PREFS_USER_SETTINGS_EMAIL, PREFS_NOT_SET_STRING);
        return name.equalsIgnoreCase(PREFS_NOT_SET_STRING) ? returnIfNotSet : name;
    }

    public final boolean canUserDeletePerson() {
        return isUserAdmin();
    }

    public final boolean canUserDeleteOrg() {
        return isUserAdmin();
    }

    public final boolean canUserDeleteActivity() {
        return true;
    }

    public final boolean canUserDeleteNote() {
        return true;
    }

    public final String getActivityTypes() {
        return getSessionPrefsString(PREFS_ACTIVITY_TYPES, PREFS_NOT_SET_STRING);
    }

    public final void setActivityTypes(String activitiesTypes) {
        setSessionPrefsString(PREFS_ACTIVITY_TYPES, activitiesTypes);
        getRuntimeCache().allActivityTypes = null;
        ActivityType.readActivityTypes(this);
    }

    public final long getLastDownloadEverythingTime() {
        return getSessionPrefsLong(PREFS_LAST_APP_START_TIME, Long.valueOf(Long.MIN_VALUE)).longValue();
    }

    public final void setLastDownloadEverythingTime(long time) {
        setSessionPrefsLong(PREFS_LAST_APP_START_TIME, Long.valueOf(time));
    }

    public final String getOrgAddressField(String returnIfNotSet) {
        String addressField = getSessionPrefsString(PREFS_ORG_ADDRESS_FIELD, PREFS_NOT_SET_STRING);
        return TextUtils.equals(addressField, PREFS_NOT_SET_STRING) ? returnIfNotSet : addressField;
    }

    public final void setOrgAddressField(String prefsOrgAddressField) {
        setSessionPrefsString(PREFS_ORG_ADDRESS_FIELD, prefsOrgAddressField);
    }

    public final void setPipelineSelectedPipelineId(@Nullable Long id) {
        if (id == null) {
            setSessionPrefsLong(PREFS_PIPELINE_SELECTED_PIPELINE_ID, Long.valueOf(Long.MIN_VALUE));
            return;
        }
        boolean pipelineSelectionChanged;
        if (id.longValue() != getPipelineSelectedPipelineId(0)) {
            pipelineSelectionChanged = true;
        } else {
            pipelineSelectionChanged = false;
        }
        if (pipelineSelectionChanged) {
            setPipelineSelectedStagePosition(0);
        }
        setSessionPrefsLong(PREFS_PIPELINE_SELECTED_PIPELINE_ID, id);
    }

    public final long getPipelineSelectedPipelineId(long returnIfNotSet) {
        long id = getSessionPrefsLong(PREFS_PIPELINE_SELECTED_PIPELINE_ID, Long.valueOf(Long.MIN_VALUE)).longValue();
        return id == Long.MIN_VALUE ? returnIfNotSet : id;
    }

    public final void setPipelineFilter(@Nullable Long id) {
        setSessionPrefsLong(PREFS_PIPELINE_PIPELINE_FILTER, id);
    }

    @IntRange(from = 0)
    public final int getPipelineSelectedStagePosition(@IntRange(from = 0) int returnIfNotSet) {
        int position = getSessionPrefsInt(PREFS_PIPELINE_SELECTED_STAGE_POSITION, Integer.valueOf(Integer.MIN_VALUE)).intValue();
        return position == Integer.MIN_VALUE ? returnIfNotSet : position;
    }

    public final void setPipelineSelectedStagePosition(@IntRange(from = 0) int position) {
        setSessionPrefsInt(PREFS_PIPELINE_SELECTED_STAGE_POSITION, Integer.valueOf(position));
    }

    public final long getPipelineFilter(long returnIfNotSet) {
        long id = getSessionPrefsLong(PREFS_PIPELINE_PIPELINE_FILTER, Long.valueOf(Long.MIN_VALUE)).longValue();
        return id == Long.MIN_VALUE ? returnIfNotSet : id;
    }

    public final void setPipelineUser(@Nullable Long id) {
        setSessionPrefsLong(PREFS_PIPELINE_PIPELINE_USER, id);
    }

    public final long getPipelineUser(long returnIfNotSet) {
        long id = getSessionPrefsLong(PREFS_PIPELINE_PIPELINE_USER, Long.valueOf(Long.MIN_VALUE)).longValue();
        return id == Long.MIN_VALUE ? returnIfNotSet : id;
    }

    @Nullable
    public final Long getPipelineUserOrNull() {
        return getSessionPrefsLong(PREFS_PIPELINE_PIPELINE_USER, null);
    }

    public final void setActivityFilterAssigneeId(int id) {
        setSessionPrefsInt(PREFS_ACTIVITY_FILTER_ASSIGNEE_ID, Integer.valueOf(id));
    }

    public final int getActivityFilterAssigneeId(int returnIfNotSet) {
        int id = getSessionPrefsInt(PREFS_ACTIVITY_FILTER_ASSIGNEE_ID, Integer.valueOf(Integer.MIN_VALUE)).intValue();
        return id == Integer.MIN_VALUE ? returnIfNotSet : id;
    }

    public final void setActivityFilterActivityTypeId(int id) {
        setSessionPrefsInt(PREFS_ACTIVITY_FILTER_ACTIVITY_TYPE_ID, Integer.valueOf(id));
    }

    public final int getActivityFilterActivityTypeId(int returnIfNotSet) {
        int id = getSessionPrefsInt(PREFS_ACTIVITY_FILTER_ACTIVITY_TYPE_ID, Integer.valueOf(Integer.MIN_VALUE)).intValue();
        return id == Integer.MIN_VALUE ? returnIfNotSet : id;
    }

    public final void setActivityFilterTime(int time) {
        setSessionPrefsInt(PREFS_ACTIVITY_FILTER_TIME, Integer.valueOf(time));
    }

    public final int getActivityFilterTime(int returnIfNotSet) {
        int id = getSessionPrefsInt(PREFS_ACTIVITY_FILTER_TIME, Integer.valueOf(Integer.MIN_VALUE)).intValue();
        return id == Integer.MIN_VALUE ? returnIfNotSet : id;
    }

    public final boolean isValid() {
        return this.mSessionCache.valid;
    }

    public final void setInvalid() {
        this.mSessionCache.valid = false;
    }

    @SuppressLint({"CommitPrefEdits"})
    void clearDatabaseAndDisposeSession() {
        if (this.mDBConnector != null) {
            this.mDBConnector.clearDatabase();
        }
        SharedSession sharedSession = getSharedSession();
        if (!(sharedSession == null || sharedSession.mSharedDBConnector == null)) {
            sharedSession.mSharedDBConnector.clearDatabase();
        }
        SessionStoreHelper.removeAllSessionPreferences(this);
        setInvalid();
        closeDatabaseIfOpen();
        if (sharedSession != null && sharedSession.mSharedDBConnector != null) {
            sharedSession.mSharedDBConnector.closeDatabase();
            sharedSession.mSharedDBConnector = null;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(Object o) {
        boolean z = true;
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Session session = (Session) o;
        if (this.mSessionCache != null) {
            if (!this.mSessionCache.equals(session.mSessionCache)) {
                return false;
            }
        } else if (session.mSessionCache != null) {
            return false;
        }
        if (this.mSessionID != null) {
            if (!this.mSessionID.equals(session.mSessionID)) {
                return false;
            }
        } else if (session.mSessionID != null) {
            return false;
        }
        if (!this.mApplicationContext.equals(session.mApplicationContext)) {
            return false;
        }
        if (this.mDBConnector == null) {
            if (session.mDBConnector == null) {
                return z;
            }
        }
        z = false;
        return z;
    }

    public int hashCode() {
        int result;
        int hashCode;
        int i = 0;
        if (this.mSessionCache != null) {
            result = this.mSessionCache.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.mSessionID != null) {
            hashCode = this.mSessionID.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (((i2 + hashCode) * 31) + this.mApplicationContext.hashCode()) * 31;
        if (this.mDBConnector != null) {
            i = this.mDBConnector.hashCode();
        }
        return hashCode + i;
    }

    public final boolean isCalendarOpen() {
        if (getSessionPrefsInt(PREFS_IS_CALENDAR_SCREEN_OPEN, Integer.valueOf(0)).intValue() == 1) {
            return true;
        }
        return false;
    }

    public void setIsCalendarOpen(boolean isCalendarOpen) {
        setSessionPrefsInt(PREFS_IS_CALENDAR_SCREEN_OPEN, Integer.valueOf(isCalendarOpen ? 1 : 0));
    }

    public void setLastSelectedCalendarDate(@Nullable Long date) {
        setSessionPrefsLong(PREFS_CALENDAR_LAST_SELECTED_DATE, date);
    }

    public void setLastSelectedCalendarPeriod(@Nullable Long date) {
        setSessionPrefsLong(PREFS_CALENDAR_LAST_SELECTED_PERIOD, date);
    }

    @Nullable
    public Long getLastSelectedCalendarDate() {
        return getSessionPrefsLong(PREFS_CALENDAR_LAST_SELECTED_DATE, null);
    }

    @Nullable
    public Long getLastSelectedCalendarPeriod() {
        return getSessionPrefsLong(PREFS_CALENDAR_LAST_SELECTED_PERIOD, null);
    }

    public void setCompanyFeatureProducts(@Nullable Boolean enabled) {
        Integer prefsValue;
        if (enabled == null) {
            prefsValue = null;
        } else {
            prefsValue = Integer.valueOf(enabled.booleanValue() ? 1 : 0);
        }
        setSessionPrefsInt(PREFS_COMPANY_FEATURE_PRODUCTS_ENABLED, prefsValue);
    }

    public boolean isCompanyFeatureProductsEnabled(boolean returnIfNotSet) {
        Integer productsEnabled = getSessionPrefsInt(PREFS_COMPANY_FEATURE_PRODUCTS_ENABLED, null);
        if (productsEnabled == null) {
            return returnIfNotSet;
        }
        return productsEnabled.intValue() == 1;
    }

    public void setCompanyFeatureProductPriceVariations(@Nullable Boolean enabled) {
        Integer prefsValue;
        if (enabled == null) {
            prefsValue = null;
        } else {
            prefsValue = Integer.valueOf(enabled.booleanValue() ? 1 : 0);
        }
        setSessionPrefsInt(PREFS_COMPANY_FEATURE_PRODUCT_PRICE_VARIATIONS_ENABLED, prefsValue);
    }

    public boolean isCompanyFeatureProductPriceVariationsEnabled(boolean returnIfNotSet) {
        Integer productsEnabled = getSessionPrefsInt(PREFS_COMPANY_FEATURE_PRODUCT_PRICE_VARIATIONS_ENABLED, null);
        if (productsEnabled == null) {
            return returnIfNotSet;
        }
        return productsEnabled.intValue() == 1;
    }

    public void setCompanyFeatureProductDurations(@Nullable Boolean enabled) {
        Integer prefsValue;
        if (enabled == null) {
            prefsValue = null;
        } else {
            prefsValue = Integer.valueOf(enabled.booleanValue() ? 1 : 0);
        }
        setSessionPrefsInt(PREFS_COMPANY_FEATURE_PRODUCT_DURATIONS, prefsValue);
    }

    public boolean isCompanyFeatureProductDurationsEnabled(boolean returnIfNotSet) {
        Integer productsEnabled = getSessionPrefsInt(PREFS_COMPANY_FEATURE_PRODUCT_DURATIONS, null);
        if (productsEnabled == null) {
            return returnIfNotSet;
        }
        return productsEnabled.intValue() == 1;
    }

    public final Long getLastGetRequestTimeInMillis() {
        return getSessionPrefsLong(PREFS_LAST_GET_REQUEST_TIME, Long.valueOf(Long.MIN_VALUE));
    }

    public final void setPrefsLastGetRequestTimeInMillis(@NonNull Long lastGetRequestTimeInMillis) {
        setSessionPrefsLong(PREFS_LAST_GET_REQUEST_TIME, lastGetRequestTimeInMillis);
    }

    public final StoreSynchronizeManager getStoreSynchronizeManager() {
        return this.storeSynchronizeManager;
    }

    @Nullable
    public Integer getLastSelectedNearbyToolbarTabIndex() {
        return getSessionPrefsInt(PREFS_LAST_SELECTED_NEARBY_TOOLBAR_TAB_INDEX, null);
    }

    public void setLastSelectedNearbyToolbarTabIndex(int selectedTabIndex) {
        setSessionPrefsInt(PREFS_LAST_SELECTED_NEARBY_TOOLBAR_TAB_INDEX, Integer.valueOf(selectedTabIndex));
    }

    @SuppressLint({"HardwareIds"})
    @NonNull
    public String getAndroidId() {
        return Secure.getString(getApplicationContext().getContentResolver(), "android_id");
    }
}
