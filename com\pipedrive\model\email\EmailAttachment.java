package com.pipedrive.model.email;

import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.model.BaseFile;
import com.pipedrive.util.networking.entities.EmailAttachmentEntity;

public class EmailAttachment extends BaseFile {
    private Boolean mActive;
    private PipedriveDateTime mAddTime;
    private String mCleanName;
    private String mComment;
    private Integer mCompanyId;
    private Boolean mInline;
    private Long mMessageId;

    public Long getMessageId() {
        return this.mMessageId;
    }

    public void setMessageId(Long messageId) {
        this.mMessageId = messageId;
    }

    public Integer getCompanyId() {
        return this.mCompanyId;
    }

    public void setCompanyId(Integer companyId) {
        this.mCompanyId = companyId;
    }

    public PipedriveDateTime getAddTime() {
        return this.mAddTime;
    }

    public void setAddTime(PipedriveDateTime addTime) {
        this.mAddTime = addTime;
    }

    public String getCleanName() {
        return this.mCleanName;
    }

    public void setCleanName(String cleanName) {
        this.mCleanName = cleanName;
    }

    public Boolean getActive() {
        return this.mActive;
    }

    public void setActive(Boolean active) {
        this.mActive = active;
    }

    public Boolean getInline() {
        return this.mInline;
    }

    public void setInline(Boolean inline) {
        this.mInline = inline;
    }

    public String getComment() {
        return this.mComment;
    }

    public void setComment(String comment) {
        this.mComment = comment;
    }

    public static EmailAttachment from(EmailAttachmentEntity entity) {
        if (entity == null) {
            return null;
        }
        EmailAttachment emailAttachment = new EmailAttachment();
        emailAttachment.setPipedriveId(entity.getId().intValue());
        emailAttachment.setCompanyId(entity.getCompanyId());
        emailAttachment.setActive(entity.getActive());
        emailAttachment.setAddTime(PipedriveDateTime.instanceFromDate(entity.getAddTime()));
        emailAttachment.setCleanName(entity.getCleanName());
        emailAttachment.setComment(entity.getComment());
        emailAttachment.setFileName(entity.getFileName());
        emailAttachment.setFileSize(entity.getFileSize());
        emailAttachment.setFileType(entity.getFileType());
        emailAttachment.setInline(entity.getInline());
        emailAttachment.setUrl(entity.getUrl());
        return emailAttachment;
    }

    public String getName() {
        return getCleanName();
    }
}
