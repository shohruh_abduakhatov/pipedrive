package com.pipedrive.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;

public interface HtmlContent {
    @Nullable
    String getHtmlContent();

    @Nullable
    String getMetaInfo(@NonNull Session session, @Nullable String str);

    void setHtmlContent(@Nullable String str);
}
