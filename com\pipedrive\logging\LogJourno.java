package com.pipedrive.logging;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.InputDeviceCompat;
import android.support.v4.view.PointerIconCompat;
import com.newrelic.agent.android.NewRelic;
import com.pipedrive.application.Session;
import com.pipedrive.util.StringUtils;
import com.zendesk.service.HttpConstants;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.HashMap;
import java.util.Map;
import okhttp3.internal.http.StatusLine;

public class LogJourno {
    private static final String newrelicAttributeEventMessage = "trackedEventMessage";
    private static final String newrelicEventName = "Journo";

    enum CATEGORY {
        NotSpecified(0, 99),
        Session(100, 199),
        Store(200, 299),
        Authorization(HttpConstants.HTTP_MULT_CHOICE, 399),
        ChangesChanger(HttpConstants.HTTP_BAD_REQUEST, 499),
        ChangesHandler(HttpConstants.HTTP_INTERNAL_ERROR, 599),
        Networking(SettingsJsonConstants.ANALYTICS_FLUSH_INTERVAL_SECS_DEFAULT, 699),
        Recents(700, 799),
        ContentProviders(800, 899),
        Database(900, 999),
        JsonParser(1000, 1099),
        Email(1100, 1199),
        MODEL(1200, 1299),
        MOBILE_DEVICES(1300, 1399);
        
        private static final String newrelicAttribute = "trackedEventCategory";
        private final int eventIdFromIncluded;
        private final int eventIdToIncluded;

        private CATEGORY(int eventIdFromIncluded, int eventIdToIncluded) {
            this.eventIdFromIncluded = eventIdFromIncluded;
            this.eventIdToIncluded = eventIdToIncluded;
        }

        protected static CATEGORY getEventCategory(@Nullable EVENT event) {
            if (event == null) {
                return NotSpecified;
            }
            if (LogJourno.isBetween(event.id, Database.eventIdFromIncluded, Database.eventIdToIncluded)) {
                return Database;
            }
            if (LogJourno.isBetween(event.id, Session.eventIdFromIncluded, Session.eventIdToIncluded)) {
                return Session;
            }
            if (LogJourno.isBetween(event.id, Store.eventIdFromIncluded, Store.eventIdToIncluded)) {
                return Store;
            }
            if (LogJourno.isBetween(event.id, Authorization.eventIdFromIncluded, Authorization.eventIdToIncluded)) {
                return Authorization;
            }
            if (LogJourno.isBetween(event.id, ChangesChanger.eventIdFromIncluded, ChangesChanger.eventIdToIncluded)) {
                return ChangesChanger;
            }
            if (LogJourno.isBetween(event.id, ChangesHandler.eventIdFromIncluded, ChangesHandler.eventIdToIncluded)) {
                return ChangesHandler;
            }
            if (LogJourno.isBetween(event.id, Networking.eventIdFromIncluded, Networking.eventIdToIncluded)) {
                return Networking;
            }
            if (LogJourno.isBetween(event.id, Recents.eventIdFromIncluded, Recents.eventIdToIncluded)) {
                return Recents;
            }
            if (LogJourno.isBetween(event.id, ContentProviders.eventIdFromIncluded, ContentProviders.eventIdToIncluded)) {
                return ContentProviders;
            }
            if (LogJourno.isBetween(event.id, JsonParser.eventIdFromIncluded, JsonParser.eventIdToIncluded)) {
                return JsonParser;
            }
            if (LogJourno.isBetween(event.id, Email.eventIdFromIncluded, Email.eventIdToIncluded)) {
                return Email;
            }
            if (LogJourno.isBetween(event.id, MODEL.eventIdFromIncluded, MODEL.eventIdToIncluded)) {
                return MODEL;
            }
            if (LogJourno.isBetween(event.id, MOBILE_DEVICES.eventIdFromIncluded, MOBILE_DEVICES.eventIdToIncluded)) {
                return MOBILE_DEVICES;
            }
            return NotSpecified;
        }
    }

    public enum EVENT {
        Test(-1),
        NotSpecified(0),
        NotSpecified_UserSettingsPipelineFilterFailed(1),
        NotSpecified_userIdExtractionFailed(2),
        NotSpecified_userFilterExtractionFailed(3),
        NotSpecified_contactUpdateTimeParsingFailed(4),
        NotSpecified_contactAddTimeParsingFailed(5),
        NotSpecified_activityTypeParsingFailed(6),
        NotSpecified_stageSelectorGetCurrentStageFailed(7),
        NotSpecified_wearableNodeConnectionFound(8),
        NotSpecified_scheduledAlarmLimitExceeded(9),
        NotSpecified_noLastKnownLocationPresent(10),
        NotSpecified_applicationLabelCannotBeRetrieved(11),
        NOT_SPECIFIED_NEARBY_VIEWPORT_CANT_BE_INITIALIZED(12),
        NOT_SPECIFIED_LOGOUT_RECEIVED_WITH_ASYNCTASKS_SCHEDULED(13),
        Session_cannotReturnActiveSessionAsNoApplicationContextIsPresent(101),
        Session_getSharedPreferencesFailed(102),
        Store_storeInitiatedWithoutSession(HttpConstants.HTTP_CREATED),
        Store_createRequestedWithNoEntity(HttpConstants.HTTP_ACCEPTED),
        Store_createRequestedWithStoredEntity(HttpConstants.HTTP_NOT_AUTHORITATIVE),
        Store_updateRequestedWithNoEntity(HttpConstants.HTTP_NO_CONTENT),
        Store_updateRequestedWithNonStoredEntity(HttpConstants.HTTP_RESET),
        Store_dcimDirectoryNotAvailableForCameraSnapshotSave(HttpConstants.HTTP_PARTIAL),
        Store_noDirectoryAvailableForCameraSnapshotSave(207),
        Store_deleteRequestedWithNoEntity(208),
        Store_deleteRequestedWithNonStoredEntity(209),
        Authorization_googleAuthUserRecoverableAuthException(HttpConstants.HTTP_MOVED_PERM),
        Authorization_googleAuthGoogleAuthException(HttpConstants.HTTP_MOVED_TEMP),
        Authorization_googleAuthException(HttpConstants.HTTP_SEE_OTHER),
        Authorization_pipedriveAuthJsonResponseException(HttpConstants.HTTP_NOT_MODIFIED),
        Authorization_pipedriveAuthIOException(HttpConstants.HTTP_USE_PROXY),
        Authorization_loginDataParsingFailed(306),
        Authorization_companyDataCompanySwitchNoPipedriveId(StatusLine.HTTP_TEMP_REDIRECT),
        Authorization_companyDataCompanySwitchNoUserId(StatusLine.HTTP_PERM_REDIRECT),
        Authorization_companyDataCompanySwitchNoApiToken(309),
        Authorization_companyDataCompanySwitchSessionCreationFailed(310),
        Authorization_companyDataCompanySwitchNotEnoughDataToSwitchNeedReLogin(311),
        Authorization_companyDataCompanySwitchFailed(312),
        Authorization_googlePlayServices_deviceNotSupported(313),
        Authorization_googlePlayServices_requestingToInstall(314),
        ChangesChanger_initialisedWithNullSession(HttpConstants.HTTP_UNAUTHORIZED),
        ChangesChanger_createChangeRequestedWithNull(HttpConstants.HTTP_PAYMENT_REQUIRED),
        ChangesChanger_updateChangeRequestedWithNull(HttpConstants.HTTP_FORBIDDEN),
        ChangesChanger_registerChangeWithNoMetadata(HttpConstants.HTTP_NOT_FOUND),
        ChangesChanger_registerChangeSavingIntoDBFailed(HttpConstants.HTTP_BAD_METHOD),
        ChangesChanger_createChangeFailed(HttpConstants.HTTP_NOT_ACCEPTABLE),
        ChangesChanger_updateChangeFailed(HttpConstants.HTTP_PROXY_AUTH),
        ChangesChanger_createChangeRequestedWithExistingChange(HttpConstants.HTTP_CLIENT_TIMEOUT),
        ChangesChanger_updateChangeRequestedWithNewChange(HttpConstants.HTTP_CONFLICT),
        ChangesChanger_updateActivityCalledWithNewDeal(HttpConstants.HTTP_GONE),
        ChangesChanger_deleteChangeRequestedWithNull(HttpConstants.HTTP_LENGTH_REQUIRED),
        ChangesChanger_deleteChangeFailed(HttpConstants.HTTP_PRECON_FAILED),
        ChangesHandler_nullSessionPassedToHandler(HttpConstants.HTTP_NOT_IMPLEMENTED),
        ChangesHandler_multipleChangesCountFound(HttpConstants.HTTP_BAD_GATEWAY),
        ChangesHandler_changeResponseTooManyRequests(HttpConstants.HTTP_UNAVAILABLE),
        ChangesHandler_changeResponseCsGone(HttpConstants.HTTP_GATEWAY_TIMEOUT),
        ChangesHandler_changeResponseWithError(HttpConstants.HTTP_VERSION),
        ChangesHandler_changeMetadataNotFound(506),
        ChangesHandler_changeDataNotFound(507),
        ChangesHandler_notAllChangePipedriveAssociationsExist(508),
        ChangesHandler_changeDataRequestToJsonParsingFailed(509),
        ChangesHandler_changeDataResponseToJsonParsingFailed(510),
        ChangesHandler_cannotUpdatePipedriveIdFromChangeDataResponse(511),
        ChangesHandler_corruptedChangeFound(512),
        ChangesHandler_unknownChangeTypeHandlingRequested(InputDeviceCompat.SOURCE_DPAD),
        ChangesHandler_changeResponseBadRequest(514),
        ChangesHandler_changeResponseForbidden(515),
        ChangesHandler_changeResponseNotFound(516),
        ChangesHandler_changeResponseUnsupportedType(517),
        Networking_httpClientManagerPostNewCallException(601),
        Networking_okHttpConnectionIOException(602),
        Networking_httpURLConnectionConnectionIOException(603),
        Networking_retryCountExceeded(604),
        Networking_non200ResponseFromBackend(605),
        Networking_downloadPageOfDealsFailed(606),
        Networking_downloadPageOfPersonsFailed(607),
        Networking_parsingCustomFieldsListFailed(611),
        Networking_pipelineFilterExtractionFailed(612),
        Networking_downloadUsersFailed(613),
        Networking_downloadFiltersFailed(614),
        Networking_downloadFlowForDealFailed(615),
        Networking_downloadFlowForOrganizationFailed(616),
        Networking_downloadFlowForPersonFailed(617),
        Networking_downloadStagesFailed(618),
        Networking_downloadPipelinesFailed(619),
        Networking_dealFieldsEndpointFailed(620),
        Networking_personFieldsEndpointFailed(621),
        Networking_organizationFieldsEndpointFailed(622),
        Networking_unknownRequestType(623),
        Recents_recentsInitiatedWithoutSession(701),
        Recents_sessionIsMissingApiTokenOrSinceTimestampForRecentsRequest(702),
        Recents_unsupportedEncodingOfRecentsResponse(703),
        Recents_pageOfRecentsJsonParsingFailed(704),
        ContentProviders_providerInitiatedWithoutSession(801),
        ContentProviders_databaseForSessionIdNotFound(802),
        ContentProviders_databaseNotFoundForQueryTypeQuery(803),
        ContentProviders_databaseNotFoundForQueryTypeInsert(804),
        ContentProviders_databaseNotFoundForQueryTypeDelete(805),
        ContentProviders_databaseNotFoundForQueryTypeUpdate(806),
        ContentProviders_unableToAttachSessionIdToUri(807),
        ContentProviders_unableToAccessContentProviderForFileConversionDueToIOException(808),
        ContentProviders_unableToAccessContentProviderForFileConversionDueToSecurityException(809),
        ContentProviders_unableToAccessContentProviderForFileConversionDueToFileNotFoundException(810),
        Database_cannotOpen(901),
        Database_updateMadeWithOfflineChangesPresent(902),
        Database_cannotReturnInfoAboutDuplicateDatabaseChanges_NoTransactionalDBConnection(903),
        JsonParser_dealCustomFieldsNotParsedFromJsonReader(1001),
        JsonParser_orgCustomFieldsNotParsedFromJsonReader(1002),
        JsonParser_personCustomFieldsNotParsedFromJsonReader(PointerIconCompat.TYPE_HELP),
        JsonParser_usersSelfParsingFailed(PointerIconCompat.TYPE_WAIT),
        JsonParser_usersSelfCurrentUserSettingsParsingFailed(1005),
        JsonParser_usersSelfCompaniesParsingFailed(PointerIconCompat.TYPE_CELL),
        JsonParser_UtcDateTypeAdapterFailed(PointerIconCompat.TYPE_CROSSHAIR),
        MODEL_ORGANIZATION_WITH_EMPTY_NAME_WAS_STORED_IN_DATABASE(1201),
        MODEL_CANNOT_CREATE_ORGANIZATION_WITH_EMPTY_NAME(1202),
        MOBILE_DEVICES_FCM_TOKEN_REFRESHED(1301),
        MOBILE_DEVICES_LOGIN_DEVICE_ERROR(1302),
        MOBILE_DEVICES_LOGOUT_DEVICE_ERROR(1303),
        MOBILE_DEVICES_UPDATE_DEVICE_ERROR(1304);
        
        private static final String newrelicAttribute = "trackedEvent";
        private final int id;

        private EVENT(int eventId) {
            this.id = eventId;
        }
    }

    public static void reportEvent(@NonNull String message) {
        report(EVENT.NotSpecified, message);
    }

    public static void reportEvent(@NonNull EVENT event) {
        report(event, null);
    }

    public static void reportEvent(@NonNull EVENT event, @Nullable String message) {
        report(event, message);
    }

    public static void reportEvent(@NonNull Session session, @NonNull EVENT event) {
        reportEvent(session, event, "");
    }

    public static void reportEvent(@NonNull Session session, @NonNull EVENT event, @Nullable String message) {
        String str = "%s %s";
        Object[] objArr = new Object[2];
        objArr[0] = getIdsFromSession(session);
        objArr[1] = message == null ? "" : "msg:" + message;
        report(event, String.format(str, objArr));
    }

    public static void reportEvent(@NonNull EVENT event, @NonNull Throwable throwable) {
        report(event, String.format("Err:[%s]", new Object[]{throwable.toString()}));
    }

    public static void reportEvent(@NonNull Session session, @NonNull EVENT event, @NonNull Exception exception) {
        report(event, String.format("%s Err:[%s]", new Object[]{getIdsFromSession(session), exception.toString()}));
    }

    public static void reportEvent(@NonNull EVENT event, @NonNull String message, @NonNull Exception exception) {
        report(event, String.format("Err:[%s] Message[%s]", new Object[]{exception.toString(), message}));
    }

    public static void reportEvent(@NonNull Session session, @NonNull EVENT event, @NonNull String message, @NonNull Exception exception) {
        report(event, String.format("%s Err:[%s] Message[%s]", new Object[]{getIdsFromSession(session), exception.toString(), message}));
    }

    @NonNull
    private static String getIdsFromSession(@NonNull Session session) {
        return String.format("cid:%s uid:%s", new Object[]{Long.valueOf(session.getSelectedCompanyID()), Long.valueOf(session.getAuthenticatedUserID())});
    }

    private static void report(@NonNull EVENT event, @Nullable String message) {
        CATEGORY category = CATEGORY.getEventCategory(event);
        Map<String, Object> attributes = new HashMap();
        attributes.put("trackedEventCategory", Integer.valueOf(category.eventIdFromIncluded));
        attributes.put("trackedEvent", Integer.valueOf(event.id));
        if (!StringUtils.isTrimmedAndEmpty(message)) {
            attributes.put(newrelicAttributeEventMessage, message);
        }
        NewRelic.recordCustomEvent(newrelicEventName, attributes);
    }

    private static boolean isBetween(int what, int betweenFromIncluded, int betweenToIncluded) {
        return what >= betweenFromIncluded && what <= betweenToIncluded;
    }
}
