package com.pipedrive.customfields.views;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Pair;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.model.Deal;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.util.CustomFieldsUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DealCustomFieldListView extends CustomFieldListView {
    public DealCustomFieldListView(Context context) {
        this(context, null);
    }

    public DealCustomFieldListView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DealCustomFieldListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @NonNull
    protected String getSpecialFieldsType() {
        return "deal";
    }

    @NonNull
    protected List<CustomField> getCustomFields(@NonNull Session session) {
        Deal deal = (Deal) new DealsDataSource(session.getDatabase()).findBySqlId(this.mSqlId.longValue());
        if (deal == null) {
            return Collections.emptyList();
        }
        Pair<ArrayList<CustomField>, Integer> strangeCustomFieldArray = CustomFieldsUtil.populateCustomAndSpecialFields("deal", deal.getCustomFields(), deal);
        return ((ArrayList) strangeCustomFieldArray.first).subList(((Integer) strangeCustomFieldArray.second).intValue(), ((ArrayList) strangeCustomFieldArray.first).size());
    }
}
