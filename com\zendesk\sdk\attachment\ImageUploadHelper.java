package com.zendesk.sdk.attachment;

import com.zendesk.belvedere.BelvedereResult;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.feedback.ui.AttachmentContainerHost.AttachmentState;
import com.zendesk.sdk.model.request.Attachment;
import com.zendesk.sdk.model.request.UploadResponse;
import com.zendesk.sdk.network.UploadProvider;
import com.zendesk.service.ErrorResponse;
import com.zendesk.service.ZendeskCallback;
import com.zendesk.util.CollectionUtils;
import com.zendesk.util.StringUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class ImageUploadHelper {
    private static final String LOG_TAG = ImageUploadHelper.class.getSimpleName();
    private List<File> mAddedForUpload = new ArrayList();
    private boolean mDeleteAllUploadsCalled = false;
    private ImageUploadProgressListener progressListener;
    private AtomicInteger uploadCounter = new AtomicInteger(0);
    private final UploadProvider uploadProvider;
    private Map<File, UploadResponse> uploadedAttachments = new HashMap();
    private AtomicInteger uploadedCounter = new AtomicInteger(0);

    public interface ImageUploadProgressListener {
        void allImagesUploaded(Map<File, UploadResponse> map);

        void imageUploadError(ErrorResponse errorResponse, BelvedereResult belvedereResult);

        void imageUploaded(UploadResponse uploadResponse, BelvedereResult belvedereResult);
    }

    public ImageUploadHelper(ImageUploadProgressListener imageUploadProgressListener, UploadProvider uploadProvider) {
        this.progressListener = imageUploadProgressListener;
        this.uploadProvider = uploadProvider;
    }

    public void uploadImage(BelvedereResult file, String mimeType) {
        this.mAddedForUpload.add(file.getFile());
        this.uploadCounter.incrementAndGet();
        this.uploadProvider.uploadAttachment(file.getFile().getName(), file.getFile(), mimeType, newZendeskCallback(file));
    }

    public boolean isImageUploadCompleted() {
        return this.uploadCounter.get() == this.uploadedCounter.get();
    }

    public List<String> getUploadTokens() {
        List<String> tokens = new ArrayList();
        for (UploadResponse uploadResponse : this.uploadedAttachments.values()) {
            tokens.add(uploadResponse.getToken());
        }
        return tokens;
    }

    public List<Attachment> getUploadedAttachments() {
        Collection<UploadResponse> values = this.uploadedAttachments.values();
        List<Attachment> uploadedAttachments = new ArrayList();
        for (UploadResponse value : values) {
            uploadedAttachments.add(value.getAttachment());
        }
        return uploadedAttachments;
    }

    public HashMap<AttachmentState, List<File>> getRecentState() {
        List<File> uploaded = new ArrayList(this.uploadedAttachments.keySet());
        List<File> uploading = new ArrayList();
        for (File f : this.mAddedForUpload) {
            if (!uploaded.contains(f)) {
                uploading.add(f);
            }
        }
        HashMap<AttachmentState, List<File>> attachmentTypeListHashMap = new HashMap();
        attachmentTypeListHashMap.put(AttachmentState.UPLOADED, uploaded);
        attachmentTypeListHashMap.put(AttachmentState.UPLOADING, uploading);
        return attachmentTypeListHashMap;
    }

    public List<BelvedereResult> removeDuplicateFilesFromList(List<BelvedereResult> files) {
        List<BelvedereResult> uniqueFiles = new ArrayList();
        for (BelvedereResult file : files) {
            if (file != null) {
                boolean contains = false;
                for (File fn : this.mAddedForUpload) {
                    if (fn.getAbsolutePath().equals(file.getFile().getAbsolutePath())) {
                        contains = true;
                    }
                }
                if (!contains) {
                    uniqueFiles.add(file);
                }
            }
        }
        return uniqueFiles;
    }

    public void reset() {
        if (this.uploadCounter.get() == this.uploadedCounter.get()) {
            this.uploadCounter.set(0);
            this.uploadedCounter.set(0);
            this.uploadedAttachments.clear();
            this.mAddedForUpload.clear();
        }
    }

    public void setImageUploadProgressListener(ImageUploadProgressListener imageUploadProgressListener) {
        this.progressListener = imageUploadProgressListener;
    }

    public void removeImage(File file) {
        boolean fileHasToken = (this.uploadedAttachments == null || this.uploadedAttachments.get(file) == null || !StringUtils.hasLength(((UploadResponse) this.uploadedAttachments.get(file)).getToken())) ? false : true;
        if (fileHasToken) {
            this.uploadProvider.deleteAttachment(((UploadResponse) this.uploadedAttachments.get(file)).getToken(), null);
        }
        this.mAddedForUpload.remove(file);
        this.uploadedAttachments.remove(file);
    }

    public void deleteAllAttachmentsBeforeShutdown() {
        this.mDeleteAllUploadsCalled = true;
        List<String> uploadTokens = getUploadTokens();
        if (CollectionUtils.isNotEmpty(uploadTokens)) {
            for (String uploadToken : uploadTokens) {
                this.uploadProvider.deleteAttachment(uploadToken, null);
            }
        }
    }

    private synchronized void checkAndNotifyState() {
        if (isImageUploadCompleted() && this.progressListener != null) {
            this.progressListener.allImagesUploaded(this.uploadedAttachments);
        }
    }

    private ZendeskCallback<UploadResponse> newZendeskCallback(final BelvedereResult file) {
        return new ZendeskCallback<UploadResponse>() {
            public void onSuccess(UploadResponse result) {
                if (!(result == null || result.getAttachment() == null)) {
                    Logger.d(ImageUploadHelper.LOG_TAG, String.format(Locale.US, "Image successfully uploaded: %s", new Object[]{result.getAttachment().getContentUrl()}), new Object[0]);
                }
                ImageUploadHelper.this.uploadedCounter.incrementAndGet();
                ImageUploadHelper.this.uploadedAttachments.put(file.getFile(), result);
                if (ImageUploadHelper.this.progressListener != null) {
                    ImageUploadHelper.this.progressListener.imageUploaded(result, file);
                }
                if (ImageUploadHelper.this.mDeleteAllUploadsCalled && result != null && StringUtils.hasLength(result.getToken())) {
                    ImageUploadHelper.this.uploadProvider.deleteAttachment(result.getToken(), null);
                }
                ImageUploadHelper.this.checkAndNotifyState();
            }

            public void onError(ErrorResponse error) {
                Logger.e(ImageUploadHelper.LOG_TAG, String.format(Locale.US, "Error; Reason: %s, Status: %s, isNetworkError: %s", new Object[]{error.getReason(), Integer.valueOf(error.getStatus()), Boolean.valueOf(error.isNetworkError())}), new Object[0]);
                ImageUploadHelper.this.uploadedCounter.incrementAndGet();
                ImageUploadHelper.this.mAddedForUpload.remove(file.getFile());
                if (ImageUploadHelper.this.progressListener != null) {
                    ImageUploadHelper.this.progressListener.imageUploadError(error, file);
                }
                ImageUploadHelper.this.checkAndNotifyState();
            }
        };
    }
}
