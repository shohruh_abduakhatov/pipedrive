package com.pipedrive.util.flowfiles;

import android.content.Context;
import android.net.Uri;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.util.networking.entities.FlowItem;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class UriFileHandler {
    public static final String TAG = UriFileHandler.class.getSimpleName();
    @NonNull
    private final Context mContext;
    @Nullable
    private File mTemporaryFile;

    public UriFileHandler(@NonNull Context context) {
        this.mContext = context;
    }

    @Nullable
    public synchronized Uri getLocalFileUri(@NonNull Uri fileUri, @Nullable String fileMimeType) {
        disposeLocalFileUri();
        if (TextUtils.indexOf(fileUri.toString(), "content") != -1) {
            fileUri = convertContentProviderToTemporaryLocalFile(fileUri, fileMimeType);
        }
        return fileUri;
    }

    public synchronized void disposeLocalFileUri() {
        if (this.mTemporaryFile != null && this.mTemporaryFile.exists() && this.mTemporaryFile.isFile()) {
            this.mTemporaryFile.delete();
        }
    }

    @Nullable
    private Uri convertContentProviderToTemporaryLocalFile(@NonNull Uri uri, @Nullable String fileMimeType) {
        FileNotFoundException e;
        Throwable th;
        IOException e2;
        SecurityException e3;
        if (TextUtils.indexOf(uri.toString(), FlowItem.ITEM_TYPE_OBJECT_FILE) != -1) {
            return uri;
        }
        String prefix = "pd_upload_" + Long.toString(SystemClock.elapsedRealtime());
        String suffix = MimeTypeMap.getSingleton().getExtensionFromMimeType(fileMimeType) != null ? TextUtils.concat(new CharSequence[]{".", MimeTypeMap.getSingleton().getExtensionFromMimeType(fileMimeType)}).toString() : "";
        String filename = TextUtils.concat(new CharSequence[]{prefix, suffix}).toString();
        FileOutputStream fileOutputStream = null;
        InputStream inputStream = null;
        try {
            inputStream = this.mContext.getContentResolver().openInputStream(uri);
            if (inputStream == null) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e4) {
                    }
                }
                if (fileOutputStream == null) {
                    return null;
                }
                try {
                    fileOutputStream.close();
                    return null;
                } catch (IOException e5) {
                    return null;
                }
            }
            this.mTemporaryFile = new File(FileHelper.getInternalFilesStorageDirectory(this.mContext), filename);
            FileOutputStream fileOutputStream2 = new FileOutputStream(this.mTemporaryFile);
            try {
                FileHelper.writeInputStreamIntoFileOutputStream(fileOutputStream2, inputStream);
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e6) {
                    }
                }
                if (fileOutputStream2 != null) {
                    try {
                        fileOutputStream2.close();
                    } catch (IOException e7) {
                    }
                }
                return Uri.fromFile(this.mTemporaryFile);
            } catch (FileNotFoundException e8) {
                e = e8;
                fileOutputStream = fileOutputStream2;
                try {
                    Log.e(e);
                    LogJourno.reportEvent(EVENT.ContentProviders_unableToAccessContentProviderForFileConversionDueToFileNotFoundException, e);
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e9) {
                        }
                    }
                    if (fileOutputStream != null) {
                        return null;
                    }
                    try {
                        fileOutputStream.close();
                        return null;
                    } catch (IOException e10) {
                        return null;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e11) {
                        }
                    }
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException e12) {
                        }
                    }
                    throw th;
                }
            } catch (IOException e13) {
                e2 = e13;
                fileOutputStream = fileOutputStream2;
                Log.e(e2);
                LogJourno.reportEvent(EVENT.ContentProviders_unableToAccessContentProviderForFileConversionDueToIOException, e2);
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e14) {
                    }
                }
                if (fileOutputStream != null) {
                    return null;
                }
                try {
                    fileOutputStream.close();
                    return null;
                } catch (IOException e15) {
                    return null;
                }
            } catch (SecurityException e16) {
                e3 = e16;
                fileOutputStream = fileOutputStream2;
                Log.e(e3);
                LogJourno.reportEvent(EVENT.ContentProviders_unableToAccessContentProviderForFileConversionDueToSecurityException, e3);
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e17) {
                    }
                }
                if (fileOutputStream != null) {
                    return null;
                }
                try {
                    fileOutputStream.close();
                    return null;
                } catch (IOException e18) {
                    return null;
                }
            } catch (Throwable th3) {
                th = th3;
                fileOutputStream = fileOutputStream2;
                if (inputStream != null) {
                    inputStream.close();
                }
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
                throw th;
            }
        } catch (FileNotFoundException e19) {
            e = e19;
            Log.e(e);
            LogJourno.reportEvent(EVENT.ContentProviders_unableToAccessContentProviderForFileConversionDueToFileNotFoundException, e);
            if (inputStream != null) {
                inputStream.close();
            }
            if (fileOutputStream != null) {
                return null;
            }
            fileOutputStream.close();
            return null;
        } catch (IOException e20) {
            e2 = e20;
            Log.e(e2);
            LogJourno.reportEvent(EVENT.ContentProviders_unableToAccessContentProviderForFileConversionDueToIOException, e2);
            if (inputStream != null) {
                inputStream.close();
            }
            if (fileOutputStream != null) {
                return null;
            }
            fileOutputStream.close();
            return null;
        } catch (SecurityException e21) {
            e3 = e21;
            Log.e(e3);
            LogJourno.reportEvent(EVENT.ContentProviders_unableToAccessContentProviderForFileConversionDueToSecurityException, e3);
            if (inputStream != null) {
                inputStream.close();
            }
            if (fileOutputStream != null) {
                return null;
            }
            fileOutputStream.close();
            return null;
        }
    }
}
