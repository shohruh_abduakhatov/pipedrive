package com.zendesk.sdk.attachment;

import android.content.Context;
import android.support.annotation.NonNull;
import com.zendesk.belvedere.Belvedere;
import com.zendesk.belvedere.BelvedereLogger;
import com.zendesk.logger.Logger;

public enum ZendeskBelvedereProvider {
    INSTANCE;
    
    private Belvedere mBelvedere;

    static class BelvedereZendeskLogger implements BelvedereLogger {
        boolean logging;

        BelvedereZendeskLogger() {
            this.logging = false;
        }

        public void d(@NonNull String tag, @NonNull String msg) {
            if (this.logging) {
                Logger.d(tag, msg, new Object[0]);
            }
        }

        public void w(@NonNull String tag, @NonNull String msg) {
            if (this.logging) {
                Logger.d(tag, msg, new Object[0]);
            }
        }

        public void e(@NonNull String tag, @NonNull String msg) {
            if (this.logging) {
                Logger.d(tag, msg, new Object[0]);
            }
        }

        public void e(@NonNull String tag, @NonNull String msg, @NonNull Throwable throwable) {
            if (this.logging) {
                Logger.d(tag, msg, throwable, new Object[0]);
            }
        }

        public void setLoggable(boolean b) {
            this.logging = b;
        }
    }

    public Belvedere getBelvedere(Context context) {
        if (this.mBelvedere == null) {
            synchronized (ZendeskBelvedereProvider.class) {
                this.mBelvedere = Belvedere.from(context).withAllowMultiple(true).withContentType("image/*").withCustomLogger(new BelvedereZendeskLogger()).withDebug(Logger.isLoggable()).build();
            }
        }
        return this.mBelvedere;
    }
}
