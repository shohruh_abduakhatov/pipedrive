package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.auth.account.zzb;
import com.google.android.gms.auth.account.zzb.zza;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;

public class zznr extends zzj<zzb> {
    public zznr(Context context, Looper looper, zzf com_google_android_gms_common_internal_zzf, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 120, com_google_android_gms_common_internal_zzf, connectionCallbacks, onConnectionFailedListener);
    }

    protected zzb zzca(IBinder iBinder) {
        return zza.zzbz(iBinder);
    }

    protected /* synthetic */ IInterface zzh(IBinder iBinder) {
        return zzca(iBinder);
    }

    protected String zzjx() {
        return "com.google.android.gms.auth.account.workaccount.START";
    }

    protected String zzjy() {
        return "com.google.android.gms.auth.account.IWorkAccountService";
    }
}
