package com.pipedrive.datasource.search;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

public class SearchConstraint {
    public static final SearchConstraint EMPTY = fromString(null);
    private static final int MINIMAL_CONSTRAINT_LENGTH_FOR_SEARCH = 2;
    @NonNull
    private final String value;

    private SearchConstraint(@NonNull String value) {
        this.value = value;
    }

    @NonNull
    public static SearchConstraint fromString(@Nullable String value) {
        if (value == null) {
            value = "";
        }
        return new SearchConstraint(value);
    }

    public String toString() {
        return this.value;
    }

    @NonNull
    public String[] getParts() {
        if (TextUtils.isEmpty(this.value)) {
            return new String[0];
        }
        return this.value.split(" ");
    }

    public boolean isLongEnoughForSearch() {
        return !TextUtils.isEmpty(this.value) && this.value.length() > 2;
    }
}
