package com.zendesk.sdk.model.settings;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.sdk.model.access.AuthenticationType;
import com.zendesk.util.CollectionUtils;
import java.util.ArrayList;
import java.util.List;

public class SafeMobileSettings {
    private final MobileSettings mobileSettings;

    public SafeMobileSettings(MobileSettings mobileSettings) {
        this.mobileSettings = mobileSettings;
    }

    public MobileSettings getMobileSettings() {
        return this.mobileSettings;
    }

    public boolean isConversationsEnabled() {
        ConversationsSettings settings = getConversationsSettings();
        return settings != null && settings.isEnabled();
    }

    public boolean isAttachmentsEnabled() {
        AttachmentSettings settings = getAttachmentSettings();
        return settings != null && settings.isEnabled();
    }

    public long getMaxAttachmentSize() {
        AttachmentSettings settings = getAttachmentSettings();
        return settings != null ? settings.getMaxAttachmentSize() : 0;
    }

    public boolean hasHelpCenterSettings() {
        return getHelpCenterSettings() != null;
    }

    public boolean isHelpCenterEnabled() {
        HelpCenterSettings settings = getHelpCenterSettings();
        return settings != null && settings.isEnabled();
    }

    @NonNull
    public String getHelpCenterLocale() {
        HelpCenterSettings settings = getHelpCenterSettings();
        return (settings == null || settings.getLocale() == null) ? "" : settings.getLocale();
    }

    public boolean isRateMyAppEnabled() {
        RateMyAppSettings settings = getRateMyAppSettings();
        return settings != null && settings.isEnabled();
    }

    @NonNull
    public String getRateMyAppStoreUrl() {
        RateMyAppSettings settings = getRateMyAppSettings();
        return (settings == null || settings.getAndroidStoreUrl() == null) ? "" : settings.getAndroidStoreUrl();
    }

    public long getRateMyAppDelay() {
        RateMyAppSettings settings = getRateMyAppSettings();
        return settings != null ? settings.getDelay() : 0;
    }

    public long getRateMyAppDuration() {
        RateMyAppSettings settings = getRateMyAppSettings();
        return settings != null ? settings.getDuration() : 0;
    }

    public long getRateMyAppVisits() {
        RateMyAppSettings settings = getRateMyAppSettings();
        return settings != null ? (long) settings.getVisits() : 0;
    }

    @NonNull
    public List<String> getRateMyAppTags() {
        RateMyAppSettings settings = getRateMyAppSettings();
        return (settings == null || !CollectionUtils.isNotEmpty(settings.getTags())) ? new ArrayList() : settings.getTags();
    }

    @Nullable
    public AuthenticationType getAuthenticationType() {
        return getAuthenticationTypeSetting();
    }

    @Nullable
    private AttachmentSettings getAttachmentSettings() {
        boolean hasAttachmentSettings = (this.mobileSettings == null || this.mobileSettings.getAccountSettings() == null || this.mobileSettings.getAccountSettings().getAttachmentSettings() == null) ? false : true;
        return hasAttachmentSettings ? this.mobileSettings.getAccountSettings().getAttachmentSettings() : null;
    }

    @Nullable
    private ConversationsSettings getConversationsSettings() {
        boolean hasConversationSettings = (this.mobileSettings == null || this.mobileSettings.getSdkSettings() == null || this.mobileSettings.getSdkSettings().getConversationsSettings() == null) ? false : true;
        return hasConversationSettings ? this.mobileSettings.getSdkSettings().getConversationsSettings() : null;
    }

    @Nullable
    public HelpCenterSettings getHelpCenterSettings() {
        boolean hasHelpCenterSettings = (this.mobileSettings == null || this.mobileSettings.getSdkSettings() == null || this.mobileSettings.getSdkSettings().getHelpCenterSettings() == null) ? false : true;
        return hasHelpCenterSettings ? this.mobileSettings.getSdkSettings().getHelpCenterSettings() : null;
    }

    @NonNull
    public List<String> getContactZendeskTags() {
        boolean hasContactUsSettings = (this.mobileSettings == null || this.mobileSettings.getSdkSettings() == null || this.mobileSettings.getSdkSettings().getContactUsSettings() == null || !CollectionUtils.isNotEmpty(this.mobileSettings.getSdkSettings().getContactUsSettings().getTags())) ? false : true;
        return hasContactUsSettings ? this.mobileSettings.getSdkSettings().getContactUsSettings().getTags() : new ArrayList();
    }

    @Nullable
    private RateMyAppSettings getRateMyAppSettings() {
        boolean hasRateMyAppSettings = (this.mobileSettings == null || this.mobileSettings.getSdkSettings() == null || this.mobileSettings.getSdkSettings().getRateMyAppSettings() == null) ? false : true;
        return hasRateMyAppSettings ? this.mobileSettings.getSdkSettings().getRateMyAppSettings() : null;
    }

    @Nullable
    private AuthenticationType getAuthenticationTypeSetting() {
        boolean hasAuthenticationType = (this.mobileSettings == null || this.mobileSettings.getSdkSettings() == null || this.mobileSettings.getSdkSettings().getAuthentication() == null) ? false : true;
        return hasAuthenticationType ? this.mobileSettings.getSdkSettings().getAuthentication() : null;
    }

    public boolean isTicketFormSupportAvailable() {
        boolean settingsValid;
        if (this.mobileSettings == null || this.mobileSettings.getSdkSettings() == null || this.mobileSettings.getSdkSettings().getTicketFormSettings() == null) {
            settingsValid = false;
        } else {
            settingsValid = true;
        }
        if (settingsValid && this.mobileSettings.getSdkSettings().getTicketFormSettings().isAvailable()) {
            return true;
        }
        return false;
    }
}
