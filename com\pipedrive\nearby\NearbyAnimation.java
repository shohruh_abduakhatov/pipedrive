package com.pipedrive.nearby;

import android.animation.ObjectAnimator;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.animation.AccelerateInterpolator;

public enum NearbyAnimation {
    ;
    
    private static final long ANIM_MEDIUM_DURATION = 0;
    private static final Float HIDDEN_ALPHA_VALUE = null;
    private static final Float VISIBLE_ALPHA_VALUE = null;

    static {
        ANIM_MEDIUM_DURATION = (long) Resources.getSystem().getInteger(17694721);
        HIDDEN_ALPHA_VALUE = Float.valueOf(0.0f);
        VISIBLE_ALPHA_VALUE = Float.valueOf(1.0f);
    }

    @NonNull
    public static ObjectAnimator getTranslationYObjectAnimator(@NonNull View view, float fromTranslationY, float toTranslationY) {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view, "translationY", new float[]{fromTranslationY, toTranslationY});
        objectAnimator.setDuration(ANIM_MEDIUM_DURATION).setInterpolator(new AccelerateInterpolator());
        return objectAnimator;
    }

    @NonNull
    public static ObjectAnimator getAlphaObjectAnimator(@NonNull View view) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(view, "alpha", new float[]{VISIBLE_ALPHA_VALUE.floatValue(), HIDDEN_ALPHA_VALUE.floatValue()});
        animator.setDuration(ANIM_MEDIUM_DURATION);
        return animator;
    }
}
