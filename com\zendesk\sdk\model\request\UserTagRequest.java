package com.zendesk.sdk.model.request;

import java.util.List;

public class UserTagRequest {
    private List<String> tags;

    public void setTags(List<String> tags) {
        this.tags = tags;
    }
}
