package com.pipedrive.model;

import android.support.annotation.Nullable;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.newrelic.agent.android.instrumentation.JSONArrayInstrumentation;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class UserSettingsPipelineFilter {
    private static final String JSON_ATTRIB_FILTER = "filter";
    private static final String JSON_ATTRIB_PIPELINE_ID = "pipeline_id";
    private String filterName;
    private long pipelineId;

    public UserSettingsPipelineFilter(long pipelineId, String filterName) {
        this.pipelineId = pipelineId;
        this.filterName = filterName;
    }

    public long getPipelineId() {
        return this.pipelineId;
    }

    private String getFilterName() {
        return this.filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public long getUserId() {
        long userId = -1;
        if (this.filterName.startsWith("user_")) {
            try {
                userId = Long.parseLong(this.filterName.substring(this.filterName.lastIndexOf(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR) + 1));
            } catch (Exception e) {
                LogJourno.reportEvent(EVENT.NotSpecified_userIdExtractionFailed, String.format("filterName:[%s]", new Object[]{this.filterName}), e);
                Log.e(new Throwable("Error parsing userId filter. userId string: " + this.filterName));
            }
        }
        return userId;
    }

    public long getFilterId() {
        long filterId = -1;
        if (this.filterName.startsWith("filter_")) {
            try {
                filterId = Long.parseLong(this.filterName.substring(this.filterName.lastIndexOf(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR) + 1));
            } catch (Exception e) {
                LogJourno.reportEvent(EVENT.NotSpecified_userFilterExtractionFailed, String.format("filterName:[%s]", new Object[]{this.filterName}), e);
                Log.e(new Throwable("Error parsing filterId filter. filterId string: " + this.filterName));
            }
        }
        return filterId;
    }

    public static List<UserSettingsPipelineFilter> getFromJSON(String json) {
        List<UserSettingsPipelineFilter> filters = new ArrayList();
        if (json != null) {
            try {
                JSONArray filtersArr = JSONArrayInstrumentation.init(json);
                for (int i = filtersArr.length() - 1; i >= 0; i--) {
                    JSONObject filterObj = filtersArr.optJSONObject(i);
                    if (filterObj != null) {
                        filters.add(new UserSettingsPipelineFilter(filterObj.getLong(JSON_ATTRIB_PIPELINE_ID), filterObj.getString(JSON_ATTRIB_FILTER)));
                    }
                }
            } catch (Exception e) {
                LogJourno.reportEvent(EVENT.NotSpecified_UserSettingsPipelineFilterFailed, String.format("fromJson:[%s]", new Object[]{json}), e);
                Log.e(new Throwable("Error reading user pipelines filter settings", e));
            }
        }
        return filters;
    }

    @Nullable
    public static String getJSON(@Nullable List<UserSettingsPipelineFilter> filters) {
        if (filters == null) {
            return null;
        }
        JsonArray filtersArr = new JsonArray();
        for (UserSettingsPipelineFilter userSettingsPipelineFilter : filters) {
            JsonElement filterObj = new JsonObject();
            filterObj.addProperty(JSON_ATTRIB_PIPELINE_ID, Long.valueOf(userSettingsPipelineFilter.getPipelineId()));
            filterObj.addProperty(JSON_ATTRIB_FILTER, userSettingsPipelineFilter.getFilterName());
            filtersArr.add(filterObj);
        }
        return filtersArr.toString();
    }

    public String toString() {
        return "UserSettingsPipelineFilter{pipelineId=" + this.pipelineId + ", filterName='" + this.filterName + '\'' + '}';
    }
}
