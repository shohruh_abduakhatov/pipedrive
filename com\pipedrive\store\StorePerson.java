package com.pipedrive.store;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.changes.PersonChanger;
import com.pipedrive.model.Person;
import com.pipedrive.util.StringUtils;

public class StorePerson extends Store<Person> {
    public StorePerson(@NonNull Session session) {
        super(session);
    }

    void cleanupBeforeCreate(@NonNull Person newPersonBeingStored) {
        if (newPersonBeingStored.getCompany() != null) {
            new StoreOrganization(getSession()).cleanupBeforeCreate(newPersonBeingStored.getCompany());
        }
        processPersonBeforeStoringShared(newPersonBeingStored);
        newPersonBeingStored.setIsActive(true);
    }

    boolean implementCreate(@NonNull Person newPerson) {
        return new PersonChanger(getSession()).create(newPerson);
    }

    void cleanupBeforeUpdate(@NonNull Person updatedPersonBeingStored) {
        if (updatedPersonBeingStored.getCompany() != null) {
            new StoreOrganization(getSession()).cleanupBeforeUpdate(updatedPersonBeingStored.getCompany());
        }
        processPersonBeforeStoringShared(updatedPersonBeingStored);
    }

    boolean implementUpdate(@NonNull Person updatedPerson) {
        return new PersonChanger(getSession()).update(updatedPerson);
    }

    private void processPersonBeforeStoringShared(Person personBeingSaved) {
        Session session = getSession();
        if (personBeingSaved.getOwnerPipedriveId() <= 0) {
            personBeingSaved.setOwnerPipedriveId(Long.valueOf(session.getAuthenticatedUserID()).intValue());
        }
        if (StringUtils.isTrimmedAndEmpty(personBeingSaved.getOwnerName()) && ((long) personBeingSaved.getOwnerPipedriveId()) == session.getAuthenticatedUserID()) {
            personBeingSaved.setOwnerName(session.getUserSettingsName(null));
        }
    }
}
