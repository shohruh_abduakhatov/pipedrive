package com.pipedrive.views.calendar;

public interface OnExpandOrCollapseListener {
    void onCollapse();

    void onExpand();
}
