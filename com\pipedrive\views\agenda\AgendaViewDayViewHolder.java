package com.pipedrive.views.agenda;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import butterknife.ButterKnife;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import java.util.Calendar;

class AgendaViewDayViewHolder extends ViewHolder {
    private final AgendaDayView agendaDayView;
    private final AllDayActivitiesView allDayActivitiesView;
    private final int allDayActivitiesViewMaxHeight;

    AgendaViewDayViewHolder(View itemView, int allDayActivitiesViewMaxHeight) {
        super(itemView);
        this.allDayActivitiesViewMaxHeight = allDayActivitiesViewMaxHeight;
        this.agendaDayView = (AgendaDayView) ButterKnife.findById(itemView, R.id.agendaDayView);
        this.allDayActivitiesView = (AllDayActivitiesView) ButterKnife.findById(itemView, R.id.allDayActivitiesView);
        this.agendaDayView.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                AgendaViewDayViewHolder.this.allDayActivitiesView.collapseAllDayActivitiesList();
            }
        });
    }

    public void bind(@NonNull Session session, @NonNull Calendar date, boolean smoothScrollToCurrentTime, OnActivityClickListener onActivityClickListener, int scrollY, boolean allDayActivityListExpanded) {
        this.allDayActivitiesView.setDateAndUpdateViews(session, date.getTime(), this.allDayActivitiesViewMaxHeight, allDayActivityListExpanded);
        if (this.allDayActivitiesView.isEmpty()) {
            this.agendaDayView.shiftUp();
        } else {
            this.agendaDayView.shiftDown();
        }
        this.agendaDayView.setup(session, date, onActivityClickListener);
        if (smoothScrollToCurrentTime) {
            this.agendaDayView.scrollToCurrentTime(true);
        } else if (scrollY != 0) {
            this.agendaDayView.scrollToY(scrollY);
        } else {
            this.agendaDayView.scrollToCurrentTime(false);
        }
    }

    public void collapseAllDayActivitiesList() {
        this.allDayActivitiesView.collapseAllDayActivitiesList();
    }

    public void refreshContent(Session session) {
        this.agendaDayView.refreshContent(session);
        this.allDayActivitiesView.refreshContent(session);
    }
}
