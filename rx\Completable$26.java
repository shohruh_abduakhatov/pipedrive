package rx;

import java.util.Arrays;
import rx.exceptions.CompositeException;
import rx.functions.Func1;
import rx.subscriptions.SerialSubscription;

class Completable$26 implements Completable$OnSubscribe {
    final /* synthetic */ Completable this$0;
    final /* synthetic */ Func1 val$errorMapper;

    Completable$26(Completable completable, Func1 func1) {
        this.this$0 = completable;
        this.val$errorMapper = func1;
    }

    public void call(final CompletableSubscriber s) {
        final SerialSubscription sd = new SerialSubscription();
        this.this$0.unsafeSubscribe(new CompletableSubscriber() {
            public void onCompleted() {
                s.onCompleted();
            }

            public void onError(Throwable e) {
                Throwable e2;
                try {
                    Completable c = (Completable) Completable$26.this.val$errorMapper.call(e);
                    if (c == null) {
                        NullPointerException npe = new NullPointerException("The completable returned is null");
                        e2 = new CompositeException(Arrays.asList(new Throwable[]{e, npe}));
                        s.onError(e2);
                        e = e2;
                        return;
                    }
                    c.unsafeSubscribe(new CompletableSubscriber() {
                        public void onCompleted() {
                            s.onCompleted();
                        }

                        public void onError(Throwable e) {
                            s.onError(e);
                        }

                        public void onSubscribe(Subscription d) {
                            sd.set(d);
                        }
                    });
                } catch (Throwable ex) {
                    e2 = new CompositeException(Arrays.asList(new Throwable[]{e, ex}));
                    s.onError(e2);
                    e = e2;
                }
            }

            public void onSubscribe(Subscription d) {
                sd.set(d);
            }
        });
    }
}
