package com.pipedrive.views.hinted;

import android.content.Context;
import android.util.AttributeSet;
import com.pipedrive.R;
import com.pipedrive.views.Spinner;

public class HintedSpinner extends BaseHintedLayout<Spinner> {
    public HintedSpinner(Context context) {
        super(context);
    }

    public HintedSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HintedSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected int getMainViewLayoutResource() {
        return R.layout.view_hinted_spinner;
    }
}
