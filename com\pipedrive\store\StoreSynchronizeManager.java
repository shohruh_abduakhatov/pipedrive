package com.pipedrive.store;

import rx.Completable;
import rx.subjects.BehaviorSubject;
import rx.subjects.Subject;

public final class StoreSynchronizeManager {
    private final Subject<Boolean, Boolean> offlineChangesSynced = BehaviorSubject.create(Boolean.valueOf(true)).toSerialized();

    public final void waitForOfflineChangesToSync() throws Exception {
        getOfflineChangesSyncCompletable().await();
    }

    Completable getOfflineChangesSyncCompletable() {
        return this.offlineChangesSynced.filter(new 1(this)).take(1).toCompletable();
    }

    final void offlineChangesSyncStarted() {
        this.offlineChangesSynced.onNext(Boolean.valueOf(false));
    }

    final void offlineChangesSyncFinished() {
        this.offlineChangesSynced.onNext(Boolean.valueOf(true));
    }
}
