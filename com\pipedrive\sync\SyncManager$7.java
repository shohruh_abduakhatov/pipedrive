package com.pipedrive.sync;

import com.pipedrive.application.Session;
import com.pipedrive.changes.ChangesHelper;
import com.pipedrive.tasks.AsyncTaskWithCallback;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished.TaskResult;

class SyncManager$7 extends AsyncTaskWithCallback<Void, Void> {
    final /* synthetic */ SyncManager this$0;

    SyncManager$7(SyncManager this$0, Session session, OnTaskFinished onTaskFinished) {
        this.this$0 = this$0;
        super(session, onTaskFinished);
    }

    protected TaskResult doInBackgroundWithCallback(Void... params) {
        ChangesHelper.syncOfflineChanges(getSession());
        return TaskResult.SUCCEEDED;
    }
}
