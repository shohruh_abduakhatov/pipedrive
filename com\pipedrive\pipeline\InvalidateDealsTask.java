package com.pipedrive.pipeline;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.pipedrive.application.Session;
import com.pipedrive.contentproviders.DealsContentProvider;
import com.pipedrive.datasource.FilterDealsDataSource;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.model.Deal;
import com.pipedrive.model.pagination.Pagination;
import com.pipedrive.tasks.StoreAwareAsyncTask;
import com.pipedrive.util.deals.DealsUtil;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.ConnectionUtil.JsonReaderInterceptor;
import com.pipedrive.util.networking.Response;
import java.io.IOException;

class InvalidateDealsTask extends StoreAwareAsyncTask<Void, Boolean, Boolean> {
    private final long mFilterId;
    private boolean mInvalidateFilters;
    private boolean mInvalidateStages;
    @Nullable
    private final OnProgressUpdated mOnProgressUpdated;
    @Nullable
    private final OnTaskFinished mOnTaskFinished;
    private final int mStageId;
    private final long mUserId;
    @NonNull
    Boolean shouldResetFilterAssociations = Boolean.valueOf(false);

    @MainThread
    interface OnProgressUpdated {
        void newDealsDownloaded(@NonNull Session session, int i, long j, long j2, @Nullable Boolean bool);
    }

    @MainThread
    public interface OnTaskFinished {
        void allDealsDownloaded(@NonNull Session session, int i, long j, long j2);

        void dealsNotDownloadedDueToError(@NonNull Session session, int i, long j, long j2);

        void filterNotFound(@NonNull Session session, int i, long j, long j2);

        void stageNotFound(@NonNull Session session, int i, long j, long j2);

        void taskNotExecutedDueToOfflineMode(@NonNull Session session, int i, long j, long j2);
    }

    InvalidateDealsTask(@NonNull Session session, int stageId, long filterId, long userId, @Nullable OnProgressUpdated onProgressUpdated, @Nullable OnTaskFinished onTaskFinished) {
        boolean z = false;
        super(session);
        this.mStageId = stageId;
        this.mFilterId = filterId;
        this.mUserId = userId;
        this.mOnProgressUpdated = onProgressUpdated;
        this.mInvalidateFilters = false;
        this.mInvalidateStages = false;
        this.mOnTaskFinished = onTaskFinished;
        if (this.mFilterId > 0) {
            z = true;
        }
        this.shouldResetFilterAssociations = Boolean.valueOf(z);
    }

    protected Boolean doInBackgroundAfterStoreSync(Void... params) {
        if (!ConnectionUtil.isConnected(getSession().getApplicationContext())) {
            return null;
        }
        long start = 0;
        long limit = 100;
        while (!isCancelled()) {
            Response response = downloadStageDeals(getSession(), this.mStageId, this.mFilterId, this.mUserId, start, limit);
            if (response.isRequestSuccessful) {
                Pagination pagination = response.getPagination();
                boolean lastPageOfDeals = pagination == null || !pagination.moreItemsInCollection;
                boolean moreDealsToBeDownloaded = !lastPageOfDeals;
                publishProgress(new Boolean[]{Boolean.valueOf(moreDealsToBeDownloaded)});
                if (lastPageOfDeals) {
                    return Boolean.valueOf(true);
                }
                start = (long) pagination.nextStart;
                limit = (long) pagination.limit;
            } else {
                if ("Stage not found.".equalsIgnoreCase(response.error)) {
                    this.mInvalidateStages = true;
                } else if ("Filter not found".equalsIgnoreCase(response.error)) {
                    this.mInvalidateFilters = true;
                }
                return Boolean.valueOf(false);
            }
        }
        return Boolean.valueOf(false);
    }

    protected void onProgressUpdate(Boolean... moreDealsInPipeline) {
        boolean z = false;
        if (this.mOnProgressUpdated != null) {
            if (moreDealsInPipeline.length > 0 && moreDealsInPipeline[0] != null && moreDealsInPipeline[0].booleanValue()) {
                z = true;
            }
            this.mOnProgressUpdated.newDealsDownloaded(getSession(), this.mStageId, this.mFilterId, this.mUserId, Boolean.valueOf(z));
        }
    }

    protected void onPostExecute(@Nullable Boolean succeeded) {
        boolean isOnTaskFinishedImplemented;
        boolean taskNotExecutedDueToOfflineMode = true;
        if (this.mOnTaskFinished != null) {
            isOnTaskFinishedImplemented = true;
        } else {
            isOnTaskFinishedImplemented = false;
        }
        if (isOnTaskFinishedImplemented) {
            if (succeeded != null) {
                taskNotExecutedDueToOfflineMode = false;
            }
            if (taskNotExecutedDueToOfflineMode) {
                this.mOnTaskFinished.taskNotExecutedDueToOfflineMode(getSession(), this.mStageId, this.mFilterId, this.mUserId);
            } else if (this.mInvalidateFilters) {
                this.mOnTaskFinished.filterNotFound(getSession(), this.mStageId, this.mFilterId, this.mUserId);
            } else if (this.mInvalidateStages) {
                this.mOnTaskFinished.stageNotFound(getSession(), this.mStageId, this.mFilterId, this.mUserId);
            } else if (succeeded.booleanValue()) {
                this.mOnTaskFinished.allDealsDownloaded(getSession(), this.mStageId, this.mFilterId, this.mUserId);
            } else {
                this.mOnTaskFinished.dealsNotDownloadedDueToError(getSession(), this.mStageId, this.mFilterId, this.mUserId);
            }
        }
    }

    private Response downloadStageDeals(@NonNull Session session, int stageId, long filterId, long userId, long start, long limit) {
        final long j = filterId;
        final int i = stageId;
        return ConnectionUtil.requestGet(buildEndpointUrl(session, (long) stageId, filterId, userId, start, limit), new JsonReaderInterceptor<Response>() {
            public Response interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull Response responseTemplate) throws IOException {
                boolean isDbUpdated = false;
                FilterDealsDataSource filterDealsDataSource = new FilterDealsDataSource(InvalidateDealsTask.this.getSession().getDatabase());
                if (jsonReader.peek() == JsonToken.BEGIN_ARRAY) {
                    jsonReader.beginArray();
                    if (InvalidateDealsTask.this.shouldResetFilterAssociations.booleanValue()) {
                        filterDealsDataSource.deleteAllFilterAssociationsForStage(Long.valueOf(j), Integer.valueOf(i));
                        InvalidateDealsTask.this.shouldResetFilterAssociations = Boolean.valueOf(false);
                    }
                    while (jsonReader.hasNext() && jsonReader.peek() == JsonToken.BEGIN_OBJECT) {
                        Deal deal = Deal.readDeal(jsonReader);
                        if (deal != null) {
                            DealsUtil.createOrUpdateDealIntoDBWithRelations(session, deal);
                            if (j > 0) {
                                filterDealsDataSource.relateDealToFilterId(deal, j);
                            }
                            isDbUpdated = true;
                        }
                    }
                    jsonReader.endArray();
                } else {
                    jsonReader.skipValue();
                }
                if (isDbUpdated) {
                    session.getApplicationContext().getContentResolver().notifyChange(new DealsContentProvider(session).createAllDealsUri(), null);
                }
                return responseTemplate;
            }
        }, new Response());
    }

    @NonNull
    private ApiUrlBuilder buildEndpointUrl(@NonNull Session session, long stageId, long filterId, long userId, long start, long limit) {
        ApiUrlBuilder urlBuilder = new ApiUrlBuilder(session, ApiUrlBuilder.ENDPOINT_PIPELINE_STAGES).dealsForStage(Long.valueOf(stageId), Long.valueOf(start), Long.valueOf(limit));
        if (filterId > 0 || userId > 0) {
            if (filterId > 0) {
                urlBuilder.param("filter_id", String.valueOf(filterId));
            }
            urlBuilder.param(PipeSQLiteHelper.COLUMN_USER_ID, userId > 0 ? String.valueOf(userId) : "all");
        } else {
            urlBuilder.param("everyone", "1");
        }
        return urlBuilder;
    }
}
