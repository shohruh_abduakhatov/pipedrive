package com.google.android.gms.analytics.internal;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.analytics.CampaignTrackingReceiver;
import com.google.android.gms.analytics.CampaignTrackingService;
import com.google.android.gms.analytics.zza;
import com.google.android.gms.analytics.zze;
import com.google.android.gms.analytics.zzg;
import com.google.android.gms.analytics.zzi;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzms;
import com.google.android.gms.internal.zzmt;
import com.google.android.gms.internal.zzmw;
import com.google.android.gms.internal.zznb;
import com.google.android.gms.internal.zzsz;
import com.pipedrive.util.networking.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

class zzl extends zzd {
    private final zzj dC;
    private final zzah dD;
    private final zzag dE;
    private final zzi dF;
    private long dG = Long.MIN_VALUE;
    private final zzt dH;
    private final zzt dI;
    private final zzal dJ;
    private long dK;
    private boolean dL;
    private boolean mStarted;

    protected zzl(zzf com_google_android_gms_analytics_internal_zzf, zzg com_google_android_gms_analytics_internal_zzg) {
        super(com_google_android_gms_analytics_internal_zzf);
        zzaa.zzy(com_google_android_gms_analytics_internal_zzg);
        this.dE = com_google_android_gms_analytics_internal_zzg.zzk(com_google_android_gms_analytics_internal_zzf);
        this.dC = com_google_android_gms_analytics_internal_zzg.zzm(com_google_android_gms_analytics_internal_zzf);
        this.dD = com_google_android_gms_analytics_internal_zzg.zzn(com_google_android_gms_analytics_internal_zzf);
        this.dF = com_google_android_gms_analytics_internal_zzg.zzo(com_google_android_gms_analytics_internal_zzf);
        this.dJ = new zzal(zzabz());
        this.dH = new zzt(this, com_google_android_gms_analytics_internal_zzf) {
            final /* synthetic */ zzl dM;

            public void run() {
                this.dM.zzadj();
            }
        };
        this.dI = new zzt(this, com_google_android_gms_analytics_internal_zzf) {
            final /* synthetic */ zzl dM;

            public void run() {
                this.dM.zzadk();
            }
        };
    }

    private void zza(zzh com_google_android_gms_analytics_internal_zzh, zzmt com_google_android_gms_internal_zzmt) {
        zzaa.zzy(com_google_android_gms_analytics_internal_zzh);
        zzaa.zzy(com_google_android_gms_internal_zzmt);
        zza com_google_android_gms_analytics_zza = new zza(zzabx());
        com_google_android_gms_analytics_zza.zzdr(com_google_android_gms_analytics_internal_zzh.zzacs());
        com_google_android_gms_analytics_zza.enableAdvertisingIdCollection(com_google_android_gms_analytics_internal_zzh.zzact());
        zze zzyu = com_google_android_gms_analytics_zza.zzyu();
        zznb com_google_android_gms_internal_zznb = (zznb) zzyu.zzb(zznb.class);
        com_google_android_gms_internal_zznb.zzeh(Response.JSON_PARAM_DATA);
        com_google_android_gms_internal_zznb.zzat(true);
        zzyu.zza((zzg) com_google_android_gms_internal_zzmt);
        zzmw com_google_android_gms_internal_zzmw = (zzmw) zzyu.zzb(zzmw.class);
        zzms com_google_android_gms_internal_zzms = (zzms) zzyu.zzb(zzms.class);
        for (Entry entry : com_google_android_gms_analytics_internal_zzh.zzmc().entrySet()) {
            String str = (String) entry.getKey();
            String str2 = (String) entry.getValue();
            if ("an".equals(str)) {
                com_google_android_gms_internal_zzms.setAppName(str2);
            } else if ("av".equals(str)) {
                com_google_android_gms_internal_zzms.setAppVersion(str2);
            } else if ("aid".equals(str)) {
                com_google_android_gms_internal_zzms.setAppId(str2);
            } else if ("aiid".equals(str)) {
                com_google_android_gms_internal_zzms.setAppInstallerId(str2);
            } else if ("uid".equals(str)) {
                com_google_android_gms_internal_zznb.setUserId(str2);
            } else {
                com_google_android_gms_internal_zzmw.set(str, str2);
            }
        }
        zzb("Sending installation campaign to", com_google_android_gms_analytics_internal_zzh.zzacs(), com_google_android_gms_internal_zzmt);
        zzyu.zzp(zzace().zzago());
        zzyu.zzzm();
    }

    private void zzadh() {
        zzzx();
        Context context = zzabx().getContext();
        if (!zzaj.zzat(context)) {
            zzev("AnalyticsReceiver is not registered or is disabled. Register the receiver for reliable dispatching on non-Google Play devices. See http://goo.gl/8Rd3yj for instructions.");
        } else if (!zzak.zzau(context)) {
            zzew("AnalyticsService is not registered or is disabled. Analytics service at risk of not starting. See http://goo.gl/8Rd3yj for instructions.");
        }
        if (!CampaignTrackingReceiver.zzat(context)) {
            zzev("CampaignTrackingReceiver is not registered, not exported or is disabled. Installation campaign tracking is not possible. See http://goo.gl/8Rd3yj for instructions.");
        } else if (!CampaignTrackingService.zzau(context)) {
            zzev("CampaignTrackingService is not registered or is disabled. Installation campaign tracking is not possible. See http://goo.gl/8Rd3yj for instructions.");
        }
    }

    private void zzadj() {
        zzb(new zzw(this) {
            final /* synthetic */ zzl dM;

            {
                this.dM = r1;
            }

            public void zzf(Throwable th) {
                this.dM.zzadp();
            }
        });
    }

    private void zzadk() {
        try {
            this.dC.zzadb();
            zzadp();
        } catch (SQLiteException e) {
            zzd("Failed to delete stale hits", e);
        }
        zzt com_google_android_gms_analytics_internal_zzt = this.dI;
        zzacb();
        com_google_android_gms_analytics_internal_zzt.zzx(86400000);
    }

    private boolean zzadq() {
        if (this.dL) {
            return false;
        }
        zzacb();
        return zzadw() > 0;
    }

    private void zzadr() {
        zzv zzacd = zzacd();
        if (zzacd.zzafn() && !zzacd.zzfy()) {
            long zzadc = zzadc();
            if (zzadc != 0 && Math.abs(zzabz().currentTimeMillis() - zzadc) <= zzacb().zzaeo()) {
                zza("Dispatch alarm scheduled (ms)", Long.valueOf(zzacb().zzaen()));
                zzacd.schedule();
            }
        }
    }

    private void zzads() {
        zzadr();
        long zzadw = zzadw();
        long zzagq = zzace().zzagq();
        if (zzagq != 0) {
            zzagq = zzadw - Math.abs(zzabz().currentTimeMillis() - zzagq);
            if (zzagq <= 0) {
                zzagq = Math.min(zzacb().zzael(), zzadw);
            }
        } else {
            zzagq = Math.min(zzacb().zzael(), zzadw);
        }
        zza("Dispatch scheduled (ms)", Long.valueOf(zzagq));
        if (this.dH.zzfy()) {
            this.dH.zzy(Math.max(1, zzagq + this.dH.zzafk()));
            return;
        }
        this.dH.zzx(zzagq);
    }

    private void zzadt() {
        zzadu();
        zzadv();
    }

    private void zzadu() {
        if (this.dH.zzfy()) {
            zzes("All hits dispatched or no network/service. Going to power save mode");
        }
        this.dH.cancel();
    }

    private void zzadv() {
        zzv zzacd = zzacd();
        if (zzacd.zzfy()) {
            zzacd.cancel();
        }
    }

    private boolean zzez(String str) {
        return zzsz.zzco(getContext()).checkCallingOrSelfPermission(str) == 0;
    }

    protected void onServiceConnected() {
        zzzx();
        zzacb();
        zzadm();
    }

    void start() {
        zzacj();
        zzaa.zza(!this.mStarted, (Object) "Analytics backend already started");
        this.mStarted = true;
        zzacc().zzg(new Runnable(this) {
            final /* synthetic */ zzl dM;

            {
                this.dM = r1;
            }

            public void run() {
                this.dM.zzadi();
            }
        });
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long zza(zzh com_google_android_gms_analytics_internal_zzh, boolean z) {
        zzaa.zzy(com_google_android_gms_analytics_internal_zzh);
        zzacj();
        zzzx();
        try {
            this.dC.beginTransaction();
            this.dC.zza(com_google_android_gms_analytics_internal_zzh.zzacr(), com_google_android_gms_analytics_internal_zzh.zzze());
            long zza = this.dC.zza(com_google_android_gms_analytics_internal_zzh.zzacr(), com_google_android_gms_analytics_internal_zzh.zzze(), com_google_android_gms_analytics_internal_zzh.zzacs());
            if (z) {
                com_google_android_gms_analytics_internal_zzh.zzr(1 + zza);
            } else {
                com_google_android_gms_analytics_internal_zzh.zzr(zza);
            }
            this.dC.zzb(com_google_android_gms_analytics_internal_zzh);
            this.dC.setTransactionSuccessful();
            try {
                this.dC.endTransaction();
                return zza;
            } catch (SQLiteException e) {
                zze("Failed to end transaction", e);
                return zza;
            }
        } catch (SQLiteException e2) {
            zze("Failed to update Analytics property", e2);
            return -1;
        } catch (Throwable th) {
            try {
                this.dC.endTransaction();
            } catch (SQLiteException e3) {
                zze("Failed to end transaction", e3);
            }
        }
    }

    public void zza(zzab com_google_android_gms_analytics_internal_zzab) {
        zzaa.zzy(com_google_android_gms_analytics_internal_zzab);
        zzi.zzzx();
        zzacj();
        if (this.dL) {
            zzet("Hit delivery not possible. Missing network permissions. See http://goo.gl/8Rd3yj for instructions");
        } else {
            zza("Delivering hit", com_google_android_gms_analytics_internal_zzab);
        }
        zzab zzf = zzf(com_google_android_gms_analytics_internal_zzab);
        zzadl();
        if (this.dF.zzb(zzf)) {
            zzet("Hit sent to the device AnalyticsService for delivery");
            return;
        }
        zzacb();
        try {
            this.dC.zzc(zzf);
            zzadp();
        } catch (SQLiteException e) {
            zze("Delivery failed to save hit to a database", e);
            zzaca().zza(zzf, "deliver: failed to insert hit to database");
        }
    }

    public void zza(zzw com_google_android_gms_analytics_internal_zzw, long j) {
        zzi.zzzx();
        zzacj();
        long j2 = -1;
        long zzagq = zzace().zzagq();
        if (zzagq != 0) {
            j2 = Math.abs(zzabz().currentTimeMillis() - zzagq);
        }
        zzb("Dispatching local hits. Elapsed time since last dispatch (ms)", Long.valueOf(j2));
        zzacb();
        zzadl();
        try {
            zzadn();
            zzace().zzagr();
            zzadp();
            if (com_google_android_gms_analytics_internal_zzw != null) {
                com_google_android_gms_analytics_internal_zzw.zzf(null);
            }
            if (this.dK != j) {
                this.dE.zzagj();
            }
        } catch (Throwable th) {
            zze("Local dispatch failed", th);
            zzace().zzagr();
            zzadp();
            if (com_google_android_gms_analytics_internal_zzw != null) {
                com_google_android_gms_analytics_internal_zzw.zzf(th);
            }
        }
    }

    public void zzabr() {
        zzi.zzzx();
        zzacj();
        zzacb();
        zzes("Delete all hits from local store");
        try {
            this.dC.zzacz();
            this.dC.zzada();
            zzadp();
        } catch (SQLiteException e) {
            zzd("Failed to delete hits from store", e);
        }
        zzadl();
        if (this.dF.zzacv()) {
            zzes("Device service unavailable. Can't clear hits stored on the device service.");
        }
    }

    public void zzabu() {
        zzi.zzzx();
        zzacj();
        zzes("Service disconnected");
    }

    void zzabw() {
        zzzx();
        this.dK = zzabz().currentTimeMillis();
    }

    public long zzadc() {
        zzi.zzzx();
        zzacj();
        try {
            return this.dC.zzadc();
        } catch (SQLiteException e) {
            zze("Failed to get min/max hit times from local store", e);
            return 0;
        }
    }

    protected void zzadi() {
        zzacj();
        zzacb();
        zzadh();
        zzace().zzago();
        if (!zzez("android.permission.ACCESS_NETWORK_STATE")) {
            zzew("Missing required android.permission.ACCESS_NETWORK_STATE. Google Analytics disabled. See http://goo.gl/8Rd3yj for instructions");
            zzadx();
        }
        if (!zzez("android.permission.INTERNET")) {
            zzew("Missing required android.permission.INTERNET. Google Analytics disabled. See http://goo.gl/8Rd3yj for instructions");
            zzadx();
        }
        if (zzak.zzau(getContext())) {
            zzes("AnalyticsService registered in the app manifest and enabled");
        } else {
            zzacb();
            zzev("AnalyticsService not registered in the app manifest. Hits might not be delivered reliably. See http://goo.gl/8Rd3yj for instructions.");
        }
        if (!this.dL) {
            zzacb();
            if (!this.dC.isEmpty()) {
                zzadl();
            }
        }
        zzadp();
    }

    protected void zzadl() {
        if (!this.dL && zzacb().zzaeg() && !this.dF.isConnected()) {
            if (this.dJ.zzz(zzacb().zzafb())) {
                this.dJ.start();
                zzes("Connecting to service");
                if (this.dF.connect()) {
                    zzes("Connected to service");
                    this.dJ.clear();
                    onServiceConnected();
                }
            }
        }
    }

    public void zzadm() {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxOverflowException: Regions stack size limit reached
	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:37)
	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:61)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:33)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r6 = this;
        com.google.android.gms.analytics.zzi.zzzx();
        r6.zzacj();
        r6.zzaby();
        r0 = r6.zzacb();
        r0 = r0.zzaeg();
        if (r0 != 0) goto L_0x0018;
    L_0x0013:
        r0 = "Service client disabled. Can't dispatch local hits to device AnalyticsService";
        r6.zzev(r0);
    L_0x0018:
        r0 = r6.dF;
        r0 = r0.isConnected();
        if (r0 != 0) goto L_0x0026;
    L_0x0020:
        r0 = "Service not connected";
        r6.zzes(r0);
    L_0x0025:
        return;
    L_0x0026:
        r0 = r6.dC;
        r0 = r0.isEmpty();
        if (r0 != 0) goto L_0x0025;
    L_0x002e:
        r0 = "Dispatching local hits to device AnalyticsService";
        r6.zzes(r0);
    L_0x0033:
        r0 = r6.dC;	 Catch:{ SQLiteException -> 0x004c }
        r1 = r6.zzacb();	 Catch:{ SQLiteException -> 0x004c }
        r1 = r1.zzaep();	 Catch:{ SQLiteException -> 0x004c }
        r2 = (long) r1;	 Catch:{ SQLiteException -> 0x004c }
        r1 = r0.zzt(r2);	 Catch:{ SQLiteException -> 0x004c }
        r0 = r1.isEmpty();	 Catch:{ SQLiteException -> 0x004c }
        if (r0 == 0) goto L_0x0062;	 Catch:{ SQLiteException -> 0x004c }
    L_0x0048:
        r6.zzadp();	 Catch:{ SQLiteException -> 0x004c }
        goto L_0x0025;
    L_0x004c:
        r0 = move-exception;
        r1 = "Failed to read hits from store";
        r6.zze(r1, r0);
        r6.zzadt();
        goto L_0x0025;
    L_0x0056:
        r1.remove(r0);
        r2 = r6.dC;	 Catch:{ SQLiteException -> 0x007b }
        r4 = r0.zzafz();	 Catch:{ SQLiteException -> 0x007b }
        r2.zzu(r4);	 Catch:{ SQLiteException -> 0x007b }
    L_0x0062:
        r0 = r1.isEmpty();
        if (r0 != 0) goto L_0x0033;
    L_0x0068:
        r0 = 0;
        r0 = r1.get(r0);
        r0 = (com.google.android.gms.analytics.internal.zzab) r0;
        r2 = r6.dF;
        r2 = r2.zzb(r0);
        if (r2 != 0) goto L_0x0056;
    L_0x0077:
        r6.zzadp();
        goto L_0x0025;
    L_0x007b:
        r0 = move-exception;
        r1 = "Failed to remove hit that was send for delivery";
        r6.zze(r1, r0);
        r6.zzadt();
        goto L_0x0025;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.analytics.internal.zzl.zzadm():void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected boolean zzadn() {
        boolean z;
        boolean z2 = true;
        zzi.zzzx();
        zzacj();
        zzes("Dispatching a batch of local hits");
        if (this.dF.isConnected()) {
            z = false;
        } else {
            zzacb();
            z = true;
        }
        if (this.dD.zzagk()) {
            z2 = false;
        }
        if (!z || !r1) {
            List<zzab> zzt;
            long max = (long) Math.max(zzacb().zzaep(), zzacb().zzaeq());
            List arrayList = new ArrayList();
            long j = 0;
            while (true) {
                this.dC.beginTransaction();
                arrayList.clear();
                try {
                    zzt = this.dC.zzt(max);
                    if (zzt.isEmpty()) {
                        break;
                    }
                    zzab com_google_android_gms_analytics_internal_zzab;
                    long j2;
                    zza("Hits loaded from store. count", Integer.valueOf(zzt.size()));
                    for (zzab com_google_android_gms_analytics_internal_zzab2 : zzt) {
                        if (com_google_android_gms_analytics_internal_zzab2.zzafz() == j) {
                            break;
                        }
                    }
                    if (this.dF.isConnected()) {
                        zzacb();
                        zzes("Service connected, sending hits to the service");
                        while (!zzt.isEmpty()) {
                            com_google_android_gms_analytics_internal_zzab2 = (zzab) zzt.get(0);
                            if (!this.dF.zzb(com_google_android_gms_analytics_internal_zzab2)) {
                                j2 = j;
                                break;
                            }
                            j = Math.max(j, com_google_android_gms_analytics_internal_zzab2.zzafz());
                            zzt.remove(com_google_android_gms_analytics_internal_zzab2);
                            zzb("Hit sent do device AnalyticsService for delivery", com_google_android_gms_analytics_internal_zzab2);
                            try {
                                this.dC.zzu(com_google_android_gms_analytics_internal_zzab2.zzafz());
                                arrayList.add(Long.valueOf(com_google_android_gms_analytics_internal_zzab2.zzafz()));
                            } catch (SQLiteException e) {
                                zze("Failed to remove hit that was send for delivery", e);
                                zzadt();
                                try {
                                    this.dC.setTransactionSuccessful();
                                    this.dC.endTransaction();
                                } catch (SQLiteException e2) {
                                    zze("Failed to commit local dispatch transaction", e2);
                                    zzadt();
                                }
                            }
                        }
                    }
                    j2 = j;
                    if (this.dD.zzagk()) {
                        Collection<Long> zzt2 = this.dD.zzt(zzt);
                        j = j2;
                        for (Long longValue : zzt2) {
                            j = Math.max(j, longValue.longValue());
                        }
                        try {
                            this.dC.zzr(zzt2);
                            arrayList.addAll(zzt2);
                            j2 = j;
                        } catch (SQLiteException e22) {
                            zze("Failed to remove successfully uploaded hits", e22);
                            zzadt();
                            try {
                                this.dC.setTransactionSuccessful();
                                this.dC.endTransaction();
                            } catch (SQLiteException e222) {
                                zze("Failed to commit local dispatch transaction", e222);
                                zzadt();
                            }
                        }
                    }
                    if (arrayList.isEmpty()) {
                        try {
                            break;
                        } catch (SQLiteException e2222) {
                            zze("Failed to commit local dispatch transaction", e2222);
                            zzadt();
                        }
                    } else {
                        try {
                            this.dC.setTransactionSuccessful();
                            this.dC.endTransaction();
                            j = j2;
                        } catch (SQLiteException e22222) {
                            zze("Failed to commit local dispatch transaction", e22222);
                            zzadt();
                        }
                    }
                } catch (SQLiteException e222222) {
                    zzd("Failed to read hits from persisted store", e222222);
                    zzadt();
                    return false;
                } finally {
                    try {
                        this.dC.setTransactionSuccessful();
                        this.dC.endTransaction();
                    } catch (SQLiteException e2222222) {
                        zze("Failed to commit local dispatch transaction", e2222222);
                        zzadt();
                    }
                }
            }
            zzd("Database contains successfully uploaded hit", Long.valueOf(j), Integer.valueOf(zzt.size()));
            zzadt();
            break;
        }
        zzes("No network or service available. Will retry later");
        return false;
    }

    public void zzado() {
        zzi.zzzx();
        zzacj();
        zzet("Sync dispatching local hits");
        long j = this.dK;
        zzacb();
        zzadl();
        try {
            zzadn();
            zzace().zzagr();
            zzadp();
            if (this.dK != j) {
                this.dE.zzagj();
            }
        } catch (Throwable th) {
            zze("Sync local dispatch failed", th);
            zzadp();
        }
    }

    public void zzadp() {
        zzabx().zzzx();
        zzacj();
        if (!zzadq()) {
            this.dE.unregister();
            zzadt();
        } else if (this.dC.isEmpty()) {
            this.dE.unregister();
            zzadt();
        } else {
            boolean z;
            if (((Boolean) zzy.eU.get()).booleanValue()) {
                z = true;
            } else {
                this.dE.zzagh();
                z = this.dE.isConnected();
            }
            if (z) {
                zzads();
                return;
            }
            zzadt();
            zzadr();
        }
    }

    public long zzadw() {
        if (this.dG != Long.MIN_VALUE) {
            return this.dG;
        }
        return zzzh().zzafu() ? ((long) zzzh().zzahl()) * 1000 : zzacb().zzaem();
    }

    public void zzadx() {
        zzacj();
        zzzx();
        this.dL = true;
        this.dF.disconnect();
        zzadp();
    }

    public void zzaw(boolean z) {
        zzadp();
    }

    public void zzb(zzw com_google_android_gms_analytics_internal_zzw) {
        zza(com_google_android_gms_analytics_internal_zzw, this.dK);
    }

    protected void zzc(zzh com_google_android_gms_analytics_internal_zzh) {
        zzzx();
        zzb("Sending first hit to property", com_google_android_gms_analytics_internal_zzh.zzacs());
        if (!zzace().zzagp().zzz(zzacb().zzafi())) {
            String zzags = zzace().zzags();
            if (!TextUtils.isEmpty(zzags)) {
                zzmt zza = zzao.zza(zzaca(), zzags);
                zzb("Found relevant installation campaign", zza);
                zza(com_google_android_gms_analytics_internal_zzh, zza);
            }
        }
    }

    zzab zzf(zzab com_google_android_gms_analytics_internal_zzab) {
        if (!TextUtils.isEmpty(com_google_android_gms_analytics_internal_zzab.zzage())) {
            return com_google_android_gms_analytics_internal_zzab;
        }
        Pair zzagw = zzace().zzagt().zzagw();
        if (zzagw == null) {
            return com_google_android_gms_analytics_internal_zzab;
        }
        Long l = (Long) zzagw.second;
        String str = (String) zzagw.first;
        String valueOf = String.valueOf(l);
        valueOf = new StringBuilder((String.valueOf(valueOf).length() + 1) + String.valueOf(str).length()).append(valueOf).append(":").append(str).toString();
        Map hashMap = new HashMap(com_google_android_gms_analytics_internal_zzab.zzmc());
        hashMap.put("_m", valueOf);
        return zzab.zza(this, com_google_android_gms_analytics_internal_zzab, hashMap);
    }

    public void zzfa(String str) {
        zzaa.zzib(str);
        zzzx();
        zzaby();
        zzmt zza = zzao.zza(zzaca(), str);
        if (zza == null) {
            zzd("Parsing failed. Ignoring invalid campaign data", str);
            return;
        }
        CharSequence zzags = zzace().zzags();
        if (str.equals(zzags)) {
            zzev("Ignoring duplicate install campaign");
        } else if (TextUtils.isEmpty(zzags)) {
            zzace().zzff(str);
            if (zzace().zzagp().zzz(zzacb().zzafi())) {
                zzd("Campaign received too late, ignoring", zza);
                return;
            }
            zzb("Received installation campaign", zza);
            for (zzh zza2 : this.dC.zzv(0)) {
                zza(zza2, zza);
            }
        } else {
            zzd("Ignoring multiple install campaigns. original, new", zzags, str);
        }
    }

    public void zzw(long j) {
        zzi.zzzx();
        zzacj();
        if (j < 0) {
            j = 0;
        }
        this.dG = j;
        zzadp();
    }

    protected void zzzy() {
        this.dC.initialize();
        this.dD.initialize();
        this.dF.initialize();
    }
}
