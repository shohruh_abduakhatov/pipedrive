package com.pipedrive.util;

import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;

public enum ImageUtil {
    ;

    @Nullable
    public static Drawable getTintedDrawable(@Nullable Drawable drawable, @ColorInt int tintColor) {
        if (drawable == null) {
            return null;
        }
        Drawable mutatedDrawable = drawable.mutate();
        mutatedDrawable.setColorFilter(tintColor, Mode.SRC_IN);
        return mutatedDrawable;
    }
}
