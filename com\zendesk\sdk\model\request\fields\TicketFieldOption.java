package com.zendesk.sdk.model.request.fields;

public class TicketFieldOption {
    private long id;
    private boolean isDefault;
    private String name;
    private String value;

    public static TicketFieldOption create(RawTicketFieldOption rawTicketFieldOption) {
        return new TicketFieldOption(rawTicketFieldOption.getId(), rawTicketFieldOption.getName(), rawTicketFieldOption.getValue(), rawTicketFieldOption.isDefault());
    }

    public TicketFieldOption(long id, String name, String value, boolean isDefault) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.isDefault = isDefault;
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getValue() {
        return this.value;
    }

    public boolean isDefault() {
        return this.isDefault;
    }
}
