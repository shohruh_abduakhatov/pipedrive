package com.google.android.gms.internal;

import android.accounts.Account;
import android.os.RemoteException;
import com.google.android.gms.auth.account.WorkAccount;
import com.google.android.gms.auth.account.WorkAccountApi;
import com.google.android.gms.auth.account.WorkAccountApi.AddAccountResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;

public class zznq implements WorkAccountApi {
    private static final Status hS = new Status(13);

    static class zza extends com.google.android.gms.auth.account.zza.zza {
        zza() {
        }

        public void zzbc(boolean z) {
            throw new UnsupportedOperationException();
        }

        public void zzd(Account account) {
            throw new UnsupportedOperationException();
        }
    }

    static class zzb implements AddAccountResult {
        private final Account gj;
        private final Status hv;

        public zzb(Status status, Account account) {
            this.hv = status;
            this.gj = account;
        }

        public Account getAccount() {
            return this.gj;
        }

        public Status getStatus() {
            return this.hv;
        }
    }

    static class zzc implements Result {
        private final Status hv;

        public zzc(Status status) {
            this.hv = status;
        }

        public Status getStatus() {
            return this.hv;
        }
    }

    public PendingResult<AddAccountResult> addWorkAccount(GoogleApiClient googleApiClient, final String str) {
        return googleApiClient.zzb(new com.google.android.gms.internal.zzqo.zza<AddAccountResult, zznr>(this, WorkAccount.API, googleApiClient) {
            final /* synthetic */ zznq hU;

            protected void zza(zznr com_google_android_gms_internal_zznr) throws RemoteException {
                ((com.google.android.gms.auth.account.zzb) com_google_android_gms_internal_zznr.zzavg()).zza(new zza(this) {
                    final /* synthetic */ AnonymousClass2 hV;

                    {
                        this.hV = r1;
                    }

                    public void zzd(Account account) {
                        this.hV.zzc(new zzb(account != null ? Status.xZ : zznq.hS, account));
                    }
                }, str);
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzf(status);
            }

            protected AddAccountResult zzf(Status status) {
                return new zzb(status, null);
            }
        });
    }

    public PendingResult<Result> removeWorkAccount(GoogleApiClient googleApiClient, final Account account) {
        return googleApiClient.zzb(new com.google.android.gms.internal.zzqo.zza<Result, zznr>(this, WorkAccount.API, googleApiClient) {
            final /* synthetic */ zznq hU;

            protected void zza(zznr com_google_android_gms_internal_zznr) throws RemoteException {
                ((com.google.android.gms.auth.account.zzb) com_google_android_gms_internal_zznr.zzavg()).zza(new zza(this) {
                    final /* synthetic */ AnonymousClass3 hW;

                    {
                        this.hW = r1;
                    }

                    public void zzbc(boolean z) {
                        this.hW.zzc(new zzc(z ? Status.xZ : zznq.hS));
                    }
                }, account);
            }

            protected Result zzc(Status status) {
                return new zzc(status);
            }
        });
    }

    public void setWorkAuthenticatorEnabled(GoogleApiClient googleApiClient, final boolean z) {
        googleApiClient.zzb(new com.google.android.gms.internal.zzqo.zza<Result, zznr>(this, WorkAccount.API, googleApiClient) {
            final /* synthetic */ zznq hU;

            protected void zza(zznr com_google_android_gms_internal_zznr) throws RemoteException {
                ((com.google.android.gms.auth.account.zzb) com_google_android_gms_internal_zznr.zzavg()).zzbd(z);
            }

            protected Result zzc(Status status) {
                return null;
            }
        });
    }
}
