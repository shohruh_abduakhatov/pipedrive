package com.pipedrive.fragments.customfields;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.pipedrive.R;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.contentproviders.OrganizationContentProvider;
import com.pipedrive.contentproviders.PersonsContentProvider;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.linking.OrganizationLinkingActivity;
import com.pipedrive.linking.PersonLinkingActivity;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.store.StoreOrganization;
import com.pipedrive.store.StorePerson;
import com.pipedrive.util.CustomFieldsUtil;
import com.pipedrive.util.ViewUtil;

public class CustomFieldEditPersonOrgFragment extends CustomFieldBaseFragment {
    private static final int LINK_ORGANIZATION_REQUEST_CODE = 2;
    private static final int LINK_PERSON_REQUEST_CODE = 1;
    private ImageView mImgRowPerson;
    private TextView mLabelPersonOrgName;
    @Nullable
    private Organization mOrg;
    @Nullable
    private Person mPerson;
    private View mRowPersonOrg;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_custom_field_edit_person_org, container, false);
        this.mLabelPersonOrgName = (TextView) mainView.findViewById(R.id.labelPersonName);
        this.mImgRowPerson = (ImageView) mainView.findViewById(R.id.imgRowPerson);
        this.mRowPersonOrg = mainView.findViewById(R.id.rowPersonOrg);
        return mainView;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        String fieldDataType = this.mCustomField.getFieldDataType();
        if ("organization".equalsIgnoreCase(fieldDataType)) {
            if (!TextUtils.isEmpty(this.mCustomField.getTempValue())) {
                Uri orgUri = OrganizationContentProvider.getOrganizationUriFromCustomField(this.mCustomField, PipedriveApp.getActiveSession(), CustomFieldsUtil.getIdFromCustomField(this.mCustomField));
                if (orgUri != null) {
                    this.mOrg = OrganizationContentProvider.getOrganizationFromUri(getActivity().getContentResolver(), orgUri);
                }
            }
            updateOrgData();
        } else if ("person".equalsIgnoreCase(fieldDataType)) {
            if (!TextUtils.isEmpty(this.mCustomField.getTempValue())) {
                Uri contactUri = PersonsContentProvider.getPersonUriFromCustomField(this.mCustomField, PipedriveApp.getActiveSession(), CustomFieldsUtil.getIdFromCustomField(this.mCustomField));
                if (contactUri != null) {
                    this.mPerson = PersonsContentProvider.getPersonDataFromUri(getActivity().getContentResolver(), contactUri);
                }
            }
            updatePersonData();
        }
        this.mRowPersonOrg.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                CustomFieldEditPersonOrgFragment.this.associateOrg();
            }
        });
    }

    protected void clearContent() {
        this.mOrg = null;
        this.mPerson = null;
    }

    protected void saveContent() {
        String fieldDataType = this.mCustomField.getFieldDataType();
        if ("organization".equalsIgnoreCase(fieldDataType)) {
            if (this.mOrg != null) {
                if (!this.mOrg.isStored()) {
                    saveOrganization();
                }
                this.mCustomField.setTempValue(CustomField.LOCAL_DATABASE_ID_PREFIX + this.mOrg.getSqlId());
                return;
            }
            this.mCustomField.setTempValue(null);
        } else if (!"person".equalsIgnoreCase(fieldDataType)) {
        } else {
            if (this.mPerson != null) {
                if (!this.mPerson.isStored()) {
                    savePerson();
                }
                this.mCustomField.setTempValue(CustomField.LOCAL_DATABASE_ID_PREFIX + this.mPerson.getSqlId());
                return;
            }
            this.mCustomField.setTempValue(null);
        }
    }

    public boolean isAllRequiredFieldsSet() {
        String fieldDataType = this.mCustomField.getFieldDataType();
        if ("organization".equalsIgnoreCase(fieldDataType)) {
            if (!(this.mOrg == null || this.mOrg.isStored())) {
                return saveOrganization();
            }
        } else if (!(!"person".equalsIgnoreCase(fieldDataType) || this.mPerson == null || this.mPerson.isStored())) {
            return savePerson();
        }
        return true;
    }

    private boolean saveOrganization() {
        boolean saveOk = new StoreOrganization(PipedriveApp.getActiveSession()).create(this.mOrg);
        if (!saveOk) {
            ViewUtil.showErrorToast(PipedriveApp.getActiveSession().getApplicationContext(), R.string.error_organization_save_failed);
        }
        return saveOk;
    }

    private boolean savePerson() {
        boolean saveOk = new StorePerson(PipedriveApp.getActiveSession()).create(this.mPerson);
        if (!saveOk) {
            ViewUtil.showErrorToast(PipedriveApp.getActiveSession().getApplicationContext(), R.string.error_contact_save_failed);
        }
        return saveOk;
    }

    private void associateOrg() {
        if (!"person".equalsIgnoreCase(this.mCustomField.getFieldDataType())) {
            OrganizationLinkingActivity.startActivityForResult(getActivity(), 2);
        } else if (getActivity() != null) {
            PersonLinkingActivity.startActivityForResult(getActivity(), 1);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Session session = PipedriveApp.getActiveSession();
        if (session != null) {
            if (requestCode == 1 && resultCode == -1) {
                this.mPerson = (Person) new PersonsDataSource(session.getDatabase()).findBySqlId(data.getLongExtra("PERSON_SQL_ID", 0));
                updatePersonData();
            }
            if (requestCode == 2 && resultCode == -1) {
                this.mOrg = (Organization) new OrganizationsDataSource(session.getDatabase()).findBySqlId(data.getLongExtra(OrganizationLinkingActivity.KEY_ORG_SQL_ID, 0));
                updateOrgData();
            }
        }
    }

    private void updateOrgData() {
        this.mImgRowPerson.setImageResource(R.drawable.icon_organization);
        if (this.mOrg != null) {
            this.mLabelPersonOrgName.setText(this.mOrg.getName());
        } else {
            this.mLabelPersonOrgName.setText(R.string.lbl_choose_organization);
        }
    }

    private void updatePersonData() {
        this.mImgRowPerson.setImageResource(R.drawable.icon_person);
        if (this.mPerson != null) {
            this.mLabelPersonOrgName.setText(this.mPerson.getName());
        } else {
            this.mLabelPersonOrgName.setText(R.string.lbl_choose_contact);
        }
    }
}
