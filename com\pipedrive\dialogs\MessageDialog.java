package com.pipedrive.dialogs;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

public class MessageDialog extends DialogFragment {
    private static final String KEY_MESSAGE_RES_ID = "MESSAGE_RES_ID";
    private static final String TAG = "MessageDialog";

    public static void showMessage(@NonNull FragmentManager fragmentManager, @StringRes int messageResId) {
        Bundle args = new Bundle();
        args.putInt(KEY_MESSAGE_RES_ID, messageResId);
        MessageDialog messageDialog = new MessageDialog();
        messageDialog.setArguments(args);
        messageDialog.show(fragmentManager, TAG);
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Builder(getActivity()).setMessage(getArguments().getInt(KEY_MESSAGE_RES_ID)).setPositiveButton(17039370, new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create();
    }
}
