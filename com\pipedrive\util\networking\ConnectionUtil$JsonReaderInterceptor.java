package com.pipedrive.util.networking;

import android.support.annotation.NonNull;
import com.google.gson.stream.JsonReader;
import com.pipedrive.application.Session;
import java.io.IOException;

public interface ConnectionUtil$JsonReaderInterceptor<T extends Response> {
    T interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull T t) throws IOException;
}
