package com.pipedrive.datasource.nearby;

import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.maps.model.LatLng;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.BaseDataSource;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.datasource.SQLTransactionManager;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Deal;
import com.pipedrive.model.DealStatus;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.nearby.model.DealNearbyItem;
import com.pipedrive.nearby.model.NearbyItem;
import com.pipedrive.nearby.model.NearbyItemType;
import com.pipedrive.nearby.model.OrganizationNearbyItem;
import com.pipedrive.nearby.model.PersonNearbyItem;
import com.pipedrive.nearby.util.Distance;
import com.pipedrive.nearby.util.Longitude;
import java.util.ArrayList;
import java.util.List;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.observables.GroupedObservable;
import rx.schedulers.Schedulers;

public class NearbyDataSource extends SQLTransactionManager {
    private static final Integer INITIAL_ITEM_LIMIT = Integer.valueOf(20);
    private static final Double INITIAL_MAX_DISTANCE_METERS = Distance.KILOMETERS.toMeters(Double.valueOf(10.0d));

    private abstract class GetDataSource {
        @NonNull
        abstract BaseDataSource<? extends BaseDatasourceEntity> call(@NonNull Session session);

        private GetDataSource() {
        }
    }

    private static abstract class GetOrganization<ENTITY extends BaseDatasourceEntity> {
        @Nullable
        abstract Organization getOrganization(@NonNull ENTITY entity);

        private GetOrganization() {
        }

        @NonNull
        Boolean hasAddress(@NonNull ENTITY entity) {
            Organization organization = getOrganization(entity);
            boolean z = (organization == null || organization.getAddress() == null || organization.getAddressLongitude() == null || organization.getAddressLatitude() == null) ? false : true;
            return Boolean.valueOf(z);
        }
    }

    private abstract class CreateNearbyItem {
        @NonNull
        abstract NearbyItem createNearbyItem(@NonNull Double d, @NonNull Double d2, @NonNull String str, @NonNull List<Long> list);

        private CreateNearbyItem() {
        }

        @NonNull
        NearbyItem createNearbyItem(@NonNull List<NearbyItemData> groupedItems) {
            LatLng location = ((NearbyItemData) groupedItems.get(0)).getLocation();
            String address = ((NearbyItemData) groupedItems.get(0)).getAddress();
            List<Long> dealIds = new ArrayList(groupedItems.size());
            for (NearbyItemData nearbyItemData : groupedItems) {
                dealIds.add(nearbyItemData.getSqlId());
            }
            return createNearbyItem(Double.valueOf(location.latitude), Double.valueOf(location.longitude), address, dealIds);
        }
    }

    private static abstract class GetEntityStatus<ENTITY extends BaseDatasourceEntity> {
        @NonNull
        abstract Boolean isActive(@NonNull ENTITY entity);

        private GetEntityStatus() {
        }
    }

    public NearbyDataSource(SQLiteDatabase dbconn) {
        super(dbconn);
    }

    @NonNull
    public Double getMaxDistanceToNearbyItemForInitialZoom(@NonNull Session session, @NonNull NearbyItemType nearbyItemType, @NonNull final Double currentLatitude, @NonNull final Double currentLongitude) {
        return (Double) getNearbyItemsWithAddress(session, nearbyItemType, currentLatitude, currentLongitude).filter(new Func1<NearbyItem, Boolean>() {
            public Boolean call(NearbyItem nearbyDeal) {
                return Boolean.valueOf(nearbyDeal.getDistanceTo(currentLatitude, currentLongitude).doubleValue() <= NearbyDataSource.INITIAL_MAX_DISTANCE_METERS.doubleValue());
            }
        }).take(INITIAL_ITEM_LIMIT.intValue()).toList().map(new Func1<List<NearbyItem>, Double>() {
            public Double call(List<NearbyItem> nearbyDeals) {
                if (nearbyDeals.isEmpty()) {
                    return NearbyDataSource.INITIAL_MAX_DISTANCE_METERS;
                }
                if (nearbyDeals.size() == NearbyDataSource.INITIAL_ITEM_LIMIT.intValue()) {
                    return ((NearbyItem) nearbyDeals.get(nearbyDeals.size() - 1)).getDistanceTo(currentLatitude, currentLongitude);
                }
                return NearbyDataSource.INITIAL_MAX_DISTANCE_METERS;
            }
        }).toBlocking().single();
    }

    @NonNull
    public Observable<NearbyItem> getInitialNearbyItems(@NonNull Session session, @NonNull NearbyItemType nearbyItemType, @NonNull final Double currentLatitude, @NonNull final Double currentLongitude) {
        return getNearbyItemsWithAddress(session, nearbyItemType, currentLatitude, currentLongitude).filter(new Func1<NearbyItem, Boolean>() {
            public Boolean call(NearbyItem nearbyDeal) {
                return Boolean.valueOf(nearbyDeal.getDistanceTo(currentLatitude, currentLongitude).doubleValue() <= NearbyDataSource.INITIAL_MAX_DISTANCE_METERS.doubleValue());
            }
        }).take(INITIAL_ITEM_LIMIT.intValue());
    }

    @NonNull
    public Observable<NearbyItem> getNearbyItems(@NonNull Session session, @NonNull NearbyItemType nearbyItemType, @NonNull Double currentLatitude, @NonNull Double currentLongitude, @NonNull Double southLatitude, @NonNull Double northLatitude, @NonNull Double westLongitude, @NonNull Double eastLongitude) {
        final Double d = westLongitude;
        final Double d2 = eastLongitude;
        final Double d3 = southLatitude;
        final Double d4 = northLatitude;
        return getNearbyItemsWithAddress(session, nearbyItemType, currentLatitude, currentLongitude).filter(new Func1<NearbyItem, Boolean>() {
            public Boolean call(NearbyItem nearbyDeal) {
                boolean z = true;
                if (d.doubleValue() <= 0.0d || d2.doubleValue() >= 0.0d) {
                    if (nearbyDeal.getLatitude().doubleValue() < d3.doubleValue() || nearbyDeal.getLatitude().doubleValue() > d4.doubleValue() || nearbyDeal.getLongitude().doubleValue() < d.doubleValue() || nearbyDeal.getLongitude().doubleValue() > d2.doubleValue()) {
                        z = false;
                    }
                    return Boolean.valueOf(z);
                }
                if (nearbyDeal.getLatitude().doubleValue() < d3.doubleValue() || nearbyDeal.getLatitude().doubleValue() > d4.doubleValue() || ((nearbyDeal.getLongitude().doubleValue() < d.doubleValue() || nearbyDeal.getLongitude().doubleValue() > Longitude.MAX_VALUE.doubleValue()) && (nearbyDeal.getLongitude().doubleValue() < Longitude.MIN_VALUE.doubleValue() || nearbyDeal.getLongitude().doubleValue() > d2.doubleValue()))) {
                    z = false;
                }
                return Boolean.valueOf(z);
            }
        });
    }

    @NonNull
    private Observable<NearbyItem> getNearbyItemsWithAddress(@NonNull Session session, @NonNull NearbyItemType nearbyItemType, @NonNull final Double currentLatitude, @NonNull final Double currentLongitude) {
        GetDataSource getDataSource = getDataSource(nearbyItemType);
        final GetOrganization getOrganization = getOrganization(nearbyItemType);
        final CreateNearbyItem createNearbyItem = createNearbyItem(nearbyItemType);
        final GetEntityStatus getEntityStatus = getStatus(nearbyItemType);
        if (getDataSource == null || getOrganization == null || createNearbyItem == null || getEntityStatus == null) {
            return Observable.empty();
        }
        return Observable.from(getDataSource.call(session).findAll()).observeOn(Schedulers.io()).filter(new Func1<BaseDatasourceEntity, Boolean>() {
            public Boolean call(BaseDatasourceEntity entity) {
                boolean z = entity != null && entity.getSqlIdOrNull() != null && getOrganization.hasAddress(entity).booleanValue() && getEntityStatus.isActive(entity).booleanValue();
                return Boolean.valueOf(z);
            }
        }).map(new Func1<BaseDatasourceEntity, NearbyItemData>() {
            public NearbyItemData call(BaseDatasourceEntity entity) {
                Organization organization = getOrganization.getOrganization(entity);
                String address = organization.getAddress();
                Double addressLongitude = organization.getAddressLongitude();
                return new NearbyItemData(entity.getSqlIdOrNull(), organization.getAddressLatitude(), addressLongitude, address);
            }
        }).groupBy(new Func1<NearbyItemData, LatLng>() {
            public LatLng call(NearbyItemData nearbyItemData) {
                return nearbyItemData.getLocation();
            }
        }).flatMap(new Func1<GroupedObservable<LatLng, NearbyItemData>, Observable<List<NearbyItemData>>>() {
            public Observable<List<NearbyItemData>> call(GroupedObservable<LatLng, NearbyItemData> groupedDealsObservable) {
                return groupedDealsObservable.toList();
            }
        }).map(new Func1<List<NearbyItemData>, NearbyItem>() {
            public NearbyItem call(List<NearbyItemData> groupedItems) {
                return createNearbyItem.createNearbyItem(groupedItems);
            }
        }).sorted(new Func2<NearbyItem, NearbyItem, Integer>() {
            public Integer call(NearbyItem nearbyItem, NearbyItem nearbyItem2) {
                return Integer.valueOf(nearbyItem.getDistanceTo(currentLatitude, currentLongitude).compareTo(nearbyItem2.getDistanceTo(currentLatitude, currentLongitude)));
            }
        });
    }

    @Nullable
    private GetDataSource getDataSource(@NonNull NearbyItemType nearbyItemType) {
        switch (nearbyItemType) {
            case DEALS:
                return new GetDataSource() {
                    @NonNull
                    public BaseDataSource<? extends BaseDatasourceEntity> call(@NonNull Session session) {
                        return new DealsDataSource(session.getDatabase());
                    }
                };
            case PERSONS:
                return new GetDataSource() {
                    @NonNull
                    public BaseDataSource<? extends BaseDatasourceEntity> call(@NonNull Session session) {
                        return new PersonsDataSource(session.getDatabase());
                    }
                };
            case ORGANIZATIONS:
                return new GetDataSource() {
                    @NonNull
                    public BaseDataSource<? extends BaseDatasourceEntity> call(@NonNull Session session) {
                        return new OrganizationsDataSource(session.getDatabase());
                    }
                };
            default:
                return null;
        }
    }

    @Nullable
    private GetOrganization getOrganization(@NonNull NearbyItemType nearbyItemType) {
        switch (nearbyItemType) {
            case DEALS:
                return new GetOrganization<Deal>() {
                    @Nullable
                    public Organization getOrganization(@NonNull Deal entity) {
                        if (entity.getOrganization() != null) {
                            return entity.getOrganization();
                        }
                        if (entity.getPerson() == null) {
                            return null;
                        }
                        return entity.getPerson().getCompany();
                    }
                };
            case PERSONS:
                return new GetOrganization<Person>() {
                    @Nullable
                    public Organization getOrganization(@NonNull Person entity) {
                        return entity.getCompany();
                    }
                };
            case ORGANIZATIONS:
                return new GetOrganization<Organization>() {
                    @Nullable
                    public Organization getOrganization(@NonNull Organization entity) {
                        return entity;
                    }
                };
            default:
                return null;
        }
    }

    @Nullable
    private CreateNearbyItem createNearbyItem(@NonNull NearbyItemType nearbyItemType) {
        switch (nearbyItemType) {
            case DEALS:
                return new CreateNearbyItem() {
                    @NonNull
                    NearbyItem createNearbyItem(@NonNull Double latitude, @NonNull Double longitude, @NonNull String address, @NonNull List<Long> itemSqlIds) {
                        return new DealNearbyItem(latitude, longitude, address, (List) itemSqlIds);
                    }
                };
            case PERSONS:
                return new CreateNearbyItem() {
                    @NonNull
                    NearbyItem createNearbyItem(@NonNull Double latitude, @NonNull Double longitude, @NonNull String address, @NonNull List<Long> itemSqlIds) {
                        return new PersonNearbyItem(latitude, longitude, address, (List) itemSqlIds);
                    }
                };
            case ORGANIZATIONS:
                return new CreateNearbyItem() {
                    @NonNull
                    NearbyItem createNearbyItem(@NonNull Double latitude, @NonNull Double longitude, @NonNull String address, @NonNull List<Long> itemSqlIds) {
                        return new OrganizationNearbyItem(latitude, longitude, address, (List) itemSqlIds);
                    }
                };
            default:
                return null;
        }
    }

    @Nullable
    private GetEntityStatus getStatus(@NonNull NearbyItemType nearbyItemType) {
        switch (nearbyItemType) {
            case DEALS:
                return new GetEntityStatus<Deal>() {
                    @NonNull
                    Boolean isActive(@NonNull Deal entity) {
                        return Boolean.valueOf(entity.getStatus() == DealStatus.OPEN);
                    }
                };
            case PERSONS:
                return new GetEntityStatus<Person>() {
                    @NonNull
                    Boolean isActive(@NonNull Person entity) {
                        return Boolean.valueOf(entity.isActive());
                    }
                };
            case ORGANIZATIONS:
                return new GetEntityStatus<Organization>() {
                    @NonNull
                    Boolean isActive(@NonNull Organization entity) {
                        return Boolean.valueOf(entity.isActive());
                    }
                };
            default:
                return null;
        }
    }
}
