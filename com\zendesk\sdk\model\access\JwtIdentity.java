package com.zendesk.sdk.model.access;

import android.support.annotation.Nullable;
import com.zendesk.logger.Logger;
import com.zendesk.util.StringUtils;

public class JwtIdentity implements Identity {
    private static final String LOG_TAG = JwtIdentity.class.getSimpleName();
    private String token;

    public JwtIdentity(String jwtUserIdentifier) {
        if (StringUtils.isEmpty(jwtUserIdentifier)) {
            Logger.e(LOG_TAG, "A null or empty JWT was specified. This will not work. Please check your initialisation of JwtIdentity!", new Object[0]);
        }
        this.token = jwtUserIdentifier;
    }

    @Nullable
    public String getJwtUserIdentifier() {
        return this.token;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        JwtIdentity that = (JwtIdentity) o;
        if (this.token != null) {
            if (this.token.equals(that.token)) {
                return true;
            }
        } else if (that.token == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.token != null ? this.token.hashCode() : 0;
    }
}
