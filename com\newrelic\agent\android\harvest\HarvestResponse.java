package com.newrelic.agent.android.harvest;

import com.zendesk.service.HttpConstants;

public class HarvestResponse {
    private static final String DISABLE_STRING = "DISABLE_NEW_RELIC";
    private String responseBody;
    private long responseTime;
    private int statusCode;

    public enum Code {
        OK(200),
        UNAUTHORIZED(HttpConstants.HTTP_UNAUTHORIZED),
        FORBIDDEN(HttpConstants.HTTP_FORBIDDEN),
        ENTITY_TOO_LARGE(HttpConstants.HTTP_ENTITY_TOO_LARGE),
        INVALID_AGENT_ID(HttpConstants.HTTP_BLOCKED),
        UNSUPPORTED_MEDIA_TYPE(HttpConstants.HTTP_UNSUPPORTED_TYPE),
        INTERNAL_SERVER_ERROR(HttpConstants.HTTP_INTERNAL_ERROR),
        UNKNOWN(-1);
        
        int statusCode;

        private Code(int statusCode) {
            this.statusCode = statusCode;
        }

        public int getStatusCode() {
            return this.statusCode;
        }

        public boolean isError() {
            return this != OK;
        }

        public boolean isOK() {
            return !isError();
        }
    }

    public Code getResponseCode() {
        if (isOK()) {
            return Code.OK;
        }
        for (Code code : Code.values()) {
            if (code.getStatusCode() == this.statusCode) {
                return code;
            }
        }
        return Code.UNKNOWN;
    }

    public boolean isDisableCommand() {
        return Code.FORBIDDEN == getResponseCode() && DISABLE_STRING.equals(getResponseBody());
    }

    public boolean isError() {
        return this.statusCode >= HttpConstants.HTTP_BAD_REQUEST;
    }

    public boolean isUnknown() {
        return getResponseCode() == Code.UNKNOWN;
    }

    public boolean isOK() {
        return !isError();
    }

    public int getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getResponseBody() {
        return this.responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    public long getResponseTime() {
        return this.responseTime;
    }

    public void setResponseTime(long responseTime) {
        this.responseTime = responseTime;
    }
}
