package com.pipedrive.util.networking.entities.product;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class ProductVariationEntity {
    @Nullable
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private Long pipedriveId;
    @NonNull
    @SerializedName("prices")
    @Expose
    private List<ProductVariationPriceEntity> prices = new ArrayList();

    @Nullable
    public Long getPipedriveId() {
        return this.pipedriveId;
    }

    @Nullable
    public String getName() {
        return this.name;
    }

    @NonNull
    public List<ProductVariationPriceEntity> getPrices() {
        return this.prices;
    }

    public String toString() {
        return "ProductVariationEntity{pipedriveId=" + getPipedriveId() + ", name='" + getName() + '\'' + ", prices.length=" + getPrices().size() + '}';
    }
}
