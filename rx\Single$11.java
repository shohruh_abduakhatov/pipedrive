package rx;

import rx.functions.Action1;

class Single$11 extends SingleSubscriber<T> {
    final /* synthetic */ Single this$0;
    final /* synthetic */ Action1 val$onError;
    final /* synthetic */ Action1 val$onSuccess;

    Single$11(Single single, Action1 action1, Action1 action12) {
        this.this$0 = single;
        this.val$onError = action1;
        this.val$onSuccess = action12;
    }

    public final void onError(Throwable e) {
        try {
            this.val$onError.call(e);
        } finally {
            unsubscribe();
        }
    }

    public final void onSuccess(T args) {
        try {
            this.val$onSuccess.call(args);
        } finally {
            unsubscribe();
        }
    }
}
