package com.zendesk.sdk.rating.impl;

import android.content.Context;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.rating.RateMyAppRule;
import com.zendesk.sdk.storage.RateMyAppStorage;

public class MetricsRateMyAppRule implements RateMyAppRule {
    private static final String LOG_TAG = "MetricsRateMyAppRule";
    private final Context mContext;

    public MetricsRateMyAppRule(Context context) {
        this.mContext = context != null ? context.getApplicationContext() : null;
    }

    public boolean permitsShowOfDialog() {
        if (this.mContext == null) {
            return false;
        }
        RateMyAppStorage rateMyAppStorage = new RateMyAppStorage(this.mContext);
        boolean numberOfLaunchesMet = rateMyAppStorage.isNumberOfLaunchesMet();
        boolean launchTimeMet = rateMyAppStorage.isLaunchTimeMet();
        boolean ratedForCurrentVersion = rateMyAppStorage.isRatedForCurrentVersion();
        boolean dontShowAgain = rateMyAppStorage.isDontShowAgain();
        Logger.d(LOG_TAG, "Number of launches met: " + numberOfLaunchesMet, new Object[0]);
        Logger.d(LOG_TAG, "Launch time met: " + launchTimeMet, new Object[0]);
        Logger.d(LOG_TAG, "Rated for current version: " + ratedForCurrentVersion, new Object[0]);
        Logger.d(LOG_TAG, "Don't show again: " + dontShowAgain, new Object[0]);
        if (!numberOfLaunchesMet || !launchTimeMet || ratedForCurrentVersion || dontShowAgain) {
            return false;
        }
        return true;
    }
}
