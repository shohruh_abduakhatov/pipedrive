package com.pipedrive.util.networking;

import android.support.annotation.Nullable;
import okhttp3.Response;

class ConnectionUtil$HttpConnectionResponseHolder {
    @Nullable
    Response response;

    private ConnectionUtil$HttpConnectionResponseHolder() {
        this.response = null;
    }

    void disposeResponse() {
        if (this.response != null && this.response.body() != null) {
            this.response.body().close();
        }
    }
}
