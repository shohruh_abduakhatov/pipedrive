package com.zendesk.sdk.storage;

import android.support.annotation.Nullable;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.access.AccessToken;
import com.zendesk.sdk.model.access.Identity;

class StubIdentityStorage implements IdentityStorage {
    private static final String LOG_TAG = "StubIdentityStorage";

    StubIdentityStorage() {
    }

    public void storeAccessToken(AccessToken accessToken) {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
    }

    public AccessToken getStoredAccessToken() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
        return new AccessToken();
    }

    public String getStoredAccessTokenAsBearerToken() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
        return "";
    }

    public String getUUID() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
        return "";
    }

    public void storeIdentity(Identity identity) {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
    }

    @Nullable
    public Identity getIdentity() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
        return new StubIdentity();
    }

    @Nullable
    public Identity anonymiseIdentity() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
        return new StubIdentity();
    }

    public void clearUserData() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
    }

    public String getCacheKey() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
        return "";
    }
}
