package rx;

import rx.Scheduler.Worker;
import rx.functions.Action0;
import rx.subscriptions.Subscriptions;

class Completable$35 implements Completable$OnSubscribe {
    final /* synthetic */ Completable this$0;
    final /* synthetic */ Scheduler val$scheduler;

    Completable$35(Completable completable, Scheduler scheduler) {
        this.this$0 = completable;
        this.val$scheduler = scheduler;
    }

    public void call(final CompletableSubscriber s) {
        this.this$0.unsafeSubscribe(new CompletableSubscriber() {
            public void onCompleted() {
                s.onCompleted();
            }

            public void onError(Throwable e) {
                s.onError(e);
            }

            public void onSubscribe(final Subscription d) {
                s.onSubscribe(Subscriptions.create(new Action0() {
                    public void call() {
                        final Worker w = Completable$35.this.val$scheduler.createWorker();
                        w.schedule(new Action0() {
                            public void call() {
                                try {
                                    d.unsubscribe();
                                } finally {
                                    w.unsubscribe();
                                }
                            }
                        });
                    }
                }));
            }
        });
    }
}
