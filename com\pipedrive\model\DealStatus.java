package com.pipedrive.model;

import android.support.annotation.Nullable;
import android.text.TextUtils;

public enum DealStatus {
    OPEN,
    WON,
    LOST,
    DELETED;

    @Nullable
    public static DealStatus from(String value) {
        DealStatus dealStatus = null;
        if (!TextUtils.isEmpty(value)) {
            try {
                dealStatus = valueOf(value.toUpperCase());
            } catch (IllegalArgumentException e) {
            }
        }
        return dealStatus;
    }

    public String toString() {
        return super.toString().toLowerCase();
    }
}
