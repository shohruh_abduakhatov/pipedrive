package com.pipedrive.views.agenda;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.pipedrive.AllDayActivityAdapter;
import com.pipedrive.CalendarAnimations;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datetime.DateFormatHelper;
import com.pipedrive.datetime.LocaleHelper;
import com.pipedrive.util.time.TimeManager;
import com.pipedrive.views.agenda.AgendaView.OnCalendarExpandListener;
import java.util.Date;

public class AllDayActivitiesView extends LinearLayout implements OnCalendarExpandListener {
    private AllDayActivityAdapter mAllDayActivitiesAdapter;
    private int mAllDayActivitiesCollapsedHeight;
    private RecyclerView mAllDayActivitiesList;
    private int mAllDayActivitiesListMaxHeight;
    private final int mAllDayActivityContainerItemHeight;
    private final int mAllDayActivityContainerItemPadding;
    private View mContainer;
    private CustomLayoutManager mCustomLayoutManager;
    private Date mDate;
    private TextView mDateTextView;
    private TextView mDayOfWeekTextView;
    private View mDivider;
    private TextView mLoadMoreTextView;

    private class CustomLayoutManager extends LinearLayoutManager {
        private boolean isScrollEnabled = true;

        CustomLayoutManager(Context context) {
            super(context);
        }

        void setScrollEnabled(boolean flag) {
            this.isScrollEnabled = flag;
        }

        public boolean canScrollVertically() {
            return this.isScrollEnabled && super.canScrollVertically();
        }
    }

    public AllDayActivitiesView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mAllDayActivityContainerItemHeight = getContext().getResources().getDimensionPixelSize(R.dimen.alldayActivityContainer.itemHeight);
        this.mAllDayActivityContainerItemPadding = getContext().getResources().getDimensionPixelSize(R.dimen.alldayActivityContainer.itemPadding);
        setup(context);
    }

    public AllDayActivitiesView(Context context) {
        this(context, null);
    }

    public void setDateAndUpdateViews(@NonNull Session session, @NonNull Date date, int allDayActivitiesViewMaxHeight, boolean expand) {
        this.mDate = date;
        this.mAllDayActivitiesListMaxHeight = allDayActivitiesViewMaxHeight;
        refreshContent(session);
        if (expand) {
            expandListWithoutAnimation();
        }
    }

    private void expandListWithoutAnimation() {
        LayoutParams layoutParams = this.mAllDayActivitiesList.getLayoutParams();
        layoutParams.height = getMaxExpandHeight();
        this.mAllDayActivitiesList.setLayoutParams(layoutParams);
        this.mLoadMoreTextView.setText(getContext().getResources().getText(R.string.show_less).toString());
    }

    private Date getDate() {
        if (this.mDate != null) {
            return this.mDate;
        }
        return new Date(TimeManager.getInstance().currentTimeMillis().longValue());
    }

    public boolean isEmpty() {
        return getNumberOfAllDayActivities() == 0;
    }

    private void setDateTextViews(@NonNull Session session, @NonNull Date date) {
        this.mDateTextView.setText(LocaleHelper.getLocaleBasedDateStringInGmtZeroDayOfMonthOnly(session, date));
        this.mDayOfWeekTextView.setText(DateFormatHelper.dateFormat6(com.pipedrive.fragments.LocaleHelper.INSTANCE.getLocale()).format(date));
    }

    private int getAllDayActivitiesCollapsedHeight() {
        return this.mAllDayActivitiesCollapsedHeight;
    }

    private void setAllDayActivitiesCollapsedHeight(int allDayActivitiesCollapsedHeight) {
        this.mAllDayActivitiesCollapsedHeight = allDayActivitiesCollapsedHeight;
    }

    private void setup(Context context) {
        View rootView = getRootView(context);
        this.mDivider = rootView.findViewById(R.id.divider);
        this.mContainer = rootView.findViewById(R.id.container);
        this.mDateTextView = (TextView) rootView.findViewById(R.id.dateTxt);
        this.mDayOfWeekTextView = (TextView) rootView.findViewById(R.id.dayOfWeek);
        this.mLoadMoreTextView = (TextView) rootView.findViewById(R.id.loadMore);
        this.mCustomLayoutManager = new CustomLayoutManager(context);
        this.mAllDayActivitiesList = (RecyclerView) rootView.findViewById(R.id.allDayActivityView);
        this.mAllDayActivitiesList.setLayoutManager(this.mCustomLayoutManager);
        setAllDayActivitiesContainerHeight();
        setupOnItemTouchListeners();
    }

    private View getRootView(Context context) {
        return ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(R.layout.view_agenda_with_alldayactivity_list, this, true);
    }

    private void setAllDayActivitiesContainerHeight() {
        int defaultAllDayContainerCollapsedHeight;
        switch (getNumberOfAllDayActivities()) {
            case 0:
                defaultAllDayContainerCollapsedHeight = 0;
                this.mDivider.setVisibility(8);
                this.mLoadMoreTextView.setVisibility(8);
                this.mContainer.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.transparent));
                break;
            case 1:
                defaultAllDayContainerCollapsedHeight = this.mAllDayActivityContainerItemHeight;
                this.mDivider.setVisibility(0);
                this.mLoadMoreTextView.setVisibility(8);
                this.mContainer.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
                break;
            case 2:
                defaultAllDayContainerCollapsedHeight = (this.mAllDayActivityContainerItemHeight * 2) + this.mAllDayActivityContainerItemPadding;
                this.mDivider.setVisibility(0);
                this.mLoadMoreTextView.setVisibility(8);
                this.mContainer.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
                break;
            default:
                defaultAllDayContainerCollapsedHeight = this.mAllDayActivityContainerItemHeight;
                this.mLoadMoreTextView.setText(getResources().getString(R.string.allDayActivityList.loadMore, new Object[]{Integer.valueOf(getNumberOfAllDayActivities() - 1), getContext().getResources().getText(R.string.n_more)}));
                this.mDivider.setVisibility(0);
                this.mLoadMoreTextView.setVisibility(0);
                this.mContainer.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
                break;
        }
        setAllDayActivitiesCollapsedHeight(defaultAllDayContainerCollapsedHeight);
        setInitialHeight(this.mAllDayActivitiesList, defaultAllDayContainerCollapsedHeight);
    }

    private void setupOnItemTouchListeners() {
        this.mLoadMoreTextView.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == 0 && (AllDayActivitiesView.this.expandAllDayActivitiesList() || AllDayActivitiesView.this.collapseAllDayActivitiesList())) {
                    return true;
                }
                return false;
            }
        });
    }

    public boolean collapseAllDayActivitiesList() {
        if (this.mAllDayActivitiesList.getHeight() <= getAllDayActivitiesCollapsedHeight()) {
            return false;
        }
        this.mCustomLayoutManager.scrollToPosition(0);
        changeHeightAndAnimate(false, this.mAllDayActivitiesList.getHeight(), getAllDayActivitiesCollapsedHeight());
        this.mLoadMoreTextView.setText(getResources().getString(R.string.allDayActivityList.loadMore, new Object[]{Integer.valueOf(getNumberOfAllDayActivities() - 1), getContext().getResources().getText(R.string.n_more)}));
        AgendaViewEventBus.getInstance().allDayActivityExpandCollapseStateChanged(false);
        return true;
    }

    private boolean expandAllDayActivitiesList() {
        if (this.mAllDayActivitiesList.getHeight() != getAllDayActivitiesCollapsedHeight()) {
            return false;
        }
        changeHeightAndAnimate(true, getAllDayActivitiesCollapsedHeight(), getMaxExpandHeight());
        this.mLoadMoreTextView.setText(getContext().getResources().getText(R.string.show_less).toString());
        AgendaViewEventBus.getInstance().allDayActivityExpandCollapseStateChanged(true);
        return true;
    }

    private int getNumberOfAllDayActivities() {
        return this.mAllDayActivitiesAdapter == null ? 0 : this.mAllDayActivitiesAdapter.getItemCount();
    }

    private void setInitialHeight(@NonNull View view, int height) {
        MarginLayoutParams layoutParams = (MarginLayoutParams) view.getLayoutParams();
        layoutParams.height = height;
        view.setLayoutParams(layoutParams);
        this.mCustomLayoutManager.setScrollEnabled(false);
    }

    private int getMaxExpandHeight() {
        int maxNumberOfItemsAllowed = this.mAllDayActivitiesListMaxHeight / (this.mAllDayActivityContainerItemHeight + this.mAllDayActivityContainerItemPadding);
        return Math.min((this.mAllDayActivityContainerItemHeight * maxNumberOfItemsAllowed) + (this.mAllDayActivityContainerItemPadding * (maxNumberOfItemsAllowed - 1)), (this.mAllDayActivityContainerItemHeight * getNumberOfAllDayActivities()) + (this.mAllDayActivityContainerItemPadding * (getNumberOfAllDayActivities() - 1)));
    }

    private void changeHeightAndAnimate(boolean enableScrolling, int startHeight, int endHeight) {
        this.mCustomLayoutManager.setScrollEnabled(enableScrolling);
        this.mCustomLayoutManager.scrollToPosition(0);
        int duration = getResources().getInteger(17694721);
        CalendarAnimations.changeViewHeight(this.mAllDayActivitiesList, startHeight, endHeight, duration);
        CalendarAnimations.startFadeAnimation(this.mLoadMoreTextView, 0.0f, duration);
    }

    public void refreshContent(@NonNull Session session) {
        setDateTextViews(session, this.mDate);
        if (this.mAllDayActivitiesAdapter == null) {
            this.mAllDayActivitiesAdapter = new AllDayActivityAdapter(session, getDate());
            this.mAllDayActivitiesList.setAdapter(this.mAllDayActivitiesAdapter);
        } else {
            this.mAllDayActivitiesAdapter.setDate(this.mDate);
            this.mAllDayActivitiesAdapter.notifyDataSetChanged();
        }
        setAllDayActivitiesContainerHeight();
    }

    public void onCalendarExpanded() {
        collapseAllDayActivitiesList();
    }
}
