package com.pipedrive.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.application.Session;
import com.pipedrive.changes.Change.ChangeOperationType;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.BaseDatasourceEntity;
import java.util.ArrayList;
import java.util.List;

public class ChangesDataSource extends BaseDataSource<Change> {

    public static class Change extends BaseDatasourceEntity {
        public com.pipedrive.changes.Change change;

        public String toString() {
            return String.format("ChangesDataSource.Change: [change: %s][sqlId: %s]", new Object[]{this.change, Long.valueOf(getSqlId())});
        }
    }

    public static class DuplicateChanges {
        Change change;
        int count;

        public int getCount() {
            return this.count;
        }

        public Change getChange() {
            return this.change;
        }

        public String toString() {
            return String.format("DuplicateChanges: Count: %s (%s)", new Object[]{Integer.valueOf(getCount()), this.change.toString()});
        }
    }

    public ChangesDataSource(@NonNull Session session) {
        super(session.getDatabase());
    }

    @Nullable
    protected String getColumnNameForPipedriveId() {
        return null;
    }

    @NonNull
    protected String getColumnNameForSqlId() {
        return PipeSQLiteHelper.COLUMN_ID;
    }

    @NonNull
    protected String getTableName() {
        return PipeSQLiteHelper.TABLE_CHANGES_JOURNAL;
    }

    @Deprecated
    public long createOrUpdate(Change change) {
        throw new RuntimeException("No supported method!");
    }

    public long createOrUpdate(com.pipedrive.changes.Change change) {
        BaseDatasourceEntity changeForDb = new Change();
        changeForDb.change = change;
        return super.createOrUpdate(changeForDb);
    }

    @NonNull
    protected String[] getAllColumns() {
        return new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_CHANGES_JOURNAL_OPERATION_TYPE, PipeSQLiteHelper.COLUMN_CHANGES_JOURNAL_OPERATION, PipeSQLiteHelper.COLUMN_CHANGES_JOURNAL_OPERATION_METADATA};
    }

    @NonNull
    public List<DuplicateChanges> getDuplicateUpdateChanges() {
        String countColumn = "count";
        String rawQuery = "SELECT *, COUNT(_id) AS count FROM " + getTableName() + " WHERE " + PipeSQLiteHelper.COLUMN_CHANGES_JOURNAL_OPERATION + " = " + ChangeOperationType.OPERATION_UPDATE.getUniqueId() + " GROUP BY " + PipeSQLiteHelper.COLUMN_CHANGES_JOURNAL_OPERATION_TYPE + ", " + PipeSQLiteHelper.COLUMN_CHANGES_JOURNAL_OPERATION_METADATA;
        List<DuplicateChanges> changes = new ArrayList();
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        if (transactionalDBConnection == null) {
            Log.e(new Throwable("getTransactionalDBConnection() returned null! Exception would be expected! Will not return the data about duplicates!"));
            LogJourno.reportEvent(EVENT.Database_cannotReturnInfoAboutDuplicateDatabaseChanges_NoTransactionalDBConnection);
        } else {
            Cursor cursor = !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.rawQuery(rawQuery, null) : SQLiteInstrumentation.rawQuery(transactionalDBConnection, rawQuery, null);
            try {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    DuplicateChanges duplicateChanges = new DuplicateChanges();
                    duplicateChanges.change = deflateCursor(cursor);
                    duplicateChanges.count = cursor.getInt(cursor.getColumnIndex("count"));
                    changes.add(duplicateChanges);
                    cursor.moveToNext();
                }
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return changes;
    }

    @NonNull
    public List<Change> getChanges(@NonNull com.pipedrive.changes.Change change) {
        return getChanges(change, null);
    }

    @NonNull
    public List<Change> getChanges(@NonNull com.pipedrive.changes.Change change, @Nullable ChangeOperationType changeOperationType) {
        boolean changeOperationTypeSetAsSelection;
        String selection = "cj_op_type = ?";
        String[] selectionArgs = new String[]{Integer.toString(change.getTypeIdentifier())};
        if (changeOperationType != null) {
            changeOperationTypeSetAsSelection = true;
        } else {
            changeOperationTypeSetAsSelection = false;
        }
        if (changeOperationTypeSetAsSelection) {
            selection = DatabaseUtils.concatenateWhere(selection, "cj_op = ?");
            selectionArgs = DatabaseUtils.appendSelectionArgs(selectionArgs, new String[]{Integer.toString(changeOperationType.getUniqueId())});
        }
        return getChanges(selection, selectionArgs);
    }

    @NonNull
    private List<Change> getChanges(String selection, String[] selectionArgs) {
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String str = PipeSQLiteHelper.TABLE_CHANGES_JOURNAL;
        String[] allColumns = getAllColumns();
        String str2 = PipeSQLiteHelper.COLUMN_ID;
        Cursor cursor = !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.query(str, allColumns, selection, selectionArgs, null, null, str2, null) : SQLiteInstrumentation.query(transactionalDBConnection, str, allColumns, selection, selectionArgs, null, null, str2, null);
        try {
            List<Change> parseCursorIntoList = parseCursorIntoList(cursor);
            return parseCursorIntoList;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @NonNull
    private List<Change> parseCursorIntoList(Cursor cursor) {
        List<Change> changes = new ArrayList();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            changes.add(deflateCursor(cursor));
            cursor.moveToNext();
        }
        return changes;
    }

    @NonNull
    protected ContentValues getContentValues(@NonNull Change change) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(PipeSQLiteHelper.COLUMN_CHANGES_JOURNAL_OPERATION_TYPE, Integer.valueOf(change.change.getTypeIdentifier()));
        contentValues.put(PipeSQLiteHelper.COLUMN_CHANGES_JOURNAL_OPERATION, Integer.valueOf(change.change.getChangeOperationType().getUniqueId()));
        Long changeMetadata = change.change.getMetaData();
        if (changeMetadata == null) {
            contentValues.putNull(PipeSQLiteHelper.COLUMN_CHANGES_JOURNAL_OPERATION_METADATA);
        } else {
            contentValues.put(PipeSQLiteHelper.COLUMN_CHANGES_JOURNAL_OPERATION_METADATA, changeMetadata);
        }
        return contentValues;
    }

    @Nullable
    protected Change deflateCursor(@NonNull Cursor cursor) {
        Change change = new Change();
        change.setSqlId(cursor.getLong(cursor.getColumnIndex(getColumnNameForSqlId())));
        int changeTypeIdentifier = cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_CHANGES_JOURNAL_OPERATION_TYPE));
        int changeOperationType = cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_CHANGES_JOURNAL_OPERATION));
        Long operationMetaData = null;
        int index = cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_CHANGES_JOURNAL_OPERATION_METADATA);
        if (!cursor.isNull(index)) {
            operationMetaData = Long.valueOf(cursor.getLong(index));
        }
        change.change = com.pipedrive.changes.Change.restoreChange(changeOperationType, changeTypeIdentifier, operationMetaData);
        return change;
    }
}
