package com.pipedrive.views.navigation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datetime.LocaleHelper;
import com.pipedrive.datetime.PipedriveDateTime;

public class NavigationToolbar extends FrameLayout {
    private TextView dateTextView;
    private final int[] mButtonIds = new int[]{R.id.btn_pipeline, R.id.btn_activities, R.id.btn_contacts, R.id.btn_search, R.id.btn_settings};
    private final View[] mButtons = new View[this.mButtonIds.length];
    private final int[] mIconIds = new int[]{R.id.btn_pipeline_icon, R.id.btn_activities_icon, R.id.btn_contacts_icon, R.id.btn_search_icon, R.id.btn_settings_icon};
    private final ImageView[] mIcons = new ImageView[this.mButtonIds.length];
    private final int[] mIndicatorIds = new int[]{R.id.btn_pipeline_indicator, R.id.btn_activities_indicator, R.id.btn_contacts_indicator, R.id.btn_search_indicator, R.id.btn_settings_indicator};
    private final ImageView[] mIndicators = new ImageView[this.mButtonIds.length];
    @Nullable
    private OnNavigationButtonClickListener mOnNavigationButtonClickListener;
    private int mSelectedIndex = -1;

    public NavigationToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    private void initView() {
        int i;
        LayoutInflater.from(getContext()).inflate(R.layout.fragment_navigation_toolbar, this, true);
        for (i = 0; i < this.mIconIds.length; i++) {
            this.mIcons[i] = (ImageView) findViewById(this.mIconIds[i]);
            this.mIndicators[i] = (ImageView) findViewById(this.mIndicatorIds[i]);
            this.mButtons[i] = findViewById(this.mButtonIds[i]);
        }
        for (i = 0; i < this.mButtons.length; i++) {
            final int buttonIndex = i;
            this.mButtons[i].setOnClickListener(new OnClickListener() {
                public void onClick(@NonNull View v) {
                    if (NavigationToolbar.this.mOnNavigationButtonClickListener == null) {
                        return;
                    }
                    if (NavigationToolbar.this.mSelectedIndex != buttonIndex || NavigationToolbar.this.mSelectedIndex == NavigationToolbarButton.ACTIVITIES.index) {
                        NavigationToolbar.this.mOnNavigationButtonClickListener.onNavigationButtonClicked(NavigationToolbarButton.values()[buttonIndex]);
                    }
                }
            });
        }
    }

    public void setDateTextView(@NonNull Session session) {
        this.dateTextView = (TextView) findViewById(R.id.dateTextView);
        this.dateTextView.setText(LocaleHelper.getLocaleBasedDateStringInCurrentTimeDayOfMonthOnly(session, PipedriveDateTime.instanceWithCurrentDateTime()));
    }

    public void setOnNavigationButtonClickListener(OnNavigationButtonClickListener onNavigationButtonClickListener) {
        this.mOnNavigationButtonClickListener = onNavigationButtonClickListener;
    }

    public void setSelectedButton(NavigationToolbarButton button) {
        if (this.mSelectedIndex != button.index) {
            this.mSelectedIndex = button.index;
            int i = 0;
            while (i < this.mIcons.length) {
                boolean z;
                this.mIcons[i].setColorFilter(ContextCompat.getColor(getContext(), this.mSelectedIndex == i ? R.color.green : R.color.mid));
                this.mIndicators[i].setVisibility(this.mSelectedIndex == i ? 0 : 8);
                View view = this.mButtons[i];
                if (this.mSelectedIndex != i || this.mSelectedIndex == NavigationToolbarButton.ACTIVITIES.index) {
                    z = true;
                } else {
                    z = false;
                }
                view.setEnabled(z);
                this.dateTextView.setTextColor(ContextCompat.getColor(getContext(), this.mSelectedIndex == NavigationToolbarButton.ACTIVITIES.index ? R.color.green : R.color.mid));
                i++;
            }
        }
    }
}
