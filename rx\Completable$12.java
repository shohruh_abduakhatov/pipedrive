package rx;

import java.util.concurrent.TimeUnit;
import rx.Scheduler.Worker;
import rx.functions.Action0;
import rx.subscriptions.MultipleAssignmentSubscription;

class Completable$12 implements Completable$OnSubscribe {
    final /* synthetic */ long val$delay;
    final /* synthetic */ Scheduler val$scheduler;
    final /* synthetic */ TimeUnit val$unit;

    Completable$12(Scheduler scheduler, long j, TimeUnit timeUnit) {
        this.val$scheduler = scheduler;
        this.val$delay = j;
        this.val$unit = timeUnit;
    }

    public void call(final CompletableSubscriber s) {
        MultipleAssignmentSubscription mad = new MultipleAssignmentSubscription();
        s.onSubscribe(mad);
        if (!mad.isUnsubscribed()) {
            final Worker w = this.val$scheduler.createWorker();
            mad.set(w);
            w.schedule(new Action0() {
                public void call() {
                    try {
                        s.onCompleted();
                    } finally {
                        w.unsubscribe();
                    }
                }
            }, this.val$delay, this.val$unit);
        }
    }
}
