package com.pipedrive.views.edit.deal;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.model.Deal;
import com.pipedrive.util.formatter.DecimalFormatter;
import com.pipedrive.views.edit.products.DecimalEditTextWithUnderlineAndLabel;

public class DealValueEditText extends DecimalEditTextWithUnderlineAndLabel {
    private Deal mDeal;
    private OnValueChangedListener mOnValueChangedListener;

    public interface OnValueChangedListener {
        void onValueChanged(double d);
    }

    public DealValueEditText(Context context) {
        this(context, null);
    }

    public DealValueEditText(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DealValueEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected int getLabelTextResourceId() {
        return R.string.deal_value;
    }

    @Nullable
    protected String getFormattedValue() {
        if (getSession() == null) {
            return null;
        }
        return new DecimalFormatter(getSession()).format(Double.valueOf(this.mDeal.getValue()));
    }

    public void setup(@NonNull Session session, @NonNull Deal deal, @Nullable OnValueChangedListener onValueChangedListener) {
        this.mDeal = deal;
        this.mOnValueChangedListener = onValueChangedListener;
        setupMainViewWithZeroValueHidden(session, deal.getValue());
    }

    protected void onValueChanged(double newValue) {
        if (this.mOnValueChangedListener != null) {
            this.mOnValueChangedListener.onValueChanged(newValue);
        }
    }
}
