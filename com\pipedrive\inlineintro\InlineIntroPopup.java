package com.pipedrive.inlineintro;

import android.content.Context;
import android.graphics.Point;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.pipedrive.R;
import com.pipedrive.application.Session;

public class InlineIntroPopup extends PopupWindow {
    public static final int ABOVE_ANCHOR = 0;
    public static final int BELOW_ANCHOR = 1;
    private static final int LEFT = 0;
    private static final int LEFT_OR_RIGHT_TO_ANCHOR = 3;
    private static final int MIDDLE = 1;
    private static final int RIGHT = 3;
    private int animatedMoveDistance;
    private final int id;
    private LayoutInflater inflater;
    @NonNull
    private final InlineIntroAnimation inlineIntroAnimation = new InlineIntroAnimation();
    private int margin;
    @NonNull
    private final View parent;
    private final int positionToAnchor;
    private int screenWidth;
    @NonNull
    private final Session session;
    private int sideShadow;

    public InlineIntroPopup(Context context, @NonNull View parent, int id, @NonNull Session session, int positionToAnchor) {
        super(context);
        this.session = session;
        this.parent = parent;
        this.id = id;
        this.positionToAnchor = positionToAnchor;
        setup(context);
    }

    private void setup(Context context) {
        this.margin = context.getResources().getDimensionPixelSize(R.dimen.inline_intro_margin);
        this.animatedMoveDistance = context.getResources().getDimensionPixelSize(R.dimen.inline_intro_animated_move_distance);
        this.sideShadow = context.getResources().getDimensionPixelSize(R.dimen.dimen2);
        this.inflater = (LayoutInflater) context.getSystemService("layout_inflater");
        setBackgroundDrawable(ContextCompat.getDrawable(context, R.color.transparent));
        Display display = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        this.screenWidth = size.x;
        setTouchInterceptor(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == 1) {
                    InlineIntroPopup.this.dismissAndDoNotShowAgain();
                }
                return true;
            }
        });
        setWidth(-2);
        setHeight(-2);
    }

    public void dismissOnce() {
        if (isShowing()) {
            this.inlineIntroAnimation.stop();
            super.dismiss();
        }
    }

    public void dismissAndDoNotShowAgain() {
        if (isShowing()) {
            storeDoNotShowSetting();
            dismissOnce();
            return;
        }
        storeDoNotShowSetting();
    }

    private void storeDoNotShowSetting() {
        this.session.getSharedSession().setDoNotShowInlineComponentWithId(this.id);
    }

    private boolean doNotShow() {
        return this.session.getSharedSession().shouldNotShowInlineIntroComponentWithId(this.id);
    }

    private View getRootView(@LayoutRes int layoutId) {
        return this.inflater.inflate(layoutId, (ViewGroup) this.parent, false);
    }

    private void showHorizontalInlineIntro(View anchor, View arrow, int yOffset, int contentViewWidth, int toAnimatedValue) {
        int anchorMidLocationX = getAnchorXY(anchor)[0] + (anchor.getMeasuredWidth() / 2);
        int halfArrowWidth = arrow.getMeasuredWidth() / 2;
        int xOffset = 0;
        switch (horizontalPosition(anchor)) {
            case 0:
                xOffset = Math.max(this.margin - this.sideShadow, anchorMidLocationX - (contentViewWidth / 2));
                arrow.setTranslationX((float) ((anchorMidLocationX - xOffset) - halfArrowWidth));
                break;
            case 1:
                xOffset = anchorMidLocationX - (contentViewWidth / 2);
                arrow.setTranslationX((float) ((contentViewWidth / 2) - halfArrowWidth));
                break;
            case 3:
                xOffset = ((this.screenWidth - this.margin) - contentViewWidth) + this.sideShadow;
                arrow.setTranslationX((float) ((anchorMidLocationX - xOffset) - halfArrowWidth));
                break;
        }
        showAtLocationInParent(xOffset, yOffset);
        this.inlineIntroAnimation.startValueAnimationByYAxis(this, yOffset, toAnimatedValue, xOffset);
    }

    public boolean isNotDismissedForGood() {
        return !doNotShow();
    }

    private void showInlineIntroAboveWithDescription(@NonNull View anchor, @NonNull String title, @Nullable String description) {
        if (!doNotShow() && !isShowing()) {
            View contentViewLayout = getMeasuredContentView(title, description, R.layout.inline_intro_horizontal_above);
            int yOffset = getAnchorXY(anchor)[1] - contentViewLayout.getMeasuredHeight();
            showHorizontalInlineIntro(anchor, contentViewLayout.findViewById(R.id.arrow), yOffset, contentViewLayout.getMeasuredWidth(), yOffset - this.animatedMoveDistance);
        }
    }

    private void showInlineIntroBelowWithDescription(@NonNull View anchor, @NonNull String title, @Nullable String description) {
        if (!doNotShow() && !isShowing()) {
            View contentViewLayout = getMeasuredContentView(title, description, R.layout.inline_intro_horizontal_below);
            int yOffset = getAnchorXY(anchor)[1] + anchor.getHeight();
            showHorizontalInlineIntro(anchor, contentViewLayout.findViewById(R.id.arrow), yOffset, contentViewLayout.getMeasuredWidth(), yOffset + this.animatedMoveDistance);
        }
    }

    private void showInlineIntroOnTheSideWithDescription(@NonNull View anchor, @NonNull String title, @Nullable String description) {
        if (!doNotShow() && !isShowing()) {
            int xOffset;
            int yOffset;
            int toAnimatedValue;
            int anchorStartX = getAnchorXY(anchor)[0];
            int anchorStartY = getAnchorXY(anchor)[1];
            int anchorEndX = anchorStartX + anchor.getWidth();
            int halfAnchorHeight = anchor.getHeight() / 2;
            if (horizontalPosition(anchor) == 3) {
                View contentViewLayout = getMeasuredContentView(title, description, R.layout.inline_intro_vertical_right);
                xOffset = anchorStartX - contentViewLayout.getMeasuredWidth();
                yOffset = anchorStartY - ((contentViewLayout.getMeasuredHeight() / 2) - halfAnchorHeight);
                toAnimatedValue = xOffset - this.animatedMoveDistance;
            } else {
                xOffset = anchorEndX;
                yOffset = anchorStartY - ((getMeasuredContentView(title, description, R.layout.inline_intro_vertical_left).getMeasuredHeight() / 2) - halfAnchorHeight);
                toAnimatedValue = xOffset + this.animatedMoveDistance;
            }
            showAtLocationInParent(xOffset, yOffset);
            this.inlineIntroAnimation.startValueAnimationByXAxis(this, xOffset, toAnimatedValue, yOffset);
        }
    }

    private void showAtLocationInParent(int xOffset, int yOffset) {
        showAtLocation(this.parent, 51, xOffset, yOffset);
    }

    @NonNull
    private View getMeasuredContentView(@NonNull String title, @Nullable String description, @LayoutRes int layoutId) {
        View layout = getRootView(layoutId);
        setContentView(layout);
        setTitle(title);
        setDescription(description);
        layout.measure(MeasureSpec.makeMeasureSpec(0, 0), MeasureSpec.makeMeasureSpec(0, 0));
        return layout;
    }

    private void setTitle(@NonNull String title) {
        View contentView = getContentView();
        if (contentView != null) {
            ((TextView) contentView.findViewById(R.id.title)).setText(title);
        }
    }

    private int[] getAnchorXY(View anchor) {
        int[] location = new int[2];
        anchor.getLocationOnScreen(location);
        return location;
    }

    private void setDescription(@Nullable String description) {
        if (description != null) {
            View contentView = getContentView();
            if (contentView != null) {
                TextView descriptionTextView = (TextView) contentView.findViewById(R.id.description);
                descriptionTextView.setText(description);
                descriptionTextView.setVisibility(0);
            }
        }
    }

    private int horizontalPosition(View anchor) {
        int anchorStartX = getAnchorXY(anchor)[0];
        int screenMiddle = this.screenWidth / 2;
        if (getAnchorXY(anchor)[0] + anchor.getWidth() <= screenMiddle) {
            return 0;
        }
        if (anchorStartX >= screenMiddle) {
            return 3;
        }
        return 1;
    }

    public void showWithTitleOnly(@NonNull View anchor, @NonNull String title) {
        showWithTitleAndDescription(anchor, title, null);
    }

    private void showWithTitleAndDescription(@NonNull View anchor, @NonNull String title, @Nullable String description) {
        switch (this.positionToAnchor) {
            case 0:
                showInlineIntroAboveWithDescription(anchor, title, description);
                return;
            case 1:
                showInlineIntroBelowWithDescription(anchor, title, description);
                return;
            case 3:
                showInlineIntroOnTheSideWithDescription(anchor, title, description);
                return;
            default:
                return;
        }
    }
}
