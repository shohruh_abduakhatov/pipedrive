package com.pipedrive.nearby.cards.cards;

import android.support.annotation.UiThread;
import android.view.View;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class SingleCardWithCommunicationButtons_ViewBinding extends SingleCard_ViewBinding {
    private SingleCardWithCommunicationButtons target;
    private View view2131821165;
    private View view2131821166;

    @UiThread
    public SingleCardWithCommunicationButtons_ViewBinding(final SingleCardWithCommunicationButtons target, View source) {
        super(target, source);
        this.target = target;
        View view = Utils.findRequiredView(source, R.id.nearbyCard.messageButton, "field 'messageButton' and method 'onMessageButtonClicked'");
        target.messageButton = view;
        this.view2131821166 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onMessageButtonClicked();
            }
        });
        view = Utils.findRequiredView(source, R.id.nearbyCard.callButton, "field 'callButton' and method 'onCallButtonClicked'");
        target.callButton = view;
        this.view2131821165 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onCallButtonClicked();
            }
        });
    }

    public void unbind() {
        SingleCardWithCommunicationButtons target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.messageButton = null;
        target.callButton = null;
        this.view2131821166.setOnClickListener(null);
        this.view2131821166 = null;
        this.view2131821165.setOnClickListener(null);
        this.view2131821165 = null;
        super.unbind();
    }
}
