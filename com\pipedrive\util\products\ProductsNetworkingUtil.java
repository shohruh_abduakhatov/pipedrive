package com.pipedrive.util.products;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.SQLTransactionManager;
import com.pipedrive.datasource.products.ProductsDataSource;
import com.pipedrive.model.Currency;
import com.pipedrive.model.products.Product;
import com.pipedrive.model.products.ProductPrice;
import com.pipedrive.model.products.ProductVariation;
import com.pipedrive.util.CurrenciesNetworkingUtil;
import com.pipedrive.util.networking.entities.product.ProductEntity;
import com.pipedrive.util.networking.entities.product.ProductPriceEntity;
import com.pipedrive.util.networking.entities.product.ProductVariationEntity;
import com.pipedrive.util.networking.entities.product.ProductVariationPriceEntity;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProductsNetworkingUtil {
    private ProductsNetworkingUtil() {
    }

    @WorkerThread
    @NonNull
    public static List<Product> createOrUpdateProductsIntoDBWithRelations(@NonNull Session session, @NonNull List<ProductEntity> productEntities) {
        List<Product> products = new ArrayList(productEntities.size());
        SQLTransactionManager sqlTransactionManager = new SQLTransactionManager(session.getDatabase());
        sqlTransactionManager.beginTransactionNonExclusive();
        for (ProductEntity productEntity : productEntities) {
            Product product = createOrUpdateProductIntoDBWithRelations(session, productEntity);
            if (product != null) {
                products.add(product);
            }
        }
        sqlTransactionManager.commit();
        return products;
    }

    public static Product createOrUpdateProductIntoDBWithRelations(@NonNull Session session, @NonNull ProductEntity entity) {
        if (entity.getPipedriveId() == null || entity.getName() == null || entity.getActiveFlag() == null || entity.getSelectable() == null) {
            return null;
        }
        List<ProductPrice> prices = new ArrayList(entity.getPrices().size());
        for (ProductPriceEntity priceEntity : entity.getPrices()) {
            ProductPrice productPrice = getProductPriceFromValues(session, priceEntity.getPipedriveId(), priceEntity.getPrice(), priceEntity.getCurrency());
            if (productPrice != null) {
                prices.add(productPrice);
            }
        }
        List<ProductVariation> variations = new ArrayList(entity.getProductVariations().size());
        for (ProductVariationEntity variationEntity : entity.getProductVariations()) {
            if (!(variationEntity.getPipedriveId() == null || variationEntity.getName() == null)) {
                List<ProductPrice> variationPrices = new ArrayList(variationEntity.getPrices().size());
                for (ProductVariationPriceEntity variationPriceEntity : variationEntity.getPrices()) {
                    productPrice = getProductPriceFromValues(session, variationPriceEntity.getPipedriveId(), variationPriceEntity.getPrice(), variationPriceEntity.getCurrency());
                    if (productPrice != null) {
                        variationPrices.add(productPrice);
                    }
                }
                variations.add(ProductVariation.create(variationEntity.getPipedriveId(), variationEntity.getName(), variationPrices));
            }
        }
        Product product = Product.create(entity.getPipedriveId(), entity.getName(), entity.getActiveFlag(), Boolean.valueOf(Boolean.FALSE.equals(entity.getSelectable())), prices, variations);
        new ProductsDataSource(session.getDatabase()).createOrUpdate(product);
        return product;
    }

    @Nullable
    static ProductPrice getProductPriceFromValues(@NonNull Session session, @Nullable Long pipedriveId, @Nullable BigDecimal value, @Nullable String currencyCode) {
        if (pipedriveId == null || value == null || currencyCode == null) {
            return null;
        }
        Currency currency = CurrenciesNetworkingUtil.getCurrencyByCodeAndDownloadIfNotFound(session, currencyCode);
        if (currency != null) {
            return ProductPrice.create(pipedriveId, value, currency);
        }
        return null;
    }

    @Nullable
    public static Product createOrUpdateDeletedProductIntoDB(@NonNull Session session, @Nullable Long pipedriveId, @Nullable String name) {
        if (pipedriveId == null || name == null) {
            return null;
        }
        Product product = Product.create(pipedriveId, name, Boolean.valueOf(true), Boolean.valueOf(true), Collections.emptyList(), Collections.emptyList());
        new ProductsDataSource(session.getDatabase()).createOrUpdate(product);
        return product;
    }
}
