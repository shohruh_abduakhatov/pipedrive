package com.pipedrive.note;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.model.notes.Note;
import com.pipedrive.store.StoreNote;
import com.pipedrive.tasks.AsyncTask;

public class DeleteNoteTask extends AsyncTask<Note, Void, Boolean> {
    private final OnTaskFinished mOnTaskFinished;

    @MainThread
    interface OnTaskFinished {
        void onNoteDeleted(boolean z);
    }

    public DeleteNoteTask(@NonNull Session session, @Nullable OnTaskFinished onTaskFinished) {
        super(session);
        this.mOnTaskFinished = onTaskFinished;
    }

    protected Boolean doInBackground(Note... params) {
        Note note = params[0];
        note.setActiveFlag(false);
        return Boolean.valueOf(new StoreNote(getSession()).update(note));
    }

    protected void onPostExecute(Boolean success) {
        if (this.mOnTaskFinished != null) {
            this.mOnTaskFinished.onNoteDeleted(success.booleanValue());
        }
    }
}
