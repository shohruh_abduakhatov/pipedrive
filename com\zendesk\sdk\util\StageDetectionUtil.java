package com.zendesk.sdk.util;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import com.zendesk.logger.Logger;
import java.io.ByteArrayInputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.security.auth.x500.X500Principal;

public class StageDetectionUtil {
    private static final X500Principal DEBUG_DN = new X500Principal("CN=Android Debug,O=Android,C=US");
    private static final String LOG_TAG = "StageDetectionUtil";

    public static boolean isDebug(Context context) {
        if (context == null) {
            return true;
        }
        boolean checks = isDebugInternal(context);
        boolean logging = checkLogging();
        if (checks || logging) {
            return true;
        }
        return false;
    }

    public static boolean isProduction(Context context) {
        return !isDebug(context);
    }

    private static boolean isDebugInternal(Context context) {
        boolean debugFlag = checkDebuggableFlag(context);
        boolean debugCertificate = checkDebugCertificate(context);
        boolean buildFlag = checkBuildFlag(context);
        Logger.d(LOG_TAG, "Debug flag: %s | Debug certificate: %s | Build flag: %s", Boolean.valueOf(debugFlag), Boolean.valueOf(debugCertificate), Boolean.valueOf(buildFlag));
        List<Boolean> isDebug = Arrays.asList(new Boolean[]{Boolean.valueOf(debugFlag), Boolean.valueOf(debugCertificate), Boolean.valueOf(buildFlag)});
        if (Collections.frequency(isDebug, Boolean.valueOf(true)) >= isDebug.size() - 1) {
            return true;
        }
        return false;
    }

    private static boolean checkDebuggableFlag(Context context) {
        return (context.getApplicationInfo().flags & 2) != 0;
    }

    private static boolean checkDebugCertificate(Context context) {
        Exception e;
        boolean debuggable = false;
        try {
            Signature[] signatures = context.getPackageManager().getPackageInfo(context.getPackageName(), 64).signatures;
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            if (signatures != null) {
                for (Signature signature : signatures) {
                    Certificate certificate = cf.generateCertificate(new ByteArrayInputStream(signature.toByteArray()));
                    if (certificate instanceof X509Certificate) {
                        debuggable = DEBUG_DN.equals(((X509Certificate) certificate).getSubjectX500Principal());
                        if (debuggable) {
                            break;
                        }
                    }
                }
            }
        } catch (NameNotFoundException e2) {
            e = e2;
            Logger.w(LOG_TAG, "Error, not able to read the certificate", e, new Object[0]);
            return debuggable;
        } catch (CertificateException e3) {
            e = e3;
            Logger.w(LOG_TAG, "Error, not able to read the certificate", e, new Object[0]);
            return debuggable;
        }
        return debuggable;
    }

    private static boolean checkBuildFlag(Context context) {
        boolean debuggable = false;
        try {
            debuggable = ((Boolean) Class.forName(context.getPackageName() + ".BuildConfig").getField("DEBUG").get(null)).booleanValue();
        } catch (Exception e) {
            Logger.w(LOG_TAG, "Error, not able to receive 'BuildConfig.DEBUG'", e, new Object[0]);
        }
        return debuggable;
    }

    private static boolean checkLogging() {
        return Logger.isLoggable();
    }

    private StageDetectionUtil() {
    }
}
