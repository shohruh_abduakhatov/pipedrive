package com.zendesk.sdk.util;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.network.impl.ApplicationScope;
import com.zendesk.sdk.network.impl.DefaultZendeskUnauthorizedInterceptor;
import com.zendesk.sdk.network.impl.ZendeskRequestInterceptor;
import com.zendesk.sdk.storage.StorageInjector;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;

public class LibraryInjector {
    public static Gson injectCachedGson(ApplicationScope applicationScope) {
        return ModuleInjector.injectCachedLibraryModule(applicationScope).getGson();
    }

    public static Gson injectGson(ApplicationScope applicationScope) {
        return new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).excludeFieldsWithModifiers(new int[]{128, 8}).create();
    }

    public static OkHttpClient injectOkHttpClient(ApplicationScope applicationScope) {
        return new Builder().addInterceptor(injectDefaultZendeskUnauthorizedInterceptor(applicationScope)).addInterceptor(injectZendeskRequestInterceptor(applicationScope)).addInterceptor(injectHttpLoggingInterceptor(applicationScope)).connectTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).writeTimeout(30, TimeUnit.SECONDS).connectionSpecs(BaseInjector.injectConnectionSpec(applicationScope)).build();
    }

    private static Interceptor injectHttpLoggingInterceptor(ApplicationScope applicationScope) {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(Logger.isLoggable() ? Level.HEADERS : Level.NONE);
        return loggingInterceptor;
    }

    private static Interceptor injectZendeskRequestInterceptor(ApplicationScope applicationScope) {
        return new ZendeskRequestInterceptor(BaseInjector.injectOAuthToken(applicationScope), BaseInjector.injectUserAgentHeader(applicationScope));
    }

    private static Interceptor injectDefaultZendeskUnauthorizedInterceptor(ApplicationScope applicationScope) {
        return new DefaultZendeskUnauthorizedInterceptor(StorageInjector.injectCachedSdkStorage(applicationScope));
    }
}
