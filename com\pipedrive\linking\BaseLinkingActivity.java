package com.pipedrive.linking;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import com.pipedrive.BaseActivity;
import com.pipedrive.R;
import com.pipedrive.adapter.BaseSectionedListAdapter;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.util.ImageUtil;
import com.pipedrive.views.CustomSearchView.OnConstraintChangeListener;
import com.pipedrive.views.CustomSearchViewForLinking;

public abstract class BaseLinkingActivity extends BaseActivity implements LinkingView {
    private BaseSectionedListAdapter mAdapter;
    @BindView(2131820734)
    CustomSearchViewForLinking mCustomSearchViewForLinking;
    @BindView(2131820884)
    TextView mEmptyView;
    @BindView(2131820590)
    FloatingActionButton mFab;
    @BindView(2131820825)
    ListView mListView;
    @NonNull
    SearchConstraint mSearchConstraint = SearchConstraint.EMPTY;
    @BindView(2131820700)
    Toolbar toolbar;

    @OnClick({2131820590})
    abstract void onAddClicked();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_linking_view);
        ButterKnife.bind((Activity) this);
        this.mListView.setEmptyView(this.mEmptyView);
    }

    private void setTranslucentStatusBar() {
        if (VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            if (window != null) {
                window.addFlags(67108864);
            }
        }
    }

    private void onAssociationSelected(int position) {
        setSelectedItemSqlId(((Cursor) this.mAdapter.getItem(position)).getLong(0));
    }

    void setSelectedItemSqlId(long selectedItemSqlId) {
        setResult(-1, new Intent().putExtra(getKey(), selectedItemSqlId));
        finish();
    }

    @OnItemClick({2131820825})
    void onItemClick(int position) {
        onAssociationSelected(position);
    }

    public void onResume() {
        super.onResume();
        setupListView();
        setupToolbar();
        setTranslucentStatusBar();
        setupSearchView();
        setupFab();
    }

    private void setupToolbar() {
        setSupportActionBar(this.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            this.toolbar.setNavigationIcon(ImageUtil.getTintedDrawable(ContextCompat.getDrawable(this, R.drawable.icon_back), ContextCompat.getColor(this, R.color.mid)));
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupFab() {
        this.mFab.setImageDrawable(getFabIcon());
        this.mFab.setVisibility(isFabVisible() ? 0 : 8);
    }

    boolean isFabVisible() {
        return true;
    }

    private void setupSearchView() {
        this.mCustomSearchViewForLinking.setSearchHint(getSearchHint());
        this.mCustomSearchViewForLinking.setOnConstraintChangeListener(new OnConstraintChangeListener() {
            public void onConstraintChanged(@NonNull final SearchConstraint searchConstraint) {
                BaseLinkingActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        BaseLinkingActivity.this.updateSearch(searchConstraint);
                    }
                });
            }
        });
    }

    private void updateSearch(@NonNull SearchConstraint searchConstraint) {
        this.mSearchConstraint = searchConstraint;
        this.mAdapter.setSearchConstraint(searchConstraint);
    }

    private void setupListView() {
        if (this.mAdapter == null) {
            this.mAdapter = getAdapter();
            this.mListView.setAdapter(this.mAdapter);
            this.mListView.setFastScrollEnabled(true);
            this.mEmptyView.setText(R.string.lbl_no_results_found);
            return;
        }
        this.mAdapter.changeCursor(getCursor());
    }
}
