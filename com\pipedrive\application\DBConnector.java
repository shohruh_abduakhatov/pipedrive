package com.pipedrive.application;

import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.datasource.PipeSQLiteOpenHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;

abstract class DBConnector {
    private static final String TAG = DBConnector.class.getSimpleName();
    @Nullable
    private SQLiteDatabase database;
    @Nullable
    private PipeSQLiteOpenHelper sqlHelper;

    @NonNull
    protected abstract PipeSQLiteOpenHelper getSQLiteOpenHelper();

    DBConnector() {
    }

    protected SQLiteDatabase getDatabase() {
        String tag = TAG + ".getDatabase()";
        if (this.database == null) {
            if (this.sqlHelper == null) {
                this.sqlHelper = getSQLiteOpenHelper();
            }
            try {
                Log.i(tag, "Opening writable DB...");
                this.database = this.sqlHelper.getWritableDatabase();
                this.database.enableWriteAheadLogging();
                Log.i(tag, "Opening writable DB. Done. Ready to use DB. ");
            } catch (Exception e) {
                Log.e(new Throwable("Error in opening database!", e));
                LogJourno.reportEvent(EVENT.Database_cannotOpen);
            }
        }
        return this.database;
    }

    protected void closeDatabase() {
        if (this.sqlHelper != null) {
            try {
                this.sqlHelper.close();
            } catch (Exception e) {
            }
            this.sqlHelper = null;
        }
        if (this.database != null) {
            try {
                this.database.close();
            } catch (Exception e2) {
            }
            this.database = null;
        }
    }

    protected void clearDatabase() {
        if (this.sqlHelper != null && this.database != null) {
            this.sqlHelper.clearDatabase(this.database);
        }
    }
}
