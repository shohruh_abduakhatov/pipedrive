package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.auth.api.proxy.ProxyApi;
import com.google.android.gms.auth.api.proxy.ProxyApi.ProxyResult;
import com.google.android.gms.auth.api.proxy.ProxyRequest;
import com.google.android.gms.auth.api.proxy.ProxyResponse;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.internal.zzaa;

public class zzoc implements ProxyApi {
    public PendingResult<ProxyResult> performProxyRequest(GoogleApiClient googleApiClient, final ProxyRequest proxyRequest) {
        zzaa.zzy(googleApiClient);
        zzaa.zzy(proxyRequest);
        return googleApiClient.zzb(new zzob(this, googleApiClient) {
            final /* synthetic */ zzoc jc;

            protected void zza(Context context, zzoa com_google_android_gms_internal_zzoa) throws RemoteException {
                com_google_android_gms_internal_zzoa.zza(new zznx(this) {
                    final /* synthetic */ AnonymousClass1 jd;

                    {
                        this.jd = r1;
                    }

                    public void zza(ProxyResponse proxyResponse) {
                        this.jd.zzc(new zzod(proxyResponse));
                    }
                }, proxyRequest);
            }
        });
    }
}
