package com.pipedrive.util.devices;

import com.google.firebase.messaging.FirebaseMessaging;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.util.networking.RetrofitUtilsKt;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import kotlin.Metadata;
import kotlin.jvm.JvmOverloads;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import rx.Completable;
import rx.functions.Func2;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0018\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0006\u0010\u0016\u001a\u00020\u0017J\u0006\u0010\u0018\u001a\u00020\u0017J\u0010\u0010\u0019\u001a\n \b*\u0004\u0018\u00010\u00170\u0017H\u0002J\b\u0010\u001a\u001a\u00020\u0011H\u0002J\u000e\u0010\u001b\u001a\u00020\u00172\u0006\u0010\u001c\u001a\u00020\u001dJ\b\u0010\u001e\u001a\u00020\u0011H\u0002J\u0006\u0010\u001f\u001a\u00020\u0017R\u0016\u0010\u0007\u001a\n \b*\u0004\u0018\u00010\u00050\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fXD¢\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000¨\u0006 "}, d2 = {"Lcom/pipedrive/util/devices/MobileDevices;", "", "session", "Lcom/pipedrive/application/Session;", "mockApi", "Lcom/pipedrive/util/devices/MobileDevicesApi;", "(Lcom/pipedrive/application/Session;Lcom/pipedrive/util/devices/MobileDevicesApi;)V", "api", "kotlin.jvm.PlatformType", "companyIdWithPrefix", "", "maxRepeatCount", "", "getSession", "()Lcom/pipedrive/application/Session;", "userIdWithPrefix", "logErrorToNewRelic", "", "logJournoEvent", "Lcom/pipedrive/logging/LogJourno$EVENT;", "error", "", "loginDevice", "Lrx/Completable;", "logoutDevice", "registerDevice", "subscribeToTopics", "switchCompany", "previousCompanyPipedriveId", "", "unsubscribeFromTopics", "updateDevice", "pipedrive_prodRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: MobileDevices.kt */
public final class MobileDevices {
    private final MobileDevicesApi api;
    private final String companyIdWithPrefix;
    private final int maxRepeatCount;
    @NotNull
    private final Session session;
    private final String userIdWithPrefix;

    @JvmOverloads
    public MobileDevices(@NotNull Session session) {
        this(session, null, 2, null);
    }

    @JvmOverloads
    public MobileDevices(@NotNull Session session, @Nullable MobileDevicesApi mockApi) {
        Intrinsics.checkParameterIsNotNull(session, SettingsJsonConstants.SESSION_KEY);
        this.session = session;
        this.maxRepeatCount = 3;
        if (mockApi == null) {
            mockApi = (MobileDevicesApi) RetrofitUtilsKt.createRetrofitFromSession(this.session).create(MobileDevicesApi.class);
        }
        this.api = mockApi;
        this.userIdWithPrefix = "user_" + this.session.getAuthenticatedUserID();
        this.companyIdWithPrefix = "company_" + this.session.getSelectedCompanyID();
    }

    @JvmOverloads
    public /* synthetic */ MobileDevices(Session session, MobileDevicesApi mobileDevicesApi, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(session, (i & 2) != 0 ? (MobileDevicesApi) null : mobileDevicesApi);
    }

    @NotNull
    public final Session getSession() {
        return this.session;
    }

    @NotNull
    public final Completable loginDevice() {
        MobileDevicesApi mobileDevicesApi = this.api;
        String androidId = this.session.getAndroidId();
        Intrinsics.checkExpressionValueIsNotNull(androidId, "session.androidId");
        Completable doOnCompleted = mobileDevicesApi.getUdId(androidId).map(MobileDevices$loginDevice$1.INSTANCE).flatMapCompletable(new MobileDevices$loginDevice$2(this)).retry((Func2) new MobileDevices$loginDevice$3(this)).doOnError(new MobileDevices$loginDevice$4(this)).onErrorResumeNext(new MobileDevices$loginDevice$5(this)).doOnCompleted(new MobileDevices$loginDevice$6(this));
        Intrinsics.checkExpressionValueIsNotNull(doOnCompleted, "api.getUdId(session.andr…d { subscribeToTopics() }");
        return doOnCompleted;
    }

    private final Completable registerDevice() {
        return this.api.register(new DeviceEntity(this.session)).retry((Func2) new MobileDevices$registerDevice$1(this)).toCompletable().onErrorResumeNext(MobileDevices$registerDevice$2.INSTANCE);
    }

    @NotNull
    public final Completable logoutDevice() {
        unsubscribeFromTopics();
        MobileDevicesApi mobileDevicesApi = this.api;
        String androidId = this.session.getAndroidId();
        Intrinsics.checkExpressionValueIsNotNull(androidId, "session.androidId");
        Completable onErrorComplete = mobileDevicesApi.getUdId(androidId).map(MobileDevices$logoutDevice$1.INSTANCE).flatMapCompletable(new MobileDevices$logoutDevice$2(this)).doOnError(new MobileDevices$logoutDevice$3(this)).onErrorComplete();
        Intrinsics.checkExpressionValueIsNotNull(onErrorComplete, "api.getUdId(session.andr…       .onErrorComplete()");
        return onErrorComplete;
    }

    @NotNull
    public final Completable updateDevice() {
        MobileDevicesApi mobileDevicesApi = this.api;
        String androidId = this.session.getAndroidId();
        Intrinsics.checkExpressionValueIsNotNull(androidId, "session.androidId");
        Completable onErrorComplete = mobileDevicesApi.getUdId(androidId).map(MobileDevices$updateDevice$1.INSTANCE).flatMapCompletable(new MobileDevices$updateDevice$2(this)).doOnError(new MobileDevices$updateDevice$3(this)).onErrorComplete();
        Intrinsics.checkExpressionValueIsNotNull(onErrorComplete, "api\n            .getUdId…       .onErrorComplete()");
        return onErrorComplete;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    @NotNull
    public final Completable switchCompany(long previousCompanyPipedriveId) {
        Completable logoutDevice;
        Session previousSession = PipedriveApp.getSessionManager().getSessionForCompanyPipedriveId(Long.valueOf(previousCompanyPipedriveId));
        if (previousSession != null) {
            Session it = previousSession;
            logoutDevice = new MobileDevices(previousSession, null, 2, null).logoutDevice();
        }
        logoutDevice = Completable.complete();
        Completable andThen = logoutDevice.andThen(loginDevice());
        Intrinsics.checkExpressionValueIsNotNull(andThen, "logoutDevice.andThen(loginDevice())");
        return andThen;
    }

    private final void logErrorToNewRelic(EVENT logJournoEvent, Throwable error) {
        Log.e(error);
        LogJourno.reportEvent(logJournoEvent, error);
    }

    private final void subscribeToTopics() {
        FirebaseMessaging.getInstance().subscribeToTopic(this.userIdWithPrefix);
        FirebaseMessaging.getInstance().subscribeToTopic(this.companyIdWithPrefix);
    }

    private final void unsubscribeFromTopics() {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(this.userIdWithPrefix);
        FirebaseMessaging.getInstance().unsubscribeFromTopic(this.companyIdWithPrefix);
    }
}
