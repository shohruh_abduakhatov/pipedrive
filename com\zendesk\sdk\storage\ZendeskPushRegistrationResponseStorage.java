package com.zendesk.sdk.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.Gson;
import com.newrelic.agent.android.instrumentation.GsonInstrumentation;
import com.zendesk.sdk.model.push.PushRegistrationResponse;
import com.zendesk.util.StringUtils;

class ZendeskPushRegistrationResponseStorage implements PushRegistrationResponseStorage {
    private static final String PUSH_ID_PREFS_NAME = "zendesk-push-token";
    private static final String PUSH_REG_RESPONSE_IDENTIFIER = "pushRegResponseIdentifier";
    private final Gson gson;
    private final SharedPreferences storage;

    ZendeskPushRegistrationResponseStorage(Context context, Gson gson) {
        SharedPreferences sharedPreferences;
        if (context == null) {
            sharedPreferences = null;
        } else {
            sharedPreferences = context.getSharedPreferences(PUSH_ID_PREFS_NAME, 0);
        }
        this.storage = sharedPreferences;
        this.gson = gson;
    }

    public void storePushRegistrationResponse(@NonNull PushRegistrationResponse response) {
        if (response != null && this.storage != null) {
            Gson gson = this.gson;
            this.storage.edit().putString(PUSH_REG_RESPONSE_IDENTIFIER, !(gson instanceof Gson) ? gson.toJson(response) : GsonInstrumentation.toJson(gson, response)).apply();
        }
    }

    @Nullable
    public PushRegistrationResponse getPushRegistrationResponse() {
        if (this.storage == null) {
            return null;
        }
        String registrationResponseString = this.storage.getString(PUSH_REG_RESPONSE_IDENTIFIER, null);
        if (!StringUtils.hasLength(registrationResponseString)) {
            return null;
        }
        Gson gson = this.gson;
        Class cls = PushRegistrationResponse.class;
        return !(gson instanceof Gson) ? gson.fromJson(registrationResponseString, cls) : GsonInstrumentation.fromJson(gson, registrationResponseString, cls);
    }

    public boolean hasStoredPushRegistrationResponse() {
        return this.storage != null && this.storage.contains(PUSH_REG_RESPONSE_IDENTIFIER);
    }

    public void removePushRegistrationResponse() {
        if (this.storage != null) {
            this.storage.edit().clear().apply();
        }
    }

    public void clearUserData() {
        removePushRegistrationResponse();
    }

    public String getCacheKey() {
        return PUSH_ID_PREFS_NAME;
    }
}
