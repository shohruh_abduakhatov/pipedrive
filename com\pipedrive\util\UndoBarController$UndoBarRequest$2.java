package com.pipedrive.util;

import com.pipedrive.logging.Log;
import com.pipedrive.util.UndoBarController.UndoBarRequest;

class UndoBarController$UndoBarRequest$2 implements Runnable {
    final /* synthetic */ UndoBarRequest this$1;
    final /* synthetic */ UndoBarController val$this$0;

    UndoBarController$UndoBarRequest$2(UndoBarRequest this$1, UndoBarController undoBarController) {
        this.this$1 = this$1;
        this.val$this$0 = undoBarController;
    }

    public void run() {
        String tag = this.this$1.TAG + ".Runnable.run()";
        if (UndoBarRequest.access$000(this.this$1) != null) {
            UndoBarRequest.access$000(this.this$1).setOnClickListener(null);
        }
        UndoBarRequest.access$200(this.this$1, true);
        if (UndoBarRequest.access$600(this.this$1) != null) {
            Log.d(tag, String.format("Executing no undo on %s with token %s", new Object[]{UndoBarRequest.access$600(this.this$1), UndoBarRequest.access$100(this.this$1)}));
            UndoBarRequest.access$600(this.this$1).onNoUndo(UndoBarRequest.access$100(this.this$1));
        } else {
            Log.d(tag, String.format("Undo was not selected with token %s", new Object[]{UndoBarRequest.access$100(this.this$1)}));
        }
        UndoBarRequest.access$502(this.this$1, true);
    }
}
