package com.pipedrive.more;

import android.app.Activity;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.pipedrive.BaseActivity;
import com.pipedrive.R;
import com.pipedrive.analytics.ScreensMapper;
import com.pipedrive.logging.Log;
import com.pipedrive.util.networking.ConnectionUtil;
import java.io.IOException;

public class HtmlOrFileLoadActivity extends BaseActivity {
    static final String KEY_FILE_TO_OPEN = "file_to_open";
    static final String KEY_LABEL = "label";
    static final String KEY_URL_TO_LOAD = "url_to_load";
    @BindView(2131820776)
    TextView textView;
    @BindView(2131820777)
    WebView webView;

    private static void startActivity(@NonNull Activity activity, @NonNull Intent intent) {
        ContextCompat.startActivity(activity, intent, ActivityOptionsCompat.makeBasic().toBundle());
    }

    static void startActivityForPrivacyPolicy(@NonNull Activity activity) {
        String urlPrivacyPolicy = "https://www.pipedrive.com/en/privacy-mobile";
        String label = ScreensMapper.SCREEN_NAME_PRIVACY_POLICY;
        startActivity(activity, new Intent(activity, HtmlOrFileLoadActivity.class).putExtra(KEY_URL_TO_LOAD, "https://www.pipedrive.com/en/privacy-mobile").putExtra("label", ScreensMapper.SCREEN_NAME_PRIVACY_POLICY));
    }

    static void startActivityForTermsOfService(@NonNull Activity activity) {
        String urlTermsOfService = "https://www.pipedrive.com/en/terms-of-service-mobile";
        String label = ScreensMapper.SCREEN_NAME_TERMS_OF_SERVICE;
        startActivity(activity, new Intent(activity, HtmlOrFileLoadActivity.class).putExtra(KEY_URL_TO_LOAD, "https://www.pipedrive.com/en/terms-of-service-mobile").putExtra("label", ScreensMapper.SCREEN_NAME_TERMS_OF_SERVICE));
    }

    static void startActivityForChangelog(@NonNull Activity activity) {
        String fileChangelog = "text_changelog.html";
        startActivity(activity, new Intent(activity, HtmlOrFileLoadActivity.class).putExtra(KEY_FILE_TO_OPEN, "text_changelog.html"));
    }

    static void startActivityForOpenSourceLicences(@NonNull Activity activity) {
        String fileOpenSourceLicence = "text_open_source_licences.html";
        startActivity(activity, new Intent(activity, HtmlOrFileLoadActivity.class).putExtra(KEY_FILE_TO_OPEN, "text_open_source_licences.html"));
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_html_or_file_load);
        ButterKnife.bind((Activity) this);
    }

    public void onResume() {
        super.onResume();
        Intent intent = getIntent();
        if (intent.getStringExtra(KEY_FILE_TO_OPEN) != null) {
            openFile(intent.getStringExtra(KEY_FILE_TO_OPEN));
        } else {
            loadLink(intent.getStringExtra(KEY_URL_TO_LOAD), intent.getStringExtra("label"));
        }
    }

    private void loadLink(@NonNull final String urlToLoad, @Nullable final String label) {
        this.textView.setVisibility(0);
        this.webView.setVisibility(8);
        this.webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                HtmlOrFileLoadActivity.this.textView.setText(HtmlOrFileLoadActivity.this.getString(R.string.lbl_preference_web_view_loading) + progress + "%");
            }
        });
        this.webView.setWebViewClient(new WebViewClient() {
            boolean loadingError = false;

            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                this.loadingError = true;
            }

            public void onPageFinished(WebView view, String url) {
                if (this.loadingError) {
                    view.setVisibility(8);
                    HtmlOrFileLoadActivity.this.textView.setVisibility(0);
                    HtmlOrFileLoadActivity.this.textView.setText(HtmlOrFileLoadActivity.this.getString(R.string.lbl_preference_web_view_loading_error, new Object[]{label, urlToLoad}));
                    return;
                }
                HtmlOrFileLoadActivity.this.textView.setVisibility(8);
                view.setVisibility(0);
            }
        });
        this.webView.loadUrl(urlToLoad);
    }

    private void openFile(@NonNull String fileToOpen) {
        try {
            String textFromResource = ConnectionUtil.inputStreamToString(getAssets().open(fileToOpen));
            this.textView.setVisibility(0);
            if (VERSION.SDK_INT >= 24) {
                this.textView.setText(Html.fromHtml(textFromResource, 0));
            } else {
                this.textView.setText(Html.fromHtml(textFromResource));
            }
        } catch (IOException e) {
            Log.e(new Throwable("Problems reading assets!", e));
        }
    }
}
