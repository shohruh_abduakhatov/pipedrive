package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import kotlin.jvm.internal.LongCompanionObject;

public class zzk implements Creator<LocationRequest> {
    static void zza(LocationRequest locationRequest, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zzc(parcel, 1, locationRequest.mPriority);
        zzb.zza(parcel, 2, locationRequest.akm);
        zzb.zza(parcel, 3, locationRequest.akn);
        zzb.zza(parcel, 4, locationRequest.VT);
        zzb.zza(parcel, 5, locationRequest.ajR);
        zzb.zzc(parcel, 6, locationRequest.ako);
        zzb.zza(parcel, 7, locationRequest.akp);
        zzb.zzc(parcel, 1000, locationRequest.getVersionCode());
        zzb.zza(parcel, 8, locationRequest.akq);
        zzb.zzaj(parcel, zzcs);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zznw(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzus(i);
    }

    public LocationRequest zznw(Parcel parcel) {
        int zzcr = zza.zzcr(parcel);
        int i = 0;
        int i2 = 102;
        long j = 3600000;
        long j2 = 600000;
        boolean z = false;
        long j3 = LongCompanionObject.MAX_VALUE;
        int i3 = Integer.MAX_VALUE;
        float f = 0.0f;
        long j4 = 0;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    i2 = zza.zzg(parcel, zzcq);
                    break;
                case 2:
                    j = zza.zzi(parcel, zzcq);
                    break;
                case 3:
                    j2 = zza.zzi(parcel, zzcq);
                    break;
                case 4:
                    z = zza.zzc(parcel, zzcq);
                    break;
                case 5:
                    j3 = zza.zzi(parcel, zzcq);
                    break;
                case 6:
                    i3 = zza.zzg(parcel, zzcq);
                    break;
                case 7:
                    f = zza.zzl(parcel, zzcq);
                    break;
                case 8:
                    j4 = zza.zzi(parcel, zzcq);
                    break;
                case 1000:
                    i = zza.zzg(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new LocationRequest(i, i2, j, j2, z, j3, i3, f, j4);
        }
        throw new zza.zza("Overread allowed size end=" + zzcr, parcel);
    }

    public LocationRequest[] zzus(int i) {
        return new LocationRequest[i];
    }
}
