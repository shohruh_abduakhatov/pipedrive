package com.pipedrive.views.viewholder.flow;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.TypedValue;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import com.newrelic.agent.android.instrumentation.JSONArrayInstrumentation;
import com.pipedrive.R;
import com.pipedrive.flow.model.FlowItemEntity;
import com.pipedrive.model.email.EmailMessage;
import com.pipedrive.views.viewholder.ViewHolder;
import org.json.JSONArray;
import org.json.JSONException;

public class EmailRowViewHolder implements ViewHolder {
    @BindView(2131821078)
    ImageView attachmentIcon;
    @BindView(2131821079)
    TextView parties;
    @BindView(2131821077)
    TextView subject;
    @BindView(2131821080)
    TextView summary;

    public int getLayoutResourceId() {
        return R.layout.row_email;
    }

    public void fill(@NonNull Context context, @NonNull EmailMessage emailMessage) {
        fill(context, emailMessage.getSubject(), emailMessage.getSendersDisplayString(), emailMessage.getRecepientsDisplayString(), emailMessage.getSummary(), (long) emailMessage.getAttachmentCount().intValue());
    }

    private void fill(@NonNull Context context, String subjectParam, String sendersParam, String recipientsParam, String summaryParam, long attachmentCount) {
        String subject;
        String summary;
        if (TextUtils.isEmpty(subjectParam)) {
            subject = context.getString(R.string.flow_item_email_no_subject);
        } else {
            subject = subjectParam;
        }
        String senders = buildSendersDisplayString(context, sendersParam);
        String recipients = buildRecepientDisplayString(context, recipientsParam, this.parties.getPaint(), senders + "   ");
        if (TextUtils.isEmpty(summaryParam)) {
            summary = context.getString(R.string.flow_item_email_no_summary);
        } else {
            summary = summaryParam;
        }
        SpannableStringBuilder partiesSpannable = new SpannableStringBuilder(senders + "   " + recipients);
        partiesSpannable.setSpan(new ImageSpan(context, R.drawable.icon_small_arrow_right, 1), senders.length() + 1, senders.length() + 2, 33);
        this.subject.setText(subject);
        this.parties.setText(partiesSpannable);
        this.summary.setText(summary);
        this.attachmentIcon.setVisibility(attachmentCount > 0 ? 0 : 8);
    }

    private String buildSendersDisplayString(@NonNull Context context, @Nullable String jsonArrayString) {
        if (TextUtils.isEmpty(jsonArrayString)) {
            return context.getString(R.string.flow_item_email_no_recipients);
        }
        try {
            JSONArray jsonArray = JSONArrayInstrumentation.init(jsonArrayString);
            if (jsonArray.length() == 0) {
                return context.getString(R.string.flow_item_email_no_recipients);
            }
            if (jsonArray.length() == 1) {
                return jsonArray.optString(0);
            }
            return String.format("%s + %d more", new Object[]{jsonArray.optString(0), Integer.valueOf(jsonArray.length() - 1)});
        } catch (JSONException e) {
            return context.getString(R.string.flow_item_email_no_recipients);
        }
    }

    private String buildRecepientDisplayString(@NonNull Context context, @Nullable String jsonArrayString, @NonNull TextPaint textPaint, @NonNull String head) {
        if (TextUtils.isEmpty(jsonArrayString)) {
            return context.getString(R.string.flow_item_email_no_recipients);
        }
        try {
            JSONArray jsonArray = JSONArrayInstrumentation.init(jsonArrayString);
            if (jsonArray.length() == 0) {
                return context.getString(R.string.flow_item_email_no_recipients);
            }
            if (jsonArray.length() == 1) {
                return jsonArray.optString(0);
            }
            int fieldWidth = (int) (((float) context.getResources().getDisplayMetrics().widthPixels) - TypedValue.applyDimension(1, 72.0f, context.getResources().getDisplayMetrics()));
            Rect bounds = new Rect();
            String value = jsonArray.optString(0);
            for (int i = 1; i < jsonArray.length(); i++) {
                String resultString = head + value + ", " + jsonArray.optString(i) + " + " + ((jsonArray.length() - i) - 1) + " more";
                textPaint.getTextBounds(resultString, 0, resultString.length(), bounds);
                if (bounds.width() > fieldWidth) {
                    return value + " + " + (jsonArray.length() - i) + " more";
                }
                value = value + ", " + jsonArray.optString(i);
            }
            return value;
        } catch (JSONException e) {
            return context.getString(R.string.flow_item_email_no_recipients);
        }
    }

    public void fill(@NonNull Context context, @NonNull FlowItemEntity flowItemEntity) {
        fill(context, flowItemEntity.getTitle(), flowItemEntity.getPersonName(), flowItemEntity.getOrgName(), flowItemEntity.getExtraString(), flowItemEntity.getExtraNumber().longValue());
    }
}
