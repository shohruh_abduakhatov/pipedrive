package com.pipedrive.sync;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import com.pipedrive.LoginActivity;
import com.pipedrive.application.Session;
import com.pipedrive.tasks.AsyncTaskWithCallback;
import com.pipedrive.tasks.CurrenciesTask;
import com.pipedrive.tasks.CustomFieldsTask;
import com.pipedrive.tasks.activities.ActivitiesListTask;
import com.pipedrive.tasks.activities.ActivityTypesTask;
import com.pipedrive.tasks.authorization.OnEnoughDataToShowUI;
import com.pipedrive.tasks.organizations.OrganizationAddressFieldTask;
import com.pipedrive.tasks.organizations.OrganizationsTask;
import com.pipedrive.tasks.persons.DownloadPersonsTask;
import com.pipedrive.tasks.session.user.DownloadUserSelfTask;
import com.pipedrive.tasks.users.DownloadUsersTask;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.recents.RecentsQueryUtil;
import com.pipedrive.util.time.TimeManager;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.Subject;

public final class SyncManager {
    private final Map<String, Subject<StateOfDownloadAll, StateOfDownloadAll>> mDownloadAllStateBusesPerSession;

    private SyncManager() {
        this.mDownloadAllStateBusesPerSession = new ConcurrentHashMap();
    }

    public static SyncManager getInstance() {
        return Lazy.instance;
    }

    @NonNull
    public Observable<StateOfDownloadAll> getStateOfDownloadAll(@NonNull Session forSession) {
        return getStateOfDownloadAllSubject(forSession);
    }

    @NonNull
    private Subject<StateOfDownloadAll, StateOfDownloadAll> getStateOfDownloadAllSubject(@NonNull Session forSession) {
        return (Subject) this.mDownloadAllStateBusesPerSession.get(registerNewDownloadAllStateBusIfNeeded(forSession));
    }

    @NonNull
    private String registerNewDownloadAllStateBusIfNeeded(@NonNull Session forSession) {
        String key = forSession.getSessionID();
        if (!this.mDownloadAllStateBusesPerSession.containsKey(key)) {
            this.mDownloadAllStateBusesPerSession.put(key, BehaviorSubject.create(DownloadEverythingInProgressMarker.isExecuting(forSession, DownloadEverythingInProgressMarker.class) ? StateOfDownloadAll.DOWNLOADING : StateOfDownloadAll.IDLE).toSerialized());
        }
        return key;
    }

    @WorkerThread
    public void syncRequestedByPushNotification(@NonNull Session session) {
        requestReSyncAllNonblocking(session, null, true, false);
    }

    @WorkerThread
    public void syncRequestedBySyncAdapter(@NonNull Session session) {
        requestReSyncAllNonblocking(session, null, false, false);
    }

    @WorkerThread
    public void syncRequestedBySyncAdapterAndSystem(@NonNull Session session) {
        requestReSyncAllNonblocking(session, null, false, true);
    }

    @WorkerThread
    public void requestLoginDataDownload(@NonNull Session session, @Nullable OnEnoughDataToShowUI onEnoughDataToShowUI) {
        requestReSyncAllNonblocking(session, onEnoughDataToShowUI, true, false);
    }

    @MainThread
    public void requestSettingsRefresh(@NonNull Session session, @NonNull OnEnoughDataToShowUI onEnoughDataToShowUI) {
        getStateOfDownloadAll(session).take(1).subscribe(new 1(this, onEnoughDataToShowUI, session), new 2(this, onEnoughDataToShowUI, session));
    }

    @MainThread
    public void requestCompanySwitchDataDownload(@NonNull Session session, @NonNull OnEnoughDataToShowUI onEnoughDataToShowUI) {
        getStateOfDownloadAll(session).take(1).subscribe(new 3(this, onEnoughDataToShowUI, session), new 4(this, onEnoughDataToShowUI, session));
    }

    private void requestReSyncAllNonblocking(@NonNull Session session, @Nullable OnEnoughDataToShowUI onEnoughDataToShowUI, boolean forceDownloadAll, boolean requestFromPeriodicSync) {
        Runnable onEnoughDataToShowUISingleRunnable = new 5(this, onEnoughDataToShowUI, session);
        if (ConnectionUtil.isConnected(session.getApplicationContext())) {
            AsyncTaskWithCallback<Void, Void> 7 = new 7(this, session, new 6(this, onEnoughDataToShowUISingleRunnable));
            long lastDownloadEverythingTime = session.getLastDownloadEverythingTime();
            boolean isAppStartedForTheFirstTime = lastDownloadEverythingTime == Long.MIN_VALUE;
            boolean isLastDownloadEverythingHappened24hAgo = !isAppStartedForTheFirstTime && TimeManager.getInstance().currentTimeMillis().longValue() - lastDownloadEverythingTime > TimeUnit.HOURS.toMillis(24);
            boolean isDatabaseSchemasRecreated = session.getRuntimeCache().DBTablesRecreated;
            if (!forceDownloadAll && !isAppStartedForTheFirstTime && !isLastDownloadEverythingHappened24hAgo && !isDatabaseSchemasRecreated) {
                onEnoughDataToShowUISingleRunnable.run();
                long recentsSyncIntervalInMilliseconds = TimeUnit.SECONDS.toMillis(LoginActivity.RECENTS_SYNC_INTERVAL_IN_SECONDS);
                if (session.isLastProcessChangesAndRecentsEnabled()) {
                    boolean processChangesAndDownloadRecents = !requestFromPeriodicSync || (TimeManager.getInstance().currentTimeMillis().longValue() - session.getLastProcessChangesAndRecentsTimeInMillis(0) >= recentsSyncIntervalInMilliseconds - 20);
                    if (processChangesAndDownloadRecents) {
                        RecentsQueryUtil.processChangesAndDownloadRecentsBlocking(session);
                        if (!CustomFieldsTask.isExecuting(session, CustomFieldsTask.class)) {
                            CustomFieldsTask.downloadAllCustomFields(session);
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            } else if (isAppStartedForTheFirstTime) {
                downloadEverything(session, new 8(this, onEnoughDataToShowUISingleRunnable), false);
                return;
            } else {
                7.execute(new Void[0]);
                return;
            }
        }
        onEnoughDataToShowUISingleRunnable.run();
    }

    private synchronized void downloadEverything(@NonNull Session session, @Nullable OnReleaseSplashScreen onReleaseSplashScreen, boolean addRecentsRequest) {
        OnReleaseSplashScreen onReleaseSplashScreenSafe = new 9(this, session, onReleaseSplashScreen);
        session.setLastDownloadEverythingTime(TimeManager.getInstance().currentTimeMillis().longValue());
        session.enableLastProcessChangesAndRecents(false);
        new DownloadUserSelfTask(session).execute(new Void[0]);
        new CurrenciesTask(session).execute(new Long[0]);
        new DownloadUsersTask(session).execute(new Void[0]);
        new ActivityTypesTask(session).execute(new String[0]);
        new CustomFieldsTask(session).execute(new String[0]);
        new 10(this, session).execute(new Void[0]);
        if (addRecentsRequest) {
            new 11(this, session).execute(new Void[0]);
        }
        if (OrganizationAddressFieldTask.isExecuting(session, OrganizationAddressFieldTask.class)) {
            onReleaseSplashScreenSafe.release();
        } else {
            new OrganizationAddressFieldTask(session, new 12(this, onReleaseSplashScreenSafe)).execute(new String[0]);
        }
        new OrganizationsTask(session).execute(new String[0]);
        new DownloadPersonsTask(session).execute(new String[0]);
        new ActivitiesListTask(session).execute(new String[0]);
        new DownloadEverythingInProgressMarker(session).execute(new Void[0]);
    }
}
