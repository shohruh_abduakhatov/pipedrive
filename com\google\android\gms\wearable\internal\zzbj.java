package com.google.android.gms.wearable.internal;

import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.wearable.Channel;
import com.google.android.gms.wearable.ChannelApi.ChannelListener;

final class zzbj implements ChannelListener {
    private final ChannelListener aUn;
    private final String hN;

    zzbj(String str, ChannelListener channelListener) {
        this.hN = (String) zzaa.zzy(str);
        this.aUn = (ChannelListener) zzaa.zzy(channelListener);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzbj)) {
            return false;
        }
        zzbj com_google_android_gms_wearable_internal_zzbj = (zzbj) obj;
        return this.aUn.equals(com_google_android_gms_wearable_internal_zzbj.aUn) && this.hN.equals(com_google_android_gms_wearable_internal_zzbj.hN);
    }

    public int hashCode() {
        return (this.hN.hashCode() * 31) + this.aUn.hashCode();
    }

    public void onChannelClosed(Channel channel, int i, int i2) {
        this.aUn.onChannelClosed(channel, i, i2);
    }

    public void onChannelOpened(Channel channel) {
        this.aUn.onChannelOpened(channel);
    }

    public void onInputClosed(Channel channel, int i, int i2) {
        this.aUn.onInputClosed(channel, i, i2);
    }

    public void onOutputClosed(Channel channel, int i, int i2) {
        this.aUn.onOutputClosed(channel, i, i2);
    }
}
