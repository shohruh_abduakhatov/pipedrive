package com.pipedrive.nearby.cards.cards;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class ActivitySummary_ViewBinding implements Unbinder {
    private ActivitySummary target;

    @UiThread
    public ActivitySummary_ViewBinding(ActivitySummary target) {
        this(target, target);
    }

    @UiThread
    public ActivitySummary_ViewBinding(ActivitySummary target, View source) {
        this.target = target;
        target.mActivityTypeImage = (ImageView) Utils.findRequiredViewAsType(source, R.id.activityTypeImage, "field 'mActivityTypeImage'", ImageView.class);
        target.mActivityTitle = (TextView) Utils.findRequiredViewAsType(source, R.id.title, "field 'mActivityTitle'", TextView.class);
        target.mActivityDate = (TextView) Utils.findRequiredViewAsType(source, R.id.date, "field 'mActivityDate'", TextView.class);
    }

    @CallSuper
    public void unbind() {
        ActivitySummary target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mActivityTypeImage = null;
        target.mActivityTitle = null;
        target.mActivityDate = null;
    }
}
