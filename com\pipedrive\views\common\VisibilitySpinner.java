package com.pipedrive.views.common;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.PersonFieldOption;
import com.pipedrive.util.persons.PersonNetworkingUtil;
import com.pipedrive.views.Spinner.LabelBuilder;
import com.pipedrive.views.Spinner.OnItemSelectedListener;
import java.util.List;

public class VisibilitySpinner extends SpinnerWithUnderlineAndLabel {

    public interface OnVisibilityChangeListener {
        void onVisibilityChanged(@NonNull PersonFieldOption personFieldOption);
    }

    public VisibilitySpinner(Context context) {
        this(context, null);
    }

    public VisibilitySpinner(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VisibilitySpinner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setup(@NonNull Session session, @NonNull final Person person, @Nullable final OnVisibilityChangeListener onVisibilityChangeListener) {
        List<PersonFieldOption> visibilityOptions = getVisibilityValues(session);
        final int defaultVisibleTo = session.getUserSettingsDefaultVisibilityForPerson(-1);
        setup(visibilityOptions, new LabelBuilder<PersonFieldOption>() {
            public String getLabel(PersonFieldOption item) {
                return item.getLabel();
            }

            public boolean isSelected(PersonFieldOption item) {
                if (person.getVisibleTo() > 0) {
                    if (person.getVisibleTo() == item.getId()) {
                        return true;
                    }
                    return false;
                } else if (defaultVisibleTo <= 0) {
                    return false;
                } else {
                    if (defaultVisibleTo != item.getId()) {
                        return false;
                    }
                    return true;
                }
            }
        }, new OnItemSelectedListener<PersonFieldOption>() {
            public void onItemSelected(PersonFieldOption item) {
                if (onVisibilityChangeListener != null) {
                    onVisibilityChangeListener.onVisibilityChanged(item);
                }
            }
        });
    }

    public void setup(@NonNull Session session, @NonNull final Deal deal, @Nullable final OnVisibilityChangeListener onVisibilityChangeListener) {
        List<PersonFieldOption> visibilityOptions = getVisibilityValues(session);
        final int defaultVisibleTo = session.getUserSettingsDefaultVisibilityForDeal(-1);
        setup(visibilityOptions, new LabelBuilder<PersonFieldOption>() {
            public String getLabel(PersonFieldOption item) {
                return item.getLabel();
            }

            public boolean isSelected(PersonFieldOption item) {
                if (deal.getVisibleTo() > 0) {
                    if (deal.getVisibleTo() == item.getId()) {
                        return true;
                    }
                    return false;
                } else if (defaultVisibleTo <= 0) {
                    return false;
                } else {
                    if (defaultVisibleTo != item.getId()) {
                        return false;
                    }
                    return true;
                }
            }
        }, new OnItemSelectedListener<PersonFieldOption>() {
            public void onItemSelected(PersonFieldOption item) {
                if (onVisibilityChangeListener != null) {
                    onVisibilityChangeListener.onVisibilityChanged(item);
                }
            }
        });
    }

    public void setup(@NonNull Session session, @NonNull final Organization organization, @Nullable final OnVisibilityChangeListener onVisibilityChangeListener) {
        List<PersonFieldOption> visibilityOptions = getVisibilityValues(session);
        final int defaultVisibleTo = session.getUserSettingsDefaultVisibilityForOrganization(-1);
        setup(visibilityOptions, new LabelBuilder<PersonFieldOption>() {
            public String getLabel(PersonFieldOption item) {
                return item.getLabel();
            }

            public boolean isSelected(PersonFieldOption item) {
                if (organization.getVisibleTo() > 0) {
                    if (organization.getVisibleTo() == item.getId()) {
                        return true;
                    }
                    return false;
                } else if (defaultVisibleTo <= 0) {
                    return false;
                } else {
                    if (defaultVisibleTo != item.getId()) {
                        return false;
                    }
                    return true;
                }
            }
        }, new OnItemSelectedListener<PersonFieldOption>() {
            public void onItemSelected(PersonFieldOption item) {
                if (onVisibilityChangeListener != null) {
                    onVisibilityChangeListener.onVisibilityChanged(item);
                }
            }
        });
    }

    private List<PersonFieldOption> getVisibilityValues(@NonNull Session session) {
        return PersonNetworkingUtil.getContactVisibilityValues(session);
    }

    protected int getLabelTextResourceId() {
        return R.string.visible_to;
    }
}
