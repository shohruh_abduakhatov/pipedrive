package com.zendesk.sdk.network.impl;

import com.zendesk.sdk.model.settings.SafeMobileSettings;
import com.zendesk.sdk.network.BaseProvider;
import com.zendesk.sdk.network.SettingsHelper;
import com.zendesk.service.ZendeskCallback;

class ZendeskSettingsHelper implements SettingsHelper {
    private BaseProvider baseProvider;

    public ZendeskSettingsHelper(BaseProvider baseProvider) {
        this.baseProvider = baseProvider;
    }

    public void loadSetting(ZendeskCallback<SafeMobileSettings> callback) {
        this.baseProvider.getSdkSettings(callback);
    }
}
