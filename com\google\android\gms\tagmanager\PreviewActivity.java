package com.google.android.gms.tagmanager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.background.ApplicationStateMonitor;
import com.newrelic.agent.android.instrumentation.Instrumented;
import com.newrelic.agent.android.tracing.TraceMachine;

@Instrumented
public class PreviewActivity extends Activity implements TraceFieldInterface {
    private void zzl(String str, String str2, String str3) {
        AlertDialog create = new Builder(this).create();
        create.setTitle(str);
        create.setMessage(str2);
        create.setButton(-1, str3, new OnClickListener(this) {
            final /* synthetic */ PreviewActivity aGk;

            {
                this.aGk = r1;
            }

            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        create.show();
    }

    public void onCreate(Bundle bundle) {
        TraceMachine.startTracing("PreviewActivity");
        try {
            TraceMachine.enterMethod(this._nr_trace, "PreviewActivity#onCreate", null);
        } catch (NoSuchFieldError e) {
            while (true) {
                TraceMachine.enterMethod(null, "PreviewActivity#onCreate", null);
            }
        }
        String valueOf;
        String str;
        try {
            super.onCreate(bundle);
            zzbo.zzdh("Preview activity");
            Uri data = getIntent().getData();
            if (!TagManager.getInstance(this).zzv(data)) {
                valueOf = String.valueOf(data);
                valueOf = new StringBuilder(String.valueOf(valueOf).length() + 73).append("Cannot preview the app with the uri: ").append(valueOf).append(". Launching current version instead.").toString();
                zzbo.zzdi(valueOf);
                zzl("Preview failure", valueOf, "Continue");
            }
            Intent launchIntentForPackage = getPackageManager().getLaunchIntentForPackage(getPackageName());
            if (launchIntentForPackage != null) {
                String str2 = "Invoke the launch activity for package name: ";
                valueOf = String.valueOf(getPackageName());
                zzbo.zzdh(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
                startActivity(launchIntentForPackage);
                TraceMachine.exitMethod();
            }
            str = "No launch activity found for package name: ";
            valueOf = String.valueOf(getPackageName());
            zzbo.zzdh(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            TraceMachine.exitMethod();
        } catch (Exception e2) {
            str = "Calling preview threw an exception: ";
            valueOf = String.valueOf(e2.getMessage());
            zzbo.e(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        }
    }

    protected void onStart() {
        super.onStart();
        ApplicationStateMonitor.getInstance().activityStarted();
    }

    protected void onStop() {
        super.onStop();
        ApplicationStateMonitor.getInstance().activityStopped();
    }
}
