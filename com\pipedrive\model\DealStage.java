package com.pipedrive.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class DealStage extends BaseDatasourceEntity implements Parcelable {
    public static final Creator CREATOR = new Creator() {
        public DealStage createFromParcel(Parcel in) {
            return new DealStage(in);
        }

        public DealStage[] newArray(int size) {
            return new DealStage[size];
        }
    };
    private int dealProbability;
    private String name;
    private int orderNr;
    private int pipelineId;

    public DealStage(Parcel in) {
        readFromParcel(in);
    }

    public int getOrderNr() {
        return this.orderNr;
    }

    public void setOrderNr(int orderNr) {
        this.orderNr = orderNr;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPipelineId() {
        return this.pipelineId;
    }

    public void setPipelineId(int pipelineId) {
        this.pipelineId = pipelineId;
    }

    public int getDealProbability() {
        return this.dealProbability;
    }

    public void setDealProbability(int dealProbability) {
        this.dealProbability = dealProbability;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(this.orderNr);
        dest.writeString(this.name);
        dest.writeInt(this.pipelineId);
        dest.writeInt(this.dealProbability);
    }

    protected void readFromParcel(Parcel in) {
        super.readFromParcel(in);
        this.orderNr = in.readInt();
        this.name = in.readString();
        this.pipelineId = in.readInt();
        this.dealProbability = in.readInt();
    }
}
