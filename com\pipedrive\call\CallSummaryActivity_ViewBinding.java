package com.pipedrive.call;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;
import com.pipedrive.views.association.DealAssociationView;

public class CallSummaryActivity_ViewBinding implements Unbinder {
    private CallSummaryActivity target;
    private View view2131820732;

    @UiThread
    public CallSummaryActivity_ViewBinding(CallSummaryActivity target) {
        this(target, target.getWindow().getDecorView());
    }

    @UiThread
    public CallSummaryActivity_ViewBinding(final CallSummaryActivity target, View source) {
        this.target = target;
        target.mToolbar = (Toolbar) Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'mToolbar'", Toolbar.class);
        target.mCheckboxDone = (CheckBox) Utils.findRequiredViewAsType(source, R.id.isDone, "field 'mCheckboxDone'", CheckBox.class);
        target.mDateAndDurationTextView = (TextView) Utils.findRequiredViewAsType(source, R.id.callDateAndDuration, "field 'mDateAndDurationTextView'", TextView.class);
        target.mOrganizationTextView = (TextView) Utils.findRequiredViewAsType(source, R.id.labelCompany, "field 'mOrganizationTextView'", TextView.class);
        target.mNameTextView = (TextView) Utils.findRequiredViewAsType(source, R.id.labelContact, "field 'mNameTextView'", TextView.class);
        target.mNoteEditText = (EditText) Utils.findRequiredViewAsType(source, R.id.note, "field 'mNoteEditText'", EditText.class);
        target.mTitle = (TextView) Utils.findRequiredViewAsType(source, R.id.title, "field 'mTitle'", TextView.class);
        target.mDealAssociationView = (DealAssociationView) Utils.findRequiredViewAsType(source, R.id.deal, "field 'mDealAssociationView'", DealAssociationView.class);
        target.mScheduleNextActivity = (SwitchCompat) Utils.findRequiredViewAsType(source, R.id.schedule_next_activity, "field 'mScheduleNextActivity'", SwitchCompat.class);
        View view = Utils.findRequiredView(source, R.id.dontLogCallButton, "method 'onDontLogButtonClicked'");
        this.view2131820732 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onDontLogButtonClicked();
            }
        });
    }

    @CallSuper
    public void unbind() {
        CallSummaryActivity target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mToolbar = null;
        target.mCheckboxDone = null;
        target.mDateAndDurationTextView = null;
        target.mOrganizationTextView = null;
        target.mNameTextView = null;
        target.mNoteEditText = null;
        target.mTitle = null;
        target.mDealAssociationView = null;
        target.mScheduleNextActivity = null;
        this.view2131820732.setOnClickListener(null);
        this.view2131820732 = null;
    }
}
