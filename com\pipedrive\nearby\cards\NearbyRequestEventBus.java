package com.pipedrive.nearby.cards;

import android.support.annotation.NonNull;
import com.pipedrive.nearby.util.Irrelevant;
import rx.Observable;
import rx.subjects.Subject;

public enum NearbyRequestEventBus {
    INSTANCE;
    
    @NonNull
    private final Subject<Irrelevant, Irrelevant> requestCompletedEvents;
    @NonNull
    private final Subject<Irrelevant, Irrelevant> requestStartedEvents;

    @NonNull
    public Observable<Irrelevant> requestStartedEvents() {
        return this.requestStartedEvents;
    }

    @NonNull
    public Observable<Irrelevant> requestCompletedEvents() {
        return this.requestCompletedEvents;
    }

    public void onRequestStarted() {
        this.requestStartedEvents.onNext(Irrelevant.INSTANCE);
    }

    public void onRequestCompleted() {
        this.requestCompletedEvents.onNext(Irrelevant.INSTANCE);
    }
}
