package com.pipedrive;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.pipedrive.activity.ActivityDetailActivity;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.ScreensMapper;
import com.pipedrive.application.Session;
import com.pipedrive.datetime.DateFormatHelper;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.fragments.LocaleHelper;
import com.pipedrive.inlineintro.InlineIntroPopup;
import com.pipedrive.util.time.TimeManager;
import com.pipedrive.views.agenda.AgendaView;
import com.pipedrive.views.agenda.AgendaView.OnTouchAgendaViewListener;
import com.pipedrive.views.calendar.CalendarView;
import com.pipedrive.views.calendar.OnExpandOrCollapseListener;
import com.pipedrive.views.navigation.NavigationToolbarButton;
import com.pipedrive.views.navigation.OnNavigationButtonClickListener;
import java.util.Calendar;
import java.util.Locale;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class CalendarActivity extends NavigationActivity implements OnNavigationButtonClickListener, OnExpandOrCollapseListener, OnTouchAgendaViewListener {
    private static final String KEY_CALENDAR = "calendar";
    private static final String KEY_SELECTED_DATE = "selectedDate";
    @BindView(2131820724)
    AgendaView agendaView;
    @NonNull
    private final CompositeSubscription allSubscriptions = new CompositeSubscription();
    @BindView(2131820721)
    View arrowImage;
    @BindView(2131820722)
    View calContainer;
    @Nullable
    private Calendar calendar;
    @BindView(2131820723)
    CalendarView calendarView;
    private InlineIntroPopup inlineIntroPopupForActivityListIcon;
    private InlineIntroPopup inlineIntroPopupForResetToTodayBtn;
    @NonNull
    private Locale locale;
    @BindView(2131820720)
    TextView monthTitle;
    @Nullable
    private Calendar selectedDate;

    private class ExpandOrCollapseAnimatorListener implements AnimatorListener {
        private ExpandOrCollapseAnimatorListener() {
        }

        public void onAnimationStart(Animator animation) {
            CalendarActivity.this.calendarView.setVisibility(8);
        }

        public void onAnimationEnd(Animator animation) {
            if (CalendarActivity.this.calendar != null) {
                CalendarActivity.this.calendarView.setup(CalendarActivity.this.locale, CalendarActivity.this.calendar, CalendarActivity.this.selectedDate);
                CalendarActivity.this.calendarView.setVisibility(0);
                LayoutParams lp = CalendarActivity.this.calContainer.getLayoutParams();
                lp.height = -2;
                CalendarActivity.this.calContainer.setLayoutParams(lp);
                if (!CalendarActivity.this.calendarView.isWeekView()) {
                    CalendarActivity.this.agendaView.onCalendarExpandAnimationEnd();
                }
            }
        }

        public void onAnimationCancel(Animator animation) {
        }

        public void onAnimationRepeat(Animator animation) {
        }
    }

    public static void startActivity(Activity activity) {
        ContextCompat.startActivity(activity, new Intent(activity, CalendarActivity.class), null);
    }

    public void onResume() {
        super.onResume();
        this.agendaView.refreshContent();
    }

    protected void onPause() {
        storeCalendarState();
        super.onPause();
    }

    protected void onSaveInstanceState(Bundle outState) {
        this.selectedDate = this.calendarView.getSelectedDate();
        outState.putSerializable(KEY_SELECTED_DATE, this.selectedDate);
        outState.putSerializable(KEY_CALENDAR, this.calendar);
        super.onSaveInstanceState(outState);
    }

    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        this.calendar = (Calendar) savedInstanceState.getSerializable(KEY_CALENDAR);
        this.selectedDate = (Calendar) savedInstanceState.getSerializable(KEY_SELECTED_DATE);
        super.onRestoreInstanceState(savedInstanceState);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_calendar);
        ButterKnife.bind((Activity) this);
        setupLocale();
        setupCalendarView();
        setupInlineIntroPopups();
    }

    private void setupInlineIntroPopups() {
        View parent = findViewById(16908290);
        this.inlineIntroPopupForActivityListIcon = new InlineIntroPopup(this, parent, 2, getSession(), 1);
        this.inlineIntroPopupForResetToTodayBtn = new InlineIntroPopup(this, parent, 3, getSession(), 0);
        this.inlineIntroPopupForActivityListIcon.setOnDismissListener(new OnDismissListener() {
            public void onDismiss() {
                CalendarActivity.this.showJumpToTodayInlineIntroIfRequired();
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_calendar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void showInlineIntroPopup(@NonNull final View anchor, @StringRes final int stringResId, @NonNull final InlineIntroPopup inlineIntroPopup) {
        anchor.post(new Runnable() {
            public void run() {
                inlineIntroPopup.showWithTitleOnly(anchor, CalendarActivity.this.getResources().getString(stringResId));
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_filter_activities:
                Toast.makeText(this, "Filter clicked", 0).show();
                return true;
            case R.id.menu_activity_list:
                this.inlineIntroPopupForActivityListIcon.dismissAndDoNotShowAgain();
                onActivityListMenuItemClicked();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onActivityListMenuItemClicked() {
        this.calendar = null;
        this.selectedDate = null;
        getSession().setIsCalendarOpen(false);
        ActivityListActivity.startActivity(this, getSession());
        finish();
        overrideTransitions(true);
    }

    private void setupLocale() {
        this.locale = Locale.getDefault();
        String languageValue = LocaleHelper.INSTANCE.getLanguageValue();
        for (Locale availableLocale : Locale.getAvailableLocales()) {
            if (availableLocale.getLanguage().equals(languageValue)) {
                this.locale = availableLocale;
            }
        }
    }

    private void setupCalendarView() {
        this.calendar = PipedriveDateTime.instanceWithCurrentDateTime().resetTime().toCalendar();
        this.selectedDate = PipedriveDateTime.instanceWithCurrentDateTime().resetTime().toCalendar();
        setSelectedDate();
        this.calendarView.setIsWeekView(true);
        this.calendarView.setup(this.locale, this.calendar, this.selectedDate);
        this.calendarView.setOnExpandOrCollapseListener(this);
        this.agendaView.setup(getSession(), this.selectedDate, false);
        this.agendaView.setOnTouchAgendaViewListener(this);
        this.allSubscriptions.add(this.calendarView.monthChanges().subscribe(new Action1<Calendar>() {
            public void call(Calendar date) {
                if (CalendarActivity.this.calendarView.isWeekView()) {
                    date.set(7, date.getFirstDayOfWeek());
                }
                CalendarActivity.this.setTitle(date);
            }
        }));
        this.allSubscriptions.add(this.calendarView.dateChanges().doOnNext(new Action1<Calendar>() {
            public void call(Calendar date) {
                if (CalendarActivity.this.calendar == null || date.get(2) == CalendarActivity.this.calendar.get(2)) {
                    CalendarActivity.this.calendar = (Calendar) date.clone();
                } else {
                    CalendarActivity.this.setTitle(date);
                }
                CalendarActivity.this.selectedDate = (Calendar) date.clone();
            }
        }).subscribe(new Action1<Calendar>() {
            public void call(Calendar calendar) {
                CalendarActivity.this.onCollapse();
                CalendarActivity.this.agendaView.setup(CalendarActivity.this.getSession(), calendar, false);
                CalendarActivity.this.showJumpToTodayInlineIntroIfRequired();
            }
        }));
        this.allSubscriptions.add(this.agendaView.activityClicks().subscribe(new Action1<com.pipedrive.model.Activity>() {
            public void call(com.pipedrive.model.Activity activity) {
                Long activitySqlId = activity.getSqlIdOrNull();
                if (activitySqlId != null) {
                    ActivityDetailActivity.startActivityForActivity(CalendarActivity.this, activitySqlId.longValue());
                }
            }
        }));
        this.allSubscriptions.add(this.agendaView.dateChanges().doOnNext(new Action1<Calendar>() {
            public void call(Calendar date) {
                CalendarActivity.this.selectedDate = (Calendar) date.clone();
                if (CalendarActivity.this.calendar != null && date.get(2) != CalendarActivity.this.calendar.get(2)) {
                    CalendarActivity.this.setTitle(date);
                }
            }
        }).subscribe(new Action1<Calendar>() {
            public void call(Calendar calendar) {
                CalendarActivity.this.calendarView.setup(CalendarActivity.this.locale, calendar, calendar);
                CalendarActivity.this.showJumpToTodayInlineIntroIfRequired();
            }
        }));
    }

    private void setSelectedDate() {
        Long savedDate = getSession().getLastSelectedCalendarDate();
        Long savedCalendar = getSession().getLastSelectedCalendarPeriod();
        if (savedDate != null) {
            this.selectedDate.setTimeInMillis(savedDate.longValue());
            this.calendar.setTimeInMillis(savedDate.longValue());
        } else if (savedCalendar != null) {
            this.calendar.setTimeInMillis(savedCalendar.longValue());
        }
    }

    private void storeCalendarState() {
        Long valueOf;
        Long l = null;
        Session session = getSession();
        if (this.selectedDate != null) {
            valueOf = Long.valueOf(this.selectedDate.getTimeInMillis());
        } else {
            valueOf = null;
        }
        session.setLastSelectedCalendarDate(valueOf);
        Session session2 = getSession();
        if (this.calendar != null) {
            l = Long.valueOf(this.calendar.getTimeInMillis());
        }
        session2.setLastSelectedCalendarPeriod(l);
    }

    @OnClick({2131820719})
    void onTitleClicked() {
        if (this.calendar == null) {
            this.calendar = PipedriveDateTime.instanceWithCurrentDateTime().resetTime().toCalendar();
        }
        if (this.calendarView.isWeekView()) {
            onExpand();
        } else {
            onCollapse();
        }
    }

    public void onExpand() {
        if (this.calendar != null) {
            this.calendarView.setIsWeekView(false);
            animateCalendarViewExpand(getResources().getDimensionPixelSize(R.dimen.calendarView.monthHeight));
            Analytics.hitScreen(ScreensMapper.SCREEN_NAME_CALENDAR_VIEW_MONTH_EXTENDED, this);
        }
    }

    public void onCollapse() {
        if (this.calendar != null) {
            if (this.selectedDate != null) {
                if (this.selectedDate.get(2) != this.calendar.get(2)) {
                    this.calendar.set(5, 1);
                } else {
                    this.calendar = (Calendar) this.selectedDate.clone();
                }
            }
            if (!this.calendarView.isWeekView()) {
                this.calendarView.setIsWeekView(true);
                animateCalendarViewCollapse(getResources().getDimensionPixelSize(R.dimen.calendarView.weekHeight));
            }
        }
    }

    private void animateCalendarViewExpand(int newHeight) {
        startExpandOrCollapseAnimation(0, 180, newHeight);
    }

    private void animateCalendarViewCollapse(int newHeight) {
        startExpandOrCollapseAnimation(180, 0, newHeight);
    }

    private void startExpandOrCollapseAnimation(int fromDegrees, int toDegrees, int height) {
        CalendarAnimations.startRotateAnimation((float) fromDegrees, (float) toDegrees, getResources().getInteger(17694721), this.arrowImage);
        AnimatorSet as = new AnimatorSet();
        as.play(CalendarAnimations.getValueAnimatorWithFading(this.calendarView.getHeight(), height, getResources().getInteger(17694721), this.calContainer, false, 1.0f));
        as.addListener(new ExpandOrCollapseAnimatorListener());
        as.start();
    }

    private void setTitle(Calendar date) {
        String monthName = DateFormatHelper.monthInStandaloneFormat(LocaleHelper.INSTANCE.getLocale()).format(date.getTime());
        if (TimeManager.getInstance().getLocalCalendar().get(1) == date.get(1)) {
            this.monthTitle.setText(monthName);
        } else {
            this.monthTitle.setText(getString(R.string.calendar.title.anotherYear, new Object[]{monthName, Integer.valueOf(selectedYear)}));
        }
        this.calendar = date;
    }

    protected void onDestroy() {
        this.allSubscriptions.clear();
        this.agendaView.unsubscribe();
        super.onDestroy();
    }

    public void onNavigationButtonClicked(NavigationToolbarButton button) {
        if (button == NavigationToolbarButton.ACTIVITIES) {
            onResetToToday();
        } else {
            super.onNavigationButtonClicked(button);
        }
    }

    private void onResetToToday() {
        if (this.calendar != null) {
            Calendar todayLocal = TimeManager.getInstance().getLocalCalendar();
            if ((!DateFormatHelper.isTodayUtc(this.calendar.getTimeInMillis())) || this.selectedDate != null) {
                this.calendar = PipedriveDateTime.instanceFromUTC(todayLocal.get(1), todayLocal.get(2), todayLocal.get(5)).toCalendar();
                this.selectedDate = (Calendar) this.calendar.clone();
                this.calendarView.setup(this.locale, this.calendar, this.selectedDate);
                setTitle(this.calendar);
                onCollapse();
                this.inlineIntroPopupForResetToTodayBtn.dismissAndDoNotShowAgain();
                this.agendaView.setup(getSession(), this.selectedDate, true);
            }
        }
    }

    @OnClick({2131820590})
    void onAddClicked() {
        if (this.selectedDate != null) {
            Calendar calendar = TimeManager.getInstance().getLocalCalendar();
            calendar.set(this.selectedDate.get(1), this.selectedDate.get(2), this.selectedDate.get(5));
            ActivityDetailActivity.startActivityForActivityOnDateWithCurrentTime(this, calendar.getTimeInMillis());
        }
    }

    public void onTouchAgendaView() {
        if (!this.calendarView.isWeekView()) {
            onCollapse();
        }
    }

    private void showJumpToTodayInlineIntroIfRequired() {
        View resetToTodayBtn = findViewById(R.id.btn_activities);
        if (this.selectedDate != null && resetToTodayBtn != null) {
            boolean selectedDateIsToday = DateFormatHelper.isTodayUtc(this.selectedDate.getTimeInMillis());
            boolean firstPopupMessagesIsNotDismissed = !nearbyItemIsDismissedForGood() || this.inlineIntroPopupForActivityListIcon.isNotDismissedForGood();
            if (!firstPopupMessagesIsNotDismissed && !selectedDateIsToday) {
                showInlineIntroPopup(resetToTodayBtn, R.string.jump_to_today, this.inlineIntroPopupForResetToTodayBtn);
            }
        }
    }

    private void showBackToListInlineIntroIfRequired() {
        View activityListIcon = findViewById(R.id.menu_activity_list);
        if (activityListIcon != null && nearbyItemIsDismissedForGood()) {
            showInlineIntroPopup(activityListIcon, R.string.back_to_list_view, this.inlineIntroPopupForActivityListIcon);
        }
    }

    protected void onNearbyIntroDismissed() {
        displayCalendarInlineIntros();
    }

    private void displayCalendarInlineIntros() {
        showBackToListInlineIntroIfRequired();
        showJumpToTodayInlineIntroIfRequired();
    }

    public void displayPopupIntros() {
        super.displayPopupIntros();
        displayCalendarInlineIntros();
    }

    public void hidePopupIntrosOnce() {
        super.hidePopupIntrosOnce();
        this.inlineIntroPopupForActivityListIcon.dismissOnce();
        this.inlineIntroPopupForResetToTodayBtn.dismissOnce();
    }
}
