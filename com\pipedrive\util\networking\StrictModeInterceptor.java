package com.pipedrive.util.networking;

import com.newrelic.agent.android.instrumentation.okhttp3.OkHttp3Instrumentation;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Interceptor;
import okhttp3.Interceptor.Chain;
import okhttp3.Request.Builder;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004XD¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lcom/pipedrive/util/networking/StrictModeInterceptor;", "Lokhttp3/Interceptor;", "()V", "PARAM_STRICT_MODE", "", "intercept", "Lokhttp3/Response;", "chain", "Lokhttp3/Interceptor$Chain;", "pipedrive_prodRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: StrictModeInterceptor.kt */
public final class StrictModeInterceptor implements Interceptor {
    private final String PARAM_STRICT_MODE = "strict_mode";

    @NotNull
    public Response intercept(@NotNull Chain chain) {
        Intrinsics.checkParameterIsNotNull(chain, "chain");
        CharSequence queryParameter = chain.request().url().queryParameter(this.PARAM_STRICT_MODE);
        Object obj = (queryParameter == null || queryParameter.length() == 0) ? 1 : null;
        if (obj != null) {
            Builder url = chain.request().newBuilder().url(chain.request().url().newBuilder().addEncodedQueryParameter(this.PARAM_STRICT_MODE, "true").build());
            Response proceed = chain.proceed(!(url instanceof Builder) ? url.build() : OkHttp3Instrumentation.build(url));
            Intrinsics.checkExpressionValueIsNotNull(proceed, "chain.proceed(newRequest)");
            return proceed;
        }
        proceed = chain.proceed(chain.request());
        Intrinsics.checkExpressionValueIsNotNull(proceed, "chain.proceed(chain.request())");
        return proceed;
    }
}
