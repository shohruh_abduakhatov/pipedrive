package com.google.android.gms.wearable.internal;

import android.content.IntentFilter;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.wearable.internal.zzaw.zza;

public class AddListenerRequest extends AbstractSafeParcelable {
    public static final Creator<AddListenerRequest> CREATOR = new zzc();
    public final zzaw aSO;
    public final IntentFilter[] aSP;
    public final String aSQ;
    public final String aSR;
    final int mVersionCode;

    AddListenerRequest(int i, IBinder iBinder, IntentFilter[] intentFilterArr, String str, String str2) {
        this.mVersionCode = i;
        if (iBinder != null) {
            this.aSO = zza.zzlu(iBinder);
        } else {
            this.aSO = null;
        }
        this.aSP = intentFilterArr;
        this.aSQ = str;
        this.aSR = str2;
    }

    public AddListenerRequest(zzbq com_google_android_gms_wearable_internal_zzbq) {
        this.mVersionCode = 1;
        this.aSO = com_google_android_gms_wearable_internal_zzbq;
        this.aSP = com_google_android_gms_wearable_internal_zzbq.zzcmy();
        this.aSQ = com_google_android_gms_wearable_internal_zzbq.zzcmz();
        this.aSR = null;
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzc.zza(this, parcel, i);
    }

    IBinder zzbai() {
        return this.aSO == null ? null : this.aSO.asBinder();
    }
}
