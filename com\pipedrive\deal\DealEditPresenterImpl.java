package com.pipedrive.deal;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.contentproviders.DealsContentProvider;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.datasource.PipelineDataSource;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Deal;
import com.pipedrive.model.DealStage;
import com.pipedrive.model.DealStatus;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.Pipeline;
import com.pipedrive.model.delta.ObjectDelta;
import com.pipedrive.store.StoreDeal;
import com.pipedrive.tasks.AsyncTask;
import java.util.ArrayList;

class DealEditPresenterImpl extends DealEditPresenter {
    private static final String STORAGE_KEY_DEAL = (DealEditPresenterImpl.class.getSimpleName() + ".deal");
    private static final String STORAGE_KEY_ORIGINAL_DEAL = (DealEditPresenterImpl.class.getSimpleName() + ".originalDeal");
    @Nullable
    private Deal mCurrentlyManagedDeal;
    private ObjectDelta<Deal> mDealObjectDelta = new ObjectDelta(Deal.class);
    private Deal mOriginalDeal;

    private class CreateDealTask extends AsyncTask<Deal, Void, Boolean> {
        CreateDealTask(Session session) {
            super(session);
        }

        protected Boolean doInBackground(Deal... params) {
            return Boolean.valueOf(new StoreDeal(getSession()).create(params[0]));
        }

        protected void onPostExecute(Boolean success) {
            if (success.booleanValue()) {
                getSession().getApplicationContext().getContentResolver().notifyChange(new DealsContentProvider(getSession()).createAllDealsUri(), null);
            }
            if (DealEditPresenterImpl.this.getView() != null) {
                ((DealEditView) DealEditPresenterImpl.this.getView()).onDealCreated(success.booleanValue());
            }
        }
    }

    private class DeleteDealTask extends AsyncTask<Long, Void, Boolean> {
        DeleteDealTask(Session session) {
            super(session);
        }

        protected Boolean doInBackground(Long... params) {
            Deal deal = (Deal) new DealsDataSource(getSession().getDatabase()).findBySqlId(params[0].longValue());
            if (deal == null) {
                return Boolean.valueOf(false);
            }
            deal.setStatus(DealStatus.DELETED);
            return Boolean.valueOf(new StoreDeal(getSession()).update(deal));
        }

        protected void onPostExecute(Boolean success) {
            if (success.booleanValue()) {
                getSession().getApplicationContext().getContentResolver().notifyChange(new DealsContentProvider(getSession()).createAllDealsUri(), null);
            }
            if (DealEditPresenterImpl.this.getView() != null) {
                ((DealEditView) DealEditPresenterImpl.this.getView()).onDealDeleted(success.booleanValue());
            }
        }
    }

    private class UpdateDealTask extends AsyncTask<Deal, Void, Boolean> {
        UpdateDealTask(Session session) {
            super(session);
        }

        protected Boolean doInBackground(Deal... params) {
            return Boolean.valueOf(new StoreDeal(getSession()).update(params[0]));
        }

        protected void onPostExecute(Boolean success) {
            if (success.booleanValue()) {
                getSession().getApplicationContext().getContentResolver().notifyChange(new DealsContentProvider(getSession()).createAllDealsUri(), null);
            }
            if (DealEditPresenterImpl.this.getView() != null) {
                ((DealEditView) DealEditPresenterImpl.this.getView()).onDealUpdated(success.booleanValue());
            }
        }
    }

    DealEditPresenterImpl(@NonNull Session session, @Nullable Bundle savedState) {
        super(session, savedState);
    }

    void requestOrRestoreDeal(@NonNull Long dealSqlId) {
        if (!restoreDeal() && !AsyncTask.isExecuting(getSession(), ReadDealTask.class)) {
            new ReadDealTask(getSession(), new Callback() {
                public void onTaskFinished(@Nullable Deal deal) {
                    DealEditPresenterImpl.this.cacheAndSendDealToView(deal);
                }
            }).executeOnAsyncTaskExecutor(this, new Long[]{dealSqlId});
        }
    }

    void requestOrRestoreNewDealForStage(@NonNull Integer stagePipedriveId) {
        if (!restoreDeal()) {
            DealStage stage = new PipelineDataSource(getSession().getDatabase()).findStageById(stagePipedriveId.intValue());
            if (stage == null) {
                cacheAndSendDealToView(null);
                return;
            }
            Deal deal = createNewDeal();
            deal.setStage(stage.getPipedriveId());
            deal.setPipelineId(stage.getPipelineId());
            cacheAndSendDealToView(deal);
        }
    }

    @NonNull
    private Deal createNewDeal() {
        Deal deal = new Deal();
        Deal.updateDealWithRequiredDefaultValues(deal, getSession());
        return deal;
    }

    void requestOrRestoreNewDealForPerson(@NonNull Long personSqlId) {
        if (!restoreDeal()) {
            Person person = (Person) new PersonsDataSource(getSession().getDatabase()).findBySqlId(personSqlId.longValue());
            if (person == null) {
                cacheAndSendDealToView(null);
                return;
            }
            Deal deal = createNewDeal();
            deal.setPerson(person);
            deal.setOrganization(person.getCompany());
            cacheAndSendDealToView(deal);
        }
    }

    void requestOrRestoreNewDealForOrganization(@NonNull Long organizationSqlId) {
        if (!restoreDeal()) {
            Organization organization = (Organization) new OrganizationsDataSource(getSession().getDatabase()).findBySqlId(organizationSqlId.longValue());
            if (organization == null) {
                cacheAndSendDealToView(null);
                return;
            }
            Deal deal = createNewDeal();
            deal.setOrganization(organization);
            cacheAndSendDealToView(deal);
        }
    }

    void requestOrRestoreNewDealWithTitle(@Nullable String dealTitle, @Nullable Long personSqlId, @Nullable Long organizationSqlId) {
        Organization organization = null;
        if (!restoreDeal()) {
            Person person;
            Deal deal = createNewDeal();
            if (personSqlId != null) {
                person = (Person) new PersonsDataSource(getSession().getDatabase()).findBySqlId(personSqlId.longValue());
            } else {
                person = null;
            }
            if (organizationSqlId != null) {
                organization = (Organization) new OrganizationsDataSource(getSession().getDatabase()).findBySqlId(organizationSqlId.longValue());
            }
            if (dealTitle != null) {
                deal.setTitle(dealTitle);
            }
            if (person != null) {
                deal.setPerson(person);
            }
            if (organization != null) {
                deal.setOrganization(organization);
            }
            cacheAndSendDealToView(deal);
        }
    }

    void changeDeal(@NonNull Deal deal) {
        if (deal.isStored()) {
            if (!AsyncTask.isExecuting(getSession(), UpdateDealTask.class)) {
                new UpdateDealTask(getSession()).executeOnAsyncTaskExecutor(this, new Deal[]{deal});
            }
        } else if (!AsyncTask.isExecuting(getSession(), CreateDealTask.class)) {
            new CreateDealTask(getSession()).executeOnAsyncTaskExecutor(this, new Deal[]{deal});
        }
    }

    void deleteDeal(@NonNull Long dealSqlId) {
        if (!AsyncTask.isExecuting(getSession(), DeleteDealTask.class)) {
            new DeleteDealTask(getSession()).executeOnAsyncTaskExecutor(this, new Long[]{dealSqlId});
        }
    }

    void associatePerson(@Nullable Person person) {
        if (this.mCurrentlyManagedDeal != null) {
            this.mCurrentlyManagedDeal.setPerson(person);
            boolean personHasOrganization = (person == null || person.getCompany() == null) ? false : true;
            boolean dealDoesntHaveOrganization;
            if (this.mCurrentlyManagedDeal.getOrganization() == null) {
                dealDoesntHaveOrganization = true;
            } else {
                dealDoesntHaveOrganization = false;
            }
            if (personHasOrganization && dealDoesntHaveOrganization) {
                this.mCurrentlyManagedDeal.setOrganization(person.getCompany());
            }
            if (getView() != null) {
                ((DealEditView) getView()).onAssociationsUpdated(this.mCurrentlyManagedDeal);
            }
        }
    }

    void associateOrganization(@Nullable Organization organization) {
        if (this.mCurrentlyManagedDeal != null) {
            this.mCurrentlyManagedDeal.setOrganization(organization);
            if (getView() != null) {
                ((DealEditView) getView()).onAssociationsUpdated(this.mCurrentlyManagedDeal);
            }
        }
    }

    void associatePersonBySqlId(@NonNull Long personSqlId) {
        associatePerson((Person) new PersonsDataSource(getSession().getDatabase()).findBySqlId(personSqlId.longValue()));
    }

    void associateOrganizationBySqlId(@NonNull Long orgSqlId) {
        associateOrganization((Organization) new OrganizationsDataSource(getSession().getDatabase()).findBySqlId(orgSqlId.longValue()));
    }

    @Nullable
    Deal getCurrentlyManagedDeal() {
        return this.mCurrentlyManagedDeal;
    }

    boolean wasDealChanged(@NonNull Deal deal) {
        return this.mDealObjectDelta.hasChanges((BaseDatasourceEntity) deal, this.mOriginalDeal);
    }

    void setPipeline(Pipeline pipeline) {
        if (this.mCurrentlyManagedDeal != null && pipeline.getPipedriveId() != this.mCurrentlyManagedDeal.getPipelineId()) {
            ArrayList<DealStage> stages = new PipelineDataSource(getSession().getDatabase()).findStagesByPipelineId((long) pipeline.getPipedriveId());
            this.mCurrentlyManagedDeal.setPipelineId(pipeline.getPipedriveId());
            if (!stages.isEmpty()) {
                this.mCurrentlyManagedDeal.setStage(((DealStage) stages.get(0)).getPipedriveId());
            }
            if (this.mView != null) {
                ((DealEditView) this.mView).onPipelineUpdated(this.mCurrentlyManagedDeal);
            }
        }
    }

    void requestDealProductsUpdate() {
        if (this.mCurrentlyManagedDeal != null && this.mCurrentlyManagedDeal.getSqlIdOrNull() != null) {
            new ReadDealTask(getSession(), new Callback() {
                public void onTaskFinished(@Nullable Deal deal) {
                    boolean dealFromDatabaseHasProducts;
                    boolean dealValueShouldBeUpdated;
                    if (deal == null || deal.getDealProducts() == null || deal.getDealProducts().isEmpty()) {
                        dealFromDatabaseHasProducts = false;
                    } else {
                        dealFromDatabaseHasProducts = true;
                    }
                    boolean productsWereRemovedFromTheCurrentlyManagedDeal;
                    if (DealEditPresenterImpl.this.mCurrentlyManagedDeal.getDealProducts() == null || DealEditPresenterImpl.this.mCurrentlyManagedDeal.getDealProducts().isEmpty() || deal == null || !(deal.getDealProducts() == null || deal.getDealProducts().isEmpty())) {
                        productsWereRemovedFromTheCurrentlyManagedDeal = false;
                    } else {
                        productsWereRemovedFromTheCurrentlyManagedDeal = true;
                    }
                    if (dealFromDatabaseHasProducts || productsWereRemovedFromTheCurrentlyManagedDeal) {
                        dealValueShouldBeUpdated = true;
                    } else {
                        dealValueShouldBeUpdated = false;
                    }
                    if (dealValueShouldBeUpdated) {
                        DealEditPresenterImpl.this.updateDealProductsAndSendToView(deal);
                    }
                }
            }).executeOnAsyncTaskExecutor(this, new Long[]{this.mCurrentlyManagedDeal.getSqlIdOrNull()});
        }
    }

    private void updateDealProductsAndSendToView(@Nullable Deal deal) {
        if (deal != null && this.mCurrentlyManagedDeal != null) {
            this.mCurrentlyManagedDeal.setValue(deal.getValue());
            this.mCurrentlyManagedDeal.setProductCount(deal.getProductCount());
            cacheAndSendDealToView(this.mCurrentlyManagedDeal);
        }
    }

    private void cacheAndSendDealToView(@Nullable Deal deal) {
        if (deal != null) {
            this.mOriginalDeal = deal.getDeepCopy();
        }
        this.mCurrentlyManagedDeal = deal;
        if (getView() != null) {
            ((DealEditView) getView()).onRequestDeal(deal);
        }
    }

    private boolean restoreDeal() {
        if (this.mCurrentlyManagedDeal == null || this.mOriginalDeal == null) {
            return false;
        }
        if (getView() != null) {
            ((DealEditView) getView()).onRequestDeal(this.mCurrentlyManagedDeal);
        }
        return true;
    }

    protected void saveToBundle(@NonNull Bundle bundle) {
        if (this.mCurrentlyManagedDeal != null) {
            bundle.putParcelable(STORAGE_KEY_DEAL, this.mCurrentlyManagedDeal);
        }
        if (this.mOriginalDeal != null) {
            bundle.putParcelable(STORAGE_KEY_ORIGINAL_DEAL, this.mOriginalDeal);
        }
    }

    protected void restoreFromBundle(@NonNull Bundle bundle) {
        this.mCurrentlyManagedDeal = (Deal) bundle.getParcelable(STORAGE_KEY_DEAL);
        this.mOriginalDeal = (Deal) bundle.getParcelable(STORAGE_KEY_ORIGINAL_DEAL);
    }

    protected String[] getKeysNeededForRestore() {
        return new String[]{STORAGE_KEY_DEAL, STORAGE_KEY_ORIGINAL_DEAL};
    }
}
