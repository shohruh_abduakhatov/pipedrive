package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzc implements Creator<ActivityTransition> {
    static void zza(ActivityTransition activityTransition, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zzc(parcel, 1, activityTransition.zzbdw());
        zzb.zzc(parcel, 2, activityTransition.zzbpx());
        zzb.zzc(parcel, 1000, activityTransition.getVersionCode());
        zzb.zzaj(parcel, zzcs);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zznq(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzuh(i);
    }

    public ActivityTransition zznq(Parcel parcel) {
        int i = 0;
        int zzcr = zza.zzcr(parcel);
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    i2 = zza.zzg(parcel, zzcq);
                    break;
                case 2:
                    i = zza.zzg(parcel, zzcq);
                    break;
                case 1000:
                    i3 = zza.zzg(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new ActivityTransition(i3, i2, i);
        }
        throw new zza.zza("Overread allowed size end=" + zzcr, parcel);
    }

    public ActivityTransition[] zzuh(int i) {
        return new ActivityTransition[i];
    }
}
