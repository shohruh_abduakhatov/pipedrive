package com.pipedrive.nearby.searchbutton;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.google.maps.android.heatmaps.WeightedLatLng;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.logging.Log;
import com.pipedrive.nearby.NearbyAnimation;
import com.pipedrive.nearby.cards.NearbyRequestEventBus;
import com.pipedrive.nearby.map.NearbyItemsProvider;
import com.pipedrive.nearby.map.Viewport;
import com.pipedrive.nearby.map.ViewportChangeProvider;
import com.pipedrive.nearby.util.Irrelevant;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.subjects.BehaviorSubject;
import rx.subjects.Subject;
import rx.subscriptions.CompositeSubscription;

public final class SearchButtonView extends FrameLayout implements SearchButton, SearchButtonClickProvider {
    private static final float INVISIBLE_ALPHA_VALUE = 0.0f;
    private static final String KEY_SAVED_VIEWPORT = (SearchButtonView.class.getSimpleName() + ".saved_viewport");
    private static final float VISIBLE_ALPHA_VALUE = 1.0f;
    @NonNull
    private final ObjectAnimator alphaObjectAnimator;
    @BindView(2131821174)
    ImageView animatedVectorContainer;
    private AnimatedVectorDrawable animatedVectorDrawable;
    @NonNull
    private final CompositeSubscription compositeSubscription = new CompositeSubscription();
    @Nullable
    private NearbyItemsProvider nearbyItemsProvider;
    @Nullable
    private Viewport savedViewport;
    @BindView(2131821173)
    View searchButton;
    @NonNull
    private final Subject<Irrelevant, Irrelevant> searchButtonClicks = BehaviorSubject.create().toSerialized();
    @Nullable
    private ViewportChangeProvider viewportChangeProvider;
    @Nullable
    private Subscription viewportChangesSubscription;

    public SearchButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.view_search_button, this);
        ButterKnife.bind((View) this);
        this.searchButton.setClickable(false);
        this.alphaObjectAnimator = NearbyAnimation.getAlphaObjectAnimator(this.searchButton);
        if (VERSION.SDK_INT >= 21) {
            setupMorphingAnimation();
        }
    }

    @RequiresApi(api = 21)
    private void setupMorphingAnimation() {
        this.animatedVectorDrawable = (AnimatedVectorDrawable) getContext().getDrawable(R.drawable.animated_vector_rect_to_circle);
        this.animatedVectorContainer.setImageDrawable(this.animatedVectorDrawable);
        this.alphaObjectAnimator.setDuration(10);
    }

    public void setup(@Nullable Bundle savedInstanceState, @NonNull Session session, @NonNull ViewportChangeProvider viewportChangeProvider, @NonNull NearbyItemsProvider nearbyItemsProvider) {
        this.viewportChangeProvider = viewportChangeProvider;
        this.nearbyItemsProvider = nearbyItemsProvider;
        if (savedInstanceState != null) {
            this.savedViewport = Viewport.fromJsonString(savedInstanceState.getString(KEY_SAVED_VIEWPORT));
        }
    }

    @NonNull
    private Boolean shouldBeVisible(@NonNull Viewport viewport) {
        boolean z = false;
        if (this.savedViewport == null) {
            return Boolean.valueOf(false);
        }
        boolean zoomLevelDiffersMoreThan1;
        if (Math.abs(this.savedViewport.getZoom().doubleValue() - viewport.getZoom().doubleValue()) >= WeightedLatLng.DEFAULT_INTENSITY) {
            zoomLevelDiffersMoreThan1 = true;
        } else {
            zoomLevelDiffersMoreThan1 = false;
        }
        boolean viewportCenterIsNotVisibleAnymore;
        if (viewport.getLatLngBounds().contains(this.savedViewport.getLatLngBounds().getCenter())) {
            viewportCenterIsNotVisibleAnymore = false;
        } else {
            viewportCenterIsNotVisibleAnymore = true;
        }
        if (zoomLevelDiffersMoreThan1 || viewportCenterIsNotVisibleAnymore) {
            z = true;
        }
        return Boolean.valueOf(z);
    }

    private void hideWithPathMorphIng() {
        if (!this.alphaObjectAnimator.isRunning() && this.searchButton.getAlpha() == VISIBLE_ALPHA_VALUE) {
            this.searchButton.setClickable(false);
            this.alphaObjectAnimator.start();
            if (VERSION.SDK_INT >= 21) {
                this.animatedVectorDrawable.start();
                this.animatedVectorContainer.setVisibility(0);
            }
        }
    }

    private void hide() {
        if (!this.alphaObjectAnimator.isRunning() && this.searchButton.getAlpha() == VISIBLE_ALPHA_VALUE) {
            this.searchButton.setClickable(false);
            this.alphaObjectAnimator.start();
        }
    }

    private void show() {
        if (!this.alphaObjectAnimator.isRunning() && this.searchButton.getAlpha() == 0.0f) {
            this.searchButton.setClickable(true);
            this.alphaObjectAnimator.reverse();
        }
    }

    @NonNull
    private Action1<Throwable> onError() {
        return new Action1<Throwable>() {
            public void call(Throwable throwable) {
                Log.e(throwable);
            }
        };
    }

    public void releaseResources() {
        this.compositeSubscription.clear();
    }

    public void bind() {
        if (this.nearbyItemsProvider != null && this.viewportChangeProvider != null) {
            this.compositeSubscription.add(NearbyRequestEventBus.INSTANCE.requestCompletedEvents().subscribe(new Action1<Irrelevant>() {
                public void call(Irrelevant irrelevant) {
                    if (!(SearchButtonView.this.viewportChangesSubscription == null || SearchButtonView.this.viewportChangesSubscription.isUnsubscribed())) {
                        SearchButtonView.this.viewportChangesSubscription.unsubscribe();
                        SearchButtonView.this.savedViewport = null;
                    }
                    SearchButtonView.this.viewportChangesSubscription = SearchButtonView.this.viewportChangeProvider.viewportChanges().observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Viewport>() {
                        public void call(Viewport viewport) {
                            if (SearchButtonView.this.savedViewport == null) {
                                SearchButtonView.this.savedViewport = viewport;
                            }
                            if (SearchButtonView.this.shouldBeVisible(viewport).booleanValue()) {
                                SearchButtonView.this.show();
                            } else {
                                SearchButtonView.this.hide();
                            }
                        }
                    }, SearchButtonView.this.onError());
                }
            }, onError()));
            this.compositeSubscription.add(NearbyRequestEventBus.INSTANCE.requestStartedEvents().observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Irrelevant>() {
                public void call(Irrelevant irrelevant) {
                    SearchButtonView.this.savedViewport = null;
                    SearchButtonView.this.hideWithPathMorphIng();
                }
            }));
        }
    }

    public void saveState(@NonNull Bundle outState) {
        if (this.savedViewport != null) {
            outState.putString(KEY_SAVED_VIEWPORT, this.savedViewport.toJsonString());
        }
    }

    public boolean isHidden() {
        return this.searchButton.getAlpha() == 0.0f;
    }

    @OnClick({2131821173})
    void onSearchButtonClicked() {
        hideWithPathMorphIng();
        this.searchButtonClicks.onNext(Irrelevant.INSTANCE);
        NearbyRequestEventBus.INSTANCE.onRequestStarted();
    }

    @NonNull
    public Observable<Irrelevant> searchButtonClicks() {
        return this.searchButtonClicks.share();
    }
}
