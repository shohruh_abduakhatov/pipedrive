package com.pipedrive.util.networking.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.Date;

public class FlowFileEntity {
    @SerializedName("active_flag")
    @Expose
    private Boolean mActive;
    @SerializedName("activity_id")
    @Expose
    private Integer mActivityId;
    @SerializedName("add_time")
    @Expose
    private Date mAddTime;
    @SerializedName("comment")
    @Expose
    private String mComment;
    @SerializedName("deal_id")
    @Expose
    private Integer mDealId;
    @SerializedName("deal_name")
    @Expose
    private String mDealName;
    @SerializedName("email_message_id")
    @Expose
    private Integer mEmailMessageId;
    @SerializedName("file_name")
    @Expose
    private String mFileName;
    @SerializedName("file_size")
    @Expose
    private Long mFileSize;
    @SerializedName("file_type")
    @Expose
    private String mFileType;
    @SerializedName("inline_flag")
    @Expose
    private Boolean mInline;
    @SerializedName("log_id")
    @Expose
    private Integer mLogId;
    @SerializedName("name")
    @Expose
    private String mName;
    @SerializedName("note_id")
    @Expose
    private Integer mNoteId;
    @SerializedName("org_id")
    @Expose
    private Integer mOrgId;
    @SerializedName("org_name")
    @Expose
    private String mOrgName;
    @SerializedName("people_name")
    @Expose
    private String mPeopleName;
    @SerializedName("person_id")
    @Expose
    private Integer mPersonId;
    @SerializedName("person_name")
    @Expose
    private String mPersonName;
    @SerializedName("id")
    @Expose
    private Integer mPipedriveId;
    @SerializedName("product_id")
    @Expose
    private Integer mProductId;
    @SerializedName("product_name")
    @Expose
    private String mProductName;
    @SerializedName("remote_id")
    @Expose
    private String mRemoteId;
    @SerializedName("remote_location")
    @Expose
    private String mRemoteLocation;
    @SerializedName("update_time")
    @Expose
    private Date mUpdateTime;
    @SerializedName("url")
    @Expose
    private String mUrl;
    @SerializedName("user_id")
    @Expose
    private Integer mUserId;

    private FlowFileEntity() {
    }

    public Integer getPipedriveId() {
        return this.mPipedriveId;
    }

    public Integer getDealId() {
        return this.mDealId;
    }

    public String getDealName() {
        return this.mDealName;
    }

    public Integer getUserId() {
        return this.mUserId;
    }

    public Integer getPersonId() {
        return this.mPersonId;
    }

    public String getPersonName() {
        return this.mPersonName;
    }

    public Integer getOrgId() {
        return this.mOrgId;
    }

    public String getOrgName() {
        return this.mOrgName;
    }

    public Integer getProductId() {
        return this.mProductId;
    }

    public String getProductName() {
        return this.mProductName;
    }

    public Integer getEmailMessageId() {
        return this.mEmailMessageId;
    }

    public Integer getActivityId() {
        return this.mActivityId;
    }

    public Integer getNoteId() {
        return this.mNoteId;
    }

    public Integer getLogId() {
        return this.mLogId;
    }

    public Date getAddTime() {
        return this.mAddTime;
    }

    public Date getUpdateTime() {
        return this.mUpdateTime;
    }

    public String getFileName() {
        return this.mFileName;
    }

    public String getFileType() {
        return this.mFileType;
    }

    public Long getFileSize() {
        return this.mFileSize;
    }

    public Boolean isActive() {
        return this.mActive;
    }

    public Boolean isInline() {
        return this.mInline;
    }

    public String getComment() {
        return this.mComment;
    }

    public String getRemoteLocation() {
        return this.mRemoteLocation;
    }

    public String getRemoteId() {
        return this.mRemoteId;
    }

    public String getPeopleName() {
        return this.mPeopleName;
    }

    public String getUrl() {
        return this.mUrl;
    }

    public String getName() {
        return this.mName;
    }
}
