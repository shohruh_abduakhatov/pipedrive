package com.google.android.gms.wearable;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzf;
import com.google.android.gms.wearable.internal.zzz;

public class DataEventBuffer extends zzf<DataEvent> implements Result {
    private final Status hv;

    public DataEventBuffer(DataHolder dataHolder) {
        super(dataHolder);
        this.hv = new Status(dataHolder.getStatusCode());
    }

    public Status getStatus() {
        return this.hv;
    }

    protected String zzauq() {
        return "path";
    }

    protected /* synthetic */ Object zzn(int i, int i2) {
        return zzy(i, i2);
    }

    protected DataEvent zzy(int i, int i2) {
        return new zzz(this.zy, i, i2);
    }
}
