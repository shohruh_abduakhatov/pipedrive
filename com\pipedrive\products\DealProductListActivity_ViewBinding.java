package com.pipedrive.products;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class DealProductListActivity_ViewBinding implements Unbinder {
    private DealProductListActivity target;
    private View view2131820824;

    @UiThread
    public DealProductListActivity_ViewBinding(DealProductListActivity target) {
        this(target, target.getWindow().getDecorView());
    }

    @UiThread
    public DealProductListActivity_ViewBinding(final DealProductListActivity target, View source) {
        this.target = target;
        target.mToolbar = (Toolbar) Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'mToolbar'", Toolbar.class);
        target.mRecyclerView = (RecyclerView) Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'mRecyclerView'", RecyclerView.class);
        target.mProgressBar = Utils.findRequiredView(source, R.id.progress, "field 'mProgressBar'");
        target.mFailedRequestMessage = (TextView) Utils.findRequiredViewAsType(source, R.id.message, "field 'mFailedRequestMessage'", TextView.class);
        View view = Utils.findRequiredView(source, R.id.fab, "field 'mFloatingActionButton' and method 'onFabClicked'");
        target.mFloatingActionButton = (FloatingActionButton) Utils.castView(view, R.id.fab, "field 'mFloatingActionButton'", FloatingActionButton.class);
        this.view2131820824 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onFabClicked();
            }
        });
        target.mContainer = Utils.findRequiredView(source, R.id.recyclerContainer, "field 'mContainer'");
    }

    @CallSuper
    public void unbind() {
        DealProductListActivity target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mToolbar = null;
        target.mRecyclerView = null;
        target.mProgressBar = null;
        target.mFailedRequestMessage = null;
        target.mFloatingActionButton = null;
        target.mContainer = null;
        this.view2131820824.setOnClickListener(null);
        this.view2131820824 = null;
    }
}
