package com.pipedrive.analytics;

import android.support.annotation.NonNull;

public enum AnalyticsEvent {
    ACTIVITY_MARKED_AS_DONE_FROM_NOTIFICATION("Android Notifications", "Mark as done"),
    ACTIVITY_OPENED_FROM_NOTIFICATION("Android Notifications", "Open notification"),
    SYNC_DATA_PRESSED("More View", "Sync data"),
    LOG_OUT_PRESSED("More View", "Log out"),
    COMPANY_CHANGED("More View", "Change company"),
    LANGUAGE_CHANGED("More View", "Change language"),
    ACTIVITY_REMINDER_OPTION_CHANGED("More View", "Change activity reminder"),
    ALL_DAY_ACTIVITY_REMINDER_OPTION_CHANGED("More View", "Change all-day activity reminder"),
    TIPS_AND_TRICKS_PRESSED("More View", "Tips and Tricks"),
    MY_FEEDBACK_PRESSED("More View", "My Feedback"),
    OPEN_DEAL_DETAIL_VIEW("Nearby Events", "Open Deal Detail View"),
    OPEN_PERSON_DETAIL_VIEW("Nearby Events", "Open Person Detail View"),
    OPEN_ORG_DETAIL_VIEW("Nearby Events", "Open Org Detail View"),
    DIRECTIONS_FOR_DEAL_EXPANDED("Nearby Events", "Directions for Deal (Expanded)"),
    DIRECTIONS_FOR_DEAL_COLLAPSED("Nearby Events", "Directions for Deal (Collapsed)"),
    DIRECTIONS_FOR_PERSON_EXPANDED("Nearby Events", "Directions for Person (Expanded)"),
    DIRECTIONS_FOR_PERSON_COLLAPSED("Nearby Events", "Directions for Person (Collapsed)"),
    DIRECTIONS_FOR_ORG_EXPANDED("Nearby Events", "Directions for Org (Expanded)"),
    DIRECTIONS_FOR_ORG_COLLAPSED("Nearby Events", "Directions for Org (Collapsed)"),
    EMAIL_DEAL("Nearby Events", " Email on Deal"),
    EMAIL_PERSON("Nearby Events", "Email on Person"),
    TEXT_MESSAGE_PERSON("Nearby Events", "Text Message on Person"),
    TEXT_MESSAGE_DEAL("Nearby Events", "Text Message on Deal"),
    CALL_DEAL("Nearby Events", "Call Person on Deal"),
    CALL_PERSON("Nearby Events", "Call Person on Person"),
    CARD_SWIPE("Nearby Events", "Card swipe"),
    TAPPING_THE_MARKER("Nearby Events", "Tapping the marker"),
    LOG_IN("Login View", "Log in"),
    SIGN_UP("Login View", "Sign up"),
    CONNECT_WITH_GOOGLE("Login View", "Connect with Google"),
    CANCEL_GOOGLE_LOGIN_MODAL("Login View", "Cancel Google login modal"),
    SIGN_UP_ON_GOOGLE_LOGIN_MODAL("Login View", "Sign up on Google login modal"),
    FORGOT_PASSWORD_ON_PASSWORD_EMAIL_LOGIN_FAILURE_MODAL("Login View", "Forgot Password on Password/email login failure modal"),
    OK_ON_PASSWORD_EMAIL_LOGIN_FAILURE_MODAL("Login View", "OK on Password/email login failure modal");
    
    @NonNull
    private final String action;
    @NonNull
    private final String category;

    enum Categories {
        private static final /* synthetic */ Categories[] $VALUES = null;
        private static final String CATEGORY_LOGIN_VIEW = "Login View";
        static final String CATEGORY_MORE_VIEW = "More View";
        static final String CATEGORY_NEARBY_EVENTS = "Nearby Events";
        static final String CATEGORY_NOTIFICATIONS = "Android Notifications";

        static {
            $VALUES = new Categories[0];
        }

        private Categories(String str, int i) {
        }

        public static Categories valueOf(String name) {
            return (Categories) Enum.valueOf(Categories.class, name);
        }

        public static Categories[] values() {
            return (Categories[]) $VALUES.clone();
        }
    }

    private AnalyticsEvent(@NonNull String category, @NonNull String action) {
        this.action = action;
        this.category = category;
    }

    @NonNull
    String getCategory() {
        return this.category;
    }

    @NonNull
    String getAction() {
        return this.action;
    }
}
