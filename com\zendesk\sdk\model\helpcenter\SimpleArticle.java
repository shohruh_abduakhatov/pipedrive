package com.zendesk.sdk.model.helpcenter;

import java.io.Serializable;

public class SimpleArticle implements Serializable {
    private Long id;
    private String title;

    public SimpleArticle(Long id, String title) {
        this.id = id;
        this.title = title;
    }

    public Long getId() {
        return this.id;
    }

    public String getTitle() {
        return this.title;
    }
}
