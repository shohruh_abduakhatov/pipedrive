package com.pipedrive.nearby.cards.cards;

import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.model.Deal;
import com.pipedrive.model.DealStatus;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.nearby.model.DealNearbyItem;
import com.pipedrive.util.ImageUtil;
import com.pipedrive.views.SelectedStagesIndicator;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

public class DealCardHeader extends CardHeader<DealNearbyItem, Deal> {
    @BindView(2131821144)
    ImageView dealIconImageView;
    @BindView(2131821147)
    TextView organizationTextView;
    @BindView(2131821146)
    TextView personTextView;
    @BindView(2131821145)
    SelectedStagesIndicator selectedStagesIndicator;
    private View view;

    @NonNull
    String getSubTitle(@NonNull Session session, @NonNull DealNearbyItem nearbyItem, @NonNull Deal entity) {
        return entity.getFormattedValue();
    }

    @NonNull
    protected String getTitle(@NonNull Deal entity) {
        return entity.getTitle();
    }

    public void bind(@NonNull ViewGroup view, @NonNull Session session, @NonNull DealNearbyItem nearbyItem, @NonNull final Deal entity, @Nullable Double distanceInMeters) {
        boolean personExists;
        int i;
        boolean organizationExists = true;
        int i2 = 0;
        super.bind(view, session, nearbyItem, entity, distanceInMeters);
        this.view = view;
        this.compositeSubscription.add(nearbyItem.getStagesTotalCountAndSelectedStageOrder(session, Integer.valueOf(entity.getStage())).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Pair<Integer, Integer>>() {
            public void call(Pair<Integer, Integer> stageData) {
                int selectedStage = ((Integer) stageData.second).intValue() + 1;
                DealCardHeader.this.setSelectedStagesIndicator(entity, selectedStage, stageData.first.intValue());
            }
        }, getDefaultOnErrorActionForCards()));
        Person person = entity.getPerson();
        if (person != null) {
            personExists = true;
        } else {
            personExists = false;
        }
        if (personExists) {
            this.personTextView.setText(person.getName());
        }
        TextView textView = this.personTextView;
        if (personExists) {
            i = 0;
        } else {
            i = 8;
        }
        textView.setVisibility(i);
        Organization organization = entity.getOrganization();
        if (organization == null) {
            organizationExists = false;
        }
        if (organizationExists) {
            this.organizationTextView.setText(organization.getName());
        }
        TextView textView2 = this.organizationTextView;
        if (!organizationExists) {
            i2 = 8;
        }
        textView2.setVisibility(i2);
        applyRottenOrNormalColors(entity.isDealRotten());
    }

    private void setSelectedStagesIndicator(@NonNull Deal entity, int selectedStage, int totalStageCount) {
        this.selectedStagesIndicator.setup(selectedStage, totalStageCount, entity.isDealRotten() ? R.color.red : R.color.white, entity.isDealRotten() ? R.color.whiteSecondary : R.color.stage_selector_stage_progress_background_green, entity.isDealRotten() ? R.color.white : R.color.green);
    }

    private void setupDrawablesTintColor(boolean isDealRotten) {
        this.organizationTextView.setCompoundDrawables(getTintedDrawable(isDealRotten, this.organizationTextView.getCompoundDrawables()[0], R.color.card_secondary_icon), null, null, null);
        this.personTextView.setCompoundDrawables(getTintedDrawable(isDealRotten, this.personTextView.getCompoundDrawables()[0], R.color.card_secondary_icon), null, null, null);
        this.dealIconImageView.setImageDrawable(getTintedDrawable(isDealRotten, this.dealIconImageView.getDrawable(), R.color.card_primary_icon));
    }

    private void setupSecondaryTextViewsColor(boolean isDealRotten) {
        int textColor = getColor(isDealRotten ? R.color.card_subtitle_text_rotten : R.color.card_subtitle_text);
        this.subtitleTextView.setTextColor(textColor);
        this.personTextView.setTextColor(textColor);
        this.organizationTextView.setTextColor(textColor);
    }

    private void applyRottenOrNormalColors(boolean isDealRotten) {
        this.view.setBackgroundColor(getColor(isDealRotten ? R.color.card_background_rotten : R.color.card_background));
        this.titleTextView.setTextColor(getColor(isDealRotten ? R.color.card_title_text_rotten : R.color.card_title_text));
        setupSecondaryTextViewsColor(isDealRotten);
        setupDrawablesTintColor(isDealRotten);
    }

    private void addStatusToSubtitle(@NonNull Deal entity) {
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder(this.subtitleTextView.getText());
        String statusString = null;
        int colorRes = 0;
        if (entity.isDealRotten()) {
            statusString = this.context.getResources().getString(R.string.rotting).toUpperCase();
            colorRes = R.color.card_subtitle_text_rotten;
        } else if (entity.getStatus() == DealStatus.WON) {
            statusString = this.context.getResources().getString(R.string.won).toUpperCase();
            colorRes = R.color.green;
        } else if (entity.getStatus() == DealStatus.DELETED) {
            statusString = this.context.getResources().getString(R.string.deleted).toUpperCase();
            colorRes = R.color.card_subtitle_text;
        } else if (entity.getStatus() == DealStatus.LOST) {
            statusString = this.context.getResources().getString(R.string.lost).toUpperCase();
            colorRes = R.color.red;
        }
        if (statusString != null) {
            stringBuilder.insert(0, statusString + this.subtitleSeparator);
            stringBuilder.setSpan(new ForegroundColorSpan(getColor(colorRes)), 0, statusString.length(), 0);
            stringBuilder.setSpan(new TypefaceSpan("sans-serif"), 0, statusString.length(), 0);
            stringBuilder.setSpan(new StyleSpan(1), 0, statusString.length(), 0);
            this.subtitleTextView.setText(stringBuilder);
        }
    }

    private Drawable getTintedDrawable(boolean isDealRotten, @NonNull Drawable drawable, @ColorRes int defaultColor) {
        if (isDealRotten) {
            defaultColor = R.color.card_icon_rotten;
        }
        return ImageUtil.getTintedDrawable(drawable, getColor(defaultColor));
    }

    private int getColor(@ColorRes int colorResId) {
        return ContextCompat.getColor(this.context, colorResId);
    }

    void setSubtitle(@NonNull Session session, @NonNull DealNearbyItem nearbyItem, @NonNull Deal entity) {
        super.setSubtitle(session, nearbyItem, entity);
        addStatusToSubtitle(entity);
    }

    public void propagateClickEvents(@NonNull Long itemSqlId) {
        CardEventBus.INSTANCE.onDealDetailsClicked(itemSqlId);
    }

    @NonNull
    private Action1<Throwable> getDefaultOnErrorActionForCards() {
        return new Action1<Throwable>() {
            public void call(Throwable throwable) {
            }
        };
    }
}
