package com.zendesk.sdk.util;

import android.support.annotation.NonNull;

public interface DependencyProvider<T> {
    @NonNull
    T provideDependency();
}
