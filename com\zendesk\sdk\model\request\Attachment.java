package com.zendesk.sdk.model.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.util.CollectionUtils;
import java.util.List;

public class Attachment {
    private String contentType;
    private String contentUrl;
    private String fileName;
    private Long id;
    private String mappedContentUrl;
    private Long size;
    private List<Attachment> thumbnails;
    private String url;

    @Nullable
    public String getUrl() {
        return this.url;
    }

    @Nullable
    public Long getId() {
        return this.id;
    }

    @Nullable
    public String getFileName() {
        return this.fileName;
    }

    @Nullable
    public String getContentUrl() {
        return this.contentUrl;
    }

    @Nullable
    public String getContentType() {
        return this.contentType;
    }

    public Long getSize() {
        return this.size;
    }

    @NonNull
    public List<Attachment> getThumbnails() {
        return CollectionUtils.copyOf(this.thumbnails);
    }
}
