package com.pipedrive.note;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.navigator.Navigator.Type;
import com.pipedrive.navigator.buttons.DeleteMenuItem;
import com.pipedrive.navigator.buttons.DiscardMenuItem;
import com.pipedrive.navigator.buttons.NavigatorMenuItem;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.util.snackbar.SnackBarManager;
import java.util.Arrays;
import java.util.List;

public class ActivityNoteEditorActivity extends HtmlEditorActivity<ActivityNoteEditorPresenter> implements ActivityNoteEditorView {
    public static final String KEY_NOTE_CONTENT = "NOTE_CONTENT";
    private static final String KEY_SQL_ID = "SQL_ID";
    private DeleteMenuItem mDeleteMenuItem = new DeleteMenuItem() {
        public boolean isEnabled() {
            return ((ActivityNoteEditorPresenter) ActivityNoteEditorActivity.this.mPresenter).isContentExist();
        }

        public void onClick() {
            ActivityNoteEditorActivity.this.onDelete();
        }
    };
    private DiscardMenuItem mDiscardMenuItem = new DiscardMenuItem() {
        public void onClick() {
            ActivityNoteEditorActivity.this.onCancel();
        }
    };

    public static void updateActivityNote(@NonNull Activity context, @NonNull com.pipedrive.model.Activity activity, int requestCode) {
        if (activity.getSqlIdOrNull() != null) {
            ActivityCompat.startActivityForResult(context, new Intent(context, ActivityNoteEditorActivity.class).putExtra(KEY_SQL_ID, activity.getSqlIdOrNull()), requestCode, null);
        }
    }

    public static void updateActivityNote(@NonNull Activity context, @Nullable String noteContent, int requestCode) {
        ActivityCompat.startActivityForResult(context, new Intent(context, ActivityNoteEditorActivity.class).putExtra(KEY_NOTE_CONTENT, noteContent), requestCode, null);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavigatorType(Type.UPDATE);
    }

    protected boolean requestActivityNote(@Nullable Bundle args) {
        if (args == null) {
            return false;
        }
        boolean existingActivity;
        long activitySqlId = args.getLong(KEY_SQL_ID);
        if (activitySqlId > 0) {
            existingActivity = true;
        } else {
            existingActivity = false;
        }
        if (existingActivity) {
            ((ActivityNoteEditorPresenter) this.mPresenter).requestExistingActivityNote(activitySqlId);
        } else {
            ((ActivityNoteEditorPresenter) this.mPresenter).requestNoteForNewActivity(args.getString(KEY_NOTE_CONTENT));
        }
        return true;
    }

    public void onResume() {
        super.onResume();
        com.pipedrive.model.Activity activity = (com.pipedrive.model.Activity) ((ActivityNoteEditorPresenter) this.mPresenter).getData();
        if (activity != null) {
            onDataRequested(activity);
        } else if (!requestActivityNote(getIntent().getExtras())) {
            finish();
        }
    }

    public void onCreateOrUpdate() {
        ((com.pipedrive.model.Activity) ((ActivityNoteEditorPresenter) this.mPresenter).getData()).setHtmlContent(getNoteContentAsHtml());
        ((ActivityNoteEditorPresenter) this.mPresenter).changeActivityNote();
    }

    public void createOrUpdateDiscardedAsRequiredFieldsAreNotSet() {
    }

    private void onDelete() {
        ViewUtil.showDeleteConfirmationDialog(this, getResources().getString(R.string.dialog_delete_note), new Runnable() {
            public void run() {
                ((ActivityNoteEditorPresenter) ActivityNoteEditorActivity.this.mPresenter).deleteActivityNote();
            }
        });
    }

    public void onCancel() {
        setResult(0);
        finish();
    }

    @Nullable
    public List<? extends NavigatorMenuItem> getAdditionalButtons() {
        return Arrays.asList(new NavigatorMenuItem[]{this.mDeleteMenuItem, this.mDiscardMenuItem});
    }

    public void onActivityNoteUpdated(boolean success) {
        processActivityNoteCrudResponse(success);
        if (success) {
            SnackBarManager.INSTANCE.setDelayedSnackBar(R.string.note_updated);
        }
    }

    public void onActivityNoteDeleted(boolean success) {
        processActivityNoteCrudResponse(success);
        if (success) {
            SnackBarManager.INSTANCE.setDelayedSnackBar(R.string.note_deleted);
        }
    }

    private void processActivityNoteCrudResponse(boolean success) {
        if (success) {
            setResult(-1, new Intent().putExtra(KEY_NOTE_CONTENT, ((com.pipedrive.model.Activity) ((ActivityNoteEditorPresenter) this.mPresenter).getData()).getHtmlContent()));
            finish();
            return;
        }
        ViewUtil.showErrorToast(this, R.string.error_note_save_failed);
    }

    protected ActivityNoteEditorPresenter instantiatePresenter(@NonNull Session session, @Nullable Bundle savedState) {
        return new ActivityNoteEditorPresenterImpl(session, savedState);
    }
}
