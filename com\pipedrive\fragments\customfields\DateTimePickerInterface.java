package com.pipedrive.fragments.customfields;

import java.util.Date;

public interface DateTimePickerInterface {
    void dateChanged(Date date, boolean z);
}
