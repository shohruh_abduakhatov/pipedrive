package com.zendesk.sdk.deeplinking.targets;

public enum DeepLinkType {
    Request,
    Article,
    Unknown
}
