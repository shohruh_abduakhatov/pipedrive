package com.pipedrive.util.devices;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import rx.Completable;
import rx.functions.Func1;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "Lrx/Completable;", "deviceEntity", "Lcom/pipedrive/util/devices/DeviceEntity;", "kotlin.jvm.PlatformType", "call"}, k = 3, mv = {1, 1, 6})
/* compiled from: MobileDevices.kt */
final class MobileDevices$updateDevice$2<T, R> implements Func1<DeviceEntity, Completable> {
    final /* synthetic */ MobileDevices this$0;

    MobileDevices$updateDevice$2(MobileDevices mobileDevices) {
        this.this$0 = mobileDevices;
    }

    @NotNull
    public final Completable call(DeviceEntity deviceEntity) {
        MobileDevicesApi access$getApi$p = this.this$0.api;
        String pipedriveId = deviceEntity.getPipedriveId();
        if (pipedriveId == null) {
            Intrinsics.throwNpe();
        }
        Intrinsics.checkExpressionValueIsNotNull(deviceEntity, "deviceEntity");
        return access$getApi$p.update(pipedriveId, deviceEntity);
    }
}
