package com.pipedrive.util;

import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.PipelineDataSource;
import com.pipedrive.gson.GsonHelper;
import com.pipedrive.model.DealStage;
import com.pipedrive.util.networking.ApiUrlBuilder.UrlTemplates;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.ConnectionUtil$JsonReaderInterceptor;
import com.pipedrive.util.networking.Response;
import com.pipedrive.util.networking.entities.dealstage.DealStageEntity;
import com.pipedrive.util.networking.entities.dealstage.DealStageParser;
import java.io.IOException;

public enum StagesNetworkingUtil {
    ;

    @WorkerThread
    public static void downloadStagesBlocking(@NonNull Session session, long pipelineId) {
        ConnectionUtil.requestGet(UrlTemplates.requestStagesForPipelineId(session, Long.valueOf(pipelineId)), parseAndStoreStages());
    }

    @NonNull
    private static ConnectionUtil$JsonReaderInterceptor<Response> parseAndStoreStages() {
        return new ConnectionUtil$JsonReaderInterceptor<Response>() {
            public Response interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull Response responseTemplate) throws IOException {
                JsonElement root = new JsonParser().parse(jsonReader);
                if (root.isJsonArray()) {
                    PipelineDataSource pipelineDataSource = new PipelineDataSource(session.getDatabase());
                    for (DealStageEntity dealDealStageEntity : (DealStageEntity[]) GsonHelper.fromJSON(root, DealStageEntity[].class)) {
                        DealStage stage = DealStageParser.getDealStageFromEntity(dealDealStageEntity);
                        if (stage != null) {
                            pipelineDataSource.createOrUpdateStage(stage);
                        }
                    }
                }
                return responseTemplate;
            }
        };
    }
}
