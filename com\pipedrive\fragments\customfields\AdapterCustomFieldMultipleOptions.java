package com.pipedrive.fragments.customfields;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import butterknife.ButterKnife;
import com.google.android.gms.plus.PlusShare;
import com.pipedrive.R;
import com.pipedrive.datasource.PipeSQLiteHelper;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import rx.Observable;
import rx.functions.Func1;

final class AdapterCustomFieldMultipleOptions extends BaseAdapter {
    @NonNull
    private final LayoutInflater mLayoutInflater;
    List<Option> mOptions = new ArrayList();

    private static class Option {
        @NonNull
        final String id;
        @NonNull
        final String name;
        @NonNull
        Boolean selected;

        Option(@NonNull String name, @NonNull String id, @NonNull Boolean selected) {
            this.name = name;
            this.id = id;
            this.selected = selected;
        }
    }

    static AdapterCustomFieldMultipleOptions getAdapter(@NonNull Context context, @Nullable JSONArray allPossibleOptions, @NonNull List<String> currentlySelectedOptions) {
        return new AdapterCustomFieldMultipleOptions(context, getOptionsForAdapter(allPossibleOptions, currentlySelectedOptions));
    }

    @NonNull
    private static List<Option> getOptionsForAdapter(@Nullable JSONArray allPossibleOptions, @NonNull List<String> currentlySelectedOption) {
        List<Option> options = new ArrayList();
        if (allPossibleOptions != null) {
            for (int i = 0; i < allPossibleOptions.length(); i++) {
                JSONObject option = allPossibleOptions.optJSONObject(i);
                if (option != null) {
                    String optionName = option.optString(PlusShare.KEY_CALL_TO_ACTION_LABEL, null);
                    String optionId = option.optString(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, null);
                    if (!(optionName == null || optionId == null)) {
                        options.add(new Option(optionName, optionId, Boolean.valueOf(currentlySelectedOption.contains(optionId))));
                    }
                }
            }
        }
        return options;
    }

    private AdapterCustomFieldMultipleOptions(@NonNull Context context, List<Option> options) {
        this.mOptions = options;
        this.mLayoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    List<String> getSelectedOptionIds() {
        return (List) Observable.from(this.mOptions).filter(new Func1<Option, Boolean>() {
            public Boolean call(Option option) {
                return option.selected;
            }
        }).map(new Func1<Option, String>() {
            public String call(Option option) {
                return option.id;
            }
        }).toList().toBlocking().single();
    }

    public int getCount() {
        return this.mOptions.size();
    }

    public Object getItem(int position) {
        return this.mOptions.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    @SuppressLint({"ViewHolder"})
    @NonNull
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = this.mLayoutInflater.inflate(R.layout.row_custom_field_edit_multiple_options, parent, false);
        CheckBox checkBox = (CheckBox) ButterKnife.findById(convertView, (int) R.id.checkBoxOption);
        Option option = (Option) getItem(position);
        if (option != null) {
            setupCheckboxView(checkBox, option);
        }
        return convertView;
    }

    private void setupCheckboxView(@NonNull CheckBox checkBox, @NonNull Option option) {
        checkBox.setTag(R.id.cf_multioptions_id, option.id);
        checkBox.setText(option.name);
        checkBox.setChecked(option.selected.booleanValue());
        checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String optionId = retrieveOptionId(buttonView);
                if (optionId != null) {
                    updateOptions(optionId, isChecked);
                }
            }

            @Nullable
            private String retrieveOptionId(@NonNull CompoundButton checkBox) {
                Object tag = checkBox.getTag(R.id.cf_multioptions_id);
                if (tag == null) {
                    return null;
                }
                return (String) tag;
            }

            private void updateOptions(@NonNull String optionId, boolean isChecked) {
                for (Option option : AdapterCustomFieldMultipleOptions.this.mOptions) {
                    if (TextUtils.equals(option.id, optionId)) {
                        option.selected = Boolean.valueOf(isChecked);
                        break;
                    }
                }
                AdapterCustomFieldMultipleOptions.this.notifyDataSetChanged();
            }
        });
    }
}
