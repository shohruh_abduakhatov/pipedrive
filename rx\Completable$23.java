package rx;

import rx.plugins.RxJavaHooks;

class Completable$23 implements Completable$OnSubscribe {
    final /* synthetic */ Completable this$0;
    final /* synthetic */ Completable$Operator val$onLift;

    Completable$23(Completable completable, Completable$Operator completable$Operator) {
        this.this$0 = completable;
        this.val$onLift = completable$Operator;
    }

    public void call(CompletableSubscriber s) {
        try {
            this.this$0.unsafeSubscribe((CompletableSubscriber) RxJavaHooks.onCompletableLift(this.val$onLift).call(s));
        } catch (NullPointerException ex) {
            throw ex;
        } catch (Throwable ex2) {
            NullPointerException toNpe = Completable.toNpe(ex2);
        }
    }
}
