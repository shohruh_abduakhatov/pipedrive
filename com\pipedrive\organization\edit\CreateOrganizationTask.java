package com.pipedrive.organization.edit;

import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.model.Organization;
import com.pipedrive.store.StoreOrganization;

class CreateOrganizationTask extends OrganizationCrudTask {
    public CreateOrganizationTask(Session session, @Nullable OnTaskFinished onTaskFinished) {
        super(session, onTaskFinished);
    }

    protected Boolean doInBackground(Organization... params) {
        return Boolean.valueOf(new StoreOrganization(getSession()).create(params[0]));
    }
}
