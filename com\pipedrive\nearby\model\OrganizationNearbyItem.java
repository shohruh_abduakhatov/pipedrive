package com.pipedrive.nearby.model;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.BaseDataSource;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.model.Organization;
import java.util.List;

public final class OrganizationNearbyItem extends DealRelatedNearbyItem<Organization> {
    public OrganizationNearbyItem(@NonNull Double latitude, @NonNull Double longitude, @NonNull String address, @NonNull Long singleItemSqlId) {
        super(latitude, longitude, address, singleItemSqlId);
    }

    public OrganizationNearbyItem(@NonNull Double latitude, @NonNull Double longitude, @NonNull String address, @NonNull List<Long> multipleItemSqlIds) {
        super(latitude, longitude, address, (List) multipleItemSqlIds);
    }

    @NonNull
    protected String getColumnForOpenDealsCount() {
        return PipeSQLiteHelper.COLUMN_DEALS_ORGANIZATION_SQL_ID;
    }

    @NonNull
    protected final String getActivitiesColumnForNearbyItem() {
        return PipeSQLiteHelper.COLUMN_ACTIVITIES_ORGANIZATION_ID_SQL;
    }

    @NonNull
    protected final BaseDataSource<Organization> getDataSource(@NonNull Session session) {
        return new OrganizationsDataSource(session.getDatabase());
    }
}
