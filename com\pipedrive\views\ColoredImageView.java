package com.pipedrive.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.pipedrive.R;
import com.pipedrive.util.ImageUtil;

public class ColoredImageView extends ImageView {
    @ColorInt
    protected int mBackgroundTintColor;
    private boolean mBackgroundTinted;
    private boolean mDrawableTinted;
    @ColorInt
    protected int mTintColor;

    public ColoredImageView(Context context) {
        this(context, null);
    }

    public ColoredImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ColoredImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mTintColor = ContextCompat.getColor(getContext(), R.color.icon_tint);
        this.mBackgroundTintColor = ContextCompat.getColor(getContext(), R.color.transparent);
        this.mDrawableTinted = false;
        this.mBackgroundTinted = false;
        setupAttributes(context, attrs);
    }

    private void setupAttributes(@NonNull Context context, AttributeSet attrs) {
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ColoredImageView, 0, 0);
        try {
            this.mTintColor = typedArray.getColor(0, this.mTintColor);
            this.mBackgroundTintColor = typedArray.getColor(1, this.mBackgroundTintColor);
        } finally {
            typedArray.recycle();
        }
    }

    public void setImageResource(int resId) {
        super.setImageResource(resId);
        this.mDrawableTinted = false;
        tintDrawableIfNotTinted();
    }

    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        this.mDrawableTinted = false;
        tintDrawableIfNotTinted();
    }

    public void requestLayout() {
        applyTintIfNotTinted();
        super.requestLayout();
    }

    public void setTintColor(@ColorRes int colorResId) {
        this.mTintColor = ContextCompat.getColor(getContext(), colorResId);
        this.mDrawableTinted = false;
        tintDrawableIfNotTinted();
    }

    public void setBackgroundTintColor(@ColorRes int colorResId) {
        this.mBackgroundTintColor = ContextCompat.getColor(getContext(), colorResId);
        this.mBackgroundTinted = false;
        tintBackgroundIfNotTinted();
    }

    private void applyTintIfNotTinted() {
        tintDrawableIfNotTinted();
        tintBackgroundIfNotTinted();
    }

    private void tintBackgroundIfNotTinted() {
        boolean backgroundExists = getBackground() != null;
        if (!this.mBackgroundTinted && backgroundExists) {
            Drawable tintedBackground = ImageUtil.getTintedDrawable(getBackground(), this.mBackgroundTintColor);
            if (VERSION.SDK_INT >= 16) {
                super.setBackground(tintedBackground);
            } else {
                super.setBackgroundDrawable(tintedBackground);
            }
            this.mBackgroundTinted = true;
        }
    }

    private void tintDrawableIfNotTinted() {
        boolean drawableExists = getDrawable() != null;
        if (!this.mDrawableTinted && drawableExists) {
            super.setImageDrawable(ImageUtil.getTintedDrawable(getDrawable(), this.mTintColor));
            this.mDrawableTinted = true;
        }
    }
}
