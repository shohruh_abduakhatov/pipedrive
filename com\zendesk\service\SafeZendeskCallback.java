package com.zendesk.service;

import com.zendesk.logger.Logger;

public class SafeZendeskCallback<T> extends ZendeskCallback<T> {
    private static final String LOG_TAG = "SafeZendeskCallback";
    private final ZendeskCallback<T> callback;
    private boolean cancelled = false;

    public SafeZendeskCallback(ZendeskCallback<T> callback) {
        this.callback = callback;
    }

    public void onSuccess(T result) {
        if (this.cancelled || this.callback == null) {
            Logger.w(LOG_TAG, "Operation was a success but callback is null or was cancelled", new Object[0]);
        } else {
            this.callback.onSuccess(result);
        }
    }

    public void onError(ErrorResponse error) {
        if (this.cancelled || this.callback == null) {
            Logger.e(LOG_TAG, error);
        } else {
            this.callback.onError(error);
        }
    }

    public void cancel() {
        this.cancelled = true;
    }

    public static <T> SafeZendeskCallback<T> from(ZendeskCallback<T> callback) {
        return new SafeZendeskCallback(callback);
    }
}
