package com.pipedrive.navigator;

import android.support.annotation.MainThread;
import android.support.annotation.Nullable;
import com.pipedrive.navigator.buttons.NavigatorMenuItem;
import java.util.List;

@MainThread
public interface Navigable {
    void createOrUpdateDiscardedAsRequiredFieldsAreNotSet();

    @Nullable
    List<? extends NavigatorMenuItem> getAdditionalButtons();

    boolean isAllRequiredFieldsSet();

    boolean isChangesMadeToFieldsAfterInitialLoad();

    void onCancel();

    void onCreateOrUpdate();
}
