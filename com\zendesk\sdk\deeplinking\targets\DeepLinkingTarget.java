package com.zendesk.sdk.deeplinking.targets;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.TaskStackBuilder;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingBroadcastReceiver;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.model.access.Identity;
import com.zendesk.sdk.model.access.JwtIdentity;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.util.StringUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class DeepLinkingTarget<E extends TargetConfiguration> {
    private static final String EXTRA_APP_ID = "extra_appid";
    private static final String EXTRA_AUTH_BUNDLE = "extra_auth_bundle";
    private static final String EXTRA_BACK_STACK_ACTIVITIES = "extra_follow_up_activities";
    private static final String EXTRA_CLIENT_ID = "extra_client_id";
    private static final String EXTRA_DEEP_LINK_TYPE = "extra_deeplink";
    private static final String EXTRA_EXTRA_BUNDLE = "extra_extra_bundle";
    private static final String EXTRA_FALLBACK_ACTIVITY = "extra_fallback_activity";
    private static final String EXTRA_LOGGABLE = "extra_loggable";
    private static final String EXTRA_URL = "extra_url";
    private static final String EXTRA_USER_TOKEN = "extra_user_token";
    private static final String LOG_TAG = DeepLinkingTarget.class.getSimpleName();

    @Nullable
    abstract E extractConfiguration(@NonNull Bundle bundle, TargetConfiguration targetConfiguration);

    @NonNull
    abstract Bundle getExtra(E e);

    abstract void openTargetActivity(Context context, @NonNull E e);

    @Nullable
    public Intent getIntent(Context context, E configuration) {
        if (!ZendeskConfig.INSTANCE.isInitialized()) {
            return null;
        }
        return getIntent(context, configuration, ZendeskConfig.INSTANCE.getZendeskUrl(), ZendeskConfig.INSTANCE.getApplicationId(), ZendeskConfig.INSTANCE.getOauthClientId());
    }

    @Nullable
    public Intent getIntent(Context context, E configuration, String zendeskUrl, String applicationId, String oauthClientId) {
        Intent intent = new Intent(context, ZendeskDeepLinkingBroadcastReceiver.class);
        String userToken = getUserToken();
        if (StringUtils.hasLength(userToken)) {
            intent.putExtra(EXTRA_LOGGABLE, Logger.isLoggable());
            intent.putExtra(EXTRA_AUTH_BUNDLE, getAuthBundle(zendeskUrl, applicationId, oauthClientId, userToken));
            intent.putExtra(EXTRA_DEEP_LINK_TYPE, configuration.getDeepLinkType());
            if (configuration.getFallbackActivity() != null) {
                checkIntent(configuration.getFallbackActivity());
                intent.putExtra(EXTRA_FALLBACK_ACTIVITY, configuration.getFallbackActivity());
            }
            if (configuration.getBackStackActivities() != null) {
                Iterator it = configuration.getBackStackActivities().iterator();
                while (it.hasNext()) {
                    checkIntent((Intent) it.next());
                }
                intent.putParcelableArrayListExtra(EXTRA_BACK_STACK_ACTIVITIES, configuration.getBackStackActivities());
            }
            intent.putExtra(EXTRA_EXTRA_BUNDLE, getExtra(configuration));
            return intent;
        }
        Logger.e(LOG_TAG, "No user configuration found", new Object[0]);
        return null;
    }

    public boolean execute(Context context, Intent intent) {
        Logger.setLoggable(intent.getBooleanExtra(EXTRA_LOGGABLE, false));
        Intent fallbackActivity = null;
        if (intent.hasExtra(EXTRA_FALLBACK_ACTIVITY) && (intent.getParcelableExtra(EXTRA_FALLBACK_ACTIVITY) instanceof Intent)) {
            fallbackActivity = (Intent) intent.getParcelableExtra(EXTRA_FALLBACK_ACTIVITY);
        }
        if (!applyAuthBundle(context, intent.getBundleExtra(EXTRA_AUTH_BUNDLE))) {
            Logger.e(LOG_TAG, "No valid auth bundle found", new Object[0]);
            fireFallBackActivity(context, fallbackActivity);
            return false;
        } else if (intent.hasExtra(EXTRA_DEEP_LINK_TYPE) && (intent.getSerializableExtra(EXTRA_DEEP_LINK_TYPE) instanceof DeepLinkType)) {
            DeepLinkType deepLinkTypeType = (DeepLinkType) intent.getSerializableExtra(EXTRA_DEEP_LINK_TYPE);
            ArrayList<Intent> backStack = new ArrayList();
            if (intent.hasExtra(EXTRA_BACK_STACK_ACTIVITIES)) {
                backStack = intent.getParcelableArrayListExtra(EXTRA_BACK_STACK_ACTIVITIES);
                Iterator it = backStack.iterator();
                while (it.hasNext()) {
                    checkIntent((Intent) it.next());
                }
            }
            if (!intent.hasExtra(EXTRA_EXTRA_BUNDLE) || intent.getBundleExtra(EXTRA_EXTRA_BUNDLE) == null) {
                Logger.e(LOG_TAG, "No activity configuration found", new Object[0]);
                fireFallBackActivity(context, fallbackActivity);
                return false;
            }
            E config = extractConfiguration(intent.getBundleExtra(EXTRA_EXTRA_BUNDLE), new TargetConfiguration(deepLinkTypeType, backStack, fallbackActivity));
            if (config == null) {
                fireFallBackActivity(context, fallbackActivity);
                return false;
            }
            openTargetActivity(context, config);
            return true;
        } else {
            Logger.e(LOG_TAG, "No valid deep link type found", new Object[0]);
            fireFallBackActivity(context, fallbackActivity);
            return false;
        }
    }

    protected static TaskStackBuilder getTaskStackBuilder(Context context, List<Intent> backStackActivities) {
        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
        for (Intent i : backStackActivities) {
            taskStackBuilder.addNextIntentWithParentStack(i);
        }
        return taskStackBuilder;
    }

    private Bundle getAuthBundle(String url, String appId, String oAuth, String userToken) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_URL, url);
        bundle.putString(EXTRA_APP_ID, appId);
        bundle.putString(EXTRA_CLIENT_ID, oAuth);
        bundle.putString(EXTRA_USER_TOKEN, userToken);
        return bundle;
    }

    private boolean applyAuthBundle(Context context, @Nullable Bundle bundle) {
        if (bundle != null) {
            if (hasBundleValidStrings(bundle, EXTRA_CLIENT_ID, EXTRA_APP_ID, EXTRA_URL, EXTRA_USER_TOKEN)) {
                ZendeskConfig.INSTANCE.init(context, bundle.getString(EXTRA_URL), bundle.getString(EXTRA_APP_ID), bundle.getString(EXTRA_CLIENT_ID));
                String userToken = bundle.getString(EXTRA_USER_TOKEN);
                if (userToken == null || userToken.equals(getUserToken())) {
                    return true;
                }
                Logger.e(LOG_TAG, "User configuration has changed", new Object[0]);
                return false;
            }
        }
        Logger.e(LOG_TAG, "Auth bundle content is invalid", new Object[0]);
        return false;
    }

    private boolean hasBundleValidStrings(Bundle bundle, String... keys) {
        boolean isValid = true;
        for (String key : keys) {
            if (!bundle.containsKey(key) || !StringUtils.hasLength(bundle.getString(key))) {
                isValid = false;
            }
        }
        return isValid;
    }

    private String getUserToken() {
        Identity identity = ZendeskConfig.INSTANCE.storage().identityStorage().getIdentity();
        if (identity != null && (identity instanceof AnonymousIdentity)) {
            return ((AnonymousIdentity) identity).getSdkGuid();
        }
        if (identity == null || !(identity instanceof JwtIdentity)) {
            return null;
        }
        return ((JwtIdentity) identity).getJwtUserIdentifier();
    }

    private void fireFallBackActivity(Context context, Intent intent) {
        if (intent != null) {
            checkIntent(intent);
            intent.addFlags(268435456);
            context.startActivity(intent);
        }
    }

    private void checkIntent(Intent intent) {
        if (intent.getComponent() == null) {
            throw new IllegalArgumentException("Illegal Intent provided. Make sure that the provided Intent has a ComponentName. To achieve this you should for example this constructor: ``Intent(Context packageContext, Class<?> cls)``");
        }
    }

    public static DeepLinkType getDeepLinkType(Intent intent) {
        if (intent.hasExtra(EXTRA_DEEP_LINK_TYPE) && (intent.getSerializableExtra(EXTRA_DEEP_LINK_TYPE) instanceof DeepLinkType)) {
            return (DeepLinkType) intent.getSerializableExtra(EXTRA_DEEP_LINK_TYPE);
        }
        return DeepLinkType.Unknown;
    }
}
