package com.pipedrive.views.common;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import com.pipedrive.R;
import com.pipedrive.views.Spinner;
import com.pipedrive.views.Spinner.LabelBuilder;
import com.pipedrive.views.Spinner.OnItemSelectedListener;
import java.util.List;

public abstract class SpinnerWithUnderlineAndLabel extends LayoutWithUnderlineAndLabel<Spinner> {
    public SpinnerWithUnderlineAndLabel(Context context) {
        this(context, null);
    }

    public SpinnerWithUnderlineAndLabel(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SpinnerWithUnderlineAndLabel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @NonNull
    protected Spinner createMainView(@NonNull Context context, AttributeSet attrs, int defStyle) {
        return new Spinner(context);
    }

    protected void setup(@NonNull List data, @NonNull LabelBuilder labelBuilder, @NonNull OnItemSelectedListener onItemSelectedListener) {
        ((Spinner) this.mMainView).loadAndInit(data, labelBuilder, onItemSelectedListener, R.layout.row_simple_spinner_selected_item_new, R.layout.row_simple_spinner_dropdown_item);
    }
}
