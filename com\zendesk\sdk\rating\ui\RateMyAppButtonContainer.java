package com.zendesk.sdk.rating.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.sdk.rating.RateMyAppButton;
import com.zendesk.sdk.util.UiUtils;
import com.zendesk.util.CollectionUtils;
import com.zendesk.util.StringUtils;
import java.io.Serializable;
import java.util.List;

@SuppressLint({"ViewConstructor"})
public class RateMyAppButtonContainer extends LinearLayout {
    private static final int FALLBACK_DIVIDER_HEIGHT_DIP = 1;
    private static final int FALLBACK_TOP_PADDING_DIP = 16;
    private static final String LOG_TAG = RateMyAppButtonContainer.class.getSimpleName();
    private static final LayoutParams TEXTVIEW_LAYOUT_PARAMS = new LayoutParams(-1, -2);
    private final List<RateMyAppButton> mButtons;
    private final Context mContext;
    private DismissableListener mDismissableListener;
    private RateMyAppSelectionListener mRateMyAppSelectionListener;

    interface DismissableListener {
        void dismissDialog();
    }

    public interface RateMyAppSelectionListener extends Serializable {
        void selectionMade(int i);
    }

    static {
        TEXTVIEW_LAYOUT_PARAMS.weight = 1.0f;
    }

    public RateMyAppButtonContainer(Context context, List<RateMyAppButton> buttons) {
        super(context);
        if (buttons == null || buttons.isEmpty()) {
            Logger.w(LOG_TAG, "No rate my app buttons supplied, dialog will be empty", new Object[0]);
        }
        this.mButtons = CollectionUtils.ensureEmpty(buttons);
        this.mContext = getContext();
        initialise();
    }

    public void setDismissableListener(DismissableListener dismissableListener) {
        this.mDismissableListener = dismissableListener;
    }

    public void setRateMyAppSelectionListener(RateMyAppSelectionListener selectionListener) {
        this.mRateMyAppSelectionListener = selectionListener;
    }

    private void initialise() {
        setOrientation(1);
        setLayoutParams(new LayoutParams(-1, -1));
        int horizontalPadding = Math.round(getResources().getDimension(R.dimen.activity_horizontal_margin));
        int topPadding = UiUtils.themeAttributeToPixels(R.attr.RateMyAppPaddingTop, this.mContext, 1, 16.0f);
        int bottomPadding = UiUtils.themeAttributeToPixels(R.attr.RateMyAppPaddingBottom, this.mContext, 1, 16.0f);
        int dividerHeight = UiUtils.themeAttributeToPixels(R.attr.RateMyAppDividerHeight, this.mContext, 1, 1.0f);
        setWeightSum((float) (this.mButtons.size() + 1));
        TextView title = new TextView(getContext(), null, R.attr.RateMyAppTitleStyle);
        title.setLayoutParams(TEXTVIEW_LAYOUT_PARAMS);
        title.setText(R.string.rate_my_app_dialog_title_label);
        title.setPadding(horizontalPadding, topPadding, horizontalPadding, bottomPadding);
        addView(title);
        addDividerView(dividerHeight);
        for (RateMyAppButton button : this.mButtons) {
            if (addActionButton(horizontalPadding, topPadding, bottomPadding, button)) {
                addDividerView(dividerHeight);
            }
        }
    }

    private boolean addActionButton(int horizontalPadding, int topPadding, int bottomPadding, final RateMyAppButton button) {
        if (button == null || !StringUtils.hasLength(button.getLabel()) || button.getOnClickListener() == null) {
            Logger.w(LOG_TAG, "Can't add a null button or a button with no label or listener", new Object[0]);
            return false;
        }
        final TextView tv = new TextView(getContext(), null, button.getStyleAttributeId());
        tv.setLayoutParams(TEXTVIEW_LAYOUT_PARAMS);
        tv.setText(button.getLabel());
        tv.setPadding(horizontalPadding, topPadding, horizontalPadding, bottomPadding);
        tv.setId(button.getId());
        tv.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (button.getOnClickListener() != null) {
                    button.getOnClickListener().onClick(tv);
                }
                if (RateMyAppButtonContainer.this.mDismissableListener != null && button.shouldDismissDialog()) {
                    RateMyAppButtonContainer.this.mDismissableListener.dismissDialog();
                }
                if (RateMyAppButtonContainer.this.mRateMyAppSelectionListener != null && button.getId() != -1) {
                    RateMyAppButtonContainer.this.mRateMyAppSelectionListener.selectionMade(button.getId());
                }
            }
        });
        addView(tv);
        return true;
    }

    private void addDividerView(int dividerHeight) {
        View divider = new View(getContext(), null, R.attr.RateMyAppDividerStyle);
        divider.setLayoutParams(new LayoutParams(-1, dividerHeight));
        addView(divider);
    }
}
