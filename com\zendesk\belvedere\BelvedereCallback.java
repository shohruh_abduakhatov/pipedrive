package com.zendesk.belvedere;

import android.os.Handler;
import android.os.Looper;

public abstract class BelvedereCallback<E> {
    private boolean canceled = false;

    public abstract void success(E e);

    public void cancel() {
        this.canceled = true;
    }

    void internalSuccess(final E result) {
        if (!this.canceled) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    BelvedereCallback.this.success(result);
                }
            });
        }
    }
}
