package com.zendesk.sdk.storage;

public interface StorageStore {
    HelpCenterSessionCache helpCenterSessionCache();

    IdentityStorage identityStorage();

    PushRegistrationResponseStorage pushStorage();

    RequestStorage requestStorage();

    SdkSettingsStorage sdkSettingsStorage();

    SdkStorage sdkStorage();
}
