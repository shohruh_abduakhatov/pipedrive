package com.zendesk.sdk.storage;

import android.support.annotation.NonNull;
import com.zendesk.sdk.model.helpcenter.LastSearch;

public interface HelpCenterSessionCache {
    @NonNull
    LastSearch getLastSearch();

    boolean isUniqueSearchResultClick();

    void setLastSearch(String str, int i);

    void unsetUniqueSearchResultClick();
}
