package com.pipedrive.views.edit.products;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.widget.TextView;
import com.pipedrive.R;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.model.products.ProductVariation;
import com.pipedrive.views.common.ClickableTextViewWithUnderlineAndLabel;

public class ProductVariationView extends ClickableTextViewWithUnderlineAndLabel {
    private ProductVariation mProductVariation;

    public ProductVariationView(Context context) {
        this(context, null);
    }

    public ProductVariationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProductVariationView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setup(@NonNull DealProduct dealProduct) {
        this.mProductVariation = dealProduct.getProductVariation();
        setupTextAndLabel();
    }

    private void setupTextAndLabel() {
        if (this.mProductVariation != null) {
            showLabel();
            ((TextView) this.mMainView).setText(this.mProductVariation.getName());
            return;
        }
        ((TextView) this.mMainView).setText("");
        hideLabel();
        ((TextView) this.mMainView).setHint(getLabelTextResourceId());
    }

    public ProductVariation getProductVariation() {
        return this.mProductVariation;
    }

    @StringRes
    protected int getLabelTextResourceId() {
        return R.string.variation;
    }
}
