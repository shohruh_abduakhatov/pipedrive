package com.pipedrive.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.pipedrive.R;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.application.SessionManager;
import com.pipedrive.pipeline.InvalidatePipelinesTask;
import com.pipedrive.pipeline.PipelineActivity;
import com.pipedrive.sync.SyncManager;
import com.pipedrive.tasks.SessionLogoutTask;
import com.pipedrive.tasks.activities.ActivitiesListTask;
import com.pipedrive.util.networking.ApiUrlBuilder;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u0000 \u000b2\u00020\u0001:\u0002\u000b\fB\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u0012\u0010\u0007\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016J\u0010\u0010\b\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\nH\u0002¨\u0006\r"}, d2 = {"Lcom/pipedrive/fcm/PipedriveFirebaseMessagingService;", "Lcom/google/firebase/messaging/FirebaseMessagingService;", "()V", "handleLogoutMessage", "", "remoteMessage", "Lcom/google/firebase/messaging/RemoteMessage;", "onMessageReceived", "sendNotification", "message", "", "Companion", "Keys", "pipedrive_prodRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: PipedriveFirebaseMessagingService.kt */
public final class PipedriveFirebaseMessagingService extends FirebaseMessagingService {
    public static final Companion Companion = new Companion();
    private static final String TAG = TAG;

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lcom/pipedrive/fcm/PipedriveFirebaseMessagingService$Companion;", "", "()V", "TAG", "", "getTAG", "()Ljava/lang/String;", "pipedrive_prodRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: PipedriveFirebaseMessagingService.kt */
    public static final class Companion {
        private Companion() {
        }

        private final String getTAG() {
            return PipedriveFirebaseMessagingService.TAG;
        }
    }

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/pipedrive/fcm/PipedriveFirebaseMessagingService$Keys;", "", "()V", "companyId", "", "getCompanyId$pipedrive_prodRelease", "()Ljava/lang/String;", "userId", "getUserId$pipedrive_prodRelease", "pipedrive_prodRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: PipedriveFirebaseMessagingService.kt */
    public static final class Keys {
        public static final Keys INSTANCE = null;
        @NotNull
        private static final String companyId = "company_id";
        @NotNull
        private static final String userId = "user_id";

        static {
            Keys keys = new Keys();
        }

        private Keys() {
            INSTANCE = this;
            userId = "user_id";
            companyId = companyId;
        }

        @NotNull
        public final String getUserId$pipedrive_prodRelease() {
            return userId;
        }

        @NotNull
        public final String getCompanyId$pipedrive_prodRelease() {
            return companyId;
        }
    }

    public void onMessageReceived(@Nullable RemoteMessage remoteMessage) {
        String access$getTAG$p = Companion.getTAG();
        StringBuilder append = new StringBuilder().append("From: ");
        if (remoteMessage == null) {
            Intrinsics.throwNpe();
        }
        Log.d(access$getTAG$p, append.append(remoteMessage.getFrom()).toString());
        if ((!remoteMessage.getData().isEmpty() ? 1 : null) != null) {
            Object obj;
            Log.d(Companion.getTAG(), "Message data payload: " + remoteMessage.getData());
            String logout = (String) remoteMessage.getData().get("logout");
            if (logout != null) {
                if (logout.length() == 0) {
                    obj = 1;
                } else {
                    obj = null;
                }
                if (obj == null && Intrinsics.areEqual("1", logout)) {
                    handleLogoutMessage(remoteMessage);
                    return;
                }
            }
            String recents = (String) remoteMessage.getData().get(ApiUrlBuilder.ENDPOINT_RECENTS);
            if (recents != null) {
                if (recents.length() == 0) {
                    obj = 1;
                } else {
                    obj = null;
                }
                if (obj == null && Intrinsics.areEqual("1", recents)) {
                    Session activeSession = PipedriveApp.getActiveSession();
                    SyncManager instance = SyncManager.getInstance();
                    if (activeSession == null) {
                        Intrinsics.throwNpe();
                    }
                    instance.syncRequestedByPushNotification(activeSession);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(InvalidatePipelinesTask.ACTION_INVALIDATE_PIPELINE));
                    LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(ActivitiesListTask.ACTION_ACTIVITIES_DOWNLOADED));
                }
            }
        }
        if (remoteMessage.getNotification() != null) {
            Log.d(Companion.getTAG(), "Message Notification Body: " + remoteMessage.getNotification().getBody());
            access$getTAG$p = remoteMessage.getNotification().getBody();
            Intrinsics.checkExpressionValueIsNotNull(access$getTAG$p, "remoteMessage.notification.body");
            sendNotification(access$getTAG$p);
        }
    }

    private final void handleLogoutMessage(RemoteMessage remoteMessage) {
        SessionManager sessionManager = PipedriveApp.getSessionManager();
        if (sessionManager.hasActiveSession()) {
            Long companyId;
            String str = (String) remoteMessage.getData().get(Keys.INSTANCE.getCompanyId$pipedrive_prodRelease());
            if (str != null) {
                companyId = Long.valueOf(Long.parseLong(str));
            } else {
                companyId = null;
            }
            str = (String) remoteMessage.getData().get(Keys.INSTANCE.getUserId$pipedrive_prodRelease());
            Long userId;
            if (str != null) {
                userId = Long.valueOf(Long.parseLong(str));
            } else {
                userId = null;
            }
            if (companyId != null && userId != null) {
                Session sessionToLogOut = sessionManager.getSessionForCompanyPipedriveId(companyId);
                if (sessionToLogOut != null) {
                    SessionLogoutTask.startIfNotRunning(sessionToLogOut);
                }
            }
        }
    }

    private final void sendNotification(String message) {
        Intent intent = new Intent(this, PipelineActivity.class);
        intent.addFlags(67108864);
        Builder notificationBuilder = new Builder(this).setSmallIcon(R.drawable.ic_notification).setContentTitle("FCM Message").setContentText(message).setAutoCancel(true).setSound(RingtoneManager.getDefaultUri(2)).setContentIntent(PendingIntent.getActivity(this, 0, intent, 1073741824));
        NotificationManager notificationManager = getSystemService("notification");
        if (notificationManager == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.app.NotificationManager");
        }
        notificationManager.notify(0, notificationBuilder.build());
    }
}
