package com.pipedrive.util.networking.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pipedrive.model.pagination.Pagination;
import java.util.List;

public class AdditionalData {
    @SerializedName("matches_filters")
    @Expose
    private List<Long> mMatchedFilters;
    @SerializedName("pagination")
    @Expose
    private Pagination mPagination;

    public List<Long> getMatchedFilters() {
        return this.mMatchedFilters;
    }

    public void setMatchedFilters(List<Long> matchedFilters) {
        this.mMatchedFilters = matchedFilters;
    }

    public Pagination getPagination() {
        return this.mPagination;
    }

    public void setPagination(Pagination pagination) {
        this.mPagination = pagination;
    }
}
