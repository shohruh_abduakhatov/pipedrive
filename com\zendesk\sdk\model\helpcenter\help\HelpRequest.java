package com.zendesk.sdk.model.helpcenter.help;

import com.zendesk.util.StringUtils;
import java.util.List;

public class HelpRequest {
    private static final int DEFAULT_ARTICLES_PER_SECTION = 5;
    private static final String INCLUDE_ALL = "categories,sections";
    private static final String INCLUDE_CATEGORIES = "categories";
    private static final String INCLUDE_SECTIONS = "sections";
    private static final String LOG_TAG = "HelpRequest";
    private int articlesPerPageLimit;
    private String categoryIds;
    private String includes;
    private String[] labelNames;
    private String sectionIds;

    public static class Builder {
        private int articlesPerSectionLimit = 5;
        private String categoryIds;
        private String includes;
        private String[] labelNames;
        private String sectionIds;

        public Builder withCategoryIds(List<Long> ids) {
            this.categoryIds = StringUtils.toCsvStringNumber((List) ids);
            return this;
        }

        public Builder withSectionIds(List<Long> ids) {
            this.sectionIds = StringUtils.toCsvStringNumber((List) ids);
            return this;
        }

        public Builder includeCategories() {
            if (StringUtils.isEmpty(this.includes)) {
                this.includes = HelpRequest.INCLUDE_CATEGORIES;
            } else if (this.includes.equals(HelpRequest.INCLUDE_SECTIONS)) {
                this.includes = HelpRequest.INCLUDE_ALL;
            }
            return this;
        }

        public Builder includeSections() {
            if (StringUtils.isEmpty(this.includes)) {
                this.includes = HelpRequest.INCLUDE_SECTIONS;
            } else if (this.includes.equals(HelpRequest.INCLUDE_CATEGORIES)) {
                this.includes = HelpRequest.INCLUDE_ALL;
            }
            return this;
        }

        public Builder withArticlesPerSectionLimit(int articlesPerSection) {
            this.articlesPerSectionLimit = articlesPerSection;
            return this;
        }

        public Builder withLabelNames(String... labelNames) {
            this.labelNames = labelNames;
            return this;
        }

        public HelpRequest build() {
            return new HelpRequest();
        }
    }

    private HelpRequest(Builder builder) {
        this.categoryIds = builder.categoryIds;
        this.sectionIds = builder.sectionIds;
        this.includes = builder.includes;
        this.articlesPerPageLimit = builder.articlesPerSectionLimit;
        this.labelNames = builder.labelNames;
    }

    public String getCategoryIds() {
        return this.categoryIds;
    }

    public String getSectionIds() {
        return this.sectionIds;
    }

    public String getIncludes() {
        return this.includes;
    }

    public int getArticlesPerPageLimit() {
        return this.articlesPerPageLimit;
    }

    public String[] getLabelNames() {
        return this.labelNames;
    }
}
