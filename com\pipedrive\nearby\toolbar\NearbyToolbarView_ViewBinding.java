package com.pipedrive.nearby.toolbar;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class NearbyToolbarView_ViewBinding implements Unbinder {
    private NearbyToolbarView target;

    @UiThread
    public NearbyToolbarView_ViewBinding(NearbyToolbarView target) {
        this(target, target);
    }

    @UiThread
    public NearbyToolbarView_ViewBinding(NearbyToolbarView target, View source) {
        this.target = target;
        target.tabLayout = (TabLayout) Utils.findRequiredViewAsType(source, R.id.tabLayout, "field 'tabLayout'", TabLayout.class);
        target.toolbar = (Toolbar) Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    }

    @CallSuper
    public void unbind() {
        NearbyToolbarView target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.tabLayout = null;
        target.toolbar = null;
    }
}
