package com.pipedrive.model.contact;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.newrelic.agent.android.instrumentation.JSONArrayInstrumentation;
import com.newrelic.agent.android.instrumentation.JSONObjectInstrumentation;
import com.newrelic.agent.android.util.SafeJsonPrimitive;
import com.pipedrive.util.StringUtils;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CommunicationMedium {
    private static final String KEY_LABEL = "label";
    private static final String KEY_PRIMARY = "primary";
    private static final String KEY_VALUE = "value";
    private boolean mIsPrimary = false;
    @Nullable
    private String mLabel;
    @Nullable
    private String mValue;

    CommunicationMedium(@Nullable JSONObject jsonObject) {
        if (jsonObject != null) {
            this.mValue = jsonObject.optString("value");
            this.mLabel = jsonObject.optString("label");
            this.mIsPrimary = jsonObject.optBoolean(KEY_PRIMARY);
        }
    }

    CommunicationMedium(@Nullable String value, @Nullable String label, boolean isPrimary) {
        this.mValue = value;
        this.mLabel = label;
        this.mIsPrimary = isPrimary;
    }

    @NonNull
    public static <CM extends CommunicationMedium> String toJsonArrayString(@NonNull List<CM> items) {
        JSONArray jsonArray = new JSONArray();
        for (CM item : items) {
            if (!StringUtils.isTrimmedAndEmpty(item.getValue())) {
                jsonArray.put(item.asJsonObject());
            }
        }
        return !(jsonArray instanceof JSONArray) ? jsonArray.toString() : JSONArrayInstrumentation.toString(jsonArray);
    }

    @Nullable
    public final String getValue() {
        return this.mValue;
    }

    @NonNull
    JSONObject asJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("value", this.mValue).put("label", this.mLabel).put(KEY_PRIMARY, this.mIsPrimary);
        } catch (JSONException e) {
        }
        return jsonObject;
    }

    @Nullable
    public final String getLabel() {
        return this.mLabel;
    }

    public final boolean isPrimary() {
        return this.mIsPrimary;
    }

    public final void setLabel(@Nullable String label) {
        this.mLabel = label;
    }

    public final void setValue(@Nullable String value) {
        this.mValue = value;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(Object o) {
        boolean z = true;
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CommunicationMedium that = (CommunicationMedium) o;
        if (this.mIsPrimary != that.mIsPrimary) {
            return false;
        }
        if (this.mValue != null) {
            if (!this.mValue.equals(that.mValue)) {
                return false;
            }
        } else if (that.mValue != null) {
            return false;
        }
        if (this.mLabel == null) {
            if (that.mLabel == null) {
                return z;
            }
        }
        z = false;
        return z;
    }

    public int hashCode() {
        int result;
        int hashCode;
        int i = 0;
        if (this.mValue != null) {
            result = this.mValue.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.mLabel != null) {
            hashCode = this.mLabel.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (i2 + hashCode) * 31;
        if (this.mIsPrimary) {
            i = 1;
        }
        return hashCode + i;
    }

    public String toString() {
        JSONObject asJsonObject = asJsonObject();
        return !(asJsonObject instanceof JSONObject) ? asJsonObject.toString() : JSONObjectInstrumentation.toString(asJsonObject);
    }

    @Nullable
    public static String getNormalizedString(@Nullable List<? extends CommunicationMedium> communicationMedia) {
        if (communicationMedia == null || communicationMedia.isEmpty()) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (CommunicationMedium medium : communicationMedia) {
            stringBuilder.append(medium.getValue()).append(SafeJsonPrimitive.NULL_CHAR);
        }
        return stringBuilder.toString();
    }
}
