package com.pipedrive.more;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindColor;
import butterknife.BindDimen;
import butterknife.ButterKnife;
import com.pipedrive.R;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;

public class MoreButton extends FrameLayout {
    @BindColor(2131755255)
    int backgroundColor;
    @BindDimen(2131493021)
    int defaultContentPadding;
    @NonNull
    private TextView subtitleTextView;
    @NonNull
    private SwitchCompat switchButton;
    @NonNull
    private TextView titleTextView;

    public MoreButton(Context context) {
        this(context, null);
    }

    public MoreButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MoreButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        ButterKnife.bind((View) this);
        init(context, attrs);
    }

    private void init(@NonNull Context context, @Nullable AttributeSet attrs) {
        inflate(context, R.layout.view_more_button, this);
        View textContainer = findViewWithTag("textContainer");
        ImageView iconImageView = (ImageView) findViewWithTag(SettingsJsonConstants.APP_ICON_KEY);
        View bottomSeparator = findViewWithTag("bottomSeparator");
        this.switchButton = (SwitchCompat) findViewWithTag("switchButton");
        this.titleTextView = (TextView) findViewWithTag("title");
        this.subtitleTextView = (TextView) findViewWithTag("subtitle");
        TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MoreButton, 0, 0);
        try {
            int iconResId = styledAttributes.getResourceId(0, 0);
            String title = styledAttributes.getString(1);
            String subtitle = styledAttributes.getString(2);
            boolean enableSwitch = styledAttributes.getBoolean(3, false);
            boolean addVerticalPadding = styledAttributes.getBoolean(4, false);
            boolean showBottomSeparator = styledAttributes.getBoolean(5, true);
            if (iconResId > 0) {
                iconImageView.setImageResource(iconResId);
            }
            iconImageView.setVisibility(iconResId > 0 ? 0 : 8);
            this.switchButton.setVisibility(enableSwitch ? 0 : 8);
            if (addVerticalPadding) {
                textContainer.setPadding(this.defaultContentPadding, this.defaultContentPadding, this.defaultContentPadding, this.defaultContentPadding);
            }
            bottomSeparator.setVisibility(showBottomSeparator ? 0 : 8);
            setTitle(title);
            setSubtitle(subtitle);
            setBackgroundColor(this.backgroundColor);
        } finally {
            styledAttributes.recycle();
        }
    }

    @UiThread
    public void setSubtitle(@Nullable String subtitle) {
        this.subtitleTextView.setText(subtitle);
        this.subtitleTextView.setVisibility(!TextUtils.isEmpty(subtitle) ? 0 : 8);
    }

    @UiThread
    public void setTitle(@Nullable String title) {
        this.titleTextView.setText(title);
        this.titleTextView.setVisibility(!TextUtils.isEmpty(title) ? 0 : 8);
    }

    @UiThread
    public void setChecked(boolean isChecked) {
        this.switchButton.setChecked(isChecked);
    }

    public void setOnCheckListenerChecked(@Nullable OnCheckedChangeListener listener) {
        this.switchButton.setOnCheckedChangeListener(listener);
    }
}
