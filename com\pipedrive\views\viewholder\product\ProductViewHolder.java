package com.pipedrive.views.viewholder.product;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.TextView;
import butterknife.BindView;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.model.Currency;
import com.pipedrive.model.products.Product;
import com.pipedrive.util.StringUtils;
import com.pipedrive.util.formatter.MonetaryFormatter;
import com.pipedrive.views.viewholder.ViewHolder;

public class ProductViewHolder implements ViewHolder {
    @BindView(2131820817)
    TextView priceView;
    @BindView(2131820650)
    TextView titleView;

    public int getLayoutResourceId() {
        return R.layout.row_product;
    }

    public void fill(@Nullable Product product, @Nullable Currency dealCurrency, @NonNull SearchConstraint searchConstraint, @NonNull Session session) {
        if (product != null && dealCurrency != null) {
            String formattedPrice = new MonetaryFormatter(session).formatPrice(product.getPriceForDealsCurrency(dealCurrency));
            this.titleView.setText(product.getName());
            this.priceView.setText(formattedPrice);
            StringUtils.highlightString(this.titleView, product.getName(), searchConstraint);
        }
    }
}
