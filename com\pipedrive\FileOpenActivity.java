package com.pipedrive;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.instrumentation.AsyncTaskInstrumentation;
import com.newrelic.agent.android.tracing.Trace;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.EmailAttachmentsDataSource;
import com.pipedrive.datasource.FlowFilesDataSource;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.model.BaseFile;
import com.pipedrive.model.FlowFile;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.util.flowfiles.FileHelper;
import com.pipedrive.util.networking.ConnectionUtil;
import java.io.File;
import java.util.concurrent.Executor;

public class FileOpenActivity extends LoadingModalActivity {
    private static final String EXTRA_ATTACHMENT_ID = "attachmentId";
    private static final String EXTRA_FILE_ID = "fileId";
    private static final String GDOC_FILE_TYPE_DOCUMENT = "document";
    private static final String GDOC_FILE_TYPE_FILE = "file";
    private static final String REMOTE_LOCATION_GOOGLEDOCS = "googledocs";
    private static final String URL_GOOGLE_COM = "http://www.google.com";
    private static final String URL_GOOGLE_DOCS = "https://docs.google.com/%s/d/%s";

    public static void startActivityForFlowFile(Context context, long fileId) {
        context.startActivity(new Intent(context, FileOpenActivity.class).putExtra(EXTRA_FILE_ID, fileId).setFlags(8388608));
    }

    public static void startActivityForEmailAttachment(Context context, long fileId) {
        context.startActivity(new Intent(context, FileOpenActivity.class).putExtra(EXTRA_ATTACHMENT_ID, fileId).setFlags(8388608));
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Session session = PipedriveApp.getActiveSession();
        if (session == null) {
            finish();
            return;
        }
        BaseFile baseFile;
        long fileId = getIntent().getLongExtra(EXTRA_FILE_ID, 0);
        long attachmentId = getIntent().getLongExtra(EXTRA_ATTACHMENT_ID, 0);
        if (fileId > 0) {
            baseFile = (BaseFile) new FlowFilesDataSource(session.getDatabase()).findBySqlId(fileId);
        } else if (attachmentId > 0) {
            baseFile = (BaseFile) new EmailAttachmentsDataSource(session.getDatabase()).findBySqlId(attachmentId);
        } else {
            finish();
            return;
        }
        if (!(baseFile == null || TextUtils.isEmpty(baseFile.getFileName()))) {
            if (TextUtils.isEmpty(baseFile.getUrl()) && (baseFile instanceof FlowFile)) {
            }
            File localFile = FileHelper.getLocalFile(baseFile);
            String[] filenameParts = baseFile.getFileName().split("\\.");
            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(filenameParts[filenameParts.length - 1]);
            Intent localFileIntent = new Intent("android.intent.action.VIEW").setDataAndType(Uri.fromFile(localFile), mimeType);
            if (baseFile instanceof FlowFile) {
                if (REMOTE_LOCATION_GOOGLEDOCS.equals(((FlowFile) baseFile).getRemoteLocation())) {
                    String gdocFileType = FileHelper.isGoogleDocsType((FlowFile) baseFile) ? GDOC_FILE_TYPE_DOCUMENT : "file";
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(String.format(URL_GOOGLE_DOCS, new Object[]{gdocFileType, ((FlowFile) baseFile).getRemoteId()})));
                    if (VERSION.SDK_INT > 14) {
                        intent.setSelector(new Intent("android.intent.action.VIEW", Uri.parse(URL_GOOGLE_COM)));
                    } else {
                        String errMessage = FileOpenActivity.class.getSimpleName() + ":onCreate:Couldn't add selector to intent";
                        LogJourno.reportEvent(errMessage);
                        Log.e(new Throwable(errMessage));
                    }
                    startFileOpenIntent(intent);
                    return;
                }
            }
            if (getPackageManager().queryIntentActivities(localFileIntent, 0).isEmpty()) {
                tryRemoteOpen(session, baseFile, mimeType);
                return;
            } else {
                tryLocalOpen(session, baseFile, mimeType);
                return;
            }
        }
        finish();
    }

    protected int getLoadingMessage() {
        return R.string.file_open_progress_message;
    }

    private void tryLocalOpen(final Session session, final BaseFile baseFile, final String mimeType) {
        AnonymousClass1 anonymousClass1 = new TraceFieldInterface() {
            public Trace _nr_trace;

            public void _nr_setTrace(Trace trace) {
                try {
                    this._nr_trace = trace;
                } catch (Exception e) {
                }
            }

            protected Intent doInBackground(Void... params) {
                File storageDirectory = FileHelper.getFilesStorageDirectory();
                File localFile = FileHelper.getLocalFile(baseFile);
                if (!(localFile.exists() && localFile.length() == baseFile.getFileSize().longValue())) {
                    if (localFile.exists()) {
                        localFile.delete();
                    }
                    if (!(ConnectionUtil.isConnected(FileOpenActivity.this.getBaseContext()) && storageDirectory.getFreeSpace() >= baseFile.getFileSize().longValue() && FileHelper.downloadFile(session, localFile, baseFile) && localFile.exists())) {
                        return null;
                    }
                }
                return new Intent("android.intent.action.VIEW").setDataAndType(Uri.fromFile(localFile), mimeType);
            }

            protected void onPostExecute(Intent intent) {
                if (intent == null) {
                    FileOpenActivity.this.tryRemoteOpen(session, baseFile, mimeType);
                } else {
                    FileOpenActivity.this.startFileOpenIntent(intent);
                }
            }
        };
        Executor executor = AsyncTask.THREAD_POOL_EXECUTOR;
        Void[] voidArr = new Void[0];
        if (anonymousClass1 instanceof AsyncTask) {
            AsyncTaskInstrumentation.executeOnExecutor(anonymousClass1, executor, voidArr);
        } else {
            anonymousClass1.executeOnExecutor(executor, voidArr);
        }
    }

    void tryRemoteOpen(final Session session, final BaseFile baseFile, final String mimeType) {
        AnonymousClass2 anonymousClass2 = new TraceFieldInterface() {
            public Trace _nr_trace;

            public void _nr_setTrace(Trace trace) {
                try {
                    this._nr_trace = trace;
                } catch (Exception e) {
                }
            }

            protected Intent doInBackground(Void... params) {
                String remoteFileUrl = FileHelper.resolveFileUrl(session, baseFile);
                if (TextUtils.isEmpty(remoteFileUrl)) {
                    return null;
                }
                return new Intent("android.intent.action.VIEW").setDataAndType(Uri.parse(remoteFileUrl), mimeType);
            }

            protected void onPostExecute(Intent intent) {
                if (intent == null) {
                    if (ConnectionUtil.isConnected(FileOpenActivity.this.getBaseContext())) {
                        ViewUtil.showErrorToast(FileOpenActivity.this.getBaseContext(), FileOpenActivity.this.getString(R.string.file_open_error, new Object[]{baseFile.getName()}));
                    } else {
                        ViewUtil.showErrorToast(FileOpenActivity.this.getBaseContext(), FileOpenActivity.this.getString(R.string.file_open_error_no_connection, new Object[]{baseFile.getName()}));
                    }
                } else if (FileOpenActivity.this.getPackageManager().queryIntentActivities(intent, 0).isEmpty()) {
                    ViewUtil.showErrorToast(FileOpenActivity.this.getBaseContext(), FileOpenActivity.this.getString(R.string.file_open_error_no_app, new Object[]{baseFile.getName()}));
                } else {
                    FileOpenActivity.this.startFileOpenIntent(intent);
                }
                FileOpenActivity.this.finish();
            }
        };
        Executor executor = AsyncTask.THREAD_POOL_EXECUTOR;
        Void[] voidArr = new Void[0];
        if (anonymousClass2 instanceof AsyncTask) {
            AsyncTaskInstrumentation.executeOnExecutor(anonymousClass2, executor, voidArr);
        } else {
            anonymousClass2.executeOnExecutor(executor, voidArr);
        }
    }

    void startFileOpenIntent(Intent intent) {
        Log.d(FileOpenActivity.class.getSimpleName(), "Opening the file with " + intent);
        startActivity(intent);
        finish();
    }
}
