package com.pipedrive.pipeline;

import android.database.Cursor;
import android.support.annotation.NonNull;
import com.pipedrive.application.Session;

interface PipelineView {
    void setDeals(int i, @NonNull StageData stageData);

    void setDealsFailedToDownload(int i);

    void setDealsLoading(int i, boolean z);

    void setFilters();

    void setFiltersLoading(boolean z);

    void setPipelines(Cursor cursor, int i);

    void setPipelinesFailedToDownload();

    void setStages(Session session, Cursor cursor);

    void setStagesFailedToDownload();
}
