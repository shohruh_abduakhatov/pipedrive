package com.pipedrive.changes;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.changes.Change.ChangeOperationType;
import com.pipedrive.datasource.FlowFilesDataSource;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.FlowFile;

public class FlowFileChanger extends Changer<FlowFile> {
    public FlowFileChanger(@NonNull Session session) {
        super(session, 100, null);
    }

    @Nullable
    Long getChangeMetaData(@NonNull FlowFile flowFile, @NonNull ChangeOperationType changeOperationType) {
        return Long.valueOf(flowFile.getSqlId());
    }

    @NonNull
    ChangeResult createChange(@NonNull FlowFile newFlowFile) {
        boolean thisIsNotANewFile = newFlowFile.isExisting() || newFlowFile.isStored();
        if (thisIsNotANewFile) {
            LogJourno.reportEvent(EVENT.ChangesChanger_createChangeRequestedWithExistingChange, FlowFileChanger.class.getSimpleName());
            Log.e(new Throwable("I can only handle new FlowFiles!"));
            return ChangeResult.FAILED;
        }
        createAndRelateFlowFileAssociations(newFlowFile);
        long newFlowFileSqlId = new FlowFilesDataSource(getSession().getDatabase()).createOrUpdate((BaseDatasourceEntity) newFlowFile);
        if (newFlowFileSqlId == -1) {
            return ChangeResult.FAILED;
        }
        newFlowFile.setSqlId(newFlowFileSqlId);
        return ChangeResult.SUCCESSFUL;
    }

    @NonNull
    ChangeResult updateChange(@NonNull FlowFile existingFlowFile) {
        throw new RuntimeException("Update is not implemented as it is not used.");
    }

    private void createAndRelateFlowFileAssociations(@NonNull FlowFile newFlowFile) {
        boolean createNewDealFromTheFlowFileAssociation;
        boolean doNotKeepRelationAsChangeFailed;
        boolean createNewPersonFromTheFlowFileAssociation;
        boolean createNewOrganizationFromTheFlowFileAssociation;
        if (newFlowFile.getDeal() == null || newFlowFile.getDeal().isStored()) {
            createNewDealFromTheFlowFileAssociation = false;
        } else {
            createNewDealFromTheFlowFileAssociation = true;
        }
        if (createNewDealFromTheFlowFileAssociation) {
            if (new DealChanger((Changer) this).create(newFlowFile.getDeal())) {
                doNotKeepRelationAsChangeFailed = false;
            } else {
                doNotKeepRelationAsChangeFailed = true;
            }
            if (doNotKeepRelationAsChangeFailed) {
                newFlowFile.setDeal(null);
            }
        }
        if (newFlowFile.getPerson() == null || newFlowFile.getPerson().isStored()) {
            createNewPersonFromTheFlowFileAssociation = false;
        } else {
            createNewPersonFromTheFlowFileAssociation = true;
        }
        if (createNewPersonFromTheFlowFileAssociation) {
            if (new PersonChanger((Changer) this).create(newFlowFile.getPerson())) {
                doNotKeepRelationAsChangeFailed = false;
            } else {
                doNotKeepRelationAsChangeFailed = true;
            }
            if (doNotKeepRelationAsChangeFailed) {
                newFlowFile.setPerson(null);
            }
        }
        if (newFlowFile.getOrganization() == null || newFlowFile.getOrganization().isStored()) {
            createNewOrganizationFromTheFlowFileAssociation = false;
        } else {
            createNewOrganizationFromTheFlowFileAssociation = true;
        }
        if (createNewOrganizationFromTheFlowFileAssociation) {
            if (new OrganizationChanger((Changer) this).create(newFlowFile.getOrganization())) {
                doNotKeepRelationAsChangeFailed = false;
            } else {
                doNotKeepRelationAsChangeFailed = true;
            }
            if (doNotKeepRelationAsChangeFailed) {
                newFlowFile.setOrganization(null);
            }
        }
    }
}
