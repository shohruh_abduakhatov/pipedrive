package org.chromium.customtabsclient.activity;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.CustomTabsServiceConnection;
import android.support.customtabs.CustomTabsSession;
import java.util.List;
import org.chromium.customtabsclient.CustomTabsHelper;
import org.chromium.customtabsclient.ServiceConnection;
import org.chromium.customtabsclient.ServiceConnectionCallback;

public class CustomTabActivityHelper implements ServiceConnectionCallback {
    private CustomTabsClient mClient;
    private CustomTabsServiceConnection mConnection;
    private ConnectionCallback mConnectionCallback;
    private CustomTabsSession mCustomTabsSession;

    public interface ConnectionCallback {
        void onCustomTabsConnected();

        void onCustomTabsDisconnected();
    }

    public interface CustomTabFallback {
        void openUri(Activity activity, Uri uri);
    }

    public static void openCustomTab(Activity activity, CustomTabsIntent customTabsIntent, Uri uri, CustomTabFallback fallback) {
        String packageName = CustomTabsHelper.getPackageNameToUse(activity);
        if (packageName != null) {
            customTabsIntent.intent.setPackage(packageName);
            customTabsIntent.launchUrl(activity, uri);
        } else if (fallback != null) {
            fallback.openUri(activity, uri);
        }
    }

    public void unbindCustomTabsService(Activity activity) {
        if (this.mConnection != null) {
            activity.unbindService(this.mConnection);
            this.mClient = null;
            this.mCustomTabsSession = null;
            this.mConnection = null;
        }
    }

    public CustomTabsSession getSession() {
        if (this.mClient == null) {
            this.mCustomTabsSession = null;
        } else if (this.mCustomTabsSession == null) {
            this.mCustomTabsSession = this.mClient.newSession(null);
        }
        return this.mCustomTabsSession;
    }

    public void setConnectionCallback(ConnectionCallback connectionCallback) {
        this.mConnectionCallback = connectionCallback;
    }

    public void bindCustomTabsService(Activity activity) {
        if (this.mClient == null) {
            String packageName = CustomTabsHelper.getPackageNameToUse(activity);
            if (packageName != null) {
                this.mConnection = new ServiceConnection(this);
                CustomTabsClient.bindCustomTabsService(activity, packageName, this.mConnection);
            }
        }
    }

    public boolean mayLaunchUrl(Uri uri, Bundle extras, List<Bundle> otherLikelyBundles) {
        if (this.mClient == null) {
            return false;
        }
        CustomTabsSession session = getSession();
        if (session != null) {
            return session.mayLaunchUrl(uri, extras, otherLikelyBundles);
        }
        return false;
    }

    public void onServiceConnected(CustomTabsClient client) {
        this.mClient = client;
        this.mClient.warmup(0);
        if (this.mConnectionCallback != null) {
            this.mConnectionCallback.onCustomTabsConnected();
        }
    }

    public void onServiceDisconnected() {
        this.mClient = null;
        this.mCustomTabsSession = null;
        if (this.mConnectionCallback != null) {
            this.mConnectionCallback.onCustomTabsDisconnected();
        }
    }
}
