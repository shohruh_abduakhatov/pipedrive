package com.zendesk.sdk.model.helpcenter;

import android.support.annotation.Nullable;
import com.zendesk.util.CollectionUtils;
import com.zendesk.util.StringUtils;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;

public class HelpCenterSearch implements Serializable, Cloneable {
    private String categoryIds;
    private String include;
    private String labelNames;
    private Locale locale;
    private Integer page;
    private Integer perPage;
    private String query;
    private String sectionIds;

    public static class Builder {
        private String categoryIds;
        private String[] include;
        private String[] labelNames;
        private Locale locale;
        private Integer page;
        private Integer perPage;
        private String query;
        private String sectionIds;

        public Builder withQuery(String query) {
            this.query = query;
            return this;
        }

        public Builder forLocale(Locale locale) {
            this.locale = locale;
            return this;
        }

        public Builder withIncludes(String... includes) {
            this.include = includes;
            return this;
        }

        public Builder withLabelNames(String... labelNames) {
            this.labelNames = labelNames;
            return this;
        }

        public Builder withCategoryId(Long categoryId) {
            if (categoryId != null) {
                this.categoryIds = Long.toString(categoryId.longValue());
            }
            return this;
        }

        public Builder withCategoryIds(List<Long> categoryIds) {
            this.categoryIds = StringUtils.toCsvStringNumber(CollectionUtils.copyOf(categoryIds));
            return this;
        }

        public Builder withSectionId(Long sectionId) {
            if (sectionId != null) {
                this.sectionIds = Long.toString(sectionId.longValue());
            }
            return this;
        }

        public Builder withSectionIds(List<Long> sectionIds) {
            this.sectionIds = StringUtils.toCsvStringNumber(CollectionUtils.copyOf(sectionIds));
            return this;
        }

        public Builder page(Integer page) {
            this.page = page;
            return this;
        }

        public Builder perPage(Integer perPage) {
            this.perPage = perPage;
            return this;
        }

        public HelpCenterSearch build() {
            HelpCenterSearch helpCenterSearch = new HelpCenterSearch();
            helpCenterSearch.query = this.query;
            helpCenterSearch.locale = this.locale;
            helpCenterSearch.include = StringUtils.toCsvString(this.include);
            helpCenterSearch.labelNames = StringUtils.toCsvString(this.labelNames);
            helpCenterSearch.categoryIds = this.categoryIds;
            helpCenterSearch.sectionIds = this.sectionIds;
            helpCenterSearch.page = this.page;
            helpCenterSearch.perPage = this.perPage;
            return helpCenterSearch;
        }
    }

    private HelpCenterSearch() {
    }

    @Nullable
    public String getQuery() {
        return this.query;
    }

    @Nullable
    public Locale getLocale() {
        return this.locale;
    }

    @Nullable
    public String getInclude() {
        return this.include;
    }

    @Nullable
    public String getLabelNames() {
        return this.labelNames;
    }

    @Nullable
    public String getCategoryIds() {
        return this.categoryIds;
    }

    @Nullable
    public String getSectionIds() {
        return this.sectionIds;
    }

    @Nullable
    public Integer getPage() {
        return this.page;
    }

    @Nullable
    public Integer getPerPage() {
        return this.perPage;
    }

    public HelpCenterSearch withQuery(String query) {
        HelpCenterSearch search = new HelpCenterSearch();
        try {
            search = (HelpCenterSearch) clone();
            search.query = query;
            return search;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return search;
        }
    }
}
