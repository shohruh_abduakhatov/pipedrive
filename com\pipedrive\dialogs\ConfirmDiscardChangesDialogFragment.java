package com.pipedrive.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog.Builder;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.background.ApplicationStateMonitor;
import com.newrelic.agent.android.instrumentation.Instrumented;

@Instrumented
public class ConfirmDiscardChangesDialogFragment extends DialogFragment implements TraceFieldInterface {
    private static final String BUTTON_LABEL_NEGATIVE = "BUTTON_LABEL_NEGATIVE";
    private static final String BUTTON_LABEL_POSITIVE = "BUTTON_LABEL_POSITIVE";
    private static final String CONTENT = "CONTENT";
    @Nullable
    private ConfirmDiscardChangesNegativeBtnListener mConfirmDiscardChangesNegativeBtnListener;
    @Nullable
    private ConfirmDiscardChangesPositiveBtnListener mConfirmDiscardChangesPositiveBtnListener;

    public interface ConfirmDiscardChangesNegativeBtnListener {
        void onDialogNegativeBtnClicked();
    }

    public interface ConfirmDiscardChangesPositiveBtnListener {
        void onDialogPositiveBtnClicked();
    }

    protected void onStart() {
        super.onStart();
        ApplicationStateMonitor.getInstance().activityStarted();
    }

    protected void onStop() {
        super.onStop();
        ApplicationStateMonitor.getInstance().activityStopped();
    }

    public static ConfirmDiscardChangesDialogFragment newInstance(@StringRes int content, @StringRes int negativeBtnLabel, @StringRes int positiveBtnLabel) {
        ConfirmDiscardChangesDialogFragment frag = new ConfirmDiscardChangesDialogFragment();
        Bundle args = new Bundle();
        args.putInt(CONTENT, content);
        args.putInt(BUTTON_LABEL_NEGATIVE, negativeBtnLabel);
        args.putInt(BUTTON_LABEL_POSITIVE, positiveBtnLabel);
        frag.setArguments(args);
        return frag;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ConfirmDiscardChangesNegativeBtnListener) {
            this.mConfirmDiscardChangesNegativeBtnListener = (ConfirmDiscardChangesNegativeBtnListener) activity;
        }
        if (activity instanceof ConfirmDiscardChangesPositiveBtnListener) {
            this.mConfirmDiscardChangesPositiveBtnListener = (ConfirmDiscardChangesPositiveBtnListener) activity;
        }
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int content = getArguments().getInt(CONTENT);
        int negativeBtnLabel = getArguments().getInt(BUTTON_LABEL_NEGATIVE);
        return new Builder(getActivity()).setMessage(content).setNegativeButton(negativeBtnLabel, new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (ConfirmDiscardChangesDialogFragment.this.mConfirmDiscardChangesNegativeBtnListener != null) {
                    ConfirmDiscardChangesDialogFragment.this.mConfirmDiscardChangesNegativeBtnListener.onDialogNegativeBtnClicked();
                }
            }
        }).setPositiveButton(getArguments().getInt(BUTTON_LABEL_POSITIVE), new OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if (ConfirmDiscardChangesDialogFragment.this.mConfirmDiscardChangesPositiveBtnListener != null) {
                    ConfirmDiscardChangesDialogFragment.this.mConfirmDiscardChangesPositiveBtnListener.onDialogPositiveBtnClicked();
                }
            }
        }).create();
    }
}
