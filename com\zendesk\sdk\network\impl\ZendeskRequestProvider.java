package com.zendesk.sdk.network.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.SdkConfiguration;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.model.access.AuthenticationType;
import com.zendesk.sdk.model.access.Identity;
import com.zendesk.sdk.model.request.Comment;
import com.zendesk.sdk.model.request.CommentsResponse;
import com.zendesk.sdk.model.request.CreateRequest;
import com.zendesk.sdk.model.request.EndUserComment;
import com.zendesk.sdk.model.request.Request;
import com.zendesk.sdk.model.request.RequestResponse;
import com.zendesk.sdk.model.request.fields.RawTicketField;
import com.zendesk.sdk.model.request.fields.RawTicketForm;
import com.zendesk.sdk.model.request.fields.RawTicketFormResponse;
import com.zendesk.sdk.model.request.fields.TicketField;
import com.zendesk.sdk.model.request.fields.TicketForm;
import com.zendesk.sdk.model.settings.SafeMobileSettings;
import com.zendesk.sdk.network.BaseProvider;
import com.zendesk.sdk.network.RequestProvider;
import com.zendesk.sdk.storage.RequestSessionCache;
import com.zendesk.sdk.storage.RequestStorage;
import com.zendesk.service.ErrorResponse;
import com.zendesk.service.ErrorResponseAdapter;
import com.zendesk.service.ZendeskCallback;
import com.zendesk.util.CollectionUtils;
import com.zendesk.util.LocaleUtil;
import com.zendesk.util.StringUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

final class ZendeskRequestProvider implements RequestProvider {
    private static final String ALL_REQUEST_STATUSES = "new,open,pending,hold,solved,closed";
    private static final String LOG_TAG = "ZendeskRequestProvider";
    private static final int MAX_TICKET_FIELDS = 5;
    private static final String SIDE_LOAD_PUBLIC_UPDATED_AT = "public_updated_at";
    private final BaseProvider baseProvider;
    private final Identity identity;
    private final Locale locale;
    private final ZendeskRequestService requestService;
    private final RequestSessionCache requestSessionCache;
    private final RequestStorage requestStorage;

    ZendeskRequestProvider(BaseProvider baseProvider, ZendeskRequestService requestService, Identity identity, RequestStorage requestStorage, RequestSessionCache requestSessionCache, Locale locale) {
        this.baseProvider = baseProvider;
        this.requestService = requestService;
        this.identity = identity;
        this.requestStorage = requestStorage;
        this.requestSessionCache = requestSessionCache;
        this.locale = locale;
    }

    public void createRequest(@NonNull final CreateRequest request, @Nullable final ZendeskCallback<CreateRequest> callback) {
        boolean isValid = true;
        if (request == null) {
            isValid = false;
        }
        if (isValid) {
            this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
                public void onSuccess(SdkConfiguration config) {
                    ZendeskRequestProvider.this.internalCreateRequest(config.getBearerAuthorizationHeader(), request, config.getMobileSettings().getAuthenticationType(), callback);
                }
            });
            return;
        }
        String error = "configuration is invalid: request null";
        Logger.e(LOG_TAG, "configuration is invalid: request null", new Object[0]);
        if (callback != null) {
            callback.onError(new ErrorResponseAdapter("configuration is invalid: request null"));
        }
    }

    void internalCreateRequest(@NonNull String header, @NonNull CreateRequest request, AuthenticationType authentication, @Nullable ZendeskCallback<CreateRequest> callback) {
        final CreateRequest createRequest = request;
        final AuthenticationType authenticationType = authentication;
        final ZendeskCallback<CreateRequest> zendeskCallback = callback;
        ZendeskCallback<Request> zendeskCallback2 = new PassThroughErrorZendeskCallback<Request>(callback) {
            public void onSuccess(Request result) {
                if (result.getId() == null) {
                    onError(new ErrorResponseAdapter("The request was created, but the ID is unknown."));
                    return;
                }
                ZendeskConfig.INSTANCE.getTracker().requestCreated();
                createRequest.setId(result.getId());
                if (authenticationType == AuthenticationType.ANONYMOUS) {
                    ZendeskRequestProvider.this.requestStorage.storeRequestId(result.getId());
                }
                if (zendeskCallback != null) {
                    zendeskCallback.onSuccess(createRequest);
                }
            }
        };
        if (authentication == AuthenticationType.ANONYMOUS && this.identity != null && (this.identity instanceof AnonymousIdentity)) {
            this.requestService.createRequest(header, this.identity.getSdkGuid(), request, zendeskCallback2);
            return;
        }
        this.requestService.createRequest(header, null, request, zendeskCallback2);
    }

    void getAllRequestsInternal(@NonNull String header, @Nullable String status, AuthenticationType authentication, @Nullable ZendeskCallback<List<Request>> callback) {
        if (StringUtils.isEmpty(status)) {
            status = ALL_REQUEST_STATUSES;
        }
        String include = SIDE_LOAD_PUBLIC_UPDATED_AT;
        if (authentication == AuthenticationType.ANONYMOUS) {
            List listOfRequestIds = this.requestStorage.getStoredRequestIds();
            if (CollectionUtils.isEmpty(listOfRequestIds)) {
                Logger.w(LOG_TAG, "getAllRequestsInternal: There are no requests to fetch", new Object[0]);
                if (callback != null) {
                    callback.onSuccess(new ArrayList());
                    return;
                }
                return;
            }
            this.requestService.getAllRequests(header, StringUtils.toCsvString(listOfRequestIds), status, SIDE_LOAD_PUBLIC_UPDATED_AT, callback);
            return;
        }
        this.requestService.getAllRequests(header, status, SIDE_LOAD_PUBLIC_UPDATED_AT, callback);
    }

    public void getAllRequests(@Nullable ZendeskCallback<List<Request>> callback) {
        getRequests(null, callback);
    }

    public void getRequests(@Nullable final String status, @Nullable final ZendeskCallback<List<Request>> callback) {
        this.baseProvider.configureSdk(new ZendeskCallback<SdkConfiguration>() {
            public void onSuccess(SdkConfiguration config) {
                if (ZendeskRequestProvider.this.areConversationsEnabled(config.getMobileSettings())) {
                    ZendeskRequestProvider.this.getAllRequestsInternal(config.getBearerAuthorizationHeader(), status, config.getMobileSettings().getAuthenticationType(), callback);
                } else {
                    ZendeskRequestProvider.this.answerCallbackOnConversationsDisabled(callback);
                }
            }

            public void onError(ErrorResponse error) {
                if (callback != null) {
                    callback.onError(error);
                }
            }
        });
    }

    private void getComments(@NonNull String header, @NonNull String requestId, @Nullable ZendeskCallback<CommentsResponse> callback) {
        this.requestService.getComments(header, requestId, callback);
    }

    public void getComments(@NonNull final String requestId, @Nullable final ZendeskCallback<CommentsResponse> callback) {
        this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
            public void onSuccess(SdkConfiguration config) {
                if (ZendeskRequestProvider.this.areConversationsEnabled(config.getMobileSettings())) {
                    ZendeskRequestProvider.this.getComments(config.getBearerAuthorizationHeader(), requestId, callback);
                } else {
                    ZendeskRequestProvider.this.answerCallbackOnConversationsDisabled(callback);
                }
            }
        });
    }

    void addCommentInternal(@NonNull String header, @NonNull String requestId, @NonNull EndUserComment endUserComment, @Nullable final ZendeskCallback<Comment> callback) {
        this.requestService.addComment(header, requestId, endUserComment, new PassThroughErrorZendeskCallback<Request>(callback) {
            public void onSuccess(Request result) {
                ZendeskConfig.INSTANCE.getTracker().requestUpdated();
                if (result.getId() == null || result.getCommentCount() == null) {
                    Logger.w(ZendeskRequestProvider.LOG_TAG, "The ID and / or comment count was missing. Cannot store comment count.", new Object[0]);
                } else {
                    ZendeskRequestProvider.this.requestStorage.setCommentCount(result.getId(), result.getCommentCount().intValue());
                }
                Comment comment = new Comment();
                comment.setAuthorId(result.getRequesterId());
                comment.setCreatedAt(new Date());
                if (callback != null) {
                    callback.onSuccess(comment);
                }
            }
        });
    }

    public void addComment(@NonNull String requestId, @NonNull EndUserComment endUserComment, @Nullable ZendeskCallback<Comment> callback) {
        final String str = requestId;
        final EndUserComment endUserComment2 = endUserComment;
        final ZendeskCallback<Comment> zendeskCallback = callback;
        this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
            public void onSuccess(SdkConfiguration config) {
                if (ZendeskRequestProvider.this.areConversationsEnabled(config.getMobileSettings())) {
                    ZendeskRequestProvider.this.addCommentInternal(config.getBearerAuthorizationHeader(), str, endUserComment2, zendeskCallback);
                } else {
                    ZendeskRequestProvider.this.answerCallbackOnConversationsDisabled(zendeskCallback);
                }
            }
        });
    }

    public void getRequest(@NonNull final String requestId, @Nullable final ZendeskCallback<Request> callback) {
        this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
            public void onSuccess(SdkConfiguration result) {
                if (ZendeskRequestProvider.this.areConversationsEnabled(result.getMobileSettings())) {
                    ZendeskRequestProvider.this.requestService.getRequest(result.getBearerAuthorizationHeader(), requestId, new PassThroughErrorZendeskCallback<RequestResponse>(callback) {
                        public void onSuccess(RequestResponse result) {
                            if (callback != null) {
                                callback.onSuccess(result.getRequest());
                            }
                        }
                    });
                } else {
                    ZendeskRequestProvider.this.answerCallbackOnConversationsDisabled(callback);
                }
            }
        });
    }

    public void getTicketFormsById(@NonNull List<Long> ids, @Nullable final ZendeskCallback<List<TicketForm>> callback) {
        if (!CollectionUtils.isEmpty(ids)) {
            final List<Long> ticketFieldIds = new ArrayList();
            if (ids.size() > 5) {
                ticketFieldIds.addAll(ids.subList(0, 5));
                Logger.d(LOG_TAG, "Maximum number of allowed ticket fields: %d.", Integer.valueOf(5));
            } else {
                ticketFieldIds.addAll(ids);
            }
            if (!this.requestSessionCache.containsAllTicketForms(ticketFieldIds)) {
                this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
                    public void onSuccess(SdkConfiguration result) {
                        if (result.getMobileSettings().isTicketFormSupportAvailable()) {
                            String idsToFilter = StringUtils.toCsvStringNumber(ticketFieldIds);
                            ZendeskRequestProvider.this.requestService.getTicketFormsById(result.getBearerAuthorizationHeader(), LocaleUtil.toLanguageTag(ZendeskRequestProvider.this.locale), idsToFilter, new PassThroughErrorZendeskCallback<RawTicketFormResponse>(callback) {
                                public void onSuccess(RawTicketFormResponse ticketFormResponse) {
                                    List<TicketForm> ticketForms = ZendeskRequestProvider.this.convertTicketFormResponse(ticketFormResponse.getTicketForms(), ticketFormResponse.getTicketFields());
                                    ZendeskRequestProvider.this.requestSessionCache.updateTicketFormCache(ticketForms);
                                    if (callback != null) {
                                        callback.onSuccess(ticketForms);
                                    }
                                }
                            });
                        } else if (callback != null) {
                            callback.onError(new ErrorResponseAdapter("Ticket form support disabled."));
                        }
                    }
                });
            } else if (callback != null) {
                callback.onSuccess(this.requestSessionCache.getTicketFormsById(ticketFieldIds));
            }
        } else if (callback != null) {
            callback.onError(new ErrorResponseAdapter("Ticket forms must at least contain 1 Id"));
        }
    }

    List<TicketForm> convertTicketFormResponse(List<RawTicketForm> rawTicketForms, List<RawTicketField> rawTicketFields) {
        List<TicketForm> ticketForms = new ArrayList();
        Map<Long, TicketField> ticketFieldMap = createTicketFieldMap(rawTicketFields);
        for (RawTicketForm rawTicketForm : rawTicketForms) {
            ticketForms.add(createTicketFormFromResponse(rawTicketForm, ticketFieldMap));
        }
        return ticketForms;
    }

    TicketForm createTicketFormFromResponse(RawTicketForm rawTicketForm, Map<Long, TicketField> ticketFieldMap) {
        List<TicketField> ticketFields = new ArrayList();
        for (Long id : rawTicketForm.getTicketFieldIds()) {
            if (!(id == null || ticketFieldMap.get(id) == null)) {
                ticketFields.add(ticketFieldMap.get(id));
            }
        }
        return TicketForm.create(rawTicketForm, ticketFields);
    }

    Map<Long, TicketField> createTicketFieldMap(List<RawTicketField> rawTicketFields) {
        Map<Long, TicketField> ticketFieldMap = new HashMap(rawTicketFields.size());
        for (RawTicketField rawTicketField : rawTicketFields) {
            ticketFieldMap.put(Long.valueOf(rawTicketField.getId()), TicketField.create(rawTicketField));
        }
        return ticketFieldMap;
    }

    boolean areConversationsEnabled(@Nullable SafeMobileSettings mobileSettings) {
        return mobileSettings == null ? false : mobileSettings.isConversationsEnabled();
    }

    void answerCallbackOnConversationsDisabled(@Nullable ZendeskCallback callback) {
        Logger.d(LOG_TAG, "Conversations disabled, this feature is not available on your plan or was disabled.", new Object[0]);
        if (callback != null) {
            callback.onError(new ErrorResponseAdapter("Access Denied"));
        }
    }
}
