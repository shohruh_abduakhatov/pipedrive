package com.pipedrive.navigator.buttons;

import com.pipedrive.R;

public abstract class ClearMenuItem implements NavigatorMenuItem {
    public int getMenuItemId() {
        return R.id.menu_clear;
    }
}
