package com.google.android.gms.wearable.internal;

import android.content.IntentFilter;
import android.net.Uri;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzrr;
import com.google.android.gms.wearable.CapabilityApi;
import com.google.android.gms.wearable.CapabilityApi.AddLocalCapabilityResult;
import com.google.android.gms.wearable.CapabilityApi.CapabilityListener;
import com.google.android.gms.wearable.CapabilityApi.GetAllCapabilitiesResult;
import com.google.android.gms.wearable.CapabilityApi.GetCapabilityResult;
import com.google.android.gms.wearable.CapabilityApi.RemoveLocalCapabilityResult;
import com.google.android.gms.wearable.CapabilityInfo;
import com.google.android.gms.wearable.Node;
import java.util.Map;
import java.util.Set;

public class zzj implements CapabilityApi {

    class AnonymousClass5 implements zza<CapabilityListener> {
        final /* synthetic */ IntentFilter[] aTc;

        AnonymousClass5(IntentFilter[] intentFilterArr) {
            this.aTc = intentFilterArr;
        }

        public void zza(zzbp com_google_android_gms_wearable_internal_zzbp, com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, CapabilityListener capabilityListener, zzrr<CapabilityListener> com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_CapabilityApi_CapabilityListener) throws RemoteException {
            com_google_android_gms_wearable_internal_zzbp.zza((com.google.android.gms.internal.zzqo.zzb) com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, capabilityListener, (zzrr) com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_CapabilityApi_CapabilityListener, this.aTc);
        }
    }

    public static class zza implements AddLocalCapabilityResult, RemoveLocalCapabilityResult {
        private final Status hv;

        public zza(Status status) {
            this.hv = status;
        }

        public Status getStatus() {
            return this.hv;
        }
    }

    private static class zzb implements CapabilityListener {
        final CapabilityListener aTd;
        final String aTe;

        zzb(CapabilityListener capabilityListener, String str) {
            this.aTd = capabilityListener;
            this.aTe = str;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            zzb com_google_android_gms_wearable_internal_zzj_zzb = (zzb) obj;
            return this.aTd.equals(com_google_android_gms_wearable_internal_zzj_zzb.aTd) ? this.aTe.equals(com_google_android_gms_wearable_internal_zzj_zzb.aTe) : false;
        }

        public int hashCode() {
            return (this.aTd.hashCode() * 31) + this.aTe.hashCode();
        }

        public void onCapabilityChanged(CapabilityInfo capabilityInfo) {
            this.aTd.onCapabilityChanged(capabilityInfo);
        }
    }

    public static class zzc implements CapabilityInfo {
        private final Set<Node> aTf;
        private final String mName;

        public zzc(CapabilityInfo capabilityInfo) {
            this(capabilityInfo.getName(), capabilityInfo.getNodes());
        }

        public zzc(String str, Set<Node> set) {
            this.mName = str;
            this.aTf = set;
        }

        public String getName() {
            return this.mName;
        }

        public Set<Node> getNodes() {
            return this.aTf;
        }
    }

    public static class zzd implements GetAllCapabilitiesResult {
        private final Map<String, CapabilityInfo> aTg;
        private final Status hv;

        public zzd(Status status, Map<String, CapabilityInfo> map) {
            this.hv = status;
            this.aTg = map;
        }

        public Map<String, CapabilityInfo> getAllCapabilities() {
            return this.aTg;
        }

        public Status getStatus() {
            return this.hv;
        }
    }

    public static class zze implements GetCapabilityResult {
        private final CapabilityInfo aTh;
        private final Status hv;

        public zze(Status status, CapabilityInfo capabilityInfo) {
            this.hv = status;
            this.aTh = capabilityInfo;
        }

        public CapabilityInfo getCapability() {
            return this.aTh;
        }

        public Status getStatus() {
            return this.hv;
        }
    }

    private static final class zzf extends zzi<Status> {
        private CapabilityListener aTd;

        private zzf(GoogleApiClient googleApiClient, CapabilityListener capabilityListener) {
            super(googleApiClient);
            this.aTd = capabilityListener;
        }

        protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
            com_google_android_gms_wearable_internal_zzbp.zza((com.google.android.gms.internal.zzqo.zzb) this, this.aTd);
            this.aTd = null;
        }

        public Status zzb(Status status) {
            this.aTd = null;
            return status;
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    private PendingResult<Status> zza(GoogleApiClient googleApiClient, CapabilityListener capabilityListener, IntentFilter[] intentFilterArr) {
        return zzb.zza(googleApiClient, zza(intentFilterArr), capabilityListener);
    }

    private static zza<CapabilityListener> zza(IntentFilter[] intentFilterArr) {
        return new AnonymousClass5(intentFilterArr);
    }

    public PendingResult<Status> addCapabilityListener(GoogleApiClient googleApiClient, CapabilityListener capabilityListener, String str) {
        String str2;
        zzaa.zzb(str != null, (Object) "capability must not be null");
        CapabilityListener com_google_android_gms_wearable_internal_zzj_zzb = new zzb(capabilityListener, str);
        IntentFilter zzrp = zzbn.zzrp(CapabilityApi.ACTION_CAPABILITY_CHANGED);
        if (str.startsWith("/")) {
            str2 = str;
        } else {
            String str3 = "/";
            str2 = String.valueOf(str);
            str2 = str2.length() != 0 ? str3.concat(str2) : new String(str3);
        }
        zzrp.addDataPath(str2, 0);
        return zza(googleApiClient, com_google_android_gms_wearable_internal_zzj_zzb, new IntentFilter[]{zzrp});
    }

    public PendingResult<Status> addListener(GoogleApiClient googleApiClient, CapabilityListener capabilityListener, Uri uri, int i) {
        zzaa.zzb(uri != null, (Object) "uri must not be null");
        boolean z = i == 0 || i == 1;
        zzaa.zzb(z, (Object) "invalid filter type");
        return zza(googleApiClient, capabilityListener, new IntentFilter[]{zzbn.zza(CapabilityApi.ACTION_CAPABILITY_CHANGED, uri, i)});
    }

    public PendingResult<AddLocalCapabilityResult> addLocalCapability(GoogleApiClient googleApiClient, final String str) {
        return googleApiClient.zza(new zzi<AddLocalCapabilityResult>(this, googleApiClient) {
            final /* synthetic */ zzj aTb;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zzv(this, str);
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzep(status);
            }

            protected AddLocalCapabilityResult zzep(Status status) {
                return new zza(status);
            }
        });
    }

    public PendingResult<GetAllCapabilitiesResult> getAllCapabilities(GoogleApiClient googleApiClient, final int i) {
        boolean z = true;
        if (!(i == 0 || i == 1)) {
            z = false;
        }
        zzaa.zzbt(z);
        return googleApiClient.zza(new zzi<GetAllCapabilitiesResult>(this, googleApiClient) {
            final /* synthetic */ zzj aTb;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zzd(this, i);
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzeo(status);
            }

            protected GetAllCapabilitiesResult zzeo(Status status) {
                return new zzd(status, null);
            }
        });
    }

    public PendingResult<GetCapabilityResult> getCapability(GoogleApiClient googleApiClient, final String str, final int i) {
        boolean z = true;
        if (!(i == 0 || i == 1)) {
            z = false;
        }
        zzaa.zzbt(z);
        return googleApiClient.zza(new zzi<GetCapabilityResult>(this, googleApiClient) {
            final /* synthetic */ zzj aTb;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zzh(this, str, i);
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzen(status);
            }

            protected GetCapabilityResult zzen(Status status) {
                return new zze(status, null);
            }
        });
    }

    public PendingResult<Status> removeCapabilityListener(GoogleApiClient googleApiClient, CapabilityListener capabilityListener, String str) {
        return googleApiClient.zza(new zzf(googleApiClient, new zzb(capabilityListener, str)));
    }

    public PendingResult<Status> removeListener(GoogleApiClient googleApiClient, CapabilityListener capabilityListener) {
        return googleApiClient.zza(new zzf(googleApiClient, capabilityListener));
    }

    public PendingResult<RemoveLocalCapabilityResult> removeLocalCapability(GoogleApiClient googleApiClient, final String str) {
        return googleApiClient.zza(new zzi<RemoveLocalCapabilityResult>(this, googleApiClient) {
            final /* synthetic */ zzj aTb;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zzw(this, str);
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzeq(status);
            }

            protected RemoveLocalCapabilityResult zzeq(Status status) {
                return new zza(status);
            }
        });
    }
}
