package com.pipedrive.util.formatter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.CurrenciesDataSource;
import com.pipedrive.datetime.LocaleHelper;
import com.pipedrive.model.Currency;
import java.text.NumberFormat;
import java.util.Locale;

abstract class BaseFormatter {
    private static final int MAX_FRACTION_DIGITS_DEFAULT = 2;
    private static final int MIN_FRACTION_DIGITS_DEFAULT = 0;
    @Nullable
    private NumberFormat numberFormat;
    @NonNull
    private final Session session;

    @NonNull
    abstract NumberFormat instantiateNumberFormat(@NonNull Locale locale);

    BaseFormatter(@NonNull Session session) {
        this.session = session;
    }

    @NonNull
    final Session getSession() {
        return this.session;
    }

    @NonNull
    final NumberFormat getNumberFormatForCurrency(@Nullable Currency currency) {
        NumberFormat numberFormatForCurrency = getNumberFormat(LocaleHelper.getAppLocaleForNumbersDatesFormatting(getSession()));
        numberFormatForCurrency.setMinimumFractionDigits(0);
        numberFormatForCurrency.setMaximumFractionDigits(getMaxFractionDigits(currency).intValue());
        return numberFormatForCurrency;
    }

    @NonNull
    private NumberFormat getNumberFormat(@NonNull Locale locale) {
        if (this.numberFormat == null) {
            this.numberFormat = instantiateNumberFormat(locale);
        }
        return this.numberFormat;
    }

    @NonNull
    private Integer getMaxFractionDigits(@Nullable Currency currency) {
        if (currency != null) {
            return Integer.valueOf(currency.getDecimalPoints());
        }
        Currency defaultCurrency = getCurrencyByCode(getSession().getUserSettingsDefaultCurrencyCode());
        return Integer.valueOf(defaultCurrency != null ? defaultCurrency.getDecimalPoints() : 2);
    }

    @Nullable
    final Currency getCurrencyByCode(String currencyCode) {
        return new CurrenciesDataSource(getSession().getDatabase()).getCurrencyByCode(currencyCode);
    }

    @NonNull
    String format(@NonNull Double value) {
        return format(value, null);
    }

    @NonNull
    String format(@NonNull Double value, @Nullable Currency currency) {
        return getNumberFormatForCurrency(currency).format(value);
    }
}
