package com.pipedrive.util.networking.entities;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

public class FlowItem {
    public static final String ITEM_TYPE_OBJECT_ACTIVITY = "activity";
    public static final String ITEM_TYPE_OBJECT_DEAL = "deal";
    public static final String ITEM_TYPE_OBJECT_EMAILMESSAGE = "emailMessage";
    public static final String ITEM_TYPE_OBJECT_FILE = "file";
    public static final String ITEM_TYPE_OBJECT_NOTE = "note";
    @SerializedName("data")
    private JsonObject mData;
    @SerializedName("object")
    private String mItemType;

    public String getItemType() {
        return this.mItemType;
    }

    public JsonObject getData() {
        return this.mData;
    }
}
