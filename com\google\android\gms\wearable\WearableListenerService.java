package com.google.android.gms.wearable;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.util.zzx;
import com.google.android.gms.common.zze;
import com.google.android.gms.common.zzf;
import com.google.android.gms.wearable.CapabilityApi.CapabilityListener;
import com.google.android.gms.wearable.ChannelApi.ChannelListener;
import com.google.android.gms.wearable.DataApi.DataListener;
import com.google.android.gms.wearable.MessageApi.MessageListener;
import com.google.android.gms.wearable.NodeApi.NodeListener;
import com.google.android.gms.wearable.internal.AmsEntityUpdateParcelable;
import com.google.android.gms.wearable.internal.AncsNotificationParcelable;
import com.google.android.gms.wearable.internal.CapabilityInfoParcelable;
import com.google.android.gms.wearable.internal.ChannelEventParcelable;
import com.google.android.gms.wearable.internal.MessageEventParcelable;
import com.google.android.gms.wearable.internal.NodeParcelable;
import java.util.List;

public abstract class WearableListenerService extends Service implements CapabilityListener, ChannelListener, DataListener, MessageListener, NodeListener {
    public static final String BIND_LISTENER_INTENT_ACTION = "com.google.android.gms.wearable.BIND_LISTENER";
    private IBinder DI;
    private boolean aSA;
    private ComponentName aSw;
    private zzb aSx;
    private Intent aSy;
    private final Object aSz = new Object();

    private class zza implements ServiceConnection {
        final /* synthetic */ WearableListenerService aSB;

        private zza(WearableListenerService wearableListenerService) {
            this.aSB = wearableListenerService;
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        }

        public void onServiceDisconnected(ComponentName componentName) {
        }
    }

    private final class zzb extends Handler {
        final /* synthetic */ WearableListenerService aSB;
        private zza aSC = new zza();
        private boolean started;

        zzb(WearableListenerService wearableListenerService, Looper looper) {
            this.aSB = wearableListenerService;
            super(looper);
        }

        @SuppressLint({"UntrackedBindService"})
        public void dispatchMessage(Message message) {
            if (!this.started) {
                if (Log.isLoggable("WearableLS", 2)) {
                    String valueOf = String.valueOf(this.aSB.aSw);
                    Log.v("WearableLS", new StringBuilder(String.valueOf(valueOf).length() + 13).append("bindService: ").append(valueOf).toString());
                }
                this.aSB.bindService(this.aSB.aSy, this.aSC, 1);
                this.started = true;
            }
            try {
                super.dispatchMessage(message);
                if (!hasMessages(0)) {
                    if (Log.isLoggable("WearableLS", 2)) {
                        valueOf = String.valueOf(this.aSB.aSw);
                        Log.v("WearableLS", new StringBuilder(String.valueOf(valueOf).length() + 15).append("unbindService: ").append(valueOf).toString());
                    }
                    try {
                        this.aSB.unbindService(this.aSC);
                    } catch (Throwable e) {
                        Log.e("WearableLS", "Exception when unbinding from local service", e);
                    }
                    this.started = false;
                }
            } catch (Throwable th) {
                if (!hasMessages(0)) {
                    if (Log.isLoggable("WearableLS", 2)) {
                        String valueOf2 = String.valueOf(this.aSB.aSw);
                        Log.v("WearableLS", new StringBuilder(String.valueOf(valueOf2).length() + 15).append("unbindService: ").append(valueOf2).toString());
                    }
                    try {
                        this.aSB.unbindService(this.aSC);
                    } catch (Throwable e2) {
                        Log.e("WearableLS", "Exception when unbinding from local service", e2);
                    }
                    this.started = false;
                }
            }
        }
    }

    private final class zzc extends com.google.android.gms.wearable.internal.zzaw.zza {
        final /* synthetic */ WearableListenerService aSB;
        private volatile int aSD;

        private zzc(WearableListenerService wearableListenerService) {
            this.aSB = wearableListenerService;
            this.aSD = -1;
        }

        private boolean zza(Runnable runnable, String str, Object obj) {
            if (Log.isLoggable("WearableLS", 3)) {
                Log.d("WearableLS", String.format("%s: %s %s", new Object[]{str, this.aSB.aSw.toString(), obj}));
            }
            zzcmi();
            synchronized (this.aSB.aSz) {
                if (this.aSB.aSA) {
                    return false;
                }
                this.aSB.aSx.post(runnable);
                return true;
            }
        }

        private void zzcmi() throws SecurityException {
            int callingUid = Binder.getCallingUid();
            if (callingUid != this.aSD) {
                if (zzf.zzbv(this.aSB).zzb(this.aSB.getPackageManager(), "com.google.android.wearable.app.cn") && zze.zzc(this.aSB, callingUid, "com.google.android.wearable.app.cn")) {
                    this.aSD = callingUid;
                } else if (zzx.zzf(this.aSB, callingUid)) {
                    this.aSD = callingUid;
                } else {
                    throw new SecurityException("Caller is not GooglePlayServices");
                }
            }
        }

        public void onConnectedNodes(final List<NodeParcelable> list) {
            zza(new Runnable(this) {
                final /* synthetic */ zzc aSF;

                public void run() {
                    this.aSF.aSB.onConnectedNodes(list);
                }
            }, "onConnectedNodes", list);
        }

        public void zza(final AmsEntityUpdateParcelable amsEntityUpdateParcelable) {
            zza(new Runnable(this) {
                final /* synthetic */ zzc aSF;

                public void run() {
                    this.aSF.aSB.onEntityUpdate(amsEntityUpdateParcelable);
                }
            }, "onEntityUpdate", amsEntityUpdateParcelable);
        }

        public void zza(final AncsNotificationParcelable ancsNotificationParcelable) {
            zza(new Runnable(this) {
                final /* synthetic */ zzc aSF;

                public void run() {
                    this.aSF.aSB.onNotificationReceived(ancsNotificationParcelable);
                }
            }, "onNotificationReceived", ancsNotificationParcelable);
        }

        public void zza(final CapabilityInfoParcelable capabilityInfoParcelable) {
            zza(new Runnable(this) {
                final /* synthetic */ zzc aSF;

                public void run() {
                    this.aSF.aSB.onCapabilityChanged(capabilityInfoParcelable);
                }
            }, "onConnectedCapabilityChanged", capabilityInfoParcelable);
        }

        public void zza(final ChannelEventParcelable channelEventParcelable) {
            zza(new Runnable(this) {
                final /* synthetic */ zzc aSF;

                public void run() {
                    channelEventParcelable.zza(this.aSF.aSB);
                }
            }, "onChannelEvent", channelEventParcelable);
        }

        public void zza(final MessageEventParcelable messageEventParcelable) {
            zza(new Runnable(this) {
                final /* synthetic */ zzc aSF;

                public void run() {
                    this.aSF.aSB.onMessageReceived(messageEventParcelable);
                }
            }, "onMessageReceived", messageEventParcelable);
        }

        public void zza(final NodeParcelable nodeParcelable) {
            zza(new Runnable(this) {
                final /* synthetic */ zzc aSF;

                public void run() {
                    this.aSF.aSB.onPeerConnected(nodeParcelable);
                }
            }, "onPeerConnected", nodeParcelable);
        }

        public void zzb(final NodeParcelable nodeParcelable) {
            zza(new Runnable(this) {
                final /* synthetic */ zzc aSF;

                public void run() {
                    this.aSF.aSB.onPeerDisconnected(nodeParcelable);
                }
            }, "onPeerDisconnected", nodeParcelable);
        }

        public void zzbq(final DataHolder dataHolder) {
            Runnable anonymousClass1 = new Runnable(this) {
                final /* synthetic */ zzc aSF;

                public void run() {
                    DataEventBuffer dataEventBuffer = new DataEventBuffer(dataHolder);
                    try {
                        this.aSF.aSB.onDataChanged(dataEventBuffer);
                    } finally {
                        dataEventBuffer.release();
                    }
                }
            };
            try {
                String valueOf = String.valueOf(dataHolder);
                if (!zza(anonymousClass1, "onDataItemChanged", new StringBuilder(String.valueOf(valueOf).length() + 18).append(valueOf).append(", rows=").append(dataHolder.getCount()).toString())) {
                }
            } finally {
                dataHolder.close();
            }
        }
    }

    public final IBinder onBind(Intent intent) {
        return BIND_LISTENER_INTENT_ACTION.equals(intent.getAction()) ? this.DI : null;
    }

    public void onCapabilityChanged(CapabilityInfo capabilityInfo) {
    }

    public void onChannelClosed(Channel channel, int i, int i2) {
    }

    public void onChannelOpened(Channel channel) {
    }

    public void onConnectedNodes(List<Node> list) {
    }

    public void onCreate() {
        super.onCreate();
        this.aSw = new ComponentName(this, getClass().getName());
        if (Log.isLoggable("WearableLS", 3)) {
            String valueOf = String.valueOf(this.aSw);
            Log.d("WearableLS", new StringBuilder(String.valueOf(valueOf).length() + 10).append("onCreate: ").append(valueOf).toString());
        }
        HandlerThread handlerThread = new HandlerThread("WearableListenerService");
        handlerThread.start();
        this.aSx = new zzb(this, handlerThread.getLooper());
        this.aSy = new Intent(BIND_LISTENER_INTENT_ACTION);
        this.aSy.setComponent(this.aSw);
        this.DI = new zzc();
    }

    public void onDataChanged(DataEventBuffer dataEventBuffer) {
    }

    public void onDestroy() {
        if (Log.isLoggable("WearableLS", 3)) {
            String valueOf = String.valueOf(this.aSw);
            Log.d("WearableLS", new StringBuilder(String.valueOf(valueOf).length() + 11).append("onDestroy: ").append(valueOf).toString());
        }
        synchronized (this.aSz) {
            this.aSA = true;
            if (this.aSx == null) {
                String valueOf2 = String.valueOf(this.aSw);
                throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf2).length() + 111).append("onDestroy: mServiceHandler not set, did you override onCreate() but forget to call super.onCreate()? component=").append(valueOf2).toString());
            } else {
                this.aSx.getLooper().quit();
            }
        }
        super.onDestroy();
    }

    public void onEntityUpdate(zzb com_google_android_gms_wearable_zzb) {
    }

    public void onInputClosed(Channel channel, int i, int i2) {
    }

    public void onMessageReceived(MessageEvent messageEvent) {
    }

    public void onNotificationReceived(zzd com_google_android_gms_wearable_zzd) {
    }

    public void onOutputClosed(Channel channel, int i, int i2) {
    }

    public void onPeerConnected(Node node) {
    }

    public void onPeerDisconnected(Node node) {
    }
}
