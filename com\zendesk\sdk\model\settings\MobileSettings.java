package com.zendesk.sdk.model.settings;

import android.support.annotation.NonNull;

public class MobileSettings {
    private AccountSettings account;
    private SdkSettings sdk;

    @NonNull
    public SdkSettings getSdkSettings() {
        return this.sdk == null ? new SdkSettings() : this.sdk;
    }

    @NonNull
    public AccountSettings getAccountSettings() {
        return this.account == null ? new AccountSettings() : this.account;
    }
}
