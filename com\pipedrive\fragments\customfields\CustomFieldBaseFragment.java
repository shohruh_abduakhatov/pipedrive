package com.pipedrive.fragments.customfields;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.pipedrive.R;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.fragments.BaseFragment;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.navigator.Navigable;
import com.pipedrive.navigator.buttons.ClearMenuItem;
import com.pipedrive.navigator.buttons.DiscardMenuItem;
import com.pipedrive.navigator.buttons.NavigatorMenuItem;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished.TaskResult;
import com.pipedrive.tasks.CustomFieldSavingTask;
import com.pipedrive.util.ViewUtil;
import java.util.Arrays;
import java.util.List;

public abstract class CustomFieldBaseFragment extends BaseFragment implements Navigable {
    static final /* synthetic */ boolean $assertionsDisabled = (!CustomFieldBaseFragment.class.desiredAssertionStatus());
    public static final String ARG_CUSTOM_FIELD = "CUSTOM_FIELD";
    public static final String ARG_ITEM_SQL_ID = "ITEM_SQL_ID";
    public static final String TAG = CustomFieldBaseFragment.class.getSimpleName();
    private ClearMenuItem mClearMenuItem = new ClearMenuItem() {
        public boolean isEnabled() {
            return CustomFieldBaseFragment.this.contentExists();
        }

        public void onClick() {
            CustomFieldBaseFragment.this.onClear();
        }
    };
    protected CustomField mCustomField;
    private DiscardMenuItem mDiscardMenuItem = new DiscardMenuItem() {
        public void onClick() {
            CustomFieldBaseFragment.this.onCancel();
        }
    };
    private long mItemSqlId;
    private CustomField mOriginalCustomField;

    protected abstract void clearContent();

    protected abstract void saveContent();

    @NonNull
    public static CustomFieldBaseFragment newInstance(@NonNull Bundle params) {
        CustomField customField = (CustomField) params.getParcelable(ARG_CUSTOM_FIELD);
        if ($assertionsDisabled || customField != null) {
            CustomFieldBaseFragment fragment;
            String fieldDataType = customField.getFieldDataType();
            if (fieldDataType != null) {
                Object obj = -1;
                switch (fieldDataType.hashCode()) {
                    case -1325958191:
                        if (fieldDataType.equals(CustomField.FIELD_DATA_TYPE_DOUBLE)) {
                            obj = 10;
                            break;
                        }
                        break;
                    case -991716523:
                        if (fieldDataType.equals("person")) {
                            obj = 3;
                            break;
                        }
                        break;
                    case -332506163:
                        if (fieldDataType.equals(CustomField.FIELD_DATA_TYPE_MONEY)) {
                            obj = 5;
                            break;
                        }
                        break;
                    case -231872945:
                        if (fieldDataType.equals(CustomField.FIELD_DATA_TYPE_DATE_RANGE)) {
                            obj = 8;
                            break;
                        }
                        break;
                    case 113762:
                        if (fieldDataType.equals(CustomField.FIELD_DATA_TYPE_MULTIPLE_OPTION)) {
                            obj = null;
                            break;
                        }
                        break;
                    case 3076014:
                        if (fieldDataType.equals("date")) {
                            obj = 6;
                            break;
                        }
                        break;
                    case 3118337:
                        if (fieldDataType.equals(CustomField.FIELD_DATA_TYPE_SINGLE_OPTION)) {
                            obj = 1;
                            break;
                        }
                        break;
                    case 3560141:
                        if (fieldDataType.equals("time")) {
                            obj = 7;
                            break;
                        }
                        break;
                    case 3599307:
                        if (fieldDataType.equals(CustomField.FIELD_DATA_TYPE_USER)) {
                            obj = 2;
                            break;
                        }
                        break;
                    case 53649040:
                        if (fieldDataType.equals(CustomField.FIELD_DATA_TYPE_TIME_RANGE)) {
                            obj = 9;
                            break;
                        }
                        break;
                    case 1178922291:
                        if (fieldDataType.equals("organization")) {
                            obj = 4;
                            break;
                        }
                        break;
                }
                switch (obj) {
                    case null:
                        fragment = new CustomFieldEditMultipleOptionsFragment();
                        break;
                    case 1:
                    case 2:
                        fragment = new CustomFieldEditSingleOptionFragment();
                        break;
                    case 3:
                    case 4:
                        fragment = new CustomFieldEditPersonOrgFragment();
                        break;
                    case 5:
                        fragment = new CustomFieldEditMonetaryFragment();
                        break;
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                        fragment = new CustomFieldEditDateAndTimeRangeFragment();
                        break;
                    case 10:
                        fragment = new CustomFieldDoubleFragment();
                        break;
                    default:
                        fragment = new CustomFieldEditTextFragment();
                        break;
                }
            }
            fragment = new CustomFieldEditTextFragment();
            fragment.setArguments(params);
            return fragment;
        }
        throw new AssertionError();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mCustomField = (CustomField) getArguments().getParcelable(ARG_CUSTOM_FIELD);
        this.mOriginalCustomField = new CustomField(this.mCustomField);
        this.mItemSqlId = getArguments().getLong(ARG_ITEM_SQL_ID);
    }

    public boolean isChangesMadeToFieldsAfterInitialLoad() {
        return hasFieldChanged();
    }

    public boolean isAllRequiredFieldsSet() {
        return true;
    }

    public void onCreateOrUpdate() {
        CustomFieldSavingTask savingTask = new CustomFieldSavingTask(PipedriveApp.getActiveSession(), new OnTaskFinished<CustomField>() {
            public void taskFinished(Session session, TaskResult taskResult, CustomField... params) {
                if (CustomFieldBaseFragment.this.getActivity() != null) {
                    CustomFieldBaseFragment.this.getActivity().finish();
                }
            }
        });
        savingTask.setParentId(this.mItemSqlId);
        savingTask.execute(new CustomField[]{this.mCustomField});
    }

    public void createOrUpdateDiscardedAsRequiredFieldsAreNotSet() {
    }

    public void onCancel() {
        if (getActivity() != null) {
            getActivity().finish();
        }
    }

    private boolean hasFieldChanged() {
        saveContent();
        if (this.mOriginalCustomField == null || this.mCustomField == null) {
            return true;
        }
        boolean customFieldMainValueHasChanged = !TextUtils.equals(this.mOriginalCustomField.getTempValue(), this.mCustomField.getTempValue());
        boolean customFieldSecondaryValueHasChanged;
        if (TextUtils.equals(this.mOriginalCustomField.getSecondaryTempValue(), this.mCustomField.getSecondaryTempValue())) {
            customFieldSecondaryValueHasChanged = false;
        } else {
            customFieldSecondaryValueHasChanged = true;
        }
        if (customFieldMainValueHasChanged || customFieldSecondaryValueHasChanged) {
            return true;
        }
        return false;
    }

    protected boolean contentExists() {
        return (this.mCustomField == null || TextUtils.isEmpty(this.mCustomField.getTempValue())) ? false : true;
    }

    void onClear() {
        ViewUtil.showDeleteConfirmationDialog(getActivity(), getResources().getString(R.string.dialog_clear_value), new Runnable() {
            public void run() {
                CustomFieldBaseFragment.this.clearContent();
                CustomFieldBaseFragment.this.saveContent();
                CustomFieldBaseFragment.this.onCreateOrUpdate();
            }
        }, getResources().getString(R.string.dialog_clear_positive_button_label));
    }

    @Nullable
    public List<? extends NavigatorMenuItem> getAdditionalButtons() {
        return Arrays.asList(new NavigatorMenuItem[]{this.mClearMenuItem, this.mDiscardMenuItem});
    }
}
