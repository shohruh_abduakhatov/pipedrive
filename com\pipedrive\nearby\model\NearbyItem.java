package com.pipedrive.nearby.model;

import android.location.Location;
import android.support.annotation.NonNull;
import com.google.android.gms.maps.model.LatLng;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.BaseDataSource;
import com.pipedrive.model.Activity;
import com.pipedrive.model.BaseDatasourceEntity;
import java.util.List;
import rx.Observable;

public abstract class NearbyItem<ENTITY extends BaseDatasourceEntity> {
    @NonNull
    private String address;
    @NonNull
    private NearbyItemImplementation<ENTITY> itemImplementation;
    @NonNull
    private Double latitude;
    @NonNull
    private Double longitude;

    @NonNull
    protected abstract String getActivitiesColumnForNearbyItem();

    @NonNull
    protected abstract BaseDataSource<ENTITY> getDataSource(@NonNull Session session);

    private NearbyItem(@NonNull Double latitude, @NonNull Double longitude, @NonNull String address) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
    }

    protected NearbyItem(@NonNull Double latitude, @NonNull Double longitude, @NonNull String address, @NonNull Long singleItemSqlId) {
        this(latitude, longitude, address);
        this.itemImplementation = new SingleNearbyItemImplementation(singleItemSqlId);
    }

    protected NearbyItem(@NonNull Double latitude, @NonNull Double longitude, @NonNull String address, @NonNull List<Long> multipleItemSqlIds) {
        this(latitude, longitude, address);
        if (multipleItemSqlIds.size() == 1) {
            this.itemImplementation = new SingleNearbyItemImplementation((Long) multipleItemSqlIds.get(0));
        } else {
            this.itemImplementation = new MultipleNearbyItemsImplementation(multipleItemSqlIds);
        }
    }

    @NonNull
    public final Observable<Activity> getLastActivity(@NonNull Session session) {
        return this.itemImplementation.getLastActivity(session, getActivitiesColumnForNearbyItem());
    }

    @NonNull
    public final Observable<Activity> getNextActivity(@NonNull Session session) {
        return this.itemImplementation.getNextActivity(session, getActivitiesColumnForNearbyItem());
    }

    @NonNull
    public final Observable<ENTITY> getData(@NonNull Session session) {
        return this.itemImplementation.getData(getDataSource(session));
    }

    @NonNull
    public final Observable<Long> getItemSqlIds() {
        return this.itemImplementation.getItemSqlIds();
    }

    @NonNull
    public final Boolean isAggregated() {
        return this.itemImplementation.hasMultipleIds();
    }

    @NonNull
    public final Double getLatitude() {
        return this.latitude;
    }

    @NonNull
    public final Double getLongitude() {
        return this.longitude;
    }

    @NonNull
    public final LatLng getItemLatLng() {
        return new LatLng(this.latitude.doubleValue(), this.longitude.doubleValue());
    }

    @NonNull
    public final Double getDistanceTo(@NonNull Double latitudeTo, @NonNull Double longitudeTo) {
        float[] distanceResults = new float[3];
        Location.distanceBetween(latitudeTo.doubleValue(), longitudeTo.doubleValue(), this.latitude.doubleValue(), this.longitude.doubleValue(), distanceResults);
        return Double.valueOf(Float.valueOf(distanceResults[0]).doubleValue());
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NearbyItem<?> that = (NearbyItem) o;
        if (this.latitude.equals(that.latitude) && this.longitude.equals(that.longitude) && this.address.equals(that.address)) {
            return this.itemImplementation.equals(that.itemImplementation);
        }
        return false;
    }

    public int hashCode() {
        return (((((this.latitude.hashCode() * 31) + this.longitude.hashCode()) * 31) + this.address.hashCode()) * 31) + this.itemImplementation.hashCode();
    }

    @NonNull
    public String getAddress() {
        return this.address;
    }
}
