package com.pipedrive.util;

import android.text.TextUtils;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datetime.DateFormatHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.Contact;
import com.pipedrive.model.Person;
import com.pipedrive.model.contacts.PageOfContacts;
import com.pipedrive.model.contacts.PageOfOrganizations;
import com.pipedrive.model.contacts.PageOfPersons;
import com.pipedrive.model.pagination.Pagination;
import com.pipedrive.store.StorePerson;
import com.pipedrive.util.networking.Response;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public enum ContactsUtil {
    ;
    
    private static final int PAGE_LIMIT = 500;
    private static final String TAG = null;

    static {
        TAG = ContactsUtil.class.getSimpleName();
    }

    public static void downloadPersons(Session session) {
        downloadContacts(session, session.getContactsListUpdateTime(Long.MIN_VALUE) == Long.MIN_VALUE, new PageOfPersons());
    }

    public static void downloadOrganizations(Session session) {
        downloadContacts(session, session.getOrganizationsListUpdateTime(Long.MIN_VALUE) == Long.MIN_VALUE, new PageOfOrganizations());
    }

    private static void downloadContacts(Session session, boolean firstDownload, PageOfContacts pageOfContacts) {
        pageOfContacts.start = 0;
        pageOfContacts.moreItemsInCollection = false;
        do {
            if (firstDownload) {
                pageOfContacts = downloadPageOfContacts(session, pageOfContacts.start, 0, true, pageOfContacts);
            } else {
                pageOfContacts = downloadPageOfContacts(session, pageOfContacts.start, pageOfContacts.newestUpdateTimeReadFromContactsQuery, firstDownload, pageOfContacts);
            }
            pageOfContacts.start = pageOfContacts.nextStart;
            pageOfContacts.nextStart = Integer.MIN_VALUE;
        } while (pageOfContacts.moreItemsInCollection);
    }

    private static SimpleDateFormat fullDateFormat2() {
        return new SimpleDateFormat(DateFormatHelper.DATE_FORMAT_LONG);
    }

    private static PageOfContacts downloadPageOfContacts(Session session, int start, long newestUpdateTimeReadFromContactsQuery, boolean firstDownload, PageOfContacts pageOfContacts) {
        pageOfContacts.start = start;
        pageOfContacts.nextStart = 0;
        pageOfContacts.moreItemsInCollection = false;
        pageOfContacts.limit = 500;
        List<Contact> contacts = readPageOfContactsFromAPI(session, pageOfContacts, firstDownload).getAllContacts();
        pageOfContacts.persistAllContacts(session);
        long pageLastUpdateTimeReadFromContactsToSave = session.getContactsListUpdateTime(0);
        int contactsSize = contacts.size();
        for (int i = 0; i < contactsSize; i++) {
            if (contacts.get(i) != null) {
                long pageLastUpdateTimeReadFromContacts = -1;
                if (!TextUtils.isEmpty(((Contact) contacts.get(i)).getUpdateTime())) {
                    try {
                        pageLastUpdateTimeReadFromContacts = fullDateFormat2().parse(((Contact) contacts.get(i)).getUpdateTime()).getTime();
                    } catch (ParseException e) {
                        LogJourno.reportEvent(EVENT.NotSpecified_contactUpdateTimeParsingFailed, String.format("pageLastUpdateTimeReadFromContacts:[%s]", new Object[]{jsonUpdateTimeToParse}), e);
                        Log.e(e);
                    }
                } else if (!TextUtils.isEmpty(((Contact) contacts.get(i)).getAddTime())) {
                    try {
                        pageLastUpdateTimeReadFromContacts = fullDateFormat2().parse(((Contact) contacts.get(i)).getAddTime()).getTime();
                    } catch (ParseException e2) {
                        LogJourno.reportEvent(EVENT.NotSpecified_contactAddTimeParsingFailed, String.format("pageLastUpdateTimeReadFromContacts:[%s]", new Object[]{jsonUpdateTimeToParse}), e2);
                        Log.e(e2);
                    }
                }
                if (pageLastUpdateTimeReadFromContacts == -1) {
                    continue;
                } else if (firstDownload) {
                    pageLastUpdateTimeReadFromContactsToSave = Math.max(pageLastUpdateTimeReadFromContacts, pageLastUpdateTimeReadFromContactsToSave);
                } else if (pageLastUpdateTimeReadFromContacts <= pageOfContacts.getNewestUpdateTimeReadFromContactsQuery(session, 0)) {
                    pageOfContacts.moreItemsInCollection = false;
                    pageOfContacts.saveNewestUpdateTimeReadFromContactsQuery(session, newestUpdateTimeReadFromContactsQuery);
                    break;
                } else {
                    newestUpdateTimeReadFromContactsQuery = Math.max(pageLastUpdateTimeReadFromContacts, newestUpdateTimeReadFromContactsQuery);
                    pageOfContacts.newestUpdateTimeReadFromContactsQuery = newestUpdateTimeReadFromContactsQuery;
                }
            }
        }
        pageOfContacts.saveNewestUpdateTimeReadFromContactsQuery(session, pageLastUpdateTimeReadFromContactsToSave);
        pageOfContacts.pageOfContactsHaveBeenLoaded(session, pageOfContacts.moreItemsInCollection);
        return pageOfContacts;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static PageOfContacts readPageOfContactsFromAPI(Session session, PageOfContacts pageOfContacts, boolean firstDownload) {
        String tag = TAG + ".readPageOfContactsFromAPI()";
        pageOfContacts.deleteAllContacts();
        Log.d(tag, String.format("Get the contact stream. start: %s; firstDownload: %s", new Object[]{Integer.valueOf(pageOfContacts.start), Boolean.valueOf(firstDownload)}));
        InputStream in = pageOfContacts.getPageOfContactsInputStream(session, pageOfContacts.start, pageOfContacts.limit, firstDownload);
        if (in == null) {
            LogJourno.reportEvent(EVENT.Networking_downloadPageOfPersonsFailed, String.format("InputStream:[%s] pageOfContacts:[%s]", new Object[]{in, pageOfContacts}));
            Log.e(new Throwable(String.format("Contacts list stream returned null! Cannot download mPersons! start: %s; firstDownload: %s", new Object[]{Integer.valueOf(pageOfContacts.start), Boolean.valueOf(firstDownload)})));
            pageOfContacts.moreItemsInCollection = false;
        } else {
            Log.d(tag, "Start contacs downloading. start: " + pageOfContacts.start);
            JsonReader reader = new JsonReader(new InputStreamReader(in, HttpRequest.CHARSET_UTF8));
            reader.beginObject();
            while (reader.hasNext()) {
                String contactsListField = reader.nextName();
                if (!Response.JSON_PARAM_SUCCESS.equals(contactsListField)) {
                    try {
                        if (Response.JSON_PARAM_DATA.equals(contactsListField) && reader.peek() == JsonToken.BEGIN_ARRAY) {
                            reader.beginArray();
                            while (reader.hasNext()) {
                                pageOfContacts.readAndSaveOneContact(session, reader);
                            }
                            reader.endArray();
                        } else if (Response.JSON_PARAM_ADDITIONAL_DATA.equals(contactsListField) && reader.peek() == JsonToken.BEGIN_OBJECT) {
                            reader.beginObject();
                            while (reader.hasNext()) {
                                if (Pagination.JSON_PARAM_OF_PAGINATION.equals(reader.nextName())) {
                                    reader.beginObject();
                                    while (reader.hasNext()) {
                                        String paginationField = reader.nextName();
                                        if ("next_start".equals(paginationField)) {
                                            pageOfContacts.nextStart = reader.nextInt();
                                        } else if ("more_items_in_collection".equals(paginationField)) {
                                            pageOfContacts.moreItemsInCollection = reader.nextBoolean();
                                        } else {
                                            reader.skipValue();
                                        }
                                    }
                                    reader.endObject();
                                } else {
                                    reader.skipValue();
                                }
                            }
                            reader.endObject();
                        } else {
                            reader.skipValue();
                        }
                    } catch (Exception e) {
                        LogJourno.reportEvent(EVENT.Networking_downloadPageOfPersonsFailed, String.format("pageOfContacts:[%s]", new Object[]{pageOfContacts}), e);
                        Log.e(new Throwable("Error downloading mPersons!", e));
                    } catch (Throwable th) {
                        try {
                            in.close();
                        } catch (Exception e2) {
                        }
                    }
                } else if (!reader.nextBoolean()) {
                    reader.close();
                }
            }
            reader.endObject();
            reader.close();
            try {
                in.close();
            } catch (Exception e3) {
            }
        }
        return pageOfContacts;
    }

    public static boolean saveContact(Person person, Session session) {
        boolean storePersonOk = false;
        if (!StringUtils.isTrimmedAndEmpty(person.getName())) {
            if (person.isStored()) {
                storePersonOk = new StorePerson(session).update(person);
            } else {
                storePersonOk = new StorePerson(session).create(person);
            }
        }
        if (!storePersonOk) {
            ViewUtil.showErrorToast(session.getApplicationContext(), (int) R.string.error_contact_save_failed);
        }
        return storePersonOk;
    }
}
