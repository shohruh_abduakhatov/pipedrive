package com.crashlytics.android.answers.shim;

import android.util.Log;

public class AnswersOptionalLogger {
    private static final String TAG = "AnswersOptionalLogger";
    private static final KitEventLogger logger;

    static {
        KitEventLogger theLogger = null;
        try {
            theLogger = AnswersKitEventLogger.create();
        } catch (NoClassDefFoundError e) {
        } catch (IllegalStateException e2) {
        } catch (Throwable t) {
            Log.w(TAG, "Unexpected error creating AnswersKitEventLogger", t);
        }
        if (theLogger == null) {
            theLogger = NoopKitEventLogger.create();
        }
        logger = theLogger;
    }

    public static KitEventLogger get() {
        return logger;
    }
}
