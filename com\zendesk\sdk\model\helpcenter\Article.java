package com.zendesk.sdk.model.helpcenter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.logger.Logger;
import com.zendesk.util.CollectionUtils;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Article implements Serializable {
    private static final String LOG_TAG = "Article";
    public static final int UNKNOWN_VOTE_COUNT = -1;
    private User author;
    private Long authorId;
    private String body;
    private boolean commentsDisabled;
    private Date createdAt;
    private boolean draft;
    private String htmlUrl;
    private Long id;
    private List<String> labelNames;
    private String locale;
    private boolean outdated;
    private Long sectionId;
    private String sourceLocale;
    private String title;
    private Date updatedAt;
    private String url;
    private Integer voteCount;
    private Integer voteSum;

    public Long getId() {
        return this.id;
    }

    @Nullable
    public String getUrl() {
        return this.url;
    }

    @Nullable
    public String getHtmlUrl() {
        return this.htmlUrl;
    }

    @Nullable
    public Long getAuthorId() {
        return this.authorId;
    }

    public boolean isCommentsDisabled() {
        return this.commentsDisabled;
    }

    @NonNull
    public List<String> getLabelNames() {
        return CollectionUtils.copyOf(this.labelNames);
    }

    public boolean isDraft() {
        return this.draft;
    }

    public int getVoteSum() {
        return this.voteSum == null ? 0 : this.voteSum.intValue();
    }

    public int getVoteCount() {
        return this.voteCount == null ? 0 : this.voteCount.intValue();
    }

    @Nullable
    public Long getSectionId() {
        return this.sectionId;
    }

    @Nullable
    public Date getCreatedAt() {
        if (this.createdAt == null) {
            return null;
        }
        return new Date(this.createdAt.getTime());
    }

    @Nullable
    public Date getUpdatedAt() {
        if (this.updatedAt == null) {
            return null;
        }
        return new Date(this.updatedAt.getTime());
    }

    @Nullable
    public String getTitle() {
        return this.title;
    }

    @Nullable
    public String getBody() {
        return this.body;
    }

    @Nullable
    public String getSourceLocale() {
        return this.sourceLocale;
    }

    @Nullable
    public String getLocale() {
        return this.locale;
    }

    public boolean isOutdated() {
        return this.outdated;
    }

    @Nullable
    public User getAuthor() {
        return this.author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public int getUpvoteCount() {
        if (this.voteSum != null && this.voteCount != null) {
            return (this.voteSum.intValue() + this.voteCount.intValue()) / 2;
        }
        Logger.e(LOG_TAG, "Cannot determine vote count because vote_sum and / or vote_count are null", new Object[0]);
        return -1;
    }

    public int getDownvoteCount() {
        if (this.voteSum != null && this.voteCount != null) {
            return Math.abs(this.voteSum.intValue() - this.voteCount.intValue()) / 2;
        }
        Logger.e(LOG_TAG, "Cannot determine vote count because vote_sum and / or vote_count are null", new Object[0]);
        return -1;
    }
}
