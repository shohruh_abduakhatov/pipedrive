package com.google.android.gms.auth.api.credentials.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.support.annotation.Nullable;
import com.google.android.gms.auth.api.Auth.AuthCredentialsOptions;
import com.google.android.gms.auth.api.credentials.internal.zzk.zza;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;

public final class zzg extends zzj<zzk> {
    @Nullable
    private final AuthCredentialsOptions iV;

    public zzg(Context context, Looper looper, zzf com_google_android_gms_common_internal_zzf, AuthCredentialsOptions authCredentialsOptions, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 68, com_google_android_gms_common_internal_zzf, connectionCallbacks, onConnectionFailedListener);
        this.iV = authCredentialsOptions;
    }

    protected Bundle zzahv() {
        return this.iV == null ? new Bundle() : this.iV.zzahv();
    }

    AuthCredentialsOptions zzaim() {
        return this.iV;
    }

    protected zzk zzce(IBinder iBinder) {
        return zza.zzcg(iBinder);
    }

    protected /* synthetic */ IInterface zzh(IBinder iBinder) {
        return zzce(iBinder);
    }

    protected String zzjx() {
        return "com.google.android.gms.auth.api.credentials.service.START";
    }

    protected String zzjy() {
        return "com.google.android.gms.auth.api.credentials.internal.ICredentialsService";
    }
}
