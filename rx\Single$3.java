package rx;

import rx.functions.Func2;
import rx.functions.FuncN;

class Single$3 implements FuncN<R> {
    final /* synthetic */ Func2 val$zipFunction;

    Single$3(Func2 func2) {
        this.val$zipFunction = func2;
    }

    public R call(Object... args) {
        return this.val$zipFunction.call(args[0], args[1]);
    }
}
