package com.zendesk.sdk.ui;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.ConnectivityManager.NetworkCallback;
import android.net.Network;
import android.net.NetworkRequest.Builder;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.background.ApplicationStateMonitor;
import com.newrelic.agent.android.instrumentation.Instrumented;
import com.newrelic.agent.android.tracing.TraceMachine;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.sdk.network.NetworkAware;
import com.zendesk.sdk.network.Retryable;
import com.zendesk.sdk.util.NetworkUtils;
import com.zendesk.util.StringUtils;

@Instrumented
public abstract class NetworkAwareActionbarActivity extends AppCompatActivity implements NetworkAware, Retryable, TraceFieldInterface {
    private static final String LOG_TAG = NetworkAwareActionbarActivity.class.getSimpleName();
    private BroadcastReceiver mBroadcastReceiver;
    protected boolean mNetworkAvailable;
    private NetworkCallback mNetworkCallback;
    private boolean mNetworkPreviouslyUnavailable;
    private View mNoNetworkView;
    private View mRetryView;

    class NetworkAvailabilityBroadcastReceiver extends BroadcastReceiver {
        private final String LOG_TAG = NetworkAvailabilityBroadcastReceiver.class.getSimpleName();

        NetworkAvailabilityBroadcastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent == null || !"android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
                Logger.w(this.LOG_TAG, "onReceive: intent was null or getAction() was mismatched", new Object[0]);
            } else if (intent.getBooleanExtra("noConnectivity", false)) {
                NetworkAwareActionbarActivity.this.onNetworkUnavailable();
            } else {
                NetworkAwareActionbarActivity.this.onNetworkAvailable();
            }
        }
    }

    protected void onStart() {
        super.onStart();
        ApplicationStateMonitor.getInstance().activityStarted();
    }

    protected void onStop() {
        super.onStop();
        ApplicationStateMonitor.getInstance().activityStopped();
    }

    public void onNetworkAvailable() {
        Logger.d(LOG_TAG, "Network is available.", new Object[0]);
        this.mNetworkAvailable = true;
        if (findNoNetworkView() == null) {
            Logger.w(LOG_TAG, "The no network view is null, nothing to do.", new Object[0]);
        } else if (this.mNetworkPreviouslyUnavailable) {
            this.mNetworkPreviouslyUnavailable = false;
            Animation slideOut = AnimationUtils.loadAnimation(this, R.anim.info_slide_out);
            slideOut.setAnimationListener(new AnimationListenerAdapter() {
                public void onAnimationEnd(Animation animation) {
                    NetworkAwareActionbarActivity.this.findNoNetworkView().setVisibility(8);
                }
            });
            findNoNetworkView().startAnimation(slideOut);
        } else {
            Logger.d(LOG_TAG, "Network was not previously unavailable, no need to perform animation", new Object[0]);
        }
    }

    public void onNetworkUnavailable() {
        Logger.d(LOG_TAG, "Network is unavailable.", new Object[0]);
        this.mNetworkAvailable = false;
        this.mNetworkPreviouslyUnavailable = true;
        if (findRetryView() != null) {
            Logger.d(LOG_TAG, "No network: ensuring that the retry view is gone.", new Object[0]);
            findRetryView().setVisibility(8);
        }
        if (findNoNetworkView() == null) {
            Logger.w(LOG_TAG, "No network: cannot show view because it is missing.", new Object[0]);
            return;
        }
        Animation slideIn = AnimationUtils.loadAnimation(this, R.anim.info_slide_in);
        slideIn.setAnimationListener(new AnimationListenerAdapter() {
            public void onAnimationStart(Animation animation) {
                NetworkAwareActionbarActivity.this.findNoNetworkView().setVisibility(0);
            }
        });
        findNoNetworkView().startAnimation(slideIn);
    }

    protected void onCreate(Bundle bundle) {
        TraceMachine.startTracing("NetworkAwareActionbarActivity");
        try {
            TraceMachine.enterMethod(this._nr_trace, "NetworkAwareActionbarActivity#onCreate", null);
        } catch (NoSuchFieldError e) {
            while (true) {
                TraceMachine.enterMethod(null, "NetworkAwareActionbarActivity#onCreate", null);
            }
        }
        super.onCreate(bundle);
        this.mNetworkAvailable = NetworkUtils.isConnected(this);
        TraceMachine.exitMethod();
    }

    protected void onResume() {
        super.onResume();
        registerForNetworkCallbacks();
    }

    protected void onPause() {
        super.onPause();
        unregisterForNetworkCallbacks();
    }

    public void onRetryAvailable(String message, OnClickListener retryAction) {
        setRetryViewState(true, message, retryAction);
    }

    public void onRetryUnavailable() {
        setRetryViewState(false, null, null);
    }

    @TargetApi(21)
    private void registerForNetworkCallbacks() {
        if (VERSION.SDK_INT < 21) {
            Logger.d(LOG_TAG, "Adding pre-Lollipop network callbacks...", new Object[0]);
            this.mBroadcastReceiver = new NetworkAvailabilityBroadcastReceiver();
            registerReceiver(this.mBroadcastReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            return;
        }
        Logger.d(LOG_TAG, "Adding Lollipop network callbacks...", new Object[0]);
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService("connectivity");
        final Handler handler = new Handler(Looper.myLooper());
        this.mNetworkCallback = new NetworkCallback() {
            public void onAvailable(Network network) {
                handler.post(new Runnable() {
                    public void run() {
                        NetworkAwareActionbarActivity.this.onNetworkAvailable();
                    }
                });
            }

            public void onLost(Network network) {
                handler.post(new Runnable() {
                    public void run() {
                        NetworkAwareActionbarActivity.this.onNetworkUnavailable();
                    }
                });
            }
        };
        connectivityManager.registerNetworkCallback(new Builder().build(), this.mNetworkCallback);
    }

    @TargetApi(21)
    private void unregisterForNetworkCallbacks() {
        if (this.mBroadcastReceiver != null) {
            unregisterReceiver(this.mBroadcastReceiver);
        }
        if (VERSION.SDK_INT >= 21 && this.mNetworkCallback != null) {
            ((ConnectivityManager) getSystemService("connectivity")).unregisterNetworkCallback(this.mNetworkCallback);
        }
    }

    private void setRetryViewState(boolean shouldShow, String message, OnClickListener retryAction) {
        if (!this.mNetworkAvailable) {
            Logger.e(LOG_TAG, "Network is not available, will not alter retry view state", new Object[0]);
        } else if (findRetryView() == null) {
            Logger.e(LOG_TAG, "No retry view was found, will not alter view state", new Object[0]);
        } else {
            if (shouldShow) {
                View retryButton = findViewById(R.id.retry_view_button);
                if (retryButton != null) {
                    retryButton.setOnClickListener(retryAction);
                }
                TextView retryText = (TextView) findViewById(R.id.retry_view_text);
                if (retryText != null) {
                    retryText.setText(message);
                }
                if (StringUtils.isEmpty(message)) {
                    Logger.e(LOG_TAG, "Slide in requested but we have no message or action to perform", new Object[0]);
                    return;
                }
                Logger.d(LOG_TAG, "Sliding in retry view...", new Object[0]);
                Animation slideIn = AnimationUtils.loadAnimation(this, R.anim.info_slide_in);
                slideIn.setAnimationListener(new AnimationListenerAdapter() {
                    public void onAnimationStart(Animation animation) {
                        NetworkAwareActionbarActivity.this.findRetryView().setVisibility(0);
                    }
                });
                findRetryView().startAnimation(slideIn);
            }
            if (!shouldShow && findRetryView().getVisibility() == 0) {
                Logger.d(LOG_TAG, "Sliding out retry view...", new Object[0]);
                Animation slideOut = AnimationUtils.loadAnimation(this, R.anim.info_slide_out);
                slideOut.setAnimationListener(new AnimationListenerAdapter() {
                    public void onAnimationEnd(Animation animation) {
                        NetworkAwareActionbarActivity.this.findRetryView().setVisibility(8);
                    }
                });
                findRetryView().startAnimation(slideOut);
            }
        }
    }

    private View findRetryView() {
        if (this.mRetryView == null) {
            this.mRetryView = findViewById(R.id.retry_view_container);
        }
        return this.mRetryView;
    }

    private View findNoNetworkView() {
        if (this.mNoNetworkView == null) {
            this.mNoNetworkView = findViewById(R.id.activity_network_no_connectivity);
        }
        return this.mNoNetworkView;
    }
}
