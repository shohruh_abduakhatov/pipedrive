package com.pipedrive.application;

import android.support.annotation.NonNull;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.datasource.PipeSQLiteOpenHelper;
import com.pipedrive.logging.Log;

class DBConnectorForSession extends DBConnector {
    private static final String TAG = DBConnectorForSession.class.getSimpleName();
    @NonNull
    private final Session mSession;

    DBConnectorForSession(@NonNull Session session) {
        this.mSession = session;
    }

    @NonNull
    protected PipeSQLiteOpenHelper getSQLiteOpenHelper() {
        Log.i(TAG + ".getSQLiteOpenHelper()", "Initializing PipeSQLiteHelper...");
        return new PipeSQLiteHelper(this.mSession);
    }
}
