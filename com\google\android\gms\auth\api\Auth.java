package com.google.android.gms.auth.api;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.auth.api.credentials.CredentialsApi;
import com.google.android.gms.auth.api.credentials.PasswordSpecification;
import com.google.android.gms.auth.api.credentials.internal.zze;
import com.google.android.gms.auth.api.credentials.internal.zzg;
import com.google.android.gms.auth.api.proxy.ProxyApi;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.internal.zzc;
import com.google.android.gms.auth.api.signin.internal.zzd;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.common.api.Api.ApiOptions.Optional;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzf;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.internal.zzns;
import com.google.android.gms.internal.zznt;
import com.google.android.gms.internal.zznu;
import com.google.android.gms.internal.zzoc;
import java.util.Collections;
import java.util.List;

public final class Auth {
    public static final Api<AuthCredentialsOptions> CREDENTIALS_API = new Api("Auth.CREDENTIALS_API", ia, hX);
    public static final CredentialsApi CredentialsApi = new zze();
    public static final Api<GoogleSignInOptions> GOOGLE_SIGN_IN_API = new Api("Auth.GOOGLE_SIGN_IN_API", ic, hZ);
    public static final GoogleSignInApi GoogleSignInApi = new zzc();
    public static final Api<zzb> PROXY_API = zza.API;
    public static final ProxyApi ProxyApi = new zzoc();
    public static final zzf<zzg> hX = new zzf();
    public static final zzf<zznu> hY = new zzf();
    public static final zzf<zzd> hZ = new zzf();
    private static final zza<zzg, AuthCredentialsOptions> ia = new zza<zzg, AuthCredentialsOptions>() {
        public zzg zza(Context context, Looper looper, com.google.android.gms.common.internal.zzf com_google_android_gms_common_internal_zzf, AuthCredentialsOptions authCredentialsOptions, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            return new zzg(context, looper, com_google_android_gms_common_internal_zzf, authCredentialsOptions, connectionCallbacks, onConnectionFailedListener);
        }
    };
    private static final zza<zznu, NoOptions> ib = new zza<zznu, NoOptions>() {
        public /* synthetic */ Api.zze zza(Context context, Looper looper, com.google.android.gms.common.internal.zzf com_google_android_gms_common_internal_zzf, Object obj, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            return zzd(context, looper, com_google_android_gms_common_internal_zzf, (NoOptions) obj, connectionCallbacks, onConnectionFailedListener);
        }

        public zznu zzd(Context context, Looper looper, com.google.android.gms.common.internal.zzf com_google_android_gms_common_internal_zzf, NoOptions noOptions, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            return new zznu(context, looper, com_google_android_gms_common_internal_zzf, connectionCallbacks, onConnectionFailedListener);
        }
    };
    private static final zza<zzd, GoogleSignInOptions> ic = new zza<zzd, GoogleSignInOptions>() {
        public zzd zza(Context context, Looper looper, com.google.android.gms.common.internal.zzf com_google_android_gms_common_internal_zzf, @Nullable GoogleSignInOptions googleSignInOptions, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            return new zzd(context, looper, com_google_android_gms_common_internal_zzf, googleSignInOptions, connectionCallbacks, onConnectionFailedListener);
        }

        public List<Scope> zza(@Nullable GoogleSignInOptions googleSignInOptions) {
            return googleSignInOptions == null ? Collections.emptyList() : googleSignInOptions.zzait();
        }

        public /* synthetic */ List zzp(@Nullable Object obj) {
            return zza((GoogleSignInOptions) obj);
        }
    };
    public static final Api<NoOptions> ie = new Api("Auth.ACCOUNT_STATUS_API", ib, hY);
    public static final zzns if = new zznt();

    public static final class AuthCredentialsOptions implements Optional {
        private final String ig;
        private final PasswordSpecification ih;

        public static class Builder {
            @NonNull
            private PasswordSpecification ih = PasswordSpecification.iG;
        }

        public Bundle zzahv() {
            Bundle bundle = new Bundle();
            bundle.putString("consumer_package", this.ig);
            bundle.putParcelable("password_specification", this.ih);
            return bundle;
        }

        public PasswordSpecification zzaid() {
            return this.ih;
        }
    }

    private Auth() {
    }
}
