package com.pipedrive.model.delta;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ModelProperty {
    Class<?> dataType() default NONE.class;

    String jsonParamName() default "";
}
