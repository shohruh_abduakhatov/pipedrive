package com.pipedrive.util.networking.entities.dealstage;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.pipedrive.gson.GsonHelper;
import com.pipedrive.model.DealStage;

public enum DealStageParser {
    ;

    @Nullable
    public static DealStage getStageFromReader(@NonNull JsonReader reader) {
        JsonElement data = new JsonParser().parse(reader);
        if (data.isJsonObject()) {
            return getDealStageFromEntity((DealStageEntity) GsonHelper.fromJSON(data, DealStageEntity.class));
        }
        return null;
    }

    @Nullable
    public static DealStage getDealStageFromEntity(@NonNull DealStageEntity entity) {
        if (entity.getPipedriveId() == null || entity.getName() == null || entity.getName().isEmpty() || entity.getDealProbability() == null || entity.getOrder() == null || entity.getPipelineId() == null) {
            return null;
        }
        DealStage stage = new DealStage();
        stage.setName(entity.getName());
        stage.setPipedriveId(entity.getPipedriveId());
        stage.setOrderNr(entity.getOrder().intValue());
        stage.setDealProbability(entity.getDealProbability().intValue());
        stage.setPipelineId(entity.getPipelineId().intValue());
        return stage;
    }
}
