package com.pipedrive.loaders;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader.ForceLoadContentObserver;
import com.pipedrive.application.Session;
import com.pipedrive.contentproviders.DealsContentProvider;
import com.pipedrive.datasource.CurrenciesDataSource;
import com.pipedrive.logging.Log;
import com.pipedrive.model.Currency;
import com.pipedrive.model.Deal;
import com.pipedrive.tasks.CurrencyTaskResult;

public class DealsValueLoader extends AsyncTaskLoader<CurrencyTaskResult> {
    private int mDealsGroupId;
    private CurrencyTaskResult mDealsSum;
    private final ForceLoadContentObserver mObserver = new ForceLoadContentObserver();
    private String[] mProjection;
    private String mSelection;
    private String[] mSelectionArgs;
    private Session mSession;
    private String mSortOrder;
    private Uri mUri;

    public CurrencyTaskResult loadInBackground() {
        CurrencyTaskResult result = new CurrencyTaskResult();
        this.mUri = new DealsContentProvider(this.mSession).attachSessionIdToUri_hackMethodToEnableCPUriBuildOutsideCP_BAD_DESIGN_AND_USAGE(this.mUri);
        Cursor cursor = getContext().getContentResolver().query(this.mUri, this.mProjection, this.mSelection, this.mSelectionArgs, this.mSortOrder);
        if (cursor != null) {
            try {
                result = calculateValue(cursor, this.mDealsGroupId, this.mSession);
                cursor.registerContentObserver(this.mObserver);
            } catch (RuntimeException ex) {
                throw ex;
            } finally {
                cursor.close();
            }
        }
        return result;
    }

    public void deliverResult(CurrencyTaskResult result) {
        if (!isReset()) {
            this.mDealsSum = result;
            if (isStarted()) {
                super.deliverResult(result);
            }
        }
    }

    public DealsValueLoader(Context context, Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder, Session session, int dealsGroupId) {
        super(context);
        this.mSession = session;
        this.mUri = uri;
        this.mProjection = projection;
        this.mSelection = selection;
        this.mSelectionArgs = selectionArgs;
        this.mSortOrder = sortOrder;
        this.mDealsGroupId = dealsGroupId;
    }

    protected void onStartLoading() {
        if (this.mDealsSum != null) {
            deliverResult(this.mDealsSum);
        }
        forceLoad();
    }

    protected void onStopLoading() {
        cancelLoad();
    }

    protected void onReset() {
        super.onReset();
        onStopLoading();
        this.mDealsSum = null;
    }

    public String[] getProjection() {
        return this.mProjection;
    }

    public void setProjection(String[] projection) {
        this.mProjection = projection;
    }

    public String getSelection() {
        return this.mSelection;
    }

    public void setSelection(String selection) {
        this.mSelection = selection;
    }

    public String[] getSelectionArgs() {
        return this.mSelectionArgs;
    }

    public void setSelectionArgs(String[] selectionArgs) {
        this.mSelectionArgs = selectionArgs;
    }

    public String getSortOrder() {
        return this.mSortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.mSortOrder = sortOrder;
    }

    private CurrencyTaskResult calculateValue(Cursor cursor, int dealsGroupId, Session session) {
        CurrencyTaskResult result = new CurrencyTaskResult();
        double stageValue = CurrencyTaskResult.UNKNOWN_VALUE;
        int numberOfDealsCalculated = 0;
        result.setDealsGroupId(dealsGroupId);
        if (!(cursor == null || cursor.isClosed())) {
            String defaultCurrencyCode = session.getUserSettingsDefaultCurrencyCode();
            CurrenciesDataSource ds = new CurrenciesDataSource(session.getDatabase());
            Currency defaultCurrency = ds.getCurrencyByCode(defaultCurrencyCode);
            if (defaultCurrency != null) {
                result.setDefaultCurrency(defaultCurrency);
                int dealsCount = cursor.getCount();
                int j = 0;
                while (j < dealsCount) {
                    try {
                        if (cursor.isClosed() || !cursor.moveToPosition(j)) {
                            break;
                        }
                        Deal deal = DealsContentProvider.dealsTableCursorToDeal(session.getDatabase(), cursor);
                        if (stageValue == CurrencyTaskResult.UNKNOWN_VALUE) {
                            stageValue = 0.0d;
                        }
                        Currency dealCurrency = ds.getCurrencyByCode(deal.getCurrencyCode());
                        if (defaultCurrency.isCustom()) {
                            if (defaultCurrency.isCustom() && defaultCurrency.getCode().equalsIgnoreCase(deal.getCurrencyCode())) {
                                stageValue += deal.getValue();
                                numberOfDealsCalculated++;
                            }
                        } else if (!(dealCurrency == null || dealCurrency.isCustom())) {
                            stageValue += getConvertedValue(deal.getValue(), dealCurrency);
                            numberOfDealsCalculated++;
                        }
                        j++;
                    } catch (Exception e) {
                        Log.e(new Throwable("Error calculating deals sum", e));
                    }
                }
            }
            result.setNumberOfDealsCalculated(numberOfDealsCalculated);
            result.setDealsGroupValue(stageValue);
        }
        return result;
    }

    private double getConvertedValue(double value, Currency currency) {
        if (currency != null) {
            return value / currency.getRateToDefault();
        }
        return 0.0d;
    }
}
