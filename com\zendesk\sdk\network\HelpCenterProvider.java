package com.zendesk.sdk.network;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.sdk.model.helpcenter.Article;
import com.zendesk.sdk.model.helpcenter.ArticleVote;
import com.zendesk.sdk.model.helpcenter.Attachment;
import com.zendesk.sdk.model.helpcenter.AttachmentType;
import com.zendesk.sdk.model.helpcenter.Category;
import com.zendesk.sdk.model.helpcenter.FlatArticle;
import com.zendesk.sdk.model.helpcenter.HelpCenterSearch;
import com.zendesk.sdk.model.helpcenter.ListArticleQuery;
import com.zendesk.sdk.model.helpcenter.SearchArticle;
import com.zendesk.sdk.model.helpcenter.Section;
import com.zendesk.sdk.model.helpcenter.SuggestedArticleResponse;
import com.zendesk.sdk.model.helpcenter.SuggestedArticleSearch;
import com.zendesk.sdk.model.helpcenter.help.HelpItem;
import com.zendesk.sdk.model.helpcenter.help.HelpRequest;
import com.zendesk.service.ZendeskCallback;
import java.util.List;
import java.util.Locale;

public interface HelpCenterProvider {
    void deleteVote(@NonNull Long l, @Nullable ZendeskCallback<Void> zendeskCallback);

    void downvoteArticle(@NonNull Long l, @Nullable ZendeskCallback<ArticleVote> zendeskCallback);

    void getArticle(@NonNull Long l, @Nullable ZendeskCallback<Article> zendeskCallback);

    void getArticles(@NonNull Long l, @Nullable ZendeskCallback<List<Article>> zendeskCallback);

    void getAttachments(@NonNull Long l, @NonNull AttachmentType attachmentType, @Nullable ZendeskCallback<List<Attachment>> zendeskCallback);

    void getCategories(@Nullable ZendeskCallback<List<Category>> zendeskCallback);

    void getCategory(@NonNull Long l, @Nullable ZendeskCallback<Category> zendeskCallback);

    void getHelp(@NonNull HelpRequest helpRequest, @Nullable ZendeskCallback<List<HelpItem>> zendeskCallback);

    void getSection(@NonNull Long l, @Nullable ZendeskCallback<Section> zendeskCallback);

    void getSections(@NonNull Long l, @Nullable ZendeskCallback<List<Section>> zendeskCallback);

    void getSuggestedArticles(@NonNull SuggestedArticleSearch suggestedArticleSearch, @Nullable ZendeskCallback<SuggestedArticleResponse> zendeskCallback);

    void listArticles(@NonNull ListArticleQuery listArticleQuery, @Nullable ZendeskCallback<List<SearchArticle>> zendeskCallback);

    void listArticlesFlat(@NonNull ListArticleQuery listArticleQuery, @Nullable ZendeskCallback<List<FlatArticle>> zendeskCallback);

    void searchArticles(@NonNull HelpCenterSearch helpCenterSearch, @Nullable ZendeskCallback<List<SearchArticle>> zendeskCallback);

    void submitRecordArticleView(@NonNull Long l, @NonNull Locale locale, @Nullable ZendeskCallback<Void> zendeskCallback);

    void upvoteArticle(@NonNull Long l, @Nullable ZendeskCallback<ArticleVote> zendeskCallback);
}
