package com.zendesk.sdk.model;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import com.zendesk.logger.Logger;

public class AppVersion {
    private static final String LOG_TAG = AppVersion.class.getSimpleName();
    private int mAppVersionCode = 0;
    private String mAppVersionName = "";

    public AppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            this.mAppVersionName = packageInfo.versionName;
            this.mAppVersionCode = packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            Logger.e(LOG_TAG, "Unable to find the package name", e, new Object[0]);
        }
    }

    public String getAppVersionName() {
        return this.mAppVersionName;
    }

    public int getAppVersionCode() {
        return this.mAppVersionCode;
    }
}
