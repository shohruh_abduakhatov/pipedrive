package com.zendesk.sdk.model.access;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public enum AuthenticationType {
    JWT("jwt"),
    ANONYMOUS("anonymous");
    
    private String authenticationType;

    private AuthenticationType(String authenticationType) {
        this.authenticationType = authenticationType;
    }

    @Nullable
    public static AuthenticationType getAuthType(String authenticationType) {
        if (JWT.authenticationType.equals(authenticationType)) {
            return JWT;
        }
        if (ANONYMOUS.authenticationType.equals(authenticationType)) {
            return ANONYMOUS;
        }
        return null;
    }

    @NonNull
    public String getAuthenticationType() {
        return this.authenticationType;
    }
}
