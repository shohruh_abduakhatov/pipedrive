package com.zendesk.service;

import com.zendesk.util.CollectionUtils;
import java.util.ArrayList;
import java.util.List;

public class ErrorResponseAdapter implements ErrorResponse {
    private final String errorMessage;

    public ErrorResponseAdapter() {
        this("");
    }

    public ErrorResponseAdapter(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isNetworkError() {
        return false;
    }

    public boolean isConversionError() {
        return false;
    }

    public boolean isHTTPError() {
        return false;
    }

    public String getReason() {
        return this.errorMessage;
    }

    public int getStatus() {
        return -1;
    }

    public String getUrl() {
        return "";
    }

    public String getResponseBody() {
        return this.errorMessage;
    }

    public String getResponseBodyType() {
        return "text/plain; charset=UTF8";
    }

    public List<Header> getResponseHeaders() {
        return CollectionUtils.unmodifiableList(new ArrayList());
    }
}
