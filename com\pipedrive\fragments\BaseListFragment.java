package com.pipedrive.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import com.pipedrive.R;
import com.pipedrive.datasource.search.SearchConstraint;

public class BaseListFragment extends BaseFragment implements OnItemClickListener {
    private TextView mEmptyView;
    private ListView mListView;
    @NonNull
    SearchConstraint mSearchConstraint = SearchConstraint.EMPTY;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    ListView getListView() {
        return this.mListView;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        this.mListView = (ListView) view.findViewById(R.id.list);
        this.mEmptyView = (TextView) view.findViewById(R.id.empty);
        this.mListView.setEmptyView(this.mEmptyView);
        this.mListView.setOnItemClickListener(this);
    }

    final void setEmptyText(int stringResId) {
        this.mEmptyView.setText(stringResId);
    }

    public void onItemClick(@NonNull AdapterView<?> adapterView, @NonNull View view, int position, long id) {
    }

    public void listFilter(@NonNull SearchConstraint searchConstraint) {
    }
}
