package com.google.android.gms.maps;

import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.dynamic.zzd;

public final class CameraUpdate {
    private final zzd anN;

    CameraUpdate(zzd com_google_android_gms_dynamic_zzd) {
        this.anN = (zzd) zzaa.zzy(com_google_android_gms_dynamic_zzd);
    }

    public zzd zzbsc() {
        return this.anN;
    }
}
