package com.pipedrive.nearby.filter.filterlist;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.filter.FilterItemList;
import java.util.ArrayList;
import java.util.List;

public class NearbyFilterListAdapter extends Adapter<NearbyFilterListViewHolder> {
    @NonNull
    private List<FilterItemList> list = new ArrayList();
    @NonNull
    private final Session session;

    public NearbyFilterListAdapter(@NonNull List<FilterItemList> list, @NonNull Session session) {
        this.list = list;
        this.session = session;
    }

    public NearbyFilterListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NearbyFilterListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_filter_item_list, parent, false));
    }

    public void onBindViewHolder(NearbyFilterListViewHolder holder, int position) {
        holder.bind((FilterItemList) this.list.get(position), this.session);
    }

    public int getItemCount() {
        return this.list.size();
    }
}
