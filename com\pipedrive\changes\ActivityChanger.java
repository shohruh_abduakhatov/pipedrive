package com.pipedrive.changes;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.changes.Change.ChangeOperationType;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.Activity;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;

public class ActivityChanger extends Changer<Activity> {
    public ActivityChanger(Session session) {
        super(session, 40, null);
    }

    protected ActivityChanger(Changer parentChanger) {
        super(parentChanger.getSession(), 40, parentChanger);
    }

    @Nullable
    protected Long getChangeMetaData(@NonNull Activity activity, @NonNull ChangeOperationType changeOperationType) {
        return Long.valueOf(activity.getSqlId());
    }

    @NonNull
    protected ChangeResult createChange(@NonNull Activity newActivity) {
        if (newActivity.isExisting() || newActivity.isStored()) {
            LogJourno.reportEvent(EVENT.ChangesChanger_createChangeRequestedWithExistingChange, ActivityChanger.class.getSimpleName());
            Log.e(new Throwable("I can only handle new Activities!"));
            return ChangeResult.FAILED;
        }
        createAndRelateNewOrgChangeIfNewOrgExists(newActivity);
        createAndRelateNewPersonChangeIfNewPersonExists(newActivity);
        createAndRelateNewDealChangeIfNewDealExists(newActivity);
        long newActivitySQLid = new ActivitiesDataSource(getSession()).createOrUpdate((BaseDatasourceEntity) newActivity);
        if (newActivitySQLid == -1) {
            return ChangeResult.FAILED;
        }
        newActivity.setSqlId(newActivitySQLid);
        return ChangeResult.SUCCESSFUL;
    }

    @NonNull
    protected ChangeResult updateChange(@NonNull Activity existingActivity) {
        ActivitiesDataSource activitiesDataSource = new ActivitiesDataSource(getSession());
        if (!existingActivity.isStored() || activitiesDataSource.findBySqlId(existingActivity.getSqlId()) == null) {
            LogJourno.reportEvent(EVENT.ChangesChanger_updateChangeRequestedWithNewChange, ActivityChanger.class.getSimpleName());
            Log.e(new Throwable("I can only handle existing Activities!"));
            return ChangeResult.FAILED;
        }
        createAndRelateNewOrgChangeIfNewOrgExists(existingActivity);
        createAndRelateNewPersonChangeIfNewPersonExists(existingActivity);
        createAndRelateNewDealChangeIfNewDealExists(existingActivity);
        if (existingActivity.getDeal() != null && existingActivity.getDeal().getSqlId() == 0) {
            LogJourno.reportEvent(EVENT.ChangesChanger_updateActivityCalledWithNewDeal, String.format("Deal:[%s]", new Object[]{existingActivity.getDeal()}));
            Log.e(new Throwable(String.format("Activity update change does not support association with new Deal (%s)! Expected behaviour?", new Object[]{existingActivity.getDeal()})));
        }
        return activitiesDataSource.createOrUpdate((BaseDatasourceEntity) existingActivity) == -1 ? ChangeResult.FAILED : ChangeResult.SUCCESSFUL;
    }

    private void createAndRelateNewPersonChangeIfNewPersonExists(Activity activity) {
        Person activityPerson = activity.getPerson();
        if (activityPerson != null && !activityPerson.isStored() && !new PersonChanger((Changer) this).create(activityPerson)) {
            activity.setPerson(null);
        }
    }

    private void createAndRelateNewOrgChangeIfNewOrgExists(Activity activity) {
        Organization activityOrganization = activity.getOrganization();
        if (activityOrganization != null && !activityOrganization.isStored() && !new OrganizationChanger((Changer) this).create(activityOrganization)) {
            activity.setOrganization(null);
        }
    }

    private void createAndRelateNewDealChangeIfNewDealExists(@NonNull Activity activity) {
        boolean newDealAssociatedWithActivity;
        boolean unrelateDealAsItsCreationFailed = true;
        Deal activityDeal = activity.getDeal();
        if (activityDeal == null || activityDeal.isStored()) {
            newDealAssociatedWithActivity = false;
        } else {
            newDealAssociatedWithActivity = true;
        }
        if (newDealAssociatedWithActivity) {
            if (new DealChanger((Changer) this).create(activityDeal)) {
                unrelateDealAsItsCreationFailed = false;
            }
            if (unrelateDealAsItsCreationFailed) {
                activity.setDeal(null);
            }
        }
    }
}
