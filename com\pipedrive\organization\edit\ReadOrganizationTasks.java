package com.pipedrive.organization.edit;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.model.Organization;
import com.pipedrive.tasks.AsyncTask;

public class ReadOrganizationTasks extends AsyncTask<Long, Void, Organization> {
    @Nullable
    private final OnTaskFinished mOnTaskFinished;

    @MainThread
    interface OnTaskFinished {
        void onOrganizationRead(@Nullable Organization organization);
    }

    public ReadOrganizationTasks(@NonNull Session session, @Nullable OnTaskFinished onTaskFinished) {
        super(session);
        this.mOnTaskFinished = onTaskFinished;
    }

    protected Organization doInBackground(Long... params) {
        return (Organization) new OrganizationsDataSource(getSession().getDatabase()).findBySqlId(params[0].longValue());
    }

    protected void onPostExecute(@Nullable Organization organization) {
        if (this.mOnTaskFinished != null) {
            this.mOnTaskFinished.onOrganizationRead(organization);
        }
    }
}
