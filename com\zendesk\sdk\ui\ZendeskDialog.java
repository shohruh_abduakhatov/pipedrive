package com.zendesk.sdk.ui;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.background.ApplicationStateMonitor;
import com.newrelic.agent.android.instrumentation.Instrumented;
import com.newrelic.agent.android.tracing.TraceMachine;
import com.zendesk.sdk.util.UiUtils;
import java.io.Serializable;

@Instrumented
public class ZendeskDialog extends DialogFragment implements Serializable, TraceFieldInterface {
    protected void onStop() {
        super.onStop();
        ApplicationStateMonitor.getInstance().activityStopped();
    }

    public void onCreate(Bundle bundle) {
        TraceMachine.startTracing("ZendeskDialog");
        try {
            TraceMachine.enterMethod(this._nr_trace, "ZendeskDialog#onCreate", null);
        } catch (NoSuchFieldError e) {
            while (true) {
                TraceMachine.enterMethod(null, "ZendeskDialog#onCreate", null);
            }
        }
        super.onCreate(bundle);
        setStyle(1, getTheme());
        TraceMachine.exitMethod();
    }

    public void onStart() {
        ApplicationStateMonitor.getInstance().activityStarted();
        super.onStart();
        UiUtils.sizeDialogWidthForTablets(getDialog(), 0.66f);
    }
}
