package com.pipedrive.sync;

import com.pipedrive.application.Session;
import com.pipedrive.tasks.AsyncTask;
import com.pipedrive.tasks.products.ReDownloadAllProductsTask;

class SyncManager$10 extends AsyncTask<Void, Void, Void> {
    final /* synthetic */ SyncManager this$0;

    SyncManager$10(SyncManager this$0, Session session) {
        this.this$0 = this$0;
        super(session);
    }

    protected Void doInBackground(Void... params) {
        if (getSession().isCompanyFeatureProductsEnabled(false)) {
            new ReDownloadAllProductsTask(getSession()).reDownloadAllProducts();
        }
        return null;
    }
}
