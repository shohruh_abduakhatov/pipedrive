package com.pipedrive.util.deals;

import android.content.Intent;
import android.support.annotation.Nullable;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.contentproviders.DealsContentProvider;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datetime.DateFormatHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.model.Activity;
import com.pipedrive.model.Deal;
import com.pipedrive.store.StoreDeal;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.util.time.TimeManager;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class DealsUtil extends DealsNetworkingUtil {
    private static final String TAG = DealsUtil.class.getSimpleName();

    private static class ActivityComparator implements Comparator<Activity> {
        private ActivityComparator() {
        }

        public int compare(Activity a, Activity b) {
            if (a == null || a.getActivityStart() == null) {
                return 1;
            }
            if (b == null || b.getActivityStart() == null) {
                return -1;
            }
            return a.getActivityStart().compareTo(b.getActivityStart());
        }
    }

    @Nullable
    public static Deal recalculateDealNextActivity(Session session, long dealSqlId, boolean saveInDB) {
        String tag = TAG + ".recalculateDealNextActivity()";
        DealsDataSource dealsDataSource = new DealsDataSource(session.getDatabase());
        Deal deal = (Deal) dealsDataSource.findBySqlId(dealSqlId);
        if (deal == null) {
            Log.d(tag, String.format("Deal for SQLid %s not found! Cannot recalculate Deal's next activity. Expected behaviour?", new Object[]{Long.valueOf(dealSqlId)}));
            return null;
        }
        Log.d(tag, String.format("Working on deal: %s", new Object[]{deal}));
        List<Activity> dealActivities = new ActivitiesDataSource(session).getActiveUndoneActivitiesByDealSqlId(dealSqlId);
        if (dealActivities.isEmpty()) {
            Log.d(tag, "All deal activities are done. No next activities for the Deal.");
            deal.setNextActivityDate(null, false);
            deal.setUndoneActivitiesCount(0);
        } else {
            Log.d(tag, String.format("Deal has %s undone activities. Lets find the one that is the earliest.", new Object[]{Integer.valueOf(dealActivities.size())}));
            Collections.sort(dealActivities, new ActivityComparator());
            Activity nextActivity = (Activity) dealActivities.get(0);
            Log.d(tag, String.format("Next activity for the Deal is calculated to be: %s", new Object[]{nextActivity}));
            deal.setNextActivityDateStr(nextActivity.getActivityStart() == null ? null : nextActivity.getActivityStart().getRepresentationInUtcForApiLongRepresentation());
            deal.setNextActivityTime(nextActivity.getDueTime() == null ? null : nextActivity.getDueTime().getRepresentationForApiShortRepresentation());
            deal.setUndoneActivitiesCount(dealActivities.size());
        }
        if (saveInDB) {
            Log.d(tag, String.format("Updating DB with deal new Deal data: %s.", new Object[]{deal}));
            dealsDataSource.createOrUpdate(deal);
        }
        String str = "Next Activity for the Deal is calculated and %s into the DB. Returning new Deal: %s.";
        Object[] objArr = new Object[2];
        objArr[0] = saveInDB ? "saved" : "not saved";
        objArr[1] = deal;
        Log.d(tag, String.format(str, objArr));
        Log.d(tag, String.format("Broadcasting %s to notify about the Deal next activity was recalculated.", new Object[]{"com.pipedrive.util.deals.DealsNetworkingUtil.ACTION_DEAL_NEXT_ACTIVITY_RECALCULATED"}));
        session.getApplicationContext().sendBroadcast(new Intent("com.pipedrive.util.deals.DealsNetworkingUtil.ACTION_DEAL_NEXT_ACTIVITY_RECALCULATED"));
        return deal;
    }

    public static boolean isDealUnderCurrentlySelectedPipeline(Session session, int dealPipedriveId) {
        return isDealUnderCurrentlySelectedPipeline(session, new DealsDataSource(session.getDatabase()).findDealByDealId((long) dealPipedriveId));
    }

    public static boolean isDealUnderCurrentlySelectedPipeline(Session session, Deal deal) {
        long selectedPipelineId = session.getPipelineSelectedPipelineId(-1);
        if (deal == null || selectedPipelineId == -1 || ((long) deal.getPipelineId()) == selectedPipelineId) {
            return true;
        }
        return false;
    }

    public static boolean saveDeal(Deal deal, Session session) {
        boolean storeDealOk;
        StoreDeal storeDeal = new StoreDeal(session);
        deal.setUpdateTime(DateFormatHelper.fullDateFormat2UTC().format(new Date(TimeManager.getInstance().currentTimeMillis().longValue())));
        if (deal.isStored()) {
            storeDealOk = storeDeal.update(deal);
        } else {
            deal.setAddTime(DateFormatHelper.fullDateFormat2UTC().format(new Date()));
            storeDealOk = storeDeal.create(deal);
        }
        if (storeDealOk) {
            session.getApplicationContext().getContentResolver().notifyChange(new DealsContentProvider(session).createAllDealsUri(), null);
        } else {
            ViewUtil.showErrorToast(session.getApplicationContext(), (int) R.string.error_deal_save_failed);
        }
        return storeDealOk;
    }
}
