package com.pipedrive.views.fab;

public interface FabMenuClickListener {
    void onMenuItemClicked(int i);
}
