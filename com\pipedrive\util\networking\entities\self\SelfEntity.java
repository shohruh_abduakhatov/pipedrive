package com.pipedrive.util.networking.entities.self;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.pipedrive.model.UserSettingsPipelineFilter;
import com.pipedrive.model.delta.ObjectDelta;
import java.util.List;

public class SelfEntity {
    private static final Integer IS_ADMIN_DEFAULT_VALUE = Integer.valueOf(0);
    private static final Integer IS_ADMIN_TRUE = Integer.valueOf(1);
    private static final String PARAM_COMPANIES = "companies";
    private static final String PARAM_CURRENT_USER_SETTINGS = "current_user_settings";
    private static final String PARAM_DEFAULT_CURRENCY = "default_currency";
    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_IS_ADMIN = "is_admin";
    private static final String PARAM_LANGUAGE = "language";
    private static final String PARAM_LOCALE = "locale";
    private static final String PARAM_NAME = "name";
    public static final String URL_NESTED_PARAM_LIST = ":(current_company_features:(products,price_variations,product_duration),current_user_settings,companies,default_currency,locale,language,is_admin,name,email)";
    @JsonAdapter(CompaniesTypeAdapter.class)
    @Nullable
    @SerializedName("companies")
    @Expose
    private List<CompanyEntity> companies;
    @JsonAdapter(CurrentUserSettingsTypeAdapter.class)
    @Nullable
    @SerializedName("current_user_settings")
    @Expose
    private CurrentUserSettings currentUserSettings;
    @Nullable
    @SerializedName("default_currency")
    @Expose
    private String defaultCurrency;
    @Nullable
    @SerializedName("locale")
    @Expose
    private String defaultLocale;
    @Nullable
    @SerializedName("email")
    @Expose
    private String email;
    @Nullable
    @SerializedName("is_admin")
    @Expose
    private Integer isAdmin = IS_ADMIN_DEFAULT_VALUE;
    @Nullable
    @SerializedName("language")
    @Expose
    private LanguageEntity languageEntity;
    @Nullable
    @SerializedName("current_company_features")
    @Expose
    private CurrentCompanyFeaturesEntity mCurrentCompanyFeaturesEntity;
    @Nullable
    @SerializedName("name")
    @Expose
    private String name;

    @Nullable
    public String getDefaultCurrency() {
        return this.defaultCurrency;
    }

    @Nullable
    public final Long getCurrentPipelineId() {
        return this.currentUserSettings == null ? null : this.currentUserSettings.getCurrentPipelineId();
    }

    @Nullable
    public List<UserSettingsPipelineFilter> getPipelineFilters() {
        return this.currentUserSettings == null ? null : this.currentUserSettings.getPipelineFilters();
    }

    @Nullable
    public Integer getDefaultVisibilityOrganization() {
        return this.currentUserSettings == null ? null : this.currentUserSettings.getDefaultVisibilityOrganization();
    }

    @Nullable
    public Integer getDefaultVisibilityPerson() {
        return this.currentUserSettings == null ? null : this.currentUserSettings.getDefaultVisibilityPerson();
    }

    @Nullable
    public Integer getDefaultVisibilityDeal() {
        return this.currentUserSettings == null ? null : this.currentUserSettings.getDefaultVisibilityDeal();
    }

    @Nullable
    public Integer getDefaultVisibilityProduct() {
        return this.currentUserSettings == null ? null : this.currentUserSettings.getDefaultVisibilityProduct();
    }

    @NonNull
    public Boolean canChangeVisibilityOfItems() {
        return this.currentUserSettings == null ? CurrentUserSettings.CAN_CHANGE_VISIBILITY_OF_ITEMS_DEFAULT_VALUE : this.currentUserSettings.canChangeVisibilityOfItems();
    }

    @NonNull
    public Boolean canDeleteDeals() {
        return this.currentUserSettings == null ? CurrentUserSettings.CAN_DELETE_DEALS_DEFAULT_VALUE : this.currentUserSettings.canDeleteDeals();
    }

    @Nullable
    public String getDefaultLanguageCode() {
        return this.languageEntity == null ? null : this.languageEntity.getDefaultLanguageCode();
    }

    @Nullable
    public String getDefaultLanguageCountry() {
        return this.languageEntity == null ? null : this.languageEntity.getDefaultLanguageCountry();
    }

    @Nullable
    public String getDefaultLocale() {
        return this.defaultLocale;
    }

    @NonNull
    public Boolean isAdmin() {
        boolean z = this.isAdmin == null || ObjectDelta.areEqual(this.isAdmin, IS_ADMIN_TRUE);
        return Boolean.valueOf(z);
    }

    @Nullable
    public String getName() {
        return this.name;
    }

    @Nullable
    public String getEmail() {
        return this.email;
    }

    @Nullable
    public List<CompanyEntity> getCompanies() {
        return this.companies;
    }

    @Nullable
    public CurrentCompanyFeaturesEntity getCurrentCompanyFeaturesEntity() {
        return this.mCurrentCompanyFeaturesEntity;
    }

    public String toString() {
        return "SelfEntity{pipelineFilters=" + getPipelineFilters() + ", defaultCurrency='" + this.defaultCurrency + '\'' + ", currentPipelineId=" + getCurrentPipelineId() + ", defaultVisibilityOrganization=" + getDefaultVisibilityOrganization() + ", defaultVisibilityPerson=" + getDefaultVisibilityPerson() + ", defaultVisibilityDeal=" + getDefaultVisibilityDeal() + ", defaultVisibilityProduct=" + getDefaultVisibilityProduct() + ", defaultLanguageCode='" + getDefaultLanguageCode() + '\'' + ", defaultLanguageCountry='" + getDefaultLanguageCountry() + '\'' + ", defaultLocale='" + this.defaultLocale + '\'' + ", name='" + this.name + '\'' + ", email='" + this.email + '\'' + ", companies=" + this.companies + ", mCanChangeVisibilityOfItems=" + canChangeVisibilityOfItems() + ", mCanDeleteDeals=" + canDeleteDeals() + ", isAdmin=" + isAdmin() + ", mCurrentCompanyFeaturesEntity=" + getCurrentCompanyFeaturesEntity() + '}';
    }
}
