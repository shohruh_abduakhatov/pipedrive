package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.List;

public class zzm implements Creator<LocationSettingsRequest> {
    static void zza(LocationSettingsRequest locationSettingsRequest, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zzc(parcel, 1, locationSettingsRequest.zzbgh(), false);
        zzb.zza(parcel, 2, locationSettingsRequest.zzbqc());
        zzb.zza(parcel, 3, locationSettingsRequest.zzbqd());
        zzb.zzc(parcel, 1000, locationSettingsRequest.getVersionCode());
        zzb.zzaj(parcel, zzcs);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzny(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzuu(i);
    }

    public LocationSettingsRequest zzny(Parcel parcel) {
        boolean z = false;
        int zzcr = zza.zzcr(parcel);
        List list = null;
        boolean z2 = false;
        int i = 0;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    list = zza.zzc(parcel, zzcq, LocationRequest.CREATOR);
                    break;
                case 2:
                    z2 = zza.zzc(parcel, zzcq);
                    break;
                case 3:
                    z = zza.zzc(parcel, zzcq);
                    break;
                case 1000:
                    i = zza.zzg(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new LocationSettingsRequest(i, list, z2, z);
        }
        throw new zza.zza("Overread allowed size end=" + zzcr, parcel);
    }

    public LocationSettingsRequest[] zzuu(int i) {
        return new LocationSettingsRequest[i];
    }
}
