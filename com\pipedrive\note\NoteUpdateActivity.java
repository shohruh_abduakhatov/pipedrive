package com.pipedrive.note;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.model.notes.Note;
import com.pipedrive.navigator.Navigator.Type;
import java.util.List;

public class NoteUpdateActivity extends NoteEditorActivity {
    private static final String ARG_UPDATE_NOTE_NOTE_SQL_ID = "com.pipedrive.note.NoteUpdateActivity.ARG_UPDATE_NOTE_NOTE_SQL_ID";

    public /* bridge */ /* synthetic */ void createOrUpdateDiscardedAsRequiredFieldsAreNotSet() {
        super.createOrUpdateDiscardedAsRequiredFieldsAreNotSet();
    }

    @Nullable
    public /* bridge */ /* synthetic */ List getAdditionalButtons() {
        return super.getAdditionalButtons();
    }

    public /* bridge */ /* synthetic */ void onCancel() {
        super.onCancel();
    }

    public /* bridge */ /* synthetic */ void onCreateOrUpdate() {
        super.onCreateOrUpdate();
    }

    public /* bridge */ /* synthetic */ void onNoteCreated(boolean z) {
        super.onNoteCreated(z);
    }

    public /* bridge */ /* synthetic */ void onNoteDeleted(boolean z) {
        super.onNoteDeleted(z);
    }

    public /* bridge */ /* synthetic */ void onNoteUpdated(boolean z) {
        super.onNoteUpdated(z);
    }

    public /* bridge */ /* synthetic */ void onResume() {
        super.onResume();
    }

    @MainThread
    public static void updateNote(@NonNull Context context, @Nullable Note note) {
        boolean cannotUpdateNote = note == null || !note.isStored();
        if (!cannotUpdateNote) {
            updateNote(context, note.getSqlId());
        }
    }

    public static void updateNote(@NonNull Context context, long sqlId) {
        context.startActivity(new Intent(context, NoteUpdateActivity.class).putExtra(ARG_UPDATE_NOTE_NOTE_SQL_ID, sqlId));
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavigatorType(Type.UPDATE);
    }

    protected boolean requestNote(@Nullable Bundle args) {
        if (args == null) {
            return false;
        }
        ((NoteEditorPresenter) this.mPresenter).requestNote(args.getLong(ARG_UPDATE_NOTE_NOTE_SQL_ID));
        return true;
    }
}
