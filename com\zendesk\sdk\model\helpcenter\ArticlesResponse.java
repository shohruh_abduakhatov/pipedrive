package com.zendesk.sdk.model.helpcenter;

import android.support.annotation.NonNull;
import java.util.List;

public interface ArticlesResponse {
    @NonNull
    List<Article> getArticles();

    @NonNull
    List<Category> getCategories();

    @NonNull
    List<Section> getSections();

    @NonNull
    List<User> getUsers();
}
