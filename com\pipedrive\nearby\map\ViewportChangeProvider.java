package com.pipedrive.nearby.map;

import android.support.annotation.NonNull;
import rx.Observable;

public interface ViewportChangeProvider {
    @NonNull
    Observable<Viewport> viewportChanges();
}
