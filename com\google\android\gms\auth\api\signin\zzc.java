package com.google.android.gms.auth.api.signin;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzc implements Creator<SignInAccount> {
    static void zza(SignInAccount signInAccount, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zzc(parcel, 1, signInAccount.versionCode);
        zzb.zza(parcel, 4, signInAccount.jg, false);
        zzb.zza(parcel, 7, signInAccount.zzaiz(), i, false);
        zzb.zza(parcel, 8, signInAccount.ck, false);
        zzb.zzaj(parcel, zzcs);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzax(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzdl(i);
    }

    public SignInAccount zzax(Parcel parcel) {
        int zzcr = zza.zzcr(parcel);
        int i = 0;
        String str = "";
        GoogleSignInAccount googleSignInAccount = null;
        String str2 = "";
        while (parcel.dataPosition() < zzcr) {
            GoogleSignInAccount googleSignInAccount2;
            String str3;
            int zzg;
            String str4;
            int zzcq = zza.zzcq(parcel);
            String str5;
            switch (zza.zzgu(zzcq)) {
                case 1:
                    str5 = str2;
                    googleSignInAccount2 = googleSignInAccount;
                    str3 = str;
                    zzg = zza.zzg(parcel, zzcq);
                    str4 = str5;
                    break;
                case 4:
                    zzg = i;
                    GoogleSignInAccount googleSignInAccount3 = googleSignInAccount;
                    str3 = zza.zzq(parcel, zzcq);
                    str4 = str2;
                    googleSignInAccount2 = googleSignInAccount3;
                    break;
                case 7:
                    str3 = str;
                    zzg = i;
                    str5 = str2;
                    googleSignInAccount2 = (GoogleSignInAccount) zza.zza(parcel, zzcq, GoogleSignInAccount.CREATOR);
                    str4 = str5;
                    break;
                case 8:
                    str4 = zza.zzq(parcel, zzcq);
                    googleSignInAccount2 = googleSignInAccount;
                    str3 = str;
                    zzg = i;
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    str4 = str2;
                    googleSignInAccount2 = googleSignInAccount;
                    str3 = str;
                    zzg = i;
                    break;
            }
            i = zzg;
            str = str3;
            googleSignInAccount = googleSignInAccount2;
            str2 = str4;
        }
        if (parcel.dataPosition() == zzcr) {
            return new SignInAccount(i, str, googleSignInAccount, str2);
        }
        throw new zza.zza("Overread allowed size end=" + zzcr, parcel);
    }

    public SignInAccount[] zzdl(int i) {
        return new SignInAccount[i];
    }
}
