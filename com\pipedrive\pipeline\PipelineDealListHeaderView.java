package com.pipedrive.pipeline;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.util.formatter.MonetaryFormatter;

public class PipelineDealListHeaderView extends FrameLayout {
    @BindView(2131821100)
    ProgressBar mProgressBar;
    @Nullable
    private Session mSession;
    @BindView(2131821099)
    TextView mSummary;
    @BindView(2131821098)
    TextView mTitle;

    public PipelineDealListHeaderView(Context context) {
        this(context, null);
    }

    public PipelineDealListHeaderView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PipelineDealListHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.row_pipeline_header, this, true);
        ButterKnife.bind(this);
    }

    public void init(@NonNull Session session, String stageName, @Nullable StageData stageData) {
        this.mSession = session;
        this.mTitle.setText(stageName);
        if (stageData == null) {
            clearSummaryData();
        } else {
            setSummaryData(stageData.getDealSumValue(), stageData.getNumberOfDealsCountedForSumValue());
        }
    }

    void setSummaryData(@Nullable Float dealSumValue, @Nullable Integer numberOfDealsCountedForSumValue) {
        boolean noInfoToShowStageSummaryData;
        int i;
        if (dealSumValue == null || numberOfDealsCountedForSumValue == null) {
            noInfoToShowStageSummaryData = true;
        } else {
            noInfoToShowStageSummaryData = false;
        }
        boolean noSessionToUse;
        if (this.mSession == null) {
            noSessionToUse = true;
        } else {
            noSessionToUse = false;
        }
        TextView textView = this.mSummary;
        if (noInfoToShowStageSummaryData) {
            i = 4;
        } else {
            i = 0;
        }
        textView.setVisibility(i);
        if (noInfoToShowStageSummaryData || noSessionToUse) {
            this.mSummary.setText(null);
            return;
        }
        String dealSum = new MonetaryFormatter(this.mSession).formatDealSum(dealSumValue);
        this.mSummary.setText(getContext().getResources().getQuantityString(R.plurals.deal_sum_and_count, numberOfDealsCountedForSumValue.intValue(), new Object[]{dealSum, numberOfDealsCountedForSumValue}));
    }

    public void setLoading(boolean loading) {
        this.mProgressBar.animate().cancel();
        if (loading && this.mProgressBar.getVisibility() == 8) {
            this.mProgressBar.setAlpha(0.0f);
            this.mProgressBar.setScaleX(0.0f);
            this.mProgressBar.setScaleY(0.0f);
            this.mProgressBar.setVisibility(0);
            this.mProgressBar.animate().alpha(1.0f).scaleX(1.0f).scaleY(1.0f).setListener(null);
        } else if (!loading && this.mProgressBar.getVisibility() == 0) {
            this.mProgressBar.animate().alpha(0.0f).scaleY(0.0f).scaleX(0.0f).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animation) {
                    PipelineDealListHeaderView.this.mProgressBar.setVisibility(8);
                }
            });
        }
    }

    public void clearSummaryData() {
        this.mSummary.setText(null);
    }
}
