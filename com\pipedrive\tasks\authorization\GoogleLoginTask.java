package com.pipedrive.tasks.authorization;

import android.app.Activity;
import android.support.annotation.Nullable;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.tasks.authorization.UserLoginTask.OnLoginListener;
import com.pipedrive.util.networking.HttpClientManager;
import okhttp3.Response;

public class GoogleLoginTask extends UserLoginTask {
    private static final String SERVER_CLIENT_ID_LIVE = "207967787620-a58qmlbt6v6k3chaculgdpteldvtiijf.apps.googleusercontent.com";
    private final GoogleApiClient mGoogleApiClient;

    public GoogleLoginTask(Activity callerActivity, OnLoginListener onLoginListener, OnEnoughDataToShowUI onEnoughDataToShowUI, GoogleApiClient googleApiClient) {
        super(callerActivity, onLoginListener, onEnoughDataToShowUI);
        this.mGoogleApiClient = googleApiClient;
    }

    protected Integer doInBackground(String... params) {
        boolean googleApiClientHasSomehowDisconnected;
        if (this.mGoogleApiClient.isConnected()) {
            googleApiClientHasSomehowDisconnected = false;
        } else {
            googleApiClientHasSomehowDisconnected = true;
        }
        if (googleApiClientHasSomehowDisconnected) {
            return Integer.valueOf(0);
        }
        return super.doInBackground(new String[]{Plus.AccountApi.getAccountName(this.mGoogleApiClient), "_dummy_data_"});
    }

    @Nullable
    protected String getLoginData(@Nullable String email, @Nullable String password) {
        String response = null;
        String scopes = "audience:server:client_id:207967787620-a58qmlbt6v6k3chaculgdpteldvtiijf.apps.googleusercontent.com";
        try {
            Response authorizationResponse = HttpClientManager.INSTANCE.authorizationGoogleAuth(this.mCallerActivity.getApplicationContext(), GoogleAuthUtil.getToken(this.mCallerActivity, Plus.AccountApi.getAccountName(this.mGoogleApiClient), "audience:server:client_id:207967787620-a58qmlbt6v6k3chaculgdpteldvtiijf.apps.googleusercontent.com"));
            boolean isAuthorized = (authorizationResponse == null || authorizationResponse.body() == null) ? false : true;
            if (isAuthorized) {
                response = authorizationResponse.body().string();
            }
        } catch (UserRecoverableAuthException e) {
            LogJourno.reportEvent(EVENT.Authorization_googleAuthUserRecoverableAuthException, e.getMessage());
            Log.e(new Throwable("Unable to get Google Auth code!", e));
            this.mCallerActivity.startActivityForResult(e.getIntent(), 1002);
        } catch (GoogleAuthException e2) {
            LogJourno.reportEvent(EVENT.Authorization_googleAuthGoogleAuthException, e2.getMessage());
            Log.e(new Throwable("Google Auth exception!", e2));
        } catch (Exception e3) {
            LogJourno.reportEvent(EVENT.Authorization_googleAuthException, e3.getMessage());
            Log.e(new Throwable("Unable to get Google Auth code!", e3));
        }
        return response;
    }
}
