package com.pipedrive.util.viewhelper;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build.VERSION;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.text.Html;
import android.util.JsonReader;
import android.util.JsonToken;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.pipedrive.logging.Log;
import com.pipedrive.util.StringUtils;
import java.io.IOException;
import java.io.StringReader;

public class HTMLContentEditableWebViewHelper {
    public static final String MIME_TYPE = "text/html; charset=UTF-8";
    private static final int WEB_VIEW_BG_COLOR_NOT_SET = Integer.MIN_VALUE;
    private final String HTML_CONTENT_ID;
    private final String JAVASCRIPT_INTERFACE_LINK;
    private final String getContentJS;
    private final String getFocusJS;
    private HTMLContentAccessor mHTMLContentAccessor;
    private int mWebViewBGColor;
    private WebView mWebViewToEdit;

    public interface OnWebViewContentAcquired {
        @MainThread
        void webViewContent(@NonNull String str);
    }

    public interface OnWebViewLoaded {
        void webViewLoaded(WebView webView);
    }

    public HTMLContentEditableWebViewHelper(WebView webView) {
        this(webView, Integer.MIN_VALUE);
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    public HTMLContentEditableWebViewHelper(WebView webView, int viewBGColor) {
        this.JAVASCRIPT_INTERFACE_LINK = "htmlcontent";
        this.HTML_CONTENT_ID = "htmlcontentdiv";
        this.mWebViewToEdit = null;
        this.mHTMLContentAccessor = null;
        this.mWebViewBGColor = Integer.MIN_VALUE;
        this.getContentJS = "document.getElementById('htmlcontentdiv').innerHTML";
        this.getFocusJS = "document.getElementById('htmlcontentdiv').focus()";
        if (webView != null) {
            this.mWebViewToEdit = webView;
            this.mWebViewBGColor = viewBGColor;
            if (this.mWebViewBGColor != Integer.MIN_VALUE) {
                this.mWebViewToEdit.setBackgroundColor(this.mWebViewBGColor);
            }
            this.mWebViewToEdit.getSettings().setJavaScriptEnabled(true);
            if (VERSION.SDK_INT < 21) {
                this.mHTMLContentAccessor = new HTMLContentAccessor(this, null);
                this.mWebViewToEdit.addJavascriptInterface(this.mHTMLContentAccessor, "htmlcontent");
            }
        }
    }

    public void loadData(String contentInHTML, final OnWebViewLoaded onWebViewLoadedCallback) {
        if (contentInHTML == null) {
            contentInHTML = "";
        }
        String webViewContent = "<div id=\"htmlcontentdiv\" contenteditable=\"true\" style=\"" + ("outline:none;" + (this.mWebViewBGColor != Integer.MIN_VALUE ? "background:#" + Integer.toHexString(this.mWebViewBGColor) + ";min-height:100%;" : "")) + "\">" + contentInHTML + "</div>";
        if (onWebViewLoadedCallback != null) {
            this.mWebViewToEdit.setWebViewClient(new WebViewClient() {
                public void onPageFinished(WebView view, String url) {
                    onWebViewLoadedCallback.webViewLoaded(view);
                }
            });
        }
        this.mWebViewToEdit.loadData(webViewContent, MIME_TYPE, null);
    }

    public void getFocus() {
        if (VERSION.SDK_INT >= 21) {
            getFocusAPI21AndAbove();
        } else {
            getFocusBelowAPI21();
        }
    }

    private void getFocusBelowAPI21() {
        this.mWebViewToEdit.loadUrl("javascript:document.getElementById('htmlcontentdiv').focus();");
    }

    @TargetApi(19)
    private void getFocusAPI21AndAbove() {
        this.mWebViewToEdit.evaluateJavascript("document.getElementById('htmlcontentdiv').focus()", null);
    }

    @MainThread
    public void getContent(@NonNull OnWebViewContentAcquired onWebViewContentAcquired, boolean ignoreEmptyContent) {
        if (VERSION.SDK_INT >= 21) {
            getWebViewContentAPI21AndAbove(onWebViewContentAcquired, ignoreEmptyContent);
        } else {
            getWebViewContentBelowAPI21(onWebViewContentAcquired, ignoreEmptyContent);
        }
    }

    @MainThread
    public void getContent(@NonNull OnWebViewContentAcquired onWebViewContentAcquired) {
        getContent(onWebViewContentAcquired, false);
    }

    private void getWebViewContentBelowAPI21(@NonNull final OnWebViewContentAcquired onWebViewContentAcquired, final boolean ignoreEmptyContent) {
        this.mHTMLContentAccessor.setOnWebViewContentAcquired(new OnWebViewContentAcquired() {
            public void webViewContent(@NonNull String contentInHTML) {
                boolean returnEmptyContent = ignoreEmptyContent && HTMLContentEditableWebViewHelper.this.isEmptyHtmlContent(contentInHTML);
                OnWebViewContentAcquired onWebViewContentAcquired = onWebViewContentAcquired;
                if (returnEmptyContent) {
                    contentInHTML = "";
                }
                onWebViewContentAcquired.webViewContent(contentInHTML);
            }
        });
        this.mWebViewToEdit.loadUrl("javascript:htmlcontent.contentInHTML(document.getElementById('htmlcontentdiv').innerHTML);");
    }

    @TargetApi(19)
    private void getWebViewContentAPI21AndAbove(@NonNull final OnWebViewContentAcquired onWebViewContentAcquired, final boolean ignoreEmptyContent) {
        this.mWebViewToEdit.evaluateJavascript("document.getElementById('htmlcontentdiv').innerHTML", new ValueCallback<String>() {
            public void onReceiveValue(String value) {
                IOException e;
                Throwable th;
                OnWebViewContentAcquired onWebViewContentAcquired;
                boolean returnEmptyContent = true;
                StringBuilder contentToReturn = new StringBuilder();
                JsonReader webViewContentReader = null;
                String contentToReturnInHtml;
                String str;
                if (value == null) {
                    if (webViewContentReader != null) {
                        try {
                            webViewContentReader.close();
                        } catch (Exception e2) {
                        }
                    }
                    contentToReturnInHtml = contentToReturn.toString();
                    if (!(ignoreEmptyContent && HTMLContentEditableWebViewHelper.this.isEmptyHtmlContent(contentToReturnInHtml))) {
                        returnEmptyContent = false;
                    }
                    OnWebViewContentAcquired onWebViewContentAcquired2 = onWebViewContentAcquired;
                    if (returnEmptyContent) {
                        str = "";
                    } else {
                        str = contentToReturnInHtml;
                    }
                    onWebViewContentAcquired2.webViewContent(str);
                    return;
                }
                try {
                    JsonReader webViewContentReader2 = new JsonReader(new StringReader(value));
                    try {
                        webViewContentReader2.setLenient(true);
                        if (webViewContentReader2.peek() != JsonToken.NULL) {
                            String webviewContent = webViewContentReader2.nextString();
                            if (webviewContent != null) {
                                contentToReturn.append(webviewContent);
                            }
                        }
                        if (webViewContentReader2 != null) {
                            try {
                                webViewContentReader2.close();
                            } catch (Exception e3) {
                            }
                        }
                        contentToReturnInHtml = contentToReturn.toString();
                        if (!(ignoreEmptyContent && HTMLContentEditableWebViewHelper.this.isEmptyHtmlContent(contentToReturnInHtml))) {
                            returnEmptyContent = false;
                        }
                        onWebViewContentAcquired2 = onWebViewContentAcquired;
                        if (returnEmptyContent) {
                            str = "";
                        } else {
                            str = contentToReturnInHtml;
                        }
                        onWebViewContentAcquired2.webViewContent(str);
                        webViewContentReader = webViewContentReader2;
                    } catch (IOException e4) {
                        e = e4;
                        webViewContentReader = webViewContentReader2;
                        try {
                            Log.e(e);
                            if (webViewContentReader != null) {
                                try {
                                    webViewContentReader.close();
                                } catch (Exception e5) {
                                }
                            }
                            contentToReturnInHtml = contentToReturn.toString();
                            returnEmptyContent = false;
                            onWebViewContentAcquired2 = onWebViewContentAcquired;
                            if (returnEmptyContent) {
                                str = "";
                            } else {
                                str = contentToReturnInHtml;
                            }
                            onWebViewContentAcquired2.webViewContent(str);
                        } catch (Throwable th2) {
                            th = th2;
                            if (webViewContentReader != null) {
                                try {
                                    webViewContentReader.close();
                                } catch (Exception e6) {
                                }
                            }
                            contentToReturnInHtml = contentToReturn.toString();
                            if (!(ignoreEmptyContent && HTMLContentEditableWebViewHelper.this.isEmptyHtmlContent(contentToReturnInHtml))) {
                                returnEmptyContent = false;
                            }
                            onWebViewContentAcquired = onWebViewContentAcquired;
                            if (returnEmptyContent) {
                                contentToReturnInHtml = "";
                            }
                            onWebViewContentAcquired.webViewContent(contentToReturnInHtml);
                            throw th;
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        webViewContentReader = webViewContentReader2;
                        if (webViewContentReader != null) {
                            webViewContentReader.close();
                        }
                        contentToReturnInHtml = contentToReturn.toString();
                        returnEmptyContent = false;
                        onWebViewContentAcquired = onWebViewContentAcquired;
                        if (returnEmptyContent) {
                            contentToReturnInHtml = "";
                        }
                        onWebViewContentAcquired.webViewContent(contentToReturnInHtml);
                        throw th;
                    }
                } catch (IOException e7) {
                    e = e7;
                    Log.e(e);
                    if (webViewContentReader != null) {
                        webViewContentReader.close();
                    }
                    contentToReturnInHtml = contentToReturn.toString();
                    if (!(ignoreEmptyContent && HTMLContentEditableWebViewHelper.this.isEmptyHtmlContent(contentToReturnInHtml))) {
                        returnEmptyContent = false;
                    }
                    onWebViewContentAcquired2 = onWebViewContentAcquired;
                    if (returnEmptyContent) {
                        str = "";
                    } else {
                        str = contentToReturnInHtml;
                    }
                    onWebViewContentAcquired2.webViewContent(str);
                }
            }
        });
    }

    private boolean isEmptyHtmlContent(@NonNull String html) {
        return StringUtils.isTrimmedAndEmpty(Html.fromHtml(html).toString().replaceAll("\\r?\\n|\\s+", ""));
    }
}
