package com.pipedrive.nearby.filter.filteritem;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.filter.FilterItem;
import com.pipedrive.views.filter.CheckableFilterRow;

class FilterItemViewHolder extends ViewHolder {
    @BindView(2131821005)
    TextView label;

    FilterItemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind((Object) this, itemView);
    }

    public void setChecked(boolean isChecked) {
        ((CheckableFilterRow) this.itemView).setChecked(isChecked);
    }

    @CallSuper
    void bind(@NonNull FilterItem entity, @NonNull Session session) {
        this.label.setText(entity.getName());
    }

    public void unbind() {
    }
}
