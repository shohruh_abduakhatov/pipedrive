package com.pipedrive.changes;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.pipedrive.application.Session;
import com.pipedrive.changes.Change.ChangeOperationType;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.products.DealProductsDataSource;
import com.pipedrive.gson.GsonHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.Deal;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ApiUrlBuilder.UrlTemplates;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.ConnectionUtil.JsonReaderInterceptor;
import com.pipedrive.util.networking.Response;
import com.pipedrive.util.networking.entities.product.DealProductEntity;
import java.io.IOException;
import java.lang.reflect.Type;

class DealProductChangeHandler extends ChangeHandler {
    private static final JsonReaderInterceptor<DataToUseForChange> jsonInterceptorForCreateAndUpdate = new JsonReaderInterceptor<DataToUseForChange>() {
        public DataToUseForChange interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull DataToUseForChange response) throws IOException {
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.skipValue();
            } else {
                DealProductEntity dealProductEntity = (DealProductEntity) GsonHelper.fromJSON(jsonReader, (Type) DealProductEntity.class);
                if (dealProductEntity == null || dealProductEntity.getPipedriveId() == null) {
                    jsonReader.skipValue();
                } else {
                    response.dealProduct.setPipedriveId(dealProductEntity.getPipedriveId());
                    new DealProductsDataSource(session.getDatabase()).createOrUpdate(response.dealProduct);
                }
            }
            return response;
        }
    };
    private static final JsonReaderInterceptor<DataToUseForChange> jsonInterceptorForDelete = new JsonReaderInterceptor<DataToUseForChange>() {
        public DataToUseForChange interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull DataToUseForChange response) throws IOException {
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.skipValue();
            } else {
                jsonReader.skipValue();
                if (response.isRequestSuccessful) {
                    Long dealProductSqlId = response.dealProduct.getSqlIdOrNull();
                    if (dealProductSqlId != null) {
                        new DealProductsDataSource(session.getDatabase()).deleteBySqlId(dealProductSqlId.longValue());
                    }
                }
            }
            return response;
        }
    };

    private static class DataToUseForChange extends Response {
        @NonNull
        final DealProduct dealProduct;
        @NonNull
        final Long dealProductDealPipedriveId;

        DataToUseForChange(@NonNull DealProduct dealProduct, @NonNull Long dealProductDealPipedriveId) {
            this.dealProduct = dealProduct;
            this.dealProductDealPipedriveId = dealProductDealPipedriveId;
        }
    }

    DealProductChangeHandler() {
    }

    @NonNull
    ChangeHandled handleChange(@NonNull Session session, @NonNull Change dealProductChange) {
        boolean isOperationCreate = dealProductChange.getChangeOperationType() == ChangeOperationType.OPERATION_CREATE;
        boolean isOperationUpdate = dealProductChange.getChangeOperationType() == ChangeOperationType.OPERATION_UPDATE;
        boolean isOperationDelete = dealProductChange.getChangeOperationType() == ChangeOperationType.OPERATION_DELETE;
        boolean isSupportedOperation = isOperationCreate || isOperationUpdate || isOperationDelete;
        if (!isSupportedOperation) {
            LogJourno.reportEvent(EVENT.ChangesHandler_corruptedChangeFound, String.format("Change:[%s]", new Object[]{dealProductChange}));
            Log.e(new Throwable(String.format("Unknown change type requested for deal product: %s", new Object[]{dealProductChange})));
            return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
        } else if (!ConnectionUtil.isConnected(session.getApplicationContext())) {
            return ChangeHandled.NOT_PROCESSED_PLEASE_RETRY;
        } else {
            DataToUseForChange dataToUseForChange = getDataToUseForCreateChange(session, dealProductChange.getMetaData());
            if (dataToUseForChange == null) {
                return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
            }
            Long dealProductPipedriveId = dataToUseForChange.dealProduct.getPipedriveIdOrNull();
            if ((isOperationUpdate || isOperationDelete) && dealProductPipedriveId == null) {
                return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
            }
            ApiUrlBuilder apiUrlBuilder;
            if (isOperationCreate) {
                apiUrlBuilder = UrlTemplates.createDealProduct(session, dataToUseForChange.dealProductDealPipedriveId);
            } else if (isOperationUpdate) {
                apiUrlBuilder = UrlTemplates.updateDealProduct(session, dataToUseForChange.dealProductDealPipedriveId, dealProductPipedriveId);
            } else {
                apiUrlBuilder = UrlTemplates.deleteDealProduct(session, dataToUseForChange.dealProductDealPipedriveId, dealProductPipedriveId);
            }
            JsonObject jsonToPost = isOperationCreate ? DealProductEntity.getJsonForCreate(dataToUseForChange.dealProduct) : isOperationUpdate ? DealProductEntity.getJsonForUpdate(dataToUseForChange.dealProduct) : new JsonObject();
            if (jsonToPost == null) {
                return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
            }
            DataToUseForChange result;
            if (isOperationCreate) {
                result = (DataToUseForChange) ConnectionUtil.requestPost(apiUrlBuilder, jsonToPost.toString(), jsonInterceptorForCreateAndUpdate, dataToUseForChange);
            } else if (isOperationUpdate) {
                result = (DataToUseForChange) ConnectionUtil.requestPut(apiUrlBuilder, jsonToPost.toString(), jsonInterceptorForCreateAndUpdate, dataToUseForChange);
            } else {
                result = (DataToUseForChange) ConnectionUtil.requestDelete(apiUrlBuilder, jsonInterceptorForDelete, dataToUseForChange);
            }
            return parseResponse(result, this, dealProductChange);
        }
    }

    @Nullable
    private DataToUseForChange getDataToUseForCreateChange(@NonNull Session session, @Nullable Long dealProductSqlId) {
        if (dealProductSqlId == null) {
            return null;
        }
        DealProduct dealProduct = (DealProduct) new DealProductsDataSource(session.getDatabase()).findBySqlId(dealProductSqlId.longValue());
        if (dealProduct == null) {
            return null;
        }
        Long dealProductDealSqlId = dealProduct.getParentSqlId();
        if (dealProductDealSqlId == null) {
            return null;
        }
        Deal dealProductsDeal = (Deal) new DealsDataSource(session.getDatabase()).findBySqlId(dealProductDealSqlId.longValue());
        if (dealProductsDeal == null || dealProductsDeal.getPipedriveIdOrNull() == null) {
            return null;
        }
        return new DataToUseForChange(dealProduct, dealProductsDeal.getPipedriveIdOrNull());
    }
}
