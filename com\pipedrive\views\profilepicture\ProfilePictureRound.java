package com.pipedrive.views.profilepicture;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.nostra13.universalimageloader.core.display.BitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.pipedrive.R;

class ProfilePictureRound extends ProfilePicture {
    public ProfilePictureRound(Context context) {
        super(context);
    }

    public ProfilePictureRound(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProfilePictureRound(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Nullable
    protected BitmapDisplayer getBitmapDisplayer() {
        return new RoundedBitmapDisplayer(Math.round(getContext().getResources().getDimension(R.dimen.dimen8)));
    }
}
