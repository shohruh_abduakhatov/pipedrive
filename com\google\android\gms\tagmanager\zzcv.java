package com.google.android.gms.tagmanager;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import com.google.android.gms.internal.zzafu.zza;
import com.google.android.gms.internal.zzafw;
import com.google.android.gms.internal.zzafw.zzc;
import com.google.android.gms.internal.zzafw.zzg;
import com.google.android.gms.internal.zzai.zzf;
import com.google.android.gms.internal.zzarz;
import com.google.android.gms.internal.zzasa;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONException;

class zzcv implements zzf {
    private final String aDY;
    private zzbn<zza> aGB;
    private final ExecutorService aGI = Executors.newSingleThreadExecutor();
    private final Context mContext;

    zzcv(Context context, String str) {
        this.mContext = context;
        this.aDY = str;
    }

    private zzc zza(ByteArrayOutputStream byteArrayOutputStream) {
        zzc com_google_android_gms_internal_zzafw_zzc = null;
        try {
            com_google_android_gms_internal_zzafw_zzc = zzbh.zzpm(byteArrayOutputStream.toString(HttpRequest.CHARSET_UTF8));
        } catch (UnsupportedEncodingException e) {
            zzbo.zzdg("Failed to convert binary resource to string for JSON parsing; the file format is not UTF-8 format.");
        } catch (JSONException e2) {
            zzbo.zzdi("Failed to extract the container from the resource file. Resource is a UTF-8 encoded string but doesn't contain a JSON container");
        }
        return com_google_android_gms_internal_zzafw_zzc;
    }

    private zzc zzal(byte[] bArr) {
        try {
            zzc zzb = zzafw.zzb(zzf.zzf(bArr));
            if (zzb == null) {
                return zzb;
            }
            zzbo.v("The container was successfully loaded from the resource (using binary file)");
            return zzb;
        } catch (zzarz e) {
            zzbo.e("The resource file is corrupted. The container cannot be extracted from the binary file");
            return null;
        } catch (zzg e2) {
            zzbo.zzdi("The resource file is invalid. The container from the binary file is invalid");
            return null;
        }
    }

    private void zzd(zza com_google_android_gms_internal_zzafu_zza) throws IllegalArgumentException {
        if (com_google_android_gms_internal_zzafu_zza.zzxv == null && com_google_android_gms_internal_zzafu_zza.aMv == null) {
            throw new IllegalArgumentException("Resource and SupplementedResource are NULL.");
        }
    }

    public synchronized void release() {
        this.aGI.shutdown();
    }

    public void zza(zzbn<zza> com_google_android_gms_tagmanager_zzbn_com_google_android_gms_internal_zzafu_zza) {
        this.aGB = com_google_android_gms_tagmanager_zzbn_com_google_android_gms_internal_zzafu_zza;
    }

    public void zzb(final zza com_google_android_gms_internal_zzafu_zza) {
        this.aGI.execute(new Runnable(this) {
            final /* synthetic */ zzcv aGJ;

            public void run() {
                this.aGJ.zzc(com_google_android_gms_internal_zzafu_zza);
            }
        });
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    boolean zzc(zza com_google_android_gms_internal_zzafu_zza) {
        File zzcgi = zzcgi();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(zzcgi);
        } catch (FileNotFoundException e) {
            zzbo.e("Error opening resource file for writing");
            return false;
        }
        try {
            fileOutputStream.write(zzasa.zzf(com_google_android_gms_internal_zzafu_zza));
            try {
                fileOutputStream.close();
            } catch (IOException e2) {
                zzbo.zzdi("error closing stream for writing resource to disk");
            }
            return true;
        } catch (IOException e3) {
            zzbo.zzdi("Error writing resource to disk. Removing resource from disk.");
            zzcgi.delete();
            return false;
        } catch (Throwable th) {
            try {
                fileOutputStream.close();
            } catch (IOException e4) {
                zzbo.zzdi("error closing stream for writing resource to disk");
            }
        }
    }

    public void zzcej() {
        this.aGI.execute(new Runnable(this) {
            final /* synthetic */ zzcv aGJ;

            {
                this.aGJ = r1;
            }

            public void run() {
                this.aGJ.zzcgh();
            }
        });
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    void zzcgh() {
        if (this.aGB == null) {
            throw new IllegalStateException("Callback must be set before execute");
        }
        this.aGB.zzcei();
        zzbo.v("Attempting to load resource from disk");
        if ((zzcj.zzcfz().zzcga() == zza.CONTAINER || zzcj.zzcfz().zzcga() == zza.CONTAINER_DEBUG) && this.aDY.equals(zzcj.zzcfz().getContainerId())) {
            this.aGB.zza(zzbn.zza.NOT_AVAILABLE);
            return;
        }
        try {
            InputStream fileInputStream = new FileInputStream(zzcgi());
            try {
                OutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                zzafw.zzc(fileInputStream, byteArrayOutputStream);
                zza zzap = zza.zzap(byteArrayOutputStream.toByteArray());
                zzd(zzap);
                this.aGB.onSuccess(zzap);
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    zzbo.zzdi("Error closing stream for reading resource from disk");
                }
            } catch (IOException e2) {
                this.aGB.zza(zzbn.zza.IO_ERROR);
                zzbo.zzdi("Failed to read the resource from disk");
            } catch (IllegalArgumentException e3) {
                this.aGB.zza(zzbn.zza.IO_ERROR);
                zzbo.zzdi("Failed to read the resource from disk. The resource is inconsistent");
                try {
                    fileInputStream.close();
                } catch (IOException e4) {
                    zzbo.zzdi("Error closing stream for reading resource from disk");
                }
            } catch (Throwable th) {
                try {
                    fileInputStream.close();
                } catch (IOException e5) {
                    zzbo.zzdi("Error closing stream for reading resource from disk");
                }
            }
            zzbo.v("The Disk resource was successfully read.");
        } catch (FileNotFoundException e6) {
            zzbo.zzdg("Failed to find the resource in the disk");
            this.aGB.zza(zzbn.zza.NOT_AVAILABLE);
        }
    }

    File zzcgi() {
        String valueOf = String.valueOf("resource_");
        String valueOf2 = String.valueOf(this.aDY);
        return new File(this.mContext.getDir("google_tagmanager", 0), valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
    }

    public zzc zzzz(int i) {
        try {
            InputStream openRawResource = this.mContext.getResources().openRawResource(i);
            String valueOf = String.valueOf(this.mContext.getResources().getResourceName(i));
            zzbo.v(new StringBuilder(String.valueOf(valueOf).length() + 66).append("Attempting to load a container from the resource ID ").append(i).append(" (").append(valueOf).append(")").toString());
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                zzafw.zzc(openRawResource, byteArrayOutputStream);
                zzc zza = zza(byteArrayOutputStream);
                if (zza == null) {
                    return zzal(byteArrayOutputStream.toByteArray());
                }
                zzbo.v("The container was successfully loaded from the resource (using JSON file format)");
                return zza;
            } catch (IOException e) {
                String valueOf2 = String.valueOf(this.mContext.getResources().getResourceName(i));
                zzbo.zzdi(new StringBuilder(String.valueOf(valueOf2).length() + 67).append("Error reading the default container with resource ID ").append(i).append(" (").append(valueOf2).append(")").toString());
                return null;
            }
        } catch (NotFoundException e2) {
            zzbo.zzdi("Failed to load the container. No default container resource found with the resource ID " + i);
            return null;
        }
    }
}
