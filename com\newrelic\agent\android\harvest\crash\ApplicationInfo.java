package com.newrelic.agent.android.harvest.crash;

import com.newrelic.agent.android.analytics.AnalyticAttribute;
import com.newrelic.agent.android.harvest.ApplicationInformation;
import com.newrelic.agent.android.harvest.type.HarvestableObject;
import com.newrelic.agent.android.util.SafeJsonPrimitive;
import com.newrelic.com.google.gson.JsonObject;

public class ApplicationInfo extends HarvestableObject {
    private String applicationBuild = "";
    private String applicationName = "";
    private String applicationVersion = "";
    private String bundleId = "";
    private int processId = 0;

    public ApplicationInfo(ApplicationInformation applicationInformation) {
        this.applicationName = applicationInformation.getAppName();
        this.applicationVersion = applicationInformation.getAppVersion();
        this.applicationBuild = applicationInformation.getAppBuild();
        this.bundleId = applicationInformation.getPackageId();
    }

    public JsonObject asJsonObject() {
        JsonObject data = new JsonObject();
        data.add(AnalyticAttribute.APP_NAME_ATTRIBUTE, SafeJsonPrimitive.factory(this.applicationName));
        data.add("appVersion", SafeJsonPrimitive.factory(this.applicationVersion));
        data.add(AnalyticAttribute.APP_BUILD_ATTRIBUTE, SafeJsonPrimitive.factory(this.applicationBuild));
        data.add(AnalyticAttribute.BUNDLE_ID_ATTRIBUTE, SafeJsonPrimitive.factory(this.bundleId));
        data.add(AnalyticAttribute.PROCESS_ID_ATTRIBUTE, SafeJsonPrimitive.factory(Integer.valueOf(this.processId)));
        return data;
    }

    public static ApplicationInfo newFromJson(JsonObject jsonObject) {
        ApplicationInfo info = new ApplicationInfo();
        info.applicationName = jsonObject.get(AnalyticAttribute.APP_NAME_ATTRIBUTE).getAsString();
        info.applicationVersion = jsonObject.get("appVersion").getAsString();
        info.applicationBuild = jsonObject.get(AnalyticAttribute.APP_BUILD_ATTRIBUTE).getAsString();
        info.bundleId = jsonObject.get(AnalyticAttribute.BUNDLE_ID_ATTRIBUTE).getAsString();
        info.processId = jsonObject.get(AnalyticAttribute.PROCESS_ID_ATTRIBUTE).getAsInt();
        return info;
    }
}
