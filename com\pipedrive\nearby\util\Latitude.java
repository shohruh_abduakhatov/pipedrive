package com.pipedrive.nearby.util;

import android.support.annotation.FloatRange;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@FloatRange(from = -90.0d, to = 90.0d)
@Retention(RetentionPolicy.SOURCE)
public @interface Latitude {
    public static final Double MAX_VALUE = Double.valueOf(90.0d);
    public static final Double MIN_VALUE = Double.valueOf(-90.0d);
}
