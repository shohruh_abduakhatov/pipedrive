package com.pipedrive.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.pipedrive.R;
import com.pipedrive.deal.view.DealDetailsActivity;
import com.pipedrive.logging.Log;
import com.pipedrive.model.Deal;
import com.pipedrive.views.GroupedDealsList;
import com.pipedrive.views.GroupedDealsList.OnDealClickListener;

public class DealsListFragment extends BaseFragment {
    private static final String KEY_CAN_SHOW_EMPTY_VIEW = "CAN_SHOW_EMPTY_VIEW";
    private static final String KEY_SHOULD_ADD_FOOTER_FOR_FAB = "SHOULD_ADD_FOOTER_FOR_FAB";
    private static final String SAVE_INSTANCE_KEY_CONTACT_SQL_ID = (TAG + ".SAVE_INSTANCE_KEY__mContactSqlId");
    private static final String SAVE_INSTANCE_KEY_ORG_ID = (TAG + ".SAVE_INSTANCE_KEY__mOrgSqlId");
    private static final String TAG = DealsListFragment.class.getSimpleName();
    private boolean canShowEmptyView = true;
    private GroupedDealsList dealsList;
    private long mContactSqlId = -1;
    private LoaderManager mLoaderManager = null;
    private long mOrgSqlId = -1;

    public static DealsListFragment newInstanceForOrg(long orgId, boolean canShowEmptyView, boolean shouldAddFooterForFab) {
        Bundle args = new Bundle();
        args.putLong(SAVE_INSTANCE_KEY_ORG_ID, orgId);
        args.putBoolean(KEY_CAN_SHOW_EMPTY_VIEW, canShowEmptyView);
        args.putBoolean(KEY_SHOULD_ADD_FOOTER_FOR_FAB, shouldAddFooterForFab);
        DealsListFragment fragment = new DealsListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static DealsListFragment newInstanceForPerson(long personId, boolean canShowEmptyView, boolean shouldAddFooterForFab) {
        Bundle args = new Bundle();
        args.putLong(SAVE_INSTANCE_KEY_CONTACT_SQL_ID, personId);
        args.putBoolean(KEY_CAN_SHOW_EMPTY_VIEW, canShowEmptyView);
        args.putBoolean(KEY_SHOULD_ADD_FOOTER_FOR_FAB, shouldAddFooterForFab);
        DealsListFragment fragment = new DealsListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.canShowEmptyView = getArguments().getBoolean(KEY_CAN_SHOW_EMPTY_VIEW);
            this.mOrgSqlId = getArguments().getLong(SAVE_INSTANCE_KEY_ORG_ID, -1);
            this.mContactSqlId = getArguments().getLong(SAVE_INSTANCE_KEY_CONTACT_SQL_ID, -1);
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_deals, container, false);
        this.dealsList = (GroupedDealsList) mainView.findViewById(R.id.dealsListComp);
        this.dealsList.canShowEmptyView(this.canShowEmptyView);
        this.dealsList.setOnDealClickListener(new OnDealClickListener() {
            public void onDealClicked(Deal deal) {
                DealDetailsActivity.start(DealsListFragment.this.getActivity(), Long.valueOf(deal.getSqlId()));
            }
        });
        this.dealsList.showFooterForFab(getArguments().getBoolean(KEY_SHOULD_ADD_FOOTER_FOR_FAB, false));
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(SAVE_INSTANCE_KEY_CONTACT_SQL_ID)) {
                this.mContactSqlId = savedInstanceState.getLong(SAVE_INSTANCE_KEY_CONTACT_SQL_ID);
            }
            if (savedInstanceState.containsKey(SAVE_INSTANCE_KEY_ORG_ID)) {
                this.mOrgSqlId = savedInstanceState.getLong(SAVE_INSTANCE_KEY_ORG_ID);
            }
        }
        loadData();
        return mainView;
    }

    private void loadData() {
        this.mLoaderManager = getLoaderManager();
        if (this.mContactSqlId != -1) {
            this.dealsList.setContactSqlId(this.mContactSqlId);
        } else if (this.mOrgSqlId != -1) {
            this.dealsList.setOrganizationSqlId(this.mOrgSqlId);
        }
        if (this.mLoaderManager != null) {
            this.dealsList.setLoaderManager(this.mLoaderManager);
        } else {
            Log.e(new Throwable("Loadermanager is missing for loading deals list for contact or organization"));
        }
    }

    public void setLoaderManager(LoaderManager loaderManager) {
        this.mLoaderManager = loaderManager;
        if (this.dealsList != null && loaderManager != null) {
            this.dealsList.setLoaderManager(loaderManager);
        }
    }

    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putLong(SAVE_INSTANCE_KEY_CONTACT_SQL_ID, this.mContactSqlId);
        outState.putLong(SAVE_INSTANCE_KEY_ORG_ID, this.mOrgSqlId);
        super.onSaveInstanceState(outState);
    }
}
