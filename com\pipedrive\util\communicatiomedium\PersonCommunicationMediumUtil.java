package com.pipedrive.util.communicatiomedium;

import android.support.annotation.NonNull;
import com.pipedrive.model.Person;
import com.pipedrive.model.contact.CommunicationMedium;
import java.util.List;

public class PersonCommunicationMediumUtil implements CommunicationMediumUtil<Person> {
    public boolean hasPhones(@NonNull Person person) {
        return person.hasPhones();
    }

    public boolean hasEmails(@NonNull Person person) {
        return person.hasEmails();
    }

    public boolean hasMultiplePhones(@NonNull Person person) {
        return person.hasMultiplePhones();
    }

    public List<? extends CommunicationMedium> getPhones(@NonNull Person person) {
        return person.getPhones();
    }

    public List<? extends CommunicationMedium> getEmails(@NonNull Person person) {
        return person.getEmails();
    }
}
