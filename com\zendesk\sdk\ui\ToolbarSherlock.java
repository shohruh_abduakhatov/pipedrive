package com.zendesk.sdk.ui;

import android.os.Build.VERSION;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;

public class ToolbarSherlock {
    private static final String LOG_TAG = "ToolbarSherlock";

    public static void installToolBar(AppCompatActivity activity) {
        if (activity.getSupportActionBar() != null) {
            Logger.d(LOG_TAG, "ActionBar available.", new Object[0]);
            return;
        }
        Logger.d(LOG_TAG, "No Actionbar detected. Installing Toolbar", new Object[0]);
        View toolBarContainer = activity.findViewById(R.id.zd_toolbar_container);
        if (toolBarContainer == null) {
            Logger.d(LOG_TAG, "Unable to find toolbar in Activity.", new Object[0]);
            return;
        }
        toolBarContainer.setVisibility(0);
        activity.setSupportActionBar((Toolbar) activity.findViewById(R.id.zd_toolbar));
        if (VERSION.SDK_INT >= 21) {
            activity.getSupportActionBar().setElevation(activity.getResources().getDimension(R.dimen.zd_toolbar_elevation));
            return;
        }
        Logger.d(LOG_TAG, "Device pre-Lollipop. Enable Toolbar shadow workaround.", new Object[0]);
        View toolBarShadow = activity.findViewById(R.id.zd_toolbar_shadow);
        if (toolBarShadow != null) {
            toolBarShadow.setVisibility(0);
        } else {
            Logger.w(LOG_TAG, "Toolbar shadow is missing", new Object[0]);
        }
    }
}
