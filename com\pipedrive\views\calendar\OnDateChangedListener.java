package com.pipedrive.views.calendar;

import android.support.annotation.NonNull;
import java.util.Calendar;

public interface OnDateChangedListener {
    void onDateChanged(@NonNull Calendar calendar);
}
