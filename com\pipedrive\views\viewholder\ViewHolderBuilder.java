package com.pipedrive.views.viewholder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;

public enum ViewHolderBuilder {
    ;

    public static View inflateViewAndTagWithViewHolder(@NonNull Context context, @Nullable ViewGroup root, boolean attachToRoot, @NonNull ViewHolder viewHolder) {
        View view = LayoutInflater.from(context).inflate(viewHolder.getLayoutResourceId(), root, attachToRoot);
        ButterKnife.bind(viewHolder, view);
        view.setTag(viewHolder);
        return view;
    }
}
