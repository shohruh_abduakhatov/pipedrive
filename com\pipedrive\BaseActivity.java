package com.pipedrive;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import butterknife.ButterKnife;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.fragments.LocaleHelper;
import com.pipedrive.navigator.Navigator;
import com.pipedrive.navigator.Navigator.Type;
import com.pipedrive.util.snackbar.SnackBarData;
import com.pipedrive.util.snackbar.SnackBarManager;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

public class BaseActivity extends BaseActivityBase {
    private static final String LANGUAGE = "language";
    private static final boolean OVERRIDE_ACTIVITY_TRANSITIONS = true;
    private String mLanguage;
    @Nullable
    private Navigator mNavigator;
    @Nullable
    private ProgressBar mProgressBar;
    @NonNull
    protected Session mSession;
    @NonNull
    private Subscription mSnackBarManagerSubscription;

    protected void onStart() {
        super.onStart();
        Analytics.hitScreen(this);
    }

    public void onResume() {
        super.onResume();
        if (!PipedriveApp.getSessionManager().hasActiveSession()) {
            LoginActivity.logOut(this, true);
            finish();
        }
        checkForLanguageChange();
        SnackBarData delayedSnackBar = SnackBarManager.INSTANCE.getDelayedSnackBar();
        if (delayedSnackBar != null) {
            showSnackBar(delayedSnackBar);
            SnackBarManager.INSTANCE.clearDelayedSnackBar();
        }
        this.mSnackBarManagerSubscription = SnackBarManager.INSTANCE.getSnackBarBus().subscribeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<SnackBarData>() {
            public void call(SnackBarData snackBarData) {
                BaseActivity.this.showSnackBar(snackBarData);
            }
        });
    }

    protected void onPause() {
        super.onPause();
        this.mSnackBarManagerSubscription.unsubscribe();
    }

    protected void checkForLanguageChange() {
        boolean appLocaleChanged;
        boolean deviceLocaleChanged;
        String languageValue = LocaleHelper.INSTANCE.getLanguageValue();
        if (TextUtils.isEmpty(this.mLanguage)) {
            this.mLanguage = languageValue;
        }
        if (this.mLanguage.equals(languageValue)) {
            appLocaleChanged = false;
        } else {
            appLocaleChanged = true;
        }
        if (this.mLanguage.equals(getResources().getConfiguration().locale.toString())) {
            deviceLocaleChanged = false;
        } else {
            deviceLocaleChanged = true;
        }
        if (appLocaleChanged) {
            this.mLanguage = languageValue;
            startActivity(getIntent());
            finish();
        } else if (deviceLocaleChanged) {
            LocaleHelper.INSTANCE.setLanguage(this, this.mLanguage, false);
            startActivity(getIntent());
            finish();
        }
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(LANGUAGE, this.mLanguage);
    }

    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        savedInstanceState.getString(LANGUAGE, null);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (PipedriveApp.getSessionManager().hasActiveSession()) {
            this.mSession = PipedriveApp.getActiveSession();
        } else {
            finish();
        }
    }

    protected void setupProgressBar(@NonNull Toolbar toolbar) {
        this.mProgressBar = (ProgressBar) ButterKnife.findById(getLayoutInflater().inflate(R.layout.layout_toolbar_progressbar, toolbar, true), (int) R.id.toolbar.progressBar);
    }

    @NonNull
    protected Session getSession() {
        return this.mSession;
    }

    @NonNull
    protected View getSnackBarContainer() {
        return findViewById(16908290);
    }

    private void showSnackBar(@NonNull SnackBarData snackBarData) {
        Snackbar.make(getSnackBarContainer(), snackBarData.getMessageResId(), snackBarData.getDuration()).show();
    }

    @Nullable
    private Navigator getNavigator() {
        if (this.mNavigator == null) {
            this.mNavigator = getNavigatorImplementation();
        }
        return this.mNavigator;
    }

    @MainThread
    protected final void setNavigatorType(@NonNull Type navigatorType) {
        Navigator navigator = getNavigator();
        if (navigator != null) {
            navigator.setViewType(navigatorType, this);
        }
    }

    @Nullable
    protected Navigator getNavigatorImplementation() {
        return null;
    }

    @CallSuper
    public boolean onCreateOptionsMenu(Menu menu) {
        Navigator navigator = getNavigator();
        if (navigator != null) {
            navigator.onCreateOptionsMenu(getMenuInflater(), menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @CallSuper
    public boolean onPrepareOptionsMenu(Menu menu) {
        Navigator navigator = getNavigator();
        if (navigator != null) {
            navigator.onPrepareOptionsMenu(menu);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @CallSuper
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean navigatorIsProvided;
        Navigator navigator = getNavigator();
        if (navigator != null) {
            navigatorIsProvided = true;
        } else {
            navigatorIsProvided = false;
        }
        if (!navigatorIsProvided) {
            return super.onOptionsItemSelected(item);
        }
        if (navigator.onOptionsItemSelected(item) || super.onOptionsItemSelected(item)) {
            return true;
        }
        return false;
    }

    @CallSuper
    public void onBackPressed() {
        Navigator navigator = getNavigator();
        if (navigator != null) {
            navigator.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    protected void showProgress() {
        if (this.mProgressBar != null) {
            this.mProgressBar.setVisibility(0);
            this.mProgressBar.setAlpha(0.0f);
            this.mProgressBar.setScaleX(0.0f);
            this.mProgressBar.setScaleY(0.0f);
            this.mProgressBar.animate().alpha(1.0f).scaleX(1.0f).scaleY(1.0f).setListener(null);
        }
    }

    protected void hideProgress() {
        if (this.mProgressBar != null) {
            this.mProgressBar.animate().alpha(0.0f).scaleX(0.0f).scaleY(0.0f).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animation) {
                    BaseActivity.this.mProgressBar.setVisibility(8);
                }
            });
        }
    }

    void overrideTransitions(boolean enterTransition) {
        int i = R.anim.null_animation;
        int i2 = enterTransition ? R.anim.fade_in : R.anim.null_animation;
        if (!enterTransition) {
            i = R.anim.fade_out;
        }
        overridePendingTransition(i2, i);
    }
}
