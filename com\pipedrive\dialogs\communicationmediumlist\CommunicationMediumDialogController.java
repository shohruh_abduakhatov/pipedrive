package com.pipedrive.dialogs.communicationmediumlist;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.contact.CommunicationMedium;
import java.util.List;

public interface CommunicationMediumDialogController<T extends BaseDatasourceEntity> {
    @NonNull
    List<? extends CommunicationMedium> getEmails(@NonNull T t);

    @Nullable
    T getEntity();

    @NonNull
    List<? extends CommunicationMedium> getPhones(@NonNull T t);

    void onCallRequested(@NonNull CommunicationMedium communicationMedium, @NonNull T t);

    void onEmailRequested(@NonNull CommunicationMedium communicationMedium, @NonNull T t);

    void onSmsRequested(@NonNull CommunicationMedium communicationMedium, @NonNull T t);
}
