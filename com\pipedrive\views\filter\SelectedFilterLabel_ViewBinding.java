package com.pipedrive.views.filter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class SelectedFilterLabel_ViewBinding implements Unbinder {
    private SelectedFilterLabel target;

    @UiThread
    public SelectedFilterLabel_ViewBinding(SelectedFilterLabel target) {
        this(target, target);
    }

    @UiThread
    public SelectedFilterLabel_ViewBinding(SelectedFilterLabel target, View source) {
        this.target = target;
        target.mSelectedFilterLabel = (TextView) Utils.findRequiredViewAsType(source, R.id.selectedFilterLabel, "field 'mSelectedFilterLabel'", TextView.class);
    }

    @CallSuper
    public void unbind() {
        SelectedFilterLabel target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mSelectedFilterLabel = null;
    }
}
