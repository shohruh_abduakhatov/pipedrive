package com.pipedrive.note;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class HtmlEditorActivity_ViewBinding implements Unbinder {
    private HtmlEditorActivity target;

    @UiThread
    public HtmlEditorActivity_ViewBinding(HtmlEditorActivity target) {
        this(target, target.getWindow().getDecorView());
    }

    @UiThread
    public HtmlEditorActivity_ViewBinding(HtmlEditorActivity target, View source) {
        this.target = target;
        target.mRootView = (ViewGroup) Utils.findRequiredViewAsType(source, R.id.root, "field 'mRootView'", ViewGroup.class);
        target.mToolbar = (Toolbar) Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'mToolbar'", Toolbar.class);
        target.mMetaInfoView = (TextView) Utils.findRequiredViewAsType(source, R.id.noteMetaInfo, "field 'mMetaInfoView'", TextView.class);
        target.mNoteEditView = (NoteEditView) Utils.findRequiredViewAsType(source, R.id.noteEditView, "field 'mNoteEditView'", NoteEditView.class);
        target.mNoteColor = ContextCompat.getColor(source.getContext(), R.color.note);
    }

    @CallSuper
    public void unbind() {
        HtmlEditorActivity target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mRootView = null;
        target.mToolbar = null;
        target.mMetaInfoView = null;
        target.mNoteEditView = null;
    }
}
