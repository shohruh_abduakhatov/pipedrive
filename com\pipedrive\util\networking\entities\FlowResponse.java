package com.pipedrive.util.networking.entities;

import com.google.gson.annotations.SerializedName;
import com.pipedrive.util.networking.Response;
import java.util.List;

public class FlowResponse extends Response {
    @SerializedName("data")
    private List<FlowItem> mItems;

    public List<FlowItem> getItems() {
        return this.mItems;
    }
}
