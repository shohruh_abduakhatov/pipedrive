package com.pipedrive.person.edit;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import com.pipedrive.model.Person;
import com.pipedrive.navigator.Navigator.Type;
import java.util.List;

public class PersonCreateActivity extends PersonActivity {
    public /* bridge */ /* synthetic */ void createOrUpdateDiscardedAsRequiredFieldsAreNotSet() {
        super.createOrUpdateDiscardedAsRequiredFieldsAreNotSet();
    }

    @Nullable
    public /* bridge */ /* synthetic */ List getAdditionalButtons() {
        return super.getAdditionalButtons();
    }

    public /* bridge */ /* synthetic */ boolean isAllRequiredFieldsSet() {
        return super.isAllRequiredFieldsSet();
    }

    public /* bridge */ /* synthetic */ boolean isChangesMadeToFieldsAfterInitialLoad() {
        return super.isChangesMadeToFieldsAfterInitialLoad();
    }

    public /* bridge */ /* synthetic */ void onCancel() {
        super.onCancel();
    }

    public /* bridge */ /* synthetic */ void onCreateOrUpdate() {
        super.onCreateOrUpdate();
    }

    public /* bridge */ /* synthetic */ void onEmailsUpdated(@NonNull Person person) {
        super.onEmailsUpdated(person);
    }

    public /* bridge */ /* synthetic */ void onOrganizationAssociationUpdated(@NonNull Person person) {
        super.onOrganizationAssociationUpdated(person);
    }

    public /* bridge */ /* synthetic */ void onPersonCreated(boolean z) {
        super.onPersonCreated(z);
    }

    public /* bridge */ /* synthetic */ void onPersonDeleted(boolean z) {
        super.onPersonDeleted(z);
    }

    public /* bridge */ /* synthetic */ void onPersonUpdated(boolean z) {
        super.onPersonUpdated(z);
    }

    public /* bridge */ /* synthetic */ void onPhonesUpdated(@NonNull Person person) {
        super.onPhonesUpdated(person);
    }

    public /* bridge */ /* synthetic */ void onResume() {
        super.onResume();
    }

    @MainThread
    public static void startActivity(@NonNull Activity activity) {
        ContextCompat.startActivity(activity, new Intent(activity, PersonCreateActivity.class), null);
    }

    public void onRequestPerson(@Nullable Person person) {
        setNavigatorType(Type.CREATE);
        super.onRequestPerson(person);
    }
}
