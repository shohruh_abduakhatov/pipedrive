package com.pipedrive.model.notes;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.NotesDataSource;
import com.pipedrive.datetime.LocaleHelper;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.model.AssociatedDatasourceEntity;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Deal;
import com.pipedrive.model.HtmlContent;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.delta.Model;
import com.pipedrive.model.delta.ModelProperty;
import com.pipedrive.util.StringUtils;
import com.pipedrive.util.networking.entities.NoteEntity;

@Model(dataSource = NotesDataSource.class)
public class Note extends BaseDatasourceEntity implements AssociatedDatasourceEntity, HtmlContent {
    private boolean activeFlag;
    private PipedriveDateTime addTime;
    @Nullable
    @ModelProperty
    private String content = null;
    @ModelProperty(jsonParamName = "deal_id")
    private Deal deal = null;
    private Organization organization = null;
    private Person person = null;

    public static Note fromEntityReturnedWithoutAssociations(NoteEntity noteEntity) {
        Note result = new Note();
        result.setPipedriveId(noteEntity.pipedriveId);
        result.setAddTime(PipedriveDateTime.instanceFormApiLongRepresentation(noteEntity.addTime));
        result.setContent(noteEntity.content);
        result.setActiveFlag(noteEntity.activeFlag);
        return result;
    }

    public boolean isAllAssociationsExisting() {
        if (getDeal() != null && !getDeal().isExisting()) {
            return false;
        }
        if (getPerson() != null && !getPerson().isExisting()) {
            return false;
        }
        if (getOrganization() == null || getOrganization().isExisting()) {
            return true;
        }
        return false;
    }

    @Nullable
    public String getContent() {
        return this.content;
    }

    public void setContent(@Nullable String content) {
        this.content = content;
    }

    public boolean isActiveFlag() {
        return this.activeFlag;
    }

    public void setActiveFlag(boolean activeFlag) {
        this.activeFlag = activeFlag;
    }

    public Deal getDeal() {
        return this.deal;
    }

    public void setDeal(Deal deal) {
        this.deal = deal;
    }

    public Person getPerson() {
        return this.person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Organization getOrganization() {
        return this.organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public PipedriveDateTime getAddTime() {
        return this.addTime;
    }

    public void setAddTime(PipedriveDateTime addTime) {
        this.addTime = addTime;
    }

    public String toString() {
        String str = " New Note: [pipedriveId:%s][sqlId:%s][activeFlag:%s][dealId:%s][personId:%s][orgId:%s][content:%s][deal_title:%s][person_name:%s][organization_name:%s][addTime:%s]";
        Object[] objArr = new Object[11];
        objArr[0] = Integer.valueOf(getPipedriveId());
        objArr[1] = Long.valueOf(getSqlId());
        objArr[2] = Boolean.valueOf(isActiveFlag());
        objArr[3] = getDeal() != null ? Integer.valueOf(getDeal().getPipedriveId()) : "NOT DEAL";
        objArr[4] = getPerson() != null ? Integer.valueOf(getPerson().getPipedriveId()) : "NO PERSON";
        objArr[5] = getOrganization() != null ? Integer.valueOf(getOrganization().getPipedriveId()) : "NO ORG";
        objArr[6] = getContent();
        objArr[7] = getDeal() != null ? getDeal().getTitle() : "DEAL IS NULL!";
        objArr[8] = getPerson() != null ? getPerson().getName() : "PERSON IS NULL!";
        objArr[9] = getOrganization() != null ? getOrganization().getName() : "ORGANIZATION IS NULL!";
        objArr[10] = getAddTime();
        return String.format(str, objArr);
    }

    @Nullable
    public String getHtmlContent() {
        return getContent();
    }

    public void setHtmlContent(@Nullable String content) {
        setContent(content);
    }

    @Nullable
    public String getMetaInfo(@NonNull Session session, @Nullable String separator) {
        StringBuilder metaInfoBuilder = new StringBuilder();
        if (getAddTime() != null) {
            metaInfoBuilder.append(LocaleHelper.getLocaleBasedDateStringInCurrentTimeZone(session, getAddTime()));
        }
        if (!(getDeal() == null || StringUtils.isTrimmedAndEmpty(getDeal().getTitle()))) {
            if (metaInfoBuilder.length() > 0) {
                metaInfoBuilder.append(separator);
            }
            metaInfoBuilder.append(getDeal().getTitle());
        }
        if (!(getPerson() == null || StringUtils.isTrimmedAndEmpty(getPerson().getName()))) {
            if (metaInfoBuilder.length() > 0) {
                metaInfoBuilder.append(separator);
            }
            metaInfoBuilder.append(getPerson().getName());
        }
        if (!(getOrganization() == null || StringUtils.isTrimmedAndEmpty(getOrganization().getName()))) {
            if (metaInfoBuilder.length() > 0) {
                metaInfoBuilder.append(separator);
            }
            metaInfoBuilder.append(getOrganization().getName());
        }
        return metaInfoBuilder.toString();
    }
}
