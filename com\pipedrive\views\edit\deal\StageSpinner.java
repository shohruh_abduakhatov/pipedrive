package com.pipedrive.views.edit.deal;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.PipelineDataSource;
import com.pipedrive.model.Deal;
import com.pipedrive.model.DealStage;
import com.pipedrive.views.Spinner.LabelBuilder;
import com.pipedrive.views.Spinner.OnItemSelectedListener;
import com.pipedrive.views.common.SpinnerWithUnderlineAndLabel;

public class StageSpinner extends SpinnerWithUnderlineAndLabel {

    public interface OnStageSelectedListener {
        void onStageSelected(@NonNull DealStage dealStage);
    }

    public StageSpinner(Context context) {
        this(context, null);
    }

    public StageSpinner(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StageSpinner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected int getLabelTextResourceId() {
        return R.string.stage;
    }

    public void setup(@NonNull Session session, @NonNull final Deal deal, @Nullable final OnStageSelectedListener onStageSelectedListener) {
        setup(new PipelineDataSource(session.getDatabase()).findStagesByPipelineId((long) deal.getPipelineId()), new LabelBuilder<DealStage>() {
            public String getLabel(DealStage item) {
                return item.getName();
            }

            public boolean isSelected(DealStage item) {
                return item.getPipedriveId() == deal.getStage();
            }
        }, new OnItemSelectedListener<DealStage>() {
            public void onItemSelected(DealStage item) {
                if (onStageSelectedListener != null) {
                    onStageSelectedListener.onStageSelected(item);
                }
            }
        });
    }
}
