package com.zendesk.sdk.ui;

import android.view.View;

public interface ListRowView<T> {
    void bind(T t);

    View getView();
}
