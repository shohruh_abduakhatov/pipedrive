package com.pipedrive.products;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.presenter.Presenter;

abstract class DealProductListPresenter extends Presenter<DealProductListView> {
    abstract void requestDealProductList(@NonNull Long l, boolean z);

    DealProductListPresenter(@NonNull Session session) {
        super(session);
    }
}
