package com.pipedrive.products;

import android.app.Dialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog.Builder;
import com.pipedrive.R;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.products.ProductsDataSource;
import com.pipedrive.dialogs.BaseDialogFragment_Native;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.model.products.Product;
import com.pipedrive.model.products.ProductVariation;
import java.util.ArrayList;
import java.util.List;

public class VariationSelectionDialogFragment extends BaseDialogFragment_Native {
    private static final String ARG_PRODUCT_SQL_ID = "ARG_PRODUCT_SQL_ID";
    private static final String ARG_SELECTED_VARIATION_NAME = "ARG_SELECTED_VARIATION_NAME";
    private static final String TAG = VariationSelectionDialogFragment.class.getSimpleName();
    @Nullable
    private OnVariationSelectedListener mOnVariationSelectedListener;

    public interface OnVariationSelectedListener {
        void onVariationSelected(@Nullable Long l);
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Session session = PipedriveApp.getActiveSession();
        if (session == null) {
            dismissFragment();
            return null;
        }
        Product product = (Product) new ProductsDataSource(session.getDatabase()).findBySqlId(Long.valueOf(getArguments().getLong(ARG_PRODUCT_SQL_ID)).longValue());
        if (product == null) {
            dismissFragment();
            return null;
        }
        if (getActivity() instanceof OnVariationSelectedListener) {
            this.mOnVariationSelectedListener = (OnVariationSelectedListener) getActivity();
        }
        final List<ProductVariation> productVariations = product.getProductVariations();
        String[] productVariationLabels = getProductVariationsLabelsWithNoneItemAsFirst(productVariations);
        String selectedVariationName = getArguments().getString(ARG_SELECTED_VARIATION_NAME);
        int checkedItemPosition = 0;
        if (selectedVariationName != null && productVariationLabels != null) {
            for (int i = 0; i < productVariationLabels.length; i++) {
                if (selectedVariationName.equals(productVariationLabels[i])) {
                    checkedItemPosition = i;
                    break;
                }
            }
        }
        return new Builder(getActivity()).setTitle(getString(R.string.variation)).setSingleChoiceItems(productVariationLabels, checkedItemPosition, new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Long variationSqlId = which == 0 ? null : ((ProductVariation) productVariations.get(which - 1)).getSqlIdOrNull();
                if (VariationSelectionDialogFragment.this.mOnVariationSelectedListener != null) {
                    VariationSelectionDialogFragment.this.mOnVariationSelectedListener.onVariationSelected(variationSqlId);
                }
                dialog.dismiss();
            }
        }).create();
    }

    private void dismissFragment() {
        setShowsDialog(false);
        dismiss();
    }

    @Nullable
    private String[] getProductVariationsLabelsWithNoneItemAsFirst(@Nullable List<ProductVariation> variations) {
        if (variations == null || variations.isEmpty()) {
            return null;
        }
        List<String> labelsList = new ArrayList();
        for (ProductVariation variation : variations) {
            labelsList.add(variation.getName());
        }
        labelsList.add(0, getString(R.string.none));
        return (String[]) labelsList.toArray(new String[labelsList.size()]);
    }

    public static void show(@NonNull FragmentManager manager, @NonNull DealProduct dealProduct) {
        if (isAbleToChangeProductVariation(PipedriveApp.getActiveSession(), dealProduct)) {
            VariationSelectionDialogFragment dialogFragment = new VariationSelectionDialogFragment();
            Bundle args = new Bundle();
            Long dealProductSqlId = dealProduct.getProduct().getSqlIdOrNull();
            if (dealProductSqlId != null) {
                args.putLong(ARG_PRODUCT_SQL_ID, dealProductSqlId.longValue());
            }
            ProductVariation selectedVariationName = dealProduct.getProductVariation();
            if (selectedVariationName != null) {
                args.putString(ARG_SELECTED_VARIATION_NAME, selectedVariationName.getName());
            }
            dialogFragment.setArguments(args);
            dialogFragment.show(manager, TAG);
        }
    }

    public static boolean isAbleToChangeProductVariation(@Nullable Session session, @NonNull DealProduct dealProduct) {
        boolean isAbleToChangeProductVariation;
        if (session == null || !session.isCompanyFeatureProductsEnabled(false) || !session.isCompanyFeatureProductPriceVariationsEnabled(false) || dealProduct.getProduct().getProductVariations().isEmpty()) {
            isAbleToChangeProductVariation = false;
        } else {
            isAbleToChangeProductVariation = true;
        }
        if (isAbleToChangeProductVariation && dealProduct.getProduct().isStored()) {
            return true;
        }
        return false;
    }
}
