package com.pipedrive.person.edit;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.model.Person;

@MainThread
interface PersonEditView {
    void onEmailsUpdated(@NonNull Person person);

    void onOrganizationAssociationUpdated(@NonNull Person person);

    void onPersonCreated(boolean z);

    void onPersonDeleted(boolean z);

    void onPersonUpdated(boolean z);

    void onPhonesUpdated(@NonNull Person person);

    void onRequestPerson(@Nullable Person person);
}
