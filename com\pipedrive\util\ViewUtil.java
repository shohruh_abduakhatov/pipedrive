package com.pipedrive.util;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Point;
import android.os.Build.VERSION;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog.Builder;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AbsListView.LayoutParams;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;
import com.pipedrive.R;

public class ViewUtil {
    private static final int ANIMATION_LENGTH_SHORT = 200;
    private static final boolean USE_CUSTOM_TOAST = false;

    public interface OnKeyboardVisibilityListener {
        public static final int INVISIBLE = 2;
        public static final int VISIBLE = 1;

        void onVisibilityChanged(int i);
    }

    public static void animateBackgroundColor(Context ctx, View viewToAnimate, int resourceIdColorFrom, int resourceIdColorTo, boolean immediate) {
        if (immediate) {
            viewToAnimate.setBackgroundColor(ContextCompat.getColor(ctx, resourceIdColorTo));
            return;
        }
        viewToAnimate.setBackgroundColor(ContextCompat.getColor(ctx, resourceIdColorFrom));
        ValueAnimator colorAnim = ObjectAnimator.ofInt(viewToAnimate, "BackgroundColor", new int[]{ContextCompat.getColor(ctx, resourceIdColorFrom), ContextCompat.getColor(ctx, resourceIdColorTo)});
        colorAnim.setDuration(200);
        colorAnim.setEvaluator(new ArgbEvaluator());
        colorAnim.start();
    }

    public static void animateTextColor(Context ctx, TextView viewToAnimate, int resourceIdColorTo, boolean immediate) {
        animateTextColor(ctx, viewToAnimate, 0, resourceIdColorTo, immediate);
    }

    private static void animateTextColor(Context ctx, TextView viewToAnimate, int resourceIdColorFrom, int resourceIdColorTo, boolean immediate) {
        if (immediate) {
            viewToAnimate.setTextColor(ContextCompat.getColor(ctx, resourceIdColorTo));
            return;
        }
        viewToAnimate.setTextColor(resourceIdColorFrom == 0 ? viewToAnimate.getTextColors().getDefaultColor() : ContextCompat.getColor(ctx, resourceIdColorFrom));
        String str = "TextColor";
        int[] iArr = new int[2];
        iArr[0] = resourceIdColorFrom == 0 ? viewToAnimate.getTextColors().getDefaultColor() : ContextCompat.getColor(ctx, resourceIdColorFrom);
        iArr[1] = ContextCompat.getColor(ctx, resourceIdColorTo);
        ValueAnimator colorAnim = ObjectAnimator.ofInt(viewToAnimate, str, iArr);
        colorAnim.setDuration(200);
        colorAnim.setEvaluator(new ArgbEvaluator());
        colorAnim.start();
    }

    public static void showErrorToast(@NonNull Context context, @StringRes int messageResId) {
        showErrorToast(context, context.getString(messageResId));
    }

    public static void showErrorToast(@NonNull Context context, @Nullable String message) {
        Toast.makeText(context, message, 1).show();
    }

    public static void onGlobalLayout(final View layoutView, final OnGlobalLayoutListener callback) {
        if (layoutView != null && callback != null) {
            layoutView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    if (VERSION.SDK_INT >= 16) {
                        layoutView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    } else {
                        layoutView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }
                    callback.onGlobalLayout();
                }
            });
        }
    }

    public static void setKeyboardVisibilityListener(final Activity activity, final View mainView, final OnKeyboardVisibilityListener onKeyboardVisibilityListener) {
        if (onKeyboardVisibilityListener != null && activity != null && mainView != null) {
            Display display = activity.getWindowManager().getDefaultDisplay();
            Point displaySize = new Point();
            display.getSize(displaySize);
            final int screenHeight = displaySize.y;
            mainView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    if (screenHeight - (mainView.getHeight() + (activity.getActionBar() != null ? activity.getActionBar().getHeight() : 0)) >= screenHeight / 3) {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                onKeyboardVisibilityListener.onVisibilityChanged(1);
                            }
                        });
                    } else {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                onKeyboardVisibilityListener.onVisibilityChanged(2);
                            }
                        });
                    }
                }
            });
        }
    }

    public static void showDeleteConfirmationDialog(Context context, String dialogMessage, Runnable deleteRequested) {
        showDeleteConfirmationDialog(context, dialogMessage, deleteRequested, context.getResources().getString(R.string.dialog_delete_positive_button_label));
    }

    public static void showDeleteConfirmationDialog(Context context, String dialogMessage, final Runnable deleteRequested, String positiveButtonText) {
        if (!(context == null)) {
            Builder dialogBuilder = new Builder(context, R.style.Theme.Pipedrive.Dialog.Alert);
            if (dialogMessage != null) {
                dialogBuilder.setTitle(dialogMessage);
            }
            dialogBuilder.setPositiveButton(positiveButtonText, new OnClickListener() {
                public void onClick(@NonNull DialogInterface dialog, int which) {
                    if (deleteRequested != null) {
                        deleteRequested.run();
                    }
                    dialog.cancel();
                }
            });
            dialogBuilder.setNegativeButton(context.getResources().getText(R.string.dialog_delete_negative_button_label), null);
            dialogBuilder.create().show();
        }
    }

    public static View getFooterForListWithFloatingButton(Context context) {
        return getFooterViewForColor(context, R.color.transparent);
    }

    public static View getColoredFooterForListWithFloatingButton(Context context, @ColorRes int colorRes) {
        return getFooterViewForColor(context, colorRes);
    }

    public static int getFooterForListWithFloatingButtonHeight(Context context) {
        return context.getResources().getDimensionPixelSize(R.dimen.dimen10) + (context.getResources().getDimensionPixelSize(R.dimen.dimen4) * 2);
    }

    private static View getFooterViewForColor(Context context, @ColorRes int colorRes) {
        View footerView;
        if (colorRes == R.color.transparent) {
            footerView = new Space(context);
        } else {
            footerView = new View(context);
            footerView.setBackgroundColor(ContextCompat.getColor(context, colorRes));
        }
        footerView.setLayoutParams(new LayoutParams(-1, getFooterForListWithFloatingButtonHeight(context)));
        return footerView;
    }
}
