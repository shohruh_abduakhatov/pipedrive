package com.pipedrive.nearby.filter;

import android.support.annotation.NonNull;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class FilterItem {
    public static final long NON_EXISTING_ID_VALUE = -1;
    static final int PIPELINE = 0;
    static final int PRIVATE_FILTER = 1;
    static final int PUBLIC_FILTER = 2;
    public static final int USER = 3;
    private final int itemType;
    @NonNull
    private final String name;
    @NonNull
    private final Long pipedriveId;
    @NonNull
    private final Long sqlId;

    @Retention(RetentionPolicy.SOURCE)
    @interface NearbyFilterItemType {
    }

    FilterItem(@NonNull Long sqlId, @NonNull Long pipedriveId, @NonNull String name, int itemType) {
        this.name = name;
        this.sqlId = sqlId;
        this.itemType = itemType;
        this.pipedriveId = pipedriveId;
    }

    public int getItemType() {
        return this.itemType;
    }

    @NonNull
    public String getName() {
        return this.name;
    }

    @NonNull
    public Long getSqlId() {
        return this.sqlId;
    }

    @NonNull
    public Long getPipedriveId() {
        return this.pipedriveId;
    }
}
