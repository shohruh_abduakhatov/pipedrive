package com.zendesk.service;

public abstract class ZendeskCallback<T> {
    public abstract void onError(ErrorResponse errorResponse);

    public abstract void onSuccess(T t);
}
