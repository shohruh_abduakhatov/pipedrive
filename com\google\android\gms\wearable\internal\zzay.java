package com.google.android.gms.wearable.internal;

import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wearable.WearableStatusCodes;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

final class zzay<T> {
    private final Map<T, zzbq<T>> VB = new HashMap();

    private static class zza<T> extends zzb<Status> {
        private WeakReference<Map<T, zzbq<T>>> aTZ;
        private WeakReference<T> aUa;

        zza(Map<T, zzbq<T>> map, T t, com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status);
            this.aTZ = new WeakReference(map);
            this.aUa = new WeakReference(t);
        }

        public void zza(Status status) {
            Map map = (Map) this.aTZ.get();
            Object obj = this.aUa.get();
            if (!(status.getStatus().isSuccess() || map == null || obj == null)) {
                synchronized (map) {
                    zzbq com_google_android_gms_wearable_internal_zzbq = (zzbq) map.remove(obj);
                    if (com_google_android_gms_wearable_internal_zzbq != null) {
                        com_google_android_gms_wearable_internal_zzbq.clear();
                    }
                }
            }
            zzbc(status);
        }
    }

    private static class zzb<T> extends zzb<Status> {
        private WeakReference<Map<T, zzbq<T>>> aTZ;
        private WeakReference<T> aUa;

        zzb(Map<T, zzbq<T>> map, T t, com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status);
            this.aTZ = new WeakReference(map);
            this.aUa = new WeakReference(t);
        }

        public void zza(Status status) {
            Map map = (Map) this.aTZ.get();
            Object obj = this.aUa.get();
            if (!(status.getStatus().getStatusCode() != WearableStatusCodes.UNKNOWN_LISTENER || map == null || obj == null)) {
                synchronized (map) {
                    zzbq com_google_android_gms_wearable_internal_zzbq = (zzbq) map.remove(obj);
                    if (com_google_android_gms_wearable_internal_zzbq != null) {
                        com_google_android_gms_wearable_internal_zzbq.clear();
                    }
                }
            }
            zzbc(status);
        }
    }

    zzay() {
    }

    public void zza(zzbp com_google_android_gms_wearable_internal_zzbp, com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, T t) throws RemoteException {
        synchronized (this.VB) {
            zzbq com_google_android_gms_wearable_internal_zzbq = (zzbq) this.VB.remove(t);
            if (com_google_android_gms_wearable_internal_zzbq == null) {
                com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status.setResult(new Status(WearableStatusCodes.UNKNOWN_LISTENER));
                return;
            }
            com_google_android_gms_wearable_internal_zzbq.clear();
            ((zzax) com_google_android_gms_wearable_internal_zzbp.zzavg()).zza(new zzb(this.VB, t, com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status), new RemoveListenerRequest(com_google_android_gms_wearable_internal_zzbq));
        }
    }

    public void zza(zzbp com_google_android_gms_wearable_internal_zzbp, com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, T t, zzbq<T> com_google_android_gms_wearable_internal_zzbq_T) throws RemoteException {
        synchronized (this.VB) {
            if (this.VB.get(t) != null) {
                com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status.setResult(new Status(WearableStatusCodes.DUPLICATE_LISTENER));
                return;
            }
            this.VB.put(t, com_google_android_gms_wearable_internal_zzbq_T);
            try {
                ((zzax) com_google_android_gms_wearable_internal_zzbp.zzavg()).zza(new zza(this.VB, t, com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status), new AddListenerRequest(com_google_android_gms_wearable_internal_zzbq_T));
            } catch (RemoteException e) {
                this.VB.remove(t);
                throw e;
            }
        }
    }

    public void zzlw(IBinder iBinder) {
        synchronized (this.VB) {
            zzax zzlv = com.google.android.gms.wearable.internal.zzax.zza.zzlv(iBinder);
            zzav com_google_android_gms_wearable_internal_zzbo_zzo = new zzo();
            for (Entry entry : this.VB.entrySet()) {
                zzbq com_google_android_gms_wearable_internal_zzbq = (zzbq) entry.getValue();
                try {
                    zzlv.zza(com_google_android_gms_wearable_internal_zzbo_zzo, new AddListenerRequest(com_google_android_gms_wearable_internal_zzbq));
                    if (Log.isLoggable("WearableClient", 2)) {
                        String valueOf = String.valueOf(entry.getKey());
                        String valueOf2 = String.valueOf(com_google_android_gms_wearable_internal_zzbq);
                        Log.d("WearableClient", new StringBuilder((String.valueOf(valueOf).length() + 27) + String.valueOf(valueOf2).length()).append("onPostInitHandler: added: ").append(valueOf).append("/").append(valueOf2).toString());
                    }
                } catch (RemoteException e) {
                    String valueOf3 = String.valueOf(entry.getKey());
                    String valueOf4 = String.valueOf(com_google_android_gms_wearable_internal_zzbq);
                    Log.d("WearableClient", new StringBuilder((String.valueOf(valueOf3).length() + 32) + String.valueOf(valueOf4).length()).append("onPostInitHandler: Didn't add: ").append(valueOf3).append("/").append(valueOf4).toString());
                }
            }
        }
    }
}
