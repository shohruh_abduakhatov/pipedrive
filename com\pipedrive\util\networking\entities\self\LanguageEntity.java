package com.pipedrive.util.networking.entities.self;

import android.support.annotation.Nullable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class LanguageEntity {
    @Nullable
    @SerializedName("language_code")
    @Expose
    private String defaultLanguageCode;
    @Nullable
    @SerializedName("country_code")
    @Expose
    private String defaultLanguageCountry;

    LanguageEntity() {
    }

    @Nullable
    String getDefaultLanguageCode() {
        return this.defaultLanguageCode;
    }

    @Nullable
    String getDefaultLanguageCountry() {
        return this.defaultLanguageCountry;
    }
}
