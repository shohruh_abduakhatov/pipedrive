package com.google.android.gms.wearable.internal;

import android.content.IntentFilter;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzrr;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.NodeApi.GetConnectedNodesResult;
import com.google.android.gms.wearable.NodeApi.GetLocalNodeResult;
import com.google.android.gms.wearable.NodeApi.NodeListener;
import java.util.ArrayList;
import java.util.List;

public final class zzbb implements NodeApi {

    class AnonymousClass3 implements zza<NodeListener> {
        final /* synthetic */ IntentFilter[] aTc;

        AnonymousClass3(IntentFilter[] intentFilterArr) {
            this.aTc = intentFilterArr;
        }

        public void zza(zzbp com_google_android_gms_wearable_internal_zzbp, com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, NodeListener nodeListener, zzrr<NodeListener> com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_NodeApi_NodeListener) throws RemoteException {
            com_google_android_gms_wearable_internal_zzbp.zza((com.google.android.gms.internal.zzqo.zzb) com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, nodeListener, (zzrr) com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_NodeApi_NodeListener, this.aTc);
        }
    }

    public static class zza implements GetConnectedNodesResult {
        private final List<Node> aUi;
        private final Status hv;

        public zza(Status status, List<Node> list) {
            this.hv = status;
            this.aUi = list;
        }

        public List<Node> getNodes() {
            return this.aUi;
        }

        public Status getStatus() {
            return this.hv;
        }
    }

    public static class zzb implements GetLocalNodeResult {
        private final Node aUj;
        private final Status hv;

        public zzb(Status status, Node node) {
            this.hv = status;
            this.aUj = node;
        }

        public Node getNode() {
            return this.aUj;
        }

        public Status getStatus() {
            return this.hv;
        }
    }

    private static zza<NodeListener> zza(IntentFilter[] intentFilterArr) {
        return new AnonymousClass3(intentFilterArr);
    }

    public PendingResult<Status> addListener(GoogleApiClient googleApiClient, NodeListener nodeListener) {
        return zzb.zza(googleApiClient, zza(new IntentFilter[]{zzbn.zzrp("com.google.android.gms.wearable.NODE_CHANGED")}), nodeListener);
    }

    public PendingResult<GetConnectedNodesResult> getConnectedNodes(GoogleApiClient googleApiClient) {
        return googleApiClient.zza(new zzi<GetConnectedNodesResult>(this, googleApiClient) {
            final /* synthetic */ zzbb aUg;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zzz(this);
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzfa(status);
            }

            protected GetConnectedNodesResult zzfa(Status status) {
                return new zza(status, new ArrayList());
            }
        });
    }

    public PendingResult<GetLocalNodeResult> getLocalNode(GoogleApiClient googleApiClient) {
        return googleApiClient.zza(new zzi<GetLocalNodeResult>(this, googleApiClient) {
            final /* synthetic */ zzbb aUg;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zzy(this);
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzez(status);
            }

            protected GetLocalNodeResult zzez(Status status) {
                return new zzb(status, null);
            }
        });
    }

    public PendingResult<Status> removeListener(GoogleApiClient googleApiClient, final NodeListener nodeListener) {
        return googleApiClient.zza(new zzi<Status>(this, googleApiClient) {
            final /* synthetic */ zzbb aUg;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zza((com.google.android.gms.internal.zzqo.zzb) this, nodeListener);
            }

            public Status zzb(Status status) {
                return status;
            }

            public /* synthetic */ Result zzc(Status status) {
                return zzb(status);
            }
        });
    }
}
