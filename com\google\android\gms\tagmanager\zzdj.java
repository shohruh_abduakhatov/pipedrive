package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;

public class zzdj {
    private Tracker aE;
    private GoogleAnalytics aG;
    private Context mContext;

    static class zza implements Logger {
        zza() {
        }

        private static int zzaai(int i) {
            switch (i) {
                case 2:
                    return 0;
                case 3:
                case 4:
                    return 1;
                case 5:
                    return 2;
                default:
                    return 3;
            }
        }

        public void error(Exception exception) {
            zzbo.zzb("", exception);
        }

        public void error(String str) {
            zzbo.e(str);
        }

        public int getLogLevel() {
            return zzaai(zzbo.getLogLevel());
        }

        public void info(String str) {
            zzbo.zzdh(str);
        }

        public void setLogLevel(int i) {
            zzbo.zzdi("GA uses GTM logger. Please use TagManager.setLogLevel(int) instead.");
        }

        public void verbose(String str) {
            zzbo.v(str);
        }

        public void warn(String str) {
            zzbo.zzdi(str);
        }
    }

    public zzdj(Context context) {
        this.mContext = context;
    }

    private synchronized void zzpv(String str) {
        if (this.aG == null) {
            this.aG = GoogleAnalytics.getInstance(this.mContext);
            this.aG.setLogger(new zza());
            this.aE = this.aG.newTracker(str);
        }
    }

    public Tracker zzpu(String str) {
        zzpv(str);
        return this.aE;
    }
}
