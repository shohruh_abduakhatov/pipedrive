package com.pipedrive.util.filepicker;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import com.pipedrive.R;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.util.flowfiles.FileHelper;
import com.pipedrive.util.networking.entities.FlowItem;
import java.io.File;

public class DocumentPickerHelper {
    private final int REQUEST_CODE = 19;
    @Nullable
    private OnActivityResult mRegisteredOnActivityResult;

    public boolean isDocumentPickerApplicationExist(@NonNull Context context) {
        return getDocumentPickerIntent().resolveActivity(context.getPackageManager()) != null;
    }

    public boolean pickDocument(@NonNull Activity fromActivity) {
        return pickDocument(fromActivity, null, null, fromActivity);
    }

    public boolean pickDocument(@NonNull Fragment fromSupportFragment) {
        return pickDocument(fromSupportFragment.getContext(), fromSupportFragment, null, null);
    }

    public boolean pickDocument(@NonNull android.app.Fragment fromFragment) {
        return pickDocument(fromFragment.getActivity(), null, fromFragment, null);
    }

    public void registerOnActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        this.mRegisteredOnActivityResult = new OnActivityResult(requestCode, resultCode, data != null ? (Intent) data.clone() : null);
    }

    public void retrievePickedDocument(@NonNull Context context, @NonNull OnDocumentPickerListener onDocumentPickerListener) {
        if (this.mRegisteredOnActivityResult != null) {
            onActivityResult(context, this.mRegisteredOnActivityResult.requestCode, this.mRegisteredOnActivityResult.resultCode, this.mRegisteredOnActivityResult.data, onDocumentPickerListener);
            this.mRegisteredOnActivityResult = null;
        }
    }

    @Deprecated
    public void onActivityResult(@NonNull Context context, int requestCode, int resultCode, Intent data, @NonNull OnDocumentPickerListener onDocumentPickerListener) {
        if (requestCode != 19) {
            onDocumentPickerListener.documentPickFailed();
            return;
        }
        if (resultCode == 0) {
            onDocumentPickerListener.documentPickCancelled();
            return;
        }
        if (data == null) {
            onDocumentPickerListener.documentPickFailed();
            messageForGenericError(context);
            return;
        }
        Uri documentUri = data.getData();
        boolean requestingDocumentFailed = documentUri == null || resultCode != -1;
        if (requestingDocumentFailed) {
            onDocumentPickerListener.documentPickFailed();
            messageForGenericError(context);
        } else if (isDocumentPickerApplicationExist(context)) {
            DocumentMetaData documentMetaData = getDocumentMetaData(documentUri, context);
            if (!onDocumentPickerListener.documentPicked(documentUri, documentMetaData.documentName, documentMetaData.fileSizeInBytes, documentMetaData.fileType)) {
                messageForGenericError(context);
            }
        } else {
            messageForNoDocumentPickerAppExists(context);
        }
    }

    @NonNull
    private DocumentMetaData getDocumentMetaData(@NonNull Uri document, @NonNull Context context) {
        boolean fileProvidedAsContentProvider;
        boolean fileProvidedAsFile;
        if (TextUtils.indexOf(document.toString(), "content") != -1) {
            fileProvidedAsContentProvider = true;
        } else {
            fileProvidedAsContentProvider = false;
        }
        if (TextUtils.indexOf(document.toString(), FlowItem.ITEM_TYPE_OBJECT_FILE) != -1) {
            fileProvidedAsFile = true;
        } else {
            fileProvidedAsFile = false;
        }
        if (fileProvidedAsContentProvider) {
            return getDocumentMetaDataFromContentProvider(document, context);
        }
        if (!fileProvidedAsFile) {
            return new DocumentMetaData();
        }
        DocumentMetaData documentMetaData = new DocumentMetaData();
        File file = FileHelper.getFile(document);
        if (file == null) {
            return documentMetaData;
        }
        documentMetaData.fileSizeInBytes = Long.valueOf(file.length());
        documentMetaData.documentName = file.getName();
        return documentMetaData;
    }

    @NonNull
    private DocumentMetaData getDocumentMetaDataFromContentProvider(@NonNull Uri documentContentProviderUri, @NonNull Context context) {
        boolean expectingOnlyOneDocumentSelection;
        int sizeIndex;
        ContentResolver contentResolver = context.getContentResolver();
        String fileType = contentResolver.getType(documentContentProviderUri);
        Cursor cursor = contentResolver.query(documentContentProviderUri, null, null, null, null, null);
        String name = null;
        Long documentSizeInBytes = null;
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    expectingOnlyOneDocumentSelection = true;
                    if (expectingOnlyOneDocumentSelection) {
                        name = cursor.getString(cursor.getColumnIndex("_display_name"));
                        sizeIndex = cursor.getColumnIndex("_size");
                        if (!cursor.isNull(sizeIndex)) {
                            documentSizeInBytes = Long.valueOf(cursor.getLong(sizeIndex));
                        }
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                    return new DocumentMetaData(name, documentSizeInBytes, fileType);
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        expectingOnlyOneDocumentSelection = false;
        if (expectingOnlyOneDocumentSelection) {
            name = cursor.getString(cursor.getColumnIndex("_display_name"));
            sizeIndex = cursor.getColumnIndex("_size");
            if (cursor.isNull(sizeIndex)) {
                documentSizeInBytes = Long.valueOf(cursor.getLong(sizeIndex));
            }
        }
        if (cursor != null) {
            cursor.close();
        }
        return new DocumentMetaData(name, documentSizeInBytes, fileType);
    }

    private void messageForNoDocumentPickerAppExists(@NonNull Context context) {
        ViewUtil.showErrorToast(context, R.string.could_not_pick_a_file_because_this_device_has_no_file_picker_apps);
    }

    private void messageForGenericError(@NonNull Context context) {
        ViewUtil.showErrorToast(context, R.string.could_not_pick_a_file_try_again_later);
    }

    private boolean pickDocument(@NonNull Context context, @Nullable Fragment fromSupportFragment, @Nullable android.app.Fragment fromFragment, @Nullable Activity fromActivity) {
        boolean unknownCaller;
        if (fromSupportFragment == null && fromFragment == null && fromActivity == null) {
            unknownCaller = true;
        } else {
            unknownCaller = false;
        }
        if (unknownCaller) {
            messageForGenericError(context);
            return false;
        }
        boolean noDocumentPickerAppExist;
        if (isDocumentPickerApplicationExist(context)) {
            noDocumentPickerAppExist = false;
        } else {
            noDocumentPickerAppExist = true;
        }
        if (noDocumentPickerAppExist) {
            messageForNoDocumentPickerAppExists(context);
            return false;
        }
        Intent documentPickerIntent = getDocumentPickerIntent();
        if (fromSupportFragment != null) {
            fromSupportFragment.startActivityForResult(documentPickerIntent, 19);
        } else if (fromFragment != null) {
            fromFragment.startActivityForResult(documentPickerIntent, 19);
        } else {
            fromActivity.startActivityForResult(documentPickerIntent, 19);
        }
        return true;
    }

    @NonNull
    private Intent getDocumentPickerIntent() {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.addCategory("android.intent.category.OPENABLE");
        intent.setType("*/*");
        return intent;
    }
}
