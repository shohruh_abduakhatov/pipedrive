package com.pipedrive.nearby.cards.cards;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.pipedrive.application.Session;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.nearby.model.NearbyItem;
import com.pipedrive.util.formatter.DistanceFormatter;
import rx.subscriptions.CompositeSubscription;

public abstract class CardHeader<NEARBY_ITEM extends NearbyItem<ENTITY>, ENTITY extends BaseDatasourceEntity> {
    CompositeSubscription compositeSubscription = new CompositeSubscription();
    Context context;
    @Nullable
    private Double distanceInMeters;
    @BindString(2131296862)
    String subtitleSeparator;
    @BindView(2131820558)
    TextView subtitleTextView;
    @BindView(2131820559)
    TextView titleTextView;

    @NonNull
    abstract String getSubTitle(@NonNull Session session, @NonNull NEARBY_ITEM nearby_item, @NonNull ENTITY entity);

    @NonNull
    protected abstract String getTitle(@NonNull ENTITY entity);

    public abstract void propagateClickEvents(@NonNull Long l);

    public void bind(@NonNull ViewGroup viewById, @NonNull Session session, @NonNull NEARBY_ITEM nearbyItem, @NonNull ENTITY entity) {
        bind(viewById, session, nearbyItem, entity, null);
    }

    public void bind(@NonNull ViewGroup view, @NonNull Session session, @NonNull NEARBY_ITEM nearbyItem, @NonNull ENTITY entity, @Nullable Double distanceInMeters) {
        this.distanceInMeters = distanceInMeters;
        if (view.getHeight() == 0) {
            view.measure(MeasureSpec.makeMeasureSpec(0, 0), MeasureSpec.makeMeasureSpec(0, 0));
        }
        ButterKnife.bind((Object) this, (View) view);
        this.context = view.getContext();
        this.titleTextView.setText(getTitle(entity));
        setSubtitle(session, nearbyItem, entity);
    }

    void setSubtitle(@NonNull Session session, @NonNull NEARBY_ITEM nearbyItem, @NonNull ENTITY entity) {
        StringBuilder subtitleString = new StringBuilder();
        if (this.distanceInMeters != null) {
            subtitleString.append(getDistanceString());
        }
        String subtitle = getSubTitle(session, nearbyItem, entity);
        if (!(this.distanceInMeters == null || subtitle.isEmpty())) {
            subtitleString.append(this.subtitleSeparator);
        }
        if (!subtitle.isEmpty()) {
            subtitleString.append(subtitle);
        }
        this.subtitleTextView.setText(subtitleString);
    }

    @NonNull
    private String getDistanceString() {
        return DistanceFormatter.formatDistance(this.distanceInMeters);
    }

    void releaseResources() {
        this.compositeSubscription.clear();
    }
}
