package com.pipedrive.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AlphabetIndexer;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.model.Organization;
import com.pipedrive.views.viewholder.ViewHolderBuilder;
import com.pipedrive.views.viewholder.contact.OrganizationRowViewHolder;

public class OrganizationListAdapter extends BaseSectionedListAdapter {
    private final OrganizationsDataSource mDataSource;
    @NonNull
    private SearchConstraint mSearchConstraint = SearchConstraint.EMPTY;

    public OrganizationListAdapter(Context context, Cursor c, Session session) {
        super(context, c, true);
        this.mDataSource = new OrganizationsDataSource(session.getDatabase());
        this.mAlphabetIndexer = getCount() <= 0 ? null : new AlphabetIndexer(getCursor(), getCursor().getColumnIndex(PipeSQLiteHelper.COLUMN_ORG_NAME_SEARCH_FIELD), this.mDataSource.getAlphabetString(this.mSearchConstraint));
    }

    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return ViewHolderBuilder.inflateViewAndTagWithViewHolder(context, parent, false, new OrganizationRowViewHolder());
    }

    public void bindView(View view, Context context, Cursor cursor) {
        Organization organization = this.mDataSource.deflateCursor(cursor);
        OrganizationRowViewHolder organizationViewHolder = (OrganizationRowViewHolder) view.getTag();
        if (organization != null) {
            organizationViewHolder.fill(organization, this.mSearchConstraint);
        }
    }

    public void setSearchConstraint(@NonNull SearchConstraint searchConstraint) {
        this.mSearchConstraint = searchConstraint;
        changeCursor(this.mDataSource.getSearchCursor(searchConstraint));
        this.mAlphabetIndexer = getCount() <= 0 ? null : new AlphabetIndexer(getCursor(), getCursor().getColumnIndex(PipeSQLiteHelper.COLUMN_ORG_NAME_SEARCH_FIELD), this.mDataSource.getAlphabetString(this.mSearchConstraint));
    }

    public void changeCursor(Cursor cursor) {
        super.changeCursor(cursor);
        this.mAlphabetIndexer = getCount() <= 0 ? null : new AlphabetIndexer(getCursor(), getCursor().getColumnIndex(PipeSQLiteHelper.COLUMN_ORG_NAME_SEARCH_FIELD), this.mDataSource.getAlphabetString(this.mSearchConstraint));
    }
}
