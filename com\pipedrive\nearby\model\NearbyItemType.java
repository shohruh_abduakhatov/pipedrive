package com.pipedrive.nearby.model;

public enum NearbyItemType {
    DEALS,
    PERSONS,
    ORGANIZATIONS
}
