package com.zendesk.sdk.model.helpcenter.help;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.annotations.SerializedName;
import com.zendesk.util.CollectionUtils;
import java.util.List;

public class CategoryItem implements HelpItem {
    private List<SectionItem> children;
    private boolean expanded = true;
    @SerializedName("id")
    private Long id;
    @SerializedName("name")
    private String name;

    @NonNull
    public String getName() {
        return this.name == null ? "" : this.name;
    }

    @Nullable
    public Long getId() {
        return this.id;
    }

    @Nullable
    public Long getParentId() {
        return null;
    }

    @NonNull
    public List<? extends HelpItem> getChildren() {
        return CollectionUtils.copyOf(this.children);
    }

    public void setSections(List<SectionItem> sections) {
        this.children = CollectionUtils.copyOf((List) sections);
    }

    public boolean isExpanded() {
        return this.expanded;
    }

    public boolean setExpanded(boolean expanded) {
        this.expanded = expanded;
        return this.expanded;
    }

    public int getViewType() {
        return 1;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CategoryItem that = (CategoryItem) o;
        if (this.id != null) {
            return this.id.equals(that.id);
        }
        if (that.id != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.id != null ? this.id.hashCode() : 0;
    }
}
