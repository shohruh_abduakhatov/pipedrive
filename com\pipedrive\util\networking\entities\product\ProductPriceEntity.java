package com.pipedrive.util.networking.entities.product;

import android.support.annotation.Nullable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.math.BigDecimal;

public class ProductPriceEntity {
    @Nullable
    @SerializedName("currency")
    @Expose
    private String currency;
    @Nullable
    @SerializedName("id")
    @Expose
    private Long pipedriveId;
    @Nullable
    @SerializedName("price")
    @Expose
    private BigDecimal price;

    @Nullable
    public Long getPipedriveId() {
        return this.pipedriveId;
    }

    @Nullable
    public BigDecimal getPrice() {
        return this.price;
    }

    @Nullable
    public String getCurrency() {
        return this.currency;
    }

    public String toString() {
        return "ProductPriceEntity{pipedriveId=" + getPipedriveId() + ", price=" + getPrice() + ", currency='" + getCurrency() + '\'' + '}';
    }
}
