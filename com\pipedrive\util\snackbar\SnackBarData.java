package com.pipedrive.util.snackbar;

import android.support.annotation.StringRes;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class SnackBarData {
    private final int duration;
    @StringRes
    private final int messageResId;

    @Retention(RetentionPolicy.SOURCE)
    public @interface SnackBarDuration {
    }

    public int getMessageResId() {
        return this.messageResId;
    }

    public int getDuration() {
        return this.duration;
    }

    public SnackBarData(@StringRes int messageResId, int duration) {
        this.messageResId = messageResId;
        this.duration = duration;
    }
}
