package com.pipedrive.sync;

import com.pipedrive.application.Session;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished.TaskResult;

class SyncManager$12 implements OnTaskFinished<String> {
    final /* synthetic */ SyncManager this$0;
    final /* synthetic */ SyncManager$OnReleaseSplashScreen val$onReleaseSplashScreenSafe;

    SyncManager$12(SyncManager this$0, SyncManager$OnReleaseSplashScreen syncManager$OnReleaseSplashScreen) {
        this.this$0 = this$0;
        this.val$onReleaseSplashScreenSafe = syncManager$OnReleaseSplashScreen;
    }

    public void taskFinished(Session session, TaskResult taskResult, String... params) {
        this.val$onReleaseSplashScreenSafe.release();
    }
}
