package com.pipedrive.nearby.model;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.datasource.BaseDataSource;
import com.pipedrive.model.Activity;
import com.pipedrive.model.BaseDatasourceEntity;
import rx.Observable;

class SingleNearbyItemImplementation<T extends BaseDatasourceEntity> implements NearbyItemImplementation<T> {
    @NonNull
    private final Long itemSqlId;

    SingleNearbyItemImplementation(@NonNull Long itemSqlId) {
        this.itemSqlId = itemSqlId;
    }

    @NonNull
    public Observable<T> getData(@NonNull BaseDataSource<T> dataSource) {
        return dataSource.findBySqlIdRx(this.itemSqlId).toObservable();
    }

    @NonNull
    public Observable<Activity> getNextActivity(@NonNull Session session, @NonNull String activitiesColumnForNearbyItem) {
        return new ActivitiesDataSource(session).getActivityForRelatedItemRx(true, activitiesColumnForNearbyItem, this.itemSqlId);
    }

    @NonNull
    public Observable<Activity> getLastActivity(@NonNull Session session, @NonNull String activitiesColumnForNearbyItem) {
        return new ActivitiesDataSource(session).getActivityForRelatedItemRx(false, activitiesColumnForNearbyItem, this.itemSqlId);
    }

    @NonNull
    public Observable<Long> getItemSqlIds() {
        return Observable.just(this.itemSqlId);
    }

    @NonNull
    public Boolean hasMultipleIds() {
        return Boolean.valueOf(false);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        return this.itemSqlId.equals(((SingleNearbyItemImplementation) o).itemSqlId);
    }

    public int hashCode() {
        return this.itemSqlId.hashCode();
    }
}
