package okhttp3.internal.http;

import com.newrelic.agent.android.instrumentation.okhttp3.OkHttp3Instrumentation;
import com.zendesk.service.HttpConstants;
import java.io.IOException;
import java.net.ProtocolException;
import okhttp3.Interceptor;
import okhttp3.Interceptor.Chain;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Response$Builder;
import okhttp3.ResponseBody;
import okhttp3.internal.Util;
import okhttp3.internal.connection.StreamAllocation;
import okio.BufferedSink;
import okio.Okio;

public final class CallServerInterceptor implements Interceptor {
    private final boolean forWebSocket;

    public CallServerInterceptor(boolean forWebSocket) {
        this.forWebSocket = forWebSocket;
    }

    public Response intercept(Chain chain) throws IOException {
        HttpCodec httpCodec = ((RealInterceptorChain) chain).httpStream();
        StreamAllocation streamAllocation = ((RealInterceptorChain) chain).streamAllocation();
        Request request = chain.request();
        long sentRequestMillis = System.currentTimeMillis();
        httpCodec.writeRequestHeaders(request);
        Response$Builder responseBuilder = null;
        if (HttpMethod.permitsRequestBody(request.method()) && request.body() != null) {
            if ("100-continue".equalsIgnoreCase(request.header("Expect"))) {
                httpCodec.flushRequest();
                responseBuilder = httpCodec.readResponseHeaders(true);
            }
            if (responseBuilder == null) {
                BufferedSink bufferedRequestBody = Okio.buffer(httpCodec.createRequestBody(request, request.body().contentLength()));
                request.body().writeTo(bufferedRequestBody);
                bufferedRequestBody.close();
            }
        }
        httpCodec.finishRequest();
        if (responseBuilder == null) {
            responseBuilder = httpCodec.readResponseHeaders(false);
        }
        Response response = responseBuilder.request(request).handshake(streamAllocation.connection().handshake()).sentRequestAtMillis(sentRequestMillis).receivedResponseAtMillis(System.currentTimeMillis()).build();
        int code = response.code();
        Response$Builder newBuilder;
        ResponseBody responseBody;
        if (this.forWebSocket && code == 101) {
            newBuilder = !(response instanceof Response$Builder) ? response.newBuilder() : OkHttp3Instrumentation.newBuilder((Response$Builder) response);
            responseBody = Util.EMPTY_RESPONSE;
            response = (!(newBuilder instanceof Response$Builder) ? newBuilder.body(responseBody) : OkHttp3Instrumentation.body(newBuilder, responseBody)).build();
        } else {
            newBuilder = !(response instanceof Response$Builder) ? response.newBuilder() : OkHttp3Instrumentation.newBuilder((Response$Builder) response);
            responseBody = httpCodec.openResponseBody(response);
            if (newBuilder instanceof Response$Builder) {
                newBuilder = OkHttp3Instrumentation.body(newBuilder, responseBody);
            } else {
                newBuilder = newBuilder.body(responseBody);
            }
            response = newBuilder.build();
        }
        if ("close".equalsIgnoreCase(response.request().header("Connection")) || "close".equalsIgnoreCase(response.header("Connection"))) {
            streamAllocation.noNewStreams();
        }
        if ((code != HttpConstants.HTTP_NO_CONTENT && code != HttpConstants.HTTP_RESET) || response.body().contentLength() <= 0) {
            return response;
        }
        throw new ProtocolException("HTTP " + code + " had non-zero Content-Length: " + response.body().contentLength());
    }
}
