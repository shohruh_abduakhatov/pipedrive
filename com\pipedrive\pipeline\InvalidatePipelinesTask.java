package com.pipedrive.pipeline;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.datasource.PipelineDataSource;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.Pipeline;
import com.pipedrive.tasks.StoreAwareAsyncTask;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.Response;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class InvalidatePipelinesTask extends StoreAwareAsyncTask<Void, Void, Boolean> {
    public static final String ACTION_INVALIDATE_PIPELINE = "com.pipedrive.pipeline.InvalidatePipelinesTask.ACTION_INVALIDATE_PIPELINE";
    @Nullable
    private final OnTaskFinished mOnTaskFinished;

    public interface OnTaskFinished {
        @MainThread
        void allPipelinesDownloaded(@NonNull Session session);

        @MainThread
        void taskNotExecutedDueToOfflineMode(@NonNull Session session);
    }

    InvalidatePipelinesTask(@NonNull Session session, @Nullable OnTaskFinished onTaskFinished) {
        super(session);
        this.mOnTaskFinished = onTaskFinished;
    }

    protected Boolean doInBackgroundAfterStoreSync(Void... params) {
        if (!ConnectionUtil.isConnected(getSession().getApplicationContext())) {
            return null;
        }
        new PipelineDataSource(getSession().getDatabase()).deleteAllPipelines();
        downloadPipelines(getSession());
        return Boolean.valueOf(true);
    }

    protected void onPostExecute(@Nullable Boolean succeeded) {
        boolean isOnTaskFinishedImplemented;
        boolean taskNotExecutedDueToOfflineMode = true;
        if (this.mOnTaskFinished != null) {
            isOnTaskFinishedImplemented = true;
        } else {
            isOnTaskFinishedImplemented = false;
        }
        if (isOnTaskFinishedImplemented) {
            if (succeeded != null) {
                taskNotExecutedDueToOfflineMode = false;
            }
            if (taskNotExecutedDueToOfflineMode) {
                this.mOnTaskFinished.taskNotExecutedDueToOfflineMode(getSession());
            } else if (succeeded.booleanValue()) {
                this.mOnTaskFinished.allPipelinesDownloaded(getSession());
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    @NonNull
    private List<Pipeline> downloadPipelines(@NonNull Session session) {
        List<Pipeline> pipelines = new ArrayList();
        InputStream in = getPipelinesListStream(session);
        if (in != null) {
            JsonReader reader = new JsonReader(new InputStreamReader(in, HttpRequest.CHARSET_UTF8));
            reader.beginObject();
            while (reader.hasNext()) {
                String contactsListField = reader.nextName();
                if (!Response.JSON_PARAM_SUCCESS.equals(contactsListField)) {
                    try {
                        if (Response.JSON_PARAM_DATA.equals(contactsListField) && reader.peek() == JsonToken.BEGIN_ARRAY) {
                            reader.beginArray();
                            while (reader.hasNext()) {
                                pipelines.add(readPipeline(reader));
                            }
                            reader.endArray();
                        } else {
                            reader.skipValue();
                        }
                    } catch (Throwable e) {
                        LogJourno.reportEvent(EVENT.Networking_downloadPipelinesFailed, e);
                        Log.e(e);
                    } catch (Throwable th) {
                        try {
                            in.close();
                        } catch (IOException e2) {
                        }
                    }
                } else if (!reader.nextBoolean()) {
                    reader.close();
                }
            }
            reader.endObject();
            reader.close();
            try {
                in.close();
            } catch (IOException e3) {
            }
            PipelineDataSource dataSource = new PipelineDataSource(session.getDatabase());
            for (Pipeline pipeline : pipelines) {
                dataSource.createOrUpdatePipeline(pipeline);
            }
        }
        return pipelines;
    }

    @Nullable
    private InputStream getPipelinesListStream(@NonNull Session session) {
        return ConnectionUtil.requestGet(new ApiUrlBuilder(session, "pipelines"));
    }

    public static Pipeline readPipeline(JsonReader reader) throws IOException {
        Pipeline pipeline = new Pipeline();
        reader.beginObject();
        while (reader.hasNext()) {
            String contactField = reader.nextName();
            if (PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID.equals(contactField)) {
                pipeline.setPipedriveId(Long.valueOf((long) reader.nextInt()));
            } else if ("name".equals(contactField) && reader.peek() != JsonToken.NULL) {
                pipeline.setName(reader.nextString());
            } else if (PipeSQLiteHelper.COLUMN_ORDER_NR.equals(contactField)) {
                if (reader.peek() == JsonToken.NULL) {
                    pipeline.setOrderNr(0);
                    reader.skipValue();
                } else {
                    pipeline.setOrderNr(reader.nextInt());
                }
            } else if (PipeSQLiteHelper.COLUMN_ACTIVE.equals(contactField)) {
                pipeline.setActive(reader.nextBoolean());
            } else if ("default".equals(contactField)) {
                pipeline.setDefault(reader.nextBoolean());
            } else if (PipeSQLiteHelper.COLUMN_SELECTED.equals(contactField)) {
                pipeline.setSelected(reader.nextBoolean());
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return pipeline;
    }
}
