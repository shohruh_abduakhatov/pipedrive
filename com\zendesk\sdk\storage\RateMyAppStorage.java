package com.zendesk.sdk.storage;

import android.content.Context;
import android.content.SharedPreferences;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.AppVersion;
import com.zendesk.sdk.model.settings.SafeMobileSettings;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.storage.SdkStorage.UserStorage;

public class RateMyAppStorage implements UserStorage {
    private static final String FEEDBACK_CONTENT = "zd_content";
    private static final String LOG_TAG = "RateMyAppStorage";
    private static final String PREFERENCES_FILE_NAME = "zd_rma";
    private static final String RMA_PREFERENCES_DONT_SHOW_AGAIN_FOR_VERSION = "rma_dont_show_again_for_version";
    private static final String RMA_PREFERENCES_NUMBER_OF_LAUNCHES = "rma_number_of_launches";
    private static final String RMA_PREFERENCES_RATED_FOR_VERSION = "rma_rated_for_version";
    private static final String RMA_PREFERENCES_SAVED_LAUNCH_TIME = "rma_saved_launch_time";
    private AppVersion mAppVersion;
    private SafeMobileSettings mMobileSettings;
    private SharedPreferences mSharedPreferences;

    public RateMyAppStorage(Context context) {
        if (context == null) {
            Logger.e(LOG_TAG, "Context is null, storage will not work", new Object[0]);
            return;
        }
        this.mSharedPreferences = context.getSharedPreferences(PREFERENCES_FILE_NAME, 0);
        this.mAppVersion = new AppVersion(context);
        this.mMobileSettings = ZendeskConfig.INSTANCE.getMobileSettings();
        ZendeskConfig.INSTANCE.storage().sdkStorage().registerUserStorage(this);
    }

    public int getNumberOfLaunches() {
        if (this.mSharedPreferences == null) {
            return 0;
        }
        return this.mSharedPreferences.getInt(RMA_PREFERENCES_NUMBER_OF_LAUNCHES, 0);
    }

    public long getCanShowAfterTime() {
        if (this.mSharedPreferences == null) {
            Logger.e(LOG_TAG, "getCanShowAfterTime() Storage is null, returning current time", new Object[0]);
            return System.currentTimeMillis();
        }
        long savedLaunchTime = this.mSharedPreferences.getLong(RMA_PREFERENCES_SAVED_LAUNCH_TIME, 0);
        if (savedLaunchTime == 0) {
            savedLaunchTime = System.currentTimeMillis();
            this.mSharedPreferences.edit().putLong(RMA_PREFERENCES_SAVED_LAUNCH_TIME, savedLaunchTime).apply();
        }
        return this.mMobileSettings.getRateMyAppDuration() + savedLaunchTime;
    }

    public void incrementNumberOfLaunches() {
        if (this.mSharedPreferences == null) {
            Logger.e(LOG_TAG, "incrementNumberOfLaunches() Storage is null, returning", new Object[0]);
            return;
        }
        int currentNumberOfLaunches = getNumberOfLaunches() + 1;
        Logger.d(LOG_TAG, "Incrementing number of RMA launches", new Object[0]);
        this.mSharedPreferences.edit().putInt(RMA_PREFERENCES_NUMBER_OF_LAUNCHES, currentNumberOfLaunches).apply();
    }

    public boolean isNumberOfLaunchesMet() {
        if (this.mMobileSettings.hasHelpCenterSettings()) {
            int currentNumberOfLaunches = getNumberOfLaunches();
            Logger.d(LOG_TAG, "Current launches is " + currentNumberOfLaunches + " and settings requires >= " + this.mMobileSettings.getRateMyAppVisits(), new Object[0]);
            if (((long) currentNumberOfLaunches) >= this.mMobileSettings.getRateMyAppVisits()) {
                return true;
            }
            return false;
        }
        Logger.d(LOG_TAG, "Settings are null", new Object[0]);
        return false;
    }

    public boolean isLaunchTimeMet() {
        if (this.mSharedPreferences == null) {
            Logger.e(LOG_TAG, "isLaunchTimeMet() Storage is null, returning false", new Object[0]);
            return false;
        }
        long canShowAfterTime = getCanShowAfterTime();
        long currentTime = System.currentTimeMillis();
        Logger.d(LOG_TAG, "Current time is " + currentTime + " and settings requires >= " + canShowAfterTime, new Object[0]);
        if (currentTime >= canShowAfterTime) {
            return true;
        }
        return false;
    }

    public boolean isRatedForCurrentVersion() {
        if (this.mSharedPreferences == null) {
            Logger.e(LOG_TAG, "isRatedForCurrentVersion() Storage is null, returning true", new Object[0]);
            return true;
        } else if (this.mSharedPreferences.getInt(RMA_PREFERENCES_RATED_FOR_VERSION, -1) != -1) {
            return true;
        } else {
            return false;
        }
    }

    public void setRatedForCurrentVersion() {
        if (this.mSharedPreferences == null) {
            Logger.e(LOG_TAG, "setRatedForCurrentVersion() Storage is null, returning", new Object[0]);
            return;
        }
        Logger.d(LOG_TAG, "Setting rated for version code %s", Integer.valueOf(this.mAppVersion.getAppVersionCode()));
        this.mSharedPreferences.edit().putInt(RMA_PREFERENCES_RATED_FOR_VERSION, appVersionCode).apply();
    }

    public boolean isDontShowAgain() {
        if (this.mSharedPreferences == null) {
            Logger.e(LOG_TAG, "isDontShowAgain() Storage is null, returning true", new Object[0]);
            return true;
        } else if (this.mSharedPreferences.getInt(RMA_PREFERENCES_DONT_SHOW_AGAIN_FOR_VERSION, -1) == -1) {
            return false;
        } else {
            return true;
        }
    }

    public void setDontShowAgain() {
        if (this.mSharedPreferences == null) {
            Logger.e(LOG_TAG, "setDontShowSagain() Storage is null, returning", new Object[0]);
            return;
        }
        Logger.d(LOG_TAG, "Setting don't show again for version code %s", Integer.valueOf(this.mAppVersion.getAppVersionCode()));
        this.mSharedPreferences.edit().putInt(RMA_PREFERENCES_DONT_SHOW_AGAIN_FOR_VERSION, appVersionCode).apply();
    }

    public String getSavedFeedback() {
        if (this.mSharedPreferences == null) {
            return null;
        }
        return this.mSharedPreferences.getString(FEEDBACK_CONTENT, null);
    }

    public void setSavedFeedback(String feedback) {
        if (this.mSharedPreferences == null) {
            Logger.e(LOG_TAG, "setSavedFeedback() Storage is null, returning", new Object[0]);
            return;
        }
        Logger.d(LOG_TAG, "Saving feedback to storage", new Object[0]);
        this.mSharedPreferences.edit().putString(FEEDBACK_CONTENT, feedback).apply();
    }

    public void reset() {
        if (this.mSharedPreferences == null) {
            Logger.e(LOG_TAG, "reset() Storage is null, returning", new Object[0]);
        } else {
            this.mSharedPreferences.edit().clear().apply();
        }
    }

    public void clearUserData() {
        if (this.mSharedPreferences == null) {
            Logger.e(LOG_TAG, "deleteSavedFeedback() Storage is null, returning", new Object[0]);
            return;
        }
        Logger.d(LOG_TAG, "Deleting saved feedback", new Object[0]);
        this.mSharedPreferences.edit().remove(FEEDBACK_CONTENT).apply();
    }

    public String getCacheKey() {
        return PREFERENCES_FILE_NAME;
    }
}
