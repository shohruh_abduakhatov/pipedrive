package com.zendesk.sdk.network;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.sdk.model.SdkConfiguration;
import com.zendesk.sdk.model.access.AccessToken;
import com.zendesk.sdk.model.settings.SafeMobileSettings;
import com.zendesk.service.ZendeskCallback;

public interface BaseProvider {
    void configureSdk(@Nullable ZendeskCallback<SdkConfiguration> zendeskCallback);

    void getAccessToken(@NonNull SafeMobileSettings safeMobileSettings, @Nullable ZendeskCallback<AccessToken> zendeskCallback);

    void getSdkSettings(@Nullable ZendeskCallback<SafeMobileSettings> zendeskCallback);
}
