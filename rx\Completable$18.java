package rx;

import rx.functions.Action0;
import rx.functions.Action1;

class Completable$18 implements Action0 {
    final /* synthetic */ Completable this$0;
    final /* synthetic */ Action1 val$onNotification;

    Completable$18(Completable completable, Action1 action1) {
        this.this$0 = completable;
        this.val$onNotification = action1;
    }

    public void call() {
        this.val$onNotification.call(Notification.createOnCompleted());
    }
}
