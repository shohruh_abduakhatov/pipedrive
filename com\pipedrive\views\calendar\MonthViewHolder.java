package com.pipedrive.views.calendar;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import java.util.Calendar;
import java.util.Locale;

class MonthViewHolder extends ViewHolder {
    public MonthViewHolder(@NonNull MonthView monthView) {
        super(monthView);
    }

    void bind(@NonNull Locale locale, int year, int month, @Nullable Calendar selectedDate) {
        ((MonthView) this.itemView).setup(locale, year, month, selectedDate);
    }
}
