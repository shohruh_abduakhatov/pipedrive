package com.pipedrive.views.edit.products;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.model.Currency;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.util.formatter.MonetaryFormatter;
import com.pipedrive.views.edit.products.DealProductNumericEditTextWithFormatting.OnValueChangedListener;

public class DealProductPriceEditText extends DealProductNumericEditTextWithFormatting {
    @Nullable
    private DealProduct mDealProduct;

    public DealProductPriceEditText(Context context) {
        this(context, null);
    }

    public DealProductPriceEditText(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DealProductPriceEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setup(@NonNull Session session, @NonNull DealProduct dealProduct, @Nullable OnValueChangedListener onValueChangedListener) {
        this.mDealProduct = dealProduct;
        super.setup(session, dealProduct, onValueChangedListener);
    }

    protected int getLabelTextResourceId() {
        return R.string.price;
    }

    @Nullable
    protected String getFormattedValue() {
        if (this.mDealProduct == null || getSession() == null || getCurrencyForFormatting() == null) {
            return null;
        }
        return new MonetaryFormatter(getSession()).format(Double.valueOf(getValue()), getCurrencyForFormatting().getCode());
    }

    double getInitialValue(@NonNull DealProduct dealProduct) {
        return dealProduct.getPrice().getValue().doubleValue();
    }

    @Nullable
    protected Currency getCurrencyForFormatting() {
        if (this.mDealProduct == null) {
            return null;
        }
        return this.mDealProduct.getPrice().getCurrency();
    }
}
