package com.pipedrive.flow.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import com.pipedrive.views.viewholder.ViewHolder;
import com.pipedrive.views.viewholder.ViewHolderBuilder;

public class RecyclerViewHolder<VH extends ViewHolder> extends RecyclerView.ViewHolder {
    private final View mItemView;

    public static <VH extends ViewHolder> RecyclerViewHolder wrapRowViewHolder(@NonNull Context context, @NonNull ViewGroup parent, @NonNull VH viewHolder) {
        return new RecyclerViewHolder(ViewHolderBuilder.inflateViewAndTagWithViewHolder(context, parent, false, viewHolder));
    }

    public RecyclerViewHolder(View itemView) {
        super(itemView);
        this.mItemView = itemView;
    }

    @Nullable
    public VH getViewHolder() {
        return (ViewHolder) this.mItemView.getTag();
    }
}
