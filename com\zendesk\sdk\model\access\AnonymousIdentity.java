package com.zendesk.sdk.model.access;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.network.impl.ZendeskConfig;

public class AnonymousIdentity implements Identity {
    private static final String LOG_TAG = "AnonymousIdentity";
    private String email;
    private String externalId;
    private String name;
    private String sdkGuid;

    public static class Builder {
        private static final String LOG_TAG = "AnonymousIdentityBuilder";
        private String email;
        private String externalId;
        private String name;
        private String sdkGuid = ZendeskConfig.INSTANCE.storage().identityStorage().getUUID();

        public Builder withNameIdentifier(String name) {
            this.name = name;
            return this;
        }

        public Builder withEmailIdentifier(String email) {
            this.email = email;
            return this;
        }

        @Deprecated
        public Builder withExternalIdentifier(String externalId) {
            Logger.w(LOG_TAG, "withExternalIdentifier is Deprecated in 1.8.0.1. This will be removed in the next release. You will need to use JWT authentication if you want to use external IDs", new Object[0]);
            this.externalId = externalId;
            return this;
        }

        public Identity build() {
            AnonymousIdentity user = new AnonymousIdentity();
            user.sdkGuid = this.sdkGuid;
            user.externalId = this.externalId;
            if (ZendeskConfig.INSTANCE.isCoppaEnabled()) {
                Logger.w(LOG_TAG, "Ignoring name and / or email because we are in COPPA mode", new Object[0]);
            } else {
                user.email = this.email;
                user.name = this.name;
            }
            return user;
        }
    }

    private AnonymousIdentity() {
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AnonymousIdentity that = (AnonymousIdentity) o;
        if (this.email == null ? that.email != null : !this.email.equals(that.email)) {
            return false;
        }
        if (this.externalId == null ? that.externalId != null : !this.externalId.equals(that.externalId)) {
            return false;
        }
        if (this.name == null ? that.name != null : !this.name.equals(that.name)) {
            return false;
        }
        if (this.sdkGuid != null) {
            if (this.sdkGuid.equals(that.sdkGuid)) {
                return true;
            }
        } else if (that.sdkGuid == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int result;
        int hashCode;
        int i = 0;
        if (this.sdkGuid != null) {
            result = this.sdkGuid.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.externalId != null) {
            hashCode = this.externalId.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.email != null) {
            hashCode = this.email.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (i2 + hashCode) * 31;
        if (this.name != null) {
            i = this.name.hashCode();
        }
        return hashCode + i;
    }

    @Nullable
    public String getEmail() {
        return ZendeskConfig.INSTANCE.isCoppaEnabled() ? null : this.email;
    }

    @Nullable
    public String getName() {
        return ZendeskConfig.INSTANCE.isCoppaEnabled() ? null : this.name;
    }

    @Nullable
    @Deprecated
    public String getExternalId() {
        Logger.w(LOG_TAG, "withExternalIdentifier is Deprecated in 1.8.0.1. This will be removed in the next release. You will need to use JWT authentication if you want to use external IDs", new Object[0]);
        return this.externalId;
    }

    @NonNull
    public String getSdkGuid() {
        return this.sdkGuid;
    }

    public void reloadGuid() {
        this.sdkGuid = ZendeskConfig.INSTANCE.storage().identityStorage().getUUID();
    }
}
