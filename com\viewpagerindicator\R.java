package com.viewpagerindicator;

public final class R {

    public static final class attr {
        public static final int centered = 2130771974;
        public static final int clipPadding = 2130772343;
        public static final int fadeDelay = 2130772372;
        public static final int fadeLength = 2130772373;
        public static final int fades = 2130772371;
        public static final int fillColor = 2130772162;
        public static final int footerColor = 2130772344;
        public static final int footerIndicatorHeight = 2130772347;
        public static final int footerIndicatorStyle = 2130772346;
        public static final int footerIndicatorUnderlinePadding = 2130772348;
        public static final int footerLineHeight = 2130772345;
        public static final int footerPadding = 2130772349;
        public static final int gapWidth = 2130772230;
        public static final int linePosition = 2130772350;
        public static final int lineWidth = 2130772229;
        public static final int pageColor = 2130772163;
        public static final int radius = 2130772164;
        public static final int selectedBold = 2130772351;
        public static final int selectedColor = 2130771983;
        public static final int snap = 2130772165;
        public static final int strokeColor = 2130772166;
        public static final int strokeWidth = 2130771984;
        public static final int titlePadding = 2130772352;
        public static final int topPadding = 2130772353;
        public static final int unselectedColor = 2130771986;
        public static final int vpiCirclePageIndicatorStyle = 2130772379;
        public static final int vpiIconPageIndicatorStyle = 2130772380;
        public static final int vpiLinePageIndicatorStyle = 2130772381;
        public static final int vpiTabPageIndicatorStyle = 2130772383;
        public static final int vpiTitlePageIndicatorStyle = 2130772382;
        public static final int vpiUnderlinePageIndicatorStyle = 2130772384;
    }

    public static final class bool {
        public static final int default_circle_indicator_centered = 2131623941;
        public static final int default_circle_indicator_snap = 2131623942;
        public static final int default_line_indicator_centered = 2131623943;
        public static final int default_title_indicator_selected_bold = 2131623944;
        public static final int default_underline_indicator_fades = 2131623945;
    }

    public static final class color {
        public static final int default_circle_indicator_fill_color = 2131755099;
        public static final int default_circle_indicator_page_color = 2131755100;
        public static final int default_circle_indicator_stroke_color = 2131755101;
        public static final int default_line_indicator_selected_color = 2131755102;
        public static final int default_line_indicator_unselected_color = 2131755103;
        public static final int default_title_indicator_footer_color = 2131755104;
        public static final int default_title_indicator_selected_color = 2131755105;
        public static final int default_title_indicator_text_color = 2131755106;
        public static final int default_underline_indicator_selected_color = 2131755107;
        public static final int vpi__background_holo_dark = 2131755247;
        public static final int vpi__background_holo_light = 2131755248;
        public static final int vpi__bright_foreground_disabled_holo_dark = 2131755249;
        public static final int vpi__bright_foreground_disabled_holo_light = 2131755250;
        public static final int vpi__bright_foreground_holo_dark = 2131755251;
        public static final int vpi__bright_foreground_holo_light = 2131755252;
        public static final int vpi__bright_foreground_inverse_holo_dark = 2131755253;
        public static final int vpi__bright_foreground_inverse_holo_light = 2131755254;
        public static final int vpi__dark_theme = 2131755296;
        public static final int vpi__light_theme = 2131755297;
    }

    public static final class dimen {
        public static final int default_circle_indicator_radius = 2131493019;
        public static final int default_circle_indicator_stroke_width = 2131493020;
        public static final int default_line_indicator_gap_width = 2131493024;
        public static final int default_line_indicator_line_width = 2131493025;
        public static final int default_line_indicator_stroke_width = 2131493026;
        public static final int default_title_indicator_clip_padding = 2131493027;
        public static final int default_title_indicator_footer_indicator_height = 2131493028;
        public static final int default_title_indicator_footer_indicator_underline_padding = 2131493029;
        public static final int default_title_indicator_footer_line_height = 2131493030;
        public static final int default_title_indicator_footer_padding = 2131493031;
        public static final int default_title_indicator_text_size = 2131493032;
        public static final int default_title_indicator_title_padding = 2131493033;
        public static final int default_title_indicator_top_padding = 2131493034;
    }

    public static final class drawable {
        public static final int vpi__tab_indicator = 2130837903;
        public static final int vpi__tab_selected_focused_holo = 2130837904;
        public static final int vpi__tab_selected_holo = 2130837905;
        public static final int vpi__tab_selected_pressed_holo = 2130837906;
        public static final int vpi__tab_unselected_focused_holo = 2130837907;
        public static final int vpi__tab_unselected_holo = 2130837908;
        public static final int vpi__tab_unselected_pressed_holo = 2130837909;
    }

    public static final class id {
        public static final int bottom = 2131820598;
        public static final int none = 2131820580;
        public static final int top = 2131820607;
        public static final int triangle = 2131820639;
        public static final int underline = 2131820640;
    }

    public static final class integer {
        public static final int default_circle_indicator_orientation = 2131689480;
        public static final int default_title_indicator_footer_indicator_style = 2131689482;
        public static final int default_title_indicator_line_position = 2131689483;
        public static final int default_underline_indicator_fade_delay = 2131689484;
        public static final int default_underline_indicator_fade_length = 2131689485;
    }

    public static final class style {
        public static final int TextAppearance_TabPageIndicator = 2131558778;
        public static final int Theme_PageIndicatorDefaults = 2131558810;
        public static final int Widget = 2131558828;
        public static final int Widget_IconPageIndicator = 2131558914;
        public static final int Widget_TabPageIndicator = 2131558931;
    }

    public static final class styleable {
        public static final int[] CirclePageIndicator = new int[]{16842948, 16842964, com.pipedrive.R.attr.centered, com.pipedrive.R.attr.strokeWidth, com.pipedrive.R.attr.fillColor, com.pipedrive.R.attr.pageColor, com.pipedrive.R.attr.radius, com.pipedrive.R.attr.snap, com.pipedrive.R.attr.strokeColor};
        public static final int CirclePageIndicator_android_background = 1;
        public static final int CirclePageIndicator_android_orientation = 0;
        public static final int CirclePageIndicator_centered = 2;
        public static final int CirclePageIndicator_fillColor = 4;
        public static final int CirclePageIndicator_pageColor = 5;
        public static final int CirclePageIndicator_radius = 6;
        public static final int CirclePageIndicator_snap = 7;
        public static final int CirclePageIndicator_strokeColor = 8;
        public static final int CirclePageIndicator_strokeWidth = 3;
        public static final int[] LinePageIndicator = new int[]{16842964, com.pipedrive.R.attr.centered, com.pipedrive.R.attr.selectedColor, com.pipedrive.R.attr.strokeWidth, com.pipedrive.R.attr.unselectedColor, com.pipedrive.R.attr.lineWidth, com.pipedrive.R.attr.gapWidth};
        public static final int LinePageIndicator_android_background = 0;
        public static final int LinePageIndicator_centered = 1;
        public static final int LinePageIndicator_gapWidth = 6;
        public static final int LinePageIndicator_lineWidth = 5;
        public static final int LinePageIndicator_selectedColor = 2;
        public static final int LinePageIndicator_strokeWidth = 3;
        public static final int LinePageIndicator_unselectedColor = 4;
        public static final int[] TitlePageIndicator = new int[]{16842901, 16842904, 16842964, com.pipedrive.R.attr.selectedColor, com.pipedrive.R.attr.clipPadding, com.pipedrive.R.attr.footerColor, com.pipedrive.R.attr.footerLineHeight, com.pipedrive.R.attr.footerIndicatorStyle, com.pipedrive.R.attr.footerIndicatorHeight, com.pipedrive.R.attr.footerIndicatorUnderlinePadding, com.pipedrive.R.attr.footerPadding, com.pipedrive.R.attr.linePosition, com.pipedrive.R.attr.selectedBold, com.pipedrive.R.attr.titlePadding, com.pipedrive.R.attr.topPadding};
        public static final int TitlePageIndicator_android_background = 2;
        public static final int TitlePageIndicator_android_textColor = 1;
        public static final int TitlePageIndicator_android_textSize = 0;
        public static final int TitlePageIndicator_clipPadding = 4;
        public static final int TitlePageIndicator_footerColor = 5;
        public static final int TitlePageIndicator_footerIndicatorHeight = 8;
        public static final int TitlePageIndicator_footerIndicatorStyle = 7;
        public static final int TitlePageIndicator_footerIndicatorUnderlinePadding = 9;
        public static final int TitlePageIndicator_footerLineHeight = 6;
        public static final int TitlePageIndicator_footerPadding = 10;
        public static final int TitlePageIndicator_linePosition = 11;
        public static final int TitlePageIndicator_selectedBold = 12;
        public static final int TitlePageIndicator_selectedColor = 3;
        public static final int TitlePageIndicator_titlePadding = 13;
        public static final int TitlePageIndicator_topPadding = 14;
        public static final int[] UnderlinePageIndicator = new int[]{16842964, com.pipedrive.R.attr.selectedColor, com.pipedrive.R.attr.fades, com.pipedrive.R.attr.fadeDelay, com.pipedrive.R.attr.fadeLength};
        public static final int UnderlinePageIndicator_android_background = 0;
        public static final int UnderlinePageIndicator_fadeDelay = 3;
        public static final int UnderlinePageIndicator_fadeLength = 4;
        public static final int UnderlinePageIndicator_fades = 2;
        public static final int UnderlinePageIndicator_selectedColor = 1;
        public static final int[] ViewPagerIndicator = new int[]{com.pipedrive.R.attr.vpiCirclePageIndicatorStyle, com.pipedrive.R.attr.vpiIconPageIndicatorStyle, com.pipedrive.R.attr.vpiLinePageIndicatorStyle, com.pipedrive.R.attr.vpiTitlePageIndicatorStyle, com.pipedrive.R.attr.vpiTabPageIndicatorStyle, com.pipedrive.R.attr.vpiUnderlinePageIndicatorStyle};
        public static final int ViewPagerIndicator_vpiCirclePageIndicatorStyle = 0;
        public static final int ViewPagerIndicator_vpiIconPageIndicatorStyle = 1;
        public static final int ViewPagerIndicator_vpiLinePageIndicatorStyle = 2;
        public static final int ViewPagerIndicator_vpiTabPageIndicatorStyle = 4;
        public static final int ViewPagerIndicator_vpiTitlePageIndicatorStyle = 3;
        public static final int ViewPagerIndicator_vpiUnderlinePageIndicatorStyle = 5;
    }
}
