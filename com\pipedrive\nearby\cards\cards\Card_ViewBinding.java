package com.pipedrive.nearby.cards.cards;

import android.content.res.Resources;
import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class Card_ViewBinding implements Unbinder {
    private Card target;
    private View view2131820556;
    private View view2131821168;

    @UiThread
    public Card_ViewBinding(final Card target, View source) {
        this.target = target;
        target.headerContainer = (ViewGroup) Utils.findRequiredViewAsType(source, R.id.nearbyCard.headerContainer, "field 'headerContainer'", ViewGroup.class);
        target.detailsContainer = (ViewGroup) Utils.findRequiredViewAsType(source, R.id.nearbyCard.detailsContainer, "field 'detailsContainer'", ViewGroup.class);
        View view = Utils.findRequiredView(source, R.id.nearbyCard.directionsFab, "field 'directionsFloatingActionButton' and method 'onDirectionsFabClicked'");
        target.directionsFloatingActionButton = (FloatingActionButton) Utils.castView(view, R.id.nearbyCard.directionsFab, "field 'directionsFloatingActionButton'", FloatingActionButton.class);
        this.view2131821168 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onDirectionsFabClicked();
            }
        });
        target.addressTextView = (TextView) Utils.findRequiredViewAsType(source, R.id.nearbyCard.address, "field 'addressTextView'", TextView.class);
        target.cardContainer = Utils.findRequiredView(source, R.id.nearbyCard.cardContainer, "field 'cardContainer'");
        view = Utils.findRequiredView(source, R.id.nearbyCard.directionsButton, "method 'onDirectionsButtonClicked'");
        this.view2131820556 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onDirectionsButtonClicked();
            }
        });
        Resources res = source.getContext().getResources();
        target.maxCardHeight = res.getDimensionPixelSize(R.dimen.nearby_card_height);
        target.fabContainerHeight = res.getDimensionPixelSize(R.dimen.nearby_card_fab_container_height);
        target.bottomMargin = res.getDimensionPixelSize(R.dimen.nearby_cards_bottom_margin);
        target.subtitleSeparator = res.getString(R.string.subtitle_separator);
    }

    @CallSuper
    public void unbind() {
        Card target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.headerContainer = null;
        target.detailsContainer = null;
        target.directionsFloatingActionButton = null;
        target.addressTextView = null;
        target.cardContainer = null;
        this.view2131821168.setOnClickListener(null);
        this.view2131821168 = null;
        this.view2131820556.setOnClickListener(null);
        this.view2131820556 = null;
    }
}
