package com.google.android.gms.wearable;

import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.wearable.internal.DataItemAssetParcelable;
import com.newrelic.agent.android.util.SafeJsonPrimitive;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class PutDataRequest extends AbstractSafeParcelable {
    public static final Creator<PutDataRequest> CREATOR = new zzh();
    public static final String WEAR_URI_SCHEME = "wear";
    private static final long aSn = TimeUnit.MINUTES.toMillis(30);
    private static final Random aSo = new SecureRandom();
    private final Bundle aSp;
    private long aSq;
    private byte[] afk;
    private final Uri mUri;
    final int mVersionCode;

    private PutDataRequest(int i, Uri uri) {
        this(i, uri, new Bundle(), null, aSn);
    }

    PutDataRequest(int i, Uri uri, Bundle bundle, byte[] bArr, long j) {
        this.mVersionCode = i;
        this.mUri = uri;
        this.aSp = bundle;
        this.aSp.setClassLoader(DataItemAssetParcelable.class.getClassLoader());
        this.afk = bArr;
        this.aSq = j;
    }

    public static PutDataRequest create(String str) {
        return zzy(zzro(str));
    }

    public static PutDataRequest createFromDataItem(DataItem dataItem) {
        PutDataRequest zzy = zzy(dataItem.getUri());
        for (Entry entry : dataItem.getAssets().entrySet()) {
            if (((DataItemAsset) entry.getValue()).getId() == null) {
                String str = "Cannot create an asset for a put request without a digest: ";
                String valueOf = String.valueOf((String) entry.getKey());
                throw new IllegalStateException(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            }
            zzy.putAsset((String) entry.getKey(), Asset.createFromRef(((DataItemAsset) entry.getValue()).getId()));
        }
        zzy.setData(dataItem.getData());
        return zzy;
    }

    public static PutDataRequest createWithAutoAppendedId(String str) {
        StringBuilder stringBuilder = new StringBuilder(str);
        if (!str.endsWith("/")) {
            stringBuilder.append("/");
        }
        stringBuilder.append("PN").append(aSo.nextLong());
        return new PutDataRequest(2, zzro(stringBuilder.toString()));
    }

    private static Uri zzro(String str) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("An empty path was supplied.");
        } else if (!str.startsWith("/")) {
            throw new IllegalArgumentException("A path must start with a single / .");
        } else if (!str.startsWith("//")) {
            return new Builder().scheme(WEAR_URI_SCHEME).path(str).build();
        } else {
            throw new IllegalArgumentException("A path must start with a single / .");
        }
    }

    public static PutDataRequest zzy(Uri uri) {
        return new PutDataRequest(2, uri);
    }

    public Asset getAsset(String str) {
        return (Asset) this.aSp.getParcelable(str);
    }

    public Map<String, Asset> getAssets() {
        Map hashMap = new HashMap();
        for (String str : this.aSp.keySet()) {
            hashMap.put(str, (Asset) this.aSp.getParcelable(str));
        }
        return Collections.unmodifiableMap(hashMap);
    }

    public byte[] getData() {
        return this.afk;
    }

    public Uri getUri() {
        return this.mUri;
    }

    public boolean hasAsset(String str) {
        return this.aSp.containsKey(str);
    }

    public boolean isUrgent() {
        return this.aSq == 0;
    }

    public PutDataRequest putAsset(String str, Asset asset) {
        zzaa.zzy(str);
        zzaa.zzy(asset);
        this.aSp.putParcelable(str, asset);
        return this;
    }

    public PutDataRequest removeAsset(String str) {
        this.aSp.remove(str);
        return this;
    }

    public PutDataRequest setData(byte[] bArr) {
        this.afk = bArr;
        return this;
    }

    public PutDataRequest setUrgent() {
        this.aSq = 0;
        return this;
    }

    public String toString() {
        return toString(Log.isLoggable(DataMap.TAG, 3));
    }

    public String toString(boolean z) {
        StringBuilder stringBuilder = new StringBuilder("PutDataRequest[");
        String valueOf = String.valueOf(this.afk == null ? SafeJsonPrimitive.NULL_STRING : Integer.valueOf(this.afk.length));
        stringBuilder.append(new StringBuilder(String.valueOf(valueOf).length() + 7).append("dataSz=").append(valueOf).toString());
        stringBuilder.append(", numAssets=" + this.aSp.size());
        valueOf = String.valueOf(this.mUri);
        stringBuilder.append(new StringBuilder(String.valueOf(valueOf).length() + 6).append(", uri=").append(valueOf).toString());
        stringBuilder.append(", syncDeadline=" + this.aSq);
        if (z) {
            stringBuilder.append("]\n  assets: ");
            for (String valueOf2 : this.aSp.keySet()) {
                String valueOf3 = String.valueOf(this.aSp.getParcelable(valueOf2));
                stringBuilder.append(new StringBuilder((String.valueOf(valueOf2).length() + 7) + String.valueOf(valueOf3).length()).append("\n    ").append(valueOf2).append(": ").append(valueOf3).toString());
            }
            stringBuilder.append("\n  ]");
            return stringBuilder.toString();
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzh.zza(this, parcel, i);
    }

    public Bundle zzcmg() {
        return this.aSp;
    }

    public long zzcmh() {
        return this.aSq;
    }
}
