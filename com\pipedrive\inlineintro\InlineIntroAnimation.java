package com.pipedrive.inlineintro;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.animation.PathInterpolatorCompat;

final class InlineIntroAnimation {
    @Nullable
    private ValueAnimator animator;

    InlineIntroAnimation() {
    }

    @NonNull
    private ValueAnimator getValueAnimator(int start, int end) {
        ValueAnimator animator = ValueAnimator.ofInt(new int[]{start, end});
        animator.setDuration(1200);
        animator.setInterpolator(PathInterpolatorCompat.create(0.2f, 0.0f, 0.2f, 1.0f));
        animator.setRepeatCount(-1);
        animator.setRepeatMode(2);
        return animator;
    }

    void startValueAnimationByYAxis(final InlineIntroPopup inlineIntroPopup, int startY, int endY, final int x) {
        this.animator = getValueAnimator(startY, endY);
        this.animator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                inlineIntroPopup.update(x, ((Integer) valueAnimator.getAnimatedValue()).intValue(), inlineIntroPopup.getWidth(), inlineIntroPopup.getHeight());
            }
        });
        this.animator.start();
    }

    void startValueAnimationByXAxis(final InlineIntroPopup inlineIntroPopup, int startX, int endX, final int y) {
        this.animator = getValueAnimator(startX, endX);
        this.animator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                inlineIntroPopup.update(((Integer) valueAnimator.getAnimatedValue()).intValue(), y, inlineIntroPopup.getWidth(), inlineIntroPopup.getHeight());
            }
        });
        this.animator.start();
    }

    void stop() {
        if (this.animator != null) {
            this.animator.removeAllUpdateListeners();
            this.animator.end();
        }
    }
}
