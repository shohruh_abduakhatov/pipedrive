package com.pipedrive.views.stageindicator;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.View.MeasureSpec;
import butterknife.BindColor;
import butterknife.ButterKnife;

public class StageIndicatorView extends View implements OnPageChangeListener {
    private static final int ACTIVE_COLOR = 2131755176;
    private static final int GAP_AND_LINE_HEIGHT = 2;
    private static final int IN_ACTIVE_COLOR = 2131755155;
    @BindColor(2131755176)
    int colorActive;
    @BindColor(2131755155)
    int colorInactive;
    private int mCurrentStage;
    private float mGapWidth;
    private float mLineWidth;
    private float mPageOffset;
    private final Paint mPaintActive;
    private final Paint mPaintInactive;
    private int mScrollState;
    private ViewPager mViewPager;

    public StageIndicatorView(Context context) {
        this(context, null);
    }

    public StageIndicatorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mPaintInactive = new Paint(1);
        this.mPaintActive = new Paint(1);
        if (!isInEditMode()) {
            ButterKnife.bind(this);
            this.mPaintInactive.setColor(this.colorInactive);
            this.mPaintActive.setColor(this.colorActive);
            float dimension = TypedValue.applyDimension(1, 2.0f, getResources().getDisplayMetrics());
            this.mGapWidth = dimension;
            setStrokeWidth(dimension);
        }
    }

    private void setStrokeWidth(float strokeWidth) {
        this.mPaintActive.setStrokeWidth(strokeWidth);
        this.mPaintInactive.setStrokeWidth(strokeWidth);
        invalidate();
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.mViewPager != null) {
            int count = this.mViewPager.getAdapter().getCount();
            if (count != 0 && count != 1) {
                if (this.mCurrentStage >= count) {
                    setCurrentItem(count - 1);
                    return;
                }
                float startX;
                this.mLineWidth = (((float) getWidth()) - (((float) (count - 1)) * this.mGapWidth)) / ((float) count);
                float lineWidthAndGap = this.mLineWidth + this.mGapWidth;
                float paddingTop = (float) getPaddingTop();
                float verticalOffset = paddingTop + (((((float) getHeight()) - paddingTop) - ((float) getPaddingBottom())) / 2.0f);
                float horizontalOffset = (float) getPaddingLeft();
                for (int i = 0; i < count; i++) {
                    startX = horizontalOffset + (((float) i) * lineWidthAndGap);
                    canvas.drawLine(startX, verticalOffset, startX + this.mLineWidth, verticalOffset, this.mPaintInactive);
                }
                startX = (horizontalOffset + (this.mPageOffset * lineWidthAndGap)) + (((float) this.mCurrentStage) * lineWidthAndGap);
                canvas.drawLine(startX, verticalOffset, startX + this.mLineWidth, verticalOffset, this.mPaintActive);
            }
        }
    }

    public void setViewPager(ViewPager viewPager) {
        if (this.mViewPager != viewPager) {
            if (this.mViewPager != null) {
                this.mViewPager.addOnPageChangeListener(null);
            }
            if (viewPager.getAdapter() == null) {
                throw new IllegalStateException("ViewPager does not have adapter instance.");
            }
            this.mViewPager = viewPager;
            this.mViewPager.addOnPageChangeListener(this);
            invalidate();
        }
    }

    private void setCurrentItem(int item) {
        if (this.mViewPager == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        this.mViewPager.setCurrentItem(item);
        this.mCurrentStage = item;
        invalidate();
    }

    public void onPageScrollStateChanged(int state) {
        this.mScrollState = state;
    }

    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        this.mCurrentStage = position;
        this.mPageOffset = positionOffset;
        invalidate();
    }

    public void onPageSelected(int position) {
        if (this.mScrollState == 0) {
            this.mCurrentStage = position;
            invalidate();
        }
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
    }

    private int measureWidth(int measureSpec) {
        float result;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        if (specMode == 1073741824 || this.mViewPager == null) {
            result = (float) specSize;
        } else {
            int count = this.mViewPager.getAdapter().getCount();
            result = (((float) (getPaddingLeft() + getPaddingRight())) + (((float) count) * this.mLineWidth)) + (((float) (count - 1)) * this.mGapWidth);
            if (specMode == Integer.MIN_VALUE) {
                result = Math.min(result, (float) specSize);
            }
        }
        return (int) Math.ceil((double) result);
    }

    private int measureHeight(int measureSpec) {
        float result;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        if (specMode == 1073741824) {
            result = (float) specSize;
        } else {
            result = (this.mPaintActive.getStrokeWidth() + ((float) getPaddingTop())) + ((float) getPaddingBottom());
            if (specMode == Integer.MIN_VALUE) {
                result = Math.min(result, (float) specSize);
            }
        }
        return (int) Math.ceil((double) result);
    }
}
