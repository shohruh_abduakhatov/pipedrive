package com.pipedrive.views.filter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.LayoutParams;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewStub;
import android.widget.ImageButton;
import com.pipedrive.R;
import com.pipedrive.util.ImageUtil;

public class FilterDrawerLayout extends DrawerLayout {
    @Nullable
    private final View mDrawerContentView;
    @Nullable
    private final View mDrawerView;
    private final int mEdgeGravityOfTheDrawer;

    public static abstract class DrawerLayoutListener {
        public abstract boolean onClear();

        public abstract void onClose();

        public boolean onApply() {
            return true;
        }
    }

    public FilterDrawerLayout(@NonNull Context context, @NonNull LayoutInflater layoutInflater, @NonNull View parentView, @LayoutRes int filterContentLayoutRes) {
        this(context, layoutInflater, parentView, filterContentLayoutRes, true);
    }

    public FilterDrawerLayout(@NonNull Context context, @NonNull LayoutInflater layoutInflater, @NonNull View parentView, @LayoutRes int filterContentLayoutRes, boolean showFooter) {
        boolean spanContentToTheEndOfFilterContent = true;
        super(context);
        this.mEdgeGravityOfTheDrawer = GravityCompat.END;
        this.mDrawerView = layoutInflater.inflate(R.layout.layout_filterdrawer, null);
        tintCloseIcon(this.mDrawerView);
        LayoutParams drawerViewLayout = new LayoutParams(getDrawableWidthInPixels(), -1);
        drawerViewLayout.gravity = GravityCompat.END;
        this.mDrawerView.setLayoutParams(drawerViewLayout);
        this.mDrawerView.setClickable(true);
        this.mDrawerView.findViewById(R.id.filterFooter).setVisibility(showFooter ? 0 : 8);
        ViewStub drawerContentViewStub = (ViewStub) this.mDrawerView.findViewById(R.id.filterContent);
        if (showFooter) {
            spanContentToTheEndOfFilterContent = false;
        }
        if (spanContentToTheEndOfFilterContent && (drawerContentViewStub.getLayoutParams() instanceof MarginLayoutParams)) {
            MarginLayoutParams layoutParams = (MarginLayoutParams) drawerContentViewStub.getLayoutParams();
            layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin, layoutParams.rightMargin, 0);
        }
        drawerContentViewStub.setLayoutResource(filterContentLayoutRes);
        this.mDrawerContentView = drawerContentViewStub.inflate();
        addView(parentView);
        addView(this.mDrawerView);
        setDrawerLayoutListener(null);
    }

    public FilterDrawerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mEdgeGravityOfTheDrawer = GravityCompat.END;
        this.mDrawerContentView = null;
        this.mDrawerView = null;
    }

    public FilterDrawerLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mEdgeGravityOfTheDrawer = GravityCompat.END;
        this.mDrawerContentView = null;
        this.mDrawerView = null;
    }

    public void setDrawerLayoutListener(@Nullable DrawerLayoutListener drawerLayoutListener) {
        setupOnClearListener(this.mDrawerView, drawerLayoutListener);
        setupOnCloseListener(this.mDrawerView, drawerLayoutListener);
        setupOnApplyListener(this.mDrawerView, drawerLayoutListener);
    }

    public void openDrawer() {
        openDrawer(GravityCompat.END);
    }

    public void closeDrawer() {
        closeDrawer(GravityCompat.END);
    }

    public boolean isDrawerOpen() {
        return isDrawerOpen(GravityCompat.END);
    }

    public void lockDrawer(boolean lockDrawer) {
        setDrawerLockMode(lockDrawer ? 1 : 0, GravityCompat.END);
    }

    @Nullable
    public View getDrawerContentView() {
        return this.mDrawerContentView;
    }

    private int getDrawableWidthInPixels() {
        return (int) (((float) getResources().getDisplayMetrics().widthPixels) - getResources().getDimension(R.dimen.filter_layout_delta_from_left));
    }

    private void tintCloseIcon(@NonNull View mDrawerView) {
        View filterButtonClose = mDrawerView.findViewById(R.id.filterButtonClose);
        if (filterButtonClose != null && (filterButtonClose instanceof ImageButton)) {
            ImageButton filterButtonCloseImageButton = (ImageButton) filterButtonClose;
            filterButtonCloseImageButton.setImageDrawable(ImageUtil.getTintedDrawable(filterButtonCloseImageButton.getDrawable(), ContextCompat.getColor(getContext(), R.color.dark)));
        }
    }

    private void setupOnClearListener(@Nullable View drawerView, @Nullable final DrawerLayoutListener drawerLayoutListener) {
        if (!(drawerView == null)) {
            drawerView.findViewById(R.id.filterButtonClear).setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (drawerLayoutListener == null) {
                        FilterDrawerLayout.this.closeDrawer();
                    } else if (drawerLayoutListener.onClear()) {
                        FilterDrawerLayout.this.closeDrawer();
                    }
                }
            });
        }
    }

    private void setupOnCloseListener(@Nullable View drawerView, @Nullable final DrawerLayoutListener drawerLayoutListener) {
        if (!(drawerView == null)) {
            drawerView.findViewById(R.id.filterButtonClose).setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    FilterDrawerLayout.this.closeDrawer();
                    if (!(drawerLayoutListener == null)) {
                        drawerLayoutListener.onClose();
                    }
                }
            });
        }
    }

    private void setupOnApplyListener(@Nullable View drawerView, @Nullable final DrawerLayoutListener drawerLayoutListener) {
        boolean cannotSetupOnApplyListener;
        boolean applyButtonNotPresent = true;
        if (drawerView == null) {
            cannotSetupOnApplyListener = true;
        } else {
            cannotSetupOnApplyListener = false;
        }
        if (!cannotSetupOnApplyListener) {
            if (drawerView.findViewById(R.id.filterButtonApply) != null) {
                applyButtonNotPresent = false;
            }
            if (!applyButtonNotPresent) {
                drawerView.findViewById(R.id.filterButtonApply).setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        if (drawerLayoutListener == null) {
                            FilterDrawerLayout.this.closeDrawer();
                        } else if (drawerLayoutListener.onApply()) {
                            FilterDrawerLayout.this.closeDrawer();
                        }
                    }
                });
            }
        }
    }
}
