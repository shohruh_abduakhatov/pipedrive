package com.pipedrive.views.viewholder.contact;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import butterknife.internal.Utils;
import com.pipedrive.R;
import com.pipedrive.views.profilepicture.ProfilePictureRoundPerson;

public class PersonRowViewHolder_ViewBinding extends ContactRowViewHolder_ViewBinding {
    private PersonRowViewHolder target;

    @UiThread
    public PersonRowViewHolder_ViewBinding(PersonRowViewHolder target, View source) {
        super(target, source);
        this.target = target;
        target.mCallButton = (ImageView) Utils.findRequiredViewAsType(source, R.id.call, "field 'mCallButton'", ImageView.class);
        target.mProfilePictureRoundPerson = (ProfilePictureRoundPerson) Utils.findRequiredViewAsType(source, R.id.profilePicture, "field 'mProfilePictureRoundPerson'", ProfilePictureRoundPerson.class);
    }

    public void unbind() {
        PersonRowViewHolder target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mCallButton = null;
        target.mProfilePictureRoundPerson = null;
        super.unbind();
    }
}
