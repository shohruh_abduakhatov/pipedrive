package com.pipedrive.changes;

interface ChangeTypeIdentifiers {
    public static final int CHANGE_TYPE_IDENTIFIER_OF_ACTIVITY_CHANGER = 40;
    public static final int CHANGE_TYPE_IDENTIFIER_OF_DEAL_CHANGER = 30;
    public static final int CHANGE_TYPE_IDENTIFIER_OF_DEAL_PRODUCTS_CHANGER = 35;
    public static final int CHANGE_TYPE_IDENTIFIER_OF_FLOW_FILE_CHANGER = 100;
    public static final int CHANGE_TYPE_IDENTIFIER_OF_NOTE_CHANGER = 50;
    public static final int CHANGE_TYPE_IDENTIFIER_OF_ORGANIZATION_CHANGER = 10;
    public static final int CHANGE_TYPE_IDENTIFIER_OF_PERSON_CHANGER = 20;
}
