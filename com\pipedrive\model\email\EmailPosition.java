package com.pipedrive.model.email;

import android.text.TextUtils;

public enum EmailPosition {
    TO,
    CC,
    BCC,
    FROM;

    public String toString() {
        return super.toString().toLowerCase();
    }

    public static EmailPosition from(String value) {
        EmailPosition emailPosition = null;
        if (!TextUtils.isEmpty(value)) {
            try {
                emailPosition = valueOf(value.toUpperCase());
            } catch (IllegalArgumentException e) {
            }
        }
        return emailPosition;
    }
}
