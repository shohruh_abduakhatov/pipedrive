package rx;

import rx.functions.Action0;
import rx.functions.Action1;
import rx.plugins.RxJavaHooks;
import rx.subscriptions.MultipleAssignmentSubscription;

class Completable$29 implements CompletableSubscriber {
    boolean done;
    final /* synthetic */ Completable this$0;
    final /* synthetic */ MultipleAssignmentSubscription val$mad;
    final /* synthetic */ Action0 val$onComplete;
    final /* synthetic */ Action1 val$onError;

    Completable$29(Completable completable, Action0 action0, MultipleAssignmentSubscription multipleAssignmentSubscription, Action1 action1) {
        this.this$0 = completable;
        this.val$onComplete = action0;
        this.val$mad = multipleAssignmentSubscription;
        this.val$onError = action1;
    }

    public void onCompleted() {
        if (!this.done) {
            this.done = true;
            try {
                this.val$onComplete.call();
                this.val$mad.unsubscribe();
            } catch (Throwable e) {
                callOnError(e);
            }
        }
    }

    public void onError(Throwable e) {
        if (this.done) {
            RxJavaHooks.onError(e);
            Completable.deliverUncaughtException(e);
            return;
        }
        this.done = true;
        callOnError(e);
    }

    void callOnError(Throwable e) {
        Throwable e2;
        Throwable th;
        try {
            this.val$onError.call(e);
            this.val$mad.unsubscribe();
        } catch (Throwable th2) {
            th = th2;
            e = e2;
            this.val$mad.unsubscribe();
            throw th;
        }
    }

    public void onSubscribe(Subscription d) {
        this.val$mad.set(d);
    }
}
