package com.zendesk.sdk.feedback;

import java.util.List;

public class WrappedZendeskFeedbackConfiguration implements ZendeskFeedbackConfiguration {
    private String mAdditionalInfo;
    private String mRequestSubject;
    private List<String> mTags;

    public WrappedZendeskFeedbackConfiguration(ZendeskFeedbackConfiguration configuration) {
        if (configuration != null) {
            this.mTags = configuration.getTags();
            this.mAdditionalInfo = configuration.getAdditionalInfo();
            this.mRequestSubject = configuration.getRequestSubject();
        }
    }

    public List<String> getTags() {
        return this.mTags;
    }

    public String getAdditionalInfo() {
        return this.mAdditionalInfo;
    }

    public String getRequestSubject() {
        return this.mRequestSubject;
    }
}
