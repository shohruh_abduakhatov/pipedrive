package com.pipedrive.more;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class AboutPipedriveActivity_ViewBinding implements Unbinder {
    private AboutPipedriveActivity target;
    private View view2131820694;
    private View view2131820695;
    private View view2131820696;
    private View view2131820697;
    private View view2131820698;

    @UiThread
    public AboutPipedriveActivity_ViewBinding(AboutPipedriveActivity target) {
        this(target, target.getWindow().getDecorView());
    }

    @UiThread
    public AboutPipedriveActivity_ViewBinding(final AboutPipedriveActivity target, View source) {
        this.target = target;
        View view = Utils.findRequiredView(source, R.id.privacyPolicy, "method 'onPolicyClick'");
        this.view2131820695 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onPolicyClick();
            }
        });
        view = Utils.findRequiredView(source, R.id.termsOfService, "method 'onTermsClick'");
        this.view2131820696 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onTermsClick();
            }
        });
        view = Utils.findRequiredView(source, R.id.betaTesting, "method 'onBetaTestingClick'");
        this.view2131820698 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onBetaTestingClick();
            }
        });
        view = Utils.findRequiredView(source, R.id.changelog, "method 'onChangelogClick'");
        this.view2131820697 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onChangelogClick();
            }
        });
        view = Utils.findRequiredView(source, R.id.openSourceLicences, "method 'onLicencesClick'");
        this.view2131820694 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onLicencesClick();
            }
        });
    }

    @CallSuper
    public void unbind() {
        if (this.target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        this.view2131820695.setOnClickListener(null);
        this.view2131820695 = null;
        this.view2131820696.setOnClickListener(null);
        this.view2131820696 = null;
        this.view2131820698.setOnClickListener(null);
        this.view2131820698 = null;
        this.view2131820697.setOnClickListener(null);
        this.view2131820697 = null;
        this.view2131820694.setOnClickListener(null);
        this.view2131820694 = null;
    }
}
