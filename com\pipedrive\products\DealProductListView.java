package com.pipedrive.products;

import android.support.annotation.NonNull;
import com.pipedrive.model.Deal;
import com.pipedrive.model.products.DealProduct;
import java.util.List;

interface DealProductListView {
    void hideProgressBar();

    void onDealProductListRequestFail();

    void onDealProductListRequestSuccess(@NonNull List<DealProduct> list, @NonNull Deal deal);

    void onLastDealProductRemoved();

    void showProgressBar();
}
