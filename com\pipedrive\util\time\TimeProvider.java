package com.pipedrive.util.time;

import android.support.annotation.NonNull;
import java.util.TimeZone;

abstract class TimeProvider {
    private TimeZone utcTimeZone = TimeZone.getTimeZone("UTC");

    @NonNull
    abstract Long getCurrentTimeMillis();

    @NonNull
    abstract TimeZone getDefaultTimeZone();

    TimeProvider() {
    }

    @NonNull
    TimeZone getUtcTimeZone() {
        return this.utcTimeZone;
    }
}
