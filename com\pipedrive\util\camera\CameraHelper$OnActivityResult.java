package com.pipedrive.util.camera;

import android.content.Intent;

class CameraHelper$OnActivityResult {
    final Intent data;
    final int requestCode;
    final int resultCode;

    public CameraHelper$OnActivityResult(int requestCode, int resultCode, Intent data) {
        this.requestCode = requestCode;
        this.resultCode = resultCode;
        this.data = data;
    }
}
