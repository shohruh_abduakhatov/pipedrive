package com.pipedrive.views.profilepicture;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import com.nostra13.universalimageloader.core.display.BitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.pipedrive.R;

public class ProfilePictureRoundUserGrey extends ProfilePictureRoundUser {
    public ProfilePictureRoundUserGrey(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected void setUpLayout(Context context) {
        LayoutInflater.from(context).inflate(R.layout.view_profilepicture_grey, this);
    }

    @Nullable
    protected BitmapDisplayer getBitmapDisplayer() {
        return new RoundedBitmapDisplayer(Math.round(getContext().getResources().getDimension(R.dimen.dimen8)), Math.round(getContext().getResources().getDimension(R.dimen.more.profilePicture.margin)));
    }
}
