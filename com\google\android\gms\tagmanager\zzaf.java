package com.google.android.gms.tagmanager;

import android.util.Base64;
import com.google.android.gms.internal.zzag;
import com.google.android.gms.internal.zzah;
import com.google.android.gms.internal.zzaj.zza;
import com.pipedrive.model.customfields.CustomField;
import java.util.Map;

class zzaf extends zzam {
    private static final String ID = zzag.ENCODE.toString();
    private static final String aFk = zzah.ARG0.toString();
    private static final String aFl = zzah.NO_PADDING.toString();
    private static final String aFm = zzah.INPUT_FORMAT.toString();
    private static final String aFn = zzah.OUTPUT_FORMAT.toString();

    public zzaf() {
        super(ID, aFk);
    }

    public zza zzay(Map<String, zza> map) {
        zza com_google_android_gms_internal_zzaj_zza = (zza) map.get(aFk);
        if (com_google_android_gms_internal_zzaj_zza == null || com_google_android_gms_internal_zzaj_zza == zzdm.zzchm()) {
            return zzdm.zzchm();
        }
        String zzg = zzdm.zzg(com_google_android_gms_internal_zzaj_zza);
        com_google_android_gms_internal_zzaj_zza = (zza) map.get(aFm);
        if (com_google_android_gms_internal_zzaj_zza == null) {
            Object obj = CustomField.FIELD_DATA_TYPE_LARGE_TEXT;
        } else {
            String zzg2 = zzdm.zzg(com_google_android_gms_internal_zzaj_zza);
        }
        com_google_android_gms_internal_zzaj_zza = (zza) map.get(aFn);
        if (com_google_android_gms_internal_zzaj_zza == null) {
            Object obj2 = "base16";
        } else {
            String zzg3 = zzdm.zzg(com_google_android_gms_internal_zzaj_zza);
        }
        com_google_android_gms_internal_zzaj_zza = (zza) map.get(aFl);
        int i = (com_google_android_gms_internal_zzaj_zza == null || !zzdm.zzk(com_google_android_gms_internal_zzaj_zza).booleanValue()) ? 2 : 3;
        try {
            byte[] bytes;
            String valueOf;
            Object zzq;
            if (CustomField.FIELD_DATA_TYPE_LARGE_TEXT.equals(obj)) {
                bytes = zzg.getBytes();
            } else if ("base16".equals(obj)) {
                bytes = zzk.zzos(zzg);
            } else if ("base64".equals(obj)) {
                bytes = Base64.decode(zzg, i);
            } else if ("base64url".equals(obj)) {
                bytes = Base64.decode(zzg, i | 8);
            } else {
                zzg3 = "Encode: unknown input format: ";
                valueOf = String.valueOf(obj);
                zzbo.e(valueOf.length() != 0 ? zzg3.concat(valueOf) : new String(zzg3));
                return zzdm.zzchm();
            }
            if ("base16".equals(obj2)) {
                zzq = zzk.zzq(bytes);
            } else if ("base64".equals(obj2)) {
                zzq = Base64.encodeToString(bytes, i);
            } else if ("base64url".equals(obj2)) {
                zzq = Base64.encodeToString(bytes, i | 8);
            } else {
                zzg2 = "Encode: unknown output format: ";
                valueOf = String.valueOf(obj2);
                zzbo.e(valueOf.length() != 0 ? zzg2.concat(valueOf) : new String(zzg2));
                return zzdm.zzchm();
            }
            return zzdm.zzat(zzq);
        } catch (IllegalArgumentException e) {
            zzbo.e("Encode: invalid input:");
            return zzdm.zzchm();
        }
    }

    public boolean zzcdu() {
        return true;
    }
}
