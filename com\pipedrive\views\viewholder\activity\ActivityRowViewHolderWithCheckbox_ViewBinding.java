package com.pipedrive.views.viewholder.activity;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.CheckBox;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class ActivityRowViewHolderWithCheckbox_ViewBinding extends ActivityRowViewHolder_ViewBinding {
    private ActivityRowViewHolderWithCheckbox target;

    @UiThread
    public ActivityRowViewHolderWithCheckbox_ViewBinding(ActivityRowViewHolderWithCheckbox target, View source) {
        super(target, source);
        this.target = target;
        target.mDone = (CheckBox) Utils.findRequiredViewAsType(source, R.id.done, "field 'mDone'", CheckBox.class);
    }

    public void unbind() {
        ActivityRowViewHolderWithCheckbox target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mDone = null;
        super.unbind();
    }
}
