package com.google.android.gms.wearable.internal;

import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.wearable.ChannelIOException;
import java.io.IOException;
import java.io.InputStream;

public final class zzp extends InputStream {
    private final InputStream aTw;
    private volatile zzm aTx;

    public zzp(InputStream inputStream) {
        this.aTw = (InputStream) zzaa.zzy(inputStream);
    }

    private int zzadv(int i) throws ChannelIOException {
        if (i == -1) {
            zzm com_google_android_gms_wearable_internal_zzm = this.aTx;
            if (com_google_android_gms_wearable_internal_zzm != null) {
                throw new ChannelIOException("Channel closed unexpectedly before stream was finished", com_google_android_gms_wearable_internal_zzm.aTn, com_google_android_gms_wearable_internal_zzm.aTo);
            }
        }
        return i;
    }

    public int available() throws IOException {
        return this.aTw.available();
    }

    public void close() throws IOException {
        this.aTw.close();
    }

    public void mark(int i) {
        this.aTw.mark(i);
    }

    public boolean markSupported() {
        return this.aTw.markSupported();
    }

    public int read() throws IOException {
        return zzadv(this.aTw.read());
    }

    public int read(byte[] bArr) throws IOException {
        return zzadv(this.aTw.read(bArr));
    }

    public int read(byte[] bArr, int i, int i2) throws IOException {
        return zzadv(this.aTw.read(bArr, i, i2));
    }

    public void reset() throws IOException {
        this.aTw.reset();
    }

    public long skip(long j) throws IOException {
        return this.aTw.skip(j);
    }

    void zza(zzm com_google_android_gms_wearable_internal_zzm) {
        this.aTx = (zzm) zzaa.zzy(com_google_android_gms_wearable_internal_zzm);
    }

    zzu zzcmt() {
        return new zzu(this) {
            final /* synthetic */ zzp aTy;

            {
                this.aTy = r1;
            }

            public void zzb(zzm com_google_android_gms_wearable_internal_zzm) {
                this.aTy.zza(com_google_android_gms_wearable_internal_zzm);
            }
        };
    }
}
