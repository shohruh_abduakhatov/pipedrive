package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.auth.api.proxy.ProxyResponse;
import com.google.android.gms.internal.zznz.zza;

public class zznx extends zza {
    public void zza(ProxyResponse proxyResponse) throws RemoteException {
        throw new UnsupportedOperationException();
    }

    public void zzfy(String str) throws RemoteException {
        throw new UnsupportedOperationException();
    }
}
