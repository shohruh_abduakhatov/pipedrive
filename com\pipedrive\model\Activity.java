package com.pipedrive.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datetime.PipedriveDate;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.datetime.PipedriveDuration;
import com.pipedrive.datetime.PipedriveTime;
import com.pipedrive.model.delta.ModelProperty;
import com.pipedrive.util.StringUtils;
import com.pipedrive.util.clone.Cloner;
import com.pipedrive.util.networking.entities.ActivityEntity;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class Activity extends BaseCloneableDatasourceEntity<Activity> implements Parcelable, AssociatedDatasourceEntity, HtmlContent {
    public static final Creator CREATOR = new Creator() {
        public Activity createFromParcel(Parcel in) {
            return new Activity(in);
        }

        public Activity[] newArray(int size) {
            return new Activity[size];
        }
    };
    public static final long DEFAULT_ACTIVITY_DURATION_SECONDS = TimeUnit.MINUTES.toSeconds(20);
    private static final int FIELD_VALUE_DONE_DONE = 1;
    private static final int FIELD_VALUE_DONE_UNDONE = 0;
    @Nullable
    @ModelProperty
    private Long activityStartInSeconds;
    @Nullable
    private ActivityStartType activityStartType;
    @ModelProperty
    private long assignedUserId;
    @ModelProperty
    private Deal deal;
    @ModelProperty
    private boolean done;
    @Nullable
    @ModelProperty
    private PipedriveDuration duration;
    private boolean isActive;
    private String note;
    @ModelProperty
    private Organization organization;
    @ModelProperty
    private Person person;
    @Nullable
    @ModelProperty
    private String subject;
    @ModelProperty
    private ActivityType type;

    public static abstract class DateTimeInfo {
        @MainThread
        public abstract void activityHasStartDate(@NonNull PipedriveDate pipedriveDate, @NonNull Activity activity);

        @MainThread
        public abstract void activityHasStartDateTime(@NonNull PipedriveDateTime pipedriveDateTime, @NonNull Activity activity);

        @MainThread
        public void activityIsCorruptedToReturnDateTimeInfo(@NonNull Activity corruptedActivity) {
        }
    }

    public static abstract class DateTimeInfoWithReturn<BACKPACK_TO_RETURN> {
        @MainThread
        public abstract BACKPACK_TO_RETURN activityHasStartDate(@NonNull PipedriveDate pipedriveDate, @NonNull Activity activity);

        @MainThread
        public abstract BACKPACK_TO_RETURN activityHasStartDateTime(@NonNull PipedriveDateTime pipedriveDateTime, @NonNull Activity activity);

        @Nullable
        @MainThread
        public BACKPACK_TO_RETURN activityIsCorruptedToReturnDateTimeInfo(@NonNull Activity corruptedActivity) {
            return null;
        }
    }

    public static class ActivityBuilderForDataSource {
        @NonNull
        private final Activity mActivityToBuild;

        public enum ActivityStartDateTimeType {
            DateTime(0),
            Date(1);
            
            private final int valueForSql;

            private ActivityStartDateTimeType(int valueForSql) {
                this.valueForSql = valueForSql;
            }

            public int getSqlValue() {
                return this.valueForSql;
            }
        }

        public ActivityBuilderForDataSource(@NonNull Session session) {
            this.mActivityToBuild = new Activity(session);
        }

        public ActivityBuilderForDataSource setActivityStartAndEnd(@Nullable Long startInSeconds, @Nullable Long endInSeconds, @Nullable Integer startDateTimeTypeSqlValue) {
            Activity activity = this.mActivityToBuild;
            ActivityStartType activityStartType = startDateTimeTypeSqlValue == null ? null : startDateTimeTypeSqlValue.intValue() == ActivityStartDateTimeType.Date.getSqlValue() ? ActivityStartType.PipedriveDate : startDateTimeTypeSqlValue.intValue() == ActivityStartDateTimeType.DateTime.getSqlValue() ? ActivityStartType.PipedriveDateTime : null;
            activity.activityStartType = activityStartType;
            this.mActivityToBuild.activityStartInSeconds = startInSeconds;
            Long durationInSeconds = (endInSeconds == null || startInSeconds == null) ? null : Long.valueOf(endInSeconds.longValue() - startInSeconds.longValue());
            this.mActivityToBuild.setDuration(PipedriveDuration.instanceFromUnixTimeRepresentation(durationInSeconds));
            return this;
        }

        public Activity getActivityBeingBuilt() {
            return this.mActivityToBuild;
        }
    }

    public static class ActivityDeflaterForDataSource {
        @NonNull
        private final Activity mActivityToDeflate;

        public ActivityDeflaterForDataSource(@NonNull Activity activityToDeflate) {
            this.mActivityToDeflate = activityToDeflate;
        }

        @Nullable
        public Long getActivityStartInSeconds() {
            return this.mActivityToDeflate.activityStartInSeconds;
        }

        @Nullable
        public Long getActivityEndInSeconds() {
            boolean noActivityEndToBeDefined = this.mActivityToDeflate.activityStartInSeconds == null || this.mActivityToDeflate.duration == null;
            if (noActivityEndToBeDefined) {
                return null;
            }
            return Long.valueOf(this.mActivityToDeflate.activityStartInSeconds.longValue() + this.mActivityToDeflate.duration.getRepresentationInUnixTime());
        }

        @Nullable
        public Integer getStartDateTimeType() {
            if (this.mActivityToDeflate.activityStartType == ActivityStartType.PipedriveDate) {
                return Integer.valueOf(ActivityStartDateTimeType.Date.getSqlValue());
            }
            if (this.mActivityToDeflate.activityStartType == ActivityStartType.PipedriveDateTime) {
                return Integer.valueOf(ActivityStartDateTimeType.DateTime.getSqlValue());
            }
            return null;
        }
    }

    private enum ActivityStartType {
        PipedriveDate,
        PipedriveDateTime
    }

    @Nullable
    public static Activity fromEntityReturnedWithoutAssociations(@NonNull Session session, @Nullable ActivityEntity activityEntity) {
        if (activityEntity == null) {
            return null;
        }
        Activity activity = new Activity();
        activity.setPipedriveId(activityEntity.pipedriveId);
        PipedriveDate dueDateFromApi = activityEntity.dueDate == null ? PipedriveDate.instanceWithCurrentDate() : PipedriveDate.instanceFrom(activityEntity.dueDate.getTime());
        PipedriveTime dueTimeFromApi = PipedriveTime.instanceFromDateTakeTimeOnly(activityEntity.dueTime);
        PipedriveDuration durationFromApi = PipedriveDuration.instanceFromDate(activityEntity.duration);
        if (dueTimeFromApi != null) {
            activity.setActivityStartAt(PipedriveDateTime.instanceFromAddition(dueDateFromApi, dueTimeFromApi), durationFromApi);
        } else {
            activity.setActivityStartAt(dueDateFromApi, durationFromApi);
        }
        if (activityEntity.activeFlag != null) {
            activity.setActive(activityEntity.activeFlag.booleanValue());
        }
        activity.setSubject(activityEntity.subject);
        activity.setNote(activityEntity.note);
        if (activityEntity.type != null) {
            activity.setType(ActivityType.getActivityTypeByName(session, activityEntity.type));
        }
        if (activityEntity.assignedToUserId != null) {
            activity.setAssignedUserId(activityEntity.assignedToUserId.longValue());
        }
        if (activityEntity.done == null) {
            return activity;
        }
        activity.setDone(activityEntity.done.booleanValue());
        return activity;
    }

    public Activity() {
        this.isActive = true;
        this.person = null;
        this.organization = null;
        this.deal = null;
    }

    public Activity(Session session) {
        this.isActive = true;
        this.person = null;
        this.organization = null;
        this.deal = null;
        setType(ActivityType.getDefaultActivityType(session));
    }

    private Activity(Parcel in) {
        this.isActive = true;
        this.person = null;
        this.organization = null;
        this.deal = null;
        readFromParcel(in);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(@NonNull Parcel dest, int flags) {
        long j = Long.MIN_VALUE;
        int i = 1;
        super.writeToParcel(dest, flags);
        dest.writeString(this.subject);
        dest.writeString(this.note);
        dest.writeInt(this.done ? 1 : 0);
        dest.writeParcelable(getType(), flags);
        dest.writeLong(this.duration == null ? Long.MIN_VALUE : this.duration.getRepresentationInUnixTime());
        dest.writeLong(this.assignedUserId);
        if (!this.isActive) {
            i = 0;
        }
        dest.writeInt(i);
        dest.writeParcelable(getPerson(), flags);
        dest.writeParcelable(getOrganization(), flags);
        dest.writeParcelable(getDeal(), flags);
        if (this.activityStartInSeconds != null) {
            j = this.activityStartInSeconds.longValue();
        }
        dest.writeLong(j);
        dest.writeInt(this.activityStartType == null ? Integer.MIN_VALUE : this.activityStartType.ordinal());
    }

    protected void readFromParcel(@NonNull Parcel in) {
        boolean z;
        boolean durationValueExist;
        PipedriveDuration instanceFromUnixTimeRepresentation;
        boolean activityStartInUnixTimeExist;
        Long valueOf;
        ActivityStartType activityStartType = null;
        super.readFromParcel(in);
        this.subject = in.readString();
        this.note = in.readString();
        if (in.readInt() == 1) {
            z = true;
        } else {
            z = false;
        }
        this.done = z;
        this.type = (ActivityType) in.readParcelable(ActivityType.class.getClassLoader());
        Long durationValue = Long.valueOf(in.readLong());
        if (durationValue.compareTo(Long.valueOf(Long.MIN_VALUE)) != 0) {
            durationValueExist = true;
        } else {
            durationValueExist = false;
        }
        if (durationValueExist) {
            instanceFromUnixTimeRepresentation = PipedriveDuration.instanceFromUnixTimeRepresentation(durationValue);
        } else {
            instanceFromUnixTimeRepresentation = null;
        }
        this.duration = instanceFromUnixTimeRepresentation;
        this.assignedUserId = in.readLong();
        if (in.readInt() == 1) {
            z = true;
        } else {
            z = false;
        }
        this.isActive = z;
        this.person = (Person) in.readParcelable(Person.class.getClassLoader());
        this.organization = (Organization) in.readParcelable(Organization.class.getClassLoader());
        this.deal = (Deal) in.readParcelable(Deal.class.getClassLoader());
        long activityStartInUnixTime = in.readLong();
        if (activityStartInUnixTime != Long.MIN_VALUE) {
            activityStartInUnixTimeExist = true;
        } else {
            activityStartInUnixTimeExist = false;
        }
        if (activityStartInUnixTimeExist) {
            valueOf = Long.valueOf(activityStartInUnixTime);
        } else {
            valueOf = null;
        }
        this.activityStartInSeconds = valueOf;
        int activityStartTypeOrdinal = in.readInt();
        if (activityStartTypeOrdinal == 0) {
            activityStartType = ActivityStartType.PipedriveDate;
        } else if (activityStartTypeOrdinal == 1) {
            activityStartType = ActivityStartType.PipedriveDateTime;
        }
        this.activityStartType = activityStartType;
    }

    public void setSubject(@Nullable String subject) {
        this.subject = subject;
    }

    @Nullable
    public String getSubject() {
        return this.subject;
    }

    public void setNote(@Nullable String note) {
        this.note = note;
    }

    @Nullable
    public String getNote() {
        return this.note;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public boolean isDone() {
        return this.done;
    }

    @Nullable
    @Deprecated
    public PipedriveTime getDueTime() {
        PipedriveDateTime activityStartAsDateTime = getActivityStartAsDateTime();
        if (activityStartAsDateTime == null) {
            return null;
        }
        return activityStartAsDateTime.getPipedriveTime();
    }

    public void setActivityStartAt(@NonNull PipedriveDate date, @NonNull PipedriveTime time, @Nullable PipedriveDuration withDuration) {
        setActivityStartAt(PipedriveDateTime.instanceFromAddition(date, time), withDuration);
    }

    public void setActivityStartAt(@NonNull PipedriveDateTime dateTime, @Nullable PipedriveDuration withDuration) {
        this.activityStartType = ActivityStartType.PipedriveDateTime;
        this.activityStartInSeconds = Long.valueOf(dateTime.getRepresentationInUnixTime());
        setDuration(withDuration);
    }

    public void setActivityStartAt(@NonNull PipedriveDate date, @Nullable PipedriveDuration withDuration) {
        this.activityStartType = ActivityStartType.PipedriveDate;
        this.activityStartInSeconds = Long.valueOf(date.getRepresentationInUnixTime());
        setDuration(withDuration);
    }

    @Nullable
    public Long getActivityStartInSeconds() {
        return this.activityStartInSeconds;
    }

    public void setActivityStartInSeconds(@Nullable Long activityStartInSeconds) {
        this.activityStartInSeconds = activityStartInSeconds;
    }

    @Nullable
    @Deprecated
    public PipedriveDateTime getActivityStart() {
        return PipedriveDateTime.instanceFromUnixTimeRepresentation(this.activityStartInSeconds);
    }

    @Nullable
    public PipedriveDate getActivityStartAsDate() {
        if (this.activityStartType != ActivityStartType.PipedriveDate) {
            return null;
        }
        return PipedriveDate.instanceFromUnixTimeRepresentation(this.activityStartInSeconds);
    }

    @Nullable
    public PipedriveDateTime getActivityStartAsDateTime() {
        if (this.activityStartType != ActivityStartType.PipedriveDateTime) {
            return null;
        }
        return PipedriveDateTime.instanceFromUnixTimeRepresentation(this.activityStartInSeconds);
    }

    @Nullable
    public PipedriveDateTime getActivityEnd() {
        PipedriveDuration activityDuration = getDuration();
        boolean noActivityEnd = activityDuration == null || this.activityStartInSeconds == null;
        if (noActivityEnd) {
            return null;
        }
        return PipedriveDateTime.instanceFromUnixTimeRepresentation(Long.valueOf(this.activityStartInSeconds.longValue() + activityDuration.getRepresentationInUnixTime()));
    }

    @MainThread
    public void request(@NonNull final DateTimeInfo dateTimeInfo) {
        request(new DateTimeInfoWithReturn<String>() {
            public String activityHasStartDateTime(@NonNull PipedriveDateTime dateTime, @NonNull Activity inActivity) {
                dateTimeInfo.activityHasStartDateTime(dateTime, inActivity);
                return null;
            }

            public String activityHasStartDate(@NonNull PipedriveDate date, @NonNull Activity inActivity) {
                dateTimeInfo.activityHasStartDate(date, inActivity);
                return null;
            }

            public String activityIsCorruptedToReturnDateTimeInfo(@NonNull Activity corruptedActivity) {
                dateTimeInfo.activityIsCorruptedToReturnDateTimeInfo(corruptedActivity);
                return null;
            }
        });
    }

    @Nullable
    @MainThread
    public <BACKPACK_TO_RETURN> BACKPACK_TO_RETURN request(@NonNull DateTimeInfoWithReturn<BACKPACK_TO_RETURN> dateTimeInfo) {
        PipedriveDateTime activityStartAsDateTime = getActivityStartAsDateTime();
        if (activityStartAsDateTime != null) {
            return dateTimeInfo.activityHasStartDateTime(activityStartAsDateTime, this);
        }
        PipedriveDate activityStartAsDate = getActivityStartAsDate();
        if (activityStartAsDate != null) {
            return dateTimeInfo.activityHasStartDate(activityStartAsDate, this);
        }
        return dateTimeInfo.activityIsCorruptedToReturnDateTimeInfo(this);
    }

    public void setType(@Nullable ActivityType type) {
        this.type = type;
    }

    @Nullable
    public ActivityType getType() {
        return this.type;
    }

    public void setDuration(@Nullable PipedriveDuration duration) {
        this.duration = duration;
    }

    @Nullable
    public PipedriveDuration getDuration() {
        return this.duration;
    }

    @Nullable
    public final String getDurationString() {
        return getDuration() == null ? null : getDuration().getRepresentationInUtcForApiShortRepresentation();
    }

    public void setAssignedUserId(long assignedUserId) {
        this.assignedUserId = assignedUserId;
    }

    public long getAssignedUserId() {
        return this.assignedUserId;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public void setPerson(@Nullable Person person) {
        this.person = person;
    }

    @Nullable
    public Person getPerson() {
        return this.person;
    }

    public void setOrganization(@Nullable Organization organization) {
        this.organization = organization;
    }

    @Nullable
    public Organization getOrganization() {
        return this.organization;
    }

    public void setDeal(@Nullable Deal deal) {
        this.deal = deal;
    }

    @Nullable
    public Deal getDeal() {
        return this.deal;
    }

    @Nullable
    public String getHtmlContent() {
        return getNote();
    }

    public void setHtmlContent(@Nullable String content) {
        setNote(content);
    }

    @Nullable
    public String getMetaInfo(@NonNull Session session, @Nullable String separator) {
        StringBuilder metaInfoBuilder = new StringBuilder();
        if (!(getDeal() == null || StringUtils.isTrimmedAndEmpty(getDeal().getTitle()))) {
            metaInfoBuilder.append(getDeal().getTitle());
        }
        if (!(getPerson() == null || StringUtils.isTrimmedAndEmpty(getPerson().getName()))) {
            if (metaInfoBuilder.length() > 0) {
                metaInfoBuilder.append(separator);
            }
            metaInfoBuilder.append(getPerson().getName());
        }
        if (!(getOrganization() == null || StringUtils.isTrimmedAndEmpty(getOrganization().getName()))) {
            if (metaInfoBuilder.length() > 0) {
                metaInfoBuilder.append(separator);
            }
            metaInfoBuilder.append(getOrganization().getName());
        }
        return metaInfoBuilder.toString();
    }

    public static boolean isOverdueFor(@NonNull PipedriveDate pipedriveDate) {
        return pipedriveDate.getRepresentationInUnixTime() < PipedriveDate.instanceWithCurrentDate().getRepresentationInUnixTime();
    }

    public static boolean isOverdueFor(@NonNull PipedriveDateTime pipedriveDateTime) {
        return pipedriveDateTime.getRepresentationInUnixTime() < PipedriveDateTime.instanceWithCurrentDateTime().getRepresentationInUnixTime();
    }

    @Nullable
    public Boolean isOverdue() {
        PipedriveDateTime activityStartAsDateTime = getActivityStartAsDateTime();
        if (activityStartAsDateTime != null) {
            return Boolean.valueOf(isOverdueFor(activityStartAsDateTime));
        }
        PipedriveDate activityStartAsDate = getActivityStartAsDate();
        if (activityStartAsDate != null) {
            return Boolean.valueOf(isOverdueFor(activityStartAsDate));
        }
        return null;
    }

    public static boolean isDueTodayFor(@NonNull PipedriveDate pipedriveDate) {
        boolean z = true;
        if (isOverdueFor(pipedriveDate)) {
            return false;
        }
        boolean sameYear;
        PipedriveDate pipedriveDateNow = PipedriveDate.instanceWithCurrentDate();
        if (pipedriveDate.getYear() == pipedriveDateNow.getYear()) {
            sameYear = true;
        } else {
            sameYear = false;
        }
        boolean sameMonth;
        if (pipedriveDate.getMonth() == pipedriveDateNow.getMonth()) {
            sameMonth = true;
        } else {
            sameMonth = false;
        }
        boolean sameDayOfMonth;
        if (pipedriveDate.getDayOfMonth() == pipedriveDateNow.getDayOfMonth()) {
            sameDayOfMonth = true;
        } else {
            sameDayOfMonth = false;
        }
        if (!(sameYear && sameMonth && sameDayOfMonth)) {
            z = false;
        }
        return z;
    }

    public static boolean isDueTodayFor(@NonNull PipedriveDateTime pipedriveDateTime) {
        boolean z = true;
        if (isOverdueFor(pipedriveDateTime)) {
            return false;
        }
        boolean sameYear;
        Calendar activityStartDateTimeInLocalCalendar = pipedriveDateTime.toLocalCalendar();
        Calendar pipedriveDateTimeNowInLocalCalendar = PipedriveDateTime.instanceWithCurrentDateTime().toLocalCalendar();
        if (activityStartDateTimeInLocalCalendar.get(1) == pipedriveDateTimeNowInLocalCalendar.get(1)) {
            sameYear = true;
        } else {
            sameYear = false;
        }
        boolean sameDayOfYear;
        if (activityStartDateTimeInLocalCalendar.get(5) == pipedriveDateTimeNowInLocalCalendar.get(5)) {
            sameDayOfYear = true;
        } else {
            sameDayOfYear = false;
        }
        if (!(sameYear && sameDayOfYear)) {
            z = false;
        }
        return z;
    }

    @Nullable
    public Boolean isDueToday() {
        PipedriveDateTime activityStartAsDateTime = getActivityStartAsDateTime();
        if (activityStartAsDateTime != null) {
            return Boolean.valueOf(isDueTodayFor(activityStartAsDateTime));
        }
        PipedriveDate activityStartAsDate = getActivityStartAsDate();
        if (activityStartAsDate != null) {
            return Boolean.valueOf(isDueTodayFor(activityStartAsDate));
        }
        return null;
    }

    public Activity getDeepCopy() {
        Activity thisCloned = (Activity) Cloner.cloneParcelable(this);
        if (this.person != null) {
            thisCloned.person = this.person.getDeepCopy();
        }
        if (this.organization != null) {
            thisCloned.organization = this.organization.getDeepCopy();
        }
        if (this.deal != null) {
            thisCloned.deal = this.deal.getDeepCopy();
        }
        return thisCloned;
    }

    public boolean isAllAssociationsExisting() {
        if (getPerson() != null && !getPerson().isExisting()) {
            return false;
        }
        if (getOrganization() != null && !getOrganization().isExisting()) {
            return false;
        }
        if (getDeal() == null || getDeal().isExisting()) {
            return true;
        }
        return false;
    }

    public String toString() {
        return String.format("SqlId: %s JSON: %s", new Object[]{Long.valueOf(getSqlId()), ActivityEntity.getJSON(this)});
    }

    @NonNull
    public String getAssociationString(@NonNull Context context) {
        boolean personNameExist;
        String personName = this.person == null ? null : this.person.getName();
        String organizationName = this.organization == null ? null : this.organization.getName();
        if (StringUtils.isTrimmedAndEmpty(personName)) {
            personNameExist = false;
        } else {
            personNameExist = true;
        }
        boolean organizationNameExist;
        if (StringUtils.isTrimmedAndEmpty(organizationName)) {
            organizationNameExist = false;
        } else {
            organizationNameExist = true;
        }
        if (!personNameExist && !organizationNameExist) {
            return "";
        }
        if (!personNameExist || !organizationNameExist) {
            return !personNameExist ? organizationName : personName;
        } else {
            return context.getString(R.string.activity_list_item_subtitle, new Object[]{personName, organizationName});
        }
    }
}
