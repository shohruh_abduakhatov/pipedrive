package com.pipedrive.views.viewholder.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class ActivityRowViewHolder_ViewBinding implements Unbinder {
    private ActivityRowViewHolder target;

    @UiThread
    public ActivityRowViewHolder_ViewBinding(ActivityRowViewHolder target, View source) {
        this.target = target;
        target.mType = (ImageView) Utils.findRequiredViewAsType(source, R.id.type, "field 'mType'", ImageView.class);
        target.mTitle = (TextView) Utils.findRequiredViewAsType(source, R.id.title, "field 'mTitle'", TextView.class);
        target.mSubTitle = (TextView) Utils.findRequiredViewAsType(source, R.id.subtitle, "field 'mSubTitle'", TextView.class);
        target.mNote = (TextView) Utils.findRequiredViewAsType(source, R.id.activityNote, "field 'mNote'", TextView.class);
    }

    @CallSuper
    public void unbind() {
        ActivityRowViewHolder target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mType = null;
        target.mTitle = null;
        target.mSubTitle = null;
        target.mNote = null;
    }
}
