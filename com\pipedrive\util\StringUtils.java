package com.pipedrive.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.Pair;
import android.widget.TextView;
import com.pipedrive.datasource.PipeSQLiteSharedHelper.Table;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.interfaces.ContactOrgInterface;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.User;
import com.pipedrive.model.email.EmailParticipant;
import java.util.ArrayList;
import java.util.List;

public enum StringUtils {
    ;

    public static void highlightString(@Nullable TextView textView, @NonNull SearchConstraint searchConstraint) {
        if (textView != null) {
            highlightString(textView, textView.getText().toString(), searchConstraint);
        }
    }

    public static void highlightString(@Nullable TextView textview, @Nullable String target, @NonNull SearchConstraint searchConstraint) {
        List<Pair<Integer, Integer>> searchLocations = new ArrayList();
        if (textview != null && target != null) {
            SpannableString highlightedTarget = new SpannableString(target);
            String lowerCaseTarget = target.toLowerCase();
            for (String string : searchConstraint.getParts()) {
                if (!(string == null) && lowerCaseTarget.contains(string.toLowerCase())) {
                    int searchIdx = lowerCaseTarget.indexOf(string.toLowerCase(), 0);
                    if (searchIdx >= 0) {
                        searchLocations.add(new Pair(Integer.valueOf(searchIdx), Integer.valueOf(string.length())));
                    }
                }
            }
            for (Pair<Integer, Integer> pair : searchLocations) {
                int start = ((Integer) pair.first).intValue();
                int end = start + ((Integer) pair.second).intValue();
                if (highlightedTarget.length() >= end) {
                    highlightedTarget.setSpan(new StyleSpan(1), start, end, 33);
                }
            }
            textview.setText(highlightedTarget);
        }
    }

    public static boolean isTrimmedAndEmpty(CharSequence str) {
        return TextUtils.isEmpty(str) || TextUtils.getTrimmedLength(str) == 0;
    }

    public static String getInitials(EmailParticipant emailParticipant) {
        if (emailParticipant == null || TextUtils.isEmpty(emailParticipant.getName())) {
            return "";
        }
        return getInitialsFromName(emailParticipant.getName());
    }

    @NonNull
    public static String getInitials(@Nullable ContactOrgInterface contact) {
        if (contact == null || TextUtils.isEmpty(contact.getName())) {
            return "";
        }
        return getInitialsFromName(contact.getName());
    }

    @NonNull
    public static String getInitials(@Nullable User user) {
        if (user == null || TextUtils.isEmpty(user.getName())) {
            return "";
        }
        return getInitialsFromName(user.getName());
    }

    @NonNull
    private static String getInitialsFromName(String name) {
        if (isTrimmedAndEmpty(name)) {
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder("");
        for (String part : name.trim().split("\\s+")) {
            if (!(part == null || TextUtils.isEmpty(part))) {
                stringBuilder.append(part.charAt(0));
            }
            if (stringBuilder.length() > 1) {
                break;
            }
        }
        return stringBuilder.toString().toUpperCase();
    }

    @NonNull
    public static String getNormalizedDealTitle(@NonNull Deal deal) {
        return isTrimmedAndEmpty(deal.getTitle()) ? "" : deal.getTitle().toLowerCase();
    }

    @NonNull
    public static String getNormalizedPersonName(@NonNull Person person) {
        return isTrimmedAndEmpty(person.getName()) ? "" : person.getName().toLowerCase();
    }

    @NonNull
    public static String getNormalizedOrganizationName(@NonNull Organization organization) {
        return isTrimmedAndEmpty(organization.getName()) ? "" : organization.getName().toLowerCase();
    }

    @NonNull
    public static String toCsvString(@NonNull List<Long> values) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < values.size(); i++) {
            Long value = (Long) values.get(i);
            if (value != null) {
                stringBuilder.append(Long.toString(value.longValue()));
                if (i < values.size() - 1) {
                    stringBuilder.append(Table.COMMA_SEP);
                }
            }
        }
        return stringBuilder.toString();
    }
}
