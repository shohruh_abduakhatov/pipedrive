package com.zendesk.sdk.feedback.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.network.SubmissionListener;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.ui.NetworkAwareActionbarActivity;
import com.zendesk.sdk.ui.ToolbarSherlock;
import com.zendesk.sdk.util.UiUtils;
import com.zendesk.service.ErrorResponse;
import com.zendesk.service.ErrorResponseAdapter;

public class ContactZendeskActivity extends NetworkAwareActionbarActivity {
    public static final String EXTRA_CONTACT_CONFIGURATION = "extra_contact_configuration";
    private static final String FRAGMENT_TAG = ContactZendeskActivity.class.getSimpleName();
    private static final String LOG_TAG = "ContactZendeskActivity";
    public static final String RESULT_ERROR_IS_NETWORK_ERROR = "extra_is_nw_error";
    public static final String RESULT_ERROR_REASON = "extra_error_reason";
    public static final String RESULT_ERROR_STATUS_CODE = "extra_status_code";
    private ContactZendeskFragment contactZendeskFragment;
    private final SubmissionListener mSubmissionListener = new SubmissionListener() {
        public void onSubmissionStarted() {
        }

        public void onSubmissionCompleted() {
            ContactZendeskActivity.this.setResult(-1, new Intent());
            ContactZendeskActivity.this.finish();
        }

        public void onSubmissionCancel() {
        }

        public void onSubmissionError(ErrorResponse errorResponse) {
            ContactZendeskActivity.this.setResult(0, ContactZendeskActivity.this.getErrorIntent(errorResponse));
        }
    };

    class DefaultContactConfiguration extends BaseZendeskFeedbackConfiguration {
        DefaultContactConfiguration() {
        }

        public String getRequestSubject() {
            return ContactZendeskActivity.this.getString(R.string.contact_fragment_request_subject);
        }
    }

    public static void startActivity(Context context, @Nullable ZendeskFeedbackConfiguration configuration) {
        boolean configurationRequiresWrapping = false;
        if (context == null) {
            Logger.e(LOG_TAG, "Context is null, cannot start the context.", new Object[0]);
            return;
        }
        Intent intent = new Intent(context, ContactZendeskActivity.class);
        if (!(configuration == null || (configuration instanceof WrappedZendeskFeedbackConfiguration))) {
            configurationRequiresWrapping = true;
        }
        if (configurationRequiresWrapping) {
            configuration = new WrappedZendeskFeedbackConfiguration(configuration);
        }
        intent.putExtra(EXTRA_CONTACT_CONFIGURATION, configuration);
        context.startActivity(intent);
    }

    protected void onCreate(Bundle savedInstanceState) {
        boolean hasSuppliedContactConfiguration = true;
        super.onCreate(savedInstanceState);
        UiUtils.setThemeIfAttributesAreMissing(this, R.attr.contactAttachmentIcon, R.attr.contactSendIcon);
        setContentView(R.layout.activity_contact_zendesk);
        ToolbarSherlock.installToolBar(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null) {
            String errorMsg = "This activity requires an AppCompat theme with an action bar, finishing activity...";
            Logger.e(LOG_TAG, "This activity requires an AppCompat theme with an action bar, finishing activity...", new Object[0]);
            setResult(0, getErrorIntent(new ErrorResponseAdapter("This activity requires an AppCompat theme with an action bar, finishing activity...")));
            finish();
            return;
        }
        actionBar.setDisplayHomeAsUpEnabled(true);
        if (ZendeskConfig.INSTANCE.storage().identityStorage().getIdentity() == null) {
            errorMsg = "No identity is present. Did you call ZendeskConfig.INSTANCE.setIdentity()?, finishing activity...";
            Logger.e(LOG_TAG, "No identity is present. Did you call ZendeskConfig.INSTANCE.setIdentity()?, finishing activity...", new Object[0]);
            setResult(0, getErrorIntent(new ErrorResponseAdapter("No identity is present. Did you call ZendeskConfig.INSTANCE.setIdentity()?, finishing activity...")));
            finish();
            return;
        }
        ZendeskFeedbackConfiguration configuration;
        if (!(getIntent().hasExtra(EXTRA_CONTACT_CONFIGURATION) && (getIntent().getSerializableExtra(EXTRA_CONTACT_CONFIGURATION) instanceof ZendeskFeedbackConfiguration))) {
            hasSuppliedContactConfiguration = false;
        }
        if (hasSuppliedContactConfiguration) {
            configuration = (ZendeskFeedbackConfiguration) getIntent().getSerializableExtra(EXTRA_CONTACT_CONFIGURATION);
        } else {
            errorMsg = "Contact configuration was not provided. Will use default configuration...";
            Logger.d(LOG_TAG, "Contact configuration was not provided. Will use default configuration...", new Object[0]);
            configuration = new DefaultContactConfiguration();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(FRAGMENT_TAG);
        if (fragment == null || !(fragment instanceof ContactZendeskFragment)) {
            this.contactZendeskFragment = ContactZendeskFragment.newInstance(configuration);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.activity_contact_zendesk_root, this.contactZendeskFragment, FRAGMENT_TAG);
            fragmentTransaction.commit();
        } else {
            this.contactZendeskFragment = (ContactZendeskFragment) fragment;
        }
        this.contactZendeskFragment.setFeedbackListener(this.mSubmissionListener);
    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.contactZendeskFragment != null && isFinishing()) {
            Logger.d(LOG_TAG, "Deleting unused attachments", new Object[0]);
            this.contactZendeskFragment.deleteUnusedAttachmentsBeforeShutdown();
        }
    }

    private Intent getErrorIntent(ErrorResponse errorResponse) {
        Intent intent = new Intent();
        intent.putExtra(RESULT_ERROR_IS_NETWORK_ERROR, errorResponse.isNetworkError());
        intent.putExtra(RESULT_ERROR_REASON, errorResponse.getReason());
        intent.putExtra(RESULT_ERROR_STATUS_CODE, errorResponse.getStatus());
        return intent;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != 16908332) {
            return false;
        }
        onBackPressed();
        return true;
    }

    public void onNetworkAvailable() {
        super.onNetworkAvailable();
        this.contactZendeskFragment.onNetworkAvailable();
    }

    public void onNetworkUnavailable() {
        super.onNetworkUnavailable();
        this.contactZendeskFragment.onNetworkUnavailable();
    }
}
