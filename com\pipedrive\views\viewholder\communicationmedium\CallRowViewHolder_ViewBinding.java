package com.pipedrive.views.viewholder.communicationmedium;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class CallRowViewHolder_ViewBinding implements Unbinder {
    private CallRowViewHolder target;

    @UiThread
    public CallRowViewHolder_ViewBinding(CallRowViewHolder target, View source) {
        this.target = target;
        target.mValue = (TextView) Utils.findRequiredViewAsType(source, R.id.value, "field 'mValue'", TextView.class);
        target.mType = (TextView) Utils.findRequiredViewAsType(source, R.id.type, "field 'mType'", TextView.class);
    }

    @CallSuper
    public void unbind() {
        CallRowViewHolder target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mValue = null;
        target.mType = null;
    }
}
