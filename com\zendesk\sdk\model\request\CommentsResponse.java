package com.zendesk.sdk.model.request;

import android.support.annotation.NonNull;
import com.zendesk.sdk.model.network.ResponseWrapper;
import com.zendesk.util.CollectionUtils;
import java.util.List;

public final class CommentsResponse extends ResponseWrapper {
    private List<CommentResponse> comments;
    private List<Organization> organizations;
    private List<User> users;

    @NonNull
    public List<CommentResponse> getComments() {
        return CollectionUtils.copyOf(this.comments);
    }

    @NonNull
    public List<User> getUsers() {
        return CollectionUtils.copyOf(this.users);
    }

    @NonNull
    public List<Organization> getOrganizations() {
        return CollectionUtils.copyOf(this.organizations);
    }
}
