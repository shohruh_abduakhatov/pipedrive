package com.zendesk.sdk.network.impl;

import com.zendesk.sdk.network.HelpCenterProvider;
import com.zendesk.sdk.network.NetworkInfoProvider;
import com.zendesk.sdk.network.PushRegistrationProvider;
import com.zendesk.sdk.network.RequestProvider;
import com.zendesk.sdk.network.SdkSettingsProvider;
import com.zendesk.sdk.network.SettingsHelper;
import com.zendesk.sdk.network.UploadProvider;
import com.zendesk.sdk.network.UserProvider;

class ZendeskProviderStore implements ProviderStore {
    private final HelpCenterProvider helpCenterProvider;
    private final NetworkInfoProvider networkInfoProvider;
    private final PushRegistrationProvider pushRegistrationProvider;
    private final RequestProvider requestProvider;
    private final SdkSettingsProvider sdkSettingsProvider;
    private final SettingsHelper settingsHelper;
    private final UploadProvider uploadProvider;
    private final UserProvider userProvider;

    ZendeskProviderStore(UserProvider userProvider, HelpCenterProvider helpCenterProvider, PushRegistrationProvider pushRegistrationProvider, RequestProvider requestProvider, UploadProvider uploadProvider, SdkSettingsProvider sdkSettingsProvider, NetworkInfoProvider networkInfoProvider, SettingsHelper settingsHelper) {
        this.userProvider = userProvider;
        this.helpCenterProvider = helpCenterProvider;
        this.pushRegistrationProvider = pushRegistrationProvider;
        this.requestProvider = requestProvider;
        this.uploadProvider = uploadProvider;
        this.sdkSettingsProvider = sdkSettingsProvider;
        this.networkInfoProvider = networkInfoProvider;
        this.settingsHelper = settingsHelper;
    }

    public UserProvider userProvider() {
        return this.userProvider;
    }

    public HelpCenterProvider helpCenterProvider() {
        return this.helpCenterProvider;
    }

    public PushRegistrationProvider pushRegistrationProvider() {
        return this.pushRegistrationProvider;
    }

    public RequestProvider requestProvider() {
        return this.requestProvider;
    }

    public UploadProvider uploadProvider() {
        return this.uploadProvider;
    }

    public SdkSettingsProvider sdkSettingsProvider() {
        return this.sdkSettingsProvider;
    }

    public SettingsHelper uiSettingsHelper() {
        return this.settingsHelper;
    }

    public NetworkInfoProvider networkInfoProvider() {
        return this.networkInfoProvider;
    }
}
