package com.pipedrive.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.ChildDataSourceEntity;
import com.pipedrive.util.CursorHelper;
import java.util.Collections;
import java.util.List;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;

public abstract class BaseChildDataSource<T extends ChildDataSourceEntity> extends BaseDataSource<T> {
    private static final String TAG = "BaseChildDataSource";

    @Nullable
    protected abstract T deflateCursor(@NonNull Cursor cursor, @Nullable Long l, @Nullable Long l2, @Nullable Long l3);

    @NonNull
    protected abstract String getColumnNameForParentSqlId();

    protected BaseChildDataSource(SQLiteDatabase dbconn) {
        super(dbconn);
    }

    @CallSuper
    @NonNull
    protected ContentValues getContentValues(@NonNull T t) {
        ContentValues values = super.getContentValues(t);
        values.put(getColumnNameForParentSqlId(), t.getParentSqlId());
        return values;
    }

    @NonNull
    private SelectionHolder getSelectionForParentSqlId(@NonNull Long parentSqlId) {
        return new SelectionHolder(getColumnNameForParentSqlId() + " = ?", new String[]{parentSqlId.toString()});
    }

    public void deleteAllRelatedToParent(@NonNull BaseDatasourceEntity parentEntity) {
        if (parentEntity.getSqlIdOrNull() != null) {
            SelectionHolder selectionForParentSqlId = getSelectionForParentSqlId(parentEntity.getSqlIdOrNull());
            SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
            String tableName = getTableName();
            String selection = selectionForParentSqlId.getSelection();
            String[] selectionArgs = selectionForParentSqlId.getSelectionArgs();
            if (transactionalDBConnection instanceof SQLiteDatabase) {
                SQLiteInstrumentation.delete(transactionalDBConnection, tableName, selection, selectionArgs);
            } else {
                transactionalDBConnection.delete(tableName, selection, selectionArgs);
            }
        }
    }

    @NonNull
    public final List<T> findAllRelatedToParent(@NonNull BaseDatasourceEntity parentEntity) {
        if (parentEntity.isStored()) {
            return findAllRelatedToParentSqlId(parentEntity.getSqlIdOrNull());
        }
        return Collections.emptyList();
    }

    @NonNull
    public final List<T> findAllRelatedToParentSqlId(@Nullable Long parentSqlId) {
        if (parentSqlId == null) {
            return Collections.emptyList();
        }
        return deflateCursorToList(findAllRelatedToParentSqlIdCursor(parentSqlId));
    }

    @NonNull
    private Cursor findAllRelatedToParentSqlIdCursor(@NonNull Long parentSqlId) {
        return query(getTableName(), getAllColumns(), getSelectionForParentSqlId(parentSqlId), null);
    }

    @Nullable
    protected final T deflateCursor(@NonNull Cursor cursor) {
        return deflateCursor(cursor, CursorHelper.getLong(cursor, getColumnNameForSqlId()), CursorHelper.getLong(cursor, getColumnNameForPipedriveId()), CursorHelper.getLong(cursor, getColumnNameForParentSqlId()));
    }

    protected final void createOrUpdate(@Nullable List<T> ts, @NonNull BaseDatasourceEntity parentEntity) {
        if (ts != null && parentEntity.getSqlIdOrNull() != null) {
            for (T t : ts) {
                t.setParentEntity(parentEntity);
                createOrUpdate((BaseDatasourceEntity) t);
            }
            if (!ts.isEmpty()) {
                String pipedriveIds = (String) Observable.from((Iterable) ts).map(new Func1<T, String>() {
                    public String call(T t) {
                        if (t.getPipedriveIdOrNull() == null) {
                            return null;
                        }
                        return String.valueOf(t.getPipedriveIdOrNull());
                    }
                }).filter(new Func1<String, Boolean>() {
                    public Boolean call(String s) {
                        return Boolean.valueOf(s != null);
                    }
                }).defaultIfEmpty(null).reduce(new Func2<String, String, String>() {
                    public String call(String s, String s2) {
                        return s.concat(Table.COMMA_SEP).concat(s2);
                    }
                }).toSingle().toBlocking().value();
                if (!TextUtils.isEmpty(pipedriveIds)) {
                    SelectionHolder selectionHolder = getSelectionForParentSqlId(parentEntity.getSqlIdOrNull());
                    String pipedriveIdWhereClause = " AND " + getColumnNameForPipedriveId() + " not in (" + pipedriveIds + ")";
                    SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
                    String tableName = getTableName();
                    String str = selectionHolder.getSelection() + pipedriveIdWhereClause;
                    String[] selectionArgs = selectionHolder.getSelectionArgs();
                    Log.d(TAG, "createOrUpdate: " + (!(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.delete(tableName, str, selectionArgs) : SQLiteInstrumentation.delete(transactionalDBConnection, tableName, str, selectionArgs)));
                }
            }
        }
    }
}
