package com.pipedrive.util.networking;

/* synthetic */ class ConnectionUtil$2 {
    static final /* synthetic */ int[] $SwitchMap$com$pipedrive$util$networking$ConnectionUtil$SendDataRequestType = new int[ConnectionUtil$SendDataRequestType.values().length];

    static {
        try {
            $SwitchMap$com$pipedrive$util$networking$ConnectionUtil$SendDataRequestType[ConnectionUtil$SendDataRequestType.HttpGet.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            $SwitchMap$com$pipedrive$util$networking$ConnectionUtil$SendDataRequestType[ConnectionUtil$SendDataRequestType.HttpPost.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            $SwitchMap$com$pipedrive$util$networking$ConnectionUtil$SendDataRequestType[ConnectionUtil$SendDataRequestType.MultipartFormData.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            $SwitchMap$com$pipedrive$util$networking$ConnectionUtil$SendDataRequestType[ConnectionUtil$SendDataRequestType.HttpPut.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            $SwitchMap$com$pipedrive$util$networking$ConnectionUtil$SendDataRequestType[ConnectionUtil$SendDataRequestType.HttpDelete.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
    }
}
