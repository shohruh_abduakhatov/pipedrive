package com.zendesk.sdk.rating.impl;

import com.zendesk.sdk.R;
import com.zendesk.sdk.rating.RateMyAppButton;

public abstract class BaseRateMyAppButton implements RateMyAppButton {
    public int getStyleAttributeId() {
        return R.attr.RateMyAppButtonStyle;
    }

    public int getId() {
        return -1;
    }

    public boolean shouldDismissDialog() {
        return true;
    }
}
