package com.zendesk.sdk.model.helpcenter;

public class RecordArticleViewRequest {
    private LastSearch lastSearch;
    private boolean uniqueSearchResultClick;

    public RecordArticleViewRequest(LastSearch lastSearch, boolean uniqueSearchResultClick) {
        this.lastSearch = lastSearch;
        this.uniqueSearchResultClick = uniqueSearchResultClick;
    }
}
