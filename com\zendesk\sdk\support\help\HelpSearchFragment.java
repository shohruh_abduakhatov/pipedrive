package com.zendesk.sdk.support.help;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.background.ApplicationStateMonitor;
import com.newrelic.agent.android.instrumentation.Instrumented;
import com.newrelic.agent.android.tracing.TraceMachine;
import com.zendesk.sdk.R;
import com.zendesk.sdk.model.helpcenter.SearchArticle;
import java.util.Collections;
import java.util.List;

@Instrumented
public class HelpSearchFragment extends Fragment implements TraceFieldInterface {
    static final String ARG_ADD_LIST_PADDING_BOTTOM = "arg_add_list_padding_bottom";
    private HelpSearchRecyclerViewAdapter adapter;
    private String query = "";
    private RecyclerView recyclerView;
    private List<SearchArticle> searchArticles = Collections.emptyList();

    protected void onStart() {
        super.onStart();
        ApplicationStateMonitor.getInstance().activityStarted();
    }

    protected void onStop() {
        super.onStop();
        ApplicationStateMonitor.getInstance().activityStopped();
    }

    public static HelpSearchFragment newInstance(boolean addEndPadding) {
        Bundle args = new Bundle();
        args.putBoolean(ARG_ADD_LIST_PADDING_BOTTOM, addEndPadding);
        HelpSearchFragment fragment = new HelpSearchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(@Nullable Bundle bundle) {
        TraceMachine.startTracing("HelpSearchFragment");
        try {
            TraceMachine.enterMethod(this._nr_trace, "HelpSearchFragment#onCreate", null);
        } catch (NoSuchFieldError e) {
            while (true) {
                TraceMachine.enterMethod(null, "HelpSearchFragment#onCreate", null);
            }
        }
        super.onCreate(bundle);
        setRetainInstance(true);
        TraceMachine.exitMethod();
    }

    @Nullable
    public View onCreateView(LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        try {
            TraceMachine.enterMethod(this._nr_trace, "HelpSearchFragment#onCreateView", null);
        } catch (NoSuchFieldError e) {
            while (true) {
                TraceMachine.enterMethod(null, "HelpSearchFragment#onCreateView", null);
            }
        }
        View view = layoutInflater.inflate(R.layout.fragment_help, viewGroup, false);
        this.recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        setupRecyclerView();
        TraceMachine.exitMethod();
        return view;
    }

    public void updateResults(List<SearchArticle> searchArticles, String query) {
        this.searchArticles = searchArticles;
        this.query = query;
        if (this.adapter != null && this.recyclerView != null) {
            this.recyclerView.setVisibility(0);
            this.adapter.update(searchArticles, query);
        }
    }

    public void clearResults() {
        if (this.adapter != null) {
            this.adapter.clearResults();
        }
    }

    private void setupRecyclerView() {
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
        this.adapter = new HelpSearchRecyclerViewAdapter(this.searchArticles, this.query, getArguments().getBoolean(ARG_ADD_LIST_PADDING_BOTTOM));
        this.recyclerView.setAdapter(this.adapter);
    }
}
