package com.pipedrive.util.camera;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import com.pipedrive.R;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.util.flowfiles.FileHelper;
import com.pipedrive.util.time.TimeManager;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CameraHelper {
    private static final String STORE_KEY_SNAPSHOT_IMAGE_FILE_CONTAINER_URI = "STORE_KEY_SNAPSHOT_IMAGE_FILE_CONTAINER_URI";
    private final int REQUEST_CODE = 18;
    @Nullable
    private OnActivityResult mRegisteredOnActivityResult;
    private Uri mSnapshotImageFileContainerUri = null;

    @CallSuper
    public void onCreate(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            this.mSnapshotImageFileContainerUri = (Uri) savedInstanceState.getParcelable(STORE_KEY_SNAPSHOT_IMAGE_FILE_CONTAINER_URI);
        }
    }

    @CallSuper
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(STORE_KEY_SNAPSHOT_IMAGE_FILE_CONTAINER_URI, this.mSnapshotImageFileContainerUri);
    }

    public boolean isDeviceHasCamera(@NonNull Context context) {
        return context.getPackageManager().hasSystemFeature("android.hardware.camera");
    }

    public boolean isCameraHandlerApplicationExist(@NonNull Context context) {
        return getTakePictureIntent().resolveActivity(context.getPackageManager()) != null;
    }

    public boolean snapPhoto(@NonNull Activity fromActivity) {
        return snapPhoto(fromActivity, null, null, fromActivity);
    }

    public boolean snapPhoto(@NonNull Fragment fromSupportFragment) {
        return snapPhoto(fromSupportFragment.getActivity(), fromSupportFragment, null, null);
    }

    public boolean snapPhoto(@NonNull android.app.Fragment fromFragment) {
        return snapPhoto(fromFragment.getActivity(), null, fromFragment, null);
    }

    public void registerOnActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        this.mRegisteredOnActivityResult = new OnActivityResult(requestCode, resultCode, data != null ? (Intent) data.clone() : null);
    }

    public void retrieveCameraSnapshot(@NonNull Context context, @NonNull OnCameraListener onCameraListener) {
        if (this.mRegisteredOnActivityResult != null) {
            onActivityResult(context, this.mRegisteredOnActivityResult.requestCode, this.mRegisteredOnActivityResult.resultCode, this.mRegisteredOnActivityResult.data, onCameraListener);
            this.mRegisteredOnActivityResult = null;
        }
    }

    @Deprecated
    public void onActivityResult(@NonNull Context context, int requestCode, int resultCode, Intent ignored, @NonNull OnCameraListener onCameraListener) {
        if (!(requestCode != 18)) {
            if (this.mSnapshotImageFileContainerUri == null) {
                snapshotFailed(context, onCameraListener);
            }
            boolean requestCameraPictureCaptured = resultCode == -1;
            boolean requestCameraPictureCanceled = resultCode == 0;
            boolean requestCameraUnknownResultCode = (requestCameraPictureCaptured || requestCameraPictureCanceled) ? false : true;
            if (requestCameraPictureCaptured) {
                registerImageForGallery(context, this.mSnapshotImageFileContainerUri);
                boolean snapshotProcessingIsSuccessful = onCameraListener.snapshot(this.mSnapshotImageFileContainerUri, FileHelper.getFileName(this.mSnapshotImageFileContainerUri));
                this.mSnapshotImageFileContainerUri = null;
                if (!snapshotProcessingIsSuccessful) {
                    messageForGenericError(context);
                }
            } else if (requestCameraPictureCanceled || requestCameraUnknownResultCode) {
                if (this.mSnapshotImageFileContainerUri != null) {
                    File temporaryImageFile = FileHelper.getFile(this.mSnapshotImageFileContainerUri);
                    if (temporaryImageFile != null) {
                        temporaryImageFile.delete();
                    }
                    this.mSnapshotImageFileContainerUri = null;
                }
                if (requestCameraPictureCanceled) {
                    snapshotCancelled(onCameraListener);
                } else {
                    snapshotFailed(context, onCameraListener);
                }
            }
        }
    }

    private void snapshotCancelled(@NonNull OnCameraListener onCameraListener) {
        onCameraListener.snapshotCancelled();
    }

    private void snapshotFailed(@NonNull Context context, @NonNull OnCameraListener onCameraListener) {
        messageForGenericError(context);
        onCameraListener.snapshotFailed();
    }

    private void messageForGenericError(@NonNull Context context) {
        ViewUtil.showErrorToast(context, R.string.could_not_take_photo_try_again_later);
    }

    private void messageForNoCameraAppExists(@NonNull Context context) {
        ViewUtil.showErrorToast(context, R.string.could_not_take_photo_because_this_device_has_no_camera_apps);
    }

    @NonNull
    private Intent getTakePictureIntent() {
        return new Intent("android.media.action.IMAGE_CAPTURE");
    }

    private boolean snapPhoto(@NonNull Context context, @Nullable Fragment fromSupportFragment, @Nullable android.app.Fragment fromFragment, @Nullable Activity fromActivity) {
        boolean unknownCaller;
        if (fromSupportFragment == null && fromFragment == null && fromActivity == null) {
            unknownCaller = true;
        } else {
            unknownCaller = false;
        }
        if (unknownCaller) {
            messageForGenericError(context);
            return false;
        }
        boolean noCamera;
        if (isDeviceHasCamera(context)) {
            noCamera = false;
        } else {
            noCamera = true;
        }
        if (noCamera) {
            return false;
        }
        boolean noCameraAppExist;
        if (isCameraHandlerApplicationExist(context)) {
            noCameraAppExist = false;
        } else {
            noCameraAppExist = true;
        }
        if (noCameraAppExist) {
            messageForNoCameraAppExists(context);
            return false;
        }
        boolean imageFileContainerCreationFailed;
        File imageFileContainer = createUniqueImageFileContainer();
        if (imageFileContainer == null) {
            imageFileContainerCreationFailed = true;
        } else {
            imageFileContainerCreationFailed = false;
        }
        if (imageFileContainerCreationFailed) {
            messageForGenericError(context);
            return false;
        }
        this.mSnapshotImageFileContainerUri = Uri.fromFile(imageFileContainer);
        Intent takePictureIntent = getTakePictureIntent();
        takePictureIntent.putExtra("output", this.mSnapshotImageFileContainerUri);
        if (fromSupportFragment != null) {
            fromSupportFragment.startActivityForResult(takePictureIntent, 18);
        } else if (fromFragment != null) {
            fromFragment.startActivityForResult(takePictureIntent, 18);
        } else {
            fromActivity.startActivityForResult(takePictureIntent, 18);
        }
        return true;
    }

    private void registerImageForGallery(@NonNull Context context, @NonNull Uri imageFileUri) {
        Intent mediaScanFileIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        mediaScanFileIntent.setData(imageFileUri);
        context.sendBroadcast(mediaScanFileIntent);
    }

    @Nullable
    private File createUniqueImageFileContainer() {
        File file = null;
        String imageFileName = "Photo_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date(TimeManager.getInstance().currentTimeMillis().longValue()));
        File storageDir = getImageFileStorageDirectory();
        if (storageDir != null) {
            try {
                file = File.createTempFile(imageFileName, ".jpg", storageDir);
            } catch (IOException e) {
            }
        }
        return file;
    }

    @Nullable
    private File getImageFileStorageDirectory() {
        boolean canWriteToDcimStorageDirectory;
        boolean canWriteToAlternativeStorageDirectory = true;
        File dcimStorageDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");
        if (dcimStorageDirectory.exists() && dcimStorageDirectory.isDirectory() && dcimStorageDirectory.canWrite()) {
            canWriteToDcimStorageDirectory = true;
        } else {
            canWriteToDcimStorageDirectory = false;
        }
        if (canWriteToDcimStorageDirectory) {
            return dcimStorageDirectory;
        }
        boolean canWriteToPicturesStorageDirectory;
        LogJourno.reportEvent(EVENT.Store_dcimDirectoryNotAvailableForCameraSnapshotSave, "d:" + dcimStorageDirectory);
        File picturesStorageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if (picturesStorageDirectory.exists() && picturesStorageDirectory.isDirectory() && picturesStorageDirectory.canWrite()) {
            canWriteToPicturesStorageDirectory = true;
        } else {
            canWriteToPicturesStorageDirectory = false;
        }
        if (canWriteToPicturesStorageDirectory) {
            return picturesStorageDirectory;
        }
        File alternativeStorageDirectory = FileHelper.getFilesStorageDirectory();
        if (!(alternativeStorageDirectory.exists() && alternativeStorageDirectory.isDirectory() && alternativeStorageDirectory.canWrite())) {
            canWriteToAlternativeStorageDirectory = false;
        }
        if (canWriteToAlternativeStorageDirectory) {
            return alternativeStorageDirectory;
        }
        LogJourno.reportEvent(EVENT.Store_noDirectoryAvailableForCameraSnapshotSave, "d:" + alternativeStorageDirectory);
        return null;
    }
}
