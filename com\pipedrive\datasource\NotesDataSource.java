package com.pipedrive.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.notes.Note;
import com.pipedrive.util.CursorHelper;

public class NotesDataSource extends BaseDataSource<Note> {
    private DealsDataSource dealsDataSource;
    private PersonsDataSource mPersonsDataSource;
    private OrganizationsDataSource organizationsDataSource;

    public NotesDataSource(SQLiteDatabase dbconn) {
        super(dbconn);
    }

    private DealsDataSource getDealsDataSource() {
        if (this.dealsDataSource != null) {
            return this.dealsDataSource;
        }
        DealsDataSource dealsDataSource = new DealsDataSource(getTransactionalDBConnection());
        this.dealsDataSource = dealsDataSource;
        return dealsDataSource;
    }

    private PersonsDataSource getPersonsDataSource() {
        if (this.mPersonsDataSource != null) {
            return this.mPersonsDataSource;
        }
        PersonsDataSource personsDataSource = new PersonsDataSource(getTransactionalDBConnection());
        this.mPersonsDataSource = personsDataSource;
        return personsDataSource;
    }

    private OrganizationsDataSource getOrganizationsDataSource() {
        if (this.organizationsDataSource != null) {
            return this.organizationsDataSource;
        }
        OrganizationsDataSource organizationsDataSource = new OrganizationsDataSource(getTransactionalDBConnection());
        this.organizationsDataSource = organizationsDataSource;
        return organizationsDataSource;
    }

    @NonNull
    protected ContentValues getContentValues(@NonNull Note note) {
        ContentValues values = new ContentValues();
        if (note.getSqlId() != 0) {
            values.put(PipeSQLiteHelper.COLUMN_ID, Long.valueOf(note.getSqlId()));
        }
        if (note.isExisting()) {
            values.put(PipeSQLiteHelper.COLUMN_NOTES_PIPEDRIVE_ID, Integer.valueOf(note.getPipedriveId()));
        }
        values.put(PipeSQLiteHelper.COLUMN_NOTES_ACTIVE, Boolean.valueOf(note.isActiveFlag()));
        if (note.getAddTime() != null) {
            values.put(PipeSQLiteHelper.COLUMN_NOTES_ADD_TIME, Long.valueOf(note.getAddTime().getRepresentationInUnixTime()));
        } else {
            values.putNull(PipeSQLiteHelper.COLUMN_NOTES_ADD_TIME);
        }
        if (note.getContent() != null) {
            values.put(PipeSQLiteHelper.COLUMN_NOTES_CONTENT, note.getContent());
        } else {
            values.putNull(PipeSQLiteHelper.COLUMN_NOTES_CONTENT);
        }
        if (note.getDeal() != null && note.getDeal().isStored()) {
            values.put(PipeSQLiteHelper.COLUMN_NOTES_DEAL_SQL_ID, Long.valueOf(note.getDeal().getSqlId()));
        }
        if (note.getPerson() != null && note.getPerson().isStored()) {
            values.put(PipeSQLiteHelper.COLUMN_NOTES_PERSON_SQL_ID, Long.valueOf(note.getPerson().getSqlId()));
        }
        if (note.getOrganization() != null && note.getOrganization().isStored()) {
            values.put(PipeSQLiteHelper.COLUMN_NOTES_ORGANIZATION_SQL_ID, Long.valueOf(note.getOrganization().getSqlId()));
        }
        return values;
    }

    @NonNull
    protected String getColumnNameForPipedriveId() {
        return PipeSQLiteHelper.COLUMN_NOTES_PIPEDRIVE_ID;
    }

    @NonNull
    protected String getColumnNameForSqlId() {
        return PipeSQLiteHelper.COLUMN_ID;
    }

    @NonNull
    protected String getTableName() {
        return "notes";
    }

    @NonNull
    protected String[] getAllColumns() {
        return new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_NOTES_PIPEDRIVE_ID, PipeSQLiteHelper.COLUMN_NOTES_ACTIVE, PipeSQLiteHelper.COLUMN_NOTES_ADD_TIME, PipeSQLiteHelper.COLUMN_NOTES_CONTENT, PipeSQLiteHelper.COLUMN_NOTES_DEAL_SQL_ID, PipeSQLiteHelper.COLUMN_NOTES_ORGANIZATION_SQL_ID, PipeSQLiteHelper.COLUMN_NOTES_PERSON_SQL_ID};
    }

    @Nullable
    protected Note deflateCursor(@NonNull Cursor cursor) {
        boolean z = true;
        Note note = new Note();
        note.setSqlId(cursor.getLong(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_ID)));
        note.setPipedriveId(cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_NOTES_PIPEDRIVE_ID)));
        if (cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_NOTES_ACTIVE)) != 1) {
            z = false;
        }
        note.setActiveFlag(z);
        note.setAddTime(CursorHelper.getPipedriveDateTimeFromColumnHeldInSeconds(cursor, PipeSQLiteHelper.COLUMN_NOTES_ADD_TIME));
        note.setContent(cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_NOTES_CONTENT)));
        int columnIndex = cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_NOTES_DEAL_SQL_ID);
        if (!cursor.isNull(columnIndex)) {
            note.setDeal((Deal) getDealsDataSource().findBySqlId(cursor.getLong(columnIndex)));
        }
        columnIndex = cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_NOTES_PERSON_SQL_ID);
        if (!cursor.isNull(columnIndex)) {
            note.setPerson((Person) getPersonsDataSource().findBySqlId(cursor.getLong(columnIndex)));
        }
        columnIndex = cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_NOTES_ORGANIZATION_SQL_ID);
        if (!cursor.isNull(columnIndex)) {
            note.setOrganization((Organization) getOrganizationsDataSource().findBySqlId(cursor.getLong(columnIndex)));
        }
        return note;
    }
}
