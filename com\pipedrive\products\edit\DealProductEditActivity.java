package com.pipedrive.products.edit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.pipedrive.BaseActivity;
import com.pipedrive.R;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.navigator.Navigable;
import com.pipedrive.navigator.Navigator;
import com.pipedrive.navigator.Navigator.Type;
import com.pipedrive.navigator.buttons.DeleteMenuItem;
import com.pipedrive.navigator.buttons.DiscardMenuItem;
import com.pipedrive.navigator.buttons.NavigatorMenuItem;
import com.pipedrive.products.VariationSelectionDialogFragment;
import com.pipedrive.products.VariationSelectionDialogFragment.OnVariationSelectedListener;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.views.edit.products.DealProductDiscountEditText;
import com.pipedrive.views.edit.products.DealProductDurationEditText;
import com.pipedrive.views.edit.products.DealProductNumericEditTextWithFormatting.OnValueChangedListener;
import com.pipedrive.views.edit.products.DealProductPriceEditText;
import com.pipedrive.views.edit.products.DealProductQuantityEditText;
import com.pipedrive.views.edit.products.ProductTotalTextView;
import com.pipedrive.views.edit.products.ProductVariationView;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class DealProductEditActivity extends BaseActivity implements DealProductEditView, Navigable, OnVariationSelectedListener {
    private static final String ARG_DEALPRODUCT_SQL_ID = "dealproduct_sql_id";
    public static final String ARG_DEALPRODUCT_WAS_REMOVED = "arg_dealproduct_was_removed";
    static final String ARG_DEAL_SQL_ID = "deal_sql_id";
    static final String ARG_PRODUCT_SQL_ID = "product_sql_id";
    @BindView(2131820820)
    DealProductDiscountEditText mDealProductDiscountEditText;
    @BindView(2131820819)
    DealProductDurationEditText mDealProductDurationEditText;
    @BindView(2131820817)
    DealProductPriceEditText mDealProductPriceEditText;
    @BindView(2131820818)
    DealProductQuantityEditText mDealProductQuantityEditText;
    private final DeleteMenuItem mDeleteMenuItem = new DeleteMenuItem() {
        public boolean isEnabled() {
            return DealProductEditActivity.this.mPresenter.canDeleteDealProduct();
        }

        public void onClick() {
            DealProductEditActivity.this.onDeleteDealProduct();
        }
    };
    private final DiscardMenuItem mDiscardMenuItem = new DiscardMenuItem() {
        public void onClick() {
            DealProductEditActivity.this.onCancel();
        }
    };
    private DealProductEditPresenter mPresenter;
    @BindView(2131820816)
    ProductVariationView mProductVariationView;
    @BindView(2131820650)
    TextView mTitle;
    @BindView(2131820700)
    Toolbar mToolbar;
    @BindView(2131821075)
    ProductTotalTextView mTotalValue;

    public static void startActivityForResult(@NonNull Activity activity, @NonNull Long dealProductSqlId, int requestCode) {
        Intent intent = new Intent(activity, DealProductEditActivity.class);
        intent.putExtra(ARG_DEALPRODUCT_SQL_ID, dealProductSqlId);
        ActivityCompat.startActivityForResult(activity, intent, requestCode, null);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_edit);
        ButterKnife.bind(this);
        this.mPresenter = new DealProductEditPresenterImpl(getSession(), savedInstanceState);
        setupToolbar();
        setNavigatorType(Type.UPDATE);
    }

    private void setupToolbar() {
        setSupportActionBar(this.mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }
    }

    private void requestDealProduct() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            boolean newDealProductRequested = extras.containsKey(ARG_PRODUCT_SQL_ID);
            boolean changeOfDealProductRequested = extras.containsKey(ARG_DEALPRODUCT_SQL_ID);
            if (newDealProductRequested) {
                this.mPresenter.requestNewDealProduct(Long.valueOf(extras.getLong(ARG_PRODUCT_SQL_ID)), Long.valueOf(extras.getLong(ARG_DEAL_SQL_ID)));
            } else if (changeOfDealProductRequested) {
                this.mPresenter.requestExistingDealProduct(Long.valueOf(extras.getLong(ARG_DEALPRODUCT_SQL_ID)));
            } else {
                finish();
            }
        }
    }

    public void onResume() {
        super.onResume();
        this.mPresenter.bindView(this);
        requestDealProduct();
    }

    private void setupPriceEditText(@NonNull DealProduct currentlyManagedDealProduct) {
        this.mDealProductPriceEditText.setup(getSession(), currentlyManagedDealProduct, new OnValueChangedListener() {
            public void onValueChanged(double value) {
                DealProductEditActivity.this.mTotalValue.updatePrice(new BigDecimal(value));
            }
        });
    }

    private void setupQuantityEditText(@NonNull DealProduct currentlyManagedDealProduct) {
        this.mDealProductQuantityEditText.setup(getSession(), currentlyManagedDealProduct, new OnValueChangedListener() {
            public void onValueChanged(double value) {
                DealProductEditActivity.this.mTotalValue.updateQuantity(value);
            }
        });
    }

    private void setupDiscountEditText(@NonNull DealProduct currentlyManagedDealProduct) {
        this.mDealProductDiscountEditText.setup(getSession(), currentlyManagedDealProduct, new OnValueChangedListener() {
            public void onValueChanged(double value) {
                DealProductEditActivity.this.mTotalValue.updateDiscount(value);
            }
        });
    }

    private void setupDurationEditText(@NonNull DealProduct currentlyManagedDealProduct) {
        int i = 0;
        boolean isDurationEnabled = getSession().isCompanyFeatureProductDurationsEnabled(false);
        DealProductDurationEditText dealProductDurationEditText = this.mDealProductDurationEditText;
        if (!isDurationEnabled) {
            i = 8;
        }
        dealProductDurationEditText.setVisibility(i);
        if (isDurationEnabled) {
            this.mDealProductDurationEditText.setup(getSession(), currentlyManagedDealProduct, new OnValueChangedListener() {
                public void onValueChanged(double value) {
                    DealProductEditActivity.this.mTotalValue.updateDuration(value);
                }
            });
        }
    }

    private void setupVariationView(@NonNull DealProduct dealProduct) {
        boolean isAbleToChangeProductVariation = VariationSelectionDialogFragment.isAbleToChangeProductVariation(getSession(), dealProduct);
        this.mProductVariationView.setVisibility(isAbleToChangeProductVariation ? 0 : 8);
        if (isAbleToChangeProductVariation) {
            this.mProductVariationView.setup(dealProduct);
        }
    }

    public void onDealProductRequested(@Nullable DealProduct dealProduct) {
        if (dealProduct == null) {
            finish();
            return;
        }
        setupPriceEditText(dealProduct);
        setupDiscountEditText(dealProduct);
        setupQuantityEditText(dealProduct);
        setupDurationEditText(dealProduct);
        setupVariationView(dealProduct);
        this.mTitle.setText(dealProduct.getProduct().getName());
        this.mTotalValue.setup(dealProduct, getSession());
    }

    public void onDealProductCreatedOrUpdated() {
        finish();
    }

    public void onDealProductDeleted() {
        setResult(-1, new Intent().putExtra(ARG_DEALPRODUCT_WAS_REMOVED, true));
        finish();
    }

    @OnClick({2131820816})
    public void onVariationClicked() {
        DealProduct dealProduct = this.mPresenter.getCurrentlyManagedDealProduct();
        if (dealProduct != null) {
            VariationSelectionDialogFragment.show(getFragmentManager(), dealProduct);
        }
    }

    public void onVariationSelected(@Nullable Long selectedVariationSqlId) {
        this.mPresenter.updateDealProductsVariation(selectedVariationSqlId);
    }

    public boolean isChangesMadeToFieldsAfterInitialLoad() {
        updateDealProductWithEnteredData();
        return this.mPresenter.changesAreMadeToDealProduct();
    }

    void updateDealProductWithEnteredData() {
        this.mPresenter.updateDealProductWithEnteredData(Double.valueOf(this.mDealProductPriceEditText.getValue()), Double.valueOf(this.mDealProductQuantityEditText.getValue()), this.mProductVariationView.getProductVariation(), Double.valueOf(this.mDealProductDiscountEditText.getValue()), getSession().isCompanyFeatureProductDurationsEnabled(false) ? Double.valueOf(this.mDealProductDurationEditText.getValue()) : null);
    }

    protected void onPause() {
        this.mPresenter.unbindView();
        super.onPause();
    }

    private void onDeleteDealProduct() {
        ViewUtil.showDeleteConfirmationDialog(this, getResources().getString(R.string.remove_this_product), new Runnable() {
            public void run() {
                DealProductEditActivity.this.mPresenter.deleteDealProduct();
            }
        });
    }

    public boolean isAllRequiredFieldsSet() {
        return true;
    }

    public void onCreateOrUpdate() {
        this.mPresenter.changeDealProduct();
    }

    public void createOrUpdateDiscardedAsRequiredFieldsAreNotSet() {
    }

    public void onCancel() {
        finish();
    }

    @Nullable
    public List<? extends NavigatorMenuItem> getAdditionalButtons() {
        return Arrays.asList(new NavigatorMenuItem[]{this.mDeleteMenuItem, this.mDiscardMenuItem});
    }

    @Nullable
    protected Navigator getNavigatorImplementation() {
        return new Navigator(this, this.mToolbar);
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        updateDealProductWithEnteredData();
        this.mPresenter.onSaveInstanceState(outState);
    }
}
