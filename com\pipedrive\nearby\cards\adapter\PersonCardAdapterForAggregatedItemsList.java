package com.pipedrive.nearby.cards.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.cards.cards.CardHeader;
import com.pipedrive.nearby.cards.cards.PersonCardHeader;
import com.pipedrive.nearby.model.NearbyItem;
import java.util.List;

public class PersonCardAdapterForAggregatedItemsList extends CardAdapterForAggregatedItemsList {
    public PersonCardAdapterForAggregatedItemsList(@NonNull Session session, @NonNull List<? extends NearbyItem> itemsList) {
        super(session, itemsList);
    }

    AggregatedItemListViewHolder createViewHolder(@NonNull ViewGroup parent) {
        return new AggregatedItemListViewHolder((ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.view_person_card_header, parent, false));
    }

    CardHeader getCardHeader() {
        return new PersonCardHeader();
    }
}
