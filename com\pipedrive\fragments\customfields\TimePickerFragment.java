package com.pipedrive.fragments.customfields;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.widget.TimePicker;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.background.ApplicationStateMonitor;
import com.newrelic.agent.android.instrumentation.Instrumented;
import com.pipedrive.datetime.DateFormatHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.util.time.TimeManager;
import java.text.ParseException;
import java.util.Calendar;

@Instrumented
public class TimePickerFragment extends DialogFragment implements OnTimeSetListener, TraceFieldInterface {
    public static final String VALUE_IS_START_FIELD = "isStartField";
    public static final String VALUE_TIME = "time";
    private DateTimePickerInterface mDateTimePickerInterface;
    private boolean mIsStartField;

    protected void onStart() {
        super.onStart();
        ApplicationStateMonitor.getInstance().activityStarted();
    }

    protected void onStop() {
        super.onStop();
        ApplicationStateMonitor.getInstance().activityStopped();
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar calendar = TimeManager.getInstance().getLocalCalendar();
        Bundle bundle = getArguments();
        String timeValueString = bundle.getString("time");
        this.mIsStartField = bundle.getBoolean("isStartField");
        if (!TextUtils.isEmpty(timeValueString)) {
            try {
                calendar.setTime(DateFormatHelper.timeFormat2().parse(timeValueString));
            } catch (ParseException e) {
                Log.e(e);
            }
        }
        int hour = calendar.get(11);
        int minute = calendar.get(12);
        return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        if (this.mDateTimePickerInterface != null) {
            Calendar calendar = TimeManager.getInstance().getLocalCalendar();
            calendar.set(11, hourOfDay);
            calendar.set(12, minute);
            this.mDateTimePickerInterface.dateChanged(calendar.getTime(), this.mIsStartField);
        }
    }

    public void setDateTimePickerCallback(DateTimePickerInterface dateTimePickerInterface) {
        this.mDateTimePickerInterface = dateTimePickerInterface;
    }
}
