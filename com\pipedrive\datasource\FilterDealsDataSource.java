package com.pipedrive.datasource;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.model.Deal;

public class FilterDealsDataSource extends SQLTransactionManager {
    public FilterDealsDataSource(SQLiteDatabase dbconn) {
        super(dbconn);
    }

    public void relateDealToFilterId(@NonNull Deal deal, @IntRange(from = 1) long filterId) {
        if (deal.isExisting()) {
            ContentValues contentValues = new ContentValues(2);
            contentValues.put(PipeSQLiteHelper.COLUMN_FILTER_DEALS_DEAL_PIPEDRIVE_ID, Integer.valueOf(deal.getPipedriveId()));
            contentValues.put(PipeSQLiteHelper.COLUMN_FILTER_DEALS_FILTER_PIPEDRIVE_ID, Long.valueOf(filterId));
            SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
            String str = PipeSQLiteHelper.TABLE_FILTER_DEALS;
            if (transactionalDBConnection instanceof SQLiteDatabase) {
                SQLiteInstrumentation.insertWithOnConflict(transactionalDBConnection, str, null, contentValues, 4);
            } else {
                transactionalDBConnection.insertWithOnConflict(str, null, contentValues, 4);
            }
        }
    }

    public void unrelateAllDealAssociations(@NonNull Deal deal) {
        if (deal.isExisting()) {
            SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
            String str = PipeSQLiteHelper.TABLE_FILTER_DEALS;
            String str2 = "filter_deals_pipedrive_id=?";
            String[] strArr = new String[]{String.valueOf(deal.getPipedriveId())};
            if (transactionalDBConnection instanceof SQLiteDatabase) {
                SQLiteInstrumentation.delete(transactionalDBConnection, str, str2, strArr);
            } else {
                transactionalDBConnection.delete(str, str2, strArr);
            }
        }
    }

    public void deleteAllFilterAssociationsForStage(@IntRange(from = 1) Long filterPipedriveId, @IntRange(from = 1) @NonNull Integer stagePipedriveId) {
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String str = PipeSQLiteHelper.TABLE_FILTER_DEALS;
        String str2 = "filter_deals_filter_pipedrive_id=? AND filter_deals_pipedrive_id IN (SELECT d_pd_id FROM deals WHERE d_stage=?)";
        String[] strArr = new String[]{String.valueOf(filterPipedriveId), String.valueOf(stagePipedriveId)};
        if (transactionalDBConnection instanceof SQLiteDatabase) {
            SQLiteInstrumentation.delete(transactionalDBConnection, str, str2, strArr);
        } else {
            transactionalDBConnection.delete(str, str2, strArr);
        }
    }
}
