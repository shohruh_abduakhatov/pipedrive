package com.pipedrive.nearby.cards.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.nearby.cards.cards.CardHeader;
import com.pipedrive.nearby.model.NearbyItem;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

class AggregatedItemListViewHolder<NEARBY_ITEM extends NearbyItem<ENTITY>, ENTITY extends BaseDatasourceEntity> extends ViewHolder {
    @Nullable
    private CardHeader<NEARBY_ITEM, ENTITY> cardHeader;
    @NonNull
    private CompositeSubscription subscription = new CompositeSubscription();

    AggregatedItemListViewHolder(View itemView) {
        super(itemView);
    }

    void bind(@NonNull final Session session, @NonNull final NEARBY_ITEM nearbyItem, @NonNull final CardHeader<NEARBY_ITEM, ENTITY> cardHeader) {
        this.cardHeader = cardHeader;
        this.subscription.add(nearbyItem.getData(session).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<ENTITY>() {
            public void call(ENTITY entity) {
                cardHeader.bind((ViewGroup) AggregatedItemListViewHolder.this.itemView.findViewById(R.id.nearbyCard.headerContainer), session, nearbyItem, entity);
                AggregatedItemListViewHolder.this.setOnItemClickListener(entity);
            }
        }, new Action1<Throwable>() {
            public void call(Throwable throwable) {
            }
        }));
    }

    private void setOnItemClickListener(final ENTITY entity) {
        this.itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                AggregatedItemListViewHolder.this.startActivityOnclick(entity.getSqlIdOrNull());
            }
        });
    }

    void unbind() {
        this.subscription.clear();
    }

    private void startActivityOnclick(@Nullable Long itemSqlId) {
        if (itemSqlId != null && this.cardHeader != null) {
            this.cardHeader.propagateClickEvents(itemSqlId);
        }
    }
}
