package com.pipedrive.note;

import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.model.notes.Note;

@MainThread
abstract class NoteEditorPresenter extends HtmlEditorPresenter<Note, NoteEditorView> {
    abstract void changeNote();

    abstract void deleteNote();

    abstract void requestNewNoteForDeal(long j);

    abstract void requestNewNoteForOrg(long j);

    abstract void requestNewNoteForPerson(long j);

    abstract void requestNote(long j);

    public NoteEditorPresenter(@NonNull Session session, @Nullable Bundle savedState) {
        super(session, savedState);
    }
}
