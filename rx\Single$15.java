package rx;

import rx.functions.Action1;

class Single$15 implements Action1<T> {
    final /* synthetic */ Single this$0;
    final /* synthetic */ Action1 val$onNotification;

    Single$15(Single single, Action1 action1) {
        this.this$0 = single;
        this.val$onNotification = action1;
    }

    public void call(T t) {
        this.val$onNotification.call(Notification.createOnNext(t));
    }
}
