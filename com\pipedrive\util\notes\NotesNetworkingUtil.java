package com.pipedrive.util.notes;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.NotesDataSource;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.notes.Note;
import com.pipedrive.util.networking.entities.NoteEntity;
import com.pipedrive.util.organizations.OrganizationsNetworkingUtil;
import com.pipedrive.util.persons.PersonNetworkingUtil;

class NotesNetworkingUtil {
    NotesNetworkingUtil() {
    }

    @WorkerThread
    public static Note createOrUpdateNoteIntoDBWithRelations(@NonNull Session session, @NonNull NoteEntity noteEntity, @Nullable Long noteSqlId) {
        Note note = Note.fromEntityReturnedWithoutAssociations(noteEntity);
        if (noteSqlId != null) {
            note.setSqlId(noteSqlId.longValue());
        }
        Person noteRelatedPerson = null;
        if (noteEntity.personId > 0) {
            noteRelatedPerson = new PersonsDataSource(session.getDatabase()).findPersonByPipedriveId((long) noteEntity.personId);
            if (noteRelatedPerson == null) {
                noteRelatedPerson = PersonNetworkingUtil.downloadPerson(session, noteEntity.personId);
                if (noteRelatedPerson != null) {
                    noteRelatedPerson = PersonNetworkingUtil.createOrUpdatePersonIntoDBWithRelations(session, noteRelatedPerson);
                }
            }
        }
        note.setPerson(noteRelatedPerson);
        Organization noteOrganization = null;
        if (noteEntity.orgId > 0) {
            noteOrganization = new OrganizationsDataSource(session.getDatabase()).findOrganizationByPipedriveId(noteEntity.orgId);
            if (noteOrganization == null) {
                noteOrganization = OrganizationsNetworkingUtil.downloadOrganization(session, noteEntity.orgId);
                if (noteOrganization != null) {
                    noteOrganization = OrganizationsNetworkingUtil.createOrUpdateOrganizationIntoDBWithRelations(session, noteOrganization);
                }
            }
        }
        note.setOrganization(noteOrganization);
        Deal noteDeal = null;
        if (noteEntity.dealId > 0) {
            noteDeal = new DealsDataSource(session.getDatabase()).findDealByDealId((long) noteEntity.dealId);
            if (noteDeal == null) {
                noteDeal = DealsNetworkingUtil.downloadDeal(session, (long) noteEntity.dealId);
                if (noteDeal != null) {
                    noteDeal = DealsNetworkingUtil.createOrUpdateDealIntoDBWithRelations(session, noteDeal);
                }
            }
        }
        note.setDeal(noteDeal);
        long createdNoteSqlId = new NotesDataSource(session.getDatabase()).createOrUpdate(note);
        if (createdNoteSqlId == -1) {
            return null;
        }
        note.setSqlId(createdNoteSqlId);
        return note;
    }

    @WorkerThread
    public static Note createOrUpdateNoteIntoDBWithRelations(Session session, NoteEntity noteEntity) {
        return createOrUpdateNoteIntoDBWithRelations(session, noteEntity, null);
    }
}
