package com.zendesk.sdk.model.request;

import android.support.annotation.Nullable;

public class UploadResponseWrapper {
    private UploadResponse upload;

    @Nullable
    public UploadResponse getUpload() {
        return this.upload;
    }
}
