package com.pipedrive.logging;

import android.support.annotation.NonNull;
import com.crashlytics.android.Crashlytics;
import com.newrelic.agent.android.NewRelic;
import com.newrelic.agent.android.analytics.AnalyticAttribute;
import com.pipedrive.application.Session;

public enum Log {
    ;
    
    private static final boolean LOGCAT_FULL_LOGGING = false;

    public static void v(String tag, String message) {
    }

    public static void d(String tag, String message) {
    }

    public static void d(@NonNull String tag, @NonNull String message, @NonNull Throwable throwable) {
    }

    public static void i(String tag, String message) {
    }

    public static void w(String tag, String message) {
    }

    public static void e(Throwable e) {
        if (!(e == null)) {
            android.util.Log.e(e.getClass().getName(), e.getMessage(), e);
        }
    }

    public static void startMethodTracing() {
    }

    public static void stopMethodTracing() {
    }

    public static void setupCrashLoggersMetadata(Session session) {
        boolean setupNewrelicMetadata = false;
        if (session != null) {
            boolean setupCrashlyticsMetadata;
            if (session.getRuntimeCache().isCrashlyticsSetup) {
                setupCrashlyticsMetadata = false;
            } else {
                setupCrashlyticsMetadata = true;
            }
            if (setupCrashlyticsMetadata) {
                Crashlytics.getInstance().core.setUserIdentifier(session.getAuthenticatedUserID() + "");
                String attributeCompanyIdCrashlytics = "Company ID";
                Crashlytics.getInstance().core.setLong("Company ID", session.getSelectedCompanyID());
                session.getRuntimeCache().isCrashlyticsSetup = true;
            }
            if (!session.getRuntimeCache().isNewRelicSetup) {
                setupNewrelicMetadata = true;
            }
            if (setupNewrelicMetadata) {
                String attributeUserIdNewRelic = AnalyticAttribute.USER_ID_ATTRIBUTE;
                NewRelic.setAttribute(AnalyticAttribute.USER_ID_ATTRIBUTE, (float) session.getAuthenticatedUserID());
                NewRelic.setUserId(String.valueOf(session.getAuthenticatedUserID()));
                String attributeCompanyIdNewRelic = "companyId";
                NewRelic.setAttribute("companyId", (float) session.getSelectedCompanyID());
                session.getRuntimeCache().isNewRelicSetup = true;
            }
        }
    }
}
