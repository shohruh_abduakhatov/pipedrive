package rx;

class Completable$11 implements Completable$OnSubscribe {
    final /* synthetic */ Single val$single;

    Completable$11(Single single) {
        this.val$single = single;
    }

    public void call(final CompletableSubscriber s) {
        SingleSubscriber<Object> te = new SingleSubscriber<Object>() {
            public void onError(Throwable e) {
                s.onError(e);
            }

            public void onSuccess(Object value) {
                s.onCompleted();
            }
        };
        s.onSubscribe(te);
        this.val$single.subscribe(te);
    }
}
