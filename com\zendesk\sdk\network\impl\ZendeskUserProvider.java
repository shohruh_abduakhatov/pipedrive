package com.zendesk.sdk.network.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.sdk.model.SdkConfiguration;
import com.zendesk.sdk.model.request.User;
import com.zendesk.sdk.model.request.UserField;
import com.zendesk.sdk.model.request.UserFieldRequest;
import com.zendesk.sdk.model.request.UserFieldResponse;
import com.zendesk.sdk.model.request.UserResponse;
import com.zendesk.sdk.model.request.UserTagRequest;
import com.zendesk.sdk.network.BaseProvider;
import com.zendesk.sdk.network.UserProvider;
import com.zendesk.service.ZendeskCallback;
import com.zendesk.util.CollectionUtils;
import com.zendesk.util.StringUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class ZendeskUserProvider implements UserProvider {
    private final BaseProvider baseProvider;
    private final ZendeskUserService userService;

    ZendeskUserProvider(BaseProvider baseProvider, ZendeskUserService userService) {
        this.baseProvider = baseProvider;
        this.userService = userService;
    }

    public void addTags(@NonNull final List<String> tags, @Nullable final ZendeskCallback<List<String>> callback) {
        this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
            public void onSuccess(SdkConfiguration config) {
                UserTagRequest userTagRequest = new UserTagRequest();
                userTagRequest.setTags(CollectionUtils.ensureEmpty(tags));
                ZendeskUserProvider.this.userService.addTags(config.getBearerAuthorizationHeader(), userTagRequest, new PassThroughErrorZendeskCallback<UserResponse>(callback) {
                    public void onSuccess(UserResponse userResponse) {
                        if (callback != null) {
                            callback.onSuccess(ZendeskUserProvider.this.getTags(userResponse));
                        }
                    }
                });
            }
        });
    }

    public void deleteTags(@NonNull final List<String> tags, @Nullable final ZendeskCallback<List<String>> callback) {
        this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
            public void onSuccess(SdkConfiguration config) {
                ZendeskUserProvider.this.userService.deleteTags(config.getBearerAuthorizationHeader(), StringUtils.toCsvString(CollectionUtils.ensureEmpty(tags)), new PassThroughErrorZendeskCallback<UserResponse>(callback) {
                    public void onSuccess(UserResponse userResponse) {
                        if (callback != null) {
                            callback.onSuccess(ZendeskUserProvider.this.getTags(userResponse));
                        }
                    }
                });
            }
        });
    }

    public void getUserFields(@Nullable final ZendeskCallback<List<UserField>> callback) {
        this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
            public void onSuccess(SdkConfiguration config) {
                ZendeskUserProvider.this.userService.getUserFields(config.getBearerAuthorizationHeader(), new PassThroughErrorZendeskCallback<UserFieldResponse>(callback) {
                    public void onSuccess(UserFieldResponse userFieldResponse) {
                        if (callback != null) {
                            callback.onSuccess(userFieldResponse.getUserFields());
                        }
                    }
                });
            }
        });
    }

    public void setUserFields(@NonNull final Map<String, String> userFields, @Nullable final ZendeskCallback<Map<String, String>> callback) {
        this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
            public void onSuccess(SdkConfiguration config) {
                ZendeskUserProvider.this.userService.setUserFields(config.getBearerAuthorizationHeader(), new UserFieldRequest(userFields), new PassThroughErrorZendeskCallback<UserResponse>(callback) {
                    public void onSuccess(UserResponse userResponse) {
                        if (callback != null) {
                            callback.onSuccess(ZendeskUserProvider.this.getUserFields(userResponse));
                        }
                    }
                });
            }
        });
    }

    public void getUser(@Nullable final ZendeskCallback<User> callback) {
        this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
            public void onSuccess(SdkConfiguration config) {
                ZendeskUserProvider.this.userService.getUser(config.getBearerAuthorizationHeader(), new PassThroughErrorZendeskCallback<UserResponse>(callback) {
                    public void onSuccess(UserResponse userResponse) {
                        if (callback != null) {
                            callback.onSuccess(userResponse.getUser());
                        }
                    }
                });
            }
        });
    }

    @NonNull
    private List<String> getTags(UserResponse userResponse) {
        return (userResponse == null || userResponse.getUser() == null) ? new ArrayList() : userResponse.getUser().getTags();
    }

    @NonNull
    private Map<String, String> getUserFields(UserResponse userResponse) {
        return (userResponse == null || userResponse.getUser() == null) ? new HashMap() : userResponse.getUser().getUserFields();
    }
}
