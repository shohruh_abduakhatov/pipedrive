package com.pipedrive.nearby.initialzoom;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.util.Distance;
import com.pipedrive.util.CursorHelper;
import java.util.TreeSet;
import rx.Single;
import rx.functions.Action2;
import rx.functions.Func0;
import rx.functions.Func1;

public class InitialZoomCalculatorRx {
    private static final Double DEGREE_METERS = Distance.KILOMETERS.toMeters(Double.valueOf(111.0d));

    @NonNull
    Single<Double> getMaxDistance(@NonNull Session session, @NonNull Double currentLatitude, @NonNull Double currentLongitude, @NonNull String tableName, @NonNull String latitudeColumnName, @NonNull String longitudeColumnName, @NonNull Integer maxCount, @NonNull Double maxDistanceMeters) {
        final LatLng currentLocation = new LatLng(currentLatitude.doubleValue(), currentLongitude.doubleValue());
        String[] columns = new String[]{latitudeColumnName, longitudeColumnName};
        String selection = latitudeColumnName + " between ? and ? " + " and " + longitudeColumnName + " between ? and ? ";
        String[] selectionArgs = new String[]{String.valueOf(currentLatitude.doubleValue() - (maxDistanceMeters.doubleValue() / DEGREE_METERS.doubleValue())), String.valueOf(currentLatitude.doubleValue() + (maxDistanceMeters.doubleValue() / DEGREE_METERS.doubleValue())), String.valueOf(currentLongitude.doubleValue() - (maxDistanceMeters.doubleValue() / (DEGREE_METERS.doubleValue() * Math.cos(Math.toRadians(currentLatitude.doubleValue()))))), String.valueOf(currentLongitude.doubleValue() + (maxDistanceMeters.doubleValue() / (DEGREE_METERS.doubleValue() * Math.cos(Math.toRadians(currentLatitude.doubleValue())))))};
        SQLiteDatabase database = session.getDatabase();
        final String str = latitudeColumnName;
        final String str2 = longitudeColumnName;
        final Integer num = maxCount;
        final Double d = maxDistanceMeters;
        return CursorHelper.observeAllRows(!(database instanceof SQLiteDatabase) ? database.query(tableName, columns, selection, selectionArgs, null, null, null) : SQLiteInstrumentation.query(database, tableName, columns, selection, selectionArgs, null, null, null)).onErrorReturn(new Func1<Throwable, Cursor>() {
            public Cursor call(Throwable throwable) {
                return null;
            }
        }).map(new Func1<Cursor, LatLng>() {
            public LatLng call(Cursor cursor) {
                if (cursor == null) {
                    return null;
                }
                Double lat = CursorHelper.getDouble(cursor, str);
                Double lon = CursorHelper.getDouble(cursor, str2);
                if (lat == null || lon == null) {
                    return null;
                }
                return new LatLng(lat.doubleValue(), lon.doubleValue());
            }
        }).filter(new Func1<LatLng, Boolean>() {
            public Boolean call(LatLng latLng) {
                return Boolean.valueOf(latLng != null);
            }
        }).map(new Func1<LatLng, Double>() {
            public Double call(LatLng latLng) {
                return Double.valueOf(SphericalUtil.computeDistanceBetween(currentLocation, latLng));
            }
        }).defaultIfEmpty(maxDistanceMeters).collect(new Func0<TreeSet<Double>>() {
            public TreeSet<Double> call() {
                return new TreeSet();
            }
        }, new Action2<TreeSet<Double>, Double>() {
            public void call(TreeSet<Double> treeSet, Double distance) {
                if (treeSet.size() < num.intValue()) {
                    treeSet.add(distance);
                } else if (distance.doubleValue() < ((Double) treeSet.last()).doubleValue()) {
                    treeSet.pollLast();
                    treeSet.add(distance);
                }
            }
        }).map(new Func1<TreeSet<Double>, Double>() {
            public Double call(TreeSet<Double> doubleSortedList) {
                return Double.valueOf(Math.min(((Double) doubleSortedList.pollLast()).doubleValue(), d.doubleValue()));
            }
        }).toSingle();
    }
}
