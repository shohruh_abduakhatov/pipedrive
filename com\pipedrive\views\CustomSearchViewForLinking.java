package com.pipedrive.views;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import com.pipedrive.R;

public class CustomSearchViewForLinking extends CustomSearchView {
    public CustomSearchViewForLinking(Context context) {
        this(context, null);
    }

    public CustomSearchViewForLinking(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomSearchViewForLinking(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @LayoutRes
    int getLayoutResId() {
        return R.layout.custom_search_base;
    }

    public void setSearchHint(@StringRes int id) {
        this.mSearchEditText.setHint(id);
    }
}
