package rx;

import rx.functions.Action0;
import rx.subscriptions.BooleanSubscription;

class Completable$8 implements Completable$OnSubscribe {
    final /* synthetic */ Action0 val$action;

    Completable$8(Action0 action0) {
        this.val$action = action0;
    }

    public void call(CompletableSubscriber s) {
        BooleanSubscription bs = new BooleanSubscription();
        s.onSubscribe(bs);
        try {
            this.val$action.call();
            if (!bs.isUnsubscribed()) {
                s.onCompleted();
            }
        } catch (Throwable e) {
            if (!bs.isUnsubscribed()) {
                s.onError(e);
            }
        }
    }
}
