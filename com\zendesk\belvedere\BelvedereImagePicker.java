package com.zendesk.belvedere;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.ClipData.Item;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import com.newrelic.agent.android.instrumentation.AsyncTaskInstrumentation;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

class BelvedereImagePicker {
    private static final String LOG_TAG = "BelvedereImagePicker";
    private final BelvedereConfig belvedereConfig;
    private final BelvedereStorage belvedereStorage;
    private final Map<Integer, BelvedereResult> cameraImages = new HashMap();
    private final BelvedereLogger log;

    BelvedereImagePicker(BelvedereConfig belvedereConfig, BelvedereStorage belvedereStorage) {
        this.belvedereConfig = belvedereConfig;
        this.belvedereStorage = belvedereStorage;
        this.log = belvedereConfig.getBelvedereLogger();
    }

    @NonNull
    List<BelvedereIntent> getBelvedereIntents(@NonNull Context context) {
        TreeSet<BelvedereSource> belvedereSources = this.belvedereConfig.getBelvedereSources();
        List<BelvedereIntent> belvedereIntents = new ArrayList();
        Iterator it = belvedereSources.iterator();
        while (it.hasNext()) {
            BelvedereIntent intent = null;
            switch ((BelvedereSource) it.next()) {
                case Gallery:
                    intent = getGalleryIntent(context);
                    break;
                case Camera:
                    intent = getCameraIntent(context);
                    break;
            }
            if (intent != null) {
                belvedereIntents.add(intent);
            }
        }
        return belvedereIntents;
    }

    @Nullable
    BelvedereIntent getGalleryIntent(@NonNull Context context) {
        if (hasGalleryApp(context)) {
            return new BelvedereIntent(getGalleryIntent(), this.belvedereConfig.getGalleryRequestCode(), BelvedereSource.Gallery);
        }
        return null;
    }

    @Nullable
    private BelvedereIntent getCameraIntent(@NonNull Context context) {
        if (canPickImageFromCamera(context)) {
            return pickImageFromCameraInternal(context);
        }
        return null;
    }

    private boolean canPickImageFromCamera(@NonNull Context context) {
        if (hasCamera(context)) {
            if (!hasPermissionInManifest(context, "android.permission.CAMERA")) {
                return true;
            }
            boolean hasCameraPermissionGranted;
            if (ContextCompat.checkSelfPermission(context, "android.permission.CAMERA") == 0) {
                hasCameraPermissionGranted = true;
            } else {
                hasCameraPermissionGranted = false;
            }
            if (hasCameraPermissionGranted) {
                return true;
            }
            this.log.w(LOG_TAG, "Found Camera permission declared in AndroidManifest.xml and the user hasn't granted that permission. Not doing any further efforts to acquire that permission.");
        }
        return false;
    }

    @NonNull
    private boolean hasCamera(@NonNull Context context) {
        boolean hasCamera;
        Intent mockIntent = new Intent();
        mockIntent.setAction("android.media.action.IMAGE_CAPTURE");
        PackageManager packageManager = context.getPackageManager();
        if (packageManager.hasSystemFeature("android.hardware.camera") || packageManager.hasSystemFeature("android.hardware.camera.front")) {
            hasCamera = true;
        } else {
            hasCamera = false;
        }
        boolean hasCameraApp = isIntentResolvable(mockIntent, context);
        this.log.d(LOG_TAG, String.format(Locale.US, "Camera present: %b, Camera App present: %b", new Object[]{Boolean.valueOf(hasCamera), Boolean.valueOf(hasCameraApp)}));
        if (hasCamera && hasCameraApp) {
            return true;
        }
        return false;
    }

    @NonNull
    private boolean hasGalleryApp(@NonNull Context context) {
        return isIntentResolvable(getGalleryIntent(), context);
    }

    void getFilesFromActivityOnResult(@NonNull Context context, int requestCode, int resultCode, @NonNull Intent data, @Nullable BelvedereCallback<List<BelvedereResult>> callback) {
        List<BelvedereResult> result = new ArrayList();
        BelvedereLogger belvedereLogger;
        String str;
        Locale locale;
        String str2;
        Object[] objArr;
        if (requestCode == this.belvedereConfig.getGalleryRequestCode()) {
            belvedereLogger = this.log;
            str = LOG_TAG;
            locale = Locale.US;
            str2 = "Parsing activity result - Gallery - Ok: %s";
            objArr = new Object[1];
            objArr[0] = Boolean.valueOf(resultCode == -1);
            belvedereLogger.d(str, String.format(locale, str2, objArr));
            if (resultCode == -1) {
                List<Uri> uris = extractUrisFromIntent(data);
                this.log.d(LOG_TAG, String.format(Locale.US, "Number of items received from gallery: %s", new Object[]{Integer.valueOf(uris.size())}));
                BelvedereResolveUriTask belvedereResolveUriTask = new BelvedereResolveUriTask(context, this.log, this.belvedereStorage, callback);
                Object[] toArray = uris.toArray(new Uri[uris.size()]);
                if (belvedereResolveUriTask instanceof AsyncTask) {
                    AsyncTaskInstrumentation.execute(belvedereResolveUriTask, toArray);
                    return;
                } else {
                    belvedereResolveUriTask.execute(toArray);
                    return;
                }
            }
        } else if (this.cameraImages.containsKey(Integer.valueOf(requestCode))) {
            belvedereLogger = this.log;
            str = LOG_TAG;
            locale = Locale.US;
            str2 = "Parsing activity result - Camera - Ok: %s";
            objArr = new Object[1];
            objArr[0] = Boolean.valueOf(resultCode == -1);
            belvedereLogger.d(str, String.format(locale, str2, objArr));
            BelvedereResult belvedereResult = (BelvedereResult) this.cameraImages.get(Integer.valueOf(requestCode));
            this.belvedereStorage.revokePermissionsFromUri(context, belvedereResult.getUri(), 3);
            if (resultCode == -1) {
                result.add(belvedereResult);
                this.log.d(LOG_TAG, String.format(Locale.US, "Image from camera: %s", new Object[]{belvedereResult.getFile()}));
            }
            this.cameraImages.remove(Integer.valueOf(requestCode));
        }
        if (callback != null) {
            callback.internalSuccess(result);
        }
    }

    public boolean oneOrMoreSourceAvailable(@NonNull Context context) {
        for (BelvedereSource s : BelvedereSource.values()) {
            if (isFunctionalityAvailable(s, context)) {
                return true;
            }
        }
        return false;
    }

    public boolean isFunctionalityAvailable(@NonNull BelvedereSource source, @NonNull Context context) {
        if (!this.belvedereConfig.getBelvedereSources().contains(source)) {
            return false;
        }
        switch (source) {
            case Gallery:
                return hasGalleryApp(context);
            case Camera:
                return canPickImageFromCamera(context);
            default:
                return false;
        }
    }

    @NonNull
    private boolean isIntentResolvable(@NonNull Intent intent, @NonNull Context context) {
        return intent.resolveActivity(context.getPackageManager()) != null;
    }

    @SuppressLint({"NewApi"})
    @NonNull
    private List<Uri> extractUrisFromIntent(@NonNull Intent intent) {
        List<Uri> images = new ArrayList();
        if (VERSION.SDK_INT >= 16 && intent.getClipData() != null) {
            ClipData clipData = intent.getClipData();
            int itemCount = clipData.getItemCount();
            for (int i = 0; i < itemCount; i++) {
                Item itemAt = clipData.getItemAt(i);
                if (itemAt.getUri() != null) {
                    images.add(itemAt.getUri());
                }
            }
        } else if (intent.getData() != null) {
            images.add(intent.getData());
        }
        return images;
    }

    @Nullable
    private BelvedereIntent pickImageFromCameraInternal(@NonNull Context context) {
        Set<Integer> integers = this.cameraImages.keySet();
        int requestId = this.belvedereConfig.getCameraRequestCodeEnd();
        for (int i = this.belvedereConfig.getCameraRequestCodeStart(); i < this.belvedereConfig.getCameraRequestCodeEnd(); i++) {
            if (!integers.contains(Integer.valueOf(i))) {
                requestId = i;
                break;
            }
        }
        File imagePath = this.belvedereStorage.getFileForCamera(context);
        if (imagePath == null) {
            this.log.w(LOG_TAG, "Camera Intent. Image path is null. There's something wrong with the storage.");
            return null;
        }
        Uri uriForFile = this.belvedereStorage.getFileProviderUri(context, imagePath);
        if (uriForFile == null) {
            this.log.w(LOG_TAG, "Camera Intent: Uri to file is null. There's something wrong with the storage or FileProvider configuration.");
            return null;
        }
        this.cameraImages.put(Integer.valueOf(requestId), new BelvedereResult(imagePath, uriForFile));
        this.log.d(LOG_TAG, String.format(Locale.US, "Camera Intent: Request Id: %s - File: %s - Uri: %s", new Object[]{Integer.valueOf(requestId), imagePath, uriForFile}));
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", uriForFile);
        this.belvedereStorage.grantPermissionsForUri(context, intent, uriForFile, 3);
        return new BelvedereIntent(intent, requestId, BelvedereSource.Camera);
    }

    @TargetApi(19)
    @NonNull
    private Intent getGalleryIntent() {
        Intent intent;
        if (VERSION.SDK_INT >= 19) {
            this.log.d(LOG_TAG, "Gallery Intent, using 'ACTION_OPEN_DOCUMENT'");
            intent = new Intent("android.intent.action.OPEN_DOCUMENT");
        } else {
            this.log.d(LOG_TAG, "Gallery Intent, using 'ACTION_GET_CONTENT'");
            intent = new Intent("android.intent.action.GET_CONTENT");
        }
        intent.setType(this.belvedereConfig.getContentType());
        intent.addCategory("android.intent.category.OPENABLE");
        if (VERSION.SDK_INT >= 18) {
            intent.putExtra("android.intent.extra.ALLOW_MULTIPLE", this.belvedereConfig.allowMultiple());
        }
        return intent;
    }

    private boolean hasPermissionInManifest(@NonNull Context context, @NonNull String permissionName) {
        try {
            String[] declaredPermissions = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions;
            if (declaredPermissions == null || declaredPermissions.length <= 0) {
                return false;
            }
            for (String p : declaredPermissions) {
                if (p.equals(permissionName)) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            this.log.e(LOG_TAG, "Not able to find permissions in manifest", e);
            return false;
        }
    }
}
