package com.google.android.gms.wearable.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzh implements Creator<AncsNotificationParcelable> {
    static void zza(AncsNotificationParcelable ancsNotificationParcelable, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zzc(parcel, 1, ancsNotificationParcelable.mVersionCode);
        zzb.zzc(parcel, 2, ancsNotificationParcelable.getId());
        zzb.zza(parcel, 3, ancsNotificationParcelable.zzup(), false);
        zzb.zza(parcel, 4, ancsNotificationParcelable.zzcml(), false);
        zzb.zza(parcel, 5, ancsNotificationParcelable.zzcmm(), false);
        zzb.zza(parcel, 6, ancsNotificationParcelable.getTitle(), false);
        zzb.zza(parcel, 7, ancsNotificationParcelable.zzbiv(), false);
        zzb.zza(parcel, 8, ancsNotificationParcelable.getDisplayName(), false);
        zzb.zza(parcel, 9, ancsNotificationParcelable.zzcmn());
        zzb.zza(parcel, 10, ancsNotificationParcelable.zzcmo());
        zzb.zza(parcel, 11, ancsNotificationParcelable.zzcmp());
        zzb.zza(parcel, 12, ancsNotificationParcelable.zzcmq());
        zzb.zza(parcel, 13, ancsNotificationParcelable.getPackageName(), false);
        zzb.zzaj(parcel, zzcs);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzuv(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzadp(i);
    }

    public AncsNotificationParcelable[] zzadp(int i) {
        return new AncsNotificationParcelable[i];
    }

    public AncsNotificationParcelable zzuv(Parcel parcel) {
        int zzcr = zza.zzcr(parcel);
        int i = 0;
        int i2 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        byte b = (byte) 0;
        byte b2 = (byte) 0;
        byte b3 = (byte) 0;
        byte b4 = (byte) 0;
        String str7 = null;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    i = zza.zzg(parcel, zzcq);
                    break;
                case 2:
                    i2 = zza.zzg(parcel, zzcq);
                    break;
                case 3:
                    str = zza.zzq(parcel, zzcq);
                    break;
                case 4:
                    str2 = zza.zzq(parcel, zzcq);
                    break;
                case 5:
                    str3 = zza.zzq(parcel, zzcq);
                    break;
                case 6:
                    str4 = zza.zzq(parcel, zzcq);
                    break;
                case 7:
                    str5 = zza.zzq(parcel, zzcq);
                    break;
                case 8:
                    str6 = zza.zzq(parcel, zzcq);
                    break;
                case 9:
                    b = zza.zze(parcel, zzcq);
                    break;
                case 10:
                    b2 = zza.zze(parcel, zzcq);
                    break;
                case 11:
                    b3 = zza.zze(parcel, zzcq);
                    break;
                case 12:
                    b4 = zza.zze(parcel, zzcq);
                    break;
                case 13:
                    str7 = zza.zzq(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new AncsNotificationParcelable(i, i2, str, str2, str3, str4, str5, str6, b, b2, b3, b4, str7);
        }
        throw new zza.zza("Overread allowed size end=" + zzcr, parcel);
    }
}
