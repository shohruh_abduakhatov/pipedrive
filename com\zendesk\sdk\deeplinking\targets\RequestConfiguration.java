package com.zendesk.sdk.deeplinking.targets;

import android.content.Intent;
import java.util.ArrayList;

public class RequestConfiguration extends TargetConfiguration {
    private final String mRequestId;
    private final String mSubject;

    public /* bridge */ /* synthetic */ ArrayList getBackStackActivities() {
        return super.getBackStackActivities();
    }

    public /* bridge */ /* synthetic */ DeepLinkType getDeepLinkType() {
        return super.getDeepLinkType();
    }

    public /* bridge */ /* synthetic */ Intent getFallbackActivity() {
        return super.getFallbackActivity();
    }

    public RequestConfiguration(String requestId, String subject, ArrayList<Intent> backStackActivities, Intent fallBackActivity) {
        super(DeepLinkType.Request, backStackActivities, fallBackActivity);
        this.mRequestId = requestId;
        this.mSubject = subject;
    }

    public String getRequestId() {
        return this.mRequestId;
    }

    public String getSubject() {
        return this.mSubject;
    }
}
