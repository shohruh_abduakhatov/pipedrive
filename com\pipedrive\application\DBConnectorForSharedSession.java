package com.pipedrive.application;

import android.content.Context;
import android.support.annotation.NonNull;
import com.pipedrive.datasource.PipeSQLiteOpenHelper;
import com.pipedrive.datasource.PipeSQLiteSharedHelper;
import com.pipedrive.logging.Log;

public class DBConnectorForSharedSession extends DBConnector {
    private static final String TAG = DBConnectorForSharedSession.class.getSimpleName();
    @NonNull
    private final Context mApplicationContext;

    DBConnectorForSharedSession(@NonNull Context context) {
        this.mApplicationContext = context.getApplicationContext();
    }

    @NonNull
    protected PipeSQLiteOpenHelper getSQLiteOpenHelper() {
        Log.i(TAG + ".getSQLiteOpenHelper()", "Initializing PipeSQLiteSharedHelper...");
        return new PipeSQLiteSharedHelper(this.mApplicationContext);
    }
}
