package com.pipedrive;

import android.support.v7.app.ActionBar;
import android.view.MenuItem;

@Deprecated
public class BaseActivityWithUpEnabled extends BaseActivity {
    private boolean isSetHomeUpIndicatorAsCancelSet = false;
    private boolean isSetHomeUpIndicatorAsSaveSet = false;

    public void onResume() {
        super.onResume();
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                boolean isHomeProcessed = onOptionsUpItemSelected();
                if (getSupportActionBar() != null) {
                    if (this.isSetHomeUpIndicatorAsCancelSet) {
                        isHomeProcessed |= onOptionsUpItemCancelSelected();
                    }
                    if (this.isSetHomeUpIndicatorAsSaveSet) {
                        isHomeProcessed |= onOptionsUpItemSaveSelected();
                    }
                }
                if (isHomeProcessed) {
                    return isHomeProcessed;
                }
                finish();
                return isHomeProcessed;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Deprecated
    protected boolean onOptionsUpItemSelected() {
        return false;
    }

    @Deprecated
    protected void setHomeUpIndicatorAsCancel() {
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.icon_close));
            supportActionBar.setHomeActionContentDescription((int) R.string.cancel);
            this.isSetHomeUpIndicatorAsCancelSet = true;
            this.isSetHomeUpIndicatorAsSaveSet = false;
        }
    }

    @Deprecated
    protected boolean onOptionsUpItemCancelSelected() {
        return false;
    }

    @Deprecated
    protected void setHomeUpIndicatorAsSave() {
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setHomeAsUpIndicator((int) R.drawable.icon_back);
            supportActionBar.setHomeActionContentDescription((int) R.string.menu_save);
            this.isSetHomeUpIndicatorAsCancelSet = false;
            this.isSetHomeUpIndicatorAsSaveSet = true;
        }
    }

    @Deprecated
    protected boolean onOptionsUpItemSaveSelected() {
        return false;
    }
}
