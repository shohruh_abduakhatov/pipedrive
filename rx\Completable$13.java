package rx;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;
import rx.exceptions.CompositeException;
import rx.exceptions.Exceptions;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func0;
import rx.functions.Func1;
import rx.plugins.RxJavaHooks;
import rx.subscriptions.Subscriptions;

class Completable$13 implements Completable$OnSubscribe {
    final /* synthetic */ Func1 val$completableFunc1;
    final /* synthetic */ Action1 val$disposer;
    final /* synthetic */ boolean val$eager;
    final /* synthetic */ Func0 val$resourceFunc0;

    Completable$13(Func0 func0, Func1 func1, Action1 action1, boolean z) {
        this.val$resourceFunc0 = func0;
        this.val$completableFunc1 = func1;
        this.val$disposer = action1;
        this.val$eager = z;
    }

    public void call(final CompletableSubscriber s) {
        try {
            final R resource = this.val$resourceFunc0.call();
            try {
                Completable cs = (Completable) this.val$completableFunc1.call(resource);
                if (cs == null) {
                    try {
                        this.val$disposer.call(resource);
                        s.onSubscribe(Subscriptions.unsubscribed());
                        s.onError(new NullPointerException("The completable supplied is null"));
                        return;
                    } catch (Throwable ex) {
                        Exceptions.throwIfFatal(ex);
                        s.onSubscribe(Subscriptions.unsubscribed());
                        s.onError(new CompositeException(Arrays.asList(new Throwable[]{new NullPointerException("The completable supplied is null"), ex})));
                        return;
                    }
                }
                final AtomicBoolean once = new AtomicBoolean();
                cs.unsafeSubscribe(new CompletableSubscriber() {
                    Subscription d;

                    void dispose() {
                        this.d.unsubscribe();
                        if (once.compareAndSet(false, true)) {
                            try {
                                Completable$13.this.val$disposer.call(resource);
                            } catch (Throwable ex) {
                                RxJavaHooks.onError(ex);
                            }
                        }
                    }

                    public void onCompleted() {
                        if (Completable$13.this.val$eager && once.compareAndSet(false, true)) {
                            try {
                                Completable$13.this.val$disposer.call(resource);
                            } catch (Throwable ex) {
                                s.onError(ex);
                                return;
                            }
                        }
                        s.onCompleted();
                        if (!Completable$13.this.val$eager) {
                            dispose();
                        }
                    }

                    public void onError(Throwable e) {
                        if (Completable$13.this.val$eager && once.compareAndSet(false, true)) {
                            try {
                                Completable$13.this.val$disposer.call(resource);
                            } catch (Throwable ex) {
                                e = new CompositeException(Arrays.asList(new Throwable[]{e, ex}));
                            }
                        }
                        s.onError(e);
                        if (!Completable$13.this.val$eager) {
                            dispose();
                        }
                    }

                    public void onSubscribe(Subscription d) {
                        this.d = d;
                        s.onSubscribe(Subscriptions.create(new Action0() {
                            public void call() {
                                AnonymousClass1.this.dispose();
                            }
                        }));
                    }
                });
            } catch (Throwable ex2) {
                Exceptions.throwIfFatal(e);
                Exceptions.throwIfFatal(ex2);
                s.onSubscribe(Subscriptions.unsubscribed());
                s.onError(new CompositeException(Arrays.asList(new Throwable[]{e, ex2})));
            }
        } catch (Throwable e) {
            s.onSubscribe(Subscriptions.unsubscribed());
            s.onError(e);
        }
    }
}
