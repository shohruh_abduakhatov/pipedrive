package com.zendesk.sdk.network.impl;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.ConnectivityManager.NetworkCallback;
import android.net.Network;
import android.net.NetworkRequest.Builder;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.network.NetworkAware;
import com.zendesk.sdk.network.NetworkInfoProvider;
import com.zendesk.sdk.network.RetryAction;
import com.zendesk.sdk.util.NetworkUtils;
import com.zendesk.util.CollectionUtils;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

class ZendeskNetworkInfoProvider implements NetworkInfoProvider, NetworkAware {
    private static final String LOG_TAG = "ZendeskNetworkInfoProvider";
    private BroadcastReceiver broadcastReceiver;
    private Context context;
    private Map<Integer, WeakReference<NetworkAware>> listeners = new HashMap();
    private boolean networkAvailable;
    private NetworkCallback networkCallback;
    private Map<Integer, WeakReference<RetryAction>> retryActions = new HashMap();

    class NetworkAvailabilityBroadcastReceiver extends BroadcastReceiver {
        private final String LOG_TAG = NetworkAvailabilityBroadcastReceiver.class.getSimpleName();

        NetworkAvailabilityBroadcastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent == null || !"android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
                Logger.w(this.LOG_TAG, "onReceive: intent was null or getAction() was mismatched", new Object[0]);
            } else if (intent.getBooleanExtra("noConnectivity", false)) {
                ZendeskNetworkInfoProvider.this.onNetworkUnavailable();
            } else {
                ZendeskNetworkInfoProvider.this.onNetworkAvailable();
            }
        }
    }

    ZendeskNetworkInfoProvider(@Nullable Context context) {
        this.context = context;
        this.networkAvailable = NetworkUtils.isConnected(context);
    }

    public void register() {
        if (this.context == null) {
            Logger.w(LOG_TAG, "Cannot register NetworkInformer, supplied Context is null.", new Object[0]);
        } else {
            registerForNetworkCallbacks();
        }
    }

    public void unregister() {
        if (this.context == null) {
            Logger.w(LOG_TAG, "Cannot unregister NetworkInformer, supplied Context is null.", new Object[0]);
        } else {
            unregisterForNetworkCallbacks();
        }
    }

    public boolean isNetworkAvailable() {
        return this.networkAvailable;
    }

    public void addNetworkAwareListener(@NonNull Integer id, @NonNull NetworkAware listener) {
        if (id != null && listener != null) {
            this.listeners.put(id, new WeakReference(listener));
        }
    }

    public void removeNetworkAwareListener(@NonNull Integer id) {
        this.listeners.remove(id);
    }

    public void clearNetworkAwareListeners() {
        this.listeners.clear();
    }

    public void addRetryAction(@NonNull Integer id, @NonNull RetryAction retryAction) {
        if (id != null && retryAction != null) {
            this.retryActions.put(id, new WeakReference(retryAction));
        }
    }

    public void removeRetryAction(@NonNull Integer id) {
        this.retryActions.remove(id);
    }

    public void clearRetryActions() {
        this.retryActions.clear();
    }

    public void onNetworkAvailable() {
        Map<Integer, WeakReference<NetworkAware>> listenerReferences = CollectionUtils.copyOf(this.listeners);
        Map<Integer, WeakReference<RetryAction>> retryActionReferences = CollectionUtils.copyOf(this.retryActions);
        this.networkAvailable = true;
        for (WeakReference<NetworkAware> listenerReference : listenerReferences.values()) {
            if (listenerReference.get() != null) {
                ((NetworkAware) listenerReference.get()).onNetworkAvailable();
            }
        }
        for (WeakReference<RetryAction> retryActionReference : retryActionReferences.values()) {
            if (retryActionReference.get() != null) {
                ((RetryAction) retryActionReference.get()).onRetry();
            }
        }
        this.retryActions.clear();
    }

    public void onNetworkUnavailable() {
        Map<Integer, WeakReference<NetworkAware>> listenerReferences = CollectionUtils.copyOf(this.listeners);
        this.networkAvailable = false;
        for (WeakReference<NetworkAware> listenerReference : listenerReferences.values()) {
            if (listenerReference.get() != null) {
                ((NetworkAware) listenerReference.get()).onNetworkUnavailable();
            }
        }
    }

    @TargetApi(21)
    private void registerForNetworkCallbacks() {
        if (VERSION.SDK_INT < 21) {
            Logger.d(LOG_TAG, "Adding pre-Lollipop network callbacks...", new Object[0]);
            this.broadcastReceiver = new NetworkAvailabilityBroadcastReceiver();
            this.context.registerReceiver(this.broadcastReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            return;
        }
        Logger.d(LOG_TAG, "Adding Lollipop network callbacks...", new Object[0]);
        ConnectivityManager connectivityManager = (ConnectivityManager) this.context.getSystemService("connectivity");
        final Handler handler = new Handler(Looper.getMainLooper());
        this.networkCallback = new NetworkCallback() {
            public void onAvailable(Network network) {
                handler.post(new Runnable() {
                    public void run() {
                        ZendeskNetworkInfoProvider.this.onNetworkAvailable();
                    }
                });
            }

            public void onLost(Network network) {
                handler.post(new Runnable() {
                    public void run() {
                        ZendeskNetworkInfoProvider.this.onNetworkUnavailable();
                    }
                });
            }
        };
        connectivityManager.registerNetworkCallback(new Builder().build(), this.networkCallback);
    }

    @TargetApi(21)
    private void unregisterForNetworkCallbacks() {
        if (this.broadcastReceiver != null) {
            this.context.unregisterReceiver(this.broadcastReceiver);
        }
        if (VERSION.SDK_INT >= 21 && this.networkCallback != null) {
            ((ConnectivityManager) this.context.getSystemService("connectivity")).unregisterNetworkCallback(this.networkCallback);
        }
    }
}
