package com.zendesk.sdk.feedback;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.sdk.model.DeviceInfo;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.model.access.Identity;
import com.zendesk.sdk.model.request.CreateRequest;
import com.zendesk.sdk.model.request.CustomField;
import com.zendesk.sdk.network.RequestProvider;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.storage.IdentityStorage;
import com.zendesk.service.ErrorResponse;
import com.zendesk.service.ErrorResponseAdapter;
import com.zendesk.service.ZendeskCallback;
import com.zendesk.util.CollectionUtils;
import com.zendesk.util.StringUtils;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ZendeskFeedbackConnector implements FeedbackConnector {
    private static final String FALLBACK_REQUEST_SUBJECT = "App Ticket";
    private static final String LOG_TAG = "ZendeskFeedbackConnector";
    private final String additionalInfo;
    private final List<String> additionalTags;
    private final List<String> configurationTags;
    private final List<CustomField> customFields;
    private final IdentityStorage identityStorage;
    private final Map<String, String> metadata;
    private final RequestProvider requestProvider;
    private final String requestSubject;
    private final Long ticketFormId;

    public static ZendeskFeedbackConnector defaultConnector(Context context, ZendeskFeedbackConfiguration configuration, List<String> additionalTags) {
        return new ZendeskFeedbackConnector(context, configuration, additionalTags, ZendeskConfig.INSTANCE.getCustomFields(), ZendeskConfig.INSTANCE.getTicketFormId(), ZendeskConfig.INSTANCE.provider().requestProvider(), ZendeskConfig.INSTANCE.storage().identityStorage());
    }

    public ZendeskFeedbackConnector(@Nullable Context context, @Nullable ZendeskFeedbackConfiguration configuration, @Nullable List<String> additionalTags, @Nullable List<CustomField> customFields, @Nullable Long ticketFormId, @NonNull RequestProvider requestProvider, @NonNull IdentityStorage identityStorage) {
        String str;
        List list;
        Map map = null;
        if (configuration == null) {
            str = null;
        } else {
            str = configuration.getAdditionalInfo();
        }
        this.additionalInfo = str;
        if (configuration == null) {
            if (context == null) {
                str = FALLBACK_REQUEST_SUBJECT;
            } else {
                str = context.getString(R.string.contact_fragment_request_subject);
            }
        } else if (configuration.getRequestSubject() == null) {
            str = FALLBACK_REQUEST_SUBJECT;
        } else {
            str = configuration.getRequestSubject();
        }
        this.requestSubject = str;
        if (configuration == null) {
            list = null;
        } else {
            list = configuration.getTags();
        }
        this.configurationTags = list;
        if (context != null) {
            map = new DeviceInfo(context).getDeviceInfoAsMap();
        }
        this.metadata = map;
        this.additionalTags = additionalTags;
        this.customFields = customFields;
        this.ticketFormId = ticketFormId;
        this.requestProvider = requestProvider;
        this.identityStorage = identityStorage;
    }

    public boolean isValid() {
        boolean providerAvailable;
        boolean hasIdentity;
        boolean isValidConnector;
        if (this.requestProvider != null) {
            providerAvailable = true;
        } else {
            providerAvailable = false;
        }
        if (this.identityStorage == null || this.identityStorage.getIdentity() == null) {
            hasIdentity = false;
        } else {
            hasIdentity = true;
        }
        if (providerAvailable && hasIdentity) {
            isValidConnector = true;
        } else {
            isValidConnector = false;
        }
        if (!isValidConnector) {
            Logger.e(LOG_TAG, "Connector is invalid, unable to send feedback. " + String.format(Locale.US, "Provider available: %s, identity is valid: %s", new Object[]{Boolean.valueOf(providerAvailable), Boolean.valueOf(hasIdentity)}), new Object[0]);
        }
        return isValidConnector;
    }

    public void sendFeedback(String feedback, @Nullable List<String> attachments, final ZendeskCallback<CreateRequest> callback) {
        if (isValid()) {
            Logger.d(LOG_TAG, "Configuration is valid, attempting to send feedback...", new Object[0]);
            final CreateRequest request = new CreateRequest();
            setCustomFieldsAndMetadata(request, this.customFields, this.ticketFormId, this.metadata);
            setEmailFromIdentity(request, this.identityStorage.getIdentity());
            setSubjectAndDescription(request, this.requestSubject, feedback, this.additionalInfo);
            setTags(request, this.configurationTags, this.additionalTags);
            if (attachments != null) {
                request.setAttachments(attachments);
            }
            this.requestProvider.createRequest(request, new ZendeskCallback<CreateRequest>() {
                public void onSuccess(CreateRequest result) {
                    Logger.d(ZendeskFeedbackConnector.LOG_TAG, "Feedback submitted successfully.", new Object[0]);
                    if (callback != null) {
                        callback.onSuccess(request);
                    }
                }

                public void onError(ErrorResponse error) {
                    Logger.e(ZendeskFeedbackConnector.LOG_TAG, error);
                    if (callback != null) {
                        callback.onError(error);
                    }
                }
            });
        } else if (callback != null) {
            callback.onError(new ErrorResponseAdapter("Configuration is invalid, unable to send feedback."));
        }
    }

    @VisibleForTesting
    void setCustomFieldsAndMetadata(@NonNull CreateRequest request, @Nullable List<CustomField> customFields, @Nullable Long ticketFormId, @Nullable Map<String, String> metadata) {
        request.setTicketFormId(ticketFormId);
        request.setCustomFields(customFields);
        request.setMetadata(metadata);
    }

    @VisibleForTesting
    void setTags(@NonNull CreateRequest request, @Nullable List<String> configurationTags, @Nullable List<String> additionalTags) {
        List<String> combinedTags = CollectionUtils.combineLists(new List[]{configurationTags, additionalTags});
        if (CollectionUtils.isNotEmpty(combinedTags)) {
            Logger.d(LOG_TAG, "Adding tags to feedback...", new Object[0]);
            request.setTags(combinedTags);
        }
    }

    @VisibleForTesting
    void setEmailFromIdentity(@NonNull CreateRequest request, @Nullable Identity identity) {
        if (identity instanceof AnonymousIdentity) {
            AnonymousIdentity anonymousIdentity = (AnonymousIdentity) identity;
            if (StringUtils.hasLength(anonymousIdentity.getEmail())) {
                request.setEmail(anonymousIdentity.getEmail());
            }
        }
    }

    @VisibleForTesting
    void setSubjectAndDescription(@NonNull CreateRequest request, @NonNull String subject, @Nullable String feedback, @Nullable String additionalInfo) {
        request.setSubject(subject);
        StringBuilder descriptionBuilder = new StringBuilder(100);
        descriptionBuilder.append(feedback);
        if (StringUtils.hasLength(additionalInfo)) {
            Logger.d(LOG_TAG, "Additional info is present. Attaching to description.", new Object[0]);
            descriptionBuilder.append("\n\n").append("------------------------------").append("\n").append("\n").append(additionalInfo);
        }
        request.setDescription(descriptionBuilder.toString());
    }
}
