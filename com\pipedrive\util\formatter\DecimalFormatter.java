package com.pipedrive.util.formatter;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.model.customfields.CustomField;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class DecimalFormatter extends BaseFormatter {
    public DecimalFormatter(@NonNull Session session) {
        super(session);
    }

    @NonNull
    NumberFormat instantiateNumberFormat(@NonNull Locale locale) {
        return DecimalFormat.getInstance(locale);
    }

    @NonNull
    public String formatDecimalCustomField(CustomField customField) {
        double doubleValue = 0.0d;
        try {
            doubleValue = Double.valueOf(customField.getTempValue()).doubleValue();
        } catch (NumberFormatException e) {
        }
        return format(Double.valueOf(doubleValue));
    }

    @NonNull
    public String format(@NonNull Double value) {
        return super.format(value);
    }

    @NonNull
    public NumberFormat getNumberFormatForDefaultCurrency() {
        return getNumberFormatForCurrency(null);
    }
}
