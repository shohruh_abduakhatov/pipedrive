package com.zendesk.sdk.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.newrelic.agent.android.instrumentation.GsonInstrumentation;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.settings.MobileSettings;
import com.zendesk.sdk.model.settings.SafeMobileSettings;
import java.util.concurrent.TimeUnit;

class ZendeskSdkSettingsStorage implements SdkSettingsStorage {
    private static final String LOG_TAG = SdkSettingsStorage.class.getSimpleName();
    private static final String MOBILE_SETTINGS_KEY = "mobileconfiguration";
    private static final String MOBILE_SETTINGS_TIMESTAMP_KEY = "mobileconfigurationtimestamp";
    static final String PREFERENCES_FILE = "ZendeskConfigStorage";
    private final Gson gson;
    private final SharedPreferences sharedPreferences;

    ZendeskSdkSettingsStorage(Context context, Gson gson) {
        this.sharedPreferences = context.getSharedPreferences(PREFERENCES_FILE, 0);
        this.gson = gson;
    }

    public boolean hasStoredSdkSettings() {
        return this.sharedPreferences.contains(MOBILE_SETTINGS_KEY);
    }

    public boolean areSettingsUpToDate(long duration, TimeUnit timeUnit) {
        Long lastDownloaded = Long.valueOf(this.sharedPreferences.getLong(MOBILE_SETTINGS_TIMESTAMP_KEY, -1));
        if (lastDownloaded.longValue() != -1 && System.currentTimeMillis() - lastDownloaded.longValue() < TimeUnit.MILLISECONDS.convert(duration, timeUnit)) {
            return true;
        }
        return false;
    }

    @Nullable
    public SafeMobileSettings getStoredSettings() {
        String settingsJson = this.sharedPreferences.getString(MOBILE_SETTINGS_KEY, null);
        MobileSettings settings = null;
        if (settingsJson != null) {
            Logger.d(LOG_TAG, "Found stored settings", new Object[0]);
            try {
                Gson gson = this.gson;
                Class cls = MobileSettings.class;
                settings = (MobileSettings) (!(gson instanceof Gson) ? gson.fromJson(settingsJson, cls) : GsonInstrumentation.fromJson(gson, settingsJson, cls));
                Logger.d(LOG_TAG, "Successfully deserialised settings from storage", new Object[0]);
            } catch (JsonSyntaxException e) {
                Logger.e(LOG_TAG, "Error deserialising MobileSettings from storage, going to remove stored settings", new Object[0]);
                deleteStoredSettings();
            }
        } else {
            Logger.w(LOG_TAG, "No stored configuration was found, returning null settings", new Object[0]);
        }
        return new SafeMobileSettings(settings);
    }

    public void setStoredSettings(SafeMobileSettings mobileSettings) {
        if (mobileSettings == null) {
            Logger.e(LOG_TAG, "Not going to save null MobileSettings, use deleteStoredSettings()", new Object[0]);
            return;
        }
        Gson gson = this.gson;
        MobileSettings mobileSettings2 = mobileSettings.getMobileSettings();
        this.sharedPreferences.edit().putString(MOBILE_SETTINGS_KEY, !(gson instanceof Gson) ? gson.toJson(mobileSettings2) : GsonInstrumentation.toJson(gson, mobileSettings2)).putLong(MOBILE_SETTINGS_TIMESTAMP_KEY, System.currentTimeMillis()).apply();
        Logger.d(LOG_TAG, "Successfully saved settings to storage", new Object[0]);
    }

    public void deleteStoredSettings() {
        Logger.d(LOG_TAG, "Deleting stored settings", new Object[0]);
        this.sharedPreferences.edit().clear().apply();
    }
}
