package com.newrelic.agent.android.util;

import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Environment;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.newrelic.agent.android.analytics.AnalyticAttribute;
import com.newrelic.agent.android.analytics.AnalyticsControllerImpl;
import com.newrelic.agent.android.logging.AgentLog;
import com.newrelic.agent.android.logging.AgentLogManager;
import com.newrelic.agent.android.metric.MetricNames;
import com.newrelic.agent.android.stats.StatsEngine;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

public class PersistentUUID {
    public static final String METRIC_UUID_RECOVERED = "UUIDRecovered";
    private static File UUID_FILE = new File(Environment.getDataDirectory(), UUID_FILENAME);
    private static final String UUID_FILENAME = "nr_installation";
    private static final String UUID_KEY = "nr_uuid";
    private static AgentLog log = AgentLogManager.getAgentLog();

    public PersistentUUID(Context context) {
        UUID_FILE = new File(context.getFilesDir(), UUID_FILENAME);
    }

    public String getDeviceId(Context context) {
        String id = generateUniqueID(context);
        if (TextUtils.isEmpty(id)) {
            return UUID.randomUUID().toString();
        }
        return id;
    }

    private String generateUniqueID(Context context) {
        String hardwareDeviceId = Build.SERIAL;
        String androidDeviceId = Build.ID;
        try {
            androidDeviceId = Secure.getString(context.getContentResolver(), "android_id");
            if (TextUtils.isEmpty(androidDeviceId)) {
                return UUID.randomUUID().toString();
            }
            try {
                TelephonyManager tm = (TelephonyManager) context.getSystemService("phone");
                if (tm != null) {
                    hardwareDeviceId = tm.getDeviceId();
                }
            } catch (Exception e) {
                hardwareDeviceId = "badf00d";
            }
            if (TextUtils.isEmpty(hardwareDeviceId)) {
                hardwareDeviceId = Build.HARDWARE + Build.DEVICE + Build.BOARD + Build.BRAND;
            }
            String str = intToHexString(androidDeviceId.hashCode(), 8) + "-" + intToHexString(hardwareDeviceId.hashCode(), 4) + "-" + intToHexString(VERSION.SDK_INT, 4) + "-" + intToHexString(VERSION.RELEASE.hashCode(), 12);
            throw new RuntimeException("Not supported (TODO)");
        } catch (Exception e2) {
            return UUID.randomUUID().toString();
        }
    }

    private String intToHexString(int value, int sublen) {
        String result = "";
        String string = Integer.toHexString(value);
        char[] chars = new char[(sublen - string.length())];
        Arrays.fill(chars, '0');
        string = new String(chars) + string;
        int count = 0;
        for (int i = string.length() - 1; i >= 0; i--) {
            count++;
            result = string.substring(i, i + 1) + result;
            if (count % sublen == 0) {
                result = "-" + result;
            }
        }
        if (result.startsWith("-")) {
            return result.substring(1, result.length());
        }
        return result;
    }

    protected void noticeUUIDMetric(String tag) {
        StatsEngine statsEngine = StatsEngine.get();
        if (statsEngine != null) {
            statsEngine.inc("Supportability/AgentHealth/" + tag);
        } else {
            log.error("StatsEngine is null. " + tag + "  not recorded.");
        }
    }

    public String getPersistentUUID() {
        String uuid = getUUIDFromFileStore();
        if (TextUtils.isEmpty(uuid)) {
            uuid = UUID.randomUUID().toString();
            log.info("Created random UUID: " + uuid);
            StatsEngine.get().inc(MetricNames.MOBILE_APP_INSTALL);
            AnalyticsControllerImpl.getInstance().addAttributeUnchecked(new AnalyticAttribute(AnalyticAttribute.APP_INSTALL_ATTRIBUTE, true), false);
            setPersistedUUID(uuid);
            return uuid;
        }
        noticeUUIDMetric(METRIC_UUID_RECOVERED);
        return uuid;
    }

    protected void setPersistedUUID(String uuid) {
        putUUIDToFileStore(uuid);
    }

    protected String getUUIDFromFileStore() {
        IOException e;
        FileNotFoundException e2;
        Throwable th;
        JSONException e3;
        NullPointerException e4;
        String uuid = "";
        if (UUID_FILE.exists()) {
            BufferedReader in = null;
            try {
                BufferedReader in2 = new BufferedReader(new FileReader(UUID_FILE));
                try {
                    uuid = new JSONObject(in2.readLine()).getString(UUID_KEY);
                    if (in2 != null) {
                        try {
                            in2.close();
                        } catch (IOException e5) {
                            log.error(e5.getMessage());
                        }
                    }
                } catch (FileNotFoundException e6) {
                    e2 = e6;
                    in = in2;
                    try {
                        log.error(e2.getMessage());
                        if (in != null) {
                            try {
                                in.close();
                            } catch (IOException e52) {
                                log.error(e52.getMessage());
                            }
                        }
                        return uuid;
                    } catch (Throwable th2) {
                        th = th2;
                        if (in != null) {
                            try {
                                in.close();
                            } catch (IOException e522) {
                                log.error(e522.getMessage());
                            }
                        }
                        throw th;
                    }
                } catch (IOException e7) {
                    e522 = e7;
                    in = in2;
                    log.error(e522.getMessage());
                    if (in != null) {
                        try {
                            in.close();
                        } catch (IOException e5222) {
                            log.error(e5222.getMessage());
                        }
                    }
                    return uuid;
                } catch (JSONException e8) {
                    e3 = e8;
                    in = in2;
                    log.error(e3.getMessage());
                    if (in != null) {
                        try {
                            in.close();
                        } catch (IOException e52222) {
                            log.error(e52222.getMessage());
                        }
                    }
                    return uuid;
                } catch (NullPointerException e9) {
                    e4 = e9;
                    in = in2;
                    log.error(e4.getMessage());
                    if (in != null) {
                        try {
                            in.close();
                        } catch (IOException e522222) {
                            log.error(e522222.getMessage());
                        }
                    }
                    return uuid;
                } catch (Throwable th3) {
                    th = th3;
                    in = in2;
                    if (in != null) {
                        in.close();
                    }
                    throw th;
                }
            } catch (FileNotFoundException e10) {
                e2 = e10;
                log.error(e2.getMessage());
                if (in != null) {
                    in.close();
                }
                return uuid;
            } catch (IOException e11) {
                e522222 = e11;
                log.error(e522222.getMessage());
                if (in != null) {
                    in.close();
                }
                return uuid;
            } catch (JSONException e12) {
                e3 = e12;
                log.error(e3.getMessage());
                if (in != null) {
                    in.close();
                }
                return uuid;
            } catch (NullPointerException e13) {
                e4 = e13;
                log.error(e4.getMessage());
                if (in != null) {
                    in.close();
                }
                return uuid;
            }
        }
        return uuid;
    }

    protected void putUUIDToFileStore(String uuid) {
        IOException e;
        Throwable th;
        JSONException e2;
        BufferedWriter out = null;
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(UUID_KEY, uuid);
            BufferedWriter out2 = new BufferedWriter(new FileWriter(UUID_FILE));
            try {
                out2.write(jsonObject.toString());
                out2.flush();
                if (out2 != null) {
                    try {
                        out2.close();
                        out = out2;
                        return;
                    } catch (IOException e3) {
                        log.error(e3.getMessage());
                        out = out2;
                        return;
                    }
                }
            } catch (IOException e4) {
                e3 = e4;
                out = out2;
                try {
                    log.error(e3.getMessage());
                    if (out != null) {
                        try {
                            out.close();
                        } catch (IOException e32) {
                            log.error(e32.getMessage());
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (out != null) {
                        try {
                            out.close();
                        } catch (IOException e322) {
                            log.error(e322.getMessage());
                        }
                    }
                    throw th;
                }
            } catch (JSONException e5) {
                e2 = e5;
                out = out2;
                log.error(e2.getMessage());
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException e3222) {
                        log.error(e3222.getMessage());
                    }
                }
            } catch (Throwable th3) {
                th = th3;
                out = out2;
                if (out != null) {
                    out.close();
                }
                throw th;
            }
        } catch (IOException e6) {
            e3222 = e6;
            log.error(e3222.getMessage());
            if (out != null) {
                out.close();
            }
        } catch (JSONException e7) {
            e2 = e7;
            log.error(e2.getMessage());
            if (out != null) {
                out.close();
            }
        }
    }
}
