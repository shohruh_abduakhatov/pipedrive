package com.pipedrive.nearby.map;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraIdleListener;
import com.google.android.gms.maps.GoogleMap.OnCameraMoveListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.LatLngBounds.Builder;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.SphericalUtil;
import com.pipedrive.R;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.AnalyticsEvent;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.nearby.NearbyDataSource;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.nearby.cards.CardSelectionProvider;
import com.pipedrive.nearby.cards.NearbyRequestEventBus;
import com.pipedrive.nearby.cards.cards.CardEventBus;
import com.pipedrive.nearby.model.NearbyItem;
import com.pipedrive.nearby.model.NearbyItemType;
import com.pipedrive.nearby.searchbutton.SearchButtonClickProvider;
import com.pipedrive.nearby.toolbar.NearbyItemTypeChangesProvider;
import com.pipedrive.nearby.util.Irrelevant;
import com.pipedrive.util.RxUtil;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import rx.Completable;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Actions;
import rx.functions.Func1;
import rx.functions.Func3;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;
import rx.subjects.Subject;
import rx.subscriptions.CompositeSubscription;

public class PDMapFragment extends SupportMapFragment implements PDMap {
    private static final int HEADING_EAST = 90;
    private static final int HEADING_NORTH = 0;
    private static final int HEADING_SOUTH = 180;
    private static final int HEADING_WEST = 270;
    private static final String KEY_IS_INITIAL_SETUP = (PDMapFragment.class.getSimpleName() + ".is_initial_setup");
    private static final String KEY_REQUEST_IN_PROGRESS = (PDMapFragment.class.getSimpleName() + ".request_in_progress");
    private static final String KEY_VIEWPORT = (PDMapFragment.class.getSimpleName() + ".viewport");
    private static final int MIN_REQUEST_RUN_DURATION_MILLIS = 350;
    public static final String TAG = PDMapFragment.class.getSimpleName();
    @NonNull
    final CompositeSubscription allSubscriptions = new CompositeSubscription();
    @Nullable
    BitmapHelper bitmapHelper;
    @Nullable
    private CardSelectionProvider cardSelectionProvider;
    @NonNull
    Boolean isCameraAnimating = Boolean.valueOf(false);
    @NonNull
    Boolean isInitialSetup = Boolean.valueOf(true);
    @Nullable
    LocationProvider locationProvider;
    @NonNull
    final Subject<GoogleMap, GoogleMap> mapReady = BehaviorSubject.create().toSerialized();
    @Nullable
    MarkerManager markerManager;
    @NonNull
    final Subject<NearbyItem, NearbyItem> markerSelection = BehaviorSubject.create().toSerialized();
    @Nullable
    NearbyItemTypeChangesProvider nearbyItemTypeChangesProvider;
    @NonNull
    final Subject<List<NearbyItem>, List<NearbyItem>> nearbyItems = BehaviorSubject.create().toSerialized();
    @NonNull
    final OnMapClickListener onMapClickListener = new OnMapClickListener() {
        public void onMapClick(LatLng latLng) {
            if (((Boolean) RxUtil.blockingFirst(CardEventBus.INSTANCE.isExpanded())).booleanValue()) {
                CardEventBus.INSTANCE.collapseCards();
            }
        }
    };
    @NonNull
    final OnMarkerClickListener onMarkerClickListener = new OnMarkerClickListener() {
        public boolean onMarkerClick(Marker marker) {
            Analytics.sendEvent(AnalyticsEvent.TAPPING_THE_MARKER);
            if (PDMapFragment.this.markerManager != null) {
                PDMapFragment.this.markerManager.selectMarker(marker);
                NearbyItem nearbyItemForMarker = PDMapFragment.this.markerManager.getNearbyItemForMarker(marker);
                if (nearbyItemForMarker != null) {
                    PDMapFragment.this.markerSelection.onNext(nearbyItemForMarker);
                }
            }
            return false;
        }
    };
    @Nullable
    private Subscription ongoingNearbyRequestSubscription;
    @NonNull
    protected Boolean requestInProgress = Boolean.valueOf(false);
    @Nullable
    Session session;
    @Nullable
    Viewport viewport;
    @NonNull
    final Subject<Viewport, Viewport> viewportChanges = BehaviorSubject.create().toSerialized();

    private static class MapAndViewport {
        final GoogleMap map;
        final LatLngBounds viewport;

        MapAndViewport(GoogleMap map, LatLngBounds viewport) {
            this.map = map;
            this.viewport = viewport;
        }
    }

    private static class MapTypeAndCurrentLocation {
        @NonNull
        final LatLng currentLocation;
        @NonNull
        final GoogleMap map;
        @NonNull
        final NearbyItemType type;

        MapTypeAndCurrentLocation(@NonNull GoogleMap map, @NonNull NearbyItemType type, @NonNull LatLng currentLocation) {
            this.map = map;
            this.type = type;
            this.currentLocation = currentLocation;
        }

        @NonNull
        static Func3<GoogleMap, NearbyItemType, LatLng, MapTypeAndCurrentLocation> create() {
            return new Func3<GoogleMap, NearbyItemType, LatLng, MapTypeAndCurrentLocation>() {
                public MapTypeAndCurrentLocation call(GoogleMap googleMap, NearbyItemType nearbyItemType, LatLng latLng) {
                    return new MapTypeAndCurrentLocation(googleMap, nearbyItemType, latLng);
                }
            };
        }
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getMapAsync(new OnMapReadyCallback() {
            public void onMapReady(final GoogleMap googleMap) {
                googleMap.setOnCameraIdleListener(new OnCameraIdleListener() {
                    public void onCameraIdle() {
                        googleMap.setOnCameraIdleListener(null);
                        PDMapFragment.this.mapReady.onNext(googleMap);
                    }
                });
            }
        });
    }

    public void onPause() {
        releaseResources();
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        bind();
    }

    private void releaseResources() {
        this.mapReady.subscribe(clearListeners(), logError());
        this.allSubscriptions.clear();
    }

    @NonNull
    private Action1<GoogleMap> clearListeners() {
        return new Action1<GoogleMap>() {
            public void call(GoogleMap googleMap) {
                googleMap.setOnCameraMoveListener(null);
                googleMap.setOnMarkerClickListener(null);
                googleMap.setOnMapClickListener(null);
            }
        };
    }

    @NonNull
    Action1<Throwable> logError() {
        return new Action1<Throwable>() {
            public void call(Throwable throwable) {
                Log.e(throwable);
            }
        };
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.bitmapHelper = new BitmapHelper(activity);
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.bitmapHelper = new BitmapHelper(context);
    }

    public void setup(@NonNull final Session session, @NonNull LocationProvider locationProvider, @NonNull final LocationSourceProvider locationSourceProvider, @NonNull NearbyItemTypeChangesProvider nearbyItemTypeChangesProvider, @NonNull SearchButtonClickProvider searchButtonClickProvider, @NonNull CardSelectionProvider cardSelectionProvider) {
        this.session = session;
        this.nearbyItemTypeChangesProvider = nearbyItemTypeChangesProvider;
        this.locationProvider = locationProvider;
        this.cardSelectionProvider = cardSelectionProvider;
        this.allSubscriptions.add(this.mapReady.first().doOnNext(setClickListeners()).subscribe(new Action1<GoogleMap>() {
            public void call(GoogleMap googleMap) {
                googleMap.setLocationSource(locationSourceProvider.getLocationSource());
                googleMap.setMyLocationEnabled(true);
                PDMapFragment.this.setMapPadding(googleMap, 0);
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                googleMap.getUiSettings().setMapToolbarEnabled(false);
                if (PDMapFragment.this.bitmapHelper != null) {
                    PDMapFragment.this.markerManager = new MarkerManager(session, googleMap, PDMapFragment.this.bitmapHelper);
                }
            }
        }, logError()));
    }

    void setMapPadding(GoogleMap googleMap, int bottomPadding) {
        int mapInitialTopPaddingPixelSize = getResources().getDimensionPixelSize(R.dimen.map_initial_top_padding);
        int mapSidePaddingPixelSize = getResources().getDimensionPixelSize(R.dimen.map_side_padding);
        googleMap.setPadding(mapSidePaddingPixelSize, mapInitialTopPaddingPixelSize, mapSidePaddingPixelSize, bottomPadding);
    }

    @NonNull
    Action1<GoogleMap> startEmittingViewportChanges() {
        return new Action1<GoogleMap>() {
            public void call(final GoogleMap googleMap) {
                googleMap.setOnCameraMoveListener(new OnCameraMoveListener() {
                    public void onCameraMove() {
                        PDMapFragment.this.viewportChanges.onNext(Viewport.createFrom(googleMap));
                    }
                });
                PDMapFragment.this.viewportChanges.onNext(Viewport.createFrom(googleMap));
                PDMapFragment.this.isCameraAnimating = Boolean.valueOf(false);
                PDMapFragment.this.onViewportReady();
            }
        };
    }

    @NonNull
    private Action1<GoogleMap> clearCameraListeners() {
        return new Action1<GoogleMap>() {
            public void call(GoogleMap googleMap) {
                googleMap.setOnCameraIdleListener(null);
                googleMap.setOnCameraMoveListener(null);
            }
        };
    }

    @NonNull
    private Action1<GoogleMap> moveCameraToViewport(@NonNull final Viewport viewport) {
        return new Action1<GoogleMap>() {
            public void call(GoogleMap googleMap) {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(viewport.getLatLngBounds().getCenter(), viewport.getZoom().floatValue()));
            }
        };
    }

    private void bind() {
        this.allSubscriptions.add(this.mapReady.first().subscribe(setClickListeners(), logError()));
        this.allSubscriptions.add(viewportChanges().first().toCompletable().subscribe(new Action0() {
            public void call() {
                PDMapFragment.this.allSubscriptions.add((PDMapFragment.this.requestInProgress.booleanValue() ? Observable.just(Irrelevant.INSTANCE) : Observable.empty()).concatWith(NearbyRequestEventBus.INSTANCE.requestStartedEvents()).subscribe(new Action1<Irrelevant>() {
                    public void call(Irrelevant irrelevant) {
                        PDMapFragment.this.makeNearbyRequest();
                    }
                }, PDMapFragment.this.logError()));
            }
        }, logError()));
        if (this.cardSelectionProvider != null) {
            this.allSubscriptions.add(this.cardSelectionProvider.cardSelection().observeOn(AndroidSchedulers.mainThread()).subscribe(selectMapMarkerForNearbyItem(), logError()));
        }
        if (this.viewport != null) {
            this.allSubscriptions.add(this.mapReady.first().doOnNext(clearCameraListeners()).doOnNext(moveCameraToViewport(this.viewport)).doOnNext(startEmittingViewportChanges()).subscribe(Actions.empty(), logError()));
        } else if (this.session == null || this.nearbyItemTypeChangesProvider == null || this.locationProvider == null) {
            Throwable error = new IllegalStateException("Viewport can't be initialized: session = [" + this.session + "]" + ", nearbyItemTypeChangesProvider = [" + this.nearbyItemTypeChangesProvider + "]" + ", locationProvider = [" + this.locationProvider + "]");
            Log.e(error);
            LogJourno.reportEvent(EVENT.NOT_SPECIFIED_NEARBY_VIEWPORT_CANT_BE_INITIALIZED, error);
        } else {
            this.allSubscriptions.add(Observable.combineLatest(this.mapReady, this.nearbyItemTypeChangesProvider.nearbyItemTypeChanges(this.session), this.locationProvider.locationChanges(), MapTypeAndCurrentLocation.create()).subscribeOn(Schedulers.io()).first().map(new Func1<MapTypeAndCurrentLocation, MapAndViewport>() {
                public MapAndViewport call(MapTypeAndCurrentLocation data) {
                    Double maxDistanceToNearbyItem = new NearbyDataSource(PDMapFragment.this.session.getDatabase()).getMaxDistanceToNearbyItemForInitialZoom(PDMapFragment.this.session, data.type, Double.valueOf(data.currentLocation.latitude), Double.valueOf(data.currentLocation.longitude));
                    return new MapAndViewport(data.map, new Builder().include(SphericalUtil.computeOffset(data.currentLocation, maxDistanceToNearbyItem.doubleValue(), 0.0d)).include(SphericalUtil.computeOffset(data.currentLocation, maxDistanceToNearbyItem.doubleValue(), 90.0d)).include(SphericalUtil.computeOffset(data.currentLocation, maxDistanceToNearbyItem.doubleValue(), 180.0d)).include(SphericalUtil.computeOffset(data.currentLocation, maxDistanceToNearbyItem.doubleValue(), 270.0d)).build());
                }
            }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<MapAndViewport>() {
                public void call(final MapAndViewport mapAndViewport) {
                    mapAndViewport.map.stopAnimation();
                    PDMapFragment.this.isCameraAnimating = Boolean.valueOf(true);
                    mapAndViewport.map.setOnCameraMoveListener(null);
                    mapAndViewport.map.setOnCameraIdleListener(new OnCameraIdleListener() {
                        public void onCameraIdle() {
                            mapAndViewport.map.setOnCameraIdleListener(null);
                            PDMapFragment.this.startEmittingViewportChanges().call(mapAndViewport.map);
                        }
                    });
                    mapAndViewport.map.animateCamera(CameraUpdateFactory.newLatLngBounds(mapAndViewport.viewport, 0));
                }
            }, logError()));
        }
        this.allSubscriptions.add(CardEventBus.INSTANCE.heightEvents().observeOn(AndroidSchedulers.mainThread()).doOnNext(new Action1<Integer>() {
            public void call(Integer mapBottomPadding) {
                GoogleMap googleMap = (GoogleMap) RxUtil.blockingFirst(PDMapFragment.this.mapReady);
                PDMapFragment.this.setMapPadding(googleMap, mapBottomPadding.intValue());
                PDMapFragment.this.centerSelectedMarkerInViewportIfNotVisible(googleMap);
            }
        }).subscribe(Actions.empty(), logError()));
    }

    void makeNearbyRequest() {
        if (!(this.ongoingNearbyRequestSubscription == null || this.ongoingNearbyRequestSubscription.isUnsubscribed())) {
            this.ongoingNearbyRequestSubscription.unsubscribe();
        }
        this.ongoingNearbyRequestSubscription = getNearbyRequestCompletable().subscribe(Actions.empty(), logError());
        this.allSubscriptions.add(this.ongoingNearbyRequestSubscription);
    }

    @NonNull
    private Action1<NearbyItem> selectMapMarkerForNearbyItem() {
        return new Action1<NearbyItem>() {
            public void call(NearbyItem nearbyItem) {
                if (PDMapFragment.this.markerManager != null) {
                    PDMapFragment.this.markerManager.selectMarkerForItem(nearbyItem);
                }
            }
        };
    }

    @NonNull
    private Action1<GoogleMap> setClickListeners() {
        return new Action1<GoogleMap>() {
            public void call(GoogleMap googleMap) {
                googleMap.setOnMarkerClickListener(PDMapFragment.this.onMarkerClickListener);
                googleMap.setOnMapClickListener(PDMapFragment.this.onMapClickListener);
            }
        };
    }

    @NonNull
    Completable getNearbyRequestCompletable() {
        if (this.session == null || this.nearbyItemTypeChangesProvider == null || this.locationProvider == null) {
            return Completable.complete();
        }
        return Observable.fromCallable(new Callable<NearbyItemsRequest>() {
            public NearbyItemsRequest call() throws Exception {
                return new Builder().session(PDMapFragment.this.session).nearbyItemType((NearbyItemType) RxUtil.blockingFirst(PDMapFragment.this.nearbyItemTypeChangesProvider.nearbyItemTypeChanges(PDMapFragment.this.session))).viewport((Viewport) RxUtil.blockingFirst(PDMapFragment.this.viewportChanges)).location((LatLng) RxUtil.blockingFirst(PDMapFragment.this.locationProvider.locationChanges())).build();
            }
        }).doOnSubscribe(new Action0() {
            public void call() {
                GoogleMap googleMap = (GoogleMap) RxUtil.blockingFirst(PDMapFragment.this.mapReady);
                PDMapFragment.this.setMapPadding(googleMap, 0);
                PDMapFragment.this.viewportChanges.onNext(Viewport.createFrom(googleMap));
            }
        }).doOnSubscribe(new Action0() {
            public void call() {
                PDMapFragment.this.requestInProgress = Boolean.valueOf(true);
            }
        }).doOnNext(clearMarkers()).observeOn(Schedulers.computation()).delay(350, TimeUnit.MILLISECONDS).map(new Func1<NearbyItemsRequest, Observable<NearbyItem>>() {
            public Observable<NearbyItem> call(NearbyItemsRequest nearbyItemsRequest) {
                if (PDMapFragment.this.isInitialSetup.booleanValue()) {
                    return nearbyItemsRequest.requestInitialNearbyItems();
                }
                return nearbyItemsRequest.requestNearbyItems();
            }
        }).observeOn(AndroidSchedulers.mainThread()).doOnNext(updateMarkersAndEmitViewport()).observeOn(Schedulers.computation()).doOnNext(provideNearbyItems()).doOnNext(requestCompleted()).doOnNext(new Action1<Observable<NearbyItem>>() {
            public void call(Observable<NearbyItem> observable) {
                if (PDMapFragment.this.isInitialSetup.booleanValue()) {
                    PDMapFragment.this.isInitialSetup = Boolean.valueOf(false);
                }
            }
        }).toCompletable();
    }

    @NonNull
    private Action1<NearbyItemsRequest> clearMarkers() {
        return new Action1<NearbyItemsRequest>() {
            public void call(NearbyItemsRequest nearbyItemsRequest) {
                if (PDMapFragment.this.markerManager != null) {
                    PDMapFragment.this.markerManager.clearMarkers();
                }
            }
        };
    }

    @NonNull
    private Action1<Observable<NearbyItem>> updateMarkersAndEmitViewport() {
        return new Action1<Observable<NearbyItem>>() {
            public void call(Observable<NearbyItem> items) {
                if (PDMapFragment.this.markerManager != null) {
                    PDMapFragment.this.markerManager.setItems(items);
                }
                if (!PDMapFragment.this.isCameraAnimating.booleanValue()) {
                    PDMapFragment.this.viewportChanges.onNext(Viewport.createFrom((GoogleMap) PDMapFragment.this.mapReady.toBlocking().first()));
                }
            }
        };
    }

    @NonNull
    private Action1<Observable<NearbyItem>> provideNearbyItems() {
        return new Action1<Observable<NearbyItem>>() {
            public void call(Observable<NearbyItem> nearbyItemObservable) {
                PDMapFragment.this.nearbyItems.onNext((List) nearbyItemObservable.toList().toBlocking().singleOrDefault(Collections.emptyList()));
            }
        };
    }

    void onViewportReady() {
        if (this.isInitialSetup.booleanValue() || this.requestInProgress.booleanValue()) {
            NearbyRequestEventBus.INSTANCE.onRequestStarted();
        }
    }

    @NonNull
    private Action1<Observable<NearbyItem>> requestCompleted() {
        return new Action1<Observable<NearbyItem>>() {
            public void call(Observable<NearbyItem> observable) {
                PDMapFragment.this.requestInProgress = Boolean.valueOf(false);
                NearbyRequestEventBus.INSTANCE.onRequestCompleted();
            }
        };
    }

    @NonNull
    public Observable<List<NearbyItem>> nearbyItems() {
        return this.nearbyItems;
    }

    @NonNull
    public Observable<Viewport> viewportChanges() {
        return this.viewportChanges.doOnNext(new Action1<Viewport>() {
            public void call(Viewport viewport) {
                PDMapFragment.this.viewport = viewport;
            }
        });
    }

    @NonNull
    public Observable<NearbyItem> markerSelection() {
        return this.markerSelection.distinctUntilChanged();
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.viewport != null) {
            bundle.putString(KEY_VIEWPORT, this.viewport.toJsonString());
        }
        bundle.putBoolean(KEY_IS_INITIAL_SETUP, this.isInitialSetup.booleanValue());
        bundle.putBoolean(KEY_REQUEST_IN_PROGRESS, this.requestInProgress.booleanValue());
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            this.viewport = Viewport.fromJsonString(bundle.getString(KEY_VIEWPORT));
            this.isInitialSetup = Boolean.valueOf(bundle.getBoolean(KEY_IS_INITIAL_SETUP, true));
            this.requestInProgress = Boolean.valueOf(bundle.getBoolean(KEY_REQUEST_IN_PROGRESS, false));
        }
    }

    void centerSelectedMarkerInViewportIfNotVisible(@NonNull GoogleMap map) {
        if (this.markerManager != null) {
            Marker currentlySelectedMarker = this.markerManager.getCurrentlySelectedMarker();
            if (currentlySelectedMarker != null) {
                map.stopAnimation();
                if (!map.getProjection().getVisibleRegion().latLngBounds.contains(currentlySelectedMarker.getPosition())) {
                    map.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(currentlySelectedMarker.getPosition()).zoom(map.getCameraPosition().zoom).build()));
                }
            }
        }
    }
}
