package com.pipedrive.nearby.cards.cards;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.ViewGroup;
import com.pipedrive.model.Organization;
import com.pipedrive.nearby.model.OrganizationNearbyItem;

public class SingleOrganizationCard extends SingleCard<OrganizationNearbyItem, Organization> {
    public /* bridge */ /* synthetic */ void unbind() {
        super.unbind();
    }

    public SingleOrganizationCard(@NonNull ViewGroup parent, @LayoutRes @NonNull Integer layoutResId) {
        super(parent, layoutResId);
    }

    @NonNull
    CardHeader<OrganizationNearbyItem, Organization> getHeader() {
        return new OrganizationCardHeader();
    }

    void onDetailsRequested(@NonNull Long sqlId) {
        CardEventBus.INSTANCE.onOrganizationDetailsClicked(sqlId);
    }
}
