package com.pipedrive.products;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.pipedrive.BaseActivity;
import com.pipedrive.R;
import com.pipedrive.adapter.DealProductListAdapter;
import com.pipedrive.adapter.DealProductListAdapter.OnDealProductItemClickListener;
import com.pipedrive.linking.ProductLinkingActivity;
import com.pipedrive.model.Deal;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.products.edit.DealProductEditActivity;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.views.DividerItemDecorationNoDividerAfterLastItem;
import java.util.List;

public class DealProductListActivity extends BaseActivity implements DealProductListView, OnDealProductItemClickListener {
    private static final String ARG_DEAL_SQL_ID = "deal_sql_id";
    private static final int EDIT_PRODUCT_REQUEST_CODE = 1;
    @BindView(2131820822)
    View mContainer;
    @BindView(2131820821)
    TextView mFailedRequestMessage;
    @BindView(2131820824)
    FloatingActionButton mFloatingActionButton;
    private boolean mLastOperationWasDealProductRemoval;
    private DealProductListPresenter mPresenter;
    @BindView(2131820794)
    View mProgressBar;
    @BindView(2131820823)
    RecyclerView mRecyclerView;
    @BindView(2131820700)
    Toolbar mToolbar;

    public static void startActivity(@NonNull Activity activity, @NonNull Long dealSqlId) {
        ActivityCompat.startActivity(activity, new Intent(activity, DealProductListActivity.class).putExtra(ARG_DEAL_SQL_ID, dealSqlId), null);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        ButterKnife.bind(this);
        setupToolbar();
        this.mPresenter = new DealProductListPresenterImpl(getSession());
    }

    private void setupToolbar() {
        setSupportActionBar(this.mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            this.mToolbar.setNavigationIcon(R.drawable.icon_back);
        }
    }

    public void onResume() {
        super.onResume();
        this.mPresenter.bindView(this);
        requestProductList(getIntent().getExtras());
    }

    private void requestProductList(@Nullable Bundle intentExtras) {
        if (intentExtras == null) {
            finish();
            return;
        }
        this.mPresenter.requestDealProductList(Long.valueOf(intentExtras.getLong(ARG_DEAL_SQL_ID)), this.mLastOperationWasDealProductRemoval);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onDealProductListRequestSuccess(@NonNull List<DealProduct> dealProducts, @NonNull Deal deal) {
        setupRecyclerView(dealProducts, deal.getCurrencyCode());
        this.mFloatingActionButton.setVisibility(0);
    }

    private void setupRecyclerView(@NonNull List<DealProduct> dealProducts, @Nullable String currencyCode) {
        this.mRecyclerView.addItemDecoration(new DividerItemDecorationNoDividerAfterLastItem(this));
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(this, 1, false));
        this.mRecyclerView.setAdapter(new DealProductListAdapter(dealProducts, getSession(), currencyCode, this));
        this.mContainer.setVisibility(0);
    }

    protected void onPause() {
        super.onPause();
        this.mPresenter.unbindView();
    }

    public void onDealProductListRequestFail() {
        if (ConnectionUtil.isConnected(this)) {
            this.mFailedRequestMessage.setText(R.string.loading_the_products_list_failed_);
        } else {
            this.mFailedRequestMessage.setText(R.string.product_list_not_available_in_);
        }
        this.mFailedRequestMessage.setVisibility(0);
    }

    public void onLastDealProductRemoved() {
        finish();
    }

    public void onItemClicked(long dealProductSqlId) {
        DealProductEditActivity.startActivityForResult(this, Long.valueOf(dealProductSqlId), 1);
    }

    @OnClick({2131820824})
    void onFabClicked() {
        ProductLinkingActivity.startActivity(this, Long.valueOf(getIntent().getExtras().getLong(ARG_DEAL_SQL_ID)));
    }

    public void showProgressBar() {
        this.mProgressBar.setVisibility(0);
        this.mContainer.setVisibility(8);
        this.mFloatingActionButton.setVisibility(8);
    }

    public void hideProgressBar() {
        this.mProgressBar.setVisibility(8);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        boolean expectedProductRequestCode = requestCode == 1 && resultCode == -1;
        if (expectedProductRequestCode && data != null && data.getExtras().containsKey(DealProductEditActivity.ARG_DEALPRODUCT_WAS_REMOVED)) {
            this.mLastOperationWasDealProductRemoval = true;
        }
    }
}
