package com.zendesk.sdk.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.logger.Logger;
import com.zendesk.util.StringUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

class ZendeskRequestStorage implements RequestStorage {
    private static final String LOG_TAG = ZendeskRequestStorage.class.getSimpleName();
    private static final String PREFS_COMMENT_COUNT_KEY_PREFIX = "request-id-cc";
    private static final String PREFS_NAME = "zendesk-authorization";
    private static final String REQUEST_KEY = "stored_requests";
    private final SharedPreferences storage;

    ZendeskRequestStorage(@NonNull Context context) {
        this.storage = context == null ? null : context.getSharedPreferences(PREFS_NAME, 0);
        if (this.storage == null) {
            Logger.w(LOG_TAG, "Storage was not initialised, requests will not be stored.", new Object[0]);
        }
    }

    @NonNull
    public List<String> getStoredRequestIds() {
        List<String> requests = new ArrayList();
        if (this.storage != null) {
            String storedRequestId = this.storage.getString(REQUEST_KEY, "");
            if (StringUtils.hasLength(storedRequestId)) {
                requests.addAll(StringUtils.fromCsv(storedRequestId));
            }
        }
        return requests;
    }

    public void storeRequestId(@NonNull String requestId) {
        if (this.storage != null && StringUtils.hasLength(requestId)) {
            List requestList = getStoredRequestIds();
            if (!requestList.contains(requestId)) {
                requestList.add(requestId);
                this.storage.edit().putString(REQUEST_KEY, StringUtils.toCsvString(requestList)).apply();
            }
        }
    }

    public void setCommentCount(@NonNull String requestId, int commentCount) {
        if (!StringUtils.hasLength(requestId) || commentCount < 0) {
            Logger.d(LOG_TAG, "Invalid requestId or commentCount provided", new Object[0]);
            return;
        }
        this.storage.edit().putInt(getCommentCountKey(requestId), commentCount).apply();
    }

    @Nullable
    public Integer getCommentCount(@NonNull String requestId) {
        if (StringUtils.hasLength(requestId)) {
            int commentCount = this.storage.getInt(getCommentCountKey(requestId), -1);
            if (commentCount != -1) {
                return Integer.valueOf(commentCount);
            }
            return null;
        }
        Logger.d(LOG_TAG, "Invalid requestId provided", new Object[0]);
        return null;
    }

    @NonNull
    private String getCommentCountKey(@Nullable String requestId) {
        return String.format(Locale.US, "%s-%s", new Object[]{PREFS_COMMENT_COUNT_KEY_PREFIX, requestId});
    }

    public void clearUserData() {
        if (this.storage != null) {
            this.storage.edit().clear().apply();
        }
    }

    @NonNull
    public String getCacheKey() {
        return PREFS_NAME;
    }
}
