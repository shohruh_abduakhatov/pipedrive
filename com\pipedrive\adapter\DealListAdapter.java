package com.pipedrive.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.views.viewholder.ViewHolderBuilder;
import com.pipedrive.views.viewholder.deal.DealRowViewHolderForLinking;

public class DealListAdapter extends BaseSectionedListAdapter {
    private final DealsDataSource mDataSource;
    @NonNull
    private SearchConstraint mSearchConstraint = SearchConstraint.EMPTY;
    @NonNull
    private final Session mSession;

    public DealListAdapter(Context context, Cursor c, @NonNull Session session) {
        super(context, c, true);
        this.mSession = session;
        this.mDataSource = new DealsDataSource(session.getDatabase());
    }

    public void setSearchConstraint(@NonNull SearchConstraint searchConstraint) {
        this.mSearchConstraint = searchConstraint;
        changeCursor(this.mDataSource.getSearchCursor(searchConstraint));
    }

    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return ViewHolderBuilder.inflateViewAndTagWithViewHolder(context, parent, false, new DealRowViewHolderForLinking());
    }

    public void bindView(View view, Context context, Cursor cursor) {
        ((DealRowViewHolderForLinking) view.getTag()).fill(this.mSession, context, this.mDataSource.deflateCursor(cursor), this.mSearchConstraint);
    }
}
