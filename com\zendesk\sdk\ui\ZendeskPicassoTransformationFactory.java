package com.zendesk.sdk.ui;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import com.squareup.picasso.Transformation;
import java.util.Locale;

public enum ZendeskPicassoTransformationFactory {
    INSTANCE;

    private class ResizeTransformHeight implements Transformation {
        private final int maxHeight;

        public ResizeTransformHeight(int maxHeight) {
            this.maxHeight = maxHeight;
        }

        public Bitmap transform(Bitmap source) {
            int targetHeight;
            int targetWidth;
            if (source.getHeight() > this.maxHeight) {
                targetHeight = this.maxHeight;
                targetWidth = (int) (((double) targetHeight) * (((double) source.getWidth()) / ((double) source.getHeight())));
            } else {
                targetHeight = source.getHeight();
                targetWidth = source.getWidth();
            }
            Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
            if (result != source) {
                source.recycle();
            }
            return result;
        }

        public String key() {
            return "max-height-" + this.maxHeight;
        }
    }

    private class ResizeTransformWidth implements Transformation {
        private final int maxWidth;

        public ResizeTransformWidth(int maxWidth) {
            this.maxWidth = maxWidth;
        }

        public Bitmap transform(Bitmap source) {
            int targetWidth;
            int targetHeight;
            if (source.getWidth() > this.maxWidth) {
                targetWidth = this.maxWidth;
                targetHeight = (int) (((double) targetWidth) * (((double) source.getHeight()) / ((double) source.getWidth())));
            } else {
                targetHeight = source.getHeight();
                targetWidth = source.getWidth();
            }
            Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
            if (result != source) {
                source.recycle();
            }
            return result;
        }

        public String key() {
            return "max-width-" + this.maxWidth;
        }
    }

    private class RoundedTransformation implements Transformation {
        private final int margin;
        private final int radius;

        public RoundedTransformation(int radius, int margin) {
            this.radius = radius;
            this.margin = margin;
        }

        public Bitmap transform(Bitmap source) {
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setShader(new BitmapShader(source, TileMode.CLAMP, TileMode.CLAMP));
            Bitmap output = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Config.ARGB_8888);
            new Canvas(output).drawRoundRect(new RectF((float) this.margin, (float) this.margin, (float) (source.getWidth() - this.margin), (float) (source.getHeight() - this.margin)), (float) this.radius, (float) this.radius, paint);
            if (source != output) {
                source.recycle();
            }
            return output;
        }

        public String key() {
            return String.format(Locale.US, "rounded-%s-%s", new Object[]{Integer.valueOf(this.radius), Integer.valueOf(this.margin)});
        }
    }

    public Transformation getResizeTransformationWidth(int maxWidth) {
        return new ResizeTransformWidth(maxWidth);
    }

    public Transformation getResizeTransformationHeight(int maxHeight) {
        return new ResizeTransformHeight(maxHeight);
    }

    public Transformation getRoundedTransformation(int radius, int margin) {
        return new RoundedTransformation(radius, margin);
    }
}
