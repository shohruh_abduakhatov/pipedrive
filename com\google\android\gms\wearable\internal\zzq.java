package com.google.android.gms.wearable.internal;

import android.util.Log;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.wearable.ChannelIOException;
import java.io.IOException;
import java.io.OutputStream;

public final class zzq extends OutputStream {
    private volatile zzm aTx;
    private final OutputStream aTz;

    public zzq(OutputStream outputStream) {
        this.aTz = (OutputStream) zzaa.zzy(outputStream);
    }

    private IOException zza(IOException iOException) {
        zzm com_google_android_gms_wearable_internal_zzm = this.aTx;
        if (com_google_android_gms_wearable_internal_zzm == null) {
            return iOException;
        }
        if (Log.isLoggable("ChannelOutputStream", 2)) {
            Log.v("ChannelOutputStream", "Caught IOException, but channel has been closed. Translating to ChannelIOException.", iOException);
        }
        return new ChannelIOException("Channel closed unexpectedly before stream was finished", com_google_android_gms_wearable_internal_zzm.aTn, com_google_android_gms_wearable_internal_zzm.aTo);
    }

    public void close() throws IOException {
        try {
            this.aTz.close();
        } catch (IOException e) {
            throw zza(e);
        }
    }

    public void flush() throws IOException {
        try {
            this.aTz.flush();
        } catch (IOException e) {
            throw zza(e);
        }
    }

    public void write(int i) throws IOException {
        try {
            this.aTz.write(i);
        } catch (IOException e) {
            throw zza(e);
        }
    }

    public void write(byte[] bArr) throws IOException {
        try {
            this.aTz.write(bArr);
        } catch (IOException e) {
            throw zza(e);
        }
    }

    public void write(byte[] bArr, int i, int i2) throws IOException {
        try {
            this.aTz.write(bArr, i, i2);
        } catch (IOException e) {
            throw zza(e);
        }
    }

    void zzc(zzm com_google_android_gms_wearable_internal_zzm) {
        this.aTx = com_google_android_gms_wearable_internal_zzm;
    }

    zzu zzcmt() {
        return new zzu(this) {
            final /* synthetic */ zzq aTA;

            {
                this.aTA = r1;
            }

            public void zzb(zzm com_google_android_gms_wearable_internal_zzm) {
                this.aTA.zzc(com_google_android_gms_wearable_internal_zzm);
            }
        };
    }
}
