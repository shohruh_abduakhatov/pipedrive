package com.zendesk.sdk.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.Nullable;
import com.google.gson.Gson;
import com.newrelic.agent.android.instrumentation.GsonInstrumentation;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.access.AccessToken;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.model.access.AnonymousIdentity.Builder;
import com.zendesk.sdk.model.access.AuthenticationType;
import com.zendesk.sdk.model.access.Identity;
import com.zendesk.sdk.model.access.JwtIdentity;
import com.zendesk.util.StringUtils;
import java.lang.reflect.Type;
import java.util.Locale;
import java.util.UUID;

class ZendeskIdentityStorage implements IdentityStorage {
    private static final String IDENTITY_KEY = "zendesk-identity";
    private static final String IDENTITY_TYPE_KEY = "zendesk-identity-type";
    private static final String LOG_TAG = ZendeskIdentityStorage.class.getSimpleName();
    private static final String PREFS_NAME = "zendesk-token";
    private static final String TOKEN_KEY = "stored_token";
    private static final String UUID_KEY = "uuid";
    private final Gson gson;
    private final SharedPreferences storage;

    ZendeskIdentityStorage(Context context, Gson gson) {
        this.storage = context == null ? null : context.getSharedPreferences(PREFS_NAME, 0);
        if (this.storage == null) {
            Logger.w(LOG_TAG, "Storage was not initialised, user token will not be stored.", new Object[0]);
        }
        this.gson = gson;
    }

    public void storeAccessToken(AccessToken accessToken) {
        if (this.storage != null && accessToken != null) {
            Gson gson = this.gson;
            this.storage.edit().putString(TOKEN_KEY, !(gson instanceof Gson) ? gson.toJson(accessToken) : GsonInstrumentation.toJson(gson, accessToken)).apply();
        }
    }

    public AccessToken getStoredAccessToken() {
        if (this.storage == null) {
            return null;
        }
        String accessTokenString = this.storage.getString(TOKEN_KEY, null);
        if (!StringUtils.hasLength(accessTokenString)) {
            return null;
        }
        Gson gson = this.gson;
        Class cls = AccessToken.class;
        return !(gson instanceof Gson) ? gson.fromJson(accessTokenString, cls) : GsonInstrumentation.fromJson(gson, accessTokenString, cls);
    }

    @Nullable
    public String getStoredAccessTokenAsBearerToken() {
        AccessToken accessToken = getStoredAccessToken();
        if (accessToken == null) {
            Logger.w(LOG_TAG, "There is no stored access token, have you initialised an identity and requested an access token?", new Object[0]);
        }
        String bearerFormat = "Bearer %s";
        if (accessToken == null) {
            return null;
        }
        return String.format(Locale.US, bearerFormat, new Object[]{accessToken.getAccessToken()});
    }

    public String getUUID() {
        if (this.storage == null || !this.storage.contains("uuid")) {
            Logger.d(LOG_TAG, "Generating new UUID", new Object[0]);
            String id = UUID.randomUUID().toString();
            if (this.storage == null) {
                return id;
            }
            Logger.d(LOG_TAG, "Storing new UUID in preference store", new Object[0]);
            this.storage.edit().putString("uuid", id).apply();
            return id;
        }
        Logger.d(LOG_TAG, "Fetching UUID from preferences store", new Object[0]);
        return this.storage.getString("uuid", "");
    }

    public void storeIdentity(Identity identity) {
        if (this.storage == null) {
            Logger.e(LOG_TAG, "Storage is not initialised, will not store the identity", new Object[0]);
        } else if (identity == null) {
            Logger.e(LOG_TAG, "identity is null, will not store the identity", new Object[0]);
        } else {
            String identityString = null;
            String authenticationType = null;
            String sdkGuid = null;
            Gson gson;
            Type type;
            if (identity instanceof AnonymousIdentity) {
                Logger.d(LOG_TAG, "Storing anonymous identity", new Object[0]);
                gson = this.gson;
                type = AnonymousIdentity.class;
                identityString = !(gson instanceof Gson) ? gson.toJson(identity, type) : GsonInstrumentation.toJson(gson, identity, type);
                authenticationType = AuthenticationType.ANONYMOUS.getAuthenticationType();
                sdkGuid = ((AnonymousIdentity) identity).getSdkGuid();
            } else if (identity instanceof JwtIdentity) {
                Logger.d(LOG_TAG, "Storing jwt identity", new Object[0]);
                gson = this.gson;
                type = JwtIdentity.class;
                identityString = !(gson instanceof Gson) ? gson.toJson(identity, type) : GsonInstrumentation.toJson(gson, identity, type);
                authenticationType = AuthenticationType.JWT.getAuthenticationType();
            } else {
                Logger.e(LOG_TAG, "Unknown authentication type, identity will not be stored", new Object[0]);
            }
            if (StringUtils.hasLength(identityString) && authenticationType != null) {
                Editor editor = this.storage.edit();
                editor.putString(IDENTITY_KEY, identityString).putString(IDENTITY_TYPE_KEY, authenticationType);
                if (StringUtils.hasLength(sdkGuid)) {
                    editor.putString("uuid", sdkGuid);
                }
                editor.apply();
            }
        }
    }

    @Nullable
    public Identity getIdentity() {
        if (this.storage == null) {
            return null;
        }
        String identityString = this.storage.getString(IDENTITY_KEY, null);
        String identityType = this.storage.getString(IDENTITY_TYPE_KEY, null);
        if (!StringUtils.hasLength(identityString) || !StringUtils.hasLength(identityType)) {
            return null;
        }
        AuthenticationType authenticationType = AuthenticationType.getAuthType(identityType);
        Gson gson;
        Class cls;
        if (AuthenticationType.JWT == authenticationType) {
            Logger.d(LOG_TAG, "Loading Jwt identity", new Object[0]);
            gson = this.gson;
            cls = JwtIdentity.class;
            return !(gson instanceof Gson) ? gson.fromJson(identityString, cls) : GsonInstrumentation.fromJson(gson, identityString, cls);
        } else if (AuthenticationType.ANONYMOUS == authenticationType) {
            Logger.d(LOG_TAG, "Loading Anonymous identity", new Object[0]);
            gson = this.gson;
            cls = AnonymousIdentity.class;
            return !(gson instanceof Gson) ? gson.fromJson(identityString, cls) : GsonInstrumentation.fromJson(gson, identityString, cls);
        } else {
            Logger.e(LOG_TAG, "Unknown identity type, identity will be null", new Object[0]);
            return null;
        }
    }

    @Nullable
    public Identity anonymiseIdentity() {
        boolean needsAnonymisation = false;
        if (this.storage == null) {
            Logger.e(LOG_TAG, "Cannot anonymise identity as the storage is null", new Object[0]);
            return null;
        }
        Identity identity = getIdentity();
        if (identity == null) {
            Logger.w(LOG_TAG, "Cannot anonymise a null identity.", new Object[0]);
            return identity;
        } else if (identity instanceof AnonymousIdentity) {
            AnonymousIdentity anonymousIdentity = (AnonymousIdentity) identity;
            if (StringUtils.hasLength(anonymousIdentity.getEmail()) || StringUtils.hasLength(anonymousIdentity.getName())) {
                needsAnonymisation = true;
            }
            if (!needsAnonymisation) {
                return identity;
            }
            Identity anonymisedIdentity = new Builder().withExternalIdentifier(anonymousIdentity.getExternalId()).build();
            storeIdentity(anonymisedIdentity);
            return anonymisedIdentity;
        } else {
            Logger.d(LOG_TAG, "Identity is not anonymous, no anonymisation is required", new Object[0]);
            return identity;
        }
    }

    public void clearUserData() {
        if (this.storage != null) {
            this.storage.edit().clear().apply();
        }
    }

    public String getCacheKey() {
        return PREFS_NAME;
    }
}
