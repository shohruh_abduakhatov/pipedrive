package com.pipedrive.util.formatter;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class PercentFormatter extends BaseFormatter {
    public PercentFormatter(@NonNull Session session) {
        super(session);
    }

    @NonNull
    NumberFormat instantiateNumberFormat(@NonNull Locale locale) {
        return DecimalFormat.getPercentInstance(locale);
    }

    @NonNull
    public String formatDealProductDiscount(@NonNull Double value) {
        return format(Double.valueOf(value.doubleValue() / 100.0d));
    }
}
