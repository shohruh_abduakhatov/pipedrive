package com.pipedrive.tasks;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.util.CurrenciesNetworkingUtil;

public class CurrenciesTask extends AsyncTask<Long, Integer, Void> {
    public CurrenciesTask(@NonNull Session session) {
        super(session);
    }

    protected Void doInBackground(Long... params) {
        CurrenciesNetworkingUtil.downloadAndStoreCurrenciesBlocking(getSession());
        return null;
    }
}
