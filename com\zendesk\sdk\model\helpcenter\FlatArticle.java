package com.zendesk.sdk.model.helpcenter;

import android.support.annotation.NonNull;

public class FlatArticle implements Comparable<FlatArticle> {
    private Article article;
    private Category category;
    private Section section;

    public FlatArticle(Category category, Section section, Article article) {
        if (category == null) {
            category = new Category();
        }
        this.category = category;
        if (section == null) {
            section = new Section();
        }
        this.section = section;
        if (article == null) {
            article = new Article();
        }
        this.article = article;
    }

    @NonNull
    public Category getCategory() {
        return this.category;
    }

    @NonNull
    public Section getSection() {
        return this.section;
    }

    @NonNull
    public Article getArticle() {
        return this.article;
    }

    public String toString() {
        return this.category.getName() + ", " + this.section.getName() + ", " + this.article.getTitle();
    }

    public int compareTo(@NonNull FlatArticle flatArticle) {
        if (flatArticle == null) {
            return -1;
        }
        return toString().compareTo(flatArticle.toString());
    }
}
