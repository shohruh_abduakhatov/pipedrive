package com.pipedrive.model.products;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.model.ChildDataSourceEntity;
import java.util.List;

public class ProductVariation extends ChildDataSourceEntity {
    @NonNull
    private final String name;
    @NonNull
    private final List<ProductPrice> prices;

    @NonNull
    public static ProductVariation create(@NonNull Long pipedriveId, @NonNull String name, @NonNull List<ProductPrice> variationPrices) {
        return create(null, pipedriveId, null, name, variationPrices);
    }

    @NonNull
    public static ProductVariation create(@Nullable Long sqlId, @Nullable Long pipedriveId, @Nullable Long productSqlId, @NonNull String name, @NonNull List<ProductPrice> prices) {
        return new ProductVariation(sqlId, pipedriveId, productSqlId, name, prices);
    }

    private ProductVariation(@Nullable Long sqlId, @Nullable Long pipedriveId, @Nullable Long parentSqlId, @NonNull String name, @NonNull List<ProductPrice> prices) {
        super(sqlId, pipedriveId, parentSqlId);
        this.name = name;
        this.prices = prices;
    }

    @NonNull
    public String getName() {
        return this.name;
    }

    @NonNull
    public List<ProductPrice> getPrices() {
        return this.prices;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass() || !super.equals(o)) {
            return false;
        }
        ProductVariation that = (ProductVariation) o;
        if (this.name.equals(that.name)) {
            return this.prices.equals(that.prices);
        }
        return false;
    }

    public int hashCode() {
        return (((super.hashCode() * 31) + this.name.hashCode()) * 31) + this.prices.hashCode();
    }

    public String toString() {
        return "ProductVariation{name='" + this.name + '\'' + ", prices=" + this.prices + '}';
    }
}
