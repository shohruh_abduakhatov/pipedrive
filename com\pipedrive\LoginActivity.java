package com.pipedrive;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.animation.LayoutTransition;
import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentSender;
import android.content.IntentSender.SendIntentException;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.background.ApplicationStateMonitor;
import com.newrelic.agent.android.instrumentation.AsyncTaskInstrumentation;
import com.newrelic.agent.android.instrumentation.Instrumented;
import com.newrelic.agent.android.tracing.TraceMachine;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.AnalyticsEvent;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.chrometab.BaseChromeTabActivity;
import com.pipedrive.dialogs.login.DeviceDisabledDialog;
import com.pipedrive.dialogs.login.EmailPasswordLoginFailedDialogFragment;
import com.pipedrive.dialogs.login.GoogleLoginFailedDialogFragment;
import com.pipedrive.logging.Log;
import com.pipedrive.tasks.authorization.GoogleLoginTask;
import com.pipedrive.tasks.authorization.OnEnoughDataToShowUI;
import com.pipedrive.tasks.authorization.UserLoginTask;
import com.pipedrive.tasks.authorization.UserLoginTask.OnLoginListener;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.util.ViewUtil.OnKeyboardVisibilityListener;
import com.pipedrive.whatsnew.WhatsNewActivity;
import java.util.concurrent.TimeUnit;

@Instrumented
public class LoginActivity extends AppCompatActivity implements TraceFieldInterface {
    private static final String KEY_DEVICE_DISABLED = "DEVICE_DISABLED";
    public static final long RECENTS_SYNC_INTERVAL_IN_SECONDS = TimeUnit.HOURS.toSeconds(2);
    public static final int REQUEST_GOOGLE_PLUS_CHOOSE_ACCOUNT = 1002;
    private static final int REQUEST_GOOGLE_PLUS_RECOVER_FROM_GOOGLE_PLAY_ERROR_DIALOG = 1003;
    private static final int REQUEST_GOOGLE_PLUS_SIGN_IN = 1001;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private UserLoginTask mAuthTask = null;
    @BindView(2131820780)
    View mCenterLayout;
    @BindView(2131820960)
    View mConnectWithGoogleButton;
    @BindView(2131821158)
    EditText mEmailView;
    private GoogleLoginTask mGoogleLoginTask = null;
    private GoogleApiClient mGoogleSignInApiClient;
    private boolean mGoogleSignInButtonClicked = false;
    private boolean mGoogleSignInIntentInProgress = false;
    private final OnEnoughDataToShowUI mOnEnoughDataToShowUI = new OnEnoughDataToShowUI() {
        public void readyToShowUI(@NonNull Session session) {
            WhatsNewActivity.startWhatsNewActivityIfRequiredOrMoveToPipelineActivity(session, LoginActivity.this);
            LoginActivity.this.finish();
        }
    };
    @BindView(2131821159)
    EditText mPasswordView;
    @BindView(2131820794)
    View mProgress;
    @BindView(2131820716)
    ViewGroup mRootView;
    @BindView(2131820779)
    View mTopLayout;

    protected void onStop() {
        super.onStop();
        ApplicationStateMonitor.getInstance().activityStopped();
    }

    @NonNull
    public static Intent startIntent(@NonNull Context context) {
        return IntentCompat.makeRestartActivityTask(new ComponentName(context, LoginActivity.class));
    }

    @NonNull
    public static Intent deviceDisabledIntent(@NonNull Context context) {
        return startIntent(context).putExtra(KEY_DEVICE_DISABLED, true);
    }

    protected void onStart() {
        ApplicationStateMonitor.getInstance().activityStarted();
        super.onStart();
        Analytics.hitScreen(this);
    }

    protected void onCreate(Bundle bundle) {
        TraceMachine.startTracing("LoginActivity");
        try {
            TraceMachine.enterMethod(this._nr_trace, "LoginActivity#onCreate", null);
        } catch (NoSuchFieldError e) {
            while (true) {
                TraceMachine.enterMethod(null, "LoginActivity#onCreate", null);
            }
        }
        super.onCreate(bundle);
        PipedriveAppBase.checkForHockeyappUpdates(this);
        logOut(this, false, null);
        setContentView((int) R.layout.activity_login);
        ButterKnife.bind((Activity) this);
        this.mGoogleSignInApiClient = new Builder(this).addConnectionCallbacks(new ConnectionCallbacks() {
            public void onConnectionSuspended(int cause) {
                LoginActivity.this.mGoogleSignInApiClient.connect();
            }

            public void onConnected(Bundle connectionHint) {
                if (LoginActivity.this.mGoogleSignInButtonClicked) {
                    LoginActivity.this.getGooglePlusTokenAsync();
                }
            }
        }).addOnConnectionFailedListener(new OnConnectionFailedListener() {
            public void onConnectionFailed(@NonNull ConnectionResult result) {
                IntentSender sender = null;
                boolean signInIntentFinished = true;
                if (!LoginActivity.this.mGoogleSignInButtonClicked || result.hasResolution() || result.getErrorCode() == 0) {
                    if (LoginActivity.this.mGoogleSignInIntentInProgress || !LoginActivity.this.mGoogleSignInButtonClicked) {
                        signInIntentFinished = false;
                    }
                    boolean signInHasFailureResult = result.hasResolution();
                    if (signInIntentFinished && signInHasFailureResult) {
                        try {
                            LoginActivity.this.mGoogleSignInIntentInProgress = true;
                            if (result.getResolution() != null) {
                                sender = result.getResolution().getIntentSender();
                            }
                            LoginActivity.this.startIntentSenderForResult(sender, 1001, null, 0, 0, 0);
                            return;
                        } catch (SendIntentException e) {
                            LoginActivity.this.mGoogleSignInIntentInProgress = false;
                            return;
                        }
                    }
                    return;
                }
                GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), LoginActivity.this, 1003, new OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        LoginActivity.this.mGoogleSignInButtonClicked = false;
                    }
                }).show();
            }
        }).addApi(Plus.API).addScope(Plus.SCOPE_PLUS_LOGIN).build();
        setupLayoutTransitions();
        ViewUtil.setKeyboardVisibilityListener(this, this.mRootView, new OnKeyboardVisibilityListener() {
            public void onVisibilityChanged(int visibility) {
                if (visibility == 1) {
                    LoginActivity.this.mRootView.post(new Runnable() {
                        public void run() {
                            LoginActivity.this.mTopLayout.setVisibility(8);
                            LoginActivity.this.mConnectWithGoogleButton.setVisibility(8);
                        }
                    });
                } else {
                    LoginActivity.this.mRootView.post(new Runnable() {
                        public void run() {
                            int i = 0;
                            LoginActivity.this.mTopLayout.setVisibility(0);
                            View view = LoginActivity.this.mConnectWithGoogleButton;
                            if (!(LoginActivity.this.mAuthTask == null && LoginActivity.this.mGoogleLoginTask == null)) {
                                i = 8;
                            }
                            view.setVisibility(i);
                        }
                    });
                }
            }
        });
        if (getIntent().getBooleanExtra(KEY_DEVICE_DISABLED, false)) {
            showDisableDeviceMessage();
        }
        TraceMachine.exitMethod();
    }

    private void setupLayoutTransitions() {
        LayoutTransition transition = new LayoutTransition();
        transition.setDuration((long) getResources().getInteger(17694720));
        transition.setStartDelay(2, 0);
        transition.setStartDelay(3, 0);
        transition.setStartDelay(0, 0);
        transition.setStartDelay(1, 0);
        this.mRootView.setLayoutTransition(transition);
    }

    @OnClick({2131821160})
    void onSignInClicked() {
        Analytics.sendEvent(this, AnalyticsEvent.LOG_IN);
        attemptLogin();
    }

    @OnClick({2131821161})
    void onSignUpClicked() {
        Analytics.sendEvent(this, AnalyticsEvent.SIGN_UP);
        BaseChromeTabActivity.start(this, getString(R.string.url_link_for_signup));
    }

    @OnClick({2131820960})
    void onGoogleSignInClicked() {
        if (!this.mGoogleSignInButtonClicked) {
            Analytics.sendEvent(this, AnalyticsEvent.CONNECT_WITH_GOOGLE);
            this.mGoogleSignInButtonClicked = true;
            if (this.mGoogleSignInApiClient.isConnected()) {
                getGooglePlusTokenAsync();
            } else {
                this.mGoogleSignInApiClient.connect();
            }
        }
    }

    @OnEditorAction({2131821159})
    boolean onPasswordEditorAction() {
        attemptLogin();
        return true;
    }

    protected void onPause() {
        revokeAccessAndDisconnectGoogleSignIn(null);
        if (this.mGoogleSignInApiClient.isConnected()) {
            this.mGoogleSignInApiClient.disconnect();
        }
        super.onPause();
    }

    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        if (requestCode == 1001 || requestCode == 1002 || requestCode == 1003) {
            this.mGoogleSignInIntentInProgress = false;
            if (responseCode != -1) {
                this.mGoogleSignInButtonClicked = false;
            } else if (this.mGoogleSignInButtonClicked) {
                this.mGoogleSignInApiClient.reconnect();
            }
        }
    }

    private void attemptLogin() {
        if (this.mAuthTask == null && !this.mGoogleSignInButtonClicked && !this.mGoogleSignInIntentInProgress) {
            this.mAuthTask = new UserLoginTask(this, new OnLoginListener() {
                public void loginNotSuccessful(int result) {
                    if (result == 3) {
                        LoginActivity.this.showDisableDeviceMessage();
                    } else {
                        EmailPasswordLoginFailedDialogFragment.show(LoginActivity.this.getSupportFragmentManager());
                        Analytics.hitScreen(LoginActivity.this);
                    }
                    LoginActivity.this.mAuthTask = null;
                    LoginActivity.this.showProgress(false);
                }

                public void loginSuccessful(int result) {
                    if (result == 2) {
                        Toast.makeText(LoginActivity.this.getApplicationContext(), R.string.account_added, 0).show();
                        LoginActivity.this.finish();
                    }
                }
            }, this.mOnEnoughDataToShowUI);
            String email = this.mEmailView.getText().toString();
            String password = this.mPasswordView.getText().toString();
            InputMethodManager mInputMethodManager = (InputMethodManager) getSystemService("input_method");
            mInputMethodManager.hideSoftInputFromWindow(this.mEmailView.getApplicationWindowToken(), 0);
            mInputMethodManager.hideSoftInputFromWindow(this.mPasswordView.getApplicationWindowToken(), 0);
            showProgress(true);
            UserLoginTask userLoginTask = this.mAuthTask;
            String[] strArr = new String[]{email, password};
            if (userLoginTask instanceof AsyncTask) {
                AsyncTaskInstrumentation.execute(userLoginTask, strArr);
            } else {
                userLoginTask.execute(strArr);
            }
        }
    }

    private void showProgress(boolean show) {
        int i;
        int i2 = 8;
        Log.d(TAG + ".showProgress()", "SHOW PROGRESS: Show? " + (show ? "hide form" : "show form"));
        this.mTopLayout.setVisibility(0);
        View view = this.mProgress;
        if (show) {
            i = 0;
        } else {
            i = 8;
        }
        view.setVisibility(i);
        view = this.mCenterLayout;
        if (show) {
            i = 8;
        } else {
            i = 0;
        }
        view.setVisibility(i);
        View view2 = this.mConnectWithGoogleButton;
        if (!show) {
            i2 = 0;
        }
        view2.setVisibility(i2);
    }

    public static void logOut(Context context, boolean startLoginActivity) {
        logOut(context, startLoginActivity, null);
    }

    public static void logOut(Context context, boolean startLoginActivity, Session sessionToRemove) {
        String tag = TAG + ".logOut()";
        Log.d(tag, "Logging user out.");
        Log.d(tag, "Killing session...");
        PipedriveApp.getSessionManager().releaseActiveSession();
        Log.d(tag, "Killing session. Done.");
        ContentResolver.cancelSync(null, "com.pipedrive.contentproviders.recentsprovider");
        Log.d(tag, "Recents sync cancelled.");
        ContentResolver.cancelSync(null, "com.android.contacts");
        Log.d(tag, "Contacts sync cancelled.");
        Log.d(tag, "Removing Android accounts...");
        AccountManager accountManager = AccountManager.get(context);
        for (Account account : accountManager.getAccountsByType("com.pipedrive.account")) {
            Log.d(tag, String.format("Removing account: %s", new Object[]{account}));
            if (VERSION.SDK_INT >= 22) {
                accountManager.removeAccount(account, null, null, null);
            } else {
                accountManager.removeAccount(account, null, null);
            }
        }
        Log.d(tag, "Removing Android accounts. Done.");
        if (startLoginActivity) {
            Log.d(tag, "Starting login activity...");
            SplashActivity.relaunchApplicationForLogout(context, sessionToRemove);
            Log.d(tag, "Starting login activity. Done.");
        } else if (sessionToRemove != null) {
            PipedriveApp.getSessionManager().clearDatabaseAndDisposeSession(sessionToRemove);
        }
        Log.d(tag, "Logout complete.");
    }

    private void getGooglePlusTokenAsync() {
        this.mGoogleLoginTask = new GoogleLoginTask(this, new OnLoginListener() {
            public void loginSuccessful(int result) {
                LoginActivity.this.onGoogleLoginSuccessful(result);
            }

            public void loginNotSuccessful(int loginResult) {
                LoginActivity.this.onGoogleLoginFailed(loginResult);
            }
        }, this.mOnEnoughDataToShowUI, this.mGoogleSignInApiClient);
        showProgress(true);
        GoogleLoginTask googleLoginTask = this.mGoogleLoginTask;
        String[] strArr = new String[0];
        if (googleLoginTask instanceof AsyncTask) {
            AsyncTaskInstrumentation.execute(googleLoginTask, strArr);
        } else {
            googleLoginTask.execute(strArr);
        }
    }

    private void onGoogleLoginFailed(final int loginResult) {
        this.mGoogleLoginTask = null;
        revokeAccessAndDisconnectGoogleSignIn(new ResultCallback<Status>() {
            public void onResult(@Nullable Status result) {
                if (loginResult == 3) {
                    LoginActivity.this.showDisableDeviceMessage();
                } else {
                    GoogleLoginFailedDialogFragment.show(LoginActivity.this.getSupportFragmentManager());
                    Analytics.hitScreen(LoginActivity.this);
                }
                LoginActivity.this.showProgress(false);
                LoginActivity.this.mGoogleSignInButtonClicked = false;
            }
        });
    }

    private void onGoogleLoginSuccessful(int result) {
        this.mGoogleLoginTask = null;
        if (this.mGoogleSignInApiClient.isConnected()) {
            revokeAccessAndDisconnectGoogleSignIn(null);
        }
        this.mGoogleSignInButtonClicked = false;
        if (result == 2) {
            Toast.makeText(getApplicationContext(), R.string.account_added, 0).show();
            finish();
        }
    }

    private void revokeAccessAndDisconnectGoogleSignIn(final ResultCallback<Status> resultCallback) {
        boolean noNeedToLogGoogleClientOut = this.mGoogleSignInApiClient == null || !this.mGoogleSignInApiClient.isConnected();
        if (!noNeedToLogGoogleClientOut) {
            Plus.AccountApi.clearDefaultAccount(this.mGoogleSignInApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(this.mGoogleSignInApiClient).setResultCallback(new ResultCallback<Status>() {
                public void onResult(Status result) {
                    if (LoginActivity.this.mGoogleSignInApiClient.isConnected()) {
                        LoginActivity.this.mGoogleSignInApiClient.disconnect();
                    }
                    if (resultCallback != null) {
                        resultCallback.onResult(result);
                    }
                }
            });
        } else if (resultCallback != null) {
            resultCallback.onResult(null);
        }
    }

    private void showDisableDeviceMessage() {
        DeviceDisabledDialog.show(getSupportFragmentManager());
    }
}
