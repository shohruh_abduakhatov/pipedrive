package com.pipedrive.navigator;

import android.app.Activity;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.pipedrive.R;
import com.pipedrive.navigator.buttons.NavigatorMenuItem;
import com.pipedrive.util.ImageUtil;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@MainThread
public class Navigator {
    @NonNull
    protected final Navigable mActivityView;
    @NonNull
    private final HashMap<Integer, NavigatorMenuItem> mButtons = new HashMap();
    @Nullable
    private final Toolbar mToolbar;
    @NonNull
    protected Type mViewType = Type.UNDEFINED;

    public enum Type {
        CREATE,
        UPDATE,
        UNDEFINED
    }

    public Navigator(@NonNull Navigable activityView, @Nullable Toolbar toolbar) {
        this.mActivityView = activityView;
        this.mToolbar = toolbar;
        List<? extends NavigatorMenuItem> additionalButtons = activityView.getAdditionalButtons();
        if (additionalButtons != null) {
            for (NavigatorMenuItem additionalButton : additionalButtons) {
                this.mButtons.put(Integer.valueOf(additionalButton.getMenuItemId()), additionalButton);
            }
        }
    }

    @NonNull
    protected List<Integer> menuItemsForNavigatorTypeUpdate() {
        List<Integer> menuItemIds = new LinkedList(Collections.singletonList(Integer.valueOf(16908332)));
        menuItemIds.addAll(this.mButtons.keySet());
        return menuItemIds;
    }

    @NonNull
    protected List<Integer> menuItemsForNavigatorTypeCreate() {
        return new LinkedList(Arrays.asList(new Integer[]{Integer.valueOf(16908332), Integer.valueOf(R.id.menu_save)}));
    }

    @NonNull
    protected List<Integer> menuItemsForNavigatorTypeUndefined() {
        return Collections.emptyList();
    }

    private void setHomeUpIndicatorForTypeCreate(Activity activity) {
        if (this.mToolbar != null) {
            this.mToolbar.setNavigationIcon(ImageUtil.getTintedDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_close), -1));
            this.mToolbar.setNavigationContentDescription((int) R.string.cancel);
        }
        activity.invalidateOptionsMenu();
    }

    private void setHomeUpIndicatorForTypeUpdate(Activity activity) {
        if (this.mToolbar != null) {
            this.mToolbar.setNavigationIcon(ImageUtil.getTintedDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_back), -1));
            this.mToolbar.setNavigationContentDescription((int) R.string.menu_save);
        }
        activity.invalidateOptionsMenu();
    }

    private void setHomeUpIndicatorForTypeUndefined(Activity activity) {
        if (this.mToolbar != null) {
            this.mToolbar.setNavigationIcon(0);
            this.mToolbar.setNavigationContentDescription((int) R.string._empty);
        }
        activity.invalidateOptionsMenu();
    }

    public void setViewType(@NonNull Type activityViewViewType, @NonNull Activity activity) {
        boolean viewIsUsedForCreatingData;
        boolean viewIsUsedForUpdatingData;
        boolean viewTypeIsUndefined;
        this.mViewType = activityViewViewType;
        if (this.mViewType == Type.CREATE) {
            viewIsUsedForCreatingData = true;
        } else {
            viewIsUsedForCreatingData = false;
        }
        if (this.mViewType == Type.UPDATE) {
            viewIsUsedForUpdatingData = true;
        } else {
            viewIsUsedForUpdatingData = false;
        }
        if (this.mViewType == Type.UNDEFINED) {
            viewTypeIsUndefined = true;
        } else {
            viewTypeIsUndefined = false;
        }
        if (viewIsUsedForCreatingData) {
            setHomeUpIndicatorForTypeCreate(activity);
        } else if (viewIsUsedForUpdatingData) {
            setHomeUpIndicatorForTypeUpdate(activity);
        } else if (viewTypeIsUndefined) {
            setHomeUpIndicatorForTypeUndefined(activity);
        } else {
            setHomeUpIndicatorForTypeUndefined(activity);
        }
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                onOptionsItemUpSelected();
                return true;
            case R.id.menu_save:
                onOptionsItemSaveSelected();
                return true;
            default:
                NavigatorMenuItem navigatorMenuItem = (NavigatorMenuItem) this.mButtons.get(Integer.valueOf(item.getItemId()));
                if (navigatorMenuItem == null) {
                    return false;
                }
                navigatorMenuItem.onClick();
                return true;
        }
    }

    public void onCreateOptionsMenu(@NonNull MenuInflater menuInflater, @NonNull Menu menu) {
        menuInflater.inflate(R.menu.menu_save_delete, menu);
    }

    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        boolean viewIsUsedForCreatingData;
        boolean viewIsUsedForUpdatingData;
        boolean viewTypeIsUndefined;
        if (this.mViewType == Type.CREATE) {
            viewIsUsedForCreatingData = true;
        } else {
            viewIsUsedForCreatingData = false;
        }
        if (this.mViewType == Type.UPDATE) {
            viewIsUsedForUpdatingData = true;
        } else {
            viewIsUsedForUpdatingData = false;
        }
        if (this.mViewType == Type.UNDEFINED) {
            viewTypeIsUndefined = true;
        } else {
            viewTypeIsUndefined = false;
        }
        boolean enableUpdateMenuItems = viewIsUsedForUpdatingData;
        boolean enableUndefinedMenuItems = viewTypeIsUndefined;
        if (viewIsUsedForCreatingData) {
            enableCreateMenuItems(menu);
        } else if (enableUpdateMenuItems) {
            enableUpdateMenuItems(menu);
        } else if (enableUndefinedMenuItems) {
            enableUndefinedMenuItems(menu);
        }
    }

    public void onBackPressed() {
        boolean viewIsUsedForCreatingData;
        boolean viewIsUsedForUpdatingData;
        if (this.mViewType == Type.CREATE) {
            viewIsUsedForCreatingData = true;
        } else {
            viewIsUsedForCreatingData = false;
        }
        if (this.mViewType == Type.UPDATE) {
            viewIsUsedForUpdatingData = true;
        } else {
            viewIsUsedForUpdatingData = false;
        }
        if (viewIsUsedForCreatingData) {
            cancelIfNoChangesAndSaveIfRequiredFieldsAreSetOrDiscard();
        } else if (viewIsUsedForUpdatingData) {
            onOptionsItemUpSelected();
        }
    }

    private void disableMenuItems(@NonNull Menu menu) {
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setVisible(false);
        }
    }

    private void enableCreateMenuItems(@NonNull Menu menu) {
        enableMenuItems(menu, menuItemsForNavigatorTypeCreate());
    }

    private void enableUpdateMenuItems(@NonNull Menu menu) {
        enableMenuItems(menu, menuItemsForNavigatorTypeUpdate());
    }

    private void enableUndefinedMenuItems(Menu menu) {
        enableMenuItems(menu, menuItemsForNavigatorTypeUndefined());
    }

    private void enableMenuItems(@NonNull Menu menu, @NonNull List<Integer> menuItemIdsToEnable) {
        disableMenuItems(menu);
        for (Integer intValue : menuItemIdsToEnable) {
            int navigatorMenuItemId = intValue.intValue();
            MenuItem menuItem = menu.findItem(navigatorMenuItemId);
            if (menuItem != null) {
                NavigatorMenuItem additionalButton = (NavigatorMenuItem) this.mButtons.get(Integer.valueOf(navigatorMenuItemId));
                if (additionalButton != null) {
                    menuItem.setVisible(additionalButton.isEnabled());
                } else {
                    menuItem.setVisible(true);
                }
            }
        }
    }

    private void onOptionsItemUpSelected() {
        boolean viewIsUsedForCreatingData;
        boolean viewIsUsedForUpdatingData;
        if (this.mViewType == Type.CREATE) {
            viewIsUsedForCreatingData = true;
        } else {
            viewIsUsedForCreatingData = false;
        }
        if (this.mViewType == Type.UPDATE) {
            viewIsUsedForUpdatingData = true;
        } else {
            viewIsUsedForUpdatingData = false;
        }
        if (viewIsUsedForCreatingData) {
            this.mActivityView.onCancel();
        } else if (viewIsUsedForUpdatingData) {
            cancelIfNoChangesAndSaveIfRequiredFieldsAreSetOrDiscard();
        }
    }

    private void onOptionsItemSaveSelected() {
        boolean viewIsUsedForCreatingData;
        if (this.mViewType == Type.CREATE) {
            viewIsUsedForCreatingData = true;
        } else {
            viewIsUsedForCreatingData = false;
        }
        boolean viewIsUsedForUpdatingData;
        if (this.mViewType == Type.UPDATE) {
            viewIsUsedForUpdatingData = true;
        } else {
            viewIsUsedForUpdatingData = false;
        }
        if (viewIsUsedForCreatingData || viewIsUsedForUpdatingData) {
            cancelIfNoChangesAndSaveIfRequiredFieldsAreSetOrDiscard();
        }
    }

    private void cancelIfNoChangesAndSaveIfRequiredFieldsAreSetOrDiscard() {
        if (!this.mActivityView.isChangesMadeToFieldsAfterInitialLoad()) {
            this.mActivityView.onCancel();
        } else {
            saveIfRequiredFieldsAreSetOrDiscard();
        }
    }

    private void saveIfRequiredFieldsAreSetOrDiscard() {
        if (!this.mActivityView.isAllRequiredFieldsSet()) {
            this.mActivityView.createOrUpdateDiscardedAsRequiredFieldsAreNotSet();
        } else {
            this.mActivityView.onCreateOrUpdate();
        }
    }
}
