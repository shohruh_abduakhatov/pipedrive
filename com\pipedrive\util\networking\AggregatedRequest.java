package com.pipedrive.util.networking;

import android.support.annotation.NonNull;
import com.google.gson.stream.JsonReader;
import com.pipedrive.application.Session;
import com.pipedrive.logging.Log;
import com.pipedrive.util.networking.AggregatedRequestBuilder.RequestMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

class AggregatedRequest {
    private static final String TAG = AggregatedRequest.class.getSimpleName();
    RequestMethod mRequestMethod = null;
    String mRequestPayloadJSON = null;
    String mRequestUrl = null;
    private Response mResponse = null;

    AggregatedRequest(String url, RequestMethod requestMethod, String requestPayloadJSON) {
        this.mRequestUrl = url;
        this.mRequestMethod = requestMethod;
        this.mRequestPayloadJSON = requestPayloadJSON;
    }

    static boolean requestAggregatedBlocking(@NonNull Session session, @NonNull AggregatedRequestBuilder requestResponse) {
        String tag = TAG + ".requestAggregated()";
        InputStream in = null;
        try {
            ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder(session, "aggregatedRequests");
            Log.d(tag, String.format("Requesting: %s", new Object[]{requestResponse.getAggregateRequestJSON()}));
            in = ConnectionUtil.requestPost(apiUrlBuilder);
            if (in != null) {
                JsonReader reader = new JsonReader(new InputStreamReader(in, HttpRequest.CHARSET_UTF8));
                reader.setLenient(true);
                requestResponse.processResponse(reader);
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e) {
                    }
                }
                return true;
            } else if (in == null) {
                return false;
            } else {
                try {
                    in.close();
                    return false;
                } catch (IOException e2) {
                    return false;
                }
            }
        } catch (IOException e3) {
            Log.e(e3);
            if (in == null) {
                return false;
            }
            try {
                in.close();
                return false;
            } catch (IOException e4) {
                return false;
            }
        } catch (Throwable th) {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e5) {
                }
            }
        }
    }

    protected void setResponse(Response response) {
        this.mResponse = response;
    }

    public Response getResponse() {
        return this.mResponse;
    }

    boolean hasResponse() {
        return getResponse() != null && getResponse().getResponseStatusCode() >= 200;
    }

    public String toString() {
        String str = "Request:[url:%s; method:%s;] Response:[request:%s; response:%s;]";
        Object[] objArr = new Object[4];
        objArr[0] = this.mRequestUrl;
        objArr[1] = this.mRequestMethod;
        objArr[2] = hasResponse() ? getResponse().mRequest : "NONE!";
        objArr[3] = hasResponse() ? getResponse().mResponse : "NONE!";
        return String.format(str, objArr);
    }
}
