package com.pipedrive.repository;

import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.nearby.NearbyDataSource;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.nearby.model.NearbyItem;
import com.pipedrive.nearby.model.NearbyItemType;
import com.pipedrive.util.networking.ApiUrlBuilder.UrlTemplates;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.ConnectionUtil$JsonReaderInterceptor;
import com.pipedrive.util.networking.Response;
import com.pipedrive.util.organizations.OrganizationsNetworkingUtil;
import com.pipedrive.util.persons.PersonNetworkingUtil;
import java.io.IOException;
import java.util.Map;
import rx.Completable;
import rx.Observable;
import rx.functions.Action0;
import rx.schedulers.Schedulers;

public enum NearbyRepository {
    INSTANCE;
    
    @NonNull
    private final Map<NearbyItemType, LatLngBounds> cache;

    static /* synthetic */ class AnonymousClass3 {
        static final /* synthetic */ int[] $SwitchMap$com$pipedrive$nearby$model$NearbyItemType = null;

        static {
            $SwitchMap$com$pipedrive$nearby$model$NearbyItemType = new int[NearbyItemType.values().length];
            try {
                $SwitchMap$com$pipedrive$nearby$model$NearbyItemType[NearbyItemType.DEALS.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$pipedrive$nearby$model$NearbyItemType[NearbyItemType.PERSONS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$pipedrive$nearby$model$NearbyItemType[NearbyItemType.ORGANIZATIONS.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    @NonNull
    public Observable<NearbyItem> getNearbyItems(@NonNull Session session, @NonNull NearbyItemType nearbyItemType, @NonNull LatLng currentLocation, @NonNull LatLngBounds viewport) {
        if (!viewport.equals(this.cache.get(nearbyItemType))) {
            Completable.fromAction(downloadAndStoreNearbyItems(session, nearbyItemType, currentLocation, viewport)).subscribeOn(Schedulers.io()).await();
            this.cache.put(nearbyItemType, viewport);
        }
        return new NearbyDataSource(session.getDatabase()).getNearbyItems(session, nearbyItemType, Double.valueOf(currentLocation.latitude), Double.valueOf(currentLocation.longitude), Double.valueOf(viewport.southwest.latitude), Double.valueOf(viewport.northeast.latitude), Double.valueOf(viewport.southwest.longitude), Double.valueOf(viewport.northeast.longitude));
    }

    @NonNull
    public Observable<NearbyItem> getInitialNearbyItems(@NonNull Session session, @NonNull NearbyItemType nearbyItemType, @NonNull LatLng currentLocation, @NonNull LatLngBounds viewport) {
        Completable.fromAction(downloadAndStoreNearbyItems(session, nearbyItemType, currentLocation, viewport)).subscribeOn(Schedulers.io()).await();
        this.cache.clear();
        this.cache.put(nearbyItemType, viewport);
        return new NearbyDataSource(session.getDatabase()).getInitialNearbyItems(session, nearbyItemType, Double.valueOf(currentLocation.latitude), Double.valueOf(currentLocation.longitude));
    }

    @WorkerThread
    @NonNull
    private Action0 downloadAndStoreNearbyItems(@NonNull Session session, @NonNull NearbyItemType nearbyItemType, @NonNull LatLng currentLocation, @NonNull LatLngBounds viewport) {
        final Session session2 = session;
        final NearbyItemType nearbyItemType2 = nearbyItemType;
        final LatLng latLng = currentLocation;
        final LatLngBounds latLngBounds = viewport;
        return new Action0() {
            public void call() {
                ConnectionUtil.requestGetWithAutomaticPaging(UrlTemplates.listNearbyItems(session2, nearbyItemType2, latLng, latLngBounds), NearbyRepository.this.parseAndStoreNearbyItems(nearbyItemType2));
            }
        };
    }

    @NonNull
    ConnectionUtil$JsonReaderInterceptor<Response> parseAndStoreNearbyItems(@NonNull final NearbyItemType nearbyItemType) {
        return new ConnectionUtil$JsonReaderInterceptor<Response>() {
            public Response interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull Response responseTemplate) throws IOException {
                if (jsonReader.peek() == JsonToken.BEGIN_ARRAY) {
                    jsonReader.beginArray();
                    while (jsonReader.hasNext() && jsonReader.peek() == JsonToken.BEGIN_OBJECT) {
                        NearbyRepository.this.parseAndStoreNearbyItem(session, nearbyItemType, jsonReader);
                    }
                    jsonReader.endArray();
                }
                return responseTemplate;
            }
        };
    }

    void parseAndStoreNearbyItem(@NonNull Session session, @NonNull NearbyItemType nearbyItemType, @NonNull JsonReader jsonReader) throws IOException {
        switch (AnonymousClass3.$SwitchMap$com$pipedrive$nearby$model$NearbyItemType[nearbyItemType.ordinal()]) {
            case 1:
                Deal deal = Deal.readDeal(jsonReader);
                if (deal != null) {
                    DealsNetworkingUtil.createOrUpdateDealIntoDBWithRelationsForNearby(session, deal);
                    return;
                }
                return;
            case 2:
                Person person = PersonNetworkingUtil.readContact(jsonReader);
                if (person != null) {
                    PersonNetworkingUtil.createOrUpdatePersonIntoDBWithRelations(session, person);
                    return;
                }
                return;
            case 3:
                Organization organization = OrganizationsNetworkingUtil.readOrg(jsonReader, session.getOrgAddressField(null));
                if (organization != null) {
                    OrganizationsNetworkingUtil.createOrUpdateOrganizationIntoDBWithRelations(session, organization);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
