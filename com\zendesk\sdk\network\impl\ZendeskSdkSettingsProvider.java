package com.zendesk.sdk.network.impl;

import android.support.annotation.Nullable;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.settings.MobileSettings;
import com.zendesk.sdk.model.settings.SafeMobileSettings;
import com.zendesk.sdk.network.SdkSettingsProvider;
import com.zendesk.sdk.storage.SdkSettingsStorage;
import com.zendesk.service.ZendeskCallback;
import java.util.Locale;

class ZendeskSdkSettingsProvider implements SdkSettingsProvider {
    private static final String LOG_TAG = "ZendeskSdkSettingsProvider";
    private final String applicationId;
    private final Locale deviceLocale;
    private final HelpCenterLocaleConverter localeConverter;
    private final SdkSettingsStorage sdkSettingsStorage;
    private final ZendeskSdkSettingsService settingsService;

    ZendeskSdkSettingsProvider(ZendeskSdkSettingsService settingsService, Locale deviceLocale, SdkSettingsStorage sdkSettingsStorage, String applicationId, HelpCenterLocaleConverter localeConverter) {
        this.settingsService = settingsService;
        this.deviceLocale = deviceLocale;
        this.sdkSettingsStorage = sdkSettingsStorage;
        this.applicationId = applicationId;
        this.localeConverter = localeConverter;
    }

    public void getSettings(@Nullable final ZendeskCallback<SafeMobileSettings> callback) {
        final String locale = this.localeConverter.toHelpCenterLocaleString(this.deviceLocale);
        this.settingsService.getSettings(locale, this.applicationId, new PassThroughErrorZendeskCallback<MobileSettings>(callback) {
            public void onSuccess(MobileSettings result) {
                boolean requestedLocaleIsNotAvailable;
                Logger.d(ZendeskSdkSettingsProvider.LOG_TAG, "Successfully retrieved SDK Settings", new Object[0]);
                SafeMobileSettings safeMobileSettings = new SafeMobileSettings(result);
                if (locale.equals(safeMobileSettings.getHelpCenterLocale())) {
                    requestedLocaleIsNotAvailable = false;
                } else {
                    requestedLocaleIsNotAvailable = true;
                }
                if (requestedLocaleIsNotAvailable) {
                    Logger.w(ZendeskSdkSettingsProvider.LOG_TAG, "No support for %s, Help Center is %s in your application settings", locale, Boolean.valueOf(safeMobileSettings.isHelpCenterEnabled()));
                }
                ZendeskSdkSettingsProvider.this.sdkSettingsStorage.setStoredSettings(safeMobileSettings);
                if (callback != null) {
                    Logger.d(ZendeskSdkSettingsProvider.LOG_TAG, "Calling back with successful result", new Object[0]);
                    callback.onSuccess(safeMobileSettings);
                }
            }
        });
    }
}
