package com.pipedrive.activity;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.contentproviders.FlowContentProvider;
import com.pipedrive.model.Activity;
import com.pipedrive.store.StoreActivity;
import com.pipedrive.tasks.AsyncTask;

public class CreateActivityTask extends AsyncTask<Activity, Void, Boolean> {
    private final OnTaskFinished mOnTaskFinished;

    @MainThread
    public interface OnTaskFinished {
        void onActivityCreated(boolean z);
    }

    public CreateActivityTask(@NonNull Session session, @Nullable OnTaskFinished onTaskFinished) {
        super(session);
        this.mOnTaskFinished = onTaskFinished;
    }

    protected Boolean doInBackground(Activity... params) {
        return Boolean.valueOf(new StoreActivity(getSession()).create(params[0]));
    }

    protected void onPostExecute(Boolean success) {
        if (success.booleanValue()) {
            FlowContentProvider.notifyChangesMadeInFlow(getSession().getApplicationContext());
        }
        if (this.mOnTaskFinished != null) {
            this.mOnTaskFinished.onActivityCreated(success.booleanValue());
        }
    }
}
