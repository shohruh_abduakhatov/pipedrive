package com.pipedrive.dialogs.login;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import com.pipedrive.R;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.AnalyticsEvent;
import com.pipedrive.util.PipedriveUtil;

public class EmailPasswordLoginFailedDialogFragment extends LoginFailedDialogFragment {
    private static final String TAG = "EmailPasswordLoginFailedDialogFragment";

    public static void show(@NonNull FragmentManager supportFragmentManager) {
        LoginFailedDialogFragment.showDialog(supportFragmentManager, TAG, new EmailPasswordLoginFailedDialogFragment(), Integer.valueOf(R.string.error_dialog_login_failed), Integer.valueOf(R.string.error_dialog_title), Integer.valueOf(R.string.error_dialog_button_negative_ok), Integer.valueOf(R.string.forgot_password));
    }

    protected void onPositiveButtonClicked() {
        super.onPositiveButtonClicked();
        Analytics.sendEvent(getContext(), AnalyticsEvent.OK_ON_PASSWORD_EMAIL_LOGIN_FAILURE_MODAL);
    }

    protected void onNegativeButtonClicked() {
        super.onNegativeButtonClicked();
        Analytics.sendEvent(getContext(), AnalyticsEvent.FORGOT_PASSWORD_ON_PASSWORD_EMAIL_LOGIN_FAILURE_MODAL);
        PipedriveUtil.openWebPage(getContext(), R.string.login_layout_reset_password_link);
    }
}
