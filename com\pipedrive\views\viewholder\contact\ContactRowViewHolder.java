package com.pipedrive.views.viewholder.contact;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.TextView;
import butterknife.BindView;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.util.StringUtils;
import com.pipedrive.views.viewholder.ViewHolder;

abstract class ContactRowViewHolder implements ViewHolder {
    @BindView(2131820922)
    TextView mName;
    @BindView(2131820992)
    TextView mOrganization;

    ContactRowViewHolder() {
    }

    void fill(@NonNull SearchConstraint searchConstraint, @Nullable String contactName, @Nullable String contactOrganizationName) {
        boolean contactHasOrgName;
        int i = 0;
        StringUtils.highlightString(this.mName, contactName, searchConstraint);
        if (contactOrganizationName != null) {
            contactHasOrgName = true;
        } else {
            contactHasOrgName = false;
        }
        TextView textView = this.mOrganization;
        if (!contactHasOrgName) {
            i = 8;
        }
        textView.setVisibility(i);
        this.mOrganization.setText(contactOrganizationName);
        StringUtils.highlightString(this.mOrganization, contactOrganizationName, searchConstraint);
    }
}
