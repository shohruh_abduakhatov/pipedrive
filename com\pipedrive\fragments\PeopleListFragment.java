package com.pipedrive.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import com.pipedrive.PersonDetailActivity;
import com.pipedrive.R;
import com.pipedrive.adapter.PeopleListAdapter;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.call.CallSummaryListener;
import com.pipedrive.call.CallSummaryListener.CallSummaryRegistration;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.views.viewholder.contact.PersonRowViewHolder.OnCallButtonClickListener;

public class PeopleListFragment extends BaseListFragment implements OnCallButtonClickListener {
    private PeopleListAdapter mAdapter;
    private Session mSession;

    public static PeopleListFragment newInstance() {
        return new PeopleListFragment();
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mSession = PipedriveApp.getActiveSession();
    }

    public void onStart() {
        super.onStart();
        Cursor cursor = new PersonsDataSource(this.mSession.getDatabase()).getSearchCursor(this.mSearchConstraint);
        if (this.mAdapter == null) {
            this.mAdapter = new PeopleListAdapter(getActivity(), cursor, this.mSession);
            getListView().addFooterView(ViewUtil.getFooterForListWithFloatingButton(getActivity()), null, false);
            getListView().setAdapter(this.mAdapter);
            getListView().setFastScrollEnabled(true);
            setEmptyText(R.string.lbl_no_results_found);
        } else {
            this.mAdapter.changeCursor(cursor);
        }
        this.mAdapter.setOnCallButtonClickListener(this);
    }

    public void listFilter(@NonNull SearchConstraint searchConstraint) {
        this.mSearchConstraint = searchConstraint;
        this.mAdapter.setSearchConstraint(searchConstraint);
    }

    public void onItemClick(@NonNull AdapterView<?> adapterView, @NonNull View view, int position, long id) {
        PersonDetailActivity.startActivity(getActivity(), Long.valueOf(((Cursor) this.mAdapter.getItem(position)).getLong(0)));
    }

    public void onCallButtonClicked(@NonNull String phoneNumberToDial, @Nullable Long personSqlId) {
        Session session = PipedriveApp.getActiveSession();
        FragmentActivity nonApplicationContext = getActivity();
        if (isResumed() && nonApplicationContext != null) {
            CallSummaryListener.makeCallWithCallSummaryRegistration(session, nonApplicationContext, new CallSummaryRegistration(phoneNumberToDial, personSqlId));
        }
    }
}
