package com.pipedrive.settings;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.preference.TwoStatePreference;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.BuildConfig;
import com.pipedrive.LoginActivity;
import com.pipedrive.R;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.ScreensMapper;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.CompaniesDataSource;
import com.pipedrive.datetime.DateFormatHelper;
import com.pipedrive.fragments.LanguageSelectorDialogFragment;
import com.pipedrive.fragments.LocaleHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.model.settings.Company;
import com.pipedrive.notification.ActivityReminderOption;
import com.pipedrive.notification.ActivityReminderSelectorDialogFragment;
import com.pipedrive.notification.AllDayActivityReminderOption;
import com.pipedrive.notification.AllDayActivityReminderSelectorDialogFragment;
import com.pipedrive.notification.UINotificationManager;
import com.pipedrive.sync.SyncManager;
import com.pipedrive.sync.SyncManager$StateOfDownloadAll;
import com.pipedrive.util.devices.MobileDevices;
import com.pipedrive.util.networking.ConnectionUtil;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.requests.RequestActivity;
import com.zendesk.sdk.support.SupportActivity.Builder;
import java.util.Date;
import java.util.List;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Actions;

public class SettingsFragment extends PreferenceFragment {
    private static final String TAG = SettingsFragment.class.getSimpleName();
    @Nullable
    private Preference manualRefresh;
    @Nullable
    private Subscription syncManagerDownloadAllProcess;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Session session = PipedriveApp.getActiveSession();
        if (session != null) {
            setupUi(session);
            this.syncManagerDownloadAllProcess = SyncManager.getInstance().getStateOfDownloadAll(session).subscribeOn(AndroidSchedulers.mainThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<SyncManager$StateOfDownloadAll>() {
                public void call(SyncManager$StateOfDownloadAll stateOfDownloadAll) {
                    if (SettingsFragment.this.manualRefresh != null) {
                        SettingsFragment.this.manualRefresh.setEnabled(stateOfDownloadAll == SyncManager$StateOfDownloadAll.IDLE);
                    }
                }
            });
        }
    }

    public void onDestroyView() {
        if (this.syncManagerDownloadAllProcess != null) {
            this.syncManagerDownloadAllProcess.unsubscribe();
        }
        super.onDestroyView();
    }

    private void setupHelpAndFeedbackPreference() {
        findPreference(getString(R.string.preference_key_help_and_feedback)).setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Analytics.hitScreen(ScreensMapper.SCREEN_NAME_HELP, SettingsFragment.this.getActivity());
                return true;
            }
        });
    }

    private void setupTipsAndTricksPreference(@Nullable final Session session) {
        if (session != null) {
            setIdentity(session);
            findPreference(getString(R.string.preference_key_tips_and_tricks)).setOnPreferenceClickListener(new OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    new Builder().withArticlesForSectionIds(201493829).withContactConfiguration(PipedriveApp.getZendeskConfiguration(session)).show(SettingsFragment.this.getActivity());
                    return true;
                }
            });
            return;
        }
        Log.e(new Throwable("Cannot init Zendesk. No active session!"));
    }

    private void setupMyFeedbackPreference(Session session) {
        if (session != null) {
            setIdentity(session);
            findPreference(getString(R.string.preference_key_my_feedback)).setOnPreferenceClickListener(new OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    SettingsFragment.this.startActivity(new Intent(SettingsFragment.this.getActivity(), RequestActivity.class));
                    return true;
                }
            });
            return;
        }
        Log.e(new Throwable("Cannot init Zendesk. No active session!"));
    }

    private void setupAboutPreference(Session session) {
        Context ctx = session.getApplicationContext();
        Preference aboutPreference = findPreference(ctx.getString(R.string.preference_key_about));
        aboutPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Analytics.hitScreen(ScreensMapper.SCREEN_NAME_ABOUT, preference.getContext());
                return false;
            }
        });
        try {
            ApplicationInfo applicationInfo = getActivity().getPackageManager().getApplicationInfo(getActivity().getPackageName(), 128);
            aboutPreference.setTitle(ctx.getString(R.string.pref_about_title, new Object[]{getActivity().getPackageManager().getApplicationLabel(applicationInfo)}));
        } catch (NameNotFoundException e) {
        }
        aboutPreference.setSummary(ctx.getString(R.string.pref_about_summary, new Object[]{BuildConfig.APP_VERSION_NAME}));
    }

    private void setupCompanySwitchPreferences(Session session) {
        Preference preference = findPreference(getString(R.string.preference_key_company_list));
        if (ConnectionUtil.isNotConnected(session.getApplicationContext())) {
            preference.setEnabled(false);
            return;
        }
        preference.setSummary("");
        List<Company> availableCompanies = new CompaniesDataSource(session).findAll();
        for (int i = 0; i < availableCompanies.size(); i++) {
            Company company = (Company) availableCompanies.get(i);
            if (((long) company.getPipedriveId()) == session.getSelectedCompanyID()) {
                Log.d(TAG, String.format("Currently logged in company ID: %s", new Object[]{Long.valueOf(session.getSelectedCompanyID())}));
                Log.d(TAG, String.format("Summary text for company settings: %s", new Object[]{company.getName()}));
                preference.setSummary(summaryText);
            }
        }
        preference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                return true;
            }
        });
    }

    private void setupManualRefreshPreference(@NonNull Session session) {
        this.manualRefresh = findPreference(session.getApplicationContext().getResources().getString(R.string.preference_key_manual_refresh));
        if (this.manualRefresh != null) {
            boolean lastRefreshTimeIsSet;
            String lastRefreshTimeAsString = "";
            long lastRefreshTimeInMillis = session.getLastGetRequestTimeInMillis().longValue();
            if (lastRefreshTimeInMillis != Long.MIN_VALUE) {
                lastRefreshTimeIsSet = true;
            } else {
                lastRefreshTimeIsSet = false;
            }
            if (lastRefreshTimeIsSet) {
                lastRefreshTimeAsString = DateFormatHelper.getTimePassedInHumanReadableForm(session.getApplicationContext(), R.string.pref_manual_refresh_subtitle, new Date(lastRefreshTimeInMillis));
            }
            this.manualRefresh.setSummary(lastRefreshTimeAsString);
            if (AccountManager.get(session.getApplicationContext()).getAccountsByType("com.pipedrive.account").length <= 0 || !ConnectionUtil.isConnected(session.getApplicationContext())) {
                this.manualRefresh.setEnabled(false);
            } else {
                this.manualRefresh.setOnPreferenceClickListener(new OnPreferenceClickListener() {
                    public boolean onPreferenceClick(Preference preference) {
                        SynchronizeDataActivity.startActivity(preference.getContext());
                        return false;
                    }
                });
            }
        }
    }

    private void setupLanguagePreference(Session session) {
        Preference preference = findPreference(session.getApplicationContext().getString(R.string.preference_key_language));
        preference.setSummary(LocaleHelper.INSTANCE.getLanguageTitle());
        preference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                SettingsFragment.this.onLanguagePreferenceClicked();
                return true;
            }
        });
    }

    private void onLanguagePreferenceClicked() {
        new LanguageSelectorDialogFragment().show(getFragmentManager(), LanguageSelectorDialogFragment.TAG);
    }

    private void setupActivityReminderPreference(@NonNull final Session session) {
        ActivityReminderOption activityReminderOption = session.getSharedSession().getActivityReminderOption();
        Preference preference = findPreference(session.getApplicationContext().getString(R.string.preference_key_activity_reminder));
        preference.setSummary(activityReminderOption.getTitle(getActivity()));
        preference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                SettingsFragment.this.onActivityReminderPreferenceClicked(session);
                return true;
            }
        });
    }

    private void onActivityReminderPreferenceClicked(@NonNull Session session) {
        ActivityReminderSelectorDialogFragment.show(session, getFragmentManager());
    }

    private void setupAllDayActivityReminderPreference(@NonNull final Session session) {
        AllDayActivityReminderOption allDayActivityReminderOption = session.getSharedSession().getAllDayActivityReminderOption();
        Preference preference = findPreference(session.getApplicationContext().getString(R.string.preference_key_all_day_activity_reminder));
        preference.setSummary(allDayActivityReminderOption.getTitle(getActivity()));
        preference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                SettingsFragment.this.onAllDayActivityReminderPreferenceClicked(session);
                return true;
            }
        });
    }

    private void onAllDayActivityReminderPreferenceClicked(@NonNull Session session) {
        AllDayActivityReminderSelectorDialogFragment.show(session, getFragmentManager());
    }

    private void setupSignUpForBetaTestingPreference(Session session) {
        findPreference(session.getApplicationContext().getString(R.string.preference_key_signup_for_beta_testing)).setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                SettingsFragment.this.onSignUpForBetaTestingPreferenceClicked();
                return true;
            }
        });
    }

    private void onSignUpForBetaTestingPreferenceClicked() {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/apps/testing/com.pipedrive"));
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private void setupLogoutPreference(final Session session) {
        Preference logout = findPreference(session.getApplicationContext().getApplicationContext().getString(R.string.preference_key_log_out));
        String userProfileName = session.getUserSettingsName(null);
        if (userProfileName != null) {
            logout.setSummary(userProfileName);
        }
        final Activity parentActivity = getActivity();
        if (parentActivity != null) {
            logout.setOnPreferenceClickListener(new OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    String positiveButtonText = parentActivity.getResources().getString(R.string.pref_logout_dialog_answer_yes);
                    new AlertDialog.Builder(parentActivity, R.style.Theme.Pipedrive.Dialog.Alert).setTitle(parentActivity.getResources().getString(R.string.pref_logout_dialog_message)).setPositiveButton(positiveButtonText, new OnClickListener() {
                        public void onClick(@NonNull DialogInterface dialog, int which) {
                            new MobileDevices(session).logoutDevice().subscribe(Actions.empty(), Actions.empty());
                            UINotificationManager.getInstance().cancelAllActivityAlarmsAndNotifications(session);
                            LoginActivity.logOut(parentActivity, true);
                            parentActivity.finish();
                            dialog.cancel();
                        }
                    }).setNegativeButton(parentActivity.getResources().getString(R.string.pref_logout_dialog_answer_no), null).show();
                    return true;
                }
            });
        }
    }

    private void setupAnalyticsPreference(final Session session) {
        TwoStatePreference analyticsSwitch = (TwoStatePreference) findPreference(session.getApplicationContext().getApplicationContext().getString(R.string.preference_key_analytics));
        analyticsSwitch.setChecked(session.getSharedSession().isAnalyticsEnabled());
        analyticsSwitch.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean analyticsEnabled = false;
                if (newValue instanceof Boolean) {
                    analyticsEnabled = ((Boolean) newValue).booleanValue();
                }
                session.getSharedSession().enableAnalytics(analyticsEnabled);
                return true;
            }
        });
    }

    private void setupOfflineSupportPreference(final Session session) {
        TwoStatePreference offlineSupportSwitch = (TwoStatePreference) findPreference(session.getApplicationContext().getApplicationContext().getString(R.string.preference_key_offlinesupport));
        if (offlineSupportSwitch != null) {
            Preference aboutPreference = findPreference(session.getApplicationContext().getString(R.string.preference_key_about));
            if (aboutPreference instanceof PreferenceScreen) {
                ((PreferenceScreen) aboutPreference).removePreference(offlineSupportSwitch);
            } else {
                offlineSupportSwitch.setEnabled(false);
            }
            offlineSupportSwitch.setChecked(session.getSharedSession().isOfflineSupportEnabled());
            offlineSupportSwitch.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    boolean offlineEnabled = false;
                    if (newValue instanceof Boolean) {
                        offlineEnabled = ((Boolean) newValue).booleanValue();
                    }
                    session.getSharedSession().enableOfflineSupport(offlineEnabled);
                    return true;
                }
            });
        }
    }

    private void setupCallLoggingPreference(final Session session) {
        TwoStatePreference callLoggingSwitch = (TwoStatePreference) findPreference(session.getApplicationContext().getApplicationContext().getString(R.string.preference_key_call_logging));
        callLoggingSwitch.setChecked(session.getSharedSession().isCallLoggingEnabled());
        callLoggingSwitch.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean callLoggingEnabled = true;
                if (newValue instanceof Boolean) {
                    callLoggingEnabled = ((Boolean) newValue).booleanValue();
                }
                session.getSharedSession().enableCallLogging(callLoggingEnabled);
                return true;
            }
        });
    }

    private void setIdentity(@NonNull Session session) {
        String userProfileName = session.getUserSettingsName("");
        String userSettingsEmail = session.getUserSettingsEmail("");
        ZendeskConfig.INSTANCE.setIdentity(new AnonymousIdentity.Builder().withNameIdentifier(userProfileName).withEmailIdentifier(userSettingsEmail).withExternalIdentifier(String.valueOf(session.getAuthenticatedUserID())).build());
    }

    void setupUi(@NonNull Session session) {
        setupCompanySwitchPreferences(session);
        setupManualRefreshPreference(session);
        setupAboutPreference(session);
        setupLanguagePreference(session);
        setupLogoutPreference(session);
        setupAnalyticsPreference(session);
        setupOfflineSupportPreference(session);
        setupHelpAndFeedbackPreference();
        setupCallLoggingPreference(session);
        setupTipsAndTricksPreference(session);
        setupMyFeedbackPreference(session);
        setupSignUpForBetaTestingPreference(session);
        setupActivityReminderPreference(session);
        setupAllDayActivityReminderPreference(session);
    }
}
