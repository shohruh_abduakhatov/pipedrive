package com.pipedrive.call;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.pipedrive.BaseActivity;
import com.pipedrive.R;
import com.pipedrive.activity.ActivityDetailActivity;
import com.pipedrive.datetime.LocaleHelper;
import com.pipedrive.datetime.PipedriveDate;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.dialogs.ConfirmDiscardChangesDialogFragment;
import com.pipedrive.dialogs.ConfirmDiscardChangesDialogFragment.ConfirmDiscardChangesPositiveBtnListener;
import com.pipedrive.model.Activity.DateTimeInfoWithReturn;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Person;
import com.pipedrive.util.ImageUtil;
import com.pipedrive.util.StringUtils;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.util.snackbar.SnackBarManager;
import com.pipedrive.views.association.AssociationView.AssociationListener;
import com.pipedrive.views.association.DealAssociationView;
import java.util.concurrent.TimeUnit;

public class CallSummaryActivity extends BaseActivity implements CallSummaryView, ConfirmDiscardChangesPositiveBtnListener {
    private static final String ARG_ACTIVITY_SQL_ID = "activity_sql_id";
    private static final String ARG_CALL_DURATION_IN_MILLIS = "call_duration_in_millis";
    private static final String ARG_CALL_START_TIME_IN_MILLIS = "call_start_time_in_millis";
    private static final String ARG_CAN_GO_BACK = "can_go_back";
    private static final String ARG_PERSON_SQL_ID = "person_sql_id";
    private static final String ARG_RELATED_DEAL_SQL_ID = "related_deal_sql_id";
    private static final int LINK_DEAL_REQUEST_CODE = 1;
    private static final int REQUEST_CODE_IS_IGNORED = 0;
    @BindView(2131820725)
    CheckBox mCheckboxDone;
    @BindView(2131820726)
    TextView mDateAndDurationTextView;
    @BindView(2131820729)
    DealAssociationView mDealAssociationView;
    @BindView(2131820727)
    TextView mNameTextView;
    @BindView(2131820730)
    EditText mNoteEditText;
    @BindView(2131820728)
    TextView mOrganizationTextView;
    @NonNull
    private CallSummaryPresenter mPresenter;
    @BindView(2131820731)
    SwitchCompat mScheduleNextActivity;
    @BindView(2131820650)
    TextView mTitle;
    @BindView(2131820700)
    Toolbar mToolbar;

    public static void startActivityForPerson(@NonNull Activity context, @Nullable Long personSqlId, @Nullable Long relatedDealSqlId, @Nullable Long callDurationInMillis, @Nullable Long callStartTimeInMillis, boolean canGoBack) {
        Intent intent = new Intent(context, CallSummaryActivity.class).putExtra(ARG_CAN_GO_BACK, canGoBack);
        if (personSqlId != null) {
            intent.putExtra(ARG_PERSON_SQL_ID, personSqlId.longValue());
        }
        if (relatedDealSqlId != null) {
            intent.putExtra(ARG_RELATED_DEAL_SQL_ID, relatedDealSqlId.longValue());
        }
        if (callStartTimeInMillis != null) {
            intent.putExtra(ARG_CALL_START_TIME_IN_MILLIS, callStartTimeInMillis.longValue());
        }
        if (callDurationInMillis != null) {
            intent.putExtra(ARG_CALL_DURATION_IN_MILLIS, callDurationInMillis.longValue());
        }
        context.startActivityForResult(intent, 0);
    }

    public static void startForExistingActivity(@NonNull Activity context, @Nullable Long activitySqlId, @Nullable Long callDurationInMillis, @Nullable Long callStartTimeInMillis, boolean canGoBack) {
        Intent intent = new Intent(context, CallSummaryActivity.class).putExtra(ARG_CAN_GO_BACK, canGoBack);
        if (activitySqlId != null) {
            intent.putExtra(ARG_ACTIVITY_SQL_ID, activitySqlId.longValue());
        }
        if (callStartTimeInMillis != null) {
            intent.putExtra(ARG_CALL_START_TIME_IN_MILLIS, callStartTimeInMillis.longValue());
        }
        if (callDurationInMillis != null) {
            intent.putExtra(ARG_CALL_DURATION_IN_MILLIS, callDurationInMillis.longValue());
        }
        context.startActivityForResult(intent, 0);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_call_summary);
        ButterKnife.bind((Activity) this);
        setupActionBar();
        setupScheduleNextActivity();
        this.mPresenter = new CallSummaryPresenterImpl(this.mSession, savedInstanceState);
    }

    private void setupActionBar() {
        setSupportActionBar(this.mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            if (canGoBack()) {
                this.mToolbar.setNavigationIcon((int) R.drawable.icon_back);
                return;
            }
            this.mToolbar.setNavigationIcon(ImageUtil.getTintedDrawable(ContextCompat.getDrawable(this, R.drawable.icon_close), -1));
        }
    }

    private boolean canGoBack() {
        return getIntent().getBooleanExtra(ARG_CAN_GO_BACK, true);
    }

    private void setupScheduleNextActivity() {
        ((ViewGroup) this.mScheduleNextActivity.getParent()).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                CallSummaryActivity.this.mScheduleNextActivity.performClick();
            }
        });
    }

    public void onResume() {
        super.onResume();
        this.mPresenter.bindView(this);
        fillViewOrFinish(getIntent().getExtras());
    }

    private void fillViewOrFinish(@Nullable Bundle extras) {
        if (extras == null) {
            finish();
            return;
        }
        Long relatedDealSqlId;
        long activitySqlId = extras.getLong(ARG_ACTIVITY_SQL_ID);
        if (extras.containsKey(ARG_RELATED_DEAL_SQL_ID)) {
            relatedDealSqlId = Long.valueOf(extras.getLong(ARG_RELATED_DEAL_SQL_ID));
        } else {
            relatedDealSqlId = null;
        }
        long startTimeInMillis = extras.getLong(ARG_CALL_START_TIME_IN_MILLIS);
        long durationInMillis = extras.getLong(ARG_CALL_DURATION_IN_MILLIS);
        if (activitySqlId != 0) {
            this.mPresenter.requestOrRestoreExistingActivity(Long.valueOf(activitySqlId), startTimeInMillis, durationInMillis);
            return;
        }
        long personSqlId = extras.getLong(ARG_PERSON_SQL_ID);
        if (personSqlId == 0) {
            finish();
        } else {
            this.mPresenter.requestOrRestoreNewCallActivityForPerson(Long.valueOf(personSqlId), relatedDealSqlId, startTimeInMillis, durationInMillis);
        }
    }

    private void fillCallDateAndDurationInfo(@Nullable com.pipedrive.model.Activity activity) {
        if (activity == null) {
            this.mDateAndDurationTextView.setVisibility(4);
            return;
        }
        String durationFormattedWithMinimumLengthOfOneMinute = getDurationFormattedWithMinimumLengthOfOneMinute(activity);
        String formattedDateAndTimeInfo = getFormattedDateAndTimeInfo(activity);
        if (durationFormattedWithMinimumLengthOfOneMinute == null) {
            durationFormattedWithMinimumLengthOfOneMinute = "";
        }
        this.mDateAndDurationTextView.setText(formattedDateAndTimeInfo.concat(durationFormattedWithMinimumLengthOfOneMinute));
        this.mDateAndDurationTextView.setVisibility(0);
    }

    @NonNull
    private String getFormattedDateAndTimeInfo(@NonNull com.pipedrive.model.Activity activity) {
        String formattedActivityDateAndTimeInfo = (String) activity.request(new DateTimeInfoWithReturn<String>() {
            public String activityHasStartDateTime(@NonNull PipedriveDateTime dateTime, @NonNull com.pipedrive.model.Activity inActivity) {
                return LocaleHelper.getLocaleBasedDateTimeStringInCurrentTimeZone(CallSummaryActivity.this.getSession(), dateTime);
            }

            public String activityHasStartDate(@NonNull PipedriveDate date, @NonNull com.pipedrive.model.Activity inActivity) {
                return LocaleHelper.getLocaleBasedDateString(CallSummaryActivity.this.getSession(), date);
            }

            public String activityIsCorruptedToReturnDateTimeInfo(@NonNull com.pipedrive.model.Activity corruptedActivity) {
                return null;
            }
        });
        String emptyString = "";
        return !StringUtils.isTrimmedAndEmpty(formattedActivityDateAndTimeInfo) ? formattedActivityDateAndTimeInfo + getResources().getString(R.string.subtitle_separator) : "";
    }

    @Nullable
    private String getDurationFormattedWithMinimumLengthOfOneMinute(@NonNull com.pipedrive.model.Activity activity) {
        Integer durationInMinutesWithMinimumLengthOfOneMinute;
        if (activity.getDuration() == null) {
            durationInMinutesWithMinimumLengthOfOneMinute = null;
        } else {
            durationInMinutesWithMinimumLengthOfOneMinute = Integer.valueOf(Long.valueOf(TimeUnit.SECONDS.toMinutes(activity.getDuration().getRepresentationInUnixTime())).intValue());
        }
        if (durationInMinutesWithMinimumLengthOfOneMinute == null) {
            return null;
        }
        return String.format(getResources().getQuantityString(R.plurals.minutes, durationInMinutesWithMinimumLengthOfOneMinute.intValue()), new Object[]{durationInMinutesWithMinimumLengthOfOneMinute});
    }

    private void fillActivityData(@Nullable com.pipedrive.model.Activity activity) {
        if (activity == null) {
            this.mNameTextView.setVisibility(8);
            this.mOrganizationTextView.setVisibility(8);
            return;
        }
        setupDoneView(activity);
        if (activity.getSubject() != null) {
            this.mTitle.setText(activity.getSubject());
        }
        Person person = activity.getPerson();
        if (person != null) {
            this.mNameTextView.setText(person.getName());
            this.mNameTextView.setVisibility(0);
        } else {
            this.mNameTextView.setVisibility(8);
        }
        if (activity.getOrganization() != null) {
            this.mOrganizationTextView.setText(activity.getOrganization().getName());
            this.mOrganizationTextView.setVisibility(0);
        } else {
            this.mOrganizationTextView.setVisibility(8);
        }
        if (activity.getDeal() != null) {
            this.mDealAssociationView.setAssociation(activity.getDeal(), Integer.valueOf(1));
        }
    }

    @OnClick({2131820732})
    void onDontLogButtonClicked() {
        if (isThereAnyChangesMadeInView()) {
            showDialog();
        } else {
            finishCallSummaryActivity();
        }
    }

    private void setupDoneView(@NonNull com.pipedrive.model.Activity activity) {
        this.mCheckboxDone.setChecked(activity.isDone());
        this.mCheckboxDone.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                CallSummaryActivity.this.mPresenter.setDone(isChecked);
            }
        });
    }

    public void onRequestCallActivity(@Nullable com.pipedrive.model.Activity activity) {
        if (activity == null) {
            finish();
        } else {
            setUpActivity(activity);
        }
    }

    private void setUpActivity(@Nullable com.pipedrive.model.Activity activity) {
        if (activity != null) {
            setupDoneView(activity);
            setupAssociateDealViewActions();
            onAssociationsUpdated(activity);
            fillActivityData(activity);
            fillCallDateAndDurationInfo(activity);
        }
    }

    public void onCallActivityCreated(boolean success) {
        processActivityCrudResponse(success);
        if (success) {
            SnackBarManager.INSTANCE.setDelayedSnackBar(R.string.activity_added);
        }
    }

    public void onAssociationsUpdated(@NonNull com.pipedrive.model.Activity activity) {
        this.mDealAssociationView.setAssociation(activity.getDeal(), Integer.valueOf(1), activity.getPerson(), activity.getOrganization());
    }

    private void setupAssociateDealViewActions() {
        this.mDealAssociationView.setAssociationListener(new AssociationListener<Deal>() {
            public void onAssociationChanged(@Nullable Deal association) {
                CallSummaryActivity.this.mPresenter.associateDeal(association);
            }
        });
    }

    public void onCallActivityUpdated(boolean success) {
        processActivityCrudResponse(success);
        if (success) {
            SnackBarManager.INSTANCE.setDelayedSnackBar(R.string.activity_updated);
        }
    }

    private void processActivityCrudResponse(boolean success) {
        if (success) {
            if (this.mScheduleNextActivity.isChecked() && this.mPresenter.getActivity() != null) {
                ActivityDetailActivity.startActivityForFollowUpActivity(this, this.mPresenter.getActivity());
            }
            finishCallSummaryActivity();
            return;
        }
        ViewUtil.showErrorToast(this, R.string.error_activity_save_failed);
    }

    protected void onPause() {
        super.onPause();
        this.mPresenter.unbindView();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_call_summary, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                if (canGoBack() || !isThereAnyChangesMadeInView()) {
                    onBackPressed();
                    return true;
                }
                showDialog();
                return true;
            case R.id.menu_save:
                onSaveClicked();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showDialog() {
        String tag = "confirmDiscardChangesDialog";
        ConfirmDiscardChangesDialogFragment.newInstance(R.string.you_have_already_filled_in_some_information_, R.string.cancel, R.string.dont_log).show(getSupportFragmentManager(), "confirmDiscardChangesDialog");
    }

    private void onSaveClicked() {
        if (this.mPresenter.getActivity() != null) {
            CharSequence note = this.mNoteEditText.getText();
            if (!StringUtils.isTrimmedAndEmpty(note)) {
                this.mPresenter.appendOrAddNote(note.toString().replaceAll("\n", "<br/>"));
            }
            this.mPresenter.changeCallActivity();
        }
    }

    public void onDialogPositiveBtnClicked() {
        finishCallSummaryActivity();
    }

    private void finishCallSummaryActivity() {
        setResult(-1);
        finish();
    }

    private boolean isThereAnyChangesMadeInView() {
        return this.mPresenter.isModified() || !StringUtils.isTrimmedAndEmpty(this.mNoteEditText.getText()) || this.mScheduleNextActivity.isChecked();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1 && requestCode == 1) {
            long dealSqlId = data.getLongExtra("DEAL_SQL_ID", 0);
            if (dealSqlId != 0) {
                this.mPresenter.associateDealBySqlId(Long.valueOf(dealSqlId));
            }
        }
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.mPresenter.onSaveInstanceState(outState);
    }
}
