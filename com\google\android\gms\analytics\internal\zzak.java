package com.google.android.gms.analytics.internal;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.RequiresPermission;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzxr;

public final class zzak {
    private static Boolean az;
    private final zza fL;
    private final Context mContext;
    private final Handler mHandler = new Handler();

    public interface zza {
        boolean callServiceStopSelfResult(int i);

        Context getContext();
    }

    public zzak(zza com_google_android_gms_analytics_internal_zzak_zza) {
        this.mContext = com_google_android_gms_analytics_internal_zzak_zza.getContext();
        zzaa.zzy(this.mContext);
        this.fL = com_google_android_gms_analytics_internal_zzak_zza;
    }

    public static boolean zzau(Context context) {
        zzaa.zzy(context);
        if (az != null) {
            return az.booleanValue();
        }
        boolean zzr = zzao.zzr(context, "com.google.android.gms.analytics.AnalyticsService");
        az = Boolean.valueOf(zzr);
        return zzr;
    }

    private void zzyz() {
        try {
            synchronized (zzaj.zzaox) {
                zzxr com_google_android_gms_internal_zzxr = zzaj.ax;
                if (com_google_android_gms_internal_zzxr != null && com_google_android_gms_internal_zzxr.isHeld()) {
                    com_google_android_gms_internal_zzxr.release();
                }
            }
        } catch (SecurityException e) {
        }
    }

    @RequiresPermission(allOf = {"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
    public void onCreate() {
        zzf zzaw = zzf.zzaw(this.mContext);
        zzaf zzaca = zzaw.zzaca();
        zzaw.zzacb();
        zzaca.zzes("Local AnalyticsService is starting up");
    }

    @RequiresPermission(allOf = {"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
    public void onDestroy() {
        zzf zzaw = zzf.zzaw(this.mContext);
        zzaf zzaca = zzaw.zzaca();
        zzaw.zzacb();
        zzaca.zzes("Local AnalyticsService is shutting down");
    }

    @RequiresPermission(allOf = {"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
    public int onStartCommand(Intent intent, int i, final int i2) {
        zzyz();
        final zzf zzaw = zzf.zzaw(this.mContext);
        final zzaf zzaca = zzaw.zzaca();
        if (intent == null) {
            zzaca.zzev("AnalyticsService started with null intent");
        } else {
            String action = intent.getAction();
            zzaw.zzacb();
            zzaca.zza("Local AnalyticsService called. startId, action", Integer.valueOf(i2), action);
            if ("com.google.android.gms.analytics.ANALYTICS_DISPATCH".equals(action)) {
                zzaw.zzzg().zza(new zzw(this) {
                    final /* synthetic */ zzak fN;

                    public void zzf(Throwable th) {
                        this.fN.mHandler.post(new Runnable(this) {
                            final /* synthetic */ AnonymousClass1 fO;

                            {
                                this.fO = r1;
                            }

                            public void run() {
                                if (this.fO.fN.fL.callServiceStopSelfResult(i2)) {
                                    zzaw.zzacb();
                                    zzaca.zzes("Local AnalyticsService processed last dispatch request");
                                }
                            }
                        });
                    }
                });
            }
        }
        return 2;
    }
}
