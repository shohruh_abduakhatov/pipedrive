package com.zendesk.belvedere;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;
import java.io.File;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

class BelvedereStorage {
    private static final String ATTACHMENT_NAME = "attachment_%s";
    private static final String CAMERA_DATETIME_STRING_FORMAT = "yyyyMMddHHmmssSSS";
    private static final String CAMERA_IMAGE_DIR = "camera";
    private static final String CAMERA_IMG_NAME = "camera_image_%s";
    private static final String CAMERA_IMG_SUFFIX = ".jpg";
    private static final String GALLERY_IMAGE_DIR = "gallery";
    private static final String LOG_TAG = "BelvedereStorage";
    private static final String REQUEST_IMAGE_DIR = "request";
    private BelvedereConfig belvedereConfig;
    private BelvedereLogger log;

    @Retention(RetentionPolicy.SOURCE)
    public @interface IntentPermissions {
    }

    BelvedereStorage(BelvedereConfig belvedereConfig) {
        this.belvedereConfig = belvedereConfig;
        this.log = belvedereConfig.getBelvedereLogger();
    }

    void grantPermissionsForUri(@NonNull Context context, @NonNull Intent intent, @NonNull Uri uri, int permission) {
        for (ResolveInfo resolveInfo : context.getPackageManager().queryIntentActivities(intent, 65536)) {
            context.grantUriPermission(resolveInfo.activityInfo.packageName, uri, permission);
        }
    }

    void revokePermissionsFromUri(@NonNull Context context, @NonNull Uri uri, int permission) {
        context.revokeUriPermission(uri, permission);
    }

    @Nullable
    Uri getFileProviderUri(@NonNull Context context, @NonNull File file) {
        Uri uri = null;
        try {
            uri = FileProvider.getUriForFile(context, getFileProviderAuthority(context), file);
        } catch (IllegalArgumentException e) {
            this.log.e(LOG_TAG, String.format(Locale.US, "The selected file can't be shared %s", new Object[]{file.toString()}));
        } catch (NullPointerException e2) {
            String msg = String.format(Locale.US, "=====================\nFileProvider failed to retrieve file uri. There might be an issue with the FileProvider \nPlease make sure that manifest-merger is working, and that you have defined the applicationId (package name) in the build.gradle\nManifest merger: http://tools.android.com/tech-docs/new-build-system/user-guide/manifest-merger\nIf your are not able to use gradle or the manifest merger, please add the following to your AndroidManifest.xml:\n        <provider\n            android:name=\"com.zendesk.belvedere.BelvedereFileProvider\"\n            android:authorities=\"${applicationId}${belvedereFileProviderAuthoritySuffix}\"\n            android:exported=\"false\"\n            android:grantUriPermissions=\"true\">\n            <meta-data\n                android:name=\"android.support.FILE_PROVIDER_PATHS\"\n                android:resource=\"@xml/belvedere_attachment_storage\" />\n        </provider>\n=====================", new Object[]{authority});
            Log.e(LOG_TAG, msg, e2);
            this.log.e(LOG_TAG, msg, e2);
        }
        return uri;
    }

    @NonNull
    String getFileProviderAuthority(@NonNull Context context) {
        String suffix = context.getString(R.string.belvedere_sdk_fpa_suffix);
        return String.format(Locale.US, "%s%s", new Object[]{context.getPackageName(), suffix});
    }

    @Nullable
    File getFileForCamera(@NonNull Context context) {
        File cacheDir = getAttachmentDir(context, CAMERA_IMAGE_DIR);
        if (cacheDir == null) {
            this.log.w(LOG_TAG, "Error creating cache directory");
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(CAMERA_DATETIME_STRING_FORMAT, Locale.US);
        return createTempFile(String.format(Locale.US, CAMERA_IMG_NAME, new Object[]{sdf.format(new Date(System.currentTimeMillis()))}), CAMERA_IMG_SUFFIX, cacheDir);
    }

    @Nullable
    File getTempFileForGalleryImage(@NonNull Context context, @NonNull Uri uri) {
        File cacheDir = getAttachmentDir(context, GALLERY_IMAGE_DIR);
        if (cacheDir == null) {
            this.log.w(LOG_TAG, "Error creating cache directory");
            return null;
        }
        String fileName = getFileNameFromUri(context, uri);
        String suffix = null;
        if (TextUtils.isEmpty(fileName)) {
            SimpleDateFormat sdf = new SimpleDateFormat(CAMERA_DATETIME_STRING_FORMAT, Locale.US);
            fileName = String.format(Locale.US, ATTACHMENT_NAME, new Object[]{sdf.format(new Date(System.currentTimeMillis()))});
            suffix = getExtension(context, uri);
        }
        return createTempFile(fileName, suffix, cacheDir);
    }

    @Nullable
    File getTempFileForRequestAttachment(Context context, String fileName) {
        File cacheDir = getAttachmentDir(context, REQUEST_IMAGE_DIR);
        if (cacheDir != null) {
            return createTempFile(fileName, null, cacheDir);
        }
        this.log.w(LOG_TAG, "Error creating cache directory");
        return null;
    }

    void clearStorage(@NonNull Context context) {
        File rootDir = new File(getRootDir(context) + File.separator + this.belvedereConfig.getDirectoryName());
        if (rootDir.isDirectory()) {
            clearDirectory(rootDir);
        }
    }

    private void clearDirectory(@NonNull File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                clearDirectory(child);
            }
        }
        fileOrDirectory.delete();
    }

    @NonNull
    private File createTempFile(@NonNull String fileName, @Nullable String suffix, @NonNull File dir) {
        StringBuilder append = new StringBuilder().append(fileName);
        if (TextUtils.isEmpty(suffix)) {
            suffix = "";
        }
        return new File(dir, append.append(suffix).toString());
    }

    @Nullable
    private File getAttachmentDir(@NonNull Context context, @Nullable String subDirectory) {
        String subDirectoryString;
        if (TextUtils.isEmpty(subDirectory)) {
            subDirectoryString = "";
        } else {
            subDirectoryString = subDirectory + File.separator;
        }
        File dir = new File(getRootDir(context) + File.separator + this.belvedereConfig.getDirectoryName() + File.separator + subDirectoryString);
        if (!dir.isDirectory()) {
            dir.mkdirs();
        }
        return dir.isDirectory() ? dir : null;
    }

    @NonNull
    private String getRootDir(@NonNull Context context) {
        return context.getCacheDir().getAbsolutePath();
    }

    @NonNull
    private String getExtension(@NonNull Context context, @NonNull Uri uri) {
        String ext = MimeTypeMap.getSingleton().getExtensionFromMimeType(context.getContentResolver().getType(uri));
        Locale locale = Locale.US;
        String str = ".%s";
        Object[] objArr = new Object[1];
        if (TextUtils.isEmpty(ext)) {
            ext = "tmp";
        }
        objArr[0] = ext;
        return String.format(locale, str, objArr);
    }

    @NonNull
    private String getFileNameFromUri(@NonNull Context context, @NonNull Uri uri) {
        Cursor cursor = context.getContentResolver().query(uri, new String[]{"_display_name"}, null, null, null);
        String path = "";
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    path = cursor.getString(0);
                }
                cursor.close();
            } catch (Throwable th) {
                cursor.close();
            }
        }
        return path;
    }
}
