package com.zendesk.sdk.requests;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.instrumentation.AsyncTaskInstrumentation;
import com.newrelic.agent.android.tracing.Trace;
import com.squareup.picasso.Picasso$LoadedFrom;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Target;
import com.zendesk.belvedere.Belvedere;
import com.zendesk.belvedere.BelvedereResult;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.request.Attachment;
import com.zendesk.sdk.network.impl.ZendeskPicassoProvider;
import com.zendesk.service.ErrorResponse;
import com.zendesk.service.ErrorResponseAdapter;
import com.zendesk.service.ZendeskCallback;
import com.zendesk.util.StringUtils;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;

enum ImageLoader {
    INSTANCE;
    
    private static final String LOG_TAG = null;
    private Target mTarget;

    class DownloadImageToExternalStorage extends AsyncTask<TaskData, Void, Result<Uri>> implements TraceFieldInterface {
        public Trace _nr_trace;
        private final Belvedere mBelvedere;
        private final ZendeskCallback<Uri> mCallback;

        public void _nr_setTrace(Trace trace) {
            try {
                this._nr_trace = trace;
            } catch (Exception e) {
            }
        }

        public DownloadImageToExternalStorage(ZendeskCallback<Uri> callback, Belvedere belvedere) {
            this.mCallback = callback;
            this.mBelvedere = belvedere;
        }

        protected Result<Uri> doInBackground(@NonNull TaskData... params) {
            String attachmentContentType;
            Long attachmentId;
            Result<Uri> result;
            IOException e;
            Throwable th;
            Bitmap bitmap = params[0].bitmap;
            Attachment attachment = params[0].attachment;
            if (attachment == null || StringUtils.isEmpty(attachment.getContentType())) {
                attachmentContentType = "";
            } else {
                attachmentContentType = attachment.getContentType();
            }
            if (attachment == null) {
                attachmentId = null;
            } else {
                attachmentId = attachment.getId();
            }
            if (!attachmentContentType.startsWith("image/")) {
                return new Result(new ErrorResponseAdapter("attachment is not an image"));
            }
            if (attachmentId == null) {
                return new Result(new ErrorResponseAdapter("attachment does not have an id"));
            }
            BelvedereResult file = this.mBelvedere.getFileRepresentation(String.format(Locale.US, "%s-%s", new Object[]{attachmentId, attachment.getFileName()}));
            if (file == null) {
                return new Result(new ErrorResponseAdapter("Error creating tmp file"));
            }
            FileOutputStream fileOutputStream = null;
            try {
                FileOutputStream fileOutputStream2 = new FileOutputStream(file.getFile());
                try {
                    bitmap.compress(CompressFormat.PNG, 42, fileOutputStream2);
                    fileOutputStream2.flush();
                    result = new Result(file.getUri());
                    if (fileOutputStream2 == null) {
                        return result;
                    }
                    try {
                        fileOutputStream2.close();
                        return result;
                    } catch (IOException e2) {
                        Logger.e(ImageLoader.LOG_TAG, "Couldn't close fileoutputstream", e2, new Object[0]);
                        return result;
                    }
                } catch (IOException e3) {
                    e2 = e3;
                    fileOutputStream = fileOutputStream2;
                    try {
                        result = new Result(new ErrorResponseAdapter(e2.getMessage()));
                        if (fileOutputStream != null) {
                            return result;
                        }
                        try {
                            fileOutputStream.close();
                            return result;
                        } catch (IOException e22) {
                            Logger.e(ImageLoader.LOG_TAG, "Couldn't close fileoutputstream", e22, new Object[0]);
                            return result;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        if (fileOutputStream != null) {
                            try {
                                fileOutputStream.close();
                            } catch (IOException e222) {
                                Logger.e(ImageLoader.LOG_TAG, "Couldn't close fileoutputstream", e222, new Object[0]);
                            }
                        }
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    fileOutputStream = fileOutputStream2;
                    if (fileOutputStream != null) {
                        fileOutputStream.close();
                    }
                    throw th;
                }
            } catch (IOException e4) {
                e222 = e4;
                result = new Result(new ErrorResponseAdapter(e222.getMessage()));
                if (fileOutputStream != null) {
                    return result;
                }
                fileOutputStream.close();
                return result;
            }
        }

        protected void onPostExecute(@NonNull Result<Uri> result) {
            if (this.mCallback == null) {
                return;
            }
            if (result.isError()) {
                this.mCallback.onError(result.getErrorResponse());
            } else {
                this.mCallback.onSuccess(result.getResult());
            }
        }
    }

    class Result<E> {
        private ErrorResponse mErrorResponse;
        private boolean mIsError;
        private E mResult;

        Result(E result) {
            this.mResult = result;
            this.mIsError = false;
        }

        Result(ErrorResponse errorResponse) {
            this.mErrorResponse = errorResponse;
            this.mIsError = true;
        }

        public E getResult() {
            if (this.mIsError) {
                return null;
            }
            return this.mResult;
        }

        ErrorResponse getErrorResponse() {
            if (this.mIsError) {
                return this.mErrorResponse;
            }
            return null;
        }

        boolean isError() {
            return this.mIsError;
        }
    }

    class TaskData {
        private Attachment attachment;
        private Bitmap bitmap;

        TaskData(Attachment attachment, Bitmap bitmap) {
            this.attachment = attachment;
            this.bitmap = bitmap;
        }
    }

    static {
        LOG_TAG = ImageLoader.class.getSimpleName();
    }

    synchronized void loadAndShowImage(final Attachment attachment, Context context, final ZendeskCallback<Uri> callback, final Belvedere belvedere) {
        BelvedereResult localAttachment = belvedere.getFileRepresentation(String.format(Locale.US, "%s-%s", new Object[]{attachment.getId(), attachment.getFileName()}));
        if (localAttachment == null || !localAttachment.getFile().isFile() || localAttachment.getFile().length() <= 0 || callback == null) {
            RequestCreator requestCreator = ZendeskPicassoProvider.getInstance(context).load(attachment.getContentUrl());
            this.mTarget = new Target() {
                public void onBitmapLoaded(Bitmap bitmap, Picasso$LoadedFrom from) {
                    DownloadImageToExternalStorage downloadImageToExternalStorage = new DownloadImageToExternalStorage(callback, belvedere);
                    TaskData[] taskDataArr = new TaskData[]{new TaskData(attachment, bitmap)};
                    if (downloadImageToExternalStorage instanceof AsyncTask) {
                        AsyncTaskInstrumentation.execute(downloadImageToExternalStorage, taskDataArr);
                    } else {
                        downloadImageToExternalStorage.execute(taskDataArr);
                    }
                }

                public void onBitmapFailed(Drawable errorDrawable) {
                    if (callback != null) {
                        callback.onError(new ErrorResponseAdapter("Error loading attachment"));
                    }
                }

                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }
            };
            requestCreator.into(this.mTarget);
        } else {
            callback.onSuccess(localAttachment.getUri());
        }
    }
}
