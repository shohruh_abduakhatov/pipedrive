package com.pipedrive.nearby;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.cards.Cards;
import com.pipedrive.nearby.cards.cards.EmptyCard;
import com.pipedrive.nearby.filter.views.FilterButton;
import com.pipedrive.nearby.filter.views.NearbyFilter;
import com.pipedrive.nearby.location.LocationChangesProvider;
import com.pipedrive.nearby.location.LocationManager;
import com.pipedrive.nearby.location.LocationServicesConnectionManager;
import com.pipedrive.nearby.map.PDMap;
import com.pipedrive.nearby.searchbutton.SearchButton;
import com.pipedrive.nearby.toolbar.NearbyToolbar;
import com.pipedrive.presenter.Presenter;

class NearbyPresenter extends Presenter<NearbyActivity> {
    @NonNull
    private final Cards cards;
    @NonNull
    private final EmptyCard emptyCard;
    @NonNull
    private final FilterButton filterButton;
    @NonNull
    private final LocationServicesConnectionManager locationServicesConnectionManager;
    @NonNull
    private final NearbyFilter nearbyFilter;
    @NonNull
    private final NearbyToolbar nearbyToolbar;
    @NonNull
    private final SearchButton searchButton;

    NearbyPresenter(@Nullable Bundle savedInstanceState, @NonNull Session session, @NonNull NearbyToolbar nearbyToolbar, @NonNull PDMap pdMap, @NonNull Cards cards, @NonNull SearchButton searchButton, @NonNull LocationServicesConnectionManager locationServicesConnectionManager, @NonNull EmptyCard emptyCardView, @NonNull NearbyFilter nearbyFilter, @NonNull FilterButton filterButton) {
        super(session);
        this.nearbyToolbar = nearbyToolbar;
        this.cards = cards;
        this.searchButton = searchButton;
        this.locationServicesConnectionManager = locationServicesConnectionManager;
        this.nearbyToolbar.setup(session);
        this.searchButton.setup(savedInstanceState, session, pdMap, pdMap);
        this.emptyCard = emptyCardView;
        this.nearbyFilter = nearbyFilter;
        this.filterButton = filterButton;
        nearbyFilter.setup(session, filterButton, nearbyToolbar);
        filterButton.setup(session, nearbyToolbar);
        LocationManager locationManager = LocationChangesProvider.getInstance();
        pdMap.setup(session, locationManager, locationManager, nearbyToolbar, searchButton, cards);
        this.cards.setup(savedInstanceState, session, pdMap, nearbyToolbar, pdMap, locationManager);
    }

    public synchronized void bindView(@NonNull NearbyActivity view) {
        super.bindView(view);
        this.locationServicesConnectionManager.connect();
        this.nearbyToolbar.bind();
        this.searchButton.bind();
        this.cards.bind();
        this.emptyCard.bind();
        this.nearbyFilter.bind();
        this.filterButton.bind();
    }

    public synchronized void unbindView() {
        this.nearbyToolbar.releaseResources();
        this.cards.releaseResources();
        this.searchButton.releaseResources();
        this.locationServicesConnectionManager.disconnect();
        this.emptyCard.releaseResources();
        this.nearbyFilter.releaseResources();
        this.filterButton.releaseResources();
        super.unbindView();
    }

    void saveState(@NonNull Bundle outState) {
        this.searchButton.saveState(outState);
        this.cards.saveState(outState);
    }
}
