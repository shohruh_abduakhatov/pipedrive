package com.pipedrive;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.pipedrive.activity.ActivityDetailActivity;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.customfields.views.CustomFieldListView;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.flow.views.FlowView.OnItemClickListener;
import com.pipedrive.fragments.OrganizationDetailFragment;
import com.pipedrive.fragments.OrganizationDetailFragment.SetupProgressBarCallback;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.note.NoteUpdateActivity;
import com.pipedrive.store.StoreActivity;
import com.pipedrive.tasks.AsyncTask;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished.TaskResult;
import com.pipedrive.tasks.flow.DownloadFlowTask.DownloadOrganizationFlowTask;
import com.pipedrive.util.PipedriveUtil;
import java.util.concurrent.Executors;

public class OrganizationDetailActivity extends BaseActivity implements SetupProgressBarCallback, OnItemClickListener, CustomFieldListView.OnItemClickListener {
    private static final String KEY_ORGANIZATION_SQL_ID = "com.pipedrive.ORGANIZATION_SQL_ID";
    public static final int REQUEST_CODE_EDIT_ORGANIZATION = 0;
    public static final int RESULT_DELETED = 2;
    @Nullable
    private AsyncTask mDownloadFlowTask;
    @Nullable
    private OrganizationDetailFragment mOrganizationDetailFragment;

    public static void startActivity(@NonNull Context context, @NonNull Long organizationSqlId) {
        if (PipedriveApp.getSessionManager().hasActiveSession()) {
            context.startActivity(new Intent(context, OrganizationDetailActivity.class).putExtra(KEY_ORGANIZATION_SQL_ID, organizationSqlId));
        }
    }

    @Deprecated
    public static void startActivity(Context context, Uri uri) {
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mOrganizationDetailFragment = (OrganizationDetailFragment) getSupportFragmentManager().findFragmentByTag(OrganizationDetailFragment.TAG);
        if (this.mOrganizationDetailFragment == null) {
            this.mOrganizationDetailFragment = OrganizationDetailFragment.newInstance(getOrganizationSqlId());
            getSupportFragmentManager().beginTransaction().disallowAddToBackStack().replace(16908290, this.mOrganizationDetailFragment, OrganizationDetailFragment.TAG).commit();
        }
    }

    public void onBackPressed() {
        if (this.mOrganizationDetailFragment == null || !this.mOrganizationDetailFragment.consumeBackPress()) {
            super.onBackPressed();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && resultCode == 2) {
            finish();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void showProgressIndicator() {
        showProgress();
    }

    public void hideProgressIndicator() {
        hideProgress();
    }

    public void setupProgressBarWithToolbar(Toolbar toolbar) {
        setupProgressBar(toolbar);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @NonNull
    private Long getOrganizationSqlId() {
        return (Long) getIntent().getSerializableExtra(KEY_ORGANIZATION_SQL_ID);
    }

    public void onResume() {
        super.onResume();
        startFlowDownloadIfNotStartedAlready();
    }

    private void startFlowDownloadIfNotStartedAlready() {
        if (!AsyncTask.isExecuting(getSession(), DownloadOrganizationFlowTask.class) && ((Organization) new OrganizationsDataSource(getSession().getDatabase()).findBySqlId(getOrganizationSqlId().longValue())) != null) {
            this.mDownloadFlowTask = new DownloadOrganizationFlowTask(getSession(), new OnTaskFinished<Integer>() {
                public void taskFinished(Session session, TaskResult taskResult, Integer... params) {
                    if (OrganizationDetailActivity.this.mOrganizationDetailFragment != null) {
                        OrganizationDetailActivity.this.mOrganizationDetailFragment.refreshFlowView(OrganizationDetailActivity.this.getSession());
                    }
                    OrganizationDetailActivity.this.hideProgressIndicator();
                }
            }).executeOnExecutor(Executors.newSingleThreadExecutor(), new Integer[]{Integer.valueOf(organization.getPipedriveId())});
            showProgressIndicator();
        }
    }

    protected void onPause() {
        stopFlowDownloadIfStarted();
        super.onPause();
    }

    private void stopFlowDownloadIfStarted() {
        boolean downloadFlowTaskIsExecuting;
        if (this.mDownloadFlowTask == null || !this.mDownloadFlowTask.isExecuting() || this.mDownloadFlowTask.isCancelled()) {
            downloadFlowTaskIsExecuting = false;
        } else {
            downloadFlowTaskIsExecuting = true;
        }
        if (downloadFlowTaskIsExecuting) {
            this.mDownloadFlowTask.cancel(false);
        }
    }

    public void onActivityClicked(long activitySqlId) {
        ActivityDetailActivity.startActivityForActivity((Activity) this, activitySqlId);
    }

    public void onNoteClicked(long noteSqlId) {
        NoteUpdateActivity.updateNote((Context) this, noteSqlId);
    }

    public void onEmailClicked(long emailSqlId) {
        Organization organization = (Organization) new OrganizationsDataSource(getSession().getDatabase()).findBySqlId(getOrganizationSqlId().longValue());
        if (organization != null) {
            EmailDetailViewActivity.startActivity(this, emailSqlId, organization.getDropBoxAddress());
        }
    }

    public void onFileClicked(long fileSqlId) {
        FileOpenActivity.startActivityForFlowFile(this, fileSqlId);
    }

    public void onActivityDoneChanged(long activitySqlId, boolean done) {
        changeActivityDoneState(activitySqlId, done);
    }

    private void changeActivityDoneState(long activitySqlId, boolean done) {
        com.pipedrive.model.Activity activity = (com.pipedrive.model.Activity) new ActivitiesDataSource(getSession()).findBySqlId(activitySqlId);
        if (activity != null) {
            activity.setDone(done);
            new StoreActivity(getSession()).update(activity);
            if (this.mOrganizationDetailFragment != null) {
                this.mOrganizationDetailFragment.refreshFlowView(getSession());
            }
        }
    }

    public void onNewActivityClicked() {
        ActivityDetailActivity.startActivityForOrganization((Context) this, getOrganizationSqlId().longValue());
    }

    public void onCustomFieldClicked(@NonNull CustomField customField) {
        CustomFieldEditActivity.startActivity(this, customField, getOrganizationSqlId().longValue());
    }

    public void onCustomFieldLongClicked(@NonNull String value) {
        PipedriveUtil.copyTextToClipboard(value, this);
    }

    public void onPhoneClicked(@NonNull String number, boolean ignored) {
        PipedriveUtil.call(this, number);
    }

    public void onPersonClicked(@NonNull Person person) {
        PersonDetailActivity.startActivity((Context) this, Long.valueOf(person.getSqlId()));
    }

    public void onOrganizationClicked(@NonNull Organization organization) {
        startActivity((Context) this, Long.valueOf(organization.getSqlId()));
    }

    public void onAddressClicked(@NonNull Uri uri) {
        startActivity(new Intent("android.intent.action.VIEW", uri));
    }
}
