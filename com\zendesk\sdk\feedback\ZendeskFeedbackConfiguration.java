package com.zendesk.sdk.feedback;

import java.io.Serializable;
import java.util.List;

public interface ZendeskFeedbackConfiguration extends Serializable {
    String getAdditionalInfo();

    String getRequestSubject();

    List<String> getTags();
}
