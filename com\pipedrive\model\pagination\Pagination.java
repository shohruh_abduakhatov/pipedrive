package com.pipedrive.model.pagination;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pagination {
    public static final int INT_UNDEFINED = Integer.MIN_VALUE;
    public static final String JSON_PARAM_OF_PAGINATION = "pagination";
    public static final String JSON_PARAM_PAGINATION_LIMIT = "limit";
    public static final String JSON_PARAM_PAGINATION_START = "start";
    @SerializedName("limit")
    @Expose
    public int limit = Integer.MIN_VALUE;
    @SerializedName("more_items_in_collection")
    @Expose
    public boolean moreItemsInCollection = false;
    @SerializedName("next_start")
    @Expose
    public int nextStart = Integer.MIN_VALUE;
    @SerializedName("start")
    @Expose
    public int start = Integer.MIN_VALUE;

    public String toString() {
        return super.toString() + String.format(" Pagination: [start:%s][next_start:%s][limit:%s][moreItemsInCollection:%s]", new Object[]{Integer.valueOf(this.start), Integer.valueOf(this.nextStart), Integer.valueOf(this.limit), Boolean.valueOf(this.moreItemsInCollection)});
    }
}
