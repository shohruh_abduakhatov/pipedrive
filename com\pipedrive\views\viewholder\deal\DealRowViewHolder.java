package com.pipedrive.views.viewholder.deal;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datetime.DateFormatHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.model.Deal;
import com.pipedrive.model.DealStatus;
import com.pipedrive.util.CompatUtil;
import com.pipedrive.util.formatter.MonetaryFormatter;
import com.pipedrive.util.time.TimeManager;
import com.pipedrive.views.ColoredImageView;
import com.pipedrive.views.viewholder.ViewHolder;
import java.text.ParseException;
import java.util.Date;

class DealRowViewHolder implements ViewHolder {
    @Nullable
    @BindView(2131820649)
    ColoredImageView mIcon;
    @BindView(2131821068)
    View mRootView;
    @BindView(2131821069)
    ImageView mStatus;
    @BindView(2131820954)
    TextView mSubTitle;
    @BindView(2131820650)
    TextView mTitle;

    DealRowViewHolder() {
    }

    public void fill(@NonNull Session session, @NonNull Context context, @NonNull Deal deal) {
        boolean dealOrgNameExist;
        boolean dealContactNameExist;
        this.mTitle.setText(deal.getTitle());
        this.mSubTitle.setText(new MonetaryFormatter(session).formatDealValue(deal));
        if (deal.getOrganization() == null || deal.getOrganization().getName() == null) {
            dealOrgNameExist = false;
        } else {
            dealOrgNameExist = true;
        }
        String separator = context.getResources().getString(R.string.subtitle_separator);
        if (dealOrgNameExist) {
            this.mSubTitle.append(separator.concat(deal.getOrganization().getName()));
        }
        if (deal.getPerson() == null || deal.getPerson().getName() == null) {
            dealContactNameExist = false;
        } else {
            dealContactNameExist = true;
        }
        if (dealContactNameExist) {
            this.mSubTitle.append(separator.concat(deal.getPerson().getName()));
        }
        this.mStatus.setImageResource(getDealNextActivityIcon(deal));
        CompatUtil.setTextAppearance(this.mTitle, getTitleTextAppearance(deal));
        CompatUtil.setTextAppearance(this.mSubTitle, getSubTitleTextAppearance(deal));
        this.mRootView.setBackgroundColor(getBackgroundColorResId(context, deal));
    }

    private int getSubTitleTextAppearance(@NonNull Deal deal) {
        return (deal.getStatus() == DealStatus.WON || deal.isDealRotten()) ? R.style.TextAppearance.Body.Dark.Secondary : R.style.TextAppearance.Body;
    }

    private int getTitleTextAppearance(@NonNull Deal deal) {
        return (deal.getStatus() == DealStatus.WON || deal.isDealRotten()) ? R.style.TextAppearance.Title.Dark : R.style.TextAppearance.Title;
    }

    private int getBackgroundColorResId(@NonNull Context context, @NonNull Deal deal) {
        int colorResId = R.color.transparent;
        if (deal.getStatus() == DealStatus.WON) {
            colorResId = R.color.green;
        } else if (deal.getStatus() == DealStatus.LOST) {
            colorResId = R.color.frost;
        } else if (deal.isDealRotten()) {
            colorResId = R.color.red;
        }
        return ContextCompat.getColor(context, colorResId);
    }

    public int getLayoutResourceId() {
        return R.layout.row_deal;
    }

    @DrawableRes
    private static int getDealNextActivityIcon(@NonNull Deal deal) {
        int iconId = R.drawable.warning;
        if (deal.getUndoneActivitiesCount() == 0) {
            return R.drawable.warning;
        }
        Date nextActivityDate = null;
        if (!TextUtils.isEmpty(deal.getNextActivityDateStr())) {
            try {
                nextActivityDate = DateFormatHelper.fullDateFormat3UTC().parse(deal.getNextActivityDateStr() + " " + (TextUtils.isEmpty(deal.getNextActivityTime()) ? "12:00" : deal.getNextActivityTime()));
            } catch (ParseException e) {
                Log.e(new Throwable("Error parsing next activity date", e));
            }
        }
        if (nextActivityDate != null) {
            if (DateFormatHelper.isToday(nextActivityDate, TextUtils.isEmpty(deal.getNextActivityTime()))) {
                iconId = R.drawable.today;
            } else if (nextActivityDate.compareTo(new Date(TimeManager.getInstance().currentTimeMillis().longValue())) < 1) {
                iconId = R.drawable.overdue;
            } else {
                iconId = R.drawable.future;
            }
        }
        return iconId;
    }
}
