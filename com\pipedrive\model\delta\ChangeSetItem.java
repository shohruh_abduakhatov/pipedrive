package com.pipedrive.model.delta;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Property;
import com.newrelic.agent.android.instrumentation.JSONObjectInstrumentation;
import com.pipedrive.logging.Log;
import org.json.JSONException;
import org.json.JSONObject;

public class ChangeSetItem<HOST, DATA> {
    private static final String PARAM_HOST_TYPE = "hostType";
    private static final String PARAM_JSON_PARAM_NAME = "jsonParamName";
    private static final String PARAM_NAME = "name";
    private static final String PARAM_VALUE_TYPE = "valueType";
    private final Class<HOST> mHostType;
    private final String mJsonParamName;
    private final Property<HOST, DATA> mProperty;

    public static ChangeSetItem of(Class<?> hostType, Class<?> valueType, String name, String jsonParamName) {
        return new ChangeSetItem(Property.of(hostType, valueType, name), jsonParamName, hostType);
    }

    public static ChangeSetItem fromString(String jsonString) {
        ChangeSetItem changeSetItem = null;
        try {
            JSONObject jsonObject = JSONObjectInstrumentation.init(jsonString);
            changeSetItem = of(Class.forName(jsonObject.optString(PARAM_HOST_TYPE)), Class.forName(jsonObject.optString(PARAM_VALUE_TYPE)), jsonObject.optString("name"), jsonObject.optString(PARAM_JSON_PARAM_NAME));
        } catch (JSONException e) {
            Log.e(e);
        } catch (ClassNotFoundException e2) {
            Log.e(e2);
        }
        return changeSetItem;
    }

    public ChangeSetItem(@NonNull Property<HOST, DATA> property, String jsonParamName, Class<HOST> hostType) {
        this.mProperty = property;
        if (TextUtils.isEmpty(jsonParamName)) {
            jsonParamName = property.getName();
        }
        this.mJsonParamName = jsonParamName;
        this.mHostType = hostType;
    }

    public String getJsonParamName() {
        return this.mJsonParamName;
    }

    @Nullable
    public Object get(@Nullable HOST item) {
        return item != null ? this.mProperty.get(item) : null;
    }

    public String toString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(PARAM_HOST_TYPE, this.mHostType.getName());
            jsonObject.put(PARAM_JSON_PARAM_NAME, this.mJsonParamName);
            jsonObject.put("name", this.mProperty.getName());
            jsonObject.put(PARAM_VALUE_TYPE, this.mProperty.getType().getName());
        } catch (JSONException e) {
            Log.e(e);
        }
        return !(jsonObject instanceof JSONObject) ? jsonObject.toString() : JSONObjectInstrumentation.toString(jsonObject);
    }

    public Class<DATA> getType() {
        return this.mProperty.getType();
    }
}
