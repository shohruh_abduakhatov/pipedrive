package rx;

import java.util.concurrent.Callable;
import rx.subscriptions.BooleanSubscription;

class Completable$9 implements Completable$OnSubscribe {
    final /* synthetic */ Callable val$callable;

    Completable$9(Callable callable) {
        this.val$callable = callable;
    }

    public void call(CompletableSubscriber s) {
        BooleanSubscription bs = new BooleanSubscription();
        s.onSubscribe(bs);
        try {
            this.val$callable.call();
            if (!bs.isUnsubscribed()) {
                s.onCompleted();
            }
        } catch (Throwable e) {
            if (!bs.isUnsubscribed()) {
                s.onError(e);
            }
        }
    }
}
