package com.pipedrive;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.fragments.BaseListFragment;
import com.pipedrive.fragments.OrganizationListFragment;
import com.pipedrive.fragments.PeopleListFragment;
import com.pipedrive.organization.edit.OrganizationCreateActivity;
import com.pipedrive.person.edit.PersonCreateActivity;
import com.pipedrive.views.CustomSearchView;
import com.pipedrive.views.CustomSearchView.OnConstraintChangeListener;
import com.viewpagerindicator.TabPageIndicator;
import java.util.List;

public class ContactsActivity extends NavigationActivity {

    private class ContactsPagerAdapter extends FragmentStatePagerAdapter {
        public ContactsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public BaseListFragment getItem(int position) {
            if (position == 0) {
                BaseListFragment peopleListFragmentFromFragmentManager = findFragmentInstanceFromFragmentManager(PeopleListFragment.class);
                if (peopleListFragmentFromFragmentManager == null) {
                    return PeopleListFragment.newInstance();
                }
                return peopleListFragmentFromFragmentManager;
            }
            BaseListFragment organizationListFragmentFromFragmentManager = findFragmentInstanceFromFragmentManager(OrganizationListFragment.class);
            if (organizationListFragmentFromFragmentManager == null) {
                organizationListFragmentFromFragmentManager = OrganizationListFragment.newInstance();
            }
            return organizationListFragmentFromFragmentManager;
        }

        @Nullable
        private BaseListFragment findFragmentInstanceFromFragmentManager(@NonNull Class fragmentClassType) {
            List<Fragment> fragments = ContactsActivity.this.getSupportFragmentManager().getFragments();
            if (fragments == null) {
                return null;
            }
            for (Fragment fragment : fragments) {
                if (fragmentClassType.isInstance(fragment) && (fragment instanceof BaseListFragment)) {
                    return (BaseListFragment) fragment;
                }
            }
            return null;
        }

        public int getCount() {
            return 2;
        }

        public CharSequence getPageTitle(int position) {
            return ContactsActivity.this.getString(position == 0 ? R.string.label_people : R.string.navigation_menu_item_organizations).toUpperCase();
        }

        void updateSearch(@NonNull SearchConstraint searchConstraint) {
            for (int i = 0; i < getCount(); i++) {
                BaseListFragment baseListFragment = getItem(i);
                if (baseListFragment != null) {
                    baseListFragment.listFilter(searchConstraint);
                }
            }
        }
    }

    public static void startActivity(Activity activity) {
        ContextCompat.startActivity(activity, new Intent(activity, ContactsActivity.class), null);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_contacts);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final ContactsPagerAdapter pagerAdapter = new ContactsPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        ((TabPageIndicator) findViewById(R.id.tab_page_indicator)).setViewPager(viewPager);
        findViewById(R.id.add).setOnClickListener(new OnClickListener() {
            public void onClick(@NonNull View v) {
                if (viewPager.getCurrentItem() == 0) {
                    ContactsActivity.this.onAddPersonClicked();
                } else {
                    ContactsActivity.this.onAddOrganizationClicked();
                }
            }
        });
        ((CustomSearchView) findViewById(R.id.search)).setOnConstraintChangeListener(new OnConstraintChangeListener() {
            public void onConstraintChanged(@NonNull final SearchConstraint searchConstraint) {
                ContactsActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        pagerAdapter.updateSearch(searchConstraint);
                    }
                });
            }
        });
    }

    private void onAddOrganizationClicked() {
        OrganizationCreateActivity.startActivity(this);
    }

    private void onAddPersonClicked() {
        PersonCreateActivity.startActivity(this);
    }
}
