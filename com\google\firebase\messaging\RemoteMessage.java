package com.google.firebase.messaging;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.firebase.iid.FirebaseInstanceId;
import java.util.Map;
import java.util.Map.Entry;

public final class RemoteMessage extends AbstractSafeParcelable {
    public static final Creator<RemoteMessage> CREATOR = new zzc();
    private Map<String, String> be;
    private Notification bkZ;
    Bundle hf;
    final int mVersionCode;

    public static class Builder {
        private final Map<String, String> be = new ArrayMap();
        private final Bundle hf = new Bundle();

        public Builder(String str) {
            if (TextUtils.isEmpty(str)) {
                String str2 = "Invalid to: ";
                String valueOf = String.valueOf(str);
                throw new IllegalArgumentException(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            }
            this.hf.putString("google.to", str);
        }

        public Builder addData(String str, String str2) {
            this.be.put(str, str2);
            return this;
        }

        public RemoteMessage build() {
            Bundle bundle = new Bundle();
            for (Entry entry : this.be.entrySet()) {
                bundle.putString((String) entry.getKey(), (String) entry.getValue());
            }
            bundle.putAll(this.hf);
            String token = FirebaseInstanceId.getInstance().getToken();
            if (token != null) {
                this.hf.putString("from", token);
            } else {
                this.hf.remove("from");
            }
            return new RemoteMessage(bundle);
        }

        public Builder clearData() {
            this.be.clear();
            return this;
        }

        public Builder setCollapseKey(String str) {
            this.hf.putString("collapse_key", str);
            return this;
        }

        public Builder setData(Map<String, String> map) {
            this.be.clear();
            this.be.putAll(map);
            return this;
        }

        public Builder setMessageId(String str) {
            this.hf.putString("google.message_id", str);
            return this;
        }

        public Builder setMessageType(String str) {
            this.hf.putString("message_type", str);
            return this;
        }

        public Builder setTtl(int i) {
            this.hf.putString("google.ttl", String.valueOf(i));
            return this;
        }
    }

    public static class Notification {
        private final String JB;
        private final String Pf;
        private final String bla;
        private final String[] blb;
        private final String blc;
        private final String[] bld;
        private final String ble;
        private final String blf;
        private final String blg;
        private final String mTag;
        private final String zzbna;

        private Notification(Bundle bundle) {
            this.JB = zza.zzf(bundle, "gcm.n.title");
            this.bla = zza.zzh(bundle, "gcm.n.title");
            this.blb = zzj(bundle, "gcm.n.title");
            this.zzbna = zza.zzf(bundle, "gcm.n.body");
            this.blc = zza.zzh(bundle, "gcm.n.body");
            this.bld = zzj(bundle, "gcm.n.body");
            this.ble = zza.zzf(bundle, "gcm.n.icon");
            this.blf = zza.zzat(bundle);
            this.mTag = zza.zzf(bundle, "gcm.n.tag");
            this.Pf = zza.zzf(bundle, "gcm.n.color");
            this.blg = zza.zzf(bundle, "gcm.n.click_action");
        }

        private String[] zzj(Bundle bundle, String str) {
            Object[] zzi = zza.zzi(bundle, str);
            if (zzi == null) {
                return null;
            }
            String[] strArr = new String[zzi.length];
            for (int i = 0; i < zzi.length; i++) {
                strArr[i] = String.valueOf(zzi[i]);
            }
            return strArr;
        }

        public String getBody() {
            return this.zzbna;
        }

        public String[] getBodyLocalizationArgs() {
            return this.bld;
        }

        public String getBodyLocalizationKey() {
            return this.blc;
        }

        public String getClickAction() {
            return this.blg;
        }

        public String getColor() {
            return this.Pf;
        }

        public String getIcon() {
            return this.ble;
        }

        public String getSound() {
            return this.blf;
        }

        public String getTag() {
            return this.mTag;
        }

        public String getTitle() {
            return this.JB;
        }

        public String[] getTitleLocalizationArgs() {
            return this.blb;
        }

        public String getTitleLocalizationKey() {
            return this.bla;
        }
    }

    RemoteMessage(int i, Bundle bundle) {
        this.mVersionCode = i;
        this.hf = bundle;
    }

    RemoteMessage(Bundle bundle) {
        this(1, bundle);
    }

    public String getCollapseKey() {
        return this.hf.getString("collapse_key");
    }

    public Map<String, String> getData() {
        if (this.be == null) {
            this.be = new ArrayMap();
            for (String str : this.hf.keySet()) {
                Object obj = this.hf.get(str);
                if (obj instanceof String) {
                    String str2 = (String) obj;
                    if (!(str.startsWith("google.") || str.startsWith("gcm.") || str.equals("from") || str.equals("message_type") || str.equals("collapse_key"))) {
                        this.be.put(str, str2);
                    }
                }
            }
        }
        return this.be;
    }

    public String getFrom() {
        return this.hf.getString("from");
    }

    public String getMessageId() {
        String string = this.hf.getString("google.message_id");
        return string == null ? this.hf.getString("message_id") : string;
    }

    public String getMessageType() {
        return this.hf.getString("message_type");
    }

    public Notification getNotification() {
        if (this.bkZ == null && zza.zzad(this.hf)) {
            this.bkZ = new Notification(this.hf);
        }
        return this.bkZ;
    }

    public long getSentTime() {
        return this.hf.getLong("google.sent_time");
    }

    public String getTo() {
        return this.hf.getString("google.to");
    }

    public int getTtl() {
        Object obj = this.hf.get("google.ttl");
        if (obj instanceof Integer) {
            return ((Integer) obj).intValue();
        }
        if (obj instanceof String) {
            try {
                return Integer.parseInt((String) obj);
            } catch (NumberFormatException e) {
                String valueOf = String.valueOf(obj);
                Log.w("FirebaseMessaging", new StringBuilder(String.valueOf(valueOf).length() + 13).append("Invalid TTL: ").append(valueOf).toString());
            }
        }
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzc.zza(this, parcel, i);
    }

    void zzak(Intent intent) {
        intent.putExtras(this.hf);
    }
}
