package com.zendesk.util;

import java.util.Locale;

public class FileUtils {
    private static final String BINARY_PREFIXES = "KMGTPE";
    private static final int BINARY_UNIT = 1024;
    private static final String SI_PREFIXES = "kMGTPE";
    private static final int SI_UNIT = 1000;

    private FileUtils() {
    }

    public static String humanReadableFileSize(Long bytes) {
        return humanReadableFileSize(bytes, true);
    }

    public static String humanReadableFileSize(Long bytes, boolean useSi) {
        if (bytes == null || bytes.longValue() < 0) {
            return "";
        }
        int unit = useSi ? 1000 : 1024;
        if (bytes.longValue() < ((long) unit)) {
            return bytes + " B";
        }
        String prefix = (useSi ? SI_PREFIXES : BINARY_PREFIXES).charAt(((int) (Math.log((double) bytes.longValue()) / Math.log((double) unit))) - 1) + (useSi ? "" : "i");
        return String.format(Locale.US, "%.1f %sB", new Object[]{Double.valueOf(((double) bytes.longValue()) / Math.pow((double) unit, (double) ((int) (Math.log((double) bytes.longValue()) / Math.log((double) unit))))), prefix});
    }

    public static String getFileExtension(String file) {
        if (!StringUtils.hasLength(file)) {
            return "";
        }
        int index = file.lastIndexOf(".");
        String suffix = "";
        if (index != -1) {
            return file.substring(index + 1).toLowerCase(Locale.US).trim();
        }
        return suffix;
    }
}
