package com.pipedrive.tasks;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import com.pipedrive.application.Session;

public abstract class StoreAwareAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {
    protected abstract Result doInBackgroundAfterStoreSync(Params... paramsArr);

    public StoreAwareAsyncTask(@NonNull Session session) {
        super(session);
    }

    @SafeVarargs
    @CallSuper
    protected final Result doInBackground(Params... params) {
        try {
            getSession().getStoreSynchronizeManager().waitForOfflineChangesToSync();
            return doInBackgroundAfterStoreSync(params);
        } catch (Exception e) {
            return null;
        }
    }
}
