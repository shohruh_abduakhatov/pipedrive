package com.pipedrive.model.products;

import android.support.annotation.NonNull;
import com.pipedrive.model.AssociatedDatasourceEntity;
import com.pipedrive.model.Currency;
import java.math.BigDecimal;

public class Price implements AssociatedDatasourceEntity {
    @NonNull
    private final Currency currency;
    @NonNull
    private final BigDecimal value;

    @NonNull
    public static Price create(@NonNull BigDecimal value, @NonNull Currency currency) {
        return new Price(value, currency);
    }

    private Price(@NonNull BigDecimal value, @NonNull Currency currency) {
        this.value = value;
        this.currency = currency;
    }

    public boolean isAllAssociationsExisting() {
        return this.currency.isExisting();
    }

    @NonNull
    public BigDecimal getValue() {
        return this.value;
    }

    @NonNull
    public Currency getCurrency() {
        return this.currency;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Price price = (Price) o;
        if (this.value.compareTo(price.value) == 0) {
            return this.currency.equals(price.currency);
        }
        return false;
    }

    public int hashCode() {
        return (this.value.hashCode() * 31) + this.currency.hashCode();
    }

    public String toString() {
        return "Price{value=" + this.value.doubleValue() + ", currency=" + this.currency + '}';
    }
}
