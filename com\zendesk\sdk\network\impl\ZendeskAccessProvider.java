package com.zendesk.sdk.network.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.access.AccessToken;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.model.access.JwtIdentity;
import com.zendesk.sdk.network.AccessProvider;
import com.zendesk.sdk.storage.IdentityStorage;
import com.zendesk.service.ErrorResponseAdapter;
import com.zendesk.service.ZendeskCallback;
import com.zendesk.util.StringUtils;

class ZendeskAccessProvider implements AccessProvider {
    private static final String LOG_TAG = "ZendeskAccessProvider";
    private final ZendeskAccessService accessService;
    private final IdentityStorage identityStorage;

    public ZendeskAccessProvider(@NonNull IdentityStorage identityStorage, @NonNull ZendeskAccessService accessService) {
        this.identityStorage = identityStorage;
        this.accessService = accessService;
    }

    public void getAndStoreAuthTokenViaJwt(@NonNull JwtIdentity identity, @Nullable final ZendeskCallback<AccessToken> callback) {
        Logger.d(LOG_TAG, "Requesting an access token for jwt identity.", new Object[0]);
        if (StringUtils.isEmpty(identity.getJwtUserIdentifier())) {
            String error = "The jwt user identifier is null or empty. We cannot proceed to get an access token";
            Logger.e(LOG_TAG, "The jwt user identifier is null or empty. We cannot proceed to get an access token", new Object[0]);
            if (callback != null) {
                callback.onError(new ErrorResponseAdapter("The jwt user identifier is null or empty. We cannot proceed to get an access token"));
                return;
            }
            return;
        }
        this.accessService.getAuthTokenViaJWT(identity, new PassThroughErrorZendeskCallback<AccessToken>(callback) {
            public void onSuccess(AccessToken accessToken) {
                ZendeskAccessProvider.this.identityStorage.storeAccessToken(accessToken);
                if (callback != null) {
                    callback.onSuccess(accessToken);
                }
            }
        });
    }

    public void getAndStoreAuthTokenViaAnonymous(@NonNull AnonymousIdentity identity, @Nullable final ZendeskCallback<AccessToken> callback) {
        Logger.d(LOG_TAG, "Requesting an access token for anonymous identity.", new Object[0]);
        this.accessService.getAuthTokenViaMobileSDK(identity, new PassThroughErrorZendeskCallback<AccessToken>(callback) {
            public void onSuccess(AccessToken accessToken) {
                ZendeskAccessProvider.this.identityStorage.storeAccessToken(accessToken);
                if (callback != null) {
                    callback.onSuccess(accessToken);
                }
            }
        });
    }
}
