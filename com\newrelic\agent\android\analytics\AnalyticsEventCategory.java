package com.newrelic.agent.android.analytics;

import io.fabric.sdk.android.services.settings.SettingsJsonConstants;

public enum AnalyticsEventCategory {
    Session,
    Interaction,
    Crash,
    Custom,
    RequestError;

    public static AnalyticsEventCategory fromString(String categoryString) {
        AnalyticsEventCategory category = Custom;
        if (categoryString == null) {
            return category;
        }
        if (categoryString.equalsIgnoreCase(SettingsJsonConstants.SESSION_KEY)) {
            return Session;
        }
        if (categoryString.equalsIgnoreCase("interaction")) {
            return Interaction;
        }
        if (categoryString.equalsIgnoreCase("crash")) {
            return Crash;
        }
        if (categoryString.equalsIgnoreCase("requesterror")) {
            return RequestError;
        }
        return category;
    }
}
