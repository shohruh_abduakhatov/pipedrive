package com.pipedrive.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.EmailMessagesDataSource;
import com.pipedrive.datasource.EmailParticipantsDataSource;
import com.pipedrive.datasource.EmailThreadsDataSource;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.email.EmailMessage;
import com.pipedrive.model.email.EmailParticipant;
import com.pipedrive.model.email.EmailPosition;
import com.pipedrive.model.email.EmailThread;
import com.pipedrive.util.networking.entities.EmailMessageEntity;
import com.pipedrive.util.networking.entities.EmailParticipantEntity;
import com.pipedrive.util.networking.entities.EmailThreadEntity;
import com.pipedrive.util.organizations.OrganizationsNetworkingUtil;
import java.util.ArrayList;
import java.util.List;

public enum EmailNetworkingUtil {
    ;

    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$pipedrive$model$email$EmailPosition = null;

        static {
            $SwitchMap$com$pipedrive$model$email$EmailPosition = new int[EmailPosition.values().length];
            try {
                $SwitchMap$com$pipedrive$model$email$EmailPosition[EmailPosition.FROM.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$pipedrive$model$email$EmailPosition[EmailPosition.TO.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$pipedrive$model$email$EmailPosition[EmailPosition.CC.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$pipedrive$model$email$EmailPosition[EmailPosition.BCC.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    public static void createOrUpdateThread(@NonNull Session session, @NonNull EmailThreadEntity entity) {
        if (session.isValid()) {
            EmailThread emailThread = EmailThread.from(entity);
            if (!(emailThread == null)) {
                Deal deal = null;
                if (entity.getDealId() != null) {
                    deal = new DealsDataSource(session.getDatabase()).findDealByDealId((long) entity.getDealId().intValue());
                    if (deal == null) {
                        deal = DealsNetworkingUtil.downloadDeal(session, (long) entity.getDealId().intValue());
                        if (deal != null) {
                            DealsNetworkingUtil.createOrUpdateDealIntoDBWithRelations(session, deal);
                        }
                    }
                }
                emailThread.setDeal(deal);
                List<Organization> organizations = new ArrayList();
                if (entity.getOrgIds() != null && entity.getOrgIds().length > 0) {
                    OrganizationsDataSource organizationsDataSource = new OrganizationsDataSource(session.getDatabase());
                    for (Integer orgId : entity.getOrgIds()) {
                        Organization organization = null;
                        if (orgId != null) {
                            organization = organizationsDataSource.findOrganizationByPipedriveId(orgId.intValue());
                            if (organization == null) {
                                organization = OrganizationsNetworkingUtil.downloadOrganization(session, orgId.intValue());
                                if (organization != null) {
                                    OrganizationsNetworkingUtil.createOrUpdateOrganizationIntoDBWithRelations(session, organization);
                                }
                            }
                        }
                        if (organization != null) {
                            organizations.add(organization);
                        }
                    }
                }
                emailThread.setOrganizations(organizations);
                new EmailThreadsDataSource(session.getDatabase()).createOrUpdate(emailThread);
            }
        }
    }

    private static EmailThread createOrUpdateThread(@NonNull Session session, @NonNull Integer threadPipedriveId, @Nullable Long dealPipedriveId, @Nullable Long orgPipedriveId) {
        EmailThreadsDataSource emailThreadsDataSource = new EmailThreadsDataSource(session.getDatabase());
        EmailThread emailThread = new EmailThread();
        emailThread.setPipedriveId(threadPipedriveId.intValue());
        Deal deal = null;
        if (dealPipedriveId != null) {
            deal = new DealsDataSource(session.getDatabase()).findDealByDealId(dealPipedriveId.longValue());
            if (deal == null) {
                deal = DealsNetworkingUtil.downloadDeal(session, dealPipedriveId.longValue());
                if (deal != null) {
                    DealsNetworkingUtil.createOrUpdateDealIntoDBWithRelations(session, deal);
                }
            }
        }
        emailThread.setDeal(deal);
        if (orgPipedriveId != null) {
            Organization organization = new OrganizationsDataSource(session.getDatabase()).findOrganizationByPipedriveId(orgPipedriveId.intValue());
            if (organization == null) {
                organization = OrganizationsNetworkingUtil.downloadOrganization(session, orgPipedriveId.intValue());
                if (organization != null) {
                    OrganizationsNetworkingUtil.createOrUpdateOrganizationIntoDBWithRelations(session, organization);
                }
            }
            boolean shouldAdd = true;
            if (emailThread.getOrganizations() != null) {
                for (Organization org : emailThread.getOrganizations()) {
                    if (org.getPipedriveId() == orgPipedriveId.intValue()) {
                        shouldAdd = false;
                        break;
                    }
                }
            }
            emailThread.setOrganizations(new ArrayList());
            if (shouldAdd) {
                emailThread.getOrganizations().add(organization);
            }
        }
        emailThreadsDataSource.createOrUpdate(emailThread);
        return emailThread;
    }

    public static void createOrUpdateMessage(@NonNull Session session, @NonNull EmailMessageEntity emailMessageEntity, @Nullable Long dealPipedriveId, @Nullable Long orgPipedriveId) {
        if (session.isValid()) {
            EmailMessagesDataSource messagesDataSource = new EmailMessagesDataSource(session.getDatabase());
            EmailThread emailThread = createOrUpdateThread(session, emailMessageEntity.getThreadId(), dealPipedriveId, orgPipedriveId);
            messagesDataSource.createOrUpdate(EmailMessage.from(emailMessageEntity, createOrUpdateParticipants(session, emailMessageEntity, EmailPosition.FROM), createOrUpdateParticipants(session, emailMessageEntity, EmailPosition.TO), createOrUpdateParticipants(session, emailMessageEntity, EmailPosition.CC), createOrUpdateParticipants(session, emailMessageEntity, EmailPosition.BCC), emailThread));
        }
    }

    private static List<EmailParticipant> createOrUpdateParticipants(@NonNull Session session, @NonNull EmailMessageEntity messageEntity, @NonNull EmailPosition position) {
        ArrayList<EmailParticipant> output = new ArrayList();
        List<EmailParticipantEntity> input = null;
        switch (AnonymousClass1.$SwitchMap$com$pipedrive$model$email$EmailPosition[position.ordinal()]) {
            case 1:
                input = messageEntity.getFrom();
                break;
            case 2:
                input = messageEntity.getTo();
                break;
            case 3:
                input = messageEntity.getCc();
                break;
            case 4:
                input = messageEntity.getBcc();
                break;
        }
        if (!(input == null || input.isEmpty())) {
            for (EmailParticipantEntity entity : input) {
                if (entity != null) {
                    EmailParticipant participant = createOrUpdateParticipant(session, entity);
                    if (participant != null) {
                        output.add(participant);
                    }
                }
            }
        }
        return output;
    }

    private static EmailParticipant createOrUpdateParticipant(@NonNull Session session, @NonNull EmailParticipantEntity participantEntity) {
        if (!session.isValid()) {
            return null;
        }
        Person person = null;
        Integer personPipedriveId = participantEntity.getPersonId();
        if (personPipedriveId != null) {
            person = new PersonsDataSource(session.getDatabase()).findPersonByPipedriveId((long) personPipedriveId.intValue());
        }
        EmailParticipant emailParticipant = EmailParticipant.from(participantEntity, person, null);
        new EmailParticipantsDataSource(session.getDatabase()).createOrUpdate(emailParticipant);
        return emailParticipant;
    }

    public static void createOrUpdateEmailMessagesIntoDBWithRelations(@NonNull Session session, @NonNull List<EmailMessageEntity> emailMessagesFromRecents) {
        EmailMessagesDataSource dataSource = new EmailMessagesDataSource(session.getDatabase());
        dataSource.beginTransactionNonExclusive();
        for (EmailMessageEntity emailMessagesFromRecent : emailMessagesFromRecents) {
            createOrUpdateMessage(session, emailMessagesFromRecent, null, null);
        }
        dataSource.commit();
    }

    public static void createOrUpdateEmailThreadsIntoDBWithRelations(@NonNull Session session, @NonNull List<EmailThreadEntity> emailThreadsFromRecents) {
        EmailThreadsDataSource dataSource = new EmailThreadsDataSource(session.getDatabase());
        dataSource.beginTransactionNonExclusive();
        for (EmailThreadEntity threadEntity : emailThreadsFromRecents) {
            createOrUpdateThread(session, threadEntity);
        }
        dataSource.commit();
    }
}
