package rx;

import java.util.concurrent.TimeUnit;
import rx.Scheduler.Worker;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;

class Completable$16 implements Completable$OnSubscribe {
    final /* synthetic */ Completable this$0;
    final /* synthetic */ long val$delay;
    final /* synthetic */ boolean val$delayError;
    final /* synthetic */ Scheduler val$scheduler;
    final /* synthetic */ TimeUnit val$unit;

    Completable$16(Completable completable, Scheduler scheduler, long j, TimeUnit timeUnit, boolean z) {
        this.this$0 = completable;
        this.val$scheduler = scheduler;
        this.val$delay = j;
        this.val$unit = timeUnit;
        this.val$delayError = z;
    }

    public void call(final CompletableSubscriber s) {
        final CompositeSubscription set = new CompositeSubscription();
        final Worker w = this.val$scheduler.createWorker();
        set.add(w);
        this.this$0.unsafeSubscribe(new CompletableSubscriber() {
            public void onCompleted() {
                set.add(w.schedule(new Action0() {
                    public void call() {
                        try {
                            s.onCompleted();
                        } finally {
                            w.unsubscribe();
                        }
                    }
                }, Completable$16.this.val$delay, Completable$16.this.val$unit));
            }

            public void onError(final Throwable e) {
                if (Completable$16.this.val$delayError) {
                    set.add(w.schedule(new Action0() {
                        public void call() {
                            try {
                                s.onError(e);
                            } finally {
                                w.unsubscribe();
                            }
                        }
                    }, Completable$16.this.val$delay, Completable$16.this.val$unit));
                } else {
                    s.onError(e);
                }
            }

            public void onSubscribe(Subscription d) {
                set.add(d);
                s.onSubscribe(set);
            }
        });
    }
}
