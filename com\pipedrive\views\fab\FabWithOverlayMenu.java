package com.pipedrive.views.fab;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.support.annotation.IdRes;
import android.support.annotation.MenuRes;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.pipedrive.R;
import com.pipedrive.util.ImageUtil;
import com.pipedrive.util.camera.CameraHelper;

public class FabWithOverlayMenu extends FrameLayout implements OnClickListener {
    private static final int DURATION = 150;
    private static final int START_DELAY_BASE = 25;
    private static final int TRANSLATION_Y = 50;
    @BindView(2131820590)
    View mAddButton;
    @BindView(2131820973)
    View mBackground;
    private FabMenuClickListener mClickListener;
    @BindView(2131820974)
    View mCloseButton;
    @BindView(2131820975)
    ViewGroup mItemContainer;
    private boolean mMenuVisible;

    public FabWithOverlayMenu(Context context) {
        this(context, null);
    }

    public FabWithOverlayMenu(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FabWithOverlayMenu(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mMenuVisible = false;
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_fab_overlay_menu, this, true);
        ButterKnife.bind(this);
    }

    public void setMenu(@MenuRes int menuRes, @IdRes int noteMenuItemId) {
        PopupMenu popupMenu = new PopupMenu(getContext(), null);
        popupMenu.inflate(menuRes);
        boolean isCameraFeatureSupported = new CameraHelper().isDeviceHasCamera(getContext());
        Menu popupMenuMenu = popupMenu.getMenu();
        this.mItemContainer.removeAllViews();
        for (int i = 0; i < popupMenuMenu.size(); i++) {
            boolean doNotAddCameraMenuItemToSelection;
            MenuItem menuItem = popupMenuMenu.getItem(i);
            if (isCameraFeatureSupported || !TextUtils.equals(menuItem.getTitle(), getContext().getResources().getString(R.string.take_photo))) {
                doNotAddCameraMenuItemToSelection = false;
            } else {
                doNotAddCameraMenuItemToSelection = true;
            }
            if (!doNotAddCameraMenuItemToSelection) {
                boolean z;
                if (menuItem.getItemId() == noteMenuItemId) {
                    z = true;
                } else {
                    z = false;
                }
                this.mItemContainer.addView(getItemView(menuItem, z));
            }
        }
    }

    @SuppressLint({"WrongViewCast"})
    @NonNull
    private View getItemView(MenuItem item, boolean isNote) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.row_fab_overlay_menu, this.mItemContainer, false);
        ((TextView) view.findViewById(R.id.title)).setText(item.getTitle());
        final View button = view.findViewById(R.id.button);
        button.setId(item.getItemId());
        button.setOnClickListener(this);
        FloatingActionButton iconFab = (FloatingActionButton) view.findViewById(R.id.icon);
        iconFab.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, @NonNull MotionEvent event) {
                event.offsetLocation(v.getX() - button.getX(), v.getY() - button.getY());
                button.onTouchEvent(event);
                return false;
            }
        });
        iconFab.setImageDrawable(ImageUtil.getTintedDrawable(item.getIcon(), ContextCompat.getColor(getContext(), R.color.dark)));
        iconFab.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getContext(), isNote ? R.color.note : R.color.white)));
        return view;
    }

    public void setOnMenuClickListener(FabMenuClickListener clickListener) {
        this.mClickListener = clickListener;
    }

    @OnClick({2131820590})
    void onAddClicked() {
        showMenu();
    }

    @OnClick({2131820974, 2131820973})
    void onCloseClicked() {
        hideMenu();
    }

    public void onClick(@NonNull View v) {
        if (this.mClickListener != null) {
            this.mClickListener.onMenuItemClicked(v.getId());
        }
        hideMenu();
    }

    private void hideMenu() {
        if (this.mMenuVisible) {
            this.mCloseButton.animate().cancel();
            this.mCloseButton.animate().alpha(0.0f).scaleY(0.0f).scaleX(0.0f).setDuration(75).setStartDelay(0).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(@NonNull Animator animation) {
                    FabWithOverlayMenu.this.mCloseButton.setVisibility(8);
                }
            });
            this.mBackground.animate().cancel();
            this.mBackground.animate().alpha(0.0f).setDuration(75).setStartDelay(0).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(@NonNull Animator animation) {
                    FabWithOverlayMenu.this.mBackground.setVisibility(8);
                }
            });
            this.mAddButton.animate().cancel();
            this.mAddButton.setAlpha(0.0f);
            this.mAddButton.setScaleX(0.0f);
            this.mAddButton.setScaleY(0.0f);
            this.mAddButton.setVisibility(0);
            this.mAddButton.animate().alpha(1.0f).scaleX(1.0f).scaleY(1.0f).setStartDelay(75).setDuration(75).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(@NonNull Animator animation) {
                    FabWithOverlayMenu.this.mMenuVisible = false;
                }
            });
            this.mItemContainer.animate().cancel();
            this.mItemContainer.animate().alpha(0.0f).translationY(20.0f).setStartDelay(0).setDuration(75).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(@NonNull Animator animation) {
                    FabWithOverlayMenu.this.mItemContainer.setVisibility(8);
                }
            });
        }
    }

    private void showMenu() {
        if (!this.mMenuVisible) {
            this.mAddButton.animate().cancel();
            this.mAddButton.animate().alpha(0.0f).scaleX(0.0f).scaleY(0.0f).setDuration(150).setStartDelay(0).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(@NonNull Animator animation) {
                    FabWithOverlayMenu.this.mAddButton.setVisibility(8);
                }
            });
            this.mCloseButton.animate().cancel();
            this.mCloseButton.setAlpha(0.0f);
            this.mCloseButton.setScaleX(0.0f);
            this.mCloseButton.setScaleY(0.0f);
            this.mCloseButton.setVisibility(0);
            this.mCloseButton.animate().alpha(1.0f).scaleX(1.0f).scaleY(1.0f).setStartDelay(150).setDuration(150).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(@NonNull Animator animation) {
                    FabWithOverlayMenu.this.mMenuVisible = true;
                }
            });
            this.mBackground.animate().cancel();
            this.mBackground.setAlpha(0.0f);
            this.mBackground.setVisibility(0);
            this.mBackground.animate().alpha(1.0f).setListener(null).setStartDelay(150).setDuration(150);
            this.mItemContainer.animate().cancel();
            this.mItemContainer.setAlpha(0.0f);
            this.mItemContainer.setTranslationY(0.0f);
            this.mItemContainer.setVisibility(0);
            this.mItemContainer.animate().alpha(1.0f).setListener(null).setStartDelay(150).setDuration(150);
            for (int i = 0; i < this.mItemContainer.getChildCount(); i++) {
                View child = this.mItemContainer.getChildAt(i);
                int delay = (this.mItemContainer.getChildCount() - i) * 25;
                child.setAlpha(0.0f);
                child.setTranslationY(50.0f);
                child.setVisibility(0);
                child.animate().cancel();
                child.animate().alpha(1.0f).translationY(0.0f).setStartDelay((long) (delay + DURATION)).setListener(null).setDuration(150);
            }
        }
    }

    public boolean consumeBackPress() {
        if (!this.mMenuVisible) {
            return false;
        }
        hideMenu();
        return true;
    }
}
