package com.zendesk.sdk.network.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.SdkConfiguration;
import com.zendesk.sdk.model.helpcenter.Article;
import com.zendesk.sdk.model.helpcenter.ArticleVote;
import com.zendesk.sdk.model.helpcenter.ArticleVoteResponse;
import com.zendesk.sdk.model.helpcenter.ArticlesListResponse;
import com.zendesk.sdk.model.helpcenter.ArticlesResponse;
import com.zendesk.sdk.model.helpcenter.ArticlesSearchResponse;
import com.zendesk.sdk.model.helpcenter.Attachment;
import com.zendesk.sdk.model.helpcenter.AttachmentType;
import com.zendesk.sdk.model.helpcenter.Category;
import com.zendesk.sdk.model.helpcenter.FlatArticle;
import com.zendesk.sdk.model.helpcenter.HelpCenterSearch;
import com.zendesk.sdk.model.helpcenter.ListArticleQuery;
import com.zendesk.sdk.model.helpcenter.RecordArticleViewRequest;
import com.zendesk.sdk.model.helpcenter.SearchArticle;
import com.zendesk.sdk.model.helpcenter.Section;
import com.zendesk.sdk.model.helpcenter.SortBy;
import com.zendesk.sdk.model.helpcenter.SortOrder;
import com.zendesk.sdk.model.helpcenter.SuggestedArticleResponse;
import com.zendesk.sdk.model.helpcenter.SuggestedArticleSearch;
import com.zendesk.sdk.model.helpcenter.User;
import com.zendesk.sdk.model.helpcenter.help.HelpItem;
import com.zendesk.sdk.model.helpcenter.help.HelpRequest;
import com.zendesk.sdk.model.helpcenter.help.HelpResponse;
import com.zendesk.sdk.model.settings.SafeMobileSettings;
import com.zendesk.sdk.network.BaseProvider;
import com.zendesk.sdk.network.HelpCenterProvider;
import com.zendesk.sdk.storage.HelpCenterSessionCache;
import com.zendesk.service.ErrorResponseAdapter;
import com.zendesk.service.ZendeskCallback;
import com.zendesk.util.CollectionUtils;
import com.zendesk.util.LocaleUtil;
import com.zendesk.util.StringUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

class ZendeskHelpCenterProvider implements HelpCenterProvider {
    private static final String EMPTY_JSON_BODY = "{}";
    private static final String LOG_TAG = "ZendeskHelpCenterProvider";
    private final BaseProvider baseProvider;
    private final ZendeskHelpCenterService helpCenterService;
    private final HelpCenterSessionCache helpCenterSessionCache;

    ZendeskHelpCenterProvider(BaseProvider baseProvider, ZendeskHelpCenterService zendeskHelpCenterService, HelpCenterSessionCache helpCenterSessionCache) {
        this.baseProvider = baseProvider;
        this.helpCenterService = zendeskHelpCenterService;
        this.helpCenterSessionCache = helpCenterSessionCache;
    }

    public void getHelp(@NonNull final HelpRequest request, @Nullable final ZendeskCallback<List<HelpItem>> callback) {
        this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
            public void onSuccess(SdkConfiguration config) {
                if (!ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(callback, config.getMobileSettings())) {
                    ZendeskHelpCenterProvider.this.helpCenterService.getHelp(config.getBearerAuthorizationHeader(), ZendeskHelpCenterProvider.this.getBestLocale(config.getMobileSettings()), request.getCategoryIds(), request.getSectionIds(), request.getIncludes(), request.getArticlesPerPageLimit(), StringUtils.toCsvString(request.getLabelNames()), new PassThroughErrorZendeskCallback<HelpResponse>(callback) {
                        public void onSuccess(HelpResponse helpResponse) {
                            ZendeskConfig.INSTANCE.getTracker().helpCenterLoaded();
                            if (callback != null) {
                                callback.onSuccess(new HelpResponseConverter(helpResponse).convert());
                            }
                        }
                    });
                }
            }
        });
    }

    public void getCategories(@Nullable final ZendeskCallback<List<Category>> callback) {
        if (!sanityCheck(callback, new Object[0])) {
            this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
                public void onSuccess(SdkConfiguration config) {
                    if (!ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(callback, config.getMobileSettings())) {
                        ZendeskHelpCenterProvider.this.helpCenterService.getCategories(config.getBearerAuthorizationHeader(), ZendeskHelpCenterProvider.this.getBestLocale(config.getMobileSettings()), callback);
                    }
                }
            });
        }
    }

    public void getSections(@Nullable final Long categoryId, @Nullable final ZendeskCallback<List<Section>> callback) {
        if (!sanityCheck(callback, categoryId)) {
            this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
                public void onSuccess(SdkConfiguration config) {
                    if (!ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(callback, config.getMobileSettings())) {
                        ZendeskHelpCenterProvider.this.helpCenterService.getSectionsForCategory(config.getBearerAuthorizationHeader(), categoryId, ZendeskHelpCenterProvider.this.getBestLocale(config.getMobileSettings()), callback);
                    }
                }
            });
        }
    }

    public void getArticles(@NonNull final Long sectionId, @Nullable final ZendeskCallback<List<Article>> callback) {
        if (!sanityCheck(callback, sectionId)) {
            this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
                public void onSuccess(SdkConfiguration config) {
                    if (!ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(callback, config.getMobileSettings())) {
                        String include = "users";
                        ZendeskHelpCenterProvider.this.helpCenterService.getArticlesForSection(config.getBearerAuthorizationHeader(), sectionId, ZendeskHelpCenterProvider.this.getBestLocale(config.getMobileSettings()), "users", callback);
                    }
                }
            });
        }
    }

    public void listArticles(@NonNull final ListArticleQuery query, @Nullable final ZendeskCallback<List<SearchArticle>> callback) {
        if (!sanityCheck(callback, query)) {
            this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
                public void onSuccess(SdkConfiguration config) {
                    if (!ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(callback, config.getMobileSettings())) {
                        String include;
                        if (query.getInclude() == null) {
                            include = StringUtils.toCsvString("categories", "sections", "users");
                        } else {
                            include = query.getInclude();
                        }
                        ZendeskHelpCenterProvider.this.helpCenterService.listArticles(config.getBearerAuthorizationHeader(), StringUtils.toCsvString(query.getLabelNames()), query.getLocale() == null ? ZendeskHelpCenterProvider.this.getBestLocale(config.getMobileSettings()) : query.getLocale(), include, (query.getSortBy() == null ? SortBy.CREATED_AT : query.getSortBy()).getApiValue(), (query.getSortOrder() == null ? SortOrder.DESCENDING : query.getSortOrder()).getApiValue(), query.getPage(), query.getResultsPerPage(), new PassThroughErrorZendeskCallback<ArticlesListResponse>(callback) {
                            public void onSuccess(ArticlesListResponse articlesListResponse) {
                                List<SearchArticle> searchArticles = ZendeskHelpCenterProvider.this.asSearchArticleList(articlesListResponse);
                                if (callback != null) {
                                    callback.onSuccess(searchArticles);
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    public void listArticlesFlat(@NonNull final ListArticleQuery query, @Nullable final ZendeskCallback<List<FlatArticle>> callback) {
        if (!sanityCheck(callback, query)) {
            this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
                public void onSuccess(SdkConfiguration config) {
                    if (!ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(callback, config.getMobileSettings())) {
                        String include = "categories,sections";
                        ZendeskHelpCenterProvider.this.helpCenterService.listArticles(config.getBearerAuthorizationHeader(), StringUtils.toCsvString(query.getLabelNames()), query.getLocale() == null ? ZendeskHelpCenterProvider.this.getBestLocale(config.getMobileSettings()) : query.getLocale(), "categories,sections", (query.getSortBy() == null ? SortBy.CREATED_AT : query.getSortBy()).getApiValue(), (query.getSortOrder() == null ? SortOrder.DESCENDING : query.getSortOrder()).getApiValue(), query.getPage(), query.getResultsPerPage(), new PassThroughErrorZendeskCallback<ArticlesListResponse>(callback) {
                            public void onSuccess(ArticlesListResponse articlesListResponse) {
                                List<FlatArticle> flatArticles = ZendeskHelpCenterProvider.this.asFlatArticleList(articlesListResponse);
                                if (callback != null) {
                                    callback.onSuccess(flatArticles);
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    public void searchArticles(@NonNull final HelpCenterSearch search, @Nullable final ZendeskCallback<List<SearchArticle>> callback) {
        if (!sanityCheck(callback, search)) {
            this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
                public void onSuccess(SdkConfiguration config) {
                    if (!ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(callback, config.getMobileSettings())) {
                        String include;
                        String labelNames;
                        Locale locale;
                        if (StringUtils.isEmpty(search.getInclude())) {
                            include = StringUtils.toCsvString("categories", "sections", "users");
                        } else {
                            include = StringUtils.toCsvString(search.getInclude());
                        }
                        if (StringUtils.isEmpty(search.getLabelNames())) {
                            labelNames = null;
                        } else {
                            labelNames = StringUtils.toCsvString(search.getLabelNames());
                        }
                        if (search.getLocale() == null) {
                            locale = ZendeskHelpCenterProvider.this.getBestLocale(config.getMobileSettings());
                        } else {
                            locale = search.getLocale();
                        }
                        ZendeskHelpCenterProvider.this.helpCenterService.searchArticles(config.getBearerAuthorizationHeader(), search.getQuery(), locale, include, labelNames, search.getCategoryIds(), search.getSectionIds(), search.getPage(), search.getPerPage(), new PassThroughErrorZendeskCallback<ArticlesSearchResponse>(callback) {
                            public void onSuccess(ArticlesSearchResponse articlesSearchResponse) {
                                ZendeskConfig.INSTANCE.getTracker().helpCenterSearched(search.getQuery());
                                int size = 0;
                                if (articlesSearchResponse != null && CollectionUtils.isNotEmpty(articlesSearchResponse.getArticles())) {
                                    size = articlesSearchResponse.getArticles().size();
                                }
                                ZendeskHelpCenterProvider.this.helpCenterSessionCache.setLastSearch(search.getQuery(), size);
                                List<SearchArticle> searchArticles = ZendeskHelpCenterProvider.this.asSearchArticleList(articlesSearchResponse);
                                if (callback != null) {
                                    callback.onSuccess(searchArticles);
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    public void getArticle(@NonNull final Long articleId, @Nullable final ZendeskCallback<Article> callback) {
        if (!sanityCheck(callback, articleId)) {
            this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
                public void onSuccess(SdkConfiguration config) {
                    if (!ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(callback, config.getMobileSettings())) {
                        String include = "users";
                        ZendeskHelpCenterProvider.this.helpCenterService.getArticle(config.getBearerAuthorizationHeader(), articleId, ZendeskHelpCenterProvider.this.getBestLocale(config.getMobileSettings()), "users", callback);
                    }
                }
            });
        }
    }

    public void getSection(@NonNull final Long sectionId, @Nullable final ZendeskCallback<Section> callback) {
        if (!sanityCheck(callback, sectionId)) {
            this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
                public void onSuccess(SdkConfiguration config) {
                    if (!ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(callback, config.getMobileSettings())) {
                        ZendeskHelpCenterProvider.this.helpCenterService.getSectionById(config.getBearerAuthorizationHeader(), sectionId, ZendeskHelpCenterProvider.this.getBestLocale(config.getMobileSettings()), callback);
                    }
                }
            });
        }
    }

    public void getCategory(@NonNull final Long categoryId, @Nullable final ZendeskCallback<Category> callback) {
        if (!sanityCheck(callback, categoryId)) {
            this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
                public void onSuccess(SdkConfiguration config) {
                    if (!ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(callback, config.getMobileSettings())) {
                        ZendeskHelpCenterProvider.this.helpCenterService.getCategoryById(config.getBearerAuthorizationHeader(), categoryId, ZendeskHelpCenterProvider.this.getBestLocale(config.getMobileSettings()), callback);
                    }
                }
            });
        }
    }

    public void getAttachments(@NonNull Long articleId, @NonNull AttachmentType attachmentType, @Nullable ZendeskCallback<List<Attachment>> callback) {
        if (!sanityCheck(callback, articleId, attachmentType)) {
            final ZendeskCallback<List<Attachment>> zendeskCallback = callback;
            final Long l = articleId;
            final AttachmentType attachmentType2 = attachmentType;
            this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
                public void onSuccess(SdkConfiguration config) {
                    if (!ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(zendeskCallback, config.getMobileSettings())) {
                        ZendeskHelpCenterProvider.this.helpCenterService.getAttachments(config.getBearerAuthorizationHeader(), ZendeskHelpCenterProvider.this.getBestLocale(config.getMobileSettings()), l, attachmentType2, zendeskCallback);
                    }
                }
            });
        }
    }

    public void upvoteArticle(@NonNull final Long articleId, @Nullable final ZendeskCallback<ArticleVote> callback) {
        if (!sanityCheck(callback, articleId)) {
            this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
                public void onSuccess(SdkConfiguration config) {
                    if (!ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(callback, config.getMobileSettings())) {
                        ZendeskHelpCenterProvider.this.helpCenterService.upvoteArticle(config.getBearerAuthorizationHeader(), articleId, ZendeskHelpCenterProvider.EMPTY_JSON_BODY, new PassThroughErrorZendeskCallback<ArticleVoteResponse>(callback) {
                            public void onSuccess(ArticleVoteResponse articleVoteResponse) {
                                if (callback != null) {
                                    callback.onSuccess(articleVoteResponse.getVote());
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    public void downvoteArticle(@NonNull final Long articleId, @Nullable final ZendeskCallback<ArticleVote> callback) {
        if (!sanityCheck(callback, articleId)) {
            this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
                public void onSuccess(SdkConfiguration config) {
                    if (!ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(callback, config.getMobileSettings())) {
                        ZendeskHelpCenterProvider.this.helpCenterService.downvoteArticle(config.getBearerAuthorizationHeader(), articleId, ZendeskHelpCenterProvider.EMPTY_JSON_BODY, new PassThroughErrorZendeskCallback<ArticleVoteResponse>(callback) {
                            public void onSuccess(ArticleVoteResponse result) {
                                if (callback != null) {
                                    callback.onSuccess(result.getVote());
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    public void deleteVote(@NonNull final Long voteId, @Nullable final ZendeskCallback<Void> callback) {
        if (!sanityCheck(callback, voteId)) {
            this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
                public void onSuccess(SdkConfiguration config) {
                    if (!ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(callback, config.getMobileSettings())) {
                        ZendeskHelpCenterProvider.this.helpCenterService.deleteVote(config.getBearerAuthorizationHeader(), voteId, new PassThroughErrorZendeskCallback<Void>(callback) {
                            public void onSuccess(Void result) {
                                if (callback != null) {
                                    callback.onSuccess(result);
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    public void getSuggestedArticles(@NonNull final SuggestedArticleSearch suggestedArticleSearch, @Nullable final ZendeskCallback<SuggestedArticleResponse> callback) {
        if (!sanityCheck(callback, suggestedArticleSearch)) {
            this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
                public void onSuccess(SdkConfiguration config) {
                    if (!ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(callback, config.getMobileSettings())) {
                        String labelNames;
                        Locale queryLocale = suggestedArticleSearch.getLocale() == null ? ZendeskHelpCenterProvider.this.getBestLocale(config.getMobileSettings()) : suggestedArticleSearch.getLocale();
                        if (StringUtils.isEmpty(suggestedArticleSearch.getLabelNames())) {
                            labelNames = null;
                        } else {
                            labelNames = StringUtils.toCsvString(suggestedArticleSearch.getLabelNames());
                        }
                        ZendeskHelpCenterProvider.this.helpCenterService.getSuggestedArticles(config.getBearerAuthorizationHeader(), suggestedArticleSearch.getQuery(), queryLocale, labelNames, suggestedArticleSearch.getCategoryId(), suggestedArticleSearch.getSectionId(), callback);
                    }
                }
            });
        }
    }

    public void submitRecordArticleView(@NonNull Long articleId, @NonNull Locale locale, @Nullable ZendeskCallback<Void> callback) {
        final ZendeskCallback<Void> zendeskCallback = callback;
        final Long l = articleId;
        final Locale locale2 = locale;
        this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
            public void onSuccess(SdkConfiguration config) {
                if (!ZendeskHelpCenterProvider.this.sanityCheckHelpCenterSettings(zendeskCallback, config.getMobileSettings())) {
                    ZendeskHelpCenterProvider.this.helpCenterService.submitRecordArticleView(config.getBearerAuthorizationHeader(), l, locale2, new RecordArticleViewRequest(ZendeskHelpCenterProvider.this.helpCenterSessionCache.getLastSearch(), ZendeskHelpCenterProvider.this.helpCenterSessionCache.isUniqueSearchResultClick()), new PassThroughErrorZendeskCallback<Void>(zendeskCallback) {
                        public void onSuccess(Void genericResponse) {
                            ZendeskHelpCenterProvider.this.helpCenterSessionCache.unsetUniqueSearchResultClick();
                            if (zendeskCallback != null) {
                                zendeskCallback.onSuccess(genericResponse);
                            }
                        }
                    });
                }
            }
        });
    }

    Locale getBestLocale(SafeMobileSettings settings) {
        String helpCenterLocale = settings != null ? settings.getHelpCenterLocale() : "";
        return StringUtils.isEmpty(helpCenterLocale) ? Locale.getDefault() : LocaleUtil.forLanguageTag(helpCenterLocale);
    }

    boolean sanityCheck(ZendeskCallback<?> callback, Object... nonNullArgs) {
        if (nonNullArgs == null) {
            return false;
        }
        boolean nonNull = true;
        for (Object o : nonNullArgs) {
            if (o == null) {
                nonNull = false;
            }
        }
        if (nonNull) {
            return false;
        }
        String message = "One or more provided parameters are null.";
        Logger.e(LOG_TAG, "One or more provided parameters are null.", new Object[0]);
        if (callback != null) {
            callback.onError(new ErrorResponseAdapter("One or more provided parameters are null."));
        }
        return true;
    }

    boolean sanityCheckHelpCenterSettings(ZendeskCallback<?> callback, SafeMobileSettings mobileSettings) {
        String message;
        if (!mobileSettings.hasHelpCenterSettings()) {
            message = "Help Center settings are null. Can not continue with the call";
            Logger.e(LOG_TAG, "Help Center settings are null. Can not continue with the call", new Object[0]);
            if (callback == null) {
                return true;
            }
            callback.onError(new ErrorResponseAdapter("Help Center settings are null. Can not continue with the call"));
            return true;
        } else if (mobileSettings.isHelpCenterEnabled()) {
            return false;
        } else {
            message = "Help Center is disabled in your app's settings. Can not continue with the call";
            Logger.e(LOG_TAG, "Help Center is disabled in your app's settings. Can not continue with the call", new Object[0]);
            if (callback == null) {
                return true;
            }
            callback.onError(new ErrorResponseAdapter("Help Center is disabled in your app's settings. Can not continue with the call"));
            return true;
        }
    }

    List<SearchArticle> asSearchArticleList(ArticlesResponse result) {
        List<SearchArticle> searchArticles = new ArrayList();
        if (result != null) {
            Section section;
            Category category;
            Map<Long, Section> sectionIdToSection = new HashMap();
            Map<Long, Category> categoryIdToCategory = new HashMap();
            Map<Long, User> userIdToUser = new HashMap();
            List<Article> articles = CollectionUtils.ensureEmpty(result.getArticles());
            List<Section> sections = CollectionUtils.ensureEmpty(result.getSections());
            List<Category> categories = CollectionUtils.ensureEmpty(result.getCategories());
            List<User> users = CollectionUtils.ensureEmpty(result.getUsers());
            for (Section section2 : sections) {
                if (section2.getId() != null) {
                    sectionIdToSection.put(section2.getId(), section2);
                }
            }
            for (Category category2 : categories) {
                if (category2.getId() != null) {
                    categoryIdToCategory.put(category2.getId(), category2);
                }
            }
            for (User user : users) {
                if (user.getId() != null) {
                    userIdToUser.put(user.getId(), user);
                }
            }
            for (Article article : articles) {
                section2 = null;
                category2 = null;
                if (article.getSectionId() != null) {
                    section2 = (Section) sectionIdToSection.get(article.getSectionId());
                } else {
                    Logger.w(LOG_TAG, "Unable to determine section as section id was null.", new Object[0]);
                }
                if (section2 == null || section2.getCategoryId() == null) {
                    Logger.w(LOG_TAG, "Unable to determine category as section was null.", new Object[0]);
                } else {
                    category2 = (Category) categoryIdToCategory.get(section2.getCategoryId());
                }
                if (article.getAuthorId() != null) {
                    article.setAuthor((User) userIdToUser.get(article.getAuthorId()));
                } else {
                    Logger.w(LOG_TAG, "Unable to determine author as author id was null.", new Object[0]);
                }
                searchArticles.add(new SearchArticle(article, section2, category2));
            }
        }
        return searchArticles;
    }

    List<FlatArticle> asFlatArticleList(ArticlesResponse articlesListResponse) {
        if (articlesListResponse == null) {
            return new ArrayList();
        }
        List<Category> categories = articlesListResponse.getCategories();
        List<Section> sections = articlesListResponse.getSections();
        List<Article> articles = articlesListResponse.getArticles();
        Map<Long, Category> categoryMap = new HashMap();
        Map<Long, Section> sectionMap = new HashMap();
        List<FlatArticle> flatArticles = new ArrayList();
        if (CollectionUtils.isNotEmpty(articles)) {
            Section section;
            for (Category category : categories) {
                categoryMap.put(category.getId(), category);
            }
            for (Section section2 : sections) {
                sectionMap.put(section2.getId(), section2);
            }
            for (Article article : articles) {
                section2 = (Section) sectionMap.get(article.getSectionId());
                flatArticles.add(new FlatArticle((Category) categoryMap.get(section2.getCategoryId()), section2, article));
            }
        } else {
            Logger.d(LOG_TAG, "There are no articles contained in this account", new Object[0]);
            flatArticles = Collections.emptyList();
        }
        Collections.sort(flatArticles);
        return flatArticles;
    }
}
