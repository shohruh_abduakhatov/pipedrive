package com.pipedrive.chrometab;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.CustomTabsIntent.Builder;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import com.pipedrive.BaseActivityBase;
import com.pipedrive.util.PipedriveUtil;
import org.chromium.customtabsclient.activity.CustomTabActivityHelper;
import org.chromium.customtabsclient.activity.CustomTabActivityHelper.ConnectionCallback;
import org.chromium.customtabsclient.activity.CustomTabActivityHelper.CustomTabFallback;

public class BaseChromeTabActivity extends BaseActivityBase implements ConnectionCallback {
    private static final String KEY_URI = "URI";
    private CustomTabActivityHelper customTabActivityHelper;

    @MainThread
    public static void start(@NonNull Activity callerActivity, @NonNull String uri) {
        ContextCompat.startActivity(callerActivity, new Intent(callerActivity, BaseChromeTabActivity.class).putExtra(KEY_URI, uri), ActivityOptionsCompat.makeBasic().toBundle());
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.customTabActivityHelper = new CustomTabActivityHelper();
        this.customTabActivityHelper.setConnectionCallback(this);
    }

    protected void onDestroy() {
        super.onDestroy();
        this.customTabActivityHelper.setConnectionCallback(null);
    }

    public void onCustomTabsConnected() {
        openChromeTabForSignUp();
        finish();
    }

    public void onCustomTabsDisconnected() {
    }

    protected void onStart() {
        super.onStart();
        this.customTabActivityHelper.bindCustomTabsService(this);
    }

    protected void onStop() {
        super.onStop();
        this.customTabActivityHelper.unbindCustomTabsService(this);
    }

    private void openChromeTabForSignUp() {
        CustomTabsIntent customTabsIntent = new Builder(this.customTabActivityHelper.getSession()).enableUrlBarHiding().build();
        customTabsIntent.intent.addFlags(1073741824);
        CustomTabActivityHelper.openCustomTab(this, customTabsIntent, Uri.parse(getIntent().getStringExtra(KEY_URI)), new CustomTabFallback() {
            public void openUri(@NonNull Activity activity, @NonNull Uri uri) {
                PipedriveUtil.openWebPage(activity, uri);
            }
        });
    }
}
