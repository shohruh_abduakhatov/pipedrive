package com.pipedrive.tasks;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;

public abstract class AsyncTaskWithCallback<Params, Progress> extends AsyncTask<Params, Progress, ResultContainer<Params>> {
    private OnTaskFinished<Params> mOnTaskFinished = null;

    public interface OnTaskFinished<Params> {

        public enum TaskResult {
            SUCCEEDED,
            FAILED
        }

        void taskFinished(Session session, TaskResult taskResult, Params... paramsArr);
    }

    static class ResultContainer<Params> {
        Params[] params = null;
        TaskResult taskResult = null;

        ResultContainer() {
        }
    }

    protected abstract TaskResult doInBackgroundWithCallback(Params... paramsArr);

    public AsyncTaskWithCallback(@NonNull Session session, @Nullable OnTaskFinished<Params> onTaskFinished) {
        super(session);
        this.mOnTaskFinished = onTaskFinished;
    }

    protected final ResultContainer<Params> doInBackground(Params... params) {
        ResultContainer<Params> result = new ResultContainer();
        result.taskResult = doInBackgroundWithCallback(params);
        result.params = params;
        return result;
    }

    protected final void onPostExecute(ResultContainer<Params> result) {
        onPostExecuteBeforeCallback(result.taskResult, result.params);
        if (this.mOnTaskFinished != null) {
            this.mOnTaskFinished.taskFinished(getSession(), result.taskResult, result.params);
        }
    }

    protected void onPostExecuteBeforeCallback(TaskResult taskResult, Params... paramsArr) {
    }
}
