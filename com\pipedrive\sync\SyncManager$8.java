package com.pipedrive.sync;

class SyncManager$8 implements SyncManager$OnReleaseSplashScreen {
    final /* synthetic */ SyncManager this$0;
    final /* synthetic */ Runnable val$onEnoughDataToShowUISingleRunnable;

    SyncManager$8(SyncManager this$0, Runnable runnable) {
        this.this$0 = this$0;
        this.val$onEnoughDataToShowUISingleRunnable = runnable;
    }

    public void release() {
        this.val$onEnoughDataToShowUISingleRunnable.run();
    }
}
