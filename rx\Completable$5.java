package rx;

import rx.functions.Func0;
import rx.subscriptions.Subscriptions;

class Completable$5 implements Completable$OnSubscribe {
    final /* synthetic */ Func0 val$completableFunc0;

    Completable$5(Func0 func0) {
        this.val$completableFunc0 = func0;
    }

    public void call(CompletableSubscriber s) {
        try {
            Completable c = (Completable) this.val$completableFunc0.call();
            if (c == null) {
                s.onSubscribe(Subscriptions.unsubscribed());
                s.onError(new NullPointerException("The completable returned is null"));
                return;
            }
            c.unsafeSubscribe(s);
        } catch (Throwable e) {
            s.onSubscribe(Subscriptions.unsubscribed());
            s.onError(e);
        }
    }
}
