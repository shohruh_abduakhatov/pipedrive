package com.pipedrive.views.viewholder.flow;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class EmailRowViewHolder_ViewBinding implements Unbinder {
    private EmailRowViewHolder target;

    @UiThread
    public EmailRowViewHolder_ViewBinding(EmailRowViewHolder target, View source) {
        this.target = target;
        target.subject = (TextView) Utils.findRequiredViewAsType(source, R.id.rowFlowFile_Subject, "field 'subject'", TextView.class);
        target.parties = (TextView) Utils.findRequiredViewAsType(source, R.id.rowFlowFile_Participants, "field 'parties'", TextView.class);
        target.summary = (TextView) Utils.findRequiredViewAsType(source, R.id.rowFlowFile_Summary, "field 'summary'", TextView.class);
        target.attachmentIcon = (ImageView) Utils.findRequiredViewAsType(source, R.id.rowFlowFile_AttachmentIcon, "field 'attachmentIcon'", ImageView.class);
    }

    @CallSuper
    public void unbind() {
        EmailRowViewHolder target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.subject = null;
        target.parties = null;
        target.summary = null;
        target.attachmentIcon = null;
    }
}
