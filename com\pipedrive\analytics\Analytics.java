package com.pipedrive.analytics;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders.AppViewBuilder;
import com.google.android.gms.analytics.HitBuilders.EventBuilder;
import com.google.android.gms.analytics.Tracker;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;

public enum Analytics {
    ;
    
    public static final boolean IS_ANALYTICS_ENABLED = true;

    public static void hitScreen(@NonNull Activity activity) {
        hitScreenForGoogleAnalytics(ScreensMapper.getScreenName(activity), activity);
    }

    public static void hitFragment(@NonNull Fragment fragment) {
        hitScreenForGoogleAnalytics(ScreensMapper.getScreenNameForSupportFragment(fragment), fragment.getActivity());
    }

    public static void hitFragment(@NonNull android.app.Fragment fragment) {
        hitScreenForGoogleAnalytics(ScreensMapper.getScreenNameForNativeFragment(fragment), fragment.getActivity());
    }

    public static void hitScreen(@Nullable String screen, @NonNull Context ctx) {
        hitScreenForGoogleAnalytics(screen, ctx);
    }

    private static void hitScreenForGoogleAnalytics(@Nullable String screen, @NonNull Context context) {
        if (!isAnalyticsDisabled(context) && screen != null) {
            Tracker tracker = PipedriveApp.getTracker(context);
            if (tracker != null) {
                tracker.setScreenName(screen);
                tracker.send(new AppViewBuilder().build());
            }
        }
    }

    public static void dispatchData(Context context) {
        if (!isAnalyticsDisabled(context)) {
            GoogleAnalytics.getInstance(context).dispatchLocalHits();
        }
    }

    public static void setAppOptOut(Context context, boolean optOut) {
        GoogleAnalytics.getInstance(context).setAppOptOut(optOut);
    }

    private static boolean isAnalyticsDisabled(@NonNull Context context) {
        return GoogleAnalytics.getInstance(context).getAppOptOut();
    }

    public static void sendEvent(@NonNull Context context, @NonNull AnalyticsEvent event) {
        if (!isAnalyticsDisabled(context)) {
            Tracker tracker = PipedriveApp.getTracker(context);
            if (tracker != null) {
                tracker.send(new EventBuilder(event.getCategory(), event.getAction()).setLabel(event.getAction()).build());
            }
        }
    }

    public static void sendEvent(@NonNull AnalyticsEvent event) {
        Session session = PipedriveApp.getActiveSession();
        if (session != null) {
            sendEvent(session.getApplicationContext(), event);
        }
    }
}
