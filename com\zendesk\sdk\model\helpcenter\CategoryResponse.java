package com.zendesk.sdk.model.helpcenter;

import android.support.annotation.Nullable;

public class CategoryResponse {
    private Category category;

    @Nullable
    public Category getCategory() {
        return this.category;
    }
}
