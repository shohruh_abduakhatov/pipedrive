package com.pipedrive.model;

public abstract class BaseFile extends BaseDatasourceEntity {
    private String mFileName;
    private Long mFileSize;
    private String mFileType;
    private String mUrl;

    public abstract String getName();

    public String getFileName() {
        return this.mFileName;
    }

    public void setFileName(String fileName) {
        this.mFileName = fileName;
    }

    public String getFileType() {
        return this.mFileType;
    }

    public void setFileType(String fileType) {
        this.mFileType = fileType;
    }

    public Long getFileSize() {
        return this.mFileSize;
    }

    public void setFileSize(Long fileSize) {
        this.mFileSize = fileSize;
    }

    public String getUrl() {
        return this.mUrl;
    }

    public void setUrl(String url) {
        this.mUrl = url;
    }

    public String getHumanReadableFileSize() {
        long bytes = this.mFileSize == null ? 0 : this.mFileSize.longValue();
        if (bytes < ((long) 1000)) {
            return bytes + " B";
        }
        char pre = "kMGTPE".charAt(((int) (Math.log((double) bytes) / Math.log((double) 1000))) - 1);
        return String.format("%.1f%sB", new Object[]{Double.valueOf(((double) bytes) / Math.pow((double) 1000, (double) ((int) (Math.log((double) bytes) / Math.log((double) 1000))))), Character.valueOf(pre)});
    }
}
