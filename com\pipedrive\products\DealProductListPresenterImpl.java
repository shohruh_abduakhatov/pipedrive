package com.pipedrive.products;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.products.DealProductsDataSource;
import com.pipedrive.model.Deal;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.util.networking.ConnectionUtil;
import java.util.List;

class DealProductListPresenterImpl extends DealProductListPresenter {
    private final OnCompletedListener ON_COMPLETED_CALLBACK = new OnCompletedListener() {
        public void onDealProductsDownloadCompleted() {
            if (DealProductListPresenterImpl.this.mRelatedDeal == null) {
                DealProductListPresenterImpl.this.onDownloadProductListFail();
            } else if (DealProductListPresenterImpl.this.mRelatedDeal.getSqlIdOrNull() == null) {
                DealProductListPresenterImpl.this.onDownloadProductListFail();
            } else {
                List<DealProduct> dealProductList = new DealProductsDataSource(DealProductListPresenterImpl.this.getSession().getDatabase()).findAllActiveDealProductsRelatedToDealSqlId(DealProductListPresenterImpl.this.mRelatedDeal.getSqlIdOrNull());
                if (dealProductList.isEmpty()) {
                    DealProductListPresenterImpl.this.onDownloadProductListFail();
                } else {
                    DealProductListPresenterImpl.this.onDownloadProductListSuccess(dealProductList, DealProductListPresenterImpl.this.mRelatedDeal);
                }
            }
        }
    };
    private final OnErrorListener ON_ERROR_CALLBACK = new OnErrorListener() {
        public void onDealProductsDownloadFailedWithError(@Nullable Throwable throwable) {
            DealProductListPresenterImpl.this.onDownloadProductListFail();
        }
    };
    @Nullable
    private Deal mRelatedDeal;

    DealProductListPresenterImpl(@NonNull Session session) {
        super(session);
    }

    void requestDealProductList(@NonNull Long dealSqlId, boolean lastOperationWasDealProductRemoval) {
        this.mRelatedDeal = (Deal) new DealsDataSource(getSession().getDatabase()).findBySqlId(dealSqlId.longValue());
        if (this.mRelatedDeal == null) {
            onDownloadProductListFail();
        } else if (this.mRelatedDeal.getSqlIdOrNull() == null) {
            onDownloadProductListFail();
        } else {
            DealProductsDataSource dealProductsDataSource = new DealProductsDataSource(getSession().getDatabase());
            if (lastOperationWasDealProductRemoval && dealProductsDataSource.getDealProductCount(dealSqlId) == 0) {
                if (getView() != null) {
                    ((DealProductListView) getView()).onLastDealProductRemoved();
                }
            } else if (ConnectionUtil.isConnected(getSession().getApplicationContext())) {
                if (getView() != null) {
                    ((DealProductListView) getView()).showProgressBar();
                }
                DownloadDealProductListTask downloadDealProductListTask = new DownloadDealProductListTask(getSession(), dealSqlId, this.ON_COMPLETED_CALLBACK, this.ON_ERROR_CALLBACK);
                if (!downloadDealProductListTask.isExecuting()) {
                    downloadDealProductListTask.executeOnAsyncTaskExecutor(this, new Void[0]);
                }
            } else {
                List<DealProduct> dealProductList = dealProductsDataSource.findAllActiveDealProductsRelatedToDealSqlId(this.mRelatedDeal.getSqlIdOrNull());
                if (dealProductList.isEmpty()) {
                    onDownloadProductListFail();
                } else {
                    onDownloadProductListSuccess(dealProductList, this.mRelatedDeal);
                }
            }
        }
    }

    private void onDownloadProductListFail() {
        if (getView() != null) {
            ((DealProductListView) getView()).onDealProductListRequestFail();
            ((DealProductListView) getView()).hideProgressBar();
        }
    }

    private void onDownloadProductListSuccess(@NonNull List<DealProduct> dealProductList, @NonNull Deal relatedDeal) {
        if (getView() != null) {
            ((DealProductListView) getView()).onDealProductListRequestSuccess(dealProductList, relatedDeal);
            ((DealProductListView) getView()).hideProgressBar();
        }
    }
}
