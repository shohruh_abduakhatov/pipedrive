package com.pipedrive.tasks;

import com.pipedrive.TrialExpiredActivity;
import com.pipedrive.application.Session;

public class PaymentRequiredHandlerTask extends NonSessionDependentTask<Void, Void, Void> {
    public PaymentRequiredHandlerTask(Session session) {
        super(session);
    }

    protected Void doInBackground(Void... params) {
        return null;
    }

    protected void onPostExecute(Void aVoid) {
        Session session = getSession();
        session.enableLastProcessChangesAndRecents(false);
        if (!session.getRuntimeCache().isTrialExpiredActivityRequested) {
            session.getRuntimeCache().isTrialExpiredActivityRequested = true;
            TrialExpiredActivity.startActivity(session);
        }
    }
}
