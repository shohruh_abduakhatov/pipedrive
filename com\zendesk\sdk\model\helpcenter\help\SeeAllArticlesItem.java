package com.zendesk.sdk.model.helpcenter.help;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Collections;
import java.util.List;

public class SeeAllArticlesItem implements HelpItem {
    private boolean isLoading;
    private SectionItem section;

    public SeeAllArticlesItem(SectionItem sectionItem) {
        this.section = sectionItem;
    }

    public int getViewType() {
        return 4;
    }

    @NonNull
    public String getName() {
        return "";
    }

    @Nullable
    public Long getId() {
        return this.section.getId();
    }

    @Nullable
    public Long getParentId() {
        return this.section.getParentId();
    }

    @NonNull
    public List<? extends HelpItem> getChildren() {
        return Collections.emptyList();
    }

    public SectionItem getSection() {
        return this.section;
    }

    public boolean isLoading() {
        return this.isLoading;
    }

    public void setLoading(boolean loading) {
        this.isLoading = loading;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SeeAllArticlesItem that = (SeeAllArticlesItem) o;
        if (this.section != null) {
            return this.section.equals(that.section);
        }
        if (that.section != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.section != null ? this.section.hashCode() : 0;
    }
}
