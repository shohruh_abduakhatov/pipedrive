package com.pipedrive.views.hinted;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff.Mode;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewStub;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.pipedrive.R;

public abstract class BaseHintedLayout<V extends View> extends FrameLayout {
    private int mAccentColor;
    private ColorStateList mDefaultHintTextColor;
    @BindView(2131821155)
    TextView mHint;
    @NonNull
    protected V mMainView;
    @BindView(2131820640)
    View mUnderline;
    @BindView(2131821156)
    ViewStub mViewStub;

    protected abstract int getMainViewLayoutResource();

    public BaseHintedLayout(Context context) {
        this(context, null);
    }

    public BaseHintedLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseHintedLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupLayout(context);
        setupAttributes(context, attrs);
        setupColors(context);
        setupTintingOnFocus();
    }

    @NonNull
    public static <V extends View> V getMainView(@NonNull Activity activity, int id) {
        return ((BaseHintedLayout) ButterKnife.findById(activity, id)).getMainView();
    }

    @NonNull
    public static <V extends View> V getMainView(@NonNull View view, int id) {
        return ((BaseHintedLayout) ButterKnife.findById(view, id)).getMainView();
    }

    private void setupTintingOnFocus() {
        this.mMainView.setOnFocusChangeListener(new OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    BaseHintedLayout.this.applyFocusTinting();
                } else {
                    BaseHintedLayout.this.resetFocusTinting();
                }
            }
        });
    }

    private void resetFocusTinting() {
        this.mHint.setTextColor(this.mDefaultHintTextColor);
        this.mUnderline.getBackground().clearColorFilter();
    }

    private void applyFocusTinting() {
        this.mHint.setTextColor(this.mAccentColor);
        this.mUnderline.getBackground().setColorFilter(this.mAccentColor, Mode.SRC_IN);
    }

    private void setupColors(Context context) {
        this.mDefaultHintTextColor = this.mHint.getTextColors();
        TypedValue accentColor = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorAccent, accentColor, true);
        this.mAccentColor = accentColor.data;
    }

    private void setupAttributes(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.BaseHintedLayout, 0, 0);
        try {
            this.mHint.setText(typedArray.getString(0));
        } finally {
            typedArray.recycle();
        }
    }

    private void setupLayout(@NonNull Context context) {
        LayoutInflater.from(context).inflate(R.layout.view_hinted_base, this, true);
        ButterKnife.bind(this);
        this.mViewStub.setLayoutResource(getMainViewLayoutResource());
        this.mMainView = this.mViewStub.inflate();
    }

    @NonNull
    V getMainView() {
        return this.mMainView;
    }
}
