package com.pipedrive.views.viewholder.activity;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import butterknife.BindView;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datetime.LocaleHelper;
import com.pipedrive.datetime.PipedriveDate;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.flow.model.FlowItemEntity;
import com.pipedrive.model.Activity;
import com.pipedrive.model.Activity.ActivityBuilderForDataSource.ActivityStartDateTimeType;
import com.pipedrive.model.ActivityType;

public class ActivityRowViewHolderWithCheckbox extends ActivityRowViewHolder {
    @BindView(2131821045)
    CheckBox mDone;

    public interface ActivityDoneChangedListener {
        void onActivityDoneStatusChanged(@NonNull Session session, long j, boolean z);
    }

    public int getLayoutResourceId() {
        return R.layout.row_activity_with_checkbox;
    }

    public void fill(@NonNull final Session session, @NonNull Context context, @NonNull final Activity activity, @Nullable final ActivityDoneChangedListener activityDoneChangedListener) {
        super.fill(session, context, activity);
        this.mDone.setOnCheckedChangeListener(null);
        this.mDone.setChecked(activity.isDone());
        if (activityDoneChangedListener == null) {
            this.mDone.setOnCheckedChangeListener(null);
        } else {
            this.mDone.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    long activitySqlId = activity.getSqlId();
                    if (activitySqlId > 0) {
                        activityDoneChangedListener.onActivityDoneStatusChanged(session, activitySqlId, isChecked);
                    }
                }
            });
        }
    }

    public void fill(@NonNull Session session, @NonNull Context context, @NonNull FlowItemEntity flowItemEntity, OnCheckedChangeListener onCheckedChangeListener) {
        String dateString;
        boolean overdue;
        boolean dueToday;
        PipedriveDateTime activityDateTimeInUtc = flowItemEntity.getTimestamp();
        boolean isActivityStartDateTimeTypeOfDate = flowItemEntity.getExtraNumber().longValue() == ((long) ActivityStartDateTimeType.Date.getSqlValue());
        if (isActivityStartDateTimeTypeOfDate) {
            dateString = LocaleHelper.getLocaleBasedDateStringInCurrentTimeZone(session, activityDateTimeInUtc);
        } else {
            dateString = LocaleHelper.getLocaleBasedDateTimeStringInCurrentTimeZone(session, activityDateTimeInUtc);
        }
        boolean done = flowItemEntity.getExtraBoolean().booleanValue();
        if (isActivityStartDateTimeTypeOfDate) {
            overdue = Activity.isOverdueFor(PipedriveDate.instanceFromUnixTimeRepresentation(Long.valueOf(activityDateTimeInUtc.getRepresentationInUnixTime())));
        } else {
            overdue = Activity.isOverdueFor(activityDateTimeInUtc);
        }
        if (isActivityStartDateTimeTypeOfDate) {
            dueToday = Activity.isDueTodayFor(PipedriveDate.instanceFromUnixTimeRepresentation(Long.valueOf(activityDateTimeInUtc.getRepresentationInUnixTime())));
        } else {
            dueToday = Activity.isDueTodayFor(activityDateTimeInUtc);
        }
        String note = flowItemEntity.getExtraString2();
        this.mDone.setOnCheckedChangeListener(null);
        fill(context, ActivityType.getActivityTypeByName(session, flowItemEntity.getExtraString()), flowItemEntity.getTitle(), flowItemEntity.getPersonName(), flowItemEntity.getOrgName(), dateString, done, Boolean.valueOf(overdue), Boolean.valueOf(dueToday), note);
        this.mDone.setChecked(done);
        this.mDone.setOnCheckedChangeListener(onCheckedChangeListener);
    }
}
