package com.google.android.gms.wearable.internal;

import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataItem;

public class zzy implements DataEvent {
    private DataItem aTI;
    private int nV;

    public zzy(DataEvent dataEvent) {
        this.nV = dataEvent.getType();
        this.aTI = (DataItem) dataEvent.getDataItem().freeze();
    }

    public /* synthetic */ Object freeze() {
        return zzcmu();
    }

    public DataItem getDataItem() {
        return this.aTI;
    }

    public int getType() {
        return this.nV;
    }

    public boolean isDataValid() {
        return true;
    }

    public String toString() {
        String str = getType() == 1 ? "changed" : getType() == 2 ? "deleted" : "unknown";
        String valueOf = String.valueOf(getDataItem());
        return new StringBuilder((String.valueOf(str).length() + 35) + String.valueOf(valueOf).length()).append("DataEventEntity{ type=").append(str).append(", dataitem=").append(valueOf).append(" }").toString();
    }

    public DataEvent zzcmu() {
        return this;
    }
}
