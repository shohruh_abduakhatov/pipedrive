package com.zendesk.sdk.rating;

import android.view.View.OnClickListener;
import java.io.Serializable;

public interface RateMyAppButton extends Serializable {
    int getId();

    String getLabel();

    OnClickListener getOnClickListener();

    int getStyleAttributeId();

    boolean shouldDismissDialog();
}
