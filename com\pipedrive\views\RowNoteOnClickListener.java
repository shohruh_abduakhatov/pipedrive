package com.pipedrive.views;

import android.view.View;

public interface RowNoteOnClickListener {
    void onCancelClick(View view);

    void onRowTextClick(View view);
}
