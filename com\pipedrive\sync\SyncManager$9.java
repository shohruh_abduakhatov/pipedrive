package com.pipedrive.sync;

import com.pipedrive.application.Session;

class SyncManager$9 implements SyncManager$OnReleaseSplashScreen {
    final /* synthetic */ SyncManager this$0;
    final /* synthetic */ SyncManager$OnReleaseSplashScreen val$onReleaseSplashScreen;
    final /* synthetic */ Session val$session;

    SyncManager$9(SyncManager this$0, Session session, SyncManager$OnReleaseSplashScreen syncManager$OnReleaseSplashScreen) {
        this.this$0 = this$0;
        this.val$session = session;
        this.val$onReleaseSplashScreen = syncManager$OnReleaseSplashScreen;
    }

    public void release() {
        this.val$session.enableLastProcessChangesAndRecents(true);
        if (this.val$onReleaseSplashScreen != null) {
            this.val$onReleaseSplashScreen.release();
        }
    }
}
