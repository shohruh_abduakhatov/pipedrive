package com.google.android.gms.wearable.internal;

import android.content.IntentFilter;
import android.net.Uri;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzrr;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageApi.MessageListener;
import com.google.android.gms.wearable.MessageApi.SendMessageResult;

public final class zzaz implements MessageApi {

    private static final class zza extends zzi<Status> {
        private zzrr<MessageListener> Bt;
        private MessageListener aUe;
        private IntentFilter[] aUf;

        private zza(GoogleApiClient googleApiClient, MessageListener messageListener, zzrr<MessageListener> com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_MessageApi_MessageListener, IntentFilter[] intentFilterArr) {
            super(googleApiClient);
            this.aUe = (MessageListener) zzaa.zzy(messageListener);
            this.Bt = (zzrr) zzaa.zzy(com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_MessageApi_MessageListener);
            this.aUf = (IntentFilter[]) zzaa.zzy(intentFilterArr);
        }

        protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
            com_google_android_gms_wearable_internal_zzbp.zza((com.google.android.gms.internal.zzqo.zzb) this, this.aUe, this.Bt, this.aUf);
            this.aUe = null;
            this.Bt = null;
            this.aUf = null;
        }

        public Status zzb(Status status) {
            this.aUe = null;
            this.Bt = null;
            this.aUf = null;
            return status;
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    public static class zzb implements SendMessageResult {
        private final int IS;
        private final Status hv;

        public zzb(Status status, int i) {
            this.hv = status;
            this.IS = i;
        }

        public int getRequestId() {
            return this.IS;
        }

        public Status getStatus() {
            return this.hv;
        }
    }

    private PendingResult<Status> zza(GoogleApiClient googleApiClient, MessageListener messageListener, IntentFilter[] intentFilterArr) {
        return googleApiClient.zza(new zza(googleApiClient, messageListener, googleApiClient.zzs(messageListener), intentFilterArr));
    }

    public PendingResult<Status> addListener(GoogleApiClient googleApiClient, MessageListener messageListener) {
        return zza(googleApiClient, messageListener, new IntentFilter[]{zzbn.zzrp(MessageApi.ACTION_MESSAGE_RECEIVED)});
    }

    public PendingResult<Status> addListener(GoogleApiClient googleApiClient, MessageListener messageListener, Uri uri, int i) {
        zzaa.zzb(uri != null, (Object) "uri must not be null");
        boolean z = i == 0 || i == 1;
        zzaa.zzb(z, (Object) "invalid filter type");
        return zza(googleApiClient, messageListener, new IntentFilter[]{zzbn.zza(MessageApi.ACTION_MESSAGE_RECEIVED, uri, i)});
    }

    public PendingResult<Status> removeListener(GoogleApiClient googleApiClient, final MessageListener messageListener) {
        return googleApiClient.zza(new zzi<Status>(this, googleApiClient) {
            final /* synthetic */ zzaz aUc;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zza((com.google.android.gms.internal.zzqo.zzb) this, messageListener);
            }

            public Status zzb(Status status) {
                return status;
            }

            public /* synthetic */ Result zzc(Status status) {
                return zzb(status);
            }
        });
    }

    public PendingResult<SendMessageResult> sendMessage(GoogleApiClient googleApiClient, String str, String str2, byte[] bArr) {
        final String str3 = str;
        final String str4 = str2;
        final byte[] bArr2 = bArr;
        return googleApiClient.zza(new zzi<SendMessageResult>(this, googleApiClient) {
            final /* synthetic */ zzaz aUc;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zza((com.google.android.gms.internal.zzqo.zzb) this, str3, str4, bArr2);
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzey(status);
            }

            protected SendMessageResult zzey(Status status) {
                return new zzb(status, -1);
            }
        });
    }
}
