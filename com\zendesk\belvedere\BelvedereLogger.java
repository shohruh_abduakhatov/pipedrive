package com.zendesk.belvedere;

import android.support.annotation.NonNull;

public interface BelvedereLogger {
    void d(@NonNull String str, @NonNull String str2);

    void e(@NonNull String str, @NonNull String str2);

    void e(@NonNull String str, @NonNull String str2, @NonNull Throwable th);

    void setLoggable(boolean z);

    void w(@NonNull String str, @NonNull String str2);
}
