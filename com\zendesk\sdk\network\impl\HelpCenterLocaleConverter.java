package com.zendesk.sdk.network.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.zendesk.util.StringUtils;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

class HelpCenterLocaleConverter {
    private static final Map<String, String> forwardLookupMap = new HashMap();

    HelpCenterLocaleConverter() {
    }

    static {
        forwardLookupMap.put("iw", "he");
        forwardLookupMap.put("nb", "no");
        forwardLookupMap.put("in", PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID);
        forwardLookupMap.put("ji", "yi");
    }

    @NonNull
    String toHelpCenterLocaleString(@Nullable Locale locale) {
        boolean isUsableLocale = locale != null && StringUtils.hasLength(locale.getLanguage());
        Locale deviceLocale = isUsableLocale ? locale : Locale.getDefault();
        String remappedLanguage = (String) forwardLookupMap.get(deviceLocale.getLanguage());
        if (!StringUtils.hasLength(remappedLanguage)) {
            remappedLanguage = deviceLocale.getLanguage();
        }
        StringBuilder localeBuilder = new StringBuilder(remappedLanguage);
        if (StringUtils.hasLength(deviceLocale.getCountry())) {
            localeBuilder.append("-").append(deviceLocale.getCountry());
        }
        return localeBuilder.toString();
    }
}
