package com.pipedrive.views;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import com.pipedrive.R;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.util.networking.ConnectionUtil;

public class OnlineOnClickListener implements OnClickListener {
    private Context mContext = null;
    private boolean mShowNotificationWhenOffline = false;

    public OnlineOnClickListener(Context context, boolean showNotificationWhenOffline) {
        this.mContext = context;
        this.mShowNotificationWhenOffline = showNotificationWhenOffline;
    }

    public void onClick(View v) {
        if (this.mContext != null) {
            boolean isOnline = ConnectionUtil.isConnected(this.mContext);
            if (isOnline) {
                onClickWhenOnline(v);
            } else {
                onClickWhenOffline(v);
            }
            if (this.mShowNotificationWhenOffline && !isOnline) {
                ViewUtil.showErrorToast(this.mContext, (int) R.string.error_operation_made_in_offline_mode);
            }
        }
    }

    public void onClickWhenOnline(View v) {
    }

    public void onClickWhenOffline(View v) {
    }
}
