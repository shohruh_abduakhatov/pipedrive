package com.pipedrive.tasks.authorization;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;

public interface OnEnoughDataToShowUI {
    void readyToShowUI(@NonNull Session session);
}
