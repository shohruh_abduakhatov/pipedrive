package com.pipedrive.tasks.organizations;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.newrelic.agent.android.util.SafeJsonPrimitive;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.tasks.AsyncTaskWithCallback;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished.TaskResult;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.Response;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class OrganizationAddressFieldTask extends AsyncTaskWithCallback<String, Integer> {
    public OrganizationAddressFieldTask(@NonNull Session session, @Nullable OnTaskFinished<String> onTaskFinished) {
        super(session, onTaskFinished);
    }

    protected TaskResult doInBackgroundWithCallback(String... params) {
        downloadOrganizationFields(getSession());
        return TaskResult.SUCCEEDED;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void downloadOrganizationFields(@NonNull Session session) {
        Map<Integer, String> addressFields = new TreeMap();
        InputStream in = getOrganizationFieldsStream(session);
        if (in != null) {
            JsonReader reader = new JsonReader(new InputStreamReader(in, HttpRequest.CHARSET_UTF8));
            reader.beginObject();
            while (reader.hasNext()) {
                String orgField = reader.nextName();
                if (!Response.JSON_PARAM_SUCCESS.equals(orgField)) {
                    try {
                        if (Response.JSON_PARAM_DATA.equals(orgField) && reader.peek() == JsonToken.BEGIN_ARRAY) {
                            reader.beginArray();
                            while (reader.hasNext()) {
                                String fieldType = null;
                                int orderNr = 0;
                                String key = null;
                                reader.beginObject();
                                while (reader.hasNext()) {
                                    orgField = reader.nextName();
                                    if ("field_type".equals(orgField) && reader.peek() == JsonToken.STRING) {
                                        fieldType = reader.nextString();
                                    } else if ("key".equals(orgField) && reader.peek() == JsonToken.STRING) {
                                        key = reader.nextString();
                                    } else if (PipeSQLiteHelper.COLUMN_ORDER_NR.equals(orgField) && reader.peek() == JsonToken.NUMBER) {
                                        orderNr = reader.nextInt();
                                    } else {
                                        reader.skipValue();
                                    }
                                }
                                reader.endObject();
                                if (!(!"address".equalsIgnoreCase(fieldType) || key == null || "".equalsIgnoreCase(key) || SafeJsonPrimitive.NULL_STRING.equalsIgnoreCase(key))) {
                                    addressFields.put(Integer.valueOf(orderNr), key);
                                }
                            }
                            reader.endArray();
                        } else {
                            reader.skipValue();
                        }
                    } catch (Exception e) {
                        Log.e(e);
                    } catch (Throwable th) {
                        try {
                            in.close();
                        } catch (IOException e2) {
                        }
                    }
                } else if (!reader.nextBoolean()) {
                    reader.close();
                }
            }
            reader.endObject();
            reader.close();
            try {
                in.close();
            } catch (IOException e3) {
            }
            for (Entry<Integer, String> entry : addressFields.entrySet()) {
                if (entry != null && entry.getValue() != null && !((String) entry.getValue()).isEmpty() && !SafeJsonPrimitive.NULL_STRING.equalsIgnoreCase((String) entry.getValue())) {
                    session.setOrgAddressField((String) entry.getValue());
                    return;
                }
            }
        }
    }

    @Nullable
    private static InputStream getOrganizationFieldsStream(@NonNull Session session) {
        String jsonFilter = ":(id,order_nr,key,field_type)";
        ApiUrlBuilder apiUriBuilder = new ApiUrlBuilder(session, ApiUrlBuilder.ENDPOINT_ORGANIZATION_FIELDS);
        apiUriBuilder.appendEncodedPath(":(id,order_nr,key,field_type)");
        return ConnectionUtil.requestGet(apiUriBuilder);
    }
}
