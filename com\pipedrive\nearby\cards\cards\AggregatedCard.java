package com.pipedrive.nearby.cards.cards;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.PluralsRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import com.google.android.gms.maps.model.LatLng;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.nearby.cards.adapter.CardAdapterForAggregatedItemsList;
import com.pipedrive.nearby.model.NearbyItem;
import com.pipedrive.views.DividerItemDecorationNoDividerAfterLastItem;
import java.util.List;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;

abstract class AggregatedCard<NEARBY_ITEM extends NearbyItem<? extends BaseDatasourceEntity>> extends Card<NEARBY_ITEM> {
    @BindView(2131821133)
    TextView directionsButtonTitle;
    private final LinearLayoutManager layoutManager;
    @BindView(2131821132)
    RecyclerView recyclerView;
    @BindView(2131820558)
    TextView subtitleTextView;
    @BindView(2131820559)
    TextView titleTextView;

    @NonNull
    abstract CardAdapterForAggregatedItemsList getAdapter(@NonNull List<? extends NearbyItem> list);

    @NonNull
    abstract Observable<String> getAggregatedInformation(@NonNull Session session, @NonNull NEARBY_ITEM nearby_item);

    @PluralsRes
    @NonNull
    abstract Integer getQuantityStringResId();

    abstract Observable<List<NEARBY_ITEM>> getSingleItemsList(@NonNull NEARBY_ITEM nearby_item);

    AggregatedCard(@NonNull ViewGroup parent, @LayoutRes @NonNull Integer layoutResId) {
        super(parent, layoutResId);
        this.layoutManager = new LinearLayoutManager(parent.getContext(), 1, false);
        this.layoutManager.setRecycleChildrenOnDetach(true);
        this.recyclerView.setLayoutManager(this.layoutManager);
        this.recyclerView.addItemDecoration(new DividerItemDecorationNoDividerAfterLastItem(parent.getContext()));
    }

    public void bind(@NonNull Session session, @NonNull final NEARBY_ITEM nearbyItem, @NonNull LatLng currentLocation) {
        super.bind(session, nearbyItem, currentLocation);
        addSubscription(nearbyItem.getItemSqlIds().count().map(new Func1<Integer, String>() {
            public String call(Integer count) {
                return AggregatedCard.this.getResources().getQuantityString(AggregatedCard.this.getQuantityStringResId().intValue(), count.intValue(), new Object[]{count});
            }
        }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<String>() {
            public void call(String title) {
                AggregatedCard.this.titleTextView.setText(title);
            }
        }, getDefaultOnErrorActionForCards()));
        addSubscription(getAggregatedInformation(session, nearbyItem).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<String>() {
            public void call(String additionalInformationString) {
                AggregatedCard.this.subtitleTextView.setText(AggregatedCard.this.getDistanceString() + AggregatedCard.this.subtitleSeparator + additionalInformationString);
                AggregatedCard.this.directionsButtonTitle.setText(AggregatedCard.this.getDistanceString() + AggregatedCard.this.subtitleSeparator + nearbyItem.getAddress());
            }
        }, getDefaultOnErrorActionForCards()));
        addSubscription(getSingleItemsList(nearbyItem).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<List<? extends NearbyItem>>() {
            public void call(List<? extends NearbyItem> nearbyItems) {
                AggregatedCard.this.layoutManager.setInitialPrefetchItemCount(Math.min(nearbyItems.size(), AggregatedCard.this.getInitialPrefetch()));
                AggregatedCard.this.recyclerView.setAdapter(AggregatedCard.this.getAdapter(nearbyItems));
            }
        }, getDefaultOnErrorActionForCards()));
    }

    int getInitialPrefetch() {
        return this.itemView.getContext().getResources().getInteger(R.integer.default_initial_prefetch_items);
    }

    void expand(@NonNull Boolean animate) {
        this.headerContainer.setVisibility(8);
        super.expand(animate);
    }

    void collapse(@NonNull Boolean animate) {
        this.headerContainer.setVisibility(0);
        super.collapse(animate);
    }
}
