package com.pipedrive.whatsnew;

import android.support.v4.view.ViewPager.PageTransformer;
import android.view.View;

public class FadeInOutPageTransformer implements PageTransformer {
    public void transformPage(View page, float position) {
        page.setAlpha(1.0f - (Math.abs(position) * 2.0f));
    }
}
