package com.pipedrive.views.calendar.recyclerview;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.util.AttributeSet;
import java.util.Calendar;

public class CalendarRecyclerView extends RecyclerView {
    public CalendarRecyclerView(Context context) {
        this(context, null);
    }

    public CalendarRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            setLayoutManager(new LinearLayoutManager(context, 0, false));
            new PagerSnapHelper().attachToRecyclerView(this);
        }
    }

    public void onScrolled(int dx, int dy) {
        if (dx != 0) {
            int completelyVisibleAgendaPosition = ((LinearLayoutManager) getLayoutManager()).findFirstCompletelyVisibleItemPosition();
            Adapter adapter = getAdapter();
            if (completelyVisibleAgendaPosition > -1 && completelyVisibleAgendaPosition < adapter.getItemCount()) {
                Calendar currentlyShownDate = ((CalendarRecyclerViewAdapter) adapter).getItem(completelyVisibleAgendaPosition);
                CalendarScrollListener calendarScrollListener = (CalendarScrollListener) getParent();
                if (completelyVisibleAgendaPosition <= 1 || completelyVisibleAgendaPosition >= adapter.getItemCount() - 1) {
                    calendarScrollListener.setupForDate(currentlyShownDate);
                }
                calendarScrollListener.onScrollToDate(currentlyShownDate);
            }
        }
    }

    public void setAdapter(Adapter adapter) {
        super.setAdapter(adapter);
        if (adapter != null && adapter.getItemCount() > 1) {
            scrollToPosition(adapter.getItemCount() / 2);
        }
    }
}
