package com.zendesk.sdk.network.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.push.PushRegistrationResponse;
import com.zendesk.sdk.storage.PushRegistrationResponseStorage;

public class StubPushRegistrationResponseStorage implements PushRegistrationResponseStorage {
    private static final String LOG_TAG = "StubPushRegStorage";

    public void storePushRegistrationResponse(@NonNull PushRegistrationResponse response) {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
    }

    @Nullable
    public PushRegistrationResponse getPushRegistrationResponse() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
        return null;
    }

    public boolean hasStoredPushRegistrationResponse() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
        return false;
    }

    public void removePushRegistrationResponse() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
    }

    public void clearUserData() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
    }

    public String getCacheKey() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
        return "";
    }
}
