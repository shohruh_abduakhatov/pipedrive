package com.pipedrive.util.devices;

import kotlin.Metadata;
import org.jetbrains.annotations.NotNull;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Completable;
import rx.Single;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H'J\u0012\u0010\u0007\u001a\u00020\b2\b\b\u0001\u0010\t\u001a\u00020\u0006H'J\u0012\u0010\n\u001a\u00020\b2\b\b\u0001\u0010\t\u001a\u00020\u0006H'J\u0018\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\f\u001a\u00020\rH'J\u001c\u0010\u000e\u001a\u00020\b2\b\b\u0001\u0010\t\u001a\u00020\u00062\b\b\u0001\u0010\f\u001a\u00020\rH'¨\u0006\u000f"}, d2 = {"Lcom/pipedrive/util/devices/MobileDevicesApi;", "", "getUdId", "Lrx/Single;", "Lcom/pipedrive/util/devices/DeviceResponse;", "androidId", "", "login", "Lrx/Completable;", "devicePipedriveId", "logout", "register", "deviceEntity", "Lcom/pipedrive/util/devices/DeviceEntity;", "update", "pipedrive_prodRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: MobileDevicesApi.kt */
public interface MobileDevicesApi {
    @NotNull
    @GET("mobile/devices/udid/{android_id}")
    Single<DeviceResponse> getUdId(@NotNull @Path("android_id") String str);

    @NotNull
    @PATCH("mobile/devices/{id}")
    Completable login(@NotNull @Path("id") String str);

    @NotNull
    @DELETE("mobile/devices/{id}")
    Completable logout(@NotNull @Path("id") String str);

    @NotNull
    @POST("mobile/devices")
    Single<DeviceResponse> register(@NotNull @Body DeviceEntity deviceEntity);

    @NotNull
    @PUT("mobile/devices/{id}")
    Completable update(@NotNull @Path("id") String str, @NotNull @Body DeviceEntity deviceEntity);
}
