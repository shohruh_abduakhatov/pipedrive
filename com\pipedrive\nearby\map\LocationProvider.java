package com.pipedrive.nearby.map;

import android.support.annotation.NonNull;
import com.google.android.gms.maps.model.LatLng;
import rx.Observable;

public interface LocationProvider {
    @NonNull
    Observable<LatLng> locationChanges();
}
