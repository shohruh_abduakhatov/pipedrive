package com.pipedrive.datasource;

import android.database.DatabaseUtils;
import android.support.annotation.NonNull;
import android.text.TextUtils;

public class SelectionHolder {
    @NonNull
    private String selection;
    @NonNull
    private String[] selectionArgs;

    public SelectionHolder(@NonNull String selection, @NonNull String[] selectionArgs) {
        this.selection = selection;
        this.selectionArgs = selectionArgs;
    }

    @NonNull
    public String getSelection() {
        return this.selection;
    }

    @NonNull
    public String[] getSelectionArgs() {
        return this.selectionArgs;
    }

    @NonNull
    public SelectionHolder add(@NonNull SelectionHolder selectionHolder) {
        if (!TextUtils.isEmpty(selectionHolder.getSelection())) {
            this.selection = DatabaseUtils.concatenateWhere(this.selection, selectionHolder.getSelection());
        }
        if (selectionHolder.getSelectionArgs().length > 0) {
            this.selectionArgs = DatabaseUtils.appendSelectionArgs(this.selectionArgs, selectionHolder.getSelectionArgs());
        }
        return this;
    }

    @NonNull
    public String buildQueryString() {
        String queryString = this.selection;
        int argIndex = 0;
        while (true) {
            int argIndex2 = argIndex + 1;
            queryString = queryString.replaceFirst("\\?", DatabaseUtils.sqlEscapeString(this.selectionArgs[argIndex]));
            if (argIndex2 == this.selectionArgs.length) {
                return queryString;
            }
            argIndex = argIndex2;
        }
    }
}
