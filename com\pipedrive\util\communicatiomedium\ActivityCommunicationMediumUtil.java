package com.pipedrive.util.communicatiomedium;

import android.support.annotation.NonNull;
import com.pipedrive.model.Activity;
import com.pipedrive.model.contact.CommunicationMedium;
import java.util.Collections;
import java.util.List;

public class ActivityCommunicationMediumUtil implements CommunicationMediumUtil<Activity> {
    public boolean hasPhones(@NonNull Activity activity) {
        return activity.getPerson() != null && activity.getPerson().hasPhones();
    }

    public boolean hasEmails(@NonNull Activity activity) {
        return activity.getPerson() != null && activity.getPerson().hasEmails();
    }

    public boolean hasMultiplePhones(@NonNull Activity activity) {
        return activity.getPerson() != null && activity.getPerson().hasMultiplePhones();
    }

    public List<? extends CommunicationMedium> getPhones(@NonNull Activity activity) {
        return activity.getPerson() != null ? activity.getPerson().getPhones() : Collections.emptyList();
    }

    public List<? extends CommunicationMedium> getEmails(@NonNull Activity activity) {
        return activity.getPerson() != null ? activity.getPerson().getEmails() : Collections.emptyList();
    }
}
