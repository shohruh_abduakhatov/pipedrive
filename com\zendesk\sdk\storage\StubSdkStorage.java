package com.zendesk.sdk.storage;

import com.zendesk.logger.Logger;
import com.zendesk.sdk.storage.SdkStorage.UserStorage;
import java.util.Set;
import java.util.TreeSet;

class StubSdkStorage implements SdkStorage {
    private static final String LOG_TAG = "StubSdkStorage";

    StubSdkStorage() {
    }

    public void registerUserStorage(UserStorage userStorage) {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
    }

    public void clearUserData() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
    }

    public Set<UserStorage> getUserStorage() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
        return new TreeSet();
    }
}
