package com.pipedrive.activity;

import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Spinner;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.pipedrive.ActivityListActivity;
import com.pipedrive.BaseActivity;
import com.pipedrive.R;
import com.pipedrive.adapter.ImageAndTextSpinnerAdapter;
import com.pipedrive.application.Session;
import com.pipedrive.call.CallSummaryListener;
import com.pipedrive.call.CallSummaryListener.CallSummaryRegistration;
import com.pipedrive.datetime.PipedriveDate;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.datetime.PipedriveDuration;
import com.pipedrive.datetime.PipedriveTime;
import com.pipedrive.dialogs.ConfirmDiscardChangesDialogFragment;
import com.pipedrive.dialogs.ConfirmDiscardChangesDialogFragment.ConfirmDiscardChangesNegativeBtnListener;
import com.pipedrive.dialogs.communicationmediumlist.CommunicationMediumDialogController;
import com.pipedrive.dialogs.communicationmediumlist.CommunicationMediumListDialogFragment;
import com.pipedrive.linking.OrganizationLinkingActivity;
import com.pipedrive.model.Activity;
import com.pipedrive.model.Activity.DateTimeInfo;
import com.pipedrive.model.ActivityType;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.User;
import com.pipedrive.model.contact.CommunicationMedium;
import com.pipedrive.navigator.ActivityEditViewNavigator;
import com.pipedrive.navigator.ActivityEditViewNavigator.OnBackPressedListener;
import com.pipedrive.navigator.Navigable;
import com.pipedrive.navigator.Navigator;
import com.pipedrive.navigator.Navigator.Type;
import com.pipedrive.navigator.buttons.DeleteMenuItem;
import com.pipedrive.navigator.buttons.DiscardMenuItem;
import com.pipedrive.navigator.buttons.NavigatorMenuItem;
import com.pipedrive.note.ActivityNoteEditorActivity;
import com.pipedrive.pipeline.PipelineActivity;
import com.pipedrive.util.EmailHelper;
import com.pipedrive.util.PipedriveUtil;
import com.pipedrive.util.StringUtils;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.util.activities.ActivityNetworkingUtil;
import com.pipedrive.util.communicatiomedium.ActivityCommunicationMediumUtil;
import com.pipedrive.util.snackbar.SnackBarManager;
import com.pipedrive.views.assignment.AssignedToView;
import com.pipedrive.views.assignment.SpinnerWithUserProfilePicture;
import com.pipedrive.views.association.AssociationView.AssociationListener;
import com.pipedrive.views.association.DealAssociationView;
import com.pipedrive.views.association.OrganizationAssociationView;
import com.pipedrive.views.association.PersonAssociationView;
import com.pipedrive.views.edit.activity.ActivitySubjectEditText;
import com.pipedrive.views.edit.activity.ActivitySubjectEditText.OnSubjectChange;
import com.pipedrive.views.edit.activity.DueDateView;
import com.pipedrive.views.edit.activity.DueDateView.OnDueDateChangedListener;
import com.pipedrive.views.edit.activity.DueTimeView;
import com.pipedrive.views.edit.activity.DueTimeView.OnDueTimeChangedListener;
import com.pipedrive.views.edit.activity.DueTimeView.OnDueTimeClearedListener;
import com.pipedrive.views.edit.activity.DurationView;
import com.pipedrive.views.edit.activity.DurationView.OnDurationChangedListener;
import com.pipedrive.views.edit.activity.DurationView.OnDurationClearedListener;
import com.pipedrive.views.edit.activity.NoteView;
import java.util.Arrays;
import java.util.List;

public class ActivityDetailActivity extends BaseActivity implements ActivityView, CommunicationMediumDialogController<Activity>, Navigable, ConfirmDiscardChangesNegativeBtnListener {
    private static final String ARG_ACTIVITY_SQL_ID = "activity_sql_id";
    private static final String ARG_DEAL_SQL_ID = "deal_sql_id";
    private static final String ARG_DUE_DATE_TIME_IN_MILLIS = "due_date_in_millis";
    private static final String ARG_FOLLOW_UP_ACTIVITY = "follow_up_activity";
    private static final String ARG_NEW_PLAIN_ACTIVITY = "new_plain_activity_please";
    private static final String ARG_ORGANIZATION_SQL_ID = "org_sql_id";
    private static final String ARG_PERSON_SQL_ID = "person_sql_id";
    private static final int LINK_DEAL_REQUEST_CODE = 3;
    private static final int LINK_ORGANIZATION_REQUEST_CODE = 2;
    private static final int LINK_PERSON_REQUEST_CODE = 1;
    private static final int REQUEST_UPDATE_ACTIVITY_NOTE = 0;
    @BindView(2131820704)
    DueDateView mActivityDueDateView;
    @BindView(2131820705)
    DueTimeView mActivityDueTime;
    @BindView(2131820706)
    DurationView mActivityDuration;
    @BindView(2131820709)
    NoteView mActivityNoteTextView;
    @BindView(2131820707)
    AssignedToView mAssignedToView;
    @BindView(2131820701)
    CheckBox mCheckboxDone;
    @BindView(2131820712)
    DealAssociationView mDealAssociationView;
    private final DeleteMenuItem mDeleteMenuItem = new DeleteMenuItem() {
        public boolean isEnabled() {
            return ActivityDetailActivity.this.getSession().canUserDeleteActivity();
        }

        public void onClick() {
            ActivityDetailActivity.this.onDelete();
        }
    };
    private final DiscardMenuItem mDiscardMenuItem = new DiscardMenuItem() {
        public void onClick() {
            ActivityDetailActivity.this.onCancel();
        }
    };
    @BindView(2131820711)
    OrganizationAssociationView mOrganizationAssociationView;
    @BindView(2131820710)
    PersonAssociationView mPersonAssociationView;
    @NonNull
    private ActivityPresenter mPresenter;
    @BindView(2131820703)
    Spinner mSpinnerActivityType;
    @BindView(2131820702)
    ActivitySubjectEditText mSubject;

    public static void startActivityForNewActivity(android.app.Activity activity) {
        ContextCompat.startActivity(activity, new Intent(activity, ActivityDetailActivity.class).putExtra(ARG_NEW_PLAIN_ACTIVITY, true), null);
    }

    public static void startActivityForFollowUpActivity(@NonNull android.app.Activity context, @NonNull Activity activity) {
        if (activity.isStored()) {
            ContextCompat.startActivity(context, getStartIntentForActivity(context, activity.getSqlId()).putExtra(ARG_FOLLOW_UP_ACTIVITY, true), null);
        } else {
            startActivityForNewActivity(context);
        }
    }

    public static void startActivityForActivity(@NonNull Context context, long activitySqlId) {
        context.startActivity(getStartIntentForActivity(context, activitySqlId));
    }

    private static Intent getStartIntentForActivity(@NonNull Context context, long activitySqlId) {
        return new Intent(context, ActivityDetailActivity.class).putExtra(ARG_ACTIVITY_SQL_ID, activitySqlId);
    }

    public static void startActivityForActivityOnDateWithCurrentTime(@NonNull Context context, long dueDateTimeInMillis) {
        context.startActivity(new Intent(context, ActivityDetailActivity.class).putExtra(ARG_DUE_DATE_TIME_IN_MILLIS, dueDateTimeInMillis));
    }

    public static void startActivityForActivity(@Nullable android.app.Activity activity, long activitySqlId) {
        if (!(activity == null)) {
            ContextCompat.startActivity(activity, getStartIntentForActivity(activity, activitySqlId), null);
        }
    }

    public static void startActivityForDeal(@NonNull Context context, long dealSqlId) {
        context.startActivity(new Intent(context, ActivityDetailActivity.class).putExtra(ARG_DEAL_SQL_ID, dealSqlId));
    }

    public static void startActivityForDeal(@Nullable android.app.Activity activity, @Nullable Deal deal) {
        boolean cannotStartActivity = activity == null || deal == null || !deal.isStored();
        if (!cannotStartActivity) {
            ContextCompat.startActivity(activity, new Intent(activity, ActivityDetailActivity.class).putExtra(ARG_DEAL_SQL_ID, deal.getSqlId()), null);
        }
    }

    public static void startActivityForOrganization(@NonNull Context context, long organizationSqlId) {
        context.startActivity(new Intent(context, ActivityDetailActivity.class).putExtra(ARG_ORGANIZATION_SQL_ID, organizationSqlId));
    }

    public static void startActivityForOrganization(@Nullable android.app.Activity activity, @Nullable Organization organization) {
        boolean cannotCreateActivityForOrganization = activity == null || organization == null || !organization.isStored();
        if (!cannotCreateActivityForOrganization) {
            ContextCompat.startActivity(activity, new Intent(activity, ActivityDetailActivity.class).putExtra(ARG_ORGANIZATION_SQL_ID, organization.getSqlId()), null);
        }
    }

    public static void startActivityForPerson(@NonNull Context context, long personSqlId) {
        context.startActivity(new Intent(context, ActivityDetailActivity.class).putExtra(ARG_PERSON_SQL_ID, personSqlId));
    }

    public static void startActivityForPerson(@Nullable android.app.Activity activity, @Nullable Person person) {
        boolean cannotCreateActivityForPerson = activity == null || person == null || !person.isStored();
        if (!cannotCreateActivityForPerson) {
            ContextCompat.startActivity(activity, new Intent(activity, ActivityDetailActivity.class).putExtra(ARG_PERSON_SQL_ID, person.getSqlId()), null);
        }
    }

    @Nullable
    protected Navigator getNavigatorImplementation() {
        Activity activity = this.mPresenter.getActivity();
        return (activity == null || activity.isStored()) ? new ActivityEditViewNavigator(this, (Toolbar) findViewById(R.id.toolbar), null) : new ActivityEditViewNavigator(this, (Toolbar) findViewById(R.id.toolbar), new OnBackPressedListener() {
            public void onBackPressed(@NonNull ActivityEditViewNavigator activityEditViewNavigator) {
                String tag = "confirmDiscardChangesDialog";
                ConfirmDiscardChangesDialogFragment.newInstance(R.string.are_you_sure_you_want_to_discard_this_activity, R.string.discard, R.string.keep_editing).show(ActivityDetailActivity.this.getSupportFragmentManager(), "confirmDiscardChangesDialog");
            }
        });
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupLayout();
        this.mPresenter = new ActivityPresenterImpl(this.mSession, savedInstanceState);
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Activity currentlyManagedActivity = this.mPresenter.getActivity();
        if (currentlyManagedActivity != null) {
            setupSubjectIntoActivity(currentlyManagedActivity);
        }
        this.mPresenter.onSaveInstanceState(outState);
    }

    private void setupLayout() {
        setContentView((int) R.layout.activity_activity_detail);
        setSupportActionBar((Toolbar) ButterKnife.findById((android.app.Activity) this, (int) R.id.toolbar));
        ButterKnife.bind((android.app.Activity) this);
    }

    public void onStart() {
        super.onStart();
        this.mPresenter.bindView(this);
        if (!requestActivity(getIntent().getExtras())) {
            finish();
        }
    }

    protected void onStop() {
        this.mPresenter.unbindView();
        super.onStop();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean manageableCallAndEmailMenuItems;
        super.onPrepareOptionsMenu(menu);
        MenuItem callMenuItem = menu.findItem(R.id.menu_call);
        MenuItem messageMenuItem = menu.findItem(R.id.menu_send_message);
        Activity currentlyManagedActivity = this.mPresenter.getActivity();
        if (currentlyManagedActivity != null) {
            manageableCallAndEmailMenuItems = true;
        } else {
            manageableCallAndEmailMenuItems = false;
        }
        if (manageableCallAndEmailMenuItems) {
            ActivityCommunicationMediumUtil util = new ActivityCommunicationMediumUtil();
            if (callMenuItem != null) {
                boolean showCallMenuItem;
                if (util.hasPhones(currentlyManagedActivity) && currentlyManagedActivity.isStored()) {
                    showCallMenuItem = true;
                } else {
                    showCallMenuItem = false;
                }
                callMenuItem.setVisible(showCallMenuItem);
            }
            if (messageMenuItem != null) {
                boolean showMessageMenuItem;
                if ((util.hasEmails(currentlyManagedActivity) || util.hasPhones(currentlyManagedActivity)) && currentlyManagedActivity.isStored()) {
                    showMessageMenuItem = true;
                } else {
                    showMessageMenuItem = false;
                }
                messageMenuItem.setVisible(showMessageMenuItem);
            }
        }
        return true;
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        boolean cannotProcessOptionsItemSelection;
        Activity currentlyManagedActivity = this.mPresenter.getActivity();
        if (currentlyManagedActivity == null) {
            cannotProcessOptionsItemSelection = true;
        } else {
            cannotProcessOptionsItemSelection = false;
        }
        if (cannotProcessOptionsItemSelection) {
            return false;
        }
        switch (item.getItemId()) {
            case R.id.menu_call:
                onOptionsItemSelectedCall(currentlyManagedActivity);
                return true;
            case R.id.menu_send_message:
                onOptionsItemSelectedMessage();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onOptionsItemSelectedMessage() {
        CommunicationMediumListDialogFragment.showSendMessageSelector(getSupportFragmentManager(), null);
    }

    private void onOptionsItemSelectedCall(@NonNull Activity activity) {
        ActivityCommunicationMediumUtil util = new ActivityCommunicationMediumUtil();
        if (util.hasMultiplePhones(activity)) {
            CommunicationMediumListDialogFragment.showCallSelector(getSupportFragmentManager(), null);
        } else if (util.hasPhones(activity)) {
            onCallRequested((CommunicationMedium) util.getPhones(activity).get(0), activity);
        }
    }

    private boolean requestActivity(@Nullable Bundle intentExtras) {
        if (intentExtras == null) {
            return false;
        }
        boolean newFollowUpActivityRequested;
        boolean changeOfActivityRequested = intentExtras.containsKey(ARG_ACTIVITY_SQL_ID);
        if (intentExtras.containsKey(ARG_ACTIVITY_SQL_ID) && intentExtras.containsKey(ARG_FOLLOW_UP_ACTIVITY)) {
            newFollowUpActivityRequested = true;
        } else {
            newFollowUpActivityRequested = false;
        }
        boolean newActivityForDealRequested = intentExtras.containsKey(ARG_DEAL_SQL_ID);
        boolean newActivityForOrganizationRequested = intentExtras.containsKey(ARG_ORGANIZATION_SQL_ID);
        boolean newActivityForPersonRequested = intentExtras.containsKey(ARG_PERSON_SQL_ID);
        boolean newPlainActivityRequested = intentExtras.containsKey(ARG_NEW_PLAIN_ACTIVITY);
        boolean newPlainActivityOnADateTimeRequested = intentExtras.containsKey(ARG_DUE_DATE_TIME_IN_MILLIS);
        if (newFollowUpActivityRequested) {
            this.mPresenter.restoreOrRequestNewFollowUpActivity(intentExtras.getLong(ARG_ACTIVITY_SQL_ID));
            return true;
        } else if (changeOfActivityRequested) {
            this.mPresenter.restoreOrRequestActivity(Long.valueOf(intentExtras.getLong(ARG_ACTIVITY_SQL_ID)));
            return true;
        } else if (newActivityForDealRequested) {
            this.mPresenter.restoreOrRequestNewActivityForDeal(Long.valueOf(intentExtras.getLong(ARG_DEAL_SQL_ID)));
            return true;
        } else if (newActivityForOrganizationRequested) {
            this.mPresenter.restoreOrRequestNewActivityForOrganization(Long.valueOf(intentExtras.getLong(ARG_ORGANIZATION_SQL_ID)));
            return true;
        } else if (newActivityForPersonRequested) {
            this.mPresenter.restoreOrRequestNewActivityForPerson(Long.valueOf(intentExtras.getLong(ARG_PERSON_SQL_ID)));
            return true;
        } else if (newPlainActivityRequested) {
            this.mPresenter.restoreOrRequestActivity(null);
            return true;
        } else if (!newPlainActivityOnADateTimeRequested) {
            return false;
        } else {
            this.mPresenter.restoreOrRequestNewActivityOnDateTime(Long.valueOf(intentExtras.getLong(ARG_DUE_DATE_TIME_IN_MILLIS)));
            return true;
        }
    }

    private void setupActionBar(@NonNull Activity activity) {
        boolean addNewActivityIsRequested = !activity.isStored();
        setTitle(addNewActivityIsRequested ? R.string.add_activity : R.string._empty);
        setNavigatorType(addNewActivityIsRequested ? Type.CREATE : Type.UPDATE);
    }

    private void setupDoneView(@NonNull final Activity activity) {
        this.mCheckboxDone.setChecked(activity.isDone());
        this.mCheckboxDone.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                activity.setDone(isChecked);
            }
        });
    }

    private void setupSubject(@NonNull Activity activity) {
        setupSubjectView(activity);
        setupSubjectViewActions();
    }

    private void setupSubjectView(@NonNull Activity activity) {
        this.mSubject.setText(activity.getSubject());
    }

    private void setupSubjectViewActions() {
        this.mSubject.setOnSubjectChange(new OnSubjectChange() {
            public void onSubjectCleared() {
                ActivityDetailActivity.this.supportInvalidateOptionsMenu();
            }

            public void onNewSubjectAdded() {
                ActivityDetailActivity.this.supportInvalidateOptionsMenu();
            }
        });
    }

    private void setupSubjectIntoActivity(@NonNull Activity inoutActivity) {
        String subject = this.mSubject.getText().toString();
        ActivityType activityType = inoutActivity.getType();
        boolean setSubjectByType = StringUtils.isTrimmedAndEmpty(subject) && activityType != null;
        if (setSubjectByType) {
            subject = activityType.getName();
        }
        inoutActivity.setSubject(subject);
    }

    private void setupTypeSpinner(@NonNull final Session session, @NonNull final Activity activity) {
        this.mSpinnerActivityType.setAdapter(new ImageAndTextSpinnerAdapter(this, R.layout.row_activity_type_selected, ActivityNetworkingUtil.loadActivityTypesList(session, true)));
        final ActivityType activityType = activity.getType();
        if (activityType != null) {
            this.mSpinnerActivityType.setSelection(ActivityType.getActivityTypeIndexByLabelName(session, true, activityType.getName(), activityType.getId()));
        }
        this.mSpinnerActivityType.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ((parent.getAdapter() instanceof ImageAndTextSpinnerAdapter) && activityType != null) {
                    ActivityType activityType = (ActivityType) ActivityNetworkingUtil.loadActivityTypesList(session, true, activityType.getId()).get(position);
                    activity.setType(activityType);
                    ActivityDetailActivity.this.mSubject.setHint(activityType.getName());
                    ActivityDetailActivity.this.supportInvalidateOptionsMenu();
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void setupDueDate(@NonNull Session session, @NonNull Activity activity) {
        setupDueDateView(session, activity);
        setupDueDateActions(session, activity);
    }

    private void setupDueDateView(@NonNull Session session, @NonNull Activity activity) {
        this.mActivityDueDateView.setDate(session, activity);
    }

    private void setupDueDateActions(@NonNull final Session session, @NonNull final Activity activity) {
        this.mActivityDueDateView.setOnDueDateChangedListener(activity, new OnDueDateChangedListener() {
            public void onDateChanged(@NonNull final PipedriveDate newDate) {
                activity.request(new DateTimeInfo() {
                    public void activityHasStartDateTime(@NonNull PipedriveDateTime dateTime, @NonNull Activity inActivity) {
                        inActivity.setActivityStartAt(PipedriveDateTime.instanceFromAdditionDependingOnDateTimezone(newDate, dateTime.getPipedriveTimeLocal()), inActivity.getDuration());
                    }

                    public void activityHasStartDate(@NonNull PipedriveDate date, @NonNull Activity inActivity) {
                        inActivity.setActivityStartAt(newDate, inActivity.getDuration());
                    }
                });
                ActivityDetailActivity.this.setupDueDateView(session, activity);
            }
        });
    }

    private void setupDueTime(@NonNull Session session, @NonNull Activity activity) {
        setupDueTimeView(session, activity);
        setupDueTimeAction(session, activity);
    }

    private void setupDueTimeView(@NonNull Session session, @NonNull Activity activity) {
        this.mActivityDueTime.setTime(session, activity);
    }

    private void setupDueTimeAction(@NonNull final Session session, @NonNull final Activity activity) {
        this.mActivityDueTime.setDueTimeListener(activity, new OnDueTimeChangedListener() {
            public void onDueTimeChanged(@NonNull final PipedriveTime timeOnlyLocal) {
                activity.request(new DateTimeInfo() {
                    public void activityHasStartDateTime(@NonNull PipedriveDateTime dateTime, @NonNull Activity inActivity) {
                        inActivity.setActivityStartAt(PipedriveDateTime.instanceFromAdditionDependingOnDateTimezone(dateTime.getPipedriveDateLocal(), timeOnlyLocal), inActivity.getDuration());
                    }

                    public void activityHasStartDate(@NonNull PipedriveDate date, @NonNull Activity inActivity) {
                        inActivity.setActivityStartAt(PipedriveDateTime.instanceFromAdditionDependingOnDateTimezone(date, timeOnlyLocal), inActivity.getDuration());
                    }
                });
                ActivityDetailActivity.this.setupDueTimeView(session, activity);
            }
        }, new OnDueTimeClearedListener() {
            public void onDueTimeCleared() {
                activity.request(new DateTimeInfo() {
                    public void activityHasStartDateTime(@NonNull PipedriveDateTime dateTime, @NonNull Activity inActivity) {
                        inActivity.setActivityStartAt(dateTime.getPipedriveDateLocal(), inActivity.getDuration());
                    }

                    public void activityHasStartDate(@NonNull PipedriveDate date, @NonNull Activity inActivity) {
                    }
                });
                ActivityDetailActivity.this.setupDueTimeView(session, activity);
            }
        });
    }

    private void setupDuration(@NonNull Activity activity) {
        setupDurationView(activity);
        setupDurationAction(activity);
    }

    private void setupDurationView(@NonNull Activity activity) {
        this.mActivityDuration.setText(activity.getDurationString());
    }

    private void setupDurationAction(@NonNull final Activity activity) {
        this.mActivityDuration.setOnDurationChangedListeners(new OnDurationChangedListener() {
            public void onDurationChanged(long durationInSeconds) {
                activity.setDuration(PipedriveDuration.instanceFromUnixTimeRepresentation(Long.valueOf(durationInSeconds)));
            }
        }, new OnDurationClearedListener() {
            public void onDurationCleared() {
                activity.setDuration(null);
            }
        });
    }

    private void setupAssignment(@NonNull Session session, @NonNull Activity activity) {
        setupAssignmentView(session, activity);
        setupAssignmentActions(activity);
    }

    private void setupAssignmentView(@NonNull Session session, @NonNull Activity activity) {
        this.mAssignedToView.loadAssignments(session, activity);
    }

    private void setupAssignmentActions(@NonNull final Activity activity) {
        this.mAssignedToView.setOnItemSelectedListener(new SpinnerWithUserProfilePicture.OnItemSelectedListener() {
            public void onItemSelected(@NonNull User user) {
                activity.setAssignedUserId((long) user.getPipedriveId());
            }
        });
    }

    private void setupNoteView(@NonNull Activity activity) {
        this.mActivityNoteTextView.setText(activity.getNote());
    }

    private void setupAssociatePersonView(@Nullable Person person) {
        this.mPersonAssociationView.setAssociation(person, Integer.valueOf(1));
        this.mPersonAssociationView.enableAssociationDetailViewOpening();
    }

    private void setupAssociatePersonViewActions() {
        this.mPersonAssociationView.setAssociationListener(new AssociationListener<Person>() {
            public void onAssociationChanged(@Nullable Person association) {
                ActivityDetailActivity.this.mPresenter.associatePerson(association);
            }
        });
    }

    private void setupAssociateOrganizationView(@Nullable Organization organization) {
        this.mOrganizationAssociationView.setAssociation(organization, Integer.valueOf(2));
        this.mOrganizationAssociationView.enableAssociationDetailViewOpening();
    }

    private void setupAssociateOrganizationViewActions() {
        this.mOrganizationAssociationView.setAssociationListener(new AssociationListener<Organization>() {
            public void onAssociationChanged(@Nullable Organization association) {
                ActivityDetailActivity.this.mPresenter.associateOrganization(association);
            }
        });
    }

    private void setupAssociateDealView(@Nullable Deal deal) {
        Organization associatedOrg = null;
        boolean activityIsNull = this.mPresenter.getActivity() == null;
        Person associatedPerson = activityIsNull ? null : this.mPresenter.getActivity().getPerson();
        if (!activityIsNull) {
            associatedOrg = this.mPresenter.getActivity().getOrganization();
        }
        this.mDealAssociationView.setAssociation(deal, Integer.valueOf(3), associatedPerson, associatedOrg);
        this.mDealAssociationView.enableAssociationDetailViewOpening();
    }

    private void setupAssociateDealViewActions() {
        this.mDealAssociationView.setAssociationListener(new AssociationListener<Deal>() {
            public void onAssociationChanged(@Nullable Deal association) {
                ActivityDetailActivity.this.mPresenter.associateDeal(association);
            }
        });
    }

    public void onRequestActivity(@Nullable Activity activity) {
        if (activity == null) {
            finish();
            return;
        }
        setupActionBar(activity);
        setupDoneView(activity);
        setupSubject(activity);
        setupTypeSpinner(getSession(), activity);
        setupDueDate(getSession(), activity);
        setupDueTime(getSession(), activity);
        setupDuration(activity);
        setupAssignment(getSession(), activity);
        setupNoteView(activity);
        setupAssociatePersonViewActions();
        setupAssociateOrganizationViewActions();
        setupAssociateDealViewActions();
        onAssociationsUpdated(activity);
        supportInvalidateOptionsMenu();
    }

    public void onActivityCreated(boolean success) {
        processActivityCrudResponse(success);
        if (success) {
            SnackBarManager.INSTANCE.setDelayedSnackBar(R.string.activity_added);
        }
    }

    public void onActivityUpdated(boolean success) {
        processActivityCrudResponse(success);
        if (success) {
            SnackBarManager.INSTANCE.setDelayedSnackBar(R.string.activity_updated);
        }
    }

    public void onActivityDeleted(boolean success) {
        processActivityCrudResponse(success);
        if (success) {
            SnackBarManager.INSTANCE.setDelayedSnackBar(R.string.activity_deleted);
        }
    }

    public void onAssociationsUpdated(@NonNull Activity activity) {
        setupAssociatePersonView(activity.getPerson());
        setupAssociateOrganizationView(activity.getOrganization());
        setupAssociateDealView(activity.getDeal());
        supportInvalidateOptionsMenu();
    }

    private void processActivityCrudResponse(boolean success) {
        if (success) {
            finish();
        } else {
            ViewUtil.showErrorToast(this, R.string.error_activity_save_failed);
        }
    }

    public void onCallRequested(@NonNull CommunicationMedium medium, @NonNull Activity activity) {
        Long personSqlId;
        CallSummaryRegistration callSummaryRegistration;
        this.mPresenter.registerActivityForIdentifyingReopenAfterCallSummaryViews();
        Person activityPerson = activity.getPerson();
        if (activityPerson == null || !activityPerson.isStored()) {
            personSqlId = null;
        } else {
            personSqlId = Long.valueOf(activityPerson.getSqlId());
        }
        if (this.mPresenter.isCachedActivityNotDoneAndTypeCall()) {
            callSummaryRegistration = new CallSummaryRegistration(medium.getValue(), personSqlId, null, Long.valueOf(activity.getSqlId()));
        } else {
            callSummaryRegistration = new CallSummaryRegistration(medium.getValue(), personSqlId);
        }
        CallSummaryListener.makeCallWithCallSummaryRegistration(getSession(), this, callSummaryRegistration);
    }

    public void onSmsRequested(@NonNull CommunicationMedium medium, @NonNull Activity activity) {
        PipedriveUtil.sendSms(this, medium.getValue());
    }

    public void onEmailRequested(@NonNull CommunicationMedium medium, @NonNull Activity activity) {
        String dropboxAddress = null;
        if (activity.getDeal() != null && !TextUtils.isEmpty(activity.getDeal().getDropBoxAddress())) {
            dropboxAddress = activity.getDeal().getDropBoxAddress();
        } else if (activity.getPerson() != null && !TextUtils.isEmpty(activity.getPerson().getDropBoxAddress())) {
            dropboxAddress = activity.getPerson().getDropBoxAddress();
        } else if (!(activity.getOrganization() == null || TextUtils.isEmpty(activity.getOrganization().getDropBoxAddress()))) {
            dropboxAddress = activity.getOrganization().getDropBoxAddress();
        }
        EmailHelper.composeAndSendEmail(this, medium.getValue(), dropboxAddress);
    }

    @NonNull
    public List<? extends CommunicationMedium> getPhones(@NonNull Activity activity) {
        return new ActivityCommunicationMediumUtil().getPhones(activity);
    }

    @NonNull
    public List<? extends CommunicationMedium> getEmails(@NonNull Activity activity) {
        return new ActivityCommunicationMediumUtil().getEmails(activity);
    }

    @Nullable
    public Activity getEntity() {
        return this.mPresenter.getActivity();
    }

    public boolean isChangesMadeToFieldsAfterInitialLoad() {
        return true;
    }

    public boolean isAllRequiredFieldsSet() {
        return true;
    }

    public void onCreateOrUpdate() {
        Activity currentlyManagedActivity = this.mPresenter.getActivity();
        if (!(currentlyManagedActivity == null)) {
            setupSubjectIntoActivity(currentlyManagedActivity);
            if (this.mPresenter.isModified()) {
                this.mPresenter.changeActivity(currentlyManagedActivity);
            } else {
                finish();
            }
        }
    }

    public void createOrUpdateDiscardedAsRequiredFieldsAreNotSet() {
    }

    private void onDelete() {
        final Activity currentlyManagedActivity = this.mPresenter.getActivity();
        if (!(currentlyManagedActivity == null)) {
            ViewUtil.showDeleteConfirmationDialog(this, getResources().getString(R.string.dialog_delete_activity), new Runnable() {
                public void run() {
                    ActivityDetailActivity.this.mPresenter.deleteActivity(Long.valueOf(currentlyManagedActivity.getSqlId()));
                }
            });
        }
    }

    public void onCancel() {
        finish();
    }

    @Nullable
    public List<? extends NavigatorMenuItem> getAdditionalButtons() {
        return Arrays.asList(new NavigatorMenuItem[]{this.mDeleteMenuItem, this.mDiscardMenuItem});
    }

    @OnClick({2131820708})
    public void onNoteClicked() {
        Activity activity = this.mPresenter.getActivity();
        if (activity != null) {
            if (activity.isStored()) {
                ActivityNoteEditorActivity.updateActivityNote((android.app.Activity) this, activity, 0);
            } else {
                ActivityNoteEditorActivity.updateActivityNote((android.app.Activity) this, activity.getNote(), 0);
            }
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            switch (requestCode) {
                case 0:
                    if (data.hasExtra(ActivityNoteEditorActivity.KEY_NOTE_CONTENT)) {
                        this.mPresenter.restoreOrRequestActivityNoteUpdate(data.getStringExtra(ActivityNoteEditorActivity.KEY_NOTE_CONTENT));
                        return;
                    }
                    return;
                case 1:
                    long personSqlId = data.getLongExtra("PERSON_SQL_ID", 0);
                    if (personSqlId != 0) {
                        this.mPresenter.associatePersonBySqlId(Long.valueOf(personSqlId));
                        return;
                    }
                    return;
                case 2:
                    long orgSqlId = data.getLongExtra(OrganizationLinkingActivity.KEY_ORG_SQL_ID, 0);
                    if (orgSqlId != 0) {
                        this.mPresenter.associateOrganizationBySqlId(Long.valueOf(orgSqlId));
                        return;
                    }
                    return;
                case 3:
                    long dealSqlId = data.getLongExtra("DEAL_SQL_ID", 0);
                    if (dealSqlId != 0) {
                        this.mPresenter.associateDealBySqlId(Long.valueOf(dealSqlId));
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void onDialogNegativeBtnClicked() {
        onCancel();
    }

    public static PendingIntent getPendingIntentForNotification(@NonNull Session session, @NonNull Activity activity) {
        Context context = session.getApplicationContext();
        return TaskStackBuilder.create(context).addNextIntent(PipelineActivity.getStartIntent(context)).addNextIntent(ActivityListActivity.getStartIntent(session, context)).addNextIntent(getStartIntentForActivity(context, activity.getSqlId())).getPendingIntent(Long.valueOf(activity.getSqlId()).intValue(), 0);
    }
}
