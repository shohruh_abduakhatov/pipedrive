package com.pipedrive.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.application.Session;
import com.pipedrive.model.Filter;
import com.pipedrive.util.CursorHelper;
import java.util.List;

public class FiltersDataSource extends BaseDataSource<Filter> {
    private static final String[] ALL_COLUMNS = new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_FILTERS_PIPEDRIVE_ID, "filters_name", PipeSQLiteHelper.COLUMN_FILTERS_IS_ACTIVE, PipeSQLiteHelper.COLUMN_FILTERS_TYPE, PipeSQLiteHelper.COLUMN_FILTERS_IS_PRIVATE};
    private static final String DEFAULT_ORDER_BY = "filters_name";

    public FiltersDataSource(Session session) {
        super(session.getDatabase());
    }

    public void deleteAllByType(@NonNull String type) {
        SelectionHolder selectionHolder = getSelectionForType(type);
        if (selectionHolder != null) {
            SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
            String tableName = getTableName();
            String selection = selectionHolder.getSelection();
            String[] selectionArgs = selectionHolder.getSelectionArgs();
            if (transactionalDBConnection instanceof SQLiteDatabase) {
                SQLiteInstrumentation.delete(transactionalDBConnection, tableName, selection, selectionArgs);
            } else {
                transactionalDBConnection.delete(tableName, selection, selectionArgs);
            }
        }
    }

    @Nullable
    private SelectionHolder getSelectionForType(@NonNull String type) {
        if (TextUtils.isEmpty(type) || TextUtils.equals(type, Filter.ALL_TYPES)) {
            return null;
        }
        String whereClause = "filters_type LIKE ?";
        return new SelectionHolder("filters_type LIKE ?", new String[]{"%" + type + "%"});
    }

    @NonNull
    public List<Filter> findAllActive() {
        return deflateCursorToList(query(getTableName(), getAllColumns(), getSelectionForActive(), "filters_name"));
    }

    @NonNull
    private SelectionHolder getSelectionForActive() {
        return new SelectionHolder("filters_is_active = ?", new String[]{String.valueOf(1)});
    }

    @NonNull
    public List<Filter> findAllActiveByType(@NonNull String type) {
        SelectionHolder selectionHolder = getSelectionForActive();
        SelectionHolder selectionForType = getSelectionForType(type);
        if (selectionForType != null) {
            selectionHolder.add(selectionForType);
        }
        return deflateCursorToList(query(getTableName(), getAllColumns(), selectionHolder, "filters_name"));
    }

    @NonNull
    protected String getColumnNameForPipedriveId() {
        return PipeSQLiteHelper.COLUMN_FILTERS_PIPEDRIVE_ID;
    }

    @NonNull
    protected String getColumnNameForSqlId() {
        return PipeSQLiteHelper.COLUMN_ID;
    }

    @NonNull
    protected String getTableName() {
        return "filters";
    }

    @NonNull
    protected ContentValues getContentValues(@NonNull Filter filter) {
        ContentValues contentValues = super.getContentValues(filter);
        CursorHelper.put(filter.getName(), contentValues, "filters_name");
        CursorHelper.put(filter.getType(), contentValues, PipeSQLiteHelper.COLUMN_FILTERS_TYPE);
        CursorHelper.put(filter.isActive(), contentValues, PipeSQLiteHelper.COLUMN_FILTERS_IS_ACTIVE);
        CursorHelper.put(filter.isPrivate(), contentValues, PipeSQLiteHelper.COLUMN_FILTERS_IS_PRIVATE);
        return contentValues;
    }

    @NonNull
    protected String[] getAllColumns() {
        return ALL_COLUMNS;
    }

    @Nullable
    protected Filter deflateCursor(@NonNull Cursor cursor) {
        Long sqlId = CursorHelper.getLong(cursor, getColumnNameForSqlId());
        Integer pipedriveId = CursorHelper.getInteger(cursor, getColumnNameForPipedriveId());
        String name = CursorHelper.getString(cursor, "filters_name");
        String type = CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_FILTERS_TYPE);
        Boolean isActive = CursorHelper.getBoolean(cursor, PipeSQLiteHelper.COLUMN_FILTERS_IS_ACTIVE);
        Boolean isPrivate = CursorHelper.getBoolean(cursor, PipeSQLiteHelper.COLUMN_FILTERS_IS_PRIVATE);
        if (name == null || type == null || isActive == null || isPrivate == null) {
            return null;
        }
        return Filter.create(sqlId, pipedriveId, name, type, isActive, isPrivate);
    }
}
