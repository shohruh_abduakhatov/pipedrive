package com.pipedrive.contentproviders;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.newrelic.agent.android.instrumentation.JSONArrayInstrumentation;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.datasource.products.DealProductsDataSource;
import com.pipedrive.logging.Log;
import com.pipedrive.model.Deal;
import com.pipedrive.model.DealStatus;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.util.CursorHelper;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONException;

public class DealsContentProvider extends BaseContentProvider {
    private static final String AUTHORITY = "com.pipedrive.contentproviders.dealscontentprovider";
    private static final String BASE_PATH = "filter_deals";
    private static final int DEALS = 10;
    private static final String DEAL_SLQ_ID_PATH = "deal_sql_id";
    private static final int DEAL_SQL_ID = 70;
    private static final int FILTER_ID = 40;
    private static final String FILTER_ID_PATH = "filter_id";
    private static final int NOT_DEFINED = 0;
    private static final int ORGANIZATION_SQL_ID = 60;
    private static final String ORGANIZATION_SQL_ID_PATH = "org_id";
    private static final int PERSON_SQL_ID = 50;
    private static final String PERSON_SQL_ID_PATH = "contact_sql_id";
    private static final String QUERY_PARAM_STATUS = "status";
    private static final UriMatcher URI_MATCHER = new UriMatcher(-1);
    private static final int USER_ID = 30;
    private static final String USER_ID_PATH = "user_id";
    private static final String dealsTableDealID = "deals.d_pd_id";
    private static final HashMap<String, String> filterDealsProjectionMap = new HashMap();
    private static final String filterDealsTableDealID = "filter_deals.filter_deals_pipedrive_id";
    private static final String filterDealsTableFilterID = "filter_deals.filter_deals_filter_pipedrive_id";

    @Nullable
    @Deprecated
    public /* bridge */ /* synthetic */ Uri attachSessionIdToUri_hackMethodToEnableCPUriBuildOutsideCP_BAD_DESIGN_AND_USAGE(Uri uri) {
        return super.attachSessionIdToUri_hackMethodToEnableCPUriBuildOutsideCP_BAD_DESIGN_AND_USAGE(uri);
    }

    public /* bridge */ /* synthetic */ int delete(@NonNull Uri uri, String str, String[] strArr, @NonNull SQLiteDatabase sQLiteDatabase) {
        return super.delete(uri, str, strArr, sQLiteDatabase);
    }

    @Nullable
    public /* bridge */ /* synthetic */ String getType(@NonNull Uri uri) {
        return super.getType(uri);
    }

    @Nullable
    public /* bridge */ /* synthetic */ Uri insert(@NonNull Uri uri, ContentValues contentValues, @NonNull SQLiteDatabase sQLiteDatabase) {
        return super.insert(uri, contentValues, sQLiteDatabase);
    }

    public /* bridge */ /* synthetic */ boolean onCreate() {
        return super.onCreate();
    }

    public /* bridge */ /* synthetic */ int update(@NonNull Uri uri, ContentValues contentValues, String str, String[] strArr, @NonNull SQLiteDatabase sQLiteDatabase) {
        return super.update(uri, contentValues, str, strArr, sQLiteDatabase);
    }

    static {
        URI_MATCHER.addURI("com.pipedrive.contentproviders.dealscontentprovider", "filter_deals", 10);
        URI_MATCHER.addURI("com.pipedrive.contentproviders.dealscontentprovider", "filter_deals/user_id/#", 30);
        URI_MATCHER.addURI("com.pipedrive.contentproviders.dealscontentprovider", "filter_deals/filter_id/#", 40);
        URI_MATCHER.addURI("com.pipedrive.contentproviders.dealscontentprovider", "filter_deals/contact_sql_id/#", 50);
        URI_MATCHER.addURI("com.pipedrive.contentproviders.dealscontentprovider", "filter_deals/org_id/#", 60);
        URI_MATCHER.addURI("com.pipedrive.contentproviders.dealscontentprovider", "filter_deals/deal_sql_id/#", 70);
        String dealsTableID = "deals._id";
        filterDealsProjectionMap.put("deals._id", "deals._id AS _id");
        filterDealsProjectionMap.put(dealsTableDealID, dealsTableDealID + " AS " + PipeSQLiteHelper.COLUMN_DEALS_PIPEDRIVE_ID);
        filterDealsProjectionMap.put(PipeSQLiteHelper.COLUMN_DEALS_TITLE, PipeSQLiteHelper.COLUMN_DEALS_TITLE);
        filterDealsProjectionMap.put(PipeSQLiteHelper.COLUMN_DEALS_VALUE, PipeSQLiteHelper.COLUMN_DEALS_VALUE);
        filterDealsProjectionMap.put(PipeSQLiteHelper.COLUMN_DEALS_CURRENCY, PipeSQLiteHelper.COLUMN_DEALS_CURRENCY);
        filterDealsProjectionMap.put(PipeSQLiteHelper.COLUMN_DEALS_STAGE, PipeSQLiteHelper.COLUMN_DEALS_STAGE);
        filterDealsProjectionMap.put(PipeSQLiteHelper.COLUMN_DEALS_STATUS, PipeSQLiteHelper.COLUMN_DEALS_STATUS);
        filterDealsProjectionMap.put(PipeSQLiteHelper.COLUMN_DEALS_PERSON_SQL_ID, PipeSQLiteHelper.COLUMN_DEALS_PERSON_SQL_ID);
        filterDealsProjectionMap.put(PipeSQLiteHelper.COLUMN_DEALS_ORGANIZATION_SQL_ID, PipeSQLiteHelper.COLUMN_DEALS_ORGANIZATION_SQL_ID);
        filterDealsProjectionMap.put(PipeSQLiteHelper.COLUMN_DEALS_PIPELINE_ID, PipeSQLiteHelper.COLUMN_DEALS_PIPELINE_ID);
        filterDealsProjectionMap.put(PipeSQLiteHelper.COLUMN_DEALS_NEXT_TASK_DATE, PipeSQLiteHelper.COLUMN_DEALS_NEXT_TASK_DATE);
        filterDealsProjectionMap.put(PipeSQLiteHelper.COLUMN_DEALS_NEXT_TASK_TIME, PipeSQLiteHelper.COLUMN_DEALS_NEXT_TASK_TIME);
        filterDealsProjectionMap.put(PipeSQLiteHelper.COLUMN_DEALS_UNDONE_ACTIVITIES_COUNT, PipeSQLiteHelper.COLUMN_DEALS_UNDONE_ACTIVITIES_COUNT);
        filterDealsProjectionMap.put(PipeSQLiteHelper.COLUMN_DEALS_FORMATTED_VALUE, PipeSQLiteHelper.COLUMN_DEALS_FORMATTED_VALUE);
        filterDealsProjectionMap.put(PipeSQLiteHelper.COLUMN_DEALS_ROTTEN_TIME, PipeSQLiteHelper.COLUMN_DEALS_ROTTEN_TIME);
        filterDealsProjectionMap.put(PipeSQLiteHelper.COLUMN_DEALS_CUSTOM_FIELDS, PipeSQLiteHelper.COLUMN_DEALS_CUSTOM_FIELDS);
        filterDealsProjectionMap.put(PipeSQLiteHelper.COLUMN_DEALS_DROP_BOX_ADDRESS, PipeSQLiteHelper.COLUMN_DEALS_DROP_BOX_ADDRESS);
        filterDealsProjectionMap.put(PipeSQLiteHelper.COLUMN_DEALS_VISIBLE_TO, PipeSQLiteHelper.COLUMN_DEALS_VISIBLE_TO);
    }

    public DealsContentProvider(@NonNull Session session) {
        super(session);
    }

    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder, @NonNull SQLiteDatabase sqLiteDatabase) {
        DealsDataSource dealsDataSource = new DealsDataSource(sqLiteDatabase);
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        int uriType = URI_MATCHER.match(uri);
        Cursor cursor = null;
        switch (uriType) {
            case 10:
                queryBuilder.setTables("deals");
                break;
            case 30:
                queryBuilder.setTables("deals");
                queryBuilder.appendWhere("d_owner_pd_id=" + uri.getLastPathSegment());
                break;
            case 40:
                queryBuilder.setTables("filter_deals JOIN deals ON (" + filterDealsTableDealID + " = " + dealsTableDealID + " ) ");
                queryBuilder.appendWhere(filterDealsTableFilterID + "=" + uri.getLastPathSegment());
                queryBuilder.setProjectionMap(filterDealsProjectionMap);
                break;
            case 50:
                queryBuilder.setTables("deals");
                queryBuilder.appendWhere("d_person_id_sql=" + uri.getLastPathSegment());
                break;
            case 60:
                queryBuilder.setTables("deals");
                queryBuilder.appendWhere("d_org_id_sql=" + uri.getLastPathSegment());
                break;
            case 70:
                cursor = dealsDataSource.findBySqlIdCursor(ContentUris.parseId(uri));
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        if (uriType == 60 || uriType == 50) {
            String dealsQuery;
            Set<String> dealsTableColumns = new HashSet(Arrays.asList(dealsDataSource.getAllColumns()));
            String mergedSelection = mergeSelectionParamsToSelection(selection, selectionArgs);
            DealStatus status = DealStatus.from(uri.getQueryParameter("status"));
            if (status == null) {
                String openDealsQuery = makeUnionSubQuery(queryBuilder, DealStatus.OPEN, mergedSelection, dealsTableColumns, sqLiteDatabase);
                String wonDealsQuery = makeUnionSubQuery(queryBuilder, DealStatus.WON, mergedSelection, dealsTableColumns, sqLiteDatabase);
                String lostDealsQuery = makeUnionSubQuery(queryBuilder, DealStatus.LOST, mergedSelection, dealsTableColumns, sqLiteDatabase);
                dealsQuery = queryBuilder.buildUnionQuery(new String[]{openDealsQuery, wonDealsQuery, lostDealsQuery}, null, null);
            } else {
                dealsQuery = makeUnionSubQuery(queryBuilder, status, mergedSelection, dealsTableColumns, sqLiteDatabase);
            }
            SQLiteDatabase transactionalDBConnection = dealsDataSource.getTransactionalDBConnection();
            cursor = !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.rawQuery(dealsQuery, null) : SQLiteInstrumentation.rawQuery(transactionalDBConnection, dealsQuery, null);
        } else if (cursor == null) {
            cursor = queryBuilder.query(dealsDataSource.getTransactionalDBConnection(), projection, selection, selectionArgs, null, null, sortOrder);
        }
        if (getContext() != null) {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return cursor;
    }

    private String makeUnionSubQuery(SQLiteQueryBuilder queryBuilder, DealStatus status, String selection, Set<String> tableColumns, @NonNull SQLiteDatabase sqLiteDatabase) {
        return queryBuilder.buildUnionSubQuery("_column_not_defined_", new DealsDataSource(sqLiteDatabase).getAllColumns(), tableColumns, 0, "_not_used_", selection + (TextUtils.isEmpty(selection) ? "" : " AND ") + PipeSQLiteHelper.COLUMN_DEALS_STATUS + " = '" + status + "'", null, null);
    }

    @Nullable
    public Uri createFilterIdUri(long filterId) {
        if (filterId == 0) {
            return null;
        }
        Builder uriBuilder = new Builder();
        uriBuilder.scheme("content").authority("com.pipedrive.contentproviders.dealscontentprovider").appendPath("filter_deals");
        uriBuilder.appendPath(FILTER_ID_PATH);
        ContentUris.appendId(uriBuilder, filterId);
        return attachSessionIdToUri(uriBuilder.build());
    }

    @Nullable
    public Uri createPersonSqlIdUri(long contactSqlId, @Nullable DealStatus status) {
        String str = null;
        if (contactSqlId <= 0) {
            return null;
        }
        Builder uriBuilder = new Builder();
        uriBuilder.scheme("content").authority("com.pipedrive.contentproviders.dealscontentprovider").appendPath("filter_deals");
        uriBuilder.appendPath(PERSON_SQL_ID_PATH);
        ContentUris.appendId(uriBuilder, contactSqlId);
        String str2 = "status";
        if (status != null) {
            str = status.toString();
        }
        uriBuilder.appendQueryParameter(str2, str);
        return attachSessionIdToUri(uriBuilder.build());
    }

    @Nullable
    public Uri createOrganizationSqlIdUri(long orgSqlId, @Nullable DealStatus status) {
        String str = null;
        if (orgSqlId <= 0) {
            return null;
        }
        Builder uriBuilder = new Builder();
        uriBuilder.scheme("content").authority("com.pipedrive.contentproviders.dealscontentprovider").appendPath("filter_deals");
        uriBuilder.appendPath(ORGANIZATION_SQL_ID_PATH);
        ContentUris.appendId(uriBuilder, orgSqlId);
        String str2 = "status";
        if (status != null) {
            str = status.toString();
        }
        uriBuilder.appendQueryParameter(str2, str);
        return attachSessionIdToUri(uriBuilder.build());
    }

    @Nullable
    public Uri createUserIdUri(long userId) {
        if (userId == 0) {
            return null;
        }
        Builder uriBuilder = new Builder();
        uriBuilder.scheme("content").authority("com.pipedrive.contentproviders.dealscontentprovider").appendPath("filter_deals");
        uriBuilder.appendPath("user_id");
        ContentUris.appendId(uriBuilder, userId);
        return attachSessionIdToUri(uriBuilder.build());
    }

    @NonNull
    public Uri createAllDealsUri() {
        Builder uriBuilder = new Builder();
        uriBuilder.scheme("content").authority("com.pipedrive.contentproviders.dealscontentprovider").appendPath("filter_deals");
        return attachSessionIdToUri(uriBuilder.build());
    }

    @Nullable
    public Uri createDealSqlIdUri(long dealSqlId) {
        if (dealSqlId <= 0) {
            return null;
        }
        Builder uriBuilder = new Builder();
        uriBuilder.scheme("content").authority("com.pipedrive.contentproviders.dealscontentprovider").appendPath("filter_deals");
        uriBuilder.appendPath(DEAL_SLQ_ID_PATH);
        ContentUris.appendId(uriBuilder, dealSqlId);
        return attachSessionIdToUri(uriBuilder.build());
    }

    @NonNull
    public static Deal dealsTableCursorToDeal(@NonNull SQLiteDatabase dbconn, @NonNull Cursor cursor) {
        Exception e;
        Deal deal = new Deal();
        Long dealSqlId = CursorHelper.getLong(cursor, DealsDataSource.COLUMN_DEAL_SQL_ID_ALIAS);
        if (dealSqlId != null) {
            deal.setSqlIdOrNull(dealSqlId);
        }
        Long dealPipedriveId = CursorHelper.getLong(cursor, PipeSQLiteHelper.COLUMN_DEALS_PIPEDRIVE_ID);
        if (dealPipedriveId != null) {
            deal.setPipedriveId(dealPipedriveId);
        }
        deal.setTitle(cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_TITLE)));
        deal.setValue(cursor.getDouble(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_VALUE)));
        deal.setCurrencyCode(cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_CURRENCY)));
        deal.setStage(cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_STAGE)));
        deal.setStatus(DealStatus.from(cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_STATUS))));
        int columnIdx = cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_PERSON_SQL_ID);
        if (!cursor.isNull(columnIdx)) {
            deal.setPerson((Person) new PersonsDataSource(dbconn).findBySqlId(cursor.getLong(columnIdx)));
        }
        columnIdx = cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_ORGANIZATION_SQL_ID);
        if (!cursor.isNull(columnIdx)) {
            deal.setOrganization((Organization) new OrganizationsDataSource(dbconn).findBySqlId(cursor.getLong(columnIdx)));
        }
        deal.setPipelineId(cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_PIPELINE_ID)));
        deal.setNextActivityDateStr(cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_NEXT_TASK_DATE)));
        deal.setNextActivityTime(cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_NEXT_TASK_TIME)));
        deal.setUndoneActivitiesCount(cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_UNDONE_ACTIVITIES_COUNT)));
        deal.setFormattedValue(cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_FORMATTED_VALUE)));
        deal.setRottenDateTime(cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_ROTTEN_TIME)));
        deal.setOwnerPipedriveId(cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_DEAL_OWNER_PD_ID)));
        byte[] otherFieldsArr = cursor.getBlob(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_CUSTOM_FIELDS));
        if (otherFieldsArr != null && otherFieldsArr.length > 0) {
            try {
                deal.setCustomFields(JSONArrayInstrumentation.init(new String(otherFieldsArr, HttpRequest.CHARSET_UTF8)));
            } catch (JSONException e2) {
                e = e2;
                Log.e(new Throwable("Error parsing other/custom values", e));
                columnIdx = cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_DROP_BOX_ADDRESS);
                if (!cursor.isNull(columnIdx)) {
                    deal.setDropBoxAddress(cursor.getString(columnIdx));
                }
                deal.setVisibleTo(cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_VISIBLE_TO)));
                deal.setOwnerName(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_DEALS_OWNER_NAME));
                deal.setExpectedCloseDate(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_DEALS_EXPECTED_CLOSE_DATE));
                deal.setAddTime(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_DEALS_ADD_TIME));
                deal.setUpdateTime(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_DEALS_UPDATE_TIME));
                deal.setWonTime(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_DEALS_WON_TIME));
                deal.setLostTime(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_DEALS_LOST_TIME));
                deal.setProductCount(CursorHelper.getDouble(cursor, PipeSQLiteHelper.COLUMN_DEALS_PRODUCT_COUNT));
                deal.setDealProducts(new DealProductsDataSource(dbconn).findAllRelatedToParent(deal));
                return deal;
            } catch (UnsupportedEncodingException e3) {
                e = e3;
                Log.e(new Throwable("Error parsing other/custom values", e));
                columnIdx = cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_DROP_BOX_ADDRESS);
                if (cursor.isNull(columnIdx)) {
                    deal.setDropBoxAddress(cursor.getString(columnIdx));
                }
                deal.setVisibleTo(cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_VISIBLE_TO)));
                deal.setOwnerName(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_DEALS_OWNER_NAME));
                deal.setExpectedCloseDate(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_DEALS_EXPECTED_CLOSE_DATE));
                deal.setAddTime(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_DEALS_ADD_TIME));
                deal.setUpdateTime(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_DEALS_UPDATE_TIME));
                deal.setWonTime(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_DEALS_WON_TIME));
                deal.setLostTime(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_DEALS_LOST_TIME));
                deal.setProductCount(CursorHelper.getDouble(cursor, PipeSQLiteHelper.COLUMN_DEALS_PRODUCT_COUNT));
                deal.setDealProducts(new DealProductsDataSource(dbconn).findAllRelatedToParent(deal));
                return deal;
            }
        }
        columnIdx = cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_DROP_BOX_ADDRESS);
        if (cursor.isNull(columnIdx)) {
            deal.setDropBoxAddress(cursor.getString(columnIdx));
        }
        deal.setVisibleTo(cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_DEALS_VISIBLE_TO)));
        deal.setOwnerName(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_DEALS_OWNER_NAME));
        deal.setExpectedCloseDate(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_DEALS_EXPECTED_CLOSE_DATE));
        deal.setAddTime(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_DEALS_ADD_TIME));
        deal.setUpdateTime(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_DEALS_UPDATE_TIME));
        deal.setWonTime(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_DEALS_WON_TIME));
        deal.setLostTime(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_DEALS_LOST_TIME));
        deal.setProductCount(CursorHelper.getDouble(cursor, PipeSQLiteHelper.COLUMN_DEALS_PRODUCT_COUNT));
        deal.setDealProducts(new DealProductsDataSource(dbconn).findAllRelatedToParent(deal));
        return deal;
    }

    private static String mergeSelectionParamsToSelection(String selection, String[] selectionArgs) {
        String mergedSelection = selection;
        for (String arg : selectionArgs) {
            String arg2;
            if (!TextUtils.isDigitsOnly(arg2)) {
                arg2 = "'" + arg2 + "'";
            }
            mergedSelection = mergedSelection.replaceFirst("\\?", arg2);
        }
        return mergedSelection;
    }

    @Nullable
    public static Deal getDealFromUri(Session session, ContentResolver resolver, @NonNull Uri uriFromDealsContentProvider) {
        Cursor cursor = resolver.query(uriFromDealsContentProvider, new DealsDataSource(session.getDatabase()).getAllColumns(), null, null, null);
        if (cursor == null) {
            return null;
        }
        Deal result = null;
        try {
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                result = dealsTableCursorToDeal(session.getDatabase(), cursor);
            }
            cursor.close();
            return result;
        } catch (Throwable th) {
            cursor.close();
        }
    }
}
