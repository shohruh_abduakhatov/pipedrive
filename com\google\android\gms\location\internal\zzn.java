package com.google.android.gms.location.internal;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzn implements Creator<LocationRequestUpdateData> {
    static void zza(LocationRequestUpdateData locationRequestUpdateData, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zzc(parcel, 1, locationRequestUpdateData.alf);
        zzb.zza(parcel, 2, locationRequestUpdateData.alg, i, false);
        zzb.zza(parcel, 3, locationRequestUpdateData.zzbqi(), false);
        zzb.zza(parcel, 4, locationRequestUpdateData.mPendingIntent, i, false);
        zzb.zza(parcel, 5, locationRequestUpdateData.zzbqj(), false);
        zzb.zza(parcel, 6, locationRequestUpdateData.zzbqk(), false);
        zzb.zzc(parcel, 1000, locationRequestUpdateData.getVersionCode());
        zzb.zzaj(parcel, zzcs);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzoe(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzvd(i);
    }

    public LocationRequestUpdateData zzoe(Parcel parcel) {
        IBinder iBinder = null;
        int zzcr = zza.zzcr(parcel);
        int i = 0;
        int i2 = 1;
        IBinder iBinder2 = null;
        PendingIntent pendingIntent = null;
        IBinder iBinder3 = null;
        LocationRequestInternal locationRequestInternal = null;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    i2 = zza.zzg(parcel, zzcq);
                    break;
                case 2:
                    locationRequestInternal = (LocationRequestInternal) zza.zza(parcel, zzcq, LocationRequestInternal.CREATOR);
                    break;
                case 3:
                    iBinder3 = zza.zzr(parcel, zzcq);
                    break;
                case 4:
                    pendingIntent = (PendingIntent) zza.zza(parcel, zzcq, PendingIntent.CREATOR);
                    break;
                case 5:
                    iBinder2 = zza.zzr(parcel, zzcq);
                    break;
                case 6:
                    iBinder = zza.zzr(parcel, zzcq);
                    break;
                case 1000:
                    i = zza.zzg(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new LocationRequestUpdateData(i, i2, locationRequestInternal, iBinder3, pendingIntent, iBinder2, iBinder);
        }
        throw new zza.zza("Overread allowed size end=" + zzcr, parcel);
    }

    public LocationRequestUpdateData[] zzvd(int i) {
        return new LocationRequestUpdateData[i];
    }
}
