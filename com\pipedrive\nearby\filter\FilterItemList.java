package com.pipedrive.nearby.filter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import java.util.ArrayList;
import java.util.List;

public class FilterItemList {
    public static final Long UNDEFINED_SELECTED_ITEM_ID = Long.valueOf(-1);
    @NonNull
    private final List<FilterItem> items = new ArrayList();
    @StringRes
    private final int labelResId;
    @NonNull
    private Long selectedItemPipedriveId = UNDEFINED_SELECTED_ITEM_ID;

    FilterItemList(@StringRes int labelResId, @NonNull List<FilterItem> items) {
        this.labelResId = labelResId;
        this.items.addAll(items);
    }

    @NonNull
    public Long getSelectedItemPipedriveId() {
        return this.selectedItemPipedriveId;
    }

    void setSelectedItemPosition(@NonNull Long selectedItemPosition) {
        this.selectedItemPipedriveId = selectedItemPosition;
    }

    @StringRes
    public int getLabelResId() {
        return this.labelResId;
    }

    void addItemAsFirstInTheList(@NonNull FilterItem item) {
        this.items.add(0, item);
    }

    @Nullable
    public FilterItem getItem(int position) {
        if (this.items.size() > position) {
            return (FilterItem) this.items.get(position);
        }
        return null;
    }

    public int getSize() {
        return this.items.size();
    }
}
