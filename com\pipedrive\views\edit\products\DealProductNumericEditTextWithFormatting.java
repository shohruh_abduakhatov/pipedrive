package com.pipedrive.views.edit.products;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.pipedrive.application.Session;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.util.formatter.DecimalFormatter;

public abstract class DealProductNumericEditTextWithFormatting extends DecimalEditTextWithUnderlineAndLabel {
    private OnValueChangedListener mOnValueChangedListener;

    public interface OnValueChangedListener {
        void onValueChanged(double d);
    }

    abstract double getInitialValue(@NonNull DealProduct dealProduct);

    public DealProductNumericEditTextWithFormatting(Context context) {
        this(context, null);
    }

    public DealProductNumericEditTextWithFormatting(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DealProductNumericEditTextWithFormatting(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setup(@NonNull Session session, @NonNull DealProduct dealProduct, @Nullable OnValueChangedListener onValueChangedListener) {
        this.mOnValueChangedListener = onValueChangedListener;
        setupMainViewWithZeroValueDisplayed(session, getInitialValue(dealProduct));
    }

    @Nullable
    protected String getFormattedValue() {
        if (getSession() == null) {
            return null;
        }
        return new DecimalFormatter(getSession()).format(Double.valueOf(getValue()));
    }

    protected void onValueChanged(double newValue) {
        if (this.mOnValueChangedListener != null) {
            this.mOnValueChangedListener.onValueChanged(newValue);
        }
    }
}
