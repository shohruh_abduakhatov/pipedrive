package com.pipedrive.util.networking.search;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pipedrive.model.Deal;
import com.pipedrive.model.DealStatus;
import com.pipedrive.model.delta.ObjectDelta;
import java.math.BigDecimal;

public class DealSearchResultEntity extends SearchResultEntity<Deal> {
    @Nullable
    @SerializedName("details")
    @Expose
    private Details details;

    private static class Details {
        @Nullable
        @SerializedName("currency")
        @Expose
        private String currencyCode;
        @Nullable
        @SerializedName("stage_id")
        @Expose
        private Integer dealStagePipedriveId;
        @Nullable
        @SerializedName("org_name")
        @Expose
        private String organizationName;
        @Nullable
        @SerializedName("org_id")
        @Expose
        private Integer organizationPipedriveId;
        @Nullable
        @SerializedName("person_name")
        @Expose
        private String personName;
        @Nullable
        @SerializedName("person_id")
        @Expose
        private Integer personPipedriveId;
        @Nullable
        @SerializedName("status")
        @Expose
        private DealStatus status;
        @Nullable
        @SerializedName("value")
        @Expose
        private BigDecimal value;

        private Details(@Nullable BigDecimal value, @Nullable String currencyCode, @Nullable DealStatus status, @Nullable Integer dealStagePipedriveId, @Nullable Integer personPipedriveId, @Nullable String personName, @Nullable Integer organizationPipedriveId, @Nullable String organizationName) {
            this.value = value;
            this.currencyCode = currencyCode;
            this.status = status;
            this.dealStagePipedriveId = dealStagePipedriveId;
            this.personPipedriveId = personPipedriveId;
            this.personName = personName;
            this.organizationPipedriveId = organizationPipedriveId;
            this.organizationName = organizationName;
        }
    }

    @NonNull
    public Boolean isDifferentFrom(@NonNull Deal deal) {
        boolean titlesDiffer;
        boolean z = true;
        if (ObjectDelta.areEqual(deal.getTitle(), getName())) {
            titlesDiffer = false;
        } else {
            titlesDiffer = true;
        }
        boolean statusesDiffer;
        if (ObjectDelta.areEqual(deal.getStatus(), getStatus())) {
            statusesDiffer = false;
        } else {
            statusesDiffer = true;
        }
        boolean currencyCodesDiffer;
        if (ObjectDelta.areEqual(deal.getCurrencyCode(), getCurrencyCode())) {
            currencyCodesDiffer = false;
        } else {
            currencyCodesDiffer = true;
        }
        boolean stagesPipedriveIdDiffer;
        if (ObjectDelta.areEqual(Integer.valueOf(deal.getStage()), getDealStagePipedriveId())) {
            stagesPipedriveIdDiffer = false;
        } else {
            stagesPipedriveIdDiffer = true;
        }
        if (titlesDiffer || statusesDiffer || currencyCodesDiffer || stagesPipedriveIdDiffer) {
            return Boolean.valueOf(true);
        }
        if (deal.getValue() != (getValue() == null ? 0.0d : getValue().doubleValue())) {
            return Boolean.valueOf(true);
        }
        if (deal.getPerson() == null && getPersonPipedriveId() == null) {
            return Boolean.valueOf(false);
        }
        if ((deal.getPerson() == null && getPersonPipedriveId() != null) || (deal.getPerson() != null && getPersonPipedriveId() == null)) {
            return Boolean.valueOf(true);
        }
        if (deal.getPerson().getPipedriveId() != getPersonPipedriveId().intValue()) {
            return Boolean.valueOf(true);
        }
        if (!TextUtils.equals(deal.getPerson().getName(), getPersonName())) {
            return Boolean.valueOf(true);
        }
        if (deal.getOrganization() == null && getOrganizationPipedriveId() == null) {
            return Boolean.valueOf(false);
        }
        if ((deal.getOrganization() == null && getOrganizationPipedriveId() != null) || (deal.getOrganization() != null && getOrganizationPipedriveId() == null)) {
            return Boolean.valueOf(true);
        }
        if (deal.getOrganization().getPipedriveId() != getOrganizationPipedriveId().intValue()) {
            return Boolean.valueOf(true);
        }
        if (TextUtils.equals(deal.getOrganization().getName(), getOrganizationName())) {
            z = false;
        }
        return Boolean.valueOf(z);
    }

    @Nullable
    public BigDecimal getValue() {
        return this.details == null ? null : this.details.value;
    }

    @Nullable
    public String getCurrencyCode() {
        return this.details == null ? null : this.details.currencyCode;
    }

    @Nullable
    public DealStatus getStatus() {
        return this.details == null ? null : this.details.status;
    }

    @Nullable
    public Integer getDealStagePipedriveId() {
        return this.details == null ? null : this.details.dealStagePipedriveId;
    }

    @Nullable
    public Integer getPersonPipedriveId() {
        return this.details == null ? null : this.details.personPipedriveId;
    }

    @Nullable
    public String getPersonName() {
        return this.details == null ? null : this.details.personName;
    }

    @Nullable
    public Integer getOrganizationPipedriveId() {
        return this.details == null ? null : this.details.organizationPipedriveId;
    }

    @Nullable
    public String getOrganizationName() {
        return this.details == null ? null : this.details.organizationName;
    }

    public DealSearchResultEntity(@Nullable Integer pipedriveId, @Nullable String name) {
        super(pipedriveId, name);
    }

    public DealSearchResultEntity(@Nullable Integer pipedriveId, @Nullable String name, @Nullable BigDecimal value, @Nullable String currencyCode, @Nullable DealStatus status, @Nullable Integer dealStagePipedriveId, @Nullable Integer personPipedriveId, @Nullable String personName, @Nullable Integer organizationPipedriveId, @Nullable String organizationName) {
        super(pipedriveId, name);
        this.details = new Details(value, currencyCode, status, dealStagePipedriveId, personPipedriveId, personName, organizationPipedriveId, organizationName);
    }
}
