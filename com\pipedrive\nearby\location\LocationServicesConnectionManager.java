package com.pipedrive.nearby.location;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import java.util.concurrent.TimeUnit;

public class LocationServicesConnectionManager implements ConnectionCallbacks, OnConnectionFailedListener {
    private static final LocationRequest REQUEST = LocationRequest.create().setFastestInterval(TimeUnit.SECONDS.toMillis(1)).setInterval(TimeUnit.SECONDS.toMillis(30)).setSmallestDisplacement(20.0f);
    @NonNull
    private final GoogleApiClient googleApiClient;
    @NonNull
    private final LocationListener locationListener = LocationChangesProvider.getInstance();

    public LocationServicesConnectionManager(Context context) {
        this.googleApiClient = new Builder(context).addApi(LocationServices.API).build();
    }

    public void onConnected(@Nullable Bundle bundle) {
        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(this.googleApiClient);
        if (lastLocation != null) {
            this.locationListener.onLocationChanged(lastLocation);
        } else {
            LogJourno.reportEvent(EVENT.NotSpecified_noLastKnownLocationPresent);
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(this.googleApiClient, REQUEST, this.locationListener);
    }

    public void onConnectionSuspended(int i) {
    }

    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    public void connect() {
        if (!this.googleApiClient.isConnected()) {
            registerConnectionListeners();
            this.googleApiClient.connect();
        }
    }

    private void registerConnectionListeners() {
        this.googleApiClient.registerConnectionCallbacks(this);
        this.googleApiClient.registerConnectionFailedListener(this);
    }

    public void disconnect() {
        if (this.googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(this.googleApiClient, this.locationListener);
            unregisterConnectionListeners();
            this.googleApiClient.disconnect();
        }
    }

    private void unregisterConnectionListeners() {
        this.googleApiClient.unregisterConnectionCallbacks(this);
        this.googleApiClient.unregisterConnectionFailedListener(this);
    }
}
