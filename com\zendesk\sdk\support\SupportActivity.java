package com.zendesk.sdk.support;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View.OnClickListener;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.background.ApplicationStateMonitor;
import com.newrelic.agent.android.instrumentation.Instrumented;
import com.newrelic.agent.android.tracing.TraceMachine;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import com.zendesk.sdk.model.helpcenter.SearchArticle;
import com.zendesk.sdk.network.RetryAction;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.requests.RequestActivity;
import com.zendesk.sdk.support.SupportMvp.ErrorType;
import com.zendesk.sdk.support.SupportMvp.Presenter;
import com.zendesk.sdk.support.SupportMvp.View;
import com.zendesk.sdk.support.help.HelpSearchFragment;
import com.zendesk.sdk.support.help.SupportHelpFragment;
import com.zendesk.sdk.ui.ToolbarSherlock;
import com.zendesk.sdk.util.UiUtils;
import com.zendesk.service.ErrorResponse;
import com.zendesk.service.ErrorResponseAdapter;
import com.zendesk.util.CollectionUtils;
import java.util.List;

@Instrumented
public class SupportActivity extends AppCompatActivity implements View, TraceFieldInterface {
    static final String EXTRA_CATEGORIES_COLLAPSED = "extra_categories_collapsed";
    static final String EXTRA_CATEGORY_IDS = "extra_category_ids";
    static final String EXTRA_LABEL_NAMES = "extra_label_names";
    static final String EXTRA_SECTION_IDS = "extra_section_ids";
    static final String EXTRA_SHOW_CONTACT_US_BUTTON = "extra_show_contact_us_button";
    static final String EXTRA_SHOW_CONVERSATIONS_MENU_BUTTON = "extra_show_conversations_menu_button";
    private static final String LOG_TAG = "SupportActivity";
    static final String SAVED_STATE_FAB_VISIBLE = "fab_visibility";
    private FloatingActionButton contactUsButton;
    private MenuItem conversationsMenuItem;
    private Snackbar errorSnackbar;
    private ZendeskFeedbackConfiguration feedbackConfiguration;
    private android.view.View loadingView;
    private Presenter presenter;
    private MenuItem searchViewMenuItem;

    public static class Builder {
        private final Bundle args = new Bundle();

        public Builder withArticlesForCategoryIds(long... categoryIds) {
            if (this.args.getLongArray(SupportActivity.EXTRA_SECTION_IDS) != null) {
                Logger.w(SupportActivity.LOG_TAG, "Builder: sections have already been specified. Removing section IDs to set category IDs.", new Object[0]);
                this.args.remove(SupportActivity.EXTRA_SECTION_IDS);
            }
            this.args.putLongArray(SupportActivity.EXTRA_CATEGORY_IDS, categoryIds);
            return this;
        }

        public Builder withArticlesForSectionIds(long... sectionIds) {
            if (this.args.getLongArray(SupportActivity.EXTRA_CATEGORY_IDS) != null) {
                Logger.w(SupportActivity.LOG_TAG, "Builder: categories have already been specified. Removing category IDs to set section IDs.", new Object[0]);
                this.args.remove(SupportActivity.EXTRA_CATEGORY_IDS);
            }
            this.args.putLongArray(SupportActivity.EXTRA_SECTION_IDS, sectionIds);
            return this;
        }

        public Builder showContactUsButton(boolean showContactUsButton) {
            this.args.putBoolean(SupportActivity.EXTRA_SHOW_CONTACT_US_BUTTON, showContactUsButton);
            return this;
        }

        public Builder withContactConfiguration(ZendeskFeedbackConfiguration configuration) {
            if (configuration != null) {
                configuration = new WrappedZendeskFeedbackConfiguration(configuration);
            }
            this.args.putSerializable(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION, configuration);
            return this;
        }

        public Builder withLabelNames(String... labelNames) {
            if (CollectionUtils.isNotEmpty(labelNames)) {
                this.args.putStringArray(SupportActivity.EXTRA_LABEL_NAMES, labelNames);
            }
            return this;
        }

        public Builder withCategoriesCollapsed(boolean categoriesCollapsed) {
            this.args.putBoolean(SupportActivity.EXTRA_CATEGORIES_COLLAPSED, categoriesCollapsed);
            return this;
        }

        public Builder showConversationsMenuButton(boolean showConversationsMenuButton) {
            this.args.putBoolean(SupportActivity.EXTRA_SHOW_CONVERSATIONS_MENU_BUTTON, showConversationsMenuButton);
            return this;
        }

        public void show(Context context) {
            Logger.d(SupportActivity.LOG_TAG, "show: showing SupportActivity", new Object[0]);
            context.startActivity(intent(context));
        }

        Intent intent(Context context) {
            Logger.d(SupportActivity.LOG_TAG, "intent: creating Intent", new Object[0]);
            Intent intent = new Intent(context, SupportActivity.class);
            intent.putExtras(this.args);
            return intent;
        }
    }

    protected void onStart() {
        super.onStart();
        ApplicationStateMonitor.getInstance().activityStarted();
    }

    protected void onStop() {
        super.onStop();
        ApplicationStateMonitor.getInstance().activityStopped();
    }

    protected void onCreate(Bundle bundle) {
        TraceMachine.startTracing(LOG_TAG);
        try {
            TraceMachine.enterMethod(this._nr_trace, "SupportActivity#onCreate", null);
        } catch (NoSuchFieldError e) {
            while (true) {
                TraceMachine.enterMethod(null, "SupportActivity#onCreate", null);
            }
        }
        super.onCreate(bundle);
        UiUtils.setThemeIfAttributesAreMissing(this, R.attr.helpConversationsIcon);
        setContentView(R.layout.activity_support);
        ToolbarSherlock.installToolBar(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null) {
            String errorMsg = "This activity requires an AppCompat theme with an action bar, finishing activity...";
            Logger.e(LOG_TAG, "This activity requires an AppCompat theme with an action bar, finishing activity...", new Object[0]);
            setResult(0, getErrorIntent(new ErrorResponseAdapter("This activity requires an AppCompat theme with an action bar, finishing activity...")));
            finish();
            TraceMachine.exitMethod();
            return;
        }
        actionBar.setDisplayHomeAsUpEnabled(true);
        this.feedbackConfiguration = getFeedbackConfiguration();
        this.loadingView = findViewById(R.id.loading_view);
        this.contactUsButton = (FloatingActionButton) findViewById(R.id.contact_us_button);
        this.contactUsButton.setOnClickListener(new OnClickListener() {
            public void onClick(android.view.View v) {
                SupportActivity.this.showContactZendesk();
            }
        });
        this.presenter = new SupportPresenter(this, new SupportModel(), ZendeskConfig.INSTANCE.provider().networkInfoProvider());
        Bundle argsBundle = getIntent().getExtras() != null ? getIntent().getExtras() : new Bundle();
        if (bundle != null) {
            argsBundle.putAll(bundle);
        }
        this.presenter.initWithBundle(argsBundle);
        addOnBackStackChangedListener();
        TraceMachine.exitMethod();
    }

    private void addOnBackStackChangedListener() {
        getSupportFragmentManager().addOnBackStackChangedListener(new OnBackStackChangedListener() {
            public void onBackStackChanged() {
                if (SupportActivity.this.getCurrentFragment().isHidden()) {
                    SupportActivity.this.getSupportFragmentManager().beginTransaction().show(SupportActivity.this.getCurrentFragment()).commit();
                    if (SupportActivity.this.getCurrentFragment() instanceof SupportHelpFragment) {
                        ((SupportHelpFragment) SupportActivity.this.getCurrentFragment()).setPresenter(SupportActivity.this.presenter);
                    }
                }
            }
        });
    }

    private boolean noFragmentAdded() {
        return getCurrentFragment() == null;
    }

    protected void onResume() {
        super.onResume();
        this.presenter.onResume(this);
    }

    protected void onPause() {
        super.onPause();
        this.presenter.onPause();
    }

    private Intent getErrorIntent(ErrorResponse errorResponse) {
        Intent intent = new Intent();
        intent.putExtra(ContactZendeskActivity.RESULT_ERROR_IS_NETWORK_ERROR, false);
        intent.putExtra(ContactZendeskActivity.RESULT_ERROR_REASON, errorResponse.getReason());
        intent.putExtra(ContactZendeskActivity.RESULT_ERROR_STATUS_CODE, errorResponse.getStatus());
        return intent;
    }

    @Nullable
    private ZendeskFeedbackConfiguration getFeedbackConfiguration() {
        boolean hasSuppliedContactConfiguration = getIntent().hasExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION) && (getIntent().getSerializableExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION) instanceof ZendeskFeedbackConfiguration);
        if (hasSuppliedContactConfiguration) {
            return (ZendeskFeedbackConfiguration) getIntent().getSerializableExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION);
        }
        return null;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fragment_help_menu_conversations, menu);
        this.conversationsMenuItem = menu.findItem(R.id.fragment_help_menu_contact);
        this.searchViewMenuItem = menu.findItem(R.id.fragment_help_menu_search);
        if (this.searchViewMenuItem != null) {
            SearchView searchView = (SearchView) this.searchViewMenuItem.getActionView();
            searchView.setImeOptions(searchView.getImeOptions() | 268435456);
            searchView.setOnQueryTextListener(new OnQueryTextListener() {
                public boolean onQueryTextSubmit(String query) {
                    if (SupportActivity.this.presenter == null) {
                        return false;
                    }
                    SupportActivity.this.presenter.onSearchSubmit(query);
                    return true;
                }

                public boolean onQueryTextChange(String newText) {
                    return false;
                }
            });
        }
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        if (!(this.presenter == null || this.searchViewMenuItem == null)) {
            this.searchViewMenuItem.setVisible(this.presenter.shouldShowSearchMenuItem());
        }
        if (!(this.presenter == null || this.conversationsMenuItem == null)) {
            this.conversationsMenuItem.setVisible(this.presenter.shouldShowConversationsMenuItem());
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == 16908332) {
            onBackPressed();
            return true;
        } else if (id != R.id.fragment_help_menu_contact) {
            return false;
        } else {
            showRequestList();
            return true;
        }
    }

    public void showContactUsButton() {
        this.contactUsButton.setVisibility(0);
    }

    public void showHelp(SupportUiConfig supportUiConfig) {
        if (noFragmentAdded()) {
            SupportHelpFragment fragment = SupportHelpFragment.newInstance(supportUiConfig);
            fragment.setPresenter(this.presenter);
            addFragment(fragment);
        } else if (getCurrentFragment() instanceof SupportHelpFragment) {
            ((SupportHelpFragment) getCurrentFragment()).setPresenter(this.presenter);
        }
        invalidateOptionsMenu();
    }

    private void addFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment, fragment.getClass().getSimpleName()).commit();
    }

    public void showLoadingState() {
        Fragment fragment = getCurrentFragment();
        if (fragment != null && fragment.isVisible()) {
            getSupportFragmentManager().beginTransaction().hide(getCurrentFragment()).commit();
        }
        this.loadingView.setVisibility(0);
    }

    public void hideLoadingState() {
        this.loadingView.setVisibility(8);
    }

    public void showSearchResults(List<SearchArticle> searchArticles, String query) {
        getSearchFragment().updateResults(searchArticles, query);
    }

    private Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.fragment_container);
    }

    private HelpSearchFragment getSearchFragment() {
        boolean z = false;
        if (getCurrentFragment() instanceof HelpSearchFragment) {
            Logger.d(LOG_TAG, "showSearchResults: current fragment is a HelpSearchFragment", new Object[0]);
            return (HelpSearchFragment) getCurrentFragment();
        }
        if (this.contactUsButton.getVisibility() == 0) {
            z = true;
        }
        HelpSearchFragment searchFragment = HelpSearchFragment.newInstance(z);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, searchFragment).addToBackStack(null).commit();
        return searchFragment;
    }

    public void clearSearchResults() {
        if (getCurrentFragment() instanceof HelpSearchFragment) {
            ((HelpSearchFragment) getCurrentFragment()).clearResults();
        }
    }

    public boolean isShowingHelp() {
        return getCurrentFragment() instanceof SupportHelpFragment;
    }

    public void showRequestList() {
        RequestActivity.startActivity(this, this.feedbackConfiguration);
    }

    public void showContactZendesk() {
        ContactZendeskActivity.startActivity(this, this.feedbackConfiguration);
    }

    public void showErrorWithRetry(ErrorType errorType, final RetryAction action) {
        String errorMessage;
        if (errorType != null) {
            switch (errorType) {
                case CATEGORY_LOAD:
                    errorMessage = getString(R.string.categories_list_fragment_error_message);
                    break;
                case SECTION_LOAD:
                    errorMessage = getString(R.string.sections_list_fragment_error_message);
                    break;
                case ARTICLES_LOAD:
                    errorMessage = getString(R.string.articles_list_fragment_error_message);
                    break;
                default:
                    Logger.w(LOG_TAG, "Unknown or unhandled error type, falling back to error type name as label", new Object[0]);
                    errorMessage = getString(R.string.help_search_no_results_label) + " " + errorType.name();
                    break;
            }
        }
        Logger.w(LOG_TAG, "ErrorType was null, falling back to 'retry' as label", new Object[0]);
        errorMessage = getString(R.string.retry_view_button_label);
        this.errorSnackbar = Snackbar.make(this.contactUsButton, errorMessage, -2);
        this.errorSnackbar.setAction(R.string.retry_view_button_label, new OnClickListener() {
            public void onClick(android.view.View v) {
                SupportActivity.this.errorSnackbar.dismiss();
                action.onRetry();
            }
        });
        this.errorSnackbar.show();
    }

    public void showError(int errorMessageId) {
        this.errorSnackbar = Snackbar.make(this.contactUsButton, errorMessageId, -2);
        this.errorSnackbar.show();
    }

    public void dismissError() {
        if (this.errorSnackbar != null) {
            this.errorSnackbar.dismiss();
        }
    }

    public Context getContext() {
        return getApplicationContext();
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(SAVED_STATE_FAB_VISIBLE, this.contactUsButton.getVisibility() == 0);
    }

    public void exitActivity() {
        finish();
    }
}
