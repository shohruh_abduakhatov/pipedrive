package com.pipedrive.util.devices;

import kotlin.Metadata;
import rx.functions.Action0;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "call"}, k = 3, mv = {1, 1, 6})
/* compiled from: MobileDevices.kt */
final class MobileDevices$loginDevice$6 implements Action0 {
    final /* synthetic */ MobileDevices this$0;

    MobileDevices$loginDevice$6(MobileDevices mobileDevices) {
        this.this$0 = mobileDevices;
    }

    public final void call() {
        this.this$0.subscribeToTopics();
    }
}
