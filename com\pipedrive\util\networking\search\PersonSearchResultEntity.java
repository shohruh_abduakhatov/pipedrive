package com.pipedrive.util.networking.search;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pipedrive.model.Person;
import com.pipedrive.model.delta.ObjectDelta;

public class PersonSearchResultEntity extends SearchResultEntity<Person> {
    @Nullable
    @SerializedName("details")
    @Expose
    private Details details;

    static class Details {
        @Nullable
        @SerializedName("email")
        @Expose
        private String email;
        @Nullable
        @SerializedName("org_name")
        @Expose
        private String organizationName;
        @Nullable
        @SerializedName("org_id")
        @Expose
        private Integer organizationPipedriveId;
        @Nullable
        @SerializedName("phone")
        @Expose
        private String phone;

        private Details(@Nullable String phone, @Nullable String email, @Nullable Integer organizationPipedriveId, @Nullable String organizationName) {
            this.phone = phone;
            this.email = email;
            this.organizationPipedriveId = organizationPipedriveId;
            this.organizationName = organizationName;
        }
    }

    @NonNull
    public Boolean isDifferentFrom(@NonNull Person person) {
        boolean namesDiffer;
        boolean z = true;
        if (ObjectDelta.areEqual(person.getName(), getName())) {
            namesDiffer = false;
        } else {
            namesDiffer = true;
        }
        boolean phonesDiffer;
        if (ObjectDelta.areEqual(person.getPhone(), getPhone())) {
            phonesDiffer = false;
        } else {
            phonesDiffer = true;
        }
        boolean emailsDiffer;
        if (ObjectDelta.areEqual(person.getEmail(), getEmail())) {
            emailsDiffer = false;
        } else {
            emailsDiffer = true;
        }
        if (namesDiffer || phonesDiffer || emailsDiffer) {
            return Boolean.valueOf(true);
        }
        if (person.getCompany() == null && getOrganizationPipedriveId() == null) {
            return Boolean.valueOf(false);
        }
        if ((person.getCompany() == null && getOrganizationPipedriveId() != null) || (person.getCompany() != null && getOrganizationPipedriveId() == null)) {
            return Boolean.valueOf(true);
        }
        if (person.getCompany().getPipedriveId() != getOrganizationPipedriveId().intValue()) {
            return Boolean.valueOf(true);
        }
        if (TextUtils.equals(person.getCompany().getName(), getOrganizationName())) {
            z = false;
        }
        return Boolean.valueOf(z);
    }

    @Nullable
    public String getPhone() {
        return this.details == null ? null : this.details.phone;
    }

    @Nullable
    public String getEmail() {
        return this.details == null ? null : this.details.email;
    }

    @Nullable
    public Integer getOrganizationPipedriveId() {
        return this.details == null ? null : this.details.organizationPipedriveId;
    }

    @Nullable
    public String getOrganizationName() {
        return this.details == null ? null : this.details.organizationName;
    }

    public PersonSearchResultEntity(@Nullable Integer pipedriveId, @Nullable String name) {
        super(pipedriveId, name);
    }

    public PersonSearchResultEntity(@Nullable Integer pipedriveId, @Nullable String name, @Nullable String phone, @Nullable String email, @Nullable Integer organizationPipedriveId, @Nullable String organizationName) {
        super(pipedriveId, name);
        this.details = new Details(phone, email, organizationPipedriveId, organizationName);
    }
}
