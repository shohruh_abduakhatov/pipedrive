package rx;

import rx.functions.Func9;
import rx.functions.FuncN;

class Single$10 implements FuncN<R> {
    final /* synthetic */ Func9 val$zipFunction;

    Single$10(Func9 func9) {
        this.val$zipFunction = func9;
    }

    public R call(Object... args) {
        return this.val$zipFunction.call(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8]);
    }
}
