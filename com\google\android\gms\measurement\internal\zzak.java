package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.zzaa;

class zzak {
    final long avX;
    final String mName;
    final String zzctj;
    final Object zzcyd;

    zzak(String str, String str2, long j, Object obj) {
        zzaa.zzib(str);
        zzaa.zzib(str2);
        zzaa.zzy(obj);
        this.zzctj = str;
        this.mName = str2;
        this.avX = j;
        this.zzcyd = obj;
    }
}
