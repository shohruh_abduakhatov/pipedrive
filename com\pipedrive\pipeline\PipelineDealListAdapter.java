package com.pipedrive.pipeline;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.views.viewholder.ViewHolderBuilder;
import com.pipedrive.views.viewholder.deal.DealRowViewHolderForDealsList;

class PipelineDealListAdapter extends CursorAdapter {
    @NonNull
    private final DealsDataSource mDealsDataSource;
    @NonNull
    private final Session mSession;

    public PipelineDealListAdapter(@NonNull Session session, @Nullable Cursor c) {
        super(session.getApplicationContext(), c, true);
        this.mDealsDataSource = new DealsDataSource(session.getDatabase());
        this.mSession = session;
    }

    @NonNull
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return ViewHolderBuilder.inflateViewAndTagWithViewHolder(context, parent, false, new DealRowViewHolderForDealsList());
    }

    public void bindView(@NonNull View view, Context context, @NonNull Cursor cursor) {
        DealRowViewHolderForDealsList tagInView = view.getTag();
        if (tagInView != null && (tagInView instanceof DealRowViewHolderForDealsList)) {
            tagInView.fill(this.mSession, context, this.mDealsDataSource.deflateCursor(cursor));
        }
    }
}
