package com.pipedrive.views.viewholder.flow;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.widget.TextView;
import butterknife.BindView;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datetime.LocaleHelper;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.flow.model.FlowItemEntity;
import com.pipedrive.model.notes.Note;
import com.pipedrive.util.notes.NotesUtil;
import com.pipedrive.views.viewholder.ViewHolder;

public class NoteRowViewHolder implements ViewHolder {
    @BindView(2131821088)
    TextView mDetails;
    @BindView(2131820730)
    TextView mNote;

    public int getLayoutResourceId() {
        return R.layout.row_note;
    }

    private void fill(@NonNull Session session, @NonNull Context context, @NonNull Note note, boolean showNoteRowAssociations) {
        fillContentAndTime(session, note.getContent(), note.getAddTime());
        if (showNoteRowAssociations) {
            this.mDetails.setText(context.getString(R.string.activity_list_item_subtitle, new Object[]{this.mDetails.getText(), ""}));
            NotesUtil.fillNoteAssociations(context.getResources(), note, this.mDetails);
        }
    }

    private void fillContentAndTime(@NonNull Session session, @Nullable String content, @NonNull PipedriveDateTime addTime) {
        this.mNote.setText(content != null ? Html.fromHtml(content).toString() : "");
        this.mDetails.setText(LocaleHelper.getLocaleBasedDateTimeStringInCurrentTimeZone(session, addTime));
    }

    public void fillWithoutAssociations(@NonNull Session session, @NonNull Context context, @NonNull Note note) {
        fill(session, context, note, false);
    }

    public void fillWithAssociations(@NonNull Session session, @NonNull Context context, @NonNull Note note) {
        fill(session, context, note, true);
    }

    public void fill(@NonNull Session session, @NonNull FlowItemEntity noteItem) {
        fillContentAndTime(session, noteItem.getTitle(), noteItem.getTimestamp());
    }
}
