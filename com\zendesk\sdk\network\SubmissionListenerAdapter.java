package com.zendesk.sdk.network;

import com.zendesk.service.ErrorResponse;

public class SubmissionListenerAdapter implements SubmissionListener {
    public void onSubmissionStarted() {
    }

    public void onSubmissionCompleted() {
    }

    public void onSubmissionCancel() {
    }

    public void onSubmissionError(ErrorResponse errorResponse) {
    }
}
