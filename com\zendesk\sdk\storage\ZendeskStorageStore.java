package com.zendesk.sdk.storage;

class ZendeskStorageStore implements StorageStore {
    private final HelpCenterSessionCache helpCenterSessionCache;
    private final IdentityStorage identityStorage;
    private final PushRegistrationResponseStorage pushRegistrationResponseStorage;
    private final RequestStorage requestStorage;
    private final SdkSettingsStorage sdkSettingsStorage;
    private final SdkStorage sdkStorage;

    ZendeskStorageStore(SdkStorage sdkStorage, IdentityStorage identityStorage, RequestStorage requestStorage, SdkSettingsStorage sdkSettingsStorage, HelpCenterSessionCache sessionCache, PushRegistrationResponseStorage pushStorage) {
        this.sdkStorage = sdkStorage;
        this.identityStorage = identityStorage;
        this.requestStorage = requestStorage;
        this.sdkSettingsStorage = sdkSettingsStorage;
        this.helpCenterSessionCache = sessionCache;
        this.pushRegistrationResponseStorage = pushStorage;
    }

    public SdkStorage sdkStorage() {
        return this.sdkStorage;
    }

    public IdentityStorage identityStorage() {
        return this.identityStorage;
    }

    public RequestStorage requestStorage() {
        return this.requestStorage;
    }

    public SdkSettingsStorage sdkSettingsStorage() {
        return this.sdkSettingsStorage;
    }

    public HelpCenterSessionCache helpCenterSessionCache() {
        return this.helpCenterSessionCache;
    }

    public PushRegistrationResponseStorage pushStorage() {
        return this.pushRegistrationResponseStorage;
    }
}
