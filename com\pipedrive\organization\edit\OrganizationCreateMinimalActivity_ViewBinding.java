package com.pipedrive.organization.edit;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class OrganizationCreateMinimalActivity_ViewBinding implements Unbinder {
    private OrganizationCreateMinimalActivity target;
    private View view2131820970;

    @UiThread
    public OrganizationCreateMinimalActivity_ViewBinding(OrganizationCreateMinimalActivity target) {
        this(target, target.getWindow().getDecorView());
    }

    @UiThread
    public OrganizationCreateMinimalActivity_ViewBinding(final OrganizationCreateMinimalActivity target, View source) {
        this.target = target;
        target.mDescriptionContainer = Utils.findRequiredView(source, R.id.descriptionContainer, "field 'mDescriptionContainer'");
        target.mMoreDetails = Utils.findRequiredView(source, R.id.moreDetails, "field 'mMoreDetails'");
        target.mMainContainer = (LinearLayout) Utils.findRequiredViewAsType(source, R.id.mainContainer, "field 'mMainContainer'", LinearLayout.class);
        target.mDescriptionText = (TextView) Utils.findRequiredViewAsType(source, R.id.description, "field 'mDescriptionText'", TextView.class);
        View view = Utils.findRequiredView(source, R.id.addMoreDetails, "method 'onClicked'");
        this.view2131820970 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onClicked();
            }
        });
    }

    @CallSuper
    public void unbind() {
        OrganizationCreateMinimalActivity target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mDescriptionContainer = null;
        target.mMoreDetails = null;
        target.mMainContainer = null;
        target.mDescriptionText = null;
        this.view2131820970.setOnClickListener(null);
        this.view2131820970 = null;
    }
}
