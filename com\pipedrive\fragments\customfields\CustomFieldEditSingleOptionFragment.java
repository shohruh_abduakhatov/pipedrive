package com.pipedrive.fragments.customfields;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.google.android.gms.plus.PlusShare;
import com.pipedrive.R;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.model.User;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.util.UsersUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CustomFieldEditSingleOptionFragment extends CustomFieldBaseFragment {
    private RadioGroup mRadioGroup;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_custom_field_edit_single_option, container, false);
        this.mRadioGroup = (RadioGroup) mainView.findViewById(R.id.customFieldRadioGroup);
        inflateSelectionOptions(inflater, container);
        return mainView;
    }

    private void inflateSelectionOptions(LayoutInflater inflater, ViewGroup container) {
        if (CustomField.FIELD_DATA_TYPE_SINGLE_OPTION.equalsIgnoreCase(this.mCustomField.getFieldDataType())) {
            try {
                generateRadioButtonsFromCustomFieldOptions(inflater, container);
                return;
            } catch (JSONException e) {
                Log.e(e);
                return;
            }
        }
        generateRadioButtonsFromUsersList(inflater, container);
    }

    protected boolean contentExists() {
        return this.mRadioGroup != null && this.mRadioGroup.getChildCount() > 0 && this.mRadioGroup.getCheckedRadioButtonId() >= 0;
    }

    protected void clearContent() {
        this.mRadioGroup.clearCheck();
    }

    protected void saveContent() {
        int selectedOptionId = this.mRadioGroup.getCheckedRadioButtonId();
        this.mCustomField.setTempValue(selectedOptionId >= 0 ? selectedOptionId + "" : null);
    }

    private void generateRadioButtonsFromCustomFieldOptions(LayoutInflater inflater, ViewGroup container) throws JSONException {
        JSONArray optionsArray = this.mCustomField.getOptions();
        for (int i = 0; i < optionsArray.length(); i++) {
            JSONObject optionsObject = optionsArray.getJSONObject(i);
            String id = optionsObject.optString(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, null);
            if (!TextUtils.isEmpty(id)) {
                RadioButton radioButton = (RadioButton) inflater.inflate(R.layout.radiobutton_custom_field_single_option, container, false);
                radioButton.setText(optionsObject.optString(PlusShare.KEY_CALL_TO_ACTION_LABEL).trim());
                radioButton.setId(Integer.valueOf(id).intValue());
                if (id.equalsIgnoreCase(this.mCustomField.getTempValue())) {
                    radioButton.setChecked(true);
                }
                this.mRadioGroup.addView(radioButton);
            }
        }
    }

    private void generateRadioButtonsFromUsersList(LayoutInflater inflater, ViewGroup container) {
        for (User user : UsersUtil.getActiveCompanyUsers(PipedriveApp.getActiveSession())) {
            String name;
            RadioButton radioButton = (RadioButton) inflater.inflate(R.layout.radiobutton_custom_field_single_option, container, false);
            if (PipedriveApp.getActiveSession().getAuthenticatedUserID() == ((long) user.getPipedriveId())) {
                name = user.getName() + getString(R.string._me);
            } else {
                name = user.getName();
            }
            radioButton.setText(name);
            radioButton.setId(user.getPipedriveId());
            if (!TextUtils.isEmpty(this.mCustomField.getTempValue()) && user.getPipedriveId() == Integer.parseInt(this.mCustomField.getTempValue())) {
                radioButton.setChecked(true);
            }
            this.mRadioGroup.addView(radioButton);
        }
    }
}
