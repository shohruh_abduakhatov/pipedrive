package com.pipedrive.model.contacts;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.google.gson.stream.JsonReader;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.SQLTransactionManager;
import com.pipedrive.logging.Log;
import com.pipedrive.model.Contact;
import com.pipedrive.model.Person;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.persons.PersonNetworkingUtil;
import com.pipedrive.util.time.TimeManager;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class PageOfPersons extends PageOfContacts {
    public static final String CONTACTS_DOWNLOADED_BROADCAST = "com.pipedrive.SendContactsDLBroadcast";
    public static final String MORE_CONTACTS_COMMING = "MORE_CONTACTS_COMMING";
    static final String TAG = PageOfPersons.class.getSimpleName();
    private List<Contact<Person>> contacts = new ArrayList();

    public List<Contact> getAllContacts() {
        List<Contact> c = new ArrayList(this.contacts.size());
        for (Contact<Person> contact : this.contacts) {
            c.add(contact);
        }
        return c;
    }

    public void deleteAllContacts() {
        this.contacts.clear();
    }

    public InputStream getPageOfContactsInputStream(Session session, int start, int limit, boolean firstStart) {
        ApiUrlBuilder uriBuilder = new ApiUrlBuilder(session, "persons");
        uriBuilder.pagination(Long.valueOf(Integer.valueOf(start).longValue()), Long.valueOf(Integer.valueOf(limit).longValue()));
        uriBuilder.param("sort_by", "update_time");
        uriBuilder.param("sort_mode", firstStart ? "asc" : "desc");
        return ConnectionUtil.requestGet(uriBuilder);
    }

    public void readAndSaveOneContact(Session session, JsonReader reader) throws IOException {
        Person person = PersonNetworkingUtil.readContact(reader);
        if (!TextUtils.isEmpty(person.getName())) {
            this.contacts.add(person);
        }
    }

    public void persistAllContacts(Session session) {
        String tag = TAG + ".saveAllContacts()";
        long time = TimeManager.getInstance().currentTimeMillis().longValue();
        Log.d(tag, "Downloaded " + this.contacts.size() + " mPersons. Starting to save them in DB under transaction.");
        SQLTransactionManager sqlTransactionManager = new SQLTransactionManager(session.getDatabase());
        sqlTransactionManager.beginTransactionNonExclusive();
        for (int i = 0; i < this.contacts.size(); i++) {
            Contact<Person> contact = (Contact) this.contacts.get(i);
            if (contact instanceof Person) {
                PersonNetworkingUtil.createOrUpdatePersonIntoDBWithRelations(session, (Person) contact);
            }
        }
        sqlTransactionManager.commit();
        Log.d(tag, "Added " + this.contacts.size() + " mPersons into database. Time: " + (TimeManager.getInstance().currentTimeMillis().longValue() - time) + "ms");
    }

    public void pageOfContactsHaveBeenLoaded(Session session, boolean moreItemsInDB) {
        Context context = session.getApplicationContext();
        Intent intent = new Intent();
        intent.setAction(CONTACTS_DOWNLOADED_BROADCAST);
        intent.putExtra(MORE_CONTACTS_COMMING, moreItemsInDB);
        context.sendBroadcast(intent);
    }

    public long getNewestUpdateTimeReadFromContactsQuery(Session session, long returnIfNotSet) {
        return session.getContactsListUpdateTime(returnIfNotSet);
    }

    public void saveNewestUpdateTimeReadFromContactsQuery(Session session, long time) {
        session.setContactsListUpdateTime(time);
    }
}
