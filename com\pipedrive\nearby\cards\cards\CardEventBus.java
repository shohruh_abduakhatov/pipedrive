package com.pipedrive.nearby.cards.cards;

import android.support.annotation.NonNull;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.AnalyticsEvent;
import com.pipedrive.nearby.model.DealNearbyItem;
import com.pipedrive.nearby.model.NearbyItem;
import com.pipedrive.nearby.model.OrganizationNearbyItem;
import com.pipedrive.nearby.util.Irrelevant;
import rx.Observable;
import rx.subjects.Subject;

public enum CardEventBus {
    INSTANCE;
    
    @NonNull
    private final Subject<Irrelevant, Irrelevant> cardSelectionEvents;
    @NonNull
    private final Subject<Irrelevant, Irrelevant> collapseEvents;
    @NonNull
    private final Subject<Long, Long> dealDetailsClicks;
    @NonNull
    private final Subject<NearbyItem, NearbyItem> directionsButtonClicks;
    @NonNull
    private final Subject<NearbyItem, NearbyItem> directionsFabClicks;
    @NonNull
    private final Subject<Irrelevant, Irrelevant> expandEvents;
    @NonNull
    private final Subject<Boolean, Boolean> expandedFlag;
    @NonNull
    private final Subject<Integer, Integer> heightChangeEvents;
    @NonNull
    private final Subject<Long, Long> organizationDetailsClicks;
    @NonNull
    private final Subject<Long, Long> personDetailsClicks;
    @NonNull
    private final Subject<Irrelevant, Irrelevant> showEmptyCardEvents;

    @NonNull
    public Observable<Irrelevant> expandEvents() {
        return this.expandEvents;
    }

    @NonNull
    public Observable<Irrelevant> cardSelectedEvents() {
        return this.cardSelectionEvents;
    }

    @NonNull
    public Observable<Integer> heightEvents() {
        return this.heightChangeEvents;
    }

    public void onCardsHeightChanged(@NonNull Integer newHeight) {
        this.heightChangeEvents.onNext(newHeight);
    }

    @NonNull
    public Observable<Irrelevant> collapseEvents() {
        return this.collapseEvents;
    }

    @NonNull
    public Observable<NearbyItem> directionsFabClicks() {
        return this.directionsFabClicks;
    }

    @NonNull
    public Observable<NearbyItem> directionsButtonClicks() {
        return this.directionsButtonClicks;
    }

    @NonNull
    public Observable<Long> dealDetailsClicks() {
        return this.dealDetailsClicks;
    }

    @NonNull
    public Observable<Long> personDetailsClicks() {
        return this.personDetailsClicks;
    }

    @NonNull
    public Observable<Long> organizationDetailsClicks() {
        return this.organizationDetailsClicks;
    }

    @NonNull
    public Observable<Irrelevant> showEmptyCardEvents() {
        return this.showEmptyCardEvents;
    }

    @NonNull
    public Observable<Boolean> isExpanded() {
        return this.expandedFlag;
    }

    public void collapseCards() {
        this.collapseEvents.onNext(Irrelevant.INSTANCE);
        this.expandedFlag.onNext(Boolean.valueOf(false));
    }

    void expandCards() {
        this.expandEvents.onNext(Irrelevant.INSTANCE);
        this.expandedFlag.onNext(Boolean.valueOf(true));
    }

    void onDirectionsButtonClicked(@NonNull NearbyItem nearbyItem) {
        this.directionsButtonClicks.onNext(nearbyItem);
        sendDirectionsAnalyticsEvent(nearbyItem, true);
    }

    void onDirectionsFabClicked(@NonNull NearbyItem nearbyItem) {
        this.directionsFabClicks.onNext(nearbyItem);
        sendDirectionsAnalyticsEvent(nearbyItem, false);
    }

    private void sendDirectionsAnalyticsEvent(@NonNull NearbyItem nearbyItem, boolean isCardExpanded) {
        if (nearbyItem instanceof DealNearbyItem) {
            Analytics.sendEvent(isCardExpanded ? AnalyticsEvent.DIRECTIONS_FOR_DEAL_EXPANDED : AnalyticsEvent.DIRECTIONS_FOR_DEAL_COLLAPSED);
        } else if (nearbyItem instanceof OrganizationNearbyItem) {
            Analytics.sendEvent(isCardExpanded ? AnalyticsEvent.DIRECTIONS_FOR_ORG_EXPANDED : AnalyticsEvent.DIRECTIONS_FOR_ORG_COLLAPSED);
        } else {
            Analytics.sendEvent(isCardExpanded ? AnalyticsEvent.DIRECTIONS_FOR_PERSON_EXPANDED : AnalyticsEvent.DIRECTIONS_FOR_PERSON_COLLAPSED);
        }
    }

    void onPersonDetailsClicked(@NonNull Long personSqlId) {
        Analytics.sendEvent(AnalyticsEvent.OPEN_PERSON_DETAIL_VIEW);
        this.personDetailsClicks.onNext(personSqlId);
    }

    void onOrganizationDetailsClicked(@NonNull Long organizationSqlId) {
        Analytics.sendEvent(AnalyticsEvent.OPEN_ORG_DETAIL_VIEW);
        this.organizationDetailsClicks.onNext(organizationSqlId);
    }

    void onDealDetailsClicked(@NonNull Long dealSqlId) {
        Analytics.sendEvent(AnalyticsEvent.OPEN_DEAL_DETAIL_VIEW);
        this.dealDetailsClicks.onNext(dealSqlId);
    }

    public void showEmptyCard() {
        this.showEmptyCardEvents.onNext(Irrelevant.INSTANCE);
    }

    public void onCardSelected() {
        this.cardSelectionEvents.onNext(Irrelevant.INSTANCE);
    }
}
