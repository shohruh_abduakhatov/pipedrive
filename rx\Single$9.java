package rx;

import rx.functions.Func8;
import rx.functions.FuncN;

class Single$9 implements FuncN<R> {
    final /* synthetic */ Func8 val$zipFunction;

    Single$9(Func8 func8) {
        this.val$zipFunction = func8;
    }

    public R call(Object... args) {
        return this.val$zipFunction.call(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
    }
}
