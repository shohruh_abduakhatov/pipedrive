package com.pipedrive.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v4.widget.CursorAdapter;
import android.widget.AlphabetIndexer;
import android.widget.SectionIndexer;
import com.pipedrive.datasource.search.SearchConstraint;

public abstract class BaseSectionedListAdapter extends CursorAdapter implements SectionIndexer {
    AlphabetIndexer mAlphabetIndexer;

    public abstract void setSearchConstraint(@NonNull SearchConstraint searchConstraint);

    BaseSectionedListAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    public Object[] getSections() {
        return this.mAlphabetIndexer == null ? new Object[0] : this.mAlphabetIndexer.getSections();
    }

    public int getPositionForSection(int sectionIndex) {
        return this.mAlphabetIndexer == null ? -1 : this.mAlphabetIndexer.getPositionForSection(sectionIndex);
    }

    public int getSectionForPosition(int position) {
        return this.mAlphabetIndexer == null ? -1 : this.mAlphabetIndexer.getSectionForPosition(position);
    }
}
