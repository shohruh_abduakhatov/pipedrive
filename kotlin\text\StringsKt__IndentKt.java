package kotlin.text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u000b\u001a!\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0002\b\u0004\u001a\u0011\u0010\u0005\u001a\u00020\u0006*\u00020\u0002H\u0002¢\u0006\u0002\b\u0007\u001a\u0014\u0010\b\u001a\u00020\u0002*\u00020\u00022\b\b\u0002\u0010\u0003\u001a\u00020\u0002\u001aJ\u0010\t\u001a\u00020\u0002*\b\u0012\u0004\u0012\u00020\u00020\n2\u0006\u0010\u000b\u001a\u00020\u00062\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\u00012\u0014\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0001H\b¢\u0006\u0002\b\u000e\u001a\u0014\u0010\u000f\u001a\u00020\u0002*\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u0002\u001a\u001e\u0010\u0011\u001a\u00020\u0002*\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u00022\b\b\u0002\u0010\u0012\u001a\u00020\u0002\u001a\n\u0010\u0013\u001a\u00020\u0002*\u00020\u0002\u001a\u0014\u0010\u0014\u001a\u00020\u0002*\u00020\u00022\b\b\u0002\u0010\u0012\u001a\u00020\u0002¨\u0006\u0015"}, d2 = {"getIndentFunction", "Lkotlin/Function1;", "", "indent", "getIndentFunction$StringsKt__IndentKt", "indentWidth", "", "indentWidth$StringsKt__IndentKt", "prependIndent", "reindent", "", "resultSizeEstimate", "indentAddFunction", "indentCutFunction", "reindent$StringsKt__IndentKt", "replaceIndent", "newIndent", "replaceIndentByMargin", "marginPrefix", "trimIndent", "trimMargin", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/text/StringsKt")
/* compiled from: Indent.kt */
class StringsKt__IndentKt {
    @NotNull
    public static /* bridge */ /* synthetic */ String trimMargin$default(String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str2 = "|";
        }
        return trimMargin(str, str2);
    }

    @NotNull
    public static final String trimMargin(@NotNull String $receiver, @NotNull String marginPrefix) {
        Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        Intrinsics.checkParameterIsNotNull(marginPrefix, "marginPrefix");
        return replaceIndentByMargin($receiver, "", marginPrefix);
    }

    @NotNull
    public static /* bridge */ /* synthetic */ String replaceIndentByMargin$default(String str, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str2 = "";
        }
        if ((i & 2) != 0) {
            str3 = "|";
        }
        return replaceIndentByMargin(str, str2, str3);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    @NotNull
    public static final String replaceIndentByMargin(@NotNull String $receiver, @NotNull String newIndent, @NotNull String marginPrefix) {
        Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        Intrinsics.checkParameterIsNotNull(newIndent, "newIndent");
        Intrinsics.checkParameterIsNotNull(marginPrefix, "marginPrefix");
        if ((!StringsKt__StringsJVMKt.isBlank((CharSequence) marginPrefix) ? 1 : null) == null) {
            throw new IllegalArgumentException("marginPrefix must be non-blank string.".toString());
        }
        String str;
        List<String> lines = StringsKt__StringsKt.lines($receiver);
        int length = $receiver.length() + (newIndent.length() * lines.size());
        Function1 indentAddFunction$iv = getIndentFunction$StringsKt__IndentKt(newIndent);
        int lastIndex$iv = CollectionsKt__CollectionsKt.getLastIndex(lines);
        Collection destination$iv$iv$iv = new ArrayList();
        int index$iv$iv$iv$iv = 0;
        for (String line : lines) {
            String it$iv$iv$iv;
            int index$iv$iv$iv$iv2 = index$iv$iv$iv$iv + 1;
            int index$iv = index$iv$iv$iv$iv;
            if ((index$iv == 0 || index$iv == lastIndex$iv) && StringsKt__StringsJVMKt.isBlank(line)) {
                it$iv$iv$iv = null;
            } else {
                Object obj;
                CharSequence $receiver$iv = line;
                index$iv = 0;
                int length2 = $receiver$iv.length() - 1;
                if (0 <= length2) {
                    while (true) {
                        if ((!CharsKt__CharJVMKt.isWhitespace($receiver$iv.charAt(index$iv)) ? 1 : null) == null) {
                            if (index$iv == length2) {
                                break;
                            }
                            index$iv++;
                        } else {
                            break;
                        }
                    }
                }
                int firstNonWhitespaceIndex = -1;
                if (firstNonWhitespaceIndex == -1) {
                    obj = null;
                } else if (StringsKt__StringsJVMKt.startsWith$default(line, marginPrefix, firstNonWhitespaceIndex, false, 4, null)) {
                    int length3 = marginPrefix.length() + firstNonWhitespaceIndex;
                    if (line == null) {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                    obj = line.substring(length3);
                    Intrinsics.checkExpressionValueIsNotNull(obj, "(this as java.lang.String).substring(startIndex)");
                } else {
                    obj = null;
                }
                if (obj != null) {
                    str = (String) indentAddFunction$iv.invoke(obj);
                    if (str != null) {
                        it$iv$iv$iv = str;
                    }
                }
                it$iv$iv$iv = line;
            }
            if (it$iv$iv$iv != null) {
                destination$iv$iv$iv.add(it$iv$iv$iv);
            }
            index$iv$iv$iv$iv = index$iv$iv$iv$iv2;
        }
        str = ((StringBuilder) CollectionsKt___CollectionsKt.joinTo$default((List) destination$iv$iv$iv, new StringBuilder(length), "\n", null, null, 0, null, null, 124, null)).toString();
        Intrinsics.checkExpressionValueIsNotNull(str, "mapIndexedNotNull { inde…\"\\n\")\n        .toString()");
        return str;
    }

    @NotNull
    public static final String trimIndent(@NotNull String $receiver) {
        Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return replaceIndent($receiver, "");
    }

    @NotNull
    public static /* bridge */ /* synthetic */ String replaceIndent$default(String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str2 = "";
        }
        return replaceIndent(str, str2);
    }

    @NotNull
    public static final String replaceIndent(@NotNull String $receiver, @NotNull String newIndent) {
        String drop;
        Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        Intrinsics.checkParameterIsNotNull(newIndent, "newIndent");
        List<String> lines = StringsKt__StringsKt.lines($receiver);
        Collection destination$iv$iv = new ArrayList();
        for (Object element$iv$iv : lines) {
            if ((!StringsKt__StringsJVMKt.isBlank((CharSequence) ((String) element$iv$iv)) ? 1 : null) != null) {
                destination$iv$iv.add(element$iv$iv);
            }
        }
        Iterable<String> iterable = (List) destination$iv$iv;
        destination$iv$iv = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(iterable, 10));
        for (String item$iv$iv : iterable) {
            destination$iv$iv.add(Integer.valueOf(indentWidth$StringsKt__IndentKt(item$iv$iv)));
        }
        Integer num = (Integer) CollectionsKt___CollectionsKt.min((Iterable) (List) destination$iv$iv);
        int minCommonIndent = num != null ? num.intValue() : 0;
        int length = $receiver.length() + (newIndent.length() * lines.size());
        Function1 indentAddFunction$iv = getIndentFunction$StringsKt__IndentKt(newIndent);
        int lastIndex$iv = CollectionsKt__CollectionsKt.getLastIndex(lines);
        Collection destination$iv$iv$iv = new ArrayList();
        int index$iv$iv$iv$iv = 0;
        for (String line : lines) {
            String it$iv$iv$iv;
            int index$iv$iv$iv$iv2 = index$iv$iv$iv$iv + 1;
            int index$iv = index$iv$iv$iv$iv;
            if ((index$iv == 0 || index$iv == lastIndex$iv) && StringsKt__StringsJVMKt.isBlank(line)) {
                it$iv$iv$iv = null;
            } else {
                drop = StringsKt___StringsKt.drop(line, minCommonIndent);
                if (drop != null) {
                    drop = (String) indentAddFunction$iv.invoke(drop);
                    if (drop != null) {
                        it$iv$iv$iv = drop;
                    }
                }
                it$iv$iv$iv = line;
            }
            if (it$iv$iv$iv != null) {
                destination$iv$iv$iv.add(it$iv$iv$iv);
            }
            index$iv$iv$iv$iv = index$iv$iv$iv$iv2;
        }
        drop = ((StringBuilder) CollectionsKt___CollectionsKt.joinTo$default((List) destination$iv$iv$iv, new StringBuilder(length), "\n", null, null, 0, null, null, 124, null)).toString();
        Intrinsics.checkExpressionValueIsNotNull(drop, "mapIndexedNotNull { inde…\"\\n\")\n        .toString()");
        return drop;
    }

    @NotNull
    public static /* bridge */ /* synthetic */ String prependIndent$default(String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str2 = "    ";
        }
        return prependIndent(str, str2);
    }

    @NotNull
    public static final String prependIndent(@NotNull String $receiver, @NotNull String indent) {
        Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        Intrinsics.checkParameterIsNotNull(indent, "indent");
        return SequencesKt___SequencesKt.joinToString$default(SequencesKt___SequencesKt.map(StringsKt__StringsKt.lineSequence($receiver), new StringsKt__IndentKt$prependIndent$1(indent)), "\n", null, null, 0, null, null, 62, null);
    }

    private static final int indentWidth$StringsKt__IndentKt(@NotNull String $receiver) {
        char it;
        CharSequence $receiver$iv = $receiver;
        char length = $receiver$iv.length() - 1;
        if ('\u0000' <= length) {
            char index$iv = '\u0000';
            while (true) {
                if ((!CharsKt__CharJVMKt.isWhitespace($receiver$iv.charAt(index$iv)) ? 1 : null) == null) {
                    if (index$iv == length) {
                        break;
                    }
                    index$iv++;
                } else {
                    break;
                }
            }
            it = index$iv;
            return it != '￿' ? $receiver.length() : it;
        }
        it = '￿';
        if (it != '￿') {
        }
    }

    private static final Function1<String, String> getIndentFunction$StringsKt__IndentKt(String indent) {
        if ((((CharSequence) indent).length() == 0 ? 1 : null) != null) {
            return StringsKt__IndentKt$getIndentFunction$1.INSTANCE;
        }
        return new StringsKt__IndentKt$getIndentFunction$2(indent);
    }

    private static final String reindent$StringsKt__IndentKt(@NotNull List<String> $receiver, int resultSizeEstimate, Function1<? super String, String> indentAddFunction, Function1<? super String, String> indentCutFunction) {
        String str;
        int lastIndex = CollectionsKt__CollectionsKt.getLastIndex($receiver);
        Collection destination$iv$iv = new ArrayList();
        int index$iv$iv$iv = 0;
        for (String str2 : $receiver) {
            String it$iv$iv;
            int index$iv$iv$iv2 = index$iv$iv$iv + 1;
            int index = index$iv$iv$iv;
            if ((index == 0 || index == lastIndex) && StringsKt__StringsJVMKt.isBlank(str2)) {
                it$iv$iv = null;
            } else {
                str = (String) indentCutFunction.invoke(str2);
                if (str != null) {
                    str = (String) indentAddFunction.invoke(str);
                    if (str != null) {
                        it$iv$iv = str;
                    }
                }
                it$iv$iv = str2;
            }
            if (it$iv$iv != null) {
                destination$iv$iv.add(it$iv$iv);
            }
            index$iv$iv$iv = index$iv$iv$iv2;
        }
        str = ((StringBuilder) CollectionsKt___CollectionsKt.joinTo$default((List) destination$iv$iv, new StringBuilder(resultSizeEstimate), "\n", null, null, 0, null, null, 124, null)).toString();
        Intrinsics.checkExpressionValueIsNotNull(str, "mapIndexedNotNull { inde…\"\\n\")\n        .toString()");
        return str;
    }
}
