package com.google.android.gms.wearable.internal;

import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.wearable.internal.zzau.zza;

public final class zzt extends zza {
    private zzu aTB;
    private zzm aTx;
    private final Object zzako = new Object();

    public void zza(zzu com_google_android_gms_wearable_internal_zzu) {
        synchronized (this.zzako) {
            this.aTB = (zzu) zzaa.zzy(com_google_android_gms_wearable_internal_zzu);
            zzm com_google_android_gms_wearable_internal_zzm = this.aTx;
        }
        if (com_google_android_gms_wearable_internal_zzm != null) {
            com_google_android_gms_wearable_internal_zzu.zzb(com_google_android_gms_wearable_internal_zzm);
        }
    }

    public void zzaa(int i, int i2) {
        synchronized (this.zzako) {
            zzu com_google_android_gms_wearable_internal_zzu = this.aTB;
            zzm com_google_android_gms_wearable_internal_zzm = new zzm(i, i2);
            this.aTx = com_google_android_gms_wearable_internal_zzm;
        }
        if (com_google_android_gms_wearable_internal_zzu != null) {
            com_google_android_gms_wearable_internal_zzu.zzb(com_google_android_gms_wearable_internal_zzm);
        }
    }
}
