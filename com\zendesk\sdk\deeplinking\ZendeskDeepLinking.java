package com.zendesk.sdk.deeplinking;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import com.zendesk.sdk.deeplinking.actions.Action;
import com.zendesk.sdk.deeplinking.actions.ActionData;
import com.zendesk.sdk.deeplinking.actions.ActionHandler;
import com.zendesk.sdk.deeplinking.actions.ActionRefreshComments.ActionRefreshCommentsData;
import com.zendesk.sdk.deeplinking.actions.ActionType;
import com.zendesk.sdk.deeplinking.targets.ArticleConfiguration;
import com.zendesk.sdk.deeplinking.targets.DeepLinkingTargetArticle;
import com.zendesk.sdk.deeplinking.targets.DeepLinkingTargetRequest;
import com.zendesk.sdk.deeplinking.targets.RequestConfiguration;
import com.zendesk.sdk.model.helpcenter.Article;
import com.zendesk.sdk.model.helpcenter.SimpleArticle;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.WeakHashMap;

public enum ZendeskDeepLinking {
    INSTANCE;
    
    private WeakHashMap<ActionHandler, Action[]> mActiveHandlers;

    public void registerAction(ActionHandler actionHandler, Action... actions) {
        this.mActiveHandlers.put(actionHandler, actions);
    }

    public void unregisterAction(ActionHandler actionHandler) {
        this.mActiveHandlers.remove(actionHandler);
    }

    public boolean refreshComments(String requestId) {
        ActionRefreshCommentsData actionRefreshCommentsData = new ActionRefreshCommentsData(requestId);
        boolean canHandle = canHandle(ActionType.RELOAD_COMMENT_STREAM, actionRefreshCommentsData);
        if (canHandle) {
            execute(ActionType.RELOAD_COMMENT_STREAM, actionRefreshCommentsData);
        }
        return canHandle;
    }

    @Nullable
    public Intent getRequestIntent(Context context, String requestId, @Nullable String subject, @Nullable ArrayList<Intent> backStackActivities, @Nullable Intent fallbackActivity) {
        return new DeepLinkingTargetRequest().getIntent(context, new RequestConfiguration(requestId, subject, backStackActivities, fallbackActivity));
    }

    public Intent getRequestIntent(Context context, String requestId, @Nullable String subject, @Nullable ArrayList<Intent> backStackActivities, @Nullable Intent fallbackActivity, String zendeskUrl, String applicationId, String oauthClientId) {
        return new DeepLinkingTargetRequest().getIntent(context, new RequestConfiguration(requestId, subject, backStackActivities, fallbackActivity), zendeskUrl, applicationId, oauthClientId);
    }

    @Nullable
    public Intent getArticleIntent(Context context, Article article, @Nullable ArrayList<Intent> backStackActivities, @Nullable Intent fallbackActivity) {
        return new DeepLinkingTargetArticle().getIntent(context, new ArticleConfiguration(article, (ArrayList) backStackActivities, fallbackActivity));
    }

    public Intent getArticleIntent(Context context, Article article, @Nullable ArrayList<Intent> backStackItemArrayList, @Nullable Intent fallbackActivity, String zendeskUrl, String applicationId, String oauthClientId) {
        return new DeepLinkingTargetArticle().getIntent(context, new ArticleConfiguration(article, (ArrayList) backStackItemArrayList, fallbackActivity), zendeskUrl, applicationId, oauthClientId);
    }

    @Nullable
    public Intent getArticleIntent(Context context, SimpleArticle simpleArticle, ArrayList<Intent> backStackActivities, Intent fallbackActivity) {
        return new DeepLinkingTargetArticle().getIntent(context, new ArticleConfiguration(simpleArticle, (ArrayList) backStackActivities, fallbackActivity));
    }

    public Intent getArticleIntent(Context context, SimpleArticle simpleArticle, ArrayList<Intent> backStackItemArrayList, Intent fallbackActivity, String zendeskUrl, String applicationId, String oauthClientId) {
        return new DeepLinkingTargetArticle().getIntent(context, new ArticleConfiguration(simpleArticle, (ArrayList) backStackItemArrayList, fallbackActivity), zendeskUrl, applicationId, oauthClientId);
    }

    private void execute(ActionType actionType, final ActionData actionData) {
        for (final Entry<ActionHandler, Action[]> entry : getCompatibleEntries(actionType, actionData)) {
            for (final Action action : (Action[]) entry.getValue()) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        action.execute((ActionHandler) entry.getKey(), actionData);
                    }
                });
            }
        }
    }

    private List<Entry<ActionHandler, Action[]>> getCompatibleEntries(ActionType actionType, ActionData actionData) {
        List<Entry<ActionHandler, Action[]>> result = new ArrayList();
        for (Entry<ActionHandler, Action[]> entry : this.mActiveHandlers.entrySet()) {
            List<Action> validActions = new ArrayList();
            for (Action action : (Action[]) entry.getValue()) {
                if (action.getActionType() == actionType && action.canHandleData(actionData)) {
                    validActions.add(action);
                }
            }
            if (validActions.size() > 0) {
                result.add(new SimpleEntry(entry.getKey(), validActions.toArray(new Action[validActions.size()])));
            }
        }
        return result;
    }

    private boolean canHandle(ActionType actionType, ActionData data) {
        return getCompatibleEntries(actionType, data).size() > 0;
    }
}
