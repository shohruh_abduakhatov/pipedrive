package com.pipedrive.note;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.navigator.Navigator.Type;
import java.util.List;

public class NoteCreateActivity extends NoteEditorActivity {
    private static final String ARG_ADD_NOTE_ORGANIZATION_SQL_ID = "com.pipedrive.note.NoteCreateActivity.ARG_ADD_NOTE_ORGANIZATION_SQL_ID";
    private static final String ARG_ADD_NOTE_PERSON_SQL_ID = "com.pipedrive.note.NoteCreateActivity.ARG_ADD_NOTE_PERSON_SQL_ID";
    private static final String ARG_CREATE_NOTE_FOR_DEAL_SQL_ID = "com.pipedrive.note.NoteCreateActivity.ARG_CREATE_NOTE_FOR_DEAL_SQL_ID";

    public /* bridge */ /* synthetic */ void createOrUpdateDiscardedAsRequiredFieldsAreNotSet() {
        super.createOrUpdateDiscardedAsRequiredFieldsAreNotSet();
    }

    @Nullable
    public /* bridge */ /* synthetic */ List getAdditionalButtons() {
        return super.getAdditionalButtons();
    }

    public /* bridge */ /* synthetic */ void onCancel() {
        super.onCancel();
    }

    public /* bridge */ /* synthetic */ void onCreateOrUpdate() {
        super.onCreateOrUpdate();
    }

    public /* bridge */ /* synthetic */ void onNoteCreated(boolean z) {
        super.onNoteCreated(z);
    }

    public /* bridge */ /* synthetic */ void onNoteDeleted(boolean z) {
        super.onNoteDeleted(z);
    }

    public /* bridge */ /* synthetic */ void onNoteUpdated(boolean z) {
        super.onNoteUpdated(z);
    }

    public /* bridge */ /* synthetic */ void onResume() {
        super.onResume();
    }

    @MainThread
    public static void createNoteForDeal(@Nullable Activity activity, @Nullable Deal deal) {
        boolean cannotCreateNote = activity == null || deal == null || !deal.isStored();
        if (!cannotCreateNote) {
            ContextCompat.startActivity(activity, new Intent(activity, NoteCreateActivity.class).putExtra(ARG_CREATE_NOTE_FOR_DEAL_SQL_ID, deal.getSqlId()), null);
        }
    }

    @MainThread
    public static void createNoteForPerson(@Nullable Activity activity, @Nullable Person person) {
        boolean cannotCreateNoteForPerson = activity == null || person == null || !person.isStored();
        if (!cannotCreateNoteForPerson) {
            ContextCompat.startActivity(activity, new Intent(activity, NoteCreateActivity.class).putExtra(ARG_ADD_NOTE_PERSON_SQL_ID, person.getSqlId()), null);
        }
    }

    @MainThread
    public static void createNoteForOrganization(@Nullable Activity activity, @Nullable Organization organization) {
        boolean cannotCreateNoteForOrganization = activity == null || organization == null || !organization.isStored();
        if (!cannotCreateNoteForOrganization) {
            ContextCompat.startActivity(activity, new Intent(activity, NoteCreateActivity.class).putExtra(ARG_ADD_NOTE_ORGANIZATION_SQL_ID, organization.getSqlId()), null);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavigatorType(Type.CREATE);
    }

    protected boolean requestNote(@Nullable Bundle args) {
        if (args == null) {
            return false;
        }
        if (args.containsKey(ARG_ADD_NOTE_ORGANIZATION_SQL_ID)) {
            ((NoteEditorPresenter) this.mPresenter).requestNewNoteForOrg(args.getLong(ARG_ADD_NOTE_ORGANIZATION_SQL_ID));
            return true;
        } else if (args.containsKey(ARG_ADD_NOTE_PERSON_SQL_ID)) {
            ((NoteEditorPresenter) this.mPresenter).requestNewNoteForPerson(args.getLong(ARG_ADD_NOTE_PERSON_SQL_ID));
            return true;
        } else if (!args.containsKey(ARG_CREATE_NOTE_FOR_DEAL_SQL_ID)) {
            return false;
        } else {
            ((NoteEditorPresenter) this.mPresenter).requestNewNoteForDeal(args.getLong(ARG_CREATE_NOTE_FOR_DEAL_SQL_ID));
            return true;
        }
    }
}
