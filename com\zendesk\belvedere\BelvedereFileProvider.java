package com.zendesk.belvedere;

import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MatrixCursor.RowBuilder;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import android.util.Log;
import java.util.Arrays;

public class BelvedereFileProvider extends FileProvider {
    private static final String LOG_TAG = "BelvedereFileProvider";

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor source = super.query(uri, projection, selection, selectionArgs, sortOrder);
        if (source == null) {
            Log.w(LOG_TAG, "Not able to apply workaround, super.query(...) returned null");
            return null;
        }
        String[] columnNames = source.getColumnNames();
        Cursor cursor = new MatrixCursor(columnNamesWithData(columnNames), source.getCount());
        source.moveToPosition(-1);
        while (source.moveToNext()) {
            RowBuilder row = cursor.newRow();
            for (int i = 0; i < columnNames.length; i++) {
                row.add(source.getString(i));
            }
        }
        source.close();
        return cursor;
    }

    private String[] columnNamesWithData(String[] columnNames) {
        for (String columnName : columnNames) {
            if ("_data".equals(columnName)) {
                return columnNames;
            }
        }
        String[] newColumnNames = (String[]) Arrays.copyOf(columnNames, columnNames.length + 1);
        newColumnNames[columnNames.length] = "_data";
        return newColumnNames;
    }
}
