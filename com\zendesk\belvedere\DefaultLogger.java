package com.zendesk.belvedere;

import android.util.Log;

class DefaultLogger implements BelvedereLogger {
    private boolean loggable = false;

    DefaultLogger() {
    }

    public void d(String tag, String msg) {
        if (this.loggable) {
            Log.d(tag, msg);
        }
    }

    public void w(String tag, String msg) {
        if (this.loggable) {
            Log.w(tag, msg);
        }
    }

    public void e(String tag, String msg) {
        if (this.loggable) {
            Log.e(tag, msg);
        }
    }

    public void e(String tag, String msg, Throwable e) {
        if (this.loggable) {
            Log.e(tag, msg, e);
        }
    }

    public void setLoggable(boolean enabled) {
        this.loggable = enabled;
    }
}
