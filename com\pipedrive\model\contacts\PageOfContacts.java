package com.pipedrive.model.contacts;

import com.google.gson.stream.JsonReader;
import com.pipedrive.application.Session;
import com.pipedrive.model.Contact;
import com.pipedrive.model.pagination.Pagination;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public abstract class PageOfContacts extends Pagination {
    public long newestUpdateTimeReadFromContactsQuery = Long.MIN_VALUE;

    public abstract void deleteAllContacts();

    public abstract List<Contact> getAllContacts();

    public abstract long getNewestUpdateTimeReadFromContactsQuery(Session session, long j);

    public abstract InputStream getPageOfContactsInputStream(Session session, int i, int i2, boolean z);

    public abstract void pageOfContactsHaveBeenLoaded(Session session, boolean z);

    public abstract void persistAllContacts(Session session);

    public abstract void readAndSaveOneContact(Session session, JsonReader jsonReader) throws IOException;

    public abstract void saveNewestUpdateTimeReadFromContactsQuery(Session session, long j);

    public String toString() {
        return "PageOfContacts{newestUpdateTimeReadFromContactsQuery=" + this.newestUpdateTimeReadFromContactsQuery + ", super.toString=" + super.toString() + "}";
    }
}
