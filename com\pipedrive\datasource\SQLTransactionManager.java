package com.pipedrive.datasource;

import android.database.sqlite.SQLiteDatabase;
import android.os.Build.VERSION;
import com.pipedrive.logging.Log;

public class SQLTransactionManager {
    private static final boolean NONEXCLUSIVE_TRANSACTION_SUPPORTED = (VERSION.SDK_INT >= 11);
    private static final String TAG = SQLTransactionManager.class.getSimpleName();
    private final SQLiteDatabase db;
    private boolean mInTransaction = false;

    public SQLTransactionManager(SQLiteDatabase dbconn) {
        this.db = dbconn;
    }

    public void beginTransaction() {
        String tag = TAG + ".beginTransaction()";
        if (NONEXCLUSIVE_TRANSACTION_SUPPORTED) {
            beginTransactionNonExclusive();
        } else if (this.mInTransaction) {
            Log.d(tag, "Transaction already effective! Will not request a new one.");
        } else {
            Log.d(tag, "Starting transaction in EXCLUSIVE mode");
            this.mInTransaction = true;
            this.db.beginTransaction();
            Log.d(tag, "done.");
        }
    }

    public void beginTransactionNonExclusive() {
        String tag = TAG + ".beginTransactionNonExclusive()";
        if (!NONEXCLUSIVE_TRANSACTION_SUPPORTED) {
            Log.d(tag, "NonExclusive transaction is not supported below APIv11. Using exclusive transaction.");
            beginTransaction();
        } else if (this.mInTransaction) {
            Log.d(tag, "Transaction already effective! Will not request a new one.");
        } else {
            Log.d(tag, "Starting NonExclusive transaction.");
            this.mInTransaction = true;
            this.db.beginTransactionNonExclusive();
            Log.d(tag, "done.");
        }
    }

    public void commit() {
        String tag = TAG + ".commit()";
        if (this.mInTransaction) {
            Log.d(tag, "Ending transaction successfully.");
            this.db.setTransactionSuccessful();
            this.db.endTransaction();
            this.mInTransaction = false;
            Log.d(tag, "done.");
            return;
        }
        Log.d(tag, "Transaction is not effective! Cannot end transaction!");
    }

    public void rollback() {
        String tag = TAG + ".rollback()";
        if (this.mInTransaction) {
            Log.d(tag, "Rolling back for the current transaction!");
            this.db.endTransaction();
            this.mInTransaction = false;
            Log.d(tag, "done.");
            return;
        }
        Log.d(tag, "Transaction is not effective! Cannot end transaction!");
    }

    public SQLiteDatabase getTransactionalDBConnection() {
        return this.db;
    }
}
