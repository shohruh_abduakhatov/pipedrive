package com.pipedrive.dialogs.communicationmediumlist;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog.Builder;
import com.pipedrive.R;
import com.pipedrive.dialogs.BaseDialogFragment;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.contact.CommunicationMedium;
import com.pipedrive.model.contact.Phone;
import com.pipedrive.nearby.cards.cards.SingleCardWithCommunicationButtons;
import java.util.ArrayList;
import java.util.List;

public class CommunicationMediumListDialogFragment extends BaseDialogFragment {
    private static final String KEY_ACTION = "ACTION";
    private static final String TAG = CommunicationMediumListDialogFragment.class.getSimpleName();
    private CommunicationMediumDialogController<BaseDatasourceEntity> mController;

    protected enum Action {
        CALL,
        MESSAGE
    }

    protected enum Source {
        DEAL,
        PERSON,
        ACTIVITY
    }

    private static CommunicationMediumListDialogFragment newInstance(@NonNull Action action, @Nullable Fragment targetFragment) {
        Bundle args = new Bundle();
        args.putSerializable(KEY_ACTION, action);
        CommunicationMediumListDialogFragment fragment = new CommunicationMediumListDialogFragment();
        fragment.setArguments(args);
        if (targetFragment != null) {
            fragment.setTargetFragment(targetFragment, 0);
        }
        return fragment;
    }

    public static void showCallSelector(@NonNull FragmentManager fragmentManager) {
        showCallSelector(fragmentManager, null);
    }

    public static void showSendMessageSelector(@NonNull FragmentManager fragmentManager) {
        showSendMessageSelector(fragmentManager, null);
    }

    public static void showCallSelector(@NonNull FragmentManager fragmentManager, @Nullable Fragment targetFragment) {
        newInstance(Action.CALL, targetFragment).show(fragmentManager, TAG);
    }

    public static void showSendMessageSelector(@NonNull FragmentManager fragmentManager, @Nullable Fragment targetFragment) {
        newInstance(Action.MESSAGE, targetFragment).show(fragmentManager, TAG);
    }

    public static void showSendMessageSelectorForCardWithCommunicationButtons(@NonNull FragmentManager fragmentManager, @NonNull SingleCardWithCommunicationButtons card) {
        showSelectorForCardWithCommunicationButtons(fragmentManager, card, Action.MESSAGE);
    }

    public static void showCallSelectorForCardWithCommunicationButtons(@NonNull FragmentManager fragmentManager, @NonNull SingleCardWithCommunicationButtons card) {
        showSelectorForCardWithCommunicationButtons(fragmentManager, card, Action.CALL);
    }

    private static void showSelectorForCardWithCommunicationButtons(@NonNull FragmentManager fragmentManager, @NonNull SingleCardWithCommunicationButtons card, @NonNull Action action) {
        CommunicationMediumListDialogFragment dialogFragment = newInstance(action, null);
        dialogFragment.setController(card);
        dialogFragment.show(fragmentManager, TAG);
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Action action = (Action) getArguments().getSerializable(KEY_ACTION);
        List<CommunicationMedium> items = new ArrayList();
        if (!(getController() == null || getController().getEntity() == null)) {
            items.addAll(getController().getPhones(getController().getEntity()));
            if (action == Action.MESSAGE) {
                items.addAll(getController().getEmails(getController().getEntity()));
            }
        }
        boolean actionIsCall = action == Action.CALL;
        int titleResId = actionIsCall ? R.string.call : R.string.send_message;
        final CommunicationMediumListAdapter adapter = new CommunicationMediumListAdapter(items, actionIsCall);
        return new Builder(getActivity()).setAdapter(adapter, new OnClickListener() {
            public void onClick(@NonNull DialogInterface dialog, int which) {
                CommunicationMedium item = adapter.getItem(which);
                if (CommunicationMediumListDialogFragment.this.getController() != null && CommunicationMediumListDialogFragment.this.getController().getEntity() != null) {
                    if (action == Action.CALL) {
                        CommunicationMediumListDialogFragment.this.getController().onCallRequested(item, CommunicationMediumListDialogFragment.this.getController().getEntity());
                    } else if (item instanceof Phone) {
                        CommunicationMediumListDialogFragment.this.getController().onSmsRequested(item, CommunicationMediumListDialogFragment.this.getController().getEntity());
                    } else {
                        CommunicationMediumListDialogFragment.this.getController().onEmailRequested(item, CommunicationMediumListDialogFragment.this.getController().getEntity());
                    }
                }
            }
        }).setTitle(titleResId).create();
    }

    @Nullable
    private CommunicationMediumDialogController<BaseDatasourceEntity> getController() {
        if (this.mController != null) {
            return this.mController;
        }
        if (getTargetFragment() != null && (getTargetFragment() instanceof CommunicationMediumDialogController)) {
            this.mController = (CommunicationMediumDialogController) getTargetFragment();
        } else if (getActivity() != null && (getActivity() instanceof CommunicationMediumDialogController)) {
            this.mController = (CommunicationMediumDialogController) getActivity();
        }
        return this.mController;
    }

    private void setController(@NonNull CommunicationMediumDialogController controller) {
        this.mController = controller;
    }

    public void show(FragmentManager manager, String tag) {
        if (manager.findFragmentByTag(tag) == null) {
            super.show(manager, tag);
        }
    }
}
