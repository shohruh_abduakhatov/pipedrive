package com.pipedrive.whatsnew;

import android.app.Activity;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.view.ViewPager.PageTransformer;
import android.view.View;
import android.widget.ImageButton;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.pipedrive.BaseActivity;
import com.pipedrive.R;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.pipeline.PipelineActivity;
import com.pipedrive.util.ImageUtil;
import com.viewpagerindicator.CirclePageIndicator;

public class WhatsNewActivity extends BaseActivity implements OnPageChangeListener, PageTransformer {
    @BindView(2131820842)
    CirclePageIndicator mCirclePageIndicator;
    @BindView(2131820841)
    View mWhatsNewNavigator;
    @BindView(2131820845)
    View mWhatsNewNavigatorForLastSlide;
    @BindView(2131820844)
    ImageButton mWhatsNewNext;
    @BindView(2131820840)
    ViewPager mWhatsNewPager;
    @NonNull
    private WhatsNewSlide[] mWhatsNewSlides = new WhatsNewSlide[0];

    public static void startWhatsNewActivityIfRequiredOrMoveToPipelineActivity(@NonNull Session session, @NonNull Activity activity) {
        boolean showWhatsNewScreens = session.getSharedSession().displayWhatsNewScreens() && hasWhatsNewScreensToShow(activity);
        if (showWhatsNewScreens) {
            ActivityCompat.startActivity(activity, new Intent(activity, WhatsNewActivity.class).addFlags(67108864), null);
        } else {
            PipelineActivity.startActivity(activity);
        }
    }

    private static boolean hasWhatsNewScreensToShow(@NonNull Activity activity) {
        return getWhatsNewSlideLayoutResIds(activity).length > 0;
    }

    private static int[] getWhatsNewSlideLayoutResIds(@NonNull Activity activity) {
        TypedArray whatsNewSlides = activity.getResources().obtainTypedArray(R.array.whatsnew_ordered_layout_listing);
        int numberOfNewSlidesFound = whatsNewSlides.length();
        int[] whatsNewSlideLayoutResIds = new int[numberOfNewSlidesFound];
        for (int i = 0; i < numberOfNewSlidesFound; i++) {
            whatsNewSlideLayoutResIds[i] = whatsNewSlides.getResourceId(i, 0);
        }
        whatsNewSlides.recycle();
        return whatsNewSlideLayoutResIds;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whatsnew);
        ButterKnife.bind(this);
        loadWhatsNewSlideLayoutResIds();
    }

    private void loadWhatsNewSlideLayoutResIds() {
        int i;
        boolean addGoogleAnalyticsScreenTags;
        int[] whatsNewSlideLayoutResIds = getWhatsNewSlideLayoutResIds(this);
        this.mWhatsNewSlides = new WhatsNewSlide[whatsNewSlideLayoutResIds.length];
        for (i = 0; i < whatsNewSlideLayoutResIds.length; i++) {
            this.mWhatsNewSlides[i] = new WhatsNewSlide(whatsNewSlideLayoutResIds[i]);
        }
        if (this.mWhatsNewSlides.length > 0) {
            addGoogleAnalyticsScreenTags = true;
        } else {
            addGoogleAnalyticsScreenTags = false;
        }
        if (addGoogleAnalyticsScreenTags) {
            String[] whatsNewSlidesGoogleAnalyticsScreenValues = getResources().getStringArray(R.array.whatsnew_ordered_layout_listing_ga_screen_values);
            for (i = 0; i < whatsNewSlidesGoogleAnalyticsScreenValues.length; i++) {
                boolean slideExistsForThisGoogleAnalyticsScreenValue;
                String whatsNewSlidesGoogleAnalyticsScreenValue = whatsNewSlidesGoogleAnalyticsScreenValues[i];
                if (i < this.mWhatsNewSlides.length) {
                    slideExistsForThisGoogleAnalyticsScreenValue = true;
                } else {
                    slideExistsForThisGoogleAnalyticsScreenValue = false;
                }
                if (slideExistsForThisGoogleAnalyticsScreenValue) {
                    WhatsNewSlide whatsNewSlide = this.mWhatsNewSlides[i];
                    whatsNewSlide.googleAnalyticsScreenHitValue = whatsNewSlidesGoogleAnalyticsScreenValue;
                    whatsNewSlide.pageTransformer = new FadeInOutPageTransformer();
                }
            }
        }
    }

    protected void onStart() {
        super.onStart();
        setupWhatsNewPager();
        setupNavigationBottomBar();
    }

    private void setupWhatsNewPager() {
        boolean onlyOneScreenIsDefined;
        WhatsNewPagerAdapter whatsNewPagerAdapter = new WhatsNewPagerAdapter(this.mWhatsNewSlides);
        this.mWhatsNewPager.setOffscreenPageLimit(this.mWhatsNewSlides.length);
        this.mWhatsNewPager.setAdapter(whatsNewPagerAdapter);
        if (this.mWhatsNewSlides.length <= 1) {
            onlyOneScreenIsDefined = true;
        } else {
            onlyOneScreenIsDefined = false;
        }
        if (!onlyOneScreenIsDefined) {
            this.mWhatsNewPager.addOnPageChangeListener(this);
            this.mWhatsNewPager.setPageTransformer(true, this);
        }
        this.mCirclePageIndicator.setViewPager(this.mWhatsNewPager);
        this.mWhatsNewPager.setCurrentItem(0);
    }

    private void setupNavigationBottomBar() {
        int i;
        int i2 = 8;
        boolean onlyOneScreenIsDefined = true;
        if (this.mWhatsNewSlides.length > 1) {
            onlyOneScreenIsDefined = false;
        }
        boolean showOnlyFinalSlideNavigator = onlyOneScreenIsDefined;
        View view = this.mWhatsNewNavigatorForLastSlide;
        if (showOnlyFinalSlideNavigator) {
            i = 0;
        } else {
            i = 8;
        }
        view.setVisibility(i);
        View view2 = this.mWhatsNewNavigator;
        if (!showOnlyFinalSlideNavigator) {
            i2 = 0;
        }
        view2.setVisibility(i2);
        if (!showOnlyFinalSlideNavigator) {
            this.mWhatsNewNext.setImageDrawable(ImageUtil.getTintedDrawable(this.mWhatsNewNext.getDrawable(), ContextCompat.getColor(this, R.color.icon_tint)));
        }
    }

    private void whatsNewSlidesConsumed() {
        if (PipedriveApp.getSessionManager().hasActiveSession()) {
            PipedriveApp.getActiveSession().getSharedSession().disableDisplayingWhatsNewScreensForThisVersionCode();
        }
        PipelineActivity.startActivity(this);
        finish();
    }

    @OnClick({2131820844})
    void onWhatsNewNextClicked() {
        this.mWhatsNewPager.setCurrentItem(this.mWhatsNewPager.getCurrentItem() + 1);
    }

    @OnClick({2131820843})
    void onWhatsNewSkipClicked() {
        whatsNewSlidesConsumed();
    }

    @OnClick({2131820846})
    void onWhatsNewOkClicked() {
        whatsNewSlidesConsumed();
    }

    public void onBackPressed() {
        PipelineActivity.startActivity(this);
        super.onBackPressed();
    }

    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        setupNavigationBar(position, positionOffset);
        sendGoogleAnalyticsHitScreen(position, positionOffset);
    }

    public void transformPage(View page, float position) {
        PageTransformer pageTransformer = this.mWhatsNewSlides[((Integer) page.getTag()).intValue()].pageTransformer;
        if (pageTransformer != null) {
            pageTransformer.transformPage(page, position);
        }
    }

    private void setupNavigationBar(int position, float positionOffset) {
        boolean lastPositionArrived;
        boolean fadeWhatsNewNavigatorViewsInOrOut;
        boolean setWhatsNewNavigatorView;
        if (position == this.mWhatsNewSlides.length - 1 && positionOffset == 0.0f) {
            lastPositionArrived = true;
        } else {
            lastPositionArrived = false;
        }
        boolean lastPositionBeingDraggedInOrOut;
        if (position != this.mWhatsNewSlides.length - 2 || positionOffset == 0.0f) {
            lastPositionBeingDraggedInOrOut = false;
        } else {
            lastPositionBeingDraggedInOrOut = true;
        }
        if (lastPositionArrived || !lastPositionBeingDraggedInOrOut) {
            fadeWhatsNewNavigatorViewsInOrOut = false;
        } else {
            fadeWhatsNewNavigatorViewsInOrOut = true;
        }
        if (lastPositionArrived || lastPositionBeingDraggedInOrOut) {
            setWhatsNewNavigatorView = false;
        } else {
            setWhatsNewNavigatorView = true;
        }
        boolean setWhatsNewNavigatorForLastSlideView = lastPositionArrived;
        if (fadeWhatsNewNavigatorViewsInOrOut) {
            this.mWhatsNewNavigator.setVisibility(0);
            this.mWhatsNewNavigator.setAlpha(1.0f - positionOffset);
            this.mWhatsNewNavigatorForLastSlide.setVisibility(0);
            this.mWhatsNewNavigatorForLastSlide.setAlpha(positionOffset);
        } else if (setWhatsNewNavigatorView) {
            this.mWhatsNewNavigator.setAlpha(1.0f);
            this.mWhatsNewNavigatorForLastSlide.setAlpha(0.0f);
        } else if (setWhatsNewNavigatorForLastSlideView) {
            this.mWhatsNewNavigator.setAlpha(0.0f);
            this.mWhatsNewNavigatorForLastSlide.setAlpha(1.0f);
        }
    }

    private void sendGoogleAnalyticsHitScreen(int position, float positionOffset) {
        boolean arrivedToAnyPosition;
        boolean canAcquireGoogleAnalyticsScreenValueForPosition = true;
        if (positionOffset == 0.0f) {
            arrivedToAnyPosition = true;
        } else {
            arrivedToAnyPosition = false;
        }
        if (arrivedToAnyPosition) {
            String googleAnalyticsScreenHitValue;
            if (position < 0 || position >= this.mWhatsNewSlides.length) {
                canAcquireGoogleAnalyticsScreenValueForPosition = false;
            }
            if (canAcquireGoogleAnalyticsScreenValueForPosition) {
                googleAnalyticsScreenHitValue = this.mWhatsNewSlides[position].googleAnalyticsScreenHitValue;
            } else {
                googleAnalyticsScreenHitValue = getResources().getString(R.string.whatsnew_ordered_layout_listing_ga_screen_value_if_not_found);
            }
            Analytics.hitScreen(googleAnalyticsScreenHitValue, this);
        }
    }

    public void onPageSelected(int position) {
    }

    public void onPageScrollStateChanged(int state) {
    }
}
