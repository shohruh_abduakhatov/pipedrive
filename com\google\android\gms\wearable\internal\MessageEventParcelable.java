package com.google.android.gms.wearable.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.wearable.MessageEvent;
import com.newrelic.agent.android.util.SafeJsonPrimitive;

public class MessageEventParcelable extends AbstractSafeParcelable implements MessageEvent {
    public static final Creator<MessageEventParcelable> CREATOR = new zzba();
    private final int IS;
    private final byte[] afk;
    private final String bQ;
    private final String mPath;
    final int mVersionCode;

    MessageEventParcelable(int i, int i2, String str, byte[] bArr, String str2) {
        this.mVersionCode = i;
        this.IS = i2;
        this.mPath = str;
        this.afk = bArr;
        this.bQ = str2;
    }

    public byte[] getData() {
        return this.afk;
    }

    public String getPath() {
        return this.mPath;
    }

    public int getRequestId() {
        return this.IS;
    }

    public String getSourceNodeId() {
        return this.bQ;
    }

    public String toString() {
        int i = this.IS;
        String str = this.mPath;
        String valueOf = String.valueOf(this.afk == null ? SafeJsonPrimitive.NULL_STRING : Integer.valueOf(this.afk.length));
        return new StringBuilder((String.valueOf(str).length() + 43) + String.valueOf(valueOf).length()).append("MessageEventParcelable[").append(i).append(Table.COMMA_SEP).append(str).append(", size=").append(valueOf).append("]").toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzba.zza(this, parcel, i);
    }
}
