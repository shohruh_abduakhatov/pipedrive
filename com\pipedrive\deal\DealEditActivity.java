package com.pipedrive.deal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.pipedrive.BaseActivity;
import com.pipedrive.R;
import com.pipedrive.dialogs.MessageDialog;
import com.pipedrive.linking.ProductLinkingActivity;
import com.pipedrive.model.Currency;
import com.pipedrive.model.Deal;
import com.pipedrive.model.DealStage;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.PersonFieldOption;
import com.pipedrive.model.Pipeline;
import com.pipedrive.model.User;
import com.pipedrive.navigator.Navigable;
import com.pipedrive.navigator.Navigator;
import com.pipedrive.navigator.Navigator.Type;
import com.pipedrive.navigator.buttons.DeleteMenuItem;
import com.pipedrive.navigator.buttons.DiscardMenuItem;
import com.pipedrive.navigator.buttons.NavigatorMenuItem;
import com.pipedrive.products.DealProductListActivity;
import com.pipedrive.util.StringUtils;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.util.formatter.MonetaryFormatter;
import com.pipedrive.util.snackbar.SnackBarManager;
import com.pipedrive.views.assignment.AssignedToView;
import com.pipedrive.views.assignment.SpinnerWithUserProfilePicture.OnItemSelectedListener;
import com.pipedrive.views.association.AssociationView.AssociationListener;
import com.pipedrive.views.association.OrganizationAssociationView;
import com.pipedrive.views.association.PersonAssociationView;
import com.pipedrive.views.common.CurrencySpinner;
import com.pipedrive.views.common.CurrencySpinner.OnCurrencySelectedListener;
import com.pipedrive.views.common.EditTextWithUnderlineAndLabel;
import com.pipedrive.views.common.VisibilitySpinner;
import com.pipedrive.views.common.VisibilitySpinner.OnVisibilityChangeListener;
import com.pipedrive.views.edit.deal.DealTitleEditText;
import com.pipedrive.views.edit.deal.DealValueEditText;
import com.pipedrive.views.edit.deal.DealValueEditText.OnValueChangedListener;
import com.pipedrive.views.edit.deal.PipelineSpinner;
import com.pipedrive.views.edit.deal.PipelineSpinner.OnPipelineSelectedListener;
import com.pipedrive.views.edit.deal.StageSpinner;
import com.pipedrive.views.edit.deal.StageSpinner.OnStageSelectedListener;
import java.util.Arrays;
import java.util.List;

public class DealEditActivity extends BaseActivity implements DealEditView, Navigable {
    public static final String ARG_DEAL_SQL_ID = "DEAL_SQL_ID";
    private static final String ARG_NEW_DEAL_TITLE = "NEW_DEAL_TITLE";
    private static final String ARG_ORG_SQL_ID = "ORG_SQL_ID";
    private static final String ARG_PERSON_SQL_ID = "PERSON_SQL_ID";
    private static final String ARG_STAGE_SQL_ID = "STAGE_SQL_ID";
    private static final int LINK_ORGANIZATION_REQUEST_CODE = 2;
    private static final int LINK_PERSON_REQUEST_CODE = 1;
    private static final int LINK_PRODUCT_REQUEST_CODE = 3;
    @BindView(2131820754)
    protected AssignedToView mAssignedToView;
    @BindView(2131820751)
    protected Button mAttachProductsButton;
    @BindView(2131820750)
    protected CurrencySpinner mCurrencySpinner;
    private final DeleteMenuItem mDeleteMenuItem = new DeleteMenuItem() {
        public boolean isEnabled() {
            return DealEditActivity.this.getSession().getUserSettingsCanDeleteDeal();
        }

        public void onClick() {
            DealEditActivity.this.onDelete();
        }
    };
    private final DiscardMenuItem mDiscardMenuItem = new DiscardMenuItem() {
        public void onClick() {
            DealEditActivity.this.onCancel();
        }
    };
    @BindView(2131820711)
    protected OrganizationAssociationView mOrganizationAssociationView;
    @BindView(2131820710)
    protected PersonAssociationView mPersonAssociationView;
    @BindView(2131820752)
    protected PipelineSpinner mPipelineSpinner;
    private DealEditPresenter mPresenter;
    @BindView(2131820747)
    protected TextView mProductCount;
    @BindView(2131820746)
    protected TextView mProductsValue;
    @BindView(2131820753)
    protected StageSpinner mStageSpinner;
    @BindView(2131820650)
    protected DealTitleEditText mTitle;
    @BindView(2131820700)
    protected Toolbar mToolbar;
    @BindView(2131820749)
    protected DealValueEditText mValue;
    @BindView(2131820748)
    protected View mValueContainerNoProducts;
    @BindView(2131820745)
    protected View mValueContainerWithProducts;
    @BindView(2131820755)
    protected VisibilitySpinner mVisibilitySpinner;

    public static void startActivityForEditingDeal(@NonNull Activity activity, @NonNull Deal deal) {
        if (deal.isStored()) {
            ContextCompat.startActivity(activity, new Intent(activity, DealEditActivity.class).putExtra("DEAL_SQL_ID", deal.getSqlIdOrNull()), null);
        }
    }

    public static void startActivityForAddingDealToStage(@NonNull Activity activity, @IntRange(from = 1) int stageId) {
        ContextCompat.startActivity(activity, new Intent(activity, DealEditActivity.class).putExtra(ARG_STAGE_SQL_ID, stageId), null);
    }

    public static void startActivityForAddingDealToPerson(@Nullable Activity activity, @Nullable Person person) {
        boolean cannotAddPersonToDeal = activity == null || person == null || !person.isStored();
        if (!cannotAddPersonToDeal) {
            ContextCompat.startActivity(activity, new Intent(activity, DealEditActivity.class).putExtra("PERSON_SQL_ID", person.getSqlIdOrNull()), null);
        }
    }

    public static void startActivityForAddingDealToOrganization(@Nullable Activity activity, @Nullable Organization organization) {
        boolean cannotCreateActivity = activity == null || organization == null || !organization.isStored();
        if (!cannotCreateActivity) {
            ContextCompat.startActivity(activity, new Intent(activity, DealEditActivity.class).putExtra("ORG_SQL_ID", organization.getSqlIdOrNull()), null);
        }
    }

    public static void startActivityForResult(@NonNull Activity activity, @Nullable String titleProvided, int requestCode, @Nullable Long personSqlId, @Nullable Long orgSqlId) {
        Intent intent = new Intent(activity, DealEditActivity.class);
        if (titleProvided != null) {
            intent.putExtra(ARG_NEW_DEAL_TITLE, titleProvided);
        }
        if (personSqlId != null) {
            intent.putExtra("PERSON_SQL_ID", personSqlId);
        }
        if (orgSqlId != null) {
            intent.putExtra("ORG_SQL_ID", orgSqlId);
        }
        ActivityCompat.startActivityForResult(activity, intent, requestCode, null);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_deal_edit);
        ButterKnife.bind((Activity) this);
        setSupportActionBar(this.mToolbar);
        this.mPresenter = new DealEditPresenterImpl(getSession(), savedInstanceState);
    }

    public void onResume() {
        super.onResume();
        this.mPresenter.bindView(this);
        Deal currentlyManagedDeal = this.mPresenter.getCurrentlyManagedDeal();
        if (currentlyManagedDeal != null) {
            onRequestDeal(currentlyManagedDeal);
            this.mPresenter.requestDealProductsUpdate();
        } else if (!requestDeal(getIntent().getExtras())) {
            finish();
        }
    }

    @Nullable
    protected Navigator getNavigatorImplementation() {
        return new Navigator(this, this.mToolbar);
    }

    protected void onPause() {
        this.mPresenter.unbindView();
        super.onPause();
    }

    private boolean requestDeal(@Nullable Bundle intentExtras) {
        Long organizationSqlId = null;
        if (intentExtras == null) {
            return false;
        }
        Long personSqlId;
        boolean newDealWithTitleRequested = intentExtras.containsKey(ARG_NEW_DEAL_TITLE);
        boolean changeOfDealRequested = intentExtras.containsKey("DEAL_SQL_ID");
        boolean newDealForStageRequested = intentExtras.containsKey(ARG_STAGE_SQL_ID);
        boolean newDealWithAssociatedOrganizationRequested = intentExtras.containsKey("ORG_SQL_ID");
        boolean newDealWithAssociatedPersonRequested = intentExtras.containsKey("PERSON_SQL_ID");
        if (newDealWithAssociatedPersonRequested) {
            personSqlId = Long.valueOf(intentExtras.getLong("PERSON_SQL_ID"));
        } else {
            personSqlId = null;
        }
        if (newDealWithAssociatedOrganizationRequested) {
            organizationSqlId = Long.valueOf(intentExtras.getLong("ORG_SQL_ID"));
        }
        if (changeOfDealRequested) {
            this.mPresenter.requestOrRestoreDeal(Long.valueOf(intentExtras.getLong("DEAL_SQL_ID")));
            return true;
        } else if (newDealWithTitleRequested) {
            this.mPresenter.requestOrRestoreNewDealWithTitle(intentExtras.getString(ARG_NEW_DEAL_TITLE), personSqlId, organizationSqlId);
            return true;
        } else if (newDealForStageRequested) {
            this.mPresenter.requestOrRestoreNewDealForStage(Integer.valueOf(intentExtras.getInt(ARG_STAGE_SQL_ID)));
            return true;
        } else if (newDealWithAssociatedOrganizationRequested) {
            this.mPresenter.requestOrRestoreNewDealForOrganization(organizationSqlId);
            return true;
        } else if (!newDealWithAssociatedPersonRequested) {
            return false;
        } else {
            this.mPresenter.requestOrRestoreNewDealForPerson(personSqlId);
            return true;
        }
    }

    private void setupVisibleToView(@NonNull final Deal deal) {
        this.mVisibilitySpinner.setup(getSession(), deal, new OnVisibilityChangeListener() {
            public void onVisibilityChanged(@NonNull PersonFieldOption personFieldOption) {
                deal.setVisibleTo(personFieldOption.getId());
            }
        });
    }

    private void setupAssignedToView(@NonNull final Deal deal) {
        this.mAssignedToView.loadAssignments(getSession(), deal, new OnItemSelectedListener() {
            public void onItemSelected(@NonNull User user) {
                deal.setOwnerPipedriveId(user.getPipedriveId());
                deal.setOwnerName(user.getName());
            }
        });
    }

    private void setupStageView(@NonNull final Deal deal) {
        this.mStageSpinner.setup(getSession(), deal, new OnStageSelectedListener() {
            public void onStageSelected(@NonNull DealStage dealStage) {
                deal.setStage(dealStage.getPipedriveId());
            }
        });
    }

    private void setupPipelineView(@NonNull Deal deal) {
        this.mPipelineSpinner.setup(getSession(), deal, new OnPipelineSelectedListener() {
            public void onPipelineSelected(@NonNull Pipeline pipeline) {
                DealEditActivity.this.mPresenter.setPipeline(pipeline);
            }
        });
    }

    private void setupCurrencyView(@NonNull final Deal deal) {
        this.mCurrencySpinner.setup(getSession(), deal.getCurrencyCode(), new OnCurrencySelectedListener() {
            public void onCurrencySelected(@NonNull Currency currency) {
                deal.setCurrencyCode(currency.getCode());
            }
        });
    }

    private void setupValueView(@NonNull final Deal deal) {
        this.mValue.setup(getSession(), deal, new OnValueChangedListener() {
            public void onValueChanged(double value) {
                deal.setValue(value);
            }
        });
    }

    public void onRequestDeal(@Nullable Deal deal) {
        if (deal == null) {
            finish();
            return;
        }
        setupActionBar(deal);
        setupAssociatePersonView(deal.getPerson());
        setupAssociatePersonViewActions();
        setupAssociateOrganizationView(deal.getOrganization());
        setupAssociateOrganizationViewActions();
        setupTitleView(deal);
        setupValueContainer(deal);
        setupCurrencyView(deal);
        setupPipelineView(deal);
        setupStageView(deal);
        setupAssignedToView(deal);
        setupVisibleToView(deal);
    }

    private void setupActionBar(@NonNull Deal deal) {
        boolean addNewDealIsRequested = !deal.isStored();
        setTitle(addNewDealIsRequested ? R.string.add_deal : R.string.edit_deal);
        setNavigatorType(addNewDealIsRequested ? Type.CREATE : Type.UPDATE);
    }

    private void setupTitleView(@NonNull final Deal deal) {
        this.mTitle.setup(deal, new EditTextWithUnderlineAndLabel.OnValueChangedListener() {
            public void onValueChanged(@NonNull String value) {
                deal.setTitle(value);
            }
        });
    }

    private void processDealCrudResponse(boolean success) {
        if (success) {
            finish();
        } else {
            ViewUtil.showErrorToast(this, R.string.error_deal_save_failed);
        }
    }

    public void onDealCreated(boolean success) {
        if (success) {
            if (this.mPresenter.getCurrentlyManagedDeal() != null) {
                setResult(-1, new Intent().putExtra("DEAL_SQL_ID", this.mPresenter.getCurrentlyManagedDeal().getSqlIdOrNull()));
            }
            SnackBarManager.INSTANCE.setDelayedSnackBar(R.string.deal_added);
        }
        processDealCrudResponse(success);
    }

    public void onDealUpdated(boolean success) {
        processDealCrudResponse(success);
        if (success) {
            SnackBarManager.INSTANCE.setDelayedSnackBar(R.string.deal_updated);
        }
    }

    public void onDealDeleted(boolean success) {
        processDealCrudResponse(success);
        if (success) {
            SnackBarManager.INSTANCE.setDelayedSnackBar(R.string.deal_deleted);
        }
    }

    public void onAssociationsUpdated(@NonNull Deal deal) {
        setupAssociatePersonView(deal.getPerson());
        setupAssociateOrganizationView(deal.getOrganization());
        setupTitleView(deal);
    }

    public void onPipelineUpdated(@NonNull Deal deal) {
        setupStageView(deal);
    }

    private void setupAssociatePersonView(@Nullable Person person) {
        this.mPersonAssociationView.setAssociation(person, Integer.valueOf(1));
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            switch (requestCode) {
                case 1:
                    long personSqlId = data.getLongExtra("PERSON_SQL_ID", 0);
                    if (personSqlId != 0) {
                        this.mPresenter.associatePersonBySqlId(Long.valueOf(personSqlId));
                        return;
                    }
                    return;
                case 2:
                    long orgSqlId = data.getLongExtra("ORG_SQL_ID", 0);
                    if (orgSqlId != 0) {
                        this.mPresenter.associateOrganizationBySqlId(Long.valueOf(orgSqlId));
                        return;
                    }
                    return;
                case 3:
                    if (this.mPresenter.getCurrentlyManagedDeal() != null && this.mPresenter.getCurrentlyManagedDeal().getSqlIdOrNull() != null) {
                        DealProductListActivity.startActivity(this, this.mPresenter.getCurrentlyManagedDeal().getSqlIdOrNull());
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    private void setupAssociatePersonViewActions() {
        this.mPersonAssociationView.setAssociationListener(new AssociationListener<Person>() {
            public void onAssociationChanged(@Nullable Person association) {
                DealEditActivity.this.mPresenter.associatePerson(association);
            }
        });
    }

    private void setupAssociateOrganizationView(@Nullable Organization organization) {
        this.mOrganizationAssociationView.setAssociation(organization, Integer.valueOf(2));
    }

    private void setupAssociateOrganizationViewActions() {
        this.mOrganizationAssociationView.setAssociationListener(new AssociationListener<Organization>() {
            public void onAssociationChanged(@Nullable Organization association) {
                DealEditActivity.this.mPresenter.associateOrganization(association);
            }
        });
    }

    private boolean setDealTitle() {
        boolean currentlyManagedDealIsPresent;
        Deal currentlyManagedDeal = this.mPresenter.getCurrentlyManagedDeal();
        if (currentlyManagedDeal == null) {
            currentlyManagedDealIsPresent = true;
        } else {
            currentlyManagedDealIsPresent = false;
        }
        if (currentlyManagedDealIsPresent) {
            return false;
        }
        boolean titleIsEntered;
        boolean titleCanBeGenerated;
        String enteredTitle = this.mTitle.getEnteredTitle();
        String generatedTitle = this.mTitle.getGeneratedTitle();
        if (StringUtils.isTrimmedAndEmpty(enteredTitle)) {
            titleIsEntered = false;
        } else {
            titleIsEntered = true;
        }
        if (StringUtils.isTrimmedAndEmpty(generatedTitle)) {
            titleCanBeGenerated = false;
        } else {
            titleCanBeGenerated = true;
        }
        String title = titleIsEntered ? enteredTitle : titleCanBeGenerated ? generatedTitle : null;
        currentlyManagedDeal.setTitle(title);
        return true;
    }

    public boolean isChangesMadeToFieldsAfterInitialLoad() {
        boolean dealTitleIsNotSet;
        boolean z = true;
        if (setDealTitle()) {
            dealTitleIsNotSet = false;
        } else {
            dealTitleIsNotSet = true;
        }
        if (dealTitleIsNotSet) {
            return false;
        }
        boolean currentlyManagedDealIsPresent;
        Deal currentlyManagedDeal = this.mPresenter.getCurrentlyManagedDeal();
        if (currentlyManagedDeal != null) {
            currentlyManagedDealIsPresent = true;
        } else {
            currentlyManagedDealIsPresent = false;
        }
        if (!(currentlyManagedDealIsPresent && this.mPresenter.wasDealChanged(currentlyManagedDeal))) {
            z = false;
        }
        return z;
    }

    public boolean isAllRequiredFieldsSet() {
        boolean dealTitleIsNotSet;
        if (setDealTitle()) {
            dealTitleIsNotSet = false;
        } else {
            dealTitleIsNotSet = true;
        }
        if (dealTitleIsNotSet) {
            return false;
        }
        boolean currentlyManagedDealIsNotPresent;
        Deal currentlyManagedDeal = this.mPresenter.getCurrentlyManagedDeal();
        if (currentlyManagedDeal == null) {
            currentlyManagedDealIsNotPresent = true;
        } else {
            currentlyManagedDealIsNotPresent = false;
        }
        if (currentlyManagedDealIsNotPresent) {
            return false;
        }
        boolean isPersonAssociated;
        if (currentlyManagedDeal.getPerson() != null) {
            isPersonAssociated = true;
        } else {
            isPersonAssociated = false;
        }
        boolean isOrganizationAssociated;
        if (currentlyManagedDeal.getOrganization() != null) {
            isOrganizationAssociated = true;
        } else {
            isOrganizationAssociated = false;
        }
        if (isPersonAssociated || isOrganizationAssociated) {
            return true;
        }
        return false;
    }

    public void onCreateOrUpdate() {
        Deal currentlyManagedDeal = this.mPresenter.getCurrentlyManagedDeal();
        if (!(currentlyManagedDeal == null)) {
            this.mPresenter.changeDeal(currentlyManagedDeal);
        }
    }

    public void createOrUpdateDiscardedAsRequiredFieldsAreNotSet() {
        MessageDialog.showMessage(getFragmentManager(), R.string.please_link_a_person_or_organization_to_your_deal);
    }

    private void onDelete() {
        final Deal currentlyManagedDeal = this.mPresenter.getCurrentlyManagedDeal();
        if ((currentlyManagedDeal != null) && currentlyManagedDeal.getSqlIdOrNull() != null) {
            ViewUtil.showDeleteConfirmationDialog(this, getResources().getString(R.string.dialog_delete_deal), new Runnable() {
                public void run() {
                    DealEditActivity.this.mPresenter.deleteDeal(currentlyManagedDeal.getSqlIdOrNull());
                }
            });
        }
    }

    public void onCancel() {
        finish();
    }

    @Nullable
    public List<? extends NavigatorMenuItem> getAdditionalButtons() {
        return Arrays.asList(new NavigatorMenuItem[]{this.mDeleteMenuItem, this.mDiscardMenuItem});
    }

    private void setupValueContainer(@NonNull Deal deal) {
        int i;
        boolean shouldShowProducts = true;
        int i2 = 8;
        Double productCount = deal.getProductCount();
        boolean productsEnabled = getSession().isCompanyFeatureProductsEnabled(false);
        boolean productsAttached;
        if (productCount == null || productCount.doubleValue() <= 0.0d) {
            productsAttached = false;
        } else {
            productsAttached = true;
        }
        if (!(productsEnabled && productsAttached)) {
            shouldShowProducts = false;
        }
        boolean dealIsSaved = deal.isStored();
        if (shouldShowProducts) {
            setUpProductCountAndValue(deal, productCount);
        } else {
            Button button = this.mAttachProductsButton;
            if (productsEnabled && dealIsSaved) {
                i = 0;
            } else {
                i = 8;
            }
            button.setVisibility(i);
            setupValueView(deal);
        }
        View view = this.mValueContainerWithProducts;
        if (shouldShowProducts) {
            i = 0;
        } else {
            i = 8;
        }
        view.setVisibility(i);
        View view2 = this.mValueContainerNoProducts;
        if (!shouldShowProducts) {
            i2 = 0;
        }
        view2.setVisibility(i2);
    }

    private void setUpProductCountAndValue(@NonNull Deal deal, Double productCount) {
        this.mProductsValue.setText(new MonetaryFormatter(getSession()).formatDealValue(deal));
        this.mProductCount.setText(getResources().getQuantityString(R.plurals.product, (int) Math.round(productCount.doubleValue()), new Object[]{Long.valueOf(Math.round(productCount.doubleValue()))}));
    }

    @OnClick({2131820751})
    protected void onAttachProductsClicked() {
        if (this.mPresenter.getCurrentlyManagedDeal() != null && this.mPresenter.getCurrentlyManagedDeal().getSqlIdOrNull() != null) {
            ProductLinkingActivity.startActivity(this, this.mPresenter.getCurrentlyManagedDeal().getSqlIdOrNull(), Integer.valueOf(3));
        }
    }

    @OnClick({2131820747})
    protected void onProductCountClicked() {
        Deal deal = this.mPresenter.getCurrentlyManagedDeal();
        if (deal != null && deal.getSqlIdOrNull() != null) {
            DealProductListActivity.startActivity(this, deal.getSqlIdOrNull());
        }
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.mPresenter.onSaveInstanceState(outState);
    }
}
