package com.pipedrive.nearby.toolbar;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.model.NearbyItemType;
import rx.Observable;

public interface NearbyItemTypeChangesProvider {
    @NonNull
    Observable<NearbyItemType> nearbyItemTypeChanges(@NonNull Session session);
}
