package com.pipedrive.model.email;

import android.support.annotation.Nullable;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Person;
import com.pipedrive.model.User;
import com.pipedrive.util.networking.entities.EmailParticipantEntity;

public class EmailParticipant extends BaseDatasourceEntity {
    private String mEmail;
    private String mName;
    private Person mPerson;
    private User mUser;

    @Nullable
    public static EmailParticipant from(@Nullable EmailParticipantEntity entity, @Nullable Person person, @Nullable User user) {
        EmailParticipant object = new EmailParticipant();
        if (entity != null) {
            object.setEmail(entity.getEmail());
            object.setName(entity.getName());
            object.setPerson(person);
            object.setUser(user);
        }
        return object;
    }

    public String getEmail() {
        return this.mEmail;
    }

    public void setEmail(String email) {
        this.mEmail = email;
    }

    public String getName() {
        return this.mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public Person getPerson() {
        return this.mPerson;
    }

    public void setPerson(Person person) {
        this.mPerson = person;
    }

    public User getUser() {
        return this.mUser;
    }

    public void setUser(User user) {
        this.mUser = user;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EmailParticipant that = (EmailParticipant) o;
        if (this.mEmail == null ? that.mEmail != null : !this.mEmail.equals(that.mEmail)) {
            if (this.mName == null) {
                if (that.mName == null) {
                }
            }
            if (this.mPerson == null) {
                if (that.mPerson == null) {
                }
            }
            if (this.mUser != null) {
                if (this.mUser.equals(that.mUser)) {
                    return true;
                }
            } else if (that.mUser == null) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        int result;
        int hashCode;
        int i = 0;
        if (this.mEmail != null) {
            result = this.mEmail.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.mName != null) {
            hashCode = this.mName.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mPerson != null) {
            hashCode = this.mPerson.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (i2 + hashCode) * 31;
        if (this.mUser != null) {
            i = this.mUser.hashCode();
        }
        return hashCode + i;
    }
}
