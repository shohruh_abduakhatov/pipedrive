package com.pipedrive.views;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import com.pipedrive.R;
import com.pipedrive.datasource.search.SearchConstraint;

public class CustomSearchView extends FrameLayout {
    private View mCloseButton;
    private OnConstraintChangeListener mOnConstraintChangeListener;
    EditText mSearchEditText;

    public interface OnConstraintChangeListener {
        void onConstraintChanged(@NonNull SearchConstraint searchConstraint);
    }

    public CustomSearchView(Context context) {
        this(context, null, 0);
    }

    public CustomSearchView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomSearchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @LayoutRes
    int getLayoutResId() {
        return R.layout.custom_search_view;
    }

    void init(Context context) {
        LayoutInflater.from(context).inflate(getLayoutResId(), this);
        if (!isInEditMode()) {
            this.mSearchEditText = (EditText) findViewById(R.id.search_edit);
            this.mSearchEditText.addTextChangedListener(new TextWatcher() {
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(@NonNull CharSequence s, int start, int before, int count) {
                }

                public void afterTextChanged(Editable s) {
                    String newString = s.toString();
                    CustomSearchView.this.mCloseButton.setVisibility(TextUtils.isEmpty(newString) ? 8 : 0);
                    if (CustomSearchView.this.mOnConstraintChangeListener != null) {
                        CustomSearchView.this.mOnConstraintChangeListener.onConstraintChanged(SearchConstraint.fromString(newString));
                    }
                }
            });
            this.mCloseButton = findViewById(R.id.search_close_btn);
            this.mCloseButton.setOnClickListener(new OnClickListener() {
                public void onClick(@NonNull View v) {
                    CustomSearchView.this.mSearchEditText.setText("");
                }
            });
        }
    }

    public void setOnConstraintChangeListener(OnConstraintChangeListener listener) {
        this.mOnConstraintChangeListener = listener;
    }
}
