package com.zendesk.sdk.model.request;

import android.support.annotation.Nullable;

public class RequestResponse {
    private Request request;

    @Nullable
    public Request getRequest() {
        return this.request;
    }
}
