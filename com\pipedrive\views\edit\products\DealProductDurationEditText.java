package com.pipedrive.views.edit.products;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import com.pipedrive.R;
import com.pipedrive.model.products.DealProduct;

public class DealProductDurationEditText extends DealProductNumericEditTextWithFormatting {
    public DealProductDurationEditText(Context context) {
        super(context);
    }

    public DealProductDurationEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DealProductDurationEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected int getLabelTextResourceId() {
        return R.string.duration;
    }

    double getInitialValue(@NonNull DealProduct dealProduct) {
        return dealProduct.getDuration().doubleValue();
    }
}
