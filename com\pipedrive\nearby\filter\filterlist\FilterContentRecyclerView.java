package com.pipedrive.nearby.filter.filterlist;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import com.pipedrive.R;
import com.pipedrive.views.DividerItemDecoration;

public class FilterContentRecyclerView extends RecyclerView {
    public FilterContentRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setLayoutManager(new LinearLayoutManager(context, 1, false));
        addItemDecoration(new DividerItemDecoration(context, R.drawable.divider_1_dp));
    }
}
