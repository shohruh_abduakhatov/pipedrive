package com.zendesk.sdk.model.push;

import com.google.gson.annotations.SerializedName;

public class PushRegistrationRequestWrapper {
    @SerializedName("push_notification_device")
    private PushRegistrationRequest pushRegistrationRequest;

    public void setPushRegistrationRequest(PushRegistrationRequest pushRegistrationRequest) {
        this.pushRegistrationRequest = pushRegistrationRequest;
    }
}
