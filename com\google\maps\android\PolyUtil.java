package com.google.maps.android;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.heatmaps.WeightedLatLng;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class PolyUtil {
    private static final double DEFAULT_TOLERANCE = 0.1d;

    private PolyUtil() {
    }

    private static double tanLatGC(double lat1, double lat2, double lng2, double lng3) {
        return ((Math.tan(lat1) * Math.sin(lng2 - lng3)) + (Math.tan(lat2) * Math.sin(lng3))) / Math.sin(lng2);
    }

    private static double mercatorLatRhumb(double lat1, double lat2, double lng2, double lng3) {
        return ((MathUtil.mercator(lat1) * (lng2 - lng3)) + (MathUtil.mercator(lat2) * lng3)) / lng2;
    }

    private static boolean intersects(double lat1, double lat2, double lng2, double lat3, double lng3, boolean geodesic) {
        if ((lng3 >= 0.0d && lng3 >= lng2) || (lng3 < 0.0d && lng3 < lng2)) {
            return false;
        }
        if (lat3 <= -1.5707963267948966d) {
            return false;
        }
        if (lat1 <= -1.5707963267948966d || lat2 <= -1.5707963267948966d || lat1 >= 1.5707963267948966d || lat2 >= 1.5707963267948966d) {
            return false;
        }
        if (lng2 <= -3.141592653589793d) {
            return false;
        }
        double linearLat = (((lng2 - lng3) * lat1) + (lat2 * lng3)) / lng2;
        if (lat1 >= 0.0d && lat2 >= 0.0d && lat3 < linearLat) {
            return false;
        }
        if (lat1 <= 0.0d && lat2 <= 0.0d && lat3 >= linearLat) {
            return true;
        }
        if (lat3 >= 1.5707963267948966d) {
            return true;
        }
        return geodesic ? Math.tan(lat3) >= tanLatGC(lat1, lat2, lng2, lng3) : MathUtil.mercator(lat3) >= mercatorLatRhumb(lat1, lat2, lng2, lng3);
    }

    public static boolean containsLocation(LatLng point, List<LatLng> polygon, boolean geodesic) {
        return containsLocation(point.latitude, point.longitude, polygon, geodesic);
    }

    public static boolean containsLocation(double latitude, double longitude, List<LatLng> polygon, boolean geodesic) {
        int size = polygon.size();
        if (size == 0) {
            return false;
        }
        double lat3 = Math.toRadians(latitude);
        double lng3 = Math.toRadians(longitude);
        LatLng prev = (LatLng) polygon.get(size - 1);
        double lat1 = Math.toRadians(prev.latitude);
        double lng1 = Math.toRadians(prev.longitude);
        int nIntersect = 0;
        for (LatLng point2 : polygon) {
            double dLng3 = MathUtil.wrap(lng3 - lng1, -3.141592653589793d, 3.141592653589793d);
            if (lat3 == lat1 && dLng3 == 0.0d) {
                return true;
            }
            double lat2 = Math.toRadians(point2.latitude);
            double lng2 = Math.toRadians(point2.longitude);
            if (intersects(lat1, lat2, MathUtil.wrap(lng2 - lng1, -3.141592653589793d, 3.141592653589793d), lat3, dLng3, geodesic)) {
                nIntersect++;
            }
            lat1 = lat2;
            lng1 = lng2;
        }
        return (nIntersect & 1) != 0;
    }

    public static boolean isLocationOnEdge(LatLng point, List<LatLng> polygon, boolean geodesic, double tolerance) {
        return isLocationOnEdgeOrPath(point, polygon, true, geodesic, tolerance);
    }

    public static boolean isLocationOnEdge(LatLng point, List<LatLng> polygon, boolean geodesic) {
        return isLocationOnEdge(point, polygon, geodesic, DEFAULT_TOLERANCE);
    }

    public static boolean isLocationOnPath(LatLng point, List<LatLng> polyline, boolean geodesic, double tolerance) {
        return isLocationOnEdgeOrPath(point, polyline, false, geodesic, tolerance);
    }

    public static boolean isLocationOnPath(LatLng point, List<LatLng> polyline, boolean geodesic) {
        return isLocationOnPath(point, polyline, geodesic, DEFAULT_TOLERANCE);
    }

    private static boolean isLocationOnEdgeOrPath(LatLng point, List<LatLng> poly, boolean closed, boolean geodesic, double toleranceEarth) {
        int size = poly.size();
        if (size == 0) {
            return false;
        }
        double tolerance = toleranceEarth / 6371009.0d;
        double havTolerance = MathUtil.hav(tolerance);
        double lat3 = Math.toRadians(point.latitude);
        double lng3 = Math.toRadians(point.longitude);
        LatLng prev = (LatLng) poly.get(closed ? size - 1 : 0);
        double lat1 = Math.toRadians(prev.latitude);
        double lng1 = Math.toRadians(prev.longitude);
        double lat2;
        double lng2;
        if (geodesic) {
            for (LatLng point2 : poly) {
                lat2 = Math.toRadians(point2.latitude);
                lng2 = Math.toRadians(point2.longitude);
                if (isOnSegmentGC(lat1, lng1, lat2, lng2, lat3, lng3, havTolerance)) {
                    return true;
                }
                lat1 = lat2;
                lng1 = lng2;
            }
        } else {
            double minAcceptable = lat3 - tolerance;
            double maxAcceptable = lat3 + tolerance;
            double y1 = MathUtil.mercator(lat1);
            double y3 = MathUtil.mercator(lat3);
            double[] xTry = new double[3];
            for (LatLng point22 : poly) {
                lat2 = Math.toRadians(point22.latitude);
                double y2 = MathUtil.mercator(lat2);
                lng2 = Math.toRadians(point22.longitude);
                if (Math.max(lat1, lat2) >= minAcceptable && Math.min(lat1, lat2) <= maxAcceptable) {
                    double x2 = MathUtil.wrap(lng2 - lng1, -3.141592653589793d, 3.141592653589793d);
                    double x3Base = MathUtil.wrap(lng3 - lng1, -3.141592653589793d, 3.141592653589793d);
                    xTry[0] = x3Base;
                    xTry[1] = 6.283185307179586d + x3Base;
                    xTry[2] = x3Base - 6.283185307179586d;
                    for (double x3 : xTry) {
                        double dy = y2 - y1;
                        double len2 = (x2 * x2) + (dy * dy);
                        double t = len2 <= 0.0d ? 0.0d : MathUtil.clamp(((x3 * x2) + ((y3 - y1) * dy)) / len2, 0.0d, WeightedLatLng.DEFAULT_INTENSITY);
                        if (MathUtil.havDistance(lat3, MathUtil.inverseMercator(y1 + (t * dy)), x3 - (t * x2)) < havTolerance) {
                            return true;
                        }
                    }
                    continue;
                }
                lat1 = lat2;
                lng1 = lng2;
                y1 = y2;
            }
        }
        return false;
    }

    private static double sinDeltaBearing(double lat1, double lng1, double lat2, double lng2, double lat3, double lng3) {
        double sinLat1 = Math.sin(lat1);
        double cosLat2 = Math.cos(lat2);
        double cosLat3 = Math.cos(lat3);
        double lng31 = lng3 - lng1;
        double lng21 = lng2 - lng1;
        double a = Math.sin(lng31) * cosLat3;
        double c = Math.sin(lng21) * cosLat2;
        double b = Math.sin(lat3 - lat1) + (((2.0d * sinLat1) * cosLat3) * MathUtil.hav(lng31));
        double d = Math.sin(lat2 - lat1) + (((2.0d * sinLat1) * cosLat2) * MathUtil.hav(lng21));
        double denom = ((a * a) + (b * b)) * ((c * c) + (d * d));
        return denom <= 0.0d ? WeightedLatLng.DEFAULT_INTENSITY : ((a * d) - (b * c)) / Math.sqrt(denom);
    }

    private static boolean isOnSegmentGC(double lat1, double lng1, double lat2, double lng2, double lat3, double lng3, double havTolerance) {
        double havDist13 = MathUtil.havDistance(lat1, lat3, lng1 - lng3);
        if (havDist13 <= havTolerance) {
            return true;
        }
        double havDist23 = MathUtil.havDistance(lat2, lat3, lng2 - lng3);
        if (havDist23 <= havTolerance) {
            return true;
        }
        double havCrossTrack = MathUtil.havFromSin(MathUtil.sinFromHav(havDist13) * sinDeltaBearing(lat1, lng1, lat2, lng2, lat3, lng3));
        if (havCrossTrack > havTolerance) {
            return false;
        }
        double havDist12 = MathUtil.havDistance(lat1, lat2, lng1 - lng2);
        double term = havDist12 + ((WeightedLatLng.DEFAULT_INTENSITY - (2.0d * havDist12)) * havCrossTrack);
        if (havDist13 > term || havDist23 > term) {
            return false;
        }
        if (havDist12 < 0.74d) {
            return true;
        }
        double cosCrossTrack = WeightedLatLng.DEFAULT_INTENSITY - (2.0d * havCrossTrack);
        return MathUtil.sinSumFromHav((havDist13 - havCrossTrack) / cosCrossTrack, (havDist23 - havCrossTrack) / cosCrossTrack) > 0.0d;
    }

    public static List<LatLng> simplify(List<LatLng> poly, double tolerance) {
        int n = poly.size();
        if (n < 1) {
            throw new IllegalArgumentException("Polyline must have at least 1 point");
        } else if (tolerance <= 0.0d) {
            throw new IllegalArgumentException("Tolerance must be greater than zero");
        } else {
            int idx;
            boolean closedPolygon = isClosedPolygon(poly);
            LatLng lastPoint = null;
            if (closedPolygon) {
                lastPoint = (LatLng) poly.get(poly.size() - 1);
                poly.remove(poly.size() - 1);
                poly.add(new LatLng(lastPoint.latitude + 1.0E-11d, lastPoint.longitude + 1.0E-11d));
            }
            int maxIdx = 0;
            Stack<int[]> stack = new Stack();
            double[] dists = new double[n];
            dists[0] = WeightedLatLng.DEFAULT_INTENSITY;
            dists[n - 1] = WeightedLatLng.DEFAULT_INTENSITY;
            if (n > 2) {
                stack.push(new int[]{0, n - 1});
                while (stack.size() > 0) {
                    int[] current = (int[]) stack.pop();
                    double maxDist = 0.0d;
                    for (idx = current[0] + 1; idx < current[1]; idx++) {
                        double dist = distanceToLine((LatLng) poly.get(idx), (LatLng) poly.get(current[0]), (LatLng) poly.get(current[1]));
                        if (dist > maxDist) {
                            maxDist = dist;
                            maxIdx = idx;
                        }
                    }
                    if (maxDist > tolerance) {
                        dists[maxIdx] = maxDist;
                        stack.push(new int[]{current[0], maxIdx});
                        stack.push(new int[]{maxIdx, current[1]});
                    }
                }
            }
            if (closedPolygon) {
                poly.remove(poly.size() - 1);
                poly.add(lastPoint);
            }
            idx = 0;
            ArrayList<LatLng> simplifiedLine = new ArrayList();
            for (LatLng l : poly) {
                if (dists[idx] != 0.0d) {
                    simplifiedLine.add(l);
                }
                idx++;
            }
            return simplifiedLine;
        }
    }

    public static boolean isClosedPolygon(List<LatLng> poly) {
        if (((LatLng) poly.get(0)).equals((LatLng) poly.get(poly.size() - 1))) {
            return true;
        }
        return false;
    }

    public static double distanceToLine(LatLng p, LatLng start, LatLng end) {
        if (start.equals(end)) {
            return SphericalUtil.computeDistanceBetween(end, p);
        }
        double s0lat = Math.toRadians(p.latitude);
        double s0lng = Math.toRadians(p.longitude);
        double s1lat = Math.toRadians(start.latitude);
        double s1lng = Math.toRadians(start.longitude);
        double s2s1lat = Math.toRadians(end.latitude) - s1lat;
        double s2s1lng = Math.toRadians(end.longitude) - s1lng;
        double u = (((s0lat - s1lat) * s2s1lat) + ((s0lng - s1lng) * s2s1lng)) / ((s2s1lat * s2s1lat) + (s2s1lng * s2s1lng));
        if (u <= 0.0d) {
            return SphericalUtil.computeDistanceBetween(p, start);
        }
        if (u >= WeightedLatLng.DEFAULT_INTENSITY) {
            return SphericalUtil.computeDistanceBetween(p, end);
        }
        return SphericalUtil.computeDistanceBetween(new LatLng(p.latitude - start.latitude, p.longitude - start.longitude), new LatLng((end.latitude - start.latitude) * u, (end.longitude - start.longitude) * u));
    }

    public static List<LatLng> decode(String encodedPath) {
        int len = encodedPath.length();
        List<LatLng> path = new ArrayList();
        int index = 0;
        int lat = 0;
        int lng = 0;
        while (index < len) {
            int index2;
            int result = 1;
            int shift = 0;
            while (true) {
                index2 = index + 1;
                int b = (encodedPath.charAt(index) - 63) - 1;
                result += b << shift;
                shift += 5;
                if (b < 31) {
                    break;
                }
                index = index2;
            }
            lat += (result & 1) != 0 ? (result >> 1) ^ -1 : result >> 1;
            result = 1;
            shift = 0;
            index = index2;
            while (true) {
                index2 = index + 1;
                b = (encodedPath.charAt(index) - 63) - 1;
                result += b << shift;
                shift += 5;
                if (b < 31) {
                    break;
                }
                index = index2;
            }
            lng += (result & 1) != 0 ? (result >> 1) ^ -1 : result >> 1;
            path.add(new LatLng(((double) lat) * 1.0E-5d, ((double) lng) * 1.0E-5d));
            index = index2;
        }
        return path;
    }

    public static String encode(List<LatLng> path) {
        long lastLat = 0;
        long lastLng = 0;
        StringBuffer result = new StringBuffer();
        for (LatLng point : path) {
            long lat = Math.round(point.latitude * 100000.0d);
            long lng = Math.round(point.longitude * 100000.0d);
            long dLng = lng - lastLng;
            encode(lat - lastLat, result);
            encode(dLng, result);
            lastLat = lat;
            lastLng = lng;
        }
        return result.toString();
    }

    private static void encode(long v, StringBuffer result) {
        v = v < 0 ? (v << 1) ^ -1 : v << 1;
        while (v >= 32) {
            result.append(Character.toChars((int) (((31 & v) | 32) + 63)));
            v >>= 5;
        }
        result.append(Character.toChars((int) (v + 63)));
    }
}
