package com.pipedrive.util.persons;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import com.google.android.gms.plus.PlusShare;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.newrelic.agent.android.instrumentation.JSONArrayInstrumentation;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.PersonFieldOption;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.tasks.persons.DownloadPersonsTask;
import com.pipedrive.util.ContactsUtil;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.organizations.OrganizationsNetworkingUtil;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public enum PersonNetworkingUtil {
    ;

    private static String readDevices(JsonReader reader) throws IOException, JSONException {
        reader.beginArray();
        JSONArray deviceArray = new JSONArray();
        while (reader.hasNext()) {
            reader.beginObject();
            JSONObject dataObj = new JSONObject();
            while (reader.hasNext()) {
                String contactField = reader.nextName();
                if (Param.VALUE.equals(contactField)) {
                    dataObj.put(Param.VALUE, reader.nextString());
                } else if ("primary".equals(contactField)) {
                    dataObj.put("primary", reader.nextBoolean());
                } else if (PlusShare.KEY_CALL_TO_ACTION_LABEL.equals(contactField)) {
                    dataObj.put(PlusShare.KEY_CALL_TO_ACTION_LABEL, reader.nextString());
                } else {
                    reader.skipValue();
                }
            }
            deviceArray.put(dataObj);
            reader.endObject();
        }
        reader.endArray();
        return !(deviceArray instanceof JSONArray) ? deviceArray.toString() : JSONArrayInstrumentation.toString(deviceArray);
    }

    public static Person readContact(JsonReader reader) throws IOException {
        return readContact(reader, 0, null);
    }

    public static Person readContact(JsonReader reader, int itemPipedriveId, String name) throws IOException {
        Person personToReturn = new Person();
        personToReturn.setPipedriveId(itemPipedriveId);
        personToReturn.setName(name);
        updateContact(reader, personToReturn);
        return personToReturn;
    }

    public static void updateContact(JsonReader reader, Person readInto) throws IOException {
        if (readInto != null && reader != null) {
            Person person = readInto;
            reader.beginObject();
            while (reader.hasNext()) {
                String contactField = reader.nextName();
                if (PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID.equals(contactField)) {
                    person.setPipedriveId(reader.nextInt());
                } else if ("name".equals(contactField) && reader.peek() != JsonToken.NULL) {
                    person.setName(reader.nextString());
                } else if ("phone".equals(contactField) && reader.peek() == JsonToken.BEGIN_ARRAY) {
                    try {
                        person.setPhones(readDevices(reader));
                    } catch (JSONException e) {
                        Log.e(new Throwable("Error reading phones for person.", e));
                    }
                } else if ("email".equals(contactField) && reader.peek() == JsonToken.BEGIN_ARRAY) {
                    try {
                        person.setEmails(readDevices(reader));
                    } catch (JSONException e2) {
                        Log.e(new Throwable("Error reading emails for person.", e2));
                    }
                } else if ("org_name".equals(contactField) && reader.peek() != JsonToken.NULL) {
                    org = new Organization();
                    if (person.getCompany() != null) {
                        org = person.getCompany();
                    }
                    org.setName(reader.nextString());
                    person.setCompany(org);
                } else if ("org_id".equals(contactField)) {
                    org = new Organization();
                    if (reader.peek() == JsonToken.BEGIN_OBJECT) {
                        reader.beginObject();
                        while (reader.hasNext()) {
                            contactField = reader.nextName();
                            if (Param.VALUE.equals(contactField)) {
                                org.setPipedriveId(reader.nextInt());
                            } else if ("name".equals(contactField)) {
                                org.setName(reader.nextString());
                            } else {
                                reader.skipValue();
                            }
                        }
                        reader.endObject();
                    } else if (reader.peek() == JsonToken.NUMBER) {
                        if (person.getCompany() != null) {
                            org = person.getCompany();
                        }
                        org.setPipedriveId(reader.nextInt());
                    } else {
                        reader.skipValue();
                    }
                    if (org.isExisting()) {
                        person.setCompany(org);
                    }
                } else if ("update_time".equals(contactField) && reader.peek() != JsonToken.NULL) {
                    person.setUpdateTime(reader.nextString());
                } else if ("add_time".equals(contactField) && reader.peek() != JsonToken.NULL) {
                    person.setAddTime(reader.nextString());
                } else if ("active_flag".equals(contactField) && reader.peek() == JsonToken.BOOLEAN) {
                    person.setIsActive(reader.nextBoolean());
                } else if (PipeSQLiteHelper.COLUMN_VISIBLE_TO.equals(contactField) && reader.peek() == JsonToken.STRING) {
                    try {
                        person.setVisibleTo(Integer.parseInt(reader.nextString()));
                    } catch (NumberFormatException e3) {
                    }
                } else if (PipeSQLiteHelper.COLUMN_OWNER_ID.equals(contactField)) {
                    if (reader.peek() == JsonToken.BEGIN_OBJECT) {
                        reader.beginObject();
                        while (reader.hasNext()) {
                            if (PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID.equals(reader.nextName())) {
                                person.setOwnerPipedriveId(reader.nextInt());
                            } else {
                                reader.skipValue();
                            }
                        }
                        reader.endObject();
                    } else if (reader.peek() == JsonToken.NUMBER) {
                        person.setOwnerPipedriveId(reader.nextInt());
                    } else {
                        reader.skipValue();
                    }
                } else if ("owner_name".equals(contactField) && reader.peek() == JsonToken.STRING) {
                    person.setOwnerName(reader.nextString());
                } else if ("im".equals(contactField) && reader.peek() == JsonToken.BEGIN_ARRAY) {
                    try {
                        person.setIms(readDevices(reader));
                    } catch (JSONException e22) {
                        Log.e(e22);
                    }
                } else if (!"postal_address".equals(contactField) || reader.peek() == JsonToken.NULL) {
                    if (contactField.length() >= 40) {
                        JSONObject customFieldObj = null;
                        if (reader.peek() == JsonToken.NULL) {
                            reader.skipValue();
                        } else {
                            try {
                                customFieldObj = CustomField.readCustomValuesJsonObject(reader, contactField);
                            } catch (Throwable e4) {
                                LogJourno.reportEvent(EVENT.JsonParser_personCustomFieldsNotParsedFromJsonReader, e4);
                                Log.e(new Throwable("Error parsing custom field for person: " + person.getPipedriveId(), e4));
                            }
                        }
                        if (customFieldObj != null) {
                            person.addToCustomFieldArray(customFieldObj);
                        }
                    } else if ("cc_email".equals(contactField) && reader.peek() != JsonToken.NULL) {
                        person.setDropBoxAddress(reader.nextString());
                    } else if ("picture_id".equals(contactField) && reader.peek() == JsonToken.NUMBER) {
                        person.setPictureId(Integer.valueOf(reader.nextInt()));
                    } else {
                        reader.skipValue();
                    }
                } else if (reader.peek() == JsonToken.STRING) {
                    person.setPostalAddress(reader.nextString());
                } else {
                    Log.e(new Throwable("`postal_address` type is: " + reader.peek().toString() + " expected: STRING"));
                }
            }
            reader.endObject();
        }
    }

    @Nullable
    @Deprecated
    private static String findSmallestProfilePictureFromPictureJsonObject(@NonNull JsonReader reader) throws IOException {
        boolean isJsonArrayHoldingPictures;
        String profilePictureUrl = null;
        int profilePictureWidth = Integer.MAX_VALUE;
        if (reader.peek() == JsonToken.BEGIN_ARRAY) {
            isJsonArrayHoldingPictures = true;
        } else {
            isJsonArrayHoldingPictures = false;
        }
        if (isJsonArrayHoldingPictures) {
            reader.beginArray();
            while (reader.hasNext()) {
                if (reader.peek() == JsonToken.BEGIN_OBJECT) {
                    boolean isProfilePictureSmallerComparedToTheCurrentlyRegisteredOne;
                    reader.beginObject();
                    String currentProfilePictureUrl = null;
                    int currentProfilePictureWidth = Integer.MAX_VALUE;
                    while (reader.hasNext()) {
                        String paramName = reader.nextName();
                        if (TextUtils.equals("url", paramName)) {
                            currentProfilePictureUrl = reader.nextString();
                        } else if (TextUtils.equals(SettingsJsonConstants.ICON_WIDTH_KEY, paramName) && reader.peek() == JsonToken.NUMBER) {
                            currentProfilePictureWidth = reader.nextInt();
                        } else {
                            reader.skipValue();
                        }
                    }
                    reader.endObject();
                    if (currentProfilePictureUrl == null || currentProfilePictureWidth >= profilePictureWidth) {
                        isProfilePictureSmallerComparedToTheCurrentlyRegisteredOne = false;
                    } else {
                        isProfilePictureSmallerComparedToTheCurrentlyRegisteredOne = true;
                    }
                    if (isProfilePictureSmallerComparedToTheCurrentlyRegisteredOne) {
                        profilePictureUrl = currentProfilePictureUrl;
                        profilePictureWidth = currentProfilePictureWidth;
                    }
                }
            }
            reader.endArray();
        } else {
            reader.skipValue();
        }
        return profilePictureUrl;
    }

    @Nullable
    @Deprecated
    public static Person downloadPerson(@NonNull Session session, int personPipedriveId) {
        return downloadPerson(session, Long.valueOf((long) personPipedriveId).longValue());
    }

    @Nullable
    @WorkerThread
    public static Person downloadPerson(@NonNull Session session, long personPipedriveId) {
        boolean cannotDownloadSpecificPerson = personPipedriveId < 0 || ConnectionUtil.isNotConnected(session.getApplicationContext());
        if (cannotDownloadSpecificPerson) {
            return null;
        }
        ApiUrlBuilder uriBuilder = new ApiUrlBuilder(session, "persons");
        uriBuilder.appendEncodedPath(Long.toString(personPipedriveId));
        return ((1PersonDetailsRequest) ConnectionUtil.requestGet(uriBuilder, new 1(), new 1PersonDetailsRequest())).contact;
    }

    public static void downloadPersons(Session session) {
        ContactsUtil.downloadPersons(session);
    }

    @NonNull
    public static List<PersonFieldOption> getContactVisibilityValues(@NonNull Session session) {
        String visibilityFieldsJson = session.getContactsVisibilityFields();
        if (!visibilityFieldsJson.equalsIgnoreCase(Session.PREFS_NOT_SET_STRING)) {
            try {
                return DownloadPersonsTask.readPersonField(new JsonReader(new InputStreamReader(new ByteArrayInputStream(visibilityFieldsJson.getBytes(HttpRequest.CHARSET_UTF8)), HttpRequest.CHARSET_UTF8))).getOptions();
            } catch (Exception e) {
                Log.e(new Throwable("Error parsing contact visibility values", e));
            }
        }
        return new ArrayList();
    }

    @WorkerThread
    public static Person createOrUpdatePersonIntoDBWithRelations(@NonNull Session session, @NonNull Person person) {
        Organization personCompany = person.getCompany();
        if (personCompany != null && personCompany.isExisting()) {
            Organization personOrganizationInDB = new OrganizationsDataSource(session.getDatabase()).findOrganizationByPipedriveId(personCompany.getPipedriveId());
            if (personOrganizationInDB != null) {
                person.setCompany(personOrganizationInDB);
            } else {
                Organization personsOrgFromAPI = OrganizationsNetworkingUtil.downloadOrganization(session, personCompany.getPipedriveId());
                if (personsOrgFromAPI != null) {
                    person.setCompany(OrganizationsNetworkingUtil.createOrUpdateOrganizationIntoDBWithRelations(session, personsOrgFromAPI));
                }
            }
        }
        long personSqlId = new PersonsDataSource(session.getDatabase()).createOrUpdate((BaseDatasourceEntity) person);
        if (personSqlId == -1) {
            return null;
        }
        person.setSqlId(personSqlId);
        return person;
    }
}
