package com.pipedrive.nearby.cards.cards;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.ViewGroup;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.cards.adapter.CardAdapterForAggregatedItemsList;
import com.pipedrive.nearby.cards.adapter.PersonCardAdapterForAggregatedItemsList;
import com.pipedrive.nearby.model.NearbyItem;
import com.pipedrive.nearby.model.PersonNearbyItem;
import java.util.List;
import rx.Observable;
import rx.functions.Func1;

public class AggregatedPersonsCard extends AggregatedCard<PersonNearbyItem> {
    public AggregatedPersonsCard(@NonNull ViewGroup parent, @LayoutRes @NonNull Integer layoutResId) {
        super(parent, layoutResId);
    }

    Observable<List<PersonNearbyItem>> getSingleItemsList(@NonNull final PersonNearbyItem nearbyItem) {
        return nearbyItem.getItemSqlIds().map(new Func1<Long, PersonNearbyItem>() {
            public PersonNearbyItem call(Long aLong) {
                return new PersonNearbyItem(nearbyItem.getLatitude(), nearbyItem.getLongitude(), nearbyItem.getAddress(), aLong);
            }
        }).toList();
    }

    @NonNull
    Observable<String> getAggregatedInformation(@NonNull Session session, @NonNull PersonNearbyItem nearbyItem) {
        return nearbyItem.getOpenDealCount(session).map(new Func1<Integer, String>() {
            public String call(Integer openDealCount) {
                return AggregatedPersonsCard.this.getResources().getQuantityString(R.plurals.cards_open_deals, openDealCount.intValue(), new Object[]{openDealCount});
            }
        });
    }

    @NonNull
    Integer getQuantityStringResId() {
        return Integer.valueOf(R.plurals.persons);
    }

    @NonNull
    CardAdapterForAggregatedItemsList getAdapter(@NonNull List<? extends NearbyItem> nearbyItems) {
        return new PersonCardAdapterForAggregatedItemsList(this.session, nearbyItems);
    }
}
