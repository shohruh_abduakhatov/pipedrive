package com.pipedrive.pipeline;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;
import com.pipedrive.views.CustomSwipeRefreshLayout;

public class PipelineDealListFragment_ViewBinding implements Unbinder {
    private PipelineDealListFragment target;
    private View view2131820825;

    @UiThread
    public PipelineDealListFragment_ViewBinding(final PipelineDealListFragment target, View source) {
        this.target = target;
        target.mSwipeRefreshLayout = (CustomSwipeRefreshLayout) Utils.findRequiredViewAsType(source, R.id.swipe_refresh_layout, "field 'mSwipeRefreshLayout'", CustomSwipeRefreshLayout.class);
        target.mHeaderView = (PipelineDealListHeaderView) Utils.findRequiredViewAsType(source, R.id.header, "field 'mHeaderView'", PipelineDealListHeaderView.class);
        View view = Utils.findRequiredView(source, R.id.list, "field 'mListView' and method 'onDealClicked'");
        target.mListView = (ListView) Utils.castView(view, R.id.list, "field 'mListView'", ListView.class);
        this.view2131820825 = view;
        ((AdapterView) view).setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> p0, View p1, int p2, long p3) {
                target.onDealClicked(p0, p1, p2, p3);
            }
        });
        target.mEmpty = (TextView) Utils.findRequiredViewAsType(source, R.id.empty, "field 'mEmpty'", TextView.class);
    }

    @CallSuper
    public void unbind() {
        PipelineDealListFragment target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mSwipeRefreshLayout = null;
        target.mHeaderView = null;
        target.mListView = null;
        target.mEmpty = null;
        ((AdapterView) this.view2131820825).setOnItemClickListener(null);
        this.view2131820825 = null;
    }
}
