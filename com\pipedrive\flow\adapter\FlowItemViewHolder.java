package com.pipedrive.flow.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import butterknife.ButterKnife;
import com.pipedrive.application.Session;

public abstract class FlowItemViewHolder<ENTITY> extends ViewHolder {
    public abstract void fill(@NonNull Session session, @NonNull Context context, @NonNull ENTITY entity);

    public FlowItemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind((Object) this, itemView);
    }
}
