package com.pipedrive.dialogs.login;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import com.pipedrive.R;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.AnalyticsEvent;
import com.pipedrive.chrometab.BaseChromeTabActivity;

public class GoogleLoginFailedDialogFragment extends LoginFailedDialogFragment {
    private static final String TAG = "GoogleLoginFailedDialog";

    public static void show(@NonNull FragmentManager supportFragmentManager) {
        LoginFailedDialogFragment.showDialog(supportFragmentManager, TAG, new GoogleLoginFailedDialogFragment(), Integer.valueOf(R.string.error_dialog_google_login_failed_no_account), Integer.valueOf(R.string.error_dialog_title), Integer.valueOf(R.string.error_dialog_button_negative_cancel), Integer.valueOf(R.string.error_dialog_button_sign_up));
    }

    protected void onPositiveButtonClicked() {
        super.onPositiveButtonClicked();
        Analytics.sendEvent(getContext(), AnalyticsEvent.CANCEL_GOOGLE_LOGIN_MODAL);
    }

    protected void onNegativeButtonClicked() {
        super.onNegativeButtonClicked();
        Analytics.sendEvent(getContext(), AnalyticsEvent.SIGN_UP_ON_GOOGLE_LOGIN_MODAL);
        BaseChromeTabActivity.start(getActivity(), getString(R.string.url_link_for_signup));
    }
}
