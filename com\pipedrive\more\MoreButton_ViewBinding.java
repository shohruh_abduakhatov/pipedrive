package com.pipedrive.more;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.content.ContextCompat;
import android.view.View;
import butterknife.Unbinder;
import com.pipedrive.R;

public class MoreButton_ViewBinding implements Unbinder {
    @UiThread
    public MoreButton_ViewBinding(MoreButton target) {
        this(target, target.getContext());
    }

    @UiThread
    @Deprecated
    public MoreButton_ViewBinding(MoreButton target, View source) {
        this(target, source.getContext());
    }

    @UiThread
    public MoreButton_ViewBinding(MoreButton target, Context context) {
        Resources res = context.getResources();
        target.backgroundColor = ContextCompat.getColor(context, R.color.white);
        target.defaultContentPadding = res.getDimensionPixelSize(R.dimen.default_content_padding);
    }

    @CallSuper
    public void unbind() {
    }
}
