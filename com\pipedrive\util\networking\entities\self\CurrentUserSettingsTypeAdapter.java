package com.pipedrive.util.networking.entities.self;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.gson.GsonHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.UserSettingsPipelineFilter;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.io.IOException;
import java.util.Map.Entry;

class CurrentUserSettingsTypeAdapter extends TypeAdapter<CurrentUserSettings> {
    CurrentUserSettingsTypeAdapter() {
    }

    public void write(JsonWriter out, CurrentUserSettings value) throws IOException {
        throw new RuntimeException("Not implemented since not used!");
    }

    public CurrentUserSettings read(JsonReader in) throws IOException {
        JsonElement root = new JsonParser().parse(in);
        if (!root.isJsonObject()) {
            return null;
        }
        JsonObject rootAsJsonObject = root.getAsJsonObject();
        CurrentUserSettings currentUserSettings = (CurrentUserSettings) GsonHelper.fromJSON(rootAsJsonObject, CurrentUserSettings.class);
        for (Entry<String, JsonElement> entry : rootAsJsonObject.entrySet()) {
            String key = (String) entry.getKey();
            if (key.startsWith("filter_pipeline_") && ((JsonElement) entry.getValue()).isJsonPrimitive()) {
                try {
                    currentUserSettings.addPipelineFilter(new UserSettingsPipelineFilter(Long.valueOf(Long.parseLong(key.substring(key.lastIndexOf(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR) + 1))).longValue(), ((JsonElement) entry.getValue()).getAsJsonPrimitive().getAsString()));
                } catch (Exception e) {
                    Session activeSession = PipedriveApp.getActiveSession();
                    if (activeSession != null) {
                        LogJourno.reportEvent(activeSession, EVENT.Networking_pipelineFilterExtractionFailed, String.format("filterIdentification:[%s] filterValue:[%s]", new Object[]{key, filterName}), e);
                        Log.e(new Throwable(String.format("Error parsing pipeline filter: filterIdentification: %s; filterValue: %s", new Object[]{key, filterName}), e));
                    }
                }
            }
        }
        return currentUserSettings;
    }
}
