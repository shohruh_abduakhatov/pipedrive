package com.pipedrive.model.delta;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.newrelic.agent.android.instrumentation.JSONArrayInstrumentation;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.model.BaseDatasourceEntity;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ChangeSet<HOST extends BaseDatasourceEntity> {
    private List<ChangeSetItem<HOST, ?>> mProperties;

    public static ChangeSet fromString(String jsonString) {
        try {
            List<ChangeSetItem> items = new ArrayList();
            JSONArray jsonArray = JSONArrayInstrumentation.init(jsonString);
            for (int i = 0; i < jsonArray.length(); i++) {
                ChangeSetItem item = ChangeSetItem.fromString(jsonArray.optString(i).toString());
                if (item != null) {
                    items.add(item);
                }
            }
            return new ChangeSet(items);
        } catch (JSONException e) {
            Log.e(e);
            return null;
        }
    }

    public ChangeSet() {
        this(new ArrayList());
    }

    public ChangeSet(@NonNull List<ChangeSetItem<HOST, ?>> properties) {
        this.mProperties = properties;
    }

    public void add(ChangeSetItem<HOST, ?> property) {
        if (this.mProperties != null) {
            this.mProperties.add(property);
        }
    }

    public ChangeSetItem<HOST, ?> get(int index) {
        if (this.mProperties != null) {
            return (ChangeSetItem) this.mProperties.get(index);
        }
        return null;
    }

    public int size() {
        return this.mProperties == null ? 0 : this.mProperties.size();
    }

    public String toString() {
        JSONArray jsonArray = new JSONArray();
        for (ChangeSetItem<HOST, ?> property : this.mProperties) {
            jsonArray.put(property);
        }
        return !(jsonArray instanceof JSONArray) ? jsonArray.toString() : JSONArrayInstrumentation.toString(jsonArray);
    }

    @Nullable
    public JSONObject getJSON(HOST item) {
        JSONObject jSONObject = null;
        if (!(item == null || !item.isExisting() || this.mProperties == null)) {
            jSONObject = new JSONObject();
            for (ChangeSetItem<HOST, ?> property : this.mProperties) {
                try {
                    if (BaseDatasourceEntity.class.isAssignableFrom(property.getType())) {
                        BaseDatasourceEntity entity = (BaseDatasourceEntity) property.get(item);
                        if (entity.isExisting()) {
                            jSONObject.put(property.getJsonParamName(), entity.getPipedriveId());
                        }
                    } else {
                        jSONObject.put(property.getJsonParamName(), property.get(item));
                    }
                } catch (JSONException e) {
                    Log.e(e);
                }
            }
            if (jSONObject.length() > 0) {
                try {
                    jSONObject.put(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, item.getPipedriveId());
                } catch (JSONException e2) {
                    Log.e(e2);
                }
            }
        }
        return jSONObject;
    }
}
