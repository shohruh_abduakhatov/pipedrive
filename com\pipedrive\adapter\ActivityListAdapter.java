package com.pipedrive.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorTreeAdapter;
import android.widget.TextView;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.model.Activity;
import com.pipedrive.views.viewholder.ViewHolderBuilder;
import com.pipedrive.views.viewholder.activity.ActivityRowViewHolderWithCheckbox;

public class ActivityListAdapter extends CursorTreeAdapter {
    public static final int GROUP_FUTURE = 2;
    public static final int GROUP_PAST = 1;
    @Nullable
    private ActivityDoneChangedListener mActivityDoneChangedListener;
    @Nullable
    private String mActivityType;
    private DataSetChangedCallback mDataSetChangedCallback;
    private final ActivitiesDataSource mDataSource = new ActivitiesDataSource(this.mSession);
    private final Session mSession;
    @Nullable
    private Integer mTimeFilter;
    @Nullable
    private Integer mUserId;

    public interface ActivityDoneChangedListener {
        void onActivityDoneStatusChanged(Activity activity, boolean z);
    }

    public interface DataSetChangedCallback {
        void onDataSetChanged();
    }

    public ActivityListAdapter(@NonNull Session session, Cursor cursor, Context context, @Nullable String activityType, @Nullable Integer timeFilter, @Nullable Integer userId) {
        super(cursor, context);
        this.mSession = session;
        this.mActivityType = activityType;
        this.mTimeFilter = timeFilter;
        this.mUserId = userId;
    }

    public void setActivityDoneChangedListener(@NonNull ActivityDoneChangedListener activityDoneChangedListener) {
        this.mActivityDoneChangedListener = activityDoneChangedListener;
    }

    public void setDataSetChangedCallback(DataSetChangedCallback dataSetChangedCallback) {
        this.mDataSetChangedCallback = dataSetChangedCallback;
    }

    protected Cursor getChildrenCursor(Cursor groupCursor) {
        switch (groupCursor.getInt(0)) {
            case 1:
                return this.mDataSource.getUndoneActivePastActivitiesCursor(this.mActivityType, Integer.valueOf(this.mTimeFilter == null ? 1 : this.mTimeFilter.intValue()), this.mUserId);
            case 2:
                int i;
                ActivitiesDataSource activitiesDataSource = this.mDataSource;
                String str = this.mActivityType;
                if (this.mTimeFilter == null) {
                    i = 1000;
                } else {
                    i = this.mTimeFilter.intValue();
                }
                return activitiesDataSource.getUndoneActiveFutureActivitiesCursor(str, Integer.valueOf(i), this.mUserId);
            default:
                return null;
        }
    }

    protected View newGroupView(Context context, Cursor cursor, boolean isExpanded, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.group_activity_list, parent, false);
    }

    protected void bindGroupView(@NonNull View view, Context context, @NonNull Cursor cursor, boolean isExpanded) {
        TextView textView = (TextView) view;
        int childrenCount = getChildrenCount(cursor.getPosition());
        textView.setText(String.format("%s %d", new Object[]{cursor.getString(1), Integer.valueOf(childrenCount)}));
        switch (cursor.getInt(0)) {
            case 1:
                textView.setTextColor(ContextCompat.getColor(context, R.color.red));
                return;
            case 2:
                textView.setTextColor(ContextCompat.getColor(context, R.color.mid));
                return;
            default:
                return;
        }
    }

    protected View newChildView(Context context, Cursor cursor, boolean isLastChild, ViewGroup parent) {
        return ViewHolderBuilder.inflateViewAndTagWithViewHolder(context, parent, false, new ActivityRowViewHolderWithCheckbox());
    }

    protected void bindChildView(@NonNull View view, Context context, @NonNull Cursor cursor, boolean isLastChild) {
        com.pipedrive.views.viewholder.activity.ActivityRowViewHolderWithCheckbox.ActivityDoneChangedListener activityDoneChangedListener;
        ActivityRowViewHolderWithCheckbox viewHolder = (ActivityRowViewHolderWithCheckbox) view.getTag();
        if (this.mActivityDoneChangedListener == null) {
            activityDoneChangedListener = null;
        } else {
            activityDoneChangedListener = new com.pipedrive.views.viewholder.activity.ActivityRowViewHolderWithCheckbox.ActivityDoneChangedListener() {
                public void onActivityDoneStatusChanged(@NonNull Session session, long sqlIdOfChangedActivity, boolean isDone) {
                    Activity activity = (Activity) new ActivitiesDataSource(session).findBySqlId(sqlIdOfChangedActivity);
                    if (activity != null) {
                        ActivityListAdapter.this.mActivityDoneChangedListener.onActivityDoneStatusChanged(activity, isDone);
                    }
                }
            };
        }
        viewHolder.fill(this.mSession, context, this.mDataSource.deflateCursor(cursor), activityDoneChangedListener);
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        if (this.mDataSetChangedCallback != null) {
            this.mDataSetChangedCallback.onDataSetChanged();
        }
    }

    public void setFilter(Context context, @Nullable String type, @Nullable Integer timeFilter, @Nullable Integer userId) {
        this.mActivityType = type;
        this.mTimeFilter = timeFilter;
        this.mUserId = userId;
        refreshCursor(context);
    }

    public int getChildrenCount() {
        int count = 0;
        for (int i = 0; i < getGroupCount(); i++) {
            count += getChildrenCount(i);
        }
        return count;
    }

    public void refreshCursor(Context context) {
        setGroupCursor(this.mDataSource.getActiveActivityListGroupCursor(context, this.mActivityType, this.mTimeFilter, this.mUserId));
        notifyDataSetChanged();
    }
}
