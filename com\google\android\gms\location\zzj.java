package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzj implements Creator<LocationAvailability> {
    static void zza(LocationAvailability locationAvailability, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zzc(parcel, 1, locationAvailability.aki);
        zzb.zzc(parcel, 2, locationAvailability.akj);
        zzb.zza(parcel, 3, locationAvailability.akk);
        zzb.zzc(parcel, 4, locationAvailability.akl);
        zzb.zzc(parcel, 1000, locationAvailability.getVersionCode());
        zzb.zzaj(parcel, zzcs);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zznv(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzup(i);
    }

    public LocationAvailability zznv(Parcel parcel) {
        int i = 1;
        int zzcr = zza.zzcr(parcel);
        int i2 = 0;
        int i3 = 1000;
        long j = 0;
        int i4 = 1;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    i4 = zza.zzg(parcel, zzcq);
                    break;
                case 2:
                    i = zza.zzg(parcel, zzcq);
                    break;
                case 3:
                    j = zza.zzi(parcel, zzcq);
                    break;
                case 4:
                    i3 = zza.zzg(parcel, zzcq);
                    break;
                case 1000:
                    i2 = zza.zzg(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new LocationAvailability(i2, i3, i4, i, j);
        }
        throw new zza.zza("Overread allowed size end=" + zzcr, parcel);
    }

    public LocationAvailability[] zzup(int i) {
        return new LocationAvailability[i];
    }
}
