package com.pipedrive.model.delta;

import com.pipedrive.datasource.BaseDataSource;
import com.pipedrive.model.BaseDatasourceEntity;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Model {
    Class<? extends BaseDataSource<? extends BaseDatasourceEntity>> dataSource();
}
