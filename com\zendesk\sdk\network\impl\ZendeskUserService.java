package com.zendesk.sdk.network.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.sdk.model.request.UserFieldRequest;
import com.zendesk.sdk.model.request.UserFieldResponse;
import com.zendesk.sdk.model.request.UserResponse;
import com.zendesk.sdk.model.request.UserTagRequest;
import com.zendesk.sdk.network.UserService;
import com.zendesk.service.RetrofitZendeskCallbackAdapter;
import com.zendesk.service.ZendeskCallback;

class ZendeskUserService {
    private final UserService userService;

    ZendeskUserService(UserService userService) {
        this.userService = userService;
    }

    void addTags(@Nullable String authorizationHeader, @NonNull UserTagRequest userTagRequest, ZendeskCallback<UserResponse> callback) {
        this.userService.addTags(authorizationHeader, userTagRequest).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }

    void deleteTags(@Nullable String authorizationHeader, @NonNull String tags, ZendeskCallback<UserResponse> callback) {
        this.userService.deleteTags(authorizationHeader, tags).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }

    void getUserFields(@Nullable String authorizationHeader, ZendeskCallback<UserFieldResponse> callback) {
        this.userService.getUserFields(authorizationHeader).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }

    void setUserFields(@Nullable String authorizationHeader, UserFieldRequest userFieldRequest, ZendeskCallback<UserResponse> callback) {
        this.userService.setUserFields(authorizationHeader, userFieldRequest).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }

    void getUser(@Nullable String authorizationHeader, ZendeskCallback<UserResponse> callback) {
        this.userService.getUser(authorizationHeader).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }
}
