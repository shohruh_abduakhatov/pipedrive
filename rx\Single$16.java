package rx;

import rx.functions.Action1;

class Single$16 implements Action1<Throwable> {
    final /* synthetic */ Single this$0;
    final /* synthetic */ Action1 val$onNotification;

    Single$16(Single single, Action1 action1) {
        this.this$0 = single;
        this.val$onNotification = action1;
    }

    public void call(Throwable throwable) {
        this.val$onNotification.call(Notification.createOnError(throwable));
    }
}
