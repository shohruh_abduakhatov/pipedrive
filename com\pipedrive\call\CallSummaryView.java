package com.pipedrive.call;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.model.Activity;

public interface CallSummaryView {
    void onAssociationsUpdated(@NonNull Activity activity);

    void onCallActivityCreated(boolean z);

    void onCallActivityUpdated(boolean z);

    void onRequestCallActivity(@Nullable Activity activity);
}
