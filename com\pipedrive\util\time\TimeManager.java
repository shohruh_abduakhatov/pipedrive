package com.pipedrive.util.time;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Calendar;
import java.util.TimeZone;

public class TimeManager {
    @Nullable
    private static TimeManager instance;
    @NonNull
    private TimeProvider defaultTimeProvider = new DefaultTimeProvider();
    @NonNull
    private TimeProvider timeProvider = this.defaultTimeProvider;

    @NonNull
    public static TimeManager getInstance() {
        if (instance == null) {
            synchronized (TimeManager.class) {
                if (instance == null) {
                    instance = new TimeManager();
                }
            }
        }
        return instance;
    }

    void setTimeProvider(@NonNull TimeProvider timeProvider) {
        this.timeProvider = timeProvider;
    }

    void resetTimeProviderToDefault() {
        this.timeProvider = this.defaultTimeProvider;
    }

    @NonNull
    public Long currentTimeMillis() {
        return this.timeProvider.getCurrentTimeMillis();
    }

    @NonNull
    public TimeZone getDefaultTimeZone() {
        return this.timeProvider.getDefaultTimeZone();
    }

    @NonNull
    public TimeZone getUtcTimeZone() {
        return this.timeProvider.getUtcTimeZone();
    }

    @NonNull
    public Calendar getUtcCalendar() {
        Calendar utcCalendar = Calendar.getInstance(getUtcTimeZone());
        utcCalendar.setTimeInMillis(currentTimeMillis().longValue());
        return utcCalendar;
    }

    @NonNull
    public Calendar getLocalCalendar() {
        Calendar localCalendar = Calendar.getInstance(getDefaultTimeZone());
        localCalendar.setTimeInMillis(currentTimeMillis().longValue());
        return localCalendar;
    }
}
