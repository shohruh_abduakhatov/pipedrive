package com.pipedrive.store;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.changes.DealChanger;
import com.pipedrive.datasource.FilterDealsDataSource;
import com.pipedrive.model.Deal;
import com.pipedrive.util.StringUtils;

public class StoreDeal extends Store<Deal> {
    public StoreDeal(@NonNull Session session) {
        super(session);
    }

    void cleanupBeforeCreate(@NonNull Deal newDealBeingStored) {
        if (newDealBeingStored.getOrganization() != null) {
            new StoreOrganization(getSession()).cleanupBeforeCreate(newDealBeingStored.getOrganization());
        }
        if (newDealBeingStored.getPerson() != null) {
            new StorePerson(getSession()).cleanupBeforeCreate(newDealBeingStored.getPerson());
        }
        processBeforeStoringShared(newDealBeingStored);
    }

    boolean implementCreate(@NonNull Deal newDeal) {
        return new DealChanger(getSession()).create(newDeal);
    }

    void cleanupBeforeUpdate(@NonNull Deal updatedDealBeingStored) {
        if (updatedDealBeingStored.getOrganization() != null) {
            new StoreOrganization(getSession()).cleanupBeforeUpdate(updatedDealBeingStored.getOrganization());
        }
        if (updatedDealBeingStored.getPerson() != null) {
            new StorePerson(getSession()).cleanupBeforeUpdate(updatedDealBeingStored.getPerson());
        }
        new FilterDealsDataSource(getSession().getDatabase()).unrelateAllDealAssociations(updatedDealBeingStored);
        processBeforeStoringShared(updatedDealBeingStored);
    }

    boolean implementUpdate(@NonNull Deal updatedDeal) {
        return new DealChanger(getSession()).update(updatedDeal);
    }

    private void processBeforeStoringShared(Deal dealBeingStored) {
        Session session = getSession();
        if (dealBeingStored.getOwnerPipedriveId() <= 0) {
            dealBeingStored.setOwnerPipedriveId(Long.valueOf(session.getAuthenticatedUserID()).intValue());
        }
        if (StringUtils.isTrimmedAndEmpty(dealBeingStored.getOwnerName()) && ((long) dealBeingStored.getOwnerPipedriveId()) == session.getAuthenticatedUserID()) {
            dealBeingStored.setOwnerName(session.getUserSettingsName(null));
        }
        if (dealBeingStored.getOrganization() != null && dealBeingStored.getPerson() != null && !dealBeingStored.getPerson().isStored() && dealBeingStored.getPerson().getCompany() == null) {
            dealBeingStored.getPerson().setCompany(dealBeingStored.getOrganization());
        }
    }
}
