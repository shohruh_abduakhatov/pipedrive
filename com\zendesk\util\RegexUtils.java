package com.zendesk.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtils {
    private static final Pattern QUANTIFIER_PATTERN = Pattern.compile(QUANTIFIER_VALIDATION_REGEX);
    private static final String QUANTIFIER_VALIDATION_REGEX = "^\\{\\d{1,},?\\d*\\}";

    private RegexUtils() {
    }

    public static String escape(String regex) {
        if (regex == null) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        char[] chars = regex.toCharArray();
        int i = 0;
        while (i < chars.length) {
            char ch = chars[i];
            if (ch == '{') {
                String expression = validateQuantifierExpression(regex.substring(i));
                if (expression != null) {
                    builder.append(expression);
                    i += expression.length() - 1;
                } else {
                    builder.append(Pattern.quote(Character.toString(ch)));
                }
            } else {
                builder.append(ch);
            }
            i++;
        }
        return builder.toString();
    }

    static String validateQuantifierExpression(String regex) {
        Matcher matcher = QUANTIFIER_PATTERN.matcher(regex);
        return matcher.find() ? matcher.group() : null;
    }
}
