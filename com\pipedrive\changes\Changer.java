package com.pipedrive.changes;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.changes.Change.ChangeOperationType;
import com.pipedrive.datasource.ChangesDataSource;
import com.pipedrive.datasource.SQLTransactionManager;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;

abstract class Changer<T> extends Change {
    @Nullable
    private final Changer mParentChanger;
    @NonNull
    private final Session mSession;

    protected enum ChangeResult {
        SUCCESSFUL,
        FAILED
    }

    @NonNull
    abstract ChangeResult createChange(@NonNull T t);

    @Nullable
    abstract Long getChangeMetaData(@NonNull T t, @NonNull ChangeOperationType changeOperationType);

    @NonNull
    abstract ChangeResult updateChange(@NonNull T t);

    protected Changer(@NonNull Session session, int changeTypeIdentifier, @Nullable Changer parentChanger) {
        this.mSession = session;
        this.mChangeTypeIdentifier = changeTypeIdentifier;
        this.mParentChanger = parentChanger;
    }

    @NonNull
    protected Session getSession() {
        return this.mSession;
    }

    @NonNull
    ChangeResult deleteChange(@NonNull T t) {
        throw new RuntimeException("Not implemented!");
    }

    private boolean registerChange(@NonNull T changer) {
        SQLTransactionManager sqlTransactionManager = null;
        if (this.mParentChanger != null) {
            sqlTransactionManager = new SQLTransactionManager(getSession().getDatabase());
            sqlTransactionManager.beginTransaction();
        }
        try {
            setChangeMetaData(getChangeMetaData(changer, getChangeOperationType()));
            if (new ChangesDataSource(getSession()).createOrUpdate((Change) this) == -1) {
                LogJourno.reportEvent(EVENT.ChangesChanger_registerChangeSavingIntoDBFailed, String.format("Change:[%s]", new Object[]{changer.toString()}));
                Log.e(new Throwable(String.format("Change creation failed! Change: %s", new Object[]{changer.toString()})));
                return false;
            }
            if (this.mParentChanger != null) {
                sqlTransactionManager.commit();
            }
            return true;
        } finally {
            if (this.mParentChanger != null) {
                sqlTransactionManager.commit();
            }
        }
    }

    public final boolean create(@Nullable T changeHolder) {
        if (changeHolder == null) {
            LogJourno.reportEvent(EVENT.ChangesChanger_createChangeRequestedWithNull);
            Log.e(new Throwable("I need the change to create the change!"));
            return false;
        }
        setChangeOperationType(ChangeOperationType.OPERATION_CREATE);
        if (createChange(changeHolder) == ChangeResult.SUCCESSFUL) {
            return registerChange(changeHolder);
        }
        LogJourno.reportEvent(EVENT.ChangesChanger_createChangeFailed, String.format("Change:[%s]", new Object[]{changeHolder.toString()}));
        Log.e(new Throwable(String.format("Failed create change will not be saved as a change! Change: %s", new Object[]{changeHolder.toString()})));
        return false;
    }

    public final boolean update(@Nullable T changeHolder) {
        if (changeHolder == null) {
            LogJourno.reportEvent(EVENT.ChangesChanger_updateChangeRequestedWithNull);
            Log.e(new Throwable("I need the change to update the change!"));
            return false;
        }
        setChangeOperationType(ChangeOperationType.OPERATION_UPDATE);
        if (updateChange(changeHolder) == ChangeResult.SUCCESSFUL) {
            return registerChange(changeHolder);
        }
        LogJourno.reportEvent(EVENT.ChangesChanger_updateChangeFailed, String.format("Change:[%s]", new Object[]{changeHolder.toString()}));
        Log.e(new Throwable(String.format("Failed update change will not be saved as a change! Change: %s", new Object[]{changeHolder.toString()})));
        return false;
    }

    public final boolean delete(@Nullable T changeHolder) {
        if (changeHolder == null) {
            LogJourno.reportEvent(EVENT.ChangesChanger_deleteChangeRequestedWithNull);
            Log.e(new Throwable("I need the change to delete the change!"));
            return false;
        }
        setChangeOperationType(ChangeOperationType.OPERATION_DELETE);
        if (deleteChange(changeHolder) == ChangeResult.SUCCESSFUL) {
            return registerChange(changeHolder);
        }
        LogJourno.reportEvent(EVENT.ChangesChanger_deleteChangeFailed, String.format("Change:[%s]", new Object[]{changeHolder.toString()}));
        Log.e(new Throwable(String.format("Failed delete change will not be saved as a change! Change: %s", new Object[]{changeHolder.toString()})));
        return false;
    }
}
