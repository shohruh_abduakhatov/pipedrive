package com.zendesk.collection;

import com.zendesk.util.CollectionUtils;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class CountedSet<E> implements Set<E> {
    public static final int NOT_FOUND = 0;
    private Map<E, Integer> countingMap;

    public CountedSet() {
        this.countingMap = new HashMap();
    }

    public CountedSet(int initialCapacity) {
        this.countingMap = new HashMap(initialCapacity);
    }

    public int size() {
        return this.countingMap.size();
    }

    public boolean isEmpty() {
        return this.countingMap.isEmpty();
    }

    public boolean contains(Object o) {
        return this.countingMap.containsKey(o);
    }

    public Iterator<E> iterator() {
        return this.countingMap.keySet().iterator();
    }

    public Object[] toArray() {
        return this.countingMap.keySet().toArray();
    }

    public <T> T[] toArray(T[] a) {
        return this.countingMap.keySet().toArray(a);
    }

    public boolean add(E e) {
        if (e == null) {
            return false;
        }
        if (this.countingMap.containsKey(e)) {
            this.countingMap.put(e, Integer.valueOf(((Integer) this.countingMap.get(e)).intValue() + 1));
            return true;
        }
        this.countingMap.put(e, Integer.valueOf(1));
        return true;
    }

    public boolean remove(Object o) {
        if (!this.countingMap.containsKey(o)) {
            return false;
        }
        E key = o;
        int count = ((Integer) this.countingMap.get(o)).intValue();
        if (count > 1) {
            this.countingMap.put(key, Integer.valueOf(count - 1));
        } else {
            this.countingMap.remove(o);
        }
        return true;
    }

    public boolean containsAll(Collection<?> collection) {
        if (!CollectionUtils.isNotEmpty(collection)) {
            return false;
        }
        for (Object object : collection) {
            if (!this.countingMap.containsKey(object)) {
                return false;
            }
        }
        return true;
    }

    public boolean addAll(Collection<? extends E> collection) {
        if (!CollectionUtils.isNotEmpty(collection)) {
            return false;
        }
        for (E element : collection) {
            if (!add(element)) {
                return false;
            }
        }
        return true;
    }

    public boolean removeAll(Collection<?> collection) {
        if (!CollectionUtils.isNotEmpty(collection)) {
            return false;
        }
        for (Object object : collection) {
            if (!this.countingMap.containsKey(object)) {
                return false;
            }
        }
        for (Object object2 : collection) {
            this.countingMap.remove(object2);
        }
        return true;
    }

    public boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    public void clear() {
        this.countingMap.clear();
    }

    public int getCount(E key) {
        if (key == null || !this.countingMap.containsKey(key)) {
            return 0;
        }
        return ((Integer) this.countingMap.get(key)).intValue();
    }
}
