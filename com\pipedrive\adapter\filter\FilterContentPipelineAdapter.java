package com.pipedrive.adapter.filter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.pipedrive.R;
import com.pipedrive.model.Filter;
import com.pipedrive.model.User;
import com.pipedrive.views.profilepicture.ProfilePictureRoundUser;
import java.util.List;

public class FilterContentPipelineAdapter extends ArrayAdapter<FilterRow> {
    private static final int TYPE_CONTENT_FILTER = 2;
    private static final int TYPE_CONTENT_USER = 1;
    private static final int TYPE_COUNT = 3;
    private static final int TYPE_HEADER = 0;

    public interface FilterRow {
        @Nullable
        Filter getFilter();

        @Nullable
        String getLabel();

        @Nullable
        User getUser();
    }

    public static class Header implements FilterRow {
        @NonNull
        final String mLabel;

        public Header(@NonNull String label) {
            this.mLabel = label;
        }

        @NonNull
        public String getLabel() {
            return this.mLabel;
        }

        @Nullable
        public User getUser() {
            return null;
        }

        @Nullable
        public Filter getFilter() {
            return null;
        }
    }

    public FilterContentPipelineAdapter(Context context, List<FilterRow> pipelineFilters) {
        super(context, 17367043, pipelineFilters);
    }

    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        int itemViewType = getItemViewType(position);
        FilterRow item = (FilterRow) getItem(position);
        boolean returnHeaderRow = itemViewType == 0 && item != null;
        boolean returnHeaderContentUser = (itemViewType != 1 || item == null || item.getUser() == null) ? false : true;
        boolean returnHeaderContentFilter = (itemViewType != 2 || item == null || item.getFilter() == null) ? false : true;
        if (convertView == null) {
            convertView = ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate(returnHeaderRow ? R.layout.row_filter_header : R.layout.row_filter_content, parent, false);
        }
        if (returnHeaderRow) {
            fillRowOfTypeHeader(convertView, item);
        } else if (returnHeaderContentUser) {
            if (((ProfilePictureRoundUser) ButterKnife.findById(convertView, (int) R.id.profilePicture)) == null) {
                ViewStub profilePictureViewStub = (ViewStub) ButterKnife.findById(convertView, (int) R.id.profilePictureViewStub);
                if (profilePictureViewStub != null) {
                    profilePictureViewStub.inflate();
                }
            }
            fillRowOfTypeContentUser(convertView, item.getUser());
        } else if (returnHeaderContentFilter) {
            fillRowOfTypeContentFilter(convertView, item.getFilter());
        }
        return convertView;
    }

    public int getViewTypeCount() {
        return 3;
    }

    public int getItemViewType(int position) {
        FilterRow item = (FilterRow) getItem(position);
        if (item == null) {
            return -1;
        }
        boolean isTypeHeader;
        boolean isTypeContentUser;
        boolean isTypeContentFilter;
        if (item.getUser() == null && item.getFilter() == null) {
            isTypeHeader = true;
        } else {
            isTypeHeader = false;
        }
        if (isTypeHeader || item.getUser() == null) {
            isTypeContentUser = false;
        } else {
            isTypeContentUser = true;
        }
        if (isTypeHeader || item.getFilter() == null) {
            isTypeContentFilter = false;
        } else {
            isTypeContentFilter = true;
        }
        if (isTypeHeader) {
            return 0;
        }
        if (isTypeContentUser) {
            return 1;
        }
        if (isTypeContentFilter) {
            return 2;
        }
        return -1;
    }

    public boolean isEnabled(int position) {
        int itemViewType = getItemViewType(position);
        if (itemViewType == 2 || itemViewType == 1) {
            return true;
        }
        return false;
    }

    private void fillRowOfTypeHeader(@NonNull View layoutToFill, @NonNull FilterRow header) {
        ((TextView) ButterKnife.findById(layoutToFill, (int) R.id.label)).setText(header.getLabel());
    }

    private void fillRowOfTypeContentUser(@NonNull View layoutToFill, @NonNull User user) {
        ProfilePictureRoundUser profilePictureRoundUser = (ProfilePictureRoundUser) ButterKnife.findById(layoutToFill, (int) R.id.profilePicture);
        if (profilePictureRoundUser != null) {
            boolean isRowWithLabelEveryone;
            if (user.getPipedriveIdOrNull() == null) {
                isRowWithLabelEveryone = true;
            } else {
                isRowWithLabelEveryone = false;
            }
            if (isRowWithLabelEveryone) {
                profilePictureRoundUser.setVisibility(4);
            } else {
                profilePictureRoundUser.setVisibility(0);
                profilePictureRoundUser.loadPicture(user);
            }
        }
        ((TextView) ButterKnife.findById(layoutToFill, (int) R.id.label)).setText(user.getLabel());
    }

    private void fillRowOfTypeContentFilter(@NonNull View layoutToFill, @NonNull Filter filter) {
        ((TextView) ButterKnife.findById(layoutToFill, (int) R.id.label)).setText(filter.getLabel());
    }
}
