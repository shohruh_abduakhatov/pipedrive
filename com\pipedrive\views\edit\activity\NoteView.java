package com.pipedrive.views.edit.activity;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

public class NoteView extends TextView {
    public NoteView(Context context) {
        super(context);
    }

    public NoteView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NoteView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public final void setText(@Nullable String text) {
        super.setText(prepareNoteForView(text));
    }

    private String prepareNoteForView(@Nullable String note) {
        String noteInHtmlFormat;
        String safeContentForWebView = "";
        if (TextUtils.isEmpty(note)) {
            noteInHtmlFormat = "";
        } else {
            noteInHtmlFormat = note;
        }
        return Html.fromHtml(noteInHtmlFormat).toString().replaceAll("\\n\\n", "\n");
    }
}
