package com.zendesk.sdk.support.help;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.sdk.model.helpcenter.SearchArticle;
import com.zendesk.sdk.support.ViewArticleActivity;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

class HelpSearchRecyclerViewAdapter extends Adapter {
    private static final String LOG_TAG = "HelpSearchRecyclerViewAdapter";
    private static final int TYPE_ARTICLE = 531;
    private static final int TYPE_NO_RESULTS = 441;
    private static final int TYPE_PADDING = 423;
    private boolean addEndPadding = false;
    private String query;
    private boolean resultsCleared = false;
    private List<SearchArticle> searchArticles;

    private class HelpSearchViewHolder extends ViewHolder {
        private Context context;
        private TextView subtitleTextView;
        private TextView titleTextView;

        HelpSearchViewHolder(View itemView, Context context) {
            super(itemView);
            this.titleTextView = (TextView) itemView.findViewById(R.id.title);
            this.subtitleTextView = (TextView) itemView.findViewById(R.id.subtitle);
            this.context = context;
        }

        void bindTo(final SearchArticle searchArticle) {
            if (searchArticle == null || searchArticle.getArticle() == null) {
                Logger.e(HelpSearchRecyclerViewAdapter.LOG_TAG, "The article was null, cannot bind the view.", new Object[0]);
                return;
            }
            int startIndex;
            String title = searchArticle.getArticle().getTitle() != null ? searchArticle.getArticle().getTitle() : "";
            if (HelpSearchRecyclerViewAdapter.this.query == null) {
                startIndex = -1;
            } else {
                startIndex = title.toLowerCase(Locale.getDefault()).indexOf(HelpSearchRecyclerViewAdapter.this.query.toLowerCase());
            }
            if (startIndex != -1) {
                SpannableString spannableString = new SpannableString(title);
                spannableString.setSpan(new StyleSpan(1), startIndex, HelpSearchRecyclerViewAdapter.this.query.length() + startIndex, 18);
                this.titleTextView.setText(spannableString);
            } else {
                this.titleTextView.setText(title);
            }
            this.subtitleTextView.setText(this.context.getString(R.string.guide_search_subtitle_format, new Object[]{searchArticle.getCategory().getName(), searchArticle.getSection().getName()}));
            this.itemView.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    ViewArticleActivity.startActivity(HelpSearchViewHolder.this.itemView.getContext(), searchArticle.getArticle());
                }
            });
        }
    }

    private class NoResultsViewHolder extends ViewHolder {
        NoResultsViewHolder(View itemView) {
            super(itemView);
        }
    }

    private class PaddingViewHolder extends ViewHolder {
        PaddingViewHolder(View itemView) {
            super(itemView);
        }
    }

    HelpSearchRecyclerViewAdapter(List<SearchArticle> searchArticles, String query, boolean addEndPadding) {
        this.searchArticles = searchArticles;
        this.query = query;
        this.addEndPadding = addEndPadding;
    }

    void update(List<SearchArticle> searchArticles, String query) {
        this.resultsCleared = false;
        this.searchArticles = searchArticles;
        this.query = query;
        notifyDataSetChanged();
    }

    void clearResults() {
        this.resultsCleared = true;
        this.searchArticles = Collections.emptyList();
        this.query = "";
        notifyDataSetChanged();
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_PADDING /*423*/:
                return new PaddingViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_padding, parent, false));
            case TYPE_NO_RESULTS /*441*/:
                return new NoResultsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_no_articles_found, parent, false));
            case TYPE_ARTICLE /*531*/:
                return new HelpSearchViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_search_article, parent, false), parent.getContext());
            default:
                return null;
        }
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        if (TYPE_ARTICLE == getItemViewType(position)) {
            ((HelpSearchViewHolder) holder).bindTo((SearchArticle) this.searchArticles.get(position));
        }
    }

    public int getItemCount() {
        if (this.resultsCleared) {
            return 0;
        }
        return Math.max(this.searchArticles.size() + getPaddingExtraItem(), 1);
    }

    private int getPaddingExtraItem() {
        return this.addEndPadding ? 1 : 0;
    }

    public int getItemViewType(int position) {
        if (position == 0 && this.searchArticles.size() == 0) {
            return TYPE_NO_RESULTS;
        }
        if (position <= 0 || position != this.searchArticles.size()) {
            return TYPE_ARTICLE;
        }
        return TYPE_PADDING;
    }
}
