package com.pipedrive.sync;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.pipedrive.LoginActivity;
import com.pipedrive.logging.Log;

class AccountAuthenticatorService$AccountAuthenticatorImpl extends AbstractAccountAuthenticator {
    private static final String TAG = "AccountAuthenticatorImpl";
    private Context mContext;

    public AccountAuthenticatorService$AccountAuthenticatorImpl(Context context) {
        super(context);
        this.mContext = context;
    }

    public Bundle addAccount(AccountAuthenticatorResponse response, String accountType, String authTokenType, String[] requiredFeatures, Bundle options) throws NetworkErrorException {
        Log.d(TAG, "addAccount");
        if (AccountManager.get(this.mContext).getAccountsByType("com.pipedrive.account").length > 0) {
            Log.d(TAG, "addAccount - account already exists. will not allow to add another one.");
            return null;
        }
        Log.d(TAG, "addAccount - account does not exist. Firing up a login activity to add one.");
        Intent i = new Intent(this.mContext, LoginActivity.class);
        i.putExtra("com.pipedrive.account", authTokenType);
        i.putExtra("accountAuthenticatorResponse", response);
        Bundle reply = new Bundle();
        reply.putParcelable("intent", i);
        return reply;
    }

    public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options) {
        Log.d(TAG, "confirmCredentials - not supported.");
        return null;
    }

    public Bundle editProperties(AccountAuthenticatorResponse response, String accountType) {
        Log.d(TAG, "editProperties - not supported.");
        return null;
    }

    public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) throws NetworkErrorException {
        Log.d(TAG, "getAuthToken - not supported.");
        return null;
    }

    public String getAuthTokenLabel(String authTokenType) {
        Log.d(TAG, "getAuthTokenLabel - not supported.");
        return null;
    }

    public Bundle hasFeatures(AccountAuthenticatorResponse response, Account account, String[] features) throws NetworkErrorException {
        Log.d(TAG, "hasFeatures - not supported.");
        return null;
    }

    public Bundle updateCredentials(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) {
        Log.d(TAG, "updateCredentials - not supported.");
        return null;
    }

    public Bundle getAccountRemovalAllowed(AccountAuthenticatorResponse response, Account account) throws NetworkErrorException {
        Log.d(TAG, "getAccountRemovalAllowed - allow removal of the accounts.");
        Bundle reply = new Bundle();
        reply.putBoolean("booleanResult", true);
        return reply;
    }
}
