package com.google.android.gms.location.internal;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.location.zzh;
import com.google.android.gms.location.zzi;
import com.google.android.gms.location.zzi.zza;

public class LocationRequestUpdateData extends AbstractSafeParcelable {
    public static final Creator<LocationRequestUpdateData> CREATOR = new zzn();
    int alf;
    LocationRequestInternal alg;
    zzi alh;
    zzh ali;
    zzg alj;
    PendingIntent mPendingIntent;
    private final int mVersionCode;

    LocationRequestUpdateData(int i, int i2, LocationRequestInternal locationRequestInternal, IBinder iBinder, PendingIntent pendingIntent, IBinder iBinder2, IBinder iBinder3) {
        zzg com_google_android_gms_location_internal_zzg = null;
        this.mVersionCode = i;
        this.alf = i2;
        this.alg = locationRequestInternal;
        this.alh = iBinder == null ? null : zza.zzhc(iBinder);
        this.mPendingIntent = pendingIntent;
        this.ali = iBinder2 == null ? null : zzh.zza.zzhb(iBinder2);
        if (iBinder3 != null) {
            com_google_android_gms_location_internal_zzg = zzg.zza.zzhe(iBinder3);
        }
        this.alj = com_google_android_gms_location_internal_zzg;
    }

    public static LocationRequestUpdateData zza(LocationRequestInternal locationRequestInternal, PendingIntent pendingIntent, @Nullable zzg com_google_android_gms_location_internal_zzg) {
        return new LocationRequestUpdateData(1, 1, locationRequestInternal, null, pendingIntent, null, com_google_android_gms_location_internal_zzg != null ? com_google_android_gms_location_internal_zzg.asBinder() : null);
    }

    public static LocationRequestUpdateData zza(LocationRequestInternal locationRequestInternal, zzh com_google_android_gms_location_zzh, @Nullable zzg com_google_android_gms_location_internal_zzg) {
        return new LocationRequestUpdateData(1, 1, locationRequestInternal, null, null, com_google_android_gms_location_zzh.asBinder(), com_google_android_gms_location_internal_zzg != null ? com_google_android_gms_location_internal_zzg.asBinder() : null);
    }

    public static LocationRequestUpdateData zza(LocationRequestInternal locationRequestInternal, zzi com_google_android_gms_location_zzi, @Nullable zzg com_google_android_gms_location_internal_zzg) {
        return new LocationRequestUpdateData(1, 1, locationRequestInternal, com_google_android_gms_location_zzi.asBinder(), null, null, com_google_android_gms_location_internal_zzg != null ? com_google_android_gms_location_internal_zzg.asBinder() : null);
    }

    public static LocationRequestUpdateData zza(zzh com_google_android_gms_location_zzh, @Nullable zzg com_google_android_gms_location_internal_zzg) {
        return new LocationRequestUpdateData(1, 2, null, null, null, com_google_android_gms_location_zzh.asBinder(), com_google_android_gms_location_internal_zzg != null ? com_google_android_gms_location_internal_zzg.asBinder() : null);
    }

    public static LocationRequestUpdateData zza(zzi com_google_android_gms_location_zzi, @Nullable zzg com_google_android_gms_location_internal_zzg) {
        return new LocationRequestUpdateData(1, 2, null, com_google_android_gms_location_zzi.asBinder(), null, null, com_google_android_gms_location_internal_zzg != null ? com_google_android_gms_location_internal_zzg.asBinder() : null);
    }

    public static LocationRequestUpdateData zzb(PendingIntent pendingIntent, @Nullable zzg com_google_android_gms_location_internal_zzg) {
        return new LocationRequestUpdateData(1, 2, null, null, pendingIntent, null, com_google_android_gms_location_internal_zzg != null ? com_google_android_gms_location_internal_zzg.asBinder() : null);
    }

    int getVersionCode() {
        return this.mVersionCode;
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzn.zza(this, parcel, i);
    }

    IBinder zzbqi() {
        return this.alh == null ? null : this.alh.asBinder();
    }

    IBinder zzbqj() {
        return this.ali == null ? null : this.ali.asBinder();
    }

    IBinder zzbqk() {
        return this.alj == null ? null : this.alj.asBinder();
    }
}
