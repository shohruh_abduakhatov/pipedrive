package com.pipedrive.tasks.search;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import com.pipedrive.application.Session;
import com.pipedrive.contentproviders.GlobalSearchContentProvider;
import com.pipedrive.datasource.BaseDataSource;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.util.networking.search.DealSearchResultEntity;
import com.pipedrive.util.networking.search.OrganizationSearchResultEntity;
import com.pipedrive.util.networking.search.PersonSearchResultEntity;
import com.pipedrive.util.networking.search.SearchResultEntity;
import com.pipedrive.util.networking.search.SearchResultsResponse;
import com.pipedrive.util.organizations.OrganizationsNetworkingUtil;
import com.pipedrive.util.persons.PersonNetworkingUtil;

class SearchResultsResponseProcessor {
    @NonNull
    private final Session session;

    public SearchResultsResponseProcessor(@NonNull Session session) {
        this.session = session;
    }

    @WorkerThread
    void process(@Nullable SearchResultsResponse response) {
        if (response != null) {
            boolean shouldNotifyContentProvider = false;
            DealsDataSource dealsDataSource = new DealsDataSource(this.session.getDatabase());
            PersonsDataSource personsDataSource = new PersonsDataSource(this.session.getDatabase());
            OrganizationsDataSource organizationsDataSource = new OrganizationsDataSource(this.session.getDatabase());
            for (DealSearchResultEntity dealSearchResultEntity : response.getDealSearchResultEntities()) {
                if (isCreateOrUpdateRequired(dealSearchResultEntity, dealsDataSource).booleanValue() && dealSearchResultEntity.getPipedriveId() != null) {
                    Deal downloadedDeal = DealsNetworkingUtil.downloadDeal(this.session, (long) dealSearchResultEntity.getPipedriveId().intValue());
                    if (downloadedDeal != null) {
                        DealsNetworkingUtil.createOrUpdateDealIntoDBWithRelations(this.session, downloadedDeal);
                        shouldNotifyContentProvider = true;
                    }
                }
            }
            for (PersonSearchResultEntity personSearchResultEntity : response.getPersonSearchResultEntities()) {
                if (isCreateOrUpdateRequired(personSearchResultEntity, personsDataSource).booleanValue() && personSearchResultEntity.getPipedriveId() != null) {
                    Person downloadedPerson = PersonNetworkingUtil.downloadPerson(this.session, personSearchResultEntity.getPipedriveId().longValue());
                    if (downloadedPerson != null) {
                        PersonNetworkingUtil.createOrUpdatePersonIntoDBWithRelations(this.session, downloadedPerson);
                        shouldNotifyContentProvider = true;
                    }
                }
            }
            for (OrganizationSearchResultEntity organizationSearchResultEntity : response.getOrganizationSearchResultEntities()) {
                if (isCreateOrUpdateRequired(organizationSearchResultEntity, organizationsDataSource).booleanValue() && organizationSearchResultEntity.getPipedriveId() != null) {
                    Organization downloadedOrganization = OrganizationsNetworkingUtil.downloadOrganization(this.session, organizationSearchResultEntity.getPipedriveId().longValue());
                    if (downloadedOrganization != null) {
                        OrganizationsNetworkingUtil.createOrUpdateOrganizationIntoDBWithRelations(this.session, downloadedOrganization);
                        shouldNotifyContentProvider = true;
                    }
                }
            }
            if (shouldNotifyContentProvider) {
                GlobalSearchContentProvider.notifyChangesForOrganizationContentProvider(this.session.getApplicationContext());
            }
        }
    }

    @NonNull
    Boolean isCreateOrUpdateRequired(@NonNull SearchResultEntity entity, @NonNull BaseDataSource dataSource) {
        if (entity.getPipedriveId() == null || entity.getName() == null) {
            return Boolean.valueOf(false);
        }
        BaseDatasourceEntity datasourceEntity = dataSource.findByPipedriveId((long) entity.getPipedriveId().intValue());
        if (datasourceEntity == null) {
            return Boolean.valueOf(true);
        }
        return entity.isDifferentFrom(datasourceEntity);
    }
}
