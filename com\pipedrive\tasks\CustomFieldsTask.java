package com.pipedrive.tasks;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.plus.PlusShare;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.CustomFieldsDataSource;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.Response;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CustomFieldsTask extends AsyncTask<String, Integer, Void> {
    public static final String ACTION_CUSTOM_FIELDS_DOWNLOADED = "com.pipedrive.CustomFieldsTask.ACTION_CUSTOM_FIELDS_DOWNLOADED";
    private static final String TAG = CustomFieldsTask.class.getSimpleName();

    public CustomFieldsTask(Session session) {
        super(session);
    }

    protected Void doInBackground(String... params) {
        String tag = TAG + ".doInBackground()";
        Log.d(tag, "Start.");
        downloadAllCustomFields(getSession());
        Log.d(tag, "Done.");
        return null;
    }

    protected void onPostExecute(Void result) {
        Log.d(TAG + ".onPostExecute()", "Done.");
    }

    private static void downloadDealFields(@NonNull Session session) {
        InputStream in = ConnectionUtil.requestGet(new ApiUrlBuilder(session, ApiUrlBuilder.ENDPOINT_DEAL_FIELDS));
        if (in != null) {
            parseCustomFieldsList(session, in, "deal");
            try {
                in.close();
                return;
            } catch (IOException e) {
                return;
            }
        }
        LogJourno.reportEvent(EVENT.Networking_dealFieldsEndpointFailed);
    }

    private static void downloadPersonFields(@NonNull Session session) {
        InputStream in = ConnectionUtil.requestGet(new ApiUrlBuilder(session, ApiUrlBuilder.ENDPOINT_PERSON_FIELDS));
        if (in != null) {
            parseCustomFieldsList(session, in, "person");
            try {
                in.close();
                return;
            } catch (IOException e) {
                return;
            }
        }
        LogJourno.reportEvent(EVENT.Networking_personFieldsEndpointFailed);
    }

    private static void downloadOrganizationFields(@NonNull Session session) {
        InputStream in = ConnectionUtil.requestGet(new ApiUrlBuilder(session, ApiUrlBuilder.ENDPOINT_ORGANIZATION_FIELDS));
        if (in != null) {
            parseCustomFieldsList(session, in, "organization");
            try {
                in.close();
                return;
            } catch (IOException e) {
                return;
            }
        }
        LogJourno.reportEvent(EVENT.Networking_organizationFieldsEndpointFailed);
    }

    public static void downloadAllCustomFields(@NonNull Session session) {
        Log.d(TAG, "Requesting to download deal fields.");
        downloadDealFields(session);
        Log.d(TAG, "Requesting to download person fields.");
        downloadPersonFields(session);
        Log.d(TAG, "Requesting to download organization fields.");
        downloadOrganizationFields(session);
        Log.d(TAG, "Broadcasting done event:com.pipedrive.CustomFieldsTask.ACTION_CUSTOM_FIELDS_DOWNLOADED");
        session.getApplicationContext().sendBroadcast(new Intent(ACTION_CUSTOM_FIELDS_DOWNLOADED));
    }

    @NonNull
    private static List<CustomField> parseCustomFieldsList(@NonNull Session session, @Nullable InputStream in, String fieldType) {
        List<CustomField> customFields = new ArrayList();
        if (in != null) {
            CustomField field;
            try {
                JsonReader reader = new JsonReader(new InputStreamReader(in, HttpRequest.CHARSET_UTF8));
                reader.beginObject();
                while (reader.hasNext()) {
                    String activityListField = reader.nextName();
                    if (Response.JSON_PARAM_SUCCESS.equals(activityListField)) {
                        if (!reader.nextBoolean()) {
                            reader.close();
                        }
                    } else if (activityListField.equals(Response.JSON_PARAM_DATA) && reader.peek() == JsonToken.BEGIN_ARRAY) {
                        reader.beginArray();
                        while (reader.hasNext()) {
                            field = readCustomField(reader);
                            field.setFieldType(fieldType);
                            customFields.add(field);
                        }
                        reader.endArray();
                    } else {
                        reader.skipValue();
                    }
                }
                reader.endObject();
                reader.close();
            } catch (Exception e) {
                LogJourno.reportEvent(EVENT.Networking_parsingCustomFieldsListFailed, e);
                Log.e(new Throwable("Error parsing custom field json", e));
            }
            CustomFieldsDataSource dataSource = new CustomFieldsDataSource(session);
            if (!customFields.isEmpty()) {
                dataSource.deleteAllFields(fieldType);
            }
            for (CustomField field2 : customFields) {
                dataSource.createCustomField(field2);
            }
        }
        return customFields;
    }

    @NonNull
    private static CustomField readCustomField(@NonNull JsonReader reader) throws IOException, JSONException {
        CustomField customField = new CustomField();
        reader.beginObject();
        while (reader.hasNext()) {
            String fieldName = reader.nextName();
            if (PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID.equals(fieldName) && reader.peek() != JsonToken.NULL) {
                customField.setPipedriveId(reader.nextLong());
            } else if ("key".equals(fieldName) && reader.peek() != JsonToken.NULL) {
                customField.setKey(reader.nextString());
            } else if ("name".equals(fieldName) && reader.peek() != JsonToken.NULL) {
                customField.setName(reader.nextString());
            } else if (PipeSQLiteHelper.COLUMN_ORDER_NR.equals(fieldName) && reader.peek() != JsonToken.NULL) {
                customField.setOrderNr(reader.nextInt());
            } else if ("field_type".equals(fieldName) && reader.peek() != JsonToken.NULL) {
                customField.setFieldDataType(reader.nextString());
            } else if ("active_flag".equals(fieldName) && reader.peek() == JsonToken.BOOLEAN) {
                customField.setActive(reader.nextBoolean());
            } else if (!"options".equals(fieldName) || reader.peek() == JsonToken.NULL) {
                reader.skipValue();
            } else {
                customField.setOptions(readOptions(reader));
            }
        }
        reader.endObject();
        return customField;
    }

    private static JSONArray readOptions(JsonReader reader) throws IOException, JSONException {
        JSONArray optionsArr = new JSONArray();
        if (reader.peek() == JsonToken.BEGIN_ARRAY) {
            reader.beginArray();
            while (reader.hasNext()) {
                JSONObject optionsObj = new JSONObject();
                reader.beginObject();
                while (reader.hasNext()) {
                    String optionField = reader.nextName();
                    if (PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID.equals(optionField)) {
                        if (reader.peek() == JsonToken.NUMBER) {
                            optionsObj.put(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, reader.nextInt() + "");
                        } else if (reader.peek() == JsonToken.STRING) {
                            optionsObj.put(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, reader.nextString());
                        }
                    } else if (!PlusShare.KEY_CALL_TO_ACTION_LABEL.equals(optionField) || reader.peek() == JsonToken.NULL) {
                        reader.skipValue();
                    } else {
                        optionsObj.put(PlusShare.KEY_CALL_TO_ACTION_LABEL, reader.nextString());
                    }
                }
                optionsArr.put(optionsObj);
                reader.endObject();
            }
            reader.endArray();
        } else {
            reader.skipValue();
        }
        return optionsArr;
    }
}
