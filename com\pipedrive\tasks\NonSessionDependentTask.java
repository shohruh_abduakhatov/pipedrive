package com.pipedrive.tasks;

import com.pipedrive.application.Session;

public abstract class NonSessionDependentTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {
    public NonSessionDependentTask(Session session) {
        super(session);
    }

    protected boolean isSessionValid() {
        return true;
    }
}
