package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.support.annotation.NonNull;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.internal.zznw.zza;

public class zznu extends zzj<zznw> {
    public zznu(Context context, Looper looper, zzf com_google_android_gms_common_internal_zzf, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 74, com_google_android_gms_common_internal_zzf, connectionCallbacks, onConnectionFailedListener);
    }

    protected zznw zzcb(IBinder iBinder) {
        return zza.zzcd(iBinder);
    }

    protected /* synthetic */ IInterface zzh(IBinder iBinder) {
        return zzcb(iBinder);
    }

    @NonNull
    protected String zzjx() {
        return "com.google.android.gms.auth.api.accountactivationstate.START";
    }

    @NonNull
    protected String zzjy() {
        return "com.google.android.gms.auth.api.accountactivationstate.internal.IAccountActivationStateService";
    }
}
