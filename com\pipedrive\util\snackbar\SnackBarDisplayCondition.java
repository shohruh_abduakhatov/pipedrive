package com.pipedrive.util.snackbar;

public enum SnackBarDisplayCondition {
    IMMEDIATELY,
    AFTER_ACTIVITY_SWITCH
}
