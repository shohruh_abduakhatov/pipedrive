package com.google.android.gms.wearable.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.wearable.Node;

public class NodeParcelable extends AbstractSafeParcelable implements Node {
    public static final Creator<NodeParcelable> CREATOR = new zzbc();
    private final int aUk;
    private final boolean aUl;
    private final String jh;
    final int mVersionCode;
    private final String zzboa;

    NodeParcelable(int i, String str, String str2, int i2, boolean z) {
        this.mVersionCode = i;
        this.zzboa = str;
        this.jh = str2;
        this.aUk = i2;
        this.aUl = z;
    }

    public boolean equals(Object obj) {
        return !(obj instanceof NodeParcelable) ? false : ((NodeParcelable) obj).zzboa.equals(this.zzboa);
    }

    public String getDisplayName() {
        return this.jh;
    }

    public int getHopCount() {
        return this.aUk;
    }

    public String getId() {
        return this.zzboa;
    }

    public int hashCode() {
        return this.zzboa.hashCode();
    }

    public boolean isNearby() {
        return this.aUl;
    }

    public String toString() {
        String str = this.jh;
        String str2 = this.zzboa;
        int i = this.aUk;
        return new StringBuilder((String.valueOf(str).length() + 45) + String.valueOf(str2).length()).append("Node{").append(str).append(", id=").append(str2).append(", hops=").append(i).append(", isNearby=").append(this.aUl).append("}").toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzbc.zza(this, parcel, i);
    }
}
