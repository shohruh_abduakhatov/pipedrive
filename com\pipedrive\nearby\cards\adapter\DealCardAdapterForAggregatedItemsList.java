package com.pipedrive.nearby.cards.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.cards.cards.CardHeader;
import com.pipedrive.nearby.cards.cards.DealCardHeader;
import com.pipedrive.nearby.model.NearbyItem;
import java.util.List;

public class DealCardAdapterForAggregatedItemsList extends CardAdapterForAggregatedItemsList {
    public DealCardAdapterForAggregatedItemsList(@NonNull Session session, @NonNull List<? extends NearbyItem> itemsList) {
        super(session, itemsList);
    }

    AggregatedItemListViewHolder createViewHolder(@NonNull ViewGroup parent) {
        return new AggregatedItemListViewHolder((ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.view_deal_card_header_for_aggregated_list, parent, false));
    }

    CardHeader getCardHeader() {
        return new DealCardHeader();
    }
}
