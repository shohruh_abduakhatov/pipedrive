package rx;

import java.util.Arrays;
import rx.exceptions.CompositeException;
import rx.exceptions.Exceptions;
import rx.functions.Func1;

class Completable$25 implements Completable$OnSubscribe {
    final /* synthetic */ Completable this$0;
    final /* synthetic */ Func1 val$predicate;

    Completable$25(Completable completable, Func1 func1) {
        this.this$0 = completable;
        this.val$predicate = func1;
    }

    public void call(final CompletableSubscriber s) {
        this.this$0.unsafeSubscribe(new CompletableSubscriber() {
            public void onCompleted() {
                s.onCompleted();
            }

            public void onError(Throwable e) {
                boolean b;
                try {
                    b = ((Boolean) Completable$25.this.val$predicate.call(e)).booleanValue();
                } catch (Throwable ex) {
                    Exceptions.throwIfFatal(ex);
                    b = false;
                    e = new CompositeException(Arrays.asList(new Throwable[]{e, ex}));
                }
                if (b) {
                    s.onCompleted();
                } else {
                    s.onError(e);
                }
            }

            public void onSubscribe(Subscription d) {
                s.onSubscribe(d);
            }
        });
    }
}
