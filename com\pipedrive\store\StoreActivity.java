package com.pipedrive.store;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.changes.ActivityChanger;
import com.pipedrive.datetime.ActivityDensityManager;
import com.pipedrive.datetime.PipedriveDate;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.model.Activity;
import com.pipedrive.model.Activity.DateTimeInfo;
import com.pipedrive.notification.UINotificationManager;
import com.pipedrive.util.deals.DealsUtil;

public class StoreActivity extends Store<Activity> {
    public StoreActivity(@NonNull Session session) {
        super(session);
    }

    void cleanupBeforeCreate(@NonNull Activity newActivityBeingStored) {
        if (newActivityBeingStored.getOrganization() != null) {
            new StoreOrganization(getSession()).cleanupBeforeCreate(newActivityBeingStored.getOrganization());
        }
        if (newActivityBeingStored.getPerson() != null) {
            new StorePerson(getSession()).cleanupBeforeCreate(newActivityBeingStored.getPerson());
        }
        if (newActivityBeingStored.getDeal() != null) {
            new StoreDeal(getSession()).cleanupBeforeCreate(newActivityBeingStored.getDeal());
        }
        processBeforeStoringShared(newActivityBeingStored);
    }

    boolean implementCreate(@NonNull Activity newActivity) {
        return new ActivityChanger(getSession()).create(newActivity);
    }

    void postProcessAfterCreate(@NonNull Activity activityStored) {
        postProcessEntityShared(activityStored);
        ActivityDensityManager.getInstance().clearDensityCache();
        UINotificationManager.getInstance().processNotificationForActivity(getSession(), activityStored);
    }

    void cleanupBeforeUpdate(@NonNull Activity updatedActivityBeingStored) {
        if (updatedActivityBeingStored.getOrganization() != null) {
            new StoreOrganization(getSession()).cleanupBeforeUpdate(updatedActivityBeingStored.getOrganization());
        }
        if (updatedActivityBeingStored.getPerson() != null) {
            new StorePerson(getSession()).cleanupBeforeUpdate(updatedActivityBeingStored.getPerson());
        }
        if (updatedActivityBeingStored.getDeal() != null) {
            new StoreDeal(getSession()).cleanupBeforeUpdate(updatedActivityBeingStored.getDeal());
        }
        processBeforeStoringShared(updatedActivityBeingStored);
    }

    boolean implementUpdate(@NonNull Activity updatedActivity) {
        return new ActivityChanger(getSession()).update(updatedActivity);
    }

    void postProcessAfterUpdate(@NonNull Activity activityStored) {
        postProcessEntityShared(activityStored);
        ActivityDensityManager.getInstance().clearDensityCache();
        UINotificationManager.getInstance().processNotificationForActivity(getSession(), activityStored);
    }

    private void processBeforeStoringShared(Activity activityBeingStored) {
        if (activityBeingStored.isDone()) {
            activityBeingStored.request(new DateTimeInfo() {
                public void activityHasStartDateTime(@NonNull PipedriveDateTime dateTime, @NonNull Activity inActivity) {
                    if (dateTime.after(PipedriveDateTime.instanceWithCurrentDateTime())) {
                        inActivity.setActivityStartAt(PipedriveDateTime.instanceWithCurrentDateTime(), inActivity.getDuration());
                    }
                }

                public void activityHasStartDate(@NonNull PipedriveDate date, @NonNull Activity inActivity) {
                    if (date.getRepresentationInUnixTime() > PipedriveDate.instanceWithCurrentDate().getRepresentationInUnixTime()) {
                        inActivity.setActivityStartAt(PipedriveDate.instanceWithCurrentDate(), inActivity.getDuration());
                    }
                }
            });
        }
        if (activityBeingStored.getAssignedUserId() <= 0) {
            activityBeingStored.setAssignedUserId(getSession().getAuthenticatedUserID());
        }
        if (activityBeingStored.getPerson() != null && activityBeingStored.getOrganization() != null && !activityBeingStored.getPerson().isStored() && activityBeingStored.getPerson().getCompany() == null) {
            activityBeingStored.getPerson().setCompany(activityBeingStored.getOrganization());
        }
    }

    private void postProcessEntityShared(Activity activityBeingStored) {
        if (activityBeingStored.getDeal() != null && activityBeingStored.getDeal().isStored()) {
            DealsUtil.recalculateDealNextActivity(getSession(), activityBeingStored.getDeal().getSqlId(), true);
        }
    }
}
