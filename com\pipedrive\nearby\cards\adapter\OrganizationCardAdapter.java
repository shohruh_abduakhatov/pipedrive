package com.pipedrive.nearby.cards.adapter;

import android.support.annotation.NonNull;
import android.view.ViewGroup;
import com.google.android.gms.maps.model.LatLng;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.cards.cards.AggregatedOrganizationsCard;
import com.pipedrive.nearby.cards.cards.Card;
import com.pipedrive.nearby.cards.cards.SingleOrganizationCard;
import com.pipedrive.nearby.model.NearbyItem;
import java.util.List;

public class OrganizationCardAdapter extends CardAdapter {
    public OrganizationCardAdapter(@NonNull Session session, @NonNull List<? extends NearbyItem> itemsList, @NonNull LatLng currentLocation) {
        super(session, itemsList, currentLocation);
    }

    Card createSingleCard(@NonNull ViewGroup parent) {
        return new SingleOrganizationCard(parent, Integer.valueOf(R.layout.view_single_organization_card));
    }

    Card createAggregatedCard(@NonNull ViewGroup parent) {
        return new AggregatedOrganizationsCard(parent, Integer.valueOf(R.layout.view_aggregated_card));
    }
}
