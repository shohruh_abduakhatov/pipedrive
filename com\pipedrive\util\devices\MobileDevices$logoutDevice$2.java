package com.pipedrive.util.devices;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import rx.Completable;
import rx.functions.Func1;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "Lrx/Completable;", "it", "", "kotlin.jvm.PlatformType", "call"}, k = 3, mv = {1, 1, 6})
/* compiled from: MobileDevices.kt */
final class MobileDevices$logoutDevice$2<T, R> implements Func1<String, Completable> {
    final /* synthetic */ MobileDevices this$0;

    MobileDevices$logoutDevice$2(MobileDevices mobileDevices) {
        this.this$0 = mobileDevices;
    }

    @NotNull
    public final Completable call(String it) {
        MobileDevicesApi access$getApi$p = this.this$0.api;
        Intrinsics.checkExpressionValueIsNotNull(it, "it");
        return access$getApi$p.logout(it);
    }
}
