package com.pipedrive.deal;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.Pipeline;
import com.pipedrive.presenter.PersistablePresenter;

abstract class DealEditPresenter extends PersistablePresenter<DealEditView> {
    abstract void associateOrganization(@Nullable Organization organization);

    abstract void associateOrganizationBySqlId(@NonNull Long l);

    abstract void associatePerson(@Nullable Person person);

    abstract void associatePersonBySqlId(@NonNull Long l);

    abstract void changeDeal(@NonNull Deal deal);

    abstract void deleteDeal(@NonNull Long l);

    @Nullable
    abstract Deal getCurrentlyManagedDeal();

    abstract void requestDealProductsUpdate();

    abstract void requestOrRestoreDeal(@NonNull Long l);

    abstract void requestOrRestoreNewDealForOrganization(@NonNull Long l);

    abstract void requestOrRestoreNewDealForPerson(@NonNull Long l);

    abstract void requestOrRestoreNewDealForStage(@NonNull Integer num);

    abstract void requestOrRestoreNewDealWithTitle(@Nullable String str, @Nullable Long l, @Nullable Long l2);

    abstract void setPipeline(Pipeline pipeline);

    abstract boolean wasDealChanged(@NonNull Deal deal);

    DealEditPresenter(@NonNull Session session, @Nullable Bundle savedState) {
        super(session, savedState);
    }
}
