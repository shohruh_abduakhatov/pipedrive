package com.pipedrive.util.organizations;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Organization;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import java.io.IOException;
import org.json.JSONObject;

public enum OrganizationsNetworkingUtil {
    ;

    public static Organization readOrg(JsonReader reader, String addressField) throws IOException {
        return readOrg(reader, addressField, 0, null);
    }

    private static Organization readOrg(JsonReader reader, String addressField, int itemPipedriveId, String name) throws IOException {
        Organization organizationToReturn = new Organization();
        organizationToReturn.setPipedriveId(Long.valueOf((long) itemPipedriveId));
        organizationToReturn.setName(name);
        updateOrg(reader, organizationToReturn, addressField);
        return organizationToReturn;
    }

    private static void updateOrg(JsonReader reader, Organization readInto, String addressField) throws IOException {
        String addressLongitudeField = null;
        if (readInto != null && reader != null) {
            String addressLatitudeField = addressField == null ? null : addressField.concat("_lat");
            if (addressField != null) {
                addressLongitudeField = addressField.concat("_long");
            }
            Organization org = readInto;
            if (reader.peek() == JsonToken.BEGIN_OBJECT) {
                reader.beginObject();
                while (reader.hasNext()) {
                    String orgField = reader.nextName();
                    if (PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID.equals(orgField) && reader.peek() != JsonToken.NULL) {
                        org.setPipedriveId(Long.valueOf(reader.nextLong()));
                    } else if ("name".equals(orgField) && reader.peek() != JsonToken.NULL) {
                        org.setName(reader.nextString());
                    } else if ("phone".equals(orgField) && reader.peek() == JsonToken.STRING) {
                        org.setPhone(reader.nextString());
                    } else if ("e_mail".equals(orgField) && reader.peek() == JsonToken.STRING) {
                        org.setEmail(reader.nextString());
                    } else if ("active_flag".equals(orgField) && reader.peek() == JsonToken.BOOLEAN) {
                        org.setIsActive(reader.nextBoolean());
                    } else if ("update_time".equals(orgField) && reader.peek() != JsonToken.NULL) {
                        org.setUpdateTime(reader.nextString());
                    } else if (addressField == null || !orgField.startsWith(addressField)) {
                        if (PipeSQLiteHelper.COLUMN_VISIBLE_TO.equals(orgField) && reader.peek() == JsonToken.STRING) {
                            try {
                                org.setVisibleTo(Integer.parseInt(reader.nextString()));
                            } catch (NumberFormatException e) {
                            }
                        } else if (PipeSQLiteHelper.COLUMN_OWNER_ID.equals(orgField) && reader.peek() == JsonToken.NUMBER) {
                            org.setOwnerPipedriveId(reader.nextInt());
                        } else if ("cc_email".equals(orgField) && reader.peek() != JsonToken.NULL) {
                            org.setDropBoxAddress(reader.nextString());
                        } else if ("owner_name".equals(orgField) && reader.peek() == JsonToken.STRING) {
                            org.setOwnerName(reader.nextString());
                        } else if (orgField.length() >= 40) {
                            JSONObject customFieldObj = null;
                            if (reader.peek() == JsonToken.NULL) {
                                reader.skipValue();
                            } else {
                                try {
                                    customFieldObj = CustomField.readCustomValuesJsonObject(reader, orgField);
                                } catch (Throwable e2) {
                                    LogJourno.reportEvent(EVENT.JsonParser_orgCustomFieldsNotParsedFromJsonReader, e2);
                                    Log.e(new Throwable("Error parsing custom field", e2));
                                }
                            }
                            if (customFieldObj != null) {
                                org.addToCustomFieldArray(customFieldObj);
                            }
                        } else {
                            reader.skipValue();
                        }
                    } else if (orgField.equals(addressField) && reader.peek() == JsonToken.STRING) {
                        org.setAddress(reader.nextString());
                    } else if (orgField.equals(addressLatitudeField) && reader.peek() == JsonToken.NUMBER) {
                        org.setAddressLatitude(Double.valueOf(reader.nextDouble()));
                    } else if (orgField.equals(addressLongitudeField) && reader.peek() == JsonToken.NUMBER) {
                        org.setAddressLongitude(Double.valueOf(reader.nextDouble()));
                    } else {
                        reader.skipValue();
                    }
                }
                reader.endObject();
            }
        }
    }

    @Nullable
    @WorkerThread
    @Deprecated
    public static Organization downloadOrganization(@NonNull Session session, int organizationPipedriveId) {
        return downloadOrganization(session, Long.valueOf((long) organizationPipedriveId).longValue());
    }

    @Nullable
    @WorkerThread
    public static Organization downloadOrganization(@NonNull Session session, long organizationPipedriveId) {
        boolean cannotDownloadSpecificOrganization = organizationPipedriveId < 0 || ConnectionUtil.isNotConnected(session.getApplicationContext());
        if (cannotDownloadSpecificOrganization) {
            return null;
        }
        ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder(session, "organizations");
        apiUrlBuilder.appendEncodedPath(Long.toString(organizationPipedriveId));
        return ((1OrganizationDetailsRequest) ConnectionUtil.requestGet(apiUrlBuilder, new 1(), new 1OrganizationDetailsRequest())).org;
    }

    @WorkerThread
    public static Organization createOrUpdateOrganizationIntoDBWithRelations(@NonNull Session session, @NonNull Organization organization) {
        long orgSqlId = new OrganizationsDataSource(session.getDatabase()).createOrUpdate((BaseDatasourceEntity) organization);
        if (orgSqlId == -1) {
            return null;
        }
        organization.setSqlIdOrNull(Long.valueOf(orgSqlId));
        return organization;
    }
}
