package com.pipedrive.pipeline;

import android.database.Cursor;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.datasource.PipelineDataSource;
import com.pipedrive.tasks.StoreAwareAsyncTask;
import com.pipedrive.util.StagesNetworkingUtil;
import com.pipedrive.util.networking.ConnectionUtil;

class InvalidateStagesTask extends StoreAwareAsyncTask<Void, Void, Boolean> {
    @Nullable
    private final OnTaskFinished mOnTaskFinished;
    private final long mPipelineId;

    public interface OnTaskFinished {
        @MainThread
        void allStagesDownloaded(@NonNull Session session, long j);

        @MainThread
        void taskNotExecutedDueToOfflineMode(@NonNull Session session, long j);
    }

    InvalidateStagesTask(@NonNull Session session, long pipelineId, @Nullable OnTaskFinished onTaskFinished) {
        super(session);
        this.mPipelineId = pipelineId;
        this.mOnTaskFinished = onTaskFinished;
    }

    protected Boolean doInBackgroundAfterStoreSync(Void... params) {
        if (!ConnectionUtil.isConnected(getSession().getApplicationContext())) {
            return null;
        }
        Cursor pipelineCursor = new PipelineDataSource(getSession().getDatabase()).getPipelinesCursor();
        try {
            pipelineCursor.moveToPosition(-1);
            while (pipelineCursor.moveToNext()) {
                StagesNetworkingUtil.downloadStagesBlocking(getSession(), (long) pipelineCursor.getInt(pipelineCursor.getColumnIndex(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID)));
            }
            return Boolean.valueOf(true);
        } finally {
            pipelineCursor.close();
        }
    }

    protected void onPostExecute(@Nullable Boolean succeeded) {
        boolean isOnTaskFinishedImplemented;
        boolean taskNotExecutedDueToOfflineMode = true;
        if (this.mOnTaskFinished != null) {
            isOnTaskFinishedImplemented = true;
        } else {
            isOnTaskFinishedImplemented = false;
        }
        if (isOnTaskFinishedImplemented) {
            if (succeeded != null) {
                taskNotExecutedDueToOfflineMode = false;
            }
            if (taskNotExecutedDueToOfflineMode) {
                this.mOnTaskFinished.taskNotExecutedDueToOfflineMode(getSession(), this.mPipelineId);
            } else if (succeeded.booleanValue()) {
                this.mOnTaskFinished.allStagesDownloaded(getSession(), this.mPipelineId);
            }
        }
    }
}
