package com.pipedrive.nearby.filter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.FiltersDataSource;
import com.pipedrive.datasource.PipelineDataSource;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Filter;
import com.pipedrive.model.Pipeline;
import com.pipedrive.model.User;
import com.pipedrive.nearby.model.NearbyItemType;
import com.pipedrive.util.UsersUtil;
import java.util.Collections;
import java.util.List;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class NearbyFilterDataRepository {
    @NonNull
    public Observable<List<FilterItemList>> getFilterList(@NonNull Session session, @NonNull NearbyItemType nearbyItemType) {
        return Observable.concat(getPipelineFilterList(session, nearbyItemType), getUserFilterList(session, nearbyItemType), getPrivateFilterList(session, nearbyItemType), getPublicFilterList(session, nearbyItemType)).subscribeOn(Schedulers.computation()).filter(new Func1<FilterItemList, Boolean>() {
            public Boolean call(FilterItemList list) {
                return Boolean.valueOf(list.getSize() > 0);
            }
        }).toList();
    }

    @NonNull
    private Observable<FilterItemList> getPrivateFilterList(@NonNull Session session, @NonNull NearbyItemType nearbyItemType) {
        return getFilterItemListForType(1, R.string.private_flter, session, nearbyItemType);
    }

    @NonNull
    private Observable<FilterItemList> getPublicFilterList(@NonNull Session session, @NonNull NearbyItemType nearbyItemType) {
        return getFilterItemListForType(2, R.string.public_filter, session, nearbyItemType);
    }

    @NonNull
    private Observable<FilterItemList> getPipelineFilterList(@NonNull final Session session, @NonNull NearbyItemType nearbyItemType) {
        if (nearbyItemType == NearbyItemType.DEALS) {
            return getFilterItemListForType(0, R.string.pipeline, session, nearbyItemType).doOnNext(new Action1<FilterItemList>() {
                public void call(FilterItemList pipelineFilterItemList) {
                    pipelineFilterItemList.addItemAsFirstInTheList(NearbyFilterDataRepository.this.createFilterItem(Long.valueOf(-1), Long.valueOf(-1), session.getApplicationContext().getResources().getString(R.string.all_pipelines), 3));
                }
            });
        }
        return Observable.empty();
    }

    @NonNull
    private Observable<FilterItemList> getUserFilterList(@NonNull final Session session, @NonNull NearbyItemType nearbyItemType) {
        return getFilterItemListForType(3, R.string.special_user_name_header_title, session, nearbyItemType).doOnNext(new Action1<FilterItemList>() {
            public void call(FilterItemList userFilterItemList) {
                userFilterItemList.addItemAsFirstInTheList(NearbyFilterDataRepository.this.createFilterItem(Long.valueOf(-1), Long.valueOf(-1), session.getApplicationContext().getResources().getString(R.string.special_user_name_everyone), 3));
            }
        });
    }

    @NonNull
    private Observable<FilterItemList> getFilterItemListForType(final int type, @StringRes final int labelResId, @NonNull final Session session, @NonNull NearbyItemType nearbyItemType) {
        return Observable.from(getList(type, session, nearbyItemType)).filter(getFilteringFunctionForType(type)).map(new Func1<BaseDatasourceEntity, FilterItem>() {
            public FilterItem call(BaseDatasourceEntity filter) {
                return NearbyFilterDataRepository.this.createFilterFromEntityItemForType(type, filter);
            }
        }).toList().map(new Func1<List<FilterItem>, FilterItemList>() {
            public FilterItemList call(List<FilterItem> filterItems) {
                FilterItemList filterItemList = new FilterItemList(labelResId, filterItems);
                filterItemList.setSelectedItemPosition(NearbyFilterDataRepository.this.readSavedFilterFromSession(type, session));
                return filterItemList;
            }
        });
    }

    @Nullable
    private FilterItem createFilterFromEntityItemForType(int type, @NonNull BaseDatasourceEntity entity) {
        switch (type) {
            case 0:
                return createFilterItem(entity.getSqlIdOrNull(), entity.getPipedriveIdOrNull(), ((Pipeline) entity).getName(), type);
            case 1:
            case 2:
                return createFilterItem(entity.getSqlIdOrNull(), entity.getPipedriveIdOrNull(), ((Filter) entity).getName(), type);
            case 3:
                return createFilterItem(entity.getSqlIdOrNull(), entity.getPipedriveIdOrNull(), ((User) entity).getName(), type);
            default:
                return null;
        }
    }

    @NonNull
    private Func1<BaseDatasourceEntity, Boolean> getFilteringFunctionForType(final int type) {
        return new Func1<BaseDatasourceEntity, Boolean>() {
            public Boolean call(BaseDatasourceEntity entity) {
                boolean z = true;
                switch (type) {
                    case 0:
                        return Boolean.valueOf(((Pipeline) entity).isActive());
                    case 1:
                        return ((Filter) entity).isPrivate();
                    case 2:
                        if (((Filter) entity).isPrivate().booleanValue()) {
                            z = false;
                        }
                        return Boolean.valueOf(z);
                    case 3:
                        User user = (User) entity;
                        if (user == null || user.getSqlIdOrNull() == null || user.getName() == null) {
                            z = false;
                        }
                        return Boolean.valueOf(z);
                    default:
                        return Boolean.valueOf(false);
                }
            }
        };
    }

    @NonNull
    private List<? extends BaseDatasourceEntity> getList(int type, @NonNull Session session, @NonNull NearbyItemType nearbyItemType) {
        switch (type) {
            case 0:
                return new PipelineDataSource(session.getDatabase()).getPipelines();
            case 1:
            case 2:
                return new FiltersDataSource(session).findAllActiveByType(getCustomFilterType(nearbyItemType));
            case 3:
                return UsersUtil.getActiveCompanyUsersWithCurrentlyAuthenticatedUserAsFirst(session);
            default:
                return Collections.emptyList();
        }
    }

    @NonNull
    private String getCustomFilterType(@NonNull NearbyItemType nearbyItemType) {
        switch (nearbyItemType) {
            case DEALS:
                return "deals";
            case ORGANIZATIONS:
                return Filter.TYPE_ORGANIZATIONS;
            case PERSONS:
                return Filter.TYPE_PEOPLE;
            default:
                return "";
        }
    }

    @NonNull
    private Long readSavedFilterFromSession(int type, @NonNull Session session) {
        long returnIfNotFoundValue = FilterItemList.UNDEFINED_SELECTED_ITEM_ID.longValue();
        switch (type) {
            case 0:
                return Long.valueOf(session.getPipelineSelectedPipelineId(returnIfNotFoundValue));
            case 1:
            case 2:
                return Long.valueOf(session.getPipelineFilter(returnIfNotFoundValue));
            case 3:
                return Long.valueOf(session.getPipelineUser(returnIfNotFoundValue));
            default:
                return Long.valueOf(returnIfNotFoundValue);
        }
    }

    @Nullable
    private FilterItem createFilterItem(@Nullable Long sqlIdOrNull, @Nullable Long pipedriveIdOrNull, @Nullable String name, int type) {
        if (sqlIdOrNull == null || pipedriveIdOrNull == null || name == null) {
            return null;
        }
        return new FilterItem(sqlIdOrNull, pipedriveIdOrNull, name, type);
    }
}
