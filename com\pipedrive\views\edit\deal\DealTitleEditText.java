package com.pipedrive.views.edit.deal;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import com.pipedrive.R;
import com.pipedrive.model.Deal;
import com.pipedrive.views.common.EditTextWithUnderlineAndLabel.OnValueChangedListener;
import com.pipedrive.views.common.TitleEditText;

public class DealTitleEditText extends TitleEditText {
    public DealTitleEditText(Context context) {
        this(context, null);
    }

    public DealTitleEditText(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DealTitleEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setup(@NonNull Deal deal, @Nullable OnValueChangedListener onValueChangedListener) {
        String dealTitle = deal.getTitle();
        boolean dealHasExplicitTitle = !TextUtils.isEmpty(dealTitle);
        if (TextUtils.isEmpty(((EditText) this.mMainView).getText().toString()) && dealHasExplicitTitle) {
            ((EditText) this.mMainView).setText(dealTitle);
        }
        ((EditText) this.mMainView).setOnFocusChangeListener(new OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (((EditText) DealTitleEditText.this.mMainView).hasFocus()) {
                    DealTitleEditText.this.showLabel();
                    if (DealTitleEditText.this.isNotGeneratedTitle()) {
                        ((EditText) DealTitleEditText.this.mMainView).setHint("");
                    }
                } else if (((EditText) DealTitleEditText.this.mMainView).getText().toString().isEmpty()) {
                    ((EditText) DealTitleEditText.this.mMainView).setHint(DealTitleEditText.this.getGeneratedTitle() == null ? DealTitleEditText.this.getResources().getString(DealTitleEditText.this.getLabelTextResourceId()) : DealTitleEditText.this.getGeneratedTitle());
                    if (DealTitleEditText.this.isNotGeneratedTitle()) {
                        DealTitleEditText.this.hideLabel();
                    }
                }
            }
        });
        String dealTitleBasedOnAssociations = deal.getTitleBasedOnAssociations(getResources());
        if (dealTitleBasedOnAssociations != null) {
            ((EditText) this.mMainView).setHint(dealTitleBasedOnAssociations);
            showLabel();
        } else {
            ((EditText) this.mMainView).setHint(getLabelTextResourceId());
            if (((EditText) this.mMainView).hasFocus()) {
                showLabel();
                ((EditText) this.mMainView).setHint("");
            } else {
                hideLabel();
            }
        }
        setupListener(onValueChangedListener);
    }

    protected int getLabelTextResourceId() {
        return R.string.deal_title;
    }

    @Nullable
    public String getEnteredTitle() {
        return ((EditText) this.mMainView).getText().toString();
    }

    @Nullable
    public String getGeneratedTitle() {
        CharSequence hint = ((EditText) this.mMainView).getHint();
        if ((hint == null) || isNotGeneratedTitle()) {
            return null;
        }
        return hint.toString();
    }

    private boolean isNotGeneratedTitle() {
        String currentHint = ((EditText) this.mMainView).getHint().toString();
        return currentHint.equals(getResources().getString(getLabelTextResourceId())) || currentHint.length() == 0;
    }
}
