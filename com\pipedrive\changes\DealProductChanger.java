package com.pipedrive.changes;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.changes.Change.ChangeOperationType;
import com.pipedrive.datasource.products.DealProductsDataSource;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.products.DealProduct;

public class DealProductChanger extends Changer<DealProduct> {
    public DealProductChanger(@NonNull Session session) {
        super(session, 35, null);
    }

    @Nullable
    Long getChangeMetaData(@NonNull DealProduct dealProduct, @NonNull ChangeOperationType changeOperationType) {
        return Long.valueOf(dealProduct.getSqlId());
    }

    @NonNull
    ChangeResult createChange(@NonNull DealProduct newDealProduct) {
        if (newDealProduct.isMetadataOkForCreate()) {
            return new DealProductsDataSource(getSession().getDatabase()).createOrUpdate((BaseDatasourceEntity) newDealProduct) == -1 ? ChangeResult.FAILED : ChangeResult.SUCCESSFUL;
        } else {
            return ChangeResult.FAILED;
        }
    }

    @NonNull
    ChangeResult updateChange(@NonNull DealProduct existingDealProduct) {
        if (existingDealProduct.isMetadataOkForUpdate()) {
            return new DealProductsDataSource(getSession().getDatabase()).createOrUpdate((BaseDatasourceEntity) existingDealProduct) == -1 ? ChangeResult.FAILED : ChangeResult.SUCCESSFUL;
        } else {
            return ChangeResult.FAILED;
        }
    }

    @NonNull
    ChangeResult deleteChange(@NonNull DealProduct dealProductToDelete) {
        if (dealProductToDelete.isMetadataOkForUpdate()) {
            return new DealProductsDataSource(getSession().getDatabase()).createOrUpdate((BaseDatasourceEntity) dealProductToDelete) == -1 ? ChangeResult.FAILED : ChangeResult.SUCCESSFUL;
        } else {
            return ChangeResult.FAILED;
        }
    }
}
