package com.zendesk.sdk.requests;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.ui.NetworkAwareActionbarActivity;
import com.zendesk.sdk.ui.ToolbarSherlock;
import com.zendesk.sdk.util.UiUtils;

public class RequestActivity extends NetworkAwareActionbarActivity {
    public static final String FRAGMENT_TAG = "request_list_fragment_tag";
    private static final String LOG_TAG = "RequestActivity";
    private ZendeskFeedbackConfiguration configuration;
    private RequestListFragment mRequestListFragment;

    public static void startActivity(Context context, @Nullable ZendeskFeedbackConfiguration configuration) {
        boolean configurationRequiresWrapping = false;
        if (context == null) {
            Logger.e(LOG_TAG, "Context is null, cannot start the Activity.", new Object[0]);
            return;
        }
        Intent intent = new Intent(context, RequestActivity.class);
        if (!(configuration == null || (configuration instanceof WrappedZendeskFeedbackConfiguration))) {
            configurationRequiresWrapping = true;
        }
        if (configurationRequiresWrapping) {
            configuration = new WrappedZendeskFeedbackConfiguration(configuration);
        }
        intent.putExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION, configuration);
        context.startActivity(intent);
    }

    protected void onCreate(Bundle savedInstanceState) {
        boolean hasSuppliedContactConfiguration = true;
        super.onCreate(savedInstanceState);
        if (!ZendeskConfig.INSTANCE.isAuthenticationAvailable()) {
            Logger.w(LOG_TAG, "Cannot show the conversations because no authentication has been set up in the SDK.", new Object[0]);
            finish();
        }
        UiUtils.setThemeIfAttributesAreMissing(this, R.attr.requestListAddIcon);
        setContentView(R.layout.activity_requests);
        ToolbarSherlock.installToolBar(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        if (!(getIntent().hasExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION) && (getIntent().getSerializableExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION) instanceof ZendeskFeedbackConfiguration))) {
            hasSuppliedContactConfiguration = false;
        }
        if (hasSuppliedContactConfiguration) {
            this.configuration = (ZendeskFeedbackConfiguration) getIntent().getSerializableExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION);
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(FRAGMENT_TAG);
        if (fragment == null || !(fragment instanceof RequestListFragment)) {
            this.mRequestListFragment = createFragment();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(R.id.activity_request_fragment, this.mRequestListFragment, FRAGMENT_TAG);
            transaction.commit();
            return;
        }
        this.mRequestListFragment = (RequestListFragment) fragment;
    }

    public void onNetworkAvailable() {
        super.onNetworkAvailable();
        if (this.mRequestListFragment != null) {
            Logger.d(LOG_TAG, "Dispatching onNetworkAvailable() to current fragment.", new Object[0]);
            this.mRequestListFragment.onNetworkAvailable();
        }
    }

    public void onNetworkUnavailable() {
        super.onNetworkUnavailable();
        if (this.mRequestListFragment != null) {
            Logger.d(LOG_TAG, "Dispatching onNetworkUnavailable() to current fragment.", new Object[0]);
            this.mRequestListFragment.onNetworkUnavailable();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_request_list_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.activity_request_list_add_icon) {
            ContactZendeskActivity.startActivity(this, this.configuration);
            return true;
        } else if (item.getItemId() != 16908332) {
            return super.onOptionsItemSelected(item);
        } else {
            onBackPressed();
            return true;
        }
    }

    private RequestListFragment createFragment() {
        RequestListFragment requestListFragment = new RequestListFragment();
        requestListFragment.setRetainInstance(true);
        return requestListFragment;
    }
}
