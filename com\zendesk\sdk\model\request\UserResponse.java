package com.zendesk.sdk.model.request;

import android.support.annotation.Nullable;

public class UserResponse {
    private User user;

    @Nullable
    public User getUser() {
        return this.user;
    }
}
