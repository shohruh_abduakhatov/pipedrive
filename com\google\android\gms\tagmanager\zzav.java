package com.google.android.gms.tagmanager;

import android.content.Context;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.concurrent.LinkedBlockingQueue;

class zzav extends Thread implements zzau {
    private static zzav aFv;
    private final LinkedBlockingQueue<Runnable> aFu = new LinkedBlockingQueue();
    private volatile zzaw aFw;
    private volatile boolean mClosed = false;
    private final Context mContext;
    private volatile boolean zzcbf = false;

    private zzav(Context context) {
        super("GAThread");
        if (context != null) {
            this.mContext = context.getApplicationContext();
        } else {
            this.mContext = context;
        }
        start();
    }

    static zzav zzee(Context context) {
        if (aFv == null) {
            aFv = new zzav(context);
        }
        return aFv;
    }

    private String zzg(Throwable th) {
        OutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(byteArrayOutputStream);
        th.printStackTrace(printStream);
        printStream.flush();
        return new String(byteArrayOutputStream.toByteArray());
    }

    public void run() {
        while (!this.mClosed) {
            try {
                Runnable runnable = (Runnable) this.aFu.take();
                if (!this.zzcbf) {
                    runnable.run();
                }
            } catch (InterruptedException e) {
                zzbo.zzdh(e.toString());
            } catch (Throwable th) {
                String str = "Error on Google TagManager Thread: ";
                String valueOf = String.valueOf(zzg(th));
                zzbo.e(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                zzbo.e("Google TagManager is shutting down.");
                this.zzcbf = true;
            }
        }
    }

    void zzl(String str, long j) {
        final zzav com_google_android_gms_tagmanager_zzav = this;
        final long j2 = j;
        final String str2 = str;
        zzp(new Runnable(this) {
            final /* synthetic */ zzav aFz;

            public void run() {
                if (this.aFz.aFw == null) {
                    zzdc zzcgt = zzdc.zzcgt();
                    zzcgt.zza(this.aFz.mContext, com_google_android_gms_tagmanager_zzav);
                    this.aFz.aFw = zzcgt.zzcgw();
                }
                this.aFz.aFw.zzg(j2, str2);
            }
        });
    }

    public void zzp(Runnable runnable) {
        this.aFu.add(runnable);
    }

    public void zzpk(String str) {
        zzl(str, System.currentTimeMillis());
    }
}
