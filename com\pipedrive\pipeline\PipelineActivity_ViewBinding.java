package com.pipedrive.pipeline;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;
import com.pipedrive.views.filter.SelectedFilterLabel;
import com.pipedrive.views.stageindicator.StageIndicatorView;

public class PipelineActivity_ViewBinding implements Unbinder {
    private PipelineActivity target;
    private View view2131820590;
    private View view2131820803;

    @UiThread
    public PipelineActivity_ViewBinding(PipelineActivity target) {
        this(target, target.getWindow().getDecorView());
    }

    @UiThread
    public PipelineActivity_ViewBinding(final PipelineActivity target, View source) {
        this.target = target;
        View view = Utils.findRequiredView(source, R.id.pipeline_spinner, "field 'mPipelineSpinner' and method 'onPipelineSelected'");
        target.mPipelineSpinner = (Spinner) Utils.castView(view, R.id.pipeline_spinner, "field 'mPipelineSpinner'", Spinner.class);
        this.view2131820803 = view;
        ((AdapterView) view).setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> p0, View p1, int p2, long p3) {
                target.onPipelineSelected(p0, p1, p2, p3);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        target.mPipelineProgress = Utils.findRequiredView(source, R.id.pipeline_progress, "field 'mPipelineProgress'");
        target.mPager = (ViewPager) Utils.findRequiredViewAsType(source, R.id.pager, "field 'mPager'", ViewPager.class);
        target.mLineStageIndicator = (StageIndicatorView) Utils.findRequiredViewAsType(source, R.id.indicator, "field 'mLineStageIndicator'", StageIndicatorView.class);
        view = Utils.findRequiredView(source, R.id.add, "field 'mAddButton' and method 'onAddClicked'");
        target.mAddButton = view;
        this.view2131820590 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onAddClicked();
            }
        });
        target.mStagesContent = Utils.findRequiredView(source, R.id.stages_content, "field 'mStagesContent'");
        target.mStagesProgress = Utils.findRequiredView(source, R.id.stages_progress, "field 'mStagesProgress'");
        target.mSelectedFilterLabel = (SelectedFilterLabel) Utils.findRequiredViewAsType(source, R.id.pipelineSelectedFilterLabel, "field 'mSelectedFilterLabel'", SelectedFilterLabel.class);
        target.mMessage = (TextView) Utils.findRequiredViewAsType(source, R.id.pipeline_message, "field 'mMessage'", TextView.class);
        target.mSeparator = Utils.findRequiredView(source, R.id.separator, "field 'mSeparator'");
    }

    @CallSuper
    public void unbind() {
        PipelineActivity target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mPipelineSpinner = null;
        target.mPipelineProgress = null;
        target.mPager = null;
        target.mLineStageIndicator = null;
        target.mAddButton = null;
        target.mStagesContent = null;
        target.mStagesProgress = null;
        target.mSelectedFilterLabel = null;
        target.mMessage = null;
        target.mSeparator = null;
        ((AdapterView) this.view2131820803).setOnItemSelectedListener(null);
        this.view2131820803 = null;
        this.view2131820590.setOnClickListener(null);
        this.view2131820590 = null;
    }
}
