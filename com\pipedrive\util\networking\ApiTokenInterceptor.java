package com.pipedrive.util.networking;

import com.newrelic.agent.android.instrumentation.okhttp3.OkHttp3Instrumentation;
import com.pipedrive.application.Session;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Interceptor;
import okhttp3.Interceptor.Chain;
import okhttp3.Request.Builder;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016R\u000e\u0010\u0005\u001a\u00020\u0006XD¢\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b¨\u0006\r"}, d2 = {"Lcom/pipedrive/util/networking/ApiTokenInterceptor;", "Lokhttp3/Interceptor;", "session", "Lcom/pipedrive/application/Session;", "(Lcom/pipedrive/application/Session;)V", "PARAM_API_TOKEN", "", "getSession", "()Lcom/pipedrive/application/Session;", "intercept", "Lokhttp3/Response;", "chain", "Lokhttp3/Interceptor$Chain;", "pipedrive_prodRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: ApiTokenInterceptor.kt */
public final class ApiTokenInterceptor implements Interceptor {
    private final String PARAM_API_TOKEN = "api_token";
    @NotNull
    private final Session session;

    public ApiTokenInterceptor(@NotNull Session session) {
        Intrinsics.checkParameterIsNotNull(session, SettingsJsonConstants.SESSION_KEY);
        this.session = session;
    }

    @NotNull
    public final Session getSession() {
        return this.session;
    }

    @NotNull
    public Response intercept(@NotNull Chain chain) {
        Intrinsics.checkParameterIsNotNull(chain, "chain");
        CharSequence queryParameter = chain.request().url().queryParameter(this.PARAM_API_TOKEN);
        Object obj = (queryParameter == null || queryParameter.length() == 0) ? 1 : null;
        if (obj != null) {
            Builder url = chain.request().newBuilder().url(chain.request().url().newBuilder().addEncodedQueryParameter(this.PARAM_API_TOKEN, this.session.getApiToken()).build());
            Response proceed = chain.proceed(!(url instanceof Builder) ? url.build() : OkHttp3Instrumentation.build(url));
            Intrinsics.checkExpressionValueIsNotNull(proceed, "chain.proceed(newRequest)");
            return proceed;
        }
        proceed = chain.proceed(chain.request());
        Intrinsics.checkExpressionValueIsNotNull(proceed, "chain.proceed(chain.request())");
        return proceed;
    }
}
