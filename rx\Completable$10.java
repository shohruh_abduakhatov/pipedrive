package rx;

class Completable$10 implements Completable$OnSubscribe {
    final /* synthetic */ Observable val$flowable;

    Completable$10(Observable observable) {
        this.val$flowable = observable;
    }

    public void call(final CompletableSubscriber cs) {
        Subscriber<Object> subscriber = new Subscriber<Object>() {
            public void onCompleted() {
                cs.onCompleted();
            }

            public void onError(Throwable t) {
                cs.onError(t);
            }

            public void onNext(Object t) {
            }
        };
        cs.onSubscribe(subscriber);
        this.val$flowable.unsafeSubscribe(subscriber);
    }
}
