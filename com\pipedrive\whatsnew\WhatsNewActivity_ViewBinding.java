package com.pipedrive.whatsnew;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;
import com.viewpagerindicator.CirclePageIndicator;

public class WhatsNewActivity_ViewBinding implements Unbinder {
    private WhatsNewActivity target;
    private View view2131820843;
    private View view2131820844;
    private View view2131820846;

    @UiThread
    public WhatsNewActivity_ViewBinding(WhatsNewActivity target) {
        this(target, target.getWindow().getDecorView());
    }

    @UiThread
    public WhatsNewActivity_ViewBinding(final WhatsNewActivity target, View source) {
        this.target = target;
        target.mWhatsNewPager = (ViewPager) Utils.findRequiredViewAsType(source, R.id.whatsNewPager, "field 'mWhatsNewPager'", ViewPager.class);
        target.mWhatsNewNavigator = Utils.findRequiredView(source, R.id.whatsNewNavigator, "field 'mWhatsNewNavigator'");
        target.mWhatsNewNavigatorForLastSlide = Utils.findRequiredView(source, R.id.whatsNewNavigatorForLastSlide, "field 'mWhatsNewNavigatorForLastSlide'");
        target.mCirclePageIndicator = (CirclePageIndicator) Utils.findRequiredViewAsType(source, R.id.whatsNewPageLocationIndicator, "field 'mCirclePageIndicator'", CirclePageIndicator.class);
        View view = Utils.findRequiredView(source, R.id.whatsNewNext, "field 'mWhatsNewNext' and method 'onWhatsNewNextClicked'");
        target.mWhatsNewNext = (ImageButton) Utils.castView(view, R.id.whatsNewNext, "field 'mWhatsNewNext'", ImageButton.class);
        this.view2131820844 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onWhatsNewNextClicked();
            }
        });
        view = Utils.findRequiredView(source, R.id.whatsNewSkip, "method 'onWhatsNewSkipClicked'");
        this.view2131820843 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onWhatsNewSkipClicked();
            }
        });
        view = Utils.findRequiredView(source, R.id.whatsNewOk, "method 'onWhatsNewOkClicked'");
        this.view2131820846 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onWhatsNewOkClicked();
            }
        });
    }

    @CallSuper
    public void unbind() {
        WhatsNewActivity target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mWhatsNewPager = null;
        target.mWhatsNewNavigator = null;
        target.mWhatsNewNavigatorForLastSlide = null;
        target.mCirclePageIndicator = null;
        target.mWhatsNewNext = null;
        this.view2131820844.setOnClickListener(null);
        this.view2131820844 = null;
        this.view2131820843.setOnClickListener(null);
        this.view2131820843 = null;
        this.view2131820846.setOnClickListener(null);
        this.view2131820846 = null;
    }
}
