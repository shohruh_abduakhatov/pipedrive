package com.zendesk.sdk.rating.impl;

import com.zendesk.sdk.rating.RateMyAppRule;
import com.zendesk.util.CollectionUtils;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RateMyAppRules {
    private List<RateMyAppRule> mRules;

    public RateMyAppRules(RateMyAppRule... rules) {
        if (CollectionUtils.isEmpty(rules)) {
            this.mRules = Collections.emptyList();
        }
        this.mRules = Arrays.asList(rules);
    }

    public boolean checkRules() {
        for (RateMyAppRule rule : this.mRules) {
            if (rule != null && !rule.permitsShowOfDialog()) {
                return false;
            }
        }
        return true;
    }
}
