package com.pipedrive.adapter.filter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.pipedrive.R;
import com.pipedrive.model.ActivityType;
import com.pipedrive.model.User;
import com.pipedrive.util.TimeFilter;
import com.pipedrive.views.profilepicture.ProfilePictureRoundUser;
import java.util.ArrayList;
import java.util.List;

public class FilterContentActivityAdapter extends BaseAdapter {
    private static final int TYPE_COUNT = 4;
    public static final int TYPE_FILTER_ACTIVITY_TYPE = 0;
    public static final int TYPE_FILTER_TIME = 1;
    public static final int TYPE_FILTER_USER = 3;
    private static final int TYPE_HEADER = 2;
    @NonNull
    private final List<Item> mAdapterItems;

    public class Item {
        @Nullable
        public final ActivityType mActivityType;
        @Nullable
        private final String mSectionHeaderLabel;
        @Nullable
        public final Integer mTimeFilterTimeId;
        @Nullable
        public final User mUser;

        private Item(@Nullable String sectionHeaderLabel) {
            this.mTimeFilterTimeId = null;
            this.mActivityType = null;
            this.mSectionHeaderLabel = sectionHeaderLabel;
            this.mUser = null;
        }

        private Item(@Nullable ActivityType activityType) {
            this.mActivityType = activityType;
            this.mTimeFilterTimeId = null;
            this.mSectionHeaderLabel = null;
            this.mUser = null;
        }

        private Item(@Nullable Integer timeFilterTimeId) {
            this.mTimeFilterTimeId = timeFilterTimeId;
            this.mActivityType = null;
            this.mSectionHeaderLabel = null;
            this.mUser = null;
        }

        private Item(@Nullable User user) {
            this.mTimeFilterTimeId = null;
            this.mActivityType = null;
            this.mSectionHeaderLabel = null;
            this.mUser = user;
        }

        public String toString() {
            return "Item{mActivityType=" + this.mActivityType + ", mTimeFilterTimeId=" + this.mTimeFilterTimeId + ", mUser=" + this.mUser + ", mSectionHeaderLabel='" + this.mSectionHeaderLabel + '\'' + '}';
        }
    }

    public FilterContentActivityAdapter(@NonNull Context context, @Nullable List<ActivityType> activityTypes, @Nullable int[] timeFilterTimeIds, List<User> users) {
        if (activityTypes == null) {
            activityTypes = new ArrayList();
        }
        if (timeFilterTimeIds == null) {
            timeFilterTimeIds = new int[0];
        }
        boolean addHeaderForActivityTypesFilterItems = !activityTypes.isEmpty();
        boolean addHeaderForTimeFilterItems = timeFilterTimeIds.length > 0;
        boolean addHeaderForUserItems = !users.isEmpty();
        this.mAdapterItems = new ArrayList(((addHeaderForUserItems ? 1 : 0) + (timeFilterTimeIds.length + ((addHeaderForTimeFilterItems ? 1 : 0) + (activityTypes.size() + (addHeaderForActivityTypesFilterItems ? 1 : 0))))) + users.size());
        if (addHeaderForActivityTypesFilterItems) {
            this.mAdapterItems.add(new Item(context.getResources().getString(R.string.activity_type)));
        }
        for (ActivityType activityType : activityTypes) {
            this.mAdapterItems.add(new Item(activityType));
        }
        if (addHeaderForTimeFilterItems) {
            this.mAdapterItems.add(new Item(context.getResources().getString(R.string.time)));
        }
        for (int timeFilterTimeId : timeFilterTimeIds) {
            this.mAdapterItems.add(new Item(Integer.valueOf(timeFilterTimeId)));
        }
        if (addHeaderForUserItems) {
            this.mAdapterItems.add(new Item(context.getResources().getString(R.string.special_user_name_header_title)));
        }
        for (User user : users) {
            this.mAdapterItems.add(new Item(user));
        }
    }

    public int getCount() {
        return this.mAdapterItems.size();
    }

    @Nullable
    public Item getItem(int position) {
        if (position < this.mAdapterItems.size()) {
            return (Item) this.mAdapterItems.get(position);
        }
        return null;
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public int getViewTypeCount() {
        return 4;
    }

    public int getItemViewType(int position) {
        boolean itemNotFoundInPosition;
        Item item = getItem(position);
        if (item == null) {
            itemNotFoundInPosition = true;
        } else {
            itemNotFoundInPosition = false;
        }
        if (itemNotFoundInPosition) {
            return super.getItemViewType(position);
        }
        boolean foundTypeFilterActivityType;
        boolean foundTypeFilterTime;
        boolean foundTypeHeader;
        boolean foundUser;
        if (item.mActivityType != null) {
            foundTypeFilterActivityType = true;
        } else {
            foundTypeFilterActivityType = false;
        }
        if (item.mTimeFilterTimeId != null) {
            foundTypeFilterTime = true;
        } else {
            foundTypeFilterTime = false;
        }
        if (item.mSectionHeaderLabel != null) {
            foundTypeHeader = true;
        } else {
            foundTypeHeader = false;
        }
        if (item.mUser != null) {
            foundUser = true;
        } else {
            foundUser = false;
        }
        if (foundTypeFilterActivityType) {
            return 0;
        }
        if (foundTypeFilterTime) {
            return 1;
        }
        if (foundTypeHeader) {
            return 2;
        }
        if (foundUser) {
            return 3;
        }
        return -1;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        boolean returnActivityTypeRow;
        boolean returnTimeTypeRow;
        boolean returnUserRow;
        boolean returnHeaderRow;
        boolean noReusableViewProvided = true;
        int itemViewType = getItemViewType(position);
        Item item = getItem(position);
        if (itemViewType != 0 || item == null || item.mActivityType == null) {
            returnActivityTypeRow = false;
        } else {
            returnActivityTypeRow = true;
        }
        if (itemViewType != 1 || item == null || item.mTimeFilterTimeId == null) {
            returnTimeTypeRow = false;
        } else {
            returnTimeTypeRow = true;
        }
        if (itemViewType != 3 || item == null || item.mUser == null) {
            returnUserRow = false;
        } else {
            returnUserRow = true;
        }
        if (itemViewType != 2 || item == null || item.mSectionHeaderLabel == null) {
            returnHeaderRow = false;
        } else {
            returnHeaderRow = true;
        }
        if (convertView != null) {
            noReusableViewProvided = false;
        }
        if (noReusableViewProvided) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(returnHeaderRow ? R.layout.row_filter_header : R.layout.row_filter_content, parent, false);
        }
        if (returnActivityTypeRow) {
            fillRowOfActivityTypeFilter(convertView, item.mActivityType);
        } else if (returnTimeTypeRow) {
            fillRowOfTimeFilter(convertView, item.mTimeFilterTimeId);
        } else if (returnUserRow) {
            fillRowOfUser(convertView, item.mUser);
        } else if (returnHeaderRow) {
            fillRowOfHeader(convertView, item.mSectionHeaderLabel);
        }
        return convertView;
    }

    private void inflatePictureIfNotFound(View convertView) {
        if (((ProfilePictureRoundUser) ButterKnife.findById(convertView, (int) R.id.profilePicture)) == null) {
            ViewStub profilePictureViewStub = (ViewStub) ButterKnife.findById(convertView, (int) R.id.profilePictureViewStub);
            if (profilePictureViewStub != null) {
                profilePictureViewStub.inflate();
            }
        }
    }

    private void fillRowOfActivityTypeFilter(@NonNull View layoutToFill, @NonNull ActivityType data) {
        boolean showActivityTypeIcon;
        int i = 0;
        setLabelText(layoutToFill, data.getLabel());
        if (data.getId() >= 0) {
            showActivityTypeIcon = true;
        } else {
            showActivityTypeIcon = false;
        }
        ImageView icon = (ImageView) ButterKnife.findById(layoutToFill, (int) R.id.icon);
        if (!showActivityTypeIcon) {
            i = 4;
        }
        icon.setVisibility(i);
        if (showActivityTypeIcon) {
            icon.setImageResource(data.getImageResourceId());
        }
    }

    private void fillRowOfTimeFilter(@NonNull View layoutToFill, @NonNull Integer timeFilterTimeId) {
        ((ImageView) ButterKnife.findById(layoutToFill, (int) R.id.icon)).setVisibility(4);
        setLabelText(layoutToFill, TimeFilter.getLabelById(layoutToFill.getContext(), timeFilterTimeId.intValue()));
    }

    private void fillRowOfHeader(@NonNull View layoutToFill, @NonNull String sectionHeaderLabel) {
        setLabelText(layoutToFill, sectionHeaderLabel);
    }

    private void fillRowOfUser(@NonNull View layoutToFill, @NonNull User user) {
        inflatePictureIfNotFound(layoutToFill);
        ProfilePictureRoundUser profilePictureRoundUser = (ProfilePictureRoundUser) ButterKnife.findById(layoutToFill, (int) R.id.profilePicture);
        if (profilePictureRoundUser != null) {
            if (user.getPipedriveIdOrNull() == null) {
                profilePictureRoundUser.setVisibility(4);
            } else {
                profilePictureRoundUser.setVisibility(0);
                profilePictureRoundUser.loadPicture(user);
            }
        }
        setLabelText(layoutToFill, user.getLabel());
    }

    private void setLabelText(@NonNull View layoutToFill, String labelText) {
        ((TextView) ButterKnife.findById(layoutToFill, (int) R.id.label)).setText(labelText);
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isEnabled(int position) {
        int itemViewType = getItemViewType(position);
        if (itemViewType == 0 || itemViewType == 1 || itemViewType == 3) {
            return true;
        }
        return false;
    }
}
