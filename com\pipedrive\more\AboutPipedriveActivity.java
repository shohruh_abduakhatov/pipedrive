package com.pipedrive.more;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.pipedrive.BaseActivity;
import com.pipedrive.R;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.ScreensMapper;

public class AboutPipedriveActivity extends BaseActivity {
    public static void startActivity(@NonNull Activity activity) {
        ContextCompat.startActivity(activity, new Intent(activity, AboutPipedriveActivity.class), ActivityOptionsCompat.makeBasic().toBundle());
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_about_pipedrive);
        ButterKnife.bind((Activity) this);
    }

    @OnClick({2131820695})
    void onPolicyClick() {
        Analytics.hitScreen(ScreensMapper.SCREEN_NAME_PRIVACY_POLICY, this);
        HtmlOrFileLoadActivity.startActivityForPrivacyPolicy(this);
    }

    @OnClick({2131820696})
    void onTermsClick() {
        Analytics.hitScreen(ScreensMapper.SCREEN_NAME_TERMS_OF_SERVICE, this);
        HtmlOrFileLoadActivity.startActivityForTermsOfService(this);
    }

    @OnClick({2131820698})
    void onBetaTestingClick() {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/apps/testing/com.pipedrive"));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    @OnClick({2131820697})
    void onChangelogClick() {
        Analytics.hitScreen(ScreensMapper.SCREEN_NAME_CHANGELOG, this);
        HtmlOrFileLoadActivity.startActivityForChangelog(this);
    }

    @OnClick({2131820694})
    void onLicencesClick() {
        Analytics.hitScreen(ScreensMapper.SCREEN_NAME_OPEN_SOURCE_LICENSES, this);
        HtmlOrFileLoadActivity.startActivityForOpenSourceLicences(this);
    }
}
