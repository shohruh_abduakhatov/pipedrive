package com.zendesk.sdk.model.helpcenter;

import android.support.annotation.Nullable;

public class LastSearch {
    private final String origin = "mobile_sdk";
    private final String query;
    private final int resultsCount;

    public LastSearch(String query, int resultCount) {
        this.query = query;
        this.resultsCount = resultCount;
    }

    @Nullable
    public String getQuery() {
        return this.query;
    }

    public int getResultsCount() {
        return this.resultsCount;
    }
}
