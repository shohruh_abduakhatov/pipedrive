package com.pipedrive.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.newrelic.agent.android.instrumentation.JSONArrayInstrumentation;
import com.newrelic.agent.android.instrumentation.JSONObjectInstrumentation;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.model.contact.CommunicationMedium;
import com.pipedrive.model.contact.Email;
import com.pipedrive.model.contact.Im;
import com.pipedrive.model.contact.Phone;
import com.pipedrive.model.delta.ModelProperty;
import com.pipedrive.util.CustomFieldsUtil;
import com.pipedrive.util.StringUtils;
import com.pipedrive.util.clone.Cloner;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Person extends Contact<Person> implements AssociatedDatasourceEntity {
    public static final Creator CREATOR = new Creator() {
        public Person createFromParcel(Parcel in) {
            return new Person(in);
        }

        public Person[] newArray(int size) {
            return new Person[size];
        }
    };
    @ModelProperty
    private Organization company;
    @ModelProperty
    @NonNull
    private final List<Email> emails = new ArrayList();
    @NonNull
    private final List<Im> ims = new ArrayList();
    @ModelProperty
    @NonNull
    private final List<Phone> phones = new ArrayList();
    @Nullable
    private Integer pictureId;
    private String postalAddress;

    public Person(Parcel in) {
        readFromParcel(in);
    }

    @Nullable
    public String getPhone() {
        return !this.phones.isEmpty() ? ((Phone) this.phones.get(0)).getPhone() : null;
    }

    @Nullable
    public String getEmail() {
        return !this.emails.isEmpty() ? ((Email) this.emails.get(0)).getEmail() : null;
    }

    @Deprecated
    public int getUserId() {
        return getPipedriveId();
    }

    public boolean hasPhones() {
        return !this.phones.isEmpty();
    }

    public int describeContents() {
        return 0;
    }

    @Deprecated
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(getCompany(), flags);
        dest.writeString(getPhonesJsonString());
        dest.writeString(getEmailsJsonString());
        dest.writeString(getImsJsonString());
        dest.writeString(this.postalAddress);
        dest.writeInt(this.pictureId == null ? 0 : this.pictureId.intValue());
    }

    @Deprecated
    protected void readFromParcel(Parcel in) {
        super.readFromParcel(in);
        this.company = (Organization) in.readParcelable(Organization.class.getClassLoader());
        setPhones(in.readString());
        setEmails(in.readString());
        setIms(in.readString());
        setPostalAddress(in.readString());
        this.pictureId = in.readInt() > 0 ? this.pictureId : null;
    }

    @NonNull
    public String getPhonesJsonString() {
        return CommunicationMedium.toJsonArrayString(this.phones);
    }

    @NonNull
    public List<Phone> getPhones() {
        return this.phones;
    }

    public boolean hasMultiplePhones() {
        return this.phones.size() > 1;
    }

    @NonNull
    public List<Im> getIms() {
        return this.ims;
    }

    public void setPhones(@Nullable String phonesJsonString) {
        this.phones.clear();
        try {
            JSONArray jsonArray = JSONArrayInstrumentation.init(phonesJsonString);
            for (int i = 0; i < jsonArray.length(); i++) {
                Phone phone = new Phone(jsonArray.optJSONObject(i));
                if (!StringUtils.isTrimmedAndEmpty(phone.getPhone())) {
                    this.phones.add(phone);
                }
            }
        } catch (JSONException e) {
        }
    }

    public void setIms(@Nullable String imsJsonString) {
        this.ims.clear();
        try {
            JSONArray jsonArray = JSONArrayInstrumentation.init(imsJsonString);
            for (int i = 0; i < jsonArray.length(); i++) {
                Im im = new Im(jsonArray.optJSONObject(i));
                if (!StringUtils.isTrimmedAndEmpty(im.getValue())) {
                    this.ims.add(im);
                }
            }
        } catch (JSONException e) {
        }
    }

    @NonNull
    public String getEmailsJsonString() {
        return CommunicationMedium.toJsonArrayString(this.emails);
    }

    @NonNull
    public List<Email> getEmails() {
        return this.emails;
    }

    public boolean hasEmails() {
        return !this.emails.isEmpty();
    }

    public void setEmails(@Nullable String emailsJson) {
        this.emails.clear();
        try {
            JSONArray jsonArray = JSONArrayInstrumentation.init(emailsJson);
            for (int i = 0; i < jsonArray.length(); i++) {
                Email email = new Email(jsonArray.optJSONObject(i));
                if (!StringUtils.isTrimmedAndEmpty(email.getEmail())) {
                    this.emails.add(email);
                }
            }
        } catch (JSONException e) {
        }
    }

    public Organization getCompany() {
        return this.company;
    }

    public void setCompany(Organization company) {
        this.company = company;
    }

    public final boolean isEqualToPerson(Session session, Person person) {
        if (person == null) {
            return false;
        }
        try {
            Object jSONObjectInstrumentation;
            CharSequence jSONObject;
            boolean z;
            JSONObject json = person.getJSON();
            if (json instanceof JSONObject) {
                jSONObjectInstrumentation = JSONObjectInstrumentation.toString(json);
            } else {
                jSONObject = json.toString();
            }
            json = getJSON();
            if (TextUtils.equals(jSONObject, !(json instanceof JSONObject) ? json.toString() : JSONObjectInstrumentation.toString(json))) {
                json = person.getCompany().getJSON(session);
                if (json instanceof JSONObject) {
                    jSONObjectInstrumentation = JSONObjectInstrumentation.toString(json);
                } else {
                    jSONObject = json.toString();
                }
                json = getCompany().getJSON(session);
                if (TextUtils.equals(jSONObject, !(json instanceof JSONObject) ? json.toString() : JSONObjectInstrumentation.toString(json))) {
                    z = true;
                    return z;
                }
            }
            z = false;
            return z;
        } catch (Exception e) {
            return false;
        }
    }

    public final JSONObject getJSON() throws JSONException {
        JSONObject personJson = new JSONObject();
        super.appendJSONParams(personJson);
        String str = "org_id";
        Object valueOf = (getCompany() == null || !getCompany().isExisting()) ? JSONObject.NULL : Integer.valueOf(getCompany().getPipedriveId());
        personJson.put(str, valueOf);
        personJson.put("phone", JSONArrayInstrumentation.init(getPhonesJsonString()));
        personJson.put("email", JSONArrayInstrumentation.init(getEmailsJsonString()));
        setCustomFields(CustomFieldsUtil.updateOrgContactReferencesInCustomFields(getCustomFields(), PipedriveApp.getActiveSession(), "person"));
        CustomFieldsUtil.addCustomFieldsToObject(getCustomFields(), personJson, "person");
        return personJson;
    }

    public Person getDeepCopy() {
        Person thisCloned = (Person) Cloner.cloneParcelable(this);
        if (this.company != null) {
            thisCloned.company = this.company.getDeepCopy();
        }
        thisCloned.setPhones(getPhonesJsonString());
        thisCloned.setEmails(getEmailsJsonString());
        thisCloned.setIms(getImsJsonString());
        return thisCloned;
    }

    public boolean isAllAssociationsExisting() {
        if (getCompany() == null || getCompany().isExisting()) {
            return true;
        }
        return false;
    }

    public String toString() {
        try {
            String str = "Person (%s) SQL id=%s; [pipedriveId=%s; name=%s; firstLetter=%s; updateTime=%s; addTime=%s; ownerPipedriveId=%s; visibleTo=%s; getPhonesJsonString()=%s; getEmailsJsonString()=%s; [json: %s]][%s]]";
            Object[] objArr = new Object[13];
            objArr[0] = Integer.valueOf(hashCode());
            objArr[1] = Long.valueOf(getSqlId());
            objArr[2] = Integer.valueOf(getPipedriveId());
            objArr[3] = getName();
            objArr[4] = getFirstLetter();
            objArr[5] = getUpdateTime();
            objArr[6] = getAddTime();
            objArr[7] = Integer.valueOf(getOwnerPipedriveId());
            objArr[8] = Integer.valueOf(getVisibleTo());
            objArr[9] = getPhonesJsonString();
            objArr[10] = getEmailsJsonString();
            JSONObject json = getJSON();
            objArr[11] = !(json instanceof JSONObject) ? json.toString() : JSONObjectInstrumentation.toString(json);
            objArr[12] = getCompany();
            return String.format(str, objArr);
        } catch (JSONException e) {
            return super.toString();
        }
    }

    public String getAddress() {
        return null;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public String getImsJsonString() {
        return CommunicationMedium.toJsonArrayString(this.ims);
    }

    public String getPostalAddress() {
        return this.postalAddress;
    }

    @Nullable
    public Integer getPictureId() {
        return this.pictureId;
    }

    public void setPictureId(@Nullable Integer pictureId) {
        this.pictureId = pictureId;
    }

    public void removeEmptyCommunicationMediums() {
        removeEmptyItems(this.phones);
        removeEmptyItems(this.emails);
        removeEmptyItems(this.ims);
    }

    private void removeEmptyItems(@NonNull List<? extends CommunicationMedium> items) {
        Iterator<? extends CommunicationMedium> iterator = items.iterator();
        while (iterator.hasNext()) {
            if (StringUtils.isTrimmedAndEmpty(((CommunicationMedium) iterator.next()).getValue())) {
                iterator.remove();
            }
        }
    }
}
