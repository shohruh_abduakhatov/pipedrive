package com.pipedrive.datetime;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.pipedrive.application.Session;
import com.pipedrive.util.time.TimeManager;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public enum LocaleHelper {
    ;

    public static Locale getAppLocaleForNumbersDatesFormatting(Session session) {
        Locale locale = Locale.getDefault();
        String localeFromWeb = session.getUserSettingsDefaultLocale();
        if (TextUtils.isEmpty(localeFromWeb)) {
            return locale;
        }
        String[] langCountryArr = localeFromWeb.split(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        if (langCountryArr.length == 2) {
            return new Locale(langCountryArr[0], langCountryArr[1]);
        }
        return locale;
    }

    private static String getLocaleBasedDateString(@NonNull Session session, @NonNull Date date, int style, boolean ignoreTimeZone) {
        DateFormat dateFormat = DateFormat.getDateInstance(style, getAppLocaleForNumbersDatesFormatting(session));
        if (ignoreTimeZone) {
            dateFormat.setTimeZone(TimeManager.getInstance().getUtcTimeZone());
        } else {
            dateFormat.setTimeZone(TimeManager.getInstance().getDefaultTimeZone());
        }
        return dateFormat.format(date);
    }

    @Deprecated
    public static String getLocaleBasedDateString(Session session, Date date) {
        return getLocaleBasedDateStringInCurrentTimeZone(session, date);
    }

    public static String getLocaleBasedDateStringInGmtZero(@NonNull Session session, @NonNull Date date) {
        return getLocaleBasedDateString(session, date, 2, true);
    }

    public static String getLocaleBasedDateStringInCurrentTimeZone(@NonNull Session session, @NonNull Date date) {
        return getLocaleBasedDateString(session, date, 2, false);
    }

    public static String getLocaleBasedDateString(@NonNull Session session, @NonNull PipedriveDate pipedriveDate) {
        return getLocaleBasedDateStringInGmtZero(session, new Date(TimeUnit.SECONDS.toMillis(pipedriveDate.getRepresentationInUnixTime())));
    }

    private static String getLocaleBasedTimeString(@NonNull Session session, @NonNull Date date, int style, boolean ignoreTimeZone) {
        DateFormat timeFormat = DateFormat.getTimeInstance(style, getAppLocaleForNumbersDatesFormatting(session));
        if (ignoreTimeZone) {
            timeFormat.setTimeZone(TimeManager.getInstance().getUtcTimeZone());
        } else {
            timeFormat.setTimeZone(TimeManager.getInstance().getDefaultTimeZone());
        }
        return timeFormat.format(date);
    }

    @Deprecated
    public static String getLocaleBasedTimeString(Session session, Date date) {
        return getLocaleBasedTimeStringInCurrentTimeZone(session, date);
    }

    public static String getLocaleBasedTimeStringInGmtZero(@NonNull Session session, @NonNull Date date) {
        return getLocaleBasedTimeString(session, date, 3, true);
    }

    public static String getLocaleBasedTimeStringInCurrentTimeZone(@NonNull Session session, @NonNull Date date) {
        return getLocaleBasedTimeString(session, date, 3, false);
    }

    private static String getLocaleBasedDateTimeString(@NonNull Session session, @NonNull Date date, int dateStyle, int timeStyle, boolean ignoreTimeZone) {
        DateFormat dateTimeFormat = DateFormat.getDateTimeInstance(dateStyle, timeStyle, getAppLocaleForNumbersDatesFormatting(session));
        if (ignoreTimeZone) {
            dateTimeFormat.setTimeZone(TimeManager.getInstance().getUtcTimeZone());
        } else {
            dateTimeFormat.setTimeZone(TimeManager.getInstance().getDefaultTimeZone());
        }
        return dateTimeFormat.format(date);
    }

    static String getLocaleBasedDateTimeStringInGmtZero(@NonNull Session session, @NonNull Date date) {
        return getLocaleBasedDateTimeString(session, date, 2, 3, true);
    }

    public static String getLocaleBasedDateTimeStringInCurrentTimeZone(@NonNull Session session, @NonNull Date date) {
        return getLocaleBasedDateTimeString(session, date, 2, 3, false);
    }

    public static String getLocaleBasedDateTimeStringInCurrentTimeZoneLongDate(@NonNull Session session, @NonNull Date date) {
        return getLocaleBasedDateTimeString(session, date, 1, 3, false);
    }

    public static String getLocaleBasedTimeStringInCurrentTimeZoneFromHourOfDay(@NonNull Session session, int hourOfDay) {
        Calendar calendarFromHourOfDay = TimeManager.getInstance().getLocalCalendar();
        calendarFromHourOfDay.set(11, hourOfDay);
        return new SimpleDateFormat("h a", getAppLocaleForNumbersDatesFormatting(session)).format(calendarFromHourOfDay.getTime());
    }

    public static String getLocaleBasedDateStringInCurrentTimeDayOfMonthOnly(@NonNull Session session, @NonNull Date date) {
        return getLocaleBasedDateStringDayOfMonthOnly(session, date, false);
    }

    public static String getLocaleBasedDateStringInGmtZeroDayOfMonthOnly(@NonNull Session session, @NonNull Date date) {
        return getLocaleBasedDateStringDayOfMonthOnly(session, date, true);
    }

    private static String getLocaleBasedDateStringDayOfMonthOnly(@NonNull Session session, @NonNull Date date, boolean ignoreTimeZone) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("d", getAppLocaleForNumbersDatesFormatting(session));
        if (ignoreTimeZone) {
            dateFormat.setTimeZone(TimeManager.getInstance().getUtcTimeZone());
        } else {
            dateFormat.setTimeZone(TimeManager.getInstance().getDefaultTimeZone());
        }
        return dateFormat.format(date);
    }
}
