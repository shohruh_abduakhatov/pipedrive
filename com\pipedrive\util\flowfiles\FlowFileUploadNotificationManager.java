package com.pipedrive.util.flowfiles;

import android.support.annotation.NonNull;
import rx.Observable;
import rx.subjects.SerializedSubject;

public enum FlowFileUploadNotificationManager {
    INSTANCE;
    
    private final SerializedSubject<Notification, Notification> fileUploadNotificationBus;

    public static class Notification {
        @NonNull
        public final Long flowFileSqlId;
        @NonNull
        public final Boolean isFileUploadSuccessful;

        public Notification(@NonNull Boolean isFileUploadSuccessful, @NonNull Long flowFileSqlId) {
            this.isFileUploadSuccessful = isFileUploadSuccessful;
            this.flowFileSqlId = flowFileSqlId;
        }

        public String toString() {
            return "Notification{isFileUploadSuccessful=" + this.isFileUploadSuccessful + ", flowFileSqlId=" + this.flowFileSqlId + '}';
        }
    }

    public void notifyFileUploadSuccessful(@NonNull Long flowFileSqlId) {
        if (this.fileUploadNotificationBus.hasObservers()) {
            this.fileUploadNotificationBus.onNext(new Notification(Boolean.valueOf(true), flowFileSqlId));
        }
    }

    public void notifyFileUploadFailed(@NonNull Long flowFileSqlId) {
        if (this.fileUploadNotificationBus.hasObservers()) {
            this.fileUploadNotificationBus.onNext(new Notification(Boolean.valueOf(false), flowFileSqlId));
        }
    }

    public Observable<Notification> getFlowFileUploadNotificationBus() {
        return this.fileUploadNotificationBus;
    }
}
