package com.pipedrive.pipeline;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.presenter.Presenter;

abstract class PipelinePresenter extends Presenter<PipelineView> {
    abstract void onFilterChanged();

    abstract void onStageRefresh(int i);

    abstract void requery(@NonNull Integer num);

    abstract void requestAllPipelineStages(int i);

    abstract void requestDeals(int i);

    abstract void requestPipelines();

    public PipelinePresenter(@NonNull Session session) {
        super(session);
    }
}
