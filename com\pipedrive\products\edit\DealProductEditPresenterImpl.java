package com.pipedrive.products.edit;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.CurrenciesDataSource;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.products.DealProductsDataSource;
import com.pipedrive.datasource.products.ProductVariationDataSource;
import com.pipedrive.datasource.products.ProductsDataSource;
import com.pipedrive.model.Currency;
import com.pipedrive.model.Deal;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.model.products.Price;
import com.pipedrive.model.products.Product;
import com.pipedrive.model.products.ProductVariation;
import com.pipedrive.products.DealProductSerializer;
import com.pipedrive.store.StoreDealProduct;
import com.pipedrive.tasks.AsyncTask;
import java.math.BigDecimal;

class DealProductEditPresenterImpl extends DealProductEditPresenter {
    private static final double DEFAULT_DISCOUNT_PERCENTAGE = 0.0d;
    private static final double DEFAULT_QUANTITY = 1.0d;
    private static final String KEY_DEALPRODUCT_AS_BUNDLE = "dealProduct_as_bundle";
    @Nullable
    private DealProduct mCurrentlyManagedDealProduct;
    @Nullable
    private Long mDealProductSqlId;
    @Nullable
    private Long mDealSqlId;
    @Nullable
    private Long mProductSqlId;

    private class CreateDealProductTask extends AsyncTask<DealProduct, Void, Void> {
        private CreateDealProductTask(@NonNull Session session) {
            super(session);
        }

        protected Void doInBackground(DealProduct... params) {
            new StoreDealProduct(getSession()).create(params[0]);
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            DealProductEditPresenterImpl.this.onUpdateDealProductFinished();
        }
    }

    private class DeleteDealProductTask extends AsyncTask<DealProduct, Void, Void> {
        private DeleteDealProductTask(@NonNull Session session) {
            super(session);
        }

        protected Void doInBackground(DealProduct... params) {
            new StoreDealProduct(getSession()).delete(params[0]);
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            DealProductEditPresenterImpl.this.onDeleteDealProductFinished();
        }
    }

    private class UpdateDealProductTask extends AsyncTask<DealProduct, Void, Void> {
        private UpdateDealProductTask(@NonNull Session session) {
            super(session);
        }

        protected Void doInBackground(DealProduct... params) {
            new StoreDealProduct(getSession()).update(params[0]);
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            DealProductEditPresenterImpl.this.onUpdateDealProductFinished();
        }
    }

    DealProductEditPresenterImpl(@NonNull Session session, @Nullable Bundle savedState) {
        super(session, savedState);
    }

    protected void saveToBundle(@NonNull Bundle bundle) {
        if (this.mCurrentlyManagedDealProduct != null) {
            bundle.putBundle(KEY_DEALPRODUCT_AS_BUNDLE, new DealProductSerializer(getSession()).saveDealProductFieldsToBundle(this.mCurrentlyManagedDealProduct));
        }
    }

    protected void restoreFromBundle(@NonNull Bundle bundle) {
        this.mCurrentlyManagedDealProduct = new DealProductSerializer(getSession()).createDealProductFromBundle(bundle.getBundle(KEY_DEALPRODUCT_AS_BUNDLE));
    }

    protected String[] getKeysNeededForRestore() {
        return new String[]{KEY_DEALPRODUCT_AS_BUNDLE};
    }

    void requestExistingDealProduct(@NonNull Long dealProductSqlId) {
        this.mDealProductSqlId = dealProductSqlId;
        if (this.mCurrentlyManagedDealProduct == null) {
            this.mCurrentlyManagedDealProduct = (DealProduct) new DealProductsDataSource(getSession().getDatabase()).findBySqlId(dealProductSqlId.longValue());
        }
        if (getView() != null) {
            ((DealProductEditView) getView()).onDealProductRequested(this.mCurrentlyManagedDealProduct);
        }
    }

    void requestNewDealProduct(@NonNull Long productSqlId, @NonNull Long dealSqlId) {
        this.mDealSqlId = dealSqlId;
        this.mProductSqlId = productSqlId;
        if (this.mCurrentlyManagedDealProduct == null) {
            this.mCurrentlyManagedDealProduct = getDealProduct(dealSqlId, productSqlId);
        }
        if (getView() != null) {
            ((DealProductEditView) getView()).onDealProductRequested(this.mCurrentlyManagedDealProduct);
        }
    }

    void updateDealProductsVariation(@Nullable Long selectedVariationSqlId) {
        if (this.mDealProductSqlId != null) {
            this.mCurrentlyManagedDealProduct = (DealProduct) new DealProductsDataSource(getSession().getDatabase()).findBySqlId(this.mDealProductSqlId.longValue());
            if (this.mCurrentlyManagedDealProduct == null) {
                return;
            }
        } else if (this.mDealSqlId != null && this.mProductSqlId != null) {
            this.mCurrentlyManagedDealProduct = getDealProduct(this.mDealSqlId, this.mProductSqlId);
        } else {
            return;
        }
        if (this.mCurrentlyManagedDealProduct != null) {
            Product product = this.mCurrentlyManagedDealProduct.getProduct();
            ProductVariation selectedProductVariation = selectedVariationSqlId != null ? (ProductVariation) new ProductVariationDataSource(getSession().getDatabase()).findBySqlId(selectedVariationSqlId.longValue()) : null;
            this.mCurrentlyManagedDealProduct.update(selectedProductVariation, product.getProductVariationPriceForCurrency(this.mCurrentlyManagedDealProduct.getPrice().getCurrency(), selectedProductVariation), this.mCurrentlyManagedDealProduct.getQuantity(), this.mCurrentlyManagedDealProduct.getDuration(), this.mCurrentlyManagedDealProduct.getDiscountPercentage());
            if (getView() != null) {
                ((DealProductEditView) getView()).onDealProductRequested(this.mCurrentlyManagedDealProduct);
            }
        }
    }

    @Nullable
    DealProduct getCurrentlyManagedDealProduct() {
        return this.mCurrentlyManagedDealProduct;
    }

    void updateDealProductWithEnteredData(@NonNull Double priceValue, @NonNull Double quantity, @Nullable ProductVariation variation, @NonNull Double discount, @Nullable Double duration) {
        if (this.mCurrentlyManagedDealProduct != null) {
            this.mCurrentlyManagedDealProduct.update(variation, Price.create(new BigDecimal(priceValue.doubleValue()), this.mCurrentlyManagedDealProduct.getPrice().getCurrency()), quantity, duration, discount);
        }
    }

    boolean changesAreMadeToDealProduct() {
        if (this.mCurrentlyManagedDealProduct == null || this.mCurrentlyManagedDealProduct.getSqlIdOrNull() == null) {
            return false;
        }
        DealProduct originalDealProduct = (DealProduct) new DealProductsDataSource(getSession().getDatabase()).findBySqlId(this.mCurrentlyManagedDealProduct.getSqlIdOrNull().longValue());
        if (originalDealProduct == null) {
            return false;
        }
        if (!this.mCurrentlyManagedDealProduct.getPrice().equals(originalDealProduct.getPrice()) || !this.mCurrentlyManagedDealProduct.getQuantity().equals(originalDealProduct.getQuantity()) || !this.mCurrentlyManagedDealProduct.getDiscountPercentage().equals(originalDealProduct.getDiscountPercentage()) || !this.mCurrentlyManagedDealProduct.getDuration().equals(originalDealProduct.getDuration())) {
            return true;
        }
        ProductVariation currentProductVariation = this.mCurrentlyManagedDealProduct.getProductVariation();
        ProductVariation originalProductVariation = originalDealProduct.getProductVariation();
        if (currentProductVariation == null && originalProductVariation != null) {
            return true;
        }
        if (currentProductVariation == null || currentProductVariation.equals(originalProductVariation)) {
            return false;
        }
        return true;
    }

    void changeDealProduct() {
        if (this.mCurrentlyManagedDealProduct == null) {
            return;
        }
        if (this.mCurrentlyManagedDealProduct.isStored()) {
            if (!AsyncTask.isExecuting(getSession(), UpdateDealProductTask.class)) {
                new UpdateDealProductTask(getSession()).executeOnAsyncTaskExecutor(this, this.mCurrentlyManagedDealProduct);
            }
        } else if (!AsyncTask.isExecuting(getSession(), CreateDealProductTask.class)) {
            new CreateDealProductTask(getSession()).executeOnAsyncTaskExecutor(this, this.mCurrentlyManagedDealProduct);
        }
    }

    void deleteDealProduct() {
        if (canDeleteDealProduct() && !AsyncTask.isExecuting(getSession(), DeleteDealProductTask.class)) {
            new DeleteDealProductTask(getSession()).executeOnAsyncTaskExecutor(this, this.mCurrentlyManagedDealProduct);
        }
    }

    boolean canDeleteDealProduct() {
        return getCurrentlyManagedDealProduct() != null && getCurrentlyManagedDealProduct().isStored();
    }

    private void onUpdateDealProductFinished() {
        if (getView() != null) {
            ((DealProductEditView) getView()).onDealProductCreatedOrUpdated();
        }
    }

    private void onDeleteDealProductFinished() {
        if (getView() != null) {
            ((DealProductEditView) getView()).onDealProductDeleted();
        }
    }

    @Nullable
    private DealProduct getDealProduct(@NonNull Long dealSqlId, @NonNull Long productSqlId) {
        Deal deal = (Deal) new DealsDataSource(getSession().getDatabase()).findBySqlId(dealSqlId.longValue());
        if (deal == null) {
            return null;
        }
        Product product = (Product) new ProductsDataSource(getSession().getDatabase()).findBySqlId(productSqlId.longValue());
        if (product == null) {
            return null;
        }
        Currency currency = new CurrenciesDataSource(getSession().getDatabase()).getCurrencyByCode(deal.getCurrencyCode());
        if (currency == null) {
            return null;
        }
        return DealProduct.create(null, deal.getSqlIdOrNull(), product, null, product.getPriceForDealsCurrency(currency), Double.valueOf(1.0d), null, Double.valueOf(DEFAULT_DISCOUNT_PERCENTAGE), Boolean.valueOf(true), null);
    }
}
