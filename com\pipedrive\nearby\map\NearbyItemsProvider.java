package com.pipedrive.nearby.map;

import android.support.annotation.NonNull;
import com.pipedrive.nearby.model.NearbyItem;
import java.util.List;
import rx.Observable;

public interface NearbyItemsProvider {
    @NonNull
    Observable<List<NearbyItem>> nearbyItems();
}
