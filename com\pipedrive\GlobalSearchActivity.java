package com.pipedrive;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;

public class GlobalSearchActivity extends NavigationActivity {
    public static void startActivity(Activity activity) {
        ContextCompat.startActivity(activity, new Intent(activity, GlobalSearchActivity.class), null);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_global_search);
    }
}
