package com.zendesk.sdk.model.helpcenter;

import com.zendesk.util.CollectionUtils;
import java.util.List;

public class SectionsResponse {
    public List<Section> sections;

    public List<Section> getSections() {
        return CollectionUtils.copyOf(this.sections);
    }
}
