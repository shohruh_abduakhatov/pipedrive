package com.pipedrive.views.viewholder.contact;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.R;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.interfaces.ContactOrgInterface;
import com.pipedrive.model.Organization;

public class OrganizationRowViewHolder extends ContactRowViewHolder {
    public void fill(@NonNull ContactOrgInterface contact, @NonNull SearchConstraint searchConstraint) {
        super.fill(searchConstraint, contact.getName(), getContactOrganizationName(contact));
    }

    @Nullable
    String getContactOrganizationName(@Nullable ContactOrgInterface contactOrgInterface) {
        if (contactOrgInterface instanceof Organization) {
            return contactOrgInterface.getAddress();
        }
        return null;
    }

    public int getLayoutResourceId() {
        return R.layout.row_organization;
    }
}
