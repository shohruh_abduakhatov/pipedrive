package com.pipedrive.views.filter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.pipedrive.adapter.filter.FilterContentActivityAdapter;
import com.pipedrive.adapter.filter.FilterContentActivityAdapter.Item;

public class ActivityFilterListView extends ListView {
    @Nullable
    private Integer mCheckedPositionForActivityType;
    @Nullable
    private Integer mCheckedPositionForUser;
    @Nullable
    private Integer mCheckedPositionTimeFilterTimeId;

    public ActivityFilterListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setChoiceMode(2);
    }

    public void setAdapter(ListAdapter adapter) {
        throw new RuntimeException("I only accept " + FilterContentActivityAdapter.class.getSimpleName());
    }

    public void setAdapter(FilterContentActivityAdapter adapter) {
        super.setAdapter(adapter);
        setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getAdapter() instanceof FilterContentActivityAdapter) {
                    ActivityFilterListView.this.onItemClick((FilterContentActivityAdapter) parent.getAdapter(), position);
                }
            }
        });
    }

    @Nullable
    public Integer getSelectedActivityTypeId() {
        Item selectedItem = getSelectedItem(this.mCheckedPositionForActivityType);
        boolean selectedActivityTypeItemFound = (selectedItem == null || selectedItem.mActivityType == null) ? false : true;
        return selectedActivityTypeItemFound ? Integer.valueOf(selectedItem.mActivityType.getId()) : null;
    }

    @Nullable
    public Integer getSelectedTimeFilterTimeId() {
        Item selectedItem = getSelectedItem(this.mCheckedPositionTimeFilterTimeId);
        return selectedItem != null ? selectedItem.mTimeFilterTimeId : null;
    }

    @Nullable
    private Item getSelectedItem(@Nullable Integer checkedPosition) {
        FilterContentActivityAdapter adapter = getAdapter();
        boolean cannotReturnItem = checkedPosition == null || adapter == null;
        if (cannotReturnItem) {
            return null;
        }
        return adapter.getItem(checkedPosition.intValue());
    }

    @Nullable
    public Integer getSelectedUserId() {
        Item selectedItem = getSelectedItem(this.mCheckedPositionForUser);
        return (!(selectedItem != null) || selectedItem.mUser == null) ? null : Integer.valueOf(selectedItem.mUser.getPipedriveId());
    }

    public boolean setActivityTypeId(int activityTypeId) {
        boolean adapterNotFoundToSearchItemsFrom;
        FilterContentActivityAdapter adapter = getAdapter();
        if (adapter == null) {
            adapterNotFoundToSearchItemsFrom = true;
        } else {
            adapterNotFoundToSearchItemsFrom = false;
        }
        if (adapterNotFoundToSearchItemsFrom) {
            return false;
        }
        for (int i = 0; i < adapter.getCount(); i++) {
            boolean foundActivityTypeWithRequestedActivityTypeId;
            Item item = adapter.getItem(i);
            if (item == null || item.mActivityType == null || item.mActivityType.getId() != activityTypeId) {
                foundActivityTypeWithRequestedActivityTypeId = false;
            } else {
                foundActivityTypeWithRequestedActivityTypeId = true;
            }
            if (foundActivityTypeWithRequestedActivityTypeId) {
                onItemClick(adapter, i);
                return true;
            }
        }
        return false;
    }

    public boolean setTimeFilterTimeId(int timeFilterTimeId) {
        boolean adapterNotFoundToSearchItemsFrom;
        FilterContentActivityAdapter adapter = getAdapter();
        if (adapter == null) {
            adapterNotFoundToSearchItemsFrom = true;
        } else {
            adapterNotFoundToSearchItemsFrom = false;
        }
        if (adapterNotFoundToSearchItemsFrom) {
            return false;
        }
        for (int i = 0; i < adapter.getCount(); i++) {
            boolean foundTimeFilterTimeIdWithRequestedId;
            Item item = adapter.getItem(i);
            if (item == null || item.mTimeFilterTimeId == null || item.mTimeFilterTimeId.intValue() != timeFilterTimeId) {
                foundTimeFilterTimeIdWithRequestedId = false;
            } else {
                foundTimeFilterTimeIdWithRequestedId = true;
            }
            if (foundTimeFilterTimeIdWithRequestedId) {
                onItemClick(adapter, i);
                return true;
            }
        }
        return false;
    }

    public boolean setUserId(int userId) {
        boolean adapterNotFoundToSearchItemsFrom;
        FilterContentActivityAdapter adapter = getAdapter();
        if (adapter == null) {
            adapterNotFoundToSearchItemsFrom = true;
        } else {
            adapterNotFoundToSearchItemsFrom = false;
        }
        if (adapterNotFoundToSearchItemsFrom) {
            return false;
        }
        for (int i = 0; i < adapter.getCount(); i++) {
            boolean foundUserWithRequestedId;
            Item item = adapter.getItem(i);
            if (item == null || item.mUser == null || item.mUser.getPipedriveId() != userId) {
                foundUserWithRequestedId = false;
            } else {
                foundUserWithRequestedId = true;
            }
            if (foundUserWithRequestedId) {
                onItemClick(adapter, i);
                return true;
            }
        }
        return false;
    }

    public void setFirstActivityTypeIdPositionChecked() {
        boolean cannotSetPosition;
        FilterContentActivityAdapter filterContentActivityAdapter = getAdapter();
        if (filterContentActivityAdapter == null) {
            cannotSetPosition = true;
        } else {
            cannotSetPosition = false;
        }
        if (!cannotSetPosition) {
            setItemPosition(filterContentActivityAdapter, 0);
        }
    }

    public void setFirstTimeFilterTimeIdPosition() {
        FilterContentActivityAdapter filterContentActivityAdapter = getAdapter();
        if (!(filterContentActivityAdapter == null)) {
            setItemPosition(filterContentActivityAdapter, 1);
        }
    }

    public void setFirstUserPosition() {
        FilterContentActivityAdapter filterContentActivityAdapter = getAdapter();
        if (!(filterContentActivityAdapter == null)) {
            setItemPosition(filterContentActivityAdapter, 3);
        }
    }

    private void setItemPosition(@NonNull FilterContentActivityAdapter filterContentActivityAdapter, int filterContentType) {
        for (int i = 0; i < filterContentActivityAdapter.getCount(); i++) {
            if (filterContentActivityAdapter.getItemViewType(i) == filterContentType) {
                onItemClick(filterContentActivityAdapter, i);
                return;
            }
        }
    }

    @Nullable
    public FilterContentActivityAdapter getAdapter() {
        if (super.getAdapter() instanceof FilterContentActivityAdapter) {
            return (FilterContentActivityAdapter) super.getAdapter();
        }
        return null;
    }

    private void onItemClick(@NonNull FilterContentActivityAdapter adapter, int position) {
        if (!isItemChecked(position)) {
            setItemChecked(position, true);
        }
        findNewSelectionsAndCheck(adapter, position);
    }

    private void findNewSelectionsAndCheck(@NonNull FilterContentActivityAdapter adapter, int position) {
        boolean activityTypeIsChecked;
        boolean timeFilterTimeIdIsChecked;
        boolean userIsChecked;
        boolean selectionChanged;
        int itemType = adapter.getItemViewType(position);
        if (itemType == 0) {
            activityTypeIsChecked = true;
        } else {
            activityTypeIsChecked = false;
        }
        if (itemType == 1) {
            timeFilterTimeIdIsChecked = true;
        } else {
            timeFilterTimeIdIsChecked = false;
        }
        if (itemType == 3) {
            userIsChecked = true;
        } else {
            userIsChecked = false;
        }
        if (activityTypeIsChecked) {
            if (this.mCheckedPositionForActivityType == null || position != this.mCheckedPositionForActivityType.intValue()) {
                selectionChanged = true;
            } else {
                selectionChanged = false;
            }
            if (selectionChanged) {
                changeSelections(this.mCheckedPositionForActivityType, Integer.valueOf(position));
                this.mCheckedPositionForActivityType = Integer.valueOf(position);
            }
        }
        if (timeFilterTimeIdIsChecked) {
            if (this.mCheckedPositionTimeFilterTimeId == null || position != this.mCheckedPositionTimeFilterTimeId.intValue()) {
                selectionChanged = true;
            } else {
                selectionChanged = false;
            }
            if (selectionChanged) {
                changeSelections(this.mCheckedPositionTimeFilterTimeId, Integer.valueOf(position));
                this.mCheckedPositionTimeFilterTimeId = Integer.valueOf(position);
            }
        }
        if (userIsChecked) {
            if (this.mCheckedPositionForUser == null || position != this.mCheckedPositionForUser.intValue()) {
                selectionChanged = true;
            } else {
                selectionChanged = false;
            }
            if (selectionChanged) {
                changeSelections(this.mCheckedPositionForUser, Integer.valueOf(position));
                this.mCheckedPositionForUser = Integer.valueOf(position);
            }
        }
    }

    private void changeSelections(@Nullable Integer positionToUncheck, @NonNull Integer positionToCheck) {
        boolean uncheckPositionRequested;
        if (positionToUncheck != null) {
            uncheckPositionRequested = true;
        } else {
            uncheckPositionRequested = false;
        }
        if (uncheckPositionRequested) {
            setItemChecked(positionToUncheck.intValue(), false);
        }
        setItemChecked(positionToCheck.intValue(), true);
    }
}
