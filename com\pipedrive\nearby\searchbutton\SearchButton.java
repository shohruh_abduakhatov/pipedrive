package com.pipedrive.nearby.searchbutton;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.map.NearbyItemsProvider;
import com.pipedrive.nearby.map.ViewportChangeProvider;

public interface SearchButton extends SearchButtonClickProvider {
    void bind();

    boolean isHidden();

    void releaseResources();

    void saveState(@NonNull Bundle bundle);

    void setup(@Nullable Bundle bundle, @NonNull Session session, @NonNull ViewportChangeProvider viewportChangeProvider, @NonNull NearbyItemsProvider nearbyItemsProvider);
}
