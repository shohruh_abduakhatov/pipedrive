package com.google.android.gms.auth.api.credentials.internal;

import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.CredentialRequestResult;
import com.google.android.gms.common.api.Status;

public final class zzd implements CredentialRequestResult {
    private final Status hv;
    private final Credential iP;

    public zzd(Status status, Credential credential) {
        this.hv = status;
        this.iP = credential;
    }

    public static zzd zzi(Status status) {
        return new zzd(status, null);
    }

    public Credential getCredential() {
        return this.iP;
    }

    public Status getStatus() {
        return this.hv;
    }
}
