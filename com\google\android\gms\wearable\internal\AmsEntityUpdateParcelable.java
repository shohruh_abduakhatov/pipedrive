package com.google.android.gms.wearable.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.wearable.zzb;

public class AmsEntityUpdateParcelable extends AbstractSafeParcelable implements zzb {
    public static final Creator<AmsEntityUpdateParcelable> CREATOR = new zzf();
    private byte aSS;
    private final byte aST;
    private final String mValue;
    final int mVersionCode;

    AmsEntityUpdateParcelable(int i, byte b, byte b2, String str) {
        this.aSS = b;
        this.mVersionCode = i;
        this.aST = b2;
        this.mValue = str;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AmsEntityUpdateParcelable amsEntityUpdateParcelable = (AmsEntityUpdateParcelable) obj;
        return this.aSS != amsEntityUpdateParcelable.aSS ? false : this.mVersionCode != amsEntityUpdateParcelable.mVersionCode ? false : this.aST != amsEntityUpdateParcelable.aST ? false : this.mValue.equals(amsEntityUpdateParcelable.mValue);
    }

    public String getValue() {
        return this.mValue;
    }

    public int hashCode() {
        return (((((this.mVersionCode * 31) + this.aSS) * 31) + this.aST) * 31) + this.mValue.hashCode();
    }

    public String toString() {
        int i = this.mVersionCode;
        byte b = this.aSS;
        byte b2 = this.aST;
        String str = this.mValue;
        return new StringBuilder(String.valueOf(str).length() + 97).append("AmsEntityUpdateParcelable{mVersionCode=").append(i).append(", mEntityId=").append(b).append(", mAttributeId=").append(b2).append(", mValue='").append(str).append("'").append("}").toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzf.zza(this, parcel, i);
    }

    public byte zzcmj() {
        return this.aSS;
    }

    public byte zzcmk() {
        return this.aST;
    }
}
