package rx;

class Single$2 implements Single$OnSubscribe<T> {
    final /* synthetic */ Single val$source;

    Single$2(Single single) {
        this.val$source = single;
    }

    public void call(final SingleSubscriber<? super T> child) {
        SingleSubscriber<Single<? extends T>> parent = new SingleSubscriber<Single<? extends T>>() {
            public void onSuccess(Single<? extends T> innerSingle) {
                innerSingle.subscribe(child);
            }

            public void onError(Throwable error) {
                child.onError(error);
            }
        };
        child.add(parent);
        this.val$source.subscribe(parent);
    }
}
