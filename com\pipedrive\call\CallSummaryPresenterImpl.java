package com.pipedrive.call;

import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.R;
import com.pipedrive.activity.CreateActivityTask;
import com.pipedrive.activity.CreateActivityTask.OnTaskFinished;
import com.pipedrive.activity.ReadActivityTask;
import com.pipedrive.activity.UpdateActivityTask;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.datetime.PipedriveDuration;
import com.pipedrive.model.Activity;
import com.pipedrive.model.ActivityType;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Person;
import com.pipedrive.model.delta.ObjectDelta;
import com.pipedrive.tasks.AsyncTask;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class CallSummaryPresenterImpl extends CallSummaryPresenter {
    private static final String STORAGE_KEY_ACTIVITY = (CallSummaryPresenterImpl.class.getSimpleName() + ".activity");
    private static final String STORAGE_KEY_ORIGINAL_ACTIVITY = (CallSummaryPresenterImpl.class.getSimpleName() + ".originalActivity");
    private final OnTaskFinished CREATE_TASK_CALLBACK = new OnTaskFinished() {
        public void onActivityCreated(boolean success) {
            if (CallSummaryPresenterImpl.this.mView != null) {
                ((CallSummaryView) CallSummaryPresenterImpl.this.mView).onCallActivityCreated(success);
            }
        }
    };
    private final ReadActivityTask.OnTaskFinished READ_TASK_CALLBACK = new ReadActivityTask.OnTaskFinished() {
        public void onActivityRead(@Nullable Activity activity) {
            CallSummaryPresenterImpl.this.setCommonFieldsAndSendToView(activity);
        }
    };
    private final UpdateActivityTask.OnTaskFinished UPDATE_TASK_CALLBACK = new UpdateActivityTask.OnTaskFinished() {
        public void onActivityUpdated(boolean success) {
            if (CallSummaryPresenterImpl.this.getView() != null) {
                ((CallSummaryView) CallSummaryPresenterImpl.this.getView()).onCallActivityUpdated(success);
            }
        }
    };
    @Nullable
    private Activity mActivity;
    @NonNull
    private final ObjectDelta<Activity> mActivityObjectDelta = new ObjectDelta(Activity.class);
    private long mDurationInMillis;
    @Nullable
    private Activity mOriginalActivity;
    private long mStartTimeInMillis;

    public CallSummaryPresenterImpl(@NonNull Session session, @Nullable Bundle savedState) {
        super(session, savedState);
    }

    @Nullable
    public Activity getActivity() {
        return this.mActivity;
    }

    void requestOrRestoreNewCallActivityForPerson(@NonNull Long personSqlId, @Nullable Long relatedDealSqlId, long startTime, long duration) {
        if (!restoreCallSummaryActivity()) {
            this.mDurationInMillis = duration;
            this.mStartTimeInMillis = startTime;
            Person person = (Person) new PersonsDataSource(getSession().getDatabase()).findBySqlId(personSqlId.longValue());
            if (person != null) {
                this.mActivity = new Activity(getSession());
                this.mActivity.setSubject(getSession().getApplicationContext().getString(R.string.outgoing_call));
                this.mActivity.setPerson(person);
                this.mActivity.setOrganization(person.getCompany());
                if (relatedDealSqlId != null) {
                    this.mActivity.setDeal((Deal) new DealsDataSource(getSession().getDatabase()).findBySqlId(relatedDealSqlId.longValue()));
                }
                setNewActivityType(this.mActivity);
                setCommonFieldsAndSendToView(this.mActivity);
            }
        }
    }

    public void setDone(boolean isChecked) {
        if (this.mActivity != null) {
            this.mActivity.setDone(isChecked);
        }
    }

    void associateDealBySqlId(@NonNull Long dealSqlId) {
        associateDeal((Deal) new DealsDataSource(getSession().getDatabase()).findBySqlId(dealSqlId.longValue()));
    }

    boolean isModified() {
        return this.mActivity != null && this.mActivityObjectDelta.hasChanges(this.mActivity, this.mOriginalActivity);
    }

    void associateDeal(@Nullable Deal deal) {
        if (this.mActivity != null) {
            boolean associationIsANewDeal = (deal == null || deal.isStored()) ? false : true;
            if (!associationIsANewDeal) {
                this.mActivity.setDeal(deal);
            } else if (fillNewDealWithData(deal, this.mActivity)) {
                this.mActivity.setDeal(deal);
            }
            if (getView() != null) {
                ((CallSummaryView) getView()).onAssociationsUpdated(this.mActivity);
            }
        }
    }

    private boolean fillNewDealWithData(@NonNull Deal inoutAssociatedDeal, @NonNull Activity activity) {
        Session session = getSession();
        boolean presetIsSuccessful = Deal.updateDealWithRequiredDefaultValuesAndCurrentPipelineAndFirstStage(inoutAssociatedDeal, session);
        inoutAssociatedDeal.setPerson(activity.getPerson());
        inoutAssociatedDeal.setOrganization(activity.getOrganization());
        if (!(activity.getPerson() != null)) {
            return false;
        }
        String dealTitleBasedOnAssociations = inoutAssociatedDeal.getTitleBasedOnAssociations(session.getApplicationContext().getResources());
        if (dealTitleBasedOnAssociations == null) {
            dealTitleBasedOnAssociations = "";
        }
        inoutAssociatedDeal.setTitle(dealTitleBasedOnAssociations);
        return presetIsSuccessful;
    }

    void appendOrAddNote(String noteContent) {
        if (!noteContent.isEmpty() && this.mActivity != null) {
            Activity activity = this.mActivity;
            if (this.mActivity.getNote() != null) {
                noteContent = this.mActivity.getNote() + "<br/><br/>" + noteContent;
            }
            activity.setNote(noteContent);
        }
    }

    public void requestOrRestoreExistingActivity(@Nullable Long sqlId, long startTimeInMillis, long durationInMillis) {
        if (!restoreCallSummaryActivity()) {
            this.mStartTimeInMillis = startTimeInMillis;
            this.mDurationInMillis = durationInMillis;
            if (!AsyncTask.isExecuting(getSession(), ReadActivityTask.class)) {
                new ReadActivityTask(getSession(), this.READ_TASK_CALLBACK).executeOnAsyncTaskExecutor(this, new Long[]{sqlId});
            }
        }
    }

    private boolean restoreCallSummaryActivity() {
        if (this.mActivity == null || this.mOriginalActivity == null) {
            return false;
        }
        if (getView() != null) {
            ((CallSummaryView) getView()).onRequestCallActivity(this.mActivity);
        }
        return true;
    }

    private void setNewActivityType(@NonNull Activity activity) {
        activity.setType(ActivityType.getActivityTypeByName(getSession(), "call"));
    }

    private long getDurationWithMinimumLengthOfOneMinuteInMillis(@IntRange(from = 0) long durationInMillis) {
        long oneMinuteInMillis = TimeUnit.MINUTES.toMillis(1);
        return durationInMillis < oneMinuteInMillis ? oneMinuteInMillis : durationInMillis;
    }

    private void setCommonFieldsAndSendToView(@Nullable Activity activity) {
        this.mActivity = activity;
        if (activity != null) {
            setDone(true);
            long assignedUserId = this.mActivity.getAssignedUserId();
            if (assignedUserId == 0 || assignedUserId != getSession().getAuthenticatedUserID()) {
                this.mActivity.setAssignedUserId(getSession().getAuthenticatedUserID());
            }
            activity.setActivityStartAt(PipedriveDateTime.instanceFromDate(new Date(this.mStartTimeInMillis)), PipedriveDuration.instanceFromUnixTimeRepresentation(Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(getDurationWithMinimumLengthOfOneMinuteInMillis(this.mDurationInMillis)))));
            if (getView() != null) {
                ((CallSummaryView) getView()).onRequestCallActivity(activity);
            }
            this.mOriginalActivity = activity.getDeepCopy();
        }
    }

    public void changeCallActivity() {
        if (this.mActivity != null) {
            boolean callSummaryRequestedThroughActivity;
            boolean operationIsUpdate = this.mActivity.isStored();
            Long currentCallSummaryChangedActivitySqlId = getSession().getRuntimeCache().callSummaryChangedActivitySqlId;
            if (currentCallSummaryChangedActivitySqlId == null || currentCallSummaryChangedActivitySqlId.longValue() != 0) {
                callSummaryRequestedThroughActivity = false;
            } else {
                callSummaryRequestedThroughActivity = true;
            }
            if (callSummaryRequestedThroughActivity) {
                getSession().getRuntimeCache().callSummaryChangedActivitySqlId = operationIsUpdate ? Long.valueOf(this.mActivity.getSqlId()) : null;
            }
            if (operationIsUpdate) {
                if (!AsyncTask.isExecuting(getSession(), UpdateActivityTask.class)) {
                    new UpdateActivityTask(getSession(), this.UPDATE_TASK_CALLBACK).executeOnAsyncTaskExecutor(this, new Activity[]{this.mActivity});
                }
            } else if (!AsyncTask.isExecuting(getSession(), CreateActivityTask.class)) {
                new CreateActivityTask(getSession(), this.CREATE_TASK_CALLBACK).executeOnAsyncTaskExecutor(this, new Activity[]{this.mActivity});
            }
        }
    }

    protected void saveToBundle(@NonNull Bundle bundle) {
        bundle.putParcelable(STORAGE_KEY_ACTIVITY, this.mActivity);
        bundle.putParcelable(STORAGE_KEY_ORIGINAL_ACTIVITY, this.mOriginalActivity);
    }

    protected void restoreFromBundle(@NonNull Bundle bundle) {
        this.mActivity = (Activity) bundle.getParcelable(STORAGE_KEY_ACTIVITY);
        this.mOriginalActivity = (Activity) bundle.getParcelable(STORAGE_KEY_ORIGINAL_ACTIVITY);
    }

    protected String[] getKeysNeededForRestore() {
        return new String[]{STORAGE_KEY_ACTIVITY, STORAGE_KEY_ORIGINAL_ACTIVITY};
    }
}
