package rx;

import java.util.concurrent.atomic.AtomicBoolean;
import rx.plugins.RxJavaHooks;
import rx.subscriptions.CompositeSubscription;

class Completable$3 implements Completable$OnSubscribe {
    final /* synthetic */ Completable[] val$sources;

    Completable$3(Completable[] completableArr) {
        this.val$sources = completableArr;
    }

    public void call(final CompletableSubscriber s) {
        final CompositeSubscription set = new CompositeSubscription();
        s.onSubscribe(set);
        final AtomicBoolean once = new AtomicBoolean();
        CompletableSubscriber inner = new CompletableSubscriber() {
            public void onCompleted() {
                if (once.compareAndSet(false, true)) {
                    set.unsubscribe();
                    s.onCompleted();
                }
            }

            public void onError(Throwable e) {
                if (once.compareAndSet(false, true)) {
                    set.unsubscribe();
                    s.onError(e);
                    return;
                }
                RxJavaHooks.onError(e);
            }

            public void onSubscribe(Subscription d) {
                set.add(d);
            }
        };
        Completable[] arr$ = this.val$sources;
        int len$ = arr$.length;
        int i$ = 0;
        while (i$ < len$) {
            Completable c = arr$[i$];
            if (!set.isUnsubscribed()) {
                if (c == null) {
                    NullPointerException npe = new NullPointerException("One of the sources is null");
                    if (once.compareAndSet(false, true)) {
                        set.unsubscribe();
                        s.onError(npe);
                        return;
                    }
                    RxJavaHooks.onError(npe);
                    return;
                } else if (!once.get() && !set.isUnsubscribed()) {
                    c.unsafeSubscribe(inner);
                    i$++;
                } else {
                    return;
                }
            }
            return;
        }
    }
}
