package com.pipedrive.flow.views;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.flow.adapter.FlowAdapter;
import com.pipedrive.flow.model.FlowDataSource;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.views.DividerItemDecoration;

public abstract class FlowView extends FrameLayout {
    private FlowDataSource mFlowDataSource;
    @Nullable
    protected OnItemClickListener mOnItemClickListener;
    @NonNull
    protected RecyclerView mRecyclerView;
    @NonNull
    protected Long mSqlId;

    public interface OnItemClickListener {
        void onActivityClicked(long j);

        void onActivityDoneChanged(long j, boolean z);

        void onEmailClicked(long j);

        void onFileClicked(long j);

        void onNewActivityClicked();

        void onNoteClicked(long j);
    }

    @NonNull
    protected abstract Cursor getPastFlowCursor(@NonNull Session session);

    @NonNull
    protected abstract Cursor getPlannedFlowCursor(@NonNull Session session);

    public FlowView(@NonNull Context context) {
        this(context, null);
    }

    public FlowView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FlowView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mRecyclerView = new RecyclerView(context, attrs, defStyle);
        this.mRecyclerView.setId(R.id.list);
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(context, 1, false));
        this.mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext()));
        addPaddingForFAB();
        addView(this.mRecyclerView, new LayoutParams(-1, -1));
    }

    private void addPaddingForFAB() {
        this.mRecyclerView.setPadding(this.mRecyclerView.getPaddingLeft(), this.mRecyclerView.getPaddingTop(), this.mRecyclerView.getPaddingRight(), this.mRecyclerView.getPaddingBottom() + ViewUtil.getFooterForListWithFloatingButtonHeight(getContext()));
        this.mRecyclerView.setClipToPadding(false);
    }

    @NonNull
    protected FlowAdapter createFlowAdapter(@NonNull Session session) {
        FlowAdapter flowAdapter = new FlowAdapter(session, getPlannedFlowCursor(session), getPastFlowCursor(session));
        flowAdapter.setOnItemClickListener(this.mOnItemClickListener);
        return flowAdapter;
    }

    @NonNull
    protected FlowDataSource getFlowDataSource(@NonNull Session session) {
        if (this.mFlowDataSource == null) {
            this.mFlowDataSource = new FlowDataSource(session);
        }
        return this.mFlowDataSource;
    }

    public void setup(@NonNull Session session, @NonNull Long sqlId, @Nullable OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
        this.mSqlId = sqlId;
        this.mRecyclerView.setAdapter(createFlowAdapter(session));
    }

    public void refreshContent(@NonNull Session session) {
        this.mRecyclerView.swapAdapter(createFlowAdapter(session), false);
    }
}
