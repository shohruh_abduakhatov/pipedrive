package com.pipedrive.nearby.map;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.annotation.WorkerThread;
import android.util.Pair;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pipedrive.application.Session;
import com.pipedrive.logging.Log;
import com.pipedrive.model.Activity;
import com.pipedrive.nearby.model.NearbyItem;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

class MarkerManager {
    @NonNull
    private final BitmapHelper bitmapHelper;
    @NonNull
    private final GoogleMap googleMap;
    @NonNull
    private final Map<Marker, NearbyItem> markers = new HashMap();
    @Nullable
    private Marker selectedMarker;
    @NonNull
    private final Session session;

    MarkerManager(@NonNull Session session, @NonNull GoogleMap googleMap, @NonNull BitmapHelper bitmapHelper) {
        this.session = session;
        this.googleMap = googleMap;
        this.bitmapHelper = bitmapHelper;
    }

    @NonNull
    private Bitmap getBitmapForMarker(@Nullable Activity activity, boolean isMarkerSelected) {
        if (activity == null) {
            return this.bitmapHelper.getIconNoCircle(isMarkerSelected);
        }
        Boolean dueToday = activity.isDueToday();
        if (dueToday != null && dueToday.booleanValue()) {
            return this.bitmapHelper.getIconGreenCircle(isMarkerSelected);
        }
        Boolean overdue = activity.isOverdue();
        if (overdue == null || !overdue.booleanValue()) {
            return this.bitmapHelper.getIconGreyCircle(isMarkerSelected);
        }
        return this.bitmapHelper.getIconRedCircle(isMarkerSelected);
    }

    @WorkerThread
    @NonNull
    private BitmapDescriptor getIconForNearbyItem(@NonNull NearbyItem<?> nearbyItem, final boolean isSelected) {
        return nearbyItem.isAggregated().booleanValue() ? BitmapDescriptorFactory.fromBitmap(getBitmapForAggregatedMarker(nearbyItem, isSelected)) : (BitmapDescriptor) nearbyItem.getNextActivity(this.session).singleOrDefault(null).map(new Func1<Activity, BitmapDescriptor>() {
            public BitmapDescriptor call(Activity activity) {
                return BitmapDescriptorFactory.fromBitmap(MarkerManager.this.getBitmapForMarker(activity, isSelected));
            }
        }).toBlocking().single();
    }

    @NonNull
    private Bitmap getBitmapForAggregatedMarker(@NonNull NearbyItem<?> nearbyItem, final boolean isSelected) {
        return (Bitmap) nearbyItem.getItemSqlIds().count().map(new Func1<Integer, Bitmap>() {
            public Bitmap call(Integer count) {
                return MarkerManager.this.bitmapHelper.getAggregatedIcon(count.intValue(), isSelected);
            }
        }).toBlocking().single();
    }

    private void updateMarkerIconWithNextActivityStatus(@NonNull final Marker marker, @NonNull NearbyItem<?> item, boolean isSelected) {
        Observable.just(getIconForNearbyItem(item, isSelected)).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<BitmapDescriptor>() {
            public void call(BitmapDescriptor icon) {
                marker.setIcon(icon);
            }
        }, logError());
    }

    @NonNull
    private Action1<Throwable> logError() {
        return new Action1<Throwable>() {
            public void call(Throwable throwable) {
                Log.e(throwable);
            }
        };
    }

    void selectMarker(@NonNull Marker marker) {
        unSelectAllMarkers();
        this.selectedMarker = marker;
        updateMarkerIconWithNextActivityStatus(marker, (NearbyItem) this.markers.get(marker), true);
    }

    private void unSelectMarker(@Nullable Marker marker) {
        if (marker != null) {
            updateMarkerIconWithNextActivityStatus(marker, (NearbyItem) this.markers.get(marker), false);
        }
    }

    @UiThread
    public void setItems(@NonNull Observable<NearbyItem> items) {
        clearMarkers();
        items.observeOn(Schedulers.computation()).map(new Func1<NearbyItem, Pair<NearbyItem, BitmapDescriptor>>() {
            public Pair<NearbyItem, BitmapDescriptor> call(NearbyItem nearbyItem) {
                return Pair.create(nearbyItem, MarkerManager.this.getIconForNearbyItem(nearbyItem, false));
            }
        }).observeOn(AndroidSchedulers.mainThread()).forEach(new Action1<Pair<NearbyItem, BitmapDescriptor>>() {
            public void call(Pair<NearbyItem, BitmapDescriptor> item) {
                MarkerManager.this.markers.put(MarkerManager.this.googleMap.addMarker(new MarkerOptions().position(((NearbyItem) item.first).getItemLatLng()).icon((BitmapDescriptor) item.second)), item.first);
            }
        });
    }

    void clearMarkers() {
        this.markers.clear();
        this.selectedMarker = null;
        this.googleMap.clear();
    }

    void unSelectAllMarkers() {
        if (this.selectedMarker != null) {
            unSelectMarker(this.selectedMarker);
        }
    }

    void selectMarkerForItem(@NonNull NearbyItem nearbyItem) {
        for (Entry<Marker, NearbyItem> entry : this.markers.entrySet()) {
            if (((NearbyItem) entry.getValue()).equals(nearbyItem)) {
                selectMarker((Marker) entry.getKey());
                return;
            }
        }
    }

    @Nullable
    NearbyItem getNearbyItemForMarker(@NonNull Marker marker) {
        return (NearbyItem) this.markers.get(marker);
    }

    @Nullable
    Marker getCurrentlySelectedMarker() {
        return this.selectedMarker;
    }
}
