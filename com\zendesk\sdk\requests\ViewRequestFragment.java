package com.zendesk.sdk.requests;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.background.ApplicationStateMonitor;
import com.newrelic.agent.android.instrumentation.Instrumented;
import com.newrelic.agent.android.tracing.TraceMachine;
import com.zendesk.belvedere.BelvedereCallback;
import com.zendesk.belvedere.BelvedereResult;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.sdk.attachment.AttachmentHelper;
import com.zendesk.sdk.attachment.ImageUploadHelper;
import com.zendesk.sdk.attachment.ImageUploadHelper.ImageUploadProgressListener;
import com.zendesk.sdk.attachment.ZendeskBelvedereProvider;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinking;
import com.zendesk.sdk.deeplinking.actions.ActionRefreshComments;
import com.zendesk.sdk.deeplinking.actions.ActionRefreshComments.ActionRefreshCommentsData;
import com.zendesk.sdk.deeplinking.actions.ActionRefreshComments.ActionRefreshCommentsListener;
import com.zendesk.sdk.feedback.ui.AttachmentContainerHost;
import com.zendesk.sdk.feedback.ui.AttachmentContainerHost.AttachmentContainerListener;
import com.zendesk.sdk.model.request.Attachment;
import com.zendesk.sdk.model.request.Comment;
import com.zendesk.sdk.model.request.CommentResponse;
import com.zendesk.sdk.model.request.CommentsResponse;
import com.zendesk.sdk.model.request.EndUserComment;
import com.zendesk.sdk.model.request.UploadResponse;
import com.zendesk.sdk.model.request.User;
import com.zendesk.sdk.model.settings.SafeMobileSettings;
import com.zendesk.sdk.network.RequestProvider;
import com.zendesk.sdk.network.Retryable;
import com.zendesk.sdk.network.SubmissionListener;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.ui.TextWatcherAdapter;
import com.zendesk.sdk.util.NetworkUtils;
import com.zendesk.sdk.util.UiUtils;
import com.zendesk.service.ErrorResponse;
import com.zendesk.service.ErrorResponseAdapter;
import com.zendesk.service.ZendeskCallback;
import com.zendesk.util.CollectionUtils;
import com.zendesk.util.StringUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Instrumented
public class ViewRequestFragment extends Fragment implements ActionRefreshCommentsListener, TraceFieldInterface {
    public static final String EXTRA_REQUEST_ID = "requestId";
    public static final String EXTRA_REQUEST_SUBJECT = "subject";
    private static final int FOURTY_PERCENT_OPACITY = 102;
    private static final int FULL_OPACTITY = 255;
    private static final String TAG = "ViewRequestFragment";
    private Button mAttachmentButton;
    private AttachmentContainerHost mAttachmentContainerHost;
    private AttachmentContainerListener mAttachmentContainerListener;
    private RequestCommentsListAdapter mCommentListAdapter;
    private ZendeskCallback<CommentsResponse> mCommentListCallback;
    private ListView mCommentListView;
    private EditText mEditText;
    private ImageUploadHelper mImageUploadHelper;
    private ImageUploadProgressListener mImageUploadProgressListener;
    private BelvedereCallback<List<BelvedereResult>> mImagesExtracted;
    private ZendeskCallback<Uri> mLoadImageCallback;
    private ProgressBar mProgressBar;
    private String mRequestId;
    private Retryable mRetryable;
    private Button mSendButton;
    private String mSubject;
    private SubmissionListener mSubmissionListener;
    private ZendeskCallback<Comment> mSubmitCommentCallback;

    private enum Action {
        SEND_COMMENT,
        LOAD_COMMENTS,
        LOAD_IMAGE
    }

    protected void onStart() {
        super.onStart();
        ApplicationStateMonitor.getInstance().activityStarted();
    }

    protected void onStop() {
        super.onStop();
        ApplicationStateMonitor.getInstance().activityStopped();
    }

    public static ViewRequestFragment newInstance(String requestId) {
        return newInstance(requestId, null, null);
    }

    public static ViewRequestFragment newInstance(String requestId, String subject) {
        return newInstance(requestId, subject, null);
    }

    public static ViewRequestFragment newInstance(String requestId, String subject, SubmissionListener listener) {
        Bundle fragmentArgs = new Bundle();
        fragmentArgs.putString("requestId", requestId);
        if (StringUtils.hasLength(subject)) {
            fragmentArgs.putString("subject", subject);
        }
        ViewRequestFragment viewRequestFragment = new ViewRequestFragment();
        viewRequestFragment.setArguments(fragmentArgs);
        viewRequestFragment.setRetainInstance(true);
        viewRequestFragment.setSubmissionListener(listener);
        return viewRequestFragment;
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        try {
            TraceMachine.enterMethod(this._nr_trace, "ViewRequestFragment#onCreateView", null);
        } catch (NoSuchFieldError e) {
            while (true) {
                TraceMachine.enterMethod(null, "ViewRequestFragment#onCreateView", null);
            }
        }
        View fragmentLayout = layoutInflater.inflate(R.layout.fragment_view_request, viewGroup, false);
        this.mEditText = (EditText) fragmentLayout.findViewById(R.id.view_request_comment_edittext);
        this.mSendButton = (Button) fragmentLayout.findViewById(R.id.view_request_comment_send_bth);
        this.mAttachmentButton = (Button) fragmentLayout.findViewById(R.id.view_request_comment_attachment_bth);
        this.mProgressBar = (ProgressBar) fragmentLayout.findViewById(R.id.view_request_fragment_progress);
        this.mCommentListView = (ListView) fragmentLayout.findViewById(R.id.view_request_comment_list);
        this.mEditText.addTextChangedListener(new TextWatcherAdapter() {
            public void afterTextChanged(Editable editable) {
                ViewRequestFragment.this.checkSendButtonState();
            }
        });
        this.mAttachmentContainerHost = (AttachmentContainerHost) fragmentLayout.findViewById(R.id.view_request_attachment_container);
        this.mAttachmentContainerHost.setParent(fragmentLayout.findViewById(R.id.view_request_comment_attachment_container));
        this.mAttachmentContainerHost.setState(this.mImageUploadHelper);
        this.mAttachmentContainerHost.setAttachmentContainerListener(this.mAttachmentContainerListener);
        this.mSendButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                ViewRequestFragment.this.sendComment();
            }
        });
        setSendButtonVisibility(false);
        setHasOptionsMenu(true);
        SafeMobileSettings storedSettings = ZendeskConfig.INSTANCE.getMobileSettings();
        if (storedSettings != null && AttachmentHelper.isAttachmentSupportEnabled(storedSettings) && canGetAttachments()) {
            UiUtils.setVisibility(this.mAttachmentButton, 0);
            UiUtils.setVisibility(this.mSendButton, 8);
            setEditTextMarginRight(true);
            this.mAttachmentButton.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    ZendeskBelvedereProvider.INSTANCE.getBelvedere(ViewRequestFragment.this.getContext()).showDialog(ViewRequestFragment.this.getChildFragmentManager());
                }
            });
        } else {
            UiUtils.setVisibility(this.mAttachmentButton, 8);
            UiUtils.setVisibility(this.mSendButton, 8);
            setEditTextMarginRight(false);
        }
        TraceMachine.exitMethod();
        return fragmentLayout;
    }

    public void onCreate(Bundle bundle) {
        TraceMachine.startTracing(TAG);
        try {
            TraceMachine.enterMethod(this._nr_trace, "ViewRequestFragment#onCreate", null);
        } catch (NoSuchFieldError e) {
            while (true) {
                TraceMachine.enterMethod(null, "ViewRequestFragment#onCreate", null);
            }
        }
        super.onCreate(bundle);
        this.mImageUploadProgressListener = new ImageUploadProgressListener() {
            public void allImagesUploaded(Map<File, UploadResponse> map) {
                ViewRequestFragment.this.checkSendButtonState();
            }

            public void imageUploaded(UploadResponse uploadResponse, BelvedereResult file) {
                ViewRequestFragment.this.mAttachmentContainerHost.setAttachmentUploaded(file.getFile());
            }

            public void imageUploadError(ErrorResponse errorResponse, BelvedereResult file) {
                AttachmentHelper.showAttachmentTryAgainDialog(ViewRequestFragment.this.getActivity(), file, errorResponse, ViewRequestFragment.this.mImageUploadHelper, ViewRequestFragment.this.mAttachmentContainerHost);
            }
        };
        this.mImageUploadHelper = new ImageUploadHelper(this.mImageUploadProgressListener, ZendeskConfig.INSTANCE.provider().uploadProvider());
        TraceMachine.exitMethod();
    }

    public void onResume() {
        super.onResume();
        this.mRequestId = getArguments().getString("requestId");
        this.mSubject = getArguments().getString("subject");
        setupCallbacks();
        if (this.mCommentListView.getAdapter() == null) {
            loadRequest();
        }
        ZendeskDeepLinking.INSTANCE.registerAction(this, new ActionRefreshComments(this.mRequestId));
    }

    public void onPause() {
        super.onPause();
        tearDownCallbacks();
        ZendeskDeepLinking.INSTANCE.unregisterAction(this);
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.mAttachmentContainerListener = new AttachmentContainerListener() {
            public void attachmentRemoved(File file) {
                ViewRequestFragment.this.mImageUploadHelper.removeImage(file);
            }
        };
        if (context instanceof Retryable) {
            this.mRetryable = (Retryable) context;
        }
    }

    public void onDetach() {
        super.onDetach();
        this.mAttachmentContainerListener = null;
        this.mRetryable = null;
    }

    public void onDestroy() {
        super.onDestroy();
        this.mImageUploadHelper.setImageUploadProgressListener(null);
        this.mImageUploadHelper.deleteAllAttachmentsBeforeShutdown();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.mImagesExtracted = new BelvedereCallback<List<BelvedereResult>>() {
            public void success(List<BelvedereResult> result) {
                SafeMobileSettings storedSettings = ZendeskConfig.INSTANCE.getMobileSettings();
                if (storedSettings != null) {
                    AttachmentHelper.processAndUploadSelectedFiles(result, ViewRequestFragment.this.mImageUploadHelper, ViewRequestFragment.this.getActivity(), ViewRequestFragment.this.mAttachmentContainerHost, storedSettings);
                }
                ViewRequestFragment.this.checkSendButtonState();
                ViewRequestFragment.this.setLoadingVisibility(false);
            }
        };
        setLoadingVisibility(true);
        ZendeskBelvedereProvider.INSTANCE.getBelvedere(getContext()).getFilesFromActivityOnResult(requestCode, resultCode, data, this.mImagesExtracted);
    }

    public void setSubmissionListener(SubmissionListener submissionListener) {
        this.mSubmissionListener = submissionListener;
    }

    void onNetworkAvailable() {
        checkSendButtonState();
    }

    void onNetworkUnavailable() {
        setAttachmentButtonVisibility(false);
        setSendButtonVisibility(false);
    }

    private void setupCallbacks() {
        this.mCommentListCallback = new ZendeskCallback<CommentsResponse>() {
            public void onSuccess(CommentsResponse result) {
                ZendeskConfig.INSTANCE.storage().requestStorage().setCommentCount(ViewRequestFragment.this.mRequestId, result.getComments().size());
                ViewRequestFragment.this.setLoadingVisibility(false);
                ViewRequestFragment.this.renderComments(result);
            }

            public void onError(ErrorResponse error) {
                ViewRequestFragment.this.setLoadingVisibility(false);
                ViewRequestFragment.this.handleError(error, Action.LOAD_COMMENTS);
            }
        };
        this.mSubmitCommentCallback = new ZendeskCallback<Comment>() {
            public void onSuccess(Comment result) {
                Integer commentCount = ZendeskConfig.INSTANCE.storage().requestStorage().getCommentCount(ViewRequestFragment.this.mRequestId);
                Integer listItemCount = ViewRequestFragment.this.mCommentListAdapter != null ? Integer.valueOf(ViewRequestFragment.this.mCommentListAdapter.getCount() + 1) : null;
                if (commentCount == null || listItemCount == null || listItemCount.intValue() >= commentCount.intValue()) {
                    ViewRequestFragment.this.setLoadingVisibility(false);
                    CommentResponse submittedComment = new CommentResponse();
                    submittedComment.setCreatedAt(new Date());
                    submittedComment.setBody(ViewRequestFragment.this.mEditText.getText().toString());
                    submittedComment.setAttachments(ViewRequestFragment.this.mImageUploadHelper.getUploadedAttachments());
                    ViewRequestFragment.this.mCommentListAdapter.insert(CommentWithUser.build(submittedComment, new User()), 0);
                    ViewRequestFragment.this.checkSendButtonState();
                } else {
                    ViewRequestFragment.this.loadRequest();
                }
                if (ViewRequestFragment.this.mSubmissionListener != null) {
                    ViewRequestFragment.this.mSubmissionListener.onSubmissionCompleted();
                }
                ViewRequestFragment.this.mEditText.setText("");
                ViewRequestFragment.this.mImageUploadHelper.reset();
                ViewRequestFragment.this.mAttachmentContainerHost.reset();
            }

            public void onError(ErrorResponse error) {
                ViewRequestFragment.this.setLoadingVisibility(false);
                if (ViewRequestFragment.this.mSubmissionListener != null) {
                    ViewRequestFragment.this.mSubmissionListener.onSubmissionError(error);
                }
                ViewRequestFragment.this.handleError(error, Action.SEND_COMMENT);
            }
        };
        this.mLoadImageCallback = new ZendeskCallback<Uri>() {
            public void onSuccess(Uri result) {
                ViewRequestFragment.this.setLoadingVisibility(false);
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                intent.setDataAndType(result, "image/*");
                ZendeskBelvedereProvider.INSTANCE.getBelvedere(ViewRequestFragment.this.getContext()).grantPermissionsForUri(intent, result);
                try {
                    ViewRequestFragment.this.startActivity(intent);
                } catch (Exception e) {
                    onError(new ErrorResponseAdapter("Couldn't start activity to view attachment"));
                }
            }

            public void onError(ErrorResponse error) {
                ViewRequestFragment.this.setLoadingVisibility(false);
                ViewRequestFragment.this.handleError(error, Action.LOAD_IMAGE);
            }
        };
    }

    private void tearDownCallbacks() {
        this.mCommentListCallback = null;
        this.mSubmitCommentCallback = null;
        this.mLoadImageCallback = null;
        this.mImagesExtracted = null;
    }

    private void loadRequest() {
        setTitle(this.mSubject);
        setLoadingVisibility(true);
        try {
            ZendeskConfig.INSTANCE.provider().requestProvider().getComments(this.mRequestId, this.mCommentListCallback);
        } catch (RuntimeException e) {
            if (getActivity() != null) {
                getActivity().finish();
            }
        }
    }

    private void loadImage(Attachment attachment) {
        setLoadingVisibility(true);
        ImageLoader.INSTANCE.loadAndShowImage(attachment, getActivity(), this.mLoadImageCallback, ZendeskBelvedereProvider.INSTANCE.getBelvedere(getContext()));
    }

    private void handleError(ErrorResponse error, Action action) {
        if (isVisible()) {
            Logger.w(TAG, "Failed to complete action: Status: " + error.getStatus() + " Reason: " + error.getReason(), new Object[0]);
            String message = null;
            OnClickListener retryAction = null;
            switch (action) {
                case SEND_COMMENT:
                    message = getString(R.string.view_request_send_comment_error);
                    retryAction = new OnClickListener() {
                        public void onClick(View v) {
                            ViewRequestFragment.this.sendComment();
                        }
                    };
                    break;
                case LOAD_COMMENTS:
                    message = getString(R.string.view_request_load_comments_error);
                    retryAction = new OnClickListener() {
                        public void onClick(View v) {
                            ViewRequestFragment.this.loadRequest();
                        }
                    };
                    break;
                case LOAD_IMAGE:
                    retryAction = null;
                    break;
            }
            if (this.mRetryable != null) {
                this.mRetryable.onRetryAvailable(message, retryAction);
            }
        }
    }

    private void renderComments(CommentsResponse response) {
        List<CommentWithUser> commentWithUserList = new ArrayList(response.getComments().size());
        for (CommentResponse comment : response.getComments()) {
            CommentWithUser commentWithUser = CommentWithUser.build(comment, response.getUsers());
            if (commentWithUser != null) {
                commentWithUserList.add(commentWithUser);
            } else {
                boolean z;
                Locale locale = Locale.US;
                String str = "Comment is null %s, users list is empty %s";
                Object[] objArr = new Object[2];
                if (comment == null) {
                    z = true;
                } else {
                    z = false;
                }
                objArr[0] = Boolean.valueOf(z);
                objArr[1] = Boolean.valueOf(CollectionUtils.isEmpty(response.getUsers()));
                Logger.w(TAG, String.format(locale, str, objArr), new Object[0]);
            }
        }
        if (getActivity() != null) {
            this.mCommentListAdapter = new RequestCommentsListAdapter(getActivity(), R.layout.row_agent_comment, commentWithUserList, new AttachmentOnClickListener() {
                public void onAttachmentClicked(View view, Attachment attachment) {
                    ViewRequestFragment.this.loadImage(attachment);
                }
            });
            this.mCommentListView.setAdapter(this.mCommentListAdapter);
        }
    }

    private void setTitle(String title) {
        if (StringUtils.hasLength(title)) {
            Activity activity = getActivity();
            if (activity instanceof AppCompatActivity) {
                ((AppCompatActivity) activity).setTitle(title);
            }
        }
    }

    private void sendComment() {
        setLoadingVisibility(true);
        if (this.mSubmissionListener != null) {
            this.mSubmissionListener.onSubmissionStarted();
        }
        RequestProvider provider = ZendeskConfig.INSTANCE.provider().requestProvider();
        EndUserComment endUserComment = new EndUserComment();
        endUserComment.setValue(this.mEditText.getText().toString());
        endUserComment.setAttachments(this.mImageUploadHelper.getUploadTokens());
        provider.addComment(this.mRequestId, endUserComment, this.mSubmitCommentCallback);
    }

    private void checkSendButtonState() {
        if (isVisible()) {
            boolean shouldShowSendButton;
            boolean shouldShowAttachmentButton;
            if (this.mEditText != null && StringUtils.hasLength(this.mEditText.getText().toString()) && NetworkUtils.isConnected(getActivity())) {
                shouldShowSendButton = true;
            } else {
                shouldShowSendButton = false;
            }
            SafeMobileSettings storedSettings = ZendeskConfig.INSTANCE.getMobileSettings();
            if (storedSettings != null && AttachmentHelper.isAttachmentSupportEnabled(storedSettings) && canGetAttachments()) {
                shouldShowAttachmentButton = true;
            } else {
                shouldShowAttachmentButton = false;
            }
            boolean imagesUploaded;
            if (this.mImageUploadHelper == null || !this.mImageUploadHelper.isImageUploadCompleted()) {
                imagesUploaded = false;
            } else {
                imagesUploaded = true;
            }
            if (shouldShowSendButton && imagesUploaded) {
                setAttachmentButtonVisibility(false);
                setSendButtonVisibility(true);
                this.mSendButton.setEnabled(true);
                this.mSendButton.getBackground().setAlpha(255);
            } else if (shouldShowSendButton) {
                setAttachmentButtonVisibility(false);
                setSendButtonVisibility(true);
                this.mSendButton.setEnabled(false);
                this.mSendButton.getBackground().setAlpha(102);
            } else {
                setSendButtonVisibility(false);
                if (shouldShowAttachmentButton) {
                    setAttachmentButtonVisibility(true);
                }
            }
        }
    }

    private void setSendButtonVisibility(boolean isVisible) {
        if (this.mSendButton != null) {
            this.mSendButton.setVisibility(isVisible ? 0 : 8);
        }
        setEditTextMarginRight(isVisible);
    }

    private void setAttachmentButtonVisibility(boolean isVisible) {
        if (this.mSendButton != null) {
            this.mAttachmentButton.setVisibility(isVisible ? 0 : 8);
        }
        setEditTextMarginRight(isVisible);
    }

    private void setEditTextMarginRight(boolean isButtonVisible) {
        if (this.mEditText != null) {
            LayoutParams params = (LayoutParams) this.mEditText.getLayoutParams();
            if (isButtonVisible) {
                params.rightMargin = UiUtils.dpToPixels(0, getResources().getDisplayMetrics());
            } else {
                params.rightMargin = getResources().getDimensionPixelSize(R.dimen.view_request_horizontal_margin);
            }
            this.mEditText.setLayoutParams(params);
        }
    }

    private boolean canGetAttachments() {
        return ZendeskBelvedereProvider.INSTANCE.getBelvedere(getContext()).oneOrMoreSourceAvailable();
    }

    private void setLoadingVisibility(boolean isVisible) {
        boolean z = true;
        if (isVisible && this.mRetryable != null) {
            this.mRetryable.onRetryUnavailable();
        }
        if (this.mProgressBar != null) {
            this.mProgressBar.setVisibility(isVisible ? 0 : 4);
        }
        if (this.mEditText != null) {
            boolean z2;
            EditText editText = this.mEditText;
            if (isVisible) {
                z2 = false;
            } else {
                z2 = true;
            }
            editText.setEnabled(z2);
        }
        if (this.mAttachmentContainerHost != null) {
            AttachmentContainerHost attachmentContainerHost = this.mAttachmentContainerHost;
            if (isVisible) {
                z = false;
            }
            attachmentContainerHost.setAttachmentsDeletable(z);
        }
        if (this.mSendButton != null && this.mSendButton.getVisibility() == 0 && isVisible) {
            this.mSendButton.setEnabled(false);
            this.mSendButton.getBackground().setAlpha(102);
        }
    }

    public void refreshComments(ActionRefreshCommentsData data) {
        loadRequest();
    }
}
