package com.pipedrive.util.networking.entities.product;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.pipedrive.gson.GsonHelper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class ProductEntity$ProductEntityPricesTypeAdapter extends TypeAdapter<List<ProductPriceEntity>> {
    private ProductEntity$ProductEntityPricesTypeAdapter() {
    }

    public void write(JsonWriter writer, List<ProductPriceEntity> list) throws IOException {
        throw new RuntimeException("Not implemented for price JSON creation is not used!");
    }

    public List<ProductPriceEntity> read(JsonReader reader) throws IOException {
        List<ProductPriceEntity> productPriceEntities = new ArrayList();
        if (reader.peek() == JsonToken.NULL) {
            reader.nextNull();
        } else if (reader.peek() == JsonToken.BEGIN_ARRAY) {
            reader.beginArray();
            while (reader.hasNext()) {
                productPriceEntities.add((ProductPriceEntity) GsonHelper.fromJSON(reader, ProductPriceEntity.class));
            }
            reader.endArray();
        } else if (reader.peek() == JsonToken.BEGIN_OBJECT) {
            reader.beginObject();
            while (reader.hasNext()) {
                if (reader.peek() == JsonToken.NULL || reader.peek() != JsonToken.BEGIN_OBJECT) {
                    reader.skipValue();
                } else {
                    productPriceEntities.add((ProductPriceEntity) GsonHelper.fromJSON(reader, ProductPriceEntity.class));
                }
            }
            reader.endObject();
        } else {
            reader.skipValue();
        }
        return productPriceEntities;
    }
}
