package com.pipedrive.views.agenda;

import android.content.Context;
import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.model.Activity;
import com.pipedrive.views.calendar.recyclerview.CalendarRecyclerView;
import com.pipedrive.views.calendar.recyclerview.CalendarScrollListener;
import java.util.Calendar;
import rx.Observable;
import rx.Observable$OnSubscribe;
import rx.Subscriber;
import rx.android.MainThreadSubscription;

public class AgendaView extends FrameLayout implements CalendarScrollListener {
    static final int ITEM_COUNT = 7;
    private final int actionBarHeight;
    private AgendaViewAdapter adapter;
    private final int allDayActivitiesViewMaxHeight;
    private final int calendarWeekViewHeight;
    @Nullable
    private OnCalendarExpandListener mOnCalendarExpandListener;
    @Nullable
    private OnTouchAgendaViewListener mOnTouchAgendaViewListener;
    private final int minVisibleAgendaHeight;
    private final int navigationToolbarHeight;
    private OnActivityClickListener onActivityClickListener;
    private CalendarRecyclerView recyclerView;
    private final int screenHeight;
    @Nullable
    private Session session;
    private final int statusBarHeight;
    private final int totalSizeOfNonAgendaItems;

    public interface OnCalendarExpandListener {
        void onCalendarExpanded();
    }

    public interface OnTouchAgendaViewListener {
        void onTouchAgendaView();
    }

    public AgendaView(Context context) {
        this(context, null);
    }

    public AgendaView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AgendaView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.calendarWeekViewHeight = getResources().getDimensionPixelSize(R.dimen.calendarView.weekHeight);
        this.navigationToolbarHeight = getResources().getDimensionPixelSize(R.dimen.navigationToolbar_height);
        this.actionBarHeight = getResources().getDimensionPixelSize(R.dimen.actionBar_height);
        this.minVisibleAgendaHeight = getResources().getDimensionPixelSize(R.dimen.agendaView.minimalVisibleAgendaHeight);
        this.statusBarHeight = getResources().getDimensionPixelSize(R.dimen.dimen6);
        this.screenHeight = getResources().getDisplayMetrics().heightPixels;
        this.totalSizeOfNonAgendaItems = (((this.calendarWeekViewHeight + this.navigationToolbarHeight) + this.actionBarHeight) + this.minVisibleAgendaHeight) + this.statusBarHeight;
        this.allDayActivitiesViewMaxHeight = this.screenHeight - this.totalSizeOfNonAgendaItems;
        init();
    }

    private void init() {
        this.recyclerView = new CalendarRecyclerView(getContext());
        addView(this.recyclerView, -1, -1);
    }

    public void setup(@NonNull Session session, @NonNull Calendar date, boolean smoothScrollToCurrentTime) {
        this.session = session;
        if (this.adapter != null) {
            this.adapter.unsubscribe();
        }
        this.adapter = new AgendaViewAdapter(session, date, smoothScrollToCurrentTime, this.allDayActivitiesViewMaxHeight);
        this.adapter.setOnActivityClickListener(this.onActivityClickListener);
        this.recyclerView.swapAdapter(this.adapter, false);
        this.recyclerView.scrollToPosition(3);
        setOnCalendarExpandListener(new OnCalendarExpandListener() {
            public void onCalendarExpanded() {
                ((AgendaViewDayViewHolder) AgendaView.this.recyclerView.findViewHolderForAdapterPosition(((LinearLayoutManager) AgendaView.this.recyclerView.getLayoutManager()).findFirstVisibleItemPosition())).collapseAllDayActivitiesList();
            }
        });
    }

    @CheckResult
    @NonNull
    public Observable<Activity> activityClicks() {
        return Observable.create(new Observable$OnSubscribe<Activity>() {
            public void call(final Subscriber<? super Activity> subscriber) {
                MainThreadSubscription.verifyMainThread();
                AgendaView.this.onActivityClickListener = new OnActivityClickListener() {
                    public void onActivityClicked(@NonNull Activity activity) {
                        if (!subscriber.isUnsubscribed()) {
                            subscriber.onNext(activity);
                        }
                    }
                };
                AgendaView.this.adapter.setOnActivityClickListener(AgendaView.this.onActivityClickListener);
                subscriber.add(new MainThreadSubscription() {
                    protected void onUnsubscribe() {
                        AgendaView.this.adapter.setOnActivityClickListener(null);
                    }
                });
            }
        });
    }

    @CheckResult
    @NonNull
    public Observable<Calendar> dateChanges() {
        return Observable.create(new Observable$OnSubscribe<Calendar>() {
            public void call(final Subscriber<? super Calendar> subscriber) {
                MainThreadSubscription.verifyMainThread();
                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) AgendaView.this.recyclerView.getLayoutManager();
                final OnScrollListener onScrollListener = new OnScrollListener() {
                    private int previousPosition = -1;

                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        if (AgendaView.this.adapter != null) {
                            int position = linearLayoutManager.findFirstVisibleItemPosition();
                            if (position != -1) {
                                int currentPosition;
                                View view = linearLayoutManager.findViewByPosition(position);
                                if (Math.abs(view.getX()) > ((float) (view.getWidth() / 2))) {
                                    currentPosition = Math.min(position + 1, linearLayoutManager.getItemCount() - 1);
                                } else {
                                    currentPosition = position;
                                }
                                if (this.previousPosition != currentPosition) {
                                    this.previousPosition = currentPosition;
                                    if (!subscriber.isUnsubscribed()) {
                                        subscriber.onNext(AgendaView.this.adapter.getItem(currentPosition));
                                    }
                                }
                            }
                        }
                    }
                };
                AgendaView.this.recyclerView.addOnScrollListener(onScrollListener);
                subscriber.add(new MainThreadSubscription() {
                    protected void onUnsubscribe() {
                        AgendaView.this.recyclerView.removeOnScrollListener(onScrollListener);
                    }
                });
            }
        });
    }

    public void setOnTouchAgendaViewListener(@NonNull OnTouchAgendaViewListener onTouchAgendaViewListener) {
        this.mOnTouchAgendaViewListener = onTouchAgendaViewListener;
    }

    public void refreshContent() {
        this.adapter.notifyDataSetChanged();
    }

    public void setupForDate(Calendar date) {
        if (this.session != null) {
            setup(this.session, date, false);
        }
    }

    public void onScrollToDate(Calendar date) {
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (this.mOnTouchAgendaViewListener != null) {
            this.mOnTouchAgendaViewListener.onTouchAgendaView();
        }
        return false;
    }

    public void unsubscribe() {
        if (this.adapter != null) {
            this.adapter.unsubscribe();
        }
    }

    private void setOnCalendarExpandListener(@NonNull OnCalendarExpandListener listener) {
        this.mOnCalendarExpandListener = listener;
    }

    public void onCalendarExpandAnimationEnd() {
        if (this.mOnCalendarExpandListener != null) {
            this.mOnCalendarExpandListener.onCalendarExpanded();
        }
    }
}
