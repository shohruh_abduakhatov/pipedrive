package com.google.android.gms.wearable;

import java.io.IOException;

public class ChannelIOException extends IOException {
    private final int aSf;
    private final int aSg;

    public ChannelIOException(String str, int i, int i2) {
        super(str);
        this.aSf = i;
        this.aSg = i2;
    }

    public int getAppSpecificErrorCode() {
        return this.aSg;
    }

    public int getCloseReason() {
        return this.aSf;
    }
}
