package com.pipedrive.person.edit;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.contact.CommunicationMedium;
import com.pipedrive.model.contact.Email;
import com.pipedrive.model.contact.Phone;
import com.pipedrive.model.delta.ObjectDelta;

class PersonEditPresenterImpl extends PersonEditPresenter {
    private static final String STORAGE_KEY_ORIGINAL_PERSON = (PersonEditPresenterImpl.class.getSimpleName() + ".originalPerson");
    private static final String STORAGE_KEY_PERSON = (PersonEditPresenterImpl.class.getSimpleName() + ".person");
    private final OnTaskFinished CREATE_TASK_CALLBACK = new OnTaskFinished() {
        public void onPersonCreated(boolean success) {
            if (PersonEditPresenterImpl.this.getView() != null) {
                ((PersonEditView) PersonEditPresenterImpl.this.getView()).onPersonCreated(success);
            }
        }
    };
    @Nullable
    private final String DEFAULT_EMAIL_LABEL;
    @Nullable
    private final String DEFAULT_PHONE_LABEL;
    private final OnTaskFinished DELETE_TASK_CALLBACK = new OnTaskFinished() {
        public void onPersonDeleted(boolean success) {
            if (PersonEditPresenterImpl.this.getView() != null) {
                ((PersonEditView) PersonEditPresenterImpl.this.getView()).onPersonDeleted(success);
            }
        }
    };
    private final OnTaskFinished READ_TASK_CALLBACK = new OnTaskFinished() {
        public void onPersonRead(@Nullable Person person) {
            PersonEditPresenterImpl.this.cacheAndSendPersonToView(person);
        }
    };
    private final OnTaskFinished UPDATE_TASK_CALLBACK = new OnTaskFinished() {
        public void onPersonUpdated(boolean success) {
            if (PersonEditPresenterImpl.this.getView() != null) {
                ((PersonEditView) PersonEditPresenterImpl.this.getView()).onPersonUpdated(success);
            }
        }
    };
    @NonNull
    private final ObjectDelta<Person> mContactObjectDelta = new ObjectDelta(Person.class);
    @Nullable
    private Person mOriginalPerson;
    @Nullable
    private Person mPerson;

    public PersonEditPresenterImpl(@NonNull Session session, @Nullable Bundle savedState) {
        String str;
        String str2 = null;
        super(session, savedState);
        String[] phoneLabels = session.getApplicationContext().getResources().getStringArray(R.array.labels_phone);
        if (phoneLabels != null) {
            str = phoneLabels[0];
        } else {
            str = null;
        }
        this.DEFAULT_PHONE_LABEL = str;
        String[] emailLabels = session.getApplicationContext().getResources().getStringArray(R.array.labels_email);
        if (emailLabels != null) {
            str2 = emailLabels[0];
        }
        this.DEFAULT_EMAIL_LABEL = str2;
    }

    void requestOrRestorePerson(@NonNull Long personSqlId) {
        if (!restorePerson()) {
            new ReadPersonTasks(getSession(), this.READ_TASK_CALLBACK).executeOnAsyncTaskExecutor(this, new Long[]{personSqlId});
        }
    }

    void requestOrRestoreNewPerson() {
        if (!restorePerson()) {
            setCommonFieldsAndSendToView(new Person());
        }
    }

    void requestOrRestoreNewPersonWithName(@Nullable String name) {
        if (!restorePerson()) {
            Person person = new Person();
            if (name != null) {
                person.setName(name);
            }
            setCommonFieldsAndSendToView(person);
        }
    }

    private void setCommonFieldsAndSendToView(@NonNull Person person) {
        Session session = getSession();
        person.setOwnerPipedriveId((int) session.getAuthenticatedUserID());
        person.setOwnerName(session.getUserSettingsName(null));
        int userSettingsDefaultVisibilityForDeal = session.getUserSettingsDefaultVisibilityForDeal(Integer.MIN_VALUE);
        if (userSettingsDefaultVisibilityForDeal != Integer.MIN_VALUE) {
            person.setVisibleTo(userSettingsDefaultVisibilityForDeal);
        }
        person.getPhones().add(new Phone("", this.DEFAULT_PHONE_LABEL));
        person.getEmails().add(new Email("", this.DEFAULT_EMAIL_LABEL));
        cacheAndSendPersonToView(person);
    }

    void changePerson() {
        if (this.mPerson != null) {
            if (this.mPerson.isStored()) {
                new UpdatePersonTask(getSession(), this.UPDATE_TASK_CALLBACK).executeOnAsyncTaskExecutor(this, new Person[]{this.mPerson});
                return;
            }
            new CreatePersonTask(getSession(), this.CREATE_TASK_CALLBACK).executeOnAsyncTaskExecutor(this, new Person[]{this.mPerson});
        }
    }

    void deletePerson() {
        new DeletePersonTask(getSession(), this.DELETE_TASK_CALLBACK).executeOnAsyncTaskExecutor(this, new Person[]{this.mPerson});
    }

    void associateOrganization(@Nullable Organization organization) {
        if (this.mPerson != null) {
            this.mPerson.setCompany(organization);
            if (getView() != null) {
                ((PersonEditView) getView()).onOrganizationAssociationUpdated(this.mPerson);
            }
        }
    }

    void associateOrganizationBySqlId(@NonNull Long orgSqlId) {
        associateOrganization((Organization) new OrganizationsDataSource(getSession().getDatabase()).findBySqlId(orgSqlId.longValue()));
    }

    @Nullable
    Person getPerson() {
        return this.mPerson;
    }

    boolean personWasModified() {
        if (this.mPerson != null) {
            this.mPerson.removeEmptyCommunicationMediums();
        }
        if (this.mOriginalPerson != null) {
            this.mOriginalPerson.removeEmptyCommunicationMediums();
        }
        return this.mPerson != null && (this.mContactObjectDelta.hasChanges(this.mPerson, this.mOriginalPerson) || !this.mPerson.isStored());
    }

    void addPhone() {
        if (this.mPerson != null) {
            this.mPerson.getPhones().add(new Phone(null, this.DEFAULT_PHONE_LABEL));
            if (getView() != null) {
                ((PersonEditView) getView()).onPhonesUpdated(this.mPerson);
            }
        }
    }

    void removePhone(@NonNull CommunicationMedium communicationMedium) {
        if (this.mPerson != null && this.mPerson.getPhones().size() >= 2) {
            this.mPerson.getPhones().remove(communicationMedium);
            if (getView() != null) {
                ((PersonEditView) getView()).onPhonesUpdated(this.mPerson);
            }
        }
    }

    void addEmail() {
        if (this.mPerson != null) {
            this.mPerson.getEmails().add(new Email(null, this.DEFAULT_EMAIL_LABEL));
            if (getView() != null) {
                ((PersonEditView) getView()).onEmailsUpdated(this.mPerson);
            }
        }
    }

    void removeEmail(@NonNull CommunicationMedium communicationMedium) {
        if (this.mPerson != null && this.mPerson.getEmails().size() >= 2) {
            this.mPerson.getEmails().remove(communicationMedium);
            if (getView() != null) {
                ((PersonEditView) getView()).onEmailsUpdated(this.mPerson);
            }
        }
    }

    private void cacheAndSendPersonToView(@Nullable Person person) {
        this.mPerson = person;
        if (this.mPerson != null && this.mPerson.getPhones().isEmpty()) {
            this.mPerson.getPhones().add(new Phone(null, this.DEFAULT_PHONE_LABEL));
        }
        if (this.mPerson != null && this.mPerson.getEmails().isEmpty()) {
            this.mPerson.getEmails().add(new Email(null, this.DEFAULT_EMAIL_LABEL));
        }
        if (this.mPerson != null) {
            this.mOriginalPerson = this.mPerson.getDeepCopy();
        }
        if (getView() != null) {
            ((PersonEditView) getView()).onRequestPerson(this.mPerson);
        }
    }

    protected void saveToBundle(@NonNull Bundle bundle) {
        bundle.putParcelable(STORAGE_KEY_PERSON, this.mPerson);
        bundle.putParcelable(STORAGE_KEY_ORIGINAL_PERSON, this.mOriginalPerson);
    }

    protected void restoreFromBundle(@NonNull Bundle bundle) {
        this.mPerson = (Person) bundle.getParcelable(STORAGE_KEY_PERSON);
        this.mOriginalPerson = (Person) bundle.getParcelable(STORAGE_KEY_ORIGINAL_PERSON);
        if (this.mPerson != null) {
            if (this.mPerson.getPhones().isEmpty()) {
                this.mPerson.getPhones().add(new Phone("", this.DEFAULT_PHONE_LABEL));
            }
            if (this.mPerson.getEmails().isEmpty()) {
                this.mPerson.getEmails().add(new Email("", this.DEFAULT_EMAIL_LABEL));
            }
        }
        if (this.mOriginalPerson != null) {
            if (this.mOriginalPerson.getPhones().isEmpty()) {
                this.mOriginalPerson.getPhones().add(new Phone("", this.DEFAULT_PHONE_LABEL));
            }
            if (this.mOriginalPerson.getEmails().isEmpty()) {
                this.mOriginalPerson.getEmails().add(new Email("", this.DEFAULT_EMAIL_LABEL));
            }
        }
    }

    protected String[] getKeysNeededForRestore() {
        return new String[]{STORAGE_KEY_PERSON, STORAGE_KEY_ORIGINAL_PERSON};
    }

    private boolean restorePerson() {
        if (this.mPerson == null || this.mOriginalPerson == null) {
            return false;
        }
        if (getView() != null) {
            ((PersonEditView) getView()).onRequestPerson(this.mPerson);
        }
        return true;
    }
}
