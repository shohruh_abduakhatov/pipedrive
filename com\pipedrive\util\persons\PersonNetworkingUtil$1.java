package com.pipedrive.util.persons;

import android.support.annotation.NonNull;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.pipedrive.application.Session;
import com.pipedrive.util.networking.ConnectionUtil$JsonReaderInterceptor;
import java.io.IOException;

class PersonNetworkingUtil$1 implements ConnectionUtil$JsonReaderInterceptor<PersonNetworkingUtil$1PersonDetailsRequest> {
    PersonNetworkingUtil$1() {
    }

    public PersonNetworkingUtil$1PersonDetailsRequest interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull PersonNetworkingUtil$1PersonDetailsRequest responseTemplate) throws IOException {
        if (jsonReader.peek() == JsonToken.NULL) {
            jsonReader.skipValue();
        } else {
            responseTemplate.contact = PersonNetworkingUtil.readContact(jsonReader);
        }
        return responseTemplate;
    }
}
