package com.pipedrive.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.model.User;
import com.pipedrive.util.CursorHelper;
import java.util.ArrayList;
import java.util.List;

public class UsersDataSource extends BaseDataSource<User> {
    public UsersDataSource(SQLiteDatabase database) {
        super(database);
    }

    @NonNull
    protected String getColumnNameForPipedriveId() {
        return PipeSQLiteHelper.COLUMN_USERS_PIPEDRIVE_ID;
    }

    @NonNull
    protected String getColumnNameForSqlId() {
        return PipeSQLiteHelper.COLUMN_ID;
    }

    @NonNull
    protected String getTableName() {
        return "users";
    }

    @NonNull
    protected String[] getAllColumns() {
        return new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_USERS_PIPEDRIVE_ID, PipeSQLiteHelper.COLUMN_USERS_NAME, PipeSQLiteHelper.COLUMN_USERS_IS_ACTIVE, PipeSQLiteHelper.COLUMN_USERS_ICON_URL};
    }

    @NonNull
    protected ContentValues getContentValues(@NonNull User user) {
        ContentValues contentValues = super.getContentValues(user);
        contentValues.putNull(PipeSQLiteHelper.COLUMN_USERS_NAME);
        contentValues.putNull(PipeSQLiteHelper.COLUMN_USERS_ICON_URL);
        String userName = user.getName();
        if (userName != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_USERS_NAME, userName);
        }
        CursorHelper.put(user.isActive(), contentValues, PipeSQLiteHelper.COLUMN_USERS_IS_ACTIVE);
        String iconUrl = user.getIconUrl();
        if (iconUrl != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_USERS_ICON_URL, iconUrl);
        }
        return contentValues;
    }

    @Nullable
    protected User deflateCursor(@NonNull Cursor cursor) {
        User user = new User();
        user.setSqlId(cursor.getLong(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_ID)));
        Integer pdId = CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_USERS_PIPEDRIVE_ID);
        if (pdId != null) {
            user.setPipedriveId(pdId.intValue());
        }
        user.setName(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_USERS_NAME));
        Boolean isActive = CursorHelper.getBoolean(cursor, PipeSQLiteHelper.COLUMN_USERS_IS_ACTIVE);
        if (isActive != null) {
            user.setActive(isActive);
        }
        user.setIconUrl(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_USERS_ICON_URL));
        return user;
    }

    @Nullable
    public User findByPipedriveId(int pipedriveId) {
        User user = null;
        String selection = getColumnNameForPipedriveId() + " = ?";
        String[] selectionArgs = new String[]{"" + pipedriveId};
        String tableName = getTableName();
        String[] allColumns = getAllColumns();
        Cursor cursor = !(this instanceof SQLiteDatabase) ? query(tableName, allColumns, selection, selectionArgs, user, user, user) : SQLiteInstrumentation.query((SQLiteDatabase) this, tableName, allColumns, selection, selectionArgs, user, user, user);
        try {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                user = deflateCursor(cursor);
                cursor.close();
            }
            return user;
        } finally {
            cursor.close();
        }
    }

    @NonNull
    public List<User> getActiveUsersOrderedByName() {
        SelectionHolder selectionHolder = getSelectionForActiveUsers();
        List<User> users = new ArrayList();
        String tableName = getTableName();
        String[] allColumns = getAllColumns();
        String selection = selectionHolder.getSelection();
        String[] selectionArgs = selectionHolder.getSelectionArgs();
        String str = "u_name COLLATE NOCASE";
        Cursor cursor = !(this instanceof SQLiteDatabase) ? query(tableName, allColumns, selection, selectionArgs, null, null, str) : SQLiteInstrumentation.query((SQLiteDatabase) this, tableName, allColumns, selection, selectionArgs, null, null, str);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                users.add(deflateCursor(cursor));
                cursor.moveToNext();
            }
            return users;
        } finally {
            cursor.close();
        }
    }

    public int getActiveUsersCount() {
        String[] columns = new String[]{"COUNT(*)"};
        SelectionHolder selectionHolder = getSelectionForActiveUsers();
        String tableName = getTableName();
        String selection = selectionHolder.getSelection();
        String[] selectionArgs = selectionHolder.getSelectionArgs();
        Cursor cursor = !(this instanceof SQLiteDatabase) ? query(tableName, columns, selection, selectionArgs, null, null, null) : SQLiteInstrumentation.query((SQLiteDatabase) this, tableName, columns, selection, selectionArgs, null, null, null);
        int i = 0;
        try {
            cursor.moveToFirst();
            i = cursor.getInt(0);
            return i;
        } finally {
            cursor.close();
        }
    }

    public SelectionHolder getSelectionForActiveUsers() {
        String selection = "u_is_active = ?";
        return new SelectionHolder("u_is_active = ?", new String[]{String.valueOf(1)});
    }
}
