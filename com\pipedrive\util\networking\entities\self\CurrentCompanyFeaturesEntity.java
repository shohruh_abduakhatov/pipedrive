package com.pipedrive.util.networking.entities.self;

import android.support.annotation.Nullable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrentCompanyFeaturesEntity {
    public static final String JSON_FIELD_SELECTOR = "current_company_features:(products,price_variations,product_duration)";
    public static final String JSON_PARAM = "current_company_features";
    private static final String JSON_PARAM_PRODUCTS = "products";
    private static final String JSON_PARAM_PRODUCT_DURATIONS = "product_duration";
    private static final String JSON_PARAM_PRODUCT_PRICE_VARIATIONS = "price_variations";
    @Nullable
    @SerializedName("product_duration")
    @Expose
    public Boolean isProductsDurationsEnabled;
    @Nullable
    @SerializedName("products")
    @Expose
    public Boolean isProductsEnabled;
    @Nullable
    @SerializedName("price_variations")
    @Expose
    public Boolean isProductsPriceVariationsEnabled;

    public String toString() {
        return "CurrentCompanyFeaturesEntity{isProductsEnabled=" + this.isProductsEnabled + ", isProductsPriceVariationsEnabled=" + this.isProductsPriceVariationsEnabled + ", isProductsDurationsEnabled=" + this.isProductsDurationsEnabled + '}';
    }
}
