package com.zendesk.sdk.feedback.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.sdk.attachment.ImageUploadHelper;
import com.zendesk.sdk.ui.ZendeskPicassoTransformationFactory;
import com.zendesk.sdk.util.UiUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AttachmentContainerHost extends LinearLayout {
    private static final String LOG_TAG = AttachmentContainerHost.class.getSimpleName();
    private List<AttachmentContainer> mAttachmentContainerList = new ArrayList();
    private AttachmentContainerListener mAttachmentContainerListener;
    private View mParent = this;

    class AttachmentContainer extends FrameLayout {
        private AttachmentState mAttachmentState = AttachmentState.UPLOADING;
        private Button mDeleteButton;
        private File mFile;
        private ImageView mImageView;
        private int mOrientation;
        private ProgressBar mProgressBar;

        public AttachmentContainer(Context context, final File file, final AttachmentContainerHost parent, int orientation) {
            super(context);
            this.mFile = file;
            this.mOrientation = orientation;
            View v = LayoutInflater.from(context).inflate(R.layout.view_attachment_container_item, this, false);
            this.mImageView = (ImageView) v.findViewById(R.id.attachment_image);
            this.mDeleteButton = (Button) v.findViewById(R.id.attachment_delete);
            this.mProgressBar = (ProgressBar) v.findViewById(R.id.attachment_progress);
            this.mProgressBar.setIndeterminate(true);
            this.mDeleteButton.setOnClickListener(new OnClickListener(AttachmentContainerHost.this) {
                public void onClick(View v) {
                    parent.removeAttachmentAndNotify(file);
                }
            });
            attachImage(this.mFile, this.mImageView, parent, context);
            addView(v);
        }

        public void setState(AttachmentState attachmentState) {
            this.mAttachmentState = attachmentState;
            switch (attachmentState) {
                case UPLOADING:
                    UiUtils.setVisibility(this.mDeleteButton, 8);
                    UiUtils.setVisibility(this.mProgressBar, 0);
                    return;
                case UPLOADED:
                    UiUtils.setVisibility(this.mDeleteButton, 0);
                    UiUtils.setVisibility(this.mProgressBar, 8);
                    return;
                case DISABLE:
                    UiUtils.setVisibility(this.mDeleteButton, 8);
                    UiUtils.setVisibility(this.mProgressBar, 8);
                    return;
                default:
                    return;
            }
        }

        public AttachmentState getAttachmentState() {
            return this.mAttachmentState;
        }

        public File getFile() {
            return this.mFile;
        }

        private void attachImage(File file, ImageView imageView, ViewGroup parent, Context context) {
            switch (this.mOrientation) {
                case 0:
                    loadFileIntoImageView(imageView, ZendeskPicassoTransformationFactory.INSTANCE.getResizeTransformationHeight(getResources().getDimensionPixelSize(R.dimen.attachment_container_host_horizontal_height) - (getResources().getDimensionPixelSize(R.dimen.attachment_container_image_margin) * 2)), file, context);
                    break;
                case 1:
                    int widthPixels = parent.getMeasuredWidth();
                    if (widthPixels == 0) {
                        widthPixels = context.getResources().getDisplayMetrics().widthPixels;
                    }
                    loadFileIntoImageView(imageView, ZendeskPicassoTransformationFactory.INSTANCE.getResizeTransformationWidth(widthPixels - (getResources().getDimensionPixelSize(R.dimen.attachment_container_image_margin) * 2)), file, context);
                    break;
            }
            setState(this.mAttachmentState);
        }

        private void loadFileIntoImageView(ImageView imageView, Transformation transformation, File file, Context context) {
            Picasso.with(context).load(file).transform(transformation).noFade().into(imageView);
        }
    }

    public interface AttachmentContainerListener {
        void attachmentRemoved(File file);
    }

    public enum AttachmentState {
        UPLOADING,
        UPLOADED,
        DISABLE
    }

    public AttachmentContainerHost(Context context) {
        super(context);
    }

    public AttachmentContainerHost(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @TargetApi(11)
    public AttachmentContainerHost(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public AttachmentContainerHost(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setAttachmentContainerListener(AttachmentContainerListener attachmentContainerListener) {
        this.mAttachmentContainerListener = attachmentContainerListener;
    }

    public void setParent(View parent) {
        this.mParent = parent;
    }

    public void setState(ImageUploadHelper imageUploadHelper) {
        if (imageUploadHelper == null) {
            Logger.d(LOG_TAG, "Please provide a non null ImageUploadHelper", new Object[0]);
            return;
        }
        HashMap<AttachmentState, List<File>> stateMap = imageUploadHelper.getRecentState();
        if (stateMap.containsKey(AttachmentState.UPLOADING)) {
            for (File file : (List) stateMap.get(AttachmentState.UPLOADING)) {
                addAttachment(file);
            }
        }
        if (stateMap.containsKey(AttachmentState.UPLOADED)) {
            for (File file2 : (List) stateMap.get(AttachmentState.UPLOADED)) {
                addAttachment(file2);
                setAttachmentUploaded(file2);
            }
        }
    }

    public void addAttachment(File file) {
        UiUtils.setVisibility(this.mParent, 0);
        AttachmentContainer attachmentContainer = new AttachmentContainer(getContext(), file, this, getOrientation());
        attachmentContainer.setId(file.hashCode());
        this.mAttachmentContainerList.add(attachmentContainer);
        addView(attachmentContainer);
    }

    public void setAttachmentUploaded(File file) {
        setAttachmentState(file, AttachmentState.UPLOADED);
    }

    public void setAttachmentState(File file, AttachmentState attachmentState) {
        for (AttachmentContainer attachmentContainer : this.mAttachmentContainerList) {
            if (attachmentContainer.getFile().getAbsolutePath().equals(file.getAbsolutePath())) {
                attachmentContainer.setState(attachmentState);
            }
        }
    }

    public void removeAttachment(File file) {
        List<AttachmentContainer> newAttachmentContainer = new ArrayList();
        for (AttachmentContainer attachmentContainer : this.mAttachmentContainerList) {
            if (!attachmentContainer.getFile().getAbsolutePath().equals(file.getAbsolutePath())) {
                newAttachmentContainer.add(attachmentContainer);
            }
        }
        this.mAttachmentContainerList = newAttachmentContainer;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if ((childAt instanceof AttachmentContainer) && ((AttachmentContainer) childAt).getFile().getAbsolutePath().equals(file.getAbsolutePath())) {
                removeViewAt(i);
                break;
            }
        }
        if (getChildCount() < 1) {
            UiUtils.setVisibility(this.mParent, 8);
        }
    }

    public synchronized void removeAttachmentAndNotify(File file) {
        removeAttachment(file);
        if (this.mAttachmentContainerListener != null) {
            this.mAttachmentContainerListener.attachmentRemoved(file);
        }
    }

    public void setAttachmentsDeletable(boolean deletable) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt instanceof AttachmentContainer) {
                AttachmentContainer attachment = (AttachmentContainer) childAt;
                AttachmentState attachmentState = attachment.getAttachmentState();
                if (deletable && attachmentState == AttachmentState.DISABLE) {
                    attachment.setState(AttachmentState.UPLOADED);
                } else if (!deletable && attachmentState == AttachmentState.UPLOADED) {
                    attachment.setState(AttachmentState.DISABLE);
                }
            }
        }
    }

    public void reset() {
        UiUtils.setVisibility(this.mParent, 8);
        removeAllViews();
    }
}
