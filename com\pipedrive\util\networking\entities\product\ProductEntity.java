package com.pipedrive.util.networking.entities.product;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class ProductEntity {
    private static final String PARAM_ACTIVE_FLAG = "active_flag";
    private static final String PARAM_ID = "id";
    private static final String PARAM_NAME = "name";
    private static final String PARAM_PRICES = "prices";
    private static final String PARAM_PRODUCT_VARIATIONS = "product_variations";
    private static final String PARAM_SELECTABLE = "selectable";
    public static final String URL_NESTED_PARAMS_LIST = ":(id,name,active_flag,selectable,prices,product_variations)";
    @Nullable
    @SerializedName("active_flag")
    @Expose
    private Boolean activeFlag;
    @Nullable
    @SerializedName("name")
    @Expose
    private String name;
    @Nullable
    @SerializedName("id")
    @Expose
    private Long pipedriveId;
    @JsonAdapter(ProductEntityPricesTypeAdapter.class)
    @NonNull
    @SerializedName("prices")
    @Expose
    private List<ProductPriceEntity> prices = new ArrayList();
    @NonNull
    @SerializedName("product_variations")
    @Expose
    private List<ProductVariationEntity> productVariations = new ArrayList();
    @Nullable
    @SerializedName("selectable")
    @Expose
    private Boolean selectable;

    @Nullable
    public Long getPipedriveId() {
        return this.pipedriveId;
    }

    @Nullable
    public String getName() {
        return this.name;
    }

    @Nullable
    public Boolean getActiveFlag() {
        return this.activeFlag;
    }

    @Nullable
    public Boolean getSelectable() {
        return this.selectable;
    }

    @NonNull
    public List<ProductPriceEntity> getPrices() {
        return this.prices;
    }

    @NonNull
    public List<ProductVariationEntity> getProductVariations() {
        return this.productVariations;
    }

    public String toString() {
        return "ProductEntity{pipedriveId=" + getPipedriveId() + ", name='" + getName() + '\'' + ", activeFlag=" + getActiveFlag() + ", selectable=" + getSelectable() + ", prices.length=" + getPrices().size() + ", productVariations.length=" + getProductVariations().size() + '}';
    }
}
