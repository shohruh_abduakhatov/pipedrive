package com.zendesk.sdk.support.help;

import com.zendesk.sdk.model.helpcenter.help.HelpItem;
import com.zendesk.sdk.model.helpcenter.help.HelpRequest.Builder;
import com.zendesk.sdk.model.helpcenter.help.SectionItem;
import com.zendesk.sdk.network.HelpCenterProvider;
import com.zendesk.sdk.support.help.HelpMvp.Model;
import com.zendesk.service.ZendeskCallback;
import java.util.Collections;
import java.util.List;

class HelpModel implements Model {
    private HelpCenterProvider provider;

    HelpModel(HelpCenterProvider provider) {
        this.provider = provider;
    }

    public void getArticles(List<Long> categoryIds, List<Long> sectionIds, String[] labelNames, ZendeskCallback<List<HelpItem>> callback) {
        this.provider.getHelp(new Builder().withCategoryIds(categoryIds).withSectionIds(sectionIds).withLabelNames(labelNames).includeCategories().includeSections().build(), callback);
    }

    public void getArticlesForSection(SectionItem section, String[] labelNames, ZendeskCallback<List<HelpItem>> callback) {
        this.provider.getHelp(new Builder().withSectionIds(Collections.singletonList(section.getId())).withArticlesPerSectionLimit(section.getTotalArticlesCount()).withLabelNames(labelNames).includeSections().build(), callback);
    }
}
