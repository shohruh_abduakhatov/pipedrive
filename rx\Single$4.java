package rx;

import rx.functions.Func3;
import rx.functions.FuncN;

class Single$4 implements FuncN<R> {
    final /* synthetic */ Func3 val$zipFunction;

    Single$4(Func3 func3) {
        this.val$zipFunction = func3;
    }

    public R call(Object... args) {
        return this.val$zipFunction.call(args[0], args[1], args[2]);
    }
}
