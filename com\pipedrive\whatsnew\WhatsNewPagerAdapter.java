package com.pipedrive.whatsnew;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import butterknife.ButterKnife;
import com.pipedrive.R;

public class WhatsNewPagerAdapter extends PagerAdapter {
    @NonNull
    private WhatsNewSlide[] mWhatsNewSlides = new WhatsNewSlide[0];

    WhatsNewPagerAdapter(@NonNull WhatsNewSlide[] whatsNewSlides) {
        this.mWhatsNewSlides = whatsNewSlides;
    }

    public Object instantiateItem(ViewGroup container, int position) {
        View whatsNewSlideDecorator = LayoutInflater.from(container.getContext()).inflate(R.layout.whatsnew_decorator_layout, container, false);
        whatsNewSlideDecorator.setTag(Integer.valueOf(position));
        ViewStub slideStub = (ViewStub) ButterKnife.findById(whatsNewSlideDecorator, R.id.whatsNewSlideViewStub);
        slideStub.setLayoutResource(this.mWhatsNewSlides[position].layoutResId);
        slideStub.inflate();
        container.addView(whatsNewSlideDecorator, 0);
        return whatsNewSlideDecorator;
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public int getCount() {
        return this.mWhatsNewSlides.length;
    }

    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
