package com.pipedrive.nearby.cards;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.map.LocationProvider;
import com.pipedrive.nearby.map.MarkerSelectionProvider;
import com.pipedrive.nearby.map.NearbyItemsProvider;
import com.pipedrive.nearby.toolbar.NearbyItemTypeChangesProvider;

public interface Cards extends CardSelectionProvider {
    void bind();

    void releaseResources();

    void saveState(@NonNull Bundle bundle);

    void setup(@Nullable Bundle bundle, @NonNull Session session, @NonNull NearbyItemsProvider nearbyItemsProvider, @NonNull NearbyItemTypeChangesProvider nearbyItemTypeChangesProvider, @NonNull MarkerSelectionProvider markerSelectionProvider, @NonNull LocationProvider locationProvider);
}
