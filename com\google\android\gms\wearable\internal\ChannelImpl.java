package com.google.android.gms.wearable.internal;

import android.content.IntentFilter;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.internal.zzrr;
import com.google.android.gms.wearable.Channel;
import com.google.android.gms.wearable.Channel.GetInputStreamResult;
import com.google.android.gms.wearable.Channel.GetOutputStreamResult;
import com.google.android.gms.wearable.ChannelApi;
import com.google.android.gms.wearable.ChannelApi.ChannelListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ChannelImpl extends AbstractSafeParcelable implements Channel {
    public static final Creator<ChannelImpl> CREATOR = new zzo();
    private final String aSk;
    private final String hN;
    private final String mPath;
    final int mVersionCode;

    class AnonymousClass7 implements zza<ChannelListener> {
        final /* synthetic */ IntentFilter[] aTc;
        final /* synthetic */ String hI;

        AnonymousClass7(String str, IntentFilter[] intentFilterArr) {
            this.hI = str;
            this.aTc = intentFilterArr;
        }

        public void zza(zzbp com_google_android_gms_wearable_internal_zzbp, com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, ChannelListener channelListener, zzrr<ChannelListener> com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_ChannelApi_ChannelListener) throws RemoteException {
            com_google_android_gms_wearable_internal_zzbp.zza((com.google.android.gms.internal.zzqo.zzb) com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, channelListener, (zzrr) com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_ChannelApi_ChannelListener, this.hI, this.aTc);
        }
    }

    static final class zza implements GetInputStreamResult {
        private final InputStream aTu;
        private final Status hv;

        zza(Status status, InputStream inputStream) {
            this.hv = (Status) zzaa.zzy(status);
            this.aTu = inputStream;
        }

        public InputStream getInputStream() {
            return this.aTu;
        }

        public Status getStatus() {
            return this.hv;
        }

        public void release() {
            if (this.aTu != null) {
                try {
                    this.aTu.close();
                } catch (IOException e) {
                }
            }
        }
    }

    static final class zzb implements GetOutputStreamResult {
        private final OutputStream aTv;
        private final Status hv;

        zzb(Status status, OutputStream outputStream) {
            this.hv = (Status) zzaa.zzy(status);
            this.aTv = outputStream;
        }

        public OutputStream getOutputStream() {
            return this.aTv;
        }

        public Status getStatus() {
            return this.hv;
        }

        public void release() {
            if (this.aTv != null) {
                try {
                    this.aTv.close();
                } catch (IOException e) {
                }
            }
        }
    }

    ChannelImpl(int i, String str, String str2, String str3) {
        this.mVersionCode = i;
        this.hN = (String) zzaa.zzy(str);
        this.aSk = (String) zzaa.zzy(str2);
        this.mPath = (String) zzaa.zzy(str3);
    }

    private static zza<ChannelListener> zza(String str, IntentFilter[] intentFilterArr) {
        return new AnonymousClass7(str, intentFilterArr);
    }

    public PendingResult<Status> addListener(GoogleApiClient googleApiClient, ChannelListener channelListener) {
        return zzb.zza(googleApiClient, zza(this.hN, new IntentFilter[]{zzbn.zzrp(ChannelApi.ACTION_CHANNEL_EVENT)}), channelListener);
    }

    public PendingResult<Status> close(GoogleApiClient googleApiClient) {
        return googleApiClient.zza(new zzi<Status>(this, googleApiClient) {
            final /* synthetic */ ChannelImpl aTq;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zzx(this, this.aTq.hN);
            }

            protected Status zzb(Status status) {
                return status;
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzb(status);
            }
        });
    }

    public PendingResult<Status> close(GoogleApiClient googleApiClient, final int i) {
        return googleApiClient.zza(new zzi<Status>(this, googleApiClient) {
            final /* synthetic */ ChannelImpl aTq;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zzi(this, this.aTq.hN, i);
            }

            protected Status zzb(Status status) {
                return status;
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzb(status);
            }
        });
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ChannelImpl)) {
            return false;
        }
        ChannelImpl channelImpl = (ChannelImpl) obj;
        return this.hN.equals(channelImpl.hN) && zzz.equal(channelImpl.aSk, this.aSk) && zzz.equal(channelImpl.mPath, this.mPath) && channelImpl.mVersionCode == this.mVersionCode;
    }

    public PendingResult<GetInputStreamResult> getInputStream(GoogleApiClient googleApiClient) {
        return googleApiClient.zza(new zzi<GetInputStreamResult>(this, googleApiClient) {
            final /* synthetic */ ChannelImpl aTq;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zzy(this, this.aTq.hN);
            }

            public /* synthetic */ Result zzc(Status status) {
                return zzes(status);
            }

            public GetInputStreamResult zzes(Status status) {
                return new zza(status, null);
            }
        });
    }

    public String getNodeId() {
        return this.aSk;
    }

    public PendingResult<GetOutputStreamResult> getOutputStream(GoogleApiClient googleApiClient) {
        return googleApiClient.zza(new zzi<GetOutputStreamResult>(this, googleApiClient) {
            final /* synthetic */ ChannelImpl aTq;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zzz(this, this.aTq.hN);
            }

            public /* synthetic */ Result zzc(Status status) {
                return zzet(status);
            }

            public GetOutputStreamResult zzet(Status status) {
                return new zzb(status, null);
            }
        });
    }

    public String getPath() {
        return this.mPath;
    }

    public String getToken() {
        return this.hN;
    }

    public int hashCode() {
        return this.hN.hashCode();
    }

    public PendingResult<Status> receiveFile(GoogleApiClient googleApiClient, final Uri uri, final boolean z) {
        zzaa.zzb((Object) googleApiClient, (Object) "client is null");
        zzaa.zzb((Object) uri, (Object) "uri is null");
        return googleApiClient.zza(new zzi<Status>(this, googleApiClient) {
            final /* synthetic */ ChannelImpl aTq;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zza((com.google.android.gms.internal.zzqo.zzb) this, this.aTq.hN, uri, z);
            }

            public Status zzb(Status status) {
                return status;
            }

            public /* synthetic */ Result zzc(Status status) {
                return zzb(status);
            }
        });
    }

    public PendingResult<Status> removeListener(GoogleApiClient googleApiClient, ChannelListener channelListener) {
        zzaa.zzb((Object) googleApiClient, (Object) "client is null");
        zzaa.zzb((Object) channelListener, (Object) "listener is null");
        return googleApiClient.zza(new zzb(googleApiClient, channelListener, this.hN));
    }

    public PendingResult<Status> sendFile(GoogleApiClient googleApiClient, Uri uri) {
        return sendFile(googleApiClient, uri, 0, -1);
    }

    public PendingResult<Status> sendFile(GoogleApiClient googleApiClient, Uri uri, long j, long j2) {
        zzaa.zzb((Object) googleApiClient, (Object) "client is null");
        zzaa.zzb(this.hN, (Object) "token is null");
        zzaa.zzb((Object) uri, (Object) "uri is null");
        zzaa.zzb(j >= 0, "startOffset is negative: %s", Long.valueOf(j));
        boolean z = j2 >= 0 || j2 == -1;
        zzaa.zzb(z, "invalid length: %s", Long.valueOf(j2));
        final Uri uri2 = uri;
        final long j3 = j;
        final long j4 = j2;
        return googleApiClient.zza(new zzi<Status>(this, googleApiClient) {
            final /* synthetic */ ChannelImpl aTq;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zza((com.google.android.gms.internal.zzqo.zzb) this, this.aTq.hN, uri2, j3, j4);
            }

            public Status zzb(Status status) {
                return status;
            }

            public /* synthetic */ Result zzc(Status status) {
                return zzb(status);
            }
        });
    }

    public String toString() {
        int i = this.mVersionCode;
        String str = this.hN;
        String str2 = this.aSk;
        String str3 = this.mPath;
        return new StringBuilder(((String.valueOf(str).length() + 66) + String.valueOf(str2).length()) + String.valueOf(str3).length()).append("ChannelImpl{versionCode=").append(i).append(", token='").append(str).append("'").append(", nodeId='").append(str2).append("'").append(", path='").append(str3).append("'").append("}").toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzo.zza(this, parcel, i);
    }
}
