package com.pipedrive.views.viewholder.flow;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datetime.LocaleHelper;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.flow.model.FlowItemEntity;
import com.pipedrive.model.FlowFile;
import com.pipedrive.util.CompatUtil;
import com.pipedrive.views.viewholder.ViewHolder;

public class FileRowViewHolder implements ViewHolder {
    @BindView(2131821088)
    TextView mDetails;
    @BindView(2131821087)
    TextView mFileName;
    @BindView(2131821089)
    View progressbarFileUpload;

    public int getLayoutResourceId() {
        return R.layout.row_file;
    }

    public void fill(@NonNull Session session, @NonNull FlowFile flowFile) {
        Integer fileUploadStatus = flowFile.getFileUploadStatus();
        boolean showFileUploadProgressbarIndicator = (fileUploadStatus == null || fileUploadStatus.intValue() == 2 || fileUploadStatus.intValue() == 3) ? false : true;
        fill(session, flowFile.getName(), flowFile.getAddTime(), showFileUploadProgressbarIndicator);
    }

    private void fill(@NonNull Session session, @Nullable String filename, @NonNull PipedriveDateTime timestamp, boolean isFileUploadInProgress) {
        this.mFileName.setText(filename);
        this.mDetails.setText(LocaleHelper.getLocaleBasedDateTimeStringInCurrentTimeZone(session, timestamp));
        fileUploadInProgress(isFileUploadInProgress);
    }

    public void fill(@NonNull Session session, @NonNull FlowItemEntity fileItem) {
        Long fileUploadStatus = fileItem.getExtraNumber();
        boolean showFileUploadProgressbarIndicator = (fileUploadStatus == null || fileUploadStatus.longValue() == 2 || fileUploadStatus.longValue() == 3) ? false : true;
        fill(session, fileItem.getTitle(), fileItem.getTimestamp(), showFileUploadProgressbarIndicator);
    }

    private void fileUploadInProgress(boolean isInProgress) {
        this.progressbarFileUpload.setVisibility(isInProgress ? 0 : 8);
        CompatUtil.setTextAppearance(this.mFileName, isInProgress ? R.style.TextAppearance.Title.Disabled : R.style.TextAppearance.Title);
    }
}
