package com.zendesk.sdk.network.impl;

import com.zendesk.sdk.model.push.PushRegistrationRequestWrapper;
import com.zendesk.sdk.model.push.PushRegistrationResponseWrapper;
import com.zendesk.sdk.network.PushRegistrationService;
import com.zendesk.service.RetrofitZendeskCallbackAdapter;
import com.zendesk.service.ZendeskCallback;

class ZendeskPushRegistrationService {
    private static final String LOG_TAG = "ZendeskPushRegistrationService";
    private final PushRegistrationService pushService;

    ZendeskPushRegistrationService(PushRegistrationService pushRegistrationService) {
        this.pushService = pushRegistrationService;
    }

    public void registerDevice(String authorization, PushRegistrationRequestWrapper registerPushResponseWrapper, ZendeskCallback<PushRegistrationResponseWrapper> callback) {
        this.pushService.registerDevice(authorization, registerPushResponseWrapper).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }

    public void unregisterDevice(String authorization, String identifier, ZendeskCallback<Void> callback) {
        this.pushService.unregisterDevice(authorization, identifier).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }
}
