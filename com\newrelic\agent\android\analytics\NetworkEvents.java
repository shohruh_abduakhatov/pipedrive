package com.newrelic.agent.android.analytics;

import com.newrelic.agent.android.harvest.HttpTransaction;
import com.newrelic.agent.android.logging.AgentLog;
import com.newrelic.agent.android.logging.AgentLogManager;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class NetworkEvents {
    static final AgentLog log = AgentLogManager.getAgentLog();

    public static void createHttpErrorEvent(HttpTransaction txn) {
        Map<String, Object> attributes = new HashMap();
        try {
            URL url = new URL(txn.getUrl());
            attributes.put(AnalyticAttribute.REQUEST_URL_ATTRIBUTE, txn.getUrl());
            attributes.put(AnalyticAttribute.REQUEST_DOMAIN_ATTRIBUTE, url.getHost());
            attributes.put(AnalyticAttribute.REQUEST_PATH_ATTRIBUTE, url.getPath());
        } catch (MalformedURLException e) {
            log.error(txn.getUrl() + " is not a valid URL. Unable to set host or path attributes.");
            attributes.put(AnalyticAttribute.REQUEST_URL_ATTRIBUTE, txn.getUrl());
        }
        attributes.put(AnalyticAttribute.STATUS_CODE_ATTRIBUTE, Integer.valueOf(txn.getStatusCode()));
        attributes.put(AnalyticAttribute.CONNECTION_TYPE_ATTRIBUTE, txn.getWanType());
        attributes.put(AnalyticAttribute.REQUEST_METHOD_ATTRIBUTE, txn.getHttpMethod());
        double totalTime = txn.getTotalTime();
        if (totalTime != 0.0d) {
            attributes.put(AnalyticAttribute.RESPONSE_TIME_ATTRIBUTE, Double.valueOf(totalTime));
        }
        double bytesSent = (double) txn.getBytesSent();
        if (bytesSent != 0.0d) {
            attributes.put(AnalyticAttribute.BYTES_SENT_ATTRIBUTE, Double.valueOf(bytesSent));
        }
        double bytesReceived = (double) txn.getBytesReceived();
        if (bytesReceived != 0.0d) {
            attributes.put(AnalyticAttribute.BYTES_RECEIVED_ATTRIBUTE, Double.valueOf(bytesReceived));
        }
        if (AnalyticsControllerImpl.getInstance().recordEvent(AnalyticAttribute.EVENT_NAME_ATTRIBUTE_REQUEST_ERROR, AnalyticsEventCategory.RequestError, AnalyticAttribute.EVENT_TYPE_ATTRIBUTE_MOBILE_REQUEST_ERROR, attributes)) {
            log.verbose(AnalyticsEventCategory.RequestError.toString() + " added to event store for request: " + txn.getUrl());
        } else {
            log.error("Failed to add MobileRequestError");
        }
    }

    public static void createNetworkFailureEvent(HttpTransaction txn) {
        Map<String, Object> attributes = new HashMap();
        try {
            URL url = new URL(txn.getUrl());
            attributes.put(AnalyticAttribute.REQUEST_URL_ATTRIBUTE, txn.getUrl());
            attributes.put(AnalyticAttribute.REQUEST_DOMAIN_ATTRIBUTE, url.getHost());
            attributes.put(AnalyticAttribute.REQUEST_PATH_ATTRIBUTE, url.getPath());
        } catch (MalformedURLException e) {
            log.error(txn.getUrl() + " is not a valid URL. Unable to set host or path attributes.");
            attributes.put(AnalyticAttribute.REQUEST_URL_ATTRIBUTE, txn.getUrl());
        }
        attributes.put(AnalyticAttribute.NETWORK_ERROR_CODE_ATTRIBUTE, Integer.valueOf(txn.getErrorCode()));
        attributes.put(AnalyticAttribute.CONNECTION_TYPE_ATTRIBUTE, txn.getWanType());
        attributes.put(AnalyticAttribute.REQUEST_METHOD_ATTRIBUTE, txn.getHttpMethod());
        double totalTime = txn.getTotalTime();
        if (totalTime != 0.0d) {
            attributes.put(AnalyticAttribute.RESPONSE_TIME_ATTRIBUTE, Double.valueOf(totalTime));
        }
        double bytesSent = (double) txn.getBytesSent();
        if (bytesSent != 0.0d) {
            attributes.put(AnalyticAttribute.BYTES_SENT_ATTRIBUTE, Double.valueOf(bytesSent));
        }
        double bytesReceived = (double) txn.getBytesReceived();
        if (bytesReceived != 0.0d) {
            attributes.put(AnalyticAttribute.BYTES_RECEIVED_ATTRIBUTE, Double.valueOf(bytesReceived));
        }
        if (AnalyticsControllerImpl.getInstance().recordEvent(AnalyticAttribute.EVENT_NAME_ATTRIBUTE_REQUEST_ERROR, AnalyticsEventCategory.RequestError, AnalyticAttribute.EVENT_TYPE_ATTRIBUTE_MOBILE_REQUEST_ERROR, attributes)) {
            log.verbose(AnalyticsEventCategory.RequestError.toString() + "added to event store for request: " + txn.getUrl());
        } else {
            log.error("Failed to add MobileRequestError");
        }
    }
}
