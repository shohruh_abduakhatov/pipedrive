package com.zendesk.belvedere;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.background.ApplicationStateMonitor;
import com.newrelic.agent.android.instrumentation.Instrumented;
import com.newrelic.agent.android.tracing.TraceMachine;
import java.util.ArrayList;
import java.util.List;

@Instrumented
public class BelvedereDialog extends AppCompatDialogFragment implements TraceFieldInterface {
    private static final String EXTRA_INTENT = "extra_intent";
    private static final String FRAGMENT_TAG = "BelvedereDialog";
    private static final String LOG_TAG = "BelvedereDialog";
    private ListView listView;

    private interface StartActivity {
        Context getContext();

        void startActivity(BelvedereIntent belvedereIntent);
    }

    private class Adapter extends ArrayAdapter<BelvedereIntent> {
        private Context context;

        Adapter(Context context, int resource, List<BelvedereIntent> objects) {
            super(context, resource, objects);
            this.context = context;
        }

        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            View row = convertView;
            if (convertView == null) {
                row = LayoutInflater.from(this.context).inflate(R$layout.belvedere_dialog_row, parent, false);
            }
            BelvedereIntent intent = (BelvedereIntent) getItem(position);
            AttachmentSource item = AttachmentSource.from(intent, this.context);
            ((ImageView) row.findViewById(R$id.belvedere_dialog_row_image)).setImageDrawable(ContextCompat.getDrawable(this.context, item.getDrawable()));
            ((TextView) row.findViewById(R$id.belvedere_dialog_row_text)).setText(item.getText());
            row.setTag(intent);
            return row;
        }
    }

    private static class AttachmentSource {
        private final int drawable;
        private final String text;

        public static AttachmentSource from(BelvedereIntent belvedereIntent, Context context) {
            switch (belvedereIntent.getSource()) {
                case Camera:
                    return new AttachmentSource(R$drawable.ic_camera, context.getString(R.string.belvedere_dialog_camera));
                case Gallery:
                    return new AttachmentSource(R$drawable.ic_image, context.getString(R.string.belvedere_dialog_gallery));
                default:
                    return new AttachmentSource(-1, context.getString(R.string.belvedere_dialog_unknown));
            }
        }

        private AttachmentSource(int drawable, String text) {
            this.drawable = drawable;
            this.text = text;
        }

        public int getDrawable() {
            return this.drawable;
        }

        public String getText() {
            return this.text;
        }
    }

    protected void onStart() {
        super.onStart();
        ApplicationStateMonitor.getInstance().activityStarted();
    }

    protected void onStop() {
        super.onStop();
        ApplicationStateMonitor.getInstance().activityStopped();
    }

    public static void showDialog(FragmentManager fm, List<BelvedereIntent> belvedereIntent) {
        if (belvedereIntent != null && belvedereIntent.size() != 0) {
            BelvedereDialog attachmentSourceSelectorDialog = new BelvedereDialog();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(EXTRA_INTENT, new ArrayList(belvedereIntent));
            attachmentSourceSelectorDialog.setArguments(bundle);
            attachmentSourceSelectorDialog.show(fm.beginTransaction(), "BelvedereDialog");
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        try {
            TraceMachine.enterMethod(this._nr_trace, "BelvedereDialog#onCreateView", null);
        } catch (NoSuchFieldError e) {
            while (true) {
                TraceMachine.enterMethod(null, "BelvedereDialog#onCreateView", null);
            }
        }
        View view = layoutInflater.inflate(R$layout.belvedere_dialog, viewGroup, false);
        this.listView = (ListView) view.findViewById(R$id.belvedere_dialog_listview);
        TraceMachine.exitMethod();
        return view;
    }

    public void onCreate(@Nullable Bundle bundle) {
        TraceMachine.startTracing("BelvedereDialog");
        try {
            TraceMachine.enterMethod(this._nr_trace, "BelvedereDialog#onCreate", null);
        } catch (NoSuchFieldError e) {
            while (true) {
                TraceMachine.enterMethod(null, "BelvedereDialog#onCreate", null);
            }
        }
        super.onCreate(bundle);
        setStyle(1, getTheme());
        TraceMachine.exitMethod();
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        List<BelvedereIntent> intents = getArguments().getParcelableArrayList(EXTRA_INTENT);
        if (getParentFragment() != null) {
            final Fragment parentFragment = getParentFragment();
            fillListView(new StartActivity() {
                public void startActivity(BelvedereIntent belvedereIntent) {
                    belvedereIntent.open(parentFragment);
                }

                public Context getContext() {
                    return parentFragment.getContext();
                }
            }, intents);
        } else if (getActivity() != null) {
            final FragmentActivity activity = getActivity();
            fillListView(new StartActivity() {
                public void startActivity(BelvedereIntent belvedereIntent) {
                    belvedereIntent.open(activity);
                }

                public Context getContext() {
                    return activity;
                }
            }, intents);
        } else {
            Log.w("BelvedereDialog", "Not able to find a valid context for starting an BelvedereIntent");
            dismiss();
        }
    }

    private void fillListView(final StartActivity activity, List<BelvedereIntent> intents) {
        this.listView.setAdapter(new Adapter(activity.getContext(), R$layout.belvedere_dialog_row, intents));
        this.listView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(@NonNull AdapterView<?> adapterView, @NonNull View view, int position, long id) {
                if (view.getTag() instanceof BelvedereIntent) {
                    activity.startActivity((BelvedereIntent) view.getTag());
                    BelvedereDialog.this.dismiss();
                }
            }
        });
    }
}
