package com.pipedrive.views.edit.activity;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.TextView;
import com.pipedrive.R;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.ScreensMapper;
import com.pipedrive.application.Session;
import com.pipedrive.datetime.LocaleHelper;
import com.pipedrive.datetime.PipedriveDate;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.model.Activity;
import com.pipedrive.model.Activity.DateTimeInfo;
import com.pipedrive.views.common.ClickableTextViewWithUnderlineAndLabel;
import java.util.Calendar;

public class DueDateView extends ClickableTextViewWithUnderlineAndLabel {

    private interface GetDateStringForPipedriveDateTime {
        String getDateStringFor(@NonNull PipedriveDateTime pipedriveDateTime);

        String getDateStringForToday();
    }

    private interface GetDateStringForPipedriveDate {
        String getDateStringFor(@NonNull PipedriveDate pipedriveDate);

        String getDateStringForToday();
    }

    public interface OnDueDateChangedListener {
        void onDateChanged(@NonNull PipedriveDate pipedriveDate);
    }

    public DueDateView(Context context) {
        this(context, null);
    }

    public DueDateView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DueDateView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected int getLabelTextResourceId() {
        return R.string.date;
    }

    @MainThread
    public void setDate(@NonNull final Session session, @NonNull Activity activity) {
        setupDateViewForDate(activity, new GetDateStringForPipedriveDateTime() {
            public String getDateStringForToday() {
                return DueDateView.this.getResources().getString(R.string.today);
            }

            public String getDateStringFor(@NonNull PipedriveDateTime dateTime) {
                return LocaleHelper.getLocaleBasedDateStringInCurrentTimeZone(session, dateTime);
            }
        }, new GetDateStringForPipedriveDate() {
            public String getDateStringForToday() {
                return DueDateView.this.getResources().getString(R.string.today);
            }

            public String getDateStringFor(@NonNull PipedriveDate date) {
                return LocaleHelper.getLocaleBasedDateString(session, date);
            }
        });
    }

    private void setupDateViewForDate(@NonNull Activity activity, @NonNull GetDateStringForPipedriveDateTime getDateStringForPipedriveDateTime, @NonNull GetDateStringForPipedriveDate getDateStringForPipedriveDate) {
        int dueDateColorRes;
        String dateStringToShow;
        setText("");
        Boolean activityIsOverdue = activity.isOverdue();
        if (activityIsOverdue == null || !activityIsOverdue.booleanValue()) {
            Boolean activityIsDueToday = activity.isDueToday();
            if (activityIsDueToday == null || !activityIsDueToday.booleanValue()) {
                dueDateColorRes = R.color.dark;
            } else {
                dueDateColorRes = R.color.green;
            }
        } else {
            dueDateColorRes = R.color.red;
        }
        boolean askDateForToday = dueDateColorRes == R.color.green;
        PipedriveDateTime activityStartAsDateTime = activity.getActivityStartAsDateTime();
        if (activityStartAsDateTime == null) {
            PipedriveDate activityStartAsDate = activity.getActivityStartAsDate();
            if (activityStartAsDate == null) {
                dateStringToShow = "";
            } else if (askDateForToday) {
                dateStringToShow = getDateStringForPipedriveDate.getDateStringForToday();
            } else {
                dateStringToShow = getDateStringForPipedriveDate.getDateStringFor(activityStartAsDate);
            }
        } else if (askDateForToday) {
            dateStringToShow = getDateStringForPipedriveDateTime.getDateStringForToday();
        } else {
            dateStringToShow = getDateStringForPipedriveDateTime.getDateStringFor(activityStartAsDateTime);
        }
        setText(dateStringToShow);
        ((TextView) getMainView()).setTextColor(ContextCompat.getColor(getContext(), dueDateColorRes));
    }

    public void setOnDueDateChangedListener(@NonNull final Activity activity, @Nullable final OnDueDateChangedListener onDueDateChangedListener) {
        setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                final OnDateSetListener onDateSetListener = new OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        if (onDueDateChangedListener != null) {
                            onDueDateChangedListener.onDateChanged(PipedriveDate.instanceFrom(year, monthOfYear, dayOfMonth));
                        }
                    }
                };
                activity.request(new DateTimeInfo() {
                    public void activityHasStartDateTime(@NonNull PipedriveDateTime dateTime, @NonNull Activity inActivity) {
                        Calendar dateTimeCalendarLocal = dateTime.toLocalCalendar();
                        DueDateView.this.showDatePickerDialog(onDateSetListener, dateTimeCalendarLocal.get(1), dateTimeCalendarLocal.get(2), dateTimeCalendarLocal.get(5));
                    }

                    public void activityHasStartDate(@NonNull PipedriveDate date, @NonNull Activity inActivity) {
                        DueDateView.this.showDatePickerDialog(onDateSetListener, date.getYear(), date.getMonth(), date.getDayOfMonth());
                    }
                });
            }
        });
    }

    private void showDatePickerDialog(@NonNull OnDateSetListener onDateSetListener, int year, int month, int dayOfMonth) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.Theme.Pipedrive.Dialog.Alert, onDateSetListener, year, month, dayOfMonth);
        Analytics.hitScreen(ScreensMapper.SCREEN_NAME_ACTIVITY_DATE_DIALOG, getContext());
        datePickerDialog.show();
    }
}
