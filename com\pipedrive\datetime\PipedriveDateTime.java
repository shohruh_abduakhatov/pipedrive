package com.pipedrive.datetime;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.gson.UtcDateTypeAdapter;
import com.pipedrive.util.time.TimeManager;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class PipedriveDateTime extends Date {
    protected PipedriveDateTime(long milliseconds) {
        super(milliseconds);
    }

    @NonNull
    public static PipedriveDateTime instanceWithCurrentDateTime() {
        return new PipedriveDateTime(TimeManager.getInstance().currentTimeMillis().longValue());
    }

    @Nullable
    public static PipedriveDateTime instanceFormApiLongRepresentation(@Nullable String datetime) {
        if (datetime == null) {
            return null;
        }
        return instanceFromDate(UtcDateTypeAdapter.parseDateTime(datetime));
    }

    @Nullable
    public static PipedriveDateTime instanceFromDate(@Nullable Date date) {
        if (date == null) {
            return null;
        }
        return new PipedriveDateTime(date.getTime());
    }

    @Nullable
    public static PipedriveDateTime instanceFromUnixTimeRepresentation(@Nullable Long unixTime) {
        if (unixTime == null) {
            return null;
        }
        return new PipedriveDateTime(TimeUnit.SECONDS.toMillis(unixTime.longValue()));
    }

    @NonNull
    public static PipedriveDateTime instanceFromAddition(@NonNull PipedriveDate pipedriveDate, @Nullable PipedriveTime pipedriveTime) {
        return new PipedriveDateTime(TimeUnit.SECONDS.toMillis(pipedriveDate.getRepresentationInUnixTime() + (pipedriveTime == null ? 0 : pipedriveTime.getRepresentationInUnixTime())));
    }

    @NonNull
    public static PipedriveDateTime instanceFromAdditionDependingOnDateTimezone(@NonNull PipedriveDate pipedriveDate, @NonNull PipedriveTime pipedriveTime) {
        return new PipedriveDateTime(TimeUnit.SECONDS.toMillis((pipedriveDate.getRepresentationInUnixTime() + pipedriveTime.getRepresentationInUnixTime()) - TimeUnit.MILLISECONDS.toSeconds((long) TimeManager.getInstance().getDefaultTimeZone().getOffset(TimeUnit.SECONDS.toMillis(pipedriveDate.getRepresentationInUnixTime())))));
    }

    @NonNull
    static PipedriveDateTime fromUtcDateTime(int year, int month, int day, int hour, int minute, int second, int millisecond) {
        Calendar calendar = TimeManager.getInstance().getUtcCalendar();
        calendar.set(year, month, day, hour, minute, second);
        calendar.set(14, millisecond);
        return new PipedriveDateTime(calendar.getTimeInMillis());
    }

    @NonNull
    static PipedriveDateTime fromUtcDate(int year, int month, int day) {
        return fromUtcDateTime(year, month, day, 0, 0, 0, 0);
    }

    @NonNull
    private static PipedriveDateTime instanceFromUTC(int year, int month, int day, int hour, int minute, int second, int millisecond) {
        Calendar calendar = TimeManager.getInstance().getLocalCalendar();
        calendar.set(year, month, day, hour, minute, second);
        calendar.set(14, millisecond);
        calendar.setTimeZone(TimeManager.getInstance().getUtcTimeZone());
        return new PipedriveDateTime(calendar.getTimeInMillis());
    }

    @NonNull
    public static PipedriveDateTime instanceFromUTC(int year, int month, int day) {
        return instanceFromUTC(year, month, day, 0, 0, 0, 0);
    }

    public long getRepresentationInUnixTime() {
        return TimeUnit.MILLISECONDS.toSeconds(getTime());
    }

    @NonNull
    public String getRepresentationInUtcForApiLongRepresentation() {
        return DateFormatHelper.fullDateFormat2UTC().format(this);
    }

    public boolean before(@NonNull PipedriveDateTime pipedriveDateTime) {
        return super.before(pipedriveDateTime);
    }

    public boolean after(@NonNull PipedriveDateTime pipedriveDateTime) {
        return super.after(pipedriveDateTime);
    }

    public long getTime() {
        return super.getTime();
    }

    @NonNull
    public PipedriveDate getPipedriveDate() {
        return new PipedriveDate(getTime());
    }

    @NonNull
    public PipedriveDate getPipedriveDateLocal() {
        return PipedriveDate.instanceFrom(toLocalCalendar());
    }

    @NonNull
    public PipedriveTime getPipedriveTime() {
        return PipedriveTime.from(getHourOfDay(), getMinute(), getSecond());
    }

    @NonNull
    public PipedriveTime getPipedriveTimeLocal() {
        Calendar localCalendar = toLocalCalendar();
        return PipedriveTime.from(localCalendar.get(11), localCalendar.get(12), localCalendar.get(13));
    }

    public Calendar toCalendar() {
        Calendar calendar = TimeManager.getInstance().getUtcCalendar();
        calendar.setTime(this);
        return calendar;
    }

    public Calendar toLocalCalendar() {
        Calendar calendar = TimeManager.getInstance().getLocalCalendar();
        calendar.setTime(this);
        return calendar;
    }

    public PipedriveDateTime resetTime() {
        Calendar calendar = toCalendar();
        calendar.set(11, 0);
        calendar.clear(12);
        calendar.clear(13);
        calendar.clear(14);
        setTime(calendar.getTimeInMillis());
        return this;
    }

    public PipedriveDateTime resetTimeLocal() {
        Calendar calendarUtc = toCalendar();
        Calendar calendarLocal = TimeManager.getInstance().getLocalCalendar();
        calendarLocal.set(calendarUtc.get(1), calendarUtc.get(2), calendarUtc.get(5), 0, 0, 0);
        calendarLocal.clear(14);
        setTime(calendarLocal.getTimeInMillis());
        return this;
    }

    public PipedriveDateTime setTimeToEndOfTheDayLocal() {
        Calendar calendarUtc = toCalendar();
        Calendar calendarLocal = TimeManager.getInstance().getLocalCalendar();
        calendarLocal.set(calendarUtc.get(1), calendarUtc.get(2), calendarUtc.get(5), 23, 59, 59);
        calendarLocal.clear(14);
        setTime(calendarLocal.getTimeInMillis());
        return this;
    }

    public PipedriveDateTime roundUpToHour() {
        Calendar calendarUtc = toCalendar();
        calendarUtc.set(13, 0);
        calendarUtc.set(14, 0);
        if (calendarUtc.get(12) > 0) {
            calendarUtc.set(12, 0);
            calendarUtc.add(11, 1);
        }
        setTime(calendarUtc.getTimeInMillis());
        return this;
    }

    public PipedriveDuration durationTo(@NonNull PipedriveDateTime to) {
        return new PipedriveDuration(Math.abs(to.getTime() - getTime()));
    }

    public int getYearOfDate() {
        return toCalendar().get(1);
    }

    public int getMonthOfDate() {
        return toCalendar().get(2);
    }

    public int getDayOfMonth() {
        return toCalendar().get(5);
    }

    public int getHourOfDay() {
        return toCalendar().get(11);
    }

    public int getMinute() {
        return toCalendar().get(12);
    }

    public int getSecond() {
        return toCalendar().get(13);
    }

    public int getDayOfMonthLocalTime() {
        return toLocalCalendar().get(5);
    }

    public boolean isBetween(@NonNull PipedriveDateTime from, @NonNull PipedriveDateTime to) {
        return from.before(to) ? after(from) && before(to) : after(to) && before(from);
    }

    public boolean isDayOfMonthSameAs(@NonNull PipedriveDateTime pipedriveDateTime) {
        return getDayOfMonthLocalTime() == pipedriveDateTime.getDayOfMonthLocalTime();
    }

    public PipedriveDateTime applyLocalDateOnly(Date date) {
        Calendar dateToApply = instanceFromDate(date).toLocalCalendar();
        int year = dateToApply.get(1);
        int month = dateToApply.get(2);
        int dayOfMonth = dateToApply.get(5);
        Calendar resultingCalendar = toLocalCalendar();
        resultingCalendar.set(year, month, dayOfMonth);
        return instanceFromDate(resultingCalendar.getTime());
    }

    public PipedriveDateTime applyLocalTimeOnly(@NonNull Date date) {
        Calendar timeToApply = instanceFromDate(date).toLocalCalendar();
        int hourOfDay = timeToApply.get(11);
        int minute = timeToApply.get(12);
        Calendar resultingCalendar = toLocalCalendar();
        resultingCalendar.set(11, hourOfDay);
        resultingCalendar.set(12, minute);
        resultingCalendar.clear(13);
        resultingCalendar.clear(14);
        return instanceFromDate(resultingCalendar.getTime());
    }

    public String toString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", new Locale("en", "US"));
        simpleDateFormat.setTimeZone(TimeManager.getInstance().getDefaultTimeZone());
        return simpleDateFormat.format(this);
    }
}
