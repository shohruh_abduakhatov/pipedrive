package com.pipedrive.datetime;

import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.gson.UtcDateTypeAdapter;
import com.pipedrive.util.time.TimeManager;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class PipedriveDate {
    private final int dayOfMonth;
    private final int month;
    private final int year;

    private PipedriveDate(@IntRange(from = 0) int year, @IntRange(from = 0) int month, @IntRange(from = 1) int dayOfMonth) {
        this.year = year;
        this.month = month;
        this.dayOfMonth = dayOfMonth;
    }

    protected PipedriveDate(long milliseconds) {
        Calendar calendar = TimeManager.getInstance().getUtcCalendar();
        calendar.setTimeInMillis(milliseconds);
        this.year = calendar.get(1);
        this.month = calendar.get(2);
        this.dayOfMonth = calendar.get(5);
    }

    @NonNull
    public static PipedriveDate instanceFrom(long milliseconds) {
        return new PipedriveDate(milliseconds);
    }

    @NonNull
    public static PipedriveDate instanceFrom(@IntRange(from = 0) int year, @IntRange(from = 0, to = 11) int monthOfYear, @IntRange(from = 1, to = 31) int dayOfMonth) {
        return new PipedriveDate(year, monthOfYear, dayOfMonth);
    }

    @NonNull
    public static PipedriveDate instanceWithCurrentDate() {
        return instanceFrom(TimeManager.getInstance().getLocalCalendar());
    }

    private Calendar toCalendar() {
        Calendar calendar = TimeManager.getInstance().getUtcCalendar();
        calendar.setTimeInMillis(0);
        calendar.set(this.year, this.month, this.dayOfMonth);
        return calendar;
    }

    @Nullable
    public static PipedriveDate instanceFormApiLongRepresentation(@Nullable String date) {
        if (date == null) {
            return null;
        }
        Date parsedDate = UtcDateTypeAdapter.parseDate(date);
        if (parsedDate != null) {
            return new PipedriveDate(parsedDate.getTime());
        }
        return null;
    }

    @NonNull
    public static PipedriveDate instanceFrom(@NonNull Calendar calendar) {
        return new PipedriveDate(calendar.get(1), calendar.get(2), calendar.get(5));
    }

    @Nullable
    public static PipedriveDate instanceFromUnixTimeRepresentation(@Nullable @IntRange(from = Long.MIN_VALUE, to = Long.MAX_VALUE) Long dateInUnixTime) {
        if (dateInUnixTime == null) {
            return null;
        }
        return new PipedriveDate(TimeUnit.SECONDS.toMillis(dateInUnixTime.longValue()));
    }

    @NonNull
    public PipedriveDate instanceClone() {
        return new PipedriveDate(TimeUnit.SECONDS.toMillis(getRepresentationInUnixTime()));
    }

    @NonNull
    public String getRepresentationForApiLongRepresentation() {
        return getRepresentationForApi();
    }

    public long getRepresentationInUnixTime() {
        return TimeUnit.MILLISECONDS.toSeconds(toCalendar().getTimeInMillis());
    }

    public int getYear() {
        return this.year;
    }

    public int getMonth() {
        return this.month;
    }

    public int getDayOfMonth() {
        return this.dayOfMonth;
    }

    public String toString() {
        return getRepresentationForApi();
    }

    private String getRepresentationForApi() {
        String yearNormalized = String.valueOf(this.year);
        String monthNormalized = normalizedDateUnit((long) (this.month + 1));
        String dayNormalized = normalizedDateUnit((long) this.dayOfMonth);
        return String.format("%s-%s-%s", new Object[]{yearNormalized, monthNormalized, dayNormalized});
    }

    private String normalizedDateUnit(long date) {
        if (date < 10) {
            return "0" + date;
        }
        return String.valueOf(date);
    }
}
