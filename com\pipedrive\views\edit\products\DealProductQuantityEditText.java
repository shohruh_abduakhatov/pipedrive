package com.pipedrive.views.edit.products;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import com.pipedrive.R;
import com.pipedrive.model.products.DealProduct;

public class DealProductQuantityEditText extends DealProductNumericEditTextWithFormatting {
    public DealProductQuantityEditText(Context context) {
        super(context);
    }

    public DealProductQuantityEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DealProductQuantityEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected int getLabelTextResourceId() {
        return R.string.lbl_quantity;
    }

    double getInitialValue(@NonNull DealProduct dealProduct) {
        return dealProduct.getQuantity().doubleValue();
    }
}
