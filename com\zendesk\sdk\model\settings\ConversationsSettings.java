package com.zendesk.sdk.model.settings;

public class ConversationsSettings {
    private boolean enabled;

    public boolean isEnabled() {
        return this.enabled;
    }
}
