package com.pipedrive.util;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.widget.Toast;
import com.pipedrive.R;

public class PipedriveUtil {
    public static final String ACCOUNT_TYPE = "com.pipedrive.account";

    private PipedriveUtil() {
    }

    public static void call(@NonNull Context nonApplicationContext, @NonNull String number) {
        if (!TextUtils.isEmpty(number)) {
            Intent callIntent = new Intent("android.intent.action.CALL");
            callIntent.setData(Uri.parse("tel:" + number));
            nonApplicationContext.startActivity(callIntent);
        }
    }

    public static void sendSms(Context ctx, String number) {
        if (!TextUtils.isEmpty(number)) {
            ctx.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("sms:" + number)));
        }
    }

    public static Uri getMapUriForAddress(@NonNull String address) {
        return Uri.parse("geo:0,0?q=" + address);
    }

    public static void openMapForAddress(Context context, @NonNull String address) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(getMapUriForAddress(address));
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    public static boolean openWebPage(@NonNull Context context, @NonNull Uri uri) {
        Intent intent = new Intent("android.intent.action.VIEW", uri);
        if (intent.resolveActivity(context.getPackageManager()) == null) {
            return false;
        }
        context.startActivity(intent);
        return true;
    }

    public static boolean openWebPage(@NonNull Context context, @StringRes int url) {
        return openWebPage(context, Uri.parse(context.getResources().getString(url)));
    }

    public static void copyTextToClipboard(String textToCopy, Context context) {
        if (!TextUtils.isEmpty(textToCopy)) {
            ((ClipboardManager) context.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText(textToCopy, textToCopy));
            Toast.makeText(context, context.getText(R.string.label_action_copied_to_clipboard), 0).show();
        }
    }

    public static void vibrate(Context ctx) {
        if (ctx != null && ((AudioManager) ctx.getSystemService("audio")).getRingerMode() != 0) {
            Vibrator vibrator = (Vibrator) ctx.getSystemService("vibrator");
            if (vibrator != null && vibrator.hasVibrator()) {
                vibrator.vibrate(10);
            }
        }
    }
}
