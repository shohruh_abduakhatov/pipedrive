package com.zendesk.util;

import android.graphics.Color;
import com.zendesk.logger.Logger;

public class ColorUtils {
    public static final String LOG_TAG = "ColorUtils";

    private ColorUtils() {
    }

    public static Integer apiColorToAndroidColor(String hexColor) {
        Integer num = null;
        if (StringUtils.isEmpty(hexColor)) {
            Logger.e(LOG_TAG, "The supplied hex value is null or empty, returning null", new Object[0]);
        } else {
            String correctedHexColor = hexColor.startsWith("#") ? hexColor : "#" + hexColor;
            if (correctedHexColor.length() != 7) {
                Logger.e(LOG_TAG, "The hex value is malformed, returning null for input: " + correctedHexColor, new Object[0]);
            } else {
                num = null;
                try {
                    num = Integer.valueOf(Color.parseColor(correctedHexColor));
                } catch (IllegalArgumentException e) {
                    Logger.e(LOG_TAG, e.getMessage(), e, new Object[0]);
                }
            }
        }
        return num;
    }

    public static java.awt.Color apiColorToJavaColor(String hexColor) {
        java.awt.Color color = null;
        if (StringUtils.isEmpty(hexColor)) {
            Logger.e(LOG_TAG, "The supplied hex value is null or empty, returning null", new Object[0]);
        } else {
            String correctedHexColor = hexColor.startsWith("#") ? hexColor : "#" + hexColor;
            if (correctedHexColor.length() != 7) {
                Logger.e(LOG_TAG, "The hex value is malformed, returning null for input: " + correctedHexColor, new Object[0]);
            } else {
                color = null;
                try {
                    color = java.awt.Color.decode(correctedHexColor);
                } catch (NumberFormatException e) {
                    Logger.e(LOG_TAG, e.getMessage(), e, new Object[0]);
                }
            }
        }
        return color;
    }
}
