package com.zendesk.sdk.storage;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.sdk.model.push.PushRegistrationResponse;
import com.zendesk.sdk.storage.SdkStorage.UserStorage;

public interface PushRegistrationResponseStorage extends UserStorage {
    @Nullable
    PushRegistrationResponse getPushRegistrationResponse();

    boolean hasStoredPushRegistrationResponse();

    void removePushRegistrationResponse();

    void storePushRegistrationResponse(@NonNull PushRegistrationResponse pushRegistrationResponse);
}
