package com.zendesk.sdk.support;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.newrelic.agent.android.instrumentation.okhttp3.OkHttp3Instrumentation;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import com.zendesk.sdk.model.helpcenter.Article;
import com.zendesk.sdk.model.helpcenter.Attachment;
import com.zendesk.sdk.model.helpcenter.AttachmentType;
import com.zendesk.sdk.model.helpcenter.SimpleArticle;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.ui.ListRowView;
import com.zendesk.sdk.ui.LoadingState;
import com.zendesk.sdk.ui.NetworkAwareActionbarActivity;
import com.zendesk.sdk.ui.ToolbarSherlock;
import com.zendesk.sdk.util.UiUtils;
import com.zendesk.service.ErrorResponse;
import com.zendesk.service.SafeZendeskCallback;
import com.zendesk.service.ZendeskCallback;
import com.zendesk.util.FileUtils;
import com.zendesk.util.LocaleUtil;
import com.zendesk.util.StringUtils;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.Response;

public class ViewArticleActivity extends NetworkAwareActionbarActivity implements OnItemClickListener {
    private static final String ARTICLE_DATE_FORMAT = "MMMM d, yyyy hh:mm";
    private static final String ARTICLE_DETAIL_FORMAT_STRING = "%s %s %s";
    private static final String CSS_FILE = "file:///android_asset/help_center_article_style.css";
    public static final String EXTRA_ARTICLE = "article";
    public static final String EXTRA_SIMPLE_ARTICLE = "article_simple";
    private static final long FETCH_ATTACHMENTS_DELAY_MILLIS = 250;
    private static final String LOG_TAG = ViewArticleActivity.class.getSimpleName();
    private static final String TYPE_TEXT_HTML = "text/html";
    private static final String UTF_8_ENCODING_TYPE = "UTF-8";
    private Article mArticle;
    private WebView mArticleContentWebView;
    private Long mArticleId;
    private ListView mAttachmentListView;
    private SafeZendeskCallback<List<Attachment>> mAttachmentRequestCallback;
    private final Handler mHandler = new Handler();
    private ProgressBar mProgressView;

    private static class ArticleAttachmentAdapter extends ArrayAdapter<Attachment> {
        public ArticleAttachmentAdapter(Context context, List<Attachment> attachments) {
            super(context, R.layout.row_article_attachment, attachments);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ArticleAttachmentRow sectionRow;
            if (convertView instanceof ArticleAttachmentRow) {
                sectionRow = (ArticleAttachmentRow) convertView;
            } else {
                sectionRow = new ArticleAttachmentRow(getContext());
            }
            sectionRow.bind((Attachment) getItem(position));
            return sectionRow.getView();
        }
    }

    private static class ArticleAttachmentRow extends RelativeLayout implements ListRowView<Attachment> {
        private final Context mContext;
        private TextView mFileName;
        private TextView mFileSize;

        public ArticleAttachmentRow(Context context) {
            super(context);
            this.mContext = context;
            initialise();
        }

        private void initialise() {
            View container = LayoutInflater.from(this.mContext).inflate(R.layout.row_article_attachment, this);
            if (container != null) {
                this.mFileName = (TextView) container.findViewById(R.id.article_attachment_row_filename_text);
                this.mFileSize = (TextView) container.findViewById(R.id.article_attachment_row_filesize_text);
            }
        }

        public void bind(Attachment attachment) {
            this.mFileName.setText(attachment.getFileName());
            this.mFileSize.setText(FileUtils.humanReadableFileSize(attachment.getSize()));
        }

        public View getView() {
            return this;
        }
    }

    class AttachmentRequestCallback extends ZendeskCallback<List<Attachment>> {
        AttachmentRequestCallback() {
        }

        public void onSuccess(List<Attachment> result) {
            ViewArticleActivity.this.mAttachmentListView.setAdapter(new ArticleAttachmentAdapter(ViewArticleActivity.this, result));
            ViewArticleActivity.this.mAttachmentListView.setOnItemClickListener(ViewArticleActivity.this);
            ViewArticleActivity.setListViewHeightBasedOnChildren(ViewArticleActivity.this.mAttachmentListView);
            ViewArticleActivity.this.setLoadingState(LoadingState.DISPLAYING);
        }

        public void onError(ErrorResponse error) {
            ViewArticleActivity.this.setLoadingState(LoadingState.ERRORED);
            Logger.e(ViewArticleActivity.LOG_TAG, error);
        }
    }

    public static void startActivity(Context context, Article article) {
        Intent intent = new Intent(context, ViewArticleActivity.class);
        intent.putExtra(EXTRA_ARTICLE, article);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, SimpleArticle simpleArticle) {
        Intent intent = new Intent(context, ViewArticleActivity.class);
        intent.putExtra(EXTRA_SIMPLE_ARTICLE, simpleArticle);
        context.startActivity(intent);
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    @TargetApi(21)
    protected void onCreate(Bundle savedInstanceState) {
        boolean hasArticleExtra;
        boolean hasSimpleArticleExtra = true;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_article);
        ToolbarSherlock.installToolBar(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        this.mArticleContentWebView = (WebView) findViewById(R.id.view_article_content_webview);
        this.mArticleContentWebView.setWebChromeClient(new WebChromeClient());
        this.mArticleContentWebView.getSettings().setJavaScriptEnabled(true);
        setupRequestInterceptor();
        if (VERSION.SDK_INT >= 21) {
            this.mArticleContentWebView.getSettings().setMixedContentMode(0);
        }
        this.mProgressView = (ProgressBar) findViewById(R.id.view_article_progress);
        this.mAttachmentListView = (ListView) findViewById(R.id.view_article_attachment_list);
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(EXTRA_ARTICLE) && (extras.getSerializable(EXTRA_ARTICLE) instanceof Article)) {
            hasArticleExtra = true;
        } else {
            hasArticleExtra = false;
        }
        if (!(extras != null && extras.containsKey(EXTRA_SIMPLE_ARTICLE) && (extras.getSerializable(EXTRA_SIMPLE_ARTICLE) instanceof SimpleArticle))) {
            hasSimpleArticleExtra = false;
        }
        if (hasArticleExtra) {
            this.mArticle = (Article) extras.getSerializable(EXTRA_ARTICLE);
            loadArticleBody();
        } else if (hasSimpleArticleExtra) {
            SimpleArticle simpleArticle = (SimpleArticle) extras.getSerializable(EXTRA_SIMPLE_ARTICLE);
            if (simpleArticle.getId() != null) {
                fetchArticle(simpleArticle.getId().longValue());
                this.mArticleId = simpleArticle.getId();
                return;
            }
            Logger.e(LOG_TAG, "EXTRA_SIMPLE_ARTICLE is present, but missing an article ID. Cannot continue.", new Object[0]);
            finish();
        } else {
            Logger.e(LOG_TAG, "EXTRA_ARTICLE or EXTRA_SIMPLE_ARTICLE is a required extra for this activity. Cannot continue.", new Object[0]);
            finish();
        }
    }

    private void setupRequestInterceptor() {
        if (this.mArticleContentWebView == null) {
            Logger.w(LOG_TAG, "The webview is null. Make sure you initialise it before trying to add the interceptor", new Object[0]);
            return;
        }
        final OkHttpClient httpClient = new OkHttpClient();
        final ZendeskDeepLinkingParser deepLinkingParser = new ZendeskDeepLinkingParser();
        final boolean interceptRequests = ZendeskConfig.INSTANCE.getSdkOptions().overrideResourceLoadingInWebview();
        this.mArticleContentWebView.setWebViewClient(new WebViewClient() {
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                if (interceptRequests) {
                    String zendeskUrl = ZendeskConfig.INSTANCE.getZendeskUrl();
                    if (StringUtils.isEmpty(zendeskUrl) || !url.startsWith(zendeskUrl)) {
                        Logger.w(ViewArticleActivity.LOG_TAG, "Will not intercept request because the url is not hosted by Zendesk" + url, new Object[0]);
                        return super.shouldInterceptRequest(view, url);
                    }
                    String bearerToken = ZendeskConfig.INSTANCE.storage().identityStorage().getStoredAccessTokenAsBearerToken();
                    if (StringUtils.isEmpty(bearerToken)) {
                        Logger.w(ViewArticleActivity.LOG_TAG, "We will not intercept the request there is no authentication present", new Object[0]);
                        return super.shouldInterceptRequest(view, url);
                    }
                    InputStream content = null;
                    String contentType = null;
                    String contentEncoding = null;
                    try {
                        Call newCall;
                        Builder addHeader = new Builder().url(url).get().addHeader("Authorization", bearerToken);
                        Request request = !(addHeader instanceof Builder) ? addHeader.build() : OkHttp3Instrumentation.build(addHeader);
                        OkHttpClient okHttpClient = httpClient;
                        if (okHttpClient instanceof OkHttpClient) {
                            newCall = OkHttp3Instrumentation.newCall(okHttpClient, request);
                        } else {
                            newCall = okHttpClient.newCall(request);
                        }
                        Response response = newCall.execute();
                        if (!(response == null || !response.isSuccessful() || response.body() == null)) {
                            content = response.body().byteStream();
                            MediaType mediaType = response.body().contentType();
                            if (mediaType != null) {
                                if (StringUtils.hasLength(mediaType.type()) && StringUtils.hasLength(mediaType.subtype())) {
                                    contentType = String.format(Locale.US, "%s/%s", new Object[]{mediaType.type(), mediaType.subtype()});
                                }
                                Charset charset = mediaType.charset();
                                if (charset != null) {
                                    contentEncoding = charset.name();
                                }
                            }
                        }
                    } catch (Exception e) {
                        Logger.e(ViewArticleActivity.LOG_TAG, "Exception encountered when trying to intercept request", e, new Object[0]);
                    }
                    return new WebResourceResponse(contentType, contentEncoding, content);
                }
                Logger.w(ViewArticleActivity.LOG_TAG, "Cannot add request interceptor because sdk options prohibit it. See ZendeskConfig.INSTANCE.getSdkOptions()", new Object[0]);
                return super.shouldInterceptRequest(view, url);
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Intent intent = deepLinkingParser.parse(url, ViewArticleActivity.this);
                Context context = ViewArticleActivity.this;
                if (intent != null) {
                    try {
                        context.startActivity(intent);
                        return true;
                    } catch (ActivityNotFoundException e) {
                        Logger.w(ViewArticleActivity.LOG_TAG, "Could not open Intent: %s", intent);
                    }
                }
                return false;
            }
        });
    }

    private static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {
            int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), 0);
            int totalHeight = 0;
            View view = null;
            for (int i = 0; i < listAdapter.getCount(); i++) {
                view = listAdapter.getView(i, view, listView);
                if (i == 0) {
                    view.setLayoutParams(new LayoutParams(desiredWidth, -2));
                }
                view.measure(desiredWidth, 0);
                totalHeight += view.getMeasuredHeight();
            }
            LayoutParams params = listView.getLayoutParams();
            params.height = (listView.getDividerHeight() * (listAdapter.getCount() - 1)) + totalHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();
        }
    }

    protected void onResume() {
        super.onResume();
        this.mAttachmentRequestCallback = SafeZendeskCallback.from(new AttachmentRequestCallback());
    }

    protected void onPause() {
        super.onPause();
        this.mAttachmentRequestCallback.cancel();
        this.mAttachmentRequestCallback = null;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != 16908332) {
            return super.onOptionsItemSelected(item);
        }
        onBackPressed();
        return true;
    }

    private void fetchAttachmentsForArticle(long articleId) {
        setLoadingState(LoadingState.LOADING);
        ZendeskConfig.INSTANCE.provider().helpCenterProvider().getAttachments(Long.valueOf(articleId), AttachmentType.BLOCK, this.mAttachmentRequestCallback);
    }

    private void fetchArticle(long articleId) {
        setLoadingState(LoadingState.LOADING);
        ZendeskConfig.INSTANCE.provider().helpCenterProvider().getArticle(Long.valueOf(articleId), new ZendeskCallback<Article>() {
            public void onSuccess(Article result) {
                ViewArticleActivity.this.mArticle = result;
                ViewArticleActivity.this.loadArticleBody();
            }

            public void onError(ErrorResponse error) {
                ViewArticleActivity.this.setLoadingState(LoadingState.ERRORED);
            }
        });
    }

    private void loadArticleBody() {
        ZendeskConfig.INSTANCE.getTracker().helpCenterArticleViewed();
        ZendeskConfig.INSTANCE.provider().helpCenterProvider().submitRecordArticleView(this.mArticle.getId(), LocaleUtil.forLanguageTag(this.mArticle.getLocale()), new ZendeskCallback<Void>() {
            public void onSuccess(Void result) {
            }

            public void onError(ErrorResponse error) {
                Logger.e(ViewArticleActivity.LOG_TAG, String.format(Locale.US, "Error submitting Help Center reporting: [reason] %s [isNetworkError] %s [status] %d", new Object[]{error.getReason(), Boolean.valueOf(error.isNetworkError()), Integer.valueOf(error.getStatus())}), new Object[0]);
            }
        });
        setLoadingState(LoadingState.DISPLAYING);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(this.mArticle.getTitle());
        }
        String dateFormatted = null;
        String authorName = this.mArticle.getAuthor() == null ? null : this.mArticle.getAuthor().getName();
        if (this.mArticle.getCreatedAt() != null) {
            dateFormatted = new SimpleDateFormat(ARTICLE_DATE_FORMAT, Locale.getDefault()).format(this.mArticle.getCreatedAt());
        }
        String details = (dateFormatted == null || authorName == null) ? "" : String.format(Locale.US, ARTICLE_DETAIL_FORMAT_STRING, new Object[]{authorName, getString(R.string.view_article_seperator), dateFormatted});
        this.mArticleContentWebView.loadDataWithBaseURL(ZendeskConfig.INSTANCE.getZendeskUrl(), String.format("<HTML><HEAD><LINK href='%s' type='text/css' rel='stylesheet'/></HEAD><body><h1>%s</h1>%s<footer><p>%s</p></footer></body></HTML>", new Object[]{CSS_FILE, this.mArticle.getTitle(), this.mArticle.getBody(), details}), TYPE_TEXT_HTML, "UTF-8", null);
        this.mHandler.postDelayed(new Runnable() {
            public void run() {
                ViewArticleActivity.this.fetchAttachmentsForArticle(ViewArticleActivity.this.mArticle.getId().longValue());
            }
        }, FETCH_ATTACHMENTS_DELAY_MILLIS);
    }

    protected void setLoadingState(LoadingState loadingState) {
        if (loadingState == null) {
            Logger.w(LOG_TAG, "LoadingState was null, nothing to do", new Object[0]);
            return;
        }
        switch (loadingState) {
            case LOADING:
                UiUtils.setVisibility(this.mProgressView, 0);
                UiUtils.setVisibility(this.mAttachmentListView, 8);
                onRetryUnavailable();
                return;
            case DISPLAYING:
                UiUtils.setVisibility(this.mProgressView, 8);
                UiUtils.setVisibility(this.mAttachmentListView, 0);
                onRetryUnavailable();
                return;
            case ERRORED:
                UiUtils.setVisibility(this.mProgressView, 8);
                UiUtils.setVisibility(this.mAttachmentListView, 8);
                onRetryAvailable(getString(R.string.view_article_attachments_error), new OnClickListener() {
                    public void onClick(View v) {
                        if (ViewArticleActivity.this.mArticleId != null && ViewArticleActivity.this.mArticle == null) {
                            ViewArticleActivity.this.fetchArticle(ViewArticleActivity.this.mArticleId.longValue());
                        } else if (ViewArticleActivity.this.mArticle != null) {
                            ViewArticleActivity.this.fetchAttachmentsForArticle(ViewArticleActivity.this.mArticle.getId().longValue());
                        }
                    }
                });
                return;
            default:
                return;
        }
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Attachment item = parent.getItemAtPosition(position);
        if (item instanceof Attachment) {
            Attachment attachment = item;
            if (attachment.getContentUrl() != null) {
                Uri attachmentUri = Uri.parse(attachment.getContentUrl());
                Intent viewAttachment = new Intent();
                viewAttachment.setAction("android.intent.action.VIEW");
                viewAttachment.setData(attachmentUri);
                startActivity(viewAttachment);
                return;
            }
            Logger.w(LOG_TAG, "Unable to launch viewer, unable to parse URI for attachment", new Object[0]);
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.mArticleContentWebView != null) {
            this.mArticleContentWebView.destroy();
        }
    }
}
