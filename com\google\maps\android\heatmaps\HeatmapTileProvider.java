package com.google.maps.android.heatmaps;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.Color;
import android.support.v4.util.LongSparseArray;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Tile;
import com.google.android.gms.maps.model.TileProvider;
import com.google.maps.android.geometry.Bounds;
import com.google.maps.android.geometry.Point;
import com.google.maps.android.quadtree.PointQuadTree;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class HeatmapTileProvider implements TileProvider {
    public static final Gradient DEFAULT_GRADIENT = new Gradient(DEFAULT_GRADIENT_COLORS, DEFAULT_GRADIENT_START_POINTS);
    private static final int[] DEFAULT_GRADIENT_COLORS = new int[]{Color.rgb(102, 225, 0), Color.rgb(255, 0, 0)};
    private static final float[] DEFAULT_GRADIENT_START_POINTS = new float[]{0.2f, 1.0f};
    private static final int DEFAULT_MAX_ZOOM = 11;
    private static final int DEFAULT_MIN_ZOOM = 5;
    public static final double DEFAULT_OPACITY = 0.7d;
    public static final int DEFAULT_RADIUS = 20;
    private static final int MAX_RADIUS = 50;
    private static final int MAX_ZOOM_LEVEL = 22;
    private static final int MIN_RADIUS = 10;
    private static final int SCREEN_SIZE = 1280;
    private static final int TILE_DIM = 512;
    static final double WORLD_WIDTH = 1.0d;
    private Bounds mBounds;
    private int[] mColorMap;
    private Collection<WeightedLatLng> mData;
    private Gradient mGradient;
    private double[] mKernel;
    private double[] mMaxIntensity;
    private double mOpacity;
    private int mRadius;
    private PointQuadTree<WeightedLatLng> mTree;

    public static class Builder {
        private Collection<WeightedLatLng> data;
        private Gradient gradient = HeatmapTileProvider.DEFAULT_GRADIENT;
        private double opacity = HeatmapTileProvider.DEFAULT_OPACITY;
        private int radius = 20;

        public Builder data(Collection<LatLng> val) {
            return weightedData(HeatmapTileProvider.wrapData(val));
        }

        public Builder weightedData(Collection<WeightedLatLng> val) {
            this.data = val;
            if (!this.data.isEmpty()) {
                return this;
            }
            throw new IllegalArgumentException("No input points.");
        }

        public Builder radius(int val) {
            this.radius = val;
            if (this.radius >= 10 && this.radius <= 50) {
                return this;
            }
            throw new IllegalArgumentException("Radius not within bounds.");
        }

        public Builder gradient(Gradient val) {
            this.gradient = val;
            return this;
        }

        public Builder opacity(double val) {
            this.opacity = val;
            if (this.opacity >= 0.0d && this.opacity <= 1.0d) {
                return this;
            }
            throw new IllegalArgumentException("Opacity must be in range [0, 1]");
        }

        public HeatmapTileProvider build() {
            if (this.data != null) {
                return new HeatmapTileProvider();
            }
            throw new IllegalStateException("No input data: you must use either .data or .weightedData before building");
        }
    }

    private HeatmapTileProvider(Builder builder) {
        this.mData = builder.data;
        this.mRadius = builder.radius;
        this.mGradient = builder.gradient;
        this.mOpacity = builder.opacity;
        this.mKernel = generateKernel(this.mRadius, ((double) this.mRadius) / 3.0d);
        setGradient(this.mGradient);
        setWeightedData(this.mData);
    }

    public void setWeightedData(Collection<WeightedLatLng> data) {
        this.mData = data;
        if (this.mData.isEmpty()) {
            throw new IllegalArgumentException("No input points.");
        }
        this.mBounds = getBounds(this.mData);
        this.mTree = new PointQuadTree(this.mBounds);
        for (WeightedLatLng l : this.mData) {
            this.mTree.add(l);
        }
        this.mMaxIntensity = getMaxIntensities(this.mRadius);
    }

    public void setData(Collection<LatLng> data) {
        setWeightedData(wrapData(data));
    }

    private static Collection<WeightedLatLng> wrapData(Collection<LatLng> data) {
        ArrayList<WeightedLatLng> weightedData = new ArrayList();
        for (LatLng l : data) {
            weightedData.add(new WeightedLatLng(l));
        }
        return weightedData;
    }

    public Tile getTile(int x, int y, int zoom) {
        double tileWidth = 1.0d / Math.pow(2.0d, (double) zoom);
        double padding = (((double) this.mRadius) * tileWidth) / 512.0d;
        double bucketWidth = (tileWidth + (2.0d * padding)) / ((double) ((this.mRadius * 2) + 512));
        double minX = (((double) x) * tileWidth) - padding;
        double maxX = (((double) (x + 1)) * tileWidth) + padding;
        double minY = (((double) y) * tileWidth) - padding;
        double maxY = (((double) (y + 1)) * tileWidth) + padding;
        double xOffset = 0.0d;
        Collection<WeightedLatLng> wrappedPoints = new ArrayList();
        if (minX < 0.0d) {
            xOffset = -1.0d;
            wrappedPoints = this.mTree.search(new Bounds(1.0d + minX, 1.0d, minY, maxY));
        } else if (maxX > 1.0d) {
            xOffset = 1.0d;
            wrappedPoints = this.mTree.search(new Bounds(0.0d, maxX - 1.0d, minY, maxY));
        }
        Bounds tileBounds = new Bounds(minX, maxX, minY, maxY);
        if (!tileBounds.intersects(new Bounds(this.mBounds.minX - padding, this.mBounds.maxX + padding, this.mBounds.minY - padding, this.mBounds.maxY + padding))) {
            return TileProvider.NO_TILE;
        }
        Collection<WeightedLatLng> points = this.mTree.search(tileBounds);
        if (points.isEmpty()) {
            return TileProvider.NO_TILE;
        }
        double[][] intensity = (double[][]) Array.newInstance(Double.TYPE, new int[]{(this.mRadius * 2) + 512, (this.mRadius * 2) + 512});
        for (WeightedLatLng w : points) {
            Point p = w.getPoint();
            int bucketY = (int) ((p.y - minY) / bucketWidth);
            double[] dArr = intensity[(int) ((p.x - minX) / bucketWidth)];
            dArr[bucketY] = dArr[bucketY] + w.getIntensity();
        }
        for (WeightedLatLng w2 : wrappedPoints) {
            p = w2.getPoint();
            bucketY = (int) ((p.y - minY) / bucketWidth);
            dArr = intensity[(int) (((p.x + xOffset) - minX) / bucketWidth)];
            dArr[bucketY] = dArr[bucketY] + w2.getIntensity();
        }
        return convertBitmap(colorize(convolve(intensity, this.mKernel), this.mColorMap, this.mMaxIntensity[zoom]));
    }

    public void setGradient(Gradient gradient) {
        this.mGradient = gradient;
        this.mColorMap = gradient.generateColorMap(this.mOpacity);
    }

    public void setRadius(int radius) {
        this.mRadius = radius;
        this.mKernel = generateKernel(this.mRadius, ((double) this.mRadius) / 3.0d);
        this.mMaxIntensity = getMaxIntensities(this.mRadius);
    }

    public void setOpacity(double opacity) {
        this.mOpacity = opacity;
        setGradient(this.mGradient);
    }

    private double[] getMaxIntensities(int radius) {
        int i;
        double[] maxIntensityArray = new double[22];
        for (i = 5; i < 11; i++) {
            maxIntensityArray[i] = getMaxValue(this.mData, this.mBounds, radius, (int) (1280.0d * Math.pow(2.0d, (double) (i - 3))));
            if (i == 5) {
                for (int j = 0; j < i; j++) {
                    maxIntensityArray[j] = maxIntensityArray[i];
                }
            }
        }
        for (i = 11; i < 22; i++) {
            maxIntensityArray[i] = maxIntensityArray[10];
        }
        return maxIntensityArray;
    }

    private static Tile convertBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.PNG, 100, stream);
        return new Tile(512, 512, stream.toByteArray());
    }

    static Bounds getBounds(Collection<WeightedLatLng> points) {
        Iterator<WeightedLatLng> iter = points.iterator();
        WeightedLatLng first = (WeightedLatLng) iter.next();
        double minX = first.getPoint().x;
        double maxX = first.getPoint().x;
        double minY = first.getPoint().y;
        double maxY = first.getPoint().y;
        while (iter.hasNext()) {
            WeightedLatLng l = (WeightedLatLng) iter.next();
            double x = l.getPoint().x;
            double y = l.getPoint().y;
            if (x < minX) {
                minX = x;
            }
            if (x > maxX) {
                maxX = x;
            }
            if (y < minY) {
                minY = y;
            }
            if (y > maxY) {
                maxY = y;
            }
        }
        return new Bounds(minX, maxX, minY, maxY);
    }

    static double[] generateKernel(int radius, double sd) {
        double[] kernel = new double[((radius * 2) + 1)];
        for (int i = -radius; i <= radius; i++) {
            kernel[i + radius] = Math.exp(((double) ((-i) * i)) / ((2.0d * sd) * sd));
        }
        return kernel;
    }

    static double[][] convolve(double[][] grid, double[] kernel) {
        int radius = (int) Math.floor(((double) kernel.length) / 2.0d);
        int dimOld = grid.length;
        int dim = dimOld - (radius * 2);
        int lowerLimit = radius;
        int upperLimit = (radius + dim) - 1;
        double[][] intermediate = (double[][]) Array.newInstance(Double.TYPE, new int[]{dimOld, dimOld});
        int x = 0;
        while (x < dimOld) {
            int y;
            for (y = 0; y < dimOld; y++) {
                double val = grid[x][y];
                if (val != 0.0d) {
                    int xUpperLimit = (upperLimit < x + radius ? upperLimit : x + radius) + 1;
                    for (int x2 = lowerLimit > x - radius ? lowerLimit : x - radius; x2 < xUpperLimit; x2++) {
                        double[] dArr = intermediate[x2];
                        dArr[y] = dArr[y] + (kernel[x2 - (x - radius)] * val);
                    }
                }
            }
            x++;
        }
        double[][] outputGrid = (double[][]) Array.newInstance(Double.TYPE, new int[]{dim, dim});
        for (x = lowerLimit; x < upperLimit + 1; x++) {
            y = 0;
            while (y < dimOld) {
                val = intermediate[x][y];
                if (val != 0.0d) {
                    int yUpperLimit = (upperLimit < y + radius ? upperLimit : y + radius) + 1;
                    for (int y2 = lowerLimit > y - radius ? lowerLimit : y - radius; y2 < yUpperLimit; y2++) {
                        dArr = outputGrid[x - radius];
                        int i = y2 - radius;
                        dArr[i] = dArr[i] + (kernel[y2 - (y - radius)] * val);
                    }
                }
                y++;
            }
        }
        return outputGrid;
    }

    static Bitmap colorize(double[][] grid, int[] colorMap, double max) {
        int maxColor = colorMap[colorMap.length - 1];
        double colorMapScaling = ((double) (colorMap.length - 1)) / max;
        int dim = grid.length;
        int[] colors = new int[(dim * dim)];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                double val = grid[j][i];
                int index = (i * dim) + j;
                int col = (int) (val * colorMapScaling);
                if (val == 0.0d) {
                    colors[index] = 0;
                } else if (col < colorMap.length) {
                    colors[index] = colorMap[col];
                } else {
                    colors[index] = maxColor;
                }
            }
        }
        Bitmap tile = Bitmap.createBitmap(dim, dim, Config.ARGB_8888);
        tile.setPixels(colors, 0, dim, 0, 0, dim, dim);
        return tile;
    }

    static double getMaxValue(Collection<WeightedLatLng> points, Bounds bounds, int radius, int screenDim) {
        double minX = bounds.minX;
        double maxX = bounds.maxX;
        double minY = bounds.minY;
        double maxY = bounds.maxY;
        double scale = ((double) ((int) (((double) (screenDim / (radius * 2))) + 0.5d))) / (maxX - minX > maxY - minY ? maxX - minX : maxY - minY);
        LongSparseArray<LongSparseArray<Double>> buckets = new LongSparseArray();
        double max = 0.0d;
        for (WeightedLatLng l : points) {
            int xBucket = (int) ((l.getPoint().x - minX) * scale);
            int yBucket = (int) ((l.getPoint().y - minY) * scale);
            LongSparseArray<Double> column = (LongSparseArray) buckets.get((long) xBucket);
            if (column == null) {
                column = new LongSparseArray();
                buckets.put((long) xBucket, column);
            }
            Double value = (Double) column.get((long) yBucket);
            if (value == null) {
                value = Double.valueOf(0.0d);
            }
            value = Double.valueOf(value.doubleValue() + l.getIntensity());
            column.put((long) yBucket, value);
            if (value.doubleValue() > max) {
                max = value.doubleValue();
            }
        }
        return max;
    }
}
