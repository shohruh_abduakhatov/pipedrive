package com.pipedrive.util.networking;

import com.pipedrive.logging.Log;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public enum CryptoUtils {
    ;
    
    public static final String ERROR_HASH_NOT_CREATED = "ERROR_HASH_NOT_CREATED";
    static final String TAG = null;

    static {
        TAG = CryptoUtils.class.getSimpleName();
    }

    public static String getMD5Hash(String hashFrom) {
        String tag = TAG + ".getMD5Hash()";
        String md5 = CommonUtils.MD5_INSTANCE;
        try {
            MessageDigest digest = MessageDigest.getInstance(CommonUtils.MD5_INSTANCE);
            digest.update(hashFrom.getBytes());
            byte[] messageDigest = digest.digest();
            StringBuilder hexString = new StringBuilder();
            for (byte byteOfMessageDigest : messageDigest) {
                String h = Integer.toHexString(byteOfMessageDigest & 255);
                while (h.length() < 2) {
                    h = "0" + h;
                }
                hexString.append(h);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            Log.e(e);
            return ERROR_HASH_NOT_CREATED;
        }
    }
}
