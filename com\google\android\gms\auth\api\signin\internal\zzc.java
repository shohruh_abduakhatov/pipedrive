package com.google.android.gms.auth.api.signin.internal;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.PendingResults;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzru;
import java.util.HashSet;

public class zzc implements GoogleSignInApi {

    private abstract class zza<R extends Result> extends com.google.android.gms.internal.zzqo.zza<R, zzd> {
        final /* synthetic */ zzc jC;

        public zza(zzc com_google_android_gms_auth_api_signin_internal_zzc, GoogleApiClient googleApiClient) {
            this.jC = com_google_android_gms_auth_api_signin_internal_zzc;
            super(Auth.GOOGLE_SIGN_IN_API, googleApiClient);
        }
    }

    private OptionalPendingResult<GoogleSignInResult> zza(GoogleApiClient googleApiClient, final GoogleSignInOptions googleSignInOptions) {
        Log.d("GoogleSignInApiImpl", "trySilentSignIn");
        return new zzru(googleApiClient.zza(new zza<GoogleSignInResult>(this, googleApiClient) {
            final /* synthetic */ zzc jC;

            protected void zza(zzd com_google_android_gms_auth_api_signin_internal_zzd) throws RemoteException {
                final zzk zzba = zzk.zzba(com_google_android_gms_auth_api_signin_internal_zzd.getContext());
                ((zzh) com_google_android_gms_auth_api_signin_internal_zzd.zzavg()).zza(new zza(this) {
                    final /* synthetic */ AnonymousClass1 jE;

                    public void zza(GoogleSignInAccount googleSignInAccount, Status status) throws RemoteException {
                        if (googleSignInAccount != null) {
                            zzba.zzb(googleSignInAccount, googleSignInOptions);
                        }
                        this.jE.zzc(new GoogleSignInResult(googleSignInAccount, status));
                    }
                }, googleSignInOptions);
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzn(status);
            }

            protected GoogleSignInResult zzn(Status status) {
                return new GoogleSignInResult(null, status);
            }
        }));
    }

    private boolean zza(Account account, Account account2) {
        return account == null ? account2 == null : account.equals(account2);
    }

    private GoogleSignInOptions zzb(GoogleApiClient googleApiClient) {
        return ((zzd) googleApiClient.zza(Auth.hZ)).zzaje();
    }

    public Intent getSignInIntent(GoogleApiClient googleApiClient) {
        zzaa.zzy(googleApiClient);
        return ((zzd) googleApiClient.zza(Auth.hZ)).zzajd();
    }

    public GoogleSignInResult getSignInResultFromIntent(Intent intent) {
        if (intent == null || (!intent.hasExtra("googleSignInStatus") && !intent.hasExtra("googleSignInAccount"))) {
            return null;
        }
        GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount) intent.getParcelableExtra("googleSignInAccount");
        Status status = (Status) intent.getParcelableExtra("googleSignInStatus");
        if (googleSignInAccount != null) {
            status = Status.xZ;
        }
        return new GoogleSignInResult(googleSignInAccount, status);
    }

    public PendingResult<Status> revokeAccess(GoogleApiClient googleApiClient) {
        zzk.zzba(googleApiClient.getContext()).zzajo();
        for (GoogleApiClient zzard : GoogleApiClient.zzarc()) {
            zzard.zzard();
        }
        return googleApiClient.zzb(new zza<Status>(this, googleApiClient) {
            final /* synthetic */ zzc jC;

            protected void zza(zzd com_google_android_gms_auth_api_signin_internal_zzd) throws RemoteException {
                ((zzh) com_google_android_gms_auth_api_signin_internal_zzd.zzavg()).zzc(new zza(this) {
                    final /* synthetic */ AnonymousClass3 jG;

                    {
                        this.jG = r1;
                    }

                    public void zzm(Status status) throws RemoteException {
                        this.jG.zzc((Result) status);
                    }
                }, com_google_android_gms_auth_api_signin_internal_zzd.zzaje());
            }

            protected Status zzb(Status status) {
                return status;
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzb(status);
            }
        });
    }

    public PendingResult<Status> signOut(GoogleApiClient googleApiClient) {
        zzk.zzba(googleApiClient.getContext()).zzajo();
        for (GoogleApiClient zzard : GoogleApiClient.zzarc()) {
            zzard.zzard();
        }
        return googleApiClient.zzb(new zza<Status>(this, googleApiClient) {
            final /* synthetic */ zzc jC;

            protected void zza(zzd com_google_android_gms_auth_api_signin_internal_zzd) throws RemoteException {
                ((zzh) com_google_android_gms_auth_api_signin_internal_zzd.zzavg()).zzb(new zza(this) {
                    final /* synthetic */ AnonymousClass2 jF;

                    {
                        this.jF = r1;
                    }

                    public void zzl(Status status) throws RemoteException {
                        this.jF.zzc((Result) status);
                    }
                }, com_google_android_gms_auth_api_signin_internal_zzd.zzaje());
            }

            protected Status zzb(Status status) {
                return status;
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzb(status);
            }
        });
    }

    public OptionalPendingResult<GoogleSignInResult> silentSignIn(GoogleApiClient googleApiClient) {
        GoogleSignInOptions zzb = zzb(googleApiClient);
        Result zza = zza(googleApiClient.getContext(), zzb);
        return zza != null ? PendingResults.zzb(zza, googleApiClient) : zza(googleApiClient, zzb);
    }

    public GoogleSignInResult zza(Context context, GoogleSignInOptions googleSignInOptions) {
        Log.d("GoogleSignInApiImpl", "getSavedSignInResultIfEligible");
        zzaa.zzy(googleSignInOptions);
        zzk zzba = zzk.zzba(context);
        GoogleSignInOptions zzajn = zzba.zzajn();
        if (zzajn == null || !zza(zzajn.getAccount(), googleSignInOptions.getAccount()) || googleSignInOptions.zzaiv()) {
            return null;
        }
        if ((googleSignInOptions.zzaiu() && (!zzajn.zzaiu() || !googleSignInOptions.zzaix().equals(zzajn.zzaix()))) || !new HashSet(zzajn.zzait()).containsAll(new HashSet(googleSignInOptions.zzait()))) {
            return null;
        }
        GoogleSignInAccount zzajm = zzba.zzajm();
        return (zzajm == null || zzajm.zza()) ? null : new GoogleSignInResult(zzajm, Status.xZ);
    }
}
