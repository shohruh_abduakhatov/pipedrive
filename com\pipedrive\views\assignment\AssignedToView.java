package com.pipedrive.views.assignment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.model.Activity;
import com.pipedrive.model.Deal;
import com.pipedrive.views.assignment.SpinnerWithUserProfilePicture.OnItemSelectedListener;

public class AssignedToView extends SpinnerWithUserProfilePicture {
    public /* bridge */ /* synthetic */ void setOnItemSelectedListener(@Nullable OnItemSelectedListener onItemSelectedListener) {
        super.setOnItemSelectedListener(onItemSelectedListener);
    }

    public AssignedToView(Context context) {
        super(context);
    }

    public AssignedToView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AssignedToView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected int getSpinnerTitleResId() {
        return R.string.assigned_to;
    }

    public void loadAssignments(@NonNull Session session, @NonNull Activity activity) {
        loadAssignments(session, activity.getAssignedUserId());
    }

    public void loadAssignments(@NonNull Session session, @NonNull Deal deal, @Nullable OnItemSelectedListener onItemSelectedListener) {
        loadAssignments(session, (long) deal.getOwnerPipedriveId(), onItemSelectedListener);
    }
}
