package com.google.android.gms.wearable;

import android.net.Uri;
import android.util.Log;
import com.google.android.gms.internal.zzagn;
import com.google.android.gms.internal.zzagn.zza;
import com.google.android.gms.internal.zzasa;

public class PutDataMapRequest {
    private final DataMap aSl = new DataMap();
    private final PutDataRequest aSm;

    private PutDataMapRequest(PutDataRequest putDataRequest, DataMap dataMap) {
        this.aSm = putDataRequest;
        if (dataMap != null) {
            this.aSl.putAll(dataMap);
        }
    }

    public static PutDataMapRequest create(String str) {
        return new PutDataMapRequest(PutDataRequest.create(str), null);
    }

    public static PutDataMapRequest createFromDataMapItem(DataMapItem dataMapItem) {
        return new PutDataMapRequest(PutDataRequest.zzy(dataMapItem.getUri()), dataMapItem.getDataMap());
    }

    public static PutDataMapRequest createWithAutoAppendedId(String str) {
        return new PutDataMapRequest(PutDataRequest.createWithAutoAppendedId(str), null);
    }

    public PutDataRequest asPutDataRequest() {
        zza zza = zzagn.zza(this.aSl);
        this.aSm.setData(zzasa.zzf(zza.aUL));
        int size = zza.aUM.size();
        int i = 0;
        while (i < size) {
            String num = Integer.toString(i);
            Asset asset = (Asset) zza.aUM.get(i);
            String valueOf;
            if (num == null) {
                valueOf = String.valueOf(asset);
                throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf).length() + 26).append("asset key cannot be null: ").append(valueOf).toString());
            } else if (asset == null) {
                String str = "asset cannot be null: key=";
                valueOf = String.valueOf(num);
                throw new IllegalStateException(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            } else {
                if (Log.isLoggable(DataMap.TAG, 3)) {
                    String str2 = DataMap.TAG;
                    String valueOf2 = String.valueOf(asset);
                    Log.d(str2, new StringBuilder((String.valueOf(num).length() + 33) + String.valueOf(valueOf2).length()).append("asPutDataRequest: adding asset: ").append(num).append(" ").append(valueOf2).toString());
                }
                this.aSm.putAsset(num, asset);
                i++;
            }
        }
        return this.aSm;
    }

    public DataMap getDataMap() {
        return this.aSl;
    }

    public Uri getUri() {
        return this.aSm.getUri();
    }

    public boolean isUrgent() {
        return this.aSm.isUrgent();
    }

    public PutDataMapRequest setUrgent() {
        this.aSm.setUrgent();
        return this;
    }
}
