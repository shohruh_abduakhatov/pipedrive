package com.pipedrive.model;

import android.os.Parcelable;
import com.pipedrive.util.clone.Cloner;

public abstract class BaseCloneableDatasourceEntity<T> extends BaseDatasourceEntity implements Parcelable {
    public abstract T getDeepCopy();

    public boolean isEqual(BaseCloneableDatasourceEntity parcelable) {
        return Cloner.isEqual(this, parcelable);
    }
}
