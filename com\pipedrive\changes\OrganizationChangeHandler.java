package com.pipedrive.changes;

import android.support.annotation.NonNull;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.newrelic.agent.android.instrumentation.JSONObjectInstrumentation;
import com.pipedrive.application.Session;
import com.pipedrive.changes.Change.ChangeOperationType;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.Organization;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.ConnectionUtil.JsonReaderInterceptor;
import com.pipedrive.util.networking.Response;
import com.pipedrive.util.organizations.OrganizationsNetworkingUtil;
import java.io.IOException;
import org.json.JSONObject;

class OrganizationChangeHandler extends ChangeHandler {
    OrganizationChangeHandler() {
    }

    @NonNull
    public ChangeHandled handleChange(@NonNull Session session, @NonNull Change organizationChange) {
        if (organizationChange.getChangeOperationType() != ChangeOperationType.OPERATION_CREATE && organizationChange.getChangeOperationType() != ChangeOperationType.OPERATION_UPDATE) {
            LogJourno.reportEvent(EVENT.ChangesHandler_corruptedChangeFound, String.format("Change:[%s]", new Object[]{organizationChange}));
            Log.e(new Throwable(String.format("Unknown change type requested for organization: %s", new Object[]{organizationChange})));
            return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
        } else if (!ConnectionUtil.isConnected(session.getApplicationContext())) {
            return ChangeHandled.NOT_PROCESSED_PLEASE_RETRY;
        } else {
            boolean isUpdateOrgOperation;
            if (organizationChange.getChangeOperationType() == ChangeOperationType.OPERATION_UPDATE) {
                isUpdateOrgOperation = true;
            } else {
                isUpdateOrgOperation = false;
            }
            JsonReaderInterceptor<AnonymousClass1OrgResponse> orgResponseParser = new JsonReaderInterceptor<AnonymousClass1OrgResponse>() {
                public AnonymousClass1OrgResponse interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull AnonymousClass1OrgResponse responseTemplate) throws IOException {
                    if (jsonReader.peek() == JsonToken.NULL) {
                        jsonReader.skipValue();
                    } else {
                        Organization orgFromResponse = OrganizationsNetworkingUtil.readOrg(jsonReader, session.getOrgAddressField(null));
                        if (orgFromResponse == null) {
                            LogJourno.reportEvent(EVENT.ChangesHandler_changeDataResponseToJsonParsingFailed, String.format("OrgResponseTemplate:[%s]", new Object[]{responseTemplate}));
                            Log.e(new Throwable(String.format("Parsing of Org from response failed! Response: %s", new Object[]{responseTemplate})));
                        } else if (responseTemplate.orgUnderChangeSqlId <= 0 || !orgFromResponse.isExisting()) {
                            LogJourno.reportEvent(EVENT.ChangesHandler_cannotUpdatePipedriveIdFromChangeDataResponse, String.format("OrgResponseTemplate:[%s] OrgFromResponse:[%s]", new Object[]{responseTemplate, orgFromResponse}));
                            Log.e(new Throwable(String.format("Cannot update PD ID! SQL ID: %s; PD ID: %s", new Object[]{Long.valueOf(responseTemplate.orgUnderChangeSqlId), Integer.valueOf(orgFromResponse.getPipedriveId())})));
                        } else {
                            new OrganizationsDataSource(session.getDatabase()).updatePipedriveIdBySqlId(responseTemplate.orgUnderChangeSqlId, orgFromResponse.getPipedriveId());
                        }
                    }
                    return responseTemplate;
                }
            };
            try {
                Long organizationSqlId = organizationChange.getMetaData();
                if (organizationSqlId == null) {
                    LogJourno.reportEvent(EVENT.ChangesHandler_changeMetadataNotFound, String.format("Change:[%s]", new Object[]{organizationChange}));
                    Log.e(new Throwable("Change metadata is missing! Cannot find the Organization to change!"));
                    return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
                }
                Organization organizationUnderChange = (Organization) new OrganizationsDataSource(session.getDatabase()).findBySqlId(organizationSqlId.longValue());
                if (organizationUnderChange == null) {
                    LogJourno.reportEvent(EVENT.ChangesHandler_changeDataNotFound, String.format("Change:[%s] SQL id:[%s]", new Object[]{organizationChange, organizationSqlId}));
                    Log.e(new Throwable(String.format("Change metadata is present (%s) but unable to find Organization that was registered for the change!", new Object[]{organizationSqlId})));
                    return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
                }
                AnonymousClass1OrgResponse result;
                ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder(session, "organizations");
                if (isUpdateOrgOperation) {
                    apiUrlBuilder.appendEncodedPath(Integer.toString(organizationUnderChange.getPipedriveId()));
                }
                AnonymousClass1OrgResponse orgResponse = new Response() {
                    long orgUnderChangeSqlId;

                    public String toString() {
                        return "OrgResponse:[orgUnderChangeSqlId=" + this.orgUnderChangeSqlId + "][super.toString:[" + super.toString() + "]]";
                    }
                };
                orgResponse.orgUnderChangeSqlId = organizationSqlId.longValue();
                JSONObject json;
                if (isUpdateOrgOperation) {
                    json = organizationUnderChange.getJSON(session);
                    result = (AnonymousClass1OrgResponse) ConnectionUtil.requestPut(apiUrlBuilder, !(json instanceof JSONObject) ? json.toString() : JSONObjectInstrumentation.toString(json), orgResponseParser, orgResponse);
                } else {
                    json = organizationUnderChange.getJSON(session);
                    result = (AnonymousClass1OrgResponse) ConnectionUtil.requestPost(apiUrlBuilder, !(json instanceof JSONObject) ? json.toString() : JSONObjectInstrumentation.toString(json), orgResponseParser, orgResponse);
                }
                return parseResponse(result, this, organizationChange);
            } catch (Exception e) {
                LogJourno.reportEvent(EVENT.ChangesHandler_changeDataRequestToJsonParsingFailed, String.format("Change:[%s]", new Object[]{organizationChange}), e);
                Log.e(new Throwable("Organization change failed", e));
                return ChangeHandled.CHANGE_FAILED;
            }
        }
    }
}
