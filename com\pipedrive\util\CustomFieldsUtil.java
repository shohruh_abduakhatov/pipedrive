package com.pipedrive.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Pair;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.newrelic.agent.android.instrumentation.JSONObjectInstrumentation;
import com.pipedrive.R;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.CustomFieldsDataSource;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.datetime.DateFormatHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.model.Deal;
import com.pipedrive.model.DealStatus;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.customfields.CustomField;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public enum CustomFieldsUtil {
    ;

    public static void addCustomFieldsToObject(JSONArray customFieldsArr, JSONObject objectToAddTo, String fieldType) {
        if (!TextUtils.isEmpty(fieldType) && customFieldsArr != null && objectToAddTo != null) {
            ArrayList<CustomField> customFields = populateCustomFields(fieldType, customFieldsArr);
            if (customFieldsArr != null && customFieldsArr.length() > 0) {
                Iterator it = customFields.iterator();
                while (it.hasNext()) {
                    CustomField field = (CustomField) it.next();
                    try {
                        objectToAddTo.put(field.getKey(), field.getTempValue() == null ? "" : field.getTempValue());
                        String secondaryCustomFieldKey = CustomField.getSecondaryFieldKey(field.getFieldDataType(), field.getKey());
                        if (!TextUtils.isEmpty(secondaryCustomFieldKey)) {
                            objectToAddTo.put(secondaryCustomFieldKey, field.getSecondaryTempValue() == null ? "" : field.getSecondaryTempValue());
                        }
                    } catch (JSONException e) {
                        Log.e(new Throwable("Error adding custom field object to parent json object", e));
                    }
                }
            }
        }
    }

    @NonNull
    private static ArrayList<CustomField> populateCustomFields(String fieldType, @NonNull JSONArray customFieldsArr) {
        ArrayList<CustomField> finalFields = new ArrayList();
        CustomFieldsDataSource ds = new CustomFieldsDataSource(PipedriveApp.getActiveSession());
        List<CustomField> fields = null;
        if ("deal".equals(fieldType)) {
            fields = ds.getDealFields();
        } else if ("person".equals(fieldType)) {
            fields = ds.getPersonFields();
        } else if ("organization".equals(fieldType)) {
            fields = ds.getOrganizationFields();
        }
        if (fields != null) {
            for (CustomField customField : fields) {
                if (customField.getKey().length() == 40) {
                    for (int i = 0; i < customFieldsArr.length(); i++) {
                        JSONObject fieldObj = customFieldsArr.optJSONObject(i);
                        if (TextUtils.isEmpty(fieldObj.optString(customField.getKey(), null))) {
                            if (CustomField.FIELD_DATA_TYPE_DATE_RANGE.equalsIgnoreCase(customField.getFieldDataType()) && !TextUtils.isEmpty(fieldObj.optString(customField.getKey() + "_until", null))) {
                                customField.setSecondaryTempValue(fieldObj.optString(customField.getKey() + "_until", null));
                            } else if (CustomField.FIELD_DATA_TYPE_TIME_RANGE.equalsIgnoreCase(customField.getFieldDataType()) && !TextUtils.isEmpty(fieldObj.optString(customField.getKey() + "_until", null))) {
                                customField.setSecondaryTempValue(fieldObj.optString(customField.getKey() + "_until", null));
                            } else if (CustomField.FIELD_DATA_TYPE_MONEY.equalsIgnoreCase(customField.getFieldDataType()) && !TextUtils.isEmpty(fieldObj.optString(customField.getKey() + CustomField.FIELD_SECONDARY_KEY_CURRENCY, null))) {
                                customField.setSecondaryTempValue(fieldObj.optString(customField.getKey() + CustomField.FIELD_SECONDARY_KEY_CURRENCY, null));
                            } else if ("address".equalsIgnoreCase(customField.getFieldDataType()) && !TextUtils.isEmpty(fieldObj.optString(customField.getKey() + CustomField.FIELD_SECONDARY_KEY_ADDRESS, null))) {
                                customField.setTempValue(fieldObj.optString(customField.getKey() + CustomField.FIELD_SECONDARY_KEY_ADDRESS, null));
                            }
                        } else if (!CustomField.FIELD_DATA_TYPE_DOUBLE.equalsIgnoreCase(customField.getFieldDataType()) || !"0".equalsIgnoreCase(fieldObj.optString(customField.getKey(), null))) {
                            if (CustomField.FIELD_DATA_TYPE_DATE_RANGE.equalsIgnoreCase(customField.getFieldDataType()) && "0000-00-00".equalsIgnoreCase(fieldObj.optString(customField.getKey(), null))) {
                                customField.setTempValue(null);
                            } else if (!((CustomField.FIELD_DATA_TYPE_MONEY.equalsIgnoreCase(customField.getFieldDataType()) && "0".equalsIgnoreCase(fieldObj.optString(customField.getKey(), null))) || (("person".equalsIgnoreCase(customField.getFieldDataType()) || "organization".equalsIgnoreCase(customField.getFieldDataType()) || CustomField.FIELD_DATA_TYPE_USER.equalsIgnoreCase(customField.getFieldDataType())) && "0".equalsIgnoreCase(fieldObj.optString(customField.getKey(), null))))) {
                                customField.setTempValue(fieldObj.optString(customField.getKey(), null));
                            }
                        }
                    }
                    finalFields.add(customField);
                }
            }
        }
        return finalFields;
    }

    public static ArrayList<CustomField> populateDealFields(Deal deal) {
        ArrayList<CustomField> finalFields = new ArrayList();
        if (deal != null) {
            Context ctx = PipedriveApp.getActiveSession().getApplicationContext();
            CustomField dealField = new CustomField();
            dealField.setName(ctx.getString(R.string.label_deal_field_expected_close_date));
            dealField.setTempValue(deal.getExpectedCloseDate());
            dealField.setFieldDataType("date");
            dealField.setFieldType("deal");
            dealField.setKey(Deal.EXPECTED_CLOSE_DATE);
            finalFields.add(dealField);
            if (!TextUtils.isEmpty(deal.getOwnerName())) {
                dealField = new CustomField();
                dealField.setName(ctx.getString(R.string.label_deal_field_owners_name));
                dealField.setTempValue(deal.getOwnerName());
                dealField.setFieldDataType(CustomField.FIELD_DATA_TYPE_TEXT);
                dealField.setIsEditable(false);
                finalFields.add(dealField);
            }
            if (!TextUtils.isEmpty(deal.getAddTime())) {
                dealField = new CustomField();
                dealField.setName(ctx.getString(R.string.label_deal_field_add_time));
                try {
                    dealField.setTempValue(DateFormatHelper.calculateAndFormatTimeSinceArgumentDate(PipedriveApp.getActiveSession().getApplicationContext(), DateFormatHelper.fullDateFormat2UTC().parse(deal.getAddTime())));
                } catch (ParseException e) {
                    Log.e(new Throwable("Error parsing deal", e));
                }
                if (dealField.getTempValue() != null) {
                    dealField.setFieldDataType(CustomField.FIELD_DATA_TYPE_TEXT);
                    dealField.setIsEditable(false);
                    finalFields.add(dealField);
                }
            }
            if (!TextUtils.isEmpty(deal.getUpdateTime())) {
                dealField = new CustomField();
                dealField.setName(ctx.getString(R.string.label_deal_field_last_updated_time));
                try {
                    dealField.setTempValue(DateFormatHelper.calculateAndFormatTimeSinceArgumentDate(PipedriveApp.getActiveSession().getApplicationContext(), DateFormatHelper.fullDateFormat2UTC().parse(deal.getUpdateTime())));
                } catch (ParseException e2) {
                    Log.e(new Throwable("Failed to parse deals update time.", e2));
                }
                if (dealField.getTempValue() != null) {
                    dealField.setFieldDataType(CustomField.FIELD_DATA_TYPE_TEXT);
                    dealField.setIsEditable(false);
                    finalFields.add(dealField);
                }
            }
            if (!TextUtils.isEmpty(deal.getWonTime()) && DealStatus.WON == deal.getStatus()) {
                dealField = new CustomField();
                dealField.setName(ctx.getString(R.string.label_deal_field_won_time));
                dealField.setTempValue(deal.getWonTime());
                dealField.setFieldDataType("date");
                dealField.setIsEditable(false);
                finalFields.add(dealField);
            }
            if (!TextUtils.isEmpty(deal.getLostTime()) && DealStatus.LOST == deal.getStatus()) {
                dealField = new CustomField();
                dealField.setName(ctx.getString(R.string.label_deal_field_lost_time));
                dealField.setTempValue(deal.getLostTime());
                dealField.setFieldDataType("date");
                finalFields.add(dealField);
            }
            if (!TextUtils.isEmpty(deal.getDropBoxAddress())) {
                dealField = new CustomField();
                dealField.setName(ctx.getString(R.string.label_deal_field_dropbox_address));
                dealField.setTempValue(deal.getDropBoxAddress());
                dealField.setFieldDataType(CustomField.FIELD_DATA_TYPE_TEXT);
                dealField.setIsEditable(false);
                finalFields.add(dealField);
            }
        }
        return finalFields;
    }

    @Nullable
    public static Pair<ArrayList<CustomField>, Integer> populateCustomAndSpecialFields(String fieldType, JSONArray customFieldsArr, Deal deal) {
        ArrayList<CustomField> finalFields = populateDealFields(deal);
        int specialFieldsSize = finalFields.size();
        if (customFieldsArr == null && finalFields.isEmpty()) {
            return null;
        }
        finalFields.addAll(populateCustomFields(fieldType, customFieldsArr));
        return new Pair(finalFields, Integer.valueOf(specialFieldsSize));
    }

    public static int getIdFromCustomField(CustomField field) {
        String idValue = field.getTempValue();
        int pipedriveId = 0;
        if (idValue.startsWith(CustomField.LOCAL_DATABASE_ID_PREFIX)) {
            idValue = idValue.substring(CustomField.LOCAL_DATABASE_ID_PREFIX.length());
        }
        if (TextUtils.isDigitsOnly(idValue)) {
            try {
                pipedriveId = Integer.parseInt(idValue);
            } catch (NumberFormatException e) {
                Log.e(e);
            }
        } else {
            try {
                pipedriveId = JSONObjectInstrumentation.init(idValue).optInt(Param.VALUE, 0);
            } catch (JSONException e2) {
                Log.e(e2);
            }
        }
        return pipedriveId;
    }

    public static JSONArray updateOrgContactReferencesInCustomFields(JSONArray customFieldsArr, Session session, String fieldType) {
        JSONArray updatedCustomFieldsArray = customFieldsArr;
        ArrayList<CustomField> orgContactReferencingFields = new ArrayList();
        Iterator it = populateCustomFields(fieldType, customFieldsArr).iterator();
        while (it.hasNext()) {
            CustomField field = (CustomField) it.next();
            if ("organization".equalsIgnoreCase(field.getFieldDataType()) && !TextUtils.isEmpty(field.getTempValue()) && field.getTempValue().startsWith(CustomField.LOCAL_DATABASE_ID_PREFIX)) {
                Organization org = (Organization) new OrganizationsDataSource(session.getDatabase()).findBySqlId(Long.parseLong(field.getTempValue().substring(CustomField.LOCAL_DATABASE_ID_PREFIX.length())));
                if (org == null || !org.isExisting()) {
                    field.setTempValue("");
                } else {
                    field.setTempValue(org.getPipedriveId() + "");
                }
                orgContactReferencingFields.add(field);
            } else if ("person".equalsIgnoreCase(field.getFieldDataType()) && !TextUtils.isEmpty(field.getTempValue()) && field.getTempValue().startsWith(CustomField.LOCAL_DATABASE_ID_PREFIX)) {
                Person person = (Person) new PersonsDataSource(session.getDatabase()).findBySqlId(Long.parseLong(field.getTempValue().substring(CustomField.LOCAL_DATABASE_ID_PREFIX.length())));
                if (person == null || !person.isExisting()) {
                    field.setTempValue("");
                } else {
                    field.setTempValue(person.getPipedriveId() + "");
                }
                orgContactReferencingFields.add(field);
            }
        }
        it = orgContactReferencingFields.iterator();
        while (it.hasNext()) {
            try {
                updatedCustomFieldsArray = getModifiedCustomFieldsJsonArray(updatedCustomFieldsArray, (CustomField) it.next());
            } catch (JSONException e) {
                Log.e(e);
            }
        }
        return updatedCustomFieldsArray;
    }

    public static JSONArray getModifiedCustomFieldsJsonArray(JSONArray customFieldsArr, CustomField field) throws JSONException {
        int i;
        if (customFieldsArr == null) {
            customFieldsArr = new JSONArray();
        }
        int fieldFoundInIdx = Integer.MIN_VALUE;
        for (i = 0; i < customFieldsArr.length(); i++) {
            if (!TextUtils.isEmpty(customFieldsArr.optJSONObject(i).optString(field.getKey()))) {
                fieldFoundInIdx = i;
                break;
            }
        }
        JSONObject customFieldObj = null;
        if (fieldFoundInIdx >= 0) {
            customFieldObj = customFieldsArr.optJSONObject(fieldFoundInIdx);
        }
        if (customFieldObj == null) {
            customFieldObj = new JSONObject();
        }
        customFieldObj.put(field.getKey(), field.getTempValue());
        String secondaryCustomFieldKey = CustomField.getSecondaryFieldKey(field.getFieldDataType(), field.getKey());
        if (!TextUtils.isEmpty(secondaryCustomFieldKey)) {
            int secondaryFieldFoundInIdx = Integer.MIN_VALUE;
            for (i = 0; i < customFieldsArr.length(); i++) {
                if (!TextUtils.isEmpty(customFieldsArr.optJSONObject(i).optString(secondaryCustomFieldKey))) {
                    secondaryFieldFoundInIdx = i;
                    break;
                }
            }
            JSONObject secondaryField = new JSONObject();
            secondaryField.put(secondaryCustomFieldKey, field.getSecondaryTempValue());
            if (secondaryFieldFoundInIdx >= 0) {
                customFieldsArr.put(secondaryFieldFoundInIdx, secondaryField);
            } else {
                customFieldsArr.put(secondaryField);
            }
        }
        if (fieldFoundInIdx >= 0) {
            customFieldsArr.put(fieldFoundInIdx, customFieldObj);
        } else {
            customFieldsArr.put(customFieldObj);
        }
        return customFieldsArr;
    }
}
