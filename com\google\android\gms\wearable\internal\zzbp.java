package com.google.android.gms.wearable.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zze;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.common.zzc;
import com.google.android.gms.common.zzf;
import com.google.android.gms.internal.zzqo.zzb;
import com.google.android.gms.internal.zzrr;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.CapabilityApi.AddLocalCapabilityResult;
import com.google.android.gms.wearable.CapabilityApi.CapabilityListener;
import com.google.android.gms.wearable.CapabilityApi.GetAllCapabilitiesResult;
import com.google.android.gms.wearable.CapabilityApi.GetCapabilityResult;
import com.google.android.gms.wearable.CapabilityApi.RemoveLocalCapabilityResult;
import com.google.android.gms.wearable.Channel.GetInputStreamResult;
import com.google.android.gms.wearable.Channel.GetOutputStreamResult;
import com.google.android.gms.wearable.ChannelApi.ChannelListener;
import com.google.android.gms.wearable.ChannelApi.OpenChannelResult;
import com.google.android.gms.wearable.DataApi.DataItemResult;
import com.google.android.gms.wearable.DataApi.DataListener;
import com.google.android.gms.wearable.DataApi.DeleteDataItemsResult;
import com.google.android.gms.wearable.DataApi.GetFdForAssetResult;
import com.google.android.gms.wearable.DataItemAsset;
import com.google.android.gms.wearable.DataItemBuffer;
import com.google.android.gms.wearable.MessageApi.MessageListener;
import com.google.android.gms.wearable.MessageApi.SendMessageResult;
import com.google.android.gms.wearable.NodeApi.GetConnectedNodesResult;
import com.google.android.gms.wearable.NodeApi.GetLocalNodeResult;
import com.google.android.gms.wearable.NodeApi.NodeListener;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.WearableStatusCodes;
import com.google.android.gms.wearable.internal.zzax.zza;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.util.networking.entities.FlowItem;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

public class zzbp extends zzj<zzax> {
    private final ExecutorService aGI;
    private final zzay<Object> aUq;
    private final zzay<Object> aUr;
    private final zzay<ChannelListener> aUs;
    private final zzay<DataListener> aUt;
    private final zzay<MessageListener> aUu;
    private final zzay<NodeListener> aUv;
    private final zzay<Object> aUw;
    private final zzay<CapabilityListener> aUx;
    private zzf aUy;

    public zzbp(Context context, Looper looper, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, com.google.android.gms.common.internal.zzf com_google_android_gms_common_internal_zzf) {
        this(context, looper, connectionCallbacks, onConnectionFailedListener, com_google_android_gms_common_internal_zzf, Executors.newCachedThreadPool(), zzf.zzbv(context));
    }

    zzbp(Context context, Looper looper, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, com.google.android.gms.common.internal.zzf com_google_android_gms_common_internal_zzf, ExecutorService executorService, zzf com_google_android_gms_common_zzf) {
        super(context, looper, 14, com_google_android_gms_common_internal_zzf, connectionCallbacks, onConnectionFailedListener);
        this.aUq = new zzay();
        this.aUr = new zzay();
        this.aUs = new zzay();
        this.aUt = new zzay();
        this.aUu = new zzay();
        this.aUv = new zzay();
        this.aUw = new zzay();
        this.aUx = new zzay();
        this.aGI = (ExecutorService) zzaa.zzy(executorService);
        this.aUy = com_google_android_gms_common_zzf;
    }

    private FutureTask<Boolean> zza(final ParcelFileDescriptor parcelFileDescriptor, final byte[] bArr) {
        return new FutureTask(new Callable<Boolean>(this) {
            final /* synthetic */ zzbp aUA;

            public /* synthetic */ Object call() throws Exception {
                return zzwa();
            }

            public java.lang.Boolean zzwa() {
                /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Can't find block by offset: 0x0105 in list []
	at jadx.core.utils.BlockUtils.getBlockByOffset(BlockUtils.java:42)
	at jadx.core.dex.instructions.IfNode.initBlocks(IfNode.java:60)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.initBlocksInIfNodes(BlockFinish.java:48)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.visit(BlockFinish.java:33)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                /*
                r6 = this;
                r1 = 3;
                r0 = "WearableClient";
                r0 = android.util.Log.isLoggable(r0, r1);
                if (r0 == 0) goto L_0x0031;
            L_0x0009:
                r0 = "WearableClient";
                r1 = r3;
                r1 = java.lang.String.valueOf(r1);
                r2 = new java.lang.StringBuilder;
                r3 = java.lang.String.valueOf(r1);
                r3 = r3.length();
                r3 = r3 + 36;
                r2.<init>(r3);
                r3 = "processAssets: writing data to FD : ";
                r2 = r2.append(r3);
                r1 = r2.append(r1);
                r1 = r1.toString();
                android.util.Log.d(r0, r1);
            L_0x0031:
                r1 = new android.os.ParcelFileDescriptor$AutoCloseOutputStream;
                r0 = r3;
                r1.<init>(r0);
                r0 = r4;	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r1.write(r0);	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r1.flush();	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r0 = "WearableClient";	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r2 = 3;	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r0 = android.util.Log.isLoggable(r0, r2);	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                if (r0 == 0) goto L_0x0071;	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
            L_0x0049:
                r0 = "WearableClient";	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r2 = r3;	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r2 = java.lang.String.valueOf(r2);	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r3 = new java.lang.StringBuilder;	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r4 = java.lang.String.valueOf(r2);	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r4 = r4.length();	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r4 = r4 + 27;	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r3.<init>(r4);	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r4 = "processAssets: wrote data: ";	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r3 = r3.append(r4);	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r2 = r3.append(r2);	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r2 = r2.toString();	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                android.util.Log.d(r0, r2);	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
            L_0x0071:
                r0 = 1;	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r0 = java.lang.Boolean.valueOf(r0);	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r2 = "WearableClient";	 Catch:{ IOException -> 0x0148 }
                r3 = 3;	 Catch:{ IOException -> 0x0148 }
                r2 = android.util.Log.isLoggable(r2, r3);	 Catch:{ IOException -> 0x0148 }
                if (r2 == 0) goto L_0x00a7;	 Catch:{ IOException -> 0x0148 }
            L_0x007f:
                r2 = "WearableClient";	 Catch:{ IOException -> 0x0148 }
                r3 = r3;	 Catch:{ IOException -> 0x0148 }
                r3 = java.lang.String.valueOf(r3);	 Catch:{ IOException -> 0x0148 }
                r4 = new java.lang.StringBuilder;	 Catch:{ IOException -> 0x0148 }
                r5 = java.lang.String.valueOf(r3);	 Catch:{ IOException -> 0x0148 }
                r5 = r5.length();	 Catch:{ IOException -> 0x0148 }
                r5 = r5 + 24;	 Catch:{ IOException -> 0x0148 }
                r4.<init>(r5);	 Catch:{ IOException -> 0x0148 }
                r5 = "processAssets: closing: ";	 Catch:{ IOException -> 0x0148 }
                r4 = r4.append(r5);	 Catch:{ IOException -> 0x0148 }
                r3 = r4.append(r3);	 Catch:{ IOException -> 0x0148 }
                r3 = r3.toString();	 Catch:{ IOException -> 0x0148 }
                android.util.Log.d(r2, r3);	 Catch:{ IOException -> 0x0148 }
            L_0x00a7:
                r1.close();	 Catch:{ IOException -> 0x0148 }
            L_0x00aa:
                return r0;
            L_0x00ab:
                r0 = move-exception;
                r0 = "WearableClient";	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r2 = r3;	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r2 = java.lang.String.valueOf(r2);	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r3 = new java.lang.StringBuilder;	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r4 = java.lang.String.valueOf(r2);	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r4 = r4.length();	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r4 = r4 + 36;	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r3.<init>(r4);	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r4 = "processAssets: writing data failed: ";	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r3 = r3.append(r4);	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r2 = r3.append(r2);	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r2 = r2.toString();	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                android.util.Log.w(r0, r2);	 Catch:{ IOException -> 0x00ab, all -> 0x010e }
                r0 = "WearableClient";
                r2 = 3;
                r0 = android.util.Log.isLoggable(r0, r2);
                if (r0 == 0) goto L_0x0105;
            L_0x00dd:
                r0 = "WearableClient";
                r2 = r3;
                r2 = java.lang.String.valueOf(r2);
                r3 = new java.lang.StringBuilder;
                r4 = java.lang.String.valueOf(r2);
                r4 = r4.length();
                r4 = r4 + 24;
                r3.<init>(r4);
                r4 = "processAssets: closing: ";
                r3 = r3.append(r4);
                r2 = r3.append(r2);
                r2 = r2.toString();
                android.util.Log.d(r0, r2);
            L_0x0105:
                r1.close();
            L_0x0108:
                r0 = 0;
                r0 = java.lang.Boolean.valueOf(r0);
                goto L_0x00aa;
            L_0x010e:
                r0 = move-exception;
                r2 = "WearableClient";	 Catch:{ IOException -> 0x0144 }
                r3 = 3;	 Catch:{ IOException -> 0x0144 }
                r2 = android.util.Log.isLoggable(r2, r3);	 Catch:{ IOException -> 0x0144 }
                if (r2 == 0) goto L_0x0140;	 Catch:{ IOException -> 0x0144 }
            L_0x0118:
                r2 = "WearableClient";	 Catch:{ IOException -> 0x0144 }
                r3 = r3;	 Catch:{ IOException -> 0x0144 }
                r3 = java.lang.String.valueOf(r3);	 Catch:{ IOException -> 0x0144 }
                r4 = new java.lang.StringBuilder;	 Catch:{ IOException -> 0x0144 }
                r5 = java.lang.String.valueOf(r3);	 Catch:{ IOException -> 0x0144 }
                r5 = r5.length();	 Catch:{ IOException -> 0x0144 }
                r5 = r5 + 24;	 Catch:{ IOException -> 0x0144 }
                r4.<init>(r5);	 Catch:{ IOException -> 0x0144 }
                r5 = "processAssets: closing: ";	 Catch:{ IOException -> 0x0144 }
                r4 = r4.append(r5);	 Catch:{ IOException -> 0x0144 }
                r3 = r4.append(r3);	 Catch:{ IOException -> 0x0144 }
                r3 = r3.toString();	 Catch:{ IOException -> 0x0144 }
                android.util.Log.d(r2, r3);	 Catch:{ IOException -> 0x0144 }
            L_0x0140:
                r1.close();	 Catch:{ IOException -> 0x0144 }
            L_0x0143:
                throw r0;
            L_0x0144:
                r1 = move-exception;
                goto L_0x0143;
            L_0x0146:
                r0 = move-exception;
                goto L_0x0108;
            L_0x0148:
                r1 = move-exception;
                goto L_0x00aa;
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.wearable.internal.zzbp.1.zzwa():java.lang.Boolean");
            }
        });
    }

    private Runnable zzb(zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, String str, Uri uri, long j, long j2) {
        zzaa.zzy(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status);
        zzaa.zzy(str);
        zzaa.zzy(uri);
        zzaa.zzb(j >= 0, "startOffset is negative: %s", Long.valueOf(j));
        zzaa.zzb(j2 >= -1, "invalid length: %s", Long.valueOf(j2));
        final Uri uri2 = uri;
        final zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status2 = com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status;
        final String str2 = str;
        final long j3 = j;
        final long j4 = j2;
        return new Runnable(this) {
            final /* synthetic */ zzbp aUA;

            /* JADX WARNING: inconsistent code. */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                if (Log.isLoggable("WearableClient", 2)) {
                    Log.v("WearableClient", "Executing sendFileToChannelTask");
                }
                if (FlowItem.ITEM_TYPE_OBJECT_FILE.equals(uri2.getScheme())) {
                    File file = new File(uri2.getPath());
                    try {
                        ParcelFileDescriptor open = ParcelFileDescriptor.open(file, 268435456);
                        try {
                            ((zzax) this.aUA.zzavg()).zza(new zzr(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status2), str2, open, j3, j4);
                            try {
                                open.close();
                                return;
                            } catch (Throwable e) {
                                Log.w("WearableClient", "Failed to close sourceFd", e);
                                return;
                            }
                        } catch (Throwable e2) {
                            Log.w("WearableClient", "Channel.sendFile failed.", e2);
                            com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status2.zzaa(new Status(8));
                            return;
                        } catch (Throwable th) {
                            try {
                                open.close();
                            } catch (Throwable e3) {
                                Log.w("WearableClient", "Failed to close sourceFd", e3);
                            }
                        }
                    } catch (FileNotFoundException e4) {
                        String valueOf = String.valueOf(file);
                        Log.w("WearableClient", new StringBuilder(String.valueOf(valueOf).length() + 46).append("File couldn't be opened for Channel.sendFile: ").append(valueOf).toString());
                        com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status2.zzaa(new Status(13));
                        return;
                    }
                }
                Log.w("WearableClient", "Channel.sendFile used with non-file URI");
                com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status2.zzaa(new Status(10, "Channel.sendFile used with non-file URI"));
            }
        };
    }

    private Runnable zzb(zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, String str, Uri uri, boolean z) {
        zzaa.zzy(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status);
        zzaa.zzy(str);
        zzaa.zzy(uri);
        final Uri uri2 = uri;
        final zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status2 = com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status;
        final boolean z2 = z;
        final String str2 = str;
        return new Runnable(this) {
            final /* synthetic */ zzbp aUA;

            /* JADX WARNING: inconsistent code. */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                if (Log.isLoggable("WearableClient", 2)) {
                    Log.v("WearableClient", "Executing receiveFileFromChannelTask");
                }
                if (FlowItem.ITEM_TYPE_OBJECT_FILE.equals(uri2.getScheme())) {
                    Object file = new File(uri2.getPath());
                    try {
                        ParcelFileDescriptor open = ParcelFileDescriptor.open(file, (z2 ? 33554432 : 0) | 671088640);
                        try {
                            ((zzax) this.aUA.zzavg()).zza(new zzu(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status2), str2, open);
                            try {
                                open.close();
                                return;
                            } catch (Throwable e) {
                                Log.w("WearableClient", "Failed to close targetFd", e);
                                return;
                            }
                        } catch (Throwable e2) {
                            Log.w("WearableClient", "Channel.receiveFile failed.", e2);
                            com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status2.zzaa(new Status(8));
                            return;
                        } catch (Throwable th) {
                            try {
                                open.close();
                            } catch (Throwable e3) {
                                Log.w("WearableClient", "Failed to close targetFd", e3);
                            }
                        }
                    } catch (FileNotFoundException e4) {
                        String valueOf = String.valueOf(file);
                        Log.w("WearableClient", new StringBuilder(String.valueOf(valueOf).length() + 49).append("File couldn't be opened for Channel.receiveFile: ").append(valueOf).toString());
                        com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status2.zzaa(new Status(13));
                        return;
                    }
                }
                Log.w("WearableClient", "Channel.receiveFile used with non-file URI");
                com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status2.zzaa(new Status(10, "Channel.receiveFile used with non-file URI"));
            }
        };
    }

    public static Intent zzel(Context context) {
        Intent intent = new Intent("com.google.android.wearable.app.cn.UPDATE_ANDROID_WEAR").setPackage("com.google.android.wearable.app.cn");
        if (context.getPackageManager().resolveActivity(intent, 65536) != null) {
            return intent;
        }
        return new Intent("android.intent.action.VIEW", Uri.parse("market://details").buildUpon().appendQueryParameter(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, "com.google.android.wearable.app.cn").build());
    }

    protected void zza(int i, IBinder iBinder, Bundle bundle, int i2) {
        if (Log.isLoggable("WearableClient", 2)) {
            Log.d("WearableClient", "onPostInitHandler: statusCode " + i);
        }
        if (i == 0) {
            this.aUq.zzlw(iBinder);
            this.aUr.zzlw(iBinder);
            this.aUs.zzlw(iBinder);
            this.aUt.zzlw(iBinder);
            this.aUu.zzlw(iBinder);
            this.aUv.zzlw(iBinder);
            this.aUw.zzlw(iBinder);
            this.aUx.zzlw(iBinder);
        }
        super.zza(i, iBinder, bundle, i2);
    }

    public void zza(@NonNull zze.zzf com_google_android_gms_common_internal_zze_zzf) {
        int i = 0;
        if (!zzaqx()) {
            try {
                Bundle bundle = getContext().getPackageManager().getApplicationInfo("com.google.android.wearable.app.cn", 128).metaData;
                if (bundle != null) {
                    i = bundle.getInt("com.google.android.wearable.api.version", 0);
                }
                if (i < zzc.GOOGLE_PLAY_SERVICES_VERSION_CODE) {
                    Log.w("WearableClient", "Android Wear out of date. Requires API version " + zzc.GOOGLE_PLAY_SERVICES_VERSION_CODE + " but found " + i);
                    zza(com_google_android_gms_common_internal_zze_zzf, new ConnectionResult(6, PendingIntent.getActivity(getContext(), 0, zzel(getContext()), 0)));
                    return;
                }
            } catch (NameNotFoundException e) {
                zza(com_google_android_gms_common_internal_zze_zzf, new ConnectionResult(16));
                return;
            }
        }
        super.zza(com_google_android_gms_common_internal_zze_zzf);
    }

    public void zza(zzb<DataItemResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataApi_DataItemResult, Uri uri) throws RemoteException {
        ((zzax) zzavg()).zza(new zzk(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataApi_DataItemResult), uri);
    }

    public void zza(zzb<DataItemBuffer> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataItemBuffer, Uri uri, int i) throws RemoteException {
        ((zzax) zzavg()).zza(new zzl(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataItemBuffer), uri, i);
    }

    public void zza(zzb<GetFdForAssetResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataApi_GetFdForAssetResult, Asset asset) throws RemoteException {
        ((zzax) zzavg()).zza(new zzm(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataApi_GetFdForAssetResult), asset);
    }

    public void zza(zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, CapabilityListener capabilityListener) throws RemoteException {
        this.aUx.zza(this, com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, capabilityListener);
    }

    public void zza(zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, CapabilityListener capabilityListener, zzrr<CapabilityListener> com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_CapabilityApi_CapabilityListener, IntentFilter[] intentFilterArr) throws RemoteException {
        this.aUx.zza(this, com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, capabilityListener, zzbq.zze(com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_CapabilityApi_CapabilityListener, intentFilterArr));
    }

    public void zza(zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, ChannelListener channelListener, zzrr<ChannelListener> com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_ChannelApi_ChannelListener, String str, IntentFilter[] intentFilterArr) throws RemoteException {
        if (str == null) {
            this.aUs.zza(this, com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, channelListener, zzbq.zzd(com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_ChannelApi_ChannelListener, intentFilterArr));
            return;
        }
        this.aUs.zza(this, com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, new zzbj(str, channelListener), zzbq.zza(com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_ChannelApi_ChannelListener, str, intentFilterArr));
    }

    public void zza(zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, ChannelListener channelListener, String str) throws RemoteException {
        if (str == null) {
            this.aUs.zza(this, com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, channelListener);
            return;
        }
        this.aUs.zza(this, com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, new zzbj(str, channelListener));
    }

    public void zza(zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, DataListener dataListener) throws RemoteException {
        this.aUt.zza(this, com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, dataListener);
    }

    public void zza(zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, DataListener dataListener, zzrr<DataListener> com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_DataApi_DataListener, IntentFilter[] intentFilterArr) throws RemoteException {
        this.aUt.zza(this, com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, dataListener, zzbq.zza(com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_DataApi_DataListener, intentFilterArr));
    }

    public void zza(zzb<GetFdForAssetResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataApi_GetFdForAssetResult, DataItemAsset dataItemAsset) throws RemoteException {
        zza((zzb) com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataApi_GetFdForAssetResult, Asset.createFromRef(dataItemAsset.getId()));
    }

    public void zza(zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, MessageListener messageListener) throws RemoteException {
        this.aUu.zza(this, com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, messageListener);
    }

    public void zza(zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, MessageListener messageListener, zzrr<MessageListener> com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_MessageApi_MessageListener, IntentFilter[] intentFilterArr) throws RemoteException {
        this.aUu.zza(this, com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, messageListener, zzbq.zzb(com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_MessageApi_MessageListener, intentFilterArr));
    }

    public void zza(zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, NodeListener nodeListener) throws RemoteException {
        this.aUv.zza(this, com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, nodeListener);
    }

    public void zza(zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, NodeListener nodeListener, zzrr<NodeListener> com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_NodeApi_NodeListener, IntentFilter[] intentFilterArr) throws RemoteException {
        this.aUv.zza(this, com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, nodeListener, zzbq.zzc(com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_NodeApi_NodeListener, intentFilterArr));
    }

    public void zza(zzb<DataItemResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataApi_DataItemResult, PutDataRequest putDataRequest) throws RemoteException {
        for (Entry value : putDataRequest.getAssets().entrySet()) {
            Asset asset = (Asset) value.getValue();
            if (asset.getData() == null && asset.getDigest() == null && asset.getFd() == null && asset.getUri() == null) {
                String valueOf = String.valueOf(putDataRequest.getUri());
                String valueOf2 = String.valueOf(asset);
                throw new IllegalArgumentException(new StringBuilder((String.valueOf(valueOf).length() + 33) + String.valueOf(valueOf2).length()).append("Put for ").append(valueOf).append(" contains invalid asset: ").append(valueOf2).toString());
            }
        }
        PutDataRequest zzy = PutDataRequest.zzy(putDataRequest.getUri());
        zzy.setData(putDataRequest.getData());
        if (putDataRequest.isUrgent()) {
            zzy.setUrgent();
        }
        List arrayList = new ArrayList();
        for (Entry value2 : putDataRequest.getAssets().entrySet()) {
            Asset asset2 = (Asset) value2.getValue();
            if (asset2.getData() != null) {
                try {
                    ParcelFileDescriptor[] createPipe = ParcelFileDescriptor.createPipe();
                    if (Log.isLoggable("WearableClient", 3)) {
                        String valueOf3 = String.valueOf(asset2);
                        String valueOf4 = String.valueOf(createPipe[0]);
                        String valueOf5 = String.valueOf(createPipe[1]);
                        Log.d("WearableClient", new StringBuilder(((String.valueOf(valueOf3).length() + 61) + String.valueOf(valueOf4).length()) + String.valueOf(valueOf5).length()).append("processAssets: replacing data with FD in asset: ").append(valueOf3).append(" read:").append(valueOf4).append(" write:").append(valueOf5).toString());
                    }
                    zzy.putAsset((String) value2.getKey(), Asset.createFromFd(createPipe[0]));
                    Runnable zza = zza(createPipe[1], asset2.getData());
                    arrayList.add(zza);
                    this.aGI.submit(zza);
                } catch (Throwable e) {
                    valueOf = String.valueOf(putDataRequest);
                    throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf).length() + 60).append("Unable to create ParcelFileDescriptor for asset in request: ").append(valueOf).toString(), e);
                }
            } else if (asset2.getUri() != null) {
                try {
                    zzy.putAsset((String) value2.getKey(), Asset.createFromFd(getContext().getContentResolver().openFileDescriptor(asset2.getUri(), "r")));
                } catch (FileNotFoundException e2) {
                    new zzq(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataApi_DataItemResult, arrayList).zza(new PutDataResponse(WearableStatusCodes.ASSET_UNAVAILABLE, null));
                    String valueOf6 = String.valueOf(asset2.getUri());
                    Log.w("WearableClient", new StringBuilder(String.valueOf(valueOf6).length() + 28).append("Couldn't resolve asset URI: ").append(valueOf6).toString());
                    return;
                }
            } else {
                zzy.putAsset((String) value2.getKey(), asset2);
            }
        }
        ((zzax) zzavg()).zza(new zzq(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataApi_DataItemResult, arrayList), zzy);
    }

    public void zza(zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, String str, Uri uri, long j, long j2) {
        try {
            this.aGI.execute(zzb(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, str, uri, j, j2));
        } catch (RuntimeException e) {
            com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status.zzaa(new Status(8));
            throw e;
        }
    }

    public void zza(zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, String str, Uri uri, boolean z) {
        try {
            this.aGI.execute(zzb(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, str, uri, z));
        } catch (RuntimeException e) {
            com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status.zzaa(new Status(8));
            throw e;
        }
    }

    public void zza(zzb<SendMessageResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_MessageApi_SendMessageResult, String str, String str2, byte[] bArr) throws RemoteException {
        ((zzax) zzavg()).zza(new zzt(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_MessageApi_SendMessageResult), str, str2, bArr);
    }

    public boolean zzaqx() {
        return !this.aUy.zzb(getContext().getPackageManager(), "com.google.android.wearable.app.cn");
    }

    protected String zzauz() {
        return this.aUy.zzb(getContext().getPackageManager(), "com.google.android.wearable.app.cn") ? "com.google.android.wearable.app.cn" : "com.google.android.gms";
    }

    public void zzb(zzb<DeleteDataItemsResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataApi_DeleteDataItemsResult, Uri uri, int i) throws RemoteException {
        ((zzax) zzavg()).zzb(new zze(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataApi_DeleteDataItemsResult), uri, i);
    }

    public void zzd(zzb<GetAllCapabilitiesResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_CapabilityApi_GetAllCapabilitiesResult, int i) throws RemoteException {
        ((zzax) zzavg()).zza(new zzf(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_CapabilityApi_GetAllCapabilitiesResult), i);
    }

    public void zze(zzb<OpenChannelResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_ChannelApi_OpenChannelResult, String str, String str2) throws RemoteException {
        ((zzax) zzavg()).zza(new zzp(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_ChannelApi_OpenChannelResult), str, str2);
    }

    protected /* synthetic */ IInterface zzh(IBinder iBinder) {
        return zzlx(iBinder);
    }

    public void zzh(zzb<GetCapabilityResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_CapabilityApi_GetCapabilityResult, String str, int i) throws RemoteException {
        ((zzax) zzavg()).zza(new zzg(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_CapabilityApi_GetCapabilityResult), str, i);
    }

    public void zzi(zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, String str, int i) throws RemoteException {
        ((zzax) zzavg()).zzb(new zzd(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status), str, i);
    }

    protected String zzjx() {
        return "com.google.android.gms.wearable.BIND";
    }

    protected String zzjy() {
        return "com.google.android.gms.wearable.internal.IWearableService";
    }

    protected zzax zzlx(IBinder iBinder) {
        return zza.zzlv(iBinder);
    }

    public void zzv(zzb<AddLocalCapabilityResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_CapabilityApi_AddLocalCapabilityResult, String str) throws RemoteException {
        ((zzax) zzavg()).zzd(new zza(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_CapabilityApi_AddLocalCapabilityResult), str);
    }

    public void zzw(zzb<RemoveLocalCapabilityResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_CapabilityApi_RemoveLocalCapabilityResult, String str) throws RemoteException {
        ((zzax) zzavg()).zze(new zzs(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_CapabilityApi_RemoveLocalCapabilityResult), str);
    }

    public void zzx(zzb<DataItemBuffer> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataItemBuffer) throws RemoteException {
        ((zzax) zzavg()).zzb(new zzl(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataItemBuffer));
    }

    public void zzx(zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, String str) throws RemoteException {
        ((zzax) zzavg()).zzf(new zzc(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status), str);
    }

    public void zzy(zzb<GetLocalNodeResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_NodeApi_GetLocalNodeResult) throws RemoteException {
        ((zzax) zzavg()).zzc(new zzn(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_NodeApi_GetLocalNodeResult));
    }

    public void zzy(zzb<GetInputStreamResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_Channel_GetInputStreamResult, String str) throws RemoteException {
        zzau com_google_android_gms_wearable_internal_zzt = new zzt();
        ((zzax) zzavg()).zza(new zzh(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_Channel_GetInputStreamResult, com_google_android_gms_wearable_internal_zzt), com_google_android_gms_wearable_internal_zzt, str);
    }

    public void zzz(zzb<GetConnectedNodesResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_NodeApi_GetConnectedNodesResult) throws RemoteException {
        ((zzax) zzavg()).zzd(new zzj(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_NodeApi_GetConnectedNodesResult));
    }

    public void zzz(zzb<GetOutputStreamResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_Channel_GetOutputStreamResult, String str) throws RemoteException {
        zzau com_google_android_gms_wearable_internal_zzt = new zzt();
        ((zzax) zzavg()).zzb(new zzi(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_Channel_GetOutputStreamResult, com_google_android_gms_wearable_internal_zzt), com_google_android_gms_wearable_internal_zzt, str);
    }
}
