package com.zendesk.sdk.network.impl;

import android.content.Context;
import android.graphics.Bitmap.Config;
import android.net.Uri;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.Builder;
import com.squareup.picasso.UrlConnectionDownloader;
import com.zendesk.logger.Logger;
import com.zendesk.util.StringUtils;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.concurrent.Executors;

public class ZendeskPicassoProvider {
    private static final String HTTPS_SCHEME = "https";
    private static final String HTTP_SCHEME = "http";
    private static final String LOG_TAG = ZendeskPicassoProvider.class.getSimpleName();
    private static Picasso singleton;

    private ZendeskPicassoProvider() {
    }

    public static Picasso getInstance(Context context) {
        if (singleton == null) {
            synchronized (ZendeskPicassoProvider.class) {
                if (singleton == null) {
                    singleton = new Builder(context).executor(Executors.newFixedThreadPool(2)).defaultBitmapConfig(Config.RGB_565).downloader(new UrlConnectionDownloader(context) {
                        protected HttpURLConnection openConnection(Uri uri) throws IOException {
                            String uriScheme = uri.getScheme();
                            if (StringUtils.hasLength(uriScheme) && ZendeskPicassoProvider.HTTP_SCHEME.equals(uriScheme)) {
                                Logger.d(ZendeskPicassoProvider.LOG_TAG, String.format("Loading image: %s - http scheme detected, enforcing https", new Object[]{uri.toString()}), new Object[0]);
                                uri = uri.buildUpon().scheme(ZendeskPicassoProvider.HTTPS_SCHEME).build();
                            }
                            HttpURLConnection connection = super.openConnection(uri);
                            String bearerToken = ZendeskConfig.INSTANCE.storage().identityStorage().getStoredAccessTokenAsBearerToken();
                            if (bearerToken != null) {
                                connection.addRequestProperty("Authorization", bearerToken);
                            }
                            return connection;
                        }
                    }).build();
                }
            }
        }
        return singleton;
    }
}
