package com.google.android.gms.wearable;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzf;
import com.google.android.gms.wearable.internal.zzaf;

public class DataItemBuffer extends zzf<DataItem> implements Result {
    private final Status hv;

    public DataItemBuffer(DataHolder dataHolder) {
        super(dataHolder);
        this.hv = new Status(dataHolder.getStatusCode());
    }

    public Status getStatus() {
        return this.hv;
    }

    protected String zzauq() {
        return "path";
    }

    protected /* synthetic */ Object zzn(int i, int i2) {
        return zzz(i, i2);
    }

    protected DataItem zzz(int i, int i2) {
        return new zzaf(this.zy, i, i2);
    }
}
