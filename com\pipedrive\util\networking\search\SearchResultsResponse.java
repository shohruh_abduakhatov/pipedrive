package com.pipedrive.util.networking.search;

import android.support.annotation.NonNull;
import com.pipedrive.util.networking.Response;
import java.util.ArrayList;
import java.util.List;

public class SearchResultsResponse extends Response {
    @NonNull
    private final List<DealSearchResultEntity> dealSearchResultEntities = new ArrayList();
    @NonNull
    private final List<OrganizationSearchResultEntity> organizationSearchResultEntities = new ArrayList();
    @NonNull
    private final List<PersonSearchResultEntity> personSearchResultEntities = new ArrayList();

    @NonNull
    public List<DealSearchResultEntity> getDealSearchResultEntities() {
        return this.dealSearchResultEntities;
    }

    @NonNull
    public List<OrganizationSearchResultEntity> getOrganizationSearchResultEntities() {
        return this.organizationSearchResultEntities;
    }

    @NonNull
    public List<PersonSearchResultEntity> getPersonSearchResultEntities() {
        return this.personSearchResultEntities;
    }
}
