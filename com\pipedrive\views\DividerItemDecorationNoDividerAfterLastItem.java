package com.pipedrive.views;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

public class DividerItemDecorationNoDividerAfterLastItem extends DividerItemDecoration {
    public DividerItemDecorationNoDividerAfterLastItem(@NonNull Context context) {
        super(context);
    }

    int getChildCountWithDivider(@NonNull RecyclerView parent) {
        return parent.getChildCount() - 1;
    }
}
