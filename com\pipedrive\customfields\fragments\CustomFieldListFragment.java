package com.pipedrive.customfields.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.customfields.views.CustomFieldListView;
import com.pipedrive.customfields.views.CustomFieldListView.OnItemClickListener;
import com.pipedrive.fragments.BaseFragment;

public abstract class CustomFieldListFragment extends BaseFragment {
    @Nullable
    private CustomFieldListView mCustomFieldListView;
    @Nullable
    private OnItemClickListener mOnItemClickListener;

    @NonNull
    protected abstract CustomFieldListView createCustomFieldListView(@NonNull Context context);

    @Nullable
    protected abstract Long getSqlId();

    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.mOnItemClickListener = (OnItemClickListener) context;
        } catch (Exception e) {
        }
    }

    public void refreshContent(@NonNull Session session) {
        if (this.mCustomFieldListView != null) {
            this.mCustomFieldListView.refreshContent(session);
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.mCustomFieldListView = createCustomFieldListView(getContext());
        return this.mCustomFieldListView;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Long sqlId = getSqlId();
        Session activeSession = getActiveSession();
        if (sqlId != null && this.mCustomFieldListView != null && activeSession != null) {
            this.mCustomFieldListView.setup(activeSession, sqlId, this.mOnItemClickListener);
        }
    }

    @Nullable
    protected Session getActiveSession() {
        return PipedriveApp.getActiveSession();
    }
}
