package com.pipedrive.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.pipedrive.activity.CreateActivityTask.OnTaskFinished;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.model.Activity;
import com.pipedrive.model.ActivityType;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.delta.ObjectDelta;
import com.pipedrive.tasks.AsyncTask;
import com.pipedrive.util.activities.ActivityNetworkingUtil;
import java.util.List;
import java.util.concurrent.TimeUnit;

class ActivityPresenterImpl extends ActivityPresenter {
    private static final String STORAGE_KEY_ACTIVITY = (ActivityPresenterImpl.class.getSimpleName() + ".activity");
    private static final String STORAGE_KEY_ORIGINAL_ACTIVITY = (ActivityPresenterImpl.class.getSimpleName() + ".originalActivity");
    private final OnTaskFinished CREATE_TASK_CALLBACK = new OnTaskFinished() {
        public void onActivityCreated(boolean success) {
            if (ActivityPresenterImpl.this.mView != null) {
                ((ActivityView) ActivityPresenterImpl.this.mView).onActivityCreated(success);
            }
        }
    };
    private final OnTaskFinished DELETE_TASK_CALLBACK = new OnTaskFinished() {
        public void onActivityDeleted(boolean success) {
            if (ActivityPresenterImpl.this.mView != null) {
                ((ActivityView) ActivityPresenterImpl.this.mView).onActivityDeleted(success);
            }
        }
    };
    private final ReadActivityTask.OnTaskFinished READ_TASK_CALLBACK = new ReadActivityTask.OnTaskFinished() {
        public void onActivityRead(@Nullable Activity activity) {
            ActivityPresenterImpl.this.cacheAndSendActivityToView(activity);
        }
    };
    private final UpdateActivityTask.OnTaskFinished UPDATE_TASK_CALLBACK = new UpdateActivityTask.OnTaskFinished() {
        public void onActivityUpdated(boolean success) {
            if (ActivityPresenterImpl.this.getView() != null) {
                ((ActivityView) ActivityPresenterImpl.this.getView()).onActivityUpdated(success);
            }
        }
    };
    @Nullable
    private Activity mActivity;
    @NonNull
    private final ObjectDelta<Activity> mActivityObjectDelta = new ObjectDelta(Activity.class);
    @Nullable
    private Activity mOriginalActivity;

    public ActivityPresenterImpl(@NonNull Session session, @Nullable Bundle savedState) {
        super(session, savedState);
    }

    @Nullable
    public Activity getActivity() {
        return this.mActivity;
    }

    public void restoreOrRequestActivity(@Nullable Long sqlId) {
        if (!processRestorationState() && !AsyncTask.isExecuting(getSession(), ReadActivityTask.class)) {
            boolean newPlainActivityRequested;
            if (sqlId == null) {
                newPlainActivityRequested = true;
            } else {
                newPlainActivityRequested = false;
            }
            if (newPlainActivityRequested) {
                Activity newActivity = new Activity(getSession());
                setNewActivityDefaultValues(newActivity);
                cacheAndSendActivityToView(newActivity);
                return;
            }
            new ReadActivityTask(getSession(), this.READ_TASK_CALLBACK).executeOnAsyncTaskExecutor(this, new Long[]{sqlId});
        }
    }

    void restoreOrRequestNewFollowUpActivity(long sqlId) {
        if (!processRestorationState()) {
            Activity newActivity = new Activity(getSession());
            setNewActivityDefaultValues(newActivity);
            Activity sourceActivity = (Activity) new ActivitiesDataSource(getSession()).findBySqlId(sqlId);
            if (sourceActivity != null) {
                newActivity.setDeal(sourceActivity.getDeal());
                newActivity.setPerson(sourceActivity.getPerson());
                newActivity.setOrganization(sourceActivity.getOrganization());
            }
            cacheAndSendActivityToView(newActivity);
        }
    }

    void restoreOrRequestNewActivityForDeal(@NonNull Long dealSqlId) {
        if (!processRestorationState()) {
            Deal deal = (Deal) new DealsDataSource(getSession().getDatabase()).findBySqlId(dealSqlId.longValue());
            if (deal != null) {
                Activity newActivity = new Activity(getSession());
                setNewActivityDefaultValues(newActivity);
                newActivity.setDeal(deal);
                newActivity.setPerson(deal.getPerson());
                newActivity.setOrganization(deal.getOrganization());
                cacheAndSendActivityToView(newActivity);
                return;
            }
            cacheAndSendActivityToView(null);
        }
    }

    void restoreOrRequestNewActivityForOrganization(@NonNull Long organizationSqlId) {
        if (!processRestorationState()) {
            Organization organization = (Organization) new OrganizationsDataSource(getSession().getDatabase()).findBySqlId(organizationSqlId.longValue());
            if (organization != null) {
                Activity newActivity = new Activity(getSession());
                setNewActivityDefaultValues(newActivity);
                newActivity.setOrganization(organization);
                cacheAndSendActivityToView(newActivity);
                return;
            }
            cacheAndSendActivityToView(null);
        }
    }

    void restoreOrRequestNewActivityForPerson(@NonNull Long personSqlId) {
        if (!processRestorationState()) {
            Person person = (Person) new PersonsDataSource(getSession().getDatabase()).findBySqlId(personSqlId.longValue());
            if (person != null) {
                Activity newActivity = new Activity(getSession());
                newActivity.setPerson(person);
                newActivity.setOrganization(person.getCompany());
                setNewActivityDefaultValues(newActivity);
                cacheAndSendActivityToView(newActivity);
                return;
            }
            cacheAndSendActivityToView(null);
        }
    }

    void restoreOrRequestActivityNoteUpdate(@Nullable String note) {
        boolean setNewNoteToActivity;
        boolean updateCacheInOrderNotToTriggerUpdate = true;
        if (this.mActivity != null) {
            setNewNoteToActivity = true;
        } else {
            setNewNoteToActivity = false;
        }
        if (setNewNoteToActivity) {
            this.mActivity.setNote(note);
        }
        if (this.mOriginalActivity == null) {
            updateCacheInOrderNotToTriggerUpdate = false;
        }
        if (updateCacheInOrderNotToTriggerUpdate) {
            this.mOriginalActivity.setNote(note);
        }
        sendActivityToView();
    }

    void restoreOrRequestNewActivityOnDateTime(@NonNull Long dueDateTimeInMillis) {
        if (!processRestorationState()) {
            Activity newActivity = new Activity(getSession());
            setNewActivityDefaultValues(newActivity, PipedriveDateTime.instanceFromUnixTimeRepresentation(Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(dueDateTimeInMillis.longValue()))));
            cacheAndSendActivityToView(newActivity);
        }
    }

    private void setNewActivityDefaultValues(@NonNull Activity activity, @NonNull PipedriveDateTime forDateTime) {
        boolean activityRequiresActivityType = true;
        ActivityType activityType = activity.getType();
        List<ActivityType> activityTypes = ActivityNetworkingUtil.loadActivityTypesList(getSession(), true);
        if (activityTypes.size() <= 1 || activityType == null) {
            activityRequiresActivityType = false;
        }
        if (activityRequiresActivityType) {
            activity.setType((ActivityType) activityTypes.get(0));
        }
        forDateTime.roundUpToHour();
        activity.setActivityStartAt(forDateTime, activity.getDuration());
    }

    private void setNewActivityDefaultValues(@NonNull Activity activity) {
        setNewActivityDefaultValues(activity, PipedriveDateTime.instanceWithCurrentDateTime());
    }

    public void changeActivity(@NonNull Activity activity) {
        if (activity.isStored()) {
            if (!AsyncTask.isExecuting(getSession(), UpdateActivityTask.class)) {
                new UpdateActivityTask(getSession(), this.UPDATE_TASK_CALLBACK).executeOnAsyncTaskExecutor(this, new Activity[]{activity});
            }
        } else if (!AsyncTask.isExecuting(getSession(), CreateActivityTask.class)) {
            new CreateActivityTask(getSession(), this.CREATE_TASK_CALLBACK).executeOnAsyncTaskExecutor(this, new Activity[]{activity});
        }
    }

    public void deleteActivity(@NonNull Long sqlId) {
        if (!AsyncTask.isExecuting(getSession(), DeleteActivityTask.class)) {
            new DeleteActivityTask(getSession(), this.DELETE_TASK_CALLBACK).executeOnAsyncTaskExecutor(this, new Long[]{sqlId});
        }
    }

    void associatePerson(@Nullable Person person) {
        boolean associationSuccessful = associatePersonForCurrentlyManagedActivity(person);
        if (getView() != null && this.mActivity != null && associationSuccessful) {
            ((ActivityView) getView()).onAssociationsUpdated(this.mActivity);
        }
    }

    private boolean associatePersonForCurrentlyManagedActivity(@Nullable Person person) {
        if (this.mActivity == null) {
            return false;
        }
        this.mActivity.setPerson(person);
        if (!(person != null)) {
            return true;
        }
        boolean associateActivityWithPersonsOrganization;
        if (this.mActivity.getOrganization() != null || person.getCompany() == null) {
            associateActivityWithPersonsOrganization = false;
        } else {
            associateActivityWithPersonsOrganization = true;
        }
        if (!associateActivityWithPersonsOrganization) {
            return true;
        }
        this.mActivity.setOrganization(person.getCompany());
        return true;
    }

    void associateOrganization(@Nullable Organization organization) {
        boolean associationSuccessful = associateOrganizationForCurrentlyManagedActivity(organization);
        if (getView() != null && this.mActivity != null && associationSuccessful) {
            ((ActivityView) getView()).onAssociationsUpdated(this.mActivity);
        }
    }

    private boolean associateOrganizationForCurrentlyManagedActivity(@Nullable Organization organization) {
        if (this.mActivity == null) {
            return false;
        }
        this.mActivity.setOrganization(organization);
        return true;
    }

    void associateDeal(@Nullable Deal deal) {
        boolean associationSuccessful = associateDealForCurrentlyManagedActivity(deal);
        if (getView() != null && this.mActivity != null && associationSuccessful) {
            ((ActivityView) getView()).onAssociationsUpdated(this.mActivity);
        }
    }

    boolean isModified() {
        boolean activityObjectDeltaHasChanges;
        boolean noteMaybeModified;
        if (this.mActivity == null || !this.mActivityObjectDelta.hasChanges(this.mActivity, this.mOriginalActivity)) {
            activityObjectDeltaHasChanges = false;
        } else {
            activityObjectDeltaHasChanges = true;
        }
        if (this.mActivity == null || this.mOriginalActivity == null) {
            noteMaybeModified = false;
        } else {
            noteMaybeModified = true;
        }
        if (!noteMaybeModified) {
            return activityObjectDeltaHasChanges;
        }
        boolean noteHaveBeenModified;
        if (TextUtils.equals((this.mActivity.getNote() == null ? "" : this.mActivity.getNote()).trim(), (this.mOriginalActivity.getNote() == null ? "" : this.mOriginalActivity.getNote()).trim())) {
            noteHaveBeenModified = false;
        } else {
            noteHaveBeenModified = true;
        }
        if (activityObjectDeltaHasChanges || noteHaveBeenModified) {
            return true;
        }
        return false;
    }

    private boolean associateDealForCurrentlyManagedActivity(@Nullable Deal deal) {
        if (this.mActivity == null) {
            return false;
        }
        this.mActivity.setDeal(deal);
        if (!(deal != null)) {
            return true;
        }
        boolean associateActivityWithDealsPerson;
        boolean associateActivityWithDealsOrganization;
        if (this.mActivity.getPerson() != null || deal.getPerson() == null) {
            associateActivityWithDealsPerson = false;
        } else {
            associateActivityWithDealsPerson = true;
        }
        if (associateActivityWithDealsPerson) {
            associatePersonForCurrentlyManagedActivity(deal.getPerson());
        }
        if (this.mActivity.getOrganization() != null || deal.getOrganization() == null) {
            associateActivityWithDealsOrganization = false;
        } else {
            associateActivityWithDealsOrganization = true;
        }
        if (!associateActivityWithDealsOrganization) {
            return true;
        }
        associateOrganizationForCurrentlyManagedActivity(deal.getOrganization());
        return true;
    }

    private void cacheAndSendActivityToView(@Nullable Activity activity) {
        this.mActivity = activity;
        this.mOriginalActivity = activity != null ? activity.getDeepCopy() : null;
        sendActivityToView();
    }

    private void sendActivityToView() {
        if (getView() != null) {
            ((ActivityView) getView()).onRequestActivity(this.mActivity);
        }
    }

    private boolean processRestorationState() {
        boolean restoredStateFound;
        if (this.mActivity == null || this.mOriginalActivity == null) {
            restoredStateFound = false;
        } else {
            restoredStateFound = true;
        }
        if (!restoredStateFound) {
            return false;
        }
        boolean callSummaryChangedThisActivityAndWeWillReloadDataFromDB;
        Long sqlIdOfActivityChangedByCallSummary = getSession().getRuntimeCache().callSummaryChangedActivitySqlId;
        if (this.mActivity.isStored() && sqlIdOfActivityChangedByCallSummary != null && sqlIdOfActivityChangedByCallSummary.longValue() == this.mActivity.getSqlId()) {
            callSummaryChangedThisActivityAndWeWillReloadDataFromDB = true;
        } else {
            callSummaryChangedThisActivityAndWeWillReloadDataFromDB = false;
        }
        if (callSummaryChangedThisActivityAndWeWillReloadDataFromDB) {
            getSession().getRuntimeCache().callSummaryChangedActivitySqlId = null;
            return false;
        }
        sendActivityToView();
        return true;
    }

    boolean registerActivityForIdentifyingReopenAfterCallSummaryViews() {
        if (this.mActivity == null) {
            return false;
        }
        getSession().getRuntimeCache().callSummaryChangedActivitySqlId = Long.valueOf(0);
        return true;
    }

    boolean isCachedActivityNotDoneAndTypeCall() {
        if (this.mOriginalActivity == null) {
            return false;
        }
        ActivityType activityType = this.mOriginalActivity.getType();
        if (activityType == null || this.mOriginalActivity.isDone() || !activityType.equals(ActivityType.getActivityTypeByName(getSession(), "call"))) {
            return false;
        }
        return true;
    }

    void associatePersonBySqlId(@NonNull Long personSqlId) {
        associatePerson((Person) new PersonsDataSource(getSession().getDatabase()).findBySqlId(personSqlId.longValue()));
    }

    void associateOrganizationBySqlId(@NonNull Long orgSqlId) {
        associateOrganization((Organization) new OrganizationsDataSource(getSession().getDatabase()).findBySqlId(orgSqlId.longValue()));
    }

    void associateDealBySqlId(@NonNull Long dealSqlId) {
        associateDeal((Deal) new DealsDataSource(getSession().getDatabase()).findBySqlId(dealSqlId.longValue()));
    }

    protected void saveToBundle(@NonNull Bundle bundle) {
        bundle.putParcelable(STORAGE_KEY_ACTIVITY, this.mActivity);
        bundle.putParcelable(STORAGE_KEY_ORIGINAL_ACTIVITY, this.mOriginalActivity);
    }

    protected void restoreFromBundle(@NonNull Bundle bundle) {
        this.mActivity = (Activity) bundle.getParcelable(STORAGE_KEY_ACTIVITY);
        this.mOriginalActivity = (Activity) bundle.getParcelable(STORAGE_KEY_ORIGINAL_ACTIVITY);
    }

    protected String[] getKeysNeededForRestore() {
        return new String[]{STORAGE_KEY_ACTIVITY, STORAGE_KEY_ORIGINAL_ACTIVITY};
    }
}
