package com.pipedrive.views.edit.person.rows;

import android.content.Context;
import android.support.annotation.ArrayRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import butterknife.ButterKnife;
import com.pipedrive.R;
import com.pipedrive.model.contact.CommunicationMedium;
import com.pipedrive.model.contact.Email;
import com.pipedrive.model.contact.Phone;
import com.pipedrive.views.Spinner;
import com.pipedrive.views.Spinner.LabelBuilder;
import com.pipedrive.views.Spinner.OnItemSelectedListener;
import java.util.Arrays;

public abstract class CommunicationMediumEditRow {
    private static final boolean SHOW_ICON_FOR_ALL_ITEMS = false;
    private final View mClear = ButterKnife.findById(this.mRootView, R.id.clear);
    private final ImageView mIcon = ((ImageView) ButterKnife.findById(this.mRootView, R.id.icon));
    private final Spinner<String> mLabel = ((Spinner) ButterKnife.findById(this.mRootView, R.id.label));
    private final View mRootView;
    private final EditText mValue = ((EditText) ButterKnife.findById(this.mRootView, R.id.value));

    public interface OnClearClickListener {
        void onClearClicked(@NonNull CommunicationMedium communicationMedium);
    }

    @StringRes
    abstract int getHintResourceId();

    @DrawableRes
    abstract int getIconResourceId();

    @ArrayRes
    abstract int getLabelsResourceId();

    CommunicationMediumEditRow(@NonNull Context context, @NonNull ViewGroup parent, int inputType) {
        this.mRootView = LayoutInflater.from(context).inflate(R.layout.row_communication_medium_edit_view, parent, false);
        this.mValue.setInputType(inputType);
    }

    @Nullable
    public static View buildRowFor(@NonNull Context context, @NonNull ViewGroup parent, @NonNull CommunicationMedium communicationMedium, boolean firstItem, @Nullable OnClearClickListener onClearClickListener) {
        CommunicationMediumEditRow editRow = null;
        if (communicationMedium instanceof Phone) {
            editRow = new PhoneEditRow(context, parent, 3);
        } else if (communicationMedium instanceof Email) {
            editRow = new EmailEditRow(context, parent, 33);
        }
        if (editRow == null) {
            return null;
        }
        editRow.init(context, communicationMedium, firstItem, onClearClickListener);
        return editRow.getRootView();
    }

    private void init(@NonNull Context context, @NonNull final CommunicationMedium communicationMedium, boolean firstItem, @Nullable final OnClearClickListener onClearClickListener) {
        int i = 0;
        this.mIcon.setImageResource(getIconResourceId());
        this.mValue.setText(communicationMedium.getValue());
        this.mValue.setHint(getHintResourceId());
        this.mValue.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
                communicationMedium.setValue(s.toString());
            }
        });
        this.mLabel.loadAndInit(Arrays.asList(context.getResources().getStringArray(getLabelsResourceId())), new LabelBuilder<String>() {
            public String getLabel(String item) {
                return item;
            }

            public boolean isSelected(String item) {
                return item.equals(communicationMedium.getLabel());
            }
        }, new OnItemSelectedListener<String>() {
            public void onItemSelected(String item) {
                communicationMedium.setLabel(item);
            }
        });
        this.mClear.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                CommunicationMediumEditRow.this.mValue.getText().clear();
                if (onClearClickListener != null) {
                    onClearClickListener.onClearClicked(communicationMedium);
                }
            }
        });
        boolean showIcon = firstItem;
        ImageView imageView = this.mIcon;
        if (!showIcon) {
            i = 4;
        }
        imageView.setVisibility(i);
    }

    private View getRootView() {
        return this.mRootView;
    }
}
