package com.pipedrive.views.agenda;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.datasource.ActivitiesDataSource.OnActivityRetrieved;
import com.pipedrive.datetime.DateFormatHelper;
import com.pipedrive.datetime.LocaleHelper;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.model.Activity;
import com.pipedrive.util.CompatUtil;
import com.pipedrive.util.time.TimeManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

class AgendaDayView extends NestedScrollView {
    @NonNull
    private Map<View, AgendaItem> agendaItemViews;
    @NonNull
    private List<AgendaItemCluster> clusters;
    @NonNull
    private FrameLayout container;
    private Calendar date;
    @NonNull
    private List<AgendaItem> items;
    @Nullable
    private OnActivityClickListener onActivityClickListener;
    private final int oneHourHeight;
    @NonNull
    private Queue<Runnable> pendingActions;
    @Nullable
    private View redCircle;
    @Nullable
    private View redLine;
    private final float scaleFactor;
    private int scrollYCompensation;
    @NonNull
    private final List<View> timeLabels;

    interface OnActivityClickListener {
        void onActivityClicked(@NonNull Activity activity);
    }

    public AgendaDayView(Context context) {
        this(context, null);
    }

    public AgendaDayView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AgendaDayView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.scaleFactor = 1.0f;
        this.items = new ArrayList();
        this.timeLabels = new ArrayList();
        this.clusters = new ArrayList();
        this.agendaItemViews = new HashMap();
        this.oneHourHeight = getResources().getDimensionPixelSize(R.dimen.agendaView.itemHeight);
        this.pendingActions = new LinkedList();
        removeAllViews();
        setupContainer();
    }

    public void setOnClickListener(OnClickListener l) {
        super.setOnClickListener(l);
        this.container.setOnClickListener(l);
    }

    public void shiftDown() {
        setContainerMarginTop(getResources().getDimensionPixelSize(R.dimen.agendaView.containerMarginTopLarge));
    }

    public void shiftUp() {
        setContainerMarginTop(getResources().getDimensionPixelSize(R.dimen.agendaView.containerMarginTopDefault));
    }

    private void setContainerMarginTop(int topMarginInPixels) {
        this.scrollYCompensation = topMarginInPixels;
        this.container.setPadding(0, topMarginInPixels, 0, 0);
    }

    protected void onScrollChanged(int x, int y, int oldX, int oldY) {
        super.onScrollChanged(x, y, oldX, oldY);
        AgendaViewEventBus.getInstance().agendaDayViewScrollYChanged(y - this.scrollYCompensation);
    }

    private void setupContainer() {
        this.container = new FrameLayout(getContext());
        addView(this.container, new LayoutParams(-1, -1));
    }

    private void setupTimeLabels(@NonNull Session session) {
        int hour = 0;
        while (hour < 25) {
            if (hour > 0 && hour < 24) {
                TextView label = new TextView(getContext());
                if (DateFormat.is24HourFormat(getContext())) {
                    label.setText(getResources().getString(R.string.agendaView.timeLabel.timeFormat, new Object[]{Integer.valueOf(hour)}));
                } else {
                    label.setText(LocaleHelper.getLocaleBasedTimeStringInCurrentTimeZoneFromHourOfDay(session, hour));
                }
                CompatUtil.setTextAppearance(label, R.style.TextAppearance.AgendaView.TimeLabel);
                label.measure(MeasureSpec.makeMeasureSpec(0, 0), MeasureSpec.makeMeasureSpec(0, 0));
                LayoutParams labelLayoutParams = new LayoutParams(-2, -2);
                labelLayoutParams.leftMargin = getResources().getDimensionPixelSize(R.dimen.agendaView.timeLabel.marginLeft);
                labelLayoutParams.topMargin = (int) (((((float) getResources().getDimensionPixelSize(R.dimen.agendaView.timeLabel.marginTop)) * 1.0f) * ((float) hour)) - ((float) (label.getMeasuredHeight() / 2)));
                this.timeLabels.add(label);
                this.container.addView(label, labelLayoutParams);
            }
            View divider = new View(getContext());
            divider.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.agendaView.timeLabel.dividerColor));
            LayoutParams separatorLayoutParams = new LayoutParams(-1, getResources().getDimensionPixelSize(R.dimen.divider_height));
            separatorLayoutParams.leftMargin = getResources().getDimensionPixelSize(R.dimen.agendaView.divider.marginLeft);
            separatorLayoutParams.rightMargin = getResources().getDimensionPixelSize(R.dimen.agendaView.divider.marginRight);
            separatorLayoutParams.topMargin = (int) ((((float) getResources().getDimensionPixelSize(R.dimen.agendaView.divider.marginTop)) * 1.0f) * ((float) hour));
            this.container.addView(divider, separatorLayoutParams);
            hour++;
        }
    }

    public void setup(@NonNull Session session, @NonNull Calendar date, @Nullable OnActivityClickListener onActivityClickListener) {
        this.date = date;
        this.onActivityClickListener = onActivityClickListener;
        refreshContent(session);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        processPendingActions();
    }

    public void scrollToCurrentTime(final boolean animate) {
        this.pendingActions.add(new Runnable() {
            public void run() {
                int yScroll = Math.max(0, AgendaDayView.this.getCurrentTimeYOffset() - AgendaDayView.this.oneHourHeight);
                if (animate) {
                    AgendaDayView.this.smoothScrollTo(0, yScroll);
                } else {
                    AgendaDayView.this.setScrollY(yScroll);
                }
            }
        });
        processPendingActions();
    }

    public void scrollToY(final int scrollY) {
        this.pendingActions.add(new Runnable() {
            public void run() {
                AgendaDayView.this.setScrollY(scrollY + AgendaDayView.this.scrollYCompensation);
            }
        });
        processPendingActions();
    }

    private void processPendingActions() {
        if (getHandler() != null) {
            while (!this.pendingActions.isEmpty()) {
                postOnAnimation((Runnable) this.pendingActions.remove());
            }
        }
    }

    private int getCurrentTimeYOffset() {
        Calendar localTime = TimeManager.getInstance().getLocalCalendar();
        return (int) ((1.0f * ((float) this.oneHourHeight)) * (((float) localTime.get(11)) + (((float) localTime.get(12)) / BitmapDescriptorFactory.HUE_YELLOW)));
    }

    public void refreshContent(@NonNull Session session) {
        removeExistingItems();
        setupTimeLabels(session);
        new ActivitiesDataSource(session).getNonAllDayActiveActivitiesForDateForAuthenticatedUserOnly(this.date, new OnActivityRetrieved() {
            public void activity(@NonNull Activity activity) {
                AgendaItem agendaItem = AgendaItem.wrap(activity);
                if (agendaItem != null) {
                    AgendaDayView.this.items.add(agendaItem);
                }
            }
        });
        Collections.sort(this.items, new Comparator<AgendaItem>() {
            public int compare(AgendaItem lhs, AgendaItem rhs) {
                int startTimeEqual = lhs.getStartTimestamp().compareTo(rhs.getStartTimestamp());
                return startTimeEqual == 0 ? (int) (rhs.getStartTimestamp().durationTo(rhs.getEndTimestamp()).toMillis() - lhs.getStartTimestamp().durationTo(lhs.getEndTimestamp()).toMillis()) : startTimeEqual;
            }
        });
        if (DateFormatHelper.isTodayUtc(this.date.getTimeInMillis())) {
            drawRedLine();
        }
        setupItems(this.onActivityClickListener);
    }

    private void removeExistingItems() {
        for (View view : this.agendaItemViews.keySet()) {
            this.container.removeView(view);
        }
        for (View view2 : this.timeLabels) {
            this.container.removeView(view2);
        }
        this.timeLabels.clear();
        this.items.clear();
        this.clusters.clear();
        this.agendaItemViews.clear();
        this.container.removeView(this.redLine);
        this.container.removeView(this.redCircle);
    }

    private void drawRedLine() {
        int circleRadius = getResources().getDimensionPixelSize(R.dimen.agendaView.redCircle.radius);
        this.redLine = new View(getContext());
        this.redLine.setBackground(ContextCompat.getDrawable(getContext(), R.color.red));
        LayoutParams lineParams = new LayoutParams(-1, getResources().getDimensionPixelSize(R.dimen.agendaView.redLine.height));
        lineParams.rightMargin = getResources().getDimensionPixelSize(R.dimen.agendaView.redLine.marginRight);
        lineParams.leftMargin = getResources().getDimensionPixelSize(R.dimen.agendaView.redLinePaddingLeft);
        lineParams.topMargin = getCurrentTimeYOffset();
        this.container.addView(this.redLine, lineParams);
        this.redCircle = new View(getContext());
        this.redCircle.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.shape_red_filled_circle));
        LayoutParams circleParams = new LayoutParams(circleRadius * 2, circleRadius * 2);
        circleParams.leftMargin = getResources().getDimensionPixelSize(R.dimen.agendaView.redCirclePaddingLeft);
        circleParams.topMargin = lineParams.topMargin - circleRadius;
        this.container.addView(this.redCircle, circleParams);
    }

    private void setupItems(@Nullable OnActivityClickListener onActivityClickListener) {
        buildClusters();
        calculateColumns();
        expandSpans();
        placeItems(onActivityClickListener);
    }

    private void buildClusters() {
        PipedriveDateTime clusterStart = null;
        PipedriveDateTime clusterEnd = null;
        AgendaItemCluster currentCluster = null;
        for (AgendaItem item : this.items) {
            if (clusterStart == null || !item.startsWithin(clusterStart, clusterEnd)) {
                currentCluster = new AgendaItemCluster();
                this.clusters.add(currentCluster);
            }
            currentCluster.addItem(item);
            if (clusterStart == null || item.getStartTimestamp().compareTo(clusterStart) < 0) {
                clusterStart = item.getStartTimestamp();
            }
            if (clusterEnd == null || item.getEndTimestamp().compareTo(clusterEnd) > 0) {
                clusterEnd = item.getEndTimestamp();
            }
        }
    }

    private void calculateColumns() {
        for (AgendaItemCluster cluster : this.clusters) {
            for (int i = 0; i < cluster.getItemCount(); i++) {
                AgendaItem item = cluster.getItem(i);
                int suggestedColumn = 0;
                boolean fit = false;
                while (!fit) {
                    fit = true;
                    int j = 0;
                    while (j < i) {
                        if (item.startsWithin(cluster.getItem(j)) && cluster.getItem(j).getColumn() == suggestedColumn) {
                            suggestedColumn++;
                            fit = false;
                        }
                        j++;
                    }
                }
                cluster.setItemColumn(item, suggestedColumn);
            }
        }
    }

    private void expandSpans() {
        for (AgendaItemCluster cluster : this.clusters) {
            List<AgendaItem> clusterItems = cluster.getItems();
            for (AgendaItem item : clusterItems) {
                int boundaryColumn = cluster.getColumnCount() + 1;
                for (AgendaItem otherItem : clusterItems) {
                    if (otherItem != item && ((otherItem.startsWithin(item) || item.startsWithin(otherItem)) && otherItem.getColumn() > item.getColumn() && otherItem.getColumn() < boundaryColumn)) {
                        boundaryColumn = otherItem.getColumn();
                    }
                }
                item.setColumnSpan(boundaryColumn - item.getColumn());
            }
        }
    }

    private void placeItems(@Nullable OnActivityClickListener onActivityClickListener) {
        int displayWidthPixelSize = getContext().getResources().getDisplayMetrics().widthPixels;
        int marginRight = getResources().getDimensionPixelSize(R.dimen.agendaView.itemMarginRight);
        int marginBottom = getResources().getDimensionPixelSize(R.dimen.agendaView.itemMarginBottom);
        int leftPadding = getResources().getDimensionPixelSize(R.dimen.agendaView.contentPaddingLeft);
        int maxItemWidth = (displayWidthPixelSize - leftPadding) - (getResources().getDimensionPixelSize(R.dimen.agendaView.padding) - marginRight);
        for (AgendaItemCluster cluster : this.clusters) {
            int columnWidth = maxItemWidth / (cluster.getColumnCount() + 1);
            for (AgendaItem item : cluster.getItems()) {
                float durationHours = getDurationInHoursForItem(item);
                int x = leftPadding + (item.getColumn() * columnWidth);
                int y = getYForItem(item);
                int heightPixels = ((int) ((1.0f * ((float) getResources().getDimensionPixelSize(R.dimen.agendaView.itemHeight))) * Math.max((1.0f * ((float) Activity.DEFAULT_ACTIVITY_DURATION_SECONDS)) / ((float) TimeUnit.HOURS.toSeconds(1)), durationHours))) - marginBottom;
                int widthPixels = (item.getColumnSpan() * columnWidth) - marginRight;
                View agendaItemView = getViewForAgendaItem(item, heightPixels, onActivityClickListener);
                this.agendaItemViews.put(agendaItemView, item);
                agendaItemView.setTranslationX((float) x);
                agendaItemView.setTranslationY((float) y);
                this.container.addView(agendaItemView, widthPixels, heightPixels);
            }
        }
    }

    private float getDurationInHoursForItem(@NonNull AgendaItem item) {
        Calendar startOfCurrentDayLocal = PipedriveDateTime.instanceFromDate(this.date.getTime()).resetTimeLocal().toLocalCalendar();
        Calendar endOfCurrentDayLocal = (Calendar) startOfCurrentDayLocal.clone();
        endOfCurrentDayLocal.add(6, 1);
        Calendar startTimestampLocal = item.getStartTimestamp().toLocalCalendar();
        return (1.0f * ((float) (Math.min(endOfCurrentDayLocal.getTimeInMillis(), item.getEndTimestamp().toLocalCalendar().getTimeInMillis()) - Math.max(startOfCurrentDayLocal.getTimeInMillis(), startTimestampLocal.getTimeInMillis())))) / ((float) TimeUnit.HOURS.toMillis(1));
    }

    private int getYForItem(@NonNull AgendaItem item) {
        Calendar startTimestampToUseForCalculation;
        Calendar startTimestampLocal = item.getStartTimestamp().toLocalCalendar();
        Calendar startOfDayLocal = PipedriveDateTime.instanceFromDate(this.date.getTime()).resetTimeLocal().toLocalCalendar();
        if (startTimestampLocal.before(startOfDayLocal)) {
            startTimestampToUseForCalculation = startOfDayLocal;
        } else {
            startTimestampToUseForCalculation = startTimestampLocal;
        }
        return (int) (1.0f * (((float) (getResources().getDimensionPixelSize(R.dimen.agendaView.itemHeight) * startTimestampToUseForCalculation.get(11))) + (((float) (getResources().getDimensionPixelSize(R.dimen.agendaView.itemHeight) * startTimestampToUseForCalculation.get(12))) / BitmapDescriptorFactory.HUE_YELLOW)));
    }

    @NonNull
    private View getViewForAgendaItem(final AgendaItem currentItem, int heightPixels, @Nullable final OnActivityClickListener onActivityClickListener) {
        boolean isDone = currentItem.isDone();
        View itemView = LayoutInflater.from(getContext()).inflate(isDone ? R.layout.row_agenda_day_view_done : R.layout.row_agenda_day_view, this.container, false);
        TextView titleView = (TextView) ButterKnife.findById(itemView, R.id.title);
        titleView.setText(currentItem.getTitle());
        if (isDone) {
            titleView.setPaintFlags(titleView.getPaintFlags() | 16);
        }
        if (heightPixels < getResources().getDimensionPixelSize(R.dimen.agendaView.itemHeightTooSmallForPadding)) {
            titleView.setPadding(0, 0, 0, 0);
        }
        setMaxTextLines(heightPixels, titleView);
        itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (onActivityClickListener != null) {
                    onActivityClickListener.onActivityClicked(currentItem.getActivity());
                }
            }
        });
        return itemView;
    }

    private void setMaxTextLines(int viewHeight, TextView titleView) {
        int maxViewHeightForSingleLineOfText = getResources().getDimensionPixelSize(R.dimen.agendaView.maxItemHeightForSingleLine);
        int incrementHeightValueToFitNextLine = getResources().getDimensionPixelSize(R.dimen.agendaView.incrementValueToAddOneMoreTextLine);
        int maxHeightForCurrentLinesCount = maxViewHeightForSingleLineOfText;
        int linesCount = 1;
        while (linesCount <= 10) {
            if (viewHeight <= maxHeightForCurrentLinesCount) {
                titleView.setMaxLines(linesCount);
                return;
            } else {
                maxHeightForCurrentLinesCount += incrementHeightValueToFitNextLine;
                linesCount++;
            }
        }
    }
}
