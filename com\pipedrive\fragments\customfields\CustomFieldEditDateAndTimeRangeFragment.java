package com.pipedrive.fragments.customfields;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.pipedrive.R;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.datetime.DateFormatHelper;
import com.pipedrive.datetime.LocaleHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.util.time.TimeManager;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class CustomFieldEditDateAndTimeRangeFragment extends CustomFieldBaseFragment implements DateTimePickerInterface {
    private static final int VALUE_DATE = -2147483647;
    private static final int VALUE_IS_START_FIELD = Integer.MIN_VALUE;
    private LinearLayout mLayoutTimeDateRange;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_custom_field_edit_time_date_range, container, false);
        this.mLayoutTimeDateRange = (LinearLayout) mainView.findViewById(R.id.layoutTimeDateRange);
        showTimeOrDateFields(inflater);
        return mainView;
    }

    private void showTimeOrDateFields(LayoutInflater inflater) {
        createTimeOrDateRow(inflater, true, this.mLayoutTimeDateRange);
        String fieldDataType = this.mCustomField.getFieldDataType();
        if (CustomField.FIELD_DATA_TYPE_TIME_RANGE.equalsIgnoreCase(fieldDataType) || CustomField.FIELD_DATA_TYPE_DATE_RANGE.equalsIgnoreCase(fieldDataType)) {
            createTimeOrDateRow(inflater, false, this.mLayoutTimeDateRange);
        }
    }

    private void setTimeValue(TextView textView, boolean isStartField) {
        Calendar cal;
        if (isStartField && TextUtils.isEmpty(this.mCustomField.getTempValue())) {
            cal = TimeManager.getInstance().getLocalCalendar();
            cal.add(11, 1);
            cal.set(12, 0);
            cal.set(13, 0);
            this.mCustomField.setTempValue(DateFormatHelper.timeFormat2().format(cal.getTime()));
        } else if (!isStartField && TextUtils.isEmpty(this.mCustomField.getSecondaryTempValue())) {
            cal = TimeManager.getInstance().getLocalCalendar();
            cal.add(11, 2);
            cal.set(12, 0);
            cal.set(13, 0);
            this.mCustomField.setSecondaryTempValue(DateFormatHelper.timeFormat2().format(cal.getTime()));
        }
        textView.setText(getTimeValue(isStartField ? this.mCustomField.getTempValue() : this.mCustomField.getSecondaryTempValue()));
    }

    private void setDateValue(TextView textView, boolean isStartField) {
        if (isStartField && TextUtils.isEmpty(this.mCustomField.getTempValue())) {
            this.mCustomField.setTempValue(DateFormatHelper.dateFormat2().format(new Date(TimeManager.getInstance().currentTimeMillis().longValue())));
        } else if (!isStartField && TextUtils.isEmpty(this.mCustomField.getSecondaryTempValue())) {
            Calendar cal = TimeManager.getInstance().getLocalCalendar();
            cal.add(5, 1);
            this.mCustomField.setSecondaryTempValue(DateFormatHelper.dateFormat2().format(cal.getTime()));
        }
        textView.setText(getDateValue(isStartField ? this.mCustomField.getTempValue() : this.mCustomField.getSecondaryTempValue()));
    }

    private void createTimeOrDateRow(LayoutInflater inflater, boolean isStartField, LinearLayout layoutTimeDateRange) {
        View timeDateRow = inflater.inflate(R.layout.row_custom_field_edit_time_range, this.mLayoutTimeDateRange, false);
        TextView textViewHeader = (TextView) timeDateRow.findViewById(R.id.textViewHeader);
        TextView textViewValue = (TextView) timeDateRow.findViewById(R.id.textViewValue);
        final String fieldDataType = this.mCustomField.getFieldDataType();
        if ("time".equalsIgnoreCase(fieldDataType)) {
            textViewHeader.setText(R.string.time);
            setTimeValue(textViewValue, isStartField);
        } else if (CustomField.FIELD_DATA_TYPE_TIME_RANGE.equalsIgnoreCase(fieldDataType)) {
            textViewHeader.setText(isStartField ? R.string.start_time : R.string.end_time);
            setTimeValue(textViewValue, isStartField);
        }
        if ("date".equalsIgnoreCase(fieldDataType)) {
            textViewHeader.setText(R.string.lbl_date);
            setDateValue(textViewValue, isStartField);
        } else if (CustomField.FIELD_DATA_TYPE_DATE_RANGE.equalsIgnoreCase(fieldDataType)) {
            textViewHeader.setText(isStartField ? R.string.start_date : R.string.end_date);
            setDateValue(textViewValue, isStartField);
        }
        timeDateRow.setTag(VALUE_DATE, isStartField ? this.mCustomField.getTempValue() : this.mCustomField.getSecondaryTempValue());
        timeDateRow.setTag(Integer.MIN_VALUE, Boolean.valueOf(isStartField));
        timeDateRow.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (CustomField.FIELD_DATA_TYPE_DATE_RANGE.equalsIgnoreCase(fieldDataType) || "date".equalsIgnoreCase(fieldDataType)) {
                    DatePickerFragment datePickerFragment = new DatePickerFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("date", (String) v.getTag(CustomFieldEditDateAndTimeRangeFragment.VALUE_DATE));
                    bundle.putBoolean("isStartField", ((Boolean) v.getTag(Integer.MIN_VALUE)).booleanValue());
                    datePickerFragment.setDateTimePickerCallback(CustomFieldEditDateAndTimeRangeFragment.this);
                    datePickerFragment.setArguments(bundle);
                    datePickerFragment.show(CustomFieldEditDateAndTimeRangeFragment.this.getFragmentManager(), "datePicker");
                    return;
                }
                TimePickerFragment timePickerFragment = new TimePickerFragment();
                bundle = new Bundle();
                bundle.putString("time", (String) v.getTag(CustomFieldEditDateAndTimeRangeFragment.VALUE_DATE));
                bundle.putBoolean("isStartField", ((Boolean) v.getTag(Integer.MIN_VALUE)).booleanValue());
                timePickerFragment.setArguments(bundle);
                timePickerFragment.setDateTimePickerCallback(CustomFieldEditDateAndTimeRangeFragment.this);
                timePickerFragment.show(CustomFieldEditDateAndTimeRangeFragment.this.getFragmentManager(), "timePicker");
            }
        });
        layoutTimeDateRange.addView(timeDateRow);
    }

    private String getTimeValue(String value) {
        Date date = null;
        if (!TextUtils.isEmpty(value)) {
            try {
                date = DateFormatHelper.timeFormat2().parse(value);
            } catch (ParseException e) {
                Log.e(e);
            }
        }
        String formattedDateString = "";
        if (date != null) {
            return LocaleHelper.getLocaleBasedTimeString(PipedriveApp.getActiveSession(), date);
        }
        return formattedDateString;
    }

    private String getDateValue(String value) {
        Date date = null;
        if (!TextUtils.isEmpty(value)) {
            try {
                date = DateFormatHelper.dateFormat2().parse(value);
            } catch (ParseException e) {
                Log.e(e);
            }
        }
        String formattedDateString = "";
        if (date != null) {
            return LocaleHelper.getLocaleBasedDateString(PipedriveApp.getActiveSession(), date);
        }
        return formattedDateString;
    }

    protected void saveContent() {
    }

    protected void clearContent() {
        this.mCustomField.setTempValue(null);
        this.mCustomField.setSecondaryTempValue(null);
    }

    public void dateChanged(Date date, boolean isStartField) {
        String fieldDataType = this.mCustomField.getFieldDataType();
        String changedValueString = null;
        if (date != null) {
            if ("time".equalsIgnoreCase(fieldDataType) || CustomField.FIELD_DATA_TYPE_TIME_RANGE.equalsIgnoreCase(fieldDataType)) {
                changedValueString = DateFormatHelper.timeFormat2().format(date);
            } else if ("date".equalsIgnoreCase(fieldDataType) || CustomField.FIELD_DATA_TYPE_DATE_RANGE.equalsIgnoreCase(fieldDataType)) {
                changedValueString = DateFormatHelper.dateFormat2().format(date);
            }
        }
        if (changedValueString != null) {
            if (isStartField) {
                Date end = null;
                String endValue = this.mCustomField.getSecondaryTempValue();
                if (!TextUtils.isEmpty(endValue)) {
                    end = getDateFromStringValueAccordingToFieldType(endValue);
                }
                if (end != null && date.after(end) && CustomField.FIELD_DATA_TYPE_DATE_RANGE.equalsIgnoreCase(fieldDataType)) {
                    this.mCustomField.setSecondaryTempValue(changedValueString);
                }
                this.mCustomField.setTempValue(changedValueString);
            } else {
                Date start = null;
                String startValue = this.mCustomField.getTempValue();
                if (!TextUtils.isEmpty(startValue)) {
                    start = getDateFromStringValueAccordingToFieldType(startValue);
                }
                if (start == null || (start.after(date) && CustomField.FIELD_DATA_TYPE_DATE_RANGE.equalsIgnoreCase(fieldDataType))) {
                    this.mCustomField.setTempValue(changedValueString);
                }
                this.mCustomField.setSecondaryTempValue(changedValueString);
            }
        }
        reloadDateTimeRows();
    }

    private Date getDateFromStringValueAccordingToFieldType(String dateValue) {
        Date date = null;
        String fieldDataType = this.mCustomField.getFieldDataType();
        if ("time".equalsIgnoreCase(fieldDataType) || CustomField.FIELD_DATA_TYPE_TIME_RANGE.equalsIgnoreCase(fieldDataType)) {
            try {
                date = DateFormatHelper.timeFormat2().parse(dateValue);
                Calendar timeCal = TimeManager.getInstance().getLocalCalendar();
                timeCal.setTime(date);
                Calendar calendar = TimeManager.getInstance().getLocalCalendar();
                calendar.set(11, timeCal.get(11));
                calendar.set(12, timeCal.get(12));
                date = calendar.getTime();
            } catch (ParseException e) {
                Log.e(e);
            }
        } else if ("date".equalsIgnoreCase(fieldDataType) || CustomField.FIELD_DATA_TYPE_DATE_RANGE.equalsIgnoreCase(fieldDataType)) {
            try {
                date = DateFormatHelper.dateFormat2().parse(dateValue);
            } catch (ParseException e2) {
                Log.e(e2);
            }
        }
        return date;
    }

    private void reloadDateTimeRows() {
        if (getActivity() != null) {
            this.mLayoutTimeDateRange.removeAllViewsInLayout();
            showTimeOrDateFields(LayoutInflater.from(getActivity()));
        }
    }
}
