package com.pipedrive.products;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.products.DealProductsDataSource;
import com.pipedrive.datasource.products.ProductVariationDataSource;
import com.pipedrive.gson.GsonHelper;
import com.pipedrive.model.Currency;
import com.pipedrive.model.Deal;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.model.products.Price;
import com.pipedrive.model.products.Product;
import com.pipedrive.model.products.ProductVariation;
import com.pipedrive.tasks.StoreAwareAsyncTask;
import com.pipedrive.util.CurrenciesNetworkingUtil;
import com.pipedrive.util.networking.ApiUrlBuilder.UrlTemplates;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.ConnectionUtil$JsonReaderInterceptor;
import com.pipedrive.util.networking.Response;
import com.pipedrive.util.networking.entities.product.DealProductEntity;
import com.pipedrive.util.products.ProductsNetworkingUtil;
import java.io.IOException;

class DownloadDealProductListTask extends StoreAwareAsyncTask<Void, Void, Void> {
    @NonNull
    private final Long dealSqlId;
    @Nullable
    private Throwable exception;
    @Nullable
    private final OnCompletedListener onCompletedListener;
    @Nullable
    private final OnErrorListener onErrorListener;
    private boolean success;

    @MainThread
    interface OnErrorListener {
        void onDealProductsDownloadFailedWithError(@Nullable Throwable th);
    }

    @MainThread
    interface OnCompletedListener {
        void onDealProductsDownloadCompleted();
    }

    public DownloadDealProductListTask(@NonNull Session session, @NonNull Long dealSqlId, @Nullable OnCompletedListener onCompletedListener, @Nullable OnErrorListener onErrorListener) {
        super(session);
        this.dealSqlId = dealSqlId;
        this.onCompletedListener = onCompletedListener;
        this.onErrorListener = onErrorListener;
    }

    protected Void doInBackgroundAfterStoreSync(Void... params) {
        downloadDealProductList();
        return null;
    }

    private void downloadDealProductList() {
        Deal deal = (Deal) new DealsDataSource(getSession().getDatabase()).findBySqlId(this.dealSqlId.longValue());
        if (deal == null) {
            this.exception = new RuntimeException("Deal not found");
            this.success = false;
            return;
        }
        Long dealPipedriveId = deal.getPipedriveIdOrNull();
        if (dealPipedriveId == null) {
            this.exception = new RuntimeException("DealPipedriveId is null");
            this.success = false;
            return;
        }
        new DealProductsDataSource(getSession().getDatabase()).deleteAllRelatedToParent(deal);
        Long start = null;
        Long limit = null;
        while (true) {
            AnonymousClass1DealProductListResponse response = (AnonymousClass1DealProductListResponse) ConnectionUtil.requestGet(UrlTemplates.listDealProducts(getSession(), dealPipedriveId, start, limit), new ConnectionUtil$JsonReaderInterceptor<AnonymousClass1DealProductListResponse>() {
                public AnonymousClass1DealProductListResponse interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull AnonymousClass1DealProductListResponse responseTemplate) throws IOException {
                    JsonElement root = new JsonParser().parse(jsonReader);
                    if (root.isJsonArray()) {
                        DealProductsDataSource dealProductsDataSource = new DealProductsDataSource(session.getDatabase());
                        for (DealProductEntity dealProductEntity : (DealProductEntity[]) GsonHelper.fromJSON(root, DealProductEntity[].class)) {
                            DealProduct dealProduct = DownloadDealProductListTask.this.createDealProductFromEntity(session, dealProductEntity);
                            if (DownloadDealProductListTask.this.isCancelled()) {
                                break;
                            }
                            dealProductsDataSource.createOrUpdate(dealProduct);
                        }
                    } else {
                        responseTemplate.interceptError = new JsonParseException("JsonArray is expected in 'data'");
                    }
                    return responseTemplate;
                }
            }, new Response() {
                Throwable interceptError = null;
            });
            if (response.interceptError != null) {
                this.exception = response.interceptError;
                this.success = false;
                return;
            } else if (!response.isRequestSuccessful) {
                this.exception = new Exception("Backend request was unsuccessful");
                this.success = false;
                return;
            } else if (response.getPagination().moreItemsInCollection) {
                start = Long.valueOf((long) response.getPagination().nextStart);
                limit = Long.valueOf((long) response.getPagination().limit);
            } else {
                this.success = true;
                return;
            }
        }
    }

    @Nullable
    DealProduct createDealProductFromEntity(@NonNull Session session, @Nullable DealProductEntity entity) {
        ProductVariation productVariation = null;
        if (entity == null || entity.getItemPrice() == null || entity.getQuantity() == null || entity.getDiscountPercentage() == null || entity.getActiveFlag() == null || entity.getOrder() == null || entity.getCurrency() == null) {
            return null;
        }
        Product product;
        if (entity.getProductEntity() == null) {
            product = ProductsNetworkingUtil.createOrUpdateDeletedProductIntoDB(session, entity.getProductPipedriveId(), entity.getName());
        } else {
            product = ProductsNetworkingUtil.createOrUpdateProductIntoDBWithRelations(session, entity.getProductEntity());
        }
        if (product == null) {
            return null;
        }
        Currency currency = CurrenciesNetworkingUtil.getCurrencyByCodeAndDownloadIfNotFound(session, entity.getCurrency());
        if (currency == null) {
            return null;
        }
        if (entity.getProductVariationPipedriveId() != null) {
            productVariation = (ProductVariation) new ProductVariationDataSource(session.getDatabase()).findByPipedriveId(entity.getProductVariationPipedriveId().longValue());
        }
        return DealProduct.create(entity.getPipedriveId(), this.dealSqlId, product, productVariation, Price.create(entity.getItemPrice(), currency), entity.getQuantity(), entity.getDuration(), entity.getDiscountPercentage(), entity.getActiveFlag(), entity.getOrder());
    }

    protected void onPostExecute(Void aVoid) {
        if (this.onCompletedListener != null && this.success) {
            this.onCompletedListener.onDealProductsDownloadCompleted();
        } else if (this.onErrorListener != null) {
            this.onErrorListener.onDealProductsDownloadFailedWithError(this.exception);
        }
    }
}
