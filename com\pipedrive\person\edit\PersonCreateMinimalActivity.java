package com.pipedrive.person.edit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.pipedrive.R;
import com.pipedrive.util.FullCreationFormExpansionWithTransition;

public class PersonCreateMinimalActivity extends PersonCreateActivity {
    @BindView(2131820738)
    View mDescriptionContainer;
    @BindView(2131820969)
    TextView mDescriptionText;
    @BindView(2131820737)
    LinearLayout mMainContainer;
    @BindView(2131820552)
    View mMoreDetails;

    int getLayoutId() {
        return R.layout.activity_person_create_minimal_form;
    }

    @MainThread
    public static void startActivityForResult(@NonNull Activity activity, @Nullable String nameProvided, int requestCode) {
        Intent intent = new Intent(activity, PersonCreateMinimalActivity.class);
        if (nameProvided != null) {
            intent.putExtra("NEW_PERSON_NAME", nameProvided);
        }
        ActivityCompat.startActivityForResult(activity, intent, requestCode, null);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind((Activity) this);
        this.mDescriptionText.setText(R.string.we_can_create_a_new_contact_with_just_the_name_);
    }

    @OnClick({2131820970})
    void onClicked() {
        new FullCreationFormExpansionWithTransition(this, this.mMainContainer).startTransition(this.mMainContainer, this.mDescriptionContainer, this.mMoreDetails);
    }
}
