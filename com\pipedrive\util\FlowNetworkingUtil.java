package com.pipedrive.util;

import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.newrelic.agent.android.instrumentation.GsonInstrumentation;
import com.pipedrive.application.Session;
import com.pipedrive.contentproviders.FlowContentProvider;
import com.pipedrive.datasource.SQLTransactionManager;
import com.pipedrive.gson.GsonHelper;
import com.pipedrive.model.Deal;
import com.pipedrive.util.activities.ActivityNetworkingUtil;
import com.pipedrive.util.flowfiles.FlowFileNetworkingUtil;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.entities.ActivityEntity;
import com.pipedrive.util.networking.entities.EmailMessageEntity;
import com.pipedrive.util.networking.entities.FlowFileEntity;
import com.pipedrive.util.networking.entities.FlowItem;
import com.pipedrive.util.networking.entities.FlowResponse;
import com.pipedrive.util.networking.entities.NoteEntity;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.lang.reflect.Type;

public enum FlowNetworkingUtil {
    ;

    @WorkerThread
    public static FlowResponse downloadAndStoreFlowData(@NonNull Session session, @NonNull String apiEndpoint, @IntRange(from = 1) long id, @Nullable Long start, @Nullable Long limit) throws IOException {
        FlowResponse flowResponse = null;
        if (!TextUtils.isEmpty(apiEndpoint) && id > 0 && ConnectionUtil.isConnected(session.getApplicationContext())) {
            InputStream inputStream = ConnectionUtil.requestGet(new ApiUrlBuilder(session, apiEndpoint).flow(Long.valueOf(id), start, limit));
            if (inputStream != null) {
                Gson gson = new Gson();
                JsonReader jsonReader = new JsonReader(new InputStreamReader(inputStream));
                Type type = FlowResponse.class;
                flowResponse = !(gson instanceof Gson) ? gson.fromJson(jsonReader, type) : GsonInstrumentation.fromJson(gson, jsonReader, type);
                if (!(flowResponse == null || !flowResponse.isRequestSuccessful || flowResponse.getItems() == null)) {
                    storeFlowData(session, flowResponse, apiEndpoint, id);
                }
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
        return flowResponse;
    }

    @WorkerThread
    private static void storeFlowData(@NonNull Session session, @NonNull FlowResponse response, @NonNull String apiEndpoint, @IntRange(from = 1) long id) throws IOException {
        boolean shouldNotify = false;
        SQLTransactionManager sqlTransactionManager = new SQLTransactionManager(session.getDatabase());
        sqlTransactionManager.beginTransactionNonExclusive();
        for (FlowItem flowItem : response.getItems()) {
            String flowItemType = flowItem.getItemType();
            Object obj = -1;
            switch (flowItemType.hashCode()) {
                case -1655966961:
                    if (flowItemType.equals(FlowItem.ITEM_TYPE_OBJECT_ACTIVITY)) {
                        obj = null;
                        break;
                    }
                    break;
                case 3079276:
                    if (flowItemType.equals("deal")) {
                        obj = 2;
                        break;
                    }
                    break;
                case 3143036:
                    if (flowItemType.equals(FlowItem.ITEM_TYPE_OBJECT_FILE)) {
                        obj = 3;
                        break;
                    }
                    break;
                case 3387378:
                    if (flowItemType.equals(FlowItem.ITEM_TYPE_OBJECT_NOTE)) {
                        obj = 1;
                        break;
                    }
                    break;
                case 1031685323:
                    if (flowItemType.equals(FlowItem.ITEM_TYPE_OBJECT_EMAILMESSAGE)) {
                        obj = 4;
                        break;
                    }
                    break;
            }
            switch (obj) {
                case null:
                    ActivityNetworkingUtil.createOrUpdateActivityIntoDBWithRelations(session, (ActivityEntity) GsonHelper.fromJSON(new JsonReader(new StringReader(flowItem.getData().toString())), ActivityEntity.class));
                    break;
                case 1:
                    NoteEntity noteEntity = (NoteEntity) GsonHelper.fromJSON(new JsonReader(new StringReader(flowItem.getData().toString())), NoteEntity.class);
                    if (noteEntity == null) {
                        break;
                    }
                    NotesNetworkingUtil.createOrUpdateNoteIntoDBWithRelations(session, noteEntity);
                    shouldNotify = true;
                    break;
                case 2:
                    Deal deal = Deal.readDeal(new JsonReader(new StringReader(flowItem.getData().toString())));
                    if (deal == null) {
                        break;
                    }
                    DealsNetworkingUtil.createOrUpdateDealIntoDBWithRelations(session, deal);
                    shouldNotify = true;
                    break;
                case 3:
                    FlowFileEntity flowFileEntity = (FlowFileEntity) GsonHelper.fromJSON(new JsonReader(new StringReader(flowItem.getData().toString())), FlowFileEntity.class);
                    if (flowFileEntity == null) {
                        break;
                    }
                    FlowFileNetworkingUtil.createOrUpdateFlowFileIntoDBWithRelations(session, flowFileEntity);
                    shouldNotify = true;
                    break;
                case 4:
                    EmailMessageEntity emailMessage = (EmailMessageEntity) GsonHelper.fromJSON(new JsonReader(new StringReader(flowItem.getData().toString())), EmailMessageEntity.class);
                    if (!(emailMessage == null || TextUtils.isEmpty(apiEndpoint))) {
                        obj = -1;
                        switch (apiEndpoint.hashCode()) {
                            case -2108114528:
                                if (apiEndpoint.equals("organizations")) {
                                    obj = 1;
                                    break;
                                }
                                break;
                            case -678441026:
                                if (apiEndpoint.equals("persons")) {
                                    obj = 2;
                                    break;
                                }
                                break;
                            case 95457671:
                                if (apiEndpoint.equals("deals")) {
                                    obj = null;
                                    break;
                                }
                                break;
                        }
                        switch (obj) {
                            case null:
                                EmailNetworkingUtil.createOrUpdateMessage(session, emailMessage, Long.valueOf(id), null);
                                shouldNotify = true;
                                break;
                            case 1:
                                EmailNetworkingUtil.createOrUpdateMessage(session, emailMessage, null, Long.valueOf(id));
                                shouldNotify = true;
                                break;
                            case 2:
                                EmailNetworkingUtil.createOrUpdateMessage(session, emailMessage, null, null);
                                shouldNotify = true;
                                break;
                            default:
                                break;
                        }
                    }
                default:
                    break;
            }
        }
        sqlTransactionManager.commit();
        if (shouldNotify) {
            FlowContentProvider.notifyChangesMadeInFlow(session.getApplicationContext());
        }
    }
}
