package com.zendesk.sdk.model.request;

import android.support.annotation.Nullable;

public class Organization {
    private Long id;
    private String name;

    @Nullable
    public Long getId() {
        return this.id;
    }

    @Nullable
    public String getName() {
        return this.name;
    }
}
