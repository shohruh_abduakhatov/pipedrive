package com.pipedrive.deal;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;
import com.pipedrive.views.assignment.AssignedToView;
import com.pipedrive.views.association.OrganizationAssociationView;
import com.pipedrive.views.association.PersonAssociationView;
import com.pipedrive.views.common.CurrencySpinner;
import com.pipedrive.views.common.VisibilitySpinner;
import com.pipedrive.views.edit.deal.DealTitleEditText;
import com.pipedrive.views.edit.deal.DealValueEditText;
import com.pipedrive.views.edit.deal.PipelineSpinner;
import com.pipedrive.views.edit.deal.StageSpinner;

public class DealEditActivity_ViewBinding implements Unbinder {
    private DealEditActivity target;
    private View view2131820747;
    private View view2131820751;

    @UiThread
    public DealEditActivity_ViewBinding(DealEditActivity target) {
        this(target, target.getWindow().getDecorView());
    }

    @UiThread
    public DealEditActivity_ViewBinding(final DealEditActivity target, View source) {
        this.target = target;
        target.mTitle = (DealTitleEditText) Utils.findRequiredViewAsType(source, R.id.title, "field 'mTitle'", DealTitleEditText.class);
        target.mValue = (DealValueEditText) Utils.findRequiredViewAsType(source, R.id.value, "field 'mValue'", DealValueEditText.class);
        target.mAssignedToView = (AssignedToView) Utils.findRequiredViewAsType(source, R.id.assignedTo, "field 'mAssignedToView'", AssignedToView.class);
        target.mCurrencySpinner = (CurrencySpinner) Utils.findRequiredViewAsType(source, R.id.currency, "field 'mCurrencySpinner'", CurrencySpinner.class);
        target.mPipelineSpinner = (PipelineSpinner) Utils.findRequiredViewAsType(source, R.id.pipeline, "field 'mPipelineSpinner'", PipelineSpinner.class);
        target.mStageSpinner = (StageSpinner) Utils.findRequiredViewAsType(source, R.id.stage, "field 'mStageSpinner'", StageSpinner.class);
        target.mVisibilitySpinner = (VisibilitySpinner) Utils.findRequiredViewAsType(source, R.id.visibleTo, "field 'mVisibilitySpinner'", VisibilitySpinner.class);
        target.mPersonAssociationView = (PersonAssociationView) Utils.findRequiredViewAsType(source, R.id.personAssociationView, "field 'mPersonAssociationView'", PersonAssociationView.class);
        target.mValueContainerNoProducts = Utils.findRequiredView(source, R.id.valueContainerNoProducts, "field 'mValueContainerNoProducts'");
        target.mValueContainerWithProducts = Utils.findRequiredView(source, R.id.valueContainerWithProducts, "field 'mValueContainerWithProducts'");
        target.mProductsValue = (TextView) Utils.findRequiredViewAsType(source, R.id.valueWithProducts, "field 'mProductsValue'", TextView.class);
        View view = Utils.findRequiredView(source, R.id.productCount, "field 'mProductCount' and method 'onProductCountClicked'");
        target.mProductCount = (TextView) Utils.castView(view, R.id.productCount, "field 'mProductCount'", TextView.class);
        this.view2131820747 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onProductCountClicked();
            }
        });
        view = Utils.findRequiredView(source, R.id.attachProducts, "field 'mAttachProductsButton' and method 'onAttachProductsClicked'");
        target.mAttachProductsButton = (Button) Utils.castView(view, R.id.attachProducts, "field 'mAttachProductsButton'", Button.class);
        this.view2131820751 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onAttachProductsClicked();
            }
        });
        target.mOrganizationAssociationView = (OrganizationAssociationView) Utils.findRequiredViewAsType(source, R.id.organizationAssociationView, "field 'mOrganizationAssociationView'", OrganizationAssociationView.class);
        target.mToolbar = (Toolbar) Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'mToolbar'", Toolbar.class);
    }

    @CallSuper
    public void unbind() {
        DealEditActivity target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mTitle = null;
        target.mValue = null;
        target.mAssignedToView = null;
        target.mCurrencySpinner = null;
        target.mPipelineSpinner = null;
        target.mStageSpinner = null;
        target.mVisibilitySpinner = null;
        target.mPersonAssociationView = null;
        target.mValueContainerNoProducts = null;
        target.mValueContainerWithProducts = null;
        target.mProductsValue = null;
        target.mProductCount = null;
        target.mAttachProductsButton = null;
        target.mOrganizationAssociationView = null;
        target.mToolbar = null;
        this.view2131820747.setOnClickListener(null);
        this.view2131820747 = null;
        this.view2131820751.setOnClickListener(null);
        this.view2131820751 = null;
    }
}
