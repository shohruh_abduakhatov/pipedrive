package com.pipedrive.datasource;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datetime.PipedriveDate;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.model.Activity;
import com.pipedrive.model.Activity.ActivityBuilderForDataSource;
import com.pipedrive.model.Activity.ActivityBuilderForDataSource.ActivityStartDateTimeType;
import com.pipedrive.model.Activity.ActivityDeflaterForDataSource;
import com.pipedrive.model.ActivityType;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.util.CursorHelper;
import com.pipedrive.util.StringUtils;
import com.pipedrive.util.TimeFilter;
import com.pipedrive.util.activities.ActivityNetworkingUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import rx.Observable;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class ActivitiesDataSource extends BaseDataSource<Activity> {
    private final DealsDataSource dealsDataSource = new DealsDataSource(getTransactionalDBConnection());
    private final PersonsDataSource mPersonsDataSource = new PersonsDataSource(getTransactionalDBConnection());
    private final Session mSession;
    private final OrganizationsDataSource orgDataSource = new OrganizationsDataSource(getTransactionalDBConnection());

    public interface OnActivityRetrieved {
        void activity(@NonNull Activity activity);
    }

    public ActivitiesDataSource(Session session) {
        super(session.getDatabase());
        this.mSession = session;
    }

    public void deleteActivity(long pipedriveId) {
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String tableName = getTableName();
        String str = "ac_pd_id = " + pipedriveId;
        if (transactionalDBConnection instanceof SQLiteDatabase) {
            SQLiteInstrumentation.delete(transactionalDBConnection, tableName, str, null);
        } else {
            transactionalDBConnection.delete(tableName, str, null);
        }
    }

    @NonNull
    public List<Activity> getActiveUndoneActivitiesByDealSqlId(long dealSqlId) {
        String selection = "ac_deal_id_sql = ?  AND ac_done = ? ";
        return getActiveActivities("ac_deal_id_sql = ?  AND ac_done = ? ", new String[]{Long.toString(dealSqlId), Integer.toString(0)});
    }

    @Nullable
    private Activity getActivityForRelatedItem(boolean activityIsNext, @NonNull String relatedItemColumn, @NonNull Long relatedItemSqlId) {
        int i;
        String orderBy = PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME + (activityIsNext ? " ASC" : " DESC");
        String selectionBuilder = relatedItemColumn + " = ? AND " + PipeSQLiteHelper.COLUMN_ACTIVITIES_IS_ACTIVE + " = ? AND " + PipeSQLiteHelper.COLUMN_ACTIVITIES_DONE + "  = ?";
        String[] selectionArgs = new String[3];
        selectionArgs[0] = String.valueOf(relatedItemSqlId);
        selectionArgs[1] = String.valueOf(1);
        if (activityIsNext) {
            i = 0;
        } else {
            i = 1;
        }
        selectionArgs[2] = String.valueOf(i);
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String tableName = getTableName();
        String[] allColumns = getAllColumns();
        String str = "1";
        Cursor cursor = !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.query(tableName, allColumns, selectionBuilder, selectionArgs, null, null, orderBy, str) : SQLiteInstrumentation.query(transactionalDBConnection, tableName, allColumns, selectionBuilder, selectionArgs, null, null, orderBy, str);
        Activity activity = null;
        cursor.moveToFirst();
        try {
            if (!cursor.isAfterLast()) {
                activity = deflateCursor(cursor);
            }
            cursor.close();
            return activity;
        } catch (Throwable th) {
            cursor.close();
        }
    }

    @NonNull
    public Observable<Activity> getActivityForRelatedItemRx(final boolean activityIsNext, @NonNull final String relatedItemColumn, @NonNull Long sqlId) {
        return Observable.just(sqlId).subscribeOn(Schedulers.io()).map(new Func1<Long, Activity>() {
            public Activity call(Long sqlId) {
                return ActivitiesDataSource.this.getActivityForRelatedItem(activityIsNext, relatedItemColumn, sqlId);
            }
        });
    }

    @NonNull
    private List<Activity> getActiveActivities(@Nullable String selection, @NonNull String[] selectionArgs) {
        List<Activity> activities = new ArrayList();
        if (StringUtils.isTrimmedAndEmpty(selection)) {
            selection = "";
        }
        StringBuilder finalSelectionBuilder = new StringBuilder(selection);
        if (finalSelectionBuilder.length() > 0) {
            finalSelectionBuilder.append(" AND ");
        }
        finalSelectionBuilder.append("ac_is_active = ? ");
        String[] finalSelectionArgs = (String[]) Arrays.copyOf(selectionArgs, selectionArgs.length + 1);
        finalSelectionArgs[finalSelectionArgs.length - 1] = Integer.toString(1);
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String tableName = getTableName();
        String[] allColumns = getAllColumns();
        String stringBuilder = finalSelectionBuilder.toString();
        Cursor cursor = !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.query(tableName, allColumns, stringBuilder, finalSelectionArgs, null, null, null, null) : SQLiteInstrumentation.query(transactionalDBConnection, tableName, allColumns, stringBuilder, finalSelectionArgs, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            activities.add(deflateCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return activities;
    }

    @Deprecated
    @NonNull
    public List<Activity> getAllActiveActivities(boolean assignedOnlyToLoggedInUser) {
        String selection = assignedOnlyToLoggedInUser ? "ac_assigned_to_user_id = ? " : null;
        return getActiveActivities(selection, selection == null ? new String[0] : new String[]{Long.toString(this.mSession.getAuthenticatedUserID())});
    }

    @NonNull
    protected String getColumnNameForPipedriveId() {
        return PipeSQLiteHelper.COLUMN_ACTIVITIES_PIPEDRIVE_ID;
    }

    @NonNull
    protected String getColumnNameForSqlId() {
        return PipeSQLiteHelper.COLUMN_ID;
    }

    @NonNull
    protected String getTableName() {
        return "activities";
    }

    @NonNull
    protected ContentValues getContentValues(@NonNull Activity activity) {
        int i;
        int i2 = 1;
        ActivityDeflaterForDataSource activityDeflater = new ActivityDeflaterForDataSource(activity);
        ContentValues values = new ContentValues();
        if (activity.getSqlId() != 0) {
            values.put(PipeSQLiteHelper.COLUMN_ID, Long.valueOf(activity.getSqlId()));
        }
        if (activity.isExisting()) {
            values.put(PipeSQLiteHelper.COLUMN_ACTIVITIES_PIPEDRIVE_ID, Integer.valueOf(activity.getPipedriveId()));
        }
        CursorHelper.put(activity.getSubject(), values, PipeSQLiteHelper.COLUMN_ACTIVITIES_SUBJECT);
        if (StringUtils.isTrimmedAndEmpty(activity.getNote())) {
            values.putNull(PipeSQLiteHelper.COLUMN_ACTIVITIES_NOTE);
        } else {
            values.put(PipeSQLiteHelper.COLUMN_ACTIVITIES_NOTE, activity.getNote());
        }
        String str = PipeSQLiteHelper.COLUMN_ACTIVITIES_DONE;
        if (activity.isDone()) {
            i = 1;
        } else {
            i = 0;
        }
        values.put(str, Integer.valueOf(i));
        values.put(PipeSQLiteHelper.COLUMN_ACTIVITIES_TYPE, activity.getType() != null ? activity.getType().getName() : "");
        if (activity.getPerson() == null || activity.getPerson().getSqlId() <= 0) {
            values.putNull(PipeSQLiteHelper.COLUMN_ACTIVITIES_PERSON_ID_SQL);
        } else {
            values.put(PipeSQLiteHelper.COLUMN_ACTIVITIES_PERSON_ID_SQL, Long.valueOf(activity.getPerson().getSqlId()));
        }
        if (activity.getOrganization() == null || activity.getOrganization().getSqlId() <= 0) {
            values.putNull(PipeSQLiteHelper.COLUMN_ACTIVITIES_ORGANIZATION_ID_SQL);
        } else {
            values.put(PipeSQLiteHelper.COLUMN_ACTIVITIES_ORGANIZATION_ID_SQL, Long.valueOf(activity.getOrganization().getSqlId()));
        }
        if (activity.getDeal() == null || activity.getDeal().getSqlId() <= 0) {
            values.putNull(PipeSQLiteHelper.COLUMN_ACTIVITIES_DEAL_ID_SQL);
        } else {
            values.put(PipeSQLiteHelper.COLUMN_ACTIVITIES_DEAL_ID_SQL, Long.valueOf(activity.getDeal().getSqlId()));
        }
        values.put(PipeSQLiteHelper.COLUMN_ACTIVITIES_ASSIGNED_TO_USER_ID, Long.valueOf(activity.getAssignedUserId()));
        String str2 = PipeSQLiteHelper.COLUMN_ACTIVITIES_IS_ACTIVE;
        if (!activity.isActive()) {
            i2 = 0;
        }
        values.put(str2, Integer.valueOf(i2));
        CursorHelper.put(activityDeflater.getActivityStartInSeconds(), values, PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME);
        CursorHelper.put(activityDeflater.getActivityEndInSeconds(), values, PipeSQLiteHelper.COLUMN_ACTIVITIES_END_DATETIME);
        CursorHelper.put(activityDeflater.getStartDateTimeType(), values, PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME_TYPE);
        return values;
    }

    @NonNull
    protected String[] getAllColumns() {
        return new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_ACTIVITIES_PIPEDRIVE_ID, PipeSQLiteHelper.COLUMN_ACTIVITIES_SUBJECT, PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME, PipeSQLiteHelper.COLUMN_ACTIVITIES_END_DATETIME, PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME_TYPE, PipeSQLiteHelper.COLUMN_ACTIVITIES_NOTE, PipeSQLiteHelper.COLUMN_ACTIVITIES_DONE, PipeSQLiteHelper.COLUMN_ACTIVITIES_TYPE, PipeSQLiteHelper.COLUMN_ACTIVITIES_PERSON_ID_SQL, PipeSQLiteHelper.COLUMN_ACTIVITIES_ORGANIZATION_ID_SQL, PipeSQLiteHelper.COLUMN_ACTIVITIES_DEAL_ID_SQL, PipeSQLiteHelper.COLUMN_ACTIVITIES_ASSIGNED_TO_USER_ID, PipeSQLiteHelper.COLUMN_ACTIVITIES_IS_ACTIVE};
    }

    @Nullable
    public Activity deflateCursor(@NonNull Cursor cursor) {
        boolean z;
        boolean z2 = true;
        ActivityBuilderForDataSource activityBuilder = new ActivityBuilderForDataSource(this.mSession);
        Activity activity = activityBuilder.getActivityBeingBuilt();
        Integer sqlId = CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_ID);
        if (sqlId != null) {
            activity.setSqlId((long) sqlId.intValue());
        }
        Integer pipedriveId = CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_ACTIVITIES_PIPEDRIVE_ID);
        if (pipedriveId != null) {
            activity.setPipedriveId(pipedriveId.intValue());
        }
        activity.setSubject(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_ACTIVITIES_SUBJECT));
        activity.setNote(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_ACTIVITIES_NOTE));
        if (CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_ACTIVITIES_DONE, 0).intValue() == 1) {
            z = true;
        } else {
            z = false;
        }
        activity.setDone(z);
        activity.setType(ActivityType.getActivityTypeByName(this.mSession, CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_ACTIVITIES_TYPE)));
        activityBuilder.setActivityStartAndEnd(CursorHelper.getLong(cursor, PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME), CursorHelper.getLong(cursor, PipeSQLiteHelper.COLUMN_ACTIVITIES_END_DATETIME), CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME_TYPE));
        activity.setPerson((Person) this.mPersonsDataSource.findBySqlId((long) CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_ACTIVITIES_PERSON_ID_SQL, 0).intValue()));
        activity.setOrganization((Organization) this.orgDataSource.findBySqlId((long) CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_ACTIVITIES_ORGANIZATION_ID_SQL, 0).intValue()));
        activity.setDeal((Deal) this.dealsDataSource.findBySqlId((long) CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_ACTIVITIES_DEAL_ID_SQL, 0).intValue()));
        Long assignedUserId = CursorHelper.getLong(cursor, PipeSQLiteHelper.COLUMN_ACTIVITIES_ASSIGNED_TO_USER_ID);
        if (assignedUserId != null) {
            activity.setAssignedUserId(assignedUserId.longValue());
        }
        if (CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_ACTIVITIES_IS_ACTIVE, 0).intValue() != 1) {
            z2 = false;
        }
        activity.setActive(z2);
        return activity;
    }

    public Cursor getUndoneActivePastActivitiesCursor(@Nullable String type, @Nullable Integer timeFilter, @Nullable Integer userId) {
        return getUndoneActiveActivitiesCursor(type, timeFilter, true, userId);
    }

    @Nullable
    public Cursor getUndoneActiveFutureActivitiesCursor() {
        return getUndoneActiveFutureActivitiesCursor(null, null, null);
    }

    public Cursor getUndoneActiveFutureActivitiesCursor(@Nullable String type, @Nullable Integer timeFilter, @Nullable Integer userId) {
        return getUndoneActiveActivitiesCursor(type, timeFilter, false, userId);
    }

    private Cursor getUndoneActiveActivitiesCursor(@Nullable String type, @Nullable Integer timeFilter, boolean past, @Nullable Integer userId) {
        StringBuilder selection = new StringBuilder("ac_is_active=? AND ac_done=?");
        List<String> selectionArgsList = new ArrayList<String>() {
            {
                add(String.valueOf(1));
                add(String.valueOf(0));
            }
        };
        if (userId != null) {
            selection.append(" AND ac_assigned_to_user_id=?");
            selectionArgsList.add(String.valueOf(userId));
        }
        if (type != null) {
            selection.append(" AND ac_type=?");
            selectionArgsList.add(type);
        }
        if (timeFilter != null) {
            selection.append(" AND ").append(TimeFilter.getSelectionForActivityListFilter(timeFilter.intValue()));
            selectionArgsList.addAll(TimeFilter.getSelectionArgsForActivityListFilter(timeFilter.intValue()));
        }
        if (past) {
            selection.append(" AND ").append(TimeFilter.getSelectionForActivityListFilter(1));
            selectionArgsList.addAll(TimeFilter.getSelectionArgsForActivityListFilter(1));
        } else {
            selection.append(" AND ").append(TimeFilter.getSelectionForActivityListFilter(1000));
            selectionArgsList.addAll(TimeFilter.getSelectionArgsForActivityListFilter(1000));
        }
        String[] selectionArgs = (String[]) selectionArgsList.toArray(new String[selectionArgsList.size()]);
        String tableName = getTableName();
        String[] allColumns = getAllColumns();
        String stringBuilder = selection.toString();
        String str = PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME;
        if (this instanceof SQLiteDatabase) {
            return SQLiteInstrumentation.query((SQLiteDatabase) this, tableName, allColumns, stringBuilder, selectionArgs, null, null, str);
        }
        return query(tableName, allColumns, stringBuilder, selectionArgs, null, null, str);
    }

    public Cursor getActiveActivityListGroupCursor(Context context, @Nullable String activityTypeSelected, @Nullable Integer timeFilterSelected, @Nullable Integer userId) {
        MatrixCursor cursor = new MatrixCursor(new String[]{PipeSQLiteHelper.COLUMN_ID, "name"});
        Cursor activitiesCursor = null;
        try {
            activitiesCursor = getUndoneActivePastActivitiesCursor(activityTypeSelected, timeFilterSelected, userId);
            if (activitiesCursor.getCount() > 0) {
                cursor.addRow(new Object[]{Integer.valueOf(1), context.getString(R.string.activity_category_type_title_overdue)});
            }
            if (activitiesCursor != null) {
                activitiesCursor.close();
            }
            try {
                activitiesCursor = getUndoneActiveFutureActivitiesCursor(activityTypeSelected, timeFilterSelected, userId);
                if (activitiesCursor.getCount() > 0) {
                    cursor.addRow(new Object[]{Integer.valueOf(2), context.getString(R.string.flow_list_section_upcoming)});
                }
                if (activitiesCursor != null) {
                    activitiesCursor.close();
                }
                return cursor;
            } catch (Throwable th) {
                if (activitiesCursor != null) {
                    activitiesCursor.close();
                }
            }
        } catch (Throwable th2) {
            if (activitiesCursor != null) {
                activitiesCursor.close();
            }
        }
    }

    @NonNull
    public Cursor getUndoneCallActiveActivitiesRelatedToPerson(@NonNull Long personSqlId) {
        int i = 0;
        String[] selectionArgs = new String[]{String.valueOf(1), String.valueOf(0), String.valueOf(personSqlId)};
        String selection = "ac_is_active =?  AND ac_done =?  AND ac_person_id_sql =? ";
        String[] callActivityTypes = ActivityNetworkingUtil.getCallActivityTypeNames(this.mSession);
        StringBuilder callActivitySelection = new StringBuilder();
        int length = callActivityTypes.length;
        while (i < length) {
            String ignored = callActivityTypes[i];
            if (callActivitySelection.length() > 0) {
                callActivitySelection.append(" OR ");
            }
            callActivitySelection.append("ac_type =? ");
            i++;
        }
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String tableName = getTableName();
        String[] allColumns = getAllColumns();
        String concatenateWhere = DatabaseUtils.concatenateWhere("ac_is_active =?  AND ac_done =?  AND ac_person_id_sql =? ", callActivitySelection.toString());
        String[] appendSelectionArgs = DatabaseUtils.appendSelectionArgs(selectionArgs, callActivityTypes);
        return !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.query(tableName, allColumns, concatenateWhere, appendSelectionArgs, null, null, null) : SQLiteInstrumentation.query(transactionalDBConnection, tableName, allColumns, concatenateWhere, appendSelectionArgs, null, null, null);
    }

    @NonNull
    private Cursor getAllDayActiveActivitiesCursor(@NonNull Date date, @Nullable Long listActivitiesAssignedToUserPipedriveId) {
        StringBuilder selection = new StringBuilder("ac_is_active =? AND ac_start_type=?");
        List<String> selectionArgsList = new ArrayList<String>() {
            {
                add(String.valueOf(1));
                add(String.valueOf(ActivityStartDateTimeType.Date.getSqlValue()));
            }
        };
        selection.append(" AND date(").append(PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME).append(") =date(?) ");
        selectionArgsList.add(String.valueOf(PipedriveDate.instanceFrom(date.getTime()).getRepresentationInUnixTime()));
        if (listActivitiesAssignedToUserPipedriveId != null) {
            selection.append(" AND ac_assigned_to_user_id =? ");
            selectionArgsList.add(String.valueOf(listActivitiesAssignedToUserPipedriveId));
        }
        String[] selectionArgs = (String[]) selectionArgsList.toArray(new String[selectionArgsList.size()]);
        String tableName = getTableName();
        String[] allColumns = getAllColumns();
        String stringBuilder = selection.toString();
        String str = PipeSQLiteHelper.COLUMN_ACTIVITIES_SUBJECT;
        return !(this instanceof SQLiteDatabase) ? query(tableName, allColumns, stringBuilder, selectionArgs, null, null, str) : SQLiteInstrumentation.query((SQLiteDatabase) this, tableName, allColumns, stringBuilder, selectionArgs, null, null, str);
    }

    @NonNull
    private Cursor getNonAllDayActiveActivitiesForDateCursor(@NonNull Calendar date, @Nullable Long userPipedriveId) {
        StringBuilder selection = new StringBuilder();
        List<String> selectionArgs = new ArrayList();
        long startOfDayUnixRepresentation = PipedriveDateTime.instanceFromDate(date.getTime()).resetTimeLocal().getRepresentationInUnixTime();
        long endOfDayUnixRepresentation = PipedriveDateTime.instanceFromDate(date.getTime()).setTimeToEndOfTheDayLocal().getRepresentationInUnixTime();
        selection.append("ac_is_active =? ");
        selectionArgs.add(String.valueOf(1));
        selection.append(" AND ");
        selection.append("ac_start_type =? ");
        selectionArgs.add(String.valueOf(ActivityStartDateTimeType.DateTime.getSqlValue()));
        if (userPipedriveId != null) {
            selection.append(" AND ");
            selection.append("ac_assigned_to_user_id =? ");
            selectionArgs.add(String.valueOf(userPipedriveId));
        }
        selection.append(" AND ");
        selection.append(" ( ");
        selection.append(" ( ");
        selection.append(PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME);
        selection.append(" BETWEEN ");
        selection.append(String.valueOf(startOfDayUnixRepresentation));
        selection.append(" AND ");
        selection.append(String.valueOf(endOfDayUnixRepresentation));
        selection.append(" ) ");
        selection.append(" OR ( ");
        selection.append(PipeSQLiteHelper.COLUMN_ACTIVITIES_END_DATETIME);
        selection.append(" BETWEEN ");
        selection.append(String.valueOf(startOfDayUnixRepresentation));
        selection.append(" AND ");
        selection.append(String.valueOf(endOfDayUnixRepresentation));
        selection.append(" ) ");
        selection.append(" OR (");
        selection.append(PipeSQLiteHelper.COLUMN_ACTIVITIES_START_DATETIME);
        selection.append(" < ");
        selection.append(String.valueOf(startOfDayUnixRepresentation));
        selection.append(" AND ");
        selection.append(PipeSQLiteHelper.COLUMN_ACTIVITIES_END_DATETIME);
        selection.append(" > ");
        selection.append(String.valueOf(endOfDayUnixRepresentation));
        selection.append(")");
        selection.append(")");
        String tableName = getTableName();
        String[] allColumns = getAllColumns();
        String stringBuilder = selection.toString();
        String[] strArr = (String[]) selectionArgs.toArray(new String[selectionArgs.size()]);
        return !(this instanceof SQLiteDatabase) ? query(tableName, allColumns, stringBuilder, strArr, null, null, null) : SQLiteInstrumentation.query((SQLiteDatabase) this, tableName, allColumns, stringBuilder, strArr, null, null, null);
    }

    public void getNonAllDayActiveActivitiesForDate(@NonNull Calendar date, @NonNull OnActivityRetrieved onActivityRetrieved) {
        getNonAllDayActiveActivitiesForDate(date, onActivityRetrieved, null);
    }

    public void getNonAllDayActiveActivitiesForDateForAuthenticatedUserOnly(@NonNull Calendar date, @NonNull OnActivityRetrieved onActivityRetrieved) {
        getNonAllDayActiveActivitiesForDate(date, onActivityRetrieved, Long.valueOf(this.mSession.getAuthenticatedUserID()));
    }

    private void getNonAllDayActiveActivitiesForDate(@NonNull Calendar date, @NonNull OnActivityRetrieved onActivityRetrieved, @Nullable Long listActivitiesAssignedToUserPipedriveId) {
        Cursor cursor = getNonAllDayActiveActivitiesForDateCursor(date, listActivitiesAssignedToUserPipedriveId);
        try {
            cursor.moveToPosition(-1);
            while (cursor.moveToNext()) {
                Activity activity = deflateCursor(cursor);
                if (activity != null) {
                    onActivityRetrieved.activity(activity);
                }
            }
        } finally {
            cursor.close();
        }
    }

    @NonNull
    public List<Activity> getAllDayActiveActivitiesListForDateAuthenticatedUserOnly(Date date) {
        Cursor cursor = getAllDayActiveActivitiesCursor(date, Long.valueOf(this.mSession.getAuthenticatedUserID()));
        List<Activity> activities = new ArrayList();
        try {
            cursor.moveToPosition(-1);
            while (cursor.moveToNext()) {
                activities.add(deflateCursor(cursor));
            }
            return activities;
        } finally {
            cursor.close();
        }
    }
}
