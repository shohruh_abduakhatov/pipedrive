package com.pipedrive.util.notes;

import android.content.res.Resources;
import android.text.TextUtils;
import android.widget.TextView;
import com.pipedrive.R;
import com.pipedrive.model.notes.Note;
import com.pipedrive.util.StringUtils;

public class NotesUtil extends NotesNetworkingUtil {
    public static boolean fillNoteAssociations(Resources resources, Note note, TextView textViewToFill) {
        if (resources == null || note == null || textViewToFill == null) {
            return false;
        }
        String associationsSeparator = resources.getString(R.string.subtitle_separator);
        StringBuilder noteAssociations = new StringBuilder();
        if (!(note.getDeal() == null || StringUtils.isTrimmedAndEmpty(note.getDeal().getTitle()))) {
            noteAssociations.append(note.getDeal().getTitle());
        }
        if (!(note.getPerson() == null || StringUtils.isTrimmedAndEmpty(note.getPerson().getName()))) {
            if (noteAssociations.length() > 0) {
                noteAssociations.append(associationsSeparator);
            }
            noteAssociations.append(note.getPerson().getName());
        }
        if (!(note.getOrganization() == null || StringUtils.isTrimmedAndEmpty(note.getOrganization().getName()))) {
            if (noteAssociations.length() > 0) {
                noteAssociations.append(associationsSeparator);
            }
            noteAssociations.append(note.getOrganization().getName());
        }
        if (noteAssociations.length() == 0) {
            return false;
        }
        CharSequence textViewToFillText = textViewToFill.getText();
        textViewToFill.setText((TextUtils.isEmpty(textViewToFillText) ? "" : textViewToFillText + "") + noteAssociations.toString());
        return true;
    }
}
