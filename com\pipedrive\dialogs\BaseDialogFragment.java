package com.pipedrive.dialogs;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.background.ApplicationStateMonitor;
import com.newrelic.agent.android.instrumentation.Instrumented;
import com.pipedrive.analytics.Analytics;

@Instrumented
public class BaseDialogFragment extends DialogFragment implements TraceFieldInterface {
    protected void onStop() {
        super.onStop();
        ApplicationStateMonitor.getInstance().activityStopped();
    }

    public void onStart() {
        ApplicationStateMonitor.getInstance().activityStarted();
        super.onStart();
        Analytics.hitFragment((Fragment) this);
    }
}
