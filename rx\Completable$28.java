package rx;

import rx.functions.Action0;
import rx.plugins.RxJavaHooks;
import rx.subscriptions.MultipleAssignmentSubscription;

class Completable$28 implements CompletableSubscriber {
    boolean done;
    final /* synthetic */ Completable this$0;
    final /* synthetic */ MultipleAssignmentSubscription val$mad;
    final /* synthetic */ Action0 val$onComplete;

    Completable$28(Completable completable, Action0 action0, MultipleAssignmentSubscription multipleAssignmentSubscription) {
        this.this$0 = completable;
        this.val$onComplete = action0;
        this.val$mad = multipleAssignmentSubscription;
    }

    public void onCompleted() {
        if (!this.done) {
            this.done = true;
            try {
                this.val$onComplete.call();
            } catch (Throwable e) {
                RxJavaHooks.onError(e);
                Completable.deliverUncaughtException(e);
            } finally {
                this.val$mad.unsubscribe();
            }
        }
    }

    public void onError(Throwable e) {
        RxJavaHooks.onError(e);
        this.val$mad.unsubscribe();
        Completable.deliverUncaughtException(e);
    }

    public void onSubscribe(Subscription d) {
        this.val$mad.set(d);
    }
}
