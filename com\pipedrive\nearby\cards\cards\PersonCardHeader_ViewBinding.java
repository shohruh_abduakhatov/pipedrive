package com.pipedrive.nearby.cards.cards;

import android.support.annotation.UiThread;
import android.view.View;
import butterknife.internal.Utils;
import com.pipedrive.R;
import com.pipedrive.views.profilepicture.ProfilePictureRoundPerson;

public class PersonCardHeader_ViewBinding extends CardHeader_ViewBinding {
    private PersonCardHeader target;

    @UiThread
    public PersonCardHeader_ViewBinding(PersonCardHeader target, View source) {
        super(target, source);
        this.target = target;
        target.profilePicture = (ProfilePictureRoundPerson) Utils.findRequiredViewAsType(source, R.id.nearbyPersonCard.profilePicture, "field 'profilePicture'", ProfilePictureRoundPerson.class);
        target.initialsTextSize = source.getContext().getResources().getInteger(R.integer.nearbyPersonCard.profilePicture.initialsTextSize);
    }

    public void unbind() {
        PersonCardHeader target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.profilePicture = null;
        super.unbind();
    }
}
