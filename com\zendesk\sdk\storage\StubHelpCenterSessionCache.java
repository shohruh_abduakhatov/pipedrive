package com.zendesk.sdk.storage;

import android.support.annotation.NonNull;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.helpcenter.LastSearch;

class StubHelpCenterSessionCache implements HelpCenterSessionCache {
    private static final String LOG_TAG = "StubZendeskHelpCenterSessionCache";

    StubHelpCenterSessionCache() {
    }

    @NonNull
    public LastSearch getLastSearch() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
        return new LastSearch("", -1);
    }

    public void setLastSearch(String query, int resultCount) {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
    }

    public void unsetUniqueSearchResultClick() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
    }

    public boolean isUniqueSearchResultClick() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
        return false;
    }
}
