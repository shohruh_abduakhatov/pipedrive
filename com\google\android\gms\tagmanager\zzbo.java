package com.google.android.gms.tagmanager;

public final class zzbo {
    static zzbp aFT = new zzz();
    static int aFU;

    public static void e(String str) {
        aFT.e(str);
    }

    public static int getLogLevel() {
        return aFU;
    }

    public static void setLogLevel(int i) {
        aFU = i;
        aFT.setLogLevel(i);
    }

    public static void v(String str) {
        aFT.v(str);
    }

    public static void zzb(String str, Throwable th) {
        aFT.zzb(str, th);
    }

    public static void zzc(String str, Throwable th) {
        aFT.zzc(str, th);
    }

    public static void zzdg(String str) {
        aFT.zzdg(str);
    }

    public static void zzdh(String str) {
        aFT.zzdh(str);
    }

    public static void zzdi(String str) {
        aFT.zzdi(str);
    }
}
