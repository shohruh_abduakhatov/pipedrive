package com.google.android.gms.measurement.internal;

abstract class zzaa extends zzz {
    private boolean cR;

    zzaa(zzx com_google_android_gms_measurement_internal_zzx) {
        super(com_google_android_gms_measurement_internal_zzx);
        this.aqw.zzb(this);
    }

    public final void initialize() {
        if (this.cR) {
            throw new IllegalStateException("Can't initialize twice");
        }
        zzzy();
        this.aqw.zzbyi();
        this.cR = true;
    }

    boolean isInitialized() {
        return this.cR;
    }

    protected void zzacj() {
        if (!isInitialized()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    boolean zzbyn() {
        return false;
    }

    protected abstract void zzzy();
}
