package com.pipedrive.sync;

import com.pipedrive.application.Session;
import com.pipedrive.tasks.authorization.OnEnoughDataToShowUI;
import java.util.concurrent.atomic.AtomicBoolean;

class SyncManager$5 implements Runnable {
    final AtomicBoolean called = new AtomicBoolean(false);
    final /* synthetic */ SyncManager this$0;
    final /* synthetic */ OnEnoughDataToShowUI val$onEnoughDataToShowUI;
    final /* synthetic */ Session val$session;

    SyncManager$5(SyncManager this$0, OnEnoughDataToShowUI onEnoughDataToShowUI, Session session) {
        this.this$0 = this$0;
        this.val$onEnoughDataToShowUI = onEnoughDataToShowUI;
        this.val$session = session;
    }

    public synchronized void run() {
        if (!this.called.getAndSet(true)) {
            if (this.val$onEnoughDataToShowUI != null) {
                this.val$onEnoughDataToShowUI.readyToShowUI(this.val$session);
            }
        }
    }
}
