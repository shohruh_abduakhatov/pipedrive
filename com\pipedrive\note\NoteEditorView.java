package com.pipedrive.note;

import android.support.annotation.MainThread;

@MainThread
interface NoteEditorView extends HtmlEditorView {
    void onNoteCreated(boolean z);

    void onNoteDeleted(boolean z);

    void onNoteUpdated(boolean z);
}
