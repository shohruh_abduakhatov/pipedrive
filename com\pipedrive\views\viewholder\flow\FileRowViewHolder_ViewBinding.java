package com.pipedrive.views.viewholder.flow;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class FileRowViewHolder_ViewBinding implements Unbinder {
    private FileRowViewHolder target;

    @UiThread
    public FileRowViewHolder_ViewBinding(FileRowViewHolder target, View source) {
        this.target = target;
        target.mFileName = (TextView) Utils.findRequiredViewAsType(source, R.id.fileName, "field 'mFileName'", TextView.class);
        target.mDetails = (TextView) Utils.findRequiredViewAsType(source, R.id.details, "field 'mDetails'", TextView.class);
        target.progressbarFileUpload = Utils.findRequiredView(source, R.id.progressbarFileUpload, "field 'progressbarFileUpload'");
    }

    @CallSuper
    public void unbind() {
        FileRowViewHolder target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mFileName = null;
        target.mDetails = null;
        target.progressbarFileUpload = null;
    }
}
