package com.google.android.gms.internal;

import android.support.annotation.NonNull;
import com.google.firebase.FirebaseException;

public class zzanu extends FirebaseException {
    public zzanu(@NonNull String str) {
        super(str);
    }
}
