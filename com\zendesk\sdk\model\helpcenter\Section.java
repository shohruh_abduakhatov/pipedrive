package com.zendesk.sdk.model.helpcenter;

import android.support.annotation.Nullable;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.Date;

public class Section implements Serializable {
    @SerializedName("article_count")
    private int articlesCount;
    private Long categoryId;
    private Date createdAt;
    private String description;
    private String htmlUrl;
    private Long id;
    private String locale;
    private String name;
    private boolean outdated;
    private int position;
    private String sorting;
    private String sourceLocale;
    private Date updatedAt;
    private String url;

    @Nullable
    public Long getId() {
        return this.id;
    }

    @Nullable
    public String getUrl() {
        return this.url;
    }

    @Nullable
    public String getHtmlUrl() {
        return this.htmlUrl;
    }

    @Nullable
    public Long getCategoryId() {
        return this.categoryId;
    }

    public int getPosition() {
        return this.position;
    }

    @Nullable
    public String getSorting() {
        return this.sorting;
    }

    @Nullable
    public Date getCreatedAt() {
        return this.createdAt == null ? null : new Date(this.createdAt.getTime());
    }

    public Date getUpdatedAt() {
        return this.updatedAt == null ? null : new Date(this.updatedAt.getTime());
    }

    @Nullable
    public String getName() {
        return this.name;
    }

    @Nullable
    public String getDescription() {
        return this.description;
    }

    @Nullable
    public String getLocale() {
        return this.locale;
    }

    @Nullable
    public String getSourceLocale() {
        return this.sourceLocale;
    }

    public boolean isOutdated() {
        return this.outdated;
    }

    public int getArticlesCount() {
        return this.articlesCount;
    }
}
