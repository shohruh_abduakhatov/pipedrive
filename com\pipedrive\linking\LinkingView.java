package com.pipedrive.linking;

import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import com.pipedrive.adapter.BaseSectionedListAdapter;

interface LinkingView {
    @Nullable
    BaseSectionedListAdapter getAdapter();

    @Nullable
    Cursor getCursor();

    @Nullable
    Drawable getFabIcon();

    @Nullable
    String getKey();

    @StringRes
    int getSearchHint();
}
