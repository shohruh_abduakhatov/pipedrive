package com.pipedrive.organization.edit;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import com.pipedrive.model.Organization;
import com.pipedrive.navigator.Navigator.Type;
import java.util.List;

public class OrganizationCreateActivity extends OrganizationActivity {
    public /* bridge */ /* synthetic */ void createOrUpdateDiscardedAsRequiredFieldsAreNotSet() {
        super.createOrUpdateDiscardedAsRequiredFieldsAreNotSet();
    }

    @Nullable
    public /* bridge */ /* synthetic */ List getAdditionalButtons() {
        return super.getAdditionalButtons();
    }

    public /* bridge */ /* synthetic */ boolean isAllRequiredFieldsSet() {
        return super.isAllRequiredFieldsSet();
    }

    public /* bridge */ /* synthetic */ boolean isChangesMadeToFieldsAfterInitialLoad() {
        return super.isChangesMadeToFieldsAfterInitialLoad();
    }

    public /* bridge */ /* synthetic */ void onCancel() {
        super.onCancel();
    }

    public /* bridge */ /* synthetic */ void onCreateOrUpdate() {
        super.onCreateOrUpdate();
    }

    public /* bridge */ /* synthetic */ void onOrganizationCreated(boolean z) {
        super.onOrganizationCreated(z);
    }

    public /* bridge */ /* synthetic */ void onOrganizationDeleted(boolean z) {
        super.onOrganizationDeleted(z);
    }

    public /* bridge */ /* synthetic */ void onOrganizationUpdated(boolean z) {
        super.onOrganizationUpdated(z);
    }

    public /* bridge */ /* synthetic */ void onResume() {
        super.onResume();
    }

    @MainThread
    public static void startActivity(@NonNull Activity activity) {
        ContextCompat.startActivity(activity, new Intent(activity, OrganizationCreateActivity.class), null);
    }

    public void onRequestOrganization(@Nullable Organization organization) {
        setNavigatorType(Type.CREATE);
        super.onRequestOrganization(organization);
    }
}
