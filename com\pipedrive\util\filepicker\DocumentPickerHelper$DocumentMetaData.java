package com.pipedrive.util.filepicker;

import android.support.annotation.Nullable;

class DocumentPickerHelper$DocumentMetaData {
    @Nullable
    String documentName;
    @Nullable
    Long fileSizeInBytes;
    @Nullable
    String fileType;

    DocumentPickerHelper$DocumentMetaData() {
    }

    DocumentPickerHelper$DocumentMetaData(@Nullable String documentName, @Nullable Long fileSizeInBytes, @Nullable String fileType) {
        this.documentName = documentName;
        this.fileSizeInBytes = fileSizeInBytes;
        this.fileType = fileType;
    }
}
