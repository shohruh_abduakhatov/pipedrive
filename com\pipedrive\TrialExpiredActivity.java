package com.pipedrive;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.view.View;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.CompaniesDataSource;
import com.pipedrive.dialogs.ChangeCompanyDialogFragment;
import com.pipedrive.model.settings.Company;
import java.util.List;

public class TrialExpiredActivity extends BaseActivity {
    @Nullable
    private List<Company> mCompanyList;

    public static void startActivity(@NonNull Session session) {
        if (!(!session.equals(PipedriveApp.getActiveSession()))) {
            session.getApplicationContext().startActivity(IntentCompat.makeRestartActivityTask(new ComponentName(session.getApplicationContext(), TrialExpiredActivity.class)));
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_trial_expired);
        ButterKnife.bind((Activity) this);
        if (PipedriveApp.getSessionManager().hasActiveSession()) {
            boolean hideChangeCompanyButton;
            int i;
            this.mCompanyList = new CompaniesDataSource(getSession()).findAll();
            if (this.mCompanyList.size() < 2) {
                hideChangeCompanyButton = true;
            } else {
                hideChangeCompanyButton = false;
            }
            View findById = ButterKnife.findById((Activity) this, (int) R.id.change_company);
            if (hideChangeCompanyButton) {
                i = 4;
            } else {
                i = 0;
            }
            findById.setVisibility(i);
            long selectedCompanyId = getSession().getSelectedCompanyID();
            String selectedCompanyName = "";
            for (Company company : this.mCompanyList) {
                if (((long) company.getPipedriveId()) == selectedCompanyId) {
                    selectedCompanyName = company.getName();
                }
            }
            ((TextView) ButterKnife.findById((Activity) this, (int) R.id.trial_expired_message)).setText(getString(R.string.lbl_trial_expired_text, new Object[]{selectedCompanyName}));
        }
    }

    @OnClick({2131820791})
    void onSignOutClicked() {
        LoginActivity.logOut(this, true);
    }

    @OnClick({2131820835})
    void onChangeCompanyClicked() {
        if (this.mCompanyList != null && this.mCompanyList.size() >= 2) {
            ChangeCompanyDialogFragment.show(getSupportFragmentManager());
        }
    }

    @SuppressLint({"MissingSuperCall"})
    public void onBackPressed() {
    }
}
