package com.pipedrive.views.hinted;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.view.ViewStub;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class BaseHintedLayout_ViewBinding implements Unbinder {
    private BaseHintedLayout target;

    @UiThread
    public BaseHintedLayout_ViewBinding(BaseHintedLayout target) {
        this(target, target);
    }

    @UiThread
    public BaseHintedLayout_ViewBinding(BaseHintedLayout target, View source) {
        this.target = target;
        target.mHint = (TextView) Utils.findRequiredViewAsType(source, R.id.hint, "field 'mHint'", TextView.class);
        target.mViewStub = (ViewStub) Utils.findRequiredViewAsType(source, R.id.stub, "field 'mViewStub'", ViewStub.class);
        target.mUnderline = Utils.findRequiredView(source, R.id.underline, "field 'mUnderline'");
    }

    @CallSuper
    public void unbind() {
        BaseHintedLayout target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mHint = null;
        target.mViewStub = null;
        target.mUnderline = null;
    }
}
