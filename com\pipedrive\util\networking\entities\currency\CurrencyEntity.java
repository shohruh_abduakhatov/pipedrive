package com.pipedrive.util.networking.entities.currency;

import android.support.annotation.Nullable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrencyEntity {
    @Nullable
    @SerializedName("code")
    @Expose
    private String code;
    @Nullable
    @SerializedName("decimal_points")
    @Expose
    private Integer decimalPoints;
    @Nullable
    @SerializedName("active_flag")
    @Expose
    private Boolean isActive;
    @Nullable
    @SerializedName("is_custom_flag")
    @Expose
    private Boolean isCustom;
    @Nullable
    @SerializedName("name")
    @Expose
    private String name;
    @Nullable
    @SerializedName("id")
    @Expose
    private Long pipedriveId;
    @Nullable
    @SerializedName("symbol")
    @Expose
    private String symbol;

    @Nullable
    public Long getPipedriveId() {
        return this.pipedriveId;
    }

    @Nullable
    public String getCode() {
        return this.code;
    }

    @Nullable
    public String getName() {
        return this.name;
    }

    @Nullable
    public String getSymbol() {
        return this.symbol;
    }

    @Nullable
    public Integer getDecimalPoints() {
        return this.decimalPoints;
    }

    @Nullable
    public Boolean isActive() {
        return this.isActive;
    }

    @Nullable
    public Boolean isCustom() {
        return this.isCustom;
    }

    public String toString() {
        return "CurrencyEntity{pipedriveId=" + this.pipedriveId + ", code='" + this.code + '\'' + ", name='" + this.name + '\'' + ", symbol='" + this.symbol + '\'' + ", decimalPoints=" + this.decimalPoints + ", isActive=" + this.isActive + ", isCustom=" + this.isCustom + '}';
    }
}
