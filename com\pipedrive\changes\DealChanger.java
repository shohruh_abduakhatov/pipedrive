package com.pipedrive.changes;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.changes.Change.ChangeOperationType;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Deal;

public class DealChanger extends Changer<Deal> {
    public DealChanger(Session session) {
        super(session, 30, null);
    }

    protected DealChanger(Changer parentChanger) {
        super(parentChanger.getSession(), 30, parentChanger);
    }

    @Nullable
    protected Long getChangeMetaData(@NonNull Deal deal, @NonNull ChangeOperationType changeOperationType) {
        return Long.valueOf(deal.getSqlId());
    }

    @NonNull
    protected ChangeResult createChange(@NonNull Deal newDeal) {
        if (newDeal.isExisting() || newDeal.isStored()) {
            LogJourno.reportEvent(EVENT.ChangesChanger_createChangeRequestedWithExistingChange, DealChanger.class.getSimpleName());
            Log.e(new Throwable("I can only handle new Deals!"));
            return ChangeResult.FAILED;
        }
        createAndRelateNewOrgChangeIfNewOrgExists(newDeal);
        createAndRelateNewPersonChangeIfNewPersonExists(newDeal);
        long newDealSqlId = new DealsDataSource(getSession().getDatabase()).createOrUpdate((BaseDatasourceEntity) newDeal);
        if (newDealSqlId == -1) {
            return ChangeResult.FAILED;
        }
        newDeal.setSqlId(newDealSqlId);
        return ChangeResult.SUCCESSFUL;
    }

    @NonNull
    protected ChangeResult updateChange(@NonNull Deal existingDeal) {
        DealsDataSource dealsDataSource = new DealsDataSource(getSession().getDatabase());
        if (!existingDeal.isStored() || dealsDataSource.findBySqlId(existingDeal.getSqlId()) == null) {
            LogJourno.reportEvent(EVENT.ChangesChanger_updateChangeRequestedWithNewChange, DealChanger.class.getSimpleName());
            Log.e(new Throwable("I can only handle existing Deals!"));
            return ChangeResult.FAILED;
        }
        createAndRelateNewOrgChangeIfNewOrgExists(existingDeal);
        createAndRelateNewPersonChangeIfNewPersonExists(existingDeal);
        return dealsDataSource.createOrUpdate((BaseDatasourceEntity) existingDeal) == -1 ? ChangeResult.FAILED : ChangeResult.SUCCESSFUL;
    }

    private void createAndRelateNewPersonChangeIfNewPersonExists(Deal deal) {
        if (deal.getPerson() != null && !deal.getPerson().isStored() && !new PersonChanger((Changer) this).create(deal.getPerson())) {
            deal.setPerson(null);
        }
    }

    private void createAndRelateNewOrgChangeIfNewOrgExists(Deal deal) {
        if (deal.getOrganization() != null && !deal.getOrganization().isStored() && !new OrganizationChanger((Changer) this).create(deal.getOrganization())) {
            deal.setOrganization(null);
        }
    }
}
