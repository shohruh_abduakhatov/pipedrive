package com.pipedrive.application;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.pipedrive.application.SessionStoreHelper.SharedSession;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.util.networking.CryptoUtils;
import java.util.WeakHashMap;

public final class SessionManager {
    private static final String PREFS_ACTIVE_SESSION_ID = "SessionManager.PREFS__ACTIVE_SESSION_ID";
    private static final String TAG = SessionManager.class.getSimpleName();
    private static Session mActiveSessionCached = null;
    private static final WeakHashMap<Session, Void> mSessions = new WeakHashMap();
    private Context mApplicationContext;
    private final Object mLock = new Object();
    private SharedSession mSharedSession = null;

    SessionManager(Context ctx) {
        if (ctx != null) {
            this.mApplicationContext = ctx.getApplicationContext();
        }
        this.mSharedSession = new SharedSession(this.mApplicationContext);
    }

    @Nullable
    public Session getActiveSession() {
        String tag = TAG + ".getActiveSession()";
        if (this.mApplicationContext == null) {
            Log.e(new Throwable("We don't have context thus we cannot have active session!"));
            LogJourno.reportEvent(EVENT.Session_cannotReturnActiveSessionAsNoApplicationContextIsPresent);
            return null;
        }
        String noSessionFoundMarker = "_no_session_found_";
        String activeSessionID = SharedSession.getSessionPrefsString(this.mApplicationContext, PREFS_ACTIVE_SESSION_ID, "_no_session_found_");
        if (TextUtils.equals(activeSessionID, "_no_session_found_")) {
            Log.w(TAG, "We don't have an open session. Please create a new session!");
            return null;
        } else if (mActiveSessionCached == null || !mActiveSessionCached.getSessionID().equals(activeSessionID)) {
            Session ret;
            synchronized (this.mLock) {
                Session sessionFromActiveSessions = findActiveSession(activeSessionID);
                if (sessionFromActiveSessions != null) {
                    Log.d(tag, "Session found from active sessions. Caching and returning it.");
                    ret = setActiveSessionCached(sessionFromActiveSessions);
                } else {
                    Log.d(tag, "We have an open session but it was not found within active sessions. Lets create an active session.");
                    ret = createActiveSession(activeSessionID);
                }
            }
            return ret;
        } else {
            Log.v(tag, "Active session cache exists. Return it!");
            return mActiveSessionCached;
        }
    }

    protected SharedSession getSharedSession() {
        return this.mSharedSession;
    }

    public boolean hasActiveSession() {
        return getActiveSession() != null;
    }

    @Nullable
    public synchronized Session createActiveSession(long pipedriveUserID, long userCompanyID) {
        Session session;
        session = createActiveSession(getSessionId(pipedriveUserID, userCompanyID));
        if (session != null) {
            session.setAuthenticatedUserID(pipedriveUserID);
            session.setSelectedCompanyID(userCompanyID);
        }
        return session;
    }

    private String getSessionId(long pipedriveUserID, long userCompanyID) {
        return CryptoUtils.getMD5Hash(pipedriveUserID + "" + userCompanyID);
    }

    @Nullable
    public synchronized Session createActiveSession(long pipedriveUserID, long userCompanyID, @Nullable String apiToken, @Nullable String domain) {
        Session session;
        session = createActiveSession(pipedriveUserID, userCompanyID);
        if (session != null) {
            session.setApiToken(apiToken);
            session.setDomain(domain);
        }
        return session;
    }

    @Nullable
    private synchronized Session createActiveSession(String sessionID) {
        Session activeSessionCached;
        String tag = TAG + ".createActiveSession()";
        Session sessionFromActiveSessions = findActiveSession(sessionID);
        if (sessionFromActiveSessions != null) {
            Log.d(tag, "Session found from active sessions. Caching and returning it.");
            activeSessionCached = setActiveSessionCached(sessionFromActiveSessions);
        } else {
            Log.d(tag, "Creating a new active session with ID:" + sessionID);
            mSessions.put(new Session(sessionID, this.mApplicationContext), null);
            SharedSession.setSessionPrefsString(this.mApplicationContext, PREFS_ACTIVE_SESSION_ID, sessionID);
            activeSessionCached = getActiveSession();
        }
        return activeSessionCached;
    }

    public synchronized void releaseActiveSession() {
        String tag = TAG + ".releaseActiveSession()";
        setActiveSessionCached(null);
        Log.d(tag, "Session is killed. No active session!");
    }

    @Nullable
    public synchronized Session findActiveSession(@Nullable String sessionID) {
        Session session;
        for (Session session2 : mSessions.keySet()) {
            if (TextUtils.equals(sessionID, session2.getSessionID())) {
                break;
            }
        }
        session2 = null;
        return session2;
    }

    private Session setActiveSessionCached(Session activeSession) {
        String str;
        String tag = TAG + ".setActiveSessionCached()";
        Log.d(tag, "Setting an active session to: " + activeSession);
        mActiveSessionCached = activeSession;
        Context context = this.mApplicationContext;
        String str2 = PREFS_ACTIVE_SESSION_ID;
        if (activeSession == null) {
            str = null;
        } else {
            str = activeSession.getSessionID();
        }
        SharedSession.setSessionPrefsString(context, str2, str);
        Log.d(tag, "Active session check. Do we have active session now? - " + hasActiveSession());
        return mActiveSessionCached;
    }

    public synchronized void clearDatabaseAndDisposeSession(@Nullable String sessionId) {
        if (!TextUtils.isEmpty(sessionId)) {
            clearDatabaseAndDisposeSession(findActiveSession(sessionId));
        }
    }

    public synchronized void clearDatabaseAndDisposeSession(@Nullable Session session) {
        if (session != null) {
            session.clearDatabaseAndDisposeSession();
            mSessions.remove(session);
        }
    }

    @Nullable
    public synchronized Session getSessionForCompanyPipedriveId(@NonNull Long companyPipedriveId) {
        Session session;
        if (getActiveSession() == null) {
            session = null;
        } else {
            String sessionId = getSessionId(getActiveSession().getAuthenticatedUserID(), companyPipedriveId.longValue());
            session = findActiveSession(sessionId);
            if (session == null) {
                session = new Session(sessionId, this.mApplicationContext);
            }
        }
        return session;
    }
}
