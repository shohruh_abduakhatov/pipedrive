package com.pipedrive.contentproviders;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.model.Person;
import com.pipedrive.model.customfields.CustomField;

public class PersonsContentProvider extends BaseContentProvider {
    private static final String AUTHORITY = "com.pipedrive.contentproviders.contactscontentprovider";
    private static final String BASE_PATH = "contacts";
    public static final int NOT_DEFINED = 0;
    private static final int ORGANIZATION_PERSONS_ORG_SQL_ID = 50;
    private static final String ORGANIZATION_PERSONS_ORG_SQL_ID_PATH = "org_sql_id";
    private static final int PERSON_PD_ID = 30;
    private static final String PERSON_PD_ID_PATH = "pipedrive_id";
    private static final int PERSON_SQL_ID = 20;
    private static final UriMatcher URI_MATCHER = new UriMatcher(-1);

    @Nullable
    @Deprecated
    public /* bridge */ /* synthetic */ Uri attachSessionIdToUri_hackMethodToEnableCPUriBuildOutsideCP_BAD_DESIGN_AND_USAGE(Uri uri) {
        return super.attachSessionIdToUri_hackMethodToEnableCPUriBuildOutsideCP_BAD_DESIGN_AND_USAGE(uri);
    }

    public /* bridge */ /* synthetic */ int delete(@NonNull Uri uri, String str, String[] strArr, @NonNull SQLiteDatabase sQLiteDatabase) {
        return super.delete(uri, str, strArr, sQLiteDatabase);
    }

    @Nullable
    public /* bridge */ /* synthetic */ String getType(@NonNull Uri uri) {
        return super.getType(uri);
    }

    @Nullable
    public /* bridge */ /* synthetic */ Uri insert(@NonNull Uri uri, ContentValues contentValues, @NonNull SQLiteDatabase sQLiteDatabase) {
        return super.insert(uri, contentValues, sQLiteDatabase);
    }

    public /* bridge */ /* synthetic */ boolean onCreate() {
        return super.onCreate();
    }

    public /* bridge */ /* synthetic */ int update(@NonNull Uri uri, ContentValues contentValues, String str, String[] strArr, @NonNull SQLiteDatabase sQLiteDatabase) {
        return super.update(uri, contentValues, str, strArr, sQLiteDatabase);
    }

    static {
        URI_MATCHER.addURI("com.pipedrive.contentproviders.contactscontentprovider", "contacts/#", 20);
        URI_MATCHER.addURI("com.pipedrive.contentproviders.contactscontentprovider", "contacts/pipedrive_id/#", 30);
        URI_MATCHER.addURI("com.pipedrive.contentproviders.contactscontentprovider", "contacts/org_sql_id/#", 50);
    }

    public PersonsContentProvider(@NonNull Session session) {
        super(session);
    }

    @Nullable
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder, @NonNull SQLiteDatabase sqLiteDatabase) {
        Cursor cursor;
        PersonsDataSource dataSource = new PersonsDataSource(sqLiteDatabase);
        switch (URI_MATCHER.match(uri)) {
            case 20:
                cursor = dataSource.findBySqlIdCursor(ContentUris.parseId(uri));
                break;
            case 30:
                cursor = dataSource.findPersonByPipedriveIdCursor(ContentUris.parseId(uri));
                break;
            case 50:
                cursor = dataSource.getOrganizationPersonsCursor(ContentUris.parseId(uri));
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        if (!(cursor == null || getContext() == null)) {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return cursor;
    }

    @Nullable
    public Uri createGetPersonUriFromPipedriveId(int pipedriveId) {
        if (pipedriveId == 0) {
            return null;
        }
        Builder uriBuilder = new Builder();
        uriBuilder.scheme("content").authority("com.pipedrive.contentproviders.contactscontentprovider").appendPath(BASE_PATH);
        uriBuilder.appendPath(PERSON_PD_ID_PATH);
        ContentUris.appendId(uriBuilder, (long) pipedriveId);
        return attachSessionIdToUri(uriBuilder.build());
    }

    @Nullable
    public Uri createGetPersonUriFromDatabaseID(long dbId) {
        if (dbId <= 0) {
            return null;
        }
        Builder uriBuilder = new Builder();
        uriBuilder.scheme("content").authority("com.pipedrive.contentproviders.contactscontentprovider").appendPath(BASE_PATH);
        ContentUris.appendId(uriBuilder, dbId);
        return attachSessionIdToUri(uriBuilder.build());
    }

    @Nullable
    public Uri createOrganizationPersonsUri(long orgSqlId) {
        Builder uriBuilder = new Builder();
        uriBuilder.scheme("content").authority("com.pipedrive.contentproviders.contactscontentprovider").appendPath(BASE_PATH);
        uriBuilder.appendPath(ORGANIZATION_PERSONS_ORG_SQL_ID_PATH);
        ContentUris.appendId(uriBuilder, orgSqlId);
        return attachSessionIdToUri(uriBuilder.build());
    }

    @Nullable
    public static Person getPersonDataFromUri(@NonNull ContentResolver resolver, @Nullable Uri uriFromPersonsContentProvider) throws IllegalStateException {
        Person person = null;
        if (uriFromPersonsContentProvider != null) {
            Session session = PipedriveApp.getActiveSession();
            if (session != null) {
                Cursor cursor = resolver.query(uriFromPersonsContentProvider, null, null, null, null);
                if (cursor != null) {
                    try {
                        cursor.moveToFirst();
                        if (!cursor.isAfterLast()) {
                            person = new PersonsDataSource(session.getDatabase()).deflateCursor(cursor);
                        } else {
                            cursor.close();
                        }
                    } finally {
                        cursor.close();
                    }
                }
            }
        }
        return person;
    }

    public static Uri getPersonUriFromCustomField(CustomField field, Session session, int id) {
        if (field.getTempValue().startsWith(CustomField.LOCAL_DATABASE_ID_PREFIX)) {
            return new PersonsContentProvider(session).createGetPersonUriFromDatabaseID((long) id);
        }
        return new PersonsContentProvider(session).createGetPersonUriFromPipedriveId(id);
    }
}
