package com.pipedrive.tasks.flow;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.pagination.Pagination;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished;
import com.pipedrive.tasks.AsyncTaskWithCallback.OnTaskFinished.TaskResult;
import com.pipedrive.tasks.StoreAwareAsyncTaskWithCallback;
import com.pipedrive.util.FlowNetworkingUtil;
import com.pipedrive.util.networking.entities.FlowResponse;
import java.io.IOException;

public abstract class DownloadFlowTask extends StoreAwareAsyncTaskWithCallback<Integer, Void> {

    public static final class DownloadDealFlowTask extends DownloadFlowTask {
        public DownloadDealFlowTask(Session session, OnTaskFinished<Integer> onTaskFinished) {
            super(session, onTaskFinished);
        }

        protected TaskResult doInBackgroundAfterStoreSyncWithCallback(Integer... params) {
            if (!isSane(params)) {
                return TaskResult.FAILED;
            }
            String apiEndpoint = "deals";
            return doWork(params[0], "deals");
        }
    }

    public static final class DownloadOrganizationFlowTask extends DownloadFlowTask {
        public DownloadOrganizationFlowTask(Session session, OnTaskFinished<Integer> onTaskFinished) {
            super(session, onTaskFinished);
        }

        protected TaskResult doInBackgroundAfterStoreSyncWithCallback(Integer... params) {
            if (!isSane(params)) {
                return TaskResult.FAILED;
            }
            String apiEndpoint = "organizations";
            return doWork(params[0], "organizations");
        }
    }

    public static final class DownloadPersonFlowTask extends DownloadFlowTask {
        public DownloadPersonFlowTask(Session session, OnTaskFinished<Integer> onTaskFinished) {
            super(session, onTaskFinished);
        }

        protected TaskResult doInBackgroundAfterStoreSyncWithCallback(Integer... params) {
            if (!isSane(params)) {
                return TaskResult.FAILED;
            }
            String apiEndpoint = "persons";
            return doWork(params[0], "persons");
        }
    }

    private DownloadFlowTask(Session session, OnTaskFinished<Integer> onTaskFinished) {
        super(session, onTaskFinished);
    }

    boolean isSane(Integer... params) {
        return params.length > 0 && params[0].intValue() > 0;
    }

    @NonNull
    TaskResult doWork(Integer pipedriveId, String apiEndpoint) {
        Long start = null;
        Long limit = null;
        while (true) {
            try {
                FlowResponse flowResponse = FlowNetworkingUtil.downloadAndStoreFlowData(getSession(), apiEndpoint, (long) pipedriveId.intValue(), start, limit);
                if (isCancelled()) {
                    return TaskResult.SUCCEEDED;
                }
                if (flowResponse == null) {
                    return TaskResult.FAILED;
                }
                Pagination pagination = flowResponse.getPagination();
                if (pagination == null) {
                    return TaskResult.FAILED;
                }
                if (!pagination.moreItemsInCollection) {
                    return TaskResult.SUCCEEDED;
                }
                start = Long.valueOf((long) pagination.nextStart);
                limit = Long.valueOf((long) pagination.limit);
            } catch (IOException e) {
                int i = -1;
                switch (apiEndpoint.hashCode()) {
                    case -2108114528:
                        if (apiEndpoint.equals("organizations")) {
                            i = 2;
                            break;
                        }
                        break;
                    case -678441026:
                        if (apiEndpoint.equals("persons")) {
                            i = 1;
                            break;
                        }
                        break;
                    case 95457671:
                        if (apiEndpoint.equals("deals")) {
                            i = 0;
                            break;
                        }
                        break;
                }
                switch (i) {
                    case 0:
                        LogJourno.reportEvent(EVENT.Networking_downloadFlowForDealFailed, String.format("pipedriveId:[%s]", new Object[]{pipedriveId}), e);
                        Log.e(new Throwable("Exception caught during downloading flow for deal " + pipedriveId, e));
                        break;
                    case 1:
                        LogJourno.reportEvent(EVENT.Networking_downloadFlowForPersonFailed, String.format("personPipedriveId:[%s]", new Object[]{pipedriveId}), e);
                        Log.e(new Throwable("Exception caught during downloading flow for person " + pipedriveId, e));
                        break;
                    case 2:
                        LogJourno.reportEvent(EVENT.Networking_downloadFlowForOrganizationFailed, String.format("organizationPipedriveId:[%s]", new Object[]{pipedriveId}), e);
                        Log.e(new Throwable("Exception caught during downloading flow for organization " + pipedriveId, e));
                        break;
                }
                return TaskResult.FAILED;
            }
        }
    }
}
