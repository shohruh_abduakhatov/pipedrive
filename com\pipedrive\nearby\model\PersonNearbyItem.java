package com.pipedrive.nearby.model;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.BaseDataSource;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.model.Person;
import java.util.List;

public final class PersonNearbyItem extends DealRelatedNearbyItem<Person> {
    public PersonNearbyItem(@NonNull Double latitude, @NonNull Double longitude, @NonNull String address, @NonNull Long singleItemSqlId) {
        super(latitude, longitude, address, singleItemSqlId);
    }

    public PersonNearbyItem(@NonNull Double latitude, @NonNull Double longitude, @NonNull String address, @NonNull List<Long> multipleItemSqlIds) {
        super(latitude, longitude, address, (List) multipleItemSqlIds);
    }

    @NonNull
    protected String getColumnForOpenDealsCount() {
        return PipeSQLiteHelper.COLUMN_DEALS_PERSON_SQL_ID;
    }

    @NonNull
    protected final String getActivitiesColumnForNearbyItem() {
        return PipeSQLiteHelper.COLUMN_ACTIVITIES_PERSON_ID_SQL;
    }

    @NonNull
    protected final BaseDataSource<Person> getDataSource(@NonNull Session session) {
        return new PersonsDataSource(session.getDatabase());
    }
}
