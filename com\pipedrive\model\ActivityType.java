package com.pipedrive.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.firebase.analytics.FirebaseAnalytics.Event;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.newrelic.agent.android.instrumentation.JSONObjectInstrumentation;
import com.pipedrive.R;
import com.pipedrive.adapter.ImageAndTextSpinnerAdapter.ImageAndTextSpinnerAdapterData;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.tasks.activities.ActivityTypesTask;
import com.pipedrive.util.activities.ActivityNetworkingUtil;
import com.pipedrive.util.networking.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class ActivityType implements Parcelable, ImageAndTextSpinnerAdapterData {
    public static final String ACTIVITY_TYPE_NAME_FOR_CALL = "call";
    public static final Creator CREATOR = new Creator() {
        public ActivityType createFromParcel(Parcel in) {
            return new ActivityType(in);
        }

        public ActivityType[] newArray(int size) {
            return new ActivityType[size];
        }
    };
    private static final boolean DEFAULT_ACTIVE_FLAG_IF_NOT_FOUND_IN_JSON = false;
    private static final String DEFAULT_ACTIVITY_ICON_KEY = "task";
    private static final int DEFAULT_INDEX = 0;
    private static final int DEFAULT_ORDER_NR_IF_NOT_FOUND_IN_JSON = 0;
    private static final Map<String, Integer> ICONS = new HashMap<String, Integer>() {
        {
            put("call", Integer.valueOf(R.drawable.activity_call));
            put("addressbook", Integer.valueOf(R.drawable.activity_addressbook));
            put("bell", Integer.valueOf(R.drawable.activity_bell));
            put("brush", Integer.valueOf(R.drawable.activity_brush));
            put("bubble", Integer.valueOf(R.drawable.activity_bubble));
            put("bulb", Integer.valueOf(R.drawable.activity_bulb));
            put("calendar", Integer.valueOf(R.drawable.activity_calendar));
            put("camera", Integer.valueOf(R.drawable.activity_camera));
            put("car", Integer.valueOf(R.drawable.activity_car));
            put("cart", Integer.valueOf(R.drawable.activity_cart));
            put("checkbox", Integer.valueOf(R.drawable.activity_checkbox));
            put("clip", Integer.valueOf(R.drawable.activity_clip));
            put("cogs", Integer.valueOf(R.drawable.activity_cogs));
            put("deadline", Integer.valueOf(R.drawable.activity_deadline));
            put("document", Integer.valueOf(R.drawable.activity_document));
            put("downarrow", Integer.valueOf(R.drawable.activity_downarrow));
            put("email", Integer.valueOf(R.drawable.activity_email));
            put("finish", Integer.valueOf(R.drawable.activity_finish));
            put("key", Integer.valueOf(R.drawable.activity_key));
            put("linegraph", Integer.valueOf(R.drawable.activity_linegraph));
            put("loop", Integer.valueOf(R.drawable.activity_loop));
            put("lunch", Integer.valueOf(R.drawable.activity_lunch));
            put("meeting", Integer.valueOf(R.drawable.activity_meeting));
            put("padlock", Integer.valueOf(R.drawable.activity_padlock));
            put("picture", Integer.valueOf(R.drawable.activity_picture));
            put("plane", Integer.valueOf(R.drawable.activity_plane));
            put("presentation", Integer.valueOf(R.drawable.activity_presentation));
            put("pricetag", Integer.valueOf(R.drawable.activity_pricetag));
            put("scissors", Integer.valueOf(R.drawable.activity_scissors));
            put(Event.SEARCH, Integer.valueOf(R.drawable.activity_search));
            put("shuffle", Integer.valueOf(R.drawable.activity_shuffle));
            put("signpost", Integer.valueOf(R.drawable.activity_signpost));
            put("smartphone", Integer.valueOf(R.drawable.activity_smartphone));
            put("sound", Integer.valueOf(R.drawable.activity_sound));
            put("suitcase", Integer.valueOf(R.drawable.activity_suitcase));
            put(ActivityType.DEFAULT_ACTIVITY_ICON_KEY, Integer.valueOf(R.drawable.activity_task));
            put("truck", Integer.valueOf(R.drawable.activity_truck));
            put("uparrow", Integer.valueOf(R.drawable.activity_uparrow));
            put("wifi", Integer.valueOf(R.drawable.activity_wifi));
            put("world", Integer.valueOf(R.drawable.activity_world));
        }
    };
    private static final int ID_NOT_FOUND_IN_JSON = -1;
    @SerializedName("active_flag")
    @Expose
    private boolean activeFlag;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("icon_key")
    @Expose
    private String iconKey;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("is_custom_flag")
    @Expose
    private boolean isCustomFlag;
    @SerializedName("key_string")
    @Expose
    private String keyString;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("order_nr")
    @Expose
    private int orderNr;

    private ActivityType() {
    }

    private ActivityType(Parcel in) {
        readFromParcel(in);
    }

    public ActivityType(int id, int orderNr, String name) {
        this.id = id;
        this.orderNr = orderNr;
        this.name = name;
    }

    public int getId() {
        return this.id;
    }

    private void setId(int id) {
        this.id = id;
    }

    public int getOrderNr() {
        return this.orderNr;
    }

    private void setOrderNr(int orderNr) {
        this.orderNr = orderNr;
    }

    @NonNull
    public String getName() {
        if (this.name == null) {
            this.name = "";
        }
        return this.name;
    }

    @NonNull
    public String getLabel() {
        return getName();
    }

    private void setName(String name) {
        this.name = name;
    }

    @Nullable
    public String getKeyString() {
        return this.keyString;
    }

    private void setKeyString(String keyString) {
        this.keyString = keyString;
    }

    public boolean isActiveFlag() {
        return this.activeFlag;
    }

    private void setActiveFlag(boolean activeFlag) {
        this.activeFlag = activeFlag;
    }

    @Nullable
    public String getColor() {
        return this.color;
    }

    private void setColor(String color) {
        this.color = color;
    }

    public boolean isCustomFlag() {
        return this.isCustomFlag;
    }

    private void setCustomFlag(boolean isCustomFlag) {
        this.isCustomFlag = isCustomFlag;
    }

    @Nullable
    private String getIconKey() {
        return this.iconKey;
    }

    private void setIconKey(String iconKey) {
        this.iconKey = iconKey;
    }

    @NonNull
    public static ActivityType getActivityTypeByName(@NonNull Session session, @Nullable String name) {
        if (name != null) {
            for (ActivityType activityType : ActivityNetworkingUtil.loadActivityTypesList(session, false, 0)) {
                if (activityType.getName().equalsIgnoreCase(name)) {
                    return activityType;
                }
                if (activityType.getKeyString() != null && activityType.getKeyString().equalsIgnoreCase(name)) {
                    return activityType;
                }
            }
        }
        return new ActivityType();
    }

    @Nullable
    public static ActivityType getActivityTypeById(@NonNull Session session, @IntRange(from = 0) int id) {
        if (id > 0) {
            for (ActivityType activityType : ActivityNetworkingUtil.loadActivityTypesList(session, false, 0)) {
                if (activityType.getId() == id) {
                    return activityType;
                }
            }
        }
        return null;
    }

    @IntRange(from = 0)
    public static int getActivityTypeIndexByLabelName(@NonNull Session session, boolean loadOnlyActive, @Nullable String labelName, int originalActivityTypeId) {
        if (labelName == null) {
            return 0;
        }
        int index = getActivityTypesLabels(session, loadOnlyActive, originalActivityTypeId).indexOf(labelName);
        if (index == -1) {
            return 0;
        }
        return index;
    }

    @NonNull
    public static List<String> getActivityTypesLabels(@NonNull Session session, boolean loadOnlyActive, int currentActivityTypeID) {
        List<String> labels = new ArrayList();
        List<ActivityType> activityTypes = ActivityNetworkingUtil.loadActivityTypesList(session, loadOnlyActive, currentActivityTypeID);
        for (int i = 0; i < activityTypes.size(); i++) {
            labels.add(((ActivityType) activityTypes.get(i)).getName());
        }
        return labels;
    }

    @NonNull
    public static ActivityType getDefaultActivityType(@NonNull Session session) {
        List<ActivityType> activityTypes = ActivityNetworkingUtil.loadActivityTypesList(session, true, 0);
        if (activityTypes == null || activityTypes.isEmpty()) {
            return new ActivityType();
        }
        return (ActivityType) activityTypes.get(0);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        int i;
        int i2 = 1;
        dest.writeInt(this.id);
        dest.writeInt(this.orderNr);
        dest.writeString(this.name);
        dest.writeString(this.keyString);
        dest.writeString(this.iconKey);
        if (this.activeFlag) {
            i = 1;
        } else {
            i = 0;
        }
        dest.writeInt(i);
        dest.writeString(this.color);
        if (!this.isCustomFlag) {
            i2 = 0;
        }
        dest.writeInt(i2);
    }

    private void readFromParcel(Parcel in) {
        boolean z;
        boolean z2 = true;
        this.id = in.readInt();
        this.orderNr = in.readInt();
        this.name = in.readString();
        this.keyString = in.readString();
        this.iconKey = in.readString();
        if (in.readInt() == 1) {
            z = true;
        } else {
            z = false;
        }
        this.activeFlag = z;
        this.color = in.readString();
        if (in.readInt() != 1) {
            z2 = false;
        }
        this.isCustomFlag = z2;
    }

    @NonNull
    public static List<ActivityType> readActivityTypes(@NonNull Session session) {
        List<ActivityType> activityTypes = new ArrayList();
        if (session.getRuntimeCache().allActivityTypes != null) {
            return session.getRuntimeCache().allActivityTypes;
        }
        String activityTypesString = session.getActivityTypes();
        if (!activityTypesString.equalsIgnoreCase(Session.PREFS_NOT_SET_STRING) || session.getApplicationContext() == null) {
            List<ActivityType> customActivityTypes = new ArrayList();
            try {
                JSONArray activityTypesArr = JSONObjectInstrumentation.init(activityTypesString).optJSONArray(Response.JSON_PARAM_DATA);
                if (activityTypesArr != null) {
                    int newContactsArrLength = activityTypesArr.length();
                    for (int i = 0; i < newContactsArrLength; i++) {
                        JSONObject activityTypeJson = activityTypesArr.optJSONObject(i);
                        ActivityType type = readActivityType(!(activityTypeJson instanceof JSONObject) ? activityTypeJson.toString() : JSONObjectInstrumentation.toString(activityTypeJson));
                        if (type != null) {
                            if (type.isCustomFlag()) {
                                customActivityTypes.add(type);
                            } else {
                                activityTypes.add(type);
                            }
                        }
                    }
                    activityTypes.addAll(customActivityTypes);
                }
            } catch (Exception e) {
                Log.e(new Throwable("Error loading activity types from preferences", e));
            }
            session.getRuntimeCache().allActivityTypes = activityTypes;
            return activityTypes;
        }
        new ActivityTypesTask(session).execute(new String[0]);
        return activityTypes;
    }

    @Nullable
    private static ActivityType readActivityType(String activityTypeJSON) {
        try {
            JSONObject activityTypeJson = JSONObjectInstrumentation.init(activityTypeJSON);
            ActivityType type = new ActivityType();
            type.setId(activityTypeJson.optInt(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, -1));
            type.setOrderNr(activityTypeJson.optInt(PipeSQLiteHelper.COLUMN_ORDER_NR, 0));
            type.setName(activityTypeJson.optString("name"));
            if (!activityTypeJson.isNull("key_string")) {
                type.setKeyString(activityTypeJson.optString("key_string"));
            }
            if (!activityTypeJson.isNull("icon_key")) {
                type.setIconKey(activityTypeJson.optString("icon_key"));
            }
            type.setActiveFlag(activityTypeJson.optBoolean("active_flag", false));
            if (!activityTypeJson.isNull("color")) {
                type.setColor(activityTypeJson.optString("color"));
            }
            type.setCustomFlag(activityTypeJson.optBoolean("is_custom_flag"));
            return type;
        } catch (Exception e) {
            Log.e(new Throwable(String.format("Error parsing activity type from [%s]", new Object[]{activityTypeJSON}), e));
            LogJourno.reportEvent(EVENT.NotSpecified_activityTypeParsingFailed, String.format("from:[%s]", new Object[]{activityTypeJSON}), e);
            return null;
        }
    }

    public int getImageResourceId() {
        String key = TextUtils.isEmpty(this.iconKey) ? TextUtils.isEmpty(this.keyString) ? DEFAULT_ACTIVITY_ICON_KEY : this.keyString : this.iconKey;
        Integer iconResId = (Integer) ICONS.get(key);
        return iconResId == null ? R.drawable.activity_task : iconResId.intValue();
    }

    @NonNull
    public static String getJSONString(List<ActivityType> activityTypes) {
        Gson jsonBuilder = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
        JsonObject activityTypesJSON = new JsonObject();
        activityTypesJSON.addProperty(Response.JSON_PARAM_SUCCESS, Boolean.valueOf(true));
        activityTypesJSON.add(Response.JSON_PARAM_DATA, jsonBuilder.toJsonTree(activityTypes));
        return activityTypesJSON.toString();
    }

    public String toString() {
        return String.format("[id=%s; name=%s; keyString=%s; iconKey=%s; isCustomFlag=%s]", new Object[]{Integer.valueOf(getId()), getName(), getKeyString(), getIconKey(), Boolean.valueOf(isCustomFlag())});
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(Object o) {
        boolean z = true;
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ActivityType that = (ActivityType) o;
        if (this.id != that.id || this.orderNr != that.orderNr || this.activeFlag != that.activeFlag || this.isCustomFlag != that.isCustomFlag) {
            return false;
        }
        if (this.name != null) {
            if (!this.name.equals(that.name)) {
                return false;
            }
        } else if (that.name != null) {
            return false;
        }
        if (this.keyString != null) {
            if (!this.keyString.equals(that.keyString)) {
                return false;
            }
        } else if (that.keyString != null) {
            return false;
        }
        if (this.iconKey != null) {
            if (!this.iconKey.equals(that.iconKey)) {
                return false;
            }
        } else if (that.iconKey != null) {
            return false;
        }
        if (this.color == null) {
            if (that.color == null) {
                return z;
            }
        }
        z = false;
        return z;
    }

    public int hashCode() {
        int hashCode;
        int i = 1;
        int hashCode2 = ((((this.id * 31) + this.orderNr) * 31) + (this.name != null ? this.name.hashCode() : 0)) * 31;
        if (this.keyString != null) {
            hashCode = this.keyString.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 31;
        if (this.iconKey != null) {
            hashCode = this.iconKey.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 31;
        if (this.activeFlag) {
            hashCode = 1;
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 31;
        if (this.color != null) {
            hashCode = this.color.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 31;
        if (!this.isCustomFlag) {
            i = 0;
        }
        return hashCode + i;
    }
}
