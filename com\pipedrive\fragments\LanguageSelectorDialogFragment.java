package com.pipedrive.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog.Builder;
import com.pipedrive.R;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.AnalyticsEvent;

public class LanguageSelectorDialogFragment extends DialogFragment implements OnClickListener {
    public static final String TAG = LanguageSelectorDialogFragment.class.getSimpleName();
    private LanguageSelectorCallback mLanguageSelectorCallback;
    private String[] mLanguages;

    public interface LanguageSelectorCallback {
        void onLanguageChanged();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mLanguages = getResources().getStringArray(R.array.language_values);
        try {
            this.mLanguageSelectorCallback = (LanguageSelectorCallback) activity;
        } catch (ClassCastException e) {
        }
    }

    public void onClick(@NonNull DialogInterface dialog, int which) {
        dialog.dismiss();
        String newLanguage = this.mLanguages[which];
        if (!newLanguage.equals(LocaleHelper.INSTANCE.getLanguageValue())) {
            LocaleHelper.INSTANCE.setLanguage(getActivity(), newLanguage, true);
            Analytics.sendEvent(getActivity(), AnalyticsEvent.LANGUAGE_CHANGED);
            if (this.mLanguageSelectorCallback != null) {
                this.mLanguageSelectorCallback.onLanguageChanged();
            }
        }
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Analytics.hitFragment((Fragment) this);
        int selectedItem = 0;
        String currentLanguage = LocaleHelper.INSTANCE.getLanguageValue();
        for (int i = 0; i < this.mLanguages.length; i++) {
            if (this.mLanguages[i].equalsIgnoreCase(currentLanguage)) {
                selectedItem = i;
                break;
            }
        }
        return new Builder(getActivity(), R.style.Theme.Pipedrive.Dialog.Alert).setSingleChoiceItems((int) R.array.language_titles, selectedItem, (OnClickListener) this).setTitle((int) R.string.pref_language_title).create();
    }
}
