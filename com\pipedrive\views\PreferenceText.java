package com.pipedrive.views;

import android.content.Context;
import android.preference.Preference;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import com.pipedrive.R;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.ScreensMapper;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.time.TimeManager;
import java.io.IOException;

public class PreferenceText extends Preference {
    private static final String FILE_CHANGELOG = "text_changelog.html";
    private static final String FILE_OPEN_SOURCE_LICENCE = "text_open_source_licences.html";
    static final String TAG = PreferenceText.class.getSimpleName();
    private static final String URL_PRIVACY_POLICY = "https://www.pipedrive.com/en/privacy-mobile";
    private static final String URL_TERMS_OF_SERVICE = "https://www.pipedrive.com/en/terms-of-service-mobile";

    abstract class OnClickListenerTrigger implements OnClickListener {
        final int TRIGGER_ON_COUNT = 5;
        long lastClickInMillis = TimeManager.getInstance().currentTimeMillis().longValue();
        int triggerCounter = 0;

        public abstract void onClickTriggered(View view);

        OnClickListenerTrigger() {
        }

        public void onClick(View v) {
            if (this.triggerCounter < 5) {
                long nowMillis = TimeManager.getInstance().currentTimeMillis().longValue();
                if (nowMillis - this.lastClickInMillis < 500) {
                    this.triggerCounter++;
                } else {
                    this.triggerCounter = 0;
                }
                this.lastClickInMillis = nowMillis;
                return;
            }
            this.triggerCounter = 0;
            onClickTriggered(v);
        }
    }

    public PreferenceText(Context context) {
        super(context);
    }

    public PreferenceText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PreferenceText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public View getView(View convertView, ViewGroup parent) {
        String tag = TAG + ".getView()";
        convertView = ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate(R.layout.preferences_text_view, null);
        Object fileToOpen = null;
        String urlToLoad = null;
        if (TextUtils.equals(getKey(), getContext().getResources().getString(R.string.preference_key_open_source_licences))) {
            fileToOpen = FILE_OPEN_SOURCE_LICENCE;
            Analytics.hitScreen(ScreensMapper.SCREEN_NAME_OPEN_SOURCE_LICENSES, getContext());
        } else if (TextUtils.equals(getKey(), getContext().getResources().getString(R.string.preference_key_privacy_policy))) {
            urlToLoad = URL_PRIVACY_POLICY;
            Analytics.hitScreen(ScreensMapper.SCREEN_NAME_PRIVACY_POLICY, getContext());
        } else if (TextUtils.equals(getKey(), getContext().getResources().getString(R.string.preference_key_terms_of_cervice))) {
            urlToLoad = URL_TERMS_OF_SERVICE;
            Analytics.hitScreen(ScreensMapper.SCREEN_NAME_TERMS_OF_SERVICE, getContext());
        } else if (TextUtils.equals(getKey(), getContext().getResources().getString(R.string.preference_key_changelog))) {
            fileToOpen = FILE_CHANGELOG;
            Analytics.hitScreen(ScreensMapper.SCREEN_NAME_CHANGELOG, getContext());
        }
        if (fileToOpen != null) {
            try {
                String textFromResource = ConnectionUtil.inputStreamToString(getContext().getAssets().open(fileToOpen));
                TextView textView = (TextView) convertView.findViewById(R.id.preferencesViewText);
                if (TextUtils.equals(fileToOpen, FILE_CHANGELOG)) {
                    textView.setOnClickListener(new OnClickListenerTrigger() {
                        public void onClickTriggered(View v) {
                            LogJourno.reportEvent(EVENT.Test, "Testreport message. Time:" + TimeManager.getInstance().currentTimeMillis());
                            throw new RuntimeException("Testcrash. Time:" + TimeManager.getInstance().currentTimeMillis());
                        }
                    });
                }
                textView.setVisibility(0);
                textView.setText(Html.fromHtml(textFromResource.toString()));
            } catch (IOException e) {
                Log.e(new Throwable("Problems reading assets!", e));
            }
        }
        if (urlToLoad != null) {
            WebView webView = (WebView) convertView.findViewById(R.id.preferencesViewWeb);
            final TextView preferencesViewWebLoadingLabel = (TextView) convertView.findViewById(R.id.preferencesViewWebLoadingLabel);
            preferencesViewWebLoadingLabel.setVisibility(0);
            webView.setVisibility(8);
            final String urlToStringFinal = urlToLoad;
            webView.setWebChromeClient(new WebChromeClient() {
                public void onProgressChanged(WebView view, int progress) {
                    preferencesViewWebLoadingLabel.setText(PreferenceText.this.getContext().getString(R.string.lbl_preference_web_view_loading) + progress + "%");
                }
            });
            webView.setWebViewClient(new WebViewClient() {
                boolean loadingError = false;

                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    this.loadingError = true;
                }

                public void onPageFinished(WebView view, String url) {
                    if (this.loadingError) {
                        view.setVisibility(8);
                        preferencesViewWebLoadingLabel.setVisibility(0);
                        Context context = PreferenceText.this.getContext();
                        Object[] objArr = new Object[2];
                        objArr[0] = TextUtils.equals(urlToStringFinal, PreferenceText.URL_PRIVACY_POLICY) ? ScreensMapper.SCREEN_NAME_PRIVACY_POLICY : ScreensMapper.SCREEN_NAME_TERMS_OF_SERVICE;
                        objArr[1] = urlToStringFinal;
                        preferencesViewWebLoadingLabel.setText(context.getString(R.string.lbl_preference_web_view_loading_error, objArr));
                        return;
                    }
                    preferencesViewWebLoadingLabel.setVisibility(8);
                    view.setVisibility(0);
                }
            });
            webView.loadUrl(urlToLoad);
        }
        return convertView;
    }
}
