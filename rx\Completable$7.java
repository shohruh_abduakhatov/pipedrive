package rx;

import rx.subscriptions.Subscriptions;

class Completable$7 implements Completable$OnSubscribe {
    final /* synthetic */ Throwable val$error;

    Completable$7(Throwable th) {
        this.val$error = th;
    }

    public void call(CompletableSubscriber s) {
        s.onSubscribe(Subscriptions.unsubscribed());
        s.onError(this.val$error);
    }
}
