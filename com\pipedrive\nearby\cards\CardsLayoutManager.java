package com.pipedrive.nearby.cards;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.view.View;
import com.pipedrive.R;

class CardsLayoutManager extends LinearLayoutManager {
    private final int maxChildWidth;

    CardsLayoutManager(Context context) {
        super(context, 0, false);
        this.maxChildWidth = context.getResources().getDisplayMetrics().widthPixels - context.getResources().getDimensionPixelSize(R.dimen.card_horizontal_offset);
        setRecycleChildrenOnDetach(true);
    }

    public void measureChildWithMargins(View child, int widthUsed, int heightUsed) {
        ((LayoutParams) child.getLayoutParams()).width = this.maxChildWidth;
        super.measureChildWithMargins(child, widthUsed, heightUsed);
    }
}
