package com.pipedrive;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class TrialExpiredActivity_ViewBinding implements Unbinder {
    private TrialExpiredActivity target;
    private View view2131820791;
    private View view2131820835;

    @UiThread
    public TrialExpiredActivity_ViewBinding(TrialExpiredActivity target) {
        this(target, target.getWindow().getDecorView());
    }

    @UiThread
    public TrialExpiredActivity_ViewBinding(final TrialExpiredActivity target, View source) {
        this.target = target;
        View view = Utils.findRequiredView(source, R.id.sign_out, "method 'onSignOutClicked'");
        this.view2131820791 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onSignOutClicked();
            }
        });
        view = Utils.findRequiredView(source, R.id.change_company, "method 'onChangeCompanyClicked'");
        this.view2131820835 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onChangeCompanyClicked();
            }
        });
    }

    @CallSuper
    public void unbind() {
        if (this.target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        this.view2131820791.setOnClickListener(null);
        this.view2131820791 = null;
        this.view2131820835.setOnClickListener(null);
        this.view2131820835 = null;
    }
}
