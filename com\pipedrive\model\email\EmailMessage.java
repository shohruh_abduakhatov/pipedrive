package com.pipedrive.model.email;

import android.text.TextUtils;
import com.newrelic.agent.android.instrumentation.JSONArrayInstrumentation;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.util.networking.entities.EmailAttachmentEntity;
import com.pipedrive.util.networking.entities.EmailMessageEntity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;

public class EmailMessage extends BaseDatasourceEntity {
    private Boolean mActive;
    private PipedriveDateTime mAddTime;
    private Integer mAttachmentCount;
    private List<EmailAttachment> mAttachments = new ArrayList();
    private List<EmailParticipant> mBcc = new ArrayList();
    private byte[] mBody;
    private List<EmailParticipant> mCc = new ArrayList();
    private Boolean mDraft;
    private List<EmailParticipant> mFrom = new ArrayList();
    private PipedriveDateTime mMessageTime;
    private String mRecepientsDisplayString;
    private String mSendersDisplayString;
    private String mSubject;
    private String mSummary;
    private EmailThread mThread;
    private List<EmailParticipant> mTo = new ArrayList();
    private PipedriveDateTime mUpdateTime;

    public static EmailMessage from(EmailMessageEntity entity, List<EmailParticipant> from, List<EmailParticipant> to, List<EmailParticipant> cc, List<EmailParticipant> bcc, EmailThread thread) {
        EmailMessage result = new EmailMessage();
        if (entity != null) {
            if (entity.getPipedriveId() != null) {
                result.setPipedriveId(entity.getPipedriveId().intValue());
            }
            result.setActive(entity.getActive());
            result.setAddTime(PipedriveDateTime.instanceFromDate(entity.getAddTime()));
            result.setBcc(bcc);
            result.setBody(TextUtils.isEmpty(entity.getBody()) ? null : entity.getBody().getBytes());
            result.setCc(cc);
            result.setDraft(entity.getDraft());
            result.setFrom(from);
            result.setMessageTime(PipedriveDateTime.instanceFromDate(entity.getMessageTime()));
            result.setSubject(entity.getSubject());
            result.setSummary(entity.getSummary());
            result.setThread(thread);
            result.setTo(to);
            result.setUpdateTime(PipedriveDateTime.instanceFromDate(entity.getUpdateTime()));
            result.setAttachmentCount(entity.getAttachmentCount());
            result.setAttachmentsFromEntities(entity.getAttachments());
        }
        return result;
    }

    public String getRecepientsDisplayString() {
        return this.mRecepientsDisplayString;
    }

    public void setRecepientsDisplayString(String recepientsDisplayString) {
        this.mRecepientsDisplayString = recepientsDisplayString;
    }

    public String getSendersDisplayString() {
        return this.mSendersDisplayString;
    }

    public void setSendersDisplayString(String sendersDisplayString) {
        this.mSendersDisplayString = sendersDisplayString;
    }

    private void calculateDestinationString() {
        JSONArray jsonArray = new JSONArray();
        if (!(this.mTo == null || this.mTo.isEmpty())) {
            for (EmailParticipant participant : this.mTo) {
                if (participant != null) {
                    jsonArray.put(TextUtils.isEmpty(participant.getName()) ? participant.getEmail() : participant.getName());
                }
            }
        }
        if (!(this.mCc == null || this.mCc.isEmpty())) {
            for (EmailParticipant participant2 : this.mCc) {
                if (participant2 != null) {
                    jsonArray.put(TextUtils.isEmpty(participant2.getName()) ? participant2.getEmail() : participant2.getName());
                }
            }
        }
        if (!(this.mBcc == null || this.mBcc.isEmpty())) {
            for (EmailParticipant participant22 : this.mBcc) {
                if (participant22 != null) {
                    jsonArray.put(TextUtils.isEmpty(participant22.getName()) ? participant22.getEmail() : participant22.getName());
                }
            }
        }
        this.mRecepientsDisplayString = !(jsonArray instanceof JSONArray) ? jsonArray.toString() : JSONArrayInstrumentation.toString(jsonArray);
    }

    public EmailThread getThread() {
        return this.mThread;
    }

    public void setThread(EmailThread thread) {
        this.mThread = thread;
    }

    public List<EmailParticipant> getFrom() {
        return this.mFrom;
    }

    public void setFrom(List<EmailParticipant> from) {
        this.mFrom = from;
        JSONArray jsonArray = new JSONArray();
        if (!(this.mFrom == null || this.mFrom.isEmpty())) {
            for (EmailParticipant participant : this.mFrom) {
                if (participant != null) {
                    jsonArray.put(TextUtils.isEmpty(participant.getName()) ? participant.getEmail() : participant.getName());
                }
            }
        }
        this.mSendersDisplayString = !(jsonArray instanceof JSONArray) ? jsonArray.toString() : JSONArrayInstrumentation.toString(jsonArray);
    }

    public String getSubject() {
        return this.mSubject;
    }

    public void setSubject(String subject) {
        this.mSubject = subject;
    }

    public String getSummary() {
        return this.mSummary;
    }

    public void setSummary(String summary) {
        this.mSummary = summary;
    }

    public byte[] getBody() {
        return this.mBody;
    }

    public void setBody(byte[] body) {
        this.mBody = body;
    }

    public PipedriveDateTime getMessageTime() {
        return this.mMessageTime;
    }

    public void setMessageTime(PipedriveDateTime messageTime) {
        this.mMessageTime = messageTime;
    }

    public PipedriveDateTime getAddTime() {
        return this.mAddTime;
    }

    public void setAddTime(PipedriveDateTime addTime) {
        this.mAddTime = addTime;
    }

    public PipedriveDateTime getUpdateTime() {
        return this.mUpdateTime;
    }

    public void setUpdateTime(PipedriveDateTime updateTime) {
        this.mUpdateTime = updateTime;
    }

    public Boolean getActive() {
        return this.mActive;
    }

    public void setActive(Boolean active) {
        this.mActive = active;
    }

    public Boolean getDraft() {
        return this.mDraft;
    }

    public void setDraft(Boolean draft) {
        this.mDraft = draft;
    }

    public List<EmailParticipant> getTo() {
        return this.mTo;
    }

    public void setTo(List<EmailParticipant> to) {
        this.mTo = to;
        calculateDestinationString();
    }

    public List<EmailParticipant> getCc() {
        return this.mCc;
    }

    public void setCc(List<EmailParticipant> cc) {
        this.mCc = cc;
    }

    public List<EmailParticipant> getBcc() {
        return this.mBcc;
    }

    public void setBcc(List<EmailParticipant> bcc) {
        this.mBcc = bcc;
    }

    public Integer getAttachmentCount() {
        return this.mAttachmentCount;
    }

    public void setAttachmentCount(Integer attachmentCount) {
        this.mAttachmentCount = attachmentCount;
    }

    public List<EmailAttachment> getAttachments() {
        return this.mAttachments;
    }

    public void setAttachmentsFromEntities(List<EmailAttachmentEntity> attachments) {
        if (this.mAttachments == null) {
            this.mAttachments = new ArrayList();
        }
        this.mAttachments.clear();
        if (attachments != null) {
            for (EmailAttachmentEntity entity : attachments) {
                EmailAttachment emailAttachment = EmailAttachment.from(entity);
                if (emailAttachment != null) {
                    this.mAttachments.add(emailAttachment);
                }
            }
        }
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EmailMessage that = (EmailMessage) o;
        if (this.mActive == null ? that.mActive != null : !this.mActive.equals(that.mActive)) {
            return false;
        }
        if (this.mAddTime == null ? that.mAddTime != null : !this.mAddTime.equals(that.mAddTime)) {
            return false;
        }
        if (this.mBcc == null ? that.mBcc != null : !this.mBcc.equals(that.mBcc)) {
            return false;
        }
        if (!Arrays.equals(this.mBody, that.mBody)) {
            return false;
        }
        if (this.mCc == null ? that.mCc != null : !this.mCc.equals(that.mCc)) {
            return false;
        }
        if (this.mDraft == null ? that.mDraft != null : !this.mDraft.equals(that.mDraft)) {
            return false;
        }
        if (this.mFrom == null ? that.mFrom != null : !this.mFrom.equals(that.mFrom)) {
            return false;
        }
        if (this.mSendersDisplayString == null ? that.mSendersDisplayString != null : !this.mSendersDisplayString.equals(that.mSendersDisplayString)) {
            return false;
        }
        if (this.mMessageTime == null ? that.mMessageTime != null : !this.mMessageTime.equals(that.mMessageTime)) {
            return false;
        }
        if (this.mRecepientsDisplayString == null ? that.mRecepientsDisplayString != null : !this.mRecepientsDisplayString.equals(that.mRecepientsDisplayString)) {
            return false;
        }
        if (this.mSubject == null ? that.mSubject != null : !this.mSubject.equals(that.mSubject)) {
            return false;
        }
        if (this.mSummary == null ? that.mSummary != null : !this.mSummary.equals(that.mSummary)) {
            return false;
        }
        if (this.mThread == null ? that.mThread != null : !this.mThread.equals(that.mThread)) {
            return false;
        }
        if (this.mTo == null ? that.mTo != null : !this.mTo.equals(that.mTo)) {
            return false;
        }
        if (this.mUpdateTime == null ? that.mUpdateTime != null : !this.mUpdateTime.equals(that.mUpdateTime)) {
            return false;
        }
        if (this.mAttachmentCount == null ? that.mAttachmentCount != null : !this.mAttachmentCount.equals(that.mAttachmentCount)) {
            return false;
        }
        if (this.mAttachments != null) {
            if (this.mAttachments.equals(that.mAttachments)) {
                return true;
            }
        } else if (that.mAttachmentCount == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int result;
        int hashCode;
        int i = 0;
        if (this.mThread != null) {
            result = this.mThread.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.mSubject != null) {
            hashCode = this.mSubject.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mSummary != null) {
            hashCode = this.mSummary.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mBody != null) {
            hashCode = Arrays.hashCode(this.mBody);
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mMessageTime != null) {
            hashCode = this.mMessageTime.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mAddTime != null) {
            hashCode = this.mAddTime.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mUpdateTime != null) {
            hashCode = this.mUpdateTime.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mActive != null) {
            hashCode = this.mActive.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mDraft != null) {
            hashCode = this.mDraft.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mFrom != null) {
            hashCode = this.mFrom.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mTo != null) {
            hashCode = this.mTo.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mCc != null) {
            hashCode = this.mCc.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mBcc != null) {
            hashCode = this.mBcc.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mSendersDisplayString != null) {
            hashCode = this.mSendersDisplayString.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mRecepientsDisplayString != null) {
            hashCode = this.mRecepientsDisplayString.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mAttachmentCount != null) {
            hashCode = this.mAttachmentCount.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (i2 + hashCode) * 31;
        if (this.mAttachments != null) {
            i = this.mAttachments.hashCode();
        }
        return hashCode + i;
    }

    public void setAttachments(List<EmailAttachment> list) {
        List arrayList;
        if (list == null) {
            arrayList = new ArrayList();
        }
        this.mAttachments = arrayList;
    }
}
