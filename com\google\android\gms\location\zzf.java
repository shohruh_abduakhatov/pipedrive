package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.location.internal.ParcelableGeofence;
import java.util.List;

public class zzf implements Creator<GeofencingRequest> {
    static void zza(GeofencingRequest geofencingRequest, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zzc(parcel, 1, geofencingRequest.zzbqa(), false);
        zzb.zzc(parcel, 2, geofencingRequest.getInitialTrigger());
        zzb.zzc(parcel, 1000, geofencingRequest.getVersionCode());
        zzb.zzaj(parcel, zzcs);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zznt(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzun(i);
    }

    public GeofencingRequest zznt(Parcel parcel) {
        int i = 0;
        int zzcr = zza.zzcr(parcel);
        List list = null;
        int i2 = 0;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    list = zza.zzc(parcel, zzcq, ParcelableGeofence.CREATOR);
                    break;
                case 2:
                    i = zza.zzg(parcel, zzcq);
                    break;
                case 1000:
                    i2 = zza.zzg(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new GeofencingRequest(i2, list, i);
        }
        throw new zza.zza("Overread allowed size end=" + zzcr, parcel);
    }

    public GeofencingRequest[] zzun(int i) {
        return new GeofencingRequest[i];
    }
}
