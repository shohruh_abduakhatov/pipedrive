package com.pipedrive.deal;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.model.Deal;

interface DealEditView {
    void onAssociationsUpdated(@NonNull Deal deal);

    void onDealCreated(boolean z);

    void onDealDeleted(boolean z);

    void onDealUpdated(boolean z);

    void onPipelineUpdated(@NonNull Deal deal);

    void onRequestDeal(@Nullable Deal deal);
}
