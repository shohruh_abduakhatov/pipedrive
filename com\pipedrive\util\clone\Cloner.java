package com.pipedrive.util.clone;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

public enum Cloner {
    ;

    public static <T extends Parcelable> T cloneParcelable(T parcelableToClone) {
        Parcel parsel = getParsel(parcelableToClone);
        Parcelable cloned = (Parcelable) parsel.readValue(parcelableToClone.getClass().getClassLoader());
        parsel.recycle();
        return cloned;
    }

    public static boolean isEqual(Parcelable parcelable1, Parcelable parcelable2) {
        return Arrays.equals(getMarshall(parcelable1), getMarshall(parcelable2));
    }

    private static Parcel getParsel(Parcelable parceable) {
        Parcel parsel = Parcel.obtain();
        parsel.setDataPosition(0);
        parsel.writeValue(parceable);
        parsel.setDataPosition(0);
        return parsel;
    }

    private static byte[] getMarshall(Parcelable parcelableToClone) {
        Parcel parsel = getParsel(parcelableToClone);
        byte[] ret = parsel.marshall();
        parsel.recycle();
        return ret;
    }
}
