package com.pipedrive.nearby.model;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.model.BaseDatasourceEntity;
import java.util.List;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

abstract class DealRelatedNearbyItem<T extends BaseDatasourceEntity> extends NearbyItem<T> {
    @NonNull
    protected abstract String getColumnForOpenDealsCount();

    DealRelatedNearbyItem(@NonNull Double latitude, @NonNull Double longitude, @NonNull String address, @NonNull Long singleItemSqlId) {
        super(latitude, longitude, address, singleItemSqlId);
    }

    DealRelatedNearbyItem(@NonNull Double latitude, @NonNull Double longitude, @NonNull String address, @NonNull List<Long> multipleItemSqlIds) {
        super(latitude, longitude, address, (List) multipleItemSqlIds);
    }

    @NonNull
    public final Observable<Integer> getOpenDealCount(@NonNull final Session session) {
        return getItemSqlIds().subscribeOn(Schedulers.io()).flatMap(new Func1<Long, Observable<Integer>>() {
            public Observable<Integer> call(Long itemSqlId) {
                return new DealsDataSource(session.getDatabase()).getOpenDealCountForItemRx(itemSqlId, DealRelatedNearbyItem.this.getColumnForOpenDealsCount());
            }
        }).reduce(new Func2<Integer, Integer, Integer>() {
            public Integer call(Integer dealCountForSomeItem, Integer dealCountForSomeOtherItem) {
                return Integer.valueOf(dealCountForSomeItem.intValue() + dealCountForSomeOtherItem.intValue());
            }
        });
    }
}
