package com.pipedrive.util.networking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pipedrive.model.pagination.Pagination;
import com.pipedrive.util.networking.entities.AdditionalData;
import java.util.ArrayList;
import java.util.List;

public class Response {
    public static final String ERROR_NO_STREAM = "InputStream is null!";
    public static final String ERROR_SERVER_INTERNAL_ERROR = "Server internal error. No error stream!";
    public static final int HTTP_STATUS_TOO_MANY_REQUESTS = 429;
    public static final String JSON_PARAM_ADDITIONAL_DATA = "additional_data";
    public static final String JSON_PARAM_DATA = "data";
    public static final String JSON_PARAM_DETAILS = "details";
    public static final String JSON_PARAM_ERROR = "error";
    public static final String JSON_PARAM_MATCHES_FILTERS = "matches_filters";
    public static final String JSON_PARAM_SUCCESS = "success";
    @SerializedName("error")
    @Expose
    public String error;
    public Integer httpCode = null;
    @SerializedName("success")
    @Expose
    public boolean isRequestSuccessful;
    @SerializedName("additional_data")
    @Expose
    private AdditionalData mAdditionalData;

    public Pagination getPagination() {
        if (this.mAdditionalData == null) {
            setPagination(new Pagination());
        }
        return this.mAdditionalData.getPagination();
    }

    public List<Long> getMatchedFilters() {
        if (this.mAdditionalData == null) {
            setMatchedFilters(new ArrayList());
        }
        return this.mAdditionalData.getMatchedFilters();
    }

    public void setPagination(Pagination pagination) {
        if (this.mAdditionalData == null) {
            this.mAdditionalData = new AdditionalData();
        }
        this.mAdditionalData.setPagination(pagination);
    }

    public void setMatchedFilters(List<Long> matchedFilters) {
        if (this.mAdditionalData == null) {
            this.mAdditionalData = new AdditionalData();
        }
        this.mAdditionalData.setMatchedFilters(matchedFilters);
    }

    public String toString() {
        return String.format(" [isRequestSuccessful:%s][error:%s][pagination:%s]", new Object[]{Boolean.valueOf(this.isRequestSuccessful), this.error, getPagination()});
    }
}
