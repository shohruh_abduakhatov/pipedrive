package com.google.maps.android.clustering.algo;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.geometry.Bounds;
import com.google.maps.android.geometry.Point;
import com.google.maps.android.heatmaps.WeightedLatLng;
import com.google.maps.android.projection.SphericalMercatorProjection;
import com.google.maps.android.quadtree.PointQuadTree;
import com.google.maps.android.quadtree.PointQuadTree.Item;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class NonHierarchicalDistanceBasedAlgorithm<T extends ClusterItem> implements Algorithm<T> {
    public static final int MAX_DISTANCE_AT_ZOOM = 100;
    private static final SphericalMercatorProjection PROJECTION = new SphericalMercatorProjection(WeightedLatLng.DEFAULT_INTENSITY);
    private final Collection<QuadItem<T>> mItems = new ArrayList();
    private final PointQuadTree<QuadItem<T>> mQuadTree = new PointQuadTree(0.0d, WeightedLatLng.DEFAULT_INTENSITY, 0.0d, WeightedLatLng.DEFAULT_INTENSITY);

    private static class QuadItem<T extends ClusterItem> implements Item, Cluster<T> {
        private final T mClusterItem;
        private final Point mPoint;
        private final LatLng mPosition;
        private Set<T> singletonSet;

        private QuadItem(T item) {
            this.mClusterItem = item;
            this.mPosition = item.getPosition();
            this.mPoint = NonHierarchicalDistanceBasedAlgorithm.PROJECTION.toPoint(this.mPosition);
            this.singletonSet = Collections.singleton(this.mClusterItem);
        }

        public Point getPoint() {
            return this.mPoint;
        }

        public LatLng getPosition() {
            return this.mPosition;
        }

        public Set<T> getItems() {
            return this.singletonSet;
        }

        public int getSize() {
            return 1;
        }

        public int hashCode() {
            return this.mClusterItem.hashCode();
        }

        public boolean equals(Object other) {
            if (other instanceof QuadItem) {
                return ((QuadItem) other).mClusterItem.equals(this.mClusterItem);
            }
            return false;
        }
    }

    public void addItem(T item) {
        QuadItem<T> quadItem = new QuadItem(item);
        synchronized (this.mQuadTree) {
            this.mItems.add(quadItem);
            this.mQuadTree.add(quadItem);
        }
    }

    public void addItems(Collection<T> items) {
        for (T item : items) {
            addItem(item);
        }
    }

    public void clearItems() {
        synchronized (this.mQuadTree) {
            this.mItems.clear();
            this.mQuadTree.clear();
        }
    }

    public void removeItem(T item) {
        QuadItem<T> quadItem = new QuadItem(item);
        synchronized (this.mQuadTree) {
            this.mItems.remove(quadItem);
            this.mQuadTree.remove(quadItem);
        }
    }

    public Set<? extends Cluster<T>> getClusters(double zoom) {
        double zoomSpecificSpan = (100.0d / Math.pow(2.0d, (double) ((int) zoom))) / 256.0d;
        Set<QuadItem<T>> visitedCandidates = new HashSet();
        Set<Cluster<T>> results = new HashSet();
        Map<QuadItem<T>, Double> distanceToCluster = new HashMap();
        Map<QuadItem<T>, StaticCluster<T>> itemToCluster = new HashMap();
        synchronized (this.mQuadTree) {
            for (QuadItem<T> candidate : this.mItems) {
                if (!visitedCandidates.contains(candidate)) {
                    Collection<QuadItem<T>> clusterItems = this.mQuadTree.search(createBoundsFromSpan(candidate.getPoint(), zoomSpecificSpan));
                    if (clusterItems.size() == 1) {
                        results.add(candidate);
                        visitedCandidates.add(candidate);
                        distanceToCluster.put(candidate, Double.valueOf(0.0d));
                    } else {
                        StaticCluster<T> cluster = new StaticCluster(candidate.mClusterItem.getPosition());
                        results.add(cluster);
                        for (QuadItem<T> clusterItem : clusterItems) {
                            Double existingDistance = (Double) distanceToCluster.get(clusterItem);
                            double distance = distanceSquared(clusterItem.getPoint(), candidate.getPoint());
                            if (existingDistance != null) {
                                if (existingDistance.doubleValue() >= distance) {
                                    ((StaticCluster) itemToCluster.get(clusterItem)).remove(clusterItem.mClusterItem);
                                }
                            }
                            distanceToCluster.put(clusterItem, Double.valueOf(distance));
                            cluster.add(clusterItem.mClusterItem);
                            itemToCluster.put(clusterItem, cluster);
                        }
                        visitedCandidates.addAll(clusterItems);
                    }
                }
            }
        }
        return results;
    }

    public Collection<T> getItems() {
        List<T> items = new ArrayList();
        synchronized (this.mQuadTree) {
            for (QuadItem<T> quadItem : this.mItems) {
                items.add(quadItem.mClusterItem);
            }
        }
        return items;
    }

    private double distanceSquared(Point a, Point b) {
        return ((a.x - b.x) * (a.x - b.x)) + ((a.y - b.y) * (a.y - b.y));
    }

    private Bounds createBoundsFromSpan(Point p, double span) {
        double halfSpan = span / 2.0d;
        return new Bounds(p.x - halfSpan, p.x + halfSpan, p.y - halfSpan, p.y + halfSpan);
    }
}
