package com.pipedrive.note;

import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.model.Activity;

@MainThread
abstract class ActivityNoteEditorPresenter extends HtmlEditorPresenter<Activity, ActivityNoteEditorView> {
    abstract void changeActivityNote();

    abstract void deleteActivityNote();

    abstract void requestExistingActivityNote(long j);

    abstract void requestNoteForNewActivity(String str);

    public ActivityNoteEditorPresenter(@NonNull Session session, @Nullable Bundle savedState) {
        super(session, savedState);
    }
}
