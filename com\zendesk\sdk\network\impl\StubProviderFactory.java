package com.zendesk.sdk.network.impl;

import com.zendesk.logger.Logger;
import com.zendesk.service.ErrorResponseAdapter;
import com.zendesk.service.ZendeskCallback;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Locale;

class StubProviderFactory {
    private static final String LOG_TAG = "StubProviderFactory";

    private static class ZendeskCallbackInvocationHandler implements InvocationHandler {
        private Class clazz;

        ZendeskCallbackInvocationHandler(Class clazz) {
            this.clazz = clazz;
        }

        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            ZendeskCallback zendeskCallback = null;
            for (ZendeskCallback o : args) {
                if (o instanceof ZendeskCallback) {
                    zendeskCallback = o;
                    break;
                }
            }
            String errorMsg = String.format(Locale.US, "Zendesk not initialised! You tried to call: %s#%s(...)", new Object[]{this.clazz.getSimpleName(), method.getName()});
            Logger.d(providerName, errorMsg, new Object[0]);
            if (zendeskCallback != null) {
                zendeskCallback.onError(new ErrorResponseAdapter(errorMsg));
            }
            return null;
        }
    }

    StubProviderFactory() {
    }

    static <E> E getStubProvider(Class<E> clazz) {
        E provider = null;
        try {
            provider = Proxy.getProxyClass(clazz.getClassLoader(), new Class[]{clazz}).getConstructor(new Class[]{InvocationHandler.class}).newInstance(new Object[]{new ZendeskCallbackInvocationHandler(clazz)});
        } catch (Exception e) {
            Logger.w(LOG_TAG, "Unable to create stub provider. Error: %s", e.getMessage());
        }
        return provider;
    }
}
