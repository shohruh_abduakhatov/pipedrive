package com.pipedrive.nearby.model;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.BaseDataSource;
import com.pipedrive.model.Activity;
import com.pipedrive.model.BaseDatasourceEntity;
import java.util.ArrayList;
import java.util.List;
import rx.Observable;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

class MultipleNearbyItemsImplementation<T extends BaseDatasourceEntity> implements NearbyItemImplementation<T> {
    @NonNull
    private final List<Long> itemSqlIds = new ArrayList();

    MultipleNearbyItemsImplementation(@NonNull List<Long> itemSqlIds) {
        this.itemSqlIds.addAll(itemSqlIds);
    }

    @NonNull
    public Observable<T> getData(@NonNull final BaseDataSource<T> dataSource) {
        return Observable.from(this.itemSqlIds).subscribeOn(Schedulers.io()).map(new Func1<Long, T>() {
            public T call(Long itemSqlId) {
                return dataSource.findBySqlId(itemSqlId.longValue());
            }
        });
    }

    @NonNull
    public Observable<Activity> getNextActivity(@NonNull Session session, @NonNull String activitiesColumnForNearbyItem) {
        return Observable.empty();
    }

    @NonNull
    public Observable<Activity> getLastActivity(@NonNull Session session, @NonNull String activitiesColumnForNearbyItem) {
        return Observable.empty();
    }

    @NonNull
    public Observable<Long> getItemSqlIds() {
        return Observable.from(this.itemSqlIds);
    }

    @NonNull
    public Boolean hasMultipleIds() {
        return Boolean.valueOf(true);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        return this.itemSqlIds.equals(((MultipleNearbyItemsImplementation) o).itemSqlIds);
    }

    public int hashCode() {
        return this.itemSqlIds.hashCode();
    }
}
