package com.pipedrive.util;

import android.content.Context;
import android.os.Build.VERSION;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.widget.Button;
import android.widget.TextView;
import com.pipedrive.R;
import com.pipedrive.logging.Log;
import com.pipedrive.util.UndoBarController.NoUndoListener;
import com.pipedrive.util.UndoBarController.UndoListener;
import com.pipedrive.util.viewhelper.ViewHelper;

class UndoBarController$UndoBarRequest {
    final String TAG = (UndoBarController.TAG + ".UndoBarRequest");
    private ViewPropertyAnimator mBarAnimator;
    private View mBarUndoView;
    private boolean mIsProcessed = false;
    private TextView mMessageView;
    private NoUndoListener<T> mNoUndoListener;
    private Runnable mNoUndoneRunnable;
    @Deprecated
    private int mTokenInt;
    private Button mUndoBarButton;
    private UndoListener<T> mUndoListener;
    private T mUndoToken;
    private ViewGroup mUndoViewAnchor;
    final /* synthetic */ UndoBarController this$0;

    UndoBarController$UndoBarRequest(UndoBarController undoBarController, int delayInMillis, CharSequence message, T undoToken, int tokenInt, UndoListener<T> undoListener, NoUndoListener<T> noUndoListener, ViewGroup undoViewAnchor, Context context) {
        boolean canAnimate = false;
        this.this$0 = undoBarController;
        if (undoViewAnchor != null) {
            this.mBarUndoView = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(R.layout.layout_undobar_undo_view, undoViewAnchor, false);
        }
        if (this.mBarUndoView != null) {
            if (Integer.valueOf(VERSION.SDK).intValue() >= 14) {
                canAnimate = true;
            }
            if (canAnimate) {
                this.mBarAnimator = this.mBarUndoView.animate();
            }
            this.mUndoViewAnchor = undoViewAnchor;
            this.mUndoViewAnchor.addView(this.mBarUndoView);
        }
        this.mUndoToken = undoToken;
        this.mTokenInt = tokenInt;
        this.mUndoListener = undoListener;
        this.mNoUndoListener = noUndoListener;
        if (this.mBarUndoView != null) {
            this.mUndoBarButton = (Button) this.mBarUndoView.findViewById(R.id.undobar_button);
            this.mUndoBarButton.setOnClickListener(new 1(this, undoBarController));
            this.mMessageView = (TextView) this.mBarUndoView.findViewById(R.id.undobar_message);
            this.mMessageView.setText(message);
        }
        this.mNoUndoneRunnable = new 2(this, undoBarController);
        UndoBarController.access$700(undoBarController).postDelayed(this.mNoUndoneRunnable, (long) delayInMillis);
        if (this.mBarAnimator != null) {
            ViewHelper.setAlpha(this.mBarUndoView, 0.0f);
            this.mBarAnimator.cancel();
            this.mBarAnimator.alpha(1.0f).setDuration((long) this.mBarUndoView.getResources().getInteger(17694720)).setListener(null);
        }
    }

    boolean isProcessed() {
        return this.mIsProcessed;
    }

    void processAtOnce() {
        if (!isProcessed()) {
            UndoBarController.access$700(this.this$0).removeCallbacks(this.mNoUndoneRunnable);
            this.mNoUndoneRunnable.run();
        }
    }

    private void hideUndoBar(boolean immediate) {
        Log.d(UndoBarController.TAG + ".hideUndoBar()", String.format("Undo bar hiding with immediate: %s", new Object[]{Boolean.valueOf(immediate)}));
        UndoBarController.access$700(this.this$0).removeCallbacks(this.mNoUndoneRunnable);
        if (immediate || this.mBarAnimator == null) {
            if (this.mBarUndoView != null) {
                this.mBarUndoView.setVisibility(8);
            }
            if (this.mUndoViewAnchor != null) {
                this.mUndoViewAnchor.removeView(this.mBarUndoView);
                return;
            }
            return;
        }
        this.mBarAnimator.cancel();
        this.mBarAnimator.alpha(0.0f).setDuration((long) this.mBarUndoView.getResources().getInteger(17694720)).setListener(new 3(this));
    }
}
