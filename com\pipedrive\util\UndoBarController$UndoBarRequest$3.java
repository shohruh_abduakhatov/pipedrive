package com.pipedrive.util;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import com.pipedrive.util.UndoBarController.UndoBarRequest;

class UndoBarController$UndoBarRequest$3 implements AnimatorListener {
    final /* synthetic */ UndoBarRequest this$1;

    UndoBarController$UndoBarRequest$3(UndoBarRequest this$1) {
        this.this$1 = this$1;
    }

    public void onAnimationStart(Animator animation) {
    }

    public void onAnimationRepeat(Animator animation) {
    }

    public void onAnimationEnd(Animator animation) {
        UndoBarRequest.access$800(this.this$1).setVisibility(8);
        UndoBarRequest.access$900(this.this$1).removeView(UndoBarRequest.access$800(this.this$1));
    }

    public void onAnimationCancel(Animator animation) {
    }
}
