package com.zendesk.sdk.network;

public interface NetworkAware {
    void onNetworkAvailable();

    void onNetworkUnavailable();
}
