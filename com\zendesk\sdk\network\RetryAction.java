package com.zendesk.sdk.network;

public interface RetryAction {
    void onRetry();
}
