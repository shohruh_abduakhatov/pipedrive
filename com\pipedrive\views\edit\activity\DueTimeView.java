package com.pipedrive.views.edit.activity;

import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.TimePicker;
import com.pipedrive.R;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.ScreensMapper;
import com.pipedrive.application.Session;
import com.pipedrive.datetime.LocaleHelper;
import com.pipedrive.datetime.PipedriveDate;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.datetime.PipedriveTime;
import com.pipedrive.model.Activity;
import com.pipedrive.model.Activity.DateTimeInfo;
import com.pipedrive.util.time.TimeManager;
import com.pipedrive.views.common.ClickableTextViewWithUnderlineAndLabel;
import java.util.Calendar;

public class DueTimeView extends ClickableTextViewWithUnderlineAndLabel {

    public interface OnDueTimeChangedListener {
        void onDueTimeChanged(@NonNull PipedriveTime pipedriveTime);
    }

    public interface OnDueTimeClearedListener {
        void onDueTimeCleared();
    }

    public DueTimeView(Context context) {
        this(context, null);
    }

    public DueTimeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DueTimeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected int getLabelTextResourceId() {
        return R.string.time;
    }

    public void setTime(@NonNull final Session session, @NonNull Activity activity) {
        setText("");
        activity.request(new DateTimeInfo() {
            public void activityHasStartDateTime(@NonNull PipedriveDateTime dateTime, @NonNull Activity inActivity) {
                DueTimeView.this.setText(LocaleHelper.getLocaleBasedTimeStringInCurrentTimeZone(session, dateTime));
            }

            public void activityHasStartDate(@NonNull PipedriveDate date, @NonNull Activity inActivity) {
                DueTimeView.this.setText("");
            }
        });
    }

    public void setDueTimeListener(@NonNull final Activity activity, @Nullable final OnDueTimeChangedListener onDueTimeChangedListener, @Nullable final OnDueTimeClearedListener onDueTimeClearedListener) {
        setOnClickListener(new OnClickListener() {
            private final boolean TIME_PICKER_onTimeSet_IS_CALLED_AFTER_BUTTON_NEUTRAL_CLICK;
            private boolean onClearClickListenerClicked;

            public void onClick(View v) {
                final OnTimeSetListener onTimeSetListener = new OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (AnonymousClass2.this.TIME_PICKER_onTimeSet_IS_CALLED_AFTER_BUTTON_NEUTRAL_CLICK && AnonymousClass2.this.onClearClickListenerClicked) {
                            AnonymousClass2.this.onClearClickListenerClicked = false;
                        } else if (onDueTimeChangedListener != null) {
                            onDueTimeChangedListener.onDueTimeChanged(PipedriveTime.from(hourOfDay, minute));
                        }
                    }
                };
                final OnClickListener onClearClickListener = new OnClickListener() {
                    public void onClick(@Nullable View v) {
                        if (AnonymousClass2.this.TIME_PICKER_onTimeSet_IS_CALLED_AFTER_BUTTON_NEUTRAL_CLICK) {
                            AnonymousClass2.this.onClearClickListenerClicked = true;
                        }
                        ((TextView) DueTimeView.this.getMainView()).setText(null);
                        if (onDueTimeClearedListener != null) {
                            onDueTimeClearedListener.onDueTimeCleared();
                        }
                    }
                };
                activity.request(new DateTimeInfo() {
                    public void activityHasStartDateTime(@NonNull PipedriveDateTime dateTime, @NonNull Activity inActivity) {
                        Calendar activityDueTimeCalendar = dateTime.toLocalCalendar();
                        DueTimeView.this.showTimePicker(onTimeSetListener, onClearClickListener, true, activityDueTimeCalendar.get(11), activityDueTimeCalendar.get(12));
                    }

                    public void activityHasStartDate(@NonNull PipedriveDate date, @NonNull Activity inActivity) {
                        Calendar calendarNow = TimeManager.getInstance().getLocalCalendar();
                        DueTimeView.this.showTimePicker(onTimeSetListener, onClearClickListener, false, calendarNow.get(11), calendarNow.get(12));
                    }
                });
            }
        });
    }

    private void showTimePicker(@NonNull OnTimeSetListener onTimeSetListener, @NonNull final OnClickListener onClearClickListener, boolean addClearTimeButton, int hourOfDay, int minute) {
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), R.style.Theme.Pipedrive.Dialog.Alert, onTimeSetListener, hourOfDay, minute, DateFormat.is24HourFormat(getContext()));
        if (addClearTimeButton) {
            timePickerDialog.setButton(-3, getResources().getText(R.string.clear).toString().toUpperCase(), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (which == -3) {
                        onClearClickListener.onClick(null);
                    }
                }
            });
        }
        Analytics.hitScreen(ScreensMapper.SCREEN_NAME_ACTIVITY_TIME_DIALOG, getContext());
        timePickerDialog.show();
    }
}
