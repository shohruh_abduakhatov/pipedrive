package com.pipedrive.customfields.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.customfields.views.CustomFieldListView.OnItemClickListener;
import com.pipedrive.model.customfields.CustomField;
import java.util.List;

public class CustomFieldListAdapter extends Adapter {
    private static final int HEADER_POSITION = 1;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM_ADDRESS = 4;
    private static final int TYPE_ITEM_EMPTY = 5;
    private static final int TYPE_ITEM_GENERIC = 1;
    private static final int TYPE_ITEM_INFO = 3;
    private static final int TYPE_ITEM_PHONE = 2;
    private static final int TYPE_SPECIAL_FIELDS = 6;
    @NonNull
    private final Long mItemSqlId;
    @NonNull
    private final List<CustomField> mItems;
    @Nullable
    private OnItemClickListener mOnItemClickListener;
    @NonNull
    private final Session mSession;
    @NonNull
    private final String mType;

    private static class CustomFieldsHeaderViewHolder extends ViewHolder {
        public CustomFieldsHeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    public CustomFieldListAdapter(@NonNull Session session, @NonNull List<CustomField> items, @NonNull String type, @NonNull Long itemSqlId) {
        this.mSession = session;
        this.mItems = items;
        this.mType = type;
        this.mItemSqlId = itemSqlId;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0:
                return new CustomFieldsHeaderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_custom_field_header, parent, false));
            case 2:
                return new CustomFieldViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_custom_field_phone, parent, false));
            case 3:
                return new CustomFieldViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_custom_field_info, parent, false));
            case 4:
                return new CustomFieldViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_custom_field_address, parent, false));
            case 5:
                return new CustomFieldViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_custom_field_empty, parent, false));
            case 6:
                String str = this.mType;
                boolean z = true;
                switch (str.hashCode()) {
                    case -991716523:
                        if (str.equals("person")) {
                            z = true;
                            break;
                        }
                        break;
                    case 3079276:
                        if (str.equals("deal")) {
                            z = false;
                            break;
                        }
                        break;
                    case 1178922291:
                        if (str.equals("organization")) {
                            z = true;
                            break;
                        }
                        break;
                }
                switch (z) {
                    case false:
                        return new DealSpecialCustomFieldViewHolder(parent.getContext(), this.mItemSqlId);
                    case true:
                        return new PersonSpecialCustomFieldViewHolder(parent.getContext(), this.mItemSqlId);
                    case true:
                        return new DummySpecialCustomFieldViewHolder(parent.getContext(), this.mItemSqlId);
                    default:
                        break;
                }
        }
        return new CustomFieldViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_custom_field, parent, false));
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case 0:
                return;
            case 5:
                ((CustomFieldViewHolder) holder).bindEmpty(getItem(position), this.mOnItemClickListener);
                return;
            case 6:
                ((SpecialCustomFieldViewHolder) holder).bind(this.mSession, this.mOnItemClickListener);
                return;
            default:
                ((CustomFieldViewHolder) holder).bind(this.mSession, getItem(position), this.mOnItemClickListener);
                return;
        }
    }

    @NonNull
    private CustomField getItem(int position) {
        return (CustomField) this.mItems.get((position - 1) - 1);
    }

    public int getItemViewType(int position) {
        if (position == 1) {
            return 0;
        }
        if (position < 1) {
            return 6;
        }
        CustomField customField = getItem(position);
        String fieldDataType = customField.getFieldDataType();
        if (customField.isEmpty()) {
            return 5;
        }
        if ("address".equalsIgnoreCase(fieldDataType)) {
            return 4;
        }
        if ("phone".equalsIgnoreCase(fieldDataType)) {
            return 2;
        }
        if ("person".equalsIgnoreCase(fieldDataType) || "organization".equalsIgnoreCase(fieldDataType)) {
            return 3;
        }
        return 1;
    }

    public int getItemCount() {
        return ((this.mItems.isEmpty() ? 0 : 1) + 1) + this.mItems.size();
    }

    public void setOnItemClickListener(@Nullable OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }
}
