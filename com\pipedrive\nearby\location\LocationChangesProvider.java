package com.pipedrive.nearby.location;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.LocationSource.OnLocationChangedListener;
import com.google.android.gms.maps.model.LatLng;
import com.pipedrive.logging.Log;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.subjects.BehaviorSubject;
import rx.subjects.Subject;

public final class LocationChangesProvider implements LocationListener, LocationManager {
    @NonNull
    private static final LocationChangesProvider instance = new LocationChangesProvider();
    @NonNull
    private final Subject<Location, Location> locationChanges = BehaviorSubject.create().toSerialized();

    @NonNull
    public static LocationChangesProvider getInstance() {
        return instance;
    }

    private LocationChangesProvider() {
    }

    public void onLocationChanged(Location location) {
        this.locationChanges.onNext(location);
    }

    @NonNull
    public LocationSource getLocationSource() {
        return new LocationSource() {
            @Nullable
            private Subscription subscription;

            public void activate(final OnLocationChangedListener onLocationChangedListener) {
                this.subscription = LocationChangesProvider.this.locationChanges.subscribe(new Action1<Location>() {
                    public void call(Location location) {
                        onLocationChangedListener.onLocationChanged(location);
                    }
                }, new Action1<Throwable>() {
                    public void call(Throwable throwable) {
                        Log.e(throwable);
                    }
                });
            }

            public void deactivate() {
                if (this.subscription != null && !this.subscription.isUnsubscribed()) {
                    this.subscription.unsubscribe();
                }
            }
        };
    }

    @NonNull
    public Observable<LatLng> locationChanges() {
        return this.locationChanges.map(new Func1<Location, LatLng>() {
            public LatLng call(Location location) {
                return new LatLng(location.getLatitude(), location.getLongitude());
            }
        }).distinctUntilChanged();
    }
}
