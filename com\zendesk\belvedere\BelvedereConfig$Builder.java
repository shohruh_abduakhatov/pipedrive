package com.zendesk.belvedere;

import android.content.Context;
import java.util.Arrays;
import java.util.TreeSet;

public class BelvedereConfig$Builder {
    private boolean mAllowMultiple = true;
    private BelvedereLogger mBelvedereLogger = new DefaultLogger();
    private int mCameraRequestCodeEnd = 1653;
    private int mCameraRequestCodeStart = 1603;
    private String mContentType = "*/*";
    private Context mContext;
    private boolean mDebugEnabled = false;
    private String mDirectory = "belvedere-data";
    private int mGalleryRequestCode = 1602;
    private TreeSet<BelvedereSource> mSources = new TreeSet(Arrays.asList(new BelvedereSource[]{BelvedereSource.Camera, BelvedereSource.Gallery}));

    BelvedereConfig$Builder(Context context) {
        this.mContext = context;
    }

    public BelvedereConfig$Builder withGalleryRequestCode(int requestCode) {
        this.mGalleryRequestCode = requestCode;
        return this;
    }

    public BelvedereConfig$Builder withCameraRequestCode(int startRequestCode, int endRequestCode) {
        if (endRequestCode - startRequestCode < 5) {
            throw new IllegalArgumentException("The following formula must be apply for the given arguments: (endRequestCode - startRequestCode) >= 5");
        }
        this.mCameraRequestCodeStart = startRequestCode;
        this.mCameraRequestCodeEnd = endRequestCode;
        return this;
    }

    public BelvedereConfig$Builder withAllowMultiple(boolean allowMultiple) {
        this.mAllowMultiple = allowMultiple;
        return this;
    }

    public BelvedereConfig$Builder withContentType(String contentType) {
        this.mContentType = contentType;
        return this;
    }

    public BelvedereConfig$Builder withCustomLogger(BelvedereLogger belvedereLogger) {
        if (belvedereLogger != null) {
            this.mBelvedereLogger = belvedereLogger;
            return this;
        }
        throw new IllegalArgumentException("Invalid logger provided");
    }

    public BelvedereConfig$Builder withSource(BelvedereSource... sources) {
        if (sources == null || sources.length == 0) {
            throw new IllegalArgumentException("Please provide at least one source");
        }
        this.mSources = new TreeSet(Arrays.asList(sources));
        return this;
    }

    public BelvedereConfig$Builder withDebug(boolean enabled) {
        this.mDebugEnabled = enabled;
        return this;
    }

    public Belvedere build() {
        this.mBelvedereLogger.setLoggable(this.mDebugEnabled);
        return new Belvedere(this.mContext, new BelvedereConfig(this));
    }
}
