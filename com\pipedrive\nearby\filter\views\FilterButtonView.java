package com.pipedrive.nearby.filter.views;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView.ScaleType;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.filter.FilterEventBus;
import com.pipedrive.nearby.model.NearbyItemType;
import com.pipedrive.nearby.toolbar.NearbyItemTypeChangesProvider;
import com.pipedrive.nearby.util.Irrelevant;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;
import rx.subscriptions.CompositeSubscription;

public class FilterButtonView extends FloatingActionButton implements FilterButton {
    @NonNull
    private final CompositeSubscription compositeSubscription = new CompositeSubscription();
    @NonNull
    private final Subject<Irrelevant, Irrelevant> filterButtonClicks = PublishSubject.create();
    @Nullable
    private NearbyItemTypeChangesProvider nearbyItemTypeChangesProvider;
    @Nullable
    private Session session;

    public FilterButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupButton(context);
    }

    private void setupButton(Context context) {
        setImageDrawable(VectorDrawableCompat.create(getResources(), R.drawable.ic_filter, context.getTheme()));
        setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.white)));
        setSize(1);
        setScaleType(ScaleType.CENTER_INSIDE);
        setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                FilterButtonView.this.filterButtonClicks.onNext(Irrelevant.INSTANCE);
            }
        });
    }

    @NonNull
    public Observable<Irrelevant> filterButtonClicks() {
        return this.filterButtonClicks;
    }

    public void setup(@NonNull Session session, @NonNull NearbyItemTypeChangesProvider nearbyItemTypeChangesProvider) {
        this.session = session;
        this.nearbyItemTypeChangesProvider = nearbyItemTypeChangesProvider;
    }

    public void bind() {
        if (!(this.nearbyItemTypeChangesProvider == null || this.session == null)) {
            this.compositeSubscription.add(this.nearbyItemTypeChangesProvider.nearbyItemTypeChanges(this.session).subscribeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<NearbyItemType>() {
                public void call(NearbyItemType nearbyItemType) {
                    FilterButtonView.this.setVisibility(8);
                }
            }, new Action1<Throwable>() {
                public void call(Throwable throwable) {
                }
            }));
        }
        this.compositeSubscription.add(FilterEventBus.INSTANCE.getFilterReady().subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Irrelevant>() {
            public void call(Irrelevant irrelevant) {
                FilterButtonView.this.setVisibility(0);
            }
        }, new Action1<Throwable>() {
            public void call(Throwable throwable) {
            }
        }));
    }

    public void releaseResources() {
        this.compositeSubscription.clear();
    }
}
