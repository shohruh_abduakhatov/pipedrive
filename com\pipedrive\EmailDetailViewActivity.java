package com.pipedrive;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.GridLayout;
import android.widget.TextView;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.EmailMessagesDataSource;
import com.pipedrive.datetime.LocaleHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.model.email.EmailMessage;
import com.pipedrive.model.email.EmailParticipant;
import com.pipedrive.util.EmailHelper;
import com.pipedrive.util.StringUtils;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.util.viewhelper.HTMLContentEditableWebViewHelper;
import com.pipedrive.views.email.detail.AttachmentListView;
import com.pipedrive.views.email.detail.MainScrollView;
import java.util.List;

public class EmailDetailViewActivity extends BaseActivityWithUpEnabled {
    private static final String EXTRA_EMAIL_MESSAGE_DROPBOX_BCC = "com.pipedrive.EmailDetailViewActivity.EXTRA_EMAIL_MESSAGE_DROPBOX_BCC";
    private static final String EXTRA_EMAIL_MESSAGE_SQL_ID = "com.pipedrive.EmailDetailViewActivity.EXTRA_EMAIL_MESSAGE_SQL_ID";
    private WebView emailDetailEmailBody;
    private TextView emailDetailEmailTime;
    private TextView emailDetailFromList;
    private GridLayout emailDetailParticipantsDetail;
    private TextView emailDetailSubject;
    private TextView emailDetailToList;
    private EmailMessage emailMessage;
    private long emailMessageSqlId = 0;
    private AttachmentListView mAttachmentListView;
    private TextView showDetailsSwitch;

    public static void startActivity(Context context, long emailMessageSqlId, String bccDropboxEmailAddress) {
        Intent intent = new Intent(context, EmailDetailViewActivity.class).putExtra(EXTRA_EMAIL_MESSAGE_SQL_ID, emailMessageSqlId).setFlags(8388608);
        if (!StringUtils.isTrimmedAndEmpty(bccDropboxEmailAddress)) {
            intent.putExtra(EXTRA_EMAIL_MESSAGE_DROPBOX_BCC, bccDropboxEmailAddress);
        }
        context.startActivity(intent);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().hasExtra(EXTRA_EMAIL_MESSAGE_SQL_ID)) {
            this.emailMessageSqlId = getIntent().getLongExtra(EXTRA_EMAIL_MESSAGE_SQL_ID, 0);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
            setContentView((int) R.layout.activity_email_detail);
            MainScrollView emailDetailMainView = (MainScrollView) findViewById(R.id.emailDetailMainView);
            this.emailDetailEmailBody = (WebView) findViewById(R.id.emailDetailEmailBody);
            emailDetailMainView.setScalableView(this.emailDetailEmailBody, findViewById(R.id.emailDetailEmailBodyContainer));
            this.emailDetailParticipantsDetail = (GridLayout) findViewById(R.id.emailDetailParticipantsDetail);
            this.emailDetailSubject = (TextView) findViewById(R.id.emailDetailSubject);
            this.emailDetailFromList = (TextView) findViewById(R.id.emailDetailFromList);
            this.emailDetailToList = (TextView) findViewById(R.id.emailDetailToList);
            this.emailDetailEmailTime = (TextView) findViewById(R.id.emailDetailEmailTime);
            this.mAttachmentListView = (AttachmentListView) findViewById(R.id.attachment_list);
            return;
        }
        Log.e(new Throwable("Need email message SQL ID to launch. Finishing!"));
        finish();
    }

    public void onResume() {
        super.onResume();
        if (PipedriveApp.getSessionManager().hasActiveSession()) {
            Session session = PipedriveApp.getActiveSession();
            if (this.emailDetailParticipantsDetail != null) {
                this.emailMessage = getEmailMessage(session, this.emailMessageSqlId);
                if (this.emailMessage == null) {
                    Log.e(new Throwable(String.format("No email message with SQL ID %s found. Finishing!", new Object[]{Long.valueOf(this.emailMessageSqlId)})));
                    finish();
                    return;
                }
                int i;
                if (this.emailMessage.getBody() == null || this.emailMessage.getBody().length <= 0) {
                    this.emailDetailEmailBody.setVisibility(8);
                    findViewById(R.id.emailDetailEmailBodyNotPresent).setVisibility(0);
                } else {
                    this.emailDetailEmailBody.loadData(EmailHelper.decorateDefineBlockQuoteCSS(new String(this.emailMessage.getBody())), HTMLContentEditableWebViewHelper.MIME_TYPE, null);
                }
                this.emailDetailSubject.setText(TextUtils.isEmpty(this.emailMessage.getSubject()) ? getString(R.string.lbl_email_no_subject) : this.emailMessage.getSubject());
                if (this.emailMessage.getFrom().isEmpty()) {
                    this.emailDetailFromList.setText(R.string.lbl_email_no_recipients);
                } else {
                    fillTextViewWithParticipants(this.emailDetailFromList, this.emailMessage.getFrom());
                }
                if (this.emailMessage.getTo().isEmpty()) {
                    this.emailDetailToList.setText(R.string.lbl_email_no_recipients);
                } else {
                    fillTextViewWithParticipants(this.emailDetailToList, this.emailMessage.getTo());
                }
                if (this.emailMessage.getFrom().isEmpty()) {
                    findViewById(R.id.emailDetailFromAvatarLayout).setVisibility(8);
                } else {
                    ((TextView) findViewById(R.id.emailDetailFromAvatarInitials)).setText(fillAvatarInitials(this.emailMessage.getFrom()));
                }
                String labelShowLess = getResources().getString(R.string.btn_email_show_participants_details_less);
                this.showDetailsSwitch = (TextView) findViewById(R.id.emailDetailEmailDetailsLink);
                this.showDetailsSwitch.setText(labelShowLess);
                onShowMessageParticipantDetails(this.showDetailsSwitch);
                this.mAttachmentListView.setEmailMessage(this.emailMessage);
                AttachmentListView attachmentListView = this.mAttachmentListView;
                if (this.emailMessage.getAttachments().isEmpty()) {
                    i = 8;
                } else {
                    i = 0;
                }
                attachmentListView.setVisibility(i);
                return;
            }
            return;
        }
        Log.e(new Throwable("No active session. Finishing!"));
        finish();
    }

    public void onShowMessageParticipantDetails(View v) {
        v = this.showDetailsSwitch;
        if (v != null && this.emailDetailParticipantsDetail != null) {
            CharSequence charSequence;
            TextView showDetailedParticipantsSwitch = (TextView) v;
            String labelDetails = v.getResources().getString(R.string.btn_email_show_participants_details_more);
            String labelShowLess = v.getResources().getString(R.string.btn_email_show_participants_details_less);
            if (showDetailedParticipantsSwitch.getText().equals(labelDetails)) {
                charSequence = labelShowLess;
            } else {
                Object obj = labelDetails;
            }
            showDetailedParticipantsSwitch.setText(charSequence);
            boolean showDetailParticipants = TextUtils.equals(showDetailedParticipantsSwitch.getText(), labelShowLess);
            if (!showDetailParticipants) {
                labelShowLess = labelDetails;
            }
            showDetailedParticipantsSwitch.setText(labelShowLess);
            if (showDetailParticipants) {
                LayoutInflater layoutInflater = getLayoutInflater();
                inflateParticipantsRow(layoutInflater, this.emailDetailParticipantsDetail, getResources().getString(R.string.lbl_email_participants_type_from), this.emailMessage.getFrom());
                inflateParticipantsRow(layoutInflater, this.emailDetailParticipantsDetail, getResources().getString(R.string.lbl_email_participants_type_to), this.emailMessage.getTo());
                inflateParticipantsRow(layoutInflater, this.emailDetailParticipantsDetail, getResources().getString(R.string.lbl_email_participants_type_cc), this.emailMessage.getCc());
                inflateParticipantsRow(layoutInflater, this.emailDetailParticipantsDetail, getResources().getString(R.string.lbl_email_participants_type_bcc), this.emailMessage.getBcc());
            } else {
                this.emailDetailParticipantsDetail.removeAllViews();
            }
            if (this.emailMessage != null && this.emailMessage.getMessageTime() != null) {
                Session activeSession = PipedriveApp.getActiveSession();
                if (activeSession != null) {
                    TextView textView = this.emailDetailEmailTime;
                    if (showDetailParticipants) {
                        charSequence = LocaleHelper.getLocaleBasedDateTimeStringInCurrentTimeZone(activeSession, this.emailMessage.getMessageTime());
                    } else {
                        charSequence = LocaleHelper.getLocaleBasedDateStringInCurrentTimeZone(activeSession, this.emailMessage.getMessageTime());
                    }
                    textView.setText(charSequence);
                    this.emailDetailEmailTime.setVisibility(0);
                    return;
                }
                this.emailDetailEmailTime.setVisibility(8);
            }
        }
    }

    private String fillAvatarInitials(List<EmailParticipant> from) {
        if (from == null || from.isEmpty()) {
            return "";
        }
        return StringUtils.getInitials((EmailParticipant) from.get(0));
    }

    private void fillTextViewWithParticipants(TextView textView, List<EmailParticipant> participants) {
        if (textView != null && participants != null) {
            StringBuilder participantsAsString = new StringBuilder();
            for (int i = 0; i < participants.size(); i++) {
                if (i > 0) {
                    participantsAsString.append(", ");
                }
                participantsAsString.append(((EmailParticipant) participants.get(i)).getName());
            }
            textView.setText(participantsAsString.toString());
        }
    }

    private void inflateParticipantsRow(LayoutInflater layoutInflater, GridLayout rootGridLayout, CharSequence sectionTitle, List<EmailParticipant> participants) {
        if (participants != null && !participants.isEmpty()) {
            TextView sectionTitleView = (TextView) layoutInflater.inflate(R.layout.layout_email_participant_row_title, rootGridLayout, false);
            if (sectionTitle == null) {
                sectionTitle = "";
            }
            sectionTitleView.setText(sectionTitle);
            rootGridLayout.addView(sectionTitleView);
            for (EmailParticipant participant : participants) {
                TextView participantView = (TextView) layoutInflater.inflate(R.layout.layout_email_participant_row_participant, rootGridLayout, false);
                decorateParticipantTextView(participantView, participant.getName(), participant.getEmail());
                rootGridLayout.addView(participantView);
            }
            rootGridLayout.addView((TextView) layoutInflater.inflate(R.layout.layout_email_participant_row_title, rootGridLayout, false));
        }
    }

    private void decorateParticipantTextView(TextView textView, String participantName, String participantEmail) {
        if (textView != null) {
            if (participantName == null) {
                participantName = "";
            }
            if (participantEmail == null) {
                participantEmail = "";
            }
            String spannableContent = participantName + " " + participantEmail;
            CharSequence spannableString = new SpannableString(spannableContent);
            if (!StringUtils.isTrimmedAndEmpty(spannableContent)) {
                int nameEnd = participantName.length();
                if (!StringUtils.isTrimmedAndEmpty(participantEmail)) {
                    int emailMatchStart = nameEnd + 1;
                    int emailMatchEnd = emailMatchStart + participantEmail.length();
                    spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.text_color_lighter_grey)), emailMatchStart, emailMatchEnd, 0);
                    spannableString.setSpan(new TextAppearanceSpan(null, 0, getResources().getDimensionPixelSize(R.dimen.dimen3), null, null), emailMatchStart, emailMatchEnd, 0);
                }
                spannableString.setSpan(new StyleSpan(1), 0, nameEnd, 0);
                spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.text_color_dark)), 0, nameEnd, 0);
                spannableString.setSpan(new TextAppearanceSpan(null, 0, getResources().getDimensionPixelSize(R.dimen.email_detail_participants_details_name_size), null, null), 0, nameEnd, 0);
            }
            textView.setText(spannableString);
        }
    }

    private EmailMessage getEmailMessage(Session session, long emailMessageSqlId) {
        if (emailMessageSqlId > 0) {
            return (EmailMessage) new EmailMessagesDataSource(session.getDatabase()).findBySqlId(emailMessageSqlId);
        }
        return null;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_email_details, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        String[] additionalBcc = getIntent().hasExtra(EXTRA_EMAIL_MESSAGE_DROPBOX_BCC) ? new String[]{getIntent().getStringExtra(EXTRA_EMAIL_MESSAGE_DROPBOX_BCC)} : null;
        switch (item.getItemId()) {
            case R.id.menu_reply:
                if (EmailHelper.composeAndSendEmailReply(this, this.emailMessage, additionalBcc)) {
                    return true;
                }
                ViewUtil.showErrorToast(this, R.string.message_email_client_not_found);
                return true;
            case R.id.menu_reply_all:
                if (EmailHelper.composeAndSendEmailReplyAll(this, this.emailMessage, additionalBcc)) {
                    return true;
                }
                ViewUtil.showErrorToast(this, R.string.message_email_client_not_found);
                return true;
            case R.id.menu_forward:
                if (EmailHelper.composeAndSendEmailForward(this, this.emailMessage, additionalBcc)) {
                    return true;
                }
                ViewUtil.showErrorToast(this, R.string.message_email_client_not_found);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
