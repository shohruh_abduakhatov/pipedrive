package com.pipedrive.deal.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.pipedrive.BaseActivity;
import com.pipedrive.CustomFieldEditActivity;
import com.pipedrive.EmailDetailViewActivity;
import com.pipedrive.FileOpenActivity;
import com.pipedrive.OrganizationDetailActivity;
import com.pipedrive.PersonDetailActivity;
import com.pipedrive.R;
import com.pipedrive.activity.ActivityDetailActivity;
import com.pipedrive.application.Session;
import com.pipedrive.call.CallSummaryListener;
import com.pipedrive.call.CallSummaryListener.CallSummaryRegistration;
import com.pipedrive.customfields.views.CustomFieldListView;
import com.pipedrive.customfields.views.DealCustomFieldListView;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.deal.DealEditActivity;
import com.pipedrive.dialogs.communicationmediumlist.CommunicationMediumDialogController;
import com.pipedrive.dialogs.communicationmediumlist.CommunicationMediumListDialogFragment;
import com.pipedrive.flow.views.DealFlowView;
import com.pipedrive.flow.views.FlowView.OnItemClickListener;
import com.pipedrive.model.Deal;
import com.pipedrive.model.DealStatus;
import com.pipedrive.model.FlowFile;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.contact.CommunicationMedium;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.note.NoteCreateActivity;
import com.pipedrive.note.NoteUpdateActivity;
import com.pipedrive.products.DealProductListActivity;
import com.pipedrive.store.StoreFlowFile;
import com.pipedrive.util.EmailHelper;
import com.pipedrive.util.PipedriveUtil;
import com.pipedrive.util.camera.CameraHelper;
import com.pipedrive.util.camera.CameraHelper.OnCameraListener;
import com.pipedrive.util.communicatiomedium.DealCommunicationMediumUtil;
import com.pipedrive.util.filepicker.DocumentPickerHelper;
import com.pipedrive.util.filepicker.DocumentPickerHelper.OnDocumentPickerListener;
import com.pipedrive.util.formatter.MonetaryFormatter;
import com.pipedrive.util.snackbar.SnackBarManager;
import com.pipedrive.views.DealStateButtonsView;
import com.pipedrive.views.DealStateButtonsView.OnClickListener;
import com.pipedrive.views.fab.FabMenuClickListener;
import com.pipedrive.views.fab.FabWithOverlayMenu;
import com.pipedrive.views.stageselector.StagesSelector;
import java.util.List;

public final class DealDetailsActivity extends BaseActivity implements OnItemClickListener, CustomFieldListView.OnItemClickListener, DealDetailsView, CommunicationMediumDialogController<Deal>, OnClickListener, FabMenuClickListener {
    private static final String KEY_DEAL_ID = "com.pipedrive.DEAL_ID";
    @Nullable
    private CameraHelper mCameraHelper;
    @NonNull
    private DealCustomFieldListView mDealCustomFieldListView;
    @NonNull
    private DealFlowView mDealFlowView;
    @BindView(2131820968)
    protected DealStateButtonsView mDealStateButtonsView;
    @Nullable
    private DocumentPickerHelper mDocumentPickerHelper;
    @BindView(2131820744)
    protected FabWithOverlayMenu mFabWithOverlayMenu;
    @BindView(2131820963)
    protected TextView mOrganization;
    private int mOrganizationViewWidth = 0;
    @BindView(2131820962)
    protected TextView mPerson;
    private int mPersonOrgRootViewWidth = 0;
    private int mPersonViewWidth = 0;
    @NonNull
    private DealDetailPresenter mPresenter;
    @BindView(2131820967)
    protected TextView mProducts;
    @BindView(2131820965)
    protected View mProductsAndValueContainer;
    @BindView(2131820964)
    protected StagesSelector mStagesSelector;
    @BindView(2131820743)
    protected TabLayout mTabLayout;
    @BindView(2131820961)
    protected TextView mTitle;
    @BindView(2131820742)
    protected Toolbar mToolbar;
    @BindView(2131820966)
    protected TextView mValue;
    @BindView(2131820741)
    protected ViewPager mViewPager;

    private class DeaLDetailPagerAdapter extends PagerAdapter {
        private DeaLDetailPagerAdapter() {
        }

        public Object instantiateItem(ViewGroup container, int position) {
            View page = position == 0 ? DealDetailsActivity.this.mDealFlowView : DealDetailsActivity.this.mDealCustomFieldListView;
            container.addView(page, new LayoutParams(-1, -1));
            return page;
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        public int getCount() {
            return 2;
        }

        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return DealDetailsActivity.this.getString(R.string.tab_title_flow);
            }
            return DealDetailsActivity.this.getString(R.string.tab_title_details);
        }
    }

    public static void start(@NonNull Activity activity, @NonNull Long dealId) {
        activity.startActivity(new Intent(activity, DealDetailsActivity.class).putExtra(KEY_DEAL_ID, dealId));
    }

    public void onActivityClicked(long activitySqlId) {
        ActivityDetailActivity.startActivityForActivity((Activity) this, activitySqlId);
    }

    public void onNoteClicked(long noteSqlId) {
        NoteUpdateActivity.updateNote((Context) this, noteSqlId);
    }

    public void onEmailClicked(long emailSqlId) {
        Deal deal = (Deal) new DealsDataSource(getSession().getDatabase()).findBySqlId(getDealId().longValue());
        EmailDetailViewActivity.startActivity(this, emailSqlId, deal == null ? null : deal.getDropBoxAddress());
    }

    private Long getDealId() {
        return (Long) getIntent().getSerializableExtra(KEY_DEAL_ID);
    }

    public void onFileClicked(long fileSqlId) {
        FileOpenActivity.startActivityForFlowFile(this, fileSqlId);
    }

    public void onActivityDoneChanged(long activitySqlId, boolean done) {
        this.mPresenter.toggleActivityDoneStatus(activitySqlId, done);
    }

    public void onNewActivityClicked() {
        ActivityDetailActivity.startActivityForDeal((Context) this, getDealId().longValue());
    }

    public void onCustomFieldClicked(@NonNull CustomField customField) {
        CustomFieldEditActivity.startActivity(this, customField, getDealId().longValue());
    }

    public void onCustomFieldLongClicked(@NonNull String value) {
        PipedriveUtil.copyTextToClipboard(value, this);
    }

    public void onPhoneClicked(@NonNull String number, boolean ignored) {
        PipedriveUtil.call(this, number);
    }

    public void onAddressClicked(@NonNull Uri addressUri) {
        startActivity(new Intent("android.intent.action.VIEW", addressUri));
    }

    @OnClick({2131820965})
    void onProductsAndValueContainerClicked() {
        if (this.mPresenter.getDeal() != null && this.mPresenter.getDeal().getSqlIdOrNull() != null) {
            DealProductListActivity.startActivity(this, this.mPresenter.getDeal().getSqlIdOrNull());
        }
    }

    public void onResume() {
        super.onResume();
        this.mPresenter.bindView(this);
        this.mDealFlowView.refreshContent(getSession());
        this.mPresenter.retrieveDeal(getDealId());
        this.mDealCustomFieldListView.refreshContent(getSession());
    }

    protected void onPause() {
        super.onPause();
        this.mPresenter.unbindView();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_deal_details);
        ButterKnife.bind((Activity) this);
        this.mDealFlowView = new DealFlowView(this);
        this.mDealFlowView.setup(getSession(), getDealId(), this);
        this.mDealCustomFieldListView = new DealCustomFieldListView(this);
        this.mDealCustomFieldListView.setup(getSession(), getDealId(), this);
        this.mPresenter = new DealDetailPresenterImpl(getSession());
        this.mPresenter.bindView(this);
        this.mCameraHelper = new CameraHelper();
        this.mCameraHelper.onCreate(savedInstanceState);
        this.mDocumentPickerHelper = new DocumentPickerHelper();
        setupActionBar();
        setupViewPager();
        setupLayoutChangeListeners();
        setupFab();
        this.mPresenter.retrieveDeal(getDealId());
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (this.mCameraHelper != null) {
            this.mCameraHelper.onSaveInstanceState(outState);
        }
    }

    private void setupFab() {
        this.mFabWithOverlayMenu.setMenu(R.menu.menu_fab_deal, R.id.action_add_note);
        this.mFabWithOverlayMenu.setOnMenuClickListener(this);
    }

    private void setupActionBar() {
        setSupportActionBar(this.mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setupProgressBar(this.mToolbar);
    }

    private void setupViewPager() {
        this.mViewPager.setAdapter(new DeaLDetailPagerAdapter());
        this.mTabLayout.setupWithViewPager(this.mViewPager);
    }

    private void setupLayoutChangeListeners() {
        this.mPerson.addOnLayoutChangeListener(new OnLayoutChangeListener() {
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                DealDetailsActivity.this.mPersonViewWidth = right - left;
                DealDetailsActivity.this.resizeContactsAndCompanyLabelsWidths();
            }
        });
        this.mOrganization.addOnLayoutChangeListener(new OnLayoutChangeListener() {
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                DealDetailsActivity.this.mOrganizationViewWidth = right - left;
                DealDetailsActivity.this.resizeContactsAndCompanyLabelsWidths();
            }
        });
        ((View) this.mPerson.getParent()).addOnLayoutChangeListener(new OnLayoutChangeListener() {
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                DealDetailsActivity.this.mPersonOrgRootViewWidth = right - left;
                DealDetailsActivity.this.resizeContactsAndCompanyLabelsWidths();
            }
        });
    }

    private void resizeContactsAndCompanyLabelsWidths() {
        if (this.mPersonViewWidth > 0 && this.mOrganizationViewWidth > 0 && this.mPersonOrgRootViewWidth > 0) {
            int halfOfScreen = this.mPersonOrgRootViewWidth / 2;
            int marginBetweenPersonAndOrg = getResources().getDimensionPixelSize(R.dimen.dimen6);
            if ((this.mOrganizationViewWidth + this.mPersonViewWidth) + marginBetweenPersonAndOrg > this.mPersonOrgRootViewWidth) {
                if (this.mPersonViewWidth > halfOfScreen && this.mOrganizationViewWidth > halfOfScreen) {
                    this.mPersonViewWidth = halfOfScreen - (marginBetweenPersonAndOrg / 2);
                    this.mOrganizationViewWidth = halfOfScreen - (marginBetweenPersonAndOrg / 2);
                } else if (this.mPersonViewWidth > halfOfScreen) {
                    if (this.mPersonViewWidth > this.mPersonOrgRootViewWidth) {
                        this.mPersonViewWidth = this.mPersonOrgRootViewWidth;
                    }
                    this.mPersonViewWidth -= ((this.mPersonViewWidth + marginBetweenPersonAndOrg) + this.mOrganizationViewWidth) - this.mPersonOrgRootViewWidth;
                } else if (this.mOrganizationViewWidth > halfOfScreen) {
                    if (this.mOrganizationViewWidth > this.mPersonOrgRootViewWidth) {
                        this.mOrganizationViewWidth = this.mPersonOrgRootViewWidth;
                    }
                    this.mOrganizationViewWidth -= ((this.mPersonViewWidth + marginBetweenPersonAndOrg) + this.mOrganizationViewWidth) - this.mPersonOrgRootViewWidth;
                }
                LayoutParams contactParams = this.mPerson.getLayoutParams();
                contactParams.width = this.mPersonViewWidth;
                this.mPerson.setLayoutParams(contactParams);
                LayoutParams companyParams = this.mOrganization.getLayoutParams();
                companyParams.width = this.mOrganizationViewWidth;
                this.mOrganization.setLayoutParams(companyParams);
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_deal_details, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Deal deal = this.mPresenter.getDeal();
        if (deal != null) {
            DealCommunicationMediumUtil util = new DealCommunicationMediumUtil();
            MenuItem menuItemMenuCall = menu.findItem(R.id.menu_call);
            if (menuItemMenuCall != null) {
                menuItemMenuCall.setVisible(util.hasPhones(deal));
            }
            MenuItem menuItemMenuSendMessage = menu.findItem(R.id.menu_send_message);
            if (menuItemMenuSendMessage != null) {
                boolean z = util.hasEmails(deal) || util.hasPhones(deal);
                menuItemMenuSendMessage.setVisible(z);
            }
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                onBackPressed();
                return true;
            case R.id.menu_call:
                onCallMenuItemClicked();
                return true;
            case R.id.menu_send_message:
                onSendMessageMenuClicked();
                return true;
            case R.id.menu_edit:
                onEditMenuItemClicked();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onEditMenuItemClicked() {
        Deal deal = this.mPresenter.getDeal();
        if (deal != null) {
            DealEditActivity.startActivityForEditingDeal(this, deal);
        }
    }

    private void onCallMenuItemClicked() {
        CommunicationMediumListDialogFragment.showCallSelector(getSupportFragmentManager());
    }

    private void onSendMessageMenuClicked() {
        CommunicationMediumListDialogFragment.showSendMessageSelector(getSupportFragmentManager());
    }

    public void onDealRetrieved(@NonNull Deal deal) {
        this.mTitle.setText(deal.getTitle());
        setPersonAndOrganization(deal);
        setupValue(getSession(), deal);
        setupProductCount(getSession(), deal);
        setupStatus(deal);
        setupStage(getSession(), deal);
        supportInvalidateOptionsMenu();
    }

    private void setPersonAndOrganization(@NonNull final Deal deal) {
        boolean personAssigned;
        boolean organizationAssigned;
        int i;
        int i2 = 0;
        String personName = deal.getPerson() == null ? "" : deal.getPerson().getName();
        String organizationName = deal.getOrganization() == null ? "" : deal.getOrganization().getName();
        if (TextUtils.isEmpty(personName)) {
            personAssigned = false;
        } else {
            personAssigned = true;
        }
        if (TextUtils.isEmpty(organizationName)) {
            organizationAssigned = false;
        } else {
            organizationAssigned = true;
        }
        this.mPerson.setText(personName);
        this.mOrganization.setText(organizationName);
        TextView textView = this.mPerson;
        if (personAssigned) {
            i = 0;
        } else {
            i = 8;
        }
        textView.setVisibility(i);
        TextView textView2 = this.mOrganization;
        if (!organizationAssigned) {
            i2 = 8;
        }
        textView2.setVisibility(i2);
        this.mPerson.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DealDetailsActivity.this.onPersonClicked(deal.getPerson());
            }
        });
        this.mOrganization.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DealDetailsActivity.this.onOrganizationClicked(deal.getOrganization());
            }
        });
    }

    private void setupValue(@NonNull Session session, @NonNull Deal deal) {
        this.mValue.setText(new MonetaryFormatter(session).formatDealValue(deal));
    }

    private void setupProductCount(@NonNull Session session, @NonNull Deal deal) {
        boolean shouldShowProductCount;
        Double productCount = deal.getProductCount();
        if (!session.isCompanyFeatureProductsEnabled(false) || productCount == null || productCount.doubleValue() <= 0.0d) {
            shouldShowProductCount = false;
        } else {
            shouldShowProductCount = true;
        }
        if (shouldShowProductCount) {
            this.mProducts.setVisibility(0);
            this.mProducts.setText(getResources().getQuantityString(R.plurals.product, (int) Math.round(productCount.doubleValue()), new Object[]{Long.valueOf(Math.round(productCount.doubleValue()))}));
        } else {
            this.mProducts.setVisibility(8);
        }
        this.mProductsAndValueContainer.setClickable(shouldShowProductCount);
    }

    private void setupStatus(@NonNull Deal deal) {
        this.mDealStateButtonsView.setup(deal, this);
    }

    private void setupStage(@NonNull Session session, Deal deal) {
        this.mStagesSelector.setup(session, deal);
    }

    public void onPersonClicked(@Nullable Person person) {
        if (person != null && person.getSqlIdOrNull() != null) {
            PersonDetailActivity.startActivity((Context) this, person.getSqlIdOrNull());
        }
    }

    public void onOrganizationClicked(@Nullable Organization organization) {
        if (organization != null && organization.getSqlIdOrNull() != null) {
            OrganizationDetailActivity.startActivity((Context) this, organization.getSqlIdOrNull());
        }
    }

    public void onDealUpdated(@NonNull Deal deal) {
        setupStage(getSession(), deal);
        setupStatus(deal);
    }

    public void onFlowUpdated() {
        this.mDealFlowView.refreshContent(getSession());
    }

    public void onCallRequested(@NonNull CommunicationMedium medium, @NonNull Deal deal) {
        boolean noDealPerson;
        boolean cannotInitializeCallRequest;
        Person dealPerson = deal.getPerson();
        Long personSqlId = dealPerson != null ? dealPerson.getSqlIdOrNull() : null;
        if (personSqlId == null) {
            noDealPerson = true;
        } else {
            noDealPerson = false;
        }
        boolean activityIsFinishing = isFinishing();
        if (noDealPerson || activityIsFinishing) {
            cannotInitializeCallRequest = true;
        } else {
            cannotInitializeCallRequest = false;
        }
        if (!cannotInitializeCallRequest) {
            CallSummaryListener.makeCallWithCallSummaryRegistration(getSession(), this, new CallSummaryRegistration(medium.getValue(), personSqlId, deal.getSqlIdOrNull()));
        }
    }

    public void onSmsRequested(@NonNull CommunicationMedium medium, @NonNull Deal deal) {
        PipedriveUtil.sendSms(this, medium.getValue());
    }

    public void onEmailRequested(@NonNull CommunicationMedium medium, @NonNull Deal deal) {
        EmailHelper.composeAndSendEmail(this, medium.getValue(), deal.getDropBoxAddress());
    }

    @Nullable
    public Deal getEntity() {
        return this.mPresenter.getDeal();
    }

    @NonNull
    public List<? extends CommunicationMedium> getPhones(@NonNull Deal deal) {
        return new DealCommunicationMediumUtil().getPhones(deal);
    }

    @NonNull
    public List<? extends CommunicationMedium> getEmails(@NonNull Deal deal) {
        return new DealCommunicationMediumUtil().getEmails(deal);
    }

    public void onDealLost() {
        this.mPresenter.updateDealStatus(DealStatus.LOST);
    }

    public void onDealReopened() {
        this.mPresenter.updateDealStatus(DealStatus.OPEN);
    }

    public void onBackPressed() {
        if (!this.mFabWithOverlayMenu.consumeBackPress()) {
            if (this.mPresenter.updateDealStage(this.mStagesSelector.getCurrentDealStage())) {
                SnackBarManager.INSTANCE.setDelayedSnackBar(R.string.deal_updated);
            }
            super.onBackPressed();
        }
    }

    public void onDealWon() {
        this.mPresenter.updateDealStatus(DealStatus.WON);
    }

    public void onMenuItemClicked(int itemId) {
        Deal deal = this.mPresenter.getDeal();
        if (deal != null) {
            switch (itemId) {
                case R.id.action_take_photo:
                    takePhoto();
                    return;
                case R.id.action_upload_file:
                    uploadDocument();
                    return;
                case R.id.action_add_activity:
                    ActivityDetailActivity.startActivityForDeal((Activity) this, deal);
                    return;
                case R.id.action_add_note:
                    NoteCreateActivity.createNoteForDeal(this, deal);
                    return;
                default:
                    return;
            }
        }
    }

    private void takePhoto() {
        if (!(this.mCameraHelper == null)) {
            this.mCameraHelper.snapPhoto((Activity) this);
        }
    }

    private void uploadDocument() {
        if (!(this.mDocumentPickerHelper == null)) {
            this.mDocumentPickerHelper.pickDocument((Activity) this);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        boolean willNotBeAbleToRelateFileToDeal;
        EmailHelper.onActivityResult(requestCode);
        final Deal deal = this.mPresenter.getDeal();
        if (this.mCameraHelper == null || deal == null) {
            willNotBeAbleToRelateFileToDeal = true;
        } else {
            willNotBeAbleToRelateFileToDeal = false;
        }
        if (willNotBeAbleToRelateFileToDeal) {
            super.onActivityResult(requestCode, resultCode, data);
            this.mDealFlowView.refreshContent(getSession());
            return;
        }
        try {
            boolean ableToPassResultToDocumentPickerHelper;
            this.mCameraHelper.onActivityResult(this, requestCode, resultCode, data, new OnCameraListener() {
                public boolean snapshot(@NonNull Uri snapshot, @Nullable String snapshotFileName) {
                    FlowFile flowFileForDeal = new FlowFile();
                    flowFileForDeal.setDeal(deal);
                    flowFileForDeal.setUrl(snapshot.toString());
                    flowFileForDeal.setName(snapshotFileName);
                    Session session = DealDetailsActivity.this.getSession();
                    boolean storedSuccessfully = new StoreFlowFile(session).create(flowFileForDeal);
                    if (storedSuccessfully) {
                        DealDetailsActivity.this.mDealFlowView.refreshContent(session);
                    }
                    return storedSuccessfully;
                }
            });
            if (this.mDocumentPickerHelper != null) {
                ableToPassResultToDocumentPickerHelper = true;
            } else {
                ableToPassResultToDocumentPickerHelper = false;
            }
            if (ableToPassResultToDocumentPickerHelper) {
                this.mDocumentPickerHelper.onActivityResult(this, requestCode, resultCode, data, new OnDocumentPickerListener() {
                    public boolean documentPicked(@NonNull Uri document, @Nullable String documentName, @Nullable Long fileSizeInBytes, @Nullable String fileType) {
                        FlowFile flowFileForDeal = new FlowFile();
                        flowFileForDeal.setDeal(deal);
                        flowFileForDeal.setUrl(document.toString());
                        flowFileForDeal.setName(documentName);
                        flowFileForDeal.setFileSize(fileSizeInBytes);
                        flowFileForDeal.setFileType(fileType);
                        Session session = DealDetailsActivity.this.getSession();
                        boolean storedSuccessfully = new StoreFlowFile(session).create(flowFileForDeal);
                        if (storedSuccessfully) {
                            DealDetailsActivity.this.mDealFlowView.refreshContent(session);
                        }
                        return storedSuccessfully;
                    }
                });
            }
            super.onActivityResult(requestCode, resultCode, data);
            this.mDealFlowView.refreshContent(getSession());
        } catch (Throwable th) {
            super.onActivityResult(requestCode, resultCode, data);
            this.mDealFlowView.refreshContent(getSession());
        }
    }

    public void showProgress() {
        super.showProgress();
    }

    public void hideProgress() {
        super.hideProgress();
    }
}
