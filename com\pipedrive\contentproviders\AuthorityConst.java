package com.pipedrive.contentproviders;

public interface AuthorityConst {
    public static final String AUTHORITY_BASE = "com.pipedrive.contentproviders";
    public static final String AUTHORITY_DEALS = "com.pipedrive.contentproviders.dealscontentprovider";
    public static final String AUTHORITY_GLOBAL_SEARCH = "com.pipedrive.contentproviders.globalsearchcontentprovider";
    public static final String AUTHORITY_ORGANIZATION = "com.pipedrive.contentproviders.organizationcontentprovider";
    public static final String AUTHORITY_PERSONS = "com.pipedrive.contentproviders.contactscontentprovider";
    public static final String AUTHORITY_RECENTS = "com.pipedrive.contentproviders.recentsprovider";
}
