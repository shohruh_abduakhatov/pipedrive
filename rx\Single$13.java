package rx;

import rx.Scheduler.Worker;
import rx.functions.Action0;

class Single$13 implements Single$OnSubscribe<T> {
    final /* synthetic */ Single this$0;
    final /* synthetic */ Scheduler val$scheduler;

    Single$13(Single single, Scheduler scheduler) {
        this.this$0 = single;
        this.val$scheduler = scheduler;
    }

    public void call(final SingleSubscriber<? super T> t) {
        final Worker w = this.val$scheduler.createWorker();
        t.add(w);
        w.schedule(new Action0() {
            public void call() {
                SingleSubscriber<T> single = new SingleSubscriber<T>() {
                    public void onSuccess(T value) {
                        try {
                            t.onSuccess(value);
                        } finally {
                            w.unsubscribe();
                        }
                    }

                    public void onError(Throwable error) {
                        try {
                            t.onError(error);
                        } finally {
                            w.unsubscribe();
                        }
                    }
                };
                t.add(single);
                Single$13.this.this$0.subscribe(single);
            }
        });
    }
}
