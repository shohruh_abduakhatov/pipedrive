package com.zendesk.sdk.network.impl;

import android.support.annotation.Nullable;
import com.zendesk.sdk.model.request.CommentsResponse;
import com.zendesk.sdk.model.request.CreateRequest;
import com.zendesk.sdk.model.request.CreateRequestWrapper;
import com.zendesk.sdk.model.request.EndUserComment;
import com.zendesk.sdk.model.request.Request;
import com.zendesk.sdk.model.request.RequestResponse;
import com.zendesk.sdk.model.request.RequestsResponse;
import com.zendesk.sdk.model.request.UpdateRequestWrapper;
import com.zendesk.sdk.model.request.fields.RawTicketFormResponse;
import com.zendesk.sdk.network.RequestService;
import com.zendesk.service.RetrofitZendeskCallbackAdapter;
import com.zendesk.service.RetrofitZendeskCallbackAdapter.RequestExtractor;
import com.zendesk.service.ZendeskCallback;
import java.util.List;

class ZendeskRequestService {
    private static final String LOG_TAG = "ZendeskRequestService";
    private static final String TICKET_FIELDS_INCLUDE = "ticket_fields";
    private final RequestService requestService;

    ZendeskRequestService(RequestService requestService) {
        this.requestService = requestService;
    }

    public void createRequest(String header, @Nullable String sdkGuid, CreateRequest request, ZendeskCallback<Request> callback) {
        CreateRequestWrapper wrapper = new CreateRequestWrapper();
        wrapper.setRequest(request);
        this.requestService.createRequest(header, sdkGuid, wrapper).enqueue(new RetrofitZendeskCallbackAdapter(callback, new RequestExtractor<RequestResponse, Request>() {
            public Request extract(RequestResponse data) {
                return data.getRequest();
            }
        }));
    }

    void getAllRequests(String header, String status, String include, ZendeskCallback<List<Request>> callback) {
        this.requestService.getAllRequests(header, status, include).enqueue(new RetrofitZendeskCallbackAdapter(callback, new RequestExtractor<RequestsResponse, List<Request>>() {
            public List<Request> extract(RequestsResponse data) {
                return data.getRequests();
            }
        }));
    }

    void getAllRequests(String header, String requestId, String status, String include, ZendeskCallback<List<Request>> callback) {
        this.requestService.getManyRequests(header, requestId, status, include).enqueue(new RetrofitZendeskCallbackAdapter(callback, new RequestExtractor<RequestsResponse, List<Request>>() {
            public List<Request> extract(RequestsResponse data) {
                return data.getRequests();
            }
        }));
    }

    void getComments(String header, String requestId, ZendeskCallback<CommentsResponse> callback) {
        this.requestService.getComments(header, requestId).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }

    void addComment(String header, String requestId, EndUserComment comment, ZendeskCallback<Request> callback) {
        UpdateRequestWrapper updateRequestWrapper = new UpdateRequestWrapper();
        Request request = new Request();
        request.setComment(comment);
        updateRequestWrapper.setRequest(request);
        this.requestService.addComment(header, requestId, updateRequestWrapper).enqueue(new RetrofitZendeskCallbackAdapter(callback, new RequestExtractor<UpdateRequestWrapper, Request>() {
            public Request extract(UpdateRequestWrapper updateRequestWrapper) {
                return updateRequestWrapper.getRequest();
            }
        }));
    }

    void getRequest(String header, String requestId, ZendeskCallback<RequestResponse> callback) {
        this.requestService.getRequest(header, requestId).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }

    void getTicketFormsById(String header, String locale, String ticketFormIds, ZendeskCallback<RawTicketFormResponse> callback) {
        this.requestService.getTicketFormsById(header, locale, ticketFormIds, TICKET_FIELDS_INCLUDE).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }
}
