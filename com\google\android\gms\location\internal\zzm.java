package com.google.android.gms.location.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.location.LocationRequest;
import java.util.List;

public class zzm implements Creator<LocationRequestInternal> {
    static void zza(LocationRequestInternal locationRequestInternal, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zza(parcel, 1, locationRequestInternal.VR, i, false);
        zzb.zza(parcel, 4, locationRequestInternal.ajy);
        zzb.zzc(parcel, 5, locationRequestInternal.ajK, false);
        zzb.zza(parcel, 6, locationRequestInternal.mTag, false);
        zzb.zza(parcel, 7, locationRequestInternal.ald);
        zzb.zzc(parcel, 1000, locationRequestInternal.getVersionCode());
        zzb.zza(parcel, 8, locationRequestInternal.ale);
        zzb.zzaj(parcel, zzcs);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzod(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzvc(i);
    }

    public LocationRequestInternal zzod(Parcel parcel) {
        String str = null;
        boolean z = false;
        int zzcr = zza.zzcr(parcel);
        boolean z2 = true;
        List list = LocationRequestInternal.alc;
        boolean z3 = false;
        LocationRequest locationRequest = null;
        int i = 0;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    locationRequest = (LocationRequest) zza.zza(parcel, zzcq, LocationRequest.CREATOR);
                    break;
                case 4:
                    z2 = zza.zzc(parcel, zzcq);
                    break;
                case 5:
                    list = zza.zzc(parcel, zzcq, ClientIdentity.CREATOR);
                    break;
                case 6:
                    str = zza.zzq(parcel, zzcq);
                    break;
                case 7:
                    z3 = zza.zzc(parcel, zzcq);
                    break;
                case 8:
                    z = zza.zzc(parcel, zzcq);
                    break;
                case 1000:
                    i = zza.zzg(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new LocationRequestInternal(i, locationRequest, z2, list, str, z3, z);
        }
        throw new zza.zza("Overread allowed size end=" + zzcr, parcel);
    }

    public LocationRequestInternal[] zzvc(int i) {
        return new LocationRequestInternal[i];
    }
}
