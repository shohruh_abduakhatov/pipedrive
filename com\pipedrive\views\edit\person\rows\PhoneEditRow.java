package com.pipedrive.views.edit.person.rows;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.ViewGroup;
import com.pipedrive.R;

class PhoneEditRow extends CommunicationMediumEditRow {
    PhoneEditRow(@NonNull Context context, @NonNull ViewGroup parent, int inputType) {
        super(context, parent, inputType);
    }

    final int getIconResourceId() {
        return R.drawable.icon_phone_edit;
    }

    final int getHintResourceId() {
        return R.string.phone_number;
    }

    final int getLabelsResourceId() {
        return R.array.labels_phone;
    }
}
