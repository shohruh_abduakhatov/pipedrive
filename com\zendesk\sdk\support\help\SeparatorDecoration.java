package com.zendesk.sdk.support.help;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.annotation.VisibleForTesting;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.support.v7.widget.RecyclerView.State;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;

class SeparatorDecoration extends ItemDecoration {
    private Drawable mDivider;

    SeparatorDecoration(Drawable divider) {
        this.mDivider = divider;
    }

    public void onDrawOver(Canvas c, RecyclerView parent, State state) {
        super.onDraw(c, parent, state);
        if (parent.getItemAnimator() == null || !parent.getItemAnimator().isRunning()) {
            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);
                if (shouldShowTopSeparator(parent, i)) {
                    int dividerTop = child.getTop() + ((LayoutParams) child.getLayoutParams()).topMargin;
                    this.mDivider.setBounds(parent.getPaddingLeft(), dividerTop, parent.getWidth() - parent.getPaddingRight(), dividerTop + this.mDivider.getIntrinsicHeight());
                    this.mDivider.draw(c);
                }
            }
        }
    }

    @VisibleForTesting
    boolean shouldShowTopSeparator(RecyclerView parent, int position) {
        boolean itemIsACategory = isItemACategory(parent.getChildViewHolder(parent.getChildAt(position)));
        boolean itemIsAnExpandedCategory = isItemAnExpandedCategory(parent.getChildViewHolder(parent.getChildAt(position)));
        boolean previousItemIsAnUnexpandedCategory;
        if (position <= 0 || !isItemAnUnexpandedCategory(parent.getChildViewHolder(parent.getChildAt(position - 1)))) {
            previousItemIsAnUnexpandedCategory = false;
        } else {
            previousItemIsAnUnexpandedCategory = true;
        }
        if (!itemIsACategory || (!itemIsAnExpandedCategory && previousItemIsAnUnexpandedCategory)) {
            return false;
        }
        return true;
    }

    private boolean isItemACategory(ViewHolder viewHolder) {
        return viewHolder instanceof CategoryViewHolder;
    }

    private boolean isItemAnExpandedCategory(ViewHolder viewHolder) {
        return (viewHolder instanceof CategoryViewHolder) && ((CategoryViewHolder) viewHolder).isExpanded();
    }

    private boolean isItemAnUnexpandedCategory(ViewHolder viewHolder) {
        return (viewHolder instanceof CategoryViewHolder) && !((CategoryViewHolder) viewHolder).isExpanded();
    }
}
