package rx;

import rx.annotations.Beta;
import rx.exceptions.MissingBackpressureException;

@Beta
public final class BackpressureOverflow {
    public static final Strategy ON_OVERFLOW_DEFAULT = ON_OVERFLOW_ERROR;
    public static final Strategy ON_OVERFLOW_DROP_LATEST = DropLatest.INSTANCE;
    public static final Strategy ON_OVERFLOW_DROP_OLDEST = DropOldest.INSTANCE;
    public static final Strategy ON_OVERFLOW_ERROR = Error.INSTANCE;

    public interface Strategy {
        boolean mayAttemptDrop() throws MissingBackpressureException;
    }

    static final class DropLatest implements Strategy {
        static final DropLatest INSTANCE = new DropLatest();

        private DropLatest() {
        }

        public boolean mayAttemptDrop() {
            return false;
        }
    }

    static final class DropOldest implements Strategy {
        static final DropOldest INSTANCE = new DropOldest();

        private DropOldest() {
        }

        public boolean mayAttemptDrop() {
            return true;
        }
    }

    static final class Error implements Strategy {
        static final Error INSTANCE = new Error();

        private Error() {
        }

        public boolean mayAttemptDrop() throws MissingBackpressureException {
            throw new MissingBackpressureException("Overflowed buffer");
        }
    }

    private BackpressureOverflow() {
        throw new IllegalStateException("No instances!");
    }
}
