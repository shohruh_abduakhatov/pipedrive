package com.zendesk.service;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitZendeskCallbackAdapter<E, F> implements Callback<E> {
    protected static final RequestExtractor DEFAULT_EXTRACTOR = new DefaultExtractor();
    private final ZendeskCallback<F> mCallback;
    private final RequestExtractor<E, F> mExtractor;

    public interface RequestExtractor<E, F> {
        F extract(E e);
    }

    static final class DefaultExtractor<E> implements RequestExtractor<E, E> {
        DefaultExtractor() {
        }

        public E extract(E data) {
            return data;
        }
    }

    public RetrofitZendeskCallbackAdapter(ZendeskCallback<F> callback, RequestExtractor<E, F> extractor) {
        this.mCallback = callback;
        this.mExtractor = extractor;
    }

    public RetrofitZendeskCallbackAdapter(ZendeskCallback<F> callback) {
        this(callback, DEFAULT_EXTRACTOR);
    }

    public void onResponse(Call<E> call, Response<E> response) {
        if (this.mCallback == null) {
            return;
        }
        if (response.isSuccessful()) {
            this.mCallback.onSuccess(this.mExtractor.extract(response.body()));
        } else {
            this.mCallback.onError(RetrofitErrorResponse.response(response));
        }
    }

    public void onFailure(Call<E> call, Throwable t) {
        if (this.mCallback != null) {
            this.mCallback.onError(RetrofitErrorResponse.throwable(t));
        }
    }
}
