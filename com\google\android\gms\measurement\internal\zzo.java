package com.google.android.gms.measurement.internal;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteFullException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.SystemClock;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.util.zze;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;

public class zzo extends zzaa {
    private final zza asx = new zza(this, getContext(), zzade());
    private boolean asy;

    @TargetApi(11)
    private class zza extends SQLiteOpenHelper {
        final /* synthetic */ zzo asz;

        zza(zzo com_google_android_gms_measurement_internal_zzo, Context context, String str) {
            this.asz = com_google_android_gms_measurement_internal_zzo;
            super(context, str, null, 1);
        }

        @WorkerThread
        public SQLiteDatabase getWritableDatabase() {
            try {
                return super.getWritableDatabase();
            } catch (SQLiteException e) {
                if (VERSION.SDK_INT < 11 || !(e instanceof SQLiteDatabaseLockedException)) {
                    this.asz.zzbwb().zzbwy().log("Opening the local database failed, dropping and recreating it");
                    String zzade = this.asz.zzade();
                    if (!this.asz.getContext().getDatabasePath(zzade).delete()) {
                        this.asz.zzbwb().zzbwy().zzj("Failed to delete corrupted local db file", zzade);
                    }
                    try {
                        return super.getWritableDatabase();
                    } catch (SQLiteException e2) {
                        this.asz.zzbwb().zzbwy().zzj("Failed to open local database. Events will bypass local storage", e2);
                        return null;
                    }
                }
                throw e2;
            }
        }

        @WorkerThread
        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            zze.zza(this.asz.zzbwb(), sQLiteDatabase);
        }

        @WorkerThread
        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (VERSION.SDK_INT < 15) {
                String str = "PRAGMA journal_mode=memory";
                Cursor rawQuery = !(sQLiteDatabase instanceof SQLiteDatabase) ? sQLiteDatabase.rawQuery(str, null) : SQLiteInstrumentation.rawQuery(sQLiteDatabase, str, null);
                try {
                    rawQuery.moveToFirst();
                } finally {
                    rawQuery.close();
                }
            }
            zze.zza(this.asz.zzbwb(), sQLiteDatabase, "messages", "create table if not exists messages ( type INTEGER NOT NULL, entry BLOB NOT NULL)", "type,entry", null);
        }

        @WorkerThread
        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        }
    }

    zzo(zzx com_google_android_gms_measurement_internal_zzx) {
        super(com_google_android_gms_measurement_internal_zzx);
    }

    @WorkerThread
    @TargetApi(11)
    private boolean zza(int i, byte[] bArr) {
        int i2;
        zzaby();
        zzzx();
        if (this.asy) {
            return false;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("type", Integer.valueOf(i));
        contentValues.put("entry", bArr);
        int i3 = 5;
        zzbwd().zzbvb();
        int i4 = 0;
        while (i4 < 5) {
            SQLiteDatabase sQLiteDatabase = null;
            try {
                sQLiteDatabase = getWritableDatabase();
                if (sQLiteDatabase == null) {
                    this.asy = true;
                    if (sQLiteDatabase != null) {
                        sQLiteDatabase.close();
                    }
                    return false;
                }
                sQLiteDatabase.beginTransaction();
                long j = 0;
                String str = "select count(1) from messages";
                Cursor rawQuery = !(sQLiteDatabase instanceof SQLiteDatabase) ? sQLiteDatabase.rawQuery(str, null) : SQLiteInstrumentation.rawQuery(sQLiteDatabase, str, null);
                if (rawQuery != null && rawQuery.moveToFirst()) {
                    j = rawQuery.getLong(0);
                }
                if (j >= 100000) {
                    zzbwb().zzbwy().log("Data loss, local db full");
                    j = (100000 - j) + 1;
                    str = "messages";
                    String str2 = "rowid in (select rowid from messages order by rowid asc limit ?)";
                    String[] strArr = new String[]{Long.toString(j)};
                    long delete = (long) (!(sQLiteDatabase instanceof SQLiteDatabase) ? sQLiteDatabase.delete(str, str2, strArr) : SQLiteInstrumentation.delete(sQLiteDatabase, str, str2, strArr));
                    if (delete != j) {
                        zzbwb().zzbwy().zzd("Different delete count than expected in local db. expected, received, difference", Long.valueOf(j), Long.valueOf(delete), Long.valueOf(j - delete));
                    }
                }
                String str3 = "messages";
                if (sQLiteDatabase instanceof SQLiteDatabase) {
                    SQLiteInstrumentation.insertOrThrow(sQLiteDatabase, str3, null, contentValues);
                } else {
                    sQLiteDatabase.insertOrThrow(str3, null, contentValues);
                }
                sQLiteDatabase.setTransactionSuccessful();
                sQLiteDatabase.endTransaction();
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
                return true;
            } catch (SQLiteFullException e) {
                zzbwb().zzbwy().zzj("Error writing entry to local database", e);
                this.asy = true;
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                    i2 = i3;
                } else {
                    i2 = i3;
                }
                i4++;
                i3 = i2;
            } catch (SQLiteException e2) {
                if (VERSION.SDK_INT < 11 || !(e2 instanceof SQLiteDatabaseLockedException)) {
                    if (sQLiteDatabase != null) {
                        if (sQLiteDatabase.inTransaction()) {
                            sQLiteDatabase.endTransaction();
                        }
                    }
                    zzbwb().zzbwy().zzj("Error writing entry to local database", e2);
                    this.asy = true;
                    i2 = i3;
                } else {
                    SystemClock.sleep((long) i3);
                    i2 = i3 + 20;
                }
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
                i4++;
                i3 = i2;
            } catch (Throwable th) {
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
            }
        }
        zzbwb().zzbxa().log("Failed to write entry to local database");
        return false;
    }

    public /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    @WorkerThread
    SQLiteDatabase getWritableDatabase() {
        if (this.asy) {
            return null;
        }
        SQLiteDatabase writableDatabase = this.asx.getWritableDatabase();
        if (writableDatabase != null) {
            return writableDatabase;
        }
        this.asy = true;
        return null;
    }

    public boolean zza(EventParcel eventParcel) {
        Parcel obtain = Parcel.obtain();
        eventParcel.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return zza(0, marshall);
        }
        zzbwb().zzbxa().log("Event is too long for local database. Sending event directly to service");
        return false;
    }

    public boolean zza(UserAttributeParcel userAttributeParcel) {
        Parcel obtain = Parcel.obtain();
        userAttributeParcel.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return zza(1, marshall);
        }
        zzbwb().zzbxa().log("User property too long for local database. Sending directly to service");
        return false;
    }

    public /* bridge */ /* synthetic */ void zzaby() {
        super.zzaby();
    }

    public /* bridge */ /* synthetic */ zze zzabz() {
        return super.zzabz();
    }

    String zzade() {
        return zzbwd().zzbus();
    }

    public /* bridge */ /* synthetic */ void zzbvo() {
        super.zzbvo();
    }

    public /* bridge */ /* synthetic */ zzc zzbvp() {
        return super.zzbvp();
    }

    public /* bridge */ /* synthetic */ zzac zzbvq() {
        return super.zzbvq();
    }

    public /* bridge */ /* synthetic */ zzn zzbvr() {
        return super.zzbvr();
    }

    public /* bridge */ /* synthetic */ zzg zzbvs() {
        return super.zzbvs();
    }

    public /* bridge */ /* synthetic */ zzae zzbvt() {
        return super.zzbvt();
    }

    public /* bridge */ /* synthetic */ zzad zzbvu() {
        return super.zzbvu();
    }

    public /* bridge */ /* synthetic */ zzo zzbvv() {
        return super.zzbvv();
    }

    public /* bridge */ /* synthetic */ zze zzbvw() {
        return super.zzbvw();
    }

    public /* bridge */ /* synthetic */ zzal zzbvx() {
        return super.zzbvx();
    }

    public /* bridge */ /* synthetic */ zzv zzbvy() {
        return super.zzbvy();
    }

    public /* bridge */ /* synthetic */ zzag zzbvz() {
        return super.zzbvz();
    }

    public /* bridge */ /* synthetic */ zzw zzbwa() {
        return super.zzbwa();
    }

    public /* bridge */ /* synthetic */ zzq zzbwb() {
        return super.zzbwb();
    }

    public /* bridge */ /* synthetic */ zzt zzbwc() {
        return super.zzbwc();
    }

    public /* bridge */ /* synthetic */ zzd zzbwd() {
        return super.zzbwd();
    }

    boolean zzbwn() {
        return getContext().getDatabasePath(zzade()).exists();
    }

    @android.annotation.TargetApi(11)
    public java.util.List<com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable> zzxe(int r25) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Unreachable block: B:126:0x010f
	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.modifyBlocksTree(BlockProcessor.java:248)
	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.processBlocksTree(BlockProcessor.java:52)
	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.rerun(BlockProcessor.java:44)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.visit(BlockFinallyExtract.java:57)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r24 = this;
        r24.zzzx();
        r24.zzaby();
        r0 = r24;
        r2 = r0.asy;
        if (r2 == 0) goto L_0x000e;
    L_0x000c:
        r2 = 0;
    L_0x000d:
        return r2;
    L_0x000e:
        r21 = new java.util.ArrayList;
        r21.<init>();
        r2 = r24.zzbwn();
        if (r2 != 0) goto L_0x001c;
    L_0x0019:
        r2 = r21;
        goto L_0x000d;
    L_0x001c:
        r20 = 5;
        r2 = 0;
        r22 = r2;
    L_0x0021:
        r2 = 5;
        r0 = r22;
        if (r0 >= r2) goto L_0x01cc;
    L_0x0026:
        r3 = 0;
        r2 = r24.getWritableDatabase();	 Catch:{ SQLiteFullException -> 0x01e2, SQLiteException -> 0x01df }
        if (r2 != 0) goto L_0x0039;
    L_0x002d:
        r3 = 1;
        r0 = r24;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r0.asy = r3;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        if (r2 == 0) goto L_0x0037;
    L_0x0034:
        r2.close();
    L_0x0037:
        r2 = 0;
        goto L_0x000d;
    L_0x0039:
        r2.beginTransaction();	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r3 = "messages";	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r4 = 3;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r4 = new java.lang.String[r4];	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r5 = 0;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r6 = "rowid";	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r4[r5] = r6;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r5 = 1;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r6 = "type";	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r4[r5] = r6;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r5 = 2;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r6 = "entry";	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r4[r5] = r6;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r5 = 0;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r6 = 0;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r7 = 0;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r8 = 0;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r9 = "rowid asc";	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r10 = java.lang.Integer.toString(r25);	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r11 = r2 instanceof android.database.sqlite.SQLiteDatabase;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        if (r11 != 0) goto L_0x00c3;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
    L_0x005e:
        r3 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10);	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r4 = r3;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
    L_0x0063:
        r6 = -1;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
    L_0x0065:
        r3 = r4.moveToNext();	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        if (r3 == 0) goto L_0x0167;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
    L_0x006b:
        r3 = 0;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r6 = r4.getLong(r3);	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r3 = 1;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r3 = r4.getInt(r3);	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r5 = 2;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r8 = r4.getBlob(r5);	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        if (r3 != 0) goto L_0x011b;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
    L_0x007c:
        r5 = android.os.Parcel.obtain();	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r3 = 0;
        r9 = r8.length;	 Catch:{ zza -> 0x00d9, all -> 0x010a }
        r5.unmarshall(r8, r3, r9);	 Catch:{ zza -> 0x00d9, all -> 0x010a }
        r3 = 0;	 Catch:{ zza -> 0x00d9, all -> 0x010a }
        r5.setDataPosition(r3);	 Catch:{ zza -> 0x00d9, all -> 0x010a }
        r3 = com.google.android.gms.measurement.internal.EventParcel.CREATOR;	 Catch:{ zza -> 0x00d9, all -> 0x010a }
        r3 = r3.createFromParcel(r5);	 Catch:{ zza -> 0x00d9, all -> 0x010a }
        r3 = (com.google.android.gms.measurement.internal.EventParcel) r3;	 Catch:{ zza -> 0x00d9, all -> 0x010a }
        r5.recycle();
        if (r3 == 0) goto L_0x0065;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
    L_0x0096:
        r0 = r21;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r0.add(r3);	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        goto L_0x0065;
    L_0x009c:
        r3 = move-exception;
        r23 = r3;
        r3 = r2;
        r2 = r23;
    L_0x00a2:
        r4 = r24.zzbwb();	 Catch:{ all -> 0x01dc }
        r4 = r4.zzbwy();	 Catch:{ all -> 0x01dc }
        r5 = "Error reading entries from local database";	 Catch:{ all -> 0x01dc }
        r4.zzj(r5, r2);	 Catch:{ all -> 0x01dc }
        r2 = 1;	 Catch:{ all -> 0x01dc }
        r0 = r24;	 Catch:{ all -> 0x01dc }
        r0.asy = r2;	 Catch:{ all -> 0x01dc }
        if (r3 == 0) goto L_0x01e5;
    L_0x00b6:
        r3.close();
        r2 = r20;
    L_0x00bb:
        r3 = r22 + 1;
        r22 = r3;
        r20 = r2;
        goto L_0x0021;
    L_0x00c3:
        r0 = r2;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r0 = (android.database.sqlite.SQLiteDatabase) r0;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r11 = r0;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r12 = r3;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r13 = r4;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r14 = r5;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r15 = r6;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r16 = r7;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r17 = r8;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r18 = r9;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r19 = r10;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r3 = com.newrelic.agent.android.instrumentation.SQLiteInstrumentation.query(r11, r12, r13, r14, r15, r16, r17, r18, r19);	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r4 = r3;
        goto L_0x0063;
    L_0x00d9:
        r3 = move-exception;
        r3 = r24.zzbwb();	 Catch:{ zza -> 0x00d9, all -> 0x010a }
        r3 = r3.zzbwy();	 Catch:{ zza -> 0x00d9, all -> 0x010a }
        r8 = "Failed to load event from local database";	 Catch:{ zza -> 0x00d9, all -> 0x010a }
        r3.log(r8);	 Catch:{ zza -> 0x00d9, all -> 0x010a }
        r5.recycle();
        goto L_0x0065;
    L_0x00ec:
        r3 = move-exception;
        r23 = r3;
        r3 = r2;
        r2 = r23;
    L_0x00f2:
        r4 = android.os.Build.VERSION.SDK_INT;	 Catch:{ all -> 0x01dc }
        r5 = 11;	 Catch:{ all -> 0x01dc }
        if (r4 < r5) goto L_0x01ab;	 Catch:{ all -> 0x01dc }
    L_0x00f8:
        r4 = r2 instanceof android.database.sqlite.SQLiteDatabaseLockedException;	 Catch:{ all -> 0x01dc }
        if (r4 == 0) goto L_0x01ab;	 Catch:{ all -> 0x01dc }
    L_0x00fc:
        r0 = r20;	 Catch:{ all -> 0x01dc }
        r4 = (long) r0;	 Catch:{ all -> 0x01dc }
        android.os.SystemClock.sleep(r4);	 Catch:{ all -> 0x01dc }
        r2 = r20 + 20;
    L_0x0104:
        if (r3 == 0) goto L_0x00bb;
    L_0x0106:
        r3.close();
        goto L_0x00bb;
    L_0x010a:
        r3 = move-exception;
        r5.recycle();	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        throw r3;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
    L_0x010f:
        r3 = move-exception;
        r23 = r3;
        r3 = r2;
        r2 = r23;
    L_0x0115:
        if (r3 == 0) goto L_0x011a;
    L_0x0117:
        r3.close();
    L_0x011a:
        throw r2;
    L_0x011b:
        r5 = 1;
        if (r3 != r5) goto L_0x0158;
    L_0x011e:
        r9 = android.os.Parcel.obtain();	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r5 = 0;
        r3 = 0;
        r10 = r8.length;	 Catch:{ zza -> 0x0140, all -> 0x0153 }
        r9.unmarshall(r8, r3, r10);	 Catch:{ zza -> 0x0140, all -> 0x0153 }
        r3 = 0;	 Catch:{ zza -> 0x0140, all -> 0x0153 }
        r9.setDataPosition(r3);	 Catch:{ zza -> 0x0140, all -> 0x0153 }
        r3 = com.google.android.gms.measurement.internal.UserAttributeParcel.CREATOR;	 Catch:{ zza -> 0x0140, all -> 0x0153 }
        r3 = r3.createFromParcel(r9);	 Catch:{ zza -> 0x0140, all -> 0x0153 }
        r3 = (com.google.android.gms.measurement.internal.UserAttributeParcel) r3;	 Catch:{ zza -> 0x0140, all -> 0x0153 }
        r9.recycle();
    L_0x0137:
        if (r3 == 0) goto L_0x0065;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
    L_0x0139:
        r0 = r21;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r0.add(r3);	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        goto L_0x0065;
    L_0x0140:
        r3 = move-exception;
        r3 = r24.zzbwb();	 Catch:{ zza -> 0x0140, all -> 0x0153 }
        r3 = r3.zzbwy();	 Catch:{ zza -> 0x0140, all -> 0x0153 }
        r8 = "Failed to load user property from local database";	 Catch:{ zza -> 0x0140, all -> 0x0153 }
        r3.log(r8);	 Catch:{ zza -> 0x0140, all -> 0x0153 }
        r9.recycle();
        r3 = r5;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        goto L_0x0137;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
    L_0x0153:
        r3 = move-exception;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r9.recycle();	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        throw r3;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
    L_0x0158:
        r3 = r24.zzbwb();	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r3 = r3.zzbwy();	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r5 = "Unknown record type in local database";	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r3.log(r5);	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        goto L_0x0065;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
    L_0x0167:
        r4.close();	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r4 = "messages";	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r5 = "rowid <= ?";	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r3 = 1;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r8 = new java.lang.String[r3];	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r3 = 0;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r6 = java.lang.Long.toString(r6);	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r8[r3] = r6;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r3 = r2 instanceof android.database.sqlite.SQLiteDatabase;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        if (r3 != 0) goto L_0x01a2;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
    L_0x017c:
        r3 = r2.delete(r4, r5, r8);	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
    L_0x0180:
        r4 = r21.size();	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        if (r3 >= r4) goto L_0x0193;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
    L_0x0186:
        r3 = r24.zzbwb();	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r3 = r3.zzbwy();	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r4 = "Fewer entries removed from local database than expected";	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r3.log(r4);	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
    L_0x0193:
        r2.setTransactionSuccessful();	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r2.endTransaction();	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        if (r2 == 0) goto L_0x019e;
    L_0x019b:
        r2.close();
    L_0x019e:
        r2 = r21;
        goto L_0x000d;
    L_0x01a2:
        r0 = r2;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r0 = (android.database.sqlite.SQLiteDatabase) r0;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r3 = r0;	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        r3 = com.newrelic.agent.android.instrumentation.SQLiteInstrumentation.delete(r3, r4, r5, r8);	 Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00ec, all -> 0x010f }
        goto L_0x0180;
    L_0x01ab:
        if (r3 == 0) goto L_0x01b6;
    L_0x01ad:
        r4 = r3.inTransaction();	 Catch:{ all -> 0x01dc }
        if (r4 == 0) goto L_0x01b6;	 Catch:{ all -> 0x01dc }
    L_0x01b3:
        r3.endTransaction();	 Catch:{ all -> 0x01dc }
    L_0x01b6:
        r4 = r24.zzbwb();	 Catch:{ all -> 0x01dc }
        r4 = r4.zzbwy();	 Catch:{ all -> 0x01dc }
        r5 = "Error reading entries from local database";	 Catch:{ all -> 0x01dc }
        r4.zzj(r5, r2);	 Catch:{ all -> 0x01dc }
        r2 = 1;	 Catch:{ all -> 0x01dc }
        r0 = r24;	 Catch:{ all -> 0x01dc }
        r0.asy = r2;	 Catch:{ all -> 0x01dc }
        r2 = r20;
        goto L_0x0104;
    L_0x01cc:
        r2 = r24.zzbwb();
        r2 = r2.zzbxa();
        r3 = "Failed to read events from database in reasonable time";
        r2.log(r3);
        r2 = 0;
        goto L_0x000d;
    L_0x01dc:
        r2 = move-exception;
        goto L_0x0115;
    L_0x01df:
        r2 = move-exception;
        goto L_0x00f2;
    L_0x01e2:
        r2 = move-exception;
        goto L_0x00a2;
    L_0x01e5:
        r2 = r20;
        goto L_0x00bb;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzo.zzxe(int):java.util.List<com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable>");
    }

    public /* bridge */ /* synthetic */ void zzzx() {
        super.zzzx();
    }

    protected void zzzy() {
    }
}
