package com.pipedrive.note;

import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.activity.UpdateActivityTask;
import com.pipedrive.activity.UpdateActivityTask.OnTaskFinished;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.model.Activity;

class ActivityNoteEditorPresenterImpl extends ActivityNoteEditorPresenter {
    private OnTaskFinished DELETE_TASK_CALLBACK = new OnTaskFinished() {
        public void onActivityNoteUpdated(boolean success) {
            if (ActivityNoteEditorPresenterImpl.this.getView() != null) {
                ((ActivityNoteEditorView) ActivityNoteEditorPresenterImpl.this.getView()).onActivityNoteDeleted(success);
            }
        }
    };
    private OnTaskFinished UPDATE_TASK_CALLBACK = new OnTaskFinished() {
        public void onActivityUpdated(boolean success) {
            if (ActivityNoteEditorPresenterImpl.this.getView() != null) {
                ((ActivityNoteEditorView) ActivityNoteEditorPresenterImpl.this.getView()).onActivityNoteUpdated(success);
            }
        }
    };

    public ActivityNoteEditorPresenterImpl(@NonNull Session session, @Nullable Bundle savedState) {
        super(session, savedState);
    }

    void requestExistingActivityNote(@IntRange(from = 1) long sqlId) {
        cacheDataAndUpdateTheView((Activity) new ActivitiesDataSource(getSession()).findBySqlId(sqlId));
    }

    void requestNoteForNewActivity(@Nullable String string) {
        Activity activity = new Activity();
        activity.setHtmlContent(string);
        cacheDataAndUpdateTheView(activity);
    }

    void changeActivityNote() {
        if (getData() != null) {
            if (((Activity) getData()).isStored()) {
                new UpdateActivityTask(getSession(), this.UPDATE_TASK_CALLBACK).executeOnAsyncTaskExecutor(this, new Activity[]{(Activity) getData()});
            } else if (getView() != null) {
                ((ActivityNoteEditorView) getView()).onActivityNoteUpdated(true);
            }
        }
    }

    void deleteActivityNote() {
        if (getData() != null) {
            ((Activity) getData()).setHtmlContent("");
            new UpdateActivityNoteTask(getSession(), this.DELETE_TASK_CALLBACK).executeOnAsyncTaskExecutor(this, new Activity[]{(Activity) getData()});
        }
    }
}
