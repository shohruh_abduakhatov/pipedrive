package okhttp3.internal.cache;

import com.newrelic.agent.android.instrumentation.okhttp3.OkHttp3Instrumentation;
import com.zendesk.service.HttpConstants;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Headers;
import okhttp3.Headers.Builder;
import okhttp3.Interceptor;
import okhttp3.Interceptor.Chain;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Response$Builder;
import okhttp3.ResponseBody;
import okhttp3.internal.Internal;
import okhttp3.internal.Util;
import okhttp3.internal.cache.CacheStrategy.Factory;
import okhttp3.internal.http.HttpHeaders;
import okhttp3.internal.http.HttpMethod;
import okhttp3.internal.http.RealResponseBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;
import okio.Sink;
import okio.Source;
import okio.Timeout;

public final class CacheInterceptor implements Interceptor {
    final InternalCache cache;

    public CacheInterceptor(InternalCache cache) {
        this.cache = cache;
    }

    public Response intercept(Chain chain) throws IOException {
        Response cacheCandidate = this.cache != null ? this.cache.get(chain.request()) : null;
        CacheStrategy strategy = new Factory(System.currentTimeMillis(), chain.request(), cacheCandidate).get();
        Request networkRequest = strategy.networkRequest;
        Response cacheResponse = strategy.cacheResponse;
        if (this.cache != null) {
            this.cache.trackResponse(strategy);
        }
        if (cacheCandidate != null && cacheResponse == null) {
            Util.closeQuietly(cacheCandidate.body());
        }
        Response$Builder message;
        if (networkRequest == null && cacheResponse == null) {
            message = new Response$Builder().request(chain.request()).protocol(Protocol.HTTP_1_1).code(HttpConstants.HTTP_GATEWAY_TIMEOUT).message("Unsatisfiable Request (only-if-cached)");
            ResponseBody responseBody = Util.EMPTY_RESPONSE;
            return (!(message instanceof Response$Builder) ? message.body(responseBody) : OkHttp3Instrumentation.body(message, responseBody)).sentRequestAtMillis(-1).receivedResponseAtMillis(System.currentTimeMillis()).build();
        } else if (networkRequest == null) {
            return (!(cacheResponse instanceof Response$Builder) ? cacheResponse.newBuilder() : OkHttp3Instrumentation.newBuilder((Response$Builder) cacheResponse)).cacheResponse(stripBody(cacheResponse)).build();
        } else {
            Response networkResponse = null;
            try {
                Response response;
                networkResponse = chain.proceed(networkRequest);
                if (cacheResponse != null) {
                    if (networkResponse.code() == HttpConstants.HTTP_NOT_MODIFIED) {
                        if (cacheResponse instanceof Response$Builder) {
                            message = OkHttp3Instrumentation.newBuilder((Response$Builder) cacheResponse);
                        } else {
                            message = cacheResponse.newBuilder();
                        }
                        response = message.headers(combine(cacheResponse.headers(), networkResponse.headers())).sentRequestAtMillis(networkResponse.sentRequestAtMillis()).receivedResponseAtMillis(networkResponse.receivedResponseAtMillis()).cacheResponse(stripBody(cacheResponse)).networkResponse(stripBody(networkResponse)).build();
                        networkResponse.body().close();
                        this.cache.trackConditionalCacheHit();
                        this.cache.update(cacheResponse, response);
                        return response;
                    }
                    Util.closeQuietly(cacheResponse.body());
                }
                response = (!(networkResponse instanceof Response$Builder) ? networkResponse.newBuilder() : OkHttp3Instrumentation.newBuilder((Response$Builder) networkResponse)).cacheResponse(stripBody(cacheResponse)).networkResponse(stripBody(networkResponse)).build();
                if (HttpHeaders.hasBody(response)) {
                    return cacheWritingResponse(maybeCache(response, networkResponse.request(), this.cache), response);
                }
                return response;
            } finally {
                if (networkResponse == null && cacheCandidate != null) {
                    Util.closeQuietly(cacheCandidate.body());
                }
            }
        }
    }

    private static Response stripBody(Response response) {
        if (response == null || response.body() == null) {
            return response;
        }
        Response$Builder newBuilder = !(response instanceof Response$Builder) ? response.newBuilder() : OkHttp3Instrumentation.newBuilder((Response$Builder) response);
        return (!(newBuilder instanceof Response$Builder) ? newBuilder.body(null) : OkHttp3Instrumentation.body(newBuilder, null)).build();
    }

    private CacheRequest maybeCache(Response userResponse, Request networkRequest, InternalCache responseCache) throws IOException {
        if (responseCache == null) {
            return null;
        }
        if (CacheStrategy.isCacheable(userResponse, networkRequest)) {
            return responseCache.put(userResponse);
        }
        if (!HttpMethod.invalidatesCache(networkRequest.method())) {
            return null;
        }
        try {
            responseCache.remove(networkRequest);
            return null;
        } catch (IOException e) {
            return null;
        }
    }

    private Response cacheWritingResponse(final CacheRequest cacheRequest, Response response) throws IOException {
        if (cacheRequest == null) {
            return response;
        }
        Sink cacheBodyUnbuffered = cacheRequest.body();
        if (cacheBodyUnbuffered == null) {
            return response;
        }
        final BufferedSource source = response.body().source();
        final BufferedSink cacheBody = Okio.buffer(cacheBodyUnbuffered);
        Source cacheWritingSource = new Source() {
            boolean cacheRequestClosed;

            public long read(Buffer sink, long byteCount) throws IOException {
                try {
                    long bytesRead = source.read(sink, byteCount);
                    if (bytesRead == -1) {
                        if (!this.cacheRequestClosed) {
                            this.cacheRequestClosed = true;
                            cacheBody.close();
                        }
                        return -1;
                    }
                    sink.copyTo(cacheBody.buffer(), sink.size() - bytesRead, bytesRead);
                    cacheBody.emitCompleteSegments();
                    return bytesRead;
                } catch (IOException e) {
                    if (!this.cacheRequestClosed) {
                        this.cacheRequestClosed = true;
                        cacheRequest.abort();
                    }
                    throw e;
                }
            }

            public Timeout timeout() {
                return source.timeout();
            }

            public void close() throws IOException {
                if (!(this.cacheRequestClosed || Util.discard(this, 100, TimeUnit.MILLISECONDS))) {
                    this.cacheRequestClosed = true;
                    cacheRequest.abort();
                }
                source.close();
            }
        };
        Response$Builder newBuilder = !(response instanceof Response$Builder) ? response.newBuilder() : OkHttp3Instrumentation.newBuilder((Response$Builder) response);
        ResponseBody realResponseBody = new RealResponseBody(response.headers(), Okio.buffer(cacheWritingSource));
        return (!(newBuilder instanceof Response$Builder) ? newBuilder.body(realResponseBody) : OkHttp3Instrumentation.body(newBuilder, realResponseBody)).build();
    }

    private static Headers combine(Headers cachedHeaders, Headers networkHeaders) {
        int i;
        Builder result = new Builder();
        int size = cachedHeaders.size();
        for (i = 0; i < size; i++) {
            String fieldName = cachedHeaders.name(i);
            String value = cachedHeaders.value(i);
            if (!("Warning".equalsIgnoreCase(fieldName) && value.startsWith("1")) && (!isEndToEnd(fieldName) || networkHeaders.get(fieldName) == null)) {
                Internal.instance.addLenient(result, fieldName, value);
            }
        }
        size = networkHeaders.size();
        for (i = 0; i < size; i++) {
            fieldName = networkHeaders.name(i);
            if (!"Content-Length".equalsIgnoreCase(fieldName) && isEndToEnd(fieldName)) {
                Internal.instance.addLenient(result, fieldName, networkHeaders.value(i));
            }
        }
        return result.build();
    }

    static boolean isEndToEnd(String fieldName) {
        if ("Connection".equalsIgnoreCase(fieldName) || "Keep-Alive".equalsIgnoreCase(fieldName) || "Proxy-Authenticate".equalsIgnoreCase(fieldName) || HttpRequest.HEADER_PROXY_AUTHORIZATION.equalsIgnoreCase(fieldName) || "TE".equalsIgnoreCase(fieldName) || "Trailers".equalsIgnoreCase(fieldName) || "Transfer-Encoding".equalsIgnoreCase(fieldName) || "Upgrade".equalsIgnoreCase(fieldName)) {
            return false;
        }
        return true;
    }
}
