package com.pipedrive.views.viewholder.activity;

import com.pipedrive.R;

public class ActivityRowViewHolderWithCheckBoxForFlow extends ActivityRowViewHolderWithCheckbox {
    public int getLayoutResourceId() {
        return R.layout.row_activity_with_checkbox_for_flow;
    }
}
