package com.pipedrive.fragments.customfields;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.widget.DatePicker;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.background.ApplicationStateMonitor;
import com.newrelic.agent.android.instrumentation.Instrumented;
import com.pipedrive.datetime.DateFormatHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.util.time.TimeManager;
import java.text.ParseException;
import java.util.Calendar;

@Instrumented
public class DatePickerFragment extends DialogFragment implements OnDateSetListener, TraceFieldInterface {
    public static final String VALUE_DATE = "date";
    public static final String VALUE_IS_START_FIELD = "isStartField";
    private DateTimePickerInterface mDateTimePickerInterface;
    private boolean mIsStartField;

    protected void onStart() {
        super.onStart();
        ApplicationStateMonitor.getInstance().activityStarted();
    }

    protected void onStop() {
        super.onStop();
        ApplicationStateMonitor.getInstance().activityStopped();
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar calendar = TimeManager.getInstance().getLocalCalendar();
        Bundle bundle = getArguments();
        String dateValueString = bundle.getString("date");
        this.mIsStartField = bundle.getBoolean("isStartField");
        if (!TextUtils.isEmpty(dateValueString)) {
            try {
                calendar.setTime(DateFormatHelper.dateFormat2().parse(dateValueString));
            } catch (ParseException e) {
                Log.e(e);
            }
        }
        return new DatePickerDialog(getActivity(), this, calendar.get(1), calendar.get(2), calendar.get(5));
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        if (this.mDateTimePickerInterface != null) {
            Calendar calendar = TimeManager.getInstance().getLocalCalendar();
            calendar.set(1, year);
            calendar.set(2, month);
            calendar.set(5, day);
            this.mDateTimePickerInterface.dateChanged(calendar.getTime(), this.mIsStartField);
        }
    }

    public void setDateTimePickerCallback(DateTimePickerInterface dateTimePickerInterface) {
        this.mDateTimePickerInterface = dateTimePickerInterface;
    }
}
