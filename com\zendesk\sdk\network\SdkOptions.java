package com.zendesk.sdk.network;

import java.util.List;
import okhttp3.ConnectionSpec;

public interface SdkOptions {

    public interface ServiceOptions {
        List<ConnectionSpec> getConnectionSpecs();
    }

    ServiceOptions getServiceOptions();

    boolean overrideResourceLoadingInWebview();
}
