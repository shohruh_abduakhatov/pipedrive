package com.zendesk.sdk.model.request.fields;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.util.CollectionUtils;
import java.util.ArrayList;
import java.util.List;

public class TicketField {
    private String description;
    private long id;
    private String regexpForValidation;
    private List<TicketFieldOption> ticketFieldOptions;
    private String title;
    private String titleInPortal;
    private TicketFieldType type;

    public static TicketField create(RawTicketField rawTicketField) {
        TicketFieldType type;
        List<TicketFieldOption> ticketFieldOptions = new ArrayList();
        for (RawTicketFieldOption rtfo : rawTicketField.getCustomFieldOptions()) {
            ticketFieldOptions.add(TicketFieldOption.create(rtfo));
        }
        if (rawTicketField.getType() != null) {
            type = rawTicketField.getType();
        } else {
            type = TicketFieldType.Unknown;
        }
        return new TicketField(rawTicketField.getId(), type, rawTicketField.getTitle(), rawTicketField.getTitleInPortal(), rawTicketField.getDescription(), rawTicketField.getRegexpForValidation(), ticketFieldOptions);
    }

    public TicketField(long id, @NonNull TicketFieldType type, @NonNull String title, @NonNull String titleInPortal, @NonNull String description, @Nullable String regexpForValidation, @NonNull List<TicketFieldOption> ticketFieldOptions) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.titleInPortal = titleInPortal;
        this.description = description;
        this.regexpForValidation = regexpForValidation;
        this.ticketFieldOptions = ticketFieldOptions;
    }

    public long getId() {
        return this.id;
    }

    @NonNull
    public TicketFieldType getType() {
        return this.type;
    }

    @NonNull
    public String getTitle() {
        return this.title;
    }

    public String getTitleInPortal() {
        return this.titleInPortal;
    }

    @NonNull
    public String getDescription() {
        return this.description;
    }

    @Nullable
    public String getRegexpForValidation() {
        return this.regexpForValidation;
    }

    public List<TicketFieldOption> getTicketFieldOptions() {
        return CollectionUtils.copyOf(this.ticketFieldOptions);
    }
}
