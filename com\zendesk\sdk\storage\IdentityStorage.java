package com.zendesk.sdk.storage;

import android.support.annotation.Nullable;
import com.zendesk.sdk.model.access.AccessToken;
import com.zendesk.sdk.model.access.Identity;
import com.zendesk.sdk.storage.SdkStorage.UserStorage;

public interface IdentityStorage extends UserStorage {
    @Nullable
    Identity anonymiseIdentity();

    @Nullable
    Identity getIdentity();

    AccessToken getStoredAccessToken();

    String getStoredAccessTokenAsBearerToken();

    String getUUID();

    void storeAccessToken(AccessToken accessToken);

    void storeIdentity(Identity identity);
}
