package com.pipedrive.nearby.cards.cards;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class CardHeader_ViewBinding implements Unbinder {
    private CardHeader target;

    @UiThread
    public CardHeader_ViewBinding(CardHeader target, View source) {
        this.target = target;
        target.titleTextView = (TextView) Utils.findRequiredViewAsType(source, R.id.nearbyCard.title, "field 'titleTextView'", TextView.class);
        target.subtitleTextView = (TextView) Utils.findRequiredViewAsType(source, R.id.nearbyCard.subtitle, "field 'subtitleTextView'", TextView.class);
        target.subtitleSeparator = source.getContext().getResources().getString(R.string.subtitle_separator);
    }

    @CallSuper
    public void unbind() {
        CardHeader target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.titleTextView = null;
        target.subtitleTextView = null;
    }
}
