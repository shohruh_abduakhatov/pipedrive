package com.google.android.gms.auth.api.credentials;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.List;

public class zzf implements Creator<PasswordSpecification> {
    static void zza(PasswordSpecification passwordSpecification, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zza(parcel, 1, passwordSpecification.iI, false);
        zzb.zzb(parcel, 2, passwordSpecification.iJ, false);
        zzb.zza(parcel, 3, passwordSpecification.iK, false);
        zzb.zzc(parcel, 4, passwordSpecification.iL);
        zzb.zzc(parcel, 5, passwordSpecification.iM);
        zzb.zzc(parcel, 1000, passwordSpecification.mVersionCode);
        zzb.zzaj(parcel, zzcs);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzao(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzdc(i);
    }

    public PasswordSpecification zzao(Parcel parcel) {
        List list = null;
        int i = 0;
        int zzcr = zza.zzcr(parcel);
        int i2 = 0;
        List list2 = null;
        String str = null;
        int i3 = 0;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    str = zza.zzq(parcel, zzcq);
                    break;
                case 2:
                    list2 = zza.zzae(parcel, zzcq);
                    break;
                case 3:
                    list = zza.zzad(parcel, zzcq);
                    break;
                case 4:
                    i2 = zza.zzg(parcel, zzcq);
                    break;
                case 5:
                    i = zza.zzg(parcel, zzcq);
                    break;
                case 1000:
                    i3 = zza.zzg(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new PasswordSpecification(i3, str, list2, list, i2, i);
        }
        throw new zza.zza("Overread allowed size end=" + zzcr, parcel);
    }

    public PasswordSpecification[] zzdc(int i) {
        return new PasswordSpecification[i];
    }
}
