package com.pipedrive.util.networking.search;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pipedrive.model.BaseDatasourceEntity;

public abstract class SearchResultEntity<ENTITY extends BaseDatasourceEntity> {
    public static final String JSON_PARAM_TYPE = "type";
    public static final String SEARCH_RESULT_TYPE_DEAL = "deal";
    public static final String SEARCH_RESULT_TYPE_ORGANIZATION = "organization";
    public static final String SEARCH_RESULT_TYPE_PERSON = "person";
    @Nullable
    @SerializedName("title")
    @Expose
    private String name;
    @Nullable
    @SerializedName("id")
    @Expose
    private Integer pipedriveId;

    @NonNull
    public abstract Boolean isDifferentFrom(@NonNull ENTITY entity);

    @Nullable
    public Integer getPipedriveId() {
        return this.pipedriveId;
    }

    @Nullable
    public String getName() {
        return this.name;
    }

    SearchResultEntity(@Nullable Integer pipedriveId, @Nullable String name) {
        this.pipedriveId = pipedriveId;
        this.name = name;
    }
}
