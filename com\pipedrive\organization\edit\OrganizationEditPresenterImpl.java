package com.pipedrive.organization.edit;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.model.Organization;
import com.pipedrive.model.delta.ObjectDelta;

class OrganizationEditPresenterImpl extends OrganizationEditPresenter {
    private final OnTaskFinished CREATE_TASK_CALLBACK = new OnTaskFinished() {
        public void taskFinished(boolean success) {
            if (OrganizationEditPresenterImpl.this.getView() != null) {
                ((OrganizationEditView) OrganizationEditPresenterImpl.this.getView()).onOrganizationCreated(success);
            }
        }
    };
    private final OnTaskFinished DELETE_TASK_CALLBACK = new OnTaskFinished() {
        public void taskFinished(boolean success) {
            if (OrganizationEditPresenterImpl.this.getView() != null) {
                ((OrganizationEditView) OrganizationEditPresenterImpl.this.getView()).onOrganizationDeleted(success);
            }
        }
    };
    private final OnTaskFinished READ_TASK_CALLBACK = new OnTaskFinished() {
        public void onOrganizationRead(@Nullable Organization organization) {
            OrganizationEditPresenterImpl.this.cacheAndSendOrganizationToView(organization);
        }
    };
    private final OnTaskFinished UPDATE_TASK_CALLBACK = new OnTaskFinished() {
        public void taskFinished(boolean success) {
            if (OrganizationEditPresenterImpl.this.getView() != null) {
                ((OrganizationEditView) OrganizationEditPresenterImpl.this.getView()).onOrganizationUpdated(success);
            }
        }
    };
    @Nullable
    private Organization mOrganization;
    @NonNull
    private final ObjectDelta<Organization> mOrganizationObjectDelta = new ObjectDelta(Organization.class);
    @Nullable
    private Organization mOriginalOrganization;

    public OrganizationEditPresenterImpl(@NonNull Session session) {
        super(session);
    }

    void requestOrganization(@NonNull Long organizationSqlId) {
        new ReadOrganizationTasks(getSession(), this.READ_TASK_CALLBACK).executeOnAsyncTaskExecutor(this, new Long[]{organizationSqlId});
    }

    void requestNewOrganization() {
        setupCommonFieldsAndSentToView(new Organization());
    }

    private void setupCommonFieldsAndSentToView(Organization organization) {
        Session session = getSession();
        organization.setOwnerPipedriveId((int) session.getAuthenticatedUserID());
        organization.setOwnerName(session.getUserSettingsName(null));
        int userSettingsDefaultVisibilityForDeal = session.getUserSettingsDefaultVisibilityForDeal(Integer.MIN_VALUE);
        if (userSettingsDefaultVisibilityForDeal != Integer.MIN_VALUE) {
            organization.setVisibleTo(userSettingsDefaultVisibilityForDeal);
        }
        cacheAndSendOrganizationToView(organization);
    }

    void requestNewOrganizationWithName(@Nullable String orgName) {
        Organization organization = new Organization();
        if (orgName != null) {
            organization.setName(orgName);
        }
        setupCommonFieldsAndSentToView(organization);
    }

    void createOrUpdateOrganization() {
        if (this.mOrganization != null) {
            if (this.mOrganization.isStored()) {
                new UpdateOrganizationTask(getSession(), this.UPDATE_TASK_CALLBACK).executeOnAsyncTaskExecutor(this, new Organization[]{this.mOrganization});
                return;
            }
            new CreateOrganizationTask(getSession(), this.CREATE_TASK_CALLBACK).executeOnAsyncTaskExecutor(this, new Organization[]{this.mOrganization});
        }
    }

    void deleteOrganization() {
        new DeleteOrganizationTask(getSession(), this.DELETE_TASK_CALLBACK).executeOnAsyncTaskExecutor(this, new Organization[]{this.mOrganization});
    }

    public boolean isModified() {
        return this.mOrganization != null && (this.mOrganizationObjectDelta.hasChanges(this.mOrganization, this.mOriginalOrganization) || !this.mOrganization.isStored());
    }

    @Nullable
    public Organization getOrganization() {
        return this.mOrganization;
    }

    @Nullable
    public Boolean enableOrganizationAddressChange() {
        if (this.mOrganization == null) {
            return null;
        }
        return Boolean.valueOf(Organization.isAddressFieldDefault(getSession()));
    }

    private void cacheAndSendOrganizationToView(@Nullable Organization contact) {
        this.mOrganization = contact;
        if (this.mOrganization != null) {
            this.mOriginalOrganization = this.mOrganization.getDeepCopy();
        }
        if (getView() != null) {
            ((OrganizationEditView) getView()).onRequestOrganization(this.mOrganization);
        }
    }
}
