package com.pipedrive.pipeline;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.datasource.PipelineDataSource;
import com.pipedrive.pipeline.InvalidateDealsTask.OnProgressUpdated;
import com.pipedrive.pipeline.InvalidatePipelinesTask.OnTaskFinished;

class PipelinePresenterImpl extends PipelinePresenter {
    public PipelinePresenterImpl(Session session) {
        super(session);
    }

    private void invalidatePipelines() {
        if (shouldInvalidatePipelines()) {
            new InvalidatePipelinesTask(getSession(), new OnTaskFinished() {
                public void allPipelinesDownloaded(@NonNull Session session) {
                    PipelinePresenterImpl.this.onPipelinesInvalidated(session);
                }

                public void taskNotExecutedDueToOfflineMode(@NonNull Session session) {
                    PipelinePresenterImpl.this.onPipelinesInvalidated(session);
                }
            }).executeOnAsyncTaskExecutor(this, new Void[0]);
            return;
        }
        onPipelinesInvalidated(getSession());
    }

    private int getSelectedPipelinePosition() {
        Cursor pipelineCursor = new PipelineDataSource(getSession().getDatabase()).getPipelinesCursor();
        try {
            pipelineCursor.moveToPosition(-1);
            int index = 0;
            long pipelineSelectedPipelineId = getSession().getPipelineSelectedPipelineId(0);
            while (pipelineCursor.moveToNext()) {
                if (pipelineCursor.getLong(pipelineCursor.getColumnIndex(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID)) == pipelineSelectedPipelineId) {
                    return index;
                }
                index++;
            }
            pipelineCursor.close();
            return 0;
        } finally {
            pipelineCursor.close();
        }
    }

    private void onPipelinesInvalidated(@NonNull Session session) {
        invalidateFilters(session);
        if (this.mView != null) {
            Cursor pipelinesCursor = new PipelineDataSource(session.getDatabase()).getPipelinesCursor();
            pipelinesCursor.moveToFirst();
            if (pipelinesCursor.getCount() == 0) {
                ((PipelineView) this.mView).setPipelinesFailedToDownload();
            } else {
                ((PipelineView) this.mView).setPipelines(pipelinesCursor, getSelectedPipelinePosition());
            }
        }
    }

    private void invalidateStages(@NonNull Session session, long pipelineId) {
        if (shouldInvalidateStages()) {
            new InvalidateStagesTask(session, pipelineId, new InvalidateStagesTask.OnTaskFinished() {
                public void allStagesDownloaded(@NonNull Session session, long pipelineId) {
                    PipelinePresenterImpl.this.getSession().getRuntimeCache().shouldInvalidateStages = false;
                    PipelinePresenterImpl.this.onStagesInvalidated(session, pipelineId);
                }

                public void taskNotExecutedDueToOfflineMode(@NonNull Session session, long pipelineId) {
                    PipelinePresenterImpl.this.onStagesInvalidated(session, pipelineId);
                }
            }).executeOnAsyncTaskExecutor(this, new Void[0]);
        } else {
            onStagesInvalidated(session, pipelineId);
        }
    }

    private void onStagesInvalidated(@NonNull Session session, long pipelineId) {
        session.setPipelineSelectedPipelineId(Long.valueOf(pipelineId));
        if (this.mView != null) {
            Cursor stagesCursor = new PipelineDataSource(getSession().getDatabase()).getStagesCursor(pipelineId);
            stagesCursor.moveToFirst();
            if (stagesCursor.getCount() == 0) {
                ((PipelineView) this.mView).setStagesFailedToDownload();
            } else {
                ((PipelineView) this.mView).setStages(session, stagesCursor);
            }
        }
    }

    private void resetStageValidations(@NonNull Session session) {
        session.getRuntimeCache().shouldInvalidateDeals.clear();
    }

    private void invalidateDeals(int stageId, long filterId, long userId) {
        onDealsInvalidating(getSession(), stageId, filterId, userId);
        if (shouldInvalidateDeals(stageId)) {
            new InvalidateDealsTask(getSession(), stageId, filterId, userId, new OnProgressUpdated() {
                public void newDealsDownloaded(@NonNull Session session, int stageId, long filterId, long userId, @Nullable Boolean moreDealsToBeDownloaded) {
                    PipelinePresenterImpl.this.onDealsInvalidating(session, stageId, filterId, userId);
                }
            }, new InvalidateDealsTask.OnTaskFinished() {
                public void filterNotFound(@NonNull Session session, int stageId, long filterId, long userId) {
                    PipelinePresenterImpl.this.resetFilterValidation(session);
                    PipelinePresenterImpl.this.invalidateFilters(session);
                }

                public void stageNotFound(@NonNull Session session, int stageId, long filterId, long userId) {
                    PipelinePresenterImpl.this.resetStageValidations(session);
                    PipelinePresenterImpl.this.invalidateStages(session, (long) ((int) PipelinePresenterImpl.this.getSession().getPipelineSelectedPipelineId(0)));
                }

                public void allDealsDownloaded(@NonNull Session session, int stageId, long filterId, long userId) {
                    PipelinePresenterImpl.this.onDealsInvalidated(session, stageId, filterId, userId);
                }

                public void taskNotExecutedDueToOfflineMode(@NonNull Session session, int stageId, long filterId, long userId) {
                    PipelinePresenterImpl.this.onDealsInvalidated(session, stageId, filterId, userId);
                }

                public void dealsNotDownloadedDueToError(@NonNull Session session, int stageId, long filterId, long userId) {
                    PipelinePresenterImpl.this.onDealsInvalidationFailed(session, stageId, filterId, userId);
                }
            }).executeOnAsyncTaskExecutor(this, new Void[0]);
            return;
        }
        onDealsInvalidated(getSession(), stageId, filterId, userId);
    }

    private void updateDealsView(int stageId, long filterId, long userId, boolean loading, boolean invalidationSucceeded) {
        if (this.mView != null) {
            StageData stageData = getStageData(getSession(), stageId, filterId, userId);
            boolean thereAreDealsToDisplay = stageData.getDealsCursor() != null && stageData.getDealsCursor().getCount() > 0;
            if (invalidationSucceeded || thereAreDealsToDisplay) {
                ((PipelineView) this.mView).setDeals(stageId, getStageData(getSession(), stageId, filterId, userId));
            } else {
                ((PipelineView) this.mView).setDealsFailedToDownload(stageId);
            }
            ((PipelineView) this.mView).setDealsLoading(stageId, loading);
        }
    }

    @NonNull
    private StageData getStageData(@NonNull Session session, int stageId, long filterId, long userId) {
        Long filterIdQueryParam = filterId <= 0 ? null : Long.valueOf(filterId);
        Long userIdQueryParam = userId <= 0 ? null : Long.valueOf(userId);
        DealsDataSource dealsDataSource = new DealsDataSource(session.getDatabase());
        Pair<Integer, Float> dealsSummaryForStage = dealsDataSource.getDealsValueForStage(session, stageId, filterIdQueryParam, userIdQueryParam);
        return new StageData(dealsSummaryForStage == null ? null : (Float) dealsSummaryForStage.second, dealsSummaryForStage == null ? null : (Integer) dealsSummaryForStage.first, dealsDataSource.getDealsForStage(session, (long) stageId, filterIdQueryParam, userIdQueryParam));
    }

    private void onDealsInvalidating(@NonNull Session session, int stageId, long filterId, long userId) {
        if (this.mView != null) {
            ((PipelineView) this.mView).setDeals(stageId, getStageData(session, stageId, filterId, userId));
        }
    }

    private void onDealsInvalidated(@NonNull Session session, int stageId, long filterId, long userId) {
        session.getRuntimeCache().shouldInvalidateDeals.append(stageId, false);
        updateDealsView(stageId, filterId, userId, false, true);
    }

    private void onDealsInvalidationFailed(@NonNull Session ignored, int stageId, long filterId, long userId) {
        updateDealsView(stageId, filterId, userId, false, false);
    }

    private void invalidateFilters(@NonNull Session session) {
        if (shouldInvalidateFilters(session)) {
            new InvalidateFilterTask(session, new InvalidateFilterTask.OnTaskFinished() {
                public void selectedFilterDoesNotExist(@NonNull Session session) {
                    session.getRuntimeCache().shouldInvalidateFilters = false;
                    PipelinePresenterImpl.this.invalidatePipelines();
                }

                public void selectedUserDoesNotExist(@NonNull Session session) {
                    session.getRuntimeCache().shouldInvalidateFilters = false;
                    PipelinePresenterImpl.this.invalidatePipelines();
                }

                public void allFiltersDownloaded(@NonNull Session session) {
                    session.getRuntimeCache().shouldInvalidateFilters = false;
                    PipelinePresenterImpl.this.onFiltersInvalidated();
                }

                public void taskNotExecutedDueToOfflineMode(@NonNull Session session) {
                    PipelinePresenterImpl.this.onFiltersInvalidated();
                }
            }).executeOnAsyncTaskExecutor(this, new Void[0]);
            if (this.mView != null) {
                ((PipelineView) this.mView).setFiltersLoading(true);
                return;
            }
            return;
        }
        onFiltersInvalidated();
    }

    private void resetFilterValidation(@NonNull Session session) {
        session.getRuntimeCache().shouldInvalidateFilters = true;
    }

    private void onFiltersInvalidated() {
        if (this.mView != null) {
            ((PipelineView) this.mView).setFilters();
            ((PipelineView) this.mView).setFiltersLoading(false);
        }
    }

    public void requestPipelines() {
        invalidatePipelines();
    }

    public void requestAllPipelineStages(int pipelineId) {
        resetStageValidations(getSession());
        invalidateStages(getSession(), (long) pipelineId);
    }

    public void onFilterChanged() {
        long pipelineId = getSession().getPipelineSelectedPipelineId(0);
        resetStageValidations(getSession());
        onStagesInvalidated(getSession(), (long) ((int) pipelineId));
    }

    public void requestDeals(int stageId) {
        invalidateDeals(stageId, getSession().getPipelineFilter(Long.MIN_VALUE), getSession().getPipelineUser(Long.MIN_VALUE));
    }

    public void onStageRefresh(int stageId) {
        resetStageValidations(getSession());
    }

    void requery(@NonNull Integer stageId) {
        long filterId = getSession().getPipelineFilter(Long.MIN_VALUE);
        long userId = getSession().getPipelineUser(Long.MIN_VALUE);
        if (getView() != null) {
            ((PipelineView) getView()).setDeals(stageId.intValue(), getStageData(getSession(), stageId.intValue(), filterId, userId));
        }
    }

    private boolean shouldInvalidateDeals(int stageId) {
        return getSession().getRuntimeCache().shouldInvalidateDeals.get(stageId, true);
    }

    private boolean shouldInvalidateStages() {
        return getSession().getRuntimeCache().shouldInvalidateStages;
    }

    private boolean shouldInvalidateFilters(@NonNull Session session) {
        return session.getRuntimeCache().shouldInvalidateFilters;
    }

    private boolean shouldInvalidatePipelines() {
        return getSession().getRuntimeCache().shouldInvalidatePipelines;
    }
}
