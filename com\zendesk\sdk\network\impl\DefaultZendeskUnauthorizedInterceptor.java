package com.zendesk.sdk.network.impl;

import com.zendesk.sdk.storage.SdkStorage;
import com.zendesk.service.HttpConstants;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Interceptor.Chain;
import okhttp3.Response;

public class DefaultZendeskUnauthorizedInterceptor implements Interceptor {
    private final SdkStorage sdkStorage;

    public DefaultZendeskUnauthorizedInterceptor(SdkStorage sdkStorage) {
        this.sdkStorage = sdkStorage;
    }

    public Response intercept(Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());
        if (!response.isSuccessful() && HttpConstants.HTTP_UNAUTHORIZED == response.code()) {
            onHttpUnauthorized();
        }
        return response;
    }

    public void onHttpUnauthorized() {
        this.sdkStorage.clearUserData();
    }
}
