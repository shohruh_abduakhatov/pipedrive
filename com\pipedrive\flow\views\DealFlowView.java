package com.pipedrive.flow.views;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import com.pipedrive.application.Session;

public final class DealFlowView extends FlowView {
    public DealFlowView(Context context) {
        this(context, null);
    }

    public DealFlowView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DealFlowView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @NonNull
    protected Cursor getPlannedFlowCursor(@NonNull Session session) {
        return getFlowDataSource(session).getDealPlannedFlowCursor(this.mSqlId);
    }

    @NonNull
    protected Cursor getPastFlowCursor(@NonNull Session session) {
        return getFlowDataSource(session).getDealPastFlowCursor(this.mSqlId);
    }
}
