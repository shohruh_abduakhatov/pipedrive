package com.pipedrive.store;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.tasks.AsyncTask;
import com.pipedrive.util.recents.RecentsQueryUtil;

class StoreSynchronizeTask extends AsyncTask<Void, Void, Void> {
    StoreSynchronizeTask(@NonNull Session session) {
        super(session);
    }

    protected void onPreExecute() {
        getSession().getStoreSynchronizeManager().offlineChangesSyncStarted();
    }

    protected Void doInBackground(Void... params) {
        RecentsQueryUtil.processChangesAndDownloadRecentsBlocking(getSession());
        return null;
    }

    protected void onPostExecute(Void aVoid) {
        getSession().getStoreSynchronizeManager().offlineChangesSyncFinished();
    }
}
