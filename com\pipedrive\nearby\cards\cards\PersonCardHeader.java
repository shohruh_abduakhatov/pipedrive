package com.pipedrive.nearby.cards.cards;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.ViewGroup;
import butterknife.BindInt;
import butterknife.BindView;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.model.Person;
import com.pipedrive.nearby.model.PersonNearbyItem;
import com.pipedrive.views.profilepicture.ProfilePictureRoundPerson;

public class PersonCardHeader extends CardHeader<PersonNearbyItem, Person> {
    @BindInt(2131689496)
    int initialsTextSize;
    @BindView(2131821169)
    ProfilePictureRoundPerson profilePicture;

    public void bind(@NonNull ViewGroup view, @NonNull Session session, @NonNull PersonNearbyItem nearbyItem, @NonNull Person entity, @Nullable Double distanceInMeters) {
        super.bind(view, session, nearbyItem, entity, distanceInMeters);
        this.profilePicture.setTextSize((float) this.initialsTextSize);
        this.profilePicture.loadPicture(session, entity);
    }

    @NonNull
    String getSubTitle(@NonNull Session session, @NonNull PersonNearbyItem nearbyItem, @NonNull Person entity) {
        Integer openDealCount = (Integer) nearbyItem.getOpenDealCount(session).toBlocking().first();
        String companyName = entity.getCompany().getName();
        String dealCount = openDealCount.intValue() == 0 ? "" : this.context.getResources().getQuantityString(R.plurals.cards_open_deals, openDealCount.intValue(), new Object[]{openDealCount});
        return dealCount.isEmpty() ? companyName : dealCount + this.subtitleSeparator + companyName;
    }

    @NonNull
    protected String getTitle(@NonNull Person entity) {
        return entity.getName();
    }

    public void propagateClickEvents(@NonNull Long itemSqlId) {
        CardEventBus.INSTANCE.onPersonDetailsClicked(itemSqlId);
    }
}
