package com.pipedrive.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import com.pipedrive.application.Session;
import com.pipedrive.contentproviders.GlobalSearchContentProvider;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.interfaces.ContactOrgInterface;
import com.pipedrive.views.viewholder.ViewHolderBuilder;
import com.pipedrive.views.viewholder.contact.OrganizationRowViewHolderForSearch;
import com.pipedrive.views.viewholder.contact.PersonRowViewHolder.OnCallButtonClickListener;
import com.pipedrive.views.viewholder.contact.PersonRowViewHolderForSearch;
import com.pipedrive.views.viewholder.deal.DealRowViewHolderForSearch;

public class GlobalSearchAdapter extends CursorAdapter {
    public static final int TYPE_CONTACT = 0;
    public static final int TYPE_DEAL = 2;
    public static final int TYPE_ORG = 1;
    private OnCallButtonClickListener mOnCallButtonClickListener;
    @NonNull
    private final Session mSession;
    @NonNull
    private SearchConstraint searchConstraint = SearchConstraint.EMPTY;

    public GlobalSearchAdapter(@NonNull Session session, @NonNull Context context, @Nullable Cursor cursor, int flags) {
        super(context, cursor, flags);
        this.mSession = session;
    }

    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        int type = getItemViewType(cursor.getPosition());
        if (type == 1) {
            return ViewHolderBuilder.inflateViewAndTagWithViewHolder(context, parent, false, new OrganizationRowViewHolderForSearch());
        }
        if (type == 0) {
            return ViewHolderBuilder.inflateViewAndTagWithViewHolder(context, parent, false, new PersonRowViewHolderForSearch());
        }
        if (type == 2) {
            return ViewHolderBuilder.inflateViewAndTagWithViewHolder(context, parent, false, new DealRowViewHolderForSearch());
        }
        return null;
    }

    public void bindView(View v, Context context, Cursor c) {
        if (v.getTag() instanceof OrganizationRowViewHolderForSearch) {
            OrganizationRowViewHolderForSearch organizationViewHolder = (OrganizationRowViewHolderForSearch) v.getTag();
            ContactOrgInterface organization = GlobalSearchContentProvider.getContactOrgInterfaceFromCursor(this.mSession, c);
            if (organization != null) {
                organizationViewHolder.fill(organization, this.searchConstraint);
            }
        } else if (v.getTag() instanceof PersonRowViewHolderForSearch) {
            PersonRowViewHolderForSearch personHolderContact = (PersonRowViewHolderForSearch) v.getTag();
            personHolderContact.setOnCallButtonClickListener(this.mOnCallButtonClickListener);
            ContactOrgInterface contact = GlobalSearchContentProvider.getContactOrgInterfaceFromCursor(this.mSession, c);
            if (contact != null) {
                personHolderContact.fill(this.mSession, contact, this.searchConstraint);
            }
        } else if (v.getTag() instanceof DealRowViewHolderForSearch) {
            ((DealRowViewHolderForSearch) v.getTag()).fill(this.mSession, context, GlobalSearchContentProvider.getDeal(this.mSession, c), this.searchConstraint);
        }
    }

    public void setSearchConstraint(@NonNull SearchConstraint searchConstraint) {
        this.searchConstraint = searchConstraint;
    }

    public int getItemViewType(int position) {
        Cursor cursor = getCursor();
        cursor.moveToPosition(position);
        int cursorRowType = GlobalSearchContentProvider.getCursorRowType(cursor);
        if (cursorRowType == 1) {
            return 0;
        }
        if (cursorRowType == 2) {
            return 1;
        }
        if (cursorRowType == 3) {
            return 2;
        }
        return -1;
    }

    public int getViewTypeCount() {
        return 3;
    }

    public Object getItem(int position) {
        getCursor().moveToPosition(position);
        return getCursor();
    }

    public void setOnCallButtonClickListener(OnCallButtonClickListener onCallButtonClickListener) {
        this.mOnCallButtonClickListener = onCallButtonClickListener;
    }
}
