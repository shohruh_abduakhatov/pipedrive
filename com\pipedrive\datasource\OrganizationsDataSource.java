package com.pipedrive.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.newrelic.agent.android.instrumentation.JSONArrayInstrumentation;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Organization;
import com.pipedrive.util.CursorHelper;
import com.pipedrive.util.StringUtils;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;

public class OrganizationsDataSource extends BaseDataSource<Organization> {
    private static final String[] ALL_COLUMNS = new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, "name", "phone", "email", "address", PipeSQLiteHelper.COLUMN_VISIBLE_TO, PipeSQLiteHelper.COLUMN_OWNER_ID, PipeSQLiteHelper.COLUMN_OTHER_FIELDS, PipeSQLiteHelper.COLUMN_ORG_DROP_BOX_ADDRESS, PipeSQLiteHelper.COLUMN_ORG_IS_ACTIVE, PipeSQLiteHelper.COLUMN_ORG_OWNER_NAME, PipeSQLiteHelper.COLUMN_ORG_NAME_SEARCH_FIELD, PipeSQLiteHelper.COLUMN_ORG_ADDRESS_LATITUDE, PipeSQLiteHelper.COLUMN_ORG_ADDRESS_LONGITUDE, PipeSQLiteHelper.COLUMN_ORG_IS_SHADOW, "organizations._id AS org_sql_id"};
    private static final String COLUMN_ORG_SQLID_ALIAS = "org_sql_id";
    @NonNull
    private final SearchHelper searchHelper = new SearchHelper("organizations.org_name_search_field", "organizations.address");

    public static class ForJoin extends OrganizationsDataSource {
        @Nullable
        public /* bridge */ /* synthetic */ BaseDatasourceEntity deflateCursor(@NonNull Cursor cursor) {
            return super.deflateCursor(cursor);
        }

        @NonNull
        protected /* bridge */ /* synthetic */ ContentValues getContentValues(@NonNull BaseDatasourceEntity baseDatasourceEntity) {
            return super.getContentValues((Organization) baseDatasourceEntity);
        }

        public ForJoin(SQLiteDatabase dbconn) {
            super(dbconn);
        }

        @NonNull
        public String[] getAllColumns() {
            return (String[]) Arrays.copyOfRange(OrganizationsDataSource.ALL_COLUMNS, 1, OrganizationsDataSource.ALL_COLUMNS.length);
        }
    }

    public OrganizationsDataSource(SQLiteDatabase dbconn) {
        super(dbconn);
    }

    public void deleteOrganization(int pipedriveId) {
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String str = "organizations";
        String str2 = "id = " + pipedriveId;
        if (transactionalDBConnection instanceof SQLiteDatabase) {
            SQLiteInstrumentation.delete(transactionalDBConnection, str, str2, null);
        } else {
            transactionalDBConnection.delete(str, str2, null);
        }
    }

    @NonNull
    public List<Organization> findOrganizationsByName(@NonNull SearchConstraint searchConstraint) {
        return deflateCursorToList(getSearchCursor(searchConstraint));
    }

    @Deprecated
    public Organization findOrganizationByPipedriveId(int pipedriveId) {
        return (Organization) findByPipedriveId((long) pipedriveId);
    }

    @Nullable
    public Organization findOrganizationByPipedriveId(long pipedriveId) {
        return (Organization) findByPipedriveId(pipedriveId);
    }

    @NonNull
    protected String getColumnNameForPipedriveId() {
        return PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID;
    }

    @NonNull
    protected String getColumnNameForSqlId() {
        return PipeSQLiteHelper.COLUMN_ID;
    }

    @NonNull
    protected String getTableName() {
        return "organizations";
    }

    @NonNull
    protected ContentValues getContentValues(@NonNull Organization organization) {
        ContentValues values = new ContentValues();
        if (organization.getSqlId() != 0) {
            values.put(PipeSQLiteHelper.COLUMN_ID, Long.valueOf(organization.getSqlId()));
        }
        if (organization.isExisting()) {
            values.put(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, Integer.valueOf(organization.getPipedriveId()));
        }
        values.put("name", organization.getName());
        values.put(PipeSQLiteHelper.COLUMN_ORG_NAME_SEARCH_FIELD, StringUtils.getNormalizedOrganizationName(organization));
        values.put("phone", organization.getPhone());
        values.put("email", organization.getEmail());
        values.put("address", organization.getAddress());
        CursorHelper.put(organization.getAddressLatitude(), values, PipeSQLiteHelper.COLUMN_ORG_ADDRESS_LATITUDE);
        CursorHelper.put(organization.getAddressLongitude(), values, PipeSQLiteHelper.COLUMN_ORG_ADDRESS_LONGITUDE);
        values.put(PipeSQLiteHelper.COLUMN_VISIBLE_TO, Integer.valueOf(organization.getVisibleTo()));
        values.put(PipeSQLiteHelper.COLUMN_OWNER_ID, Integer.valueOf(organization.getOwnerPipedriveId()));
        if (organization.getCustomFields() != null) {
            String str = PipeSQLiteHelper.COLUMN_OTHER_FIELDS;
            JSONArray customFields = organization.getCustomFields();
            values.put(str, !(customFields instanceof JSONArray) ? customFields.toString() : JSONArrayInstrumentation.toString(customFields));
        }
        values.put(PipeSQLiteHelper.COLUMN_ORG_DROP_BOX_ADDRESS, organization.getDropBoxAddress());
        values.put(PipeSQLiteHelper.COLUMN_ORG_IS_ACTIVE, Boolean.valueOf(organization.isActive()));
        values.put(PipeSQLiteHelper.COLUMN_ORG_OWNER_NAME, organization.getOwnerName());
        values.put(PipeSQLiteHelper.COLUMN_ORG_IS_SHADOW, Boolean.valueOf(organization.isShadow()));
        return values;
    }

    @NonNull
    public String[] getAllColumns() {
        return ALL_COLUMNS;
    }

    @Nullable
    public Organization deflateCursor(@NonNull Cursor cursor) {
        boolean z = true;
        String name = cursor.getString(cursor.getColumnIndex("name"));
        if (name == null) {
            LogJourno.reportEvent(EVENT.MODEL_ORGANIZATION_WITH_EMPTY_NAME_WAS_STORED_IN_DATABASE);
            return null;
        }
        Organization organization;
        Long pipedriveId = Long.valueOf(cursor.getLong(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID)));
        if (pipedriveId.longValue() > 0) {
            boolean isShadow;
            if (cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_ORG_IS_SHADOW)) == 1) {
                isShadow = true;
            } else {
                isShadow = false;
            }
            organization = isShadow ? Organization.createShadowOrganization(pipedriveId, name) : Organization.createFullOrganization(pipedriveId, name);
        } else {
            organization = Organization.createLocalOrganization(name);
        }
        if (organization == null) {
            return organization;
        }
        organization.setSqlIdOrNull(Long.valueOf(cursor.getLong(cursor.getColumnIndex(COLUMN_ORG_SQLID_ALIAS))));
        organization.setPipedriveId(pipedriveId);
        organization.setPhone(cursor.getString(cursor.getColumnIndex("phone")));
        organization.setEmail(cursor.getString(cursor.getColumnIndex("email")));
        organization.setAddress(cursor.getString(cursor.getColumnIndex("address")));
        organization.setAddressLatitude(CursorHelper.getDouble(cursor, PipeSQLiteHelper.COLUMN_ORG_ADDRESS_LATITUDE));
        organization.setAddressLongitude(CursorHelper.getDouble(cursor, PipeSQLiteHelper.COLUMN_ORG_ADDRESS_LONGITUDE));
        organization.setVisibleTo(cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_VISIBLE_TO)));
        organization.setOwnerPipedriveId(cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_OWNER_ID)));
        organization.setCustomFields(cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_OTHER_FIELDS)));
        organization.setDropBoxAddress(cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_ORG_DROP_BOX_ADDRESS)));
        if (cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_ORG_IS_ACTIVE)) != 1) {
            z = false;
        }
        organization.setIsActive(z);
        organization.setOwnerName(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_ORG_OWNER_NAME));
        return organization;
    }

    @NonNull
    public Cursor getSearchCursor(@NonNull SearchConstraint searchConstraint) {
        SelectionHolder selectionHolder = getSelectionForSearch(searchConstraint);
        String tableName = getTableName();
        String[] allColumns = getAllColumns();
        String selection = selectionHolder.getSelection();
        String[] selectionArgs = selectionHolder.getSelectionArgs();
        String str = PipeSQLiteHelper.COLUMN_ORG_NAME_SEARCH_FIELD;
        return !(this instanceof SQLiteDatabase) ? query(tableName, allColumns, selection, selectionArgs, null, null, str) : SQLiteInstrumentation.query((SQLiteDatabase) this, tableName, allColumns, selection, selectionArgs, null, null, str);
    }

    @NonNull
    public String getAlphabetString(@NonNull SearchConstraint searchConstraint) {
        SelectionHolder selectionHolder = getSelectionForSearch(searchConstraint);
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String tableName = getTableName();
        String[] strArr = new String[]{"SUBSTR(org_name_search_field, 1, 1)"};
        String selection = selectionHolder.getSelection();
        String[] selectionArgs = selectionHolder.getSelectionArgs();
        String str = PipeSQLiteHelper.COLUMN_ORG_NAME_SEARCH_FIELD;
        String emptyString = "";
        return ((String) CursorHelper.observeAllRows(!(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.query(true, tableName, strArr, selection, selectionArgs, null, null, str, null) : SQLiteInstrumentation.query(transactionalDBConnection, true, tableName, strArr, selection, selectionArgs, null, null, str, null)).map(new Func1<Cursor, String>() {
            public String call(Cursor cursor) {
                return cursor.getString(0);
            }
        }).switchIfEmpty(Observable.just("")).distinct().reduce(new Func2<String, String, String>() {
            public String call(String s, String s2) {
                return s.concat(s2);
            }
        }).toBlocking().single()).toUpperCase();
    }

    @NonNull
    public SelectionHolder getSelectionForSearch(@NonNull SearchConstraint searchConstraint) {
        String emptyString = "";
        return new SelectionHolder(getTableName() + '.' + PipeSQLiteHelper.COLUMN_ORG_IS_ACTIVE + " =? AND " + "name" + " !=? AND " + "name" + " IS NOT NULL ", new String[]{String.valueOf(1), ""}).add(this.searchHelper.getSelectionHolderForSearchConstraint(searchConstraint));
    }
}
