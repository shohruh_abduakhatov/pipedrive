package com.pipedrive.application;

import android.util.SparseBooleanArray;
import com.pipedrive.model.ActivityType;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class SessionRuntimeCache {
    public boolean DBTablesRecreated = false;
    public List<ActivityType> allActivityTypes = null;
    public Long callSummaryChangedActivitySqlId = null;
    public boolean isCrashlyticsSetup = false;
    public boolean isNewRelicSetup = false;
    public boolean isTrialExpiredActivityRequested = false;
    public int lastSelectedPipelineFilter = -1;
    public final ConcurrentMap<String, Integer> runningAsyncTaskRegister = new ConcurrentHashMap();
    public volatile SparseBooleanArray shouldInvalidateDeals = new SparseBooleanArray();
    public volatile boolean shouldInvalidateFilters = true;
    public volatile boolean shouldInvalidatePipelines = true;
    public volatile boolean shouldInvalidateStages = true;
    public boolean valid = false;

    protected SessionRuntimeCache() {
    }
}
