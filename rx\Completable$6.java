package rx;

import rx.functions.Func0;
import rx.subscriptions.Subscriptions;

class Completable$6 implements Completable$OnSubscribe {
    final /* synthetic */ Func0 val$errorFunc0;

    Completable$6(Func0 func0) {
        this.val$errorFunc0 = func0;
    }

    public void call(CompletableSubscriber s) {
        Throwable error;
        s.onSubscribe(Subscriptions.unsubscribed());
        try {
            error = (Throwable) this.val$errorFunc0.call();
        } catch (Throwable e) {
            error = e;
        }
        if (error == null) {
            error = new NullPointerException("The error supplied is null");
        }
        s.onError(error);
    }
}
