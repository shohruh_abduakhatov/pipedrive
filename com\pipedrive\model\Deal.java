package com.pipedrive.model;

import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.newrelic.agent.android.instrumentation.JSONArrayInstrumentation;
import com.newrelic.agent.android.instrumentation.JSONObjectInstrumentation;
import com.pipedrive.R;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.datasource.PipelineDataSource;
import com.pipedrive.datetime.DateFormatHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.model.delta.Model;
import com.pipedrive.model.delta.ModelProperty;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.util.CustomFieldsUtil;
import com.pipedrive.util.StringUtils;
import com.pipedrive.util.clone.Cloner;
import com.pipedrive.util.time.TimeManager;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Model(dataSource = DealsDataSource.class)
public class Deal extends BaseCloneableDatasourceEntity<Deal> implements AssociatedDatasourceEntity {
    public static final Creator CREATOR = new Creator() {
        public Deal createFromParcel(Parcel in) {
            return new Deal(in);
        }

        public Deal[] newArray(int size) {
            return new Deal[size];
        }
    };
    public static final String EXPECTED_CLOSE_DATE = "expected_close_date";
    private String addTime;
    @ModelProperty
    private String currencyCode;
    private JSONArray customFields;
    @Nullable
    private List<DealProduct> dealProducts;
    private String dropBoxAddress;
    private String expectedCloseDate;
    private String formattedValue;
    private String lostTime;
    private String nextActivityDateStr;
    private String nextActivityTime;
    @ModelProperty
    private Organization organization;
    private String ownerName;
    @ModelProperty
    private int ownerPipedriveId;
    @ModelProperty
    private Person person;
    @ModelProperty
    private int pipelineId;
    @Nullable
    @ModelProperty
    private Double productCount;
    private String rottenDateTime;
    @ModelProperty
    private int stage;
    private DealStatus status;
    @ModelProperty
    private String title;
    private int undoneActivitiesCount;
    private String updateTime;
    @ModelProperty
    private double value;
    @ModelProperty
    private int visibleTo;
    private String wonTime;

    public Deal() {
        this.status = DealStatus.OPEN;
        this.customFields = new JSONArray();
    }

    public static boolean updateDealWithRequiredDefaultValues(@NonNull Deal inoutDealTemplate, @NonNull Session session) {
        boolean presetIsSuccessful = true;
        inoutDealTemplate.setCurrencyCode(session.getUserSettingsDefaultCurrencyCode());
        inoutDealTemplate.setOwnerPipedriveId((int) session.getAuthenticatedUserID());
        inoutDealTemplate.setOwnerName(session.getUserSettingsName(null));
        int userSettingsDefaultVisibilityForDeal = session.getUserSettingsDefaultVisibilityForDeal(Integer.MIN_VALUE);
        if (userSettingsDefaultVisibilityForDeal == Integer.MIN_VALUE) {
            presetIsSuccessful = false;
        } else {
            inoutDealTemplate.setVisibleTo(userSettingsDefaultVisibilityForDeal);
        }
        inoutDealTemplate.productCount = Double.valueOf(0.0d);
        return presetIsSuccessful;
    }

    public static boolean updateDealWithRequiredDefaultValuesAndCurrentPipelineAndFirstStage(@NonNull Deal inoutDealTemplate, @NonNull Session session) {
        boolean presetIsSuccessful = updateDealWithRequiredDefaultValues(inoutDealTemplate, session);
        long currentlySelectedPipelineId = session.getPipelineSelectedPipelineId(Long.MIN_VALUE);
        if (currentlySelectedPipelineId == Long.MIN_VALUE) {
            presetIsSuccessful = false;
        } else {
            inoutDealTemplate.setPipelineId((int) currentlySelectedPipelineId);
        }
        ArrayList<DealStage> pipelineStages = new PipelineDataSource(session.getDatabase()).findStagesByPipelineId((long) inoutDealTemplate.getPipelineId());
        if (pipelineStages.isEmpty()) {
            return false;
        }
        inoutDealTemplate.setStage(((DealStage) pipelineStages.get(0)).getPipedriveId());
        return presetIsSuccessful;
    }

    private Deal(Parcel in) {
        this.status = DealStatus.OPEN;
        this.customFields = new JSONArray();
        readFromParcel(in);
    }

    public static Deal readDeal(@NonNull JsonReader reader) throws IOException {
        Deal dealToReturn = new Deal();
        updateDeal(reader, dealToReturn);
        return dealToReturn;
    }

    public static void updateDeal(@NonNull JsonReader reader, @NonNull Deal readInto) throws IOException {
        reader.beginObject();
        Person person = new Person();
        Organization org = new Organization();
        while (reader.hasNext()) {
            String field = reader.nextName();
            if (PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID.equals(field)) {
                readInto.setPipedriveId(Long.valueOf(reader.nextLong()));
            } else if ("update_time".equals(field) && reader.peek() != JsonToken.NULL) {
                readInto.setUpdateTime(reader.nextString());
            } else if ("title".equals(field) && reader.peek() != JsonToken.NULL) {
                readInto.setTitle(reader.nextString());
            } else if ("stage_id".equals(field) && reader.peek() != JsonToken.NULL) {
                readInto.setStage(reader.nextInt());
            } else if ("status".equals(field) && reader.peek() != JsonToken.NULL) {
                readInto.setStatus(DealStatus.from(reader.nextString()));
            } else if (Param.CURRENCY.equals(field) && reader.peek() != JsonToken.NULL) {
                readInto.setCurrencyCode(reader.nextString());
            } else if ("formatted_value".equals(field) && reader.peek() != JsonToken.NULL) {
                readInto.setFormattedValue(reader.nextString());
            } else if (Param.VALUE.equals(field)) {
                readInto.setValue(reader.nextDouble());
            } else if ("next_activity_date".equals(field) && reader.peek() != JsonToken.NULL) {
                readInto.setNextActivityDateStr(reader.nextString());
            } else if ("next_activity_time".equals(field) && reader.peek() != JsonToken.NULL) {
                readInto.setNextActivityTime(reader.nextString());
            } else if ("pipeline_id".equals(field) && reader.peek() != JsonToken.NULL) {
                readInto.setPipelineId(reader.nextInt());
            } else if ("undone_activities_count".equals(field) && reader.peek() != JsonToken.NULL) {
                readInto.setUndoneActivitiesCount(reader.nextInt());
            } else if ("rotten_time".equals(field) && reader.peek() != JsonToken.NULL) {
                readInto.setRottenDateTime(reader.nextString());
            } else if ("org_name".equals(field) && reader.peek() != JsonToken.NULL) {
                org.setName(reader.nextString());
            } else if ("person_name".equals(field) && reader.peek() != JsonToken.NULL) {
                person.setName(reader.nextString());
            } else if ("person_id".equals(field)) {
                if (reader.peek() == JsonToken.BEGIN_OBJECT) {
                    reader.beginObject();
                    while (reader.hasNext()) {
                        field = reader.nextName();
                        if (Param.VALUE.equals(field)) {
                            person.setPipedriveId(Long.valueOf(reader.nextLong()));
                        } else if (!"name".equals(field) || reader.peek() == JsonToken.NULL) {
                            reader.skipValue();
                        } else {
                            person.setName(reader.nextString());
                        }
                    }
                    reader.endObject();
                } else if (reader.peek() == JsonToken.NUMBER) {
                    person.setPipedriveId(Long.valueOf(reader.nextLong()));
                } else if (reader.peek() == JsonToken.STRING) {
                    pipedriveIdString = reader.nextString();
                    if (!TextUtils.isEmpty(pipedriveIdString) && TextUtils.isDigitsOnly(pipedriveIdString)) {
                        person.setPipedriveId(Long.valueOf(Long.parseLong(pipedriveIdString)));
                    }
                } else {
                    reader.skipValue();
                }
                if (person.isExisting()) {
                    readInto.setPerson(person);
                }
            } else if (PipeSQLiteHelper.COLUMN_USER_ID.equals(field)) {
                if (reader.peek() == JsonToken.BEGIN_OBJECT) {
                    reader.beginObject();
                    while (reader.hasNext()) {
                        if (PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID.equals(reader.nextName())) {
                            readInto.setOwnerPipedriveId(reader.nextInt());
                        } else {
                            reader.skipValue();
                        }
                    }
                    reader.endObject();
                } else if (reader.peek() == JsonToken.NUMBER) {
                    readInto.setOwnerPipedriveId(reader.nextInt());
                } else {
                    reader.skipValue();
                }
            } else if ("org_id".equals(field)) {
                if (reader.peek() == JsonToken.BEGIN_OBJECT) {
                    reader.beginObject();
                    while (reader.hasNext()) {
                        field = reader.nextName();
                        if (Param.VALUE.equals(field)) {
                            org.setPipedriveId(Long.valueOf(reader.nextLong()));
                        } else if (!"name".equals(field) || reader.peek() == JsonToken.NULL) {
                            reader.skipValue();
                        } else {
                            org.setName(reader.nextString());
                        }
                    }
                    reader.endObject();
                } else if (reader.peek() == JsonToken.NUMBER) {
                    org.setPipedriveId(Long.valueOf(reader.nextLong()));
                } else if (reader.peek() == JsonToken.STRING) {
                    pipedriveIdString = reader.nextString();
                    if (!TextUtils.isEmpty(pipedriveIdString) && TextUtils.isDigitsOnly(pipedriveIdString)) {
                        org.setPipedriveId(Long.valueOf(Long.parseLong(pipedriveIdString)));
                    }
                } else {
                    reader.skipValue();
                }
                if (org.isExisting()) {
                    readInto.setOrganization(org);
                }
            } else if ("cc_email".equals(field) && reader.peek() != JsonToken.NULL) {
                readInto.setDropBoxAddress(reader.nextString());
            } else if (PipeSQLiteHelper.COLUMN_VISIBLE_TO.equals(field) && reader.peek() == JsonToken.STRING) {
                readInto.setVisibleTo(Integer.valueOf(reader.nextString()).intValue());
            } else if ("owner_name".equals(field) && reader.peek() == JsonToken.STRING) {
                readInto.setOwnerName(reader.nextString());
            } else if (field.length() >= 40) {
                JSONObject customFieldObj = null;
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                } else {
                    try {
                        customFieldObj = CustomField.readCustomValuesJsonObject(reader, field);
                    } catch (Throwable e) {
                        LogJourno.reportEvent(EVENT.JsonParser_dealCustomFieldsNotParsedFromJsonReader, e);
                        Log.e(new Throwable("Error parsing custom field", e));
                    }
                }
                if (customFieldObj != null) {
                    readInto.addToCustomFieldArray(customFieldObj);
                }
            } else if (EXPECTED_CLOSE_DATE.equals(field) && reader.peek() != JsonToken.NULL) {
                readInto.setExpectedCloseDate(reader.nextString());
            } else if ("won_time".equals(field) && reader.peek() != JsonToken.NULL) {
                readInto.setWonTime(reader.nextString());
            } else if ("lost_time".equals(field) && reader.peek() != JsonToken.NULL) {
                readInto.setLostTime(reader.nextString());
            } else if ("add_time".equals(field) && reader.peek() != JsonToken.NULL) {
                readInto.setAddTime(reader.nextString());
            } else if (!"products_count".equals(field) || reader.peek() == JsonToken.NULL) {
                reader.skipValue();
            } else {
                readInto.setProductCount(Double.valueOf(reader.nextDouble()));
            }
        }
        reader.endObject();
    }

    public int getVisibleTo() {
        return this.visibleTo;
    }

    public void setVisibleTo(int visibleTo) {
        this.visibleTo = visibleTo;
    }

    public String getTitle() {
        return this.title;
    }

    @Nullable
    public String getTitleBasedOnAssociations(@NonNull Resources resources) {
        boolean dealHasNamedOrganizationAssociation;
        if (getOrganization() == null || StringUtils.isTrimmedAndEmpty(getOrganization().getName())) {
            dealHasNamedOrganizationAssociation = false;
        } else {
            dealHasNamedOrganizationAssociation = true;
        }
        if (dealHasNamedOrganizationAssociation) {
            return resources.getString(R.string.hint_default_deal_subject, new Object[]{getOrganization().getName()});
        }
        boolean dealHasNamedPersonAssociation;
        if (getPerson() == null || StringUtils.isTrimmedAndEmpty(getPerson().getName())) {
            dealHasNamedPersonAssociation = false;
        } else {
            dealHasNamedPersonAssociation = true;
        }
        if (!dealHasNamedPersonAssociation) {
            return null;
        }
        return resources.getString(R.string.hint_default_deal_subject, new Object[]{getPerson().getName()});
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getValue() {
        return this.value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getCurrencyCode() {
        return this.currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public int getStage() {
        return this.stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    public DealStatus getStatus() {
        return this.status;
    }

    public void setStatus(DealStatus status) {
        this.status = status;
    }

    public String getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getAddTime() {
        return this.addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public int getPipelineId() {
        return this.pipelineId;
    }

    public void setPipelineId(int pipelineId) {
        this.pipelineId = pipelineId;
    }

    public Person getPerson() {
        return this.person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Organization getOrganization() {
        return this.organization;
    }

    public void setOrganization(Organization org) {
        this.organization = org;
    }

    public String getNextActivityDateStr() {
        return this.nextActivityDateStr;
    }

    public void setNextActivityDateStr(String nextActivityDate) {
        this.nextActivityDateStr = nextActivityDate;
    }

    public void setNextActivityDate(Date dueDate, boolean isAllDay) {
        String str = null;
        setNextActivityDateStr(dueDate == null ? null : DateFormatHelper.dateFormat2UTC().format(dueDate));
        if (isAllDay) {
            if (dueDate != null) {
                str = DateFormatHelper.timeFormat2UTC().format(dueDate);
            }
            setNextActivityTime(str);
            return;
        }
        setNextActivityTime(null);
    }

    public String getNextActivityTime() {
        return this.nextActivityTime;
    }

    public void setNextActivityTime(String nextActivityTime) {
        this.nextActivityTime = nextActivityTime;
    }

    public int getUndoneActivitiesCount() {
        return this.undoneActivitiesCount;
    }

    public void setUndoneActivitiesCount(int undoneActivitiesCount) {
        this.undoneActivitiesCount = undoneActivitiesCount;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        String str;
        super.writeToParcel(dest, flags);
        dest.writeInt(this.pipelineId);
        dest.writeString(this.title);
        dest.writeDouble(this.value);
        dest.writeString(this.currencyCode);
        dest.writeInt(this.stage);
        if (this.status == null) {
            str = null;
        } else {
            str = this.status.toString();
        }
        dest.writeString(str);
        dest.writeString(this.updateTime);
        dest.writeString(this.addTime);
        dest.writeParcelable(getPerson(), flags);
        dest.writeParcelable(getOrganization(), flags);
        dest.writeString(this.nextActivityDateStr);
        dest.writeString(this.nextActivityTime);
        dest.writeInt(this.undoneActivitiesCount);
        dest.writeString(this.formattedValue);
        dest.writeInt(this.ownerPipedriveId);
        dest.writeString(this.rottenDateTime);
        JSONArray jSONArray = this.customFields;
        dest.writeString(!(jSONArray instanceof JSONArray) ? jSONArray.toString() : JSONArrayInstrumentation.toString(jSONArray));
        dest.writeString(this.dropBoxAddress);
        dest.writeInt(this.visibleTo);
        dest.writeString(this.ownerName);
        dest.writeString(this.expectedCloseDate);
        dest.writeString(this.wonTime);
        dest.writeString(this.lostTime);
    }

    protected void readFromParcel(Parcel in) {
        super.readFromParcel(in);
        this.pipelineId = in.readInt();
        this.title = in.readString();
        this.value = in.readDouble();
        this.currencyCode = in.readString();
        this.stage = in.readInt();
        this.status = DealStatus.from(in.readString());
        this.updateTime = in.readString();
        this.addTime = in.readString();
        this.person = (Person) in.readParcelable(Person.class.getClassLoader());
        this.organization = (Organization) in.readParcelable(Organization.class.getClassLoader());
        this.nextActivityDateStr = in.readString();
        this.nextActivityTime = in.readString();
        this.undoneActivitiesCount = in.readInt();
        setFormattedValue(in.readString());
        this.ownerPipedriveId = in.readInt();
        this.rottenDateTime = in.readString();
        try {
            this.customFields = JSONArrayInstrumentation.init(in.readString());
        } catch (JSONException e) {
            Log.e(new Throwable("Error parsing custom fields array", e));
        }
        this.dropBoxAddress = in.readString();
        this.visibleTo = in.readInt();
        this.ownerName = in.readString();
        this.expectedCloseDate = in.readString();
        this.wonTime = in.readString();
        this.lostTime = in.readString();
    }

    public String getFormattedValue() {
        return this.formattedValue;
    }

    public void setFormattedValue(String formattedValue) {
        this.formattedValue = formattedValue;
    }

    public final JSONObject getJSON() throws JSONException {
        JSONObject dealJson = new JSONObject();
        if (getPipedriveIdOrNull() != null) {
            dealJson.put(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, getPipedriveIdOrNull());
        }
        dealJson.put("title", getTitle());
        dealJson.put(Param.VALUE, getValue());
        dealJson.put(Param.CURRENCY, getCurrencyCode());
        String str = "person_id";
        Object pipedriveIdOrNull = (getPerson() == null || getPerson().getPipedriveIdOrNull() == null) ? JSONObject.NULL : getPerson().getPipedriveIdOrNull();
        dealJson.put(str, pipedriveIdOrNull);
        str = "org_id";
        pipedriveIdOrNull = (getOrganization() == null || getOrganization().getPipedriveIdOrNull() == null) ? JSONObject.NULL : getOrganization().getPipedriveIdOrNull();
        dealJson.put(str, pipedriveIdOrNull);
        dealJson.put("stage_id", getStage());
        dealJson.put("status", getStatus());
        dealJson.put(PipeSQLiteHelper.COLUMN_USER_ID, getOwnerPipedriveId());
        dealJson.put(PipeSQLiteHelper.COLUMN_VISIBLE_TO, String.valueOf(getVisibleTo()));
        dealJson.put(EXPECTED_CLOSE_DATE, getExpectedCloseDate());
        setCustomFields(CustomFieldsUtil.updateOrgContactReferencesInCustomFields(getCustomFields(), PipedriveApp.getActiveSession(), "deal"));
        CustomFieldsUtil.addCustomFieldsToObject(getCustomFields(), dealJson, "deal");
        return dealJson;
    }

    public boolean isAllAssociationsExisting() {
        if (getOrganization() != null && !getOrganization().isExisting()) {
            return false;
        }
        if (getPerson() == null || getPerson().isExisting()) {
            return true;
        }
        return false;
    }

    private boolean hasEqualPerson(Session session, Person person) {
        if (getPerson() == person) {
            return true;
        }
        try {
            return getPerson().isEqualToPerson(session, person);
        } catch (Exception e) {
            return false;
        }
    }

    private boolean hasEqualOrganization(Session session, Organization organization) {
        if (getOrganization() == organization) {
            return true;
        }
        try {
            CharSequence jSONObject;
            JSONObject json = getOrganization().getJSON(session);
            if (json instanceof JSONObject) {
                Object jSONObjectInstrumentation = JSONObjectInstrumentation.toString(json);
            } else {
                jSONObject = json.toString();
            }
            json = organization.getJSON(session);
            return TextUtils.equals(jSONObject, !(json instanceof JSONObject) ? json.toString() : JSONObjectInstrumentation.toString(json));
        } catch (Exception e) {
            return false;
        }
    }

    public final boolean isEqual(Session session, Deal deal) {
        if (deal == null) {
            return false;
        }
        try {
            CharSequence jSONObject;
            JSONObject json = getJSON();
            if (json instanceof JSONObject) {
                Object jSONObjectInstrumentation = JSONObjectInstrumentation.toString(json);
            } else {
                jSONObject = json.toString();
            }
            json = deal.getJSON();
            boolean isEqual = TextUtils.equals(jSONObject, !(json instanceof JSONObject) ? json.toString() : JSONObjectInstrumentation.toString(json));
            if (isEqual) {
                isEqual = hasEqualPerson(session, deal.getPerson()) && hasEqualOrganization(session, deal.getOrganization());
            }
            return isEqual;
        } catch (JSONException e) {
            return false;
        }
    }

    public final Deal getDeepCopy() {
        Deal thisCloned = (Deal) Cloner.cloneParcelable(this);
        if (this.organization != null) {
            thisCloned.organization = this.organization.getDeepCopy();
        }
        if (this.person != null) {
            thisCloned.person = this.person.getDeepCopy();
        }
        return thisCloned;
    }

    public String toString() {
        try {
            String str = "Deal (%s) SQL id=%s; [getNextActivityDateStr=%s; getNextActivityTime=%s; getStatus=%s; getPipedriveId=%s; getPipelineId=%s; getUpdateTime=%s; getAddTime=%s; getUndoneActivitiesCount=%s; getFormattedValue=%s[json: %s][getOrganization(): %s] [getPerson(): %s]]";
            Object[] objArr = new Object[14];
            objArr[0] = getTitle();
            objArr[1] = getSqlIdOrNull();
            objArr[2] = getNextActivityDateStr();
            objArr[3] = getNextActivityTime();
            objArr[4] = getStatus();
            objArr[5] = getPipedriveIdOrNull();
            objArr[6] = Integer.valueOf(getPipelineId());
            objArr[7] = getUpdateTime();
            objArr[8] = getAddTime();
            objArr[9] = Integer.valueOf(getUndoneActivitiesCount());
            objArr[10] = getFormattedValue();
            JSONObject json = getJSON();
            objArr[11] = !(json instanceof JSONObject) ? json.toString() : JSONObjectInstrumentation.toString(json);
            objArr[12] = getOrganization();
            objArr[13] = getPerson();
            return String.format(str, objArr);
        } catch (JSONException e) {
            return super.toString();
        }
    }

    public int getOwnerPipedriveId() {
        return this.ownerPipedriveId;
    }

    public void setOwnerPipedriveId(int ownerPdId) {
        this.ownerPipedriveId = ownerPdId;
    }

    public String getRottenDateTime() {
        return this.rottenDateTime;
    }

    public void setRottenDateTime(String rottenDateTime) {
        this.rottenDateTime = rottenDateTime;
    }

    public boolean isDealRotten() {
        try {
            return !TextUtils.isEmpty(this.rottenDateTime) && DateFormatHelper.fullDateFormat2UTC().parse(this.rottenDateTime).before(new Date(TimeManager.getInstance().currentTimeMillis().longValue()));
        } catch (ParseException e) {
            Log.e(new Throwable("Error parsing rotten time", e));
            return false;
        }
    }

    public JSONArray getCustomFields() {
        return this.customFields;
    }

    public void setCustomFields(JSONArray customFields) {
        this.customFields = customFields;
    }

    private void addToCustomFieldArray(JSONObject obj) {
        if (this.customFields == null) {
            this.customFields = new JSONArray();
        }
        this.customFields.put(obj);
    }

    public String getDropBoxAddress() {
        return this.dropBoxAddress;
    }

    public void setDropBoxAddress(String dropBoxAddress) {
        this.dropBoxAddress = dropBoxAddress;
    }

    public String getOwnerName() {
        return this.ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getExpectedCloseDate() {
        return this.expectedCloseDate;
    }

    public void setExpectedCloseDate(String expectedCloseDate) {
        this.expectedCloseDate = expectedCloseDate;
    }

    public String getWonTime() {
        return this.wonTime;
    }

    public void setWonTime(String wonTime) {
        this.wonTime = wonTime;
    }

    public String getLostTime() {
        return this.lostTime;
    }

    public void setLostTime(String lostTime) {
        this.lostTime = lostTime;
    }

    @Nullable
    public List<DealProduct> getDealProducts() {
        return this.dealProducts;
    }

    public void setDealProducts(@Nullable List<DealProduct> dealProducts) {
        this.dealProducts = dealProducts;
    }

    @Nullable
    public Double getProductCount() {
        return this.productCount;
    }

    public void setProductCount(@Nullable Double productCount) {
        this.productCount = Double.valueOf(productCount == null ? 0.0d : productCount.doubleValue());
    }
}
