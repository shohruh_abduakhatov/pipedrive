package com.pipedrive;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.PopupWindow.OnDismissListener;
import com.pipedrive.inlineintro.InlineIntroDisplayManager;
import com.pipedrive.inlineintro.InlineIntroPopup;
import com.pipedrive.more.MoreActivity;
import com.pipedrive.pipeline.PipelineActivity;
import com.pipedrive.views.navigation.NavigationToolbar;
import com.pipedrive.views.navigation.NavigationToolbarButton;
import com.pipedrive.views.navigation.OnNavigationButtonClickListener;

public class NavigationActivity extends BaseActivity implements OnNavigationButtonClickListener, InlineIntroDisplayManager {
    private InlineIntroPopup inlineIntroNearby;
    private NavigationToolbar mNavigationToolbar;

    static /* synthetic */ class AnonymousClass4 {
        static final /* synthetic */ int[] $SwitchMap$com$pipedrive$views$navigation$NavigationToolbarButton = new int[NavigationToolbarButton.values().length];

        static {
            try {
                $SwitchMap$com$pipedrive$views$navigation$NavigationToolbarButton[NavigationToolbarButton.PIPELINE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$pipedrive$views$navigation$NavigationToolbarButton[NavigationToolbarButton.ACTIVITIES.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$pipedrive$views$navigation$NavigationToolbarButton[NavigationToolbarButton.CONTACTS.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$pipedrive$views$navigation$NavigationToolbarButton[NavigationToolbarButton.SEARCH.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$pipedrive$views$navigation$NavigationToolbarButton[NavigationToolbarButton.SETTINGS.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
        }
    }

    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        this.mNavigationToolbar = (NavigationToolbar) findViewById(R.id.navigation_toolbar);
        if (this.mNavigationToolbar != null) {
            NavigationToolbarButton selectedButton;
            this.mNavigationToolbar.setDateTextView(getSession());
            this.mNavigationToolbar.setOnNavigationButtonClickListener(this);
            if (this instanceof PipelineActivity) {
                selectedButton = NavigationToolbarButton.PIPELINE;
            } else if ((this instanceof ActivityListActivity) || (this instanceof CalendarActivity)) {
                selectedButton = NavigationToolbarButton.ACTIVITIES;
            } else if (this instanceof ContactsActivity) {
                selectedButton = NavigationToolbarButton.CONTACTS;
            } else if (this instanceof GlobalSearchActivity) {
                selectedButton = NavigationToolbarButton.SEARCH;
            } else if (this instanceof MoreActivity) {
                selectedButton = NavigationToolbarButton.SETTINGS;
            } else {
                selectedButton = null;
            }
            if (selectedButton != null) {
                this.mNavigationToolbar.setSelectedButton(selectedButton);
            }
        }
        final View view = findViewById(16908290);
        if (view != null) {
            view.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    if (((double) view.getHeight()) < ((double) NavigationActivity.this.getResources().getDisplayMetrics().heightPixels) * 0.75d) {
                        if (NavigationActivity.this.mNavigationToolbar.getVisibility() != 8) {
                            NavigationActivity.this.mNavigationToolbar.setVisibility(8);
                        }
                    } else if (NavigationActivity.this.mNavigationToolbar.getVisibility() != 0) {
                        NavigationActivity.this.mNavigationToolbar.setVisibility(0);
                    }
                }
            });
            setupNearbyInlineIntro(view);
        }
    }

    private void setupNearbyInlineIntro(@NonNull View view) {
        this.inlineIntroNearby = new InlineIntroPopup(this, view, 4, getSession(), 0);
        this.inlineIntroNearby.setOnDismissListener(new OnDismissListener() {
            public void onDismiss() {
                if (NavigationActivity.this.nearbyItemIsDismissedForGood()) {
                    NavigationActivity.this.onNearbyIntroDismissed();
                }
            }
        });
    }

    public void onNavigationButtonClicked(NavigationToolbarButton button) {
        switch (AnonymousClass4.$SwitchMap$com$pipedrive$views$navigation$NavigationToolbarButton[button.ordinal()]) {
            case 1:
                PipelineActivity.startActivity(this);
                overrideTransitions(true);
                return;
            case 2:
                ActivityListActivity.startActivity(this, getSession());
                overrideTransitions(true);
                return;
            case 3:
                ContactsActivity.startActivity(this);
                overrideTransitions(true);
                return;
            case 4:
                GlobalSearchActivity.startActivity(this);
                overrideTransitions(true);
                return;
            case 5:
                MoreActivity.startActivity(this);
                overrideTransitions(true);
                dismissNearbyActivityForever();
                return;
            default:
                return;
        }
    }

    private void dismissNearbyActivityForever() {
        this.inlineIntroNearby.setOnDismissListener(null);
        this.inlineIntroNearby.dismissAndDoNotShowAgain();
    }

    public boolean nearbyItemIsDismissedForGood() {
        return !this.inlineIntroNearby.isNotDismissedForGood();
    }

    public void onBackPressed() {
        super.onBackPressed();
        overrideTransitions(false);
    }

    protected void showNearbyIntroPopup() {
        final View anchor = findViewById(R.id.btn_settings);
        anchor.post(new Runnable() {
            public void run() {
                NavigationActivity.this.inlineIntroNearby.showWithTitleOnly(anchor, NavigationActivity.this.getResources().getString(R.string.new_feature_nearby));
            }
        });
    }

    protected void onNearbyIntroDismissed() {
    }

    private void dismissNearbyIntroOnce() {
        this.inlineIntroNearby.dismissOnce();
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            displayPopupIntros();
        } else {
            hidePopupIntrosOnce();
        }
    }

    @CallSuper
    public void displayPopupIntros() {
        showNearbyIntroPopup();
    }

    @CallSuper
    public void hidePopupIntrosOnce() {
        dismissNearbyIntroOnce();
    }
}
