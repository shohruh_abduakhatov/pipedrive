package com.pipedrive.util.networking.entities.self;

import android.support.annotation.Nullable;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.pipedrive.gson.GsonHelper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

class CompaniesTypeAdapter extends TypeAdapter<List<CompanyEntity>> {
    CompaniesTypeAdapter() {
    }

    public void write(JsonWriter out, List<CompanyEntity> list) throws IOException {
        throw new RuntimeException("Not implemented!");
    }

    @Nullable
    public List<CompanyEntity> read(JsonReader in) throws IOException {
        JsonElement root = new JsonParser().parse(in);
        if (!root.isJsonObject()) {
            return null;
        }
        List<CompanyEntity> result = new ArrayList();
        for (Entry<String, JsonElement> entry : root.getAsJsonObject().entrySet()) {
            if (((JsonElement) entry.getValue()).isJsonObject()) {
                result.add((CompanyEntity) GsonHelper.fromJSON((JsonElement) entry.getValue(), CompanyEntity.class));
            }
        }
        return result;
    }
}
