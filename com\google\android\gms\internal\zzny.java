package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.text.TextUtils;
import com.google.android.gms.auth.api.zza;
import com.google.android.gms.auth.api.zzb;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;

public final class zzny extends zzj<zzoa> {
    private final Bundle ik;

    public zzny(Context context, Looper looper, zzf com_google_android_gms_common_internal_zzf, zzb com_google_android_gms_auth_api_zzb, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 16, com_google_android_gms_common_internal_zzf, connectionCallbacks, onConnectionFailedListener);
        this.ik = com_google_android_gms_auth_api_zzb == null ? new Bundle() : com_google_android_gms_auth_api_zzb.zzaie();
    }

    protected Bundle zzahv() {
        return this.ik;
    }

    public boolean zzain() {
        zzf zzawb = zzawb();
        return (TextUtils.isEmpty(zzawb.getAccountName()) || zzawb.zzc(zza.API).isEmpty()) ? false : true;
    }

    protected zzoa zzch(IBinder iBinder) {
        return zzoa.zza.zzcj(iBinder);
    }

    protected /* synthetic */ IInterface zzh(IBinder iBinder) {
        return zzch(iBinder);
    }

    protected String zzjx() {
        return "com.google.android.gms.auth.service.START";
    }

    protected String zzjy() {
        return "com.google.android.gms.auth.api.internal.IAuthService";
    }
}
