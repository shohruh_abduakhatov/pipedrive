package com.google.android.gms.identity.intents;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.identity.intents.model.CountrySpecification;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class UserAddressRequest extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Creator<UserAddressRequest> CREATOR = new zza();
    List<CountrySpecification> ahN;
    private final int mVersionCode;

    public final class Builder {
        final /* synthetic */ UserAddressRequest ahO;

        private Builder(UserAddressRequest userAddressRequest) {
            this.ahO = userAddressRequest;
        }

        public Builder addAllowedCountrySpecification(CountrySpecification countrySpecification) {
            if (this.ahO.ahN == null) {
                this.ahO.ahN = new ArrayList();
            }
            this.ahO.ahN.add(countrySpecification);
            return this;
        }

        public Builder addAllowedCountrySpecifications(Collection<CountrySpecification> collection) {
            if (this.ahO.ahN == null) {
                this.ahO.ahN = new ArrayList();
            }
            this.ahO.ahN.addAll(collection);
            return this;
        }

        public UserAddressRequest build() {
            if (this.ahO.ahN != null) {
                this.ahO.ahN = Collections.unmodifiableList(this.ahO.ahN);
            }
            return this.ahO;
        }
    }

    UserAddressRequest() {
        this.mVersionCode = 1;
    }

    UserAddressRequest(int i, List<CountrySpecification> list) {
        this.mVersionCode = i;
        this.ahN = list;
    }

    public static Builder newBuilder() {
        UserAddressRequest userAddressRequest = new UserAddressRequest();
        userAddressRequest.getClass();
        return new Builder();
    }

    public int getVersionCode() {
        return this.mVersionCode;
    }

    public void writeToParcel(Parcel parcel, int i) {
        zza.zza(this, parcel, i);
    }
}
