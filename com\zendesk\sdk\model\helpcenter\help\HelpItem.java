package com.zendesk.sdk.model.helpcenter.help;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.List;

public interface HelpItem {
    public static final int TYPE_ARTICLE = 3;
    public static final int TYPE_CATEGORY = 1;
    public static final int TYPE_LOADING = 5;
    public static final int TYPE_NO_RESULTS = 7;
    public static final int TYPE_PADDING = 8;
    public static final int TYPE_SECTION = 2;
    public static final int TYPE_SEE_ALL = 4;

    @NonNull
    List<? extends HelpItem> getChildren();

    @Nullable
    Long getId();

    @NonNull
    String getName();

    @Nullable
    Long getParentId();

    int getViewType();
}
