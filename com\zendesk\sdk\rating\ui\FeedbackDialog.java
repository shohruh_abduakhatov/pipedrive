package com.zendesk.sdk.rating.ui;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.sdk.feedback.FeedbackConnector;
import com.zendesk.sdk.model.request.CreateRequest;
import com.zendesk.sdk.network.SubmissionListener;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.storage.RateMyAppStorage;
import com.zendesk.sdk.ui.TextWatcherAdapter;
import com.zendesk.sdk.ui.ZendeskDialog;
import com.zendesk.service.ErrorResponse;
import com.zendesk.service.ErrorResponseAdapter;
import com.zendesk.service.ZendeskCallback;
import com.zendesk.util.StringUtils;
import java.util.ArrayList;
import java.util.List;

public class FeedbackDialog extends ZendeskDialog {
    private static final String LOG_TAG = FeedbackDialog.class.getSimpleName();
    private FeedbackConnector mFeedBackConnector;
    private SubmissionListener mFeedbackListener;

    static class NullSafeFeedbackConnector implements FeedbackConnector {
        NullSafeFeedbackConnector() {
        }

        public void sendFeedback(String feedback, List<String> list, ZendeskCallback<CreateRequest> callback) {
            String error = "Connector is null, cannot send feedback!";
            Logger.e(FeedbackDialog.LOG_TAG, "Connector is null, cannot send feedback!", new Object[0]);
            if (callback != null) {
                callback.onError(new ErrorResponseAdapter("Connector is null, cannot send feedback!"));
            }
        }

        public boolean isValid() {
            return false;
        }
    }

    public void setFeedbackListener(SubmissionListener feedbackListener) {
        this.mFeedbackListener = feedbackListener;
    }

    public static FeedbackDialog newInstance(FeedbackConnector connector) {
        FeedbackDialog feedbackDialog = new FeedbackDialog();
        if (connector == null) {
            Logger.e(LOG_TAG, "Supplied connector was null, falling back to a null safe connector but no feedback will be sent!", new Object[0]);
            connector = new NullSafeFeedbackConnector();
        }
        feedbackDialog.setFeedBackConnector(connector);
        return feedbackDialog;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View dialog = inflater.inflate(R.layout.fragment_send_feedback, container, false);
        final View cancelButton = dialog.findViewById(R.id.rma_feedback_issue_cancel_button);
        final View sendButton = dialog.findViewById(R.id.rma_feedback_issue_send_button);
        final EditText editText = (EditText) dialog.findViewById(R.id.rma_feedback_issue_edittext);
        final ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.rma_feedback_issue_progress);
        final Context context = inflater.getContext();
        final RateMyAppStorage rateMyAppStorage = new RateMyAppStorage(context);
        if (cancelButton != null) {
            Logger.d(LOG_TAG, "Inflating cancel button...", new Object[0]);
            cancelButton.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    Logger.d(FeedbackDialog.LOG_TAG, "Cancel button was clicked...", new Object[0]);
                    FeedbackDialog.this.dismiss();
                    if (FeedbackDialog.this.mFeedbackListener != null) {
                        Logger.d(FeedbackDialog.LOG_TAG, "Notifying feedback listener of submission cancellation", new Object[0]);
                        FeedbackDialog.this.mFeedbackListener.onSubmissionCancel();
                    }
                }
            });
        }
        final FeedbackConnector connector = this.mFeedBackConnector;
        String savedFeedback = rateMyAppStorage.getSavedFeedback();
        if (StringUtils.hasLength(savedFeedback)) {
            Logger.d(LOG_TAG, "Saved feedback was found.", new Object[0]);
            if (editText != null) {
                editText.setText(savedFeedback);
            }
            if (sendButton != null) {
                sendButton.setEnabled(true);
            }
        }
        if (sendButton == null || editText == null) {
            Logger.w(LOG_TAG, "Could not setOnClickListener because views were null", new Object[0]);
        } else {
            sendButton.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    if (FeedbackDialog.this.mFeedbackListener != null) {
                        FeedbackDialog.this.mFeedbackListener.onSubmissionStarted();
                    }
                    if (cancelButton != null) {
                        cancelButton.setEnabled(false);
                    }
                    sendButton.setEnabled(false);
                    editText.setEnabled(false);
                    progressBar.setVisibility(0);
                    final String feedback = editText.getText().toString();
                    connector.sendFeedback(feedback, new ArrayList(), new ZendeskCallback<CreateRequest>() {
                        public void onSuccess(CreateRequest result) {
                            Logger.d(FeedbackDialog.LOG_TAG, "Feedback was submitted successfully.", new Object[0]);
                            ZendeskConfig.INSTANCE.getTracker().rateMyAppFeedbackSent();
                            if (FeedbackDialog.this.mFeedbackListener != null) {
                                Logger.d(FeedbackDialog.LOG_TAG, "Notifying feedback listener of success", new Object[0]);
                                rateMyAppStorage.clearUserData();
                                FeedbackDialog.this.mFeedbackListener.onSubmissionCompleted();
                            }
                            rateMyAppStorage.setDontShowAgain();
                            if (FeedbackDialog.this.isVisible()) {
                                Toast.makeText(context, FeedbackDialog.this.getString(R.string.rate_my_app_dialog_feedback_send_success_toast), 0).show();
                                if (FeedbackDialog.this.isResumed()) {
                                    FeedbackDialog.this.dismiss();
                                }
                            }
                        }

                        public void onError(ErrorResponse error) {
                            Logger.e(FeedbackDialog.LOG_TAG, error);
                            rateMyAppStorage.setSavedFeedback(feedback);
                            if (cancelButton != null) {
                                cancelButton.setEnabled(true);
                            }
                            sendButton.setEnabled(true);
                            editText.setEnabled(true);
                            progressBar.setVisibility(8);
                            if (!FeedbackDialog.this.isVisible()) {
                                return;
                            }
                            if (error.isNetworkError()) {
                                Toast.makeText(context, FeedbackDialog.this.getString(R.string.rate_my_app_dialog_feedback_send_error_no_connectivity_toast), 0).show();
                            } else {
                                Toast.makeText(context, FeedbackDialog.this.getString(R.string.rate_my_app_dialog_feedback_send_error_toast), 0).show();
                            }
                        }
                    });
                }
            });
        }
        if (editText != null) {
            editText.addTextChangedListener(new TextWatcherAdapter() {
                public void afterTextChanged(Editable editable) {
                    if (sendButton == null) {
                        Logger.e(FeedbackDialog.LOG_TAG, "ignoring afterTextChanged() because sendButton is null", new Object[0]);
                    } else if (editable == null || editable.length() == 0) {
                        sendButton.setEnabled(false);
                    } else {
                        sendButton.setEnabled(true);
                    }
                }
            });
        }
        return dialog;
    }

    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }

    void setFeedBackConnector(FeedbackConnector feedBackConnector) {
        this.mFeedBackConnector = feedBackConnector;
    }
}
