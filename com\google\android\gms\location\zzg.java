package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.List;

public class zzg implements Creator<GestureRequest> {
    static void zza(GestureRequest gestureRequest, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zza(parcel, 1, gestureRequest.zzbqb(), false);
        zzb.zzc(parcel, 1000, gestureRequest.getVersionCode());
        zzb.zzaj(parcel, zzcs);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zznu(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzuo(i);
    }

    public GestureRequest zznu(Parcel parcel) {
        int zzcr = zza.zzcr(parcel);
        int i = 0;
        List list = null;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    list = zza.zzad(parcel, zzcq);
                    break;
                case 1000:
                    i = zza.zzg(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new GestureRequest(i, list);
        }
        throw new zza.zza("Overread allowed size end=" + zzcr, parcel);
    }

    public GestureRequest[] zzuo(int i) {
        return new GestureRequest[i];
    }
}
