package com.pipedrive.views.email.detail;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.widget.ScrollView;
import com.pipedrive.R;
import com.pipedrive.util.ViewUtil;

public class MainScrollView extends ScrollView {
    private static final boolean isScaleDoesNotAffectViewTranslate = (VERSION.SDK_INT >= 21);
    private final Context mContext;
    private GestureDetector mGestureDetector;
    private ScaleGestureDetector mScaleGestureDetector;
    private View mViewToScaleContainer;
    private WebView mWebView;

    private class ViewSimpleOnGestureListener extends SimpleOnGestureListener {
        boolean ignoreScrollGesture;
        private MotionEvent registeredOnScrollFirstDown;
        private MotionEvent registeredOnSingleTapUp;

        private ViewSimpleOnGestureListener() {
            this.registeredOnSingleTapUp = null;
            this.registeredOnScrollFirstDown = null;
            this.ignoreScrollGesture = true;
        }

        public boolean onDown(MotionEvent e) {
            this.ignoreScrollGesture = true;
            return super.onDown(e);
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            boolean onScrollHandled = false;
            if (e1 == null || e2 == null) {
                return 0;
            }
            if (this.registeredOnScrollFirstDown == null || this.registeredOnScrollFirstDown.getEventTime() != e1.getEventTime()) {
                this.registeredOnScrollFirstDown = MotionEvent.obtain(e1);
                onScrollHandled = false | MainScrollView.this.mWebView.onTouchEvent(this.registeredOnScrollFirstDown);
            }
            onScrollHandled |= MainScrollView.this.mWebView.onTouchEvent(e2);
            if (this.ignoreScrollGesture) {
                this.ignoreScrollGesture = false;
            } else {
                MainScrollView.this.keepScaledViewInHorizontalBounds(MainScrollView.this.mWebView, distanceX);
            }
            return onScrollHandled;
        }

        public boolean onSingleTapConfirmed(MotionEvent e) {
            for (int i = 0; i < MainScrollView.this.getChildCount(); i++) {
                View childView = MainScrollView.this.getChildAt(i);
                if (childView != null) {
                    int scrollX = MainScrollView.this.getScrollX();
                    int scrollY = MainScrollView.this.getScrollY();
                    e.offsetLocation((float) scrollX, (float) scrollY);
                    if (childView.dispatchTouchEvent(e)) {
                        this.registeredOnSingleTapUp.offsetLocation((float) scrollX, (float) scrollY);
                        childView.dispatchTouchEvent(this.registeredOnSingleTapUp);
                    }
                }
            }
            this.registeredOnSingleTapUp = null;
            return super.onSingleTapConfirmed(e);
        }

        public boolean onSingleTapUp(MotionEvent e) {
            this.registeredOnSingleTapUp = MotionEvent.obtain(e);
            return super.onSingleTapUp(e);
        }

        public void onLongPress(MotionEvent e) {
            if (MainScrollView.this.mWebView.getScaleX() > 1.0f) {
                ViewUtil.showErrorToast(MainScrollView.this.getContext(), (int) R.string.message_email_copy_not_enabled_when_zoomed);
                return;
            }
            MotionEvent longPressEventCopy = MotionEvent.obtain(e);
            for (int i = 0; i < MainScrollView.this.getChildCount(); i++) {
                View childView = MainScrollView.this.getChildAt(i);
                if (childView != null) {
                    longPressEventCopy.offsetLocation((float) MainScrollView.this.getScrollX(), (float) MainScrollView.this.getScrollY());
                    childView.dispatchTouchEvent(longPressEventCopy);
                }
            }
            super.onLongPress(e);
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return super.onFling(e1, e2, velocityX, velocityY);
        }
    }

    private class ViewSimpleOnScaleGestureListener extends SimpleOnScaleGestureListener {
        private int initialViewToScaleContainerHeight = -1;
        private int initialViewToScaleContainerWidth = -1;
        private final int mMaxScaleMultiplier = 4;
        private final float mViewToScaleInitialScale;
        private float mViewToScaleScale;

        ViewSimpleOnScaleGestureListener() {
            float scaleX = MainScrollView.this.mWebView.getScaleX();
            this.mViewToScaleScale = scaleX;
            this.mViewToScaleInitialScale = scaleX;
        }

        public boolean onScale(ScaleGestureDetector detector) {
            this.mViewToScaleScale *= detector.getScaleFactor();
            this.mViewToScaleScale = Math.max(this.mViewToScaleInitialScale, Math.min(this.mViewToScaleScale, this.mViewToScaleInitialScale * 4.0f));
            MainScrollView.this.mWebView.setPivotY(0.0f);
            MainScrollView.this.mWebView.setScaleX(this.mViewToScaleScale);
            MainScrollView.this.mWebView.setScaleY(this.mViewToScaleScale);
            LayoutParams viewToScaleContainerLayoutParams = MainScrollView.this.mViewToScaleContainer.getLayoutParams();
            viewToScaleContainerLayoutParams.width = Math.round(((float) this.initialViewToScaleContainerWidth) * MainScrollView.this.mWebView.getScaleX());
            viewToScaleContainerLayoutParams.height = Math.round(((float) this.initialViewToScaleContainerHeight) * MainScrollView.this.mWebView.getScaleY());
            MainScrollView.this.mViewToScaleContainer.setLayoutParams(viewToScaleContainerLayoutParams);
            MainScrollView.this.keepScaledViewInHorizontalBounds(MainScrollView.this.mWebView, 0.0f);
            return true;
        }

        public boolean onScaleBegin(ScaleGestureDetector detector) {
            if (this.initialViewToScaleContainerWidth == -1) {
                this.initialViewToScaleContainerWidth = MainScrollView.this.mViewToScaleContainer.getWidth();
            }
            if (this.initialViewToScaleContainerHeight == -1) {
                this.initialViewToScaleContainerHeight = MainScrollView.this.mViewToScaleContainer.getHeight();
            }
            LayoutParams mViewToScaleLayoutParams = MainScrollView.this.mWebView.getLayoutParams();
            mViewToScaleLayoutParams.width = MainScrollView.this.mWebView.getWidth();
            mViewToScaleLayoutParams.height = MainScrollView.this.mWebView.getHeight();
            MainScrollView.this.mWebView.setLayoutParams(mViewToScaleLayoutParams);
            return super.onScaleBegin(detector);
        }
    }

    public MainScrollView(Context context) {
        super(context);
        this.mContext = context;
    }

    public MainScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    public MainScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
    }

    public void setScalableView(@NonNull WebView viewToScale, @NonNull View viewToScaleContainer) {
        if (viewToScale != null) {
            this.mWebView = viewToScale;
            this.mWebView.setOverScrollMode(2);
            this.mViewToScaleContainer = viewToScaleContainer;
            this.mScaleGestureDetector = new ScaleGestureDetector(this.mContext, new ViewSimpleOnScaleGestureListener());
            this.mGestureDetector = new GestureDetector(this.mContext, new ViewSimpleOnGestureListener());
        }
    }

    private void keepScaledViewInHorizontalBounds(View scaledView, float translateScaledViewX) {
        if (scaledView.getScaleX() == 1.0f) {
            scaledView.setTranslationX(0.0f);
            return;
        }
        int viewMaxWidth = scaledView.getWidth();
        float horizontalDeltaScale = (((float) viewMaxWidth) * scaledView.getScaleX()) - ((float) viewMaxWidth);
        if (horizontalDeltaScale > 0.0f) {
            horizontalDeltaScale /= 2.0f;
        }
        if (isScaleDoesNotAffectViewTranslate) {
            scaledView.setTranslationX(keepScaledViewInHorizontalBoundsLockBounds(scaledView.getTranslationX() - translateScaledViewX, horizontalDeltaScale));
        } else {
            scaledView.setTranslationX(keepScaledViewInHorizontalBoundsLockBounds((scaledView.getTranslationX() + horizontalDeltaScale) - translateScaledViewX, horizontalDeltaScale) - horizontalDeltaScale);
        }
    }

    private float keepScaledViewInHorizontalBoundsLockBounds(float x, float maxWidth) {
        if (x > 0.0f) {
            return 0.0f;
        }
        if (Math.abs(x) >= maxWidth * 2.0f) {
            return -(maxWidth * 2.0f);
        }
        return x;
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (this.mScaleGestureDetector == null) {
            return super.onTouchEvent(ev);
        }
        return this.mScaleGestureDetector.onTouchEvent(ev) || this.mGestureDetector.onTouchEvent(ev) || super.onTouchEvent(ev);
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return true;
    }
}
