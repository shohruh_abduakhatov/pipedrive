package com.pipedrive.util.networking.entities;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.model.settings.Company;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LoginDataEntity {
    @NonNull
    private final List<Company> companies = new ArrayList();
    @Nullable
    private Long defaultCompanyId;
    private boolean isAdmin = false;
    @Nullable
    private String mUserProfileEmail;
    @Nullable
    private String mUserProfileName;

    @Nullable
    public Company getDefaultCompany() {
        if (this.defaultCompanyId == null) {
            return null;
        }
        for (Company company : getCompanies()) {
            if (((long) company.getPipedriveId()) == this.defaultCompanyId.longValue()) {
                return company;
            }
        }
        return null;
    }

    public void setDefaultCompanyId(long defaultCompanyId) {
        this.defaultCompanyId = Long.valueOf(defaultCompanyId);
    }

    public boolean isAdmin() {
        return this.isAdmin;
    }

    public void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    @NonNull
    public List<Company> getCompanies() {
        return this.companies;
    }

    public void addLoginCompany(@NonNull Company loginCompany) {
        this.companies.add(loginCompany);
    }

    @Nullable
    public final String getUserProfileEmail() {
        return this.mUserProfileEmail;
    }

    public final void setUserProfileEmail(@Nullable String userProfileEmail) {
        this.mUserProfileEmail = userProfileEmail;
    }

    @Nullable
    public final String getUserProfileName() {
        return this.mUserProfileName;
    }

    public final void setUserProfileName(@Nullable String userProfileName) {
        this.mUserProfileName = userProfileName;
    }

    public String toString() {
        return "LoginDataEntity{defaultCompanyId=" + this.defaultCompanyId + ", isAdmin=" + this.isAdmin + ", mUserProfileEmail='" + this.mUserProfileEmail + '\'' + ", mUserProfileName='" + this.mUserProfileName + '\'' + ", companies=" + Arrays.toString(this.companies.toArray()) + '}';
    }
}
