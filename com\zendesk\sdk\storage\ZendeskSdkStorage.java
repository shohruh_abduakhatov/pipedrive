package com.zendesk.sdk.storage;

import com.zendesk.logger.Logger;
import com.zendesk.sdk.storage.SdkStorage.UserStorage;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

class ZendeskSdkStorage implements SdkStorage {
    private static final String LOG_TAG = ZendeskSdkStorage.class.getSimpleName();
    private final Set<UserStorage> mUserStorage = Collections.synchronizedSet(new LinkedHashSet());

    ZendeskSdkStorage() {
    }

    public void registerUserStorage(UserStorage userStorage) {
        if (this.mUserStorage == null) {
            Logger.e(LOG_TAG, "Additional storage set is null, returning...", new Object[0]);
        } else if (userStorage == null) {
            Logger.e(LOG_TAG, "Supplied UserStorage is null, returning...", new Object[0]);
        } else {
            synchronized (this.mUserStorage) {
                boolean instanceAlreadyRegistered = false;
                Iterator<UserStorage> iterator = this.mUserStorage.iterator();
                while (iterator.hasNext() && !instanceAlreadyRegistered) {
                    UserStorage currentStorage = (UserStorage) iterator.next();
                    if (currentStorage != null && currentStorage.getClass().equals(userStorage.getClass())) {
                        boolean currentStorageShouldBeCleared;
                        Logger.d(LOG_TAG, "There is already an instance of %s registered, will not add a duplicate", userStorage.getClass().getSimpleName());
                        instanceAlreadyRegistered = true;
                        if (currentStorage.getCacheKey().equals(userStorage.getCacheKey())) {
                            currentStorageShouldBeCleared = false;
                        } else {
                            currentStorageShouldBeCleared = true;
                        }
                        if (currentStorageShouldBeCleared) {
                            currentStorage.clearUserData();
                        }
                        iterator.remove();
                    }
                }
            }
            boolean modified = this.mUserStorage.add(userStorage);
            Logger.d(LOG_TAG, "Additional user storage size %s, modified %s", Integer.valueOf(this.mUserStorage.size()), Boolean.valueOf(modified));
        }
    }

    public void clearUserData() {
        if (this.mUserStorage == null) {
            Logger.w(LOG_TAG, "Additional user storage is null, returning...", new Object[0]);
            return;
        }
        synchronized (this.mUserStorage) {
            for (UserStorage userStorage : this.mUserStorage) {
                if (userStorage != null) {
                    userStorage.clearUserData();
                }
            }
        }
        this.mUserStorage.clear();
    }

    public Set<UserStorage> getUserStorage() {
        return Collections.unmodifiableSet(this.mUserStorage);
    }
}
