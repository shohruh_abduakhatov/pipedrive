package com.zendesk.sdk.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources.Theme;
import android.support.annotation.AttrRes;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import java.util.Locale;

public class UiUtils {
    private static final String LOG_TAG = "UiUtils";

    public enum ScreenSize {
        UNKNOWN,
        UNDEFINED,
        X_LARGE,
        LARGE,
        NORMAL,
        SMALL
    }

    private UiUtils() {
    }

    public static int dpToPixels(int sizeInDp, DisplayMetrics displayMetrics) {
        return Math.round(TypedValue.applyDimension(1, (float) sizeInDp, displayMetrics));
    }

    public static int spToPixels(int sizeInSp, DisplayMetrics displayMetrics) {
        return Math.round(TypedValue.applyDimension(2, (float) sizeInSp, displayMetrics));
    }

    public static int themeAttributeToPixels(int themeAttributeId, Context context, int fallbackUnitType, float fallbackUnitValue) {
        TypedValue outValue = new TypedValue();
        if (context.getTheme().resolveAttribute(themeAttributeId, outValue, true)) {
            return Math.round(outValue.getDimension(context.getResources().getDisplayMetrics()));
        }
        Logger.e(LOG_TAG, String.format(Locale.US, "Resource %d not found. Resource is either missing or you are using a non-ui context.", new Object[]{Integer.valueOf(themeAttributeId)}), new Object[0]);
        return Math.round(TypedValue.applyDimension(fallbackUnitType, fallbackUnitValue, context.getResources().getDisplayMetrics()));
    }

    public static int themeAttributeToColor(@AttrRes int themeAttributeId, @NonNull Context context, @ColorRes int fallbackColorId) {
        if (themeAttributeId == 0 || context == null || fallbackColorId == 0) {
            Logger.d(LOG_TAG, "themeAttributeId, context, and fallbackColorId are required.", new Object[0]);
            return ViewCompat.MEASURED_STATE_MASK;
        }
        TypedValue outValue = new TypedValue();
        if (!context.getTheme().resolveAttribute(themeAttributeId, outValue, true)) {
            Logger.e(LOG_TAG, String.format(Locale.US, "Resource %d not found. Resource is either missing or you are using a non-ui context.", new Object[]{Integer.valueOf(themeAttributeId)}), new Object[0]);
            return resolveColor(fallbackColorId, context);
        } else if (outValue.resourceId == 0) {
            return outValue.data;
        } else {
            return resolveColor(outValue.resourceId, context);
        }
    }

    private static int resolveColor(@ColorRes int colorId, @NonNull Context context) {
        return ContextCompat.getColor(context, colorId);
    }

    public static ScreenSize getScreenSize(Context context) {
        int screeLayout = context.getResources().getConfiguration().screenLayout & 15;
        ScreenSize screenSize = ScreenSize.UNKNOWN;
        if (screeLayout == 1) {
            return ScreenSize.SMALL;
        }
        if (screeLayout == 2) {
            return ScreenSize.NORMAL;
        }
        if (screeLayout == 3) {
            return ScreenSize.LARGE;
        }
        if (screeLayout == 4) {
            return ScreenSize.X_LARGE;
        }
        if (screeLayout == 0) {
            return ScreenSize.UNDEFINED;
        }
        return screenSize;
    }

    public static boolean isTablet(Context context) {
        ScreenSize screenSize = getScreenSize(context);
        return screenSize == ScreenSize.LARGE || screenSize == ScreenSize.X_LARGE;
    }

    public static void sizeDialogWidthForTablets(Dialog dialog, float percentageWidth) {
        if (dialog == null || !isTablet(dialog.getContext())) {
            Logger.e(LOG_TAG, "Dialog is null or the device is not a tablet", new Object[0]);
            return;
        }
        dialog.getWindow().setLayout(Math.round(((float) dialog.getContext().getResources().getDisplayMetrics().widthPixels) * percentageWidth), -2);
    }

    public static void setVisibility(View view, int visibilityState) {
        if (view == null) {
            Logger.w(LOG_TAG, "View is is null and can't change visibility", new Object[0]);
        } else {
            view.setVisibility(visibilityState);
        }
    }

    public static void dismissKeyboard(@Nullable Activity activity) {
        if (activity == null) {
            Logger.w(LOG_TAG, "Cannot dismiss the keyboard when fragment is detached or the activity is null.", new Object[0]);
            return;
        }
        InputMethodManager systemService = activity.getSystemService("input_method");
        if (systemService instanceof InputMethodManager) {
            InputMethodManager inputMethodManager = systemService;
            View currentFocus = activity.getCurrentFocus();
            if (currentFocus != null) {
                inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
                return;
            } else {
                Logger.w(LOG_TAG, "Cannot hide soft input because window token could not be obtained", new Object[0]);
                return;
            }
        }
        Logger.w(LOG_TAG, "Cannot hide soft input because we could not get the InputMethodManager", new Object[0]);
    }

    public static void setThemeIfAttributesAreMissing(Activity activity, @AttrRes int... attributes) {
        boolean z = false;
        if (activity == null || attributes == null) {
            String str = LOG_TAG;
            String str2 = "Cannot continue. Activity is null %b, attributes null %b";
            Object[] objArr = new Object[2];
            objArr[0] = Boolean.valueOf(activity == null);
            if (attributes == null) {
                z = true;
            }
            objArr[1] = Boolean.valueOf(z);
            Logger.e(str, str2, objArr);
        } else if (attributes.length == 0) {
            Logger.w(LOG_TAG, "No attributes were passed to check. This is likely a mistake.", new Object[0]);
        } else {
            Theme theme = activity.getTheme();
            boolean atLeastOneAttributeIsMissing = false;
            for (int i = 0; i < attributes.length && !atLeastOneAttributeIsMissing; i++) {
                if (theme.resolveAttribute(attributes[i], new TypedValue(), true)) {
                    atLeastOneAttributeIsMissing = false;
                } else {
                    atLeastOneAttributeIsMissing = true;
                }
            }
            if (atLeastOneAttributeIsMissing) {
                Logger.e(LOG_TAG, "Some required attributes were missing. Forcing a Zendesk theme. Please read https://developer.zendesk.com/embeddables/docs/android/customize_the_look#use-or-extend-an-sdk-theme", new Object[0]);
                activity.setTheme(R.style.ZendeskSdkTheme_Light);
            }
        }
    }
}
