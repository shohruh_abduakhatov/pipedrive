package com.pipedrive.sync;

import com.pipedrive.application.Session;
import com.pipedrive.tasks.AsyncTask;
import com.pipedrive.util.recents.RecentsQueryUtil;

class SyncManager$11 extends AsyncTask<Void, Void, Void> {
    final /* synthetic */ SyncManager this$0;

    SyncManager$11(SyncManager this$0, Session session) {
        this.this$0 = this$0;
        super(session);
    }

    protected Void doInBackground(Void... params) {
        RecentsQueryUtil.processChangesAndDownloadRecentsBlocking(getSession());
        return null;
    }
}
