package com.pipedrive.customfields.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.customfields.views.CustomFieldListView.OnItemClickListener;

class DummySpecialCustomFieldViewHolder extends SpecialCustomFieldViewHolder {
    public DummySpecialCustomFieldViewHolder(@NonNull Context context, @NonNull Long itemSqlId) {
        super(context, itemSqlId);
    }

    void bind(@NonNull Session session, @Nullable OnItemClickListener onItemClickListener) {
    }
}
