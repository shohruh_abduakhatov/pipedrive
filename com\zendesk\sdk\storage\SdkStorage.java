package com.zendesk.sdk.storage;

import java.util.Set;

public interface SdkStorage {

    public interface UserStorage {
        void clearUserData();

        String getCacheKey();
    }

    void clearUserData();

    Set<UserStorage> getUserStorage();

    void registerUserStorage(UserStorage userStorage);
}
