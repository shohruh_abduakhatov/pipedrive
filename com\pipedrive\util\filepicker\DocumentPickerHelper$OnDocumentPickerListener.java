package com.pipedrive.util.filepicker;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public abstract class DocumentPickerHelper$OnDocumentPickerListener {
    public abstract boolean documentPicked(@NonNull Uri uri, @Nullable String str, @Nullable Long l, @Nullable String str2);

    void documentPickCancelled() {
    }

    void documentPickFailed() {
    }
}
