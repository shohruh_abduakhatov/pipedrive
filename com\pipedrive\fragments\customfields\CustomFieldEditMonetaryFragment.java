package com.pipedrive.fragments.customfields;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import com.pipedrive.R;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.model.Currency;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.views.common.CurrencySpinner;
import com.pipedrive.views.common.CurrencySpinner.OnCurrencySelectedListener;
import com.pipedrive.views.common.DecimalEditText;

public class CustomFieldEditMonetaryFragment extends CustomFieldBaseFragment {
    private DecimalEditText mEditText;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(CustomFieldBaseFragment.ARG_CUSTOM_FIELD)) {
            this.mCustomField = (CustomField) getArguments().getParcelable(CustomFieldBaseFragment.ARG_CUSTOM_FIELD);
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_custom_field_edit_monetary, container, false);
        this.mEditText = (DecimalEditText) mainView.findViewById(R.id.customFieldTextEdit);
        return mainView;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        Session activeSession = PipedriveApp.getActiveSession();
        if (activeSession != null) {
            this.mEditText.init(activeSession);
            if (!TextUtils.isEmpty(this.mCustomField.getTempValue())) {
                this.mEditText.setDecimalValueWithoutTailingZeroesFromText(this.mCustomField.getTempValue());
            }
            this.mEditText.setMovementMethod(new ScrollingMovementMethod());
            setupCurrencySelectionView(activeSession, view);
        }
    }

    private void setupCurrencySelectionView(@NonNull Session session, @NonNull View layoutView) {
        boolean customFieldExist;
        boolean currencyCodeInCustomField = true;
        CurrencySpinner selectionSpinner = (CurrencySpinner) ButterKnife.findById(layoutView, (int) R.id.currency);
        if (this.mCustomField != null) {
            customFieldExist = true;
        } else {
            customFieldExist = false;
        }
        if (!customFieldExist || TextUtils.isEmpty(this.mCustomField.getSecondaryTempValue())) {
            currencyCodeInCustomField = false;
        }
        String selectedCurrencyCode = currencyCodeInCustomField ? this.mCustomField.getSecondaryTempValue() : session.getUserSettingsDefaultCurrencyCode();
        if (customFieldExist) {
            selectionSpinner.setup(session, selectedCurrencyCode, new OnCurrencySelectedListener() {
                public void onCurrencySelected(@NonNull Currency currency) {
                    CustomFieldEditMonetaryFragment.this.mCustomField.setSecondaryTempValue(currency.getCode());
                }
            });
        }
    }

    protected void clearContent() {
        this.mEditText.setText("");
        this.mCustomField.setSecondaryTempValue(null);
    }

    protected void saveContent() {
        this.mCustomField.setTempValue(this.mEditText.getText().toString());
    }
}
