package com.google.android.gms.auth.api.credentials;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;

public final class CredentialRequest extends AbstractSafeParcelable {
    public static final Creator<CredentialRequest> CREATOR = new zzc();
    private final CredentialPickerConfig iA;
    private final CredentialPickerConfig iB;
    private final boolean iy;
    private final String[] iz;
    final int mVersionCode;

    public static final class Builder {
        private CredentialPickerConfig iA;
        private CredentialPickerConfig iB;
        private boolean iy;
        private String[] iz;

        public CredentialRequest build() {
            if (this.iz == null) {
                this.iz = new String[0];
            }
            if (this.iy || this.iz.length != 0) {
                return new CredentialRequest();
            }
            throw new IllegalStateException("At least one authentication method must be specified");
        }

        public Builder setAccountTypes(String... strArr) {
            if (strArr == null) {
                strArr = new String[0];
            }
            this.iz = strArr;
            return this;
        }

        public Builder setCredentialHintPickerConfig(CredentialPickerConfig credentialPickerConfig) {
            this.iB = credentialPickerConfig;
            return this;
        }

        public Builder setCredentialPickerConfig(CredentialPickerConfig credentialPickerConfig) {
            this.iA = credentialPickerConfig;
            return this;
        }

        public Builder setPasswordLoginSupported(boolean z) {
            this.iy = z;
            return this;
        }

        @Deprecated
        public Builder setSupportsPasswordLogin(boolean z) {
            return setPasswordLoginSupported(z);
        }
    }

    CredentialRequest(int i, boolean z, String[] strArr, CredentialPickerConfig credentialPickerConfig, CredentialPickerConfig credentialPickerConfig2) {
        this.mVersionCode = i;
        this.iy = z;
        this.iz = (String[]) zzaa.zzy(strArr);
        if (credentialPickerConfig == null) {
            credentialPickerConfig = new com.google.android.gms.auth.api.credentials.CredentialPickerConfig.Builder().build();
        }
        this.iA = credentialPickerConfig;
        if (credentialPickerConfig2 == null) {
            credentialPickerConfig2 = new com.google.android.gms.auth.api.credentials.CredentialPickerConfig.Builder().build();
        }
        this.iB = credentialPickerConfig2;
    }

    private CredentialRequest(Builder builder) {
        this(2, builder.iy, builder.iz, builder.iA, builder.iB);
    }

    @NonNull
    public String[] getAccountTypes() {
        return this.iz;
    }

    @NonNull
    public CredentialPickerConfig getCredentialHintPickerConfig() {
        return this.iB;
    }

    @NonNull
    public CredentialPickerConfig getCredentialPickerConfig() {
        return this.iA;
    }

    @Deprecated
    public boolean getSupportsPasswordLogin() {
        return isPasswordLoginSupported();
    }

    public boolean isPasswordLoginSupported() {
        return this.iy;
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzc.zza(this, parcel, i);
    }
}
