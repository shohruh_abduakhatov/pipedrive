package com.zendesk.sdk.network;

public interface Constants {
    public static final String ACCEPT_HEADER = "Accept";
    public static final String ACCEPT_LANGUAGE_HEADER = "Accept-Language";
    public static final String APPLICATION_JSON = "application/json";
    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String CLIENT_IDENTIFIER_HEADER = "Client-Identifier";
    public static final String ENVIRONMENT_DEBUG = "Development";
    public static final String ENVIRONMENT_PRODUCTION = "Production";
    public static final String HEADER_SUFFIX_FORMAT = " %s/%s";
    public static final int HEADER_SUFFIX_MAX_LENGTH = 1024;
    public static final String SDK_GUID_HEADER = "Mobile-Sdk-Identity";
    public static final String USER_AGENT_HEADER = "User-Agent";
    public static final String ZENDESK_SDK_FOR_ANDROID = "Zendesk-SDK/%s Android/%d Env/%s";
}
