package com.pipedrive.views.agenda;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.model.Activity;

final class AgendaItem {
    @NonNull
    private Activity activity;
    private int column;
    private int columnSpan = 1;
    @NonNull
    private PipedriveDateTime endTimestamp;
    @NonNull
    private PipedriveDateTime startTimestamp;

    static AgendaItem wrap(@NonNull Activity activity) {
        if (activity.getActivityStartAsDateTime() == null) {
            return null;
        }
        return new AgendaItem(activity);
    }

    private AgendaItem(@NonNull Activity activity) {
        boolean endTimestampIsNotSet;
        this.activity = activity;
        this.startTimestamp = activity.getActivityStartAsDateTime();
        if (activity.getActivityEnd() == null) {
            endTimestampIsNotSet = true;
        } else {
            endTimestampIsNotSet = false;
        }
        boolean durationIsSetToLessThanDefault;
        if (activity.getDuration() == null || activity.getDuration().toSeconds() >= ((float) Activity.DEFAULT_ACTIVITY_DURATION_SECONDS)) {
            durationIsSetToLessThanDefault = false;
        } else {
            durationIsSetToLessThanDefault = true;
        }
        if (endTimestampIsNotSet || durationIsSetToLessThanDefault) {
            this.endTimestamp = PipedriveDateTime.instanceFromUnixTimeRepresentation(Long.valueOf(this.startTimestamp.getRepresentationInUnixTime() + Activity.DEFAULT_ACTIVITY_DURATION_SECONDS));
        } else {
            this.endTimestamp = activity.getActivityEnd();
        }
    }

    @NonNull
    PipedriveDateTime getStartTimestamp() {
        return this.startTimestamp;
    }

    @NonNull
    PipedriveDateTime getEndTimestamp() {
        return this.endTimestamp;
    }

    @NonNull
    public String getTitle() {
        return TextUtils.isEmpty(this.activity.getSubject()) ? "" : this.activity.getSubject();
    }

    public boolean isDone() {
        return this.activity.isDone();
    }

    public int getColumn() {
        return this.column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    int getColumnSpan() {
        return this.columnSpan;
    }

    void setColumnSpan(int columnSpan) {
        this.columnSpan = columnSpan;
    }

    boolean startsWithin(@NonNull PipedriveDateTime start, @NonNull PipedriveDateTime end) {
        return this.startTimestamp.getTime() == start.getTime() || this.startTimestamp.isBetween(start, end);
    }

    boolean startsWithin(AgendaItem agendaItem) {
        return startsWithin(agendaItem.startTimestamp, agendaItem.endTimestamp);
    }

    @NonNull
    public Activity getActivity() {
        return this.activity;
    }
}
