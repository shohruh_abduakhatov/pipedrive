package com.pipedrive.views.edit.person.rows;

import android.content.Context;
import android.view.ViewGroup;
import com.pipedrive.R;

class EmailEditRow extends CommunicationMediumEditRow {
    EmailEditRow(Context context, ViewGroup parent, int inputType) {
        super(context, parent, inputType);
    }

    final int getIconResourceId() {
        return R.drawable.activity_email_white;
    }

    final int getHintResourceId() {
        return R.string.email_address;
    }

    final int getLabelsResourceId() {
        return R.array.labels_email;
    }
}
