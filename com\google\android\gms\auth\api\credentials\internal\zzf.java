package com.google.android.gms.auth.api.credentials.internal;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.internal.zzqo.zza;

abstract class zzf<R extends Result> extends zza<R, zzg> {
    zzf(GoogleApiClient googleApiClient) {
        super(Auth.CREDENTIALS_API, googleApiClient);
    }

    protected abstract void zza(Context context, zzk com_google_android_gms_auth_api_credentials_internal_zzk) throws DeadObjectException, RemoteException;

    protected final void zza(zzg com_google_android_gms_auth_api_credentials_internal_zzg) throws DeadObjectException, RemoteException {
        zza(com_google_android_gms_auth_api_credentials_internal_zzg.getContext(), (zzk) com_google_android_gms_auth_api_credentials_internal_zzg.zzavg());
    }
}
