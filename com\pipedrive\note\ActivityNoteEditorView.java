package com.pipedrive.note;

import android.support.annotation.MainThread;

@MainThread
interface ActivityNoteEditorView extends HtmlEditorView {
    void onActivityNoteDeleted(boolean z);

    void onActivityNoteUpdated(boolean z);
}
