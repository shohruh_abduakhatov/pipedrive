package com.pipedrive.datasource.products;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.datasource.BaseDataSource;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.datasource.SearchHelper;
import com.pipedrive.datasource.SelectionHolder;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.model.products.Product;
import com.pipedrive.util.CursorHelper;

public class ProductsDataSource extends BaseDataSource<Product> {
    private static final String[] ALL_COLUMNS = new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_PRODUCTS_PIPEDRIVE_ID, PipeSQLiteHelper.COLUMN_PRODUCTS_NAME, PipeSQLiteHelper.COLUMN_PRODUCTS_IS_ACTIVE, PipeSQLiteHelper.COLUMN_PRODUCTS_IS_REMOVED};

    public ProductsDataSource(SQLiteDatabase dbconn) {
        super(dbconn);
    }

    @NonNull
    protected String getColumnNameForPipedriveId() {
        return PipeSQLiteHelper.COLUMN_PRODUCTS_PIPEDRIVE_ID;
    }

    @NonNull
    protected String getColumnNameForSqlId() {
        return PipeSQLiteHelper.COLUMN_ID;
    }

    @NonNull
    protected String getTableName() {
        return "products";
    }

    @Nullable
    public Product deflateCursor(@NonNull Cursor cursor) {
        String name = CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_PRODUCTS_NAME);
        Boolean isActive = CursorHelper.getBoolean(cursor, PipeSQLiteHelper.COLUMN_PRODUCTS_IS_ACTIVE);
        Boolean isRemoved = CursorHelper.getBoolean(cursor, PipeSQLiteHelper.COLUMN_PRODUCTS_IS_REMOVED);
        if (name == null || isActive == null || isRemoved == null) {
            return null;
        }
        Long sqlId = CursorHelper.getLong(cursor, getColumnNameForSqlId());
        return Product.create(sqlId, CursorHelper.getLong(cursor, getColumnNameForPipedriveId()), name, isActive, isRemoved, new ProductPriceDataSource(getTransactionalDBConnection()).findAllRelatedToParentSqlId(sqlId), new ProductVariationDataSource(getTransactionalDBConnection()).findAllRelatedToParentSqlId(sqlId));
    }

    @NonNull
    protected ContentValues getContentValues(@NonNull Product product) {
        ContentValues contentValues = super.getContentValues(product);
        CursorHelper.put(product.getName(), contentValues, PipeSQLiteHelper.COLUMN_PRODUCTS_NAME);
        CursorHelper.put(product.isActive(), contentValues, PipeSQLiteHelper.COLUMN_PRODUCTS_IS_ACTIVE);
        CursorHelper.put(product.isRemoved(), contentValues, PipeSQLiteHelper.COLUMN_PRODUCTS_IS_REMOVED);
        return contentValues;
    }

    protected void onSuccessfulCreateOrUpdate(@NonNull Product product) {
        new ProductPriceDataSource(getTransactionalDBConnection()).createOrUpdateProduct(product);
        new ProductVariationDataSource(getTransactionalDBConnection()).createOrUpdateProduct(product);
    }

    @NonNull
    protected String[] getAllColumns() {
        return ALL_COLUMNS;
    }

    @NonNull
    public Cursor getSearchCursor(@NonNull SearchConstraint searchConstraint) {
        return query(getTableName(), getAllColumns(), getSelectionForSearch(searchConstraint), "products_name COLLATE NOCASE ASC");
    }

    @NonNull
    private SelectionHolder getSelectionForSearch(@NonNull SearchConstraint searchConstraint) {
        String selection = "products_is_active =? AND products_is_removed =? ";
        return new SelectionHolder("products_is_active =? AND products_is_removed =? ", new String[]{String.valueOf(1), String.valueOf(0)}).add(new SearchHelper(PipeSQLiteHelper.COLUMN_PRODUCTS_NAME).getSelectionHolderForSearchConstraint(searchConstraint));
    }
}
