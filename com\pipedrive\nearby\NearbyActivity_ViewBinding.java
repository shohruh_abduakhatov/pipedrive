package com.pipedrive.nearby;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;
import com.pipedrive.nearby.cards.Cards;
import com.pipedrive.nearby.cards.cards.EmptyCard;
import com.pipedrive.nearby.filter.views.FilterButton;
import com.pipedrive.nearby.searchbutton.SearchButton;
import com.pipedrive.nearby.toolbar.NearbyToolbar;

public class NearbyActivity_ViewBinding implements Unbinder {
    private NearbyActivity target;

    @UiThread
    public NearbyActivity_ViewBinding(NearbyActivity target) {
        this(target, target.getWindow().getDecorView());
    }

    @UiThread
    public NearbyActivity_ViewBinding(NearbyActivity target, View source) {
        this.target = target;
        target.cards = (Cards) Utils.findRequiredViewAsType(source, R.id.cards, "field 'cards'", Cards.class);
        target.searchButton = (SearchButton) Utils.findRequiredViewAsType(source, R.id.searchButtonView, "field 'searchButton'", SearchButton.class);
        target.nearbyToolbar = (NearbyToolbar) Utils.findRequiredViewAsType(source, R.id.nearbyToolbar, "field 'nearbyToolbar'", NearbyToolbar.class);
        target.progress = Utils.findRequiredView(source, R.id.progress, "field 'progress'");
        target.emptyCard = (EmptyCard) Utils.findRequiredViewAsType(source, R.id.emptyCard, "field 'emptyCard'", EmptyCard.class);
        target.filterButton = (FilterButton) Utils.findRequiredViewAsType(source, R.id.filterButton, "field 'filterButton'", FilterButton.class);
        target.progressTranslationY = source.getContext().getResources().getDimensionPixelSize(R.dimen.nearby.progressSpinner.translationY);
    }

    @CallSuper
    public void unbind() {
        NearbyActivity target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.cards = null;
        target.searchButton = null;
        target.nearbyToolbar = null;
        target.progress = null;
        target.emptyCard = null;
        target.filterButton = null;
    }
}
