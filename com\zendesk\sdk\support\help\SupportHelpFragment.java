package com.zendesk.sdk.support.help;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.background.ApplicationStateMonitor;
import com.newrelic.agent.android.instrumentation.Instrumented;
import com.newrelic.agent.android.tracing.TraceMachine;
import com.zendesk.sdk.R;
import com.zendesk.sdk.support.SupportMvp.Presenter;
import com.zendesk.sdk.support.SupportUiConfig;

@Instrumented
public class SupportHelpFragment extends Fragment implements TraceFieldInterface {
    private static final String ARG_SDK_UI_CONFIG = "arg_sdk_ui_config";
    public static final String LOG_TAG = "SupportHelpFragment";
    private HelpRecyclerViewAdapter adapter;
    private Presenter presenter;
    private RecyclerView recyclerView;

    protected void onStart() {
        super.onStart();
        ApplicationStateMonitor.getInstance().activityStarted();
    }

    protected void onStop() {
        super.onStop();
        ApplicationStateMonitor.getInstance().activityStopped();
    }

    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
        if (this.adapter != null) {
            this.adapter.setContentUpdateListener(presenter);
        }
    }

    public void onCreate(@Nullable Bundle bundle) {
        TraceMachine.startTracing(LOG_TAG);
        try {
            TraceMachine.enterMethod(this._nr_trace, "SupportHelpFragment#onCreate", null);
        } catch (NoSuchFieldError e) {
            while (true) {
                TraceMachine.enterMethod(null, "SupportHelpFragment#onCreate", null);
            }
        }
        super.onCreate(bundle);
        setRetainInstance(true);
        this.adapter = new HelpRecyclerViewAdapter((SupportUiConfig) getArguments().getParcelable(ARG_SDK_UI_CONFIG));
        if (this.presenter != null) {
            this.adapter.setContentUpdateListener(this.presenter);
        }
        TraceMachine.exitMethod();
    }

    @Nullable
    public View onCreateView(LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        try {
            TraceMachine.enterMethod(this._nr_trace, "SupportHelpFragment#onCreateView", null);
        } catch (NoSuchFieldError e) {
            while (true) {
                TraceMachine.enterMethod(null, "SupportHelpFragment#onCreateView", null);
            }
        }
        View view = layoutInflater.inflate(R.layout.fragment_help, viewGroup, false);
        this.recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        setupRecyclerView();
        TraceMachine.exitMethod();
        return view;
    }

    private void setupRecyclerView() {
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
        this.recyclerView.addItemDecoration(new SeparatorDecoration(ContextCompat.getDrawable(getContext(), R.drawable.help_separator)));
        this.recyclerView.setAdapter(this.adapter);
    }

    public static SupportHelpFragment newInstance(SupportUiConfig supportUiConfig) {
        SupportHelpFragment fragment = new SupportHelpFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_SDK_UI_CONFIG, supportUiConfig);
        fragment.setArguments(args);
        return fragment;
    }
}
