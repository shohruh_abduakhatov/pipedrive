package com.pipedrive.model.products;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.model.ChildDataSourceEntity;
import com.pipedrive.model.Currency;
import java.math.BigDecimal;

public class ProductPrice extends ChildDataSourceEntity {
    @NonNull
    private final Price price;

    @NonNull
    public static ProductPrice create(@NonNull Long pipedriveId, @NonNull BigDecimal value, @NonNull Currency currency) {
        return create(null, pipedriveId, null, Price.create(value, currency));
    }

    @NonNull
    public static ProductPrice create(@Nullable Long sqlId, @Nullable Long pipedriveId, @Nullable Long parentSqlId, @NonNull Price price) {
        return new ProductPrice(sqlId, pipedriveId, parentSqlId, price);
    }

    private ProductPrice(@Nullable Long sqlId, @Nullable Long pipedriveId, @Nullable Long parentSqlId, @NonNull Price price) {
        super(sqlId, pipedriveId, parentSqlId);
        this.price = price;
    }

    @NonNull
    public Price getPrice() {
        return this.price;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass() || !super.equals(o)) {
            return false;
        }
        return this.price.equals(((ProductPrice) o).price);
    }

    public int hashCode() {
        return (super.hashCode() * 31) + this.price.hashCode();
    }

    public String toString() {
        return "ProductPrice{price=" + this.price + '}';
    }
}
