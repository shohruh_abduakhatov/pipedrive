package com.pipedrive.views.stageselector;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.TouchDelegate;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.PipelineDataSource;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.Deal;
import com.pipedrive.model.DealStage;
import com.pipedrive.model.DealStatus;
import com.pipedrive.util.PipedriveUtil;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.views.SelectedStagesIndicator;
import java.util.ArrayList;
import java.util.List;

public class StagesSelector extends LinearLayout {
    public static final int COLORING_SCHEME_GREEN = 1;
    public static final int COLORING_SCHEME_RED = 2;
    static final String TAG = StagesSelector.class.getSimpleName();
    private int mActiveColoringScheme;
    private FrameLayout mGradientOverlayContainer;
    private PagerTitleStrip mPagerTitleStrip;
    private SelectedStagesIndicator mSelectedStagesIndicator;
    private TouchDelegateComposite mTouchDelegateComposite;
    private final OnPageChangeListener pageChangeListener;
    @Nullable
    private ViewPager stageSelector;
    private ViewGroup stageSelectorMainLayout;
    private ImageView stageSelectorPickerArrow;
    private List<DealStage> stages;
    private List<StageSelectorStageView> stagesFragments;
    private StagesPagerAdapter stagesPagerAdapter;
    private StagesSelectorIF stagesSelectorIF;

    private static class StageTitleHeightDelegate implements Runnable {
        private final TouchDelegateComposite mTouchDelegateComposite;
        private View mView = null;

        StageTitleHeightDelegate(@NonNull View view, @NonNull TouchDelegateComposite touchDelegateComposite) {
            this.mView = view;
            this.mTouchDelegateComposite = touchDelegateComposite;
        }

        public void run() {
            View viewParent = (View) this.mView.getParent();
            if (viewParent != null) {
                Rect delegateArea = new Rect();
                this.mView.getHitRect(delegateArea);
                delegateArea.top -= viewParent.getHeight() / 2;
                delegateArea.bottom += viewParent.getHeight() / 2;
                this.mTouchDelegateComposite.addDelegate(new TouchDelegate(delegateArea, this.mView));
                viewParent.setTouchDelegate(this.mTouchDelegateComposite);
            }
        }
    }

    private class StagesPagerAdapter extends FragmentStatePagerAdapter {
        public StagesPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public StageSelectorStageView getItem(int position) {
            return (StageSelectorStageView) StagesSelector.this.stagesFragments.get(position);
        }

        public int getCount() {
            return StagesSelector.this.stagesFragments.size();
        }

        public CharSequence getPageTitle(int position) {
            return ((DealStage) StagesSelector.this.stages.get(position)).getName();
        }
    }

    public interface StagesSelectorIF {
        void pageScrollChanged(int i);
    }

    private static class TouchDelegateComposite extends TouchDelegate {
        private final List<TouchDelegate> mTouchDelegates;

        public TouchDelegateComposite(Rect rect, View view) {
            super(rect, view);
            this.mTouchDelegates = new ArrayList();
        }

        public TouchDelegateComposite(Context context) {
            this(new Rect(), new View(context));
        }

        public void addDelegate(TouchDelegate delegate) {
            this.mTouchDelegates.add(delegate);
        }

        public void clearDelegates() {
            if (!this.mTouchDelegates.isEmpty()) {
                this.mTouchDelegates.clear();
            }
        }

        public boolean onTouchEvent(MotionEvent event) {
            boolean eventHandled = false;
            float x = event.getX();
            float y = event.getY();
            for (TouchDelegate delegate : this.mTouchDelegates) {
                event.setLocation(x, y);
                eventHandled = delegate.onTouchEvent(event) || eventHandled;
            }
            return eventHandled;
        }
    }

    public void setup(@NonNull Session session, @NonNull Deal deal) {
        List<DealStage> stages = new PipelineDataSource(session.getDatabase()).getAllStagesWithinPipeline(deal.getStage());
        if (stages != null) {
            int stageId = deal.getStage();
            for (int i = stages.size() - 1; i >= 0; i--) {
                DealStage dealStage = (DealStage) stages.get(i);
                if (dealStage != null && dealStage.getPipedriveIdOrNull() != null && dealStage.getPipedriveIdOrNull().longValue() == ((long) stageId)) {
                    setStages(stages, i);
                    break;
                }
            }
        }
        if (deal.getStatus() == DealStatus.OPEN || deal.getStatus() == DealStatus.WON) {
            setColoringScheme(1);
        } else {
            setColoringScheme(2);
        }
        this.mPagerTitleStrip.forceLayout();
    }

    public void setStagesSelectorIF(StagesSelectorIF stagesSelectorIF) {
        this.stagesSelectorIF = stagesSelectorIF;
    }

    public StagesSelector(Context context) {
        this(context, null);
    }

    public StagesSelector(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.stagesFragments = new ArrayList();
        this.stages = new ArrayList();
        this.mActiveColoringScheme = 1;
        this.pageChangeListener = new OnPageChangeListener() {
            public void onPageSelected(int position) {
                StagesSelector.this.adjustProgress(position + 1);
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                for (int index = 0; index < StagesSelector.this.mPagerTitleStrip.getChildCount(); index++) {
                    if (StagesSelector.this.mPagerTitleStrip.getChildAt(index) instanceof TextView) {
                        TextView stageTitle = (TextView) StagesSelector.this.mPagerTitleStrip.getChildAt(index);
                        stageTitle.setEllipsize(TruncateAt.MIDDLE);
                        switch (index) {
                            case 0:
                                stageTitle.setTypeface(null, 0);
                                stageTitle.setFocusable(true);
                                stageTitle.setOnClickListener(new OnClickListener() {
                                    public void onClick(View v) {
                                        if (StagesSelector.this.stageSelector != null) {
                                            StagesSelector.this.stageSelector.setCurrentItem(StagesSelector.this.stageSelector.getCurrentItem() - 1);
                                        }
                                    }
                                });
                                break;
                            case 2:
                                stageTitle.setFocusable(true);
                                stageTitle.setOnClickListener(new OnClickListener() {
                                    public void onClick(View v) {
                                        if (StagesSelector.this.stageSelector != null) {
                                            StagesSelector.this.stageSelector.setCurrentItem(StagesSelector.this.stageSelector.getCurrentItem() + 1);
                                        }
                                    }
                                });
                                break;
                            default:
                                stageTitle.setTypeface(null, 1);
                                StagesSelector.this.resizeLongStageTitle(stageTitle);
                                break;
                        }
                    }
                }
            }

            public void onPageScrollStateChanged(int state) {
                if (StagesSelector.this.stagesSelectorIF != null) {
                    StagesSelector.this.stagesSelectorIF.pageScrollChanged(state);
                }
                if (state == 0) {
                    PipedriveUtil.vibrate(StagesSelector.this.getContext());
                }
                StagesSelector.this.mTouchDelegateComposite.clearDelegates();
                StagesSelector.this.addTouchDelegatesToStageTitleViews();
            }
        };
        setOrientation(0);
        setGravity(16);
        View parentView = getRootView(context);
        if (!isInEditMode()) {
            setupView(context, parentView);
        }
    }

    private View getRootView(Context context) {
        return ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(R.layout.stage_selector, this, true);
    }

    private void setupView(Context context, View parentView) {
        int spacingBetweenStageTitles = getResources().getDimensionPixelSize(R.dimen.dimen6);
        Activity fragmentActivity = (Activity) context;
        this.mSelectedStagesIndicator = (SelectedStagesIndicator) parentView.findViewById(R.id.stageIndicator);
        this.stageSelectorMainLayout = (ViewGroup) parentView.findViewById(R.id.stageSelectorMainLayout);
        this.stageSelector = (ViewPager) parentView.findViewById(R.id.stageSelectorPager);
        this.stageSelectorPickerArrow = (ImageView) parentView.findViewById(R.id.stageSelectorPickerArrow);
        this.stagesPagerAdapter = new StagesPagerAdapter(fragmentActivity.getFragmentManager());
        this.stageSelector.setAdapter(this.stagesPagerAdapter);
        this.mPagerTitleStrip = (PagerTitleStrip) parentView.findViewById(R.id.pagerTitleStrip);
        this.mPagerTitleStrip.setTextSpacing(spacingBetweenStageTitles);
        this.mPagerTitleStrip.setNonPrimaryAlpha(0.5f);
        this.mTouchDelegateComposite = new TouchDelegateComposite(context);
        addTouchDelegatesToStageTitleViews();
        this.mGradientOverlayContainer = (FrameLayout) parentView.findViewById(R.id.gradientContainer);
        this.mGradientOverlayContainer.setForeground(ContextCompat.getDrawable(context, R.drawable.shape_gradient_green));
        this.stageSelector.addOnPageChangeListener(this.pageChangeListener);
        setColoringScheme(1);
    }

    private void addTouchDelegatesToStageTitleViews() {
        View previousStageTitle = this.mPagerTitleStrip.getChildAt(0);
        View nextStageTitle = this.mPagerTitleStrip.getChildAt(2);
        this.mPagerTitleStrip.post(new StageTitleHeightDelegate(previousStageTitle, this.mTouchDelegateComposite));
        this.mPagerTitleStrip.post(new StageTitleHeightDelegate(nextStageTitle, this.mTouchDelegateComposite));
    }

    public List<DealStage> getStages() {
        return this.stages;
    }

    public void setStages(List<DealStage> stages, final int currentItem) {
        this.stages = stages;
        if (stages != null) {
            if (this.stagesFragments == null) {
                this.stagesFragments = new ArrayList(stages.size());
            }
            this.stagesFragments.clear();
            for (DealStage dealStage : stages) {
                this.stagesFragments.add(StageSelectorStageView.newInstance());
            }
            if (this.stagesPagerAdapter != null) {
                this.stagesPagerAdapter.notifyDataSetChanged();
            }
            if (this.mActiveColoringScheme == 1) {
                this.mSelectedStagesIndicator.setup(currentItem + 1, this.stages.size(), R.color.stage_selector_selector_background_green, R.color.stage_selector_stage_progress_background_green, R.color.green);
            } else {
                this.mSelectedStagesIndicator.setup(currentItem + 1, this.stages.size(), R.color.stage_selector_selector_background_red, R.color.stage_selector_stage_progress_background_red, R.color.red);
            }
            ViewUtil.onGlobalLayout(this, new OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    if (StagesSelector.this.stageSelector != null && currentItem >= 0) {
                        if (StagesSelector.this.stages.size() > currentItem + 1) {
                            StagesSelector.this.stageSelector.setCurrentItem(currentItem + 1, false);
                        } else {
                            StagesSelector.this.stageSelector.setCurrentItem(currentItem - 1, false);
                        }
                        StagesSelector.this.stageSelector.setCurrentItem(currentItem, false);
                    }
                }
            });
        }
    }

    @Nullable
    public DealStage getCurrentDealStage() {
        try {
            return (DealStage) this.stages.get(this.stageSelector.getCurrentItem());
        } catch (Exception e) {
            LogJourno.reportEvent(EVENT.NotSpecified_stageSelectorGetCurrentStageFailed, e);
            return null;
        }
    }

    private void adjustProgress(int progress) {
        this.mSelectedStagesIndicator.setSelectedStageAndClearLayout(progress);
    }

    public final void setColoringScheme(int coloringScheme) {
        String tag = TAG + ".setColoringScheme()";
        if (this.mActiveColoringScheme == coloringScheme) {
            String str = "No changes to the current coloring scheme %s will be made.";
            Object[] objArr = new Object[1];
            String str2 = this.mActiveColoringScheme == 1 ? "GREEN" : this.mActiveColoringScheme == 2 ? "RED" : "_UNKNOWN_";
            objArr[0] = str2;
            Log.d(tag, String.format(str, objArr));
        } else if (coloringScheme == 1) {
            Log.d(tag, "Green coloring scheme requested.");
            this.mActiveColoringScheme = coloringScheme;
            this.stageSelectorMainLayout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.stage_selector_selector_background_green));
            this.stageSelectorPickerArrow.setImageResource(R.drawable.deal_stage_picker_arrow);
            this.mGradientOverlayContainer.setForeground(ContextCompat.getDrawable(getContext(), R.drawable.shape_gradient_green));
            this.mSelectedStagesIndicator.setColors(R.color.stage_selector_selector_background_green, R.color.stage_selector_stage_progress_background_green, R.color.green);
        } else if (coloringScheme == 2) {
            Log.d(tag, "Red coloring scheme requested.");
            this.mActiveColoringScheme = coloringScheme;
            this.stageSelectorMainLayout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.stage_selector_selector_background_red));
            this.stageSelectorPickerArrow.setImageResource(R.drawable.deal_stage_picker_arrow_lost);
            this.mGradientOverlayContainer.setForeground(ContextCompat.getDrawable(getContext(), R.drawable.shape_gradient_red));
            this.mSelectedStagesIndicator.setColors(R.color.stage_selector_selector_background_red, R.color.stage_selector_stage_progress_background_red, R.color.red);
        }
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        resizeLongStageTitle((TextView) this.mPagerTitleStrip.getChildAt(1));
    }

    private void resizeLongStageTitle(TextView stageTitle) {
        int parentWidth = this.mPagerTitleStrip.getWidth();
        int parentHeight = this.mPagerTitleStrip.getHeight();
        if (stageTitle.getWidth() > parentWidth / 2) {
            stageTitle.measure(MeasureSpec.makeMeasureSpec(parentWidth / 2, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(parentHeight, Integer.MIN_VALUE));
        }
    }
}
