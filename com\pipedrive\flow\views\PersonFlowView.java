package com.pipedrive.flow.views;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import com.pipedrive.application.Session;

public class PersonFlowView extends FlowView {
    public PersonFlowView(Context context) {
        this(context, null);
    }

    public PersonFlowView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PersonFlowView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @NonNull
    protected Cursor getPlannedFlowCursor(@NonNull Session session) {
        return getFlowDataSource(session).getPersonPlannedFlowCursor(this.mSqlId);
    }

    @NonNull
    protected Cursor getPastFlowCursor(@NonNull Session session) {
        return getFlowDataSource(session).getPersonPastFlowCursor(this.mSqlId);
    }
}
