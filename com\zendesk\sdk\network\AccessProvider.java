package com.zendesk.sdk.network;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.sdk.model.access.AccessToken;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.model.access.JwtIdentity;
import com.zendesk.service.ZendeskCallback;

public interface AccessProvider {
    void getAndStoreAuthTokenViaAnonymous(@NonNull AnonymousIdentity anonymousIdentity, @Nullable ZendeskCallback<AccessToken> zendeskCallback);

    void getAndStoreAuthTokenViaJwt(@NonNull JwtIdentity jwtIdentity, @Nullable ZendeskCallback<AccessToken> zendeskCallback);
}
