package com.pipedrive.nearby.filter.views;

import android.support.annotation.NonNull;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.toolbar.NearbyItemTypeChangesProvider;
import com.pipedrive.nearby.util.Irrelevant;
import rx.Observable;

public interface FilterButton {
    void bind();

    @NonNull
    Observable<Irrelevant> filterButtonClicks();

    void releaseResources();

    void setup(@NonNull Session session, @NonNull NearbyItemTypeChangesProvider nearbyItemTypeChangesProvider);
}
