package com.pipedrive.model.customfields;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.newrelic.agent.android.instrumentation.JSONArrayInstrumentation;
import com.pipedrive.logging.Log;
import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CustomField implements Parcelable, Cloneable {
    public static final Creator CREATOR = new Creator() {
        public CustomField createFromParcel(Parcel in) {
            return new CustomField(in);
        }

        public CustomField[] newArray(int size) {
            return new CustomField[size];
        }
    };
    public static final String FIELD_DATA_TYPE_ADDRESS = "address";
    public static final String FIELD_DATA_TYPE_AUTOCOMPLETE = "autocomplete";
    public static final String FIELD_DATA_TYPE_DATE = "date";
    public static final String FIELD_DATA_TYPE_DATE_RANGE = "daterange";
    public static final String FIELD_DATA_TYPE_DOUBLE = "double";
    public static final String FIELD_DATA_TYPE_LARGE_TEXT = "text";
    public static final String FIELD_DATA_TYPE_MONEY = "monetary";
    public static final String FIELD_DATA_TYPE_MULTIPLE_OPTION = "set";
    public static final String FIELD_DATA_TYPE_ORGANIZATION = "organization";
    public static final String FIELD_DATA_TYPE_PERSON = "person";
    public static final String FIELD_DATA_TYPE_PHONE = "phone";
    public static final String FIELD_DATA_TYPE_SINGLE_OPTION = "enum";
    public static final String FIELD_DATA_TYPE_TEXT = "varchar";
    public static final String FIELD_DATA_TYPE_TIME = "time";
    public static final String FIELD_DATA_TYPE_TIME_RANGE = "timerange";
    public static final String FIELD_DATA_TYPE_USER = "user";
    public static final String FIELD_SECONDARY_KEY_ADDRESS = "_formatted_address";
    public static final String FIELD_SECONDARY_KEY_CURRENCY = "_currency";
    public static final String FIELD_SECONDARY_KEY_DATE_RANGE = "_until";
    public static final String FIELD_SECONDARY_KEY_TIME_RANGE = "_until";
    public static final String FIELD_TYPE_DEAL_FIELD = "deal";
    public static final String FIELD_TYPE_ORGANIZATION_FIELD = "organization";
    public static final String FIELD_TYPE_PERSON_FIELD = "person";
    public static final String LOCAL_DATABASE_ID_PREFIX = "sql_id_";
    private boolean active;
    private String fieldDataType;
    private String fieldType;
    private int id;
    private boolean isEditable = true;
    private String key;
    private String name;
    private JSONArray options;
    private int orderNr;
    private long pipedriveId;
    private String secondaryTempValue;
    private String tempValue;

    public CustomField(Parcel in) {
        readFromParcel(in);
    }

    public CustomField(CustomField customField) {
        if (customField != null) {
            this.active = customField.active;
            this.fieldDataType = customField.fieldDataType;
            this.fieldType = customField.fieldType;
            this.id = customField.id;
            this.isEditable = customField.isEditable;
            this.key = customField.key;
            this.name = customField.name;
            this.options = customField.options;
            this.orderNr = customField.orderNr;
            this.pipedriveId = customField.pipedriveId;
            this.secondaryTempValue = customField.secondaryTempValue;
            this.tempValue = customField.tempValue;
        }
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getPipedriveId() {
        return this.pipedriveId;
    }

    public void setPipedriveId(long pipedriveId) {
        this.pipedriveId = pipedriveId;
    }

    public int getOrderNr() {
        return this.orderNr;
    }

    public void setOrderNr(int orderNr) {
        this.orderNr = orderNr;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFieldDataType() {
        return this.fieldDataType;
    }

    public void setFieldDataType(String fieldDataType) {
        this.fieldDataType = fieldDataType;
    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public JSONArray getOptions() {
        return this.options;
    }

    public void setOptions(JSONArray options) {
        this.options = options;
    }

    public String getTempValue() {
        return this.tempValue;
    }

    public void setTempValue(String tempValue) {
        this.tempValue = tempValue;
    }

    public String getFieldType() {
        return this.fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public static JSONObject readCustomValuesJsonObject(JsonReader reader, String field) throws IOException, JSONException {
        JSONObject customFieldObj = new JSONObject();
        if (reader.peek() == JsonToken.NUMBER) {
            customFieldObj.put(field, reader.nextDouble());
        } else if (reader.peek() == JsonToken.STRING) {
            customFieldObj.put(field, reader.nextString());
        } else if (reader.peek() == JsonToken.BOOLEAN) {
            customFieldObj.put(field, reader.nextBoolean());
        } else if (reader.peek() == JsonToken.BEGIN_ARRAY) {
            reader.beginArray();
            JSONArray arr = new JSONArray();
            while (reader.hasNext()) {
                customFieldObjInner = new JSONObject();
                reader.beginObject();
                while (reader.hasNext()) {
                    contactFieldInner = reader.nextName();
                    if (reader.peek() == JsonToken.NUMBER) {
                        customFieldObjInner.put(contactFieldInner, reader.nextDouble());
                    } else if (reader.peek() == JsonToken.STRING) {
                        customFieldObjInner.put(contactFieldInner, reader.nextString());
                    } else if (reader.peek() == JsonToken.BOOLEAN) {
                        customFieldObjInner.put(contactFieldInner, reader.nextBoolean());
                    }
                }
                arr.put(customFieldObjInner);
                reader.endObject();
            }
            customFieldObj.put(field, arr);
            reader.endArray();
        } else if (reader.peek() == JsonToken.BEGIN_OBJECT) {
            reader.beginObject();
            customFieldObjInner = new JSONObject();
            while (reader.hasNext() && reader.peek() != JsonToken.NULL) {
                contactFieldInner = reader.nextName();
                if (reader.peek() == JsonToken.NUMBER) {
                    customFieldObjInner.put(contactFieldInner, reader.nextDouble());
                } else if (reader.peek() == JsonToken.STRING) {
                    customFieldObjInner.put(contactFieldInner, reader.nextString());
                } else if (reader.peek() == JsonToken.BOOLEAN) {
                    customFieldObjInner.put(contactFieldInner, reader.nextBoolean());
                } else {
                    reader.skipValue();
                }
            }
            customFieldObj.put(field, customFieldObjInner);
            reader.endObject();
        } else {
            reader.skipValue();
        }
        return customFieldObj;
    }

    public String getSecondaryTempValue() {
        return this.secondaryTempValue;
    }

    public void setSecondaryTempValue(String secondaryTempValue) {
        this.secondaryTempValue = secondaryTempValue;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        String jSONArray;
        int i = 1;
        dest.writeInt(this.id);
        dest.writeLong(this.pipedriveId);
        dest.writeInt(this.orderNr);
        dest.writeString(this.key);
        dest.writeString(this.name);
        dest.writeString(this.fieldDataType);
        dest.writeInt(this.active ? 1 : 0);
        if (!this.isEditable) {
            i = 0;
        }
        dest.writeInt(i);
        if (this.options != null) {
            JSONArray jSONArray2 = this.options;
            jSONArray = !(jSONArray2 instanceof JSONArray) ? jSONArray2.toString() : JSONArrayInstrumentation.toString(jSONArray2);
        } else {
            jSONArray = null;
        }
        dest.writeString(jSONArray);
        dest.writeString(this.fieldType);
        dest.writeString(this.tempValue);
        dest.writeString(this.secondaryTempValue);
    }

    private void readFromParcel(Parcel in) {
        boolean z;
        boolean z2 = true;
        this.id = in.readInt();
        this.pipedriveId = in.readLong();
        this.orderNr = in.readInt();
        this.key = in.readString();
        this.name = in.readString();
        this.fieldDataType = in.readString();
        if (in.readInt() == 1) {
            z = true;
        } else {
            z = false;
        }
        this.active = z;
        if (in.readInt() != 1) {
            z2 = false;
        }
        this.isEditable = z2;
        String optionsString = in.readString();
        if (!TextUtils.isEmpty(optionsString)) {
            try {
                this.options = JSONArrayInstrumentation.init(optionsString);
            } catch (JSONException e) {
                Log.e(new Throwable("Error reading Custom Field from parcel", e));
            }
        }
        this.fieldType = in.readString();
        this.tempValue = in.readString();
        this.secondaryTempValue = in.readString();
    }

    public static String getSecondaryFieldKey(String fieldDataType, String fieldKey) {
        if (FIELD_DATA_TYPE_TIME_RANGE.equalsIgnoreCase(fieldDataType)) {
            return fieldKey + "_until";
        }
        if (FIELD_DATA_TYPE_DATE_RANGE.equalsIgnoreCase(fieldDataType)) {
            return fieldKey + "_until";
        }
        if (FIELD_DATA_TYPE_MONEY.equalsIgnoreCase(fieldDataType)) {
            return fieldKey + FIELD_SECONDARY_KEY_CURRENCY;
        }
        if ("address".equalsIgnoreCase(fieldDataType)) {
            return fieldKey + FIELD_SECONDARY_KEY_ADDRESS;
        }
        return null;
    }

    public boolean isEditable() {
        return this.isEditable;
    }

    public void setIsEditable(boolean isEditable) {
        this.isEditable = isEditable;
    }

    public boolean isEmpty() {
        return TextUtils.isEmpty(this.tempValue);
    }
}
