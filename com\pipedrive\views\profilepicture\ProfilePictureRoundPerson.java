package com.pipedrive.views.profilepicture;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.pipedrive.application.Session;
import com.pipedrive.interfaces.ContactOrgInterface;
import com.pipedrive.model.Person;
import com.pipedrive.util.StringUtils;
import com.pipedrive.util.networking.ApiUrlBuilder;

public class ProfilePictureRoundPerson extends ProfilePictureRound {
    private static final String PICTURE_DOWNLOAD_URL = "pictures/%d/download";

    public /* bridge */ /* synthetic */ void setTextSize(float f) {
        super.setTextSize(f);
    }

    public ProfilePictureRoundPerson(Context context) {
        super(context);
    }

    public ProfilePictureRoundPerson(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProfilePictureRoundPerson(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public boolean loadPicture(@NonNull Session session, @Nullable Person person) {
        boolean canLoadPicture;
        if (person != null) {
            canLoadPicture = true;
        } else {
            canLoadPicture = false;
        }
        if (!canLoadPicture) {
            return false;
        }
        super.loadPicture(StringUtils.getInitials((ContactOrgInterface) person), getProfilePictureUrlEndpoint(session, person.getPictureId()));
        return true;
    }

    public boolean loadPicture(@NonNull Session session, @Nullable ContactOrgInterface contact) {
        boolean canLoadPicture;
        if (contact != null) {
            canLoadPicture = true;
        } else {
            canLoadPicture = false;
        }
        if (!canLoadPicture) {
            return false;
        }
        super.loadPicture(StringUtils.getInitials(contact), getProfilePictureUrlEndpoint(session, contact.getPictureId()));
        return true;
    }

    @Nullable
    private String getProfilePictureUrlEndpoint(@NonNull Session session, @Nullable Integer profileImageId) {
        if (profileImageId == null || profileImageId.intValue() <= 0) {
            return null;
        }
        ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder(session, String.format(PICTURE_DOWNLOAD_URL, new Object[]{profileImageId}));
        apiUrlBuilder.param("size", "128");
        return apiUrlBuilder.build();
    }
}
