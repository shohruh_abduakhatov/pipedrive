package com.zendesk.sdk.network.impl;

import com.newrelic.agent.android.instrumentation.okhttp3.OkHttp3Instrumentation;
import com.zendesk.sdk.network.Constants;
import com.zendesk.util.StringUtils;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Interceptor.Chain;
import okhttp3.Request.Builder;
import okhttp3.Response;

public class ZendeskRequestInterceptor implements Interceptor {
    private final String oauthId;
    private final String userAgent;

    public ZendeskRequestInterceptor(String oauthId, String userAgent) {
        this.oauthId = oauthId;
        this.userAgent = userAgent;
    }

    public Response intercept(Chain chain) throws IOException {
        Builder requestHeadersBuilder = chain.request().newBuilder().addHeader("User-Agent", this.userAgent).addHeader("Accept", "application/json");
        if (StringUtils.hasLength(this.oauthId)) {
            requestHeadersBuilder.addHeader(Constants.CLIENT_IDENTIFIER_HEADER, this.oauthId);
        }
        return chain.proceed(!(requestHeadersBuilder instanceof Builder) ? requestHeadersBuilder.build() : OkHttp3Instrumentation.build(requestHeadersBuilder));
    }
}
