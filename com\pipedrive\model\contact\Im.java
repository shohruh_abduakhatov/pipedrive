package com.pipedrive.model.contact;

import org.json.JSONObject;

public class Im extends CommunicationMedium {
    public Im(String content, String label, boolean isPrimary) {
        super(content, label, isPrimary);
    }

    public Im(JSONObject jsonObject) {
        super(jsonObject);
    }
}
