package com.pipedrive.dialogs.communicationmediumlist;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.pipedrive.model.contact.CommunicationMedium;
import com.pipedrive.views.viewholder.ViewHolderBuilder;
import com.pipedrive.views.viewholder.communicationmedium.CallRowViewHolder;
import com.pipedrive.views.viewholder.communicationmedium.SendMessageRowViewHolder;
import java.util.List;

class CommunicationMediumListAdapter extends BaseAdapter {
    private final boolean mActionIsCall;
    private final List<? extends CommunicationMedium> mItems;

    public CommunicationMediumListAdapter(List<? extends CommunicationMedium> items, boolean actionIsCall) {
        this.mItems = items;
        this.mActionIsCall = actionIsCall;
    }

    public int getCount() {
        return this.mItems.size();
    }

    public CommunicationMedium getItem(int position) {
        return (CommunicationMedium) this.mItems.get(position);
    }

    public long getItemId(int position) {
        return Integer.valueOf(position).longValue();
    }

    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        boolean viewTagIsWrong;
        CallRowViewHolder viewHolder;
        Object viewTag = convertView == null ? null : convertView.getTag();
        if (viewTag == null || !(viewTag instanceof CallRowViewHolder)) {
            viewTagIsWrong = true;
        } else {
            viewTagIsWrong = false;
        }
        if (viewTagIsWrong) {
            viewHolder = this.mActionIsCall ? new CallRowViewHolder() : new SendMessageRowViewHolder();
            convertView = ViewHolderBuilder.inflateViewAndTagWithViewHolder(parent.getContext(), parent, false, viewHolder);
        } else {
            viewHolder = (CallRowViewHolder) viewTag;
        }
        viewHolder.fill(parent.getContext(), getItem(position));
        return convertView;
    }
}
