package com.pipedrive;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.views.agenda.AgendaView;
import com.pipedrive.views.calendar.CalendarView;

public class CalendarActivity_ViewBinding implements Unbinder {
    private CalendarActivity target;
    private View view2131820590;
    private View view2131820719;

    @UiThread
    public CalendarActivity_ViewBinding(CalendarActivity target) {
        this(target, target.getWindow().getDecorView());
    }

    @UiThread
    public CalendarActivity_ViewBinding(final CalendarActivity target, View source) {
        this.target = target;
        target.monthTitle = (TextView) Utils.findRequiredViewAsType(source, R.id.monthTitle, "field 'monthTitle'", TextView.class);
        target.calendarView = (CalendarView) Utils.findRequiredViewAsType(source, R.id.calendar, "field 'calendarView'", CalendarView.class);
        target.agendaView = (AgendaView) Utils.findRequiredViewAsType(source, R.id.agendaView, "field 'agendaView'", AgendaView.class);
        target.calContainer = Utils.findRequiredView(source, R.id.calendarContainer, "field 'calContainer'");
        target.arrowImage = Utils.findRequiredView(source, R.id.arrow, "field 'arrowImage'");
        View view = Utils.findRequiredView(source, R.id.monthTitleContainer, "method 'onTitleClicked'");
        this.view2131820719 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onTitleClicked();
            }
        });
        view = Utils.findRequiredView(source, R.id.add, "method 'onAddClicked'");
        this.view2131820590 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onAddClicked();
            }
        });
    }

    @CallSuper
    public void unbind() {
        CalendarActivity target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.monthTitle = null;
        target.calendarView = null;
        target.agendaView = null;
        target.calContainer = null;
        target.arrowImage = null;
        this.view2131820719.setOnClickListener(null);
        this.view2131820719 = null;
        this.view2131820590.setOnClickListener(null);
        this.view2131820590 = null;
    }
}
