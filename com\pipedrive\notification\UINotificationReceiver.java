package com.pipedrive.notification;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.pipedrive.activity.ActivityDetailActivity;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.AnalyticsEvent;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.logging.Log;
import com.pipedrive.model.Activity;
import com.pipedrive.store.StoreActivity;

public class UINotificationReceiver extends BroadcastReceiver {
    private static final String ACTION_MARK_ACTIVITY_AS_DONE = "MARK_ACTIVIY_AS_DONE";
    private static final String ACTION_OPEN_ACTIVITY_FROM_NOTIFICATION = "OPEN_ACTIVITY_FROM_NOTIFICATION";
    private static final String ACTION_SHOW_ACTIVITY_NOTIFICATION = "SHOW_ACTIVITY_NOTIFICATION";
    private static final String KEY_ACTIVITY_ID = "ACTIVITY_ID";
    private static final String KEY_SESSION_ID = "SESSION_ID";
    private static final int REQUEST_CODE_MARK_ACTIVITY_AS_DONE = 1;
    private static final int REQUEST_CODE_OPEN_ACTIVITY_FROM_NOTIFICATION = 2;
    private static final int REQUEST_CODE_SHOW_ACTIVITY_NOTIFICATION = 0;

    private static PendingIntent getPendingIntent(@NonNull Session session, long activitySqlId, @NonNull String action, int requestCode) {
        Context context = session.getApplicationContext();
        return PendingIntent.getBroadcast(context, requestCode, new Intent(action, Uri.parse(String.valueOf(activitySqlId))).setComponent(new ComponentName(context, UINotificationReceiver.class)).putExtra(KEY_SESSION_ID, session.getSessionID()).putExtra(KEY_ACTIVITY_ID, activitySqlId), 134217728);
    }

    static PendingIntent getIntentForActivityNotification(@NonNull Session session, long activitySqlId) {
        return getPendingIntent(session, activitySqlId, ACTION_SHOW_ACTIVITY_NOTIFICATION, 0);
    }

    static PendingIntent getIntentForMarkingActivityAsDone(@NonNull Session session, long activitySqlId) {
        return getPendingIntent(session, activitySqlId, ACTION_MARK_ACTIVITY_AS_DONE, 1);
    }

    static PendingIntent getIntentForOpeningActivityFromNotification(@NonNull Session session, long activitySqlId) {
        return getPendingIntent(session, activitySqlId, ACTION_OPEN_ACTIVITY_FROM_NOTIFICATION, 2);
    }

    public void onReceive(@NonNull Context context, @NonNull Intent intent) {
        if (!TextUtils.isEmpty(intent.getAction())) {
            String sessionId = intent.getStringExtra(KEY_SESSION_ID);
            long activitySqlId = intent.getLongExtra(KEY_ACTIVITY_ID, -1);
            Session session = PipedriveApp.getSessionManager().findActiveSession(sessionId);
            if (session != null) {
                Activity activity = (Activity) new ActivitiesDataSource(session).findBySqlId(activitySqlId);
                if (activity != null) {
                    String action = intent.getAction();
                    Object obj = -1;
                    switch (action.hashCode()) {
                        case -1481592743:
                            if (action.equals(ACTION_SHOW_ACTIVITY_NOTIFICATION)) {
                                obj = null;
                                break;
                            }
                            break;
                        case -1066970811:
                            if (action.equals(ACTION_OPEN_ACTIVITY_FROM_NOTIFICATION)) {
                                obj = 2;
                                break;
                            }
                            break;
                        case 1535757389:
                            if (action.equals(ACTION_MARK_ACTIVITY_AS_DONE)) {
                                obj = 1;
                                break;
                            }
                            break;
                    }
                    switch (obj) {
                        case null:
                            UINotificationManager.getInstance().showActivityNotification(session, activity);
                            return;
                        case 1:
                            markActivityAsDone(session, activity);
                            return;
                        case 2:
                            openActivityFromNotification(session, activity);
                            return;
                        default:
                            return;
                    }
                }
            }
        }
    }

    private void openActivityFromNotification(@NonNull Session session, @NonNull Activity activity) {
        try {
            ActivityDetailActivity.getPendingIntentForNotification(session, activity).send();
            Analytics.sendEvent(session.getApplicationContext(), AnalyticsEvent.ACTIVITY_OPENED_FROM_NOTIFICATION);
        } catch (CanceledException e) {
            Log.e(e);
        }
    }

    private void markActivityAsDone(@NonNull Session session, @NonNull Activity activity) {
        activity.setDone(true);
        new StoreActivity(session).update(activity);
        Analytics.sendEvent(session.getApplicationContext(), AnalyticsEvent.ACTIVITY_MARKED_AS_DONE_FROM_NOTIFICATION);
    }
}
