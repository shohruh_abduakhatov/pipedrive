package com.google.android.gms.location.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.location.internal.zzi.zza;

public class zzb extends zzj<zzi> {
    private final String akG;
    protected final zzp<zzi> akH = new zzp<zzi>(this) {
        final /* synthetic */ zzb akI;

        {
            this.akI = r1;
        }

        public void zzavf() {
            this.akI.zzavf();
        }

        public /* synthetic */ IInterface zzavg() throws DeadObjectException {
            return zzbqe();
        }

        public zzi zzbqe() throws DeadObjectException {
            return (zzi) this.akI.zzavg();
        }
    };

    public zzb(Context context, Looper looper, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, String str, zzf com_google_android_gms_common_internal_zzf) {
        super(context, looper, 23, com_google_android_gms_common_internal_zzf, connectionCallbacks, onConnectionFailedListener);
        this.akG = str;
    }

    protected Bundle zzahv() {
        Bundle bundle = new Bundle();
        bundle.putString("client_name", this.akG);
        return bundle;
    }

    protected /* synthetic */ IInterface zzh(IBinder iBinder) {
        return zzhd(iBinder);
    }

    protected zzi zzhd(IBinder iBinder) {
        return zza.zzhg(iBinder);
    }

    protected String zzjx() {
        return "com.google.android.location.internal.GoogleLocationManagerService.START";
    }

    protected String zzjy() {
        return "com.google.android.gms.location.internal.IGoogleLocationManagerService";
    }
}
