package com.pipedrive.util;

import android.view.View;
import android.view.View.OnClickListener;
import com.pipedrive.util.UndoBarController.UndoBarRequest;

class UndoBarController$UndoBarRequest$1 implements OnClickListener {
    final /* synthetic */ UndoBarRequest this$1;
    final /* synthetic */ UndoBarController val$this$0;

    UndoBarController$UndoBarRequest$1(UndoBarRequest this$1, UndoBarController undoBarController) {
        this.this$1 = this$1;
        this.val$this$0 = undoBarController;
    }

    public void onClick(View view) {
        UndoBarRequest.access$000(this.this$1).setOnClickListener(null);
        T token = UndoBarRequest.access$100(this.this$1);
        UndoBarRequest.access$200(this.this$1, false);
        if (UndoBarRequest.access$300(this.this$1) != null) {
            UndoBarRequest.access$300(this.this$1).onUndo(token, UndoBarRequest.access$400(this.this$1));
        }
        UndoBarRequest.access$502(this.this$1, true);
    }
}
