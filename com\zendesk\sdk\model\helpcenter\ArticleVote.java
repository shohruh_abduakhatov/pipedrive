package com.zendesk.sdk.model.helpcenter;

import android.support.annotation.Nullable;
import java.util.Date;

public class ArticleVote {
    private Date createdAt;
    private Long id;
    private Long itemId;
    private String itemType;
    private Date updatedAt;
    private String url;
    private Long userId;
    private Integer value;

    @Nullable
    public Long getId() {
        return this.id;
    }

    @Nullable
    public String getUrl() {
        return this.url;
    }

    @Nullable
    public Long getUserId() {
        return this.userId;
    }

    @Nullable
    public Integer getValue() {
        return this.value;
    }

    @Nullable
    public Long getItemId() {
        return this.itemId;
    }

    @Nullable
    public String getItemType() {
        return this.itemType;
    }

    @Nullable
    public Date getCreatedAt() {
        return this.createdAt == null ? null : new Date(this.createdAt.getTime());
    }

    @Nullable
    public Date getUpdatedAt() {
        return this.updatedAt == null ? null : new Date(this.updatedAt.getTime());
    }
}
