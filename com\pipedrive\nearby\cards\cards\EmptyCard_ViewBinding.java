package com.pipedrive.nearby.cards.cards;

import android.content.Context;
import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import com.pipedrive.R;

public class EmptyCard_ViewBinding implements Unbinder {
    @UiThread
    public EmptyCard_ViewBinding(EmptyCard target) {
        this(target, target.getContext());
    }

    @UiThread
    @Deprecated
    public EmptyCard_ViewBinding(EmptyCard target, View source) {
        this(target, source.getContext());
    }

    @UiThread
    public EmptyCard_ViewBinding(EmptyCard target, Context context) {
        target.defaultElevation = context.getResources().getDimensionPixelSize(R.dimen.dimen1);
    }

    @CallSuper
    public void unbind() {
    }
}
