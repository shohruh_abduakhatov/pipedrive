package com.pipedrive.sync.recents;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncInfo;
import android.content.SyncResult;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.sync.SyncManager;
import com.pipedrive.util.networking.ConnectionUtil;

public class RecentsSyncAdapter extends AbstractThreadedSyncAdapter {
    private static final String EXTRA_MANUAL = "force";
    private static final String KEY_SESSION_ID = "SESSION_ID";
    private static final String TAG = RecentsSyncAdapter.class.getSimpleName();

    public RecentsSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
    }

    public static void requestSyncAdapterToSync(@NonNull Session session) {
        if (!syncInProgress()) {
            Account[] accounts = AccountManager.get(session.getApplicationContext()).getAccountsByType("com.pipedrive.account");
            Bundle requestSyncBundle = new Bundle();
            requestSyncBundle.putString(KEY_SESSION_ID, session.getSessionID());
            requestSyncBundle.putBoolean(EXTRA_MANUAL, true);
            requestSyncBundle.putBoolean("expedited", true);
            for (Account account : accounts) {
                ContentResolver.requestSync(account, "com.pipedrive.contentproviders.recentsprovider", requestSyncBundle);
            }
        }
    }

    private static boolean syncInProgress() {
        for (SyncInfo syncInfo : ContentResolver.getCurrentSyncs()) {
            if (TextUtils.equals(syncInfo.authority, "com.pipedrive.contentproviders.recentsprovider")) {
                Log.d(TAG, String.format("Sync for %s is already in progress. Not requesting another one.", new Object[]{"com.pipedrive.contentproviders.recentsprovider"}));
                return true;
            }
        }
        return false;
    }

    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        boolean cannotMakeSync;
        boolean extrasDoNotHaveSessionInfo;
        if (extras == null || extras.getString(KEY_SESSION_ID) == null) {
            extrasDoNotHaveSessionInfo = true;
        } else {
            extrasDoNotHaveSessionInfo = false;
        }
        if (!ConnectionUtil.isConnected(getContext()) || extrasDoNotHaveSessionInfo) {
            cannotMakeSync = true;
        } else {
            cannotMakeSync = false;
        }
        if (!cannotMakeSync) {
            Session session = PipedriveApp.getSessionManager().findActiveSession(extras.getString(KEY_SESSION_ID));
            if (session != null) {
                boolean isRequestFromPeriodicSyncBySystem;
                com.pipedrive.logging.Log.setupCrashLoggersMetadata(session);
                if (extras.getBoolean(EXTRA_MANUAL, false)) {
                    isRequestFromPeriodicSyncBySystem = false;
                } else {
                    isRequestFromPeriodicSyncBySystem = true;
                }
                if (isRequestFromPeriodicSyncBySystem) {
                    SyncManager.getInstance().syncRequestedBySyncAdapterAndSystem(session);
                    return;
                } else {
                    SyncManager.getInstance().syncRequestedBySyncAdapter(session);
                    return;
                }
            }
            LogJourno.reportEvent(EVENT.Recents_recentsInitiatedWithoutSession);
            com.pipedrive.logging.Log.e(new Throwable(String.format("Recents onPerformSync called without active session. sessionId: %s", new Object[]{sessionId})));
        }
    }
}
