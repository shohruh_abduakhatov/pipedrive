package com.pipedrive.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.pipedrive.R;
import java.util.ArrayList;
import java.util.List;

public class ImageAndTextSpinnerAdapter extends ArrayAdapter<String> {
    @NonNull
    private LayoutInflater mLayoutInflater;
    private List<? extends ImageAndTextSpinnerAdapterData> spinnerData = new ArrayList();

    public interface ImageAndTextSpinnerAdapterData {
        int getImageResourceId();

        String getLabel();
    }

    public static class Entry implements ImageAndTextSpinnerAdapterData {
        private Integer mImageResourceIds = Integer.valueOf(0);
        private String mLabel;

        public Entry(String label, Integer imageResourceIds) {
            this.mLabel = label;
            this.mImageResourceIds = imageResourceIds;
        }

        public String getLabel() {
            return this.mLabel;
        }

        public int getImageResourceId() {
            return this.mImageResourceIds.intValue();
        }
    }

    public ImageAndTextSpinnerAdapter(Context context, int textViewResourceId, List<? extends ImageAndTextSpinnerAdapterData> spinnerData) {
        super(context, textViewResourceId);
        this.mLayoutInflater = LayoutInflater.from(context);
        this.spinnerData = spinnerData;
    }

    public int getCount() {
        return this.spinnerData.size();
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        int i = 0;
        View row = this.mLayoutInflater.inflate(R.layout.row_activity_type, parent, false);
        ImageAndTextSpinnerAdapterData rowData = (ImageAndTextSpinnerAdapterData) this.spinnerData.get(position);
        ((TextView) row.findViewById(R.id.labelActivityType)).setText(rowData.getLabel());
        ImageView icon = (ImageView) row.findViewById(R.id.image);
        int imageResId = getDropdownImageResId(rowData);
        if (imageResId > 0) {
            icon.setImageResource(imageResId);
        }
        if (imageResId <= 0) {
            i = 8;
        }
        icon.setVisibility(i);
        return row;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        int i = 0;
        View row = this.mLayoutInflater.inflate(R.layout.row_activity_type_selected_view, parent, false);
        ImageView icon = (ImageView) row.findViewById(R.id.activityTypeImage);
        int imageResId = getImageResId((ImageAndTextSpinnerAdapterData) this.spinnerData.get(position));
        if (imageResId > 0) {
            icon.setImageResource(imageResId);
        }
        if (imageResId <= 0) {
            i = 8;
        }
        icon.setVisibility(i);
        return row;
    }

    public int getDropdownImageResId(ImageAndTextSpinnerAdapterData data) {
        return data.getImageResourceId();
    }

    public int getImageResId(ImageAndTextSpinnerAdapterData data) {
        return data.getImageResourceId();
    }
}
