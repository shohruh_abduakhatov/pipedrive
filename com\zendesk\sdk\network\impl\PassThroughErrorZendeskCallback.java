package com.zendesk.sdk.network.impl;

import com.zendesk.service.ErrorResponse;
import com.zendesk.service.ZendeskCallback;

abstract class PassThroughErrorZendeskCallback<E> extends ZendeskCallback<E> {
    private final ZendeskCallback callback;

    public abstract void onSuccess(E e);

    PassThroughErrorZendeskCallback(ZendeskCallback callback) {
        this.callback = callback;
    }

    public void onError(ErrorResponse errorResponse) {
        if (this.callback != null) {
            this.callback.onError(errorResponse);
        }
    }
}
