package com.pipedrive.tasks;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.pipedrive.model.Currency;

public class CurrencyTaskResult implements Parcelable {
    public static final int CALCULATING = Integer.MIN_VALUE;
    public static final Creator CREATOR = new Creator() {
        public CurrencyTaskResult createFromParcel(Parcel in) {
            return new CurrencyTaskResult(in);
        }

        public CurrencyTaskResult[] newArray(int size) {
            return new CurrencyTaskResult[size];
        }
    };
    public static final double UNKNOWN_VALUE = Double.MIN_VALUE;
    private int dealsGroupId;
    private double dealsGroupValue = UNKNOWN_VALUE;
    public Currency defaultCurrency = null;
    public int numberOfDealsCalculated = Integer.MIN_VALUE;

    public CurrencyTaskResult(Parcel in) {
        readFromParcel(in);
    }

    public double getDealsGroupValue() {
        return this.dealsGroupValue;
    }

    public void setDealsGroupValue(double dealsGroupValue) {
        this.dealsGroupValue = dealsGroupValue;
    }

    public int getNumberOfDealsCalculated() {
        return this.numberOfDealsCalculated;
    }

    public void setNumberOfDealsCalculated(int numberOfDealsCalculated) {
        this.numberOfDealsCalculated = numberOfDealsCalculated;
    }

    public Currency getDefaultCurrency() {
        return this.defaultCurrency;
    }

    public void setDefaultCurrency(Currency defaultCurrency) {
        this.defaultCurrency = defaultCurrency;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.dealsGroupValue);
        dest.writeInt(this.numberOfDealsCalculated);
        dest.writeParcelable(this.defaultCurrency, flags);
        dest.writeInt(this.dealsGroupId);
    }

    private void readFromParcel(Parcel in) {
        this.dealsGroupValue = in.readDouble();
        this.numberOfDealsCalculated = in.readInt();
        this.defaultCurrency = (Currency) in.readParcelable(Currency.class.getClassLoader());
        this.dealsGroupId = in.readInt();
    }

    public int getDealsGroupId() {
        return this.dealsGroupId;
    }

    public void setDealsGroupId(int dealsGroupId) {
        this.dealsGroupId = dealsGroupId;
    }
}
