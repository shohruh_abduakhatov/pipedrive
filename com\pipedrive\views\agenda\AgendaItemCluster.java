package com.pipedrive.views.agenda;

import android.support.annotation.NonNull;
import java.util.ArrayList;
import java.util.List;

class AgendaItemCluster {
    private int columnCount;
    @NonNull
    private List<AgendaItem> items = new ArrayList();

    AgendaItemCluster() {
    }

    @NonNull
    public List<AgendaItem> getItems() {
        return this.items;
    }

    public int getItemCount() {
        return this.items.size();
    }

    @NonNull
    public AgendaItem getItem(int i) {
        return (AgendaItem) this.items.get(i);
    }

    public void addItem(@NonNull AgendaItem item) {
        this.items.add(item);
    }

    public void setItemColumn(@NonNull AgendaItem item, int column) {
        item.setColumn(column);
        this.columnCount = Math.max(column, this.columnCount);
    }

    public int getColumnCount() {
        return this.columnCount;
    }
}
