package com.pipedrive.util.networking.client;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.newrelic.agent.android.instrumentation.okhttp3.OkHttp3Instrumentation;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Interceptor.Chain;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.Response;

abstract class HeaderInterceptor implements Interceptor {
    @NonNull
    abstract String getHeaderName();

    @Nullable
    abstract String getHeaderValue(@NonNull Request request);

    HeaderInterceptor() {
    }

    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();
        Builder newRequestBuilder = originalRequest.newBuilder();
        String headerValue = getHeaderValue(originalRequest);
        if (headerValue != null) {
            newRequestBuilder.addHeader(getHeaderName(), headerValue);
        }
        return chain.proceed(!(newRequestBuilder instanceof Builder) ? newRequestBuilder.build() : OkHttp3Instrumentation.build(newRequestBuilder));
    }
}
