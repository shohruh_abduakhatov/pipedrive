package com.google.android.gms.wearable.internal;

import android.content.IntentFilter;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzrr;
import com.google.android.gms.wearable.Channel;
import com.google.android.gms.wearable.ChannelApi;
import com.google.android.gms.wearable.ChannelApi.ChannelListener;
import com.google.android.gms.wearable.ChannelApi.OpenChannelResult;

public final class zzl implements ChannelApi {

    class AnonymousClass2 implements zza<ChannelListener> {
        final /* synthetic */ IntentFilter[] aTc;

        AnonymousClass2(IntentFilter[] intentFilterArr) {
            this.aTc = intentFilterArr;
        }

        public void zza(zzbp com_google_android_gms_wearable_internal_zzbp, com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, ChannelListener channelListener, zzrr<ChannelListener> com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_ChannelApi_ChannelListener) throws RemoteException {
            com_google_android_gms_wearable_internal_zzbp.zza((com.google.android.gms.internal.zzqo.zzb) com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, channelListener, (zzrr) com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_ChannelApi_ChannelListener, null, this.aTc);
        }
    }

    static final class zza implements OpenChannelResult {
        private final Channel aTl;
        private final Status hv;

        zza(Status status, Channel channel) {
            this.hv = (Status) zzaa.zzy(status);
            this.aTl = channel;
        }

        public Channel getChannel() {
            return this.aTl;
        }

        public Status getStatus() {
            return this.hv;
        }
    }

    static final class zzb extends zzi<Status> {
        private ChannelListener aTm;
        private final String hN;

        zzb(GoogleApiClient googleApiClient, ChannelListener channelListener, String str) {
            super(googleApiClient);
            this.aTm = (ChannelListener) zzaa.zzy(channelListener);
            this.hN = str;
        }

        protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
            com_google_android_gms_wearable_internal_zzbp.zza((com.google.android.gms.internal.zzqo.zzb) this, this.aTm, this.hN);
            this.aTm = null;
        }

        public Status zzb(Status status) {
            this.aTm = null;
            return status;
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    private static zza<ChannelListener> zza(IntentFilter[] intentFilterArr) {
        return new AnonymousClass2(intentFilterArr);
    }

    public PendingResult<Status> addListener(GoogleApiClient googleApiClient, ChannelListener channelListener) {
        zzaa.zzb((Object) googleApiClient, (Object) "client is null");
        zzaa.zzb((Object) channelListener, (Object) "listener is null");
        return zzb.zza(googleApiClient, zza(new IntentFilter[]{zzbn.zzrp(ChannelApi.ACTION_CHANNEL_EVENT)}), channelListener);
    }

    public PendingResult<OpenChannelResult> openChannel(GoogleApiClient googleApiClient, final String str, final String str2) {
        zzaa.zzb((Object) googleApiClient, (Object) "client is null");
        zzaa.zzb((Object) str, (Object) "nodeId is null");
        zzaa.zzb((Object) str2, (Object) "path is null");
        return googleApiClient.zza(new zzi<OpenChannelResult>(this, googleApiClient) {
            final /* synthetic */ zzl aTk;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zze(this, str, str2);
            }

            public /* synthetic */ Result zzc(Status status) {
                return zzer(status);
            }

            public OpenChannelResult zzer(Status status) {
                return new zza(status, null);
            }
        });
    }

    public PendingResult<Status> removeListener(GoogleApiClient googleApiClient, ChannelListener channelListener) {
        zzaa.zzb((Object) googleApiClient, (Object) "client is null");
        zzaa.zzb((Object) channelListener, (Object) "listener is null");
        return googleApiClient.zza(new zzb(googleApiClient, channelListener, null));
    }
}
