package com.pipedrive.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.pipedrive.R;
import com.pipedrive.activity.ActivityDetailActivity;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.customfields.fragments.OrganizationCustomFieldListFragment;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.deal.DealEditActivity;
import com.pipedrive.flow.fragments.OrganizationFlowFragment;
import com.pipedrive.model.FlowFile;
import com.pipedrive.model.Organization;
import com.pipedrive.note.NoteCreateActivity;
import com.pipedrive.organization.edit.OrganizationUpdateActivity;
import com.pipedrive.store.StoreFlowFile;
import com.pipedrive.util.PipedriveUtil;
import com.pipedrive.util.StringUtils;
import com.pipedrive.util.camera.CameraHelper;
import com.pipedrive.util.camera.CameraHelper.OnCameraListener;
import com.pipedrive.util.filepicker.DocumentPickerHelper;
import com.pipedrive.util.filepicker.DocumentPickerHelper.OnDocumentPickerListener;
import com.pipedrive.util.flowfiles.FlowFileUploadNotificationManager;
import com.pipedrive.util.flowfiles.FlowFileUploadNotificationManager.Notification;
import com.pipedrive.views.fab.FabMenuClickListener;
import com.pipedrive.views.fab.FabWithOverlayMenu;
import java.util.Arrays;
import java.util.List;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

public final class OrganizationDetailFragment extends BaseFragment implements FabMenuClickListener {
    private static final String KEY_ORGANIZATION_SQL_ID = "ORGANIZATION_SQL_ID";
    public static final String TAG = OrganizationDetailFragment.class.getSimpleName();
    @BindView(2131820923)
    TextView mAddressTextView;
    @Nullable
    private CameraHelper mCameraHelper;
    @Nullable
    private OrganizationCustomFieldListFragment mCustomFieldsListFragment;
    @Nullable
    private DocumentPickerHelper mDocumentPickerHelper;
    @BindView(2131820824)
    FabWithOverlayMenu mFabWithOverlayMenu;
    @Nullable
    private Subscription mFlowFileUploadNotificationManagerSubscription;
    @BindView(2131820922)
    TextView mNameTextView;
    private Organization mOrganization;
    @Nullable
    private OrganizationFlowFragment mOrganizationFlowFragment;
    @Nullable
    private SetupProgressBarCallback mSetupProgressBarCallback;
    @BindView(2131820925)
    TabLayout mTabLayout;
    @Nullable
    private Unbinder mUnbinder;
    @BindView(2131820736)
    ViewPager mViewPager;

    public interface SetupProgressBarCallback {
        void setupProgressBarWithToolbar(Toolbar toolbar);
    }

    private static class OrganizationDetailsPagerAdapter extends FragmentStatePagerAdapter {
        @NonNull
        private final List<BaseFragment> fragments;
        @NonNull
        private final Session session;

        public OrganizationDetailsPagerAdapter(@NonNull Session session, @NonNull FragmentManager fm, @NonNull List<BaseFragment> fragments) {
            super(fm);
            this.session = session;
            this.fragments = fragments;
        }

        public Fragment getItem(int position) {
            return (Fragment) this.fragments.get(position);
        }

        public int getCount() {
            return this.fragments.size();
        }

        public CharSequence getPageTitle(int position) {
            String title;
            switch (position) {
                case 0:
                    title = this.session.getApplicationContext().getString(R.string.tab_title_contact_general);
                    break;
                case 1:
                    title = this.session.getApplicationContext().getString(R.string.tab_title_contact_flow);
                    break;
                case 2:
                    title = this.session.getApplicationContext().getString(R.string.tab_title_contact_deals);
                    break;
                case 3:
                    title = this.session.getApplicationContext().getString(R.string.tab_title_contact_people);
                    break;
                default:
                    title = "";
                    break;
            }
            return title.toUpperCase();
        }
    }

    public static OrganizationDetailFragment newInstance(@NonNull Long organizationSqlId) {
        Bundle args = new Bundle();
        args.putSerializable("ORGANIZATION_SQL_ID", organizationSqlId);
        OrganizationDetailFragment fragment = new OrganizationDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mCameraHelper = new CameraHelper();
        this.mCameraHelper.onCreate(savedInstanceState);
        this.mDocumentPickerHelper = new DocumentPickerHelper();
        Session activeSession = PipedriveApp.getActiveSession();
        Long organizationSqlId = (Long) getArguments().getSerializable("ORGANIZATION_SQL_ID");
        if (activeSession == null || organizationSqlId == null) {
            getActivity().finish();
            return;
        }
        this.mOrganization = (Organization) new OrganizationsDataSource(activeSession.getDatabase()).findBySqlId(organizationSqlId.longValue());
        if (this.mOrganization == null) {
            getActivity().finish();
        } else {
            setHasOptionsMenu(true);
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.mSetupProgressBarCallback = (SetupProgressBarCallback) context;
        } catch (ClassCastException e) {
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        if (this.mCameraHelper != null) {
            this.mCameraHelper.onSaveInstanceState(outState);
        }
        super.onSaveInstanceState(outState);
    }

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_organization_details, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mUnbinder = ButterKnife.bind((Object) this, view);
        Session activeSession = PipedriveApp.getActiveSession();
        if (activeSession != null) {
            ViewPager viewPager = this.mViewPager;
            FragmentManager childFragmentManager = getChildFragmentManager();
            BaseFragment[] baseFragmentArr = new BaseFragment[4];
            OrganizationCustomFieldListFragment newInstance = OrganizationCustomFieldListFragment.newInstance(Long.valueOf(this.mOrganization.getSqlId()));
            this.mCustomFieldsListFragment = newInstance;
            baseFragmentArr[0] = newInstance;
            OrganizationFlowFragment newInstance2 = OrganizationFlowFragment.newInstance(Long.valueOf(this.mOrganization.getSqlId()));
            this.mOrganizationFlowFragment = newInstance2;
            baseFragmentArr[1] = newInstance2;
            baseFragmentArr[2] = DealsListFragment.newInstanceForOrg(this.mOrganization.getSqlId(), false, true);
            baseFragmentArr[3] = OrganizationPeopleListFragment.newInstance(this.mOrganization.getSqlId(), true);
            viewPager.setAdapter(new OrganizationDetailsPagerAdapter(activeSession, childFragmentManager, Arrays.asList(baseFragmentArr)));
            this.mTabLayout.setupWithViewPager(this.mViewPager);
        }
        this.mFabWithOverlayMenu.setMenu(R.menu.menu_fab_org, R.id.action_add_note);
        this.mFabWithOverlayMenu.setOnMenuClickListener(this);
        if (this.mSetupProgressBarCallback != null) {
            this.mSetupProgressBarCallback.setupProgressBarWithToolbar(this.mToolbar);
        }
        setupActionBar();
    }

    private void setupActionBar() {
        if ((getContext() instanceof AppCompatActivity) && ((AppCompatActivity) getContext()).getSupportActionBar() != null) {
            ((AppCompatActivity) getContext()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @OnClick({2131820923})
    void onAddressClicked() {
        if (this.mOrganization != null && !TextUtils.isEmpty(this.mOrganization.getAddress())) {
            PipedriveUtil.openMapForAddress(getActivity(), this.mOrganization.getAddress());
        }
    }

    public void onDestroyView() {
        if (this.mUnbinder != null) {
            this.mUnbinder.unbind();
        }
        super.onDestroyView();
    }

    public void onResume() {
        super.onResume();
        if (!PipedriveApp.getSessionManager().hasActiveSession()) {
            getActivity().finish();
        }
        final Session activeSession = PipedriveApp.getActiveSession();
        this.mFlowFileUploadNotificationManagerSubscription = FlowFileUploadNotificationManager.INSTANCE.getFlowFileUploadNotificationBus().observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Notification>() {
            public void call(Notification notification) {
                OrganizationDetailFragment.this.refreshFlowView(activeSession);
            }
        });
        this.mNameTextView.setText(this.mOrganization.getName());
        this.mAddressTextView.setText(this.mOrganization.getAddress());
        this.mAddressTextView.setVisibility(StringUtils.isTrimmedAndEmpty(this.mOrganization.getAddress()) ? 8 : 0);
        refreshFlowView(activeSession);
        if (this.mCustomFieldsListFragment != null) {
            this.mCustomFieldsListFragment.refreshContent(activeSession);
        }
    }

    public void onPause() {
        if (this.mFlowFileUploadNotificationManagerSubscription != null) {
            this.mFlowFileUploadNotificationManagerSubscription.unsubscribe();
            this.mFlowFileUploadNotificationManagerSubscription = null;
        }
        super.onPause();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_organization_details, menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_edit:
                onEditMenuItemClicked();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onEditMenuItemClicked() {
        OrganizationUpdateActivity.startActivityForResult(getActivity(), this.mOrganization, 0);
    }

    public void onMenuItemClicked(int itemId) {
        switch (itemId) {
            case R.id.action_take_photo:
                takePhoto();
                return;
            case R.id.action_upload_file:
                uploadDocument();
                return;
            case R.id.action_add_activity:
                ActivityDetailActivity.startActivityForOrganization(getActivity(), this.mOrganization);
                return;
            case R.id.action_add_note:
                NoteCreateActivity.createNoteForOrganization(getActivity(), this.mOrganization);
                return;
            case R.id.action_add_deal:
                DealEditActivity.startActivityForAddingDealToOrganization(getActivity(), this.mOrganization);
                return;
            default:
                return;
        }
    }

    private void takePhoto() {
        if (!(this.mCameraHelper == null)) {
            this.mCameraHelper.snapPhoto((Fragment) this);
        }
    }

    private void uploadDocument() {
        if (!(this.mDocumentPickerHelper == null)) {
            this.mDocumentPickerHelper.pickDocument((Fragment) this);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        boolean willNotBeAbleToRelateFileToOrganization;
        final Session activeSession = PipedriveApp.getActiveSession();
        Context context = getActivity();
        if (this.mCameraHelper == null || this.mOrganization == null || activeSession == null || context == null) {
            willNotBeAbleToRelateFileToOrganization = true;
        } else {
            willNotBeAbleToRelateFileToOrganization = false;
        }
        if (willNotBeAbleToRelateFileToOrganization) {
            super.onActivityResult(requestCode, resultCode, data);
            return;
        }
        try {
            boolean ableToPassResultToDocumentPickerHelper;
            this.mCameraHelper.onActivityResult(context, requestCode, resultCode, data, new OnCameraListener() {
                public boolean snapshot(@NonNull Uri snapshot, @Nullable String snapshotFileName) {
                    FlowFile flowFileForDeal = new FlowFile();
                    flowFileForDeal.setOrganization(OrganizationDetailFragment.this.mOrganization);
                    flowFileForDeal.setUrl(snapshot.toString());
                    flowFileForDeal.setName(snapshotFileName);
                    boolean storeIsSuccess = new StoreFlowFile(activeSession).create(flowFileForDeal);
                    if (storeIsSuccess) {
                        OrganizationDetailFragment.this.refreshFlowView(activeSession);
                    }
                    return storeIsSuccess;
                }
            });
            if (this.mDocumentPickerHelper != null) {
                ableToPassResultToDocumentPickerHelper = true;
            } else {
                ableToPassResultToDocumentPickerHelper = false;
            }
            if (ableToPassResultToDocumentPickerHelper) {
                this.mDocumentPickerHelper.onActivityResult(context, requestCode, resultCode, data, new OnDocumentPickerListener() {
                    public boolean documentPicked(@NonNull Uri document, @Nullable String documentName, @Nullable Long fileSizeInBytes, @Nullable String fileType) {
                        FlowFile flowFileForDeal = new FlowFile();
                        flowFileForDeal.setOrganization(OrganizationDetailFragment.this.mOrganization);
                        flowFileForDeal.setUrl(document.toString());
                        flowFileForDeal.setName(documentName);
                        flowFileForDeal.setFileSize(fileSizeInBytes);
                        flowFileForDeal.setFileType(fileType);
                        boolean storeIsSuccess = new StoreFlowFile(activeSession).create(flowFileForDeal);
                        if (storeIsSuccess) {
                            OrganizationDetailFragment.this.refreshFlowView(activeSession);
                        }
                        return storeIsSuccess;
                    }
                });
            }
            super.onActivityResult(requestCode, resultCode, data);
        } catch (Throwable th) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public boolean consumeBackPress() {
        return this.mFabWithOverlayMenu.consumeBackPress();
    }

    public void refreshFlowView(@NonNull Session session) {
        if (this.mOrganizationFlowFragment != null) {
            this.mOrganizationFlowFragment.refreshContent(session);
        }
    }
}
