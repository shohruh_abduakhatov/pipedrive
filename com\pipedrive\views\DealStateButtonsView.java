package com.pipedrive.views;

import android.content.Context;
import android.support.annotation.MenuRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.pipedrive.R;
import com.pipedrive.model.Deal;
import com.pipedrive.model.DealStatus;

public class DealStateButtonsView extends FrameLayout {
    @BindView(2131821152)
    protected View mButtons;
    @BindView(2131821150)
    protected View mDeletedStateButton;
    @BindView(2131821149)
    protected View mLostStateButton;
    @Nullable
    private OnClickListener mOnClickListener;
    @NonNull
    private OnMenuItemClickListener mOnMenuItemClickListener;
    @BindView(2131821148)
    protected View mStateButtons;
    @BindView(2131821151)
    protected View mWonStateButton;

    static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$com$pipedrive$model$DealStatus = new int[DealStatus.values().length];

        static {
            try {
                $SwitchMap$com$pipedrive$model$DealStatus[DealStatus.LOST.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$pipedrive$model$DealStatus[DealStatus.DELETED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$pipedrive$model$DealStatus[DealStatus.WON.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    public interface OnClickListener {
        void onDealLost();

        void onDealReopened();

        void onDealWon();
    }

    public DealStateButtonsView(Context context) {
        this(context, null);
    }

    public DealStateButtonsView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DealStateButtonsView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mOnMenuItemClickListener = new OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_deal_reopen:
                        if (DealStateButtonsView.this.mOnClickListener == null) {
                            return true;
                        }
                        DealStateButtonsView.this.mOnClickListener.onDealReopened();
                        return true;
                    case R.id.menu_deal_won:
                        if (DealStateButtonsView.this.mOnClickListener == null) {
                            return true;
                        }
                        DealStateButtonsView.this.mOnClickListener.onDealWon();
                        return true;
                    case R.id.menu_deal_lost:
                        if (DealStateButtonsView.this.mOnClickListener == null) {
                            return true;
                        }
                        DealStateButtonsView.this.mOnClickListener.onDealLost();
                        return true;
                    default:
                        return false;
                }
            }
        };
        LayoutInflater.from(context).inflate(R.layout.view_deal_state_buttons, this, true);
        ButterKnife.bind(this);
    }

    public void setup(@NonNull Deal deal, @Nullable OnClickListener onClickListener) {
        this.mOnClickListener = onClickListener;
        DealStatus dealStatus = deal.getStatus();
        if (dealStatus == DealStatus.OPEN) {
            this.mStateButtons.setVisibility(8);
            this.mButtons.setVisibility(0);
            return;
        }
        this.mButtons.setVisibility(8);
        this.mStateButtons.setVisibility(0);
        this.mWonStateButton.setVisibility(8);
        this.mLostStateButton.setVisibility(8);
        this.mDeletedStateButton.setVisibility(8);
        switch (AnonymousClass2.$SwitchMap$com$pipedrive$model$DealStatus[dealStatus.ordinal()]) {
            case 1:
                this.mLostStateButton.setVisibility(0);
                return;
            case 2:
                this.mDeletedStateButton.setVisibility(0);
                return;
            case 3:
                this.mWonStateButton.setVisibility(0);
                return;
            default:
                return;
        }
    }

    @OnClick({2131821151})
    protected void onWonStateButtonClicked() {
        showPopupMenu(this.mWonStateButton, R.menu.menu_deal_details_won);
    }

    @OnClick({2131821149})
    protected void onLostStateButtonClicked() {
        showPopupMenu(this.mLostStateButton, R.menu.menu_deal_details_lost);
    }

    @OnClick({2131821150})
    protected void onDeletedStateButtonClicked() {
        showPopupMenu(this.mDeletedStateButton, R.menu.menu_deal_details_deleted);
    }

    private void showPopupMenu(@NonNull View anchorView, @MenuRes int menuResId) {
        PopupMenu popupMenu = new PopupMenu(getContext(), anchorView);
        popupMenu.setOnMenuItemClickListener(this.mOnMenuItemClickListener);
        popupMenu.getMenuInflater().inflate(menuResId, popupMenu.getMenu());
        popupMenu.show();
    }

    @OnClick({2131821153})
    protected void onWonButtonClicked() {
        if (this.mOnClickListener != null) {
            this.mOnClickListener.onDealWon();
        }
    }

    @OnClick({2131821154})
    protected void onLostButtonClicked() {
        if (this.mOnClickListener != null) {
            this.mOnClickListener.onDealLost();
        }
    }
}
