package com.pipedrive.views.assignment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.views.assignment.SpinnerWithUserProfilePicture.OnItemSelectedListener;

public class OwnerView extends SpinnerWithUserProfilePicture {
    public /* bridge */ /* synthetic */ void setOnItemSelectedListener(@Nullable OnItemSelectedListener onItemSelectedListener) {
        super.setOnItemSelectedListener(onItemSelectedListener);
    }

    public OwnerView(Context context) {
        super(context);
    }

    public OwnerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OwnerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected int getSpinnerTitleResId() {
        return R.string.owner;
    }

    public void loadOwnership(@NonNull Session session, @Nullable Person person, @Nullable OnItemSelectedListener onItemSelectedListener) {
        loadAssignments(session, person == null ? session.getAuthenticatedUserID() : (long) person.getOwnerPipedriveId(), onItemSelectedListener);
    }

    public void loadOwnership(@NonNull Session session, @Nullable Organization organization, @Nullable OnItemSelectedListener onItemSelectedListener) {
        loadAssignments(session, organization == null ? session.getAuthenticatedUserID() : (long) organization.getOwnerPipedriveId(), onItemSelectedListener);
    }
}
