package com.pipedrive.util.time;

import android.support.annotation.NonNull;
import java.util.TimeZone;

class DefaultTimeProvider extends TimeProvider {
    DefaultTimeProvider() {
    }

    @NonNull
    public Long getCurrentTimeMillis() {
        return Long.valueOf(System.currentTimeMillis());
    }

    @NonNull
    public TimeZone getDefaultTimeZone() {
        return TimeZone.getDefault();
    }
}
