package com.zendesk.sdk.model.helpcenter.help;

import android.support.annotation.NonNull;
import com.zendesk.util.CollectionUtils;
import java.util.List;

public class HelpResponse {
    private List<ArticleItem> articles;
    private List<CategoryItem> categories;
    private List<SectionItem> sections;

    @NonNull
    public List<CategoryItem> getCategories() {
        return CollectionUtils.copyOf(this.categories);
    }

    @NonNull
    public List<SectionItem> getSections() {
        return CollectionUtils.copyOf(this.sections);
    }

    @NonNull
    public List<ArticleItem> getArticles() {
        return CollectionUtils.copyOf(this.articles);
    }
}
