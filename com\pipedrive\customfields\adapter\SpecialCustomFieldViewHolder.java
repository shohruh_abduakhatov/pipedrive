package com.pipedrive.customfields.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.widget.FrameLayout;
import com.pipedrive.application.Session;
import com.pipedrive.customfields.views.CustomFieldListView.OnItemClickListener;

abstract class SpecialCustomFieldViewHolder extends ViewHolder {
    @NonNull
    final Long itemSqlId;
    @NonNull
    final FrameLayout rootContainer = ((FrameLayout) this.itemView);

    abstract void bind(@NonNull Session session, @Nullable OnItemClickListener onItemClickListener);

    SpecialCustomFieldViewHolder(@NonNull Context context, @NonNull Long itemSqlId) {
        super(new FrameLayout(context));
        this.itemSqlId = itemSqlId;
    }
}
