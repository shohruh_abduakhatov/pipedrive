package com.pipedrive.products;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.CurrenciesDataSource;
import com.pipedrive.datasource.products.DealProductsDataSource;
import com.pipedrive.datasource.products.ProductVariationDataSource;
import com.pipedrive.datasource.products.ProductsDataSource;
import com.pipedrive.model.Currency;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.model.products.Price;
import com.pipedrive.model.products.Product;
import com.pipedrive.model.products.ProductVariation;
import java.math.BigDecimal;

public class DealProductSerializer {
    private static final String STORAGE_KEY_DEALPRODUCT_CURRENCY_SQL_ID = "dealProduct_currencyId";
    private static final String STORAGE_KEY_DEALPRODUCT_DEAL_SQL_ID = "dealProduct_dealSqlId";
    private static final String STORAGE_KEY_DEALPRODUCT_DISCOUNT = "dealProduct_discount";
    private static final String STORAGE_KEY_DEALPRODUCT_DURATION = "dealProduct_duration";
    private static final String STORAGE_KEY_DEALPRODUCT_IS_ACTIVE = "dealProduct_isActive";
    private static final String STORAGE_KEY_DEALPRODUCT_PRICE_VALUE = "dealProduct_price";
    private static final String STORAGE_KEY_DEALPRODUCT_PRODUCT_SQL_ID = "dealProduct_productId";
    private static final String STORAGE_KEY_DEALPRODUCT_QUANTITY = "dealProduct_quantity";
    private static final String STORAGE_KEY_DEALPRODUCT_SQL_ID = "dealProduct_dealProductSqlId";
    private static final String STORAGE_KEY_DEALPRODUCT_VARIATION_SQL_ID = "dealProduct_variationId";
    @NonNull
    private final Session mSession;

    public DealProductSerializer(@NonNull Session session) {
        this.mSession = session;
    }

    public Bundle saveDealProductFieldsToBundle(@NonNull DealProduct dealProduct) {
        Bundle dealProductBundle = new Bundle();
        if (dealProduct.getSqlIdOrNull() != null) {
            dealProductBundle.putSerializable(STORAGE_KEY_DEALPRODUCT_SQL_ID, dealProduct.getSqlIdOrNull());
        }
        if (dealProduct.getProductVariation() != null) {
            dealProductBundle.putSerializable(STORAGE_KEY_DEALPRODUCT_VARIATION_SQL_ID, dealProduct.getProductVariation().getSqlIdOrNull());
        }
        dealProductBundle.putSerializable(STORAGE_KEY_DEALPRODUCT_PRODUCT_SQL_ID, dealProduct.getProduct().getSqlIdOrNull());
        dealProductBundle.putSerializable(STORAGE_KEY_DEALPRODUCT_DEAL_SQL_ID, dealProduct.getDealSqlId());
        dealProductBundle.putSerializable(STORAGE_KEY_DEALPRODUCT_CURRENCY_SQL_ID, dealProduct.getPrice().getCurrency().getSqlIdOrNull());
        dealProductBundle.putSerializable(STORAGE_KEY_DEALPRODUCT_PRICE_VALUE, dealProduct.getPrice().getValue());
        dealProductBundle.putSerializable(STORAGE_KEY_DEALPRODUCT_DISCOUNT, dealProduct.getDiscountPercentage());
        dealProductBundle.putSerializable(STORAGE_KEY_DEALPRODUCT_QUANTITY, dealProduct.getQuantity());
        dealProductBundle.putSerializable(STORAGE_KEY_DEALPRODUCT_DURATION, dealProduct.getDuration());
        dealProductBundle.putSerializable(STORAGE_KEY_DEALPRODUCT_IS_ACTIVE, dealProduct.isActive());
        return dealProductBundle;
    }

    @Nullable
    public DealProduct createDealProductFromBundle(@Nullable Bundle dealProductAsBundle) {
        if (dealProductAsBundle == null) {
            return null;
        }
        if (!bundleContainsAllNonNullDealProductFields(dealProductAsBundle)) {
            return null;
        }
        Currency currency = (Currency) new CurrenciesDataSource(this.mSession.getDatabase()).findBySqlId(dealProductAsBundle.getLong(STORAGE_KEY_DEALPRODUCT_CURRENCY_SQL_ID));
        if (currency == null) {
            return null;
        }
        BigDecimal value = (BigDecimal) dealProductAsBundle.getSerializable(STORAGE_KEY_DEALPRODUCT_PRICE_VALUE);
        if (value == null) {
            return null;
        }
        ProductVariation productVariation;
        Price price = Price.create(value, currency);
        if (dealProductAsBundle.containsKey(STORAGE_KEY_DEALPRODUCT_VARIATION_SQL_ID)) {
            productVariation = (ProductVariation) new ProductVariationDataSource(this.mSession.getDatabase()).findBySqlId(dealProductAsBundle.getLong(STORAGE_KEY_DEALPRODUCT_VARIATION_SQL_ID));
        } else {
            productVariation = null;
        }
        Double quantity = Double.valueOf(dealProductAsBundle.getDouble(STORAGE_KEY_DEALPRODUCT_QUANTITY));
        Double duration = Double.valueOf(dealProductAsBundle.getDouble(STORAGE_KEY_DEALPRODUCT_DURATION));
        Double discount = Double.valueOf(dealProductAsBundle.getDouble(STORAGE_KEY_DEALPRODUCT_DISCOUNT));
        boolean isActive = dealProductAsBundle.getBoolean(STORAGE_KEY_DEALPRODUCT_IS_ACTIVE);
        if (dealProductAsBundle.getSerializable(STORAGE_KEY_DEALPRODUCT_SQL_ID) != null) {
            DealProduct dealProduct = (DealProduct) new DealProductsDataSource(this.mSession.getDatabase()).findBySqlId(dealProductAsBundle.getLong(STORAGE_KEY_DEALPRODUCT_SQL_ID));
            if (dealProduct == null) {
                return dealProduct;
            }
            dealProduct.update(productVariation, price, quantity, duration, discount);
            return dealProduct;
        }
        boolean canCreateNewDealProduct;
        Product product;
        if (dealProductAsBundle.getSerializable(STORAGE_KEY_DEALPRODUCT_DEAL_SQL_ID) != null) {
            if (dealProductAsBundle.getSerializable(STORAGE_KEY_DEALPRODUCT_PRODUCT_SQL_ID) != null) {
                canCreateNewDealProduct = true;
                if (canCreateNewDealProduct) {
                    return null;
                }
                product = (Product) new ProductsDataSource(this.mSession.getDatabase()).findBySqlId(dealProductAsBundle.getLong(STORAGE_KEY_DEALPRODUCT_PRODUCT_SQL_ID));
                if (product == null) {
                    return null;
                }
                return DealProduct.create(null, Long.valueOf(dealProductAsBundle.getLong(STORAGE_KEY_DEALPRODUCT_DEAL_SQL_ID)), product, productVariation, price, quantity, duration, discount, Boolean.valueOf(isActive), null);
            }
        }
        canCreateNewDealProduct = false;
        if (canCreateNewDealProduct) {
            return null;
        }
        product = (Product) new ProductsDataSource(this.mSession.getDatabase()).findBySqlId(dealProductAsBundle.getLong(STORAGE_KEY_DEALPRODUCT_PRODUCT_SQL_ID));
        if (product == null) {
            return null;
        }
        return DealProduct.create(null, Long.valueOf(dealProductAsBundle.getLong(STORAGE_KEY_DEALPRODUCT_DEAL_SQL_ID)), product, productVariation, price, quantity, duration, discount, Boolean.valueOf(isActive), null);
    }

    private boolean bundleContainsAllNonNullDealProductFields(@NonNull Bundle bundle) {
        for (String key : getKeysNeededToRestoreDealProduct()) {
            if (!bundle.containsKey(key) || bundle.getSerializable(key) == null) {
                return false;
            }
        }
        return true;
    }

    @NonNull
    private static String[] getKeysNeededToRestoreDealProduct() {
        return new String[]{STORAGE_KEY_DEALPRODUCT_PRODUCT_SQL_ID, STORAGE_KEY_DEALPRODUCT_DEAL_SQL_ID, STORAGE_KEY_DEALPRODUCT_QUANTITY, STORAGE_KEY_DEALPRODUCT_PRICE_VALUE, STORAGE_KEY_DEALPRODUCT_CURRENCY_SQL_ID, STORAGE_KEY_DEALPRODUCT_DISCOUNT, STORAGE_KEY_DEALPRODUCT_IS_ACTIVE};
    }
}
