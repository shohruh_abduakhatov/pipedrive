package com.pipedrive.dialogs;

import android.app.DialogFragment;
import android.app.Fragment;
import com.pipedrive.analytics.Analytics;

public class BaseDialogFragment_Native extends DialogFragment {
    public void onStart() {
        super.onStart();
        Analytics.hitFragment((Fragment) this);
    }
}
