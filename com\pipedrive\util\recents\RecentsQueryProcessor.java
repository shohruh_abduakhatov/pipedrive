package com.pipedrive.util.recents;

import android.content.Intent;
import com.pipedrive.application.Session;
import com.pipedrive.contentproviders.DealsContentProvider;
import com.pipedrive.contentproviders.FlowContentProvider;
import com.pipedrive.contentproviders.GlobalSearchContentProvider;
import com.pipedrive.contentproviders.OrganizationContentProvider;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.FilterDealsDataSource;
import com.pipedrive.datasource.FiltersDataSource;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.datasource.PipelineDataSource;
import com.pipedrive.datasource.SQLTransactionManager;
import com.pipedrive.logging.Log;
import com.pipedrive.model.ActivityType;
import com.pipedrive.model.Deal;
import com.pipedrive.model.DealStage;
import com.pipedrive.model.Filter;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.Pipeline;
import com.pipedrive.model.contacts.PageOfOrganizations;
import com.pipedrive.model.contacts.PageOfPersons;
import com.pipedrive.tasks.activities.ActivitiesListTask;
import com.pipedrive.tasks.session.user.DownloadUserSelfTask;
import com.pipedrive.util.EmailNetworkingUtil;
import com.pipedrive.util.FiltersUtil;
import com.pipedrive.util.activities.ActivityNetworkingUtil;
import com.pipedrive.util.deals.DealsUtil;
import com.pipedrive.util.flowfiles.FlowFileNetworkingUtil;
import com.pipedrive.util.networking.entities.ActivityEntity;
import com.pipedrive.util.networking.entities.NoteEntity;
import com.pipedrive.util.organizations.OrganizationsNetworkingUtil;
import com.pipedrive.util.persons.PersonNetworkingUtil;
import com.pipedrive.util.products.ProductsNetworkingUtil;
import java.util.ArrayList;

enum RecentsQueryProcessor {
    ;
    
    private static final String TAG = null;

    static {
        TAG = RecentsQueryProcessor.class.getSimpleName();
    }

    static RecentsResponsePage processResponsePage(Session session, RecentsResponsePage responsePage) {
        PipelineDataSource dataSource;
        String tag = TAG + ".processResponsePage()";
        if (!responsePage.deals.isEmpty()) {
            FilterDealsDataSource filterDealsDataSource = new FilterDealsDataSource(session.getDatabase());
            Log.d(tag, String.format("Adding/updating %s deals into DB.", new Object[]{Integer.valueOf(responsePage.deals.size())}));
            SQLTransactionManager sQLTransactionManager = new SQLTransactionManager(session.getDatabase());
            sQLTransactionManager.beginTransactionNonExclusive();
            for (Deal deal : responsePage.deals) {
                if (!responsePage.pipelineReloadFilter) {
                    responsePage.pipelineReloadFilter = DealsUtil.isDealUnderCurrentlySelectedPipeline(session, deal);
                }
                Deal dealStoredInDb = DealsNetworkingUtil.createOrUpdateDealIntoDBWithRelations(session, deal);
                if (dealStoredInDb != null) {
                    filterDealsDataSource.unrelateAllDealAssociations(dealStoredInDb);
                }
            }
            sQLTransactionManager.commit();
            session.getApplicationContext().getContentResolver().notifyChange(new DealsContentProvider(session).createAllDealsUri(), null);
        }
        if (!responsePage.persons.isEmpty()) {
            Log.d(tag, String.format("Adding/updating %s mPersons into DB.", new Object[]{Integer.valueOf(responsePage.persons.size())}));
            sQLTransactionManager = new SQLTransactionManager(session.getDatabase());
            sQLTransactionManager.beginTransactionNonExclusive();
            for (Person person : responsePage.persons) {
                PersonNetworkingUtil.createOrUpdatePersonIntoDBWithRelations(session, person);
            }
            sQLTransactionManager.commit();
        }
        if (!responsePage.organizations.isEmpty()) {
            Log.d(tag, String.format("Adding/updating %s organizations into DB.", new Object[]{Integer.valueOf(responsePage.organizations.size())}));
            sQLTransactionManager = new SQLTransactionManager(session.getDatabase());
            sQLTransactionManager.beginTransactionNonExclusive();
            for (Organization org : responsePage.organizations) {
                OrganizationsNetworkingUtil.createOrUpdateOrganizationIntoDBWithRelations(session, org);
            }
            sQLTransactionManager.commit();
        }
        if (!responsePage.activities.isEmpty()) {
            Log.d(tag, String.format("Adding/updating %s activities into DB.", new Object[]{Integer.valueOf(responsePage.activities.size())}));
            sQLTransactionManager = new SQLTransactionManager(session.getDatabase());
            sQLTransactionManager.beginTransactionNonExclusive();
            for (ActivityEntity activityEntity : responsePage.activities) {
                ActivityNetworkingUtil.createOrUpdateActivityIntoDBWithRelations(session, activityEntity);
            }
            sQLTransactionManager.commit();
            FlowContentProvider.notifyChangesMadeInFlow(session.getApplicationContext());
        }
        if (!responsePage.dealStages.isEmpty()) {
            Log.d(tag, String.format("Adding/updating %s deal stages into DB.", new Object[]{Integer.valueOf(responsePage.dealStages.size())}));
            dataSource = new PipelineDataSource(session.getDatabase());
            dataSource.beginTransactionNonExclusive();
            for (DealStage dealStage : responsePage.dealStages) {
                dataSource.createOrUpdateStage(dealStage);
            }
            dataSource.commit();
        }
        if (!responsePage.dealsToDelete.isEmpty()) {
            Log.d(tag, String.format("Removing %s deals from the DB.", new Object[]{Integer.valueOf(responsePage.dealsToDelete.size())}));
            DealsDataSource dataSource2 = new DealsDataSource(session.getDatabase());
            dataSource2.beginTransactionNonExclusive();
            for (Integer id : responsePage.dealsToDelete) {
                if (!responsePage.pipelineReloadFilter) {
                    responsePage.pipelineReloadFilter = DealsUtil.isDealUnderCurrentlySelectedPipeline(session, id.intValue());
                }
                dataSource2.deleteDeal(id.intValue());
            }
            dataSource2.commit();
            session.getApplicationContext().getContentResolver().notifyChange(new DealsContentProvider(session).createAllDealsUri(), null);
        }
        if (!responsePage.contactsToDelete.isEmpty()) {
            Log.d(tag, String.format("Removing %s mPersons from the DB.", new Object[]{Integer.valueOf(responsePage.contactsToDelete.size())}));
            PersonsDataSource dataSource3 = new PersonsDataSource(session.getDatabase());
            dataSource3.beginTransactionNonExclusive();
            for (Integer id2 : responsePage.contactsToDelete) {
                dataSource3.deletePerson(id2.intValue());
            }
            dataSource3.commit();
        }
        if (!responsePage.organizationsToDelete.isEmpty()) {
            Log.d(tag, String.format("Removing %s organizations from the DB.", new Object[]{Integer.valueOf(responsePage.organizationsToDelete.size())}));
            OrganizationsDataSource dataSource4 = new OrganizationsDataSource(session.getDatabase());
            dataSource4.beginTransactionNonExclusive();
            for (Integer id22 : responsePage.organizationsToDelete) {
                dataSource4.deleteOrganization(id22.intValue());
            }
            dataSource4.commit();
        }
        if (!responsePage.activitiesToDelete.isEmpty()) {
            Log.d(tag, String.format("Removing %s activities from the DB.", new Object[]{Integer.valueOf(responsePage.activitiesToDelete.size())}));
            ActivitiesDataSource dataSource5 = new ActivitiesDataSource(session);
            dataSource5.beginTransactionNonExclusive();
            for (Integer id222 : responsePage.activitiesToDelete) {
                dataSource5.deleteActivity((long) id222.intValue());
            }
            dataSource5.commit();
        }
        if (!responsePage.dealStagesToDelete.isEmpty()) {
            Log.d(tag, String.format("Removing %s deal stages from the DB.", new Object[]{Integer.valueOf(responsePage.dealStagesToDelete.size())}));
            dataSource = new PipelineDataSource(session.getDatabase());
            dataSource.beginTransactionNonExclusive();
            for (Integer dealStagePipedriveId : responsePage.dealStagesToDelete) {
                dataSource.deleteStageByPipedriveId(dealStagePipedriveId.intValue());
            }
            dataSource.commit();
        }
        if (!responsePage.notesFromRecents.isEmpty()) {
            sQLTransactionManager = new SQLTransactionManager(session.getDatabase());
            sQLTransactionManager.beginTransactionNonExclusive();
            for (NoteEntity noteEntity : responsePage.notesFromRecents.values()) {
                NotesNetworkingUtil.createOrUpdateNoteIntoDBWithRelations(session, noteEntity);
            }
            sQLTransactionManager.commit();
            FlowContentProvider.notifyChangesMadeInFlow(session.getApplicationContext());
        }
        if (!responsePage.filesFromRecents.isEmpty()) {
            FlowFileNetworkingUtil.createOrUpdateFlowFilesIntoDBWithRelations(session, responsePage.filesFromRecents);
        }
        if (!responsePage.emailMessagesFromRecents.isEmpty()) {
            Log.d(TAG, String.format("Storing %d email messages", new Object[]{Integer.valueOf(responsePage.emailMessagesFromRecents.size())}));
            EmailNetworkingUtil.createOrUpdateEmailMessagesIntoDBWithRelations(session, responsePage.emailMessagesFromRecents);
        }
        if (!responsePage.emailThreadsFromRecents.isEmpty()) {
            Log.d(TAG, String.format("Storing %d email threads", new Object[]{Integer.valueOf(responsePage.emailThreadsFromRecents.size())}));
            EmailNetworkingUtil.createOrUpdateEmailThreadsIntoDBWithRelations(session, responsePage.emailThreadsFromRecents);
        }
        ProductsNetworkingUtil.createOrUpdateProductsIntoDBWithRelations(session, responsePage.productEntitiesFromRecents);
        if (!responsePage.activityTypesFromRecents.isEmpty()) {
            for (ActivityType currentActivityType : ActivityNetworkingUtil.loadAllActivityTypesList(session)) {
                if (!responsePage.activityTypesFromRecents.containsKey(Integer.valueOf(currentActivityType.getId()))) {
                    responsePage.activityTypesFromRecents.put(Integer.valueOf(currentActivityType.getId()), currentActivityType);
                }
            }
            session.setActivityTypes(ActivityType.getJSONString(new ArrayList(responsePage.activityTypesFromRecents.values())));
        }
        if (!responsePage.filtersFromRecents.isEmpty()) {
            int currentFilterPipedriveId = FiltersUtil.getSelectedPipelineFilterId(session, Integer.MIN_VALUE);
            FiltersDataSource filtersDataSource = new FiltersDataSource(session);
            filtersDataSource.beginTransactionNonExclusive();
            for (Filter filterFromRecents : responsePage.filtersFromRecents.values()) {
                filtersDataSource.createOrUpdate(filterFromRecents);
                if (currentFilterPipedriveId != Integer.MIN_VALUE && filterFromRecents.getPipedriveId() == currentFilterPipedriveId) {
                    responsePage.pipelineReloadToDefaultFilter = true;
                }
            }
            filtersDataSource.commit();
        }
        if (!responsePage.pipelinesFromRecents.isEmpty()) {
            dataSource = new PipelineDataSource(session.getDatabase());
            long selectedPipelineId = session.getPipelineSelectedPipelineId(-2147483648L);
            dataSource.beginTransactionNonExclusive();
            for (Pipeline pipeline : responsePage.pipelinesFromRecents.values()) {
                dataSource.createOrUpdatePipeline(pipeline);
                if (!(selectedPipelineId == -2147483648L || ((long) pipeline.getPipedriveId()) != selectedPipelineId || pipeline.isActive())) {
                    responsePage.pipelineReloadToDefaultFilterAndPipeline = true;
                }
            }
            dataSource.commit();
        }
        if (!(responsePage.activities.isEmpty() && responsePage.activitiesToDelete.isEmpty() && responsePage.deals.isEmpty() && responsePage.dealsToDelete.isEmpty() && responsePage.organizations.isEmpty() && responsePage.organizationsToDelete.isEmpty() && responsePage.dealStages.isEmpty() && responsePage.dealStagesToDelete.isEmpty() && responsePage.pipelinesFromRecents.isEmpty())) {
            if (responsePage.pipelineReloadToDefaultFilter || responsePage.pipelineReloadToDefaultFilterAndPipeline || responsePage.pipelineReloadFilter) {
            }
            if (!(responsePage.activities.isEmpty() && responsePage.activitiesToDelete.isEmpty())) {
                session.getApplicationContext().sendBroadcast(new Intent().setAction(ActivitiesListTask.ACTION_ACTIVITIES_DOWNLOADED));
            }
        }
        if (!(responsePage.persons.isEmpty() && responsePage.contactsToDelete.isEmpty())) {
            session.getApplicationContext().sendBroadcast(new Intent().setAction(PageOfPersons.CONTACTS_DOWNLOADED_BROADCAST));
        }
        if (!(responsePage.organizations.isEmpty() && responsePage.organizationsToDelete.isEmpty())) {
            session.getApplicationContext().sendBroadcast(new Intent().setAction(PageOfOrganizations.ACTION_ORGANIZATIONS_DOWNLOADED));
            OrganizationContentProvider.notifyChangesForOrganizationContentProvider(session.getApplicationContext());
        }
        GlobalSearchContentProvider.notifyChangesForOrganizationContentProvider(session.getApplicationContext());
        responsePage.clear();
        return responsePage;
    }

    static RecentsResponse processResponse(Session session, RecentsResponse response) {
        if (response.pipelineReloadToDefaultFilter || response.pipelineReloadToDefaultFilterAndPipeline) {
            new DownloadUserSelfTask(session).downloadAndStoreBlocking();
            response.pipelineReloadFilter = true;
        }
        long selectedPipelineId = session.getPipelineSelectedPipelineId(-1);
        if (response.pipelineReloadFilter && selectedPipelineId != -1) {
            response.pipelineReloadFilter = false;
            response.pipelineReloadToDefaultFilter = false;
            response.pipelineReloadToDefaultFilterAndPipeline = false;
        }
        return response;
    }
}
