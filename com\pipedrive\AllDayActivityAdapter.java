package com.pipedrive;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.pipedrive.activity.ActivityDetailActivity;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.model.Activity;
import java.util.Date;
import java.util.List;

public class AllDayActivityAdapter extends Adapter<ViewHolder> {
    private static final int DONE_ACTIVITY = 1;
    private static final int UNDONE_ACTIVITY = 0;
    private final ActivitiesDataSource mActivitiesDataSource;
    private List<Activity> mAllDayActivitiesList;

    public static class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        Activity activity;
        private final TextView textView;

        public ViewHolder(View view, int viewType) {
            super(view);
            view.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    ActivityDetailActivity.startActivityForActivity(v.getContext(), ViewHolder.this.activity.getSqlId());
                }
            });
            this.textView = (TextView) view.findViewById(R.id.allDayTextView);
            if (viewType == 1) {
                this.textView.setPaintFlags(this.textView.getPaintFlags() | 16);
            }
        }

        public TextView getTextView() {
            return this.textView;
        }
    }

    public AllDayActivityAdapter(@NonNull Session session, @NonNull Date date) {
        this.mActivitiesDataSource = new ActivitiesDataSource(session);
        this.mAllDayActivitiesList = this.mActivitiesDataSource.getAllDayActiveActivitiesListForDateAuthenticatedUserOnly(date);
    }

    public void setDate(@NonNull Date date) {
        this.mAllDayActivitiesList = this.mActivitiesDataSource.getAllDayActiveActivitiesListForDateAuthenticatedUserOnly(date);
    }

    @NonNull
    private Activity getItem(int position) {
        return (Activity) this.mAllDayActivitiesList.get(position);
    }

    public int getItemViewType(int position) {
        return getItem(position).isDone() ? 1 : 0;
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(viewType == 1 ? R.layout.row_all_day_done : R.layout.row_all_day, viewGroup, false), viewType);
    }

    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.activity = getItem(position);
        viewHolder.getTextView().setText(viewHolder.activity.getSubject());
    }

    public int getItemCount() {
        return this.mAllDayActivitiesList != null ? this.mAllDayActivitiesList.size() : 0;
    }
}
