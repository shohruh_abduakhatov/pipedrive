package com.pipedrive.views.viewholder.deal;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class DealProductTotalPriceViewHolder_ViewBinding implements Unbinder {
    private DealProductTotalPriceViewHolder target;

    @UiThread
    public DealProductTotalPriceViewHolder_ViewBinding(DealProductTotalPriceViewHolder target, View source) {
        this.target = target;
        target.mTotalPrice = (TextView) Utils.findRequiredViewAsType(source, R.id.totalPrice, "field 'mTotalPrice'", TextView.class);
    }

    @CallSuper
    public void unbind() {
        DealProductTotalPriceViewHolder target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mTotalPrice = null;
    }
}
