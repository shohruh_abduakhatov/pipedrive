package com.pipedrive.application;

public enum ApplicationSettings {
    ;
    
    public static final boolean ENABLE_CRASHLYTICS = true;
    public static final boolean ENABLE_GOOGLE_ANALYTICS = true;
    public static final boolean ENABLE_NEARBY_FILTER = false;
    public static final boolean ENABLE_NEWRELIC = true;
    public static final boolean ENABLE_SHADOW = false;
    public static final int INLINE_INTRO_POPUP_FOR_ACTIVITY_LIST_ICON_ID = 2;
    public static final int INLINE_INTRO_POPUP_FOR_CALENDAR_ICON_ID = 1;
    public static final int INLINE_INTRO_POPUP_FOR_NEARBY = 4;
    public static final int INLINE_INTRO_POPUP_FOR_RESET_TO_TODAY_BTN_ID = 3;
    public static final int LOADER_ID_CONTACT_DEALS_LIST = 50;
    public static final int LOADER_ID_CURRENCIES_DEALS_LIST_LOST = 62;
    public static final int LOADER_ID_CURRENCIES_DEALS_LIST_OPEN = 60;
    public static final int LOADER_ID_CURRENCIES_DEALS_LIST_WON = 61;
    public static final int LOADER_ID_FLOW = 2;
    public static final int LOADER_ID_GLOBAL_SEARCH = 3;
    public static final int LOADER_ID_ORGANIZATION_DEALS_LIST = 70;
    public static final int LOADER_ID_ORG_PERSONS_LIST = 1;
    public static final int LOADER_ID_STAGE_LOADER_ID_START = 100;
}
