package com.zendesk.sdk.deeplinking.actions;

public class ActionRefreshComments extends Action<ActionRefreshCommentsListener, ActionRefreshCommentsData> {

    public static class ActionRefreshCommentsData implements ActionData {
        private final String requestId;

        public ActionRefreshCommentsData(String requestId) {
            this.requestId = requestId;
        }

        public String getRequestId() {
            return this.requestId;
        }
    }

    public interface ActionRefreshCommentsListener extends ActionHandler {
        void refreshComments(ActionRefreshCommentsData actionRefreshCommentsData);
    }

    public ActionRefreshComments(String requestId) {
        super(ActionType.RELOAD_COMMENT_STREAM, new ActionRefreshCommentsData(requestId));
    }

    public void execute(ActionRefreshCommentsListener refreshCommentsAction, ActionRefreshCommentsData data) {
        refreshCommentsAction.refreshComments(data);
    }

    public boolean canHandleData(ActionRefreshCommentsData data) {
        return ((ActionRefreshCommentsData) getActionData()).getRequestId().equals(data.getRequestId());
    }
}
