package com.pipedrive.settings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import com.pipedrive.NavigationActivity;
import com.pipedrive.R;
import com.pipedrive.fragments.LanguageSelectorDialogFragment.LanguageSelectorCallback;
import com.pipedrive.notification.ActivityReminderSelectorDialogFragment.Callback;
import com.pipedrive.notification.AllDayActivityReminderSelectorDialogFragment;
import com.pipedrive.notification.UINotificationManager;

public class SettingsActivity extends NavigationActivity implements LanguageSelectorCallback, Callback, AllDayActivityReminderSelectorDialogFragment.Callback {
    public static void startActivity(@NonNull Activity activity) {
        ActivityCompat.startActivity(activity, new Intent(activity, SettingsActivity.class), null);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }

    public void onLanguageChanged() {
        checkForLanguageChange();
        overridePendingTransition(17432576, 17432577);
    }

    @Nullable
    private SettingsFragment getSettingsFragment() {
        return (SettingsFragment) getFragmentManager().findFragmentById(R.id.settings);
    }

    public void onActivityReminderOptionChanged() {
        UINotificationManager.getInstance().processNotificationsForAllActivities(getSession());
        SettingsFragment settingsFragment = getSettingsFragment();
        if (settingsFragment != null) {
            settingsFragment.setupUi(getSession());
        }
    }

    public void onAllDayActivityReminderOptionChanged() {
        UINotificationManager.getInstance().processNotificationsForAllActivities(getSession());
        SettingsFragment settingsFragment = getSettingsFragment();
        if (settingsFragment != null) {
            settingsFragment.setupUi(getSession());
        }
    }
}
