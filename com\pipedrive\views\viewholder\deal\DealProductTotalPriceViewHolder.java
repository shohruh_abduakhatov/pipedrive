package com.pipedrive.views.viewholder.deal;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.pipedrive.application.Session;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.util.formatter.MonetaryFormatter;
import java.math.BigDecimal;
import java.util.List;

public class DealProductTotalPriceViewHolder extends ViewHolder {
    @BindView(2131821075)
    TextView mTotalPrice;

    public DealProductTotalPriceViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void fill(@NonNull Session session, @NonNull List<DealProduct> dealProductList, @Nullable String currencyCode) {
        this.mTotalPrice.setText(getFormattedTotalSum(session, dealProductList, currencyCode));
    }

    @NonNull
    private String getFormattedTotalSum(@NonNull Session session, @NonNull List<DealProduct> dealProductList, @Nullable String currencyCode) {
        BigDecimal totalSum = BigDecimal.ZERO;
        for (DealProduct product : dealProductList) {
            totalSum = totalSum.add(product.getSum());
        }
        return new MonetaryFormatter(session).format(Double.valueOf(totalSum.doubleValue()), currencyCode);
    }
}
