package com.pipedrive.activity;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.model.Activity;

interface ActivityView {
    void onActivityCreated(boolean z);

    void onActivityDeleted(boolean z);

    void onActivityUpdated(boolean z);

    void onAssociationsUpdated(@NonNull Activity activity);

    void onRequestActivity(@Nullable Activity activity);
}
