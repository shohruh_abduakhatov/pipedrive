package com.pipedrive.changes;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.pipedrive.application.Session;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.util.networking.Response;
import com.zendesk.service.HttpConstants;

abstract class ChangeHandler {

    enum ChangeHandled {
        NOT_PROCESSED_PLEASE_RETRY,
        CHANGE_SUCCESSFUL,
        CHANGE_FAILED,
        CHANGE_CORRUPTED_PLEASE_DELETE
    }

    @NonNull
    abstract ChangeHandled handleChange(@NonNull Session session, @NonNull Change change);

    ChangeHandler() {
    }

    ChangeHandled parseResponse(@NonNull Response result, @NonNull ChangeHandler changeHandler, @NonNull Change changeUnderChange) {
        if (result.isRequestSuccessful) {
            return ChangeHandled.CHANGE_SUCCESSFUL;
        }
        String changeHandlerName = changeHandler.getClass().getSimpleName();
        if (result.httpCode != null) {
            switch (result.httpCode.intValue()) {
                case HttpConstants.HTTP_BAD_REQUEST /*400*/:
                    logJourno(EVENT.ChangesHandler_changeResponseBadRequest, result, changeHandler, changeUnderChange);
                    logErrDeletion(result, changeHandler, changeUnderChange);
                    return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
                case HttpConstants.HTTP_FORBIDDEN /*403*/:
                    logJourno(EVENT.ChangesHandler_changeResponseForbidden, result, changeHandler, changeUnderChange);
                    logErrDeletion(result, changeHandler, changeUnderChange);
                    return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
                case HttpConstants.HTTP_NOT_FOUND /*404*/:
                    logJourno(EVENT.ChangesHandler_changeResponseNotFound, result, changeHandler, changeUnderChange);
                    logErrDeletion(result, changeHandler, changeUnderChange);
                    return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
                case HttpConstants.HTTP_GONE /*410*/:
                    logJourno(EVENT.ChangesHandler_changeResponseCsGone, result, changeHandler, changeUnderChange);
                    logErrDeletion(result, changeHandler, changeUnderChange);
                    return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
                case HttpConstants.HTTP_UNSUPPORTED_TYPE /*415*/:
                    logJourno(EVENT.ChangesHandler_changeResponseUnsupportedType, result, changeHandler, changeUnderChange);
                    logErrDeletion(result, changeHandler, changeUnderChange);
                    return ChangeHandled.CHANGE_CORRUPTED_PLEASE_DELETE;
                case Response.HTTP_STATUS_TOO_MANY_REQUESTS /*429*/:
                    logJourno(EVENT.ChangesHandler_changeResponseTooManyRequests, result, changeHandler, changeUnderChange);
                    Log.e(new Throwable(String.format("%s change unsuccessful! Server is busy with networking error: %s. Will try later the change: %s", new Object[]{changeHandlerName, result.error, changeUnderChange})));
                    return ChangeHandled.CHANGE_FAILED;
            }
        }
        if (!TextUtils.equals(result.error, Response.ERROR_NO_STREAM)) {
            logJourno(EVENT.ChangesHandler_changeResponseWithError, result, changeHandler, changeUnderChange);
            Log.e(new Throwable(String.format("%s change unsuccessful! Unknown HTTP code %s with networking error: %s. Failing change: %s", new Object[]{changeHandlerName, result.httpCode, result.error, changeUnderChange})));
        }
        return ChangeHandled.CHANGE_FAILED;
    }

    private void logErrDeletion(@NonNull Response result, @NonNull ChangeHandler changeHandler, @NonNull Change changeUnderChange) {
        Log.e(new Throwable(String.format("%s change unsuccessful! Known HTTP code %s with networking error: %s. Deleting the change: %s", new Object[]{changeHandler.getClass().getSimpleName(), result.httpCode, result.error, changeUnderChange})));
    }

    private void logJourno(@NonNull EVENT event, @NonNull Response result, @NonNull ChangeHandler changeHandler, @NonNull Change changeUnderChange) {
        String changeHandlerName = changeHandler.getClass().getSimpleName();
        LogJourno.reportEvent(event, String.format("Handler:[%s] HTTP code:[%s] Error:[%s] Change:[%s]", new Object[]{changeHandlerName, result.httpCode, result.error, changeUnderChange}));
    }
}
