package com.pipedrive.views.assignment;

import android.content.Context;
import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import com.pipedrive.R;

public class SpinnerWithUserProfilePicture_ViewBinding implements Unbinder {
    @UiThread
    public SpinnerWithUserProfilePicture_ViewBinding(SpinnerWithUserProfilePicture target) {
        this(target, target.getContext());
    }

    @UiThread
    @Deprecated
    public SpinnerWithUserProfilePicture_ViewBinding(SpinnerWithUserProfilePicture target, View source) {
        this(target, source.getContext());
    }

    @UiThread
    public SpinnerWithUserProfilePicture_ViewBinding(SpinnerWithUserProfilePicture target, Context context) {
        target.hiddenUserName = context.getResources().getString(R.string._hidden_user_);
    }

    @CallSuper
    public void unbind() {
    }
}
