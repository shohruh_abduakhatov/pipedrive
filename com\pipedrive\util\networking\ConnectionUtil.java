package com.pipedrive.util.networking;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.newrelic.agent.android.instrumentation.okhttp3.OkHttp3Instrumentation;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.application.Session;
import com.pipedrive.gson.GsonHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.pagination.Pagination;
import com.pipedrive.tasks.PaymentRequiredHandlerTask;
import com.pipedrive.tasks.UnauthorizedHandlerTask;
import com.pipedrive.util.networking.entities.FlowItem;
import com.pipedrive.util.time.TimeManager;
import com.zendesk.service.HttpConstants;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.RequestBody;

public class ConnectionUtil {
    private static final int ERROR_COUNT_THRESHOLD = 3;
    private static final String TAG = ConnectionUtil.class.getSimpleName();
    private static final boolean TESTING_ENABLED = false;
    static final String UTF_8 = "UTF-8";
    private static Map<String, String> regexResponseRegister = new HashMap();
    private static Map<String, String> responseRegister = new HashMap();
    private static String singleResponse = null;

    @Nullable
    @Deprecated
    public static InputStream requestGet(@NonNull ApiUrlBuilder apiUrlBuilder) {
        return requestInputStream(apiUrlBuilder, SendDataRequestType.HttpGet);
    }

    @Nullable
    @Deprecated
    static InputStream requestPost(@NonNull ApiUrlBuilder apiUrlBuilder) {
        return requestInputStream(apiUrlBuilder, SendDataRequestType.HttpPost);
    }

    @Nullable
    private static InputStream requestInputStream(@NonNull ApiUrlBuilder apiUrlBuilder, @NonNull SendDataRequestType sendDataRequestType) {
        HttpConnectionResponseHolder httpConnection = getHttpConnection(apiUrlBuilder.getSession(), apiUrlBuilder.build(), null, sendDataRequestType, null);
        if (httpConnection.response == null || httpConnection.response.body() == null) {
            return null;
        }
        return httpConnection.response.body().byteStream();
    }

    @NonNull
    public static Response requestGet(@NonNull ApiUrlBuilder apiUrlBuilder, @Nullable JsonReaderInterceptor<Response> streamInterceptor) {
        return requestGet(apiUrlBuilder, streamInterceptor, new Response());
    }

    public static void requestGetWithAutomaticPaging(@NonNull ApiUrlBuilder apiUrlBuilder, @Nullable JsonReaderInterceptor<Response> streamInterceptor) {
        ApiUrlBuilder urlBuilder = new ApiUrlBuilder(apiUrlBuilder);
        Response response;
        do {
            response = requestGet(urlBuilder, streamInterceptor);
            urlBuilder.updateUrlPaginationParametersToQueryNextPage(response.getPagination());
        } while (response.getPagination().moreItemsInCollection);
    }

    @NonNull
    public static <T extends Response> T requestGet(@NonNull ApiUrlBuilder apiUrlBuilder, @Nullable JsonReaderInterceptor<T> streamInterceptor, @Nullable T responseTemplate) {
        return interceptedRequest(apiUrlBuilder.getSession(), apiUrlBuilder.build(), null, SendDataRequestType.HttpGet, null, streamInterceptor, responseTemplate);
    }

    @NonNull
    public static Response requestPost(@NonNull ApiUrlBuilder apiUrlBuilder, @Nullable String jsonToPost, @Nullable JsonReaderInterceptor<Response> streamInterceptor) {
        return requestPost(apiUrlBuilder, jsonToPost, streamInterceptor, new Response());
    }

    @NonNull
    public static <T extends Response> T requestPost(@NonNull ApiUrlBuilder apiUrlBuilder, @Nullable String jsonToPost, @Nullable JsonReaderInterceptor<T> streamInterceptor, @Nullable T responseTemplate) {
        return interceptedRequest(apiUrlBuilder.getSession(), apiUrlBuilder.build(), jsonToPost, SendDataRequestType.HttpPost, null, streamInterceptor, responseTemplate);
    }

    @NonNull
    public static Response requestPut(@NonNull ApiUrlBuilder apiUrlBuilder, @Nullable String jsonToPut, @Nullable JsonReaderInterceptor<Response> streamInterceptor) {
        return requestPut(apiUrlBuilder, jsonToPut, streamInterceptor, new Response());
    }

    @NonNull
    public static <T extends Response> T requestPut(@NonNull ApiUrlBuilder apiUrlBuilder, @Nullable String jsonToPut, @Nullable JsonReaderInterceptor<T> streamInterceptor, @Nullable T responseTemplate) {
        return interceptedRequest(apiUrlBuilder.getSession(), apiUrlBuilder.build(), jsonToPut, SendDataRequestType.HttpPut, null, streamInterceptor, responseTemplate);
    }

    @NonNull
    public static <T extends Response> T requestDelete(@NonNull ApiUrlBuilder apiUrlBuilder, @Nullable JsonReaderInterceptor<T> streamInterceptor, @Nullable T responseTemplate) {
        return interceptedRequest(apiUrlBuilder.getSession(), apiUrlBuilder.build(), null, SendDataRequestType.HttpDelete, null, streamInterceptor, responseTemplate);
    }

    @NonNull
    public static <T extends Response> T requestMultipartFormData(@NonNull RequestMultipart multipartRequest, @Nullable JsonReaderInterceptor<T> streamInterceptor, @Nullable T responseTemplate) {
        ApiUrlBuilder apiUrlBuilder = multipartRequest.getApiUrlBuilder();
        return interceptedRequest(apiUrlBuilder.getSession(), apiUrlBuilder.build(), null, SendDataRequestType.MultipartFormData, multipartRequest, streamInterceptor, responseTemplate);
    }

    private static <T extends Response> T interceptedRequest(@NonNull Session session, @NonNull String urlString, @Nullable String json, @NonNull SendDataRequestType requestType, @Nullable RequestMultipart multipartRequest, @Nullable JsonReaderInterceptor<T> streamInterceptor, @Nullable T responseTemplate) {
        T response;
        if (responseTemplate == null) {
            response = new Response();
        } else {
            response = responseTemplate;
        }
        response.isRequestSuccessful = true;
        HttpConnectionResponseHolder httpConnectionResponse = getHttpConnection(session, urlString, json, requestType, multipartRequest);
        if (httpConnectionResponse.response == null || !(httpConnectionResponse.response.code() == 200 || httpConnectionResponse.response.code() == 201)) {
            response.isRequestSuccessful = false;
        }
        if (httpConnectionResponse.response == null || httpConnectionResponse.response.code() >= 500) {
            response.error = Response.ERROR_SERVER_INTERNAL_ERROR;
            response.isRequestSuccessful = false;
        } else {
            try {
                JsonReader jsonReader = GsonHelper.getJSONReader(httpConnectionResponse.response.body().byteStream());
                jsonReader.beginObject();
                while (jsonReader.hasNext()) {
                    String fieldName = jsonReader.nextName();
                    if (TextUtils.equals(fieldName, Response.JSON_PARAM_SUCCESS) && jsonReader.peek() == JsonToken.BOOLEAN) {
                        response.isRequestSuccessful = jsonReader.nextBoolean();
                    } else if (!TextUtils.equals(fieldName, Response.JSON_PARAM_DATA) || jsonReader.peek() == JsonToken.NULL) {
                        if (TextUtils.equals(fieldName, Response.JSON_PARAM_ERROR)) {
                            response.error = jsonReader.nextString();
                            response.isRequestSuccessful = false;
                        } else if (!TextUtils.equals(fieldName, Response.JSON_PARAM_ADDITIONAL_DATA) || jsonReader.peek() == JsonToken.NULL) {
                            jsonReader.skipValue();
                        } else {
                            jsonReader.beginObject();
                            while (jsonReader.hasNext()) {
                                fieldName = jsonReader.nextName();
                                if (TextUtils.equals(fieldName, Pagination.JSON_PARAM_OF_PAGINATION)) {
                                    response.setPagination((Pagination) GsonHelper.fromJSON(jsonReader, (Type) Pagination.class));
                                } else if (TextUtils.equals(fieldName, Response.JSON_PARAM_MATCHES_FILTERS)) {
                                    response.setMatchedFilters((List) GsonHelper.fromJSON(jsonReader, new 1().getType()));
                                } else {
                                    jsonReader.skipValue();
                                }
                            }
                            jsonReader.endObject();
                        }
                    } else if (streamInterceptor != null) {
                        boolean origSuccess = response.isRequestSuccessful;
                        String origError = response.error;
                        Integer origHttpCode = response.httpCode;
                        Response parseResponseObject = streamInterceptor.interceptReader(session, jsonReader, response);
                        if (parseResponseObject != null) {
                            Pagination origPagination = response.getPagination();
                            List<Long> matchedFilters = response.getMatchedFilters();
                            response = parseResponseObject;
                            response.isRequestSuccessful = origSuccess;
                            response.error = origError;
                            response.httpCode = origHttpCode;
                            response.setPagination(origPagination);
                            response.setMatchedFilters(matchedFilters);
                        }
                    } else {
                        jsonReader.skipValue();
                    }
                }
                jsonReader.endObject();
            } catch (IOException e) {
                response.error = e.getMessage();
                response.isRequestSuccessful = false;
            }
        }
        if (httpConnectionResponse.response != null) {
            try {
                httpConnectionResponse.response.close();
            } catch (Exception e2) {
            }
        }
        httpConnectionResponse.disposeResponse();
        if (!(response.isRequestSuccessful || response.httpCode == null || (response.httpCode.intValue() != 400 && response.httpCode.intValue() < 404))) {
            String urlToLog = ApiUrlBuilder$Tools.stripAPIKeyFromURL(urlString);
            String logMessage = String.format("Response code:[%s] url:[%s] Error:[%s]", new Object[]{response.httpCode, urlToLog, response.error});
            LogJourno.reportEvent(EVENT.Networking_non200ResponseFromBackend, logMessage);
            Log.e(new Throwable("Networking not successful! " + logMessage));
        }
        return response;
    }

    @NonNull
    private static HttpConnectionResponseHolder getHttpConnection(@NonNull Session session, @NonNull String url, @Nullable String json, @NonNull SendDataRequestType requestType, @Nullable RequestMultipart multipartRequest) {
        HttpConnectionResponseHolder ret = new HttpConnectionResponseHolder(null);
        ret = call(session, url, json, requestType, multipartRequest, 0);
        Analytics.dispatchData(session.getApplicationContext());
        return ret;
    }

    @NonNull
    private static HttpConnectionResponseHolder call(@NonNull Session session, @NonNull String url, @Nullable String json, @NonNull SendDataRequestType requestType, @Nullable RequestMultipart multipartRequest, int errorCount) {
        String urlToLog;
        HttpConnectionResponseHolder httpConnectionResponseHolder = new HttpConnectionResponseHolder(null);
        boolean willNotProceedAsNotConnectedOfInvalidSession = (isConnected(session.getApplicationContext()) && session.isValid()) ? false : true;
        if (willNotProceedAsNotConnectedOfInvalidSession) {
            return httpConnectionResponseHolder;
        }
        Builder requestBuilder = new Builder().url(url.replace(" ", "%20")).header("Content-Type", "application/json").header("User-Agent", HttpClientManager.INSTANCE.getUserAgent());
        switch (2.$SwitchMap$com$pipedrive$util$networking$ConnectionUtil$SendDataRequestType[requestType.ordinal()]) {
            case 1:
                break;
            case 2:
                if (json != null) {
                    requestBuilder.post(RequestBody.create(MediaType.parse("application/json"), json));
                    break;
                }
                break;
            case 3:
                if (!(multipartRequest == null)) {
                    MultipartBody.Builder multipartRequestBodyBuilder = new MultipartBody.Builder().setType(MultipartBody.FORM);
                    File fileToBeUploaded = multipartRequest.getFile();
                    if (fileToBeUploaded != null) {
                        multipartRequestBodyBuilder.addFormDataPart(FlowItem.ITEM_TYPE_OBJECT_FILE, multipartRequest.getFileNameToBeUsed(Long.toString(TimeManager.getInstance().currentTimeMillis().longValue())), RequestBody.create(MediaType.parse(multipartRequest.getMimeType()), fileToBeUploaded));
                    }
                    Map<String, String> requestNameValueEntries = multipartRequest.getRequestNameValueEntries();
                    for (String requestNameValueEntriesKey : requestNameValueEntries.keySet()) {
                        String requestNameValueEntriesValue = (String) requestNameValueEntries.get(requestNameValueEntriesKey);
                        if (requestNameValueEntriesValue != null) {
                            multipartRequestBodyBuilder.addFormDataPart(requestNameValueEntriesKey, requestNameValueEntriesValue);
                        }
                    }
                    requestBuilder.post(multipartRequestBodyBuilder.build());
                    break;
                }
                httpConnectionResponseHolder.disposeResponse();
                return httpConnectionResponseHolder;
            case 4:
                if (json != null) {
                    requestBuilder.put(RequestBody.create(MediaType.parse("application/json"), json));
                    break;
                }
                break;
            case 5:
                requestBuilder.delete();
                break;
            default:
                LogJourno.reportEvent(EVENT.Networking_unknownRequestType);
                break;
        }
        if (requestType == SendDataRequestType.HttpGet) {
            session.setPrefsLastGetRequestTimeInMillis(TimeManager.getInstance().currentTimeMillis());
        }
        try {
            Call newCall;
            OkHttpClient httpClient = HttpClientManager.INSTANCE.getHttpClient();
            Request build = !(requestBuilder instanceof Builder) ? requestBuilder.build() : OkHttp3Instrumentation.build(requestBuilder);
            if (httpClient instanceof OkHttpClient) {
                newCall = OkHttp3Instrumentation.newCall(httpClient, build);
            } else {
                newCall = httpClient.newCall(build);
            }
            httpConnectionResponseHolder.response = newCall.execute();
        } catch (Exception e) {
            urlToLog = ApiUrlBuilder$Tools.stripAPIKeyFromURL(url);
            errorCount++;
            if (errorCount >= 3 || !(httpConnectionResponseHolder.response == null || httpConnectionResponseHolder.response.code() == 429 || (httpConnectionResponseHolder.response.code() > 499 && httpConnectionResponseHolder.response.code() < 599))) {
                LogJourno.reportEvent(EVENT.Networking_okHttpConnectionIOException, String.format("Url:[%s]", new Object[]{urlToLog}), e);
                Log.e(new Throwable(String.format("Error in processing url: %s", new Object[]{urlToLog}), e));
            } else {
                Log.e(new Throwable(String.format("All good, but retry in progress for url: %s thread: %s", new Object[]{urlToLog, Thread.currentThread()}), e));
                try {
                    TimeUnit timeUnit = TimeUnit.SECONDS;
                    long j = (httpConnectionResponseHolder.response == null || httpConnectionResponseHolder.response.code() != 429) ? 5 : 20;
                    timeUnit.sleep(j);
                } catch (InterruptedException e2) {
                }
                return call(session, url, json, requestType, multipartRequest, errorCount);
            }
        }
        boolean willRetryTheRequest = httpConnectionResponseHolder.response == null || !httpConnectionResponseHolder.response.isSuccessful();
        if (httpConnectionResponseHolder.response != null) {
            switch (httpConnectionResponseHolder.response.code()) {
                case 200:
                case HttpConstants.HTTP_CREATED /*201*/:
                case HttpConstants.HTTP_BAD_REQUEST /*400*/:
                case HttpConstants.HTTP_FORBIDDEN /*403*/:
                case HttpConstants.HTTP_NOT_FOUND /*404*/:
                case HttpConstants.HTTP_GONE /*410*/:
                case HttpConstants.HTTP_UNSUPPORTED_TYPE /*415*/:
                case Response.HTTP_STATUS_TOO_MANY_REQUESTS /*429*/:
                    break;
                case HttpConstants.HTTP_UNAUTHORIZED /*401*/:
                    if (!UnauthorizedHandlerTask.isExecuting(session, UnauthorizedHandlerTask.class)) {
                        Log.d(TAG, "Async task handler is not running. Starting new one");
                        session.setInvalid();
                        new UnauthorizedHandlerTask(session).execute(new Void[0]);
                        break;
                    }
                    Log.d(TAG, "Async task handler is already running");
                    break;
                case HttpConstants.HTTP_PAYMENT_REQUIRED /*402*/:
                    if (!PaymentRequiredHandlerTask.isExecuting(session, PaymentRequiredHandlerTask.class)) {
                        new PaymentRequiredHandlerTask(session).execute(new Void[0]);
                        break;
                    }
                    break;
                default:
                    willRetryTheRequest = true;
                    break;
            }
        }
        if (!willRetryTheRequest) {
            return httpConnectionResponseHolder;
        }
        errorCount++;
        if (errorCount >= 3 || !(httpConnectionResponseHolder.response == null || httpConnectionResponseHolder.response.code() == 429 || (httpConnectionResponseHolder.response.code() > 499 && httpConnectionResponseHolder.response.code() < 599))) {
            Integer responseCodeToLog;
            urlToLog = ApiUrlBuilder$Tools.stripAPIKeyFromURL(url);
            if (httpConnectionResponseHolder.response == null) {
                responseCodeToLog = null;
            } else {
                responseCodeToLog = Integer.valueOf(httpConnectionResponseHolder.response.code());
            }
            LogJourno.reportEvent(EVENT.Networking_retryCountExceeded, String.format("Url:[%s] Response code:[%s] Request type:[%s]", new Object[]{urlToLog, responseCodeToLog, requestType.toString()}));
            Log.e(new Throwable(String.format("Response code from server to %s was %s (not HTTP_OK)! Will fail the HTTP %s request.", new Object[]{urlToLog, responseCodeToLog, requestType.toString()})));
            return httpConnectionResponseHolder;
        }
        try {
            timeUnit = TimeUnit.SECONDS;
            j = (httpConnectionResponseHolder.response == null || httpConnectionResponseHolder.response.code() != 429) ? 5 : 20;
            timeUnit.sleep(j);
        } catch (InterruptedException e3) {
        }
        return call(session, url, json, requestType, multipartRequest, errorCount);
    }

    @Nullable
    public static String inputStreamToString(@Nullable InputStream inputStream) {
        if (inputStream == null) {
            return null;
        }
        BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder total = new StringBuilder();
        while (true) {
            try {
                String line = r.readLine();
                if (line == null) {
                    break;
                }
                total.append(line);
            } catch (IOException e) {
                Log.e(new Throwable("Error in processing inputstream", e));
            }
        }
        return total.toString();
    }

    public static boolean isConnected(Context context) {
        NetworkInfo activeNetwork = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static boolean isNotConnected(Context context) {
        return !isConnected(context);
    }

    static void registerResponse(String urlPattern, String json) {
        singleResponse = null;
        responseRegister.remove(urlPattern);
        responseRegister.put(urlPattern, json);
    }

    static void registerResponse(String json) {
        singleResponse = json;
    }

    static void registerRegexResponse(@NonNull String urlRegex, @NonNull String json) {
        singleResponse = null;
        regexResponseRegister.remove(urlRegex);
        regexResponseRegister.put(urlRegex, json);
    }

    static void clearResponseRegisters() {
        singleResponse = null;
        responseRegister = new HashMap();
    }

    private static InputStream getInputStream(String url) {
        if (singleResponse != null) {
            return new ByteArrayInputStream(Charset.forName("UTF-8").encode(singleResponse).array());
        }
        for (String responseRegisterUrl : responseRegister.keySet()) {
            if (TextUtils.indexOf(url, responseRegisterUrl) != -1) {
                return new ByteArrayInputStream(Charset.forName("UTF-8").encode((String) responseRegister.get(responseRegisterUrl)).array());
            }
        }
        for (String urlRegex : regexResponseRegister.keySet()) {
            if (url.matches(urlRegex)) {
                return new ByteArrayInputStream(Charset.forName("UTF-8").encode((String) regexResponseRegister.get(urlRegex)).array());
            }
        }
        return null;
    }
}
