package com.pipedrive.datasource;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.pipedrive.datasource.search.SearchConstraint;

public class SearchHelper {
    @NonNull
    private final String[] columns;

    public SearchHelper(@NonNull String... columns) {
        this.columns = columns;
    }

    @NonNull
    public SelectionHolder getSelectionHolderForSearchConstraint(@NonNull SearchConstraint searchConstraint) {
        StringBuilder selectionBuilder = new StringBuilder();
        String[] searchConstraints = searchConstraint.getParts();
        if (searchConstraints.length > 0) {
            for (String column : this.columns) {
                StringBuilder subQueryForColumn = new StringBuilder();
                for (String ignored : searchConstraints) {
                    if (!TextUtils.isEmpty(subQueryForColumn)) {
                        subQueryForColumn.append(" AND ");
                    }
                    subQueryForColumn.append(column).append(" LIKE ?");
                }
                if (!TextUtils.isEmpty(selectionBuilder)) {
                    selectionBuilder.append(" OR ");
                }
                selectionBuilder.append('(').append(subQueryForColumn).append(')');
            }
        }
        String selection = selectionBuilder.toString();
        String[] selectionArgs = new String[(searchConstraints.length * this.columns.length)];
        for (int i = 0; i < selectionArgs.length; i++) {
            selectionArgs[i] = '%' + searchConstraints[i % searchConstraints.length].toLowerCase() + '%';
        }
        return new SelectionHolder(selection, selectionArgs);
    }
}
