package com.pipedrive.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import butterknife.ButterKnife;
import com.pipedrive.R;
import com.pipedrive.activity.ActivityDetailActivity;
import com.pipedrive.adapter.ActivityListAdapter;
import com.pipedrive.adapter.ActivityListAdapter.ActivityDoneChangedListener;
import com.pipedrive.adapter.ActivityListAdapter.DataSetChangedCallback;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.datasource.UsersDataSource;
import com.pipedrive.model.Activity;
import com.pipedrive.model.ActivityType;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.User;
import com.pipedrive.store.StoreActivity;
import com.pipedrive.tasks.AsyncTask;
import com.pipedrive.tasks.AsyncTaskWithCallback;
import com.pipedrive.tasks.activities.ActivitiesListTask;
import com.pipedrive.util.TimeFilter;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.views.filter.SelectedFilterLabel;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class ActivityListFragment extends BaseFragment implements ActivityDoneChangedListener, DataSetChangedCallback {
    private static final long DONE_PROCESSING_DELAY = TimeUnit.SECONDS.toMillis(3);
    @Nullable
    private ActivityListAdapter mAdapter;
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(@NonNull Context context, @NonNull Intent intent) {
            if (ActivityListFragment.this.mAdapter != null) {
                ActivityListFragment.this.mAdapter.refreshCursor(context);
            }
            if (ActivityListFragment.this.mSwipeRefreshableLayout != null) {
                ActivityListFragment.this.mSwipeRefreshableLayout.setRefreshing(false);
            }
        }
    };
    private SparseArray<Activity> mDoneActivities = null;
    private ExpandableListView mListView;
    private Runnable mProcessingRunnable = new Runnable() {
        public void run() {
            if (ActivityListFragment.this.getActivity() != null) {
                ActivityListFragment.this.getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        ActivityListFragment.this.processDoneMarks();
                    }
                });
            }
        }
    };
    @Nullable
    private SelectedFilterLabel mSelectedFilterLabel;
    @Nullable
    private Session mSession;
    private SwipeRefreshLayout mSwipeRefreshableLayout;
    private Timer mTimer = new Timer();
    private TimerTask mTimerTask = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mSession = PipedriveApp.getActiveSession();
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_activity_list, container, false);
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        this.mListView = (ExpandableListView) view.findViewById(R.id.list);
        this.mListView.setEmptyView(view.findViewById(R.id.empty));
        this.mAdapter = initAdapter();
        if (this.mAdapter != null) {
            this.mAdapter.setActivityDoneChangedListener(this);
            this.mAdapter.setDataSetChangedCallback(this);
            this.mListView.addFooterView(ViewUtil.getFooterForListWithFloatingButton(getActivity()), null, false);
            this.mListView.setAdapter(this.mAdapter);
            this.mListView.setOnGroupClickListener(new OnGroupClickListener() {
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                    return true;
                }
            });
            this.mListView.setOnChildClickListener(new OnChildClickListener() {
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                    ActivityListFragment.this.onActivityClicked(ActivityListFragment.this.mAdapter.getChild(groupPosition, childPosition));
                    return true;
                }
            });
            view.findViewById(R.id.add).setOnClickListener(new OnClickListener() {
                public void onClick(@NonNull View v) {
                    ActivityListFragment.this.onAddClicked();
                }
            });
            this.mSwipeRefreshableLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
            this.mSwipeRefreshableLayout.setOnRefreshListener(new OnRefreshListener() {
                public void onRefresh() {
                    ActivityListFragment.this.processDoneMarks();
                    if (!AsyncTaskWithCallback.isExecuting(ActivityListFragment.this.mSession, ActivitiesListTask.class)) {
                        new ActivitiesListTask(ActivityListFragment.this.mSession).execute(new String[0]);
                    }
                }
            });
        }
    }

    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.mSelectedFilterLabel = (SelectedFilterLabel) ButterKnife.findById(getActivity(), (int) R.id.activityListSelectedFilterLabel);
    }

    private ActivityListAdapter initAdapter() {
        if (this.mSession == null) {
            return null;
        }
        ActivityType activityType = ActivityType.getActivityTypeById(this.mSession, this.mSession.getActivityFilterActivityTypeId(0));
        String activityTypeSelected = activityType == null ? null : activityType.getName();
        int savedTimeFilter = this.mSession.getActivityFilterTime(-1);
        Integer timeFilterSelected = savedTimeFilter == -1 ? null : Integer.valueOf(savedTimeFilter);
        Integer userSelectedId = getSelectedUserId(this.mSession);
        return new ActivityListAdapter(this.mSession, new ActivitiesDataSource(this.mSession).getActiveActivityListGroupCursor(getActivity(), activityTypeSelected, timeFilterSelected, userSelectedId), getActivity(), activityTypeSelected, timeFilterSelected, userSelectedId);
    }

    @Nullable
    private Integer getSelectedUserId(@NonNull Session session) {
        boolean onlyOneActiveUserFound;
        boolean noSpecificUserSavedInSession = false;
        if (new UsersDataSource(session.getDatabase()).getActiveUsersCount() == 1) {
            onlyOneActiveUserFound = true;
        } else {
            onlyOneActiveUserFound = false;
        }
        if (onlyOneActiveUserFound) {
            int authenticatedUserId = Long.valueOf(session.getAuthenticatedUserID()).intValue();
            session.setActivityFilterAssigneeId(authenticatedUserId);
            return Integer.valueOf(authenticatedUserId);
        }
        int savedUserId = session.getActivityFilterAssigneeId(-1);
        if (savedUserId == -1 || savedUserId == 0) {
            noSpecificUserSavedInSession = true;
        }
        if (noSpecificUserSavedInSession || resetSavedInSessionUserIfNotActive(session, savedUserId)) {
            return null;
        }
        return Integer.valueOf(savedUserId);
    }

    private boolean resetSavedInSessionUserIfNotActive(@NonNull Session session, int savedUserId) {
        User user = new UsersDataSource(session.getDatabase()).findByPipedriveId(savedUserId);
        if (user == null || !Boolean.FALSE.equals(user.isActive())) {
            return false;
        }
        session.setActivityFilterAssigneeId(-1);
        return true;
    }

    private void onActivityClicked(Cursor cursor) {
        if (cursor != null) {
            ActivityDetailActivity.startActivityForActivity(getActivity(), cursor.getLong(0));
        }
    }

    private void onAddClicked() {
        ActivityDetailActivity.startActivityForNewActivity(getActivity());
    }

    private synchronized void processDoneMarks() {
        if (this.mTimerTask != null) {
            this.mTimerTask.cancel();
        }
        if (this.mDoneActivities != null) {
            for (int i = 0; i < this.mDoneActivities.size(); i++) {
                new StoreActivity(this.mSession).update((BaseDatasourceEntity) this.mDoneActivities.get(this.mDoneActivities.keyAt(i)));
            }
            this.mDoneActivities.clear();
            if (this.mAdapter != null) {
                this.mAdapter.notifyDataSetChanged();
            }
        }
    }

    public void onResume() {
        super.onResume();
        if (this.mAdapter != null) {
            this.mAdapter.refreshCursor(getActivity());
        }
        if (this.mSwipeRefreshableLayout != null) {
            this.mSwipeRefreshableLayout.setRefreshing(AsyncTask.isExecuting(this.mSession, ActivitiesListTask.class));
        }
        getActivity().registerReceiver(this.mBroadcastReceiver, new IntentFilter(ActivitiesListTask.ACTION_ACTIVITIES_DOWNLOADED));
        refreshFilterLabelTextAndVisibility(this.mSession);
    }

    public void onPause() {
        super.onPause();
        processDoneMarks();
        getActivity().unregisterReceiver(this.mBroadcastReceiver);
    }

    public void onFilterChanged(Session session, int assigneeId, int activityTypeId, int timeIdx) {
        Integer selectedUserId = null;
        processDoneMarks();
        session.setActivityFilterAssigneeId(assigneeId <= 0 ? Integer.MIN_VALUE : assigneeId);
        session.setActivityFilterActivityTypeId(activityTypeId);
        session.setActivityFilterTime(timeIdx);
        ActivityCompat.invalidateOptionsMenu(getActivity());
        refreshFilterLabelTextAndVisibility(session);
        if (this.mAdapter != null) {
            ActivityType activityType = ActivityType.getActivityTypeById(session, activityTypeId);
            String activityTypeName = activityType == null ? null : activityType.getName();
            Integer timeFilterId = timeIdx == -1 ? null : Integer.valueOf(timeIdx);
            if (!(assigneeId == -1 || assigneeId == 0)) {
                selectedUserId = Integer.valueOf(assigneeId);
            }
            this.mAdapter.setFilter(getActivity(), activityTypeName, timeFilterId, selectedUserId);
        }
    }

    public void onActivityDoneStatusChanged(Activity activity, boolean isDone) {
        addActivityDoneMark(activity, isDone);
    }

    private synchronized void addActivityDoneMark(Activity activity, boolean isDone) {
        if (this.mTimerTask != null) {
            this.mTimerTask.cancel();
        }
        activity.setDone(isDone);
        this.mDoneActivities.append(Long.valueOf(activity.getSqlId()).intValue(), activity);
        this.mTimerTask = new TimerTask() {
            public void run() {
                ActivityListFragment.this.mProcessingRunnable.run();
            }
        };
        this.mTimer.schedule(this.mTimerTask, DONE_PROCESSING_DELAY);
    }

    public void onDataSetChanged() {
        if (this.mAdapter != null) {
            this.mDoneActivities = new SparseArray(this.mAdapter.getChildrenCount());
            for (int i = 0; i < this.mAdapter.getGroupCount(); i++) {
                this.mListView.expandGroup(i);
            }
        }
    }

    @Nullable
    private String getFilterLabel(@NonNull Session session) {
        int assigneeId = session.getActivityFilterAssigneeId(-1);
        int activityTypeId = session.getActivityFilterActivityTypeId(-1);
        int filterTimeId = session.getActivityFilterTime(-1);
        boolean filterTimeIdIsDefined = filterTimeId != -1;
        User assignee = null;
        ActivityType activityType = null;
        boolean moreThanOneActiveUserFound = new UsersDataSource(session.getDatabase()).getActiveUsersCount() > 1;
        if (assigneeId != -1 && moreThanOneActiveUserFound) {
            assignee = new UsersDataSource(session.getDatabase()).findByPipedriveId(assigneeId);
        }
        if (activityTypeId != -1) {
            activityType = ActivityType.getActivityTypeById(session, activityTypeId);
        }
        if (assignee == null && activityType == null && !filterTimeIdIsDefined) {
            return null;
        }
        StringBuilder filterText = new StringBuilder();
        if (activityType != null) {
            appendToFilterText(filterText, activityType.getName());
        }
        if (filterTimeIdIsDefined) {
            appendToFilterText(filterText, TimeFilter.getLabelById(getActivity(), filterTimeId));
        }
        if (assignee != null) {
            appendToFilterText(filterText, assignee.getName());
        }
        if (filterText.length() > 0) {
            return filterText.toString();
        }
        return null;
    }

    private void appendToFilterText(@NonNull StringBuilder filterText, String name) {
        if (filterText.length() > 0) {
            filterText.append(getResources().getText(R.string.space_plus_space));
        }
        filterText.append(name);
    }

    private void refreshFilterLabelTextAndVisibility(@Nullable Session session) {
        boolean ableToShowFilterLabel = (this.mSelectedFilterLabel == null || session == null) ? false : true;
        if (ableToShowFilterLabel) {
            String filterLabel = getFilterLabel(session);
            if (filterLabel != null) {
                this.mSelectedFilterLabel.setLabel(filterLabel);
            } else {
                this.mSelectedFilterLabel.disableLabel();
            }
        }
    }
}
