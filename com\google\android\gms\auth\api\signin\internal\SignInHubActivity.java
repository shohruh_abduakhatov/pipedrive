package com.google.android.gms.auth.api.signin.internal;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.SignInAccount;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.background.ApplicationStateMonitor;
import com.newrelic.agent.android.instrumentation.Instrumented;
import com.newrelic.agent.android.tracing.TraceMachine;

@KeepName
@Instrumented
public class SignInHubActivity extends FragmentActivity implements TraceFieldInterface {
    private zzk jM;
    private SignInConfiguration jN;
    private boolean jO;
    private int jP;
    private Intent jQ;

    private class zza implements LoaderCallbacks<Void> {
        final /* synthetic */ SignInHubActivity jR;

        private zza(SignInHubActivity signInHubActivity) {
            this.jR = signInHubActivity;
        }

        public Loader<Void> onCreateLoader(int i, Bundle bundle) {
            return new zzb(this.jR, GoogleApiClient.zzarc());
        }

        public /* synthetic */ void onLoadFinished(Loader loader, Object obj) {
            zza(loader, (Void) obj);
        }

        public void onLoaderReset(Loader<Void> loader) {
        }

        public void zza(Loader<Void> loader, Void voidR) {
            this.jR.setResult(this.jR.jP, this.jR.jQ);
            this.jR.finish();
        }
    }

    private void zza(int i, Intent intent) {
        if (intent != null) {
            SignInAccount signInAccount = (SignInAccount) intent.getParcelableExtra(GoogleSignInApi.EXTRA_SIGN_IN_ACCOUNT);
            if (signInAccount != null && signInAccount.zzaiz() != null) {
                Parcelable zzaiz = signInAccount.zzaiz();
                this.jM.zzb(zzaiz, this.jN.zzajk());
                intent.removeExtra(GoogleSignInApi.EXTRA_SIGN_IN_ACCOUNT);
                intent.putExtra("googleSignInAccount", zzaiz);
                this.jO = true;
                this.jP = i;
                this.jQ = intent;
                zzajl();
                return;
            } else if (intent.hasExtra("errorCode")) {
                zzdn(intent.getIntExtra("errorCode", 8));
                return;
            }
        }
        zzdn(8);
    }

    private void zzajl() {
        getSupportLoaderManager().initLoader(0, null, new zza());
    }

    private void zzdn(int i) {
        Parcelable status = new Status(i);
        Intent intent = new Intent();
        intent.putExtra("googleSignInStatus", status);
        setResult(0, intent);
        finish();
    }

    private void zzj(Intent intent) {
        intent.setPackage("com.google.android.gms");
        intent.putExtra("config", this.jN);
        try {
            startActivityForResult(intent, 40962);
        } catch (ActivityNotFoundException e) {
            Log.w("AuthSignInClient", "Could not launch sign in Intent. Google Play Service is probably being updated...");
            zzdn(8);
        }
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        return true;
    }

    protected void onActivityResult(int i, int i2, Intent intent) {
        setResult(0);
        switch (i) {
            case 40962:
                zza(i2, intent);
                return;
            default:
                return;
        }
    }

    protected void onCreate(Bundle bundle) {
        TraceMachine.startTracing("SignInHubActivity");
        try {
            TraceMachine.enterMethod(this._nr_trace, "SignInHubActivity#onCreate", null);
        } catch (NoSuchFieldError e) {
            while (true) {
                TraceMachine.enterMethod(null, "SignInHubActivity#onCreate", null);
            }
        }
        super.onCreate(bundle);
        this.jM = zzk.zzba(this);
        Intent intent = getIntent();
        if (!"com.google.android.gms.auth.GOOGLE_SIGN_IN".equals(intent.getAction())) {
            String str = "AuthSignInClient";
            String str2 = "Unknown action: ";
            String valueOf = String.valueOf(intent.getAction());
            Log.e(str, valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            finish();
        }
        this.jN = (SignInConfiguration) intent.getParcelableExtra("config");
        if (this.jN == null) {
            Log.e("AuthSignInClient", "Activity started with invalid configuration.");
            setResult(0);
            finish();
            TraceMachine.exitMethod();
            return;
        }
        if (bundle == null) {
            zzj(new Intent("com.google.android.gms.auth.GOOGLE_SIGN_IN"));
        } else {
            this.jO = bundle.getBoolean("signingInGoogleApiClients");
            if (this.jO) {
                this.jP = bundle.getInt("signInResultCode");
                this.jQ = (Intent) bundle.getParcelable("signInResultData");
                zzajl();
            }
        }
        TraceMachine.exitMethod();
    }

    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("signingInGoogleApiClients", this.jO);
        if (this.jO) {
            bundle.putInt("signInResultCode", this.jP);
            bundle.putParcelable("signInResultData", this.jQ);
        }
    }

    protected void onStart() {
        super.onStart();
        ApplicationStateMonitor.getInstance().activityStarted();
    }

    protected void onStop() {
        super.onStop();
        ApplicationStateMonitor.getInstance().activityStopped();
    }
}
