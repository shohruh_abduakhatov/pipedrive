package com.google.android.gms.analytics.internal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri.Builder;
import android.os.Build.VERSION;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.util.zzn;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.Closeable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class zzj extends zzd implements Closeable {
    private static final String dw = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' INTEGER NOT NULL, '%s' TEXT NOT NULL, '%s' TEXT NOT NULL, '%s' INTEGER);", new Object[]{"hits2", "hit_id", "hit_time", "hit_url", "hit_string", "hit_app_id"});
    private static final String dx = String.format("SELECT MAX(%s) FROM %s WHERE 1;", new Object[]{"hit_time", "hits2"});
    private final zzal dA = new zzal(zzabz());
    private final zza dy;
    private final zzal dz = new zzal(zzabz());

    class zza extends SQLiteOpenHelper {
        final /* synthetic */ zzj dB;

        zza(zzj com_google_android_gms_analytics_internal_zzj, Context context, String str) {
            this.dB = com_google_android_gms_analytics_internal_zzj;
            super(context, str, null, 1);
        }

        private void zza(SQLiteDatabase sQLiteDatabase) {
            int i = 1;
            Set zzb = zzb(sQLiteDatabase, "hits2");
            String[] strArr = new String[]{"hit_id", "hit_string", "hit_time", "hit_url"};
            int i2 = 0;
            while (i2 < 4) {
                Object obj = strArr[i2];
                if (zzb.remove(obj)) {
                    i2++;
                } else {
                    String str = "Database hits2 is missing required column: ";
                    String valueOf = String.valueOf(obj);
                    throw new SQLiteException(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                }
            }
            if (zzb.remove("hit_app_id")) {
                i = 0;
            }
            if (!zzb.isEmpty()) {
                throw new SQLiteException("Database hits2 has extra columns");
            } else if (i != 0) {
                valueOf = "ALTER TABLE hits2 ADD COLUMN hit_app_id INTEGER";
                if (sQLiteDatabase instanceof SQLiteDatabase) {
                    SQLiteInstrumentation.execSQL(sQLiteDatabase, valueOf);
                } else {
                    sQLiteDatabase.execSQL(valueOf);
                }
            }
        }

        private boolean zza(SQLiteDatabase sQLiteDatabase, String str) {
            Object e;
            Throwable th;
            Cursor cursor = null;
            Cursor query;
            try {
                String str2 = "SQLITE_MASTER";
                String[] strArr = new String[]{"name"};
                String str3 = "name=?";
                String[] strArr2 = new String[]{str};
                query = !(sQLiteDatabase instanceof SQLiteDatabase) ? sQLiteDatabase.query(str2, strArr, str3, strArr2, null, null, null) : SQLiteInstrumentation.query(sQLiteDatabase, str2, strArr, str3, strArr2, null, null, null);
                try {
                    boolean moveToFirst = query.moveToFirst();
                    if (query == null) {
                        return moveToFirst;
                    }
                    query.close();
                    return moveToFirst;
                } catch (SQLiteException e2) {
                    e = e2;
                    try {
                        this.dB.zzc("Error querying for table", str, e);
                        if (query != null) {
                            query.close();
                        }
                        return false;
                    } catch (Throwable th2) {
                        th = th2;
                        cursor = query;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
            } catch (SQLiteException e3) {
                e = e3;
                query = null;
                this.dB.zzc("Error querying for table", str, e);
                if (query != null) {
                    query.close();
                }
                return false;
            } catch (Throwable th3) {
                th = th3;
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }

        private Set<String> zzb(SQLiteDatabase sQLiteDatabase, String str) {
            Set<String> hashSet = new HashSet();
            String stringBuilder = new StringBuilder(String.valueOf(str).length() + 22).append("SELECT * FROM ").append(str).append(" LIMIT 0").toString();
            Cursor rawQuery = !(sQLiteDatabase instanceof SQLiteDatabase) ? sQLiteDatabase.rawQuery(stringBuilder, null) : SQLiteInstrumentation.rawQuery(sQLiteDatabase, stringBuilder, null);
            try {
                String[] columnNames = rawQuery.getColumnNames();
                for (Object add : columnNames) {
                    hashSet.add(add);
                }
                return hashSet;
            } finally {
                rawQuery.close();
            }
        }

        private void zzb(SQLiteDatabase sQLiteDatabase) {
            int i = 0;
            Set zzb = zzb(sQLiteDatabase, "properties");
            String[] strArr = new String[]{"app_uid", "cid", "tid", "params", "adid", "hits_count"};
            while (i < 6) {
                Object obj = strArr[i];
                if (zzb.remove(obj)) {
                    i++;
                } else {
                    String str = "Database properties is missing required column: ";
                    String valueOf = String.valueOf(obj);
                    throw new SQLiteException(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                }
            }
            if (!zzb.isEmpty()) {
                throw new SQLiteException("Database properties table has extra columns");
            }
        }

        public SQLiteDatabase getWritableDatabase() {
            if (this.dB.dA.zzz(3600000)) {
                SQLiteDatabase writableDatabase;
                try {
                    writableDatabase = super.getWritableDatabase();
                } catch (SQLiteException e) {
                    this.dB.dA.start();
                    this.dB.zzew("Opening the database failed, dropping the table and recreating it");
                    this.dB.getContext().getDatabasePath(this.dB.zzade()).delete();
                    try {
                        writableDatabase = super.getWritableDatabase();
                        this.dB.dA.clear();
                    } catch (SQLiteException e2) {
                        this.dB.zze("Failed to open freshly created database", e2);
                        throw e2;
                    }
                }
                return writableDatabase;
            }
            throw new SQLiteException("Database open failed");
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            zzx.zzfd(sQLiteDatabase.getPath());
        }

        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (VERSION.SDK_INT < 15) {
                String str = "PRAGMA journal_mode=memory";
                Cursor rawQuery = !(sQLiteDatabase instanceof SQLiteDatabase) ? sQLiteDatabase.rawQuery(str, null) : SQLiteInstrumentation.rawQuery(sQLiteDatabase, str, null);
                try {
                    rawQuery.moveToFirst();
                } finally {
                    rawQuery.close();
                }
            }
            if (zza(sQLiteDatabase, "hits2")) {
                zza(sQLiteDatabase);
            } else {
                str = zzj.dw;
                if (sQLiteDatabase instanceof SQLiteDatabase) {
                    SQLiteInstrumentation.execSQL(sQLiteDatabase, str);
                } else {
                    sQLiteDatabase.execSQL(str);
                }
            }
            if (zza(sQLiteDatabase, "properties")) {
                zzb(sQLiteDatabase);
                return;
            }
            String str2 = "CREATE TABLE IF NOT EXISTS properties ( app_uid INTEGER NOT NULL, cid TEXT NOT NULL, tid TEXT NOT NULL, params TEXT NOT NULL, adid INTEGER NOT NULL, hits_count INTEGER NOT NULL, PRIMARY KEY (app_uid, cid, tid)) ;";
            if (sQLiteDatabase instanceof SQLiteDatabase) {
                SQLiteInstrumentation.execSQL(sQLiteDatabase, str2);
            } else {
                sQLiteDatabase.execSQL(str2);
            }
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        }
    }

    zzj(zzf com_google_android_gms_analytics_internal_zzf) {
        super(com_google_android_gms_analytics_internal_zzf);
        this.dy = new zza(this, com_google_android_gms_analytics_internal_zzf.getContext(), zzade());
    }

    private long zza(String str, String[] strArr, long j) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        Cursor cursor = null;
        try {
            cursor = !(writableDatabase instanceof SQLiteDatabase) ? writableDatabase.rawQuery(str, strArr) : SQLiteInstrumentation.rawQuery(writableDatabase, str, strArr);
            if (cursor.moveToFirst()) {
                j = cursor.getLong(0);
                if (cursor != null) {
                    cursor.close();
                }
            } else if (cursor != null) {
                cursor.close();
            }
            return j;
        } catch (SQLiteException e) {
            zzd("Database error", str, e);
            throw e;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void zzadd() {
        int zzafc = zzacb().zzafc();
        long zzacu = zzacu();
        if (zzacu > ((long) (zzafc - 1))) {
            List zzs = zzs((zzacu - ((long) zzafc)) + 1);
            zzd("Store full, deleting hits to make room, count", Integer.valueOf(zzs.size()));
            zzr(zzs);
        }
    }

    private String zzade() {
        zzacb();
        return zzacb().zzafe();
    }

    private static String zzas(Map<String, String> map) {
        zzaa.zzy(map);
        Builder builder = new Builder();
        for (Entry entry : map.entrySet()) {
            builder.appendQueryParameter((String) entry.getKey(), (String) entry.getValue());
        }
        String encodedQuery = builder.build().getEncodedQuery();
        return encodedQuery == null ? "" : encodedQuery;
    }

    private long zzb(String str, String[] strArr) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        Cursor cursor = null;
        try {
            cursor = !(writableDatabase instanceof SQLiteDatabase) ? writableDatabase.rawQuery(str, strArr) : SQLiteInstrumentation.rawQuery(writableDatabase, str, strArr);
            if (cursor.moveToFirst()) {
                long j = cursor.getLong(0);
                if (cursor != null) {
                    cursor.close();
                }
                return j;
            }
            throw new SQLiteException("Database returned empty set");
        } catch (SQLiteException e) {
            zzd("Database error", str, e);
            throw e;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private String zzd(zzab com_google_android_gms_analytics_internal_zzab) {
        return com_google_android_gms_analytics_internal_zzab.zzagc() ? zzacb().zzaes() : zzacb().zzaet();
    }

    private static String zze(zzab com_google_android_gms_analytics_internal_zzab) {
        zzaa.zzy(com_google_android_gms_analytics_internal_zzab);
        Builder builder = new Builder();
        for (Entry entry : com_google_android_gms_analytics_internal_zzab.zzmc().entrySet()) {
            String str = (String) entry.getKey();
            if (!("ht".equals(str) || "qt".equals(str) || "AppUID".equals(str))) {
                builder.appendQueryParameter(str, (String) entry.getValue());
            }
        }
        String encodedQuery = builder.build().getEncodedQuery();
        return encodedQuery == null ? "" : encodedQuery;
    }

    public void beginTransaction() {
        zzacj();
        getWritableDatabase().beginTransaction();
    }

    public void close() {
        try {
            this.dy.close();
        } catch (SQLiteException e) {
            zze("Sql error closing database", e);
        } catch (IllegalStateException e2) {
            zze("Error closing database", e2);
        }
    }

    public void endTransaction() {
        zzacj();
        getWritableDatabase().endTransaction();
    }

    SQLiteDatabase getWritableDatabase() {
        try {
            return this.dy.getWritableDatabase();
        } catch (SQLiteException e) {
            zzd("Error opening database", e);
            throw e;
        }
    }

    boolean isEmpty() {
        return zzacu() == 0;
    }

    public void setTransactionSuccessful() {
        zzacj();
        getWritableDatabase().setTransactionSuccessful();
    }

    public long zza(long j, String str, String str2) {
        zzaa.zzib(str);
        zzaa.zzib(str2);
        zzacj();
        zzzx();
        return zza("SELECT hits_count FROM properties WHERE app_uid=? AND cid=? AND tid=?", new String[]{String.valueOf(j), str, str2}, 0);
    }

    public void zza(long j, String str) {
        zzaa.zzib(str);
        zzacj();
        zzzx();
        SQLiteDatabase writableDatabase = getWritableDatabase();
        String str2 = "properties";
        String str3 = "app_uid=? AND cid<>?";
        String[] strArr = new String[]{String.valueOf(j), str};
        int delete = !(writableDatabase instanceof SQLiteDatabase) ? writableDatabase.delete(str2, str3, strArr) : SQLiteInstrumentation.delete(writableDatabase, str2, str3, strArr);
        if (delete > 0) {
            zza("Deleted property records", Integer.valueOf(delete));
        }
    }

    public long zzacu() {
        zzzx();
        zzacj();
        return zzb("SELECT COUNT(*) FROM hits2", null);
    }

    public void zzacz() {
        zzzx();
        zzacj();
        SQLiteDatabase writableDatabase = getWritableDatabase();
        String str = "hits2";
        if (writableDatabase instanceof SQLiteDatabase) {
            SQLiteInstrumentation.delete(writableDatabase, str, null, null);
        } else {
            writableDatabase.delete(str, null, null);
        }
    }

    public void zzada() {
        zzzx();
        zzacj();
        SQLiteDatabase writableDatabase = getWritableDatabase();
        String str = "properties";
        if (writableDatabase instanceof SQLiteDatabase) {
            SQLiteInstrumentation.delete(writableDatabase, str, null, null);
        } else {
            writableDatabase.delete(str, null, null);
        }
    }

    public int zzadb() {
        zzzx();
        zzacj();
        if (!this.dz.zzz(86400000)) {
            return 0;
        }
        this.dz.start();
        zzes("Deleting stale hits (if any)");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        String str = "hits2";
        String str2 = "hit_time < ?";
        String[] strArr = new String[]{Long.toString(zzabz().currentTimeMillis() - 2592000000L)};
        int delete = !(writableDatabase instanceof SQLiteDatabase) ? writableDatabase.delete(str, str2, strArr) : SQLiteInstrumentation.delete(writableDatabase, str, str2, strArr);
        zza("Deleted stale hits, count", Integer.valueOf(delete));
        return delete;
    }

    public long zzadc() {
        zzzx();
        zzacj();
        return zza(dx, null, 0);
    }

    public void zzb(zzh com_google_android_gms_analytics_internal_zzh) {
        zzaa.zzy(com_google_android_gms_analytics_internal_zzh);
        zzacj();
        zzzx();
        SQLiteDatabase writableDatabase = getWritableDatabase();
        String zzas = zzas(com_google_android_gms_analytics_internal_zzh.zzmc());
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_uid", Long.valueOf(com_google_android_gms_analytics_internal_zzh.zzacr()));
        contentValues.put("cid", com_google_android_gms_analytics_internal_zzh.zzze());
        contentValues.put("tid", com_google_android_gms_analytics_internal_zzh.zzacs());
        contentValues.put("adid", Integer.valueOf(com_google_android_gms_analytics_internal_zzh.zzact() ? 1 : 0));
        contentValues.put("hits_count", Long.valueOf(com_google_android_gms_analytics_internal_zzh.zzacu()));
        contentValues.put("params", zzas);
        try {
            String str = "properties";
            if ((!(writableDatabase instanceof SQLiteDatabase) ? writableDatabase.insertWithOnConflict(str, null, contentValues, 5) : SQLiteInstrumentation.insertWithOnConflict(writableDatabase, str, null, contentValues, 5)) == -1) {
                zzew("Failed to insert/update a property (got -1)");
            }
        } catch (SQLiteException e) {
            zze("Error storing a property", e);
        }
    }

    public void zzc(zzab com_google_android_gms_analytics_internal_zzab) {
        zzaa.zzy(com_google_android_gms_analytics_internal_zzab);
        zzzx();
        zzacj();
        String zze = zze(com_google_android_gms_analytics_internal_zzab);
        if (zze.length() > 8192) {
            zzaca().zza(com_google_android_gms_analytics_internal_zzab, "Hit length exceeds the maximum allowed size");
            return;
        }
        zzadd();
        SQLiteDatabase writableDatabase = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("hit_string", zze);
        contentValues.put("hit_time", Long.valueOf(com_google_android_gms_analytics_internal_zzab.zzaga()));
        contentValues.put("hit_app_id", Integer.valueOf(com_google_android_gms_analytics_internal_zzab.zzafy()));
        contentValues.put("hit_url", zzd(com_google_android_gms_analytics_internal_zzab));
        try {
            zze = "hits2";
            long insert = !(writableDatabase instanceof SQLiteDatabase) ? writableDatabase.insert(zze, null, contentValues) : SQLiteInstrumentation.insert(writableDatabase, zze, null, contentValues);
            if (insert == -1) {
                zzew("Failed to insert a hit (got -1)");
            } else {
                zzb("Hit saved to database. db-id, hit", Long.valueOf(insert), com_google_android_gms_analytics_internal_zzab);
            }
        } catch (SQLiteException e) {
            zze("Error storing a hit", e);
        }
    }

    Map<String, String> zzex(String str) {
        if (TextUtils.isEmpty(str)) {
            return new HashMap(0);
        }
        try {
            if (!str.startsWith("?")) {
                String str2 = "?";
                String valueOf = String.valueOf(str);
                str = valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2);
            }
            return zzn.zza(new URI(str), HttpRequest.CHARSET_UTF8);
        } catch (URISyntaxException e) {
            zze("Error parsing hit parameters", e);
            return new HashMap(0);
        }
    }

    Map<String, String> zzey(String str) {
        if (TextUtils.isEmpty(str)) {
            return new HashMap(0);
        }
        try {
            String str2 = "?";
            String valueOf = String.valueOf(str);
            return zzn.zza(new URI(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2)), HttpRequest.CHARSET_UTF8);
        } catch (URISyntaxException e) {
            zze("Error parsing property parameters", e);
            return new HashMap(0);
        }
    }

    public void zzr(List<Long> list) {
        zzaa.zzy(list);
        zzzx();
        zzacj();
        if (!list.isEmpty()) {
            StringBuilder stringBuilder = new StringBuilder("hit_id");
            stringBuilder.append(" in (");
            for (int i = 0; i < list.size(); i++) {
                Long l = (Long) list.get(i);
                if (l == null || l.longValue() == 0) {
                    throw new SQLiteException("Invalid hit id");
                }
                if (i > 0) {
                    stringBuilder.append(Table.COMMA_SEP);
                }
                stringBuilder.append(l);
            }
            stringBuilder.append(")");
            String stringBuilder2 = stringBuilder.toString();
            try {
                SQLiteDatabase writableDatabase = getWritableDatabase();
                zza("Deleting dispatched hits. count", Integer.valueOf(list.size()));
                String str = "hits2";
                int delete = !(writableDatabase instanceof SQLiteDatabase) ? writableDatabase.delete(str, stringBuilder2, null) : SQLiteInstrumentation.delete(writableDatabase, str, stringBuilder2, null);
                if (delete != list.size()) {
                    zzb("Deleted fewer hits then expected", Integer.valueOf(list.size()), Integer.valueOf(delete), stringBuilder2);
                }
            } catch (SQLiteException e) {
                zze("Error deleting hits", e);
                throw e;
            }
        }
    }

    public List<Long> zzs(long j) {
        Cursor query;
        Object e;
        Throwable th;
        Cursor cursor = null;
        zzzx();
        zzacj();
        if (j <= 0) {
            return Collections.emptyList();
        }
        SQLiteDatabase writableDatabase = getWritableDatabase();
        List<Long> arrayList = new ArrayList();
        try {
            String str = "hits2";
            String[] strArr = new String[]{"hit_id"};
            String format = String.format("%s ASC", new Object[]{"hit_id"});
            String l = Long.toString(j);
            query = !(writableDatabase instanceof SQLiteDatabase) ? writableDatabase.query(str, strArr, null, null, null, null, format, l) : SQLiteInstrumentation.query(writableDatabase, str, strArr, null, null, null, null, format, l);
            try {
                if (query.moveToFirst()) {
                    do {
                        arrayList.add(Long.valueOf(query.getLong(0)));
                    } while (query.moveToNext());
                }
                if (query != null) {
                    query.close();
                }
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    zzd("Error selecting hit ids", e);
                    if (query != null) {
                        query.close();
                    }
                    return arrayList;
                } catch (Throwable th2) {
                    th = th2;
                    cursor = query;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            query = null;
            zzd("Error selecting hit ids", e);
            if (query != null) {
                query.close();
            }
            return arrayList;
        } catch (Throwable th3) {
            th = th3;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return arrayList;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public List<zzab> zzt(long j) {
        Throwable th;
        boolean z = true;
        Cursor cursor = null;
        if (j < 0) {
            z = false;
        }
        zzaa.zzbt(z);
        zzzx();
        zzacj();
        SQLiteDatabase writableDatabase = getWritableDatabase();
        try {
            String str = "hits2";
            String[] strArr = new String[]{"hit_id", "hit_time", "hit_string", "hit_url", "hit_app_id"};
            String format = String.format("%s ASC", new Object[]{"hit_id"});
            String l = Long.toString(j);
            cursor = !(writableDatabase instanceof SQLiteDatabase) ? writableDatabase.query(str, strArr, null, null, null, null, format, l) : SQLiteInstrumentation.query(writableDatabase, str, strArr, null, null, null, null, format, l);
            List<zzab> arrayList = new ArrayList();
            if (cursor.moveToFirst()) {
                do {
                    long j2 = cursor.getLong(0);
                    long j3 = cursor.getLong(1);
                    String string = cursor.getString(2);
                    str = cursor.getString(3);
                    arrayList.add(new zzab(this, zzex(string), j3, zzao.zzfn(str), j2, cursor.getInt(4)));
                } while (cursor.moveToNext());
            }
            if (cursor != null) {
                cursor.close();
            }
            return arrayList;
        } catch (SQLiteException e) {
            Object e2 = e;
            Cursor cursor2 = cursor;
            try {
                zze("Error loading hits from the database", e2);
                throw e2;
            } catch (Throwable th2) {
                th = th2;
                cursor = cursor2;
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public void zzu(long j) {
        zzzx();
        zzacj();
        List arrayList = new ArrayList(1);
        arrayList.add(Long.valueOf(j));
        zza("Deleting hit, id", Long.valueOf(j));
        zzr(arrayList);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public List<zzh> zzv(long j) {
        zzacj();
        zzzx();
        SQLiteDatabase writableDatabase = getWritableDatabase();
        Cursor cursor = null;
        try {
            String[] strArr = new String[]{"cid", "tid", "adid", "hits_count", "params"};
            int zzafd = zzacb().zzafd();
            String valueOf = String.valueOf(zzafd);
            String str = "app_uid=?";
            String[] strArr2 = new String[]{String.valueOf(j)};
            String str2 = "properties";
            cursor = !(writableDatabase instanceof SQLiteDatabase) ? writableDatabase.query(str2, strArr, str, strArr2, null, null, null, valueOf) : SQLiteInstrumentation.query(writableDatabase, str2, strArr, str, strArr2, null, null, null, valueOf);
            List<zzh> arrayList = new ArrayList();
            if (cursor.moveToFirst()) {
                do {
                    Object string = cursor.getString(0);
                    Object string2 = cursor.getString(1);
                    boolean z = cursor.getInt(2) != 0;
                    long j2 = (long) cursor.getInt(3);
                    Map zzey = zzey(cursor.getString(4));
                    if (TextUtils.isEmpty(string) || TextUtils.isEmpty(string2)) {
                        zzc("Read property with empty client id or tracker id", string, string2);
                    } else {
                        arrayList.add(new zzh(j, string, string2, z, j2, zzey));
                    }
                } while (cursor.moveToNext());
            }
            if (arrayList.size() >= zzafd) {
                zzev("Sending hits to too many properties. Campaign report might be incorrect");
            }
            if (cursor != null) {
                cursor.close();
            }
            return arrayList;
        } catch (SQLiteException e) {
            Object e2 = e;
            Cursor cursor2 = cursor;
        } catch (Throwable th) {
            Throwable th2 = th;
        }
    }

    protected void zzzy() {
    }
}
