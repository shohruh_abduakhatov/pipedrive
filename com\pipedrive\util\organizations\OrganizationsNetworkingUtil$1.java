package com.pipedrive.util.organizations;

import android.support.annotation.NonNull;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.pipedrive.application.Session;
import com.pipedrive.util.networking.ConnectionUtil$JsonReaderInterceptor;
import java.io.IOException;

class OrganizationsNetworkingUtil$1 implements ConnectionUtil$JsonReaderInterceptor<OrganizationsNetworkingUtil$1OrganizationDetailsRequest> {
    OrganizationsNetworkingUtil$1() {
    }

    public OrganizationsNetworkingUtil$1OrganizationDetailsRequest interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull OrganizationsNetworkingUtil$1OrganizationDetailsRequest responseTemplate) throws IOException {
        if (jsonReader.peek() == JsonToken.NULL) {
            jsonReader.skipValue();
        } else {
            responseTemplate.org = OrganizationsNetworkingUtil.readOrg(jsonReader, session.getOrgAddressField(null));
        }
        return responseTemplate;
    }
}
