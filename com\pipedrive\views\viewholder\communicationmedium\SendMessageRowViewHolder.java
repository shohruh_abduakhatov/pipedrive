package com.pipedrive.views.viewholder.communicationmedium;

import android.content.Context;
import android.support.annotation.NonNull;
import com.pipedrive.R;
import com.pipedrive.model.contact.CommunicationMedium;
import com.pipedrive.model.contact.Phone;

public class SendMessageRowViewHolder extends CallRowViewHolder {
    public void fill(@NonNull Context context, @NonNull CommunicationMedium communicationMedium) {
        String actionString = context.getString(communicationMedium instanceof Phone ? R.string.text : R.string.email);
        this.mValue.setText(String.format("%s %s", new Object[]{actionString, communicationMedium.getValue()}));
        this.mType.setText(communicationMedium.getLabel());
    }
}
