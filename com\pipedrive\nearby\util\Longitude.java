package com.pipedrive.nearby.util;

import android.support.annotation.FloatRange;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@FloatRange(from = -180.0d, to = 180.0d)
@Retention(RetentionPolicy.SOURCE)
public @interface Longitude {
    public static final Double MAX_VALUE = Double.valueOf(180.0d);
    public static final Double MIN_VALUE = Double.valueOf(-180.0d);
}
