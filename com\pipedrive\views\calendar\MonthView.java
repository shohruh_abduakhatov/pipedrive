package com.pipedrive.views.calendar;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.GridLayout.LayoutParams;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.pipedrive.R;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.datetime.ActivityDensityManager;
import com.pipedrive.datetime.DateFormatHelper;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.util.CompatUtil;
import com.pipedrive.util.time.TimeManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

class MonthView extends GridLayout {
    private static final String TAG_WEEKDAY_BASE = "weekdayItem";
    private static final String TAG_WEEKDAY_ROOT = "weekdayItem_root_";
    private static final String TAG_WEEKDAY_TEXT = "weekdayItem_text_";
    @NonNull
    private List<View> dayViews;
    @Nullable
    private OnDateChangedInternalListener onDateChangedInternalListener;

    interface OnDateChangedInternalListener {
        void onDateChangedInternal(@NonNull Calendar calendar);
    }

    public MonthView(Context context) {
        this(context, null);
    }

    public MonthView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MonthView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.dayViews = new ArrayList();
        setPadding(0, 0, 0, getResources().getDimensionPixelSize(R.dimen.dimen5));
        init();
    }

    void init() {
        setColumnCount(7);
    }

    @NonNull
    List<View> getDayViews() {
        return this.dayViews;
    }

    public void setup(@NonNull Locale locale, int year, int month, @Nullable Calendar selectedDate) {
        String tag = String.format("%d.%d", new Object[]{Integer.valueOf(year), Integer.valueOf(month)});
        if (tag.equalsIgnoreCase((String) getTag())) {
            updateSelection(selectedDate);
            return;
        }
        this.dayViews.clear();
        removeAllViewsInLayout();
        addWeekdays(locale);
        addDays(selectedDate, year, month);
        setTag(tag);
    }

    void updateSelection(@Nullable Calendar selectedDate) {
        for (View dayView : this.dayViews) {
            updateViewBackground(selectedDate, dayView);
        }
    }

    private void updateViewBackground(@Nullable Calendar selectedDate, View dayView) {
        long dateMillis = ((Long) dayView.getTag()).longValue();
        View textView = dayView.findViewWithTag(getTextViewTag(dateMillis));
        if (selectedDate != null && DateFormatHelper.isSameDayUtc(dateMillis, selectedDate)) {
            textView.setBackgroundResource(R.drawable.bg_calendar_item_selected);
        } else if (DateFormatHelper.isTodayUtc(dateMillis)) {
            textView.setBackgroundResource(R.drawable.bg_calendar_item_today);
        } else {
            textView.setBackground(null);
        }
    }

    void addWeekdays(@NonNull Locale locale) {
        Calendar cal = Calendar.getInstance(locale);
        cal.set(4, 2);
        cal.set(7, cal.getFirstDayOfWeek());
        for (int i = 0; i < 7; i++) {
            addView(getWeekdayView(cal.getDisplayName(7, 1, locale), i), new LayoutParams(GridLayout.spec(0, 1), GridLayout.spec(i, 1, CENTER, 1.0f)));
            cal.add(7, 1);
        }
    }

    private void addDays(@Nullable Calendar selectedDate, int year, int month) {
        Calendar cal = PipedriveDateTime.instanceFromUTC(year, month, 1).toCalendar();
        int currentMonth = cal.get(2);
        int firstDayOfWeek = cal.getFirstDayOfWeek();
        int row = 1;
        cal.set(5, 1);
        while (cal.get(2) == currentMonth) {
            View dayView = getDayView(cal, selectedDate);
            this.dayViews.add(dayView);
            if (cal.get(7) == firstDayOfWeek && cal.get(5) != 1) {
                row++;
            }
            int column = cal.get(7) - cal.getFirstDayOfWeek();
            if (column < 0) {
                column = 6;
            }
            addView(dayView, new LayoutParams(GridLayout.spec(row, 1), GridLayout.spec(column, 1, FILL, 1.0f)));
            cal.add(5, 1);
        }
    }

    @NonNull
    private View getWeekdayView(@NonNull String text, int position) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.view_calendar_item_day_of_week, this, false);
        view.setTag(TAG_WEEKDAY_ROOT + position);
        TextView textView = (TextView) view.findViewById(R.id.calendarItem.text);
        textView.setId(-1);
        textView.setTag(TAG_WEEKDAY_TEXT + position);
        textView.setText(text.substring(0, 1));
        return view;
    }

    @NonNull
    View getDayView(@NonNull Calendar date, @Nullable Calendar selectedDate) {
        boolean isWeekend = false;
        int day = date.get(5);
        long dateInMillis = date.getTimeInMillis();
        View view = LayoutInflater.from(getContext()).inflate(R.layout.view_calendar_item, this, false);
        view.setTag(Long.valueOf(dateInMillis));
        view.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (MonthView.this.onDateChangedInternalListener != null) {
                    long dateMillis = ((Long) v.getTag()).longValue();
                    Calendar date = TimeManager.getInstance().getUtcCalendar();
                    date.setTimeInMillis(dateMillis);
                    MonthView.this.onDateChangedInternalListener.onDateChangedInternal(date);
                }
            }
        });
        TextView textView = (TextView) view.findViewById(R.id.calendarItem.text);
        textView.setId(-1);
        textView.setTag(getTextViewTag(dateInMillis));
        textView.setText(String.valueOf(day));
        updateViewBackground(selectedDate, view);
        if (date.get(7) == 1 || date.get(7) == 7) {
            isWeekend = true;
        }
        CompatUtil.setTextAppearance(textView, isWeekend ? R.style.TextAppearance.CalendarView.Weekend : R.style.TextAppearance.CalendarView.WorkDay);
        View densityIndicator = view.findViewById(R.id.calendarItem.densityIndicator);
        Session activeSession = PipedriveApp.getActiveSession();
        if (activeSession != null) {
            ActivityDensityManager.getInstance().displayDensity(activeSession, PipedriveDateTime.instanceFromDate(date.getTime()).resetTime(), densityIndicator);
        }
        return view;
    }

    @NonNull
    private String getTextViewTag(long dateInMillis) {
        return String.format("%d_textView", new Object[]{Long.valueOf(dateInMillis)});
    }

    void setOnDateChangedInternalListener(@Nullable OnDateChangedInternalListener listener) {
        this.onDateChangedInternalListener = listener;
    }
}
