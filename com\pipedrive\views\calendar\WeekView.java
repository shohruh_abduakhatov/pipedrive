package com.pipedrive.views.calendar;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.GridLayout.LayoutParams;
import android.util.AttributeSet;
import android.view.View;
import com.pipedrive.R;
import com.pipedrive.datetime.PipedriveDateTime;
import java.util.Calendar;
import java.util.Locale;

class WeekView extends MonthView {
    public WeekView(Context context) {
        this(context, null);
    }

    public WeekView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WeekView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setMinimumHeight(getResources().getDimensionPixelSize(R.dimen.calendarView.weekHeight));
        setPadding(0, 0, 0, 0);
        init();
    }

    public void setup(@NonNull Locale locale, @Nullable Calendar selectedDate, int year, int month, int weekOfMonth, int weekOfYear) {
        String tag = String.format("%d.%d", new Object[]{Integer.valueOf(year), Integer.valueOf(weekOfYear)});
        if (tag.equalsIgnoreCase((String) getTag())) {
            updateSelection(selectedDate);
            return;
        }
        getDayViews().clear();
        removeAllViewsInLayout();
        addWeekdays(locale);
        addDays(selectedDate, year, month, weekOfMonth, weekOfYear);
        setTag(tag);
    }

    private void addDays(@Nullable Calendar selectedDate, int year, int month, int weekOfMonth, int weekOfYear) {
        Calendar cal = PipedriveDateTime.instanceWithCurrentDateTime().resetTime().toCalendar();
        cal.set(1, year);
        cal.set(2, month);
        cal.set(4, weekOfMonth);
        cal.set(3, weekOfYear);
        cal.set(7, cal.getFirstDayOfWeek());
        int currentWeek = cal.get(3);
        int firstDayOfWeek = cal.getFirstDayOfWeek();
        while (cal.get(3) == currentWeek) {
            View dayView = getDayView(cal, selectedDate);
            getDayViews().add(dayView);
            int column = cal.get(7) - firstDayOfWeek;
            if (column < 0) {
                column = 6;
            }
            addView(dayView, new LayoutParams(GridLayout.spec(1, 1), GridLayout.spec(column, 1, FILL, 1.0f)));
            cal.add(5, 1);
        }
    }
}
