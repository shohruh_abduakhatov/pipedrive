package com.pipedrive.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.model.DealStage;
import com.pipedrive.model.Pipeline;
import java.util.ArrayList;
import java.util.List;
import rx.Single;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class PipelineDataSource extends SQLTransactionManager {
    private static final String[] pipelinesColumns = new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, "name", PipeSQLiteHelper.COLUMN_ORDER_NR, PipeSQLiteHelper.COLUMN_ACTIVE, PipeSQLiteHelper.COLUMN_DEFAULT, PipeSQLiteHelper.COLUMN_SELECTED};
    private static final String[] stagesColumns = new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, PipeSQLiteHelper.COLUMN_DEALS_PIPELINE_ID, "name", PipeSQLiteHelper.COLUMN_DEAL_PROBABILITY, PipeSQLiteHelper.COLUMN_ORDER_NR};

    public PipelineDataSource(SQLiteDatabase dbconn) {
        super(dbconn);
    }

    public Pipeline createOrUpdatePipeline(Pipeline pipeline) {
        int i;
        int i2 = 1;
        ContentValues values = new ContentValues();
        values.put(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, Integer.valueOf(pipeline.getPipedriveId()));
        values.put("name", pipeline.getName());
        values.put(PipeSQLiteHelper.COLUMN_ORDER_NR, Integer.valueOf(pipeline.getOrderNr()));
        String str = PipeSQLiteHelper.COLUMN_ACTIVE;
        if (pipeline.isActive()) {
            i = 1;
        } else {
            i = 0;
        }
        values.put(str, Integer.valueOf(i));
        str = PipeSQLiteHelper.COLUMN_DEFAULT;
        if (pipeline.isDefault()) {
            i = 1;
        } else {
            i = 0;
        }
        values.put(str, Integer.valueOf(i));
        String str2 = PipeSQLiteHelper.COLUMN_SELECTED;
        if (!pipeline.isSelected()) {
            i2 = 0;
        }
        values.put(str2, Integer.valueOf(i2));
        long insertId = -1;
        try {
            SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
            String str3 = "pipelines";
            insertId = !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.insertWithOnConflict(str3, null, values, 4) : SQLiteInstrumentation.insertWithOnConflict(transactionalDBConnection, str3, null, values, 4);
        } catch (Exception e) {
        }
        if (insertId == -1) {
            transactionalDBConnection = getTransactionalDBConnection();
            str3 = "pipelines";
            if (transactionalDBConnection instanceof SQLiteDatabase) {
                SQLiteInstrumentation.replace(transactionalDBConnection, str3, null, values);
            } else {
                transactionalDBConnection.replace(str3, null, values);
            }
        } else {
            pipeline.setSqlId(insertId);
        }
        return pipeline;
    }

    public DealStage createOrUpdateStage(DealStage stage) {
        ContentValues values = new ContentValues();
        values.put(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, Integer.valueOf(stage.getPipedriveId()));
        values.put(PipeSQLiteHelper.COLUMN_DEALS_PIPELINE_ID, Integer.valueOf(stage.getPipelineId()));
        values.put("name", stage.getName());
        values.put(PipeSQLiteHelper.COLUMN_DEAL_PROBABILITY, Integer.valueOf(stage.getDealProbability()));
        values.put(PipeSQLiteHelper.COLUMN_ORDER_NR, Integer.valueOf(stage.getOrderNr()));
        long insertId = -1;
        try {
            SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
            String str = PipeSQLiteHelper.TABLE_PIPELINE_STAGES;
            insertId = !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.insertWithOnConflict(str, null, values, 4) : SQLiteInstrumentation.insertWithOnConflict(transactionalDBConnection, str, null, values, 4);
        } catch (Exception e) {
        }
        if (insertId == -1) {
            transactionalDBConnection = getTransactionalDBConnection();
            str = PipeSQLiteHelper.TABLE_PIPELINE_STAGES;
            if (transactionalDBConnection instanceof SQLiteDatabase) {
                SQLiteInstrumentation.replace(transactionalDBConnection, str, null, values);
            } else {
                transactionalDBConnection.replace(str, null, values);
            }
        } else {
            stage.setSqlId(insertId);
        }
        return stage;
    }

    public void deleteStage(DealStage stage) {
        if (stage != null) {
            deleteStageByPipedriveId(stage.getPipedriveId());
        }
    }

    public void deleteStageByPipedriveId(int pipedriveId) {
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String str = PipeSQLiteHelper.TABLE_PIPELINE_STAGES;
        String str2 = "id = " + pipedriveId;
        if (transactionalDBConnection instanceof SQLiteDatabase) {
            SQLiteInstrumentation.delete(transactionalDBConnection, str, str2, null);
        } else {
            transactionalDBConnection.delete(str, str2, null);
        }
    }

    public DealStage findStageById(int pipedriveId) {
        List<DealStage> stages = getStages(1, "id=" + pipedriveId, null);
        if (stages.isEmpty()) {
            return null;
        }
        return (DealStage) stages.get(0);
    }

    @NonNull
    public ArrayList<DealStage> findStagesByPipelineId(long pipelineId) {
        return getStages(100, "d_pipeline_id=" + pipelineId, null);
    }

    @Nullable
    public List<DealStage> getAllStagesWithinPipeline(int stageId) {
        DealStage stage = findStageById(stageId);
        if (stage == null) {
            return null;
        }
        return findStagesByPipelineId((long) stage.getPipelineId());
    }

    public List<DealStage> getStages(int limit) {
        return getStages(limit, null, null);
    }

    @NonNull
    public ArrayList<DealStage> getStages(int limit, String selection, String[] selectionArgs) {
        Cursor cursor;
        ArrayList<DealStage> stages = new ArrayList();
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String str = PipeSQLiteHelper.TABLE_PIPELINE_STAGES;
        String[] strArr = stagesColumns;
        String str2 = PipeSQLiteHelper.COLUMN_ORDER_NR;
        String str3 = "" + limit;
        if (transactionalDBConnection instanceof SQLiteDatabase) {
            cursor = SQLiteInstrumentation.query(transactionalDBConnection, str, strArr, selection, selectionArgs, null, null, str2, str3);
        } else {
            cursor = transactionalDBConnection.query(str, strArr, selection, selectionArgs, null, null, str2, str3);
        }
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                stages.add(cursorToStage(cursor));
                cursor.moveToNext();
            }
            return stages;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private DealStage cursorToStage(Cursor cursor) {
        DealStage stage = new DealStage();
        stage.setSqlId(cursor.getLong(0));
        stage.setPipedriveId(cursor.getInt(1));
        stage.setPipelineId(cursor.getInt(2));
        stage.setName(cursor.getString(3));
        stage.setDealProbability(cursor.getInt(4));
        stage.setOrderNr(cursor.getInt(5));
        return stage;
    }

    public List<Pipeline> getPipelines() {
        return getPipelines(-1, null, null);
    }

    public List<Pipeline> getPipelines(int limit) {
        return getPipelines(limit, null, null);
    }

    private List<Pipeline> getPipelines(int limit, String selection, String[] selectionArgs) {
        String str;
        Cursor cursor;
        List<Pipeline> pipelines = new ArrayList();
        if (selection == null) {
            selection = "";
        }
        StringBuilder selectionBuilder = new StringBuilder(selection);
        if (selectionBuilder.length() > 0) {
            selectionBuilder.append(" AND ");
        }
        selectionBuilder.append("active=1");
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String str2 = "pipelines";
        String[] strArr = pipelinesColumns;
        String stringBuilder = selectionBuilder.toString();
        String str3 = PipeSQLiteHelper.COLUMN_ORDER_NR;
        if (limit < 0) {
            str = null;
        } else {
            str = "" + limit;
        }
        if (transactionalDBConnection instanceof SQLiteDatabase) {
            cursor = SQLiteInstrumentation.query(transactionalDBConnection, str2, strArr, stringBuilder, selectionArgs, null, null, str3, str);
        } else {
            cursor = transactionalDBConnection.query(str2, strArr, stringBuilder, selectionArgs, null, null, str3, str);
        }
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                pipelines.add(cursorToPipeline(cursor));
                cursor.moveToNext();
            }
            return pipelines;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private Pipeline cursorToPipeline(Cursor cursor) {
        boolean z;
        boolean z2 = true;
        Pipeline pipeline = new Pipeline();
        pipeline.setSqlId(cursor.getLong(0));
        pipeline.setPipedriveId(cursor.getInt(1));
        pipeline.setName(cursor.getString(2));
        pipeline.setOrderNr(cursor.getInt(3));
        if (cursor.getInt(4) == 1) {
            z = true;
        } else {
            z = false;
        }
        pipeline.setActive(z);
        if (cursor.getInt(5) == 1) {
            z = true;
        } else {
            z = false;
        }
        pipeline.setDefault(z);
        if (cursor.getInt(6) != 1) {
            z2 = false;
        }
        pipeline.setSelected(z2);
        return pipeline;
    }

    public Cursor getPipelinesCursor() {
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String str = "pipelines";
        String[] strArr = pipelinesColumns;
        String str2 = "active=?";
        String[] strArr2 = new String[]{String.valueOf(1)};
        String str3 = PipeSQLiteHelper.COLUMN_ORDER_NR;
        return !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.query(str, strArr, str2, strArr2, null, null, str3) : SQLiteInstrumentation.query(transactionalDBConnection, str, strArr, str2, strArr2, null, null, str3);
    }

    public Cursor getStagesCursor(long pipelineId) {
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String str = PipeSQLiteHelper.TABLE_PIPELINE_STAGES;
        String[] strArr = stagesColumns;
        String str2 = "d_pipeline_id=?";
        String[] strArr2 = new String[]{String.valueOf(pipelineId)};
        String str3 = PipeSQLiteHelper.COLUMN_ORDER_NR;
        return !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.query(str, strArr, str2, strArr2, null, null, str3) : SQLiteInstrumentation.query(transactionalDBConnection, str, strArr, str2, strArr2, null, null, str3);
    }

    public DealStage getStage(long stageId) {
        DealStage result = null;
        Cursor cursor = null;
        try {
            SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
            String str = PipeSQLiteHelper.TABLE_PIPELINE_STAGES;
            String[] strArr = stagesColumns;
            String str2 = "id=?";
            String[] strArr2 = new String[]{String.valueOf(stageId)};
            if (transactionalDBConnection instanceof SQLiteDatabase) {
                cursor = SQLiteInstrumentation.query(transactionalDBConnection, str, strArr, str2, strArr2, null, null, null);
            } else {
                cursor = transactionalDBConnection.query(str, strArr, str2, strArr2, null, null, null);
            }
            if (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) {
                result = cursorToStage(cursor);
            }
            if (cursor != null) {
                cursor.close();
            }
            return result;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void deleteAllPipelines() {
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String str = "pipelines";
        if (transactionalDBConnection instanceof SQLiteDatabase) {
            SQLiteInstrumentation.delete(transactionalDBConnection, str, null, null);
        } else {
            transactionalDBConnection.delete(str, null, null);
        }
    }

    public void deleteAllStages() {
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String str = PipeSQLiteHelper.TABLE_PIPELINE_STAGES;
        if (transactionalDBConnection instanceof SQLiteDatabase) {
            SQLiteInstrumentation.delete(transactionalDBConnection, str, null, null);
        } else {
            transactionalDBConnection.delete(str, null, null);
        }
    }

    @Nullable
    private Pair<Integer, Integer> getSelectedStageOrder(@NonNull Integer stageId) {
        DealStage stage = findStageById(stageId.intValue());
        if (stage == null) {
            return null;
        }
        Cursor cursor = getStagesCursor((long) stage.getPipelineId());
        int stageOrderNumber = -1;
        cursor.moveToFirst();
        int count = cursor.getCount();
        for (int i = 0; i < count; i++) {
            if (cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID)) == stageId.intValue()) {
                stageOrderNumber = cursor.getPosition();
                break;
            }
            cursor.moveToNext();
        }
        cursor.close();
        return new Pair(Integer.valueOf(count), Integer.valueOf(stageOrderNumber));
    }

    @NonNull
    public Single<Pair<Integer, Integer>> getSelectedStageOrderRx(@NonNull final Integer sqlId) {
        return Single.just(sqlId).subscribeOn(Schedulers.io()).map(new Func1<Integer, Pair<Integer, Integer>>() {
            public Pair<Integer, Integer> call(Integer id) {
                return PipelineDataSource.this.getSelectedStageOrder(sqlId);
            }
        });
    }
}
