package com.zendesk.sdk.rating.impl;

import android.content.Context;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.model.access.Identity;
import com.zendesk.sdk.model.access.JwtIdentity;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.rating.RateMyAppRule;
import com.zendesk.util.StringUtils;

public class IdentityRateMyAppRule implements RateMyAppRule {
    private static final String LOG_TAG = "IdentityRateMyAppRule";
    private final Context mContext;

    public IdentityRateMyAppRule(Context context) {
        this.mContext = context != null ? context.getApplicationContext() : null;
    }

    public boolean permitsShowOfDialog() {
        if (this.mContext == null) {
            return false;
        }
        boolean isAnonymousIdentityWithEmail;
        boolean isAnonymousIdentityAndConversationsEnabled;
        boolean feedbackIsPossible;
        Identity identity = ZendeskConfig.INSTANCE.storage().identityStorage().getIdentity();
        boolean conversationsEnabled = ZendeskConfig.INSTANCE.getMobileSettings().isConversationsEnabled();
        boolean isJwt = identity instanceof JwtIdentity;
        boolean isAnonymousIdentity = identity instanceof AnonymousIdentity;
        if (isAnonymousIdentity && StringUtils.hasLength(((AnonymousIdentity) identity).getEmail())) {
            isAnonymousIdentityWithEmail = true;
        } else {
            isAnonymousIdentityWithEmail = false;
        }
        if (isAnonymousIdentity && conversationsEnabled) {
            isAnonymousIdentityAndConversationsEnabled = true;
        } else {
            isAnonymousIdentityAndConversationsEnabled = false;
        }
        if (isJwt || isAnonymousIdentityAndConversationsEnabled || isAnonymousIdentityWithEmail) {
            feedbackIsPossible = true;
        } else {
            feedbackIsPossible = false;
        }
        Logger.d(LOG_TAG, "Is Jwt: %s, is anonymous with conversations enabled: %s, is anonymous with email: %s", Boolean.valueOf(isJwt), Boolean.valueOf(isAnonymousIdentityAndConversationsEnabled), Boolean.valueOf(isAnonymousIdentityWithEmail));
        return feedbackIsPossible;
    }
}
