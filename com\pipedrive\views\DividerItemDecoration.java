package com.pipedrive.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;
import com.pipedrive.R;

public class DividerItemDecoration extends ItemDecoration {
    private static final int DEFAULT_DIVIDER_RES_ID = 2130837713;
    private final Drawable mDivider;

    public DividerItemDecoration(@NonNull Context context) {
        this(context, R.drawable.divider);
    }

    public DividerItemDecoration(@NonNull Context context, @DrawableRes int drawableResId) {
        this.mDivider = ContextCompat.getDrawable(context, drawableResId);
    }

    public void onDrawOver(Canvas c, RecyclerView parent, State state) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();
        int childCountWithDivider = getChildCountWithDivider(parent);
        for (int i = 0; i < childCountWithDivider; i++) {
            View child = parent.getChildAt(i);
            int top = child.getBottom() + ((LayoutParams) child.getLayoutParams()).bottomMargin;
            this.mDivider.setBounds(left, top, right, top + this.mDivider.getIntrinsicHeight());
            this.mDivider.draw(c);
        }
    }

    int getChildCountWithDivider(@NonNull RecyclerView parent) {
        return parent.getChildCount();
    }
}
