package com.zendesk.sdk.model.access;

public class AccessToken {
    private String accessToken;

    public String getAccessToken() {
        return this.accessToken;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AccessToken that = (AccessToken) o;
        if (this.accessToken != null) {
            if (this.accessToken.equals(that.accessToken)) {
                return true;
            }
        } else if (that.accessToken == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.accessToken != null ? this.accessToken.hashCode() : 0;
    }
}
