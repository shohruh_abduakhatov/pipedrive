package com.pipedrive.contentproviders;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.DealsDataSource.ForJoin;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.interfaces.ContactOrgInterface;
import com.pipedrive.logging.Log;
import com.pipedrive.model.Deal;

public class GlobalSearchContentProvider extends BaseContentProvider {
    private static final String AUTHORITY = "com.pipedrive.contentproviders.globalsearchcontentprovider";
    private static final String BASE_PATH = "global_search";
    private static final String COLUMN_GLOBAL_SEARCH_ITEM_TYPE = "global_search_item_type";
    private static final String COLUMN_GLOBAL_SEARCH_SORT_ORDER = "global_search_sort_order";
    private static final long COLUMN_ID_VALUE_THAT_CAN_BE_IGNORED = -1;
    public static final int GLOBAL_SEARCH_ITEM_TYPE_DEAL = 3;
    public static final int GLOBAL_SEARCH_ITEM_TYPE_ORG = 2;
    public static final int GLOBAL_SEARCH_ITEM_TYPE_PERSON = 1;
    private final String URI_PARAMETER_SEARCH_STRING = "searchfor";

    @Nullable
    @Deprecated
    public /* bridge */ /* synthetic */ Uri attachSessionIdToUri_hackMethodToEnableCPUriBuildOutsideCP_BAD_DESIGN_AND_USAGE(Uri uri) {
        return super.attachSessionIdToUri_hackMethodToEnableCPUriBuildOutsideCP_BAD_DESIGN_AND_USAGE(uri);
    }

    public /* bridge */ /* synthetic */ int delete(@NonNull Uri uri, String str, String[] strArr, @NonNull SQLiteDatabase sQLiteDatabase) {
        return super.delete(uri, str, strArr, sQLiteDatabase);
    }

    @Nullable
    public /* bridge */ /* synthetic */ String getType(@NonNull Uri uri) {
        return super.getType(uri);
    }

    @Nullable
    public /* bridge */ /* synthetic */ Uri insert(@NonNull Uri uri, ContentValues contentValues, @NonNull SQLiteDatabase sQLiteDatabase) {
        return super.insert(uri, contentValues, sQLiteDatabase);
    }

    public /* bridge */ /* synthetic */ boolean onCreate() {
        return super.onCreate();
    }

    public /* bridge */ /* synthetic */ int update(@NonNull Uri uri, ContentValues contentValues, String str, String[] strArr, @NonNull SQLiteDatabase sQLiteDatabase) {
        return super.update(uri, contentValues, str, strArr, sQLiteDatabase);
    }

    public GlobalSearchContentProvider(@NonNull Session session) {
        super(session);
    }

    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder, @NonNull SQLiteDatabase sqLiteDatabase) {
        SearchConstraint searchConstraint = SearchConstraint.fromString(uri.getQueryParameter("searchfor"));
        String unionQuery = new SQLiteQueryBuilder().buildUnionQuery(new String[]{getSearchQueryFoDeals(sqLiteDatabase, searchConstraint), getSearchQueryForPersons(sqLiteDatabase, searchConstraint), getSearchQueryForOrganizations(sqLiteDatabase, searchConstraint)}, "global_search_sort_order COLLATE NOCASE", null);
        Cursor searchCursor = !(sqLiteDatabase instanceof SQLiteDatabase) ? sqLiteDatabase.rawQuery(unionQuery, null) : SQLiteInstrumentation.rawQuery(sqLiteDatabase, unionQuery, null);
        if (!(searchCursor == null || getContext() == null)) {
            searchCursor.setNotificationUri(getContext().getContentResolver(), contentChangedUri());
        }
        return searchCursor;
    }

    @NonNull
    private String getSearchQueryFoDeals(@NonNull SQLiteDatabase sqLiteDatabase, @NonNull SearchConstraint searchConstraint) {
        DealsDataSource dealsDataSource = new DealsDataSource(sqLiteDatabase);
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables("deals LEFT JOIN persons ON persons._id = deals.d_person_id_sql LEFT JOIN organizations ON organizations._id = deals.d_org_id_sql");
        return queryBuilder.buildQuery(new String[]{"deals._id AS _id", getDealColumnStringForJoin(sqLiteDatabase), getPersonColumnStringForJoin(sqLiteDatabase), getOrganizationColumnStringForJoin(sqLiteDatabase), "3 AS global_search_item_type", "deals.d_title_search_field AS global_search_sort_order"}, dealsDataSource.getSelectionForSearch(searchConstraint).buildQueryString(), null, null, null, null);
    }

    @NonNull
    private String getSearchQueryForOrganizations(@NonNull SQLiteDatabase sqLiteDatabase, @NonNull SearchConstraint searchConstraint) {
        OrganizationsDataSource organizationsDataSource = new OrganizationsDataSource(sqLiteDatabase);
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables("organizations LEFT JOIN persons ON persons._id = -1 LEFT JOIN deals ON deals._id = -1");
        return queryBuilder.buildQuery(new String[]{"organizations._id AS _id", getDealColumnStringForJoin(sqLiteDatabase), getPersonColumnStringForJoin(sqLiteDatabase), getOrganizationColumnStringForJoin(sqLiteDatabase), "2 AS global_search_item_type", "organizations.org_name_search_field AS global_search_sort_order"}, organizationsDataSource.getSelectionForSearch(searchConstraint).buildQueryString(), null, null, null, null);
    }

    @NonNull
    private String getSearchQueryForPersons(@NonNull SQLiteDatabase sqLiteDatabase, @NonNull SearchConstraint searchConstraint) {
        PersonsDataSource personsDataSource = new PersonsDataSource(sqLiteDatabase);
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables("persons LEFT JOIN organizations ON organizations._id = persons.c_org_id_sql LEFT JOIN deals ON deals._id = -1");
        return queryBuilder.buildQuery(new String[]{"persons._id AS _id", getDealColumnStringForJoin(sqLiteDatabase), getPersonColumnStringForJoin(sqLiteDatabase), getOrganizationColumnStringForJoin(sqLiteDatabase), "1 AS global_search_item_type", "persons.c_name_search_field AS global_search_sort_order"}, personsDataSource.getSelectionForSearch(searchConstraint).buildQueryString(), null, null, null, null);
    }

    @NonNull
    private String getDealColumnStringForJoin(@NonNull SQLiteDatabase database) {
        return TextUtils.join(Table.COMMA_SEP, new ForJoin(database).getAllColumns());
    }

    @NonNull
    private String getPersonColumnStringForJoin(@NonNull SQLiteDatabase database) {
        return TextUtils.join(Table.COMMA_SEP, new PersonsDataSource.ForJoin(database).getAllColumns());
    }

    @NonNull
    private String getOrganizationColumnStringForJoin(@NonNull SQLiteDatabase database) {
        return TextUtils.join(Table.COMMA_SEP, new OrganizationsDataSource.ForJoin(database).getAllColumns());
    }

    @NonNull
    public static Deal getDeal(@NonNull Session session, Cursor cursor) {
        return new DealsDataSource(session.getDatabase()).deflateCursor(cursor);
    }

    public static int getCursorRowType(Cursor c) {
        return c.getInt(c.getColumnIndex(COLUMN_GLOBAL_SEARCH_ITEM_TYPE));
    }

    public static ContactOrgInterface getContactOrgInterfaceFromCursor(@NonNull Session session, Cursor cursor) {
        int rowType = getCursorRowType(cursor);
        if (2 == rowType) {
            try {
                return new OrganizationsDataSource(session.getDatabase()).deflateCursor(cursor);
            } catch (Exception e) {
                Log.e(new Throwable("Error getting contact from cursor", e));
            }
        } else {
            if (1 == rowType) {
                return new PersonsDataSource(session.getDatabase()).deflateCursor(cursor);
            }
            return null;
        }
    }

    @Nullable
    public Uri search(@NonNull SearchConstraint searchConstraint) {
        Builder uriBuilder = new Builder();
        uriBuilder.scheme("content").authority("com.pipedrive.contentproviders.globalsearchcontentprovider").appendPath(BASE_PATH);
        uriBuilder.appendQueryParameter("searchfor", searchConstraint.toString());
        return attachSessionIdToUri(uriBuilder.build());
    }

    public static void notifyChangesForOrganizationContentProvider(Context context) {
        if (!(context == null)) {
            context.getContentResolver().notifyChange(contentChangedUri(), null);
        }
    }

    @NonNull
    private static Uri contentChangedUri() {
        Builder builder = new Builder();
        builder.scheme("content").authority("com.pipedrive.contentproviders.globalsearchcontentprovider").appendPath("/searchdatachanged");
        return builder.build();
    }
}
