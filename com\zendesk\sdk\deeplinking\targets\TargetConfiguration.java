package com.zendesk.sdk.deeplinking.targets;

import android.content.Intent;
import java.util.ArrayList;
import java.util.List;

class TargetConfiguration {
    private final List<Intent> mBackStackActivities;
    private final DeepLinkType mDeepLinkType;
    private final Intent mFallbackActivity;

    TargetConfiguration(DeepLinkType deepLinkType, List<Intent> backStackActivities, Intent fallbackActivity) {
        this.mDeepLinkType = deepLinkType;
        this.mBackStackActivities = backStackActivities;
        this.mFallbackActivity = fallbackActivity;
    }

    public DeepLinkType getDeepLinkType() {
        return this.mDeepLinkType;
    }

    public ArrayList<Intent> getBackStackActivities() {
        if (this.mBackStackActivities != null) {
            return new ArrayList(this.mBackStackActivities);
        }
        return new ArrayList();
    }

    public Intent getFallbackActivity() {
        return this.mFallbackActivity;
    }
}
