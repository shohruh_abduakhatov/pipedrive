package com.pipedrive.model;

import android.support.annotation.Nullable;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.util.networking.entities.FlowFileEntity;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class FlowFile extends BaseFile implements AssociatedDatasourceEntity {
    public static final int FILE_UPLOAD_STATUS_REGISTERED_FOR_UPLOAD = 0;
    public static final int FILE_UPLOAD_STATUS_UPLOADING_IN_PROGRESS = 1;
    public static final int FILE_UPLOAD_STATUS_UPLOAD_FAILED = 3;
    public static final int FILE_UPLOAD_STATUS_UPLOAD_SUCCESSFUL = 2;
    private Boolean mActive;
    private Integer mActivityId;
    private PipedriveDateTime mAddTime;
    private String mComment;
    private Deal mDeal;
    private Integer mEmailMessageId;
    @Nullable
    private Integer mFileUploadStatus;
    private Boolean mInline;
    private Integer mLogId;
    private String mName;
    private Integer mNoteId;
    private Organization mOrganization;
    private String mPeopleName;
    private Person mPerson;
    private Integer mProductId;
    private String mProductName;
    private String mRemoteId;
    private String mRemoteLocation;
    private PipedriveDateTime mUpdateTime;
    private Integer mUserId;

    @Retention(RetentionPolicy.SOURCE)
    public @interface FileUploadStatus {
    }

    public static FlowFile from(FlowFileEntity entity, Deal deal, Person person, Organization organization) {
        FlowFile result = new FlowFile();
        result.setPipedriveId(entity.getPipedriveId().intValue());
        result.mUserId = entity.getUserId();
        result.mDeal = deal;
        result.mPerson = person;
        result.mOrganization = organization;
        result.mProductId = entity.getProductId();
        result.mProductName = entity.getProductName();
        result.mEmailMessageId = entity.getEmailMessageId();
        result.mActivityId = entity.getActivityId();
        result.mNoteId = entity.getNoteId();
        result.mLogId = entity.getLogId();
        result.mAddTime = PipedriveDateTime.instanceFromDate(entity.getAddTime());
        result.mUpdateTime = PipedriveDateTime.instanceFromDate(entity.getUpdateTime());
        result.setFileName(entity.getFileName());
        result.setFileType(entity.getFileType());
        result.setFileSize(entity.getFileSize());
        result.mActive = entity.isActive();
        result.mInline = entity.isInline();
        result.mComment = entity.getComment();
        result.mRemoteLocation = entity.getRemoteLocation();
        result.mRemoteId = entity.getRemoteId();
        result.mPeopleName = entity.getPeopleName();
        result.setUrl(entity.getUrl());
        result.mName = entity.getName();
        return result;
    }

    public boolean isAllAssociationsExisting() {
        if (getDeal() != null && !getDeal().isExisting()) {
            return false;
        }
        if (getPerson() != null && !getPerson().isExisting()) {
            return false;
        }
        if (getOrganization() == null || getOrganization().isExisting()) {
            return true;
        }
        return false;
    }

    public Deal getDeal() {
        return this.mDeal;
    }

    public Person getPerson() {
        return this.mPerson;
    }

    public Organization getOrganization() {
        return this.mOrganization;
    }

    public void setActive(Boolean active) {
        this.mActive = active;
    }

    public void setInline(Boolean inline) {
        this.mInline = inline;
    }

    public Integer getUserId() {
        return this.mUserId;
    }

    public void setUserId(Integer userId) {
        this.mUserId = userId;
    }

    public Integer getProductId() {
        return this.mProductId;
    }

    public void setProductId(Integer productId) {
        this.mProductId = productId;
    }

    public String getProductName() {
        return this.mProductName;
    }

    public void setProductName(String productName) {
        this.mProductName = productName;
    }

    public Integer getEmailMessageId() {
        return this.mEmailMessageId;
    }

    public void setEmailMessageId(Integer emailMessageId) {
        this.mEmailMessageId = emailMessageId;
    }

    public Integer getActivityId() {
        return this.mActivityId;
    }

    public void setActivityId(Integer activityId) {
        this.mActivityId = activityId;
    }

    public Integer getNoteId() {
        return this.mNoteId;
    }

    public void setNoteId(Integer noteId) {
        this.mNoteId = noteId;
    }

    public Integer getLogId() {
        return this.mLogId;
    }

    public void setLogId(Integer logId) {
        this.mLogId = logId;
    }

    public PipedriveDateTime getAddTime() {
        return this.mAddTime;
    }

    public void setAddTime(PipedriveDateTime addTime) {
        this.mAddTime = addTime;
    }

    public PipedriveDateTime getUpdateTime() {
        return this.mUpdateTime;
    }

    public void setUpdateTime(PipedriveDateTime updateTime) {
        this.mUpdateTime = updateTime;
    }

    public Boolean isActive() {
        return this.mActive;
    }

    public Boolean isInline() {
        return this.mInline;
    }

    public String getComment() {
        return this.mComment;
    }

    public void setComment(String comment) {
        this.mComment = comment;
    }

    public String getRemoteLocation() {
        return this.mRemoteLocation;
    }

    public void setRemoteLocation(String remoteLocation) {
        this.mRemoteLocation = remoteLocation;
    }

    public String getRemoteId() {
        return this.mRemoteId;
    }

    public void setRemoteId(String remoteId) {
        this.mRemoteId = remoteId;
    }

    public String getPeopleName() {
        return this.mPeopleName;
    }

    public void setPeopleName(String peopleName) {
        this.mPeopleName = peopleName;
    }

    public String getName() {
        return this.mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public void setDeal(Deal deal) {
        this.mDeal = deal;
    }

    public void setOrganization(Organization organization) {
        this.mOrganization = organization;
    }

    public void setPerson(Person person) {
        this.mPerson = person;
    }

    @Nullable
    public Integer getFileUploadStatus() {
        return this.mFileUploadStatus;
    }

    public void setFileUploadStatus(@Nullable Integer fileUploadStatus) {
        this.mFileUploadStatus = fileUploadStatus;
    }
}
