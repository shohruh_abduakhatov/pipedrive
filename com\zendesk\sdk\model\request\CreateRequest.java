package com.zendesk.sdk.model.request;

import android.support.annotation.Nullable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateRequest {
    private static final transient String METADATA_SDK_KEY = "sdk";
    private Comment comment;
    private List<CustomField> customFields;
    private String email;
    private String id;
    private Map<String, Map<String, String>> metadata;
    private String subject;
    private List<String> tags;
    private Long ticketFormId;

    @Nullable
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Nullable
    public List<String> getTags() {
        return this.tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    @Nullable
    public String getDescription() {
        return this.comment == null ? null : this.comment.getBody();
    }

    public void setDescription(String description) {
        if (this.comment == null) {
            this.comment = new Comment();
        }
        this.comment.setBody(description);
    }

    public void setAttachments(List<String> attachments) {
        if (this.comment == null) {
            this.comment = new Comment();
        }
        this.comment.setAttachments(attachments);
    }

    @Nullable
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Nullable
    public String getSubject() {
        return this.subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setCustomFields(List<CustomField> customFields) {
        this.customFields = customFields;
    }

    public void setMetadata(Map<String, String> metadata) {
        this.metadata = new HashMap();
        this.metadata.put("sdk", metadata);
    }

    public void setTicketFormId(Long ticketFormId) {
        this.ticketFormId = ticketFormId;
    }
}
