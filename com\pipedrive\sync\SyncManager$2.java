package com.pipedrive.sync;

import com.pipedrive.application.Session;
import com.pipedrive.tasks.authorization.OnEnoughDataToShowUI;
import rx.functions.Action1;

class SyncManager$2 implements Action1<Throwable> {
    final /* synthetic */ SyncManager this$0;
    final /* synthetic */ OnEnoughDataToShowUI val$onEnoughDataToShowUI;
    final /* synthetic */ Session val$session;

    SyncManager$2(SyncManager this$0, OnEnoughDataToShowUI onEnoughDataToShowUI, Session session) {
        this.this$0 = this$0;
        this.val$onEnoughDataToShowUI = onEnoughDataToShowUI;
        this.val$session = session;
    }

    public void call(Throwable throwable) {
        this.val$onEnoughDataToShowUI.readyToShowUI(this.val$session);
    }
}
