package com.pipedrive.util.camera;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public abstract class CameraHelper$OnCameraListener {
    public abstract boolean snapshot(@NonNull Uri uri, @Nullable String str);

    void snapshotCancelled() {
    }

    void snapshotFailed() {
    }
}
