package com.pipedrive.views.calendar;

import android.content.Context;
import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.SimpleOnItemTouchListener;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.pipedrive.R;
import com.pipedrive.views.calendar.recyclerview.CalendarRecyclerView;
import com.pipedrive.views.calendar.recyclerview.CalendarScrollListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import rx.Observable;
import rx.Observable$OnSubscribe;
import rx.Subscriber;
import rx.android.MainThreadSubscription;

public class CalendarView extends FrameLayout implements CalendarScrollListener {
    private static final int ITEM_COUNT = 7;
    @Nullable
    private CalendarViewAdapter adapter;
    @NonNull
    private Calendar currentTimePeriod;
    private boolean isWeekView;
    @NonNull
    private final List<Calendar> items;
    @NonNull
    private Locale locale;
    private boolean modeChanged;
    @Nullable
    private OnDateChangedListener onDateChangedListener;
    @Nullable
    private OnExpandOrCollapseListener onExpandOrCollapseListener;
    @Nullable
    private OnMonthChangedListener onMonthChangedListener;
    @NonNull
    private CalendarRecyclerView recyclerView;
    @Nullable
    private Calendar selectedDate;

    public CalendarView(Context context) {
        this(context, null);
    }

    public CalendarView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CalendarView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.items = new ArrayList(7);
        this.locale = Locale.getDefault();
        this.modeChanged = false;
        init();
    }

    public boolean isWeekView() {
        return this.isWeekView;
    }

    @Nullable
    public Calendar getSelectedDate() {
        return this.selectedDate;
    }

    public void setIsWeekView(boolean isWeekView) {
        this.isWeekView = isWeekView;
        this.modeChanged = true;
    }

    private void init() {
        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.dark));
        this.recyclerView = new CalendarRecyclerView(getContext());
        this.recyclerView.addOnItemTouchListener(new SimpleOnItemTouchListener() {
            final float minDeltaValue = 15.0f;
            float startXPosition = 0.0f;
            float startYPosition = 0.0f;

            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                if (e.getAction() == 0) {
                    this.startYPosition = e.getY();
                    this.startXPosition = e.getX();
                    return false;
                } else if (e.getAction() != 2 || CalendarView.this.onExpandOrCollapseListener == null) {
                    return false;
                } else {
                    float yDelta = this.startYPosition - e.getY();
                    boolean xDeltaIsLessThanMinDeltaValue;
                    if (Math.abs(this.startXPosition - e.getX()) < 15.0f) {
                        xDeltaIsLessThanMinDeltaValue = true;
                    } else {
                        xDeltaIsLessThanMinDeltaValue = false;
                    }
                    if (!CalendarView.this.isWeekView || yDelta >= 0.0f) {
                        if (CalendarView.this.isWeekView || yDelta <= 0.0f || yDelta <= 15.0f || !xDeltaIsLessThanMinDeltaValue) {
                            return false;
                        }
                        CalendarView.this.onExpandOrCollapseListener.onCollapse();
                        return true;
                    } else if (Math.abs(yDelta) <= 15.0f || !xDeltaIsLessThanMinDeltaValue) {
                        return false;
                    } else {
                        CalendarView.this.onExpandOrCollapseListener.onExpand();
                        return true;
                    }
                }
            }
        });
        addView(this.recyclerView, new LayoutParams(-1, -2));
    }

    public void setup(@NonNull Locale locale, @NonNull Calendar currentTimePeriod, @Nullable Calendar selectedDate) {
        boolean isInitialized;
        boolean onlySelectedDateChanged;
        if (this.adapter != null) {
            isInitialized = true;
        } else {
            isInitialized = false;
        }
        boolean isSameLocale = locale.equals(this.locale);
        boolean isSameMode;
        if (this.modeChanged) {
            isSameMode = false;
        } else {
            isSameMode = true;
        }
        if (isInitialized && isSameTimePeriod(currentTimePeriod) && isSameLocale && isSameMode) {
            onlySelectedDateChanged = true;
        } else {
            onlySelectedDateChanged = false;
        }
        if (onlySelectedDateChanged) {
            this.selectedDate = selectedDate;
            this.adapter.setSelectedDate(selectedDate);
            return;
        }
        this.locale = locale;
        this.currentTimePeriod = currentTimePeriod;
        this.selectedDate = selectedDate;
        Calendar pastTimePeriod = (Calendar) currentTimePeriod.clone();
        Calendar futureTimePeriod = (Calendar) currentTimePeriod.clone();
        this.items.clear();
        this.items.add(currentTimePeriod);
        if (isWeekView()) {
            setLastAndNextWeek(locale, selectedDate, pastTimePeriod, futureTimePeriod);
        } else {
            setLastAndNextMonth(locale, selectedDate, pastTimePeriod, futureTimePeriod);
        }
        if (this.adapter != null) {
            this.adapter.setInternalListener(new OnDateChangedInternalListener() {
                public void onDateChangedInternal(@NonNull Calendar date) {
                    CalendarView.this.adapter.setSelectedDate(date);
                    CalendarView.this.selectedDate = date;
                    if (CalendarView.this.onDateChangedListener != null) {
                        CalendarView.this.onDateChangedListener.onDateChanged(date);
                    }
                }
            });
            this.recyclerView.setAdapter(this.adapter);
        }
        this.modeChanged = false;
    }

    private boolean isSameTimePeriod(@NonNull Calendar newTimePeriod) {
        Calendar startOfCurrentTimePeriod = (Calendar) this.currentTimePeriod.clone();
        Calendar startOfNewTimePeriod = (Calendar) newTimePeriod.clone();
        if (this.isWeekView) {
            startOfCurrentTimePeriod.set(7, startOfCurrentTimePeriod.getFirstDayOfWeek());
            startOfNewTimePeriod.set(7, startOfNewTimePeriod.getFirstDayOfWeek());
        } else {
            startOfCurrentTimePeriod.set(5, 1);
            startOfNewTimePeriod.set(5, 1);
        }
        if (startOfCurrentTimePeriod.get(6) == startOfNewTimePeriod.get(6) && startOfCurrentTimePeriod.get(1) == startOfNewTimePeriod.get(1)) {
            return true;
        }
        return false;
    }

    private void setLastAndNextMonth(@NonNull Locale locale, @Nullable Calendar selectedDate, Calendar pastTimePeriod, Calendar futureTimePeriod) {
        for (int i = 0; i < 3; i++) {
            if (pastTimePeriod.get(2) == 0) {
                pastTimePeriod.roll(1, false);
            }
            pastTimePeriod.roll(2, false);
            this.items.add(0, (Calendar) pastTimePeriod.clone());
            if (futureTimePeriod.get(2) == 11) {
                futureTimePeriod.roll(1, true);
            }
            futureTimePeriod.roll(2, true);
            this.items.add((Calendar) futureTimePeriod.clone());
        }
        this.adapter = new MonthAdapter(locale, this.items, selectedDate);
    }

    private void setLastAndNextWeek(@NonNull Locale locale, @Nullable Calendar selectedDate, Calendar pastTimePeriod, Calendar futureTimePeriod) {
        for (int i = 0; i < 3; i++) {
            pastTimePeriod.add(5, -7);
            this.items.add(0, (Calendar) pastTimePeriod.clone());
            futureTimePeriod.add(5, 7);
            this.items.add((Calendar) futureTimePeriod.clone());
        }
        this.adapter = new WeekAdapter(locale, this.items, selectedDate);
    }

    @CheckResult
    @NonNull
    public Observable<Calendar> monthChanges() {
        return Observable.create(new Observable$OnSubscribe<Calendar>() {
            public void call(final Subscriber<? super Calendar> subscriber) {
                MainThreadSubscription.verifyMainThread();
                CalendarView.this.setOnMonthChangedListener(new OnMonthChangedListener() {
                    public void onMonthChanged(@NonNull Calendar month) {
                        if (!subscriber.isUnsubscribed()) {
                            subscriber.onNext(month);
                        }
                    }
                });
                subscriber.add(new MainThreadSubscription() {
                    protected void onUnsubscribe() {
                        CalendarView.this.setOnMonthChangedListener(null);
                    }
                });
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(CalendarView.this.currentTimePeriod);
                }
            }
        });
    }

    private void setOnMonthChangedListener(@Nullable OnMonthChangedListener onMonthChangedListener) {
        this.onMonthChangedListener = onMonthChangedListener;
    }

    @CheckResult
    @NonNull
    public Observable<Calendar> dateChanges() {
        return Observable.create(new Observable$OnSubscribe<Calendar>() {
            public void call(final Subscriber<? super Calendar> subscriber) {
                MainThreadSubscription.verifyMainThread();
                CalendarView.this.setOnDateChangedListener(new OnDateChangedListener() {
                    public void onDateChanged(@NonNull Calendar date) {
                        if (!subscriber.isUnsubscribed()) {
                            subscriber.onNext(date);
                        }
                    }
                });
                subscriber.add(new MainThreadSubscription() {
                    protected void onUnsubscribe() {
                        CalendarView.this.setOnDateChangedListener(null);
                    }
                });
            }
        });
    }

    private void setOnDateChangedListener(@Nullable OnDateChangedListener listener) {
        this.onDateChangedListener = listener;
    }

    public void setupForDate(Calendar date) {
        setup(this.locale, date, this.selectedDate);
    }

    public void onScrollToDate(Calendar date) {
        if (this.onMonthChangedListener != null) {
            this.onMonthChangedListener.onMonthChanged(date);
        }
    }

    public void setOnExpandOrCollapseListener(@NonNull OnExpandOrCollapseListener listener) {
        this.onExpandOrCollapseListener = listener;
    }
}
