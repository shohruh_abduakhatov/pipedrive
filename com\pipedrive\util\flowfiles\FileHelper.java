package com.pipedrive.util.flowfiles;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.newrelic.agent.android.instrumentation.okhttp3.OkHttp3Instrumentation;
import com.pipedrive.application.Session;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.model.BaseFile;
import com.pipedrive.model.FlowFile;
import com.pipedrive.util.networking.ApiUrlBuilder$Tools;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.Response;

public class FileHelper {
    private static final String API_URI_SEGMENT_FILE_NOT_FOUND = "file_not_found";
    private static final int BUFFER_SIZE = 8192;
    private static final String FILE_TYPE_GDOC = "gdoc";
    private static final String FILE_TYPE_GDRAW = "gdraw";
    private static final String FILE_TYPE_GFORM = "gform";
    private static final String FILE_TYPE_GSLIDES = "gslides";
    private static final String HEADER_LOCATION = "Location";
    private static final String TAG = FileHelper.class.getSimpleName();

    private FileHelper() {
    }

    public static void initialize() {
        if ("mounted".equals(Environment.getExternalStorageState())) {
            File filesStorageDirectory = getFilesStorageDirectory();
            if (!filesStorageDirectory.exists()) {
                filesStorageDirectory.mkdir();
                return;
            }
            return;
        }
        Log.d(TAG, "External media not present. Files dir will not be created");
    }

    private static String getRemoteFilePath(Session session, BaseFile baseFile) {
        return ApiUrlBuilder$Tools.appendAPITokenToURLIfMissing(baseFile.getUrl(), session);
    }

    public static String resolveFileUrl(Session session, BaseFile baseFile) {
        String result = null;
        Log.d(TAG, String.format(Locale.getDefault(), "Resolving URL for file with pipedriveId: %d", new Object[]{baseFile.getPipedriveIdOrNull()}));
        String pipedriveUrl = getRemoteFilePath(session, baseFile);
        Log.d(TAG, String.format("Initial URL: %s", new Object[]{ApiUrlBuilder$Tools.stripAPIKeyFromURL(pipedriveUrl)}));
        OkHttpClient okHttpClient = createNewHttpClient();
        try {
            Log.d(TAG, "Making request to " + ApiUrlBuilder$Tools.stripAPIKeyFromURL(pipedriveUrl));
            Builder url = new Builder().url(pipedriveUrl);
            Request build = !(url instanceof Builder) ? url.build() : OkHttp3Instrumentation.build(url);
            Response response = (!(okHttpClient instanceof OkHttpClient) ? okHttpClient.newCall(build) : OkHttp3Instrumentation.newCall(okHttpClient, build)).execute();
            if (response == null) {
                throw new IOException("Response is null for request: " + ApiUrlBuilder$Tools.stripAPIKeyFromURL(pipedriveUrl));
            }
            result = response.header("Location");
            if (!TextUtils.isEmpty(result) && API_URI_SEGMENT_FILE_NOT_FOUND.equals(Uri.parse(result).getLastPathSegment())) {
                result = null;
            }
            Log.d(TAG, String.format("File URL resolved to %s", new Object[]{result}));
            return result;
        } catch (IOException e) {
            Log.e(e);
        }
    }

    @NonNull
    private static OkHttpClient createNewHttpClient() {
        return new OkHttpClient.Builder().followRedirects(false).followSslRedirects(false).connectTimeout(10, TimeUnit.SECONDS).build();
    }

    public static boolean downloadFile(Session session, File localFile, BaseFile baseFile) {
        IOException e;
        Throwable th;
        boolean result = false;
        String fileUrl = resolveFileUrl(session, baseFile);
        if (fileUrl == null) {
            Log.e(new Throwable(String.format("Could not download file from null URL. PD URL: %s", new Object[]{baseFile.getUrl()})));
            return false;
        }
        Log.d(TAG, String.format("Opening connection for file download to %s", new Object[]{ApiUrlBuilder$Tools.stripAPIKeyFromURL(fileUrl)}));
        InputStream inputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            OkHttpClient okHttpClient = createNewHttpClient();
            Builder url = new Builder().url(fileUrl);
            Request build = !(url instanceof Builder) ? url.build() : OkHttp3Instrumentation.build(url);
            Response response = (!(okHttpClient instanceof OkHttpClient) ? okHttpClient.newCall(build) : OkHttp3Instrumentation.newCall(okHttpClient, build)).execute();
            if (!response.isSuccessful() || response.body().byteStream() == null) {
                throw new IOException(response.message());
            }
            inputStream = response.body().byteStream();
            Log.d(TAG, String.format("Opening output stream to storing file at %s", new Object[]{localFile.getAbsolutePath()}));
            FileOutputStream fileOutputStream2 = new FileOutputStream(localFile.getAbsolutePath());
            try {
                long fileSize = writeInputStreamIntoFileOutputStream(fileOutputStream2, inputStream);
                Log.d(TAG, String.format(Locale.getDefault(), "Stored %s with size %d / %d bytes", new Object[]{localFile.getAbsolutePath(), Long.valueOf(fileSize), Long.valueOf(localFile.length())}));
                result = true;
                if (fileOutputStream2 != null) {
                    try {
                        fileOutputStream2.close();
                    } catch (IOException e2) {
                    }
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                        fileOutputStream = fileOutputStream2;
                    } catch (IOException e3) {
                        fileOutputStream = fileOutputStream2;
                    }
                }
            } catch (IOException e4) {
                e = e4;
                fileOutputStream = fileOutputStream2;
                try {
                    LogJourno.reportEvent(String.format("%s:downloadFile:[%s]", new Object[]{FileHelper.class.getSimpleName(), e}));
                    Log.e(e);
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException e5) {
                        }
                    }
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e6) {
                        }
                    }
                    return result;
                } catch (Throwable th2) {
                    th = th2;
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException e7) {
                        }
                    }
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e8) {
                        }
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                fileOutputStream = fileOutputStream2;
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
                throw th;
            }
            return result;
        } catch (IOException e9) {
            e = e9;
            LogJourno.reportEvent(String.format("%s:downloadFile:[%s]", new Object[]{FileHelper.class.getSimpleName(), e}));
            Log.e(e);
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
            return result;
        }
    }

    static long writeInputStreamIntoFileOutputStream(@NonNull FileOutputStream fileOutputStream, @NonNull InputStream inputStream) throws IOException {
        byte[] buf = new byte[8192];
        long fileSize = 0;
        Log.d(TAG, "Storing file");
        while (true) {
            int count = inputStream.read(buf, 0, buf.length);
            if (count > 0) {
                fileSize += (long) count;
                fileOutputStream.write(buf, 0, count);
            } else {
                fileOutputStream.flush();
                return fileSize;
            }
        }
    }

    public static File getFilesStorageDirectory() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
    }

    static File getInternalFilesStorageDirectory(@NonNull Context context) {
        return context.getFilesDir();
    }

    public static File getLocalFile(BaseFile baseFile) {
        String fileName = baseFile.getFileName();
        if (fileName.contains(File.separator)) {
            fileName = fileName.substring(fileName.lastIndexOf(File.separator));
        }
        return new File(getFilesStorageDirectory() + File.separator + fileName);
    }

    public static boolean isGoogleDocsType(FlowFile flowFile) {
        String fileType = flowFile.getFileType();
        return FILE_TYPE_GDOC.equals(fileType) || FILE_TYPE_GDRAW.equals(fileType) || FILE_TYPE_GFORM.equals(fileType) || FILE_TYPE_GSLIDES.equals(fileType);
    }

    @Nullable
    public static File getFile(@Nullable Uri uri) {
        if (uri == null) {
            return null;
        }
        String filePath = uri.getPath();
        if (filePath != null) {
            return new File(filePath);
        }
        return null;
    }

    @Nullable
    public static String getFileName(@Nullable Uri uri) {
        String filename = null;
        if (uri != null) {
            File file = getFile(uri);
            if (file != null) {
                filename = file.getName();
            }
        }
        return filename;
    }
}
