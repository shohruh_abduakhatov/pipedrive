package com.zendesk.sdk.network;

import com.zendesk.sdk.model.settings.MobileSettings;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface SdkSettingsService {
    @GET("/api/mobile/sdk/settings/{applicationId}.json")
    Call<MobileSettings> getSettings(@Header("Accept-Language") String str, @Path("applicationId") String str2);
}
