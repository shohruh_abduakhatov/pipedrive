package com.pipedrive.model.pagination;

import com.pipedrive.util.networking.Response;
import java.util.ArrayList;
import java.util.List;

public class PageOf<T> extends Response {
    public List<T> pageOf = new ArrayList();
}
