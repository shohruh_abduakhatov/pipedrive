package com.pipedrive.datasource;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

public abstract class PipeSQLiteOpenHelper extends SQLiteOpenHelper {
    public abstract void clearDatabase(@NonNull SQLiteDatabase sQLiteDatabase);

    public PipeSQLiteOpenHelper(@NonNull Context context, @NonNull String databaseFileName, int version) {
        super(context.getApplicationContext(), databaseFileName, null, version);
    }
}
