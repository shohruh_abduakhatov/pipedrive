package com.pipedrive.views.viewholder.contact;

import android.support.annotation.NonNull;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.interfaces.ContactOrgInterface;
import com.pipedrive.model.Person;

public class PersonRowViewHolderForSearch extends PersonRowViewHolder {
    public void fill(@NonNull Session session, @NonNull ContactOrgInterface contact, @NonNull SearchConstraint searchConstraint) {
        String contactOrganizationName = null;
        if ((contact instanceof Person) && ((Person) contact).getCompany() != null) {
            contactOrganizationName = ((Person) contact).getCompany().getName();
        }
        fill(searchConstraint, contact.getName(), contactOrganizationName, contact.getPhone(), contact.getSqlId() > 0 ? Long.valueOf(contact.getSqlId()) : null);
        this.mProfilePictureRoundPerson.loadPicture(session, contact);
    }

    public int getLayoutResourceId() {
        return R.layout.row_person_search;
    }
}
