package com.pipedrive.util.devices;

import com.google.firebase.iid.FirebaseInstanceId;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import rx.Completable;
import rx.functions.Func1;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u000e\u0010\u0003\u001a\n \u0002*\u0004\u0018\u00010\u00040\u0004H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "Lrx/Completable;", "kotlin.jvm.PlatformType", "deviceEntity", "Lcom/pipedrive/util/devices/DeviceEntity;", "call"}, k = 3, mv = {1, 1, 6})
/* compiled from: MobileDevices.kt */
final class MobileDevices$loginDevice$2<T, R> implements Func1<DeviceEntity, Completable> {
    final /* synthetic */ MobileDevices this$0;

    MobileDevices$loginDevice$2(MobileDevices mobileDevices) {
        this.this$0 = mobileDevices;
    }

    public final Completable call(DeviceEntity deviceEntity) {
        String devicePipedriveId = deviceEntity.getPipedriveId();
        if (devicePipedriveId == null) {
            Intrinsics.throwNpe();
        }
        return this.this$0.api.login(devicePipedriveId).andThen(this.this$0.api.update(devicePipedriveId, DeviceEntity.copy$default(deviceEntity, null, null, FirebaseInstanceId.getInstance().getToken(), null, 11, null)));
    }
}
