package com.zendesk.sdk.deeplinking.targets;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.TaskStackBuilder;
import com.zendesk.sdk.model.helpcenter.Article;
import com.zendesk.sdk.model.helpcenter.SimpleArticle;
import com.zendesk.sdk.support.ViewArticleActivity;

public class DeepLinkingTargetArticle extends DeepLinkingTarget<ArticleConfiguration> {
    public static String EXTRA_ARTICLE = "extra_article";
    public static String EXTRA_ARTICLE_SIMPLE = "extra_article_simple";

    void openTargetActivity(Context context, @NonNull ArticleConfiguration configuration) {
        Intent intent = new Intent(context, ViewArticleActivity.class);
        if (configuration.getArticle() != null) {
            intent.putExtra(ViewArticleActivity.EXTRA_ARTICLE, configuration.getArticle());
        } else if (configuration.getSimpleArticle() != null) {
            intent.putExtra(ViewArticleActivity.EXTRA_SIMPLE_ARTICLE, configuration.getSimpleArticle());
        } else {
            return;
        }
        TaskStackBuilder taskStackBuilder = DeepLinkingTarget.getTaskStackBuilder(context, configuration.getBackStackActivities());
        taskStackBuilder.addParentStack(ViewArticleActivity.class);
        taskStackBuilder.addNextIntent(intent);
        taskStackBuilder.startActivities();
    }

    ArticleConfiguration extractConfiguration(@NonNull Bundle bundle, TargetConfiguration targetConfiguration) {
        if (bundle.containsKey(EXTRA_ARTICLE) && (bundle.getSerializable(EXTRA_ARTICLE) instanceof Article)) {
            return new ArticleConfiguration((Article) bundle.getSerializable(EXTRA_ARTICLE), targetConfiguration.getBackStackActivities(), targetConfiguration.getFallbackActivity());
        }
        if (bundle.containsKey(EXTRA_ARTICLE_SIMPLE) && (bundle.getSerializable(EXTRA_ARTICLE_SIMPLE) instanceof SimpleArticle)) {
            return new ArticleConfiguration((SimpleArticle) bundle.getSerializable(EXTRA_ARTICLE_SIMPLE), targetConfiguration.getBackStackActivities(), targetConfiguration.getFallbackActivity());
        }
        return null;
    }

    @NonNull
    Bundle getExtra(ArticleConfiguration configuration) {
        Bundle bundle = new Bundle();
        if (configuration.getArticle() != null) {
            bundle.putSerializable(EXTRA_ARTICLE, configuration.getArticle());
        } else if (configuration.getSimpleArticle() != null) {
            bundle.putSerializable(EXTRA_ARTICLE_SIMPLE, configuration.getSimpleArticle());
        }
        return bundle;
    }
}
