package rx;

class Single$1 implements Single$OnSubscribe<T> {
    final /* synthetic */ Throwable val$exception;

    Single$1(Throwable th) {
        this.val$exception = th;
    }

    public void call(SingleSubscriber<? super T> te) {
        te.onError(this.val$exception);
    }
}
