package com.pipedrive.fragments.customfields;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.pipedrive.R;
import java.util.List;
import rx.Observable;
import rx.functions.Func2;

public class CustomFieldEditMultipleOptionsFragment extends CustomFieldBaseFragment {
    @NonNull
    private AdapterCustomFieldMultipleOptions mAdapterCustomFieldMultipleOptions;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_custom_field_edit_multiple_options, container, false);
        ListView optionsList = (ListView) mainView.findViewById(R.id.listViewMultipleOptions);
        this.mAdapterCustomFieldMultipleOptions = AdapterCustomFieldMultipleOptions.getAdapter(inflater.getContext(), this.mCustomField.getOptions(), getInitiallySelectedOptionsList());
        optionsList.setAdapter(this.mAdapterCustomFieldMultipleOptions);
        return mainView;
    }

    @NonNull
    private List<String> getInitiallySelectedOptionsList() {
        String currentlySelectedOptionIds = this.mCustomField.getTempValue();
        return (List) Observable.from(currentlySelectedOptionIds == null ? new String[0] : currentlySelectedOptionIds.split(Table.COMMA_SEP)).toList().toBlocking().single();
    }

    protected void clearContent() {
    }

    protected void saveContent() {
        this.mCustomField.setTempValue(gatherAllSelectedOptionsForSaveFormat(this.mAdapterCustomFieldMultipleOptions.getSelectedOptionIds()));
    }

    @NonNull
    private String gatherAllSelectedOptionsForSaveFormat(@NonNull List<String> selectedOptions) {
        return (String) Observable.from((Iterable) selectedOptions).defaultIfEmpty("").reduce(new Func2<String, String, String>() {
            public String call(String s1, String s2) {
                return s1.concat(Table.COMMA_SEP).concat(s2);
            }
        }).toBlocking().single();
    }
}
