package com.zendesk.util;

import com.pipedrive.datasource.PipeSQLiteSharedHelper.Table;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kotlin.text.Typography;

public class StringUtils {
    public static final String EMPTY_STRING = "";
    private static final String ISO_8601_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    private static Map<Character, String> JS_ESCAPE_LOOKUP_MAP = new HashMap();
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");

    static {
        JS_ESCAPE_LOOKUP_MAP.put(Character.valueOf('\''), "\\'");
        JS_ESCAPE_LOOKUP_MAP.put(Character.valueOf(Typography.quote), "\\\"");
        JS_ESCAPE_LOOKUP_MAP.put(Character.valueOf('\\'), "\\\\");
        JS_ESCAPE_LOOKUP_MAP.put(Character.valueOf('/'), "\\/");
        JS_ESCAPE_LOOKUP_MAP.put(Character.valueOf('\b'), "\\b");
        JS_ESCAPE_LOOKUP_MAP.put(Character.valueOf('\n'), "\\n");
        JS_ESCAPE_LOOKUP_MAP.put(Character.valueOf('\t'), "\\t");
        JS_ESCAPE_LOOKUP_MAP.put(Character.valueOf('\f'), "\\f");
        JS_ESCAPE_LOOKUP_MAP.put(Character.valueOf('\r'), "\\r");
    }

    private StringUtils() {
    }

    public static boolean hasLength(String s) {
        return s != null && s.trim().length() > 0;
    }

    public static boolean hasLengthMany(String... strings) {
        if (strings == null || strings.length == 0) {
            return false;
        }
        for (String currentString : strings) {
            if (isEmpty(currentString)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isEmpty(String s) {
        return !hasLength(s);
    }

    public static String toCsvString(String... values) {
        return toCsvString(values == null ? null : Arrays.asList(values));
    }

    public static String toCsvString(List<String> values) {
        if (values == null) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < values.size(); i++) {
            if (hasLength((String) values.get(i))) {
                stringBuilder.append((String) values.get(i));
                if (i < values.size() - 1) {
                    stringBuilder.append(Table.COMMA_SEP);
                }
            }
        }
        return stringBuilder.toString();
    }

    public static String toCsvStringNumber(Number... values) {
        return toCsvStringNumber(values == null ? null : Arrays.asList(values));
    }

    public static String toCsvStringNumber(List<? extends Number> values) {
        List numbersAsStrings = null;
        if (values != null) {
            numbersAsStrings = new ArrayList();
            for (Number number : values) {
                if (number != null) {
                    numbersAsStrings.add(number.toString());
                }
            }
        }
        return toCsvString(numbersAsStrings);
    }

    public static List<String> fromCsv(String csvString) {
        if (!hasLength(csvString)) {
            return CollectionUtils.unmodifiableList(new ArrayList(0));
        }
        String[] values = csvString.split(Table.COMMA_SEP);
        List<String> list = new ArrayList();
        for (String string : values) {
            if (hasLength(string)) {
                list.add(string);
            }
        }
        return CollectionUtils.unmodifiableList(list);
    }

    public static String toCsvString(long... values) {
        if (values == null) {
            return null;
        }
        List valuesList = new ArrayList();
        for (long value : values) {
            valuesList.add(Long.valueOf(value));
        }
        return toCsvStringNumber(valuesList);
    }

    public static String toDateInIsoFormat(Date dateToFormat) {
        if (dateToFormat != null) {
            return new SimpleDateFormat(ISO_8601_DATE_FORMAT).format(dateToFormat);
        }
        return "";
    }

    public static String escapeEcmaScript(String input) {
        if (isEmpty(input)) {
            return input;
        }
        StringBuilder escapedString = new StringBuilder(input.length() * 2);
        int len = input.length();
        for (int i = 0; i < len; i++) {
            char c = input.charAt(i);
            if (JS_ESCAPE_LOOKUP_MAP.containsKey(Character.valueOf(c))) {
                escapedString.append((String) JS_ESCAPE_LOOKUP_MAP.get(Character.valueOf(c)));
            } else {
                escapedString.append(c);
            }
        }
        return escapedString.toString();
    }

    public static String capitalize(String input) {
        if (!hasLength(input)) {
            return input == null ? null : input;
        } else {
            if (Character.isUpperCase(input.charAt(0))) {
                return input;
            }
            StringBuilder stringBuilder = new StringBuilder(input.length());
            stringBuilder.append(Character.toTitleCase(input.charAt(0)));
            stringBuilder.append(input.substring(1));
            return stringBuilder.toString();
        }
    }

    public static boolean isNumeric(String input) {
        if (isEmpty(input)) {
            return false;
        }
        int inputLength = input.length();
        for (int i = 0; i < inputLength; i++) {
            if (!Character.isDigit(input.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static boolean startsWithIdeographic(String input) {
        if (hasLength(input)) {
            return Character.isIdeographic(input.codePointAt(0));
        }
        return false;
    }
}
