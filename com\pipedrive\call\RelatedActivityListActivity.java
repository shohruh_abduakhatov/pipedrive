package com.pipedrive.call;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import butterknife.ButterKnife;
import com.pipedrive.BaseActivity;
import com.pipedrive.R;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.util.CursorHelper;

public class RelatedActivityListActivity extends BaseActivity {
    private static final String EXTRA_ACTIVITY_SQL_ID = "activitySqlId";
    private static final String EXTRA_CALL_DURATION_IN_MILLIS = "callDurationInMillis";
    private static final String EXTRA_CALL_START_TIME_IN_MILLIS = "callStartTimeInMillis";
    private static final String EXTRA_PERSON_SQL_ID = "personSqlId";
    private static final String EXTRA_RELATED_DEAL_SQL_ID = "relatedDealSqlId";

    public static void startInNewTask(@NonNull Context context, @NonNull Long personSqlId, @Nullable Long relatedDealSqlId, @Nullable Long activitySqlId, @Nullable Long callDurationInMillis, @Nullable Long callStartTimeInMillis) {
        context.startActivity(new Intent(context, RelatedActivityListActivity.class).addFlags(268435456).putExtra(EXTRA_PERSON_SQL_ID, personSqlId).putExtra(EXTRA_RELATED_DEAL_SQL_ID, relatedDealSqlId).putExtra(EXTRA_ACTIVITY_SQL_ID, activitySqlId).putExtra(EXTRA_CALL_DURATION_IN_MILLIS, callDurationInMillis).putExtra(EXTRA_CALL_START_TIME_IN_MILLIS, callStartTimeInMillis));
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (((Long) getIntent().getSerializableExtra(EXTRA_ACTIVITY_SQL_ID)) != null) {
            startCallSummaryForExistingActivity(false);
            finish();
            return;
        }
        Cursor cursor = getUndoneCallActivitiesRelatedToPerson();
        if (!cursor.moveToFirst() || cursor.getCount() == 0) {
            createNewCallActivity(false);
            finish();
            return;
        }
        setContentView((int) R.layout.activity_related_activity_list);
        setupListView(cursor);
    }

    private void setupListView(Cursor cursor) {
        ListView listView = (ListView) ButterKnife.findById((Activity) this, (int) R.id.list);
        setupHeaderAndFooters(listView);
        listView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RelatedActivityListActivity.this.getIntent().putExtra(RelatedActivityListActivity.EXTRA_ACTIVITY_SQL_ID, CursorHelper.getLong((Cursor) parent.getAdapter().getItem(position), PipeSQLiteHelper.COLUMN_ID));
                RelatedActivityListActivity.this.startCallSummaryForExistingActivity(true);
            }
        });
        listView.setAdapter(new RelatedActivityListAdapter(getSession(), this, cursor));
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            finish();
        }
    }

    private void setupHeaderAndFooters(ListView listView) {
        View headerView = getLayoutInflater().inflate(R.layout.row_related_activity_list_header, listView, false);
        View addNewFooterView = getLayoutInflater().inflate(R.layout.row_related_activity_list_footer_add_new_activity, listView, false);
        View dontLogFooterView = getLayoutInflater().inflate(R.layout.row_related_activity_list_footer_dont_log_this_call, listView, false);
        listView.addHeaderView(headerView, null, false);
        listView.addFooterView(addNewFooterView, null, true);
        listView.addFooterView(dontLogFooterView, null, false);
        addNewFooterView.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                RelatedActivityListActivity.this.createNewCallActivity(true);
            }
        });
        dontLogFooterView.findViewById(R.id.dont_log_this_call).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                RelatedActivityListActivity.this.finish();
            }
        });
    }

    private Cursor getUndoneCallActivitiesRelatedToPerson() {
        return new ActivitiesDataSource(getSession()).getUndoneCallActiveActivitiesRelatedToPerson((Long) getIntent().getSerializableExtra(EXTRA_PERSON_SQL_ID));
    }

    private void createNewCallActivity(boolean canGoBack) {
        CallSummaryActivity.startActivityForPerson(this, Long.valueOf(getIntent().getLongExtra(EXTRA_PERSON_SQL_ID, 0)), Long.valueOf(getIntent().getLongExtra(EXTRA_RELATED_DEAL_SQL_ID, 0)), Long.valueOf(getIntent().getLongExtra(EXTRA_CALL_DURATION_IN_MILLIS, 0)), Long.valueOf(getIntent().getLongExtra(EXTRA_CALL_START_TIME_IN_MILLIS, 0)), canGoBack);
    }

    private void startCallSummaryForExistingActivity(boolean canGoBack) {
        CallSummaryActivity.startForExistingActivity(this, Long.valueOf(getIntent().getLongExtra(EXTRA_ACTIVITY_SQL_ID, 0)), Long.valueOf(getIntent().getLongExtra(EXTRA_CALL_DURATION_IN_MILLIS, 0)), Long.valueOf(getIntent().getLongExtra(EXTRA_CALL_START_TIME_IN_MILLIS, 0)), canGoBack);
    }
}
