package com.pipedrive.datetime;

import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.gson.UtcDateTypeAdapter;
import com.pipedrive.util.time.TimeManager;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class PipedriveTime {
    private final long timeInMilliseconds;

    protected PipedriveTime(long timeInMilliseconds) {
        this.timeInMilliseconds = timeInMilliseconds;
    }

    @NonNull
    public static PipedriveTime instanceWithCurrentTimeInUtc() {
        Calendar calendarNow = TimeManager.getInstance().getUtcCalendar();
        return new PipedriveTime((TimeUnit.HOURS.toMillis((long) calendarNow.get(11)) + TimeUnit.MINUTES.toMillis((long) calendarNow.get(12))) + TimeUnit.SECONDS.toMillis((long) calendarNow.get(13)));
    }

    @Nullable
    public static PipedriveTime instanceFormApiLongRepresentation(@Nullable String time) {
        if (time == null) {
            return null;
        }
        return instanceFromDate(UtcDateTypeAdapter.parseTime(time));
    }

    @Nullable
    public static PipedriveTime instanceFormApiShortRepresentation(@Nullable String time) {
        if (time == null) {
            return null;
        }
        return instanceFromDate(UtcDateTypeAdapter.parseTime(time));
    }

    @Nullable
    protected static PipedriveTime instanceFromDate(@Nullable Date date) {
        if (date == null) {
            return null;
        }
        return new PipedriveTime(date.getTime());
    }

    @Nullable
    public static PipedriveTime instanceFromDateTakeTimeOnly(@Nullable Date date, boolean skipSeconds) {
        if (date == null) {
            return null;
        }
        long timeComponentHHMMSSInSeconds = TimeUnit.MILLISECONDS.toSeconds(date.getTime()) % TimeUnit.HOURS.toSeconds(24);
        if (skipSeconds) {
            return instanceFromUnixTimeRepresentation(Long.valueOf(timeComponentHHMMSSInSeconds - (timeComponentHHMMSSInSeconds % 60)));
        }
        return instanceFromUnixTimeRepresentation(Long.valueOf(timeComponentHHMMSSInSeconds));
    }

    @Nullable
    public static PipedriveTime instanceFromDateTakeTimeOnly(@Nullable Date date) {
        return instanceFromDateTakeTimeOnly(date, false);
    }

    @Nullable
    public static PipedriveTime instanceFromUnixTimeRepresentation(@Nullable @IntRange(from = 0, to = Long.MAX_VALUE) Long onlyTimeUnixTime) {
        if (onlyTimeUnixTime != null && onlyTimeUnixTime.longValue() >= 0) {
            return new PipedriveTime(TimeUnit.SECONDS.toMillis(onlyTimeUnixTime.longValue()));
        }
        return null;
    }

    @NonNull
    public static PipedriveTime from(@IntRange(from = 0) int hourOfDay, @IntRange(from = 0) int minute, @IntRange(from = 0) int seconds) {
        return new PipedriveTime((TimeUnit.HOURS.toMillis((long) hourOfDay) + TimeUnit.MINUTES.toMillis((long) minute)) + TimeUnit.SECONDS.toMillis((long) seconds));
    }

    @NonNull
    public static PipedriveTime from(@IntRange(from = 0) int hourOfDay, @IntRange(from = 0) int minute) {
        return from(hourOfDay, minute, 0);
    }

    public long getRepresentationInUnixTime() {
        return TimeUnit.MILLISECONDS.toSeconds(this.timeInMilliseconds);
    }

    @NonNull
    public String getRepresentationForApiLongRepresentation() {
        return getRepresentationForApi(true);
    }

    @NonNull
    public String getRepresentationForApiShortRepresentation() {
        return getRepresentationForApi(false);
    }

    @NonNull
    public Calendar toCalendar(@NonNull PipedriveDate dateForTimeConversion) {
        Calendar calendar = TimeManager.getInstance().getUtcCalendar();
        calendar.setTimeInMillis(TimeUnit.SECONDS.toMillis(dateForTimeConversion.instanceClone().getRepresentationInUnixTime()) + this.timeInMilliseconds);
        return calendar;
    }

    public String toString() {
        return getRepresentationForApi(true);
    }

    private String getRepresentationForApi(boolean includeSeconds) {
        long timeInSeconds = getRepresentationInUnixTime();
        long hours = 0;
        if (timeInSeconds > 0) {
            hours = TimeUnit.SECONDS.toHours(timeInSeconds);
            timeInSeconds -= TimeUnit.HOURS.toSeconds(hours);
        }
        long minutes = 0;
        if (timeInSeconds > 0) {
            minutes = TimeUnit.SECONDS.toMinutes(timeInSeconds);
            timeInSeconds -= TimeUnit.MINUTES.toSeconds(minutes);
        }
        long seconds = 0;
        if (timeInSeconds > 0) {
            seconds = TimeUnit.SECONDS.toSeconds(timeInSeconds);
        }
        String hoursNormalized = normalizeTimeUnit(hours);
        String minutesNormalized = normalizeTimeUnit(minutes);
        StringBuilder representationForApi = new StringBuilder(String.format("%s:%s", new Object[]{hoursNormalized, minutesNormalized}));
        if (includeSeconds) {
            representationForApi.append(":");
            representationForApi.append(normalizeTimeUnit(seconds));
        }
        return representationForApi.toString();
    }

    private String normalizeTimeUnit(long timeUnitInInt) {
        if (timeUnitInInt <= 0) {
            return "00";
        }
        if (timeUnitInInt < 10) {
            return "0" + timeUnitInInt;
        }
        return String.valueOf(timeUnitInInt);
    }
}
