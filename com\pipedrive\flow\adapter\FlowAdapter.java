package com.pipedrive.flow.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.flow.model.FlowDataSource;
import com.pipedrive.flow.model.FlowItemEntity;
import com.pipedrive.flow.views.FlowView.OnItemClickListener;
import com.pipedrive.util.networking.entities.FlowItem;
import com.pipedrive.views.viewholder.activity.ActivityRowViewHolderWithCheckBoxForFlow;
import com.pipedrive.views.viewholder.flow.EmailRowViewHolder;
import com.pipedrive.views.viewholder.flow.FileRowViewHolder;
import com.pipedrive.views.viewholder.flow.NoteRowViewHolder;

public class FlowAdapter extends Adapter {
    private static final int TYPE_ACTIVITY = 0;
    private static final int TYPE_ADD_NEW_ACTIVITY = 5;
    private static final int TYPE_EMAIL = 2;
    private static final int TYPE_FILE = 3;
    private static final int TYPE_INVALID = -1;
    private static final int TYPE_NOTE = 1;
    private static final int TYPE_SECTION_HEADER = 4;
    @NonNull
    private final FlowDataSource mFlowDataSource;
    @Nullable
    private OnItemClickListener mOnItemClickListener;
    @NonNull
    private Cursor mPastCursor;
    private int mPastHeaderPosition;
    @NonNull
    private Cursor mPlannedCursor;
    private final int mPlannedHeaderPosition = 0;
    @NonNull
    private final Session mSession;

    private static class AddNewActivityViewHolder extends FlowItemViewHolder {
        public AddNewActivityViewHolder(View itemView) {
            super(itemView);
        }

        public void fill(@NonNull Session session, @NonNull Context context, @NonNull Object o) {
        }
    }

    private static class FlowSectionViewHolder extends FlowItemViewHolder<FlowItemEntity> {
        private final TextView mTitle;

        public FlowSectionViewHolder(View view) {
            super(view);
            this.mTitle = (TextView) ButterKnife.findById(view, (int) R.id.rowFlowSectionTimeDefinition);
        }

        public void fill(@NonNull Session session, @NonNull Context context, @NonNull FlowItemEntity flowItemEntity) {
            this.mTitle.setText(flowItemEntity.getTitle());
        }
    }

    public FlowAdapter(@NonNull Session session, @NonNull Cursor plannedCursor, @NonNull Cursor pastCursor) {
        this.mSession = session;
        this.mFlowDataSource = new FlowDataSource(session);
        replaceCursors(plannedCursor, pastCursor);
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0:
                return RecyclerViewHolder.wrapRowViewHolder(parent.getContext(), parent, new ActivityRowViewHolderWithCheckBoxForFlow());
            case 1:
                return RecyclerViewHolder.wrapRowViewHolder(parent.getContext(), parent, new NoteRowViewHolder());
            case 2:
                return RecyclerViewHolder.wrapRowViewHolder(parent.getContext(), parent, new EmailRowViewHolder());
            case 3:
                return RecyclerViewHolder.wrapRowViewHolder(parent.getContext(), parent, new FileRowViewHolder());
            case 4:
                return new FlowSectionViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_flow_section_time, parent, false));
            case 5:
                return new AddNewActivityViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_flow_section_add_new_activity, parent, false));
            default:
                return null;
        }
    }

    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (holder != null) {
            Context context = holder.itemView.getContext();
            switch (getItemViewType(position)) {
                case 0:
                    FlowItemEntity activityItem = this.mFlowDataSource.deflateCursor(position < this.mPastHeaderPosition ? this.mPlannedCursor : this.mPastCursor);
                    ActivityRowViewHolderWithCheckBoxForFlow activityRowViewHolderWithCheckBoxForFlow = (ActivityRowViewHolderWithCheckBoxForFlow) ((RecyclerViewHolder) holder).getViewHolder();
                    if (activityRowViewHolderWithCheckBoxForFlow != null) {
                        activityRowViewHolderWithCheckBoxForFlow.fill(this.mSession, context, activityItem, new OnCheckedChangeListener() {
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                long itemSqlId = FlowAdapter.this.getItemId(holder.getAdapterPosition());
                                if (FlowAdapter.this.mOnItemClickListener != null) {
                                    FlowAdapter.this.mOnItemClickListener.onActivityDoneChanged(itemSqlId, isChecked);
                                }
                            }
                        });
                        break;
                    }
                    break;
                case 1:
                    FlowItemEntity noteItem = this.mFlowDataSource.deflateCursor(this.mPastCursor);
                    NoteRowViewHolder viewHolder = (NoteRowViewHolder) ((RecyclerViewHolder) holder).getViewHolder();
                    if (viewHolder != null) {
                        viewHolder.fill(this.mSession, noteItem);
                        break;
                    }
                    break;
                case 2:
                    FlowItemEntity emailItem = this.mFlowDataSource.deflateCursor(this.mPastCursor);
                    EmailRowViewHolder emailRowViewHolder = (EmailRowViewHolder) ((RecyclerViewHolder) holder).getViewHolder();
                    if (emailRowViewHolder != null) {
                        emailRowViewHolder.fill(context, emailItem);
                        break;
                    }
                    break;
                case 3:
                    FlowItemEntity fileItem = this.mFlowDataSource.deflateCursor(this.mPastCursor);
                    FileRowViewHolder fileRowViewHolder = (FileRowViewHolder) ((RecyclerViewHolder) holder).getViewHolder();
                    if (fileRowViewHolder != null) {
                        fileRowViewHolder.fill(this.mSession, fileItem);
                        break;
                    }
                    break;
                case 4:
                    ((FlowSectionViewHolder) holder).fill(this.mSession, context, new FlowItemEntity(context.getResources().getString(position == this.mPastHeaderPosition ? R.string.flow_list_section_past : R.string.flow_list_section_upcoming)));
                    break;
            }
            holder.itemView.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    int adapterPosition = holder.getAdapterPosition();
                    long itemSqlId = FlowAdapter.this.getItemId(adapterPosition);
                    switch (FlowAdapter.this.getItemViewType(adapterPosition)) {
                        case 0:
                            if (FlowAdapter.this.mOnItemClickListener != null) {
                                FlowAdapter.this.mOnItemClickListener.onActivityClicked(itemSqlId);
                                return;
                            }
                            return;
                        case 1:
                            if (FlowAdapter.this.mOnItemClickListener != null) {
                                FlowAdapter.this.mOnItemClickListener.onNoteClicked(itemSqlId);
                                return;
                            }
                            return;
                        case 2:
                            if (FlowAdapter.this.mOnItemClickListener != null) {
                                FlowAdapter.this.mOnItemClickListener.onEmailClicked(itemSqlId);
                                return;
                            }
                            return;
                        case 3:
                            if (FlowAdapter.this.mOnItemClickListener != null) {
                                FlowAdapter.this.mOnItemClickListener.onFileClicked(itemSqlId);
                                return;
                            }
                            return;
                        case 5:
                            if (FlowAdapter.this.mOnItemClickListener != null) {
                                FlowAdapter.this.mOnItemClickListener.onNewActivityClicked();
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                }
            });
        }
    }

    public int getItemCount() {
        boolean noPlannedItems;
        boolean noPastItems;
        int pastSectionItems = 0;
        int plannedItemsCount = this.mPlannedCursor.getCount();
        if (plannedItemsCount == 0) {
            noPlannedItems = true;
        } else {
            noPlannedItems = false;
        }
        if (noPlannedItems) {
            plannedItemsCount = 1;
        }
        int plannedSectionItems = plannedItemsCount + 1;
        int pastItemsCount = this.mPastCursor.getCount();
        if (pastItemsCount == 0) {
            noPastItems = true;
        } else {
            noPastItems = false;
        }
        if (!noPastItems) {
            pastSectionItems = pastItemsCount + 1;
        }
        return plannedSectionItems + pastSectionItems;
    }

    public int getItemViewType(int position) {
        if (position == 0 || position == this.mPastHeaderPosition) {
            return 4;
        }
        if (position == 1 && this.mPlannedCursor.getCount() == 0) {
            return 5;
        }
        String typeString;
        if (position < Math.max(this.mPlannedCursor.getCount() + 1, 2)) {
            this.mPlannedCursor.moveToPosition(position - 1);
            typeString = this.mFlowDataSource.getTypeString(this.mPlannedCursor);
        } else {
            this.mPastCursor.moveToPosition((position - this.mPastHeaderPosition) - 1);
            typeString = this.mFlowDataSource.getTypeString(this.mPastCursor);
        }
        if (FlowItem.ITEM_TYPE_OBJECT_ACTIVITY.equals(typeString)) {
            return 0;
        }
        if (FlowItem.ITEM_TYPE_OBJECT_NOTE.equals(typeString)) {
            return 1;
        }
        if (FlowItem.ITEM_TYPE_OBJECT_EMAILMESSAGE.equals(typeString)) {
            return 2;
        }
        if (FlowItem.ITEM_TYPE_OBJECT_FILE.equals(typeString)) {
            return 3;
        }
        return -1;
    }

    public long getItemId(int position) {
        FlowItemEntity flowItemEntity = null;
        switch (getItemViewType(position)) {
            case 0:
                flowItemEntity = this.mFlowDataSource.deflateCursor(position < this.mPastHeaderPosition ? this.mPlannedCursor : this.mPastCursor);
                break;
            case 1:
            case 2:
            case 3:
                flowItemEntity = this.mFlowDataSource.deflateCursor(this.mPastCursor);
                break;
        }
        if (flowItemEntity == null) {
            return 0;
        }
        return flowItemEntity.getSqlId().longValue();
    }

    public void setOnItemClickListener(@Nullable OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    public void replaceCursors(@NonNull Cursor plannedFlowCursor, @NonNull Cursor pastFlowCursor) {
        this.mPlannedCursor = plannedFlowCursor;
        this.mPlannedCursor.moveToFirst();
        this.mPastCursor = pastFlowCursor;
        this.mPastCursor.moveToFirst();
        this.mPastHeaderPosition = Math.max(this.mPlannedCursor.getCount(), 1) + 1;
    }
}
