package com.pipedrive.views.navigation;

public enum NavigationToolbarButton {
    PIPELINE(0),
    ACTIVITIES(1),
    CONTACTS(2),
    SEARCH(3),
    SETTINGS(4);
    
    int index;

    private NavigationToolbarButton(int index) {
        this.index = index;
    }
}
