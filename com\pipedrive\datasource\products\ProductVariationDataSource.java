package com.pipedrive.datasource.products;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.datasource.BaseChildDataSource;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.model.ChildDataSourceEntity;
import com.pipedrive.model.products.Product;
import com.pipedrive.model.products.ProductVariation;
import com.pipedrive.util.CursorHelper;

public class ProductVariationDataSource extends BaseChildDataSource<ProductVariation> {
    private static final String[] ALL_COLUMNS = new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_PRODUCT_VARIATIONS_PIPEDRIVE_ID, PipeSQLiteHelper.COLUMN_PRODUCT_VARIATIONS_PRODUCT_SQL_ID, PipeSQLiteHelper.COLUMN_PRODUCT_VARIATIONS_NAME};

    public ProductVariationDataSource(SQLiteDatabase dbconn) {
        super(dbconn);
    }

    @NonNull
    protected String getColumnNameForParentSqlId() {
        return PipeSQLiteHelper.COLUMN_PRODUCT_VARIATIONS_PRODUCT_SQL_ID;
    }

    @NonNull
    protected ContentValues getContentValues(@NonNull ProductVariation productVariation) {
        ContentValues contentValues = super.getContentValues((ChildDataSourceEntity) productVariation);
        CursorHelper.put(productVariation.getName(), contentValues, PipeSQLiteHelper.COLUMN_PRODUCT_VARIATIONS_NAME);
        return contentValues;
    }

    @Nullable
    protected ProductVariation deflateCursor(@NonNull Cursor cursor, @Nullable Long sqlId, @Nullable Long pipedriveId, @Nullable Long parentSqlId) {
        String name = CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_PRODUCT_VARIATIONS_NAME);
        if (name == null) {
            return null;
        }
        return ProductVariation.create(sqlId, pipedriveId, parentSqlId, name, new ProductVariationPriceDataSource(getTransactionalDBConnection()).findAllRelatedToParentSqlId(sqlId));
    }

    protected void onSuccessfulCreateOrUpdate(@NonNull ProductVariation productVariation) {
        new ProductVariationPriceDataSource(getTransactionalDBConnection()).createOrUpdateProductVariation(productVariation);
    }

    @NonNull
    protected String getColumnNameForPipedriveId() {
        return PipeSQLiteHelper.COLUMN_PRODUCT_VARIATIONS_PIPEDRIVE_ID;
    }

    @NonNull
    protected String getColumnNameForSqlId() {
        return PipeSQLiteHelper.COLUMN_ID;
    }

    @NonNull
    protected String getTableName() {
        return PipeSQLiteHelper.TABLE_PRODUCT_VARIATIONS;
    }

    @NonNull
    protected String[] getAllColumns() {
        return ALL_COLUMNS;
    }

    void createOrUpdateProduct(@NonNull Product product) {
        createOrUpdate(product.getProductVariations(), product);
    }
}
