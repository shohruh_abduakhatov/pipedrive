package com.google.android.gms.wearable.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.List;

public class GetConnectedNodesResponse extends AbstractSafeParcelable {
    public static final Creator<GetConnectedNodesResponse> CREATOR = new zzaq();
    public final List<NodeParcelable> aTV;
    public final int statusCode;
    public final int versionCode;

    GetConnectedNodesResponse(int i, int i2, List<NodeParcelable> list) {
        this.versionCode = i;
        this.statusCode = i2;
        this.aTV = list;
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzaq.zza(this, parcel, i);
    }
}
