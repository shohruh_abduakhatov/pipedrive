package com.zendesk.sdk.model.request;

import android.support.annotation.Nullable;
import java.util.Date;

public class UploadResponse {
    private Attachment attachment;
    private Date expiresAt;
    private String token;

    @Nullable
    public String getToken() {
        return this.token;
    }

    @Nullable
    public Date getExpiresAt() {
        return this.expiresAt == null ? null : new Date(this.expiresAt.getTime());
    }

    @Nullable
    public Attachment getAttachment() {
        return this.attachment;
    }
}
