package com.pipedrive.util.networking.entities.product;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.model.products.ProductVariation;
import java.math.BigDecimal;

public class DealProductEntity {
    private static final String JSON_PARAM_ACTIVE_FLAG = "active_flag";
    private static final String JSON_PARAM_CURRENCY = "currency";
    private static final String JSON_PARAM_DISCOUNT_PERCENTAGE = "discount_percentage";
    private static final String JSON_PARAM_DURATION = "duration";
    private static final String JSON_PARAM_ID = "id";
    private static final String JSON_PARAM_ITEM_PRICE = "item_price";
    private static final String JSON_PARAM_NAME = "name";
    private static final String JSON_PARAM_ORDER_NR = "order_nr";
    private static final String JSON_PARAM_PRODUCT = "product";
    private static final String JSON_PARAM_PRODUCT_ID = "product_id";
    private static final String JSON_PARAM_PRODUCT_VARIATION_ID = "product_variation_id";
    private static final String JSON_PARAM_QUANTITY = "quantity";
    public static final String URL_NESTED_PARAMS_LIST = ":(id,order_nr,product_id,product_variation_id,item_price,discount_percentage,duration,currency,active_flag,name,quantity,product:(id,name,active_flag,selectable,prices,product_variations))";
    @Nullable
    @SerializedName("active_flag")
    @Expose
    private Boolean activeFlag;
    @Nullable
    @SerializedName("currency")
    @Expose
    private String currency;
    @Nullable
    @SerializedName("discount_percentage")
    @Expose
    private Double discountPercentage;
    @Nullable
    @SerializedName("duration")
    @Expose
    private Double duration;
    @Nullable
    @SerializedName("item_price")
    @Expose
    private BigDecimal itemPrice;
    @Nullable
    @SerializedName("name")
    @Expose
    private String name;
    @Nullable
    @SerializedName("order_nr")
    @Expose
    private Integer order;
    @Nullable
    @SerializedName("id")
    @Expose
    private Long pipedriveId;
    @Nullable
    @SerializedName("product")
    @Expose
    private ProductEntity productEntity;
    @Nullable
    @SerializedName("product_id")
    @Expose
    private Long productPipedriveId;
    @Nullable
    @SerializedName("product_variation_id")
    @Expose
    private Long productVariationPipedriveId;
    @Nullable
    @SerializedName("quantity")
    @Expose
    private Double quantity;

    @Nullable
    public Long getPipedriveId() {
        return this.pipedriveId;
    }

    @Nullable
    public Integer getOrder() {
        return this.order;
    }

    @Nullable
    public Long getProductPipedriveId() {
        return this.productPipedriveId;
    }

    @Nullable
    public BigDecimal getItemPrice() {
        return this.itemPrice;
    }

    @Nullable
    public Double getDiscountPercentage() {
        return this.discountPercentage;
    }

    @Nullable
    public Double getDuration() {
        return this.duration;
    }

    @Nullable
    public String getCurrency() {
        return this.currency;
    }

    @Nullable
    public Boolean getActiveFlag() {
        return this.activeFlag;
    }

    @Nullable
    public String getName() {
        return this.name;
    }

    @Nullable
    public Double getQuantity() {
        return this.quantity;
    }

    @Nullable
    public ProductEntity getProductEntity() {
        return this.productEntity;
    }

    @Nullable
    public Long getProductVariationPipedriveId() {
        return this.productVariationPipedriveId;
    }

    @Nullable
    public static JsonObject getJsonForCreate(@NonNull DealProduct dealProduct) {
        Number dealProductProductPipedriveId = dealProduct.getProduct().getPipedriveIdOrNull();
        if (dealProductProductPipedriveId == null) {
            return null;
        }
        JsonObject json = getJson(dealProduct);
        json.addProperty(JSON_PARAM_PRODUCT_ID, dealProductProductPipedriveId);
        return json;
    }

    @Nullable
    public static JsonObject getJsonForUpdate(@NonNull DealProduct dealProduct) {
        Number dealProductPipedriveId = dealProduct.getPipedriveIdOrNull();
        if (dealProductPipedriveId == null) {
            return null;
        }
        JsonObject json = getJson(dealProduct);
        json.addProperty("deal_product_id", dealProductPipedriveId);
        return json;
    }

    @NonNull
    private static JsonObject getJson(DealProduct dealProduct) {
        JsonObject json = new JsonObject();
        Number dealProductPipedriveId = dealProduct.getPipedriveIdOrNull();
        if (dealProductPipedriveId != null) {
            json.addProperty("id", dealProductPipedriveId);
        }
        json.addProperty(JSON_PARAM_ITEM_PRICE, dealProduct.getPrice().getValue());
        json.addProperty("quantity", dealProduct.getQuantity());
        json.addProperty(JSON_PARAM_DISCOUNT_PERCENTAGE, dealProduct.getDiscountPercentage());
        json.addProperty(JSON_PARAM_DURATION, dealProduct.getDuration());
        json.addProperty(JSON_PARAM_ACTIVE_FLAG, Integer.valueOf(dealProduct.isActive().booleanValue() ? 1 : 0));
        ProductVariation productVariation = dealProduct.getProductVariation();
        Number productVariationPipedriveId = productVariation == null ? null : productVariation.getPipedriveIdOrNull();
        if (productVariation == null) {
            json.addProperty(JSON_PARAM_PRODUCT_VARIATION_ID, "");
        } else {
            json.addProperty(JSON_PARAM_PRODUCT_VARIATION_ID, productVariationPipedriveId);
        }
        return json;
    }

    public String toString() {
        return "DealProductEntity{pipedriveId=" + getPipedriveId() + ", productPipedriveId=" + getProductPipedriveId() + ", productVariationPipedriveId=" + getProductVariationPipedriveId() + ", itemPrice=" + getItemPrice() + ", discountPercentage=" + getDiscountPercentage() + ", duration=" + getDuration() + ", currency='" + getCurrency() + '\'' + ", activeFlag=" + getActiveFlag() + ", name='" + getName() + '\'' + ", quantity=" + getQuantity() + ", order=" + getOrder() + ", productEntity=" + getProductEntity() + '}';
    }
}
