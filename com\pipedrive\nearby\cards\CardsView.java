package com.pipedrive.nearby.cards;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.google.android.gms.maps.model.LatLng;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.logging.Log;
import com.pipedrive.nearby.NearbyAnimation;
import com.pipedrive.nearby.cards.adapter.CardAdapter;
import com.pipedrive.nearby.cards.adapter.DealCardAdapter;
import com.pipedrive.nearby.cards.adapter.OrganizationCardAdapter;
import com.pipedrive.nearby.cards.adapter.PersonCardAdapter;
import com.pipedrive.nearby.cards.cards.Card;
import com.pipedrive.nearby.cards.cards.CardEventBus;
import com.pipedrive.nearby.map.LocationProvider;
import com.pipedrive.nearby.map.MarkerSelectionProvider;
import com.pipedrive.nearby.map.NearbyItemsProvider;
import com.pipedrive.nearby.model.DealNearbyItem;
import com.pipedrive.nearby.model.NearbyItem;
import com.pipedrive.nearby.model.PersonNearbyItem;
import com.pipedrive.nearby.toolbar.NearbyItemTypeChangesProvider;
import com.pipedrive.nearby.util.Irrelevant;
import com.pipedrive.util.RxUtil;
import java.util.List;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Actions;
import rx.functions.Func1;
import rx.subjects.BehaviorSubject;
import rx.subjects.Subject;
import rx.subscriptions.CompositeSubscription;

public class CardsView extends RecyclerView implements Cards {
    private static final String KEY_SELECTED_ITEM_INDEX = (CardsView.class.getSimpleName() + ".selected_item_index");
    @NonNull
    private final CompositeSubscription allSubscriptions = new CompositeSubscription();
    private final int bottomMargin = getContext().getResources().getDimensionPixelSize(R.dimen.dimen4);
    @NonNull
    private final Subject<NearbyItem, NearbyItem> cardSelection = BehaviorSubject.create().toSerialized();
    @Nullable
    private LocationProvider locationProvider;
    @Nullable
    private MarkerSelectionProvider markerSelectionProvider;
    private final int minHeight = getContext().getResources().getDimensionPixelSize(R.dimen.dimen88);
    @Nullable
    private NearbyItemTypeChangesProvider nearbyItemTypeChangesProvider;
    @Nullable
    private NearbyItemsProvider nearbyItemsProvider;
    @NonNull
    private final OnScrollListener onScrollListener = new OnScrollListener() {
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (newState == 0) {
                if (CardsView.this.selectingCardFromMarker.booleanValue()) {
                    CardsView.this.selectingCardFromMarker = Boolean.valueOf(false);
                }
                CardEventBus.INSTANCE.onCardSelected();
            }
        }

        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (!CardsView.this.selectingCardFromMarker.booleanValue()) {
                int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                int lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                if (lastVisibleItemPosition - firstVisibleItemPosition > 1) {
                    CardsView.this.selectedItemIndex = Integer.valueOf(lastVisibleItemPosition - 1);
                } else if (dx > 0) {
                    CardsView.this.selectedItemIndex = Integer.valueOf(lastVisibleItemPosition);
                } else {
                    CardsView.this.selectedItemIndex = Integer.valueOf(firstVisibleItemPosition);
                }
                NearbyItem item = CardsView.this.getAdapter().getItem(CardsView.this.selectedItemIndex);
                if (item != null) {
                    CardsView.this.cardSelection.onNext(item);
                }
            }
        }
    };
    @NonNull
    private Integer selectedItemIndex = Integer.valueOf(0);
    @NonNull
    private Boolean selectingCardFromMarker = Boolean.valueOf(false);
    @Nullable
    private Session session;
    @Nullable
    private Subscription updateAdapterSubscription;

    public CardsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setLayoutManager(new CardsLayoutManager(context));
        addItemDecoration(new VerticalSpaceItemDecoration(context.getResources().getDimensionPixelSize(R.dimen.card_side_margin), context.getResources().getDimensionPixelSize(R.dimen.card_side_margin_large)));
        new PagerSnapHelper().attachToRecyclerView(this);
        setDescendantFocusability(393216);
    }

    public void setup(@Nullable Bundle savedInstanceState, @NonNull Session session, @NonNull NearbyItemsProvider nearbyItemsProvider, @NonNull NearbyItemTypeChangesProvider nearbyItemTypeChangesProvider, @NonNull MarkerSelectionProvider markerSelectionProvider, @NonNull LocationProvider locationProvider) {
        this.session = session;
        this.nearbyItemsProvider = nearbyItemsProvider;
        this.nearbyItemTypeChangesProvider = nearbyItemTypeChangesProvider;
        this.markerSelectionProvider = markerSelectionProvider;
        this.locationProvider = locationProvider;
        if (savedInstanceState != null) {
            this.selectedItemIndex = Integer.valueOf(savedInstanceState.getInt(KEY_SELECTED_ITEM_INDEX, 0));
        }
    }

    public boolean onTouchEvent(MotionEvent e) {
        if (e.getAction() == 0) {
            Card card = getCardViewHolderForSelectedItemIndex();
            if (card == null || card.shouldIgnoreTouchDownEvent(e)) {
                return false;
            }
        }
        return super.onTouchEvent(e);
    }

    @Nullable
    private Card getCardViewHolderForSelectedItemIndex() {
        return (Card) findViewHolderForLayoutPosition(this.selectedItemIndex.intValue());
    }

    public void bind() {
        this.allSubscriptions.add(NearbyRequestEventBus.INSTANCE.requestStartedEvents().doOnNext(new Action1<Irrelevant>() {
            public void call(Irrelevant irrelevant) {
                CardsView.this.hide();
            }
        }).doOnNext(new Action1<Irrelevant>() {
            public void call(Irrelevant irrelevant) {
                CardsView.this.subscribeToRequestCompletedEvents();
            }
        }).subscribe(Actions.empty(), logError()));
        if (this.markerSelectionProvider != null) {
            this.allSubscriptions.add(this.markerSelectionProvider.markerSelection().observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<NearbyItem>() {
                public void call(NearbyItem nearbyItem) {
                    CardsView.this.selectCardForNearbyItem(nearbyItem);
                }
            }, logError()));
        }
        addOnScrollListener(this.onScrollListener);
    }

    private void subscribeToRequestCompletedEvents() {
        if (this.session != null && this.nearbyItemsProvider != null && this.nearbyItemTypeChangesProvider != null && this.updateAdapterSubscription == null) {
            this.updateAdapterSubscription = NearbyRequestEventBus.INSTANCE.requestCompletedEvents().map(new Func1<Irrelevant, List<NearbyItem>>() {
                public List<NearbyItem> call(Irrelevant irrelevant) {
                    return (List) RxUtil.blockingFirst(CardsView.this.nearbyItemsProvider.nearbyItems());
                }
            }).map(new Func1<List<NearbyItem>, CardAdapter>() {
                public CardAdapter call(List<NearbyItem> nearbyItems) {
                    if (nearbyItems == null || nearbyItems.isEmpty()) {
                        CardsView.this.showEmptyCardAndEmitItsHeight();
                        return null;
                    }
                    LatLng currentLocation = CardsView.this.locationProvider == null ? new LatLng(0.0d, 0.0d) : (LatLng) RxUtil.blockingFirst(CardsView.this.locationProvider.locationChanges());
                    NearbyItem firstItem = (NearbyItem) nearbyItems.get(0);
                    if (firstItem instanceof DealNearbyItem) {
                        return new DealCardAdapter(CardsView.this.session, nearbyItems, currentLocation);
                    }
                    if (firstItem instanceof PersonNearbyItem) {
                        return new PersonCardAdapter(CardsView.this.session, nearbyItems, currentLocation);
                    }
                    return new OrganizationCardAdapter(CardsView.this.session, nearbyItems, currentLocation);
                }
            }).doOnNext(new Action1<CardAdapter>() {
                public void call(CardAdapter cardAdapter) {
                    CardsView.this.selectedItemIndex = Integer.valueOf(0);
                }
            }).observeOn(AndroidSchedulers.mainThread()).doOnNext(new Action1<CardAdapter>() {
                public void call(CardAdapter cardAdapter) {
                    CardsView.this.setAdapter(cardAdapter);
                    CardsView.this.collapseCardsIfExpanded();
                    CardsView.this.show();
                }
            }).doOnNext(new Action1<CardAdapter>() {
                public void call(CardAdapter cardAdapter) {
                    CardsView.this.removeOnScrollListener(CardsView.this.onScrollListener);
                    CardsView.this.getLayoutManager().scrollToPosition(CardsView.this.selectedItemIndex.intValue());
                    CardsView.this.addOnScrollListener(CardsView.this.onScrollListener);
                }
            }).subscribe(Actions.empty(), logError());
        }
    }

    void showEmptyCardAndEmitItsHeight() {
        CardEventBus.INSTANCE.showEmptyCard();
        CardEventBus.INSTANCE.onCardsHeightChanged(Integer.valueOf(getContext().getResources().getDimensionPixelSize(R.dimen.nearby_empty_card_height) + getContext().getResources().getDimensionPixelSize(R.dimen.nearby_empty_card_bottom_margin)));
    }

    public void saveState(@NonNull Bundle outState) {
        outState.putInt(KEY_SELECTED_ITEM_INDEX, this.selectedItemIndex.intValue());
    }

    private void selectCardForNearbyItem(@NonNull NearbyItem nearbyItem) {
        int position = getAdapter().indexOf(nearbyItem).intValue();
        this.selectingCardFromMarker = Boolean.valueOf(true);
        getLayoutManager().smoothScrollToPosition(this, null, position);
    }

    private void show() {
        ObjectAnimator objectAnimator = getCardsViewTranslationObjectAnimator();
        objectAnimator.removeAllListeners();
        objectAnimator.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animation) {
                CardsViewEventBus.INSTANCE.onCardsViewShowCalled();
            }
        });
        objectAnimator.start();
    }

    @NonNull
    private ObjectAnimator getCardsViewTranslationObjectAnimator() {
        return NearbyAnimation.getTranslationYObjectAnimator(this, (float) (Math.max(this.minHeight, getMeasuredHeight()) + this.bottomMargin), 0.0f);
    }

    private void hide() {
        CardsViewEventBus.INSTANCE.onCardsViewHideCalled();
        getCardsViewTranslationObjectAnimator().reverse();
    }

    private void collapseCardsIfExpanded() {
        if (((Boolean) RxUtil.blockingFirst(CardEventBus.INSTANCE.isExpanded())).booleanValue()) {
            CardEventBus.INSTANCE.collapseCards();
        }
    }

    @NonNull
    private Action1<Throwable> logError() {
        return new Action1<Throwable>() {
            public void call(Throwable throwable) {
                Log.e(throwable);
            }
        };
    }

    public void releaseResources() {
        this.allSubscriptions.clear();
        if (this.updateAdapterSubscription != null) {
            this.updateAdapterSubscription.unsubscribe();
            this.updateAdapterSubscription = null;
        }
        removeOnScrollListener(this.onScrollListener);
    }

    @NonNull
    public Observable<NearbyItem> cardSelection() {
        return this.cardSelection.distinctUntilChanged();
    }

    public CardAdapter getAdapter() {
        return (CardAdapter) super.getAdapter();
    }
}
