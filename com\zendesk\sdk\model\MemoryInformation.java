package com.zendesk.sdk.model;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.v4.media.session.PlaybackStateCompat;
import com.pipedrive.util.networking.entities.FlowItem;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Locale;
import java.util.StringTokenizer;

public class MemoryInformation {
    public static final double BYTES_MULTIPLIER = 1024.0d;
    public static final int EXPECTED_TOKEN_COUNT = 3;
    private static final String LOG_TAG = MemoryInformation.class.getSimpleName();
    private final ActivityManager mActivityManager;
    private final Context mContext;
    private int mTotalMemory = getTotalMemory();

    public MemoryInformation(Context context) {
        this.mActivityManager = (ActivityManager) context.getSystemService(FlowItem.ITEM_TYPE_OBJECT_ACTIVITY);
        this.mContext = context;
    }

    public String formatMemoryUsage() {
        return String.format(Locale.US, this.mContext.getString(R.string.rate_my_app_dialog_feedback_device_memory), new Object[]{String.valueOf(getUsedMemory()), String.valueOf(this.mTotalMemory)});
    }

    public int getUsedMemory() {
        MemoryInfo memoryInfo = new MemoryInfo();
        this.mActivityManager.getMemoryInfo(memoryInfo);
        return this.mTotalMemory - bytesToMegabytes(memoryInfo.availMem);
    }

    @TargetApi(16)
    public int getTotalMemory() {
        if (VERSION.SDK_INT >= 16) {
            Logger.d(LOG_TAG, "Using getTotalMemoryApi() to determine memory", new Object[0]);
            return getTotalMemoryApi();
        }
        Logger.d(LOG_TAG, "Using getTotalMemoryCompat() to determine memory", new Object[0]);
        return getTotalMemoryCompat();
    }

    public int getTotalMemoryCompat() {
        IOException e;
        StringTokenizer st;
        Throwable th;
        long totalMemoryBytes = 0;
        BufferedReader memoryInfoReader = null;
        String totalMemory = "";
        try {
            BufferedReader memoryInfoReader2 = new BufferedReader(new FileReader("/proc/meminfo"));
            try {
                totalMemory = memoryInfoReader2.readLine();
                if (memoryInfoReader2 != null) {
                    try {
                        memoryInfoReader2.close();
                        memoryInfoReader = memoryInfoReader2;
                    } catch (IOException e2) {
                        Logger.w(LOG_TAG, "Failed to close /proc/meminfo file stream: " + e2.getMessage(), e2, new Object[0]);
                        memoryInfoReader = memoryInfoReader2;
                    }
                }
            } catch (IOException e3) {
                e2 = e3;
                memoryInfoReader = memoryInfoReader2;
                try {
                    Logger.e(LOG_TAG, "Failed to determine total memory from /proc/meminfo: " + e2.getMessage(), e2, new Object[0]);
                    if (memoryInfoReader != null) {
                        try {
                            memoryInfoReader.close();
                        } catch (IOException e22) {
                            Logger.w(LOG_TAG, "Failed to close /proc/meminfo file stream: " + e22.getMessage(), e22, new Object[0]);
                        }
                    }
                    st = new StringTokenizer(totalMemory);
                    if (st.countTokens() == 3) {
                        st.nextToken();
                        totalMemoryBytes = Long.valueOf(st.nextToken()).longValue() * PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID;
                    }
                    return bytesToMegabytes(totalMemoryBytes);
                } catch (Throwable th2) {
                    th = th2;
                    if (memoryInfoReader != null) {
                        try {
                            memoryInfoReader.close();
                        } catch (IOException e222) {
                            Logger.w(LOG_TAG, "Failed to close /proc/meminfo file stream: " + e222.getMessage(), e222, new Object[0]);
                        }
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                memoryInfoReader = memoryInfoReader2;
                if (memoryInfoReader != null) {
                    memoryInfoReader.close();
                }
                throw th;
            }
        } catch (IOException e4) {
            e222 = e4;
            Logger.e(LOG_TAG, "Failed to determine total memory from /proc/meminfo: " + e222.getMessage(), e222, new Object[0]);
            if (memoryInfoReader != null) {
                memoryInfoReader.close();
            }
            st = new StringTokenizer(totalMemory);
            if (st.countTokens() == 3) {
                st.nextToken();
                totalMemoryBytes = Long.valueOf(st.nextToken()).longValue() * PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID;
            }
            return bytesToMegabytes(totalMemoryBytes);
        }
        st = new StringTokenizer(totalMemory);
        if (st.countTokens() == 3) {
            st.nextToken();
            totalMemoryBytes = Long.valueOf(st.nextToken()).longValue() * PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID;
        }
        return bytesToMegabytes(totalMemoryBytes);
    }

    @TargetApi(16)
    public int getTotalMemoryApi() {
        if (VERSION.SDK_INT < 16) {
            Logger.w(LOG_TAG, "Sorry, this call is not available on your API level, please use getTotalMemory() instead", new Object[0]);
            return 0;
        }
        MemoryInfo memoryInfo = new MemoryInfo();
        this.mActivityManager.getMemoryInfo(memoryInfo);
        return bytesToMegabytes(memoryInfo.totalMem);
    }

    public boolean isLowMemory() {
        MemoryInfo memoryInfo = new MemoryInfo();
        this.mActivityManager.getMemoryInfo(memoryInfo);
        return memoryInfo.lowMemory;
    }

    private int bytesToMegabytes(long bytes) {
        return (int) Math.round((((double) bytes) / BYTES_MULTIPLIER) / BYTES_MULTIPLIER);
    }
}
