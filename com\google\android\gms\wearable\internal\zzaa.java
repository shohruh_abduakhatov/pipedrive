package com.google.android.gms.wearable.internal;

import com.google.android.gms.wearable.DataItemAsset;

public class zzaa implements DataItemAsset {
    private final String zzbcn;
    private final String zzboa;

    public zzaa(DataItemAsset dataItemAsset) {
        this.zzboa = dataItemAsset.getId();
        this.zzbcn = dataItemAsset.getDataItemKey();
    }

    public /* synthetic */ Object freeze() {
        return zzcmv();
    }

    public String getDataItemKey() {
        return this.zzbcn;
    }

    public String getId() {
        return this.zzboa;
    }

    public boolean isDataValid() {
        return true;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("DataItemAssetEntity[");
        stringBuilder.append("@");
        stringBuilder.append(Integer.toHexString(hashCode()));
        if (this.zzboa == null) {
            stringBuilder.append(",noid");
        } else {
            stringBuilder.append(Table.COMMA_SEP);
            stringBuilder.append(this.zzboa);
        }
        stringBuilder.append(", key=");
        stringBuilder.append(this.zzbcn);
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    public DataItemAsset zzcmv() {
        return this;
    }
}
