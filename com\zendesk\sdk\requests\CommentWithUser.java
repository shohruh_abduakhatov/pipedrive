package com.zendesk.sdk.requests;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.request.CommentResponse;
import com.zendesk.sdk.model.request.User;
import com.zendesk.util.CollectionUtils;
import java.util.List;

class CommentWithUser {
    private static final String LOG_TAG = "CommentWithUser";
    private final User mAuthor;
    private final CommentResponse mComment;

    private CommentWithUser(CommentResponse comment, User author) {
        this.mComment = comment;
        this.mAuthor = author;
    }

    public CommentResponse getComment() {
        return this.mComment;
    }

    @NonNull
    public User getAuthor() {
        if (this.mAuthor != null) {
            return this.mAuthor;
        }
        Logger.w(LOG_TAG, "Author is null, returning default author", new Object[0]);
        return new User();
    }

    @Nullable
    public static CommentWithUser build(CommentResponse comment, List<User> users) {
        if (comment == null) {
            return null;
        }
        User author = null;
        if (CollectionUtils.isNotEmpty(users)) {
            for (User user : users) {
                Long userId = user == null ? null : user.getId();
                if (userId != null && userId.equals(comment.getAuthorId())) {
                    author = user;
                }
            }
        }
        return new CommentWithUser(comment, author);
    }

    @Nullable
    public static CommentWithUser build(CommentResponse comment, User user) {
        if (comment != null) {
            return new CommentWithUser(comment, user);
        }
        return null;
    }
}
