package com.zendesk.sdk.rating.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.sdk.feedback.FeedbackConnector;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConnector;
import com.zendesk.sdk.model.settings.SafeMobileSettings;
import com.zendesk.sdk.network.SubmissionListener;
import com.zendesk.sdk.network.SubmissionListenerAdapter;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.rating.RateMyAppButton;
import com.zendesk.sdk.rating.RateMyAppRule;
import com.zendesk.sdk.rating.impl.IdentityRateMyAppRule;
import com.zendesk.sdk.rating.impl.MetricsRateMyAppRule;
import com.zendesk.sdk.rating.impl.NetworkRateMyAppRule;
import com.zendesk.sdk.rating.impl.RateMyAppDontAskAgainButton;
import com.zendesk.sdk.rating.impl.RateMyAppRules;
import com.zendesk.sdk.rating.impl.RateMyAppSendFeedbackButton;
import com.zendesk.sdk.rating.impl.RateMyAppStoreButton;
import com.zendesk.sdk.rating.impl.SettingsRateMyAppRule;
import com.zendesk.sdk.rating.ui.RateMyAppButtonContainer.RateMyAppSelectionListener;
import com.zendesk.sdk.storage.RateMyAppStorage;
import com.zendesk.sdk.ui.ZendeskDialog;
import com.zendesk.service.ErrorResponse;
import com.zendesk.service.ZendeskCallback;
import com.zendesk.util.CollectionUtils;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class RateMyAppDialog extends ZendeskDialog implements Serializable {
    private static final String LOG_TAG = RateMyAppDialog.class.getSimpleName();
    public static final String RMA_DIALOG_TAG = "rma_dialog";
    private Handler mHandler;
    private OnShowListener mOnShowListener;
    private RateMyAppButtonContainer mRateMyAppButtonContainer;
    private List<RateMyAppButton> mRateMyAppButtonList;
    private RateMyAppRules mRateMyAppRules;
    private RateMyAppSelectionListener mSelectionListener;
    private boolean mShowAlways;
    private boolean mShowPending;

    public static class Builder {
        private final FragmentActivity mActivity;
        private final ArrayList<RateMyAppButton> mButtons = new ArrayList();
        private OnShowListener mOnShowListener;
        private RateMyAppRule[] mRules;
        private RateMyAppSelectionListener mSelectionListener;

        public Builder(FragmentActivity activity) {
            this.mActivity = activity;
            if (this.mActivity == null) {
                Logger.w(RateMyAppDialog.LOG_TAG, "Activity is null, many things will not work", new Object[0]);
            }
        }

        public Builder withButton(RateMyAppButton button) {
            if (!(this.mActivity == null || button == null)) {
                this.mButtons.add(button);
            }
            return this;
        }

        public Builder withAndroidStoreRatingButton() {
            if (this.mActivity != null) {
                this.mButtons.add(new RateMyAppStoreButton(this.mActivity));
            }
            return this;
        }

        public Builder withDontRemindMeAgainButton() {
            if (this.mActivity != null) {
                this.mButtons.add(new RateMyAppDontAskAgainButton(this.mActivity));
            }
            return this;
        }

        public Builder withSendFeedbackButton() {
            return withSendFeedbackButton(new DefaultContactConfiguration(this.mActivity), new SubmissionListenerAdapter());
        }

        public Builder withSendFeedbackButton(ZendeskFeedbackConfiguration feedbackConfiguration) {
            return withSendFeedbackButton(feedbackConfiguration, new SubmissionListenerAdapter());
        }

        public Builder withSendFeedbackButton(ZendeskFeedbackConfiguration feedbackConfiguration, SubmissionListener listener) {
            if (this.mActivity != null) {
                FeedbackConnector connector = ZendeskFeedbackConnector.defaultConnector(this.mActivity, new WrappedZendeskFeedbackConfiguration(feedbackConfiguration), ZendeskConfig.INSTANCE.getMobileSettings().getRateMyAppTags());
                if (connector.isValid()) {
                    RateMyAppSendFeedbackButton feedbackButton = new RateMyAppSendFeedbackButton(this.mActivity, connector);
                    feedbackButton.setFeedbackListener(listener);
                    this.mButtons.add(feedbackButton);
                }
            }
            return this;
        }

        public Builder withSelectionListener(RateMyAppSelectionListener selectionListener) {
            this.mSelectionListener = selectionListener;
            return this;
        }

        public Builder withOnShowListener(OnShowListener onShowListener) {
            this.mOnShowListener = onShowListener;
            return this;
        }

        public Builder withRules(RateMyAppRule... rules) {
            this.mRules = rules;
            return this;
        }

        public RateMyAppDialog build() {
            RateMyAppDialog dialog = new RateMyAppDialog();
            dialog.setRateMyAppOnShowListener(this.mOnShowListener);
            dialog.setRateMyAppOnSelectionListener(this.mSelectionListener);
            dialog.setRateMyAppButtonList(this.mButtons);
            ensureSaneRules(dialog);
            dialog.mShowPending = false;
            dialog.setRetainInstance(true);
            return dialog;
        }

        private void ensureSaneRules(RateMyAppDialog dialog) {
            if (CollectionUtils.isNotEmpty(this.mRules)) {
                dialog.setRateMyAppRules(new RateMyAppRules(this.mRules));
                return;
            }
            dialog.setRateMyAppRules(new RateMyAppRules(new SettingsRateMyAppRule(this.mActivity), new MetricsRateMyAppRule(this.mActivity), new NetworkRateMyAppRule(this.mActivity), new IdentityRateMyAppRule(this.mActivity)));
        }
    }

    public interface OnShowListener extends Serializable {
        void onShow(DialogInterface dialogInterface);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ArrayList<RateMyAppButton> buttons = new ArrayList();
        if (this.mRateMyAppButtonList != null) {
            buttons = new ArrayList(this.mRateMyAppButtonList);
        } else {
            Logger.e(LOG_TAG, "buttons extra was not an instance of ArrayList<RateMyAppButton>, RateMyAppDialog will be dismissed.", new Object[0]);
        }
        this.mRateMyAppButtonContainer = new RateMyAppButtonContainer(getActivity(), buttons);
        this.mRateMyAppButtonContainer.setDismissableListener(new DismissableListener() {
            public void dismissDialog() {
                RateMyAppDialog.this.dismiss();
            }
        });
        this.mRateMyAppButtonContainer.setRateMyAppSelectionListener(this.mSelectionListener);
        this.mRateMyAppButtonContainer.setId(R.id.rma_dialog_root);
        return this.mRateMyAppButtonContainer;
    }

    public void onStart() {
        super.onStart();
        if (!ZendeskConfig.INSTANCE.isInitialized()) {
            Logger.d(LOG_TAG, "ZendeskConfig has not been initialized, dismissing RateMyAppDialog.", new Object[0]);
            dismiss();
        }
    }

    public void showAlways(FragmentActivity fragmentActivity) {
        this.mShowAlways = true;
        final WeakReference<FragmentActivity> fragmentActivityRef = new WeakReference(fragmentActivity);
        ZendeskConfig.INSTANCE.provider().uiSettingsHelper().loadSetting(new ZendeskCallback<SafeMobileSettings>() {
            public void onSuccess(SafeMobileSettings mobileSettings) {
                FragmentActivity fa = (FragmentActivity) fragmentActivityRef.get();
                if (fa != null) {
                    RateMyAppDialog.this.showInternal(fa);
                } else {
                    Logger.d(RateMyAppDialog.LOG_TAG, "Host Activity is gone. Cannot display the RateMyAppDialog.", new Object[0]);
                }
            }

            public void onError(ErrorResponse errorResponse) {
                Logger.w(RateMyAppDialog.LOG_TAG, "Error showing RMA, unable to load settings | reason: %s", errorResponse.getReason());
            }
        });
    }

    public void show(FragmentActivity fragmentActivity) {
        show(fragmentActivity, false);
    }

    public void show(FragmentActivity fragmentActivity, final boolean overrideShowDelay) {
        final WeakReference<FragmentActivity> fragmentActivityRef = new WeakReference(fragmentActivity);
        ZendeskConfig.INSTANCE.provider().uiSettingsHelper().loadSetting(new ZendeskCallback<SafeMobileSettings>() {
            public void onSuccess(SafeMobileSettings mobileSettings) {
                FragmentActivity fa = (FragmentActivity) fragmentActivityRef.get();
                if (fa != null) {
                    RateMyAppDialog.this.show(fa, overrideShowDelay, mobileSettings);
                } else {
                    Logger.d(RateMyAppDialog.LOG_TAG, "Host Activity is gone. Cannot display the RateMyAppDialog.", new Object[0]);
                }
            }

            public void onError(ErrorResponse errorResponse) {
                Logger.w(RateMyAppDialog.LOG_TAG, "Error showing RMA, unable to load settings | reason: %s", errorResponse.getReason());
            }
        });
    }

    private void show(FragmentActivity fragmentActivity, boolean overrideShowDelay, SafeMobileSettings mobileSettings) {
        Logger.i(LOG_TAG, "show called, show immediate override is: " + overrideShowDelay, new Object[0]);
        if (fragmentActivity == null) {
            Logger.w(LOG_TAG, "Cannot show dialog, activity is null", new Object[0]);
        } else if (mobileSettings.isRateMyAppEnabled()) {
            final WeakReference<FragmentActivity> activityReference = new WeakReference(fragmentActivity);
            this.mHandler = new Handler(Looper.getMainLooper());
            this.mHandler.postDelayed(new Runnable() {
                public void run() {
                    FragmentActivity fragmentActivity = (FragmentActivity) activityReference.get();
                    if (fragmentActivity != null && !fragmentActivity.isFinishing()) {
                        RateMyAppDialog.this.showInternal(fragmentActivity);
                    }
                }
            }, overrideShowDelay ? 0 : mobileSettings.getRateMyAppDelay());
        } else {
            Logger.w(LOG_TAG, "Cannot show dialog, no rate my app settings have been retrieved from the server or rate my app is not enabled.", new Object[0]);
        }
    }

    void setRateMyAppOnShowListener(OnShowListener onShowListener) {
        this.mOnShowListener = onShowListener;
        if (this.mRateMyAppButtonContainer != null) {
            this.mRateMyAppButtonContainer.setRateMyAppSelectionListener((RateMyAppSelectionListener) this.mOnShowListener);
        }
    }

    void setRateMyAppOnSelectionListener(RateMyAppSelectionListener mSelectionListener) {
        this.mSelectionListener = mSelectionListener;
    }

    void setRateMyAppButtonList(List<RateMyAppButton> rateMyAppButtonList) {
        this.mRateMyAppButtonList = rateMyAppButtonList;
    }

    void setRateMyAppRules(RateMyAppRules rateMyAppRules) {
        this.mRateMyAppRules = rateMyAppRules;
    }

    private void showInternal(FragmentActivity fragmentActivity) {
        if (this.mShowPending) {
            Logger.i(LOG_TAG, "Can't show the dialog because another call to show is pending", new Object[0]);
            return;
        }
        boolean canShowDialog;
        new RateMyAppStorage(fragmentActivity).incrementNumberOfLaunches();
        if (this.mShowAlways || this.mRateMyAppRules.checkRules()) {
            canShowDialog = true;
        } else {
            canShowDialog = false;
        }
        if (canShowDialog) {
            this.mShowPending = true;
            this.mShowAlways = false;
            FragmentManager fragmentManager = fragmentActivity.getSupportFragmentManager();
            DialogFragment prev = (DialogFragment) fragmentManager.findFragmentByTag(RMA_DIALOG_TAG);
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if (prev != null) {
                if (prev.getDialog() != null) {
                    prev.getDialog().dismiss();
                }
                ft.remove(prev);
            }
            try {
                show(ft, RMA_DIALOG_TAG);
                getFragmentManager().executePendingTransactions();
                if (this.mOnShowListener != null) {
                    this.mOnShowListener.onShow(getDialog());
                    return;
                }
                return;
            } catch (Exception e) {
                Logger.e(LOG_TAG, e.getMessage(), e, new Object[0]);
                return;
            }
        }
        Logger.i(LOG_TAG, "Can't show the dialog due to configuration", new Object[0]);
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        this.mShowPending = false;
        return super.onCreateDialog(savedInstanceState);
    }

    public void tearDownDialog(FragmentManager fragmentManager) {
        if (fragmentManager == null) {
            Logger.d(LOG_TAG, "Supplied FragmentManager was null, cannot continue...", new Object[0]);
            return;
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment feedbackDialogFragment = fragmentManager.findFragmentByTag(RateMyAppSendFeedbackButton.FEEDBACK_DIALOG_TAG);
        if (feedbackDialogFragment != null) {
            Logger.d(LOG_TAG, "feedback dialog found for removal", new Object[0]);
            fragmentTransaction.remove(feedbackDialogFragment);
        }
        fragmentTransaction.commit();
        fragmentTransaction = fragmentManager.beginTransaction();
        Fragment rmaDialog = fragmentManager.findFragmentByTag(RMA_DIALOG_TAG);
        if (rmaDialog != null) {
            Logger.d(LOG_TAG, "RateMyApp dialog found for removal", new Object[0]);
            fragmentTransaction.remove(rmaDialog);
        }
        fragmentTransaction.commit();
        fragmentManager.popBackStackImmediate();
        fragmentManager.executePendingTransactions();
        if (this.mHandler != null) {
            this.mHandler.removeCallbacksAndMessages(null);
        }
    }
}
