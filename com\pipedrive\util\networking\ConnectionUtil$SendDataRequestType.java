package com.pipedrive.util.networking;

enum ConnectionUtil$SendDataRequestType {
    HttpGet,
    HttpPost,
    HttpPut,
    HttpDelete,
    MultipartFormData;

    public String toString() {
        return super.toString().substring(4).toUpperCase();
    }
}
