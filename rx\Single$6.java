package rx;

import rx.functions.Func5;
import rx.functions.FuncN;

class Single$6 implements FuncN<R> {
    final /* synthetic */ Func5 val$zipFunction;

    Single$6(Func5 func5) {
        this.val$zipFunction = func5;
    }

    public R call(Object... args) {
        return this.val$zipFunction.call(args[0], args[1], args[2], args[3], args[4]);
    }
}
