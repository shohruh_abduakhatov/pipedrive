package com.google.android.gms.wearable.internal;

import android.os.ParcelFileDescriptor.AutoCloseInputStream;
import android.os.ParcelFileDescriptor.AutoCloseOutputStream;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.wearable.CapabilityApi.AddLocalCapabilityResult;
import com.google.android.gms.wearable.CapabilityApi.GetAllCapabilitiesResult;
import com.google.android.gms.wearable.CapabilityApi.GetCapabilityResult;
import com.google.android.gms.wearable.CapabilityApi.RemoveLocalCapabilityResult;
import com.google.android.gms.wearable.CapabilityInfo;
import com.google.android.gms.wearable.Channel.GetInputStreamResult;
import com.google.android.gms.wearable.Channel.GetOutputStreamResult;
import com.google.android.gms.wearable.ChannelApi.OpenChannelResult;
import com.google.android.gms.wearable.DataApi.DataItemResult;
import com.google.android.gms.wearable.DataApi.DeleteDataItemsResult;
import com.google.android.gms.wearable.DataApi.GetFdForAssetResult;
import com.google.android.gms.wearable.DataItemBuffer;
import com.google.android.gms.wearable.MessageApi.SendMessageResult;
import com.google.android.gms.wearable.NodeApi.GetConnectedNodesResult;
import com.google.android.gms.wearable.NodeApi.GetLocalNodeResult;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.FutureTask;

final class zzbo {

    static abstract class zzb<T> extends zza {
        private com.google.android.gms.internal.zzqo.zzb<T> gY;

        public zzb(com.google.android.gms.internal.zzqo.zzb<T> com_google_android_gms_internal_zzqo_zzb_T) {
            this.gY = com_google_android_gms_internal_zzqo_zzb_T;
        }

        public void zzbc(T t) {
            com.google.android.gms.internal.zzqo.zzb com_google_android_gms_internal_zzqo_zzb = this.gY;
            if (com_google_android_gms_internal_zzqo_zzb != null) {
                com_google_android_gms_internal_zzqo_zzb.setResult(t);
                this.gY = null;
            }
        }
    }

    static final class zza extends zzb<AddLocalCapabilityResult> {
        public zza(com.google.android.gms.internal.zzqo.zzb<AddLocalCapabilityResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_CapabilityApi_AddLocalCapabilityResult) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_CapabilityApi_AddLocalCapabilityResult);
        }

        public void zza(AddLocalCapabilityResponse addLocalCapabilityResponse) {
            zzbc(new com.google.android.gms.wearable.internal.zzj.zza(zzbk.zzqv(addLocalCapabilityResponse.statusCode)));
        }
    }

    static final class zzc extends zzb<Status> {
        public zzc(com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status);
        }

        public void zza(CloseChannelResponse closeChannelResponse) {
            zzbc(new Status(closeChannelResponse.statusCode));
        }
    }

    static final class zzd extends zzb<Status> {
        public zzd(com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status);
        }

        public void zzb(CloseChannelResponse closeChannelResponse) {
            zzbc(new Status(closeChannelResponse.statusCode));
        }
    }

    static final class zze extends zzb<DeleteDataItemsResult> {
        public zze(com.google.android.gms.internal.zzqo.zzb<DeleteDataItemsResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataApi_DeleteDataItemsResult) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataApi_DeleteDataItemsResult);
        }

        public void zza(DeleteDataItemsResponse deleteDataItemsResponse) {
            zzbc(new com.google.android.gms.wearable.internal.zzx.zzb(zzbk.zzqv(deleteDataItemsResponse.statusCode), deleteDataItemsResponse.aTM));
        }
    }

    static final class zzf extends zzb<GetAllCapabilitiesResult> {
        public zzf(com.google.android.gms.internal.zzqo.zzb<GetAllCapabilitiesResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_CapabilityApi_GetAllCapabilitiesResult) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_CapabilityApi_GetAllCapabilitiesResult);
        }

        public void zza(GetAllCapabilitiesResponse getAllCapabilitiesResponse) {
            zzbc(new com.google.android.gms.wearable.internal.zzj.zzd(zzbk.zzqv(getAllCapabilitiesResponse.statusCode), zzbo.zzao(getAllCapabilitiesResponse.aTN)));
        }
    }

    static final class zzg extends zzb<GetCapabilityResult> {
        public zzg(com.google.android.gms.internal.zzqo.zzb<GetCapabilityResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_CapabilityApi_GetCapabilityResult) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_CapabilityApi_GetCapabilityResult);
        }

        public void zza(GetCapabilityResponse getCapabilityResponse) {
            zzbc(new com.google.android.gms.wearable.internal.zzj.zze(zzbk.zzqv(getCapabilityResponse.statusCode), new com.google.android.gms.wearable.internal.zzj.zzc(getCapabilityResponse.aTO)));
        }
    }

    static final class zzh extends zzb<GetInputStreamResult> {
        private final zzt aUp;

        public zzh(com.google.android.gms.internal.zzqo.zzb<GetInputStreamResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_Channel_GetInputStreamResult, zzt com_google_android_gms_wearable_internal_zzt) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_Channel_GetInputStreamResult);
            this.aUp = (zzt) zzaa.zzy(com_google_android_gms_wearable_internal_zzt);
        }

        public void zza(GetChannelInputStreamResponse getChannelInputStreamResponse) {
            InputStream inputStream = null;
            if (getChannelInputStreamResponse.aTP != null) {
                inputStream = new zzp(new AutoCloseInputStream(getChannelInputStreamResponse.aTP));
                this.aUp.zza(inputStream.zzcmt());
            }
            zzbc(new zza(new Status(getChannelInputStreamResponse.statusCode), inputStream));
        }
    }

    static final class zzi extends zzb<GetOutputStreamResult> {
        private final zzt aUp;

        public zzi(com.google.android.gms.internal.zzqo.zzb<GetOutputStreamResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_Channel_GetOutputStreamResult, zzt com_google_android_gms_wearable_internal_zzt) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_Channel_GetOutputStreamResult);
            this.aUp = (zzt) zzaa.zzy(com_google_android_gms_wearable_internal_zzt);
        }

        public void zza(GetChannelOutputStreamResponse getChannelOutputStreamResponse) {
            OutputStream outputStream = null;
            if (getChannelOutputStreamResponse.aTP != null) {
                outputStream = new zzq(new AutoCloseOutputStream(getChannelOutputStreamResponse.aTP));
                this.aUp.zza(outputStream.zzcmt());
            }
            zzbc(new zzb(new Status(getChannelOutputStreamResponse.statusCode), outputStream));
        }
    }

    static final class zzj extends zzb<GetConnectedNodesResult> {
        public zzj(com.google.android.gms.internal.zzqo.zzb<GetConnectedNodesResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_NodeApi_GetConnectedNodesResult) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_NodeApi_GetConnectedNodesResult);
        }

        public void zza(GetConnectedNodesResponse getConnectedNodesResponse) {
            List arrayList = new ArrayList();
            arrayList.addAll(getConnectedNodesResponse.aTV);
            zzbc(new com.google.android.gms.wearable.internal.zzbb.zza(zzbk.zzqv(getConnectedNodesResponse.statusCode), arrayList));
        }
    }

    static final class zzk extends zzb<DataItemResult> {
        public zzk(com.google.android.gms.internal.zzqo.zzb<DataItemResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataApi_DataItemResult) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataApi_DataItemResult);
        }

        public void zza(GetDataItemResponse getDataItemResponse) {
            zzbc(new com.google.android.gms.wearable.internal.zzx.zza(zzbk.zzqv(getDataItemResponse.statusCode), getDataItemResponse.aTW));
        }
    }

    static final class zzl extends zzb<DataItemBuffer> {
        public zzl(com.google.android.gms.internal.zzqo.zzb<DataItemBuffer> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataItemBuffer) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataItemBuffer);
        }

        public void zzbr(DataHolder dataHolder) {
            zzbc(new DataItemBuffer(dataHolder));
        }
    }

    static final class zzm extends zzb<GetFdForAssetResult> {
        public zzm(com.google.android.gms.internal.zzqo.zzb<GetFdForAssetResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataApi_GetFdForAssetResult) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataApi_GetFdForAssetResult);
        }

        public void zza(GetFdForAssetResponse getFdForAssetResponse) {
            zzbc(new com.google.android.gms.wearable.internal.zzx.zzc(zzbk.zzqv(getFdForAssetResponse.statusCode), getFdForAssetResponse.aTX));
        }
    }

    static final class zzn extends zzb<GetLocalNodeResult> {
        public zzn(com.google.android.gms.internal.zzqo.zzb<GetLocalNodeResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_NodeApi_GetLocalNodeResult) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_NodeApi_GetLocalNodeResult);
        }

        public void zza(GetLocalNodeResponse getLocalNodeResponse) {
            zzbc(new com.google.android.gms.wearable.internal.zzbb.zzb(zzbk.zzqv(getLocalNodeResponse.statusCode), getLocalNodeResponse.aTY));
        }
    }

    static final class zzo extends zza {
        zzo() {
        }

        public void zza(Status status) {
        }
    }

    static final class zzp extends zzb<OpenChannelResult> {
        public zzp(com.google.android.gms.internal.zzqo.zzb<OpenChannelResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_ChannelApi_OpenChannelResult) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_ChannelApi_OpenChannelResult);
        }

        public void zza(OpenChannelResponse openChannelResponse) {
            zzbc(new zza(zzbk.zzqv(openChannelResponse.statusCode), openChannelResponse.aTp));
        }
    }

    static final class zzq extends zzb<DataItemResult> {
        private final List<FutureTask<Boolean>> zzbrd;

        zzq(com.google.android.gms.internal.zzqo.zzb<DataItemResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataApi_DataItemResult, List<FutureTask<Boolean>> list) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_DataApi_DataItemResult);
            this.zzbrd = list;
        }

        public void zza(PutDataResponse putDataResponse) {
            zzbc(new com.google.android.gms.wearable.internal.zzx.zza(zzbk.zzqv(putDataResponse.statusCode), putDataResponse.aTW));
            if (putDataResponse.statusCode != 0) {
                for (FutureTask cancel : this.zzbrd) {
                    cancel.cancel(true);
                }
            }
        }
    }

    static final class zzr extends zzb<Status> {
        public zzr(com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status);
        }

        public void zza(ChannelSendFileResponse channelSendFileResponse) {
            zzbc(new Status(channelSendFileResponse.statusCode));
        }
    }

    static final class zzs extends zzb<RemoveLocalCapabilityResult> {
        public zzs(com.google.android.gms.internal.zzqo.zzb<RemoveLocalCapabilityResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_CapabilityApi_RemoveLocalCapabilityResult) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_CapabilityApi_RemoveLocalCapabilityResult);
        }

        public void zza(RemoveLocalCapabilityResponse removeLocalCapabilityResponse) {
            zzbc(new com.google.android.gms.wearable.internal.zzj.zza(zzbk.zzqv(removeLocalCapabilityResponse.statusCode)));
        }
    }

    static final class zzt extends zzb<SendMessageResult> {
        public zzt(com.google.android.gms.internal.zzqo.zzb<SendMessageResult> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_MessageApi_SendMessageResult) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_wearable_MessageApi_SendMessageResult);
        }

        public void zza(SendMessageResponse sendMessageResponse) {
            zzbc(new com.google.android.gms.wearable.internal.zzaz.zzb(zzbk.zzqv(sendMessageResponse.statusCode), sendMessageResponse.aiw));
        }
    }

    static final class zzu extends zzb<Status> {
        public zzu(com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status) {
            super(com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status);
        }

        public void zza(ChannelReceiveFileResponse channelReceiveFileResponse) {
            zzbc(new Status(channelReceiveFileResponse.statusCode));
        }
    }

    private static Map<String, CapabilityInfo> zzao(List<CapabilityInfoParcelable> list) {
        Map hashMap = new HashMap(list.size() * 2);
        for (CapabilityInfoParcelable capabilityInfoParcelable : list) {
            hashMap.put(capabilityInfoParcelable.getName(), new com.google.android.gms.wearable.internal.zzj.zzc(capabilityInfoParcelable));
        }
        return hashMap;
    }
}
