package com.pipedrive.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.model.Activity;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.presenter.PersistablePresenter;

abstract class ActivityPresenter extends PersistablePresenter<ActivityView> {
    abstract void associateDeal(@Nullable Deal deal);

    abstract void associateDealBySqlId(@NonNull Long l);

    abstract void associateOrganization(@Nullable Organization organization);

    abstract void associateOrganizationBySqlId(@NonNull Long l);

    abstract void associatePerson(@Nullable Person person);

    abstract void associatePersonBySqlId(@NonNull Long l);

    abstract void changeActivity(@NonNull Activity activity);

    abstract void deleteActivity(@NonNull Long l);

    @Nullable
    abstract Activity getActivity();

    abstract boolean isCachedActivityNotDoneAndTypeCall();

    abstract boolean isModified();

    abstract boolean registerActivityForIdentifyingReopenAfterCallSummaryViews();

    abstract void restoreOrRequestActivity(@Nullable Long l);

    abstract void restoreOrRequestActivityNoteUpdate(@Nullable String str);

    abstract void restoreOrRequestNewActivityForDeal(@NonNull Long l);

    abstract void restoreOrRequestNewActivityForOrganization(@NonNull Long l);

    abstract void restoreOrRequestNewActivityForPerson(@NonNull Long l);

    abstract void restoreOrRequestNewActivityOnDateTime(@NonNull Long l);

    abstract void restoreOrRequestNewFollowUpActivity(long j);

    public ActivityPresenter(@NonNull Session session, @Nullable Bundle savedState) {
        super(session, savedState);
    }
}
