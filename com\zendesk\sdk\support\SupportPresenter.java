package com.zendesk.sdk.support;

import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.R;
import com.zendesk.sdk.model.helpcenter.SearchArticle;
import com.zendesk.sdk.model.settings.SafeMobileSettings;
import com.zendesk.sdk.network.NetworkAware;
import com.zendesk.sdk.network.NetworkInfoProvider;
import com.zendesk.sdk.network.RetryAction;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.support.SupportMvp.ErrorType;
import com.zendesk.sdk.support.SupportMvp.Model;
import com.zendesk.sdk.support.SupportMvp.Presenter;
import com.zendesk.sdk.support.SupportMvp.View;
import com.zendesk.service.ErrorResponse;
import com.zendesk.service.ZendeskCallback;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class SupportPresenter implements Presenter, NetworkAware {
    private static final String LOG_TAG = "SupportPresenter";
    private static final Integer NETWORK_AWARE_ID = Integer.valueOf(31);
    private static final Integer RETRY_ACTION_ID = Integer.valueOf(8642);
    private SupportUiConfig config;
    private Set<RetryAction> internalRetryActions = new HashSet();
    private SafeMobileSettings mobileSettings;
    private Model model;
    private NetworkInfoProvider networkInfoProvider;
    private boolean networkPreviouslyUnavailable;
    private View view;

    @VisibleForTesting
    class ViewSafeRetryZendeskCallback extends ZendeskCallback<List<SearchArticle>> {
        private String query;

        ViewSafeRetryZendeskCallback(String query) {
            this.query = query;
        }

        public void onSuccess(final List<SearchArticle> searchArticles) {
            if (SupportPresenter.this.view != null) {
                SupportPresenter.this.view.hideLoadingState();
                SupportPresenter.this.view.showSearchResults(searchArticles, this.query);
                if (SupportPresenter.this.config.isShowContactUsButton()) {
                    SupportPresenter.this.view.showContactUsButton();
                    return;
                }
                return;
            }
            SupportPresenter.this.internalRetryActions.add(new RetryAction() {
                public void onRetry() {
                    ViewSafeRetryZendeskCallback.this.onSuccess(searchArticles);
                }
            });
        }

        public void onError(final ErrorResponse errorResponse) {
            if (SupportPresenter.this.view != null) {
                SupportPresenter.this.view.hideLoadingState();
                SupportPresenter.this.view.showErrorWithRetry(ErrorType.ARTICLES_LOAD, new RetryAction() {
                    public void onRetry() {
                        SupportPresenter.this.onSearchSubmit(ViewSafeRetryZendeskCallback.this.query);
                    }
                });
                return;
            }
            SupportPresenter.this.internalRetryActions.add(new RetryAction() {
                public void onRetry() {
                    ViewSafeRetryZendeskCallback.this.onError(errorResponse);
                }
            });
        }
    }

    SupportPresenter(View view, Model model, NetworkInfoProvider networkInfoProvider) {
        this.view = view;
        this.model = model;
        this.networkInfoProvider = networkInfoProvider;
        this.mobileSettings = ZendeskConfig.INSTANCE.getMobileSettings();
    }

    private void extractDataFromBundle(Bundle bundle) {
        this.config = builderWithHelpCenterIdsFromBundle(bundle).withLabelNames(bundle.getStringArray("extra_label_names")).withShowContactUsButton(bundle.getBoolean("extra_show_contact_us_button", true)).withAddListPaddingBottom(bundle.getBoolean("extra_show_contact_us_button", true)).withCollapseCategories(bundle.getBoolean("extra_categories_collapsed", false)).withShowConversationsMenuButton(bundle.getBoolean("extra_show_conversations_menu_button", true)).build();
    }

    private Builder builderWithHelpCenterIdsFromBundle(Bundle bundle) {
        Builder builder = new Builder();
        boolean idsSpecified = false;
        if (bundle.getLongArray("extra_category_ids") != null) {
            builder = builder.withCategoryIds(convertToLongList(bundle.getLongArray("extra_category_ids")));
            idsSpecified = true;
        }
        if (bundle.getLongArray("extra_section_ids") != null) {
            builder = builder.withSectionIds(convertToLongList(bundle.getLongArray("extra_section_ids")));
            idsSpecified = true;
        }
        if (!idsSpecified) {
            logNoCategoriesOrSectionsSet();
        }
        return builder;
    }

    private void logNoCategoriesOrSectionsSet() {
        Logger.d(LOG_TAG, "No category or section IDs have been set.", new Object[0]);
    }

    public void onResume(View view) {
        this.view = view;
        this.networkInfoProvider.addNetworkAwareListener(NETWORK_AWARE_ID, this);
        this.networkInfoProvider.register();
        if (!this.networkInfoProvider.isNetworkAvailable()) {
            view.showError(R.string.network_activity_no_connectivity);
            view.hideLoadingState();
            this.networkPreviouslyUnavailable = true;
        }
        invokeRetryActions();
    }

    private void invokeRetryActions() {
        for (RetryAction retryAction : this.internalRetryActions) {
            retryAction.onRetry();
        }
        this.internalRetryActions.clear();
    }

    public void onPause() {
        this.view = null;
        this.networkInfoProvider.removeNetworkAwareListener(NETWORK_AWARE_ID);
        this.networkInfoProvider.removeRetryAction(RETRY_ACTION_ID);
        this.networkInfoProvider.unregister();
    }

    private List<Long> convertToLongList(long[] longArray) {
        if (longArray == null) {
            return new ArrayList();
        }
        List<Long> list = new ArrayList(longArray.length);
        for (long entry : longArray) {
            list.add(Long.valueOf(entry));
        }
        return list;
    }

    public void onSearchSubmit(final String query) {
        if (this.networkInfoProvider.isNetworkAvailable()) {
            this.view.dismissError();
            this.view.showLoadingState();
            this.view.clearSearchResults();
            this.model.search(this.config.getCategoryIds(), this.config.getSectionIds(), query, this.config.getLabelNames(), new ViewSafeRetryZendeskCallback(query));
            return;
        }
        this.networkInfoProvider.addRetryAction(RETRY_ACTION_ID, new RetryAction() {
            public void onRetry() {
                SupportPresenter.this.onSearchSubmit(query);
            }
        });
    }

    public void onLoad() {
        if (!this.config.isShowContactUsButton()) {
            return;
        }
        if (this.view != null) {
            this.view.showContactUsButton();
        } else {
            this.internalRetryActions.add(new RetryAction() {
                public void onRetry() {
                    SupportPresenter.this.view.showContactUsButton();
                }
            });
        }
    }

    public void onErrorWithRetry(final ErrorType errorType, final RetryAction action) {
        if (this.view == null) {
            this.internalRetryActions.add(new RetryAction() {
                public void onRetry() {
                    if (SupportPresenter.this.view != null && SupportPresenter.this.view.isShowingHelp()) {
                        SupportPresenter.this.view.hideLoadingState();
                        SupportPresenter.this.view.showErrorWithRetry(errorType, action);
                    }
                }
            });
        } else if (this.view.isShowingHelp()) {
            this.view.hideLoadingState();
            this.view.showErrorWithRetry(errorType, action);
        }
    }

    public void onNetworkAvailable() {
        Logger.d(LOG_TAG, "Network is available.", new Object[0]);
        if (this.networkPreviouslyUnavailable) {
            this.networkPreviouslyUnavailable = false;
            if (this.view != null) {
                this.view.dismissError();
                return;
            } else {
                this.internalRetryActions.add(new RetryAction() {
                    public void onRetry() {
                        SupportPresenter.this.view.dismissError();
                    }
                });
                return;
            }
        }
        Logger.d(LOG_TAG, "Network was not previously unavailable, no need to dismiss Snackbar", new Object[0]);
    }

    public void onNetworkUnavailable() {
        Logger.d(LOG_TAG, "Network is unavailable.", new Object[0]);
        this.networkPreviouslyUnavailable = true;
        if (this.view != null) {
            this.view.showError(R.string.network_activity_no_connectivity);
            this.view.hideLoadingState();
        }
    }

    public void initWithBundle(final Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            extractDataFromBundle(savedInstanceState);
        } else {
            this.config = new Builder().build();
            logNoCategoriesOrSectionsSet();
        }
        this.view.showLoadingState();
        this.model.getSettings(new ZendeskCallback<SafeMobileSettings>() {
            public void onSuccess(SafeMobileSettings safeMobileSettings) {
                if (SupportPresenter.this.view != null) {
                    SupportPresenter.this.view.hideLoadingState();
                } else {
                    SupportPresenter.this.internalRetryActions.add(new RetryAction() {
                        public void onRetry() {
                            SupportPresenter.this.view.hideLoadingState();
                        }
                    });
                }
                SupportPresenter.this.mobileSettings = safeMobileSettings;
                if (safeMobileSettings.isHelpCenterEnabled()) {
                    Logger.d(SupportPresenter.LOG_TAG, "Help center is enabled. starting with Help Center", new Object[0]);
                    if (SupportPresenter.this.view != null) {
                        SupportPresenter.this.view.showHelp(SupportPresenter.this.config);
                    } else {
                        SupportPresenter.this.internalRetryActions.add(new RetryAction() {
                            public void onRetry() {
                                SupportPresenter.this.view.showHelp(SupportPresenter.this.config);
                            }
                        });
                    }
                    if (savedInstanceState != null && savedInstanceState.getBoolean("fab_visibility")) {
                        Logger.d(SupportPresenter.LOG_TAG, "Saved instance states that we should show the contact FAB", new Object[0]);
                        if (SupportPresenter.this.view != null) {
                            SupportPresenter.this.view.showContactUsButton();
                            return;
                        } else {
                            SupportPresenter.this.internalRetryActions.add(new RetryAction() {
                                public void onRetry() {
                                    SupportPresenter.this.view.showContactUsButton();
                                }
                            });
                            return;
                        }
                    }
                    return;
                }
                Logger.d(SupportPresenter.LOG_TAG, "Help center is disabled", new Object[0]);
                if (safeMobileSettings.isConversationsEnabled()) {
                    Logger.d(SupportPresenter.LOG_TAG, "Starting with conversations", new Object[0]);
                    if (SupportPresenter.this.view != null) {
                        SupportPresenter.this.view.showRequestList();
                        SupportPresenter.this.view.exitActivity();
                        return;
                    }
                    SupportPresenter.this.internalRetryActions.add(new RetryAction() {
                        public void onRetry() {
                            SupportPresenter.this.view.showRequestList();
                            SupportPresenter.this.view.exitActivity();
                        }
                    });
                    return;
                }
                Logger.d(SupportPresenter.LOG_TAG, "Starting with contact", new Object[0]);
                if (SupportPresenter.this.view != null) {
                    SupportPresenter.this.view.showContactZendesk();
                    SupportPresenter.this.view.exitActivity();
                    return;
                }
                SupportPresenter.this.internalRetryActions.add(new RetryAction() {
                    public void onRetry() {
                        SupportPresenter.this.view.showContactZendesk();
                        SupportPresenter.this.view.exitActivity();
                    }
                });
            }

            public void onError(ErrorResponse errorResponse) {
                Logger.e(SupportPresenter.LOG_TAG, "Failed to get mobile settings. Cannot determine start screen.", new Object[0]);
                Logger.e(SupportPresenter.LOG_TAG, errorResponse);
                if (SupportPresenter.this.view != null) {
                    SupportPresenter.this.view.hideLoadingState();
                    SupportPresenter.this.view.exitActivity();
                    return;
                }
                SupportPresenter.this.internalRetryActions.add(new RetryAction() {
                    public void onRetry() {
                        SupportPresenter.this.view.hideLoadingState();
                        SupportPresenter.this.view.exitActivity();
                    }
                });
            }
        });
    }

    public boolean shouldShowConversationsMenuItem() {
        return this.mobileSettings.isConversationsEnabled() && this.config.isShowConversationsMenuButton();
    }

    public boolean shouldShowSearchMenuItem() {
        return this.mobileSettings.hasHelpCenterSettings();
    }
}
