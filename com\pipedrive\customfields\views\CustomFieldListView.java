package com.pipedrive.customfields.views;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.customfields.adapter.CustomFieldListAdapter;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.customfields.CustomField;
import com.pipedrive.util.ViewUtil;
import com.pipedrive.views.DividerItemDecoration;
import java.util.List;

public abstract class CustomFieldListView extends FrameLayout {
    @NonNull
    private TextView mEmptyView;
    @Nullable
    private OnItemClickListener mOnItemClickListener;
    @NonNull
    private RecyclerView mRecyclerView;
    @NonNull
    Long mSqlId;

    @MainThread
    public interface OnItemClickListener {
        void onAddressClicked(@NonNull Uri uri);

        void onCustomFieldClicked(@NonNull CustomField customField);

        void onCustomFieldLongClicked(@NonNull String str);

        void onOrganizationClicked(@NonNull Organization organization);

        void onPersonClicked(@NonNull Person person);

        void onPhoneClicked(@NonNull String str, boolean z);
    }

    @MainThread
    public static abstract class OnItemClickAdapter implements OnItemClickListener {
        public void onCustomFieldClicked(@NonNull CustomField customField) {
        }

        public void onCustomFieldLongClicked(@NonNull String value) {
        }

        public void onPhoneClicked(@NonNull String number, boolean clickedFromCustomField) {
        }

        public void onPersonClicked(@NonNull Person person) {
        }

        public void onOrganizationClicked(@NonNull Organization organization) {
        }

        public void onAddressClicked(@NonNull Uri uri) {
        }
    }

    @NonNull
    protected abstract List<CustomField> getCustomFields(@NonNull Session session);

    @NonNull
    protected abstract String getSpecialFieldsType();

    public CustomFieldListView(Context context) {
        this(context, null);
    }

    public CustomFieldListView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomFieldListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        this.mRecyclerView = new RecyclerView(getContext());
        this.mRecyclerView.setId(R.id.list);
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
        this.mRecyclerView.setHasFixedSize(true);
        this.mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext()));
        this.mRecyclerView.setPadding(this.mRecyclerView.getPaddingLeft(), this.mRecyclerView.getPaddingTop(), this.mRecyclerView.getPaddingRight(), this.mRecyclerView.getPaddingBottom() + ViewUtil.getFooterForListWithFloatingButtonHeight(getContext()));
        this.mRecyclerView.setClipToPadding(false);
        addView(this.mRecyclerView, generateDefaultLayoutParams());
        this.mEmptyView = new TextView(getContext());
        this.mEmptyView.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_dark_grey));
        this.mEmptyView.setTextSize(2, 18.0f);
        this.mEmptyView.setText(getContext().getString(R.string.label_empty_header_custom_fields_list));
        this.mEmptyView.setGravity(17);
        this.mEmptyView.setVisibility(8);
        LayoutParams emptyViewLayoutParams = generateDefaultLayoutParams();
        emptyViewLayoutParams.gravity = 17;
        addView(this.mEmptyView, emptyViewLayoutParams);
    }

    public final void setup(@NonNull Session session, @NonNull Long sqlId, @Nullable OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
        this.mSqlId = sqlId;
        CustomFieldListAdapter adapter = createAdapter(session);
        this.mRecyclerView.setAdapter(adapter);
        updateEmptyViewVisibility(adapter);
    }

    private void updateEmptyViewVisibility(CustomFieldListAdapter adapter) {
        if (adapter.getItemCount() == 0) {
            this.mEmptyView.setVisibility(0);
        } else {
            this.mEmptyView.setVisibility(8);
        }
    }

    @NonNull
    private CustomFieldListAdapter createAdapter(@NonNull Session session) {
        CustomFieldListAdapter adapter = new CustomFieldListAdapter(session, getCustomFields(session), getSpecialFieldsType(), this.mSqlId);
        adapter.setOnItemClickListener(this.mOnItemClickListener);
        return adapter;
    }

    public final void refreshContent(@NonNull Session session) {
        CustomFieldListAdapter newAdapter = createAdapter(session);
        this.mRecyclerView.swapAdapter(newAdapter, true);
        updateEmptyViewVisibility(newAdapter);
    }
}
