package com.pipedrive.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.adapter.filter.FilterContentPipelineAdapter.FilterRow;

public class Filter extends BaseDatasourceEntity implements FilterRow {
    public static final String ALL_TYPES = "_all_types_";
    public static final String TYPE_DEALS = "deals";
    public static final String TYPE_ORGANIZATIONS = "org";
    public static final String TYPE_PEOPLE = "people";
    public static final String TYPE_PRODUCTS = "products";
    @NonNull
    private final Boolean isActive;
    @NonNull
    private final Boolean isPrivate;
    @NonNull
    private final String name;
    @NonNull
    private final String type;

    @NonNull
    public static Filter create(@NonNull String name, @NonNull String type, @NonNull Boolean isActive) {
        return create(null, null, name, type, isActive, Boolean.valueOf(false));
    }

    @NonNull
    public static Filter create(@NonNull Integer pipedriveId, @NonNull String name, @NonNull String type, @NonNull Boolean isActive, @NonNull Boolean isPrivate) {
        return new Filter(null, pipedriveId, name, type, isActive, isPrivate);
    }

    @NonNull
    public static Filter create(@Nullable Long sqlId, @Nullable Integer pipedriveId, @NonNull String name, @NonNull String type, @NonNull Boolean isActive, @NonNull Boolean isPrivate) {
        return new Filter(sqlId, pipedriveId, name, type, isActive, isPrivate);
    }

    public Filter(@Nullable Long sqlId, @Nullable Integer pipedriveId, @NonNull String name, @NonNull String type, @NonNull Boolean isActive, @NonNull Boolean isPrivate) {
        if (sqlId != null) {
            setSqlId(sqlId.longValue());
        }
        if (pipedriveId != null) {
            setPipedriveId(pipedriveId.intValue());
        }
        this.name = name;
        this.type = type;
        this.isActive = isActive;
        this.isPrivate = isPrivate;
    }

    @NonNull
    public String getName() {
        return this.name;
    }

    @NonNull
    public String getType() {
        return this.type;
    }

    @NonNull
    public Boolean isActive() {
        return this.isActive;
    }

    @NonNull
    public String getLabel() {
        return getName();
    }

    @Nullable
    public User getUser() {
        return null;
    }

    @Nullable
    public Filter getFilter() {
        return this;
    }

    @NonNull
    public Boolean isPrivate() {
        return this.isPrivate;
    }
}
