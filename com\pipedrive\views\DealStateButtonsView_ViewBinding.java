package com.pipedrive.views;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class DealStateButtonsView_ViewBinding implements Unbinder {
    private DealStateButtonsView target;
    private View view2131821149;
    private View view2131821150;
    private View view2131821151;
    private View view2131821153;
    private View view2131821154;

    @UiThread
    public DealStateButtonsView_ViewBinding(DealStateButtonsView target) {
        this(target, target);
    }

    @UiThread
    public DealStateButtonsView_ViewBinding(final DealStateButtonsView target, View source) {
        this.target = target;
        target.mButtons = Utils.findRequiredView(source, R.id.dealStateButtons.buttons, "field 'mButtons'");
        target.mStateButtons = Utils.findRequiredView(source, R.id.dealStateButtons.stateButtons, "field 'mStateButtons'");
        View view = Utils.findRequiredView(source, R.id.dealStateButtons.wonState, "field 'mWonStateButton' and method 'onWonStateButtonClicked'");
        target.mWonStateButton = view;
        this.view2131821151 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onWonStateButtonClicked();
            }
        });
        view = Utils.findRequiredView(source, R.id.dealStateButtons.lostState, "field 'mLostStateButton' and method 'onLostStateButtonClicked'");
        target.mLostStateButton = view;
        this.view2131821149 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onLostStateButtonClicked();
            }
        });
        view = Utils.findRequiredView(source, R.id.dealStateButtons.deletedState, "field 'mDeletedStateButton' and method 'onDeletedStateButtonClicked'");
        target.mDeletedStateButton = view;
        this.view2131821150 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onDeletedStateButtonClicked();
            }
        });
        view = Utils.findRequiredView(source, R.id.dealStateButtons.won, "method 'onWonButtonClicked'");
        this.view2131821153 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onWonButtonClicked();
            }
        });
        view = Utils.findRequiredView(source, R.id.dealStateButtons.lost, "method 'onLostButtonClicked'");
        this.view2131821154 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onLostButtonClicked();
            }
        });
    }

    @CallSuper
    public void unbind() {
        DealStateButtonsView target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mButtons = null;
        target.mStateButtons = null;
        target.mWonStateButton = null;
        target.mLostStateButton = null;
        target.mDeletedStateButton = null;
        this.view2131821151.setOnClickListener(null);
        this.view2131821151 = null;
        this.view2131821149.setOnClickListener(null);
        this.view2131821149 = null;
        this.view2131821150.setOnClickListener(null);
        this.view2131821150 = null;
        this.view2131821153.setOnClickListener(null);
        this.view2131821153 = null;
        this.view2131821154.setOnClickListener(null);
        this.view2131821154 = null;
    }
}
