package com.pipedrive.sync;

import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.pipedrive.LoginActivity;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.logging.Log;

public class AccountManagerReceiver extends BroadcastReceiver {
    private static final String TAG = AccountManagerReceiver.class.getSimpleName();

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equalsIgnoreCase("android.accounts.LOGIN_ACCOUNTS_CHANGED")) {
            Log.d(TAG, "Accounts have changed. Checking if accounts exist for app. If there is no accounts, log appliction out.");
            if (AccountManager.get(context).getAccountsByType("com.pipedrive.account").length <= 0) {
                Log.d(TAG, "No accounts exist, log user out. Login will create new accounts.");
                if (PipedriveApp.getSessionManager().hasActiveSession()) {
                    LoginActivity.logOut(context, true);
                    return;
                }
                return;
            }
            Log.d(TAG, "We have correct account. Do not log user out.");
        }
    }
}
