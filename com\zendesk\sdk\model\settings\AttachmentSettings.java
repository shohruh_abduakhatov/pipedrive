package com.zendesk.sdk.model.settings;

public class AttachmentSettings {
    private boolean enabled;
    private long maxAttachmentSize;

    public boolean isEnabled() {
        return this.enabled;
    }

    public long getMaxAttachmentSize() {
        return this.maxAttachmentSize;
    }
}
