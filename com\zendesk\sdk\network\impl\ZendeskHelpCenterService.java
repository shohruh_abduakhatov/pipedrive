package com.zendesk.sdk.network.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.helpcenter.Article;
import com.zendesk.sdk.model.helpcenter.ArticleResponse;
import com.zendesk.sdk.model.helpcenter.ArticleVoteResponse;
import com.zendesk.sdk.model.helpcenter.ArticlesListResponse;
import com.zendesk.sdk.model.helpcenter.ArticlesSearchResponse;
import com.zendesk.sdk.model.helpcenter.Attachment;
import com.zendesk.sdk.model.helpcenter.AttachmentResponse;
import com.zendesk.sdk.model.helpcenter.AttachmentType;
import com.zendesk.sdk.model.helpcenter.CategoriesResponse;
import com.zendesk.sdk.model.helpcenter.Category;
import com.zendesk.sdk.model.helpcenter.CategoryResponse;
import com.zendesk.sdk.model.helpcenter.RecordArticleViewRequest;
import com.zendesk.sdk.model.helpcenter.Section;
import com.zendesk.sdk.model.helpcenter.SectionResponse;
import com.zendesk.sdk.model.helpcenter.SectionsResponse;
import com.zendesk.sdk.model.helpcenter.SortBy;
import com.zendesk.sdk.model.helpcenter.SortOrder;
import com.zendesk.sdk.model.helpcenter.SuggestedArticleResponse;
import com.zendesk.sdk.model.helpcenter.User;
import com.zendesk.sdk.model.helpcenter.help.HelpResponse;
import com.zendesk.sdk.network.HelpCenterService;
import com.zendesk.service.ErrorResponseAdapter;
import com.zendesk.service.RetrofitZendeskCallbackAdapter;
import com.zendesk.service.RetrofitZendeskCallbackAdapter.RequestExtractor;
import com.zendesk.service.ZendeskCallback;
import com.zendesk.util.CollectionUtils;
import com.zendesk.util.LocaleUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

class ZendeskHelpCenterService {
    private static final String LOG_TAG = "ZendeskHelpCenterService";
    private static final int NUMBER_PER_PAGE = 1000;
    private final HelpCenterService helpCenterService;

    ZendeskHelpCenterService(HelpCenterService helpCenterService) {
        this.helpCenterService = helpCenterService;
    }

    void getHelp(@Nullable String authorizationHeader, Locale locale, String categoryIds, String sectionIds, String include, int articlesPerPageLimit, String labelNames, ZendeskCallback<HelpResponse> callback) {
        this.helpCenterService.getHelp(authorizationHeader, LocaleUtil.toLanguageTag(locale), categoryIds, sectionIds, include, articlesPerPageLimit, labelNames, 1000, SortBy.CREATED_AT.getApiValue(), SortOrder.DESCENDING.getApiValue()).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }

    public void getCategories(@Nullable String authorizationHeader, Locale locale, ZendeskCallback<List<Category>> callback) {
        this.helpCenterService.getCategories(authorizationHeader, LocaleUtil.toLanguageTag(locale)).enqueue(new RetrofitZendeskCallbackAdapter(callback, new RequestExtractor<CategoriesResponse, List<Category>>() {
            public List<Category> extract(CategoriesResponse data) {
                return data.getCategories();
            }
        }));
    }

    public void getSectionsForCategory(@Nullable String authorizationHeader, Long categoryId, Locale locale, ZendeskCallback<List<Section>> callback) {
        this.helpCenterService.getSections(authorizationHeader, LocaleUtil.toLanguageTag(locale), categoryId, 1000).enqueue(new RetrofitZendeskCallbackAdapter(callback, new RequestExtractor<SectionsResponse, List<Section>>() {
            public List<Section> extract(SectionsResponse data) {
                return data.getSections();
            }
        }));
    }

    public void getArticlesForSection(@Nullable String authorizationHeader, Long sectionId, Locale locale, String include, ZendeskCallback<List<Article>> callback) {
        this.helpCenterService.getArticles(authorizationHeader, LocaleUtil.toLanguageTag(locale), sectionId, include, 1000).enqueue(new RetrofitZendeskCallbackAdapter(callback, new RequestExtractor<ArticlesListResponse, List<Article>>() {
            public List<Article> extract(ArticlesListResponse articlesListResponse) {
                return ZendeskHelpCenterService.this.matchArticlesWithUsers(articlesListResponse.getUsers(), articlesListResponse.getArticles());
            }
        }));
    }

    List<Article> matchArticlesWithUsers(List<User> users, List<Article> articles) {
        Map<Long, User> mapOfUsers = new HashMap();
        for (User user : users) {
            User user2;
            mapOfUsers.put(user2.getId(), user2);
        }
        List<Article> matchedArticles = new ArrayList();
        for (Article article : articles) {
            user2 = (User) mapOfUsers.get(article.getAuthorId());
            if (user2 != null) {
                article.setAuthor(user2);
            }
            matchedArticles.add(article);
        }
        return matchedArticles;
    }

    public void listArticles(@Nullable String authorizationHeader, String labelNames, Locale locale, String include, String sortBy, String sortOrder, Integer page, Integer resultsPerPage, ZendeskCallback<ArticlesListResponse> callback) {
        this.helpCenterService.listArticles(authorizationHeader, LocaleUtil.toLanguageTag(locale), labelNames, include, sortBy, sortOrder, page, resultsPerPage).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }

    public void searchArticles(@Nullable String authorizationHeader, String query, Locale locale, String include, String labelNames, String categoryIds, String sectionIds, Integer page, Integer resultsPerPage, ZendeskCallback<ArticlesSearchResponse> callback) {
        this.helpCenterService.searchArticles(authorizationHeader, query, LocaleUtil.toLanguageTag(locale), include, labelNames, categoryIds, sectionIds, page, resultsPerPage).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }

    public void getArticle(@Nullable String authorizationHeader, Long articleId, Locale locale, String include, ZendeskCallback<Article> callback) {
        this.helpCenterService.getArticle(authorizationHeader, LocaleUtil.toLanguageTag(locale), articleId, include).enqueue(new RetrofitZendeskCallbackAdapter(callback, new RequestExtractor<ArticleResponse, Article>() {
            public Article extract(ArticleResponse articleResponse) {
                return ZendeskHelpCenterService.this.matchArticleWithUsers(articleResponse.getArticle(), CollectionUtils.ensureEmpty(articleResponse.getUsers()));
            }
        }));
    }

    @NonNull
    Article matchArticleWithUsers(@Nullable Article article, @NonNull List<User> users) {
        if (article == null) {
            return new Article();
        }
        for (User user : users) {
            if (user.getId() != null && user.getId().equals(article.getAuthorId())) {
                article.setAuthor(user);
                return article;
            }
        }
        return article;
    }

    public void getSectionById(@Nullable String authorizationHeader, Long sectionId, Locale locale, ZendeskCallback<Section> callback) {
        this.helpCenterService.getSectionById(authorizationHeader, LocaleUtil.toLanguageTag(locale), sectionId).enqueue(new RetrofitZendeskCallbackAdapter(callback, new RequestExtractor<SectionResponse, Section>() {
            public Section extract(SectionResponse data) {
                return data.getSection();
            }
        }));
    }

    public void getCategoryById(@Nullable String authorizationHeader, Long categoryId, Locale locale, ZendeskCallback<Category> callback) {
        this.helpCenterService.getCategoryById(authorizationHeader, LocaleUtil.toLanguageTag(locale), categoryId).enqueue(new RetrofitZendeskCallbackAdapter(callback, new RequestExtractor<CategoryResponse, Category>() {
            public Category extract(CategoryResponse data) {
                return data.getCategory();
            }
        }));
    }

    public void getAttachments(@Nullable String authorizationHeader, Locale locale, Long articleId, AttachmentType type, ZendeskCallback<List<Attachment>> callback) {
        if (type == null) {
            String error = "getAttachments() was called with null attachment type";
            Logger.e(LOG_TAG, "getAttachments() was called with null attachment type", new Object[0]);
            if (callback != null) {
                callback.onError(new ErrorResponseAdapter("getAttachments() was called with null attachment type"));
                return;
            }
            return;
        }
        this.helpCenterService.getAttachments(authorizationHeader, LocaleUtil.toLanguageTag(locale), articleId, type.getAttachmentType()).enqueue(new RetrofitZendeskCallbackAdapter(callback, new RequestExtractor<AttachmentResponse, List<Attachment>>() {
            public List<Attachment> extract(AttachmentResponse data) {
                return data.getArticleAttachments();
            }
        }));
    }

    public void upvoteArticle(@Nullable String authorizationHeader, Long articleId, String body, ZendeskCallback<ArticleVoteResponse> callback) {
        if (articleId == null) {
            String reason = "The article id was null, can not create up vote";
            Logger.e(LOG_TAG, "The article id was null, can not create up vote", new Object[0]);
            if (callback != null) {
                callback.onError(new ErrorResponseAdapter("The article id was null, can not create up vote"));
                return;
            }
            return;
        }
        this.helpCenterService.upvoteArticle(authorizationHeader, articleId, body).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }

    public void downvoteArticle(@Nullable String authorizationHeader, Long articleId, String body, ZendeskCallback<ArticleVoteResponse> callback) {
        if (articleId == null) {
            String reason = "The article id was null, can not create down vote";
            Logger.e(LOG_TAG, "The article id was null, can not create down vote", new Object[0]);
            if (callback != null) {
                callback.onError(new ErrorResponseAdapter("The article id was null, can not create down vote"));
                return;
            }
            return;
        }
        this.helpCenterService.downvoteArticle(authorizationHeader, articleId, body).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }

    public void deleteVote(@Nullable String authorizationHeader, Long voteId, ZendeskCallback<Void> callback) {
        if (voteId == null) {
            String reason = "The vote id was null, can not delete the vote";
            Logger.e(LOG_TAG, "The vote id was null, can not delete the vote", new Object[0]);
            if (callback != null) {
                callback.onError(new ErrorResponseAdapter("The vote id was null, can not delete the vote"));
                return;
            }
            return;
        }
        this.helpCenterService.deleteVote(authorizationHeader, voteId).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }

    public void getSuggestedArticles(String authorizationHeader, String query, Locale locale, String labelNames, Long category, Long section, ZendeskCallback<SuggestedArticleResponse> callback) {
        this.helpCenterService.getSuggestedArticles(authorizationHeader, query, LocaleUtil.toLanguageTag(locale), labelNames, category, section).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }

    public void submitRecordArticleView(String authorizationHeader, Long articleId, Locale locale, RecordArticleViewRequest recordArticleViewRequest, @Nullable ZendeskCallback<Void> callback) {
        this.helpCenterService.submitRecordArticleView(authorizationHeader, articleId, LocaleUtil.toLanguageTag(locale), recordArticleViewRequest).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }
}
