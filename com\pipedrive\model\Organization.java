package com.pipedrive.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.newrelic.agent.android.instrumentation.JSONObjectInstrumentation;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.contact.Email;
import com.pipedrive.model.contact.Phone;
import com.pipedrive.model.delta.ModelProperty;
import com.pipedrive.util.CustomFieldsUtil;
import com.pipedrive.util.clone.Cloner;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class Organization extends Contact<Organization> {
    public static final Creator CREATOR = new Creator() {
        public Organization createFromParcel(Parcel in) {
            return new Organization(in);
        }

        public Organization[] newArray(int size) {
            return new Organization[size];
        }
    };
    private static final String DEFAULT_FIELD_ADDRESS = "address";
    private final int OPTIONAL_FIELD_DOES_NOT_EXIST = 0;
    private final int OPTIONAL_FIELD_EXIST = 1;
    @ModelProperty
    private String address;
    @Nullable
    @ModelProperty
    private Double addressLatitude;
    @Nullable
    @ModelProperty
    private Double addressLongitude;
    private String email;
    private String phone;

    public Organization(Parcel in) {
        readFromParcel(in);
    }

    private Organization(@Nullable Long pipedriveId, @NonNull String name) {
        if (pipedriveId != null) {
            setPipedriveId(pipedriveId);
        }
        setName(name);
    }

    @Nullable
    public static Organization createShadowOrganization(@IntRange(from = 1) @NonNull Long pipedriveId, @NonNull String name) {
        Organization organization = createFullOrganization(pipedriveId, name);
        if (organization != null) {
            organization.setShadow(true);
        }
        return organization;
    }

    @Nullable
    public static Organization createLocalOrganization(@NonNull String name) {
        if (!name.isEmpty()) {
            return new Organization(null, name);
        }
        LogJourno.reportEvent(EVENT.MODEL_CANNOT_CREATE_ORGANIZATION_WITH_EMPTY_NAME);
        return null;
    }

    @Nullable
    public static Organization createFullOrganization(@IntRange(from = 1) @NonNull Long pipedriveId, @NonNull String name) {
        if (name.isEmpty()) {
            LogJourno.reportEvent(EVENT.MODEL_CANNOT_CREATE_ORGANIZATION_WITH_EMPTY_NAME);
            return null;
        } else if (pipedriveId.longValue() > 0) {
            return new Organization(pipedriveId, name);
        } else {
            return null;
        }
    }

    public static boolean isAddressFieldDefault(@NonNull Session session) {
        return "address".equals(session.getOrgAddressField(null));
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return this.address;
    }

    @Nullable
    public Integer getPictureId() {
        return null;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.phone);
        dest.writeString(this.email);
        dest.writeString(this.address);
        if (this.addressLatitude != null) {
            dest.writeInt(1);
            dest.writeDouble(this.addressLatitude.doubleValue());
        } else {
            dest.writeInt(0);
        }
        if (this.addressLongitude != null) {
            dest.writeInt(1);
            dest.writeDouble(this.addressLongitude.doubleValue());
            return;
        }
        dest.writeInt(0);
    }

    protected void readFromParcel(Parcel in) {
        super.readFromParcel(in);
        this.phone = in.readString();
        this.email = in.readString();
        this.address = in.readString();
        if (in.readInt() == 1) {
            this.addressLatitude = Double.valueOf(in.readDouble());
        }
        if (in.readInt() == 1) {
            this.addressLongitude = Double.valueOf(in.readDouble());
        }
    }

    public final JSONObject getJSON(@Nullable Session session) throws JSONException {
        JSONObject orgJson = new JSONObject();
        super.appendJSONParams(orgJson);
        if (!(session == null || getAddress() == null || !isAddressFieldDefault(session))) {
            orgJson.put("address", getAddress());
        }
        setCustomFields(CustomFieldsUtil.updateOrgContactReferencesInCustomFields(getCustomFields(), PipedriveApp.getActiveSession(), "organization"));
        CustomFieldsUtil.addCustomFieldsToObject(getCustomFields(), orgJson, "organization");
        return orgJson;
    }

    public Organization getDeepCopy() {
        return (Organization) Cloner.cloneParcelable(this);
    }

    public String toString() {
        try {
            String jSONObjectInstrumentation;
            String str = "Organization (%s) SQL id=%s; [pipedriveId=%s; name=%s; phone=%s; email=%s; updateTime=%s; addTime=%s; address=%s; addressLatitude=%f, addressLongitude=%f; isActive=%s; ownerPipedriveId=%s; visibleTo=%s; [json: %s]]";
            Object[] objArr = new Object[15];
            objArr[0] = Integer.valueOf(hashCode());
            objArr[1] = getSqlIdOrNull();
            objArr[2] = getPipedriveIdOrNull();
            objArr[3] = getName();
            objArr[4] = this.phone;
            objArr[5] = this.email;
            objArr[6] = getUpdateTime();
            objArr[7] = getAddTime();
            objArr[8] = this.address;
            objArr[9] = this.addressLatitude;
            objArr[10] = this.addressLongitude;
            objArr[11] = Boolean.valueOf(isActive());
            objArr[12] = Integer.valueOf(getOwnerPipedriveId());
            objArr[13] = Integer.valueOf(getVisibleTo());
            JSONObject json = getJSON(PipedriveApp.getActiveSession());
            if (json instanceof JSONObject) {
                jSONObjectInstrumentation = JSONObjectInstrumentation.toString(json);
            } else {
                jSONObjectInstrumentation = json.toString();
            }
            objArr[14] = jSONObjectInstrumentation;
            return String.format(str, objArr);
        } catch (JSONException e) {
            return super.toString();
        }
    }

    public boolean hasPhones() {
        return false;
    }

    public List<Email> getEmails() {
        List<Email> emails = new ArrayList();
        if (!TextUtils.isEmpty(this.email)) {
            emails.add(new Email(this.email, "Office", true));
        }
        return emails;
    }

    public List<Phone> getPhones() {
        List<Phone> phones = new ArrayList();
        if (!TextUtils.isEmpty(this.phone)) {
            phones.add(new Phone(this.phone, "Office", true));
        }
        return phones;
    }

    @Nullable
    public Double getAddressLatitude() {
        return this.addressLatitude;
    }

    public void setAddressLatitude(@Nullable Double addressLatitude) {
        this.addressLatitude = addressLatitude;
    }

    @Nullable
    public Double getAddressLongitude() {
        return this.addressLongitude;
    }

    public void setAddressLongitude(@Nullable Double addressLongitude) {
        this.addressLongitude = addressLongitude;
    }
}
