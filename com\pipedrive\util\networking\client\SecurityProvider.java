package com.pipedrive.util.networking.client;

import android.support.annotation.Nullable;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

public class SecurityProvider implements SecurityProviderInterface {
    public void initSSLContext() {
    }

    @Nullable
    public X509TrustManager getX509TrustManager() {
        return null;
    }

    @Nullable
    public SSLSocketFactory getSslSocketFactory() {
        return null;
    }

    @Nullable
    public HostnameVerifier getHostnameVerifier() {
        return null;
    }
}
