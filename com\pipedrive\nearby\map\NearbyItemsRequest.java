package com.pipedrive.nearby.map;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.model.NearbyItem;
import com.pipedrive.nearby.model.NearbyItemType;
import com.pipedrive.repository.NearbyRepository;
import rx.Observable;

class NearbyItemsRequest {
    @Nullable
    private LatLngBounds latLngBounds;
    @Nullable
    private LatLng location;
    @Nullable
    private NearbyItemType nearbyItemType;
    @Nullable
    private Session session;

    static class Builder {
        private final NearbyItemsRequest request = new NearbyItemsRequest();

        Builder() {
        }

        @NonNull
        Builder session(@NonNull Session session) {
            this.request.session = session;
            return this;
        }

        @NonNull
        Builder location(@NonNull LatLng location) {
            this.request.location = location;
            return this;
        }

        @NonNull
        Builder nearbyItemType(@NonNull NearbyItemType nearbyItemType) {
            this.request.nearbyItemType = nearbyItemType;
            return this;
        }

        @NonNull
        Builder viewport(@NonNull Viewport viewport) {
            this.request.latLngBounds = viewport.getLatLngBounds();
            return this;
        }

        @NonNull
        NearbyItemsRequest build() {
            return this.request;
        }
    }

    NearbyItemsRequest() {
    }

    @NonNull
    Observable<NearbyItem> requestInitialNearbyItems() {
        if (this.session == null || this.nearbyItemType == null || this.location == null || this.latLngBounds == null) {
            return Observable.empty();
        }
        return NearbyRepository.INSTANCE.getInitialNearbyItems(this.session, this.nearbyItemType, this.location, this.latLngBounds);
    }

    @NonNull
    Observable<NearbyItem> requestNearbyItems() {
        if (this.session == null || this.nearbyItemType == null || this.location == null || this.latLngBounds == null) {
            return Observable.empty();
        }
        return NearbyRepository.INSTANCE.getNearbyItems(this.session, this.nearbyItemType, this.location, this.latLngBounds);
    }
}
