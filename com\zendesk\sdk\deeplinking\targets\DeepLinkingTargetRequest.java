package com.zendesk.sdk.deeplinking.targets;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.TaskStackBuilder;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinking;
import com.zendesk.sdk.requests.RequestActivity;
import com.zendesk.sdk.requests.ViewRequestActivity;
import com.zendesk.util.StringUtils;

public class DeepLinkingTargetRequest extends DeepLinkingTarget<RequestConfiguration> {
    private static String EXTRA_REQUEST_ID = "extra_request_id";
    private static String EXTRA_REQUEST_SUBJECT = "extra_request_subject";

    void openTargetActivity(Context context, @NonNull RequestConfiguration configuration) {
        if (!ZendeskDeepLinking.INSTANCE.refreshComments(configuration.getRequestId())) {
            Intent intent = new Intent(context, ViewRequestActivity.class);
            intent.putExtra("requestId", configuration.getRequestId());
            if (StringUtils.hasLength(configuration.getSubject())) {
                intent.putExtra("subject", configuration.getSubject());
            }
            TaskStackBuilder taskStackBuilder = DeepLinkingTarget.getTaskStackBuilder(context, configuration.getBackStackActivities());
            taskStackBuilder.addNextIntentWithParentStack(new Intent(context, RequestActivity.class));
            taskStackBuilder.addNextIntentWithParentStack(intent);
            taskStackBuilder.startActivities();
        }
    }

    RequestConfiguration extractConfiguration(@NonNull Bundle bundle, TargetConfiguration targetConfiguration) {
        if (bundle.containsKey(EXTRA_REQUEST_ID)) {
            return new RequestConfiguration(bundle.getString(EXTRA_REQUEST_ID), bundle.getString(EXTRA_REQUEST_SUBJECT), targetConfiguration.getBackStackActivities(), targetConfiguration.getFallbackActivity());
        }
        return null;
    }

    @NonNull
    Bundle getExtra(RequestConfiguration configuration) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_REQUEST_ID, configuration.getRequestId());
        if (StringUtils.hasLength(configuration.getSubject())) {
            bundle.putString(EXTRA_REQUEST_SUBJECT, configuration.getSubject());
        }
        return bundle;
    }
}
