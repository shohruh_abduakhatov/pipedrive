package com.pipedrive.inlineintro;

public interface InlineIntroDisplayManager {
    void displayPopupIntros();

    void hidePopupIntrosOnce();
}
