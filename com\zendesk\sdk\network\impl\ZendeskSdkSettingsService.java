package com.zendesk.sdk.network.impl;

import com.zendesk.sdk.model.settings.MobileSettings;
import com.zendesk.sdk.network.SdkSettingsService;
import com.zendesk.service.RetrofitZendeskCallbackAdapter;
import com.zendesk.service.ZendeskCallback;

class ZendeskSdkSettingsService {
    private static final String LOG_TAG = "ZendeskSdkSettingsService";
    private final SdkSettingsService sdkSettingsService;

    ZendeskSdkSettingsService(SdkSettingsService sdkSettingsService) {
        this.sdkSettingsService = sdkSettingsService;
    }

    public void getSettings(String deviceLocale, String applicationId, ZendeskCallback<MobileSettings> callback) {
        this.sdkSettingsService.getSettings(deviceLocale, applicationId).enqueue(new RetrofitZendeskCallbackAdapter(callback));
    }
}
