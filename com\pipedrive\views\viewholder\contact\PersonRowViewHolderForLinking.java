package com.pipedrive.views.viewholder.contact;

import android.support.annotation.Nullable;
import com.pipedrive.R;

public class PersonRowViewHolderForLinking extends PersonRowViewHolder {
    public int getLayoutResourceId() {
        return R.layout.row_person_search;
    }

    protected void setupCallButton(@Nullable String contactPhone, @Nullable Long personSqlId) {
        this.mCallButton.setVisibility(8);
    }
}
