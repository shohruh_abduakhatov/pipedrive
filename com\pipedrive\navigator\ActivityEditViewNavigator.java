package com.pipedrive.navigator;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import com.pipedrive.R;

public class ActivityEditViewNavigator extends Navigator {
    @Nullable
    private OnBackPressedListener mOnBackPressedListener;

    public interface OnBackPressedListener {
        void onBackPressed(@NonNull ActivityEditViewNavigator activityEditViewNavigator);
    }

    public ActivityEditViewNavigator(@NonNull Navigable activityView, @Nullable Toolbar toolbar, @Nullable OnBackPressedListener onBackPressedListener) {
        super(activityView, toolbar);
        this.mOnBackPressedListener = onBackPressedListener;
    }

    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem menuItemDelete = menu.findItem(R.id.menu_delete);
        if (menuItemDelete != null) {
            menuItemDelete.setIcon(null);
            menuItemDelete.setShowAsAction(0);
        }
        MenuItem menuItemDiscard = menu.findItem(R.id.menu_discard);
        if (menuItemDiscard != null) {
            menuItemDiscard.setEnabled(true);
            menuItemDiscard.setShowAsAction(0);
        }
    }

    @MainThread
    public void onBackPressed() {
        if (this.mOnBackPressedListener != null) {
            this.mOnBackPressedListener.onBackPressed(this);
        } else {
            super.onBackPressed();
        }
    }
}
