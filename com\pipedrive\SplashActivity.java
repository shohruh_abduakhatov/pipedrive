package com.pipedrive;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import com.google.android.gms.common.GoogleApiAvailability;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.background.ApplicationStateMonitor;
import com.newrelic.agent.android.instrumentation.Instrumented;
import com.newrelic.agent.android.tracing.TraceMachine;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.settings.Company;
import com.pipedrive.notification.UINotificationManager;
import com.pipedrive.pipeline.PipelineActivity;
import com.pipedrive.sync.SyncManager;
import com.pipedrive.sync.recents.RecentsSyncAdapter;
import com.pipedrive.tasks.authorization.OnEnoughDataToShowUI;
import com.pipedrive.util.devices.MobileDevices;
import com.pipedrive.whatsnew.WhatsNewActivity;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;

@Instrumented
public class SplashActivity extends AppCompatActivity implements TraceFieldInterface {
    private static final String EXTRA_COMPANY_SWITCH = "companySwitch";
    private static final String EXTRA_PREVIOUS_COMPANY = "previousCompany";
    private static final String EXTRA_REMOVE_SESSION = "removeSession";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    static final String TAG = SplashActivity.class.getSimpleName();

    public enum CompanySwitch {
        CompanyNotFoundInSession("CompanyNotFoundInSession"),
        CompanyAlreadyChosen("CompanyAlreadyChosen"),
        CompanySwitchDataIsCorrupted("CompanySwitchDataIsCorrupted"),
        CompanySwitchSessionCreationFailed("CompanySwitchSessionCreationFailed"),
        Success("Success");
        
        private final String mType;

        private CompanySwitch(@NonNull String type) {
            this.mType = type;
        }

        public String toString() {
            return String.format("[%s]", new Object[]{this.mType});
        }
    }

    protected void onStart() {
        super.onStart();
        ApplicationStateMonitor.getInstance().activityStarted();
    }

    protected void onStop() {
        super.onStop();
        ApplicationStateMonitor.getInstance().activityStopped();
    }

    static void relaunchApplicationForLogout(@NonNull Context context, @Nullable Session sessionToRemove) {
        Bundle extras;
        if (sessionToRemove != null) {
            extras = new Bundle();
            extras.putString(EXTRA_REMOVE_SESSION, sessionToRemove.getSessionID());
        } else {
            extras = null;
        }
        relaunchApplication(context, extras);
    }

    public static CompanySwitch relaunchApplicationForCompanySwitch(@NonNull Context context, @NonNull Session currentSession, @NonNull Company switchToCompany) {
        Long companyPipedriveId = switchToCompany.getPipedriveIdOrNull();
        boolean cannotSwitchCompanyDueToMissingCompanyPipedriveId = companyPipedriveId == null || companyPipedriveId.longValue() <= 0;
        if (cannotSwitchCompanyDueToMissingCompanyPipedriveId) {
            LogJourno.reportEvent(EVENT.Authorization_companyDataCompanySwitchNoPipedriveId);
            return CompanySwitch.CompanyNotFoundInSession;
        }
        long currentlySelectedCompanyID = currentSession.getSelectedCompanyID();
        if (currentlySelectedCompanyID == companyPipedriveId.longValue()) {
            return CompanySwitch.CompanyAlreadyChosen;
        }
        Long companyUserId = switchToCompany.getUserPipedriveId();
        if (companyUserId == null) {
            LogJourno.reportEvent(EVENT.Authorization_companyDataCompanySwitchNoUserId);
            return CompanySwitch.CompanySwitchDataIsCorrupted;
        }
        String companyApiToken = switchToCompany.getApiToken();
        if (companyApiToken == null) {
            LogJourno.reportEvent(EVENT.Authorization_companyDataCompanySwitchNoApiToken);
            return CompanySwitch.CompanySwitchDataIsCorrupted;
        }
        String companyDomain = switchToCompany.getDomain();
        if (companyDomain == null) {
            return CompanySwitch.CompanySwitchDataIsCorrupted;
        }
        UINotificationManager.getInstance().cancelAllActivityAlarmsAndNotifications(currentSession);
        long currentlyAuthenticatedUserID = currentSession.getAuthenticatedUserID();
        PipedriveApp.getSessionManager().releaseActiveSession();
        Session switchedCompanySession = PipedriveApp.getSessionManager().createActiveSession(companyUserId.longValue(), companyPipedriveId.longValue(), companyApiToken, companyDomain);
        if (switchedCompanySession == null) {
            PipedriveApp.getSessionManager().createActiveSession(currentlyAuthenticatedUserID, currentlySelectedCompanyID);
            LogJourno.reportEvent(EVENT.Authorization_companyDataCompanySwitchSessionCreationFailed);
            return CompanySwitch.CompanySwitchSessionCreationFailed;
        }
        switchedCompanySession.enableLastProcessChangesAndRecents(true);
        Log.d(TAG, String.format("Company changed to company id: %s", new Object[]{Long.valueOf(switchedCompanySession.getSelectedCompanyID())}));
        Bundle bundle = new Bundle();
        bundle.putLong(EXTRA_COMPANY_SWITCH, selectedCompanyID);
        bundle.putLong(EXTRA_PREVIOUS_COMPANY, currentSession.getSelectedCompanyID());
        relaunchApplication(context, bundle);
        return CompanySwitch.Success;
    }

    private static void relaunchApplication(@NonNull Context context, @Nullable Bundle extras) {
        Intent intent = IntentCompat.makeRestartActivityTask(new Intent(context, SplashActivity.class).getComponent());
        if (extras != null) {
            intent.putExtras(extras);
        }
        context.startActivity(intent);
    }

    public void startActivityForResult(Intent intent, int requestCode) {
        if (requestCode == PLAY_SERVICES_RESOLUTION_REQUEST) {
            finish();
        } else {
            super.startActivityForResult(intent, requestCode);
        }
    }

    public void onCreate(Bundle bundle) {
        boolean companySwitchRequested = true;
        TraceMachine.startTracing("SplashActivity");
        try {
            TraceMachine.enterMethod(this._nr_trace, "SplashActivity#onCreate", null);
        } catch (NoSuchFieldError e) {
            while (true) {
                TraceMachine.enterMethod(null, "SplashActivity#onCreate", null);
            }
        }
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_splash);
        if (checkPlayServices()) {
            boolean removalOfDatabaseIsRequested;
            if (getIntent() == null || !getIntent().hasExtra(EXTRA_REMOVE_SESSION)) {
                removalOfDatabaseIsRequested = false;
            } else {
                removalOfDatabaseIsRequested = true;
            }
            if (removalOfDatabaseIsRequested) {
                PipedriveApp.getSessionManager().clearDatabaseAndDisposeSession(getIntent().getStringExtra(EXTRA_REMOVE_SESSION));
            }
            final Session session = PipedriveApp.getActiveSession();
            if (session != null) {
                if (session.getLastSyncTime(null) == null) {
                    session.setLastSyncTimeToNow();
                }
                if (getIntent() == null || !getIntent().hasExtra(EXTRA_COMPANY_SWITCH)) {
                    companySwitchRequested = false;
                }
                if (companySwitchRequested) {
                    new MobileDevices(session).switchCompany(getIntent().getLongExtra(EXTRA_PREVIOUS_COMPANY, 0)).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action0() {
                        public void call() {
                            Log.d(SplashActivity.TAG, "Company switch requested. Downloading all for that company.");
                            SyncManager.getInstance().requestCompanySwitchDataDownload(session, new OnEnoughDataToShowUI() {
                                public void readyToShowUI(@NonNull Session session) {
                                    SplashActivity splashActivity = SplashActivity.this;
                                    PipelineActivity.startActivity(splashActivity);
                                    splashActivity.finish();
                                }
                            });
                        }
                    }, new Action1<Throwable>() {
                        public void call(Throwable throwable) {
                            SplashActivity.this.onLoginDeviceFailed(session);
                        }
                    });
                } else {
                    new MobileDevices(session).loginDevice().subscribe(new Action0() {
                        public void call() {
                            Log.setupCrashLoggersMetadata(session);
                            RecentsSyncAdapter.requestSyncAdapterToSync(session);
                            WhatsNewActivity.startWhatsNewActivityIfRequiredOrMoveToPipelineActivity(session, SplashActivity.this);
                            SplashActivity.this.finish();
                        }
                    }, new Action1<Throwable>() {
                        public void call(Throwable throwable) {
                            SplashActivity.this.onLoginDeviceFailed(session);
                        }
                    });
                }
            } else {
                ContextCompat.startActivity(this, LoginActivity.startIntent(this), ActivityOptionsCompat.makeBasic().toBundle());
            }
            TraceMachine.exitMethod();
            return;
        }
        TraceMachine.exitMethod();
    }

    void onLoginDeviceFailed(@NonNull Session session) {
        PipedriveApp.getSessionManager().clearDatabaseAndDisposeSession(session);
        PipedriveApp.getSessionManager().releaseActiveSession();
        ContextCompat.startActivity(this, LoginActivity.deviceDisabledIntent(this), ActivityOptionsCompat.makeBasic().toBundle());
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode == 0) {
            return true;
        }
        if (apiAvailability.isUserResolvableError(resultCode)) {
            LogJourno.reportEvent(EVENT.Authorization_googlePlayServices_requestingToInstall);
            apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
        } else {
            LogJourno.reportEvent(EVENT.Authorization_googlePlayServices_deviceNotSupported);
            finish();
        }
        return false;
    }
}
