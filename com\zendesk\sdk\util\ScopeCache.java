package com.zendesk.sdk.util;

import com.zendesk.logger.Logger;
import java.util.Locale;

public class ScopeCache<T> {
    private static final String LOG_TAG = "ScopeCache";
    private T cache = null;
    private boolean provided = false;

    public synchronized T get(DependencyProvider<? extends T> provider) {
        if (!this.provided) {
            this.cache = provider.provideDependency();
            this.provided = true;
            Logger.d(String.format(Locale.US, "%s - %s", new Object[]{LOG_TAG, this.cache.getClass().getSimpleName()}), "Creating new object", new Object[0]);
        }
        return this.cache;
    }
}
