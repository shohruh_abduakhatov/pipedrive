package com.pipedrive.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.pipedrive.R;
import com.pipedrive.util.ImageUtil;
import java.util.ArrayList;
import java.util.List;

public class SelectedStagesIndicator extends RelativeLayout {
    private List<ImageView> cachedArrowImages;
    @BindDimen(2131493018)
    int defaultWidthPixelSize;
    private int imageSize;
    @BindView(2131821175)
    ImageView progress;
    private float progressWidth;
    @BindView(2131820716)
    RelativeLayout rootLayout;
    @ColorInt
    private int selectedStageColor;
    private int selectedStageNumber;
    @ColorInt
    private int separatingArrowColor;
    private int totalStageNumber;
    @ColorInt
    private int unSelectedStageColor;

    public SelectedStagesIndicator(Context context) {
        super(context);
        init();
    }

    public SelectedStagesIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        ButterKnife.bind(this, ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate(R.layout.view_selected_stage_indicator, this, true));
    }

    public void setup(int selectedStageNumber, int totalStageNumber, @ColorRes int separatingArrowColor, @ColorRes int unselectedStageColor, @ColorRes int selectedStageColor) {
        this.totalStageNumber = totalStageNumber;
        this.selectedStageNumber = selectedStageNumber;
        setColors(separatingArrowColor, unselectedStageColor, selectedStageColor);
        post(new Runnable() {
            public void run() {
                SelectedStagesIndicator.this.fillStages();
            }
        });
    }

    public void setColors(@ColorRes int separatingArrowColor, @ColorRes int unselectedStageColor, @ColorRes int selectedStageColor) {
        this.separatingArrowColor = ContextCompat.getColor(getContext(), separatingArrowColor);
        this.selectedStageColor = ContextCompat.getColor(getContext(), selectedStageColor);
        this.unSelectedStageColor = ContextCompat.getColor(getContext(), unselectedStageColor);
        Drawable progressDrawable = this.progress.getDrawable();
        this.rootLayout.setBackgroundColor(ContextCompat.getColor(getContext(), unselectedStageColor));
        this.progress.setImageDrawable(ImageUtil.getTintedDrawable(progressDrawable, this.selectedStageColor));
    }

    public void setSelectedStage(int selectedStage) {
        if (selectedStage >= this.totalStageNumber) {
            this.rootLayout.setBackgroundColor(this.selectedStageColor);
        } else {
            this.progress.getLayoutParams().width = (int) ((this.progressWidth * ((float) selectedStage)) + ((float) (this.imageSize / 2)));
        }
        this.rootLayout.requestLayout();
        this.selectedStageNumber = selectedStage;
    }

    public void setSelectedStageAndClearLayout(int selectedStage) {
        this.rootLayout.setBackgroundColor(this.unSelectedStageColor);
        setSelectedStage(selectedStage);
    }

    void fillStages() {
        clearCachedArrowImages();
        this.imageSize = getMeasuredHeight();
        this.progressWidth = ((float) getDefaultOrMeasuredWidth()) / ((float) this.totalStageNumber);
        int separatingArrowCount = this.totalStageNumber - 1;
        Drawable tintedArrowDrawable = ImageUtil.getTintedDrawable(ContextCompat.getDrawable(getContext(), R.drawable.deal_stage_separator), this.separatingArrowColor);
        this.cachedArrowImages = new ArrayList(separatingArrowCount);
        for (int i = separatingArrowCount; i > 0; i--) {
            ImageView arrow = new ImageView(getContext());
            arrow.setImageDrawable(tintedArrowDrawable);
            LayoutParams arrowParams = new LayoutParams(this.imageSize, this.imageSize);
            arrowParams.setMargins((int) (this.progressWidth * ((float) i)), 0, 0, 0);
            this.rootLayout.addView(arrow, arrowParams);
            this.cachedArrowImages.add(arrow);
        }
        setSelectedStage(this.selectedStageNumber);
    }

    int getDefaultOrMeasuredWidth() {
        return Math.max(getMeasuredWidth(), getContext().getResources().getDisplayMetrics().widthPixels - this.defaultWidthPixelSize);
    }

    private void clearCachedArrowImages() {
        if (this.cachedArrowImages != null && this.cachedArrowImages.size() > 0) {
            for (ImageView arrow : this.cachedArrowImages) {
                this.rootLayout.removeView(arrow);
            }
            this.cachedArrowImages.clear();
        }
    }
}
