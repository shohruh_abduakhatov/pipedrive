package com.zendesk.sdk.network;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.sdk.model.push.PushRegistrationResponse;
import com.zendesk.service.ZendeskCallback;
import java.util.Locale;

public interface PushRegistrationProvider {
    void registerDeviceWithIdentifier(@NonNull String str, @NonNull Locale locale, @Nullable ZendeskCallback<PushRegistrationResponse> zendeskCallback);

    void registerDeviceWithUAChannelId(@NonNull String str, @NonNull Locale locale, @Nullable ZendeskCallback<PushRegistrationResponse> zendeskCallback);

    void unregisterDevice(@NonNull String str, @Nullable ZendeskCallback<Void> zendeskCallback);
}
