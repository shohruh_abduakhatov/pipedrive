package com.pipedrive.nearby.filter.filteritem;

import android.support.annotation.UiThread;
import android.view.View;
import android.view.ViewStub;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class UserFilterItemViewHolder_ViewBinding extends FilterItemViewHolder_ViewBinding {
    private UserFilterItemViewHolder target;

    @UiThread
    public UserFilterItemViewHolder_ViewBinding(UserFilterItemViewHolder target, View source) {
        super(target, source);
        this.target = target;
        target.profilePictureViewStub = (ViewStub) Utils.findRequiredViewAsType(source, R.id.profilePictureViewStub, "field 'profilePictureViewStub'", ViewStub.class);
    }

    public void unbind() {
        UserFilterItemViewHolder target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.profilePictureViewStub = null;
        super.unbind();
    }
}
