package com.zendesk.sdk.model.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.util.CollectionUtils;
import java.util.Date;
import java.util.List;

public class Request {
    private List<Long> collaboratorIds;
    private EndUserComment comment;
    private Integer commentCount;
    private Date createdAt;
    private String description;
    private Date dueAt;
    private String id;
    private Long organizationId;
    private String priority;
    private Date publicUpdatedAt;
    private Long requesterId;
    private String status;
    private String subject;
    private String type;
    private Date updatedAt;
    private String url;

    @Nullable
    public String getId() {
        return this.id;
    }

    @Nullable
    public String getUrl() {
        return this.url;
    }

    @Nullable
    public String getSubject() {
        return this.subject;
    }

    @Nullable
    public String getDescription() {
        return this.description;
    }

    @Nullable
    public String getStatus() {
        return this.status;
    }

    @Nullable
    public String getPriority() {
        return this.priority;
    }

    @Nullable
    public String getType() {
        return this.type;
    }

    @Nullable
    public Long getOrganizationId() {
        return this.organizationId;
    }

    @Nullable
    public Long getRequesterId() {
        return this.requesterId;
    }

    @NonNull
    public List<Long> getCollaboratorIds() {
        return CollectionUtils.copyOf(this.collaboratorIds);
    }

    @Nullable
    public Date getDueAt() {
        return this.dueAt == null ? null : new Date(this.dueAt.getTime());
    }

    @Nullable
    public Date getCreatedAt() {
        return this.createdAt == null ? null : new Date(this.createdAt.getTime());
    }

    @Nullable
    public Date getUpdatedAt() {
        return this.updatedAt == null ? null : new Date(this.updatedAt.getTime());
    }

    @Nullable
    public Date getPublicUpdatedAt() {
        return this.publicUpdatedAt == null ? null : new Date(this.publicUpdatedAt.getTime());
    }

    public void setComment(EndUserComment comment) {
        this.comment = comment;
    }

    @Nullable
    public Integer getCommentCount() {
        return this.commentCount;
    }
}
