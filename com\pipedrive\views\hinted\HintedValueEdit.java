package com.pipedrive.views.hinted;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;
import com.pipedrive.R;

public class HintedValueEdit extends HintedEditText<EditText> {
    public HintedValueEdit(Context context) {
        this(context, null);
    }

    public HintedValueEdit(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HintedValueEdit(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected int getMainViewLayoutResource() {
        return R.layout.view_hinted_value;
    }
}
