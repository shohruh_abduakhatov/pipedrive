package com.pipedrive.datasource;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.logging.Log;

public class PipeSQLiteSharedHelper extends PipeSQLiteOpenHelper {
    public static final String DATABASE_FILE_NAME = "pdshareddb";
    private static final int DATABASE_VERSION = 1;
    final String TAG = PipeSQLiteSharedHelper.class.getName();
    private Table[] mActiveTables = new Table[]{new TableCompany()};

    private interface Table {
        public static final String COMMA_SEP = ",";

        @NonNull
        String createTableSql();

        @NonNull
        String deleteTableSql();
    }

    public static class TableCompany implements Table, BaseColumns {
        public static final String COLUMN_COMPANY_PIPEDRIVE_ID = "c_pid";
        public static final String COLUMN_NAME_API_TOKEN = "c_api_token";
        public static final String COLUMN_NAME_DOMAIN_NAME = "c_domain";
        public static final String COLUMN_NAME_NAME = "c_name";
        public static final String COLUMN_NAME_USER_PIPEDRIVE_ID = "c_user_pid";
        public static final String TABLE_NAME = "company";

        @NonNull
        public String createTableSql() {
            return "create table company ( _id integer primary key autoincrement ,c_pid integer ,c_name text ,c_api_token text ,c_user_pid integer ,c_domain text  )";
        }

        @NonNull
        public String deleteTableSql() {
            return "DROP TABLE IF EXISTS company";
        }
    }

    public PipeSQLiteSharedHelper(@NonNull Context context) {
        super(context, DATABASE_FILE_NAME, 1);
        Log.d(this.TAG, "SQL connection initialized to database: pdshareddb");
    }

    public void onCreate(@NonNull SQLiteDatabase database) {
        createTables(database);
    }

    public void onUpgrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(this.TAG, "Upgrading database from version " + oldVersion + " to " + newVersion + ".");
    }

    public void clearDatabase(@NonNull SQLiteDatabase db) {
        for (Table activeTable : this.mActiveTables) {
            String tableDeleteScript = activeTable.deleteTableSql();
            Log.i(this.TAG, "Delete table: " + tableDeleteScript);
            if (db instanceof SQLiteDatabase) {
                SQLiteInstrumentation.execSQL(db, tableDeleteScript);
            } else {
                db.execSQL(tableDeleteScript);
            }
        }
        createTables(db);
    }

    private void createTables(@NonNull SQLiteDatabase db) {
        for (Table activeTable : this.mActiveTables) {
            String tableCreateScript = activeTable.createTableSql();
            Log.i(this.TAG, "Create table: " + tableCreateScript);
            if (db instanceof SQLiteDatabase) {
                SQLiteInstrumentation.execSQL(db, tableCreateScript);
            } else {
                db.execSQL(tableCreateScript);
            }
        }
        Log.i(this.TAG, "Recreation done.");
    }
}
