package com.pipedrive.util.snackbar;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import rx.Observable;
import rx.subjects.Subject;

public enum SnackBarManager {
    INSTANCE;
    
    public static final int SNACKBAR_DEFAULT_DURATION = -1;
    @Nullable
    private SnackBarData delayedSnackBar;
    @NonNull
    private final Subject<SnackBarData, SnackBarData> snackBarBus;

    public void showSnackBar(@StringRes int messageResId) {
        if (this.snackBarBus.hasObservers()) {
            this.snackBarBus.onNext(new SnackBarData(messageResId, -1));
        }
    }

    @NonNull
    public Observable<SnackBarData> getSnackBarBus() {
        return this.snackBarBus;
    }

    public void setDelayedSnackBar(@StringRes int messageResId) {
        this.delayedSnackBar = new SnackBarData(messageResId, -1);
    }

    public void clearDelayedSnackBar() {
        this.delayedSnackBar = null;
    }

    @Nullable
    public SnackBarData getDelayedSnackBar() {
        return this.delayedSnackBar;
    }
}
