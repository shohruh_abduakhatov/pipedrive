package com.pipedrive.util.flowfiles;

import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.FlowFilesDataSource;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.datasource.SQLTransactionManager;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.model.Deal;
import com.pipedrive.model.FlowFile;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.util.networking.entities.FlowFileEntity;
import com.pipedrive.util.organizations.OrganizationsNetworkingUtil;
import com.pipedrive.util.persons.PersonNetworkingUtil;
import java.util.List;

public enum FlowFileNetworkingUtil {
    ;

    public static void createOrUpdateFlowFilesIntoDBWithRelations(Session session, List<FlowFileEntity> list) {
        if (list != null && !list.isEmpty()) {
            SQLTransactionManager transactionManager = new SQLTransactionManager(session.getDatabase());
            transactionManager.beginTransactionNonExclusive();
            for (FlowFileEntity flowFileEntity : list) {
                createOrUpdateFlowFileIntoDBWithRelations(session, flowFileEntity);
            }
            transactionManager.commit();
        }
    }

    public static FlowFile createOrUpdateFlowFileIntoDBWithRelations(Session session, FlowFileEntity flowFileEntity) {
        return createOrUpdateFlowFileIntoDBWithRelations(session, flowFileEntity, null);
    }

    public static FlowFile createOrUpdateFlowFileIntoDBWithRelations(Session session, FlowFileEntity flowFileEntity, @Nullable Long flowFileSqlId) {
        if (flowFileEntity == null) {
            String logMessage = FlowFileNetworkingUtil.class.getSimpleName() + ":createOrUpdateFlowFileIntoDBWithRelations:Null FlowFileEntity passed!";
            LogJourno.reportEvent(logMessage);
            Log.e(new Throwable(logMessage));
            return null;
        }
        Person filePerson = null;
        if (flowFileEntity.getPersonId() != null && flowFileEntity.getPersonId().intValue() > 0) {
            filePerson = new PersonsDataSource(session.getDatabase()).findPersonByPipedriveId((long) flowFileEntity.getPersonId().intValue());
            if (filePerson == null) {
                filePerson = PersonNetworkingUtil.downloadPerson(session, flowFileEntity.getPersonId().intValue());
                if (filePerson != null) {
                    filePerson = PersonNetworkingUtil.createOrUpdatePersonIntoDBWithRelations(session, filePerson);
                }
            }
        }
        Organization fileOrganization = null;
        if (flowFileEntity.getOrgId() != null && flowFileEntity.getOrgId().intValue() > 0) {
            fileOrganization = new OrganizationsDataSource(session.getDatabase()).findOrganizationByPipedriveId(flowFileEntity.getOrgId().intValue());
            if (fileOrganization == null) {
                fileOrganization = OrganizationsNetworkingUtil.downloadOrganization(session, flowFileEntity.getOrgId().intValue());
                if (fileOrganization != null) {
                    fileOrganization = OrganizationsNetworkingUtil.createOrUpdateOrganizationIntoDBWithRelations(session, fileOrganization);
                }
            }
        }
        Deal fileDeal = null;
        if (flowFileEntity.getDealId() != null && flowFileEntity.getDealId().intValue() > 0) {
            fileDeal = new DealsDataSource(session.getDatabase()).findDealByDealId((long) flowFileEntity.getDealId().intValue());
            if (fileDeal == null) {
                fileDeal = DealsNetworkingUtil.downloadDeal(session, (long) flowFileEntity.getDealId().intValue());
                if (fileDeal != null) {
                    fileDeal = DealsNetworkingUtil.createOrUpdateDealIntoDBWithRelations(session, fileDeal);
                }
            }
        }
        FlowFile flowFile = FlowFile.from(flowFileEntity, fileDeal, filePerson, fileOrganization);
        if (flowFileSqlId != null) {
            flowFile.setSqlId(flowFileSqlId.longValue());
        }
        long fileSqlId = new FlowFilesDataSource(session.getDatabase()).createOrUpdate(flowFile);
        if (fileSqlId == -1) {
            return null;
        }
        flowFile.setSqlId(fileSqlId);
        return flowFile;
    }
}
