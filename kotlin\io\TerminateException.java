package kotlin.io;

import com.pipedrive.util.networking.entities.FlowItem;
import java.io.File;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004¨\u0006\u0005"}, d2 = {"Lkotlin/io/TerminateException;", "Lkotlin/io/FileSystemException;", "file", "Ljava/io/File;", "(Ljava/io/File;)V", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Utils.kt */
final class TerminateException extends FileSystemException {
    public TerminateException(@NotNull File file) {
        Intrinsics.checkParameterIsNotNull(file, FlowItem.ITEM_TYPE_OBJECT_FILE);
        super(file, null, null, 6, null);
    }
}
