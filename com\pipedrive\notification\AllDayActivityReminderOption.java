package com.pipedrive.notification;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import com.pipedrive.R;
import com.pipedrive.datetime.PipedriveDate;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.model.Activity;
import com.pipedrive.util.time.TimeManager;
import java.util.Calendar;

public enum AllDayActivityReminderOption implements ReminderOption {
    DONT_REMIND(null, R.string.activity.reminder.none),
    REMIND_ON_DAY_OF_EVENT_AT_09_00(Integer.valueOf(0), R.string.activity_reminder_on_day_of_event_at_09_00),
    REMIND_1_DAY_BEFORE_AT_09_00(Integer.valueOf(1), R.string.activity_reminder_1_day_before_at_09_00),
    REMIND_2_DAYS_BEFORE_AT_09_00(Integer.valueOf(2), R.string.activity_reminder_2_days_before_at_09_00);
    
    public static final AllDayActivityReminderOption DEFAULT = null;
    public static final int HOUR_OF_DAY_TO_REMIND = 9;
    @Nullable
    private final Integer deltaDays;
    @StringRes
    private final int titleResId;

    static {
        DEFAULT = REMIND_ON_DAY_OF_EVENT_AT_09_00;
    }

    private AllDayActivityReminderOption(@Nullable Integer deltaDays, @StringRes int titleResId) {
        this.deltaDays = deltaDays;
        this.titleResId = titleResId;
    }

    @NonNull
    public String getTitle(@NonNull Context context) {
        return context.getString(this.titleResId);
    }

    @Nullable
    public PipedriveDateTime getRemindDateTime(@Nullable Activity activity) {
        if (this.deltaDays == null || activity == null || activity.getActivityStartAsDate() == null) {
            return null;
        }
        PipedriveDate date = activity.getActivityStartAsDate();
        Calendar remindCalendar = TimeManager.getInstance().getLocalCalendar();
        remindCalendar.clear();
        remindCalendar.set(date.getYear(), date.getMonth(), date.getDayOfMonth());
        remindCalendar.add(6, -this.deltaDays.intValue());
        remindCalendar.set(11, 9);
        return PipedriveDateTime.instanceFromDate(remindCalendar.getTime());
    }
}
