package com.pipedrive.note;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.HtmlContent;
import com.pipedrive.model.delta.ObjectDelta;
import com.pipedrive.presenter.PersistablePresenter;

abstract class HtmlEditorPresenter<NOTE extends BaseDatasourceEntity & HtmlContent, VIEW extends HtmlEditorView> extends PersistablePresenter<VIEW> {
    private static final String STORAGE_KEY_HASH_CODE = (HtmlEditorPresenter.class.getSimpleName() + ".hashCode");
    private static final String STORAGE_KEY_NOTE_CONTENT = (HtmlEditorPresenter.class.getSimpleName() + ".note");
    @Nullable
    private NOTE mNote;
    @Nullable
    private String mNoteFromStorage;
    @Nullable
    private Integer mOriginalNoteHashCode;

    HtmlEditorPresenter(@NonNull Session session, @Nullable Bundle savedState) {
        super(session, savedState);
    }

    protected NOTE getData() {
        return this.mNote;
    }

    void setHtmlContentForStorage(@Nullable String content) {
        ((HtmlContent) getData()).setHtmlContent(content);
    }

    void cacheDataAndUpdateTheView(@Nullable NOTE note) {
        boolean noteShouldBeRestoredFromStore = (note == null || this.mNoteFromStorage == null) ? false : true;
        if (noteShouldBeRestoredFromStore) {
            ((HtmlContent) note).setHtmlContent(this.mNoteFromStorage);
            this.mNoteFromStorage = null;
        }
        this.mNote = note;
        if (getView() != null) {
            ((HtmlEditorView) getView()).onDataRequested((HtmlContent) this.mNote);
        }
    }

    boolean isContentExist() {
        return (getData() == null || ((HtmlContent) getData()).getHtmlContent() == null || ((HtmlContent) getData()).getHtmlContent().isEmpty()) ? false : true;
    }

    protected void saveToBundle(@NonNull Bundle bundle) {
        bundle.putString(STORAGE_KEY_NOTE_CONTENT, ((HtmlContent) getData()).getHtmlContent());
        bundle.putSerializable(STORAGE_KEY_HASH_CODE, this.mOriginalNoteHashCode);
    }

    protected void restoreFromBundle(@NonNull Bundle bundle) {
        this.mNoteFromStorage = bundle.getString(STORAGE_KEY_NOTE_CONTENT);
        this.mOriginalNoteHashCode = (Integer) bundle.getSerializable(STORAGE_KEY_HASH_CODE);
    }

    protected String[] getKeysNeededForRestore() {
        return new String[]{STORAGE_KEY_NOTE_CONTENT};
    }

    void setOriginalNoteHashCode(@NonNull Integer originalNoteHashCode) {
        if (this.mOriginalNoteHashCode == null) {
            this.mOriginalNoteHashCode = originalNoteHashCode;
        }
    }

    @NonNull
    Boolean hashCodeEquals(@NonNull Integer hashCode) {
        boolean z = this.mOriginalNoteHashCode != null && ObjectDelta.areEqual(this.mOriginalNoteHashCode, hashCode);
        return Boolean.valueOf(z);
    }
}
