package com.pipedrive.nearby.cards.cards;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datetime.LocaleHelper;
import com.pipedrive.datetime.PipedriveDate;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.model.Activity;
import com.pipedrive.model.Activity.DateTimeInfoWithReturn;
import com.pipedrive.model.ActivityType;

public class ActivitySummary extends LinearLayout {
    @BindView(2131820830)
    TextView mActivityDate;
    @BindView(2131820650)
    TextView mActivityTitle;
    @BindView(2131820829)
    ImageView mActivityTypeImage;

    public ActivitySummary(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ActivitySummary(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        ButterKnife.bind((Object) this, ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(R.layout.activity_summary, this, true));
    }

    void setup(@NonNull Session session, @Nullable Activity activity, @StringRes int nextOrLastLabel) {
        if (activity == null) {
            setVisibility(8);
            return;
        }
        setVisibility(0);
        ActivityType type = activity.getType();
        if (type != null) {
            this.mActivityTypeImage.setImageResource(type.getImageResourceId());
        }
        this.mActivityTitle.setText(activity.getSubject());
        this.mActivityDate.setText(nextOrLastLabel);
        this.mActivityDate.append(getContext().getResources().getString(R.string.subtitle_separator));
        CharSequence formattedActivityDateAndTimeInfo = getFormattedTimeAsString(session, activity);
        this.mActivityDate.append(formattedActivityDateAndTimeInfo);
        if (nextOrLastLabel == R.string.next) {
            paintTimeStringRed(formattedActivityDateAndTimeInfo, activity.isOverdue());
        }
    }

    private void paintTimeStringRed(CharSequence formattedActivityDateAndTimeInfo, @Nullable Boolean overdue) {
        if (overdue != null && overdue.booleanValue()) {
            Spannable string = (Spannable) this.mActivityDate.getText();
            int start = string.length() - formattedActivityDateAndTimeInfo.length();
            string.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.red)), start, start + formattedActivityDateAndTimeInfo.length(), 33);
        }
    }

    private String getFormattedTimeAsString(@NonNull final Session session, @NonNull Activity activity) {
        return (String) activity.request(new DateTimeInfoWithReturn<String>() {
            public String activityHasStartDateTime(@NonNull PipedriveDateTime dateTime, @NonNull Activity inActivity) {
                return LocaleHelper.getLocaleBasedDateTimeStringInCurrentTimeZoneLongDate(session, dateTime);
            }

            public String activityHasStartDate(@NonNull PipedriveDate date, @NonNull Activity inActivity) {
                return LocaleHelper.getLocaleBasedDateString(session, date);
            }

            public String activityIsCorruptedToReturnDateTimeInfo(@NonNull Activity corruptedActivity) {
                return null;
            }
        });
    }
}
