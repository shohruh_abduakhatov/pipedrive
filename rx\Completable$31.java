package rx;

import rx.Scheduler.Worker;
import rx.functions.Action0;

class Completable$31 implements Completable$OnSubscribe {
    final /* synthetic */ Completable this$0;
    final /* synthetic */ Scheduler val$scheduler;

    Completable$31(Completable completable, Scheduler scheduler) {
        this.this$0 = completable;
        this.val$scheduler = scheduler;
    }

    public void call(final CompletableSubscriber s) {
        final Worker w = this.val$scheduler.createWorker();
        w.schedule(new Action0() {
            public void call() {
                try {
                    Completable$31.this.this$0.unsafeSubscribe(s);
                } finally {
                    w.unsubscribe();
                }
            }
        });
    }
}
