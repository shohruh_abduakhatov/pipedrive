package com.pipedrive.dialogs.login;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import com.pipedrive.R;

public class DeviceDisabledDialog extends LoginFailedDialogFragment {
    private static final String TAG = "DeviceDisabledDialog";

    public static void show(@NonNull FragmentManager supportFragmentManager) {
        LoginFailedDialogFragment.showDialog(supportFragmentManager, TAG, new DeviceDisabledDialog(), Integer.valueOf(R.string.error_dialog_device_disabled), Integer.valueOf(R.string.error_dialog_title), Integer.valueOf(R.string.error_dialog_button_negative_ok), null);
    }
}
