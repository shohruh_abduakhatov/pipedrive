package com.zendesk.sdk.network;

import android.support.annotation.Nullable;
import com.zendesk.sdk.model.settings.SafeMobileSettings;
import com.zendesk.service.ZendeskCallback;

public interface SdkSettingsProvider {
    void getSettings(@Nullable ZendeskCallback<SafeMobileSettings> zendeskCallback);
}
