package com.pipedrive.views.viewholder.deal;

import android.content.Context;
import android.support.annotation.NonNull;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.model.Deal;
import com.pipedrive.model.DealStatus;
import com.pipedrive.util.StringUtils;

public class DealRowViewHolderForSearch extends DealRowViewHolder {
    public /* bridge */ /* synthetic */ void fill(@NonNull Session session, @NonNull Context context, @NonNull Deal deal) {
        super.fill(session, context, deal);
    }

    public void fill(@NonNull Session session, @NonNull Context context, @NonNull Deal deal, @NonNull SearchConstraint searchConstraint) {
        super.fill(session, context, deal);
        StringUtils.highlightString(this.mTitle, searchConstraint);
        StringUtils.highlightString(this.mSubTitle, searchConstraint);
        tintIcon(deal);
    }

    private void tintIcon(@NonNull Deal deal) {
        boolean backgroundIsDark = deal.getStatus() == DealStatus.WON || deal.isDealRotten();
        int tintColorResId = backgroundIsDark ? R.color.white : R.color.icon_tint;
        int backgroundTintColorResId = backgroundIsDark ? R.color.whiteMuted : R.color.separatorOnTopOfView;
        if (this.mIcon != null) {
            this.mIcon.setTintColor(tintColorResId);
            this.mIcon.setBackgroundTintColor(backgroundTintColorResId);
        }
    }

    public int getLayoutResourceId() {
        return R.layout.row_deal_search;
    }
}
