package com.zendesk.belvedere;

import java.util.TreeSet;

public class BelvedereConfig {
    private boolean allowMultiple;
    private BelvedereLogger belvedereLogger;
    private int cameraRequestCodeEnd;
    private int cameraRequestCodeStart;
    private String contentType;
    private String directoryName;
    private int galleryRequestCode;
    private TreeSet<BelvedereSource> sources;

    BelvedereConfig(Builder builder) {
        this.directoryName = Builder.access$000(builder);
        this.galleryRequestCode = Builder.access$100(builder);
        this.cameraRequestCodeStart = Builder.access$200(builder);
        this.cameraRequestCodeEnd = Builder.access$300(builder);
        this.allowMultiple = Builder.access$400(builder);
        this.contentType = Builder.access$500(builder);
        this.belvedereLogger = Builder.access$600(builder);
        this.sources = Builder.access$700(builder);
    }

    String getDirectoryName() {
        return this.directoryName;
    }

    int getGalleryRequestCode() {
        return this.galleryRequestCode;
    }

    int getCameraRequestCodeStart() {
        return this.cameraRequestCodeStart;
    }

    int getCameraRequestCodeEnd() {
        return this.cameraRequestCodeEnd;
    }

    boolean allowMultiple() {
        return this.allowMultiple;
    }

    String getContentType() {
        return this.contentType;
    }

    BelvedereLogger getBelvedereLogger() {
        return this.belvedereLogger;
    }

    TreeSet<BelvedereSource> getBelvedereSources() {
        return this.sources;
    }
}
