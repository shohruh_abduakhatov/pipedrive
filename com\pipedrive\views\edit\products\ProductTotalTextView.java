package com.pipedrive.views.edit.products;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;
import com.pipedrive.application.Session;
import com.pipedrive.model.Currency;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.util.formatter.MonetaryFormatter;
import java.math.BigDecimal;

public class ProductTotalTextView extends TextView {
    @Nullable
    private Currency mCurrency;
    private double mDiscountValue;
    private double mDuration;
    @Nullable
    private BigDecimal mPriceValue;
    private double mQuantityValue;
    @Nullable
    private Session mSession;

    public ProductTotalTextView(Context context) {
        this(context, null);
    }

    public ProductTotalTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProductTotalTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setup(@NonNull DealProduct dealProduct, @NonNull Session session) {
        this.mPriceValue = dealProduct.getPrice().getValue();
        this.mQuantityValue = dealProduct.getQuantity().doubleValue();
        this.mDiscountValue = dealProduct.getDiscountPercentage().doubleValue();
        this.mDuration = dealProduct.getDuration().doubleValue();
        this.mSession = session;
        this.mCurrency = dealProduct.getPrice().getCurrency();
        setText(getFormattedTotalValue());
    }

    @Nullable
    private String getFormattedTotalValue() {
        boolean canShowFormattedValue = (this.mPriceValue == null || this.mCurrency == null || this.mSession == null) ? false : true;
        if (!canShowFormattedValue) {
            return null;
        }
        return new MonetaryFormatter(this.mSession).format(Double.valueOf(DealProduct.getCalculatedDealProductSum(this.mPriceValue, Double.valueOf(this.mQuantityValue), Double.valueOf(this.mDuration), Double.valueOf(this.mDiscountValue)).doubleValue()), this.mCurrency.getCode());
    }

    public void updatePrice(@NonNull BigDecimal price) {
        this.mPriceValue = price;
        setText(getFormattedTotalValue());
    }

    public void updateQuantity(double quantity) {
        this.mQuantityValue = quantity;
        setText(getFormattedTotalValue());
    }

    public void updateDiscount(double discount) {
        this.mDiscountValue = discount;
        setText(getFormattedTotalValue());
    }

    public void updateDuration(double duration) {
        this.mDuration = duration;
        setText(getFormattedTotalValue());
    }
}
