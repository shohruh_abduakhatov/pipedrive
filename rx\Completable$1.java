package rx;

import rx.subscriptions.Subscriptions;

class Completable$1 implements Completable$OnSubscribe {
    Completable$1() {
    }

    public void call(CompletableSubscriber s) {
        s.onSubscribe(Subscriptions.unsubscribed());
        s.onCompleted();
    }
}
