package rx;

import java.util.Arrays;
import rx.exceptions.CompositeException;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.plugins.RxJavaHooks;
import rx.subscriptions.Subscriptions;

class Completable$19 implements Completable$OnSubscribe {
    final /* synthetic */ Completable this$0;
    final /* synthetic */ Action0 val$onAfterTerminate;
    final /* synthetic */ Action0 val$onComplete;
    final /* synthetic */ Action1 val$onError;
    final /* synthetic */ Action1 val$onSubscribe;
    final /* synthetic */ Action0 val$onUnsubscribe;

    Completable$19(Completable completable, Action0 action0, Action0 action02, Action1 action1, Action1 action12, Action0 action03) {
        this.this$0 = completable;
        this.val$onComplete = action0;
        this.val$onAfterTerminate = action02;
        this.val$onError = action1;
        this.val$onSubscribe = action12;
        this.val$onUnsubscribe = action03;
    }

    public void call(final CompletableSubscriber s) {
        this.this$0.unsafeSubscribe(new CompletableSubscriber() {
            public void onCompleted() {
                try {
                    Completable$19.this.val$onComplete.call();
                    s.onCompleted();
                    try {
                        Completable$19.this.val$onAfterTerminate.call();
                    } catch (Throwable e) {
                        RxJavaHooks.onError(e);
                    }
                } catch (Throwable e2) {
                    s.onError(e2);
                }
            }

            public void onError(Throwable e) {
                try {
                    Completable$19.this.val$onError.call(e);
                } catch (Throwable ex) {
                    e = new CompositeException(Arrays.asList(new Throwable[]{e, ex}));
                }
                s.onError(e);
                try {
                    Completable$19.this.val$onAfterTerminate.call();
                } catch (Throwable ex2) {
                    RxJavaHooks.onError(ex2);
                }
            }

            public void onSubscribe(final Subscription d) {
                try {
                    Completable$19.this.val$onSubscribe.call(d);
                    s.onSubscribe(Subscriptions.create(new Action0() {
                        public void call() {
                            try {
                                Completable$19.this.val$onUnsubscribe.call();
                            } catch (Throwable e) {
                                RxJavaHooks.onError(e);
                            }
                            d.unsubscribe();
                        }
                    }));
                } catch (Throwable ex) {
                    d.unsubscribe();
                    s.onSubscribe(Subscriptions.unsubscribed());
                    s.onError(ex);
                }
            }
        });
    }
}
