package com.pipedrive.util.deals;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.CurrenciesDataSource;
import com.pipedrive.datasource.DealsDataSource;
import com.pipedrive.datasource.OrganizationsDataSource;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.datasource.PipelineDataSource;
import com.pipedrive.datasource.SQLTransactionManager;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.PageOfDeals;
import com.pipedrive.model.Person;
import com.pipedrive.model.pagination.Pagination;
import com.pipedrive.util.CurrenciesNetworkingUtil;
import com.pipedrive.util.StagesNetworkingUtil;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.ConnectionUtil$JsonReaderInterceptor;
import com.pipedrive.util.networking.Response;
import com.pipedrive.util.organizations.OrganizationsNetworkingUtil;
import com.pipedrive.util.persons.PersonNetworkingUtil;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

class DealsNetworkingUtil {
    static final String ACTION_DEAL_NEXT_ACTIVITY_RECALCULATED = "com.pipedrive.util.deals.DealsNetworkingUtil.ACTION_DEAL_NEXT_ACTIVITY_RECALCULATED";
    private static final String TAG = DealsNetworkingUtil.class.getSimpleName();

    DealsNetworkingUtil() {
    }

    @Nullable
    public static Deal downloadDeal(@NonNull Session session, long dealPipedriveId) {
        boolean cannotDownloadSpecificDeal = dealPipedriveId < 0 || ConnectionUtil.isNotConnected(session.getApplicationContext());
        if (cannotDownloadSpecificDeal) {
            return null;
        }
        ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder(session, "deals");
        apiUrlBuilder.appendEncodedPath(Long.toString(dealPipedriveId));
        return ((AnonymousClass1DealDetailsRequest) ConnectionUtil.requestGet(apiUrlBuilder, new ConnectionUtil$JsonReaderInterceptor<AnonymousClass1DealDetailsRequest>() {
            public AnonymousClass1DealDetailsRequest interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull AnonymousClass1DealDetailsRequest responseTemplate) throws IOException {
                if (jsonReader.peek() == JsonToken.NULL) {
                    jsonReader.skipValue();
                } else {
                    responseTemplate.deal = Deal.readDeal(jsonReader);
                }
                return responseTemplate;
            }
        }, new Response() {
            Deal deal = null;
        })).deal;
    }

    public static boolean downloadContactOrganizationDeals(long orgId, long contactId, Session session) {
        new Response().isRequestSuccessful = true;
        PageOfDeals pageOfDeals = new PageOfDeals();
        pageOfDeals.start = 0;
        pageOfDeals.moreItemsInCollection = false;
        do {
            pageOfDeals = downloadPageOfDeals(session, orgId, contactId, pageOfDeals.start);
            pageOfDeals.start = pageOfDeals.nextStart;
            pageOfDeals.nextStart = Integer.MIN_VALUE;
        } while (pageOfDeals.moreItemsInCollection);
        return pageOfDeals.isSuccessful;
    }

    private static PageOfDeals downloadPageOfDeals(Session session, long orgId, long contactId, int start) {
        InputStream in;
        PageOfDeals pageOfDeals = new PageOfDeals();
        pageOfDeals.start = start;
        pageOfDeals.nextStart = 0;
        pageOfDeals.moreItemsInCollection = false;
        if (orgId > 0) {
            in = getOrganizationDealsListStream(session, orgId, start);
        } else {
            in = getContactDealsListStream(session, contactId, start);
        }
        if (parseDealsFromInputStream(session, in, pageOfDeals) == null) {
            pageOfDeals.isSuccessful = false;
        } else {
            pageOfDeals.isSuccessful = true;
        }
        return pageOfDeals;
    }

    private static InputStream getOrganizationDealsListStream(Session session, long orgId, int start) {
        ApiUrlBuilder uriBuilder = new ApiUrlBuilder(session, "organizations");
        uriBuilder.appendEncodedPath(Long.toString(orgId));
        uriBuilder.appendEncodedPath("deals");
        uriBuilder.pagination(Long.valueOf(Integer.valueOf(start).longValue()), Long.valueOf(Integer.valueOf(0).longValue()));
        return ConnectionUtil.requestGet(uriBuilder);
    }

    private static InputStream getContactDealsListStream(Session session, long contactId, int start) {
        ApiUrlBuilder uriBuilder = new ApiUrlBuilder(session, "persons");
        uriBuilder.appendEncodedPath(Long.toString(contactId));
        uriBuilder.appendEncodedPath("deals");
        uriBuilder.pagination(Long.valueOf(Integer.valueOf(start).longValue()), Long.valueOf(Integer.valueOf(0).longValue()));
        return ConnectionUtil.requestGet(uriBuilder);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    @Nullable
    private static List<Deal> parseDealsFromInputStream(Session session, InputStream in, PageOfDeals pageOfDeals) {
        List<Deal> deals = new ArrayList();
        if (in == null || session == null) {
            Log.e(new Throwable(String.format("Deals list stream returned null! Cannot download deals! start: %s.", new Object[]{Integer.valueOf(pageOfDeals.start)})));
            pageOfDeals.moreItemsInCollection = false;
            return null;
        }
        JsonReader reader = new JsonReader(new InputStreamReader(in, HttpRequest.CHARSET_UTF8));
        reader.beginObject();
        while (reader.hasNext() && reader.peek() != JsonToken.NULL) {
            String contactsListField = reader.nextName();
            if (!Response.JSON_PARAM_SUCCESS.equals(contactsListField)) {
                try {
                    if (Response.JSON_PARAM_DATA.equals(contactsListField) && reader.peek() == JsonToken.BEGIN_ARRAY) {
                        reader.beginArray();
                        while (reader.hasNext()) {
                            deals.add(Deal.readDeal(reader));
                        }
                        reader.endArray();
                    } else if (Response.JSON_PARAM_ADDITIONAL_DATA.equals(contactsListField) && reader.peek() == JsonToken.BEGIN_OBJECT) {
                        reader.beginObject();
                        while (reader.hasNext()) {
                            if (Pagination.JSON_PARAM_OF_PAGINATION.equals(reader.nextName())) {
                                reader.beginObject();
                                while (reader.hasNext()) {
                                    String paginationField = reader.nextName();
                                    if ("next_start".equals(paginationField)) {
                                        pageOfDeals.nextStart = reader.nextInt();
                                    } else if ("more_items_in_collection".equals(paginationField)) {
                                        pageOfDeals.moreItemsInCollection = reader.nextBoolean();
                                    } else {
                                        reader.skipValue();
                                    }
                                }
                                reader.endObject();
                            } else {
                                reader.skipValue();
                            }
                        }
                        reader.endObject();
                    } else {
                        reader.skipValue();
                    }
                } catch (Exception e) {
                    LogJourno.reportEvent(EVENT.Networking_downloadPageOfDealsFailed, String.format("PageOfDeals:[%s]", new Object[]{pageOfDeals}), e);
                    Log.e(new Throwable("Error downloading deals!", e));
                } catch (Throwable th) {
                    try {
                        in.close();
                    } catch (IOException e2) {
                    }
                }
            } else if (!reader.nextBoolean()) {
                reader.close();
            }
        }
        reader.endObject();
        reader.close();
        try {
            in.close();
        } catch (IOException e3) {
        }
        Log.d(TAG, String.format("Adding/updating %s Deals into DB.", new Object[]{Integer.valueOf(deals.size())}));
        SQLTransactionManager sqltransaction = new SQLTransactionManager(session.getDatabase());
        sqltransaction.beginTransactionNonExclusive();
        for (Deal deal : deals) {
            createOrUpdateDealIntoDBWithRelations(session, deal);
        }
        sqltransaction.commit();
        return deals;
    }

    @WorkerThread
    public static Deal createOrUpdateDealIntoDBWithRelations(@NonNull Session session, @NonNull Deal deal) {
        findOrCreateRelatedOrg(session, deal, true);
        return updateDealWithAllRelationsExceptOrg(session, deal);
    }

    @WorkerThread
    public static void createOrUpdateDealIntoDBWithRelationsForNearby(@NonNull Session session, @NonNull Deal deal) {
        findOrCreateRelatedOrg(session, deal, false);
        updateDealWithAllRelationsExceptOrg(session, deal);
    }

    @Nullable
    private static Deal updateDealWithAllRelationsExceptOrg(@NonNull Session session, @NonNull Deal deal) {
        boolean currencyDoesntExist;
        boolean stageDoesntExist = true;
        Person dealPerson = deal.getPerson();
        if (dealPerson != null && dealPerson.isExisting()) {
            Person dealPersonInDB = new PersonsDataSource(session.getDatabase()).findPersonByPipedriveId((long) dealPerson.getPipedriveId());
            if (dealPersonInDB != null) {
                deal.setPerson(dealPersonInDB);
            } else {
                Person dealPersonFromAPI = PersonNetworkingUtil.downloadPerson(session, dealPerson.getPipedriveId());
                if (dealPersonFromAPI != null) {
                    deal.setPerson(PersonNetworkingUtil.createOrUpdatePersonIntoDBWithRelations(session, dealPersonFromAPI));
                }
            }
        }
        if (new CurrenciesDataSource(session.getDatabase()).getCurrencyByCode(deal.getCurrencyCode()) == null) {
            currencyDoesntExist = true;
        } else {
            currencyDoesntExist = false;
        }
        if (currencyDoesntExist) {
            CurrenciesNetworkingUtil.downloadAndStoreCurrenciesBlocking(session);
        }
        if (new PipelineDataSource(session.getDatabase()).findStageById(deal.getStage()) != null) {
            stageDoesntExist = false;
        }
        if (stageDoesntExist) {
            StagesNetworkingUtil.downloadStagesBlocking(session, (long) deal.getPipelineId());
        }
        long dealSqlId = new DealsDataSource(session.getDatabase()).createOrUpdate(deal);
        if (dealSqlId == -1) {
            return null;
        }
        deal.setSqlId(dealSqlId);
        return deal;
    }

    private static void findOrCreateRelatedOrg(@NonNull Session session, @NonNull Deal deal, boolean canBeShadow) {
        Organization dealOrganization = deal.getOrganization();
        if (dealOrganization != null && dealOrganization.isExisting()) {
            Organization dealOrganizationInDB = (Organization) new OrganizationsDataSource(session.getDatabase()).findByPipedriveId((long) dealOrganization.getPipedriveId());
            if (dealOrganizationInDB != null) {
                deal.setOrganization(dealOrganizationInDB);
            } else {
                createOrgAndRelateItToDeal(session, deal, dealOrganization, canBeShadow);
            }
        }
    }

    private static void createOrgAndRelateItToDeal(@NonNull Session session, @NonNull Deal deal, @NonNull Organization dealOrganization, boolean canBeShadow) {
        if (dealOrganization.getPipedriveIdOrNull() != null) {
            Organization dealOrgFromAPI = OrganizationsNetworkingUtil.downloadOrganization(session, dealOrganization.getPipedriveIdOrNull().longValue());
            if (dealOrgFromAPI != null) {
                deal.setOrganization(OrganizationsNetworkingUtil.createOrUpdateOrganizationIntoDBWithRelations(session, dealOrgFromAPI));
            }
        }
    }
}
