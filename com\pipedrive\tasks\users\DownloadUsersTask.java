package com.pipedrive.tasks.users;

import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.UsersDataSource;
import com.pipedrive.gson.GsonHelper;
import com.pipedrive.model.User;
import com.pipedrive.tasks.AsyncTask;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.ConnectionUtil$JsonReaderInterceptor;
import com.pipedrive.util.networking.Response;
import com.pipedrive.util.networking.entities.UserEntity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DownloadUsersTask extends AsyncTask<Void, Void, Void> {
    public DownloadUsersTask(@NonNull Session session) {
        super(session);
    }

    protected Void doInBackground(Void... params) {
        downloadUsers();
        return null;
    }

    @WorkerThread
    public static void downloadUsers(@NonNull Session session) {
        new DownloadUsersTask(session).downloadUsers();
    }

    @WorkerThread
    private void downloadUsers() {
        AnonymousClass1UsersStreamInterceptor streamInterceptor = new ConnectionUtil$JsonReaderInterceptor<Response>() {
            private final List<UserEntity> downloadedUserEntities = new ArrayList();

            public Response interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull Response responseTemplate) throws IOException {
                if (jsonReader.peek() == JsonToken.NULL) {
                    jsonReader.skipValue();
                } else if (jsonReader.peek() == JsonToken.BEGIN_ARRAY) {
                    jsonReader.beginArray();
                    while (jsonReader.hasNext()) {
                        this.downloadedUserEntities.add((UserEntity) GsonHelper.fromJSON(jsonReader, UserEntity.class));
                    }
                    jsonReader.endArray();
                }
                return responseTemplate;
            }
        };
        ConnectionUtil.requestGetWithAutomaticPaging(new ApiUrlBuilder(getSession(), "users").pagination(Long.valueOf(0), Long.valueOf(1)), streamInterceptor);
        UsersDataSource usersDataSource = new UsersDataSource(getSession().getDatabase());
        usersDataSource.deleteAll();
        for (UserEntity downloadedUserEntity : streamInterceptor.downloadedUserEntities) {
            usersDataSource.createOrUpdate(User.fromEntity(downloadedUserEntity));
        }
    }
}
