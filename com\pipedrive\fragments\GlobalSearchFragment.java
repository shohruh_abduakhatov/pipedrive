package com.pipedrive.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import com.pipedrive.OrganizationDetailActivity;
import com.pipedrive.PersonDetailActivity;
import com.pipedrive.R;
import com.pipedrive.adapter.GlobalSearchAdapter;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.call.CallSummaryListener;
import com.pipedrive.call.CallSummaryListener.CallSummaryRegistration;
import com.pipedrive.contentproviders.GlobalSearchContentProvider;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.deal.view.DealDetailsActivity;
import com.pipedrive.interfaces.ContactOrgInterface;
import com.pipedrive.tasks.search.GlobalSearchQueryTask;
import com.pipedrive.views.CustomSearchView;
import com.pipedrive.views.CustomSearchView.OnConstraintChangeListener;
import com.pipedrive.views.viewholder.contact.PersonRowViewHolder.OnCallButtonClickListener;

public class GlobalSearchFragment extends BaseFragment implements LoaderCallbacks<Cursor>, OnCallButtonClickListener {
    private GlobalSearchAdapter adapter;
    private TextView layoutEmpty;
    @NonNull
    private SearchConstraint searchConstraint = SearchConstraint.EMPTY;

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        final Session activeSession = PipedriveApp.getActiveSession();
        if (activeSession != null) {
            ListView searchListView = (ListView) view.findViewById(R.id.searchList);
            searchListView.setOnItemClickListener(new OnItemClickListener() {
                public void onItemClick(@NonNull AdapterView<?> adapterView, @NonNull View view, int position, long id) {
                    if (GlobalSearchFragment.this.adapter.getCount() > position) {
                        Cursor cur = (Cursor) GlobalSearchFragment.this.adapter.getItem(position);
                        int rowType = GlobalSearchFragment.this.adapter.getItemViewType(position);
                        if (rowType == 0 || rowType == 1) {
                            ContactOrgInterface contact = GlobalSearchContentProvider.getContactOrgInterfaceFromCursor(activeSession, cur);
                            if (contact == null) {
                                return;
                            }
                            if (rowType == 0) {
                                PersonDetailActivity.startActivity(GlobalSearchFragment.this.getActivity(), Long.valueOf(contact.getSqlId()));
                            } else {
                                OrganizationDetailActivity.startActivity(GlobalSearchFragment.this.getActivity(), Long.valueOf(contact.getSqlId()));
                            }
                        } else if (rowType == 2) {
                            DealDetailsActivity.start(GlobalSearchFragment.this.getActivity(), Long.valueOf(GlobalSearchContentProvider.getDeal(activeSession, cur).getSqlId()));
                        }
                    }
                }
            });
            this.layoutEmpty = (TextView) view.findViewById(R.id.empty);
            fillEmptyViewData();
            searchListView.setEmptyView(this.layoutEmpty);
            this.adapter = new GlobalSearchAdapter(activeSession, getActivity(), null, 2);
            this.adapter.setOnCallButtonClickListener(this);
            searchListView.setAdapter(this.adapter);
            ((CustomSearchView) view.findViewById(R.id.search)).setOnConstraintChangeListener(new OnConstraintChangeListener() {
                public void onConstraintChanged(@NonNull SearchConstraint searchConstraint) {
                    GlobalSearchFragment.this.setSearchText(searchConstraint);
                }
            });
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_global_search, container, false);
    }

    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        CursorLoader cursorLoader = new CursorLoader(getActivity());
        Session activeSession = PipedriveApp.getActiveSession();
        if (activeSession != null) {
            cursorLoader.setUri(new GlobalSearchContentProvider(activeSession).search(this.searchConstraint));
        }
        return cursorLoader;
    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        this.adapter.setSearchConstraint(this.searchConstraint);
        this.adapter.swapCursor(data);
        fillEmptyViewData();
    }

    public void onLoaderReset(Loader<Cursor> loader) {
        this.adapter.swapCursor(null);
        fillEmptyViewData();
    }

    private void setSearchText(@NonNull SearchConstraint searchConstraint) {
        this.searchConstraint = searchConstraint;
        if (searchConstraint.isLongEnoughForSearch()) {
            getLoaderManager().restartLoader(3, null, this);
            initializeSearchTask();
            return;
        }
        getLoaderManager().destroyLoader(3);
        if (this.adapter != null) {
            this.adapter.swapCursor(null);
        }
    }

    private void fillEmptyViewData() {
        if (this.searchConstraint.isLongEnoughForSearch()) {
            this.layoutEmpty.setText(R.string.label_empty_header_global_search_list_no_data);
        } else {
            this.layoutEmpty.setText(R.string.label_empty_header_global_search_list);
        }
    }

    private void initializeSearchTask() {
        if (!GlobalSearchQueryTask.isExecuting(PipedriveApp.getActiveSession(), GlobalSearchQueryTask.class)) {
            new GlobalSearchQueryTask(PipedriveApp.getActiveSession()).execute(new SearchConstraint[]{this.searchConstraint});
        }
    }

    public void onCallButtonClicked(@NonNull String phoneNumberToDial, @Nullable Long personSqlId) {
        Session session = PipedriveApp.getActiveSession();
        FragmentActivity nonApplicationContext = getActivity();
        if (isResumed() && nonApplicationContext != null) {
            CallSummaryListener.makeCallWithCallSummaryRegistration(session, nonApplicationContext, new CallSummaryRegistration(phoneNumberToDial, personSqlId));
        }
    }
}
