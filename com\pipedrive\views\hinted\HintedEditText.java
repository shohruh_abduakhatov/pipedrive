package com.pipedrive.views.hinted;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.TextView;

abstract class HintedEditText<T extends TextView> extends BaseHintedLayout<T> {
    public HintedEditText(Context context) {
        this(context, null);
    }

    public HintedEditText(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HintedEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupTextChangeCallback();
    }

    private void setupTextChangeCallback() {
        if (hintValueShouldBeShared()) {
            ((TextView) this.mMainView).setHint(this.mHint.getText());
            ((TextView) this.mMainView).addTextChangedListener(new TextWatcher() {
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(@NonNull CharSequence s, int start, int before, int count) {
                }

                public void afterTextChanged(Editable s) {
                    boolean textIsEmpty = TextUtils.isEmpty(s.toString());
                    if (textIsEmpty && HintedEditText.this.mHint.getVisibility() == 0) {
                        HintedEditText.this.mHint.setVisibility(4);
                    } else if (!textIsEmpty && HintedEditText.this.mHint.getVisibility() != 0) {
                        HintedEditText.this.mHint.setVisibility(0);
                    }
                }
            });
        }
    }

    protected boolean hintValueShouldBeShared() {
        return true;
    }
}
