package com.pipedrive.fragments;

import android.support.annotation.UiThread;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;
import com.pipedrive.views.fab.FabWithOverlayMenu;
import com.pipedrive.views.profilepicture.ProfilePictureRoundPerson;

public final class PersonDetailFragment_ViewBinding implements Unbinder {
    private PersonDetailFragment target;
    private View view2131820992;

    @UiThread
    public PersonDetailFragment_ViewBinding(final PersonDetailFragment target, View source) {
        this.target = target;
        target.mNameTextView = (TextView) Utils.findRequiredViewAsType(source, R.id.name, "field 'mNameTextView'", TextView.class);
        View view = Utils.findRequiredView(source, R.id.organization, "field 'mOrganizationTextView' and method 'onOrganizationClicked'");
        target.mOrganizationTextView = (TextView) Utils.castView(view, R.id.organization, "field 'mOrganizationTextView'", TextView.class);
        this.view2131820992 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onOrganizationClicked((TextView) Utils.castParam(p0, "doClick", 0, "onOrganizationClicked", 0));
            }
        });
        target.mProfilePictureRoundPerson = (ProfilePictureRoundPerson) Utils.findRequiredViewAsType(source, R.id.profilePicture, "field 'mProfilePictureRoundPerson'", ProfilePictureRoundPerson.class);
        target.mFabWithOverlayMenu = (FabWithOverlayMenu) Utils.findRequiredViewAsType(source, R.id.fab, "field 'mFabWithOverlayMenu'", FabWithOverlayMenu.class);
        target.mViewPager = (ViewPager) Utils.findRequiredViewAsType(source, R.id.pager, "field 'mViewPager'", ViewPager.class);
        target.mTabLayout = (TabLayout) Utils.findRequiredViewAsType(source, R.id.tab_layout, "field 'mTabLayout'", TabLayout.class);
    }

    public void unbind() {
        PersonDetailFragment target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mNameTextView = null;
        target.mOrganizationTextView = null;
        target.mProfilePictureRoundPerson = null;
        target.mFabWithOverlayMenu = null;
        target.mViewPager = null;
        target.mTabLayout = null;
        this.view2131820992.setOnClickListener(null);
        this.view2131820992 = null;
    }
}
