package com.zendesk.sdk.network.impl;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.model.access.Identity;
import com.zendesk.sdk.model.push.PushRegistrationResponse;
import com.zendesk.sdk.model.request.CustomField;
import com.zendesk.sdk.model.settings.MobileSettings;
import com.zendesk.sdk.model.settings.SafeMobileSettings;
import com.zendesk.sdk.network.Constants;
import com.zendesk.sdk.network.SdkOptions;
import com.zendesk.sdk.network.ZendeskTracker;
import com.zendesk.sdk.network.impl.ApplicationScope.Builder;
import com.zendesk.sdk.storage.IdentityStorage;
import com.zendesk.sdk.storage.SdkStorage;
import com.zendesk.sdk.storage.StorageStore;
import com.zendesk.service.ErrorResponseAdapter;
import com.zendesk.service.ZendeskCallback;
import com.zendesk.util.CollectionUtils;
import com.zendesk.util.StringUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public enum ZendeskConfig {
    INSTANCE;
    
    public static final int HEADER_SUFFIX_MAX_LENGTH = 1024;
    private static final String LOG_TAG = "ZendeskConfiguration";
    private static final String SLASH = "/";
    private ApplicationScope applicationScope;
    private boolean initialised;
    private ZendeskTracker tracker;
    private List<Pair<String, String>> uaHeaderSuffixes;
    private ZendeskConfigHelper zendeskConfigHelper;

    public void init(Context context, String zendeskUrl, String applicationId, String oauthClientId) {
        this.uaHeaderSuffixes = null;
        if (this.initialised) {
            String message = "ZendeskConfig is already initialised, skipping reinitialisation.";
            Logger.i(LOG_TAG, "ZendeskConfig is already initialised, skipping reinitialisation.", new Object[0]);
            initApplicationScope(this.applicationScope.newBuilder().userAgentHeader(buildUserAgentHeader()).build());
        } else if (context != null && StringUtils.hasLength(zendeskUrl) && StringUtils.hasLength(applicationId) && StringUtils.hasLength(oauthClientId)) {
            installTracker(new AnswersTracker());
            initApplicationScope(new Builder(context.getApplicationContext(), zendeskUrl, applicationId, oauthClientId).userAgentHeader(buildUserAgentHeader()).build());
            this.initialised = true;
        } else {
            String str = LOG_TAG;
            String str2 = "Invalid configuration provided | Context: %b | Url: %b | Application Id: %b | OauthClientId: %b";
            Throwable configurationRuntimeException = new ConfigurationRuntimeException("Could not validate minimal configuration");
            Object[] objArr = new Object[4];
            objArr[0] = Boolean.valueOf(context != null);
            objArr[1] = Boolean.valueOf(StringUtils.hasLength(zendeskUrl));
            objArr[2] = Boolean.valueOf(StringUtils.hasLength(applicationId));
            objArr[3] = Boolean.valueOf(StringUtils.hasLength(oauthClientId));
            Logger.e(str, str2, configurationRuntimeException, objArr);
        }
    }

    public boolean isInitialized() {
        return this.initialised;
    }

    public String getZendeskUrl() {
        return this.applicationScope.getUrl();
    }

    public String getOauthClientId() {
        return this.applicationScope.getOAuthToken();
    }

    public SafeMobileSettings getMobileSettings() {
        SafeMobileSettings storedSettings = this.zendeskConfigHelper.getStorageStore().sdkSettingsStorage().getStoredSettings();
        return storedSettings == null ? new SafeMobileSettings(new MobileSettings()) : storedSettings;
    }

    public boolean isAuthenticationAvailable() {
        return this.zendeskConfigHelper.getStorageStore().identityStorage().getIdentity() != null;
    }

    public String getApplicationId() {
        return this.applicationScope.getAppId();
    }

    public void setIdentity(Identity identity) {
        IdentityStorage identityStorage = this.zendeskConfigHelper.getStorageStore().identityStorage();
        Identity currentIdentity = this.zendeskConfigHelper.getStorageStore().identityStorage().getIdentity();
        if (currentIdentity == null) {
            Logger.d(LOG_TAG, "No previous identity set, storing identity", new Object[0]);
            identityStorage.storeIdentity(identity);
            initApplicationScope(this.applicationScope.newBuilder().build());
            return;
        }
        boolean endUserWasCleared;
        boolean endUserHasChanged;
        if (identity == null) {
            endUserWasCleared = true;
        } else {
            endUserWasCleared = false;
        }
        if (currentIdentity.equals(identity)) {
            endUserHasChanged = false;
        } else {
            endUserHasChanged = true;
        }
        if (endUserWasCleared || endUserHasChanged) {
            Logger.d(LOG_TAG, String.format(Locale.US, "Clearing user data. End user cleared: %s, end user changed: %s", new Object[]{Boolean.valueOf(endUserWasCleared), Boolean.valueOf(endUserHasChanged)}), new Object[0]);
            this.zendeskConfigHelper.getStorageStore().sdkStorage().clearUserData();
            if (identity != null) {
                Logger.d(LOG_TAG, "Identity has changed, storing new identity", new Object[0]);
                if (identity instanceof AnonymousIdentity) {
                    ((AnonymousIdentity) identity).reloadGuid();
                }
                identityStorage.storeIdentity(identity);
                initApplicationScope(this.applicationScope.newBuilder().build());
            }
        }
    }

    public void setCustomFields(List<CustomField> customFields) {
        initApplicationScope(this.applicationScope.newBuilder().customFields(customFields).build());
    }

    @Nullable
    public List<CustomField> getCustomFields() {
        return this.applicationScope.getCustomFields();
    }

    public void setTicketFormId(Long ticketFormId) {
        initApplicationScope(this.applicationScope.newBuilder().ticketFormId(ticketFormId).build());
    }

    @Nullable
    public Long getTicketFormId() {
        return this.applicationScope.getTicketFormId();
    }

    public void setDeviceLocale(Locale deviceLocale) {
        if (this.initialised) {
            initApplicationScope(this.applicationScope.newBuilder().locale(deviceLocale).build());
            Logger.d(LOG_TAG, "Locale has been overridden, requesting new settings.", new Object[0]);
            this.zendeskConfigHelper.getProviderStore().sdkSettingsProvider().getSettings(null);
            return;
        }
        Logger.w(LOG_TAG, "Locale cannot be set before SDK has been initialised. init() must be called before setDeviceLocale().", new Object[0]);
    }

    Locale getDeviceLocale() {
        return this.applicationScope.getLocale();
    }

    public void setCoppaEnabled(boolean enabled) {
        if (this.initialised && enabled) {
            this.zendeskConfigHelper.getStorageStore().identityStorage().anonymiseIdentity();
        }
        initApplicationScope(this.applicationScope.newBuilder().coppa(enabled).build());
    }

    public boolean isCoppaEnabled() {
        return this.applicationScope.isCoppaEnabled();
    }

    public void enablePushWithIdentifier(String identifier, @Nullable ZendeskCallback<PushRegistrationResponse> callback) {
        if (this.initialised) {
            this.zendeskConfigHelper.getProviderStore().pushRegistrationProvider().registerDeviceWithIdentifier(identifier, getDeviceLocale(), callback);
        } else if (callback != null) {
            callback.onError(new ErrorResponseAdapter("Zendesk not initialized, skipping enablePush()"));
        }
    }

    public void enablePushWithUAChannelId(String identifier, @Nullable ZendeskCallback<PushRegistrationResponse> callback) {
        if (this.initialised) {
            this.zendeskConfigHelper.getProviderStore().pushRegistrationProvider().registerDeviceWithUAChannelId(identifier, getDeviceLocale(), callback);
        } else if (callback != null) {
            callback.onError(new ErrorResponseAdapter("Zendesk not initialized, skipping enablePush()"));
        }
    }

    public void disablePush(String identifier, @Nullable ZendeskCallback<Void> callback) {
        if (this.initialised) {
            this.zendeskConfigHelper.getProviderStore().pushRegistrationProvider().unregisterDevice(identifier, callback);
        } else if (callback != null) {
            callback.onError(new ErrorResponseAdapter("Zendesk not initialized, skipping disablePush()"));
        }
    }

    public SdkOptions getSdkOptions() {
        return this.applicationScope.getSdkOptions();
    }

    public void setSdkOptions(SdkOptions sdkOptions) {
        initApplicationScope(this.applicationScope.newBuilder().sdkOptions(sdkOptions).build());
    }

    boolean addUserAgentHeaderSuffix(Pair<String, String> suffix) {
        if (suffix == null || suffix.first == null || suffix.second == null) {
            Logger.e(LOG_TAG, "User agent header suffix must be a valid non-null name-value pair. Suffix not added.", new Object[0]);
            return false;
        } else if (suffixContainsInvalidCharacter(suffix)) {
            Logger.e(LOG_TAG, "Neither element of the suffix Pair can contain a slash or a new line character. Suffix not added.", new Object[0]);
            return false;
        } else if (suffixWouldMakeHeaderTooLong(suffix)) {
            Logger.e(LOG_TAG, "Header must be less than 1024 characters. Suffix not added.", new Object[0]);
            return false;
        } else {
            if (this.uaHeaderSuffixes == null) {
                this.uaHeaderSuffixes = new ArrayList(1);
            }
            this.uaHeaderSuffixes.add(suffix);
            initApplicationScope(this.applicationScope.newBuilder().userAgentHeader(buildUserAgentHeader()).build());
            return true;
        }
    }

    private boolean suffixContainsInvalidCharacter(Pair<String, String> suffix) {
        return ((String) suffix.first).contains(SLASH) || ((String) suffix.second).contains(SLASH) || ((String) suffix.first).contains(StringUtils.LINE_SEPARATOR) || ((String) suffix.second).contains(StringUtils.LINE_SEPARATOR);
    }

    private boolean suffixWouldMakeHeaderTooLong(Pair<String, String> suffix) {
        String formattedSuffix = String.format(Constants.HEADER_SUFFIX_FORMAT, new Object[]{suffix.first, suffix.second});
        int currentHeaderLength = getUserAgentHeader() == null ? 0 : getUserAgentHeader().length();
        if (formattedSuffix.length() >= 1024 || formattedSuffix.length() + currentHeaderLength >= 1024) {
            return true;
        }
        return false;
    }

    void clearUserAgentHeaderSuffixes() {
        if (this.uaHeaderSuffixes != null) {
            this.uaHeaderSuffixes.clear();
        }
        initApplicationScope(this.applicationScope.newBuilder().userAgentHeader(buildUserAgentHeader()).build());
    }

    String getUserAgentHeader() {
        return this.applicationScope.getUserAgentHeader();
    }

    private String buildUserAgentHeader() {
        String environment = INSTANCE.isDevelopment() ? Constants.ENVIRONMENT_DEBUG : Constants.ENVIRONMENT_PRODUCTION;
        StringBuilder stringBuilder = new StringBuilder(String.format(Locale.US, Constants.ZENDESK_SDK_FOR_ANDROID, new Object[]{"1.9.2.1", Integer.valueOf(VERSION.SDK_INT), environment}));
        if (CollectionUtils.isNotEmpty(this.uaHeaderSuffixes)) {
            for (Pair<String, String> suffix : this.uaHeaderSuffixes) {
                stringBuilder.append(String.format(Locale.US, Constants.HEADER_SUFFIX_FORMAT, new Object[]{suffix.first, suffix.second}));
            }
        }
        return stringBuilder.toString();
    }

    boolean isDevelopment() {
        return this.applicationScope.isDevelopmentMode();
    }

    public ProviderStore provider() {
        return this.zendeskConfigHelper.getProviderStore();
    }

    public StorageStore storage() {
        return this.zendeskConfigHelper.getStorageStore();
    }

    private void initApplicationScope(ApplicationScope applicationScope) {
        this.applicationScope = applicationScope;
        this.zendeskConfigHelper = ZendeskConfigInjector.injectZendeskConfigHelper(applicationScope);
        SdkStorage sdkStorage = this.zendeskConfigHelper.getStorageStore().sdkStorage();
        sdkStorage.registerUserStorage(this.zendeskConfigHelper.getStorageStore().identityStorage());
        sdkStorage.registerUserStorage(this.zendeskConfigHelper.getStorageStore().requestStorage());
        sdkStorage.registerUserStorage(this.zendeskConfigHelper.getStorageStore().pushStorage());
    }

    private void initWithStubs() {
        this.applicationScope = new Builder(null, "", "", "").build();
        this.zendeskConfigHelper = ZendeskConfigInjector.injectStubZendeskConfigHelper(this.applicationScope);
        this.initialised = false;
    }

    void reset(Context context) {
        Logger.d(LOG_TAG, "Tearing down ZendeskConfig", new Object[0]);
        this.uaHeaderSuffixes = null;
        storage().sdkStorage().clearUserData();
        storage().sdkSettingsStorage().deleteStoredSettings();
        initWithStubs();
    }

    void installTracker(ZendeskTracker tracker) {
        this.tracker = tracker;
    }

    @NonNull
    public ZendeskTracker getTracker() {
        return this.tracker == null ? new NoOpTracker() : this.tracker;
    }
}
