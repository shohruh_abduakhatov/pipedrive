package com.pipedrive.customfields.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.customfields.views.CustomFieldListView.OnItemClickListener;
import com.pipedrive.customfields.views.PersonCustomFieldListView.OnPersonCustomFieldListItemClickListener;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.model.Person;
import com.pipedrive.model.contact.Email;
import com.pipedrive.model.contact.Im;
import com.pipedrive.model.contact.Phone;
import com.pipedrive.util.PipedriveUtil;

class PersonSpecialCustomFieldViewHolder extends SpecialCustomFieldViewHolder {
    public PersonSpecialCustomFieldViewHolder(@NonNull Context context, @NonNull Long itemSqlId) {
        super(context, itemSqlId);
        LayoutInflater.from(context).inflate(R.layout.layout_person_special_custom_fields, this.rootContainer, true);
    }

    void bind(@NonNull Session session, @Nullable OnItemClickListener onItemClickListener) {
        Person person = (Person) new PersonsDataSource(session.getDatabase()).findBySqlId(this.itemSqlId.longValue());
        if (person != null) {
            fillPersonData(this.itemView.getContext(), person, onItemClickListener);
        }
    }

    private void fillPersonData(@NonNull Context context, @NonNull Person person, @Nullable OnItemClickListener onItemClickListener) {
        fillPhones(context, person, onItemClickListener);
        fillEmails(context, person, onItemClickListener);
        fillIms(context, person, onItemClickListener);
        fillPostalAddress(person, onItemClickListener);
    }

    private void fillPostalAddress(@NonNull Person person, @Nullable final OnItemClickListener onItemClickListener) {
        if (!TextUtils.isEmpty(person.getPostalAddress())) {
            this.itemView.findViewById(R.id.labelPostalAddress).setVisibility(0);
            View layout = this.itemView.findViewById(R.id.layoutPostalAddress);
            layout.findViewById(R.id.icon).setVisibility(8);
            ((TextView) layout.findViewById(R.id.content)).setText(person.getPostalAddress());
            layout.findViewById(R.id.label).setVisibility(8);
            layout.setTag(person.getPostalAddress());
            layout.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    boolean listenerExist;
                    if (onItemClickListener != null) {
                        listenerExist = true;
                    } else {
                        listenerExist = false;
                    }
                    boolean valueExist;
                    if (v.getTag() != null) {
                        valueExist = true;
                    } else {
                        valueExist = false;
                    }
                    if (listenerExist && valueExist) {
                        onItemClickListener.onAddressClicked(PipedriveUtil.getMapUriForAddress((String) v.getTag()));
                    }
                }
            });
            layout.setOnLongClickListener(new OnLongClickListener() {
                public boolean onLongClick(View v) {
                    return PersonSpecialCustomFieldViewHolder.this.onSpecialFieldLongClicked(v, onItemClickListener);
                }
            });
            layout.setVisibility(0);
        }
    }

    private void fillIms(Context context, @NonNull Person person, @Nullable final OnItemClickListener onItemClickListener) {
        boolean contactHasIms = !person.getIms().isEmpty();
        TextView labelContactIm = (TextView) this.itemView.findViewById(R.id.labelContactIm);
        LinearLayout layoutIms = (LinearLayout) this.itemView.findViewById(R.id.layoutIms);
        layoutIms.removeAllViews();
        if (contactHasIms) {
            for (Im im : person.getIms()) {
                View itemLayout = LayoutInflater.from(context).inflate(R.layout.row_person_detail_person, layoutIms, false);
                ((ImageView) itemLayout.findViewById(R.id.icon)).setVisibility(8);
                if (!TextUtils.isEmpty(im.getLabel())) {
                    ((TextView) itemLayout.findViewById(R.id.label)).setText(im.getLabel());
                }
                ((TextView) itemLayout.findViewById(R.id.content)).setText(im.getValue());
                itemLayout.setTag(im.getValue());
                itemLayout.setOnLongClickListener(new OnLongClickListener() {
                    public boolean onLongClick(View v) {
                        return PersonSpecialCustomFieldViewHolder.this.onSpecialFieldLongClicked(v, onItemClickListener);
                    }
                });
                layoutIms.addView(itemLayout, getSpecialItemLayoutParams(context));
            }
        }
        labelContactIm.setVisibility(contactHasIms ? 0 : 8);
        layoutIms.setVisibility(contactHasIms ? 0 : 8);
    }

    private void fillEmails(@NonNull Context context, @NonNull final Person person, @Nullable final OnItemClickListener onItemClickListener) {
        int i;
        int i2 = 0;
        TextView labelContactEmail = (TextView) this.itemView.findViewById(R.id.labelContactEmail);
        LinearLayout layoutEmails = (LinearLayout) this.itemView.findViewById(R.id.layoutEmails);
        layoutEmails.removeAllViews();
        if (!person.getEmails().isEmpty()) {
            for (Email email : person.getEmails()) {
                View itemLayout = LayoutInflater.from(context).inflate(R.layout.row_person_detail_person, layoutEmails, false);
                ((ImageView) itemLayout.findViewById(R.id.icon)).setImageLevel(1);
                if (!TextUtils.isEmpty(email.getLabel())) {
                    ((TextView) itemLayout.findViewById(R.id.label)).setText(email.getLabel());
                }
                ((TextView) itemLayout.findViewById(R.id.content)).setText(email.getEmail());
                itemLayout.setTag(email.getEmail());
                itemLayout.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        boolean smartListenerExist;
                        OnPersonCustomFieldListItemClickListener smartListener = PersonSpecialCustomFieldViewHolder.this.getPersonCustomFieldListViewOnItemClickListener(onItemClickListener);
                        if (smartListener != null) {
                            smartListenerExist = true;
                        } else {
                            smartListenerExist = false;
                        }
                        boolean valueExist;
                        if (v.getTag() != null) {
                            valueExist = true;
                        } else {
                            valueExist = false;
                        }
                        if (smartListenerExist && valueExist) {
                            smartListener.onEmailClicked((String) v.getTag(), person.getDropBoxAddress());
                        }
                    }
                });
                itemLayout.setOnLongClickListener(new OnLongClickListener() {
                    public boolean onLongClick(View v) {
                        return PersonSpecialCustomFieldViewHolder.this.onSpecialFieldLongClicked(v, onItemClickListener);
                    }
                });
                layoutEmails.addView(itemLayout, getSpecialItemLayoutParams(context));
            }
        }
        if (person.hasEmails()) {
            i = 0;
        } else {
            i = 8;
        }
        labelContactEmail.setVisibility(i);
        if (!person.hasEmails()) {
            i2 = 8;
        }
        layoutEmails.setVisibility(i2);
    }

    private void fillPhones(@NonNull Context context, @NonNull Person person, @Nullable final OnItemClickListener onItemClickListener) {
        int i;
        int i2 = 0;
        TextView labelContactPhone = (TextView) this.itemView.findViewById(R.id.labelContactPhone);
        LinearLayout layoutPhones = (LinearLayout) this.itemView.findViewById(R.id.layoutPhones);
        layoutPhones.removeAllViews();
        if (!person.getPhones().isEmpty()) {
            for (Phone phone : person.getPhones()) {
                View itemLayout = LayoutInflater.from(context).inflate(R.layout.row_person_detail_person, layoutPhones, false);
                ((ImageView) itemLayout.findViewById(R.id.icon)).setImageLevel(0);
                if (!TextUtils.isEmpty(phone.getLabel())) {
                    ((TextView) itemLayout.findViewById(R.id.label)).setText(phone.getLabel());
                }
                ((TextView) itemLayout.findViewById(R.id.content)).setText(phone.getPhone());
                itemLayout.setTag(phone.getPhone());
                itemLayout.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        boolean listenerExist;
                        if (onItemClickListener != null) {
                            listenerExist = true;
                        } else {
                            listenerExist = false;
                        }
                        boolean valueExist;
                        if (v.getTag() != null) {
                            valueExist = true;
                        } else {
                            valueExist = false;
                        }
                        if (listenerExist && valueExist) {
                            onItemClickListener.onPhoneClicked((String) v.getTag(), false);
                        }
                    }
                });
                itemLayout.setOnLongClickListener(new OnLongClickListener() {
                    public boolean onLongClick(View v) {
                        return PersonSpecialCustomFieldViewHolder.this.onSpecialFieldLongClicked(v, onItemClickListener);
                    }
                });
                layoutPhones.addView(itemLayout, getSpecialItemLayoutParams(context));
            }
        }
        if (person.hasPhones()) {
            i = 0;
        } else {
            i = 8;
        }
        labelContactPhone.setVisibility(i);
        if (!person.hasPhones()) {
            i2 = 8;
        }
        layoutPhones.setVisibility(i2);
    }

    @NonNull
    private LayoutParams getSpecialItemLayoutParams(@NonNull Context context) {
        int margin = (int) TypedValue.applyDimension(1, 16.0f, context.getResources().getDisplayMetrics());
        LayoutParams layoutParams = new LayoutParams(-1, (int) TypedValue.applyDimension(1, 48.0f, context.getResources().getDisplayMetrics()));
        layoutParams.setMargins(margin, 0, margin, 0);
        return layoutParams;
    }

    private boolean onSpecialFieldLongClicked(View v, @Nullable OnItemClickListener listener) {
        boolean listenerExist;
        if (listener != null) {
            listenerExist = true;
        } else {
            listenerExist = false;
        }
        boolean valueExist;
        if (v.getTag() != null) {
            valueExist = true;
        } else {
            valueExist = false;
        }
        if (!listenerExist || !valueExist) {
            return false;
        }
        listener.onCustomFieldLongClicked((String) v.getTag());
        return true;
    }

    @Nullable
    private OnPersonCustomFieldListItemClickListener getPersonCustomFieldListViewOnItemClickListener(@Nullable OnItemClickListener listener) {
        boolean listenerIsNotSet;
        boolean listenerIsSimple = true;
        if (listener == null) {
            listenerIsNotSet = true;
        } else {
            listenerIsNotSet = false;
        }
        if (listenerIsNotSet) {
            return null;
        }
        if (OnPersonCustomFieldListItemClickListener.class.isAssignableFrom(listener.getClass())) {
            listenerIsSimple = false;
        }
        if (listenerIsSimple) {
            return null;
        }
        return (OnPersonCustomFieldListItemClickListener) listener;
    }
}
