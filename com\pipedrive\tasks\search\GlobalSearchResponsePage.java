package com.pipedrive.tasks.search;

import com.pipedrive.model.Deal;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.model.pagination.Pagination;
import java.util.ArrayList;
import java.util.List;

class GlobalSearchResponsePage extends Pagination {
    protected static final String ITEM_TYPE_MARKER_CONTACT = "person";
    protected static final String ITEM_TYPE_MARKER_DEAL = "deal";
    protected static final String ITEM_TYPE_MARKER_ORGANIZATION = "organization";
    protected static final int NEXT_PAGE_START_UNDEFINED = -1;
    protected final List<Deal> deals = new ArrayList();
    protected int mNextPage = -1;
    protected final List<Person> mPersons = new ArrayList();
    protected final List<Organization> organizations = new ArrayList();

    GlobalSearchResponsePage() {
    }

    protected void clear() {
        this.deals.clear();
        this.mPersons.clear();
        this.organizations.clear();
    }
}
