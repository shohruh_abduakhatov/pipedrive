package com.pipedrive.util.networking.entities;

import android.support.annotation.NonNull;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pipedrive.model.notes.Note;

public class NoteEntity {
    private static final String JSON_PARAM_ACTIVE_FLAG = "active_flag";
    private static final String JSON_PARAM_ADD_TIME = "add_time";
    private static final String JSON_PARAM_CONTENT = "content";
    private static final String JSON_PARAM_DEAL = "deal";
    private static final String JSON_PARAM_DEAL_ID = "deal_id";
    private static final String JSON_PARAM_DEAL_TITLE = "title";
    private static final String JSON_PARAM_ORGANIZATION = "organization";
    private static final String JSON_PARAM_ORGANIZATION_NAME = "name";
    private static final String JSON_PARAM_ORG_ID = "org_id";
    private static final String JSON_PARAM_PERSON = "person";
    private static final String JSON_PARAM_PERSON_ID = "person_id";
    private static final String JSON_PARAM_PERSON_NAME = "name";
    private static final String JSON_PARAM_PIPEDRIVE_ID = "id";
    private static final int UNDEFINED_INT = 0;
    private static final String UNDEFINED_STRING = null;
    @SerializedName("active_flag")
    @Expose
    public boolean activeFlag;
    @SerializedName("add_time")
    @Expose
    public String addTime;
    @SerializedName("content")
    @Expose
    public String content = UNDEFINED_STRING;
    @SerializedName("deal")
    @Expose
    public Deal deal = new Deal();
    @SerializedName("deal_id")
    @Expose
    public int dealId = 0;
    @SerializedName("org_id")
    @Expose
    public int orgId = 0;
    @SerializedName("organization")
    @Expose
    public Organization organization = new Organization();
    @SerializedName("person")
    @Expose
    public Person person = new Person();
    @SerializedName("person_id")
    @Expose
    public int personId = 0;
    @SerializedName("id")
    @Expose
    public int pipedriveId;

    public static class Deal {
        @SerializedName("title")
        @Expose
        public String title = NoteEntity.UNDEFINED_STRING;
    }

    public static class Organization {
        @SerializedName("name")
        @Expose
        public String name = NoteEntity.UNDEFINED_STRING;
    }

    public static class Person {
        @SerializedName("name")
        @Expose
        public String name = NoteEntity.UNDEFINED_STRING;
    }

    @NonNull
    public static JsonObject getJSON(Note note) {
        JsonObject noteJson = new JsonObject();
        if (note.getDeal() != null && note.getDeal().isExisting()) {
            noteJson.addProperty(JSON_PARAM_DEAL_ID, Integer.valueOf(note.getDeal().getPipedriveId()));
        }
        if (note.getPerson() != null && note.getPerson().isExisting()) {
            noteJson.addProperty(JSON_PARAM_PERSON_ID, Integer.valueOf(note.getPerson().getPipedriveId()));
        }
        if (note.getOrganization() != null && note.getOrganization().isExisting()) {
            noteJson.addProperty(JSON_PARAM_ORG_ID, Integer.valueOf(note.getOrganization().getPipedriveId()));
        }
        if (note.getContent() != null) {
            noteJson.addProperty(JSON_PARAM_CONTENT, note.getContent());
        }
        noteJson.addProperty(JSON_PARAM_ACTIVE_FLAG, Boolean.valueOf(note.isActiveFlag()));
        return noteJson;
    }

    public String toString() {
        return "NoteEntity{content='" + this.content + '\'' + ", activeFlag=" + this.activeFlag + ", dealId=" + this.dealId + ", deal=" + this.deal + ", personId=" + this.personId + ", person=" + this.person + ", orgId=" + this.orgId + ", organization=" + this.organization + ", pipedriveId=" + this.pipedriveId + ", addTime='" + this.addTime + '\'' + '}';
    }
}
