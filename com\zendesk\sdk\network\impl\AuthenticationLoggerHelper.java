package com.zendesk.sdk.network.impl;

import android.support.annotation.Nullable;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.model.access.AuthenticationType;
import com.zendesk.sdk.model.access.Identity;
import com.zendesk.sdk.model.access.JwtIdentity;

class AuthenticationLoggerHelper {
    private final String logMessage;

    AuthenticationLoggerHelper(@Nullable AuthenticationType expectedAuthenticationType, @Nullable Identity storedIdentity) {
        StringBuilder stringBuilder = new StringBuilder(160);
        stringBuilder.append("The expected type of authentication is ");
        if (expectedAuthenticationType == null) {
            stringBuilder.append("null. Check that settings have been downloaded.");
        } else if (expectedAuthenticationType == AuthenticationType.ANONYMOUS) {
            stringBuilder.append("anonymous.");
        } else if (expectedAuthenticationType == AuthenticationType.JWT) {
            stringBuilder.append("jwt.");
        }
        stringBuilder.append('\n');
        stringBuilder.append("The local identity is");
        if (storedIdentity == null) {
            stringBuilder.append(" not");
        }
        stringBuilder.append(" present.\n");
        if (storedIdentity != null) {
            stringBuilder.append("The local identity is ");
            if (storedIdentity instanceof AnonymousIdentity) {
                stringBuilder.append("anonymous.");
            } else if (storedIdentity instanceof JwtIdentity) {
                stringBuilder.append("jwt.");
            } else {
                stringBuilder.append("unknown.");
            }
        }
        this.logMessage = stringBuilder.toString();
    }

    public String getLogMessage() {
        return this.logMessage;
    }
}
