package com.pipedrive.tasks.persons;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.plus.PlusShare;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.newrelic.agent.android.instrumentation.JSONObjectInstrumentation;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.PipeSQLiteHelper;
import com.pipedrive.logging.Log;
import com.pipedrive.model.PersonField;
import com.pipedrive.model.PersonFieldOption;
import com.pipedrive.tasks.AsyncTask;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.Response;
import com.pipedrive.util.persons.PersonNetworkingUtil;
import com.pipedrive.util.time.TimeManager;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;

public class DownloadPersonsTask extends AsyncTask<String, Integer, Void> {
    private static final String TAG = DownloadPersonsTask.class.getSimpleName();

    public DownloadPersonsTask(Session session) {
        super(session);
    }

    protected Void doInBackground(String... params) {
        String tag = TAG + ".doInBackground()";
        downloadPersonFields(getSession());
        long dlStart = TimeManager.getInstance().currentTimeMillis().longValue();
        Log.d(tag, "Starting mPersons download...");
        PersonNetworkingUtil.downloadPersons(getSession());
        Log.d(tag, "Contacts downloaded. Time: took " + (TimeManager.getInstance().currentTimeMillis().longValue() - dlStart) + "ms");
        return null;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void downloadPersonFields(@NonNull Session session) {
        PersonField visibilityField = null;
        InputStream in = getPersonFieldsStream(session);
        if (in != null) {
            JsonReader reader = new JsonReader(new InputStreamReader(in, HttpRequest.CHARSET_UTF8));
            reader.beginObject();
            while (reader.hasNext()) {
                String contactsListField = reader.nextName();
                if (!Response.JSON_PARAM_SUCCESS.equals(contactsListField)) {
                    try {
                        if (Response.JSON_PARAM_DATA.equals(contactsListField) && reader.peek() == JsonToken.BEGIN_ARRAY) {
                            reader.beginArray();
                            while (reader.hasNext()) {
                                PersonField personField = readPersonField(reader);
                                if (personField.getKey() != null && PipeSQLiteHelper.COLUMN_VISIBLE_TO.equalsIgnoreCase(personField.getKey())) {
                                    visibilityField = personField;
                                }
                            }
                            reader.endArray();
                        } else {
                            reader.skipValue();
                        }
                    } catch (Exception e) {
                        Log.e(e);
                    } catch (Throwable th) {
                        try {
                            in.close();
                        } catch (IOException e2) {
                        }
                    }
                } else if (!reader.nextBoolean()) {
                    reader.close();
                }
            }
            reader.endObject();
            reader.close();
            try {
                in.close();
            } catch (IOException e3) {
            }
        }
        if (visibilityField != null) {
            try {
                JSONObject visibilityJson = new JSONObject();
                JSONArray optionsArr = new JSONArray();
                for (PersonFieldOption option : visibilityField.getOptions()) {
                    JSONObject optionObj = new JSONObject();
                    optionObj.putOpt(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, Integer.valueOf(option.getId()));
                    optionObj.putOpt(PlusShare.KEY_CALL_TO_ACTION_LABEL, option.getLabel());
                    optionsArr.put(optionObj);
                }
                visibilityJson.putOpt("options", optionsArr);
                visibilityJson.putOpt(PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID, Integer.valueOf(visibilityField.getId()));
                visibilityJson.putOpt("key", visibilityField.getKey());
                session.setContactsVisibilityFields(!(visibilityJson instanceof JSONObject) ? visibilityJson.toString() : JSONObjectInstrumentation.toString(visibilityJson));
            } catch (Exception e4) {
                Log.e(new Throwable("Error saving visibility fields.", e4));
            }
        }
    }

    @Nullable
    private static InputStream getPersonFieldsStream(@NonNull Session session) {
        return ConnectionUtil.requestGet(new ApiUrlBuilder(session, ApiUrlBuilder.ENDPOINT_PERSON_FIELDS));
    }

    @NonNull
    public static PersonField readPersonField(@NonNull JsonReader reader) throws IOException {
        PersonField field = new PersonField();
        reader.beginObject();
        while (reader.hasNext()) {
            String contactField = reader.nextName();
            if (PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID.equals(contactField) && reader.peek() != JsonToken.NULL) {
                field.setId(reader.nextInt());
            } else if ("key".equals(contactField) && reader.peek() != JsonToken.NULL) {
                field.setKey(reader.nextString());
            } else if ("options".equals(contactField) && reader.peek() == JsonToken.BEGIN_ARRAY) {
                reader.beginArray();
                while (reader.hasNext()) {
                    PersonFieldOption option = new PersonFieldOption();
                    reader.beginObject();
                    while (reader.hasNext()) {
                        contactField = reader.nextName();
                        if (PipeSQLiteHelper.COLUMN_PIPEDRIVE_ID.equals(contactField) && reader.peek() != JsonToken.NULL) {
                            option.setId(reader.nextInt());
                        } else if (!PlusShare.KEY_CALL_TO_ACTION_LABEL.equals(contactField) || reader.peek() == JsonToken.NULL) {
                            reader.skipValue();
                        } else {
                            option.setLabel(reader.nextString());
                        }
                    }
                    field.addOption(option);
                    reader.endObject();
                }
                reader.endArray();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return field;
    }
}
