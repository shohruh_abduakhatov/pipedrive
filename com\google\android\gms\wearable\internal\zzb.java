package com.google.android.gms.wearable.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzrr;

final class zzb<T> extends zzi<Status> {
    private zzrr<T> Bt;
    private zza<T> aSN;
    private T mListener;

    interface zza<T> {
        void zza(zzbp com_google_android_gms_wearable_internal_zzbp, com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, T t, zzrr<T> com_google_android_gms_internal_zzrr_T) throws RemoteException;
    }

    private zzb(GoogleApiClient googleApiClient, T t, zzrr<T> com_google_android_gms_internal_zzrr_T, zza<T> com_google_android_gms_wearable_internal_zzb_zza_T) {
        super(googleApiClient);
        this.mListener = zzaa.zzy(t);
        this.Bt = (zzrr) zzaa.zzy(com_google_android_gms_internal_zzrr_T);
        this.aSN = (zza) zzaa.zzy(com_google_android_gms_wearable_internal_zzb_zza_T);
    }

    static <T> PendingResult<Status> zza(GoogleApiClient googleApiClient, zza<T> com_google_android_gms_wearable_internal_zzb_zza_T, T t) {
        return googleApiClient.zza(new zzb(googleApiClient, t, googleApiClient.zzs(t), com_google_android_gms_wearable_internal_zzb_zza_T));
    }

    protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
        this.aSN.zza(com_google_android_gms_wearable_internal_zzbp, this, this.mListener, this.Bt);
        this.mListener = null;
        this.Bt = null;
    }

    protected Status zzb(Status status) {
        this.mListener = null;
        this.Bt = null;
        return status;
    }

    protected /* synthetic */ Result zzc(Status status) {
        return zzb(status);
    }
}
