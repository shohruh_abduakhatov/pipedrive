package com.pipedrive.views.viewholder.flow;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class NoteRowViewHolder_ViewBinding implements Unbinder {
    private NoteRowViewHolder target;

    @UiThread
    public NoteRowViewHolder_ViewBinding(NoteRowViewHolder target, View source) {
        this.target = target;
        target.mNote = (TextView) Utils.findRequiredViewAsType(source, R.id.note, "field 'mNote'", TextView.class);
        target.mDetails = (TextView) Utils.findRequiredViewAsType(source, R.id.details, "field 'mDetails'", TextView.class);
    }

    @CallSuper
    public void unbind() {
        NoteRowViewHolder target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mNote = null;
        target.mDetails = null;
    }
}
