package com.pipedrive.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.maps.android.heatmaps.WeightedLatLng;
import com.pipedrive.util.networking.entities.currency.CurrencyEntity;

public class Currency extends BaseDatasourceEntity implements Parcelable {
    public static final Creator CREATOR = new Creator() {
        public Currency createFromParcel(Parcel in) {
            return new Currency(in);
        }

        public Currency[] newArray(int size) {
            return new Currency[size];
        }
    };
    private String code;
    private int decimalPoints;
    private boolean isActive;
    private boolean isCustom;
    private String name;
    private double rateToDefault;
    private String symbol;

    @NonNull
    public static Currency create(@NonNull String code, @NonNull String name, @Nullable String symbol, @NonNull Integer decimalPoints, @NonNull Boolean isActive, @NonNull Boolean isCustom) {
        Currency currency = new Currency();
        currency.setCode(code);
        currency.setName(name);
        currency.setSymbol(symbol);
        currency.setDecimalPoints(decimalPoints.intValue());
        currency.setActive(isActive.booleanValue());
        currency.setCustom(isCustom.booleanValue());
        return currency;
    }

    public Currency() {
        this.rateToDefault = WeightedLatLng.DEFAULT_INTENSITY;
    }

    private Currency(Parcel in) {
        this.rateToDefault = WeightedLatLng.DEFAULT_INTENSITY;
        readFromParcel(in);
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return this.symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getDecimalPoints() {
        return this.decimalPoints;
    }

    public void setDecimalPoints(int decimalPoints) {
        this.decimalPoints = decimalPoints;
    }

    public double getRateToDefault() {
        return this.rateToDefault;
    }

    public void setRateToDefault(double rateToDefault) {
        this.rateToDefault = rateToDefault;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isCustom() {
        return this.isCustom;
    }

    public void setCustom(boolean isCustom) {
        this.isCustom = isCustom;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        int i;
        int i2 = 1;
        super.writeToParcel(dest, flags);
        dest.writeString(this.code);
        dest.writeString(this.name);
        dest.writeString(this.symbol);
        dest.writeInt(this.decimalPoints);
        dest.writeDouble(this.rateToDefault);
        if (this.isActive) {
            i = 1;
        } else {
            i = 0;
        }
        dest.writeInt(i);
        if (!this.isCustom) {
            i2 = 0;
        }
        dest.writeInt(i2);
    }

    protected void readFromParcel(Parcel in) {
        boolean z;
        boolean z2 = true;
        super.readFromParcel(in);
        this.code = in.readString();
        this.name = in.readString();
        this.symbol = in.readString();
        this.decimalPoints = in.readInt();
        this.rateToDefault = in.readDouble();
        if (in.readInt() == 1) {
            z = true;
        } else {
            z = false;
        }
        this.isActive = z;
        if (in.readInt() != 1) {
            z2 = false;
        }
        this.isCustom = z2;
    }

    public String toString() {
        return "Currency{code='" + this.code + '\'' + ", name='" + this.name + '\'' + ", symbol='" + this.symbol + '\'' + ", decimalPoints=" + this.decimalPoints + ", rateToDefault=" + this.rateToDefault + ", isActive=" + this.isActive + ", isCustom=" + this.isCustom + '}';
    }

    public boolean equals(Object o) {
        boolean z = true;
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass() || !super.equals(o)) {
            return false;
        }
        Currency currency = (Currency) o;
        if (this.decimalPoints != currency.decimalPoints || Double.compare(currency.rateToDefault, this.rateToDefault) != 0 || this.isActive != currency.isActive || this.isCustom != currency.isCustom || !this.code.equals(currency.code) || !this.name.equals(currency.name)) {
            return false;
        }
        if (this.symbol != null) {
            z = this.symbol.equals(currency.symbol);
        } else if (currency.symbol != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int hashCode;
        int i = 1;
        int hashCode2 = ((((super.hashCode() * 31) + this.code.hashCode()) * 31) + this.name.hashCode()) * 31;
        if (this.symbol != null) {
            hashCode = this.symbol.hashCode();
        } else {
            hashCode = 0;
        }
        int result = ((hashCode2 + hashCode) * 31) + this.decimalPoints;
        long temp = Double.doubleToLongBits(this.rateToDefault);
        hashCode2 = ((result * 31) + ((int) ((temp >>> 32) ^ temp))) * 31;
        if (this.isActive) {
            hashCode = 1;
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 31;
        if (!this.isCustom) {
            i = 0;
        }
        return hashCode + i;
    }

    @Nullable
    public static Currency fromEntity(@NonNull CurrencyEntity currencyEntity) {
        if (currencyEntity.getPipedriveId() == null || currencyEntity.getCode() == null || currencyEntity.getName() == null || currencyEntity.getSymbol() == null || currencyEntity.getDecimalPoints() == null || currencyEntity.isActive() == null || currencyEntity.isCustom() == null) {
            return null;
        }
        Currency currency = new Currency();
        currency.setPipedriveId(currencyEntity.getPipedriveId().intValue());
        currency.setCode(currencyEntity.getCode());
        currency.setName(currencyEntity.getName());
        currency.setSymbol(currencyEntity.getSymbol());
        currency.setDecimalPoints(currencyEntity.getDecimalPoints().intValue());
        currency.setActive(currencyEntity.isActive().booleanValue());
        currency.setCustom(currencyEntity.isCustom().booleanValue());
        return currency;
    }
}
