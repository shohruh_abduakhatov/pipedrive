package com.pipedrive.util.networking.client;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.util.Hmac;
import io.fabric.sdk.android.services.network.HttpRequest.Base64;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.HttpUrl;
import okhttp3.Interceptor.Chain;
import okhttp3.Request;
import okhttp3.Response;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;

public final class MobileSignatureInterceptor extends HeaderInterceptor {
    private static final String DEFAULT_SHARED_SECRET_KEY = "42dfaZ93x9532c9ad8e83cdxf9fc7S87x27fb8xd";
    private static final String PARAM_API_TOKEN = "api_token";
    private static final String PARAM_ENDPOINT = "endpoint";

    public /* bridge */ /* synthetic */ Response intercept(Chain chain) throws IOException {
        return super.intercept(chain);
    }

    @NonNull
    String getHeaderName() {
        return "X-Pipedrive-Mobile-Signature";
    }

    @Nullable
    String getHeaderValue(@NonNull Request request) {
        return getHeaderValue(request, DEFAULT_SHARED_SECRET_KEY, TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
    }

    @Nullable
    String getHeaderValue(@NonNull Request request, @NonNull Long timeSeconds) {
        return getHeaderValue(request, DEFAULT_SHARED_SECRET_KEY, timeSeconds.longValue());
    }

    @Nullable
    String getHeaderValue(@NonNull Request request, @Nullable String sharedSecretKey) {
        if (sharedSecretKey == null) {
            sharedSecretKey = DEFAULT_SHARED_SECRET_KEY;
        }
        return getHeaderValue(request, sharedSecretKey, TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
    }

    @Nullable
    final String getHeaderValue(@NonNull Request request, @NonNull String sharedSecretKey, final long timeSeconds) {
        String apiToken = request.url().queryParameter(PARAM_API_TOKEN);
        if (apiToken == null) {
            return null;
        }
        HttpUrl httpUrl = request.url();
        final String key = apiToken.concat(sharedSecretKey);
        return (String) endpointFromUrl(httpUrl).concatWith(queryParamsFromUrl(httpUrl)).filter(new Func1<String, Boolean>() {
            public Boolean call(String s) {
                return Boolean.valueOf(!s.startsWith(MobileSignatureInterceptor.PARAM_API_TOKEN));
            }
        }).sorted().concatWith(Observable.just(String.valueOf(Long.valueOf(timeSeconds)))).reduce(new Func2<String, String, String>() {
            public String call(String s, String s2) {
                return s.concat(s2);
            }
        }).map(new Func1<String, String>() {
            public String call(String s) {
                return Hmac.md5(key, s);
            }
        }).map(new Func1<String, String>() {
            public String call(String s) {
                return s.concat(Long.toHexString(timeSeconds));
            }
        }).map(new Func1<String, String>() {
            public String call(String s) {
                return Base64.encode(s);
            }
        }).toBlocking().single();
    }

    @NonNull
    String urlEncode(@NonNull String s) {
        return Uri.encode(s);
    }

    @NonNull
    private Observable<String> endpointFromUrl(@NonNull HttpUrl httpUrl) {
        return Observable.just(httpUrl).map(new Func1<HttpUrl, String>() {
            public String call(HttpUrl httpUrl) {
                return String.format("%s=%s", new Object[]{MobileSignatureInterceptor.PARAM_ENDPOINT, MobileSignatureInterceptor.this.urlEncode(httpUrl.encodedPath())});
            }
        });
    }

    @NonNull
    private Observable<String> queryParamsFromUrl(@NonNull final HttpUrl httpUrl) {
        return Observable.from(httpUrl.queryParameterNames()).map(new Func1<String, String>() {
            public String call(String paramName) {
                return String.format("%s=%s", new Object[]{paramName, MobileSignatureInterceptor.this.urlEncode(httpUrl.queryParameter(paramName))});
            }
        });
    }
}
