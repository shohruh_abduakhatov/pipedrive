package com.zendesk.sdk.deeplinking;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.newrelic.agent.android.util.SafeJsonPrimitive;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.helpcenter.SimpleArticle;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.support.ViewArticleActivity;
import com.zendesk.util.CollectionUtils;
import com.zendesk.util.StringUtils;
import java.util.ArrayList;
import java.util.List;
import okhttp3.HttpUrl;

public class ZendeskDeepLinkingParser {
    private static final String LOG_TAG = "ZendeskDeepLinkingParser";
    private final List<ZendeskDeepLinkParserModule> mParserModuleList = new ArrayList();

    interface ZendeskDeepLinkParserModule {
        @Nullable
        Intent parse(@NonNull String str, @NonNull Context context);
    }

    static class DefaultParser implements ZendeskDeepLinkParserModule {
        DefaultParser() {
        }

        @Nullable
        public Intent parse(@NonNull String url, @NonNull Context context) {
            if (!StringUtils.hasLength(url)) {
                return null;
            }
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse(url));
            return intent;
        }
    }

    static class HelpCenterParser implements ZendeskDeepLinkParserModule {
        private static final String HC_PATH_ELEMENT_ARTICLE = "articles";
        private static final String HC_PATH_ELEMENT_HC = "hc";
        private static final String HC_PATH_ELEMENT_NAME_SEPARATOR = "-";

        HelpCenterParser() {
        }

        @Nullable
        public Intent parse(@NonNull String url, @NonNull Context context) {
            String zendeskUrl = ZendeskConfig.INSTANCE.getZendeskUrl();
            if (StringUtils.hasLength(zendeskUrl)) {
                HttpUrl zendeskHttpUrl = HttpUrl.parse(zendeskUrl);
                HttpUrl linkHttpUrl = HttpUrl.parse(url);
                if (linkHttpUrl == null || zendeskHttpUrl == null || !zendeskHttpUrl.host().equals(linkHttpUrl.host())) {
                    return null;
                }
                List<String> pathSegments = linkHttpUrl.pathSegments();
                if (pathSegments.size() < 4 && pathSegments.size() > 5) {
                    return null;
                }
                int articleKeywordPathIndex = pathSegments.indexOf(HC_PATH_ELEMENT_ARTICLE);
                if (!HC_PATH_ELEMENT_HC.equals(pathSegments.get(0))) {
                    return null;
                }
                if ((articleKeywordPathIndex != 1 && articleKeywordPathIndex != 2) || articleKeywordPathIndex + 2 != pathSegments.size()) {
                    return null;
                }
                SimpleArticle articleInformation = extractArticleId((String) pathSegments.get(articleKeywordPathIndex + 1));
                if (articleInformation == null) {
                    return null;
                }
                Intent intent = new Intent(context, ViewArticleActivity.class);
                intent.putExtra(ViewArticleActivity.EXTRA_SIMPLE_ARTICLE, articleInformation);
                return intent;
            }
            Logger.w(ZendeskDeepLinkingParser.LOG_TAG, "Zendesk not initialised.", new Object[0]);
            return null;
        }

        private SimpleArticle extractArticleId(String pathSegment) {
            String[] split = pathSegment.split(HC_PATH_ELEMENT_NAME_SEPARATOR);
            if (!CollectionUtils.isNotEmpty(split)) {
                return null;
            }
            try {
                Long articleId = Long.valueOf(Long.parseLong(split[0]));
                String articleName = "";
                StringBuilder articleNameBuilder = new StringBuilder(pathSegment.length());
                if (split.length > 1) {
                    int len = split.length;
                    for (int i = 1; i < len; i++) {
                        articleNameBuilder.append(split[i]);
                        articleNameBuilder.append(SafeJsonPrimitive.NULL_CHAR);
                    }
                    articleName = articleNameBuilder.toString().trim();
                }
                return new SimpleArticle(articleId, articleName);
            } catch (NumberFormatException e) {
                return null;
            }
        }
    }

    static class HttpParser implements ZendeskDeepLinkParserModule {
        HttpParser() {
        }

        @Nullable
        public Intent parse(@NonNull String url, @NonNull Context context) {
            if (!StringUtils.hasLength(url) || HttpUrl.parse(url) == null) {
                return null;
            }
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse(url));
            return intent;
        }
    }

    static class MailParser implements ZendeskDeepLinkParserModule {
        private static final String MAIL_PREFIX = "mailto:";

        MailParser() {
        }

        @Nullable
        public Intent parse(@NonNull String url, @NonNull Context context) {
            if (!StringUtils.hasLength(url) || !url.startsWith(MAIL_PREFIX)) {
                return null;
            }
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse(url));
            return intent;
        }
    }

    public ZendeskDeepLinkingParser() {
        this.mParserModuleList.add(new HelpCenterParser());
        this.mParserModuleList.add(new MailParser());
        this.mParserModuleList.add(new HttpParser());
        this.mParserModuleList.add(new DefaultParser());
    }

    @Nullable
    public Intent parse(@NonNull String url, @NonNull Context context) {
        if (!StringUtils.hasLength(url) || context == null) {
            return null;
        }
        for (ZendeskDeepLinkParserModule parserModule : this.mParserModuleList) {
            Intent parse = parserModule.parse(url, context);
            if (parse != null) {
                return parse;
            }
        }
        return null;
    }
}
