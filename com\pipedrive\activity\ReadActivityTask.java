package com.pipedrive.activity;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.model.Activity;
import com.pipedrive.tasks.AsyncTask;

public class ReadActivityTask extends AsyncTask<Long, Void, Activity> {
    @Nullable
    private OnTaskFinished mOnTaskFinished;

    @MainThread
    public interface OnTaskFinished {
        void onActivityRead(@Nullable Activity activity);
    }

    public ReadActivityTask(@NonNull Session session, @Nullable OnTaskFinished onTaskFinished) {
        super(session);
        this.mOnTaskFinished = onTaskFinished;
    }

    protected Activity doInBackground(Long... params) {
        boolean activitySqlIdNotKnown;
        if (params.length == 0 || params[0] == null) {
            activitySqlIdNotKnown = true;
        } else {
            activitySqlIdNotKnown = false;
        }
        return activitySqlIdNotKnown ? null : (Activity) new ActivitiesDataSource(getSession()).findBySqlId(params[0].longValue());
    }

    protected void onPostExecute(@Nullable Activity activity) {
        if (this.mOnTaskFinished != null) {
            this.mOnTaskFinished.onActivityRead(activity);
        }
    }
}
