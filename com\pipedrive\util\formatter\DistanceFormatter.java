package com.pipedrive.util.formatter;

import android.os.Build.VERSION;
import android.os.LocaleList;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.maps.android.heatmaps.WeightedLatLng;
import com.pipedrive.nearby.util.Distance;
import java.util.Locale;

public enum DistanceFormatter {
    ;

    @NonNull
    private static Boolean useImperialUnit() {
        String countryOfDefaultLocale;
        boolean z = false;
        if (VERSION.SDK_INT >= 24) {
            countryOfDefaultLocale = LocaleList.getDefault().get(0).getCountry();
        } else {
            countryOfDefaultLocale = Locale.getDefault().getCountry();
        }
        if (Locale.US.getCountry().equals(countryOfDefaultLocale) || Locale.UK.getCountry().equals(countryOfDefaultLocale)) {
            z = true;
        }
        return Boolean.valueOf(z);
    }

    @NonNull
    public static String formatDistance(@Nullable Double distanceMeters) {
        if (distanceMeters == null) {
            return "";
        }
        if (useImperialUnit().booleanValue()) {
            return String.format(Locale.getDefault(), "%.1f mi", new Object[]{Distance.METERS.toMiles(distanceMeters)});
        } else if (distanceMeters.doubleValue() > Distance.KILOMETERS.toMeters(Double.valueOf(WeightedLatLng.DEFAULT_INTENSITY)).doubleValue()) {
            return String.format(Locale.getDefault(), "%.1f km", new Object[]{Distance.METERS.toKilometers(distanceMeters)});
        } else {
            return String.format(Locale.getDefault(), "%.1f m", new Object[]{distanceMeters});
        }
    }
}
