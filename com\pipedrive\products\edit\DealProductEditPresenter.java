package com.pipedrive.products.edit;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.model.products.DealProduct;
import com.pipedrive.model.products.ProductVariation;
import com.pipedrive.presenter.PersistablePresenter;

abstract class DealProductEditPresenter extends PersistablePresenter<DealProductEditView> {
    abstract boolean canDeleteDealProduct();

    abstract void changeDealProduct();

    abstract boolean changesAreMadeToDealProduct();

    abstract void deleteDealProduct();

    @Nullable
    abstract DealProduct getCurrentlyManagedDealProduct();

    abstract void requestExistingDealProduct(@NonNull Long l);

    abstract void requestNewDealProduct(@NonNull Long l, @NonNull Long l2);

    abstract void updateDealProductWithEnteredData(@NonNull Double d, @NonNull Double d2, @Nullable ProductVariation productVariation, @NonNull Double d3, @Nullable Double d4);

    abstract void updateDealProductsVariation(@Nullable Long l);

    DealProductEditPresenter(@NonNull Session session, @Nullable Bundle savedState) {
        super(session, savedState);
    }
}
