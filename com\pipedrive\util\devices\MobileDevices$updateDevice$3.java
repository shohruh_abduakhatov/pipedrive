package com.pipedrive.util.devices;

import com.pipedrive.logging.LogJourno.EVENT;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import rx.functions.Action1;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "it", "", "kotlin.jvm.PlatformType", "call"}, k = 3, mv = {1, 1, 6})
/* compiled from: MobileDevices.kt */
final class MobileDevices$updateDevice$3<T> implements Action1<Throwable> {
    final /* synthetic */ MobileDevices this$0;

    MobileDevices$updateDevice$3(MobileDevices mobileDevices) {
        this.this$0 = mobileDevices;
    }

    public final void call(Throwable it) {
        MobileDevices mobileDevices = this.this$0;
        EVENT event = EVENT.MOBILE_DEVICES_UPDATE_DEVICE_ERROR;
        Intrinsics.checkExpressionValueIsNotNull(it, "it");
        mobileDevices.logErrorToNewRelic(event, it);
    }
}
