package com.google.android.gms.wearable.internal;

import android.content.IntentFilter;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.os.ParcelFileDescriptor.AutoCloseInputStream;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzrr;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataApi.DataItemResult;
import com.google.android.gms.wearable.DataApi.DataListener;
import com.google.android.gms.wearable.DataApi.DeleteDataItemsResult;
import com.google.android.gms.wearable.DataApi.GetFdForAssetResult;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataItemAsset;
import com.google.android.gms.wearable.DataItemBuffer;
import com.google.android.gms.wearable.PutDataRequest;
import java.io.IOException;
import java.io.InputStream;

public final class zzx implements DataApi {

    class AnonymousClass8 implements zza<DataListener> {
        final /* synthetic */ IntentFilter[] aTc;

        AnonymousClass8(IntentFilter[] intentFilterArr) {
            this.aTc = intentFilterArr;
        }

        public void zza(zzbp com_google_android_gms_wearable_internal_zzbp, com.google.android.gms.internal.zzqo.zzb<Status> com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, DataListener dataListener, zzrr<DataListener> com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_DataApi_DataListener) throws RemoteException {
            com_google_android_gms_wearable_internal_zzbp.zza((com.google.android.gms.internal.zzqo.zzb) com_google_android_gms_internal_zzqo_zzb_com_google_android_gms_common_api_Status, dataListener, (zzrr) com_google_android_gms_internal_zzrr_com_google_android_gms_wearable_DataApi_DataListener, this.aTc);
        }
    }

    public static class zza implements DataItemResult {
        private final DataItem aTI;
        private final Status hv;

        public zza(Status status, DataItem dataItem) {
            this.hv = status;
            this.aTI = dataItem;
        }

        public DataItem getDataItem() {
            return this.aTI;
        }

        public Status getStatus() {
            return this.hv;
        }
    }

    public static class zzb implements DeleteDataItemsResult {
        private final int aTJ;
        private final Status hv;

        public zzb(Status status, int i) {
            this.hv = status;
            this.aTJ = i;
        }

        public int getNumDeleted() {
            return this.aTJ;
        }

        public Status getStatus() {
            return this.hv;
        }
    }

    public static class zzc implements GetFdForAssetResult {
        private volatile ParcelFileDescriptor aTK;
        private volatile InputStream aTu;
        private final Status hv;
        private volatile boolean mClosed = false;

        public zzc(Status status, ParcelFileDescriptor parcelFileDescriptor) {
            this.hv = status;
            this.aTK = parcelFileDescriptor;
        }

        public ParcelFileDescriptor getFd() {
            if (!this.mClosed) {
                return this.aTK;
            }
            throw new IllegalStateException("Cannot access the file descriptor after release().");
        }

        public InputStream getInputStream() {
            if (this.mClosed) {
                throw new IllegalStateException("Cannot access the input stream after release().");
            } else if (this.aTK == null) {
                return null;
            } else {
                if (this.aTu == null) {
                    this.aTu = new AutoCloseInputStream(this.aTK);
                }
                return this.aTu;
            }
        }

        public Status getStatus() {
            return this.hv;
        }

        public void release() {
            if (this.aTK != null) {
                if (this.mClosed) {
                    throw new IllegalStateException("releasing an already released result.");
                }
                try {
                    if (this.aTu != null) {
                        this.aTu.close();
                    } else {
                        this.aTK.close();
                    }
                    this.mClosed = true;
                    this.aTK = null;
                    this.aTu = null;
                } catch (IOException e) {
                }
            }
        }
    }

    private PendingResult<Status> zza(GoogleApiClient googleApiClient, DataListener dataListener, IntentFilter[] intentFilterArr) {
        return zzb.zza(googleApiClient, zza(intentFilterArr), dataListener);
    }

    private static zza<DataListener> zza(IntentFilter[] intentFilterArr) {
        return new AnonymousClass8(intentFilterArr);
    }

    private void zza(Asset asset) {
        if (asset == null) {
            throw new IllegalArgumentException("asset is null");
        } else if (asset.getDigest() == null) {
            throw new IllegalArgumentException("invalid asset");
        } else if (asset.getData() != null) {
            throw new IllegalArgumentException("invalid asset");
        }
    }

    public PendingResult<Status> addListener(GoogleApiClient googleApiClient, DataListener dataListener) {
        return zza(googleApiClient, dataListener, new IntentFilter[]{zzbn.zzrp(DataApi.ACTION_DATA_CHANGED)});
    }

    public PendingResult<Status> addListener(GoogleApiClient googleApiClient, DataListener dataListener, Uri uri, int i) {
        zzaa.zzb(uri != null, (Object) "uri must not be null");
        boolean z = i == 0 || i == 1;
        zzaa.zzb(z, (Object) "invalid filter type");
        return zza(googleApiClient, dataListener, new IntentFilter[]{zzbn.zza(DataApi.ACTION_DATA_CHANGED, uri, i)});
    }

    public PendingResult<DeleteDataItemsResult> deleteDataItems(GoogleApiClient googleApiClient, Uri uri) {
        return deleteDataItems(googleApiClient, uri, 0);
    }

    public PendingResult<DeleteDataItemsResult> deleteDataItems(GoogleApiClient googleApiClient, final Uri uri, final int i) {
        boolean z = false;
        zzaa.zzb(uri != null, (Object) "uri must not be null");
        if (i == 0 || i == 1) {
            z = true;
        }
        zzaa.zzb(z, (Object) "invalid filter type");
        return googleApiClient.zza(new zzi<DeleteDataItemsResult>(this, googleApiClient) {
            final /* synthetic */ zzx aTD;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zzb(this, uri, i);
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzew(status);
            }

            protected DeleteDataItemsResult zzew(Status status) {
                return new zzb(status, 0);
            }
        });
    }

    public PendingResult<DataItemResult> getDataItem(GoogleApiClient googleApiClient, final Uri uri) {
        return googleApiClient.zza(new zzi<DataItemResult>(this, googleApiClient) {
            final /* synthetic */ zzx aTD;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zza((com.google.android.gms.internal.zzqo.zzb) this, uri);
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzeu(status);
            }

            protected DataItemResult zzeu(Status status) {
                return new zza(status, null);
            }
        });
    }

    public PendingResult<DataItemBuffer> getDataItems(GoogleApiClient googleApiClient) {
        return googleApiClient.zza(new zzi<DataItemBuffer>(this, googleApiClient) {
            final /* synthetic */ zzx aTD;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zzx(this);
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzev(status);
            }

            protected DataItemBuffer zzev(Status status) {
                return new DataItemBuffer(DataHolder.zzgb(status.getStatusCode()));
            }
        });
    }

    public PendingResult<DataItemBuffer> getDataItems(GoogleApiClient googleApiClient, Uri uri) {
        return getDataItems(googleApiClient, uri, 0);
    }

    public PendingResult<DataItemBuffer> getDataItems(GoogleApiClient googleApiClient, final Uri uri, final int i) {
        boolean z = false;
        zzaa.zzb(uri != null, (Object) "uri must not be null");
        if (i == 0 || i == 1) {
            z = true;
        }
        zzaa.zzb(z, (Object) "invalid filter type");
        return googleApiClient.zza(new zzi<DataItemBuffer>(this, googleApiClient) {
            final /* synthetic */ zzx aTD;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zza((com.google.android.gms.internal.zzqo.zzb) this, uri, i);
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzev(status);
            }

            protected DataItemBuffer zzev(Status status) {
                return new DataItemBuffer(DataHolder.zzgb(status.getStatusCode()));
            }
        });
    }

    public PendingResult<GetFdForAssetResult> getFdForAsset(GoogleApiClient googleApiClient, final Asset asset) {
        zza(asset);
        return googleApiClient.zza(new zzi<GetFdForAssetResult>(this, googleApiClient) {
            final /* synthetic */ zzx aTD;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zza((com.google.android.gms.internal.zzqo.zzb) this, asset);
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzex(status);
            }

            protected GetFdForAssetResult zzex(Status status) {
                return new zzc(status, null);
            }
        });
    }

    public PendingResult<GetFdForAssetResult> getFdForAsset(GoogleApiClient googleApiClient, final DataItemAsset dataItemAsset) {
        return googleApiClient.zza(new zzi<GetFdForAssetResult>(this, googleApiClient) {
            final /* synthetic */ zzx aTD;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zza((com.google.android.gms.internal.zzqo.zzb) this, dataItemAsset);
            }

            protected /* synthetic */ Result zzc(Status status) {
                return zzex(status);
            }

            protected GetFdForAssetResult zzex(Status status) {
                return new zzc(status, null);
            }
        });
    }

    public PendingResult<DataItemResult> putDataItem(GoogleApiClient googleApiClient, final PutDataRequest putDataRequest) {
        return googleApiClient.zza(new zzi<DataItemResult>(this, googleApiClient) {
            final /* synthetic */ zzx aTD;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zza((com.google.android.gms.internal.zzqo.zzb) this, putDataRequest);
            }

            public /* synthetic */ Result zzc(Status status) {
                return zzeu(status);
            }

            public DataItemResult zzeu(Status status) {
                return new zza(status, null);
            }
        });
    }

    public PendingResult<Status> removeListener(GoogleApiClient googleApiClient, final DataListener dataListener) {
        return googleApiClient.zza(new zzi<Status>(this, googleApiClient) {
            final /* synthetic */ zzx aTD;

            protected void zza(zzbp com_google_android_gms_wearable_internal_zzbp) throws RemoteException {
                com_google_android_gms_wearable_internal_zzbp.zza((com.google.android.gms.internal.zzqo.zzb) this, dataListener);
            }

            public Status zzb(Status status) {
                return status;
            }

            public /* synthetic */ Result zzc(Status status) {
                return zzb(status);
            }
        });
    }
}
