package com.zendesk.sdk.ui;

public enum LoadingState {
    LOADING,
    DISPLAYING,
    ERRORED
}
