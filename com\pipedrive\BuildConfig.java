package com.pipedrive;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.pipedrive";
    public static final String APP_VERSION_NAME = "5.2";
    public static final boolean BUILD_GOOGLE_PLAY = true;
    public static final boolean BUILD_HOCKEYAPP = false;
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "prod";
    public static final String OVERRIDE_API_HOSTNAME = null;
    public static final int VERSION_CODE = 251;
    public static final String VERSION_NAME = "";
    public static final int WHATS_NEW_SCREENS_VERSION = 6;
}
