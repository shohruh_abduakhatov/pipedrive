package com.zendesk.sdk.model;

import com.zendesk.sdk.model.access.AccessToken;
import com.zendesk.sdk.model.settings.SafeMobileSettings;
import java.util.Locale;

public class SdkConfiguration {
    private static final String BEARER_FORMAT = "Bearer %s";
    private final AccessToken mAccessToken;
    private final SafeMobileSettings mMobileSettings;

    public SdkConfiguration(AccessToken accessToken, SafeMobileSettings mobileSettings) {
        this.mAccessToken = accessToken;
        this.mMobileSettings = mobileSettings;
    }

    public AccessToken getAccessToken() {
        return this.mAccessToken;
    }

    public SafeMobileSettings getMobileSettings() {
        return this.mMobileSettings;
    }

    public String getBearerAuthorizationHeader() {
        Locale locale = Locale.US;
        String str = BEARER_FORMAT;
        Object[] objArr = new Object[1];
        objArr[0] = this.mAccessToken == null ? null : this.mAccessToken.getAccessToken();
        return String.format(locale, str, objArr);
    }
}
