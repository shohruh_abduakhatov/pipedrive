package com.zendesk.sdk.network.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.zendesk.sdk.model.SdkConfiguration;
import com.zendesk.sdk.model.request.UploadResponse;
import com.zendesk.sdk.model.request.UploadResponseWrapper;
import com.zendesk.sdk.network.BaseProvider;
import com.zendesk.sdk.network.UploadProvider;
import com.zendesk.service.ZendeskCallback;
import java.io.File;

class ZendeskUploadProvider implements UploadProvider {
    private static final String LOG_TAG = "ZendeskUploadProvider";
    private final BaseProvider baseProvider;
    private final ZendeskUploadService uploadService;

    ZendeskUploadProvider(BaseProvider baseProvider, ZendeskUploadService uploadService) {
        this.baseProvider = baseProvider;
        this.uploadService = uploadService;
    }

    public void uploadAttachment(@NonNull String fileName, @NonNull File file, @NonNull String mimeType, @Nullable ZendeskCallback<UploadResponse> callback) {
        final String str = fileName;
        final File file2 = file;
        final String str2 = mimeType;
        final ZendeskCallback<UploadResponse> zendeskCallback = callback;
        this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
            public void onSuccess(SdkConfiguration config) {
                ZendeskUploadProvider.this.uploadService.uploadAttachment(config.getBearerAuthorizationHeader(), str, file2, str2, new PassThroughErrorZendeskCallback<UploadResponseWrapper>(zendeskCallback) {
                    public void onSuccess(UploadResponseWrapper result) {
                        if (zendeskCallback != null) {
                            zendeskCallback.onSuccess(result.getUpload());
                        }
                    }
                });
            }
        });
    }

    public void deleteAttachment(@NonNull final String token, @Nullable final ZendeskCallback<Void> callback) {
        this.baseProvider.configureSdk(new PassThroughErrorZendeskCallback<SdkConfiguration>(callback) {
            public void onSuccess(SdkConfiguration sdkConfiguration) {
                ZendeskUploadProvider.this.uploadService.deleteAttachment(sdkConfiguration.getBearerAuthorizationHeader(), token, new PassThroughErrorZendeskCallback<Void>(callback) {
                    public void onSuccess(Void result) {
                        if (callback != null) {
                            callback.onSuccess(result);
                        }
                    }
                });
            }
        });
    }
}
