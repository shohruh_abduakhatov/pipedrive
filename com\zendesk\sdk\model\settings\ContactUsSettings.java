package com.zendesk.sdk.model.settings;

import android.support.annotation.NonNull;
import com.zendesk.util.CollectionUtils;
import java.io.Serializable;
import java.util.List;

public class ContactUsSettings implements Serializable {
    private List<String> tags;

    @NonNull
    public List<String> getTags() {
        return CollectionUtils.copyOf(this.tags);
    }
}
