package com.zendesk.sdk.network.impl;

import com.zendesk.sdk.network.SdkOptions;
import com.zendesk.sdk.network.SdkOptions.ServiceOptions;
import java.util.Collections;
import java.util.List;
import okhttp3.ConnectionSpec;

public class DefaultSdkOptions implements SdkOptions {
    public boolean overrideResourceLoadingInWebview() {
        return false;
    }

    public ServiceOptions getServiceOptions() {
        return new ServiceOptions() {
            public List<ConnectionSpec> getConnectionSpecs() {
                return Collections.singletonList(ConnectionSpec.MODERN_TLS);
            }
        };
    }
}
