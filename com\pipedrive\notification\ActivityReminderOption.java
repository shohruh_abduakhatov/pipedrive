package com.pipedrive.notification;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import com.pipedrive.R;
import com.pipedrive.datetime.PipedriveDateTime;
import com.pipedrive.model.Activity;
import java.util.concurrent.TimeUnit;

public enum ActivityReminderOption implements ReminderOption {
    DONT_REMIND(null, R.string.activity.reminder.none),
    REMIND_AT_TIME_OF_EVENT(Long.valueOf(0), R.string.activity.reminder.at_time_of_the_event),
    REMIND_5_MINUTES_BEFORE(Long.valueOf(TimeUnit.MINUTES.toMillis(5)), R.string.activity.reminder.5_minutes_before),
    REMIND_10_MINUTES_BEFORE(Long.valueOf(TimeUnit.MINUTES.toMillis(10)), R.string.activity.reminder.10_minutes_before),
    REMIND_15_MINUTES_BEFORE(Long.valueOf(TimeUnit.MINUTES.toMillis(15)), R.string.activity.reminder.15_minutes_before),
    REMIND_30_MINUTES_BEFORE(Long.valueOf(TimeUnit.MINUTES.toMillis(30)), R.string.activity.reminder.30_minutes_before),
    REMIND_1_HOUR_BEFORE(Long.valueOf(TimeUnit.HOURS.toMillis(1)), R.string.activity.reminder.1_hour_before),
    REMIND_2_HOURS_BEFORE(Long.valueOf(TimeUnit.HOURS.toMillis(2)), R.string.activity.reminder.2_hours_before),
    REMIND_1_DAY_BEFORE(Long.valueOf(TimeUnit.DAYS.toMillis(1)), R.string.activity.reminder.1_day_before),
    REMIND_2_DAYS_BEFORE(Long.valueOf(TimeUnit.DAYS.toMillis(2)), R.string.activity.reminder.2_days_before);
    
    public static final ActivityReminderOption DEFAULT = null;
    @Nullable
    private final Long delta;
    @StringRes
    private final int titleResId;

    static {
        DEFAULT = REMIND_10_MINUTES_BEFORE;
    }

    private ActivityReminderOption(@Nullable Long delta, @StringRes int titleResId) {
        this.delta = delta;
        this.titleResId = titleResId;
    }

    @NonNull
    public String getTitle(@NonNull Context context) {
        return context.getString(this.titleResId);
    }

    @Nullable
    public PipedriveDateTime getRemindDateTime(@Nullable Activity activity) {
        if (this.delta == null || activity == null || activity.getActivityStartAsDateTime() == null) {
            return null;
        }
        return PipedriveDateTime.instanceFromUnixTimeRepresentation(Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(activity.getActivityStartAsDateTime().getTime() - this.delta.longValue())));
    }
}
