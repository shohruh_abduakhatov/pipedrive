package com.pipedrive.nearby.cards.cards;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.ViewGroup;
import com.pipedrive.analytics.Analytics;
import com.pipedrive.analytics.AnalyticsEvent;
import com.pipedrive.model.Deal;
import com.pipedrive.model.Person;
import com.pipedrive.model.contact.CommunicationMedium;
import com.pipedrive.nearby.model.DealNearbyItem;

public class SingleDealCard extends SingleCardWithCommunicationButtons<DealNearbyItem, Deal> {
    public SingleDealCard(@NonNull ViewGroup parent, @LayoutRes @NonNull Integer layoutResId) {
        super(parent, layoutResId);
    }

    @NonNull
    CardHeader<DealNearbyItem, Deal> getHeader() {
        return new DealCardHeader();
    }

    void onDetailsRequested(@NonNull Long sqlId) {
        CardEventBus.INSTANCE.onDealDetailsClicked(sqlId);
    }

    @Nullable
    public Person getEntity() {
        return this.entity == null ? null : ((Deal) this.entity).getPerson();
    }

    public void onCallRequested(@NonNull CommunicationMedium medium, @NonNull Person person) {
        super.onCallRequested(medium, person);
        Analytics.sendEvent(AnalyticsEvent.CALL_DEAL);
    }

    public void onSmsRequested(@NonNull CommunicationMedium medium, @NonNull Person person) {
        super.onSmsRequested(medium, person);
        Analytics.sendEvent(AnalyticsEvent.TEXT_MESSAGE_DEAL);
    }

    public void onEmailRequested(@NonNull CommunicationMedium medium, @NonNull Person person) {
        super.onEmailRequested(medium, person);
        Analytics.sendEvent(AnalyticsEvent.EMAIL_DEAL);
    }
}
