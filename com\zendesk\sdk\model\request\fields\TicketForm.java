package com.zendesk.sdk.model.request.fields;

import com.zendesk.util.CollectionUtils;
import java.util.List;

public class TicketForm {
    private String displayName;
    private long id;
    private String name;
    private List<TicketField> ticketFields;

    public static TicketForm create(RawTicketForm tf, List<TicketField> ticketFields) {
        return new TicketForm(tf.getId(), tf.getName(), tf.getDisplayName(), ticketFields);
    }

    public TicketForm(long id, String name, String displayName, List<TicketField> ticketFields) {
        this.id = id;
        this.name = name;
        this.displayName = displayName;
        this.ticketFields = CollectionUtils.copyOf(ticketFields);
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public List<TicketField> getTicketFields() {
        return CollectionUtils.copyOf(this.ticketFields);
    }
}
