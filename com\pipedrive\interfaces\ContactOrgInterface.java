package com.pipedrive.interfaces;

import android.os.Parcelable;
import android.support.annotation.Nullable;
import com.pipedrive.model.contact.Email;
import com.pipedrive.model.contact.Phone;
import java.util.List;
import org.json.JSONArray;

public interface ContactOrgInterface extends Parcelable {
    String getAddress();

    JSONArray getCustomFields();

    String getDropBoxAddress();

    String getEmail();

    List<Email> getEmails();

    String getFirstLetter();

    String getName();

    String getPhone();

    List<Phone> getPhones();

    @Nullable
    Integer getPictureId();

    int getPipedriveId();

    long getSqlId();

    boolean hasPhones();
}
