package com.pipedrive.util.networking;

import android.net.Uri;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.pipedrive.BuildConfig;

class ApiUrlBuilderWithoutSession {
    private static final String API_URL_API_VERSION = "v1";
    private static final String API_URL_DEFAULT_DOMAIN_LABEL = "app";
    private static final String API_URL_HOSTNAME_TEMPLATE = "%s.pipedrive.com";
    private static final String API_URL_SCHEME = "https";
    static final String ENDPOINT_AUTHORIZATIONS = "authorizations";
    static final String ENDPOINT_AUTHORIZATIONS_GOOGLE_PLUS = "authorizations/googleOAuth2";
    private static final String PARAM_STRICT_MODE = "strict_mode";
    @NonNull
    Builder mBuilder = new Builder();

    ApiUrlBuilderWithoutSession(@NonNull ApiUrlBuilderWithoutSession cloneFromApiUrlBuilderWithoutSession) {
        this.mBuilder = cloneFromApiUrlBuilderWithoutSession.mBuilder.build().buildUpon();
    }

    ApiUrlBuilderWithoutSession(@NonNull String requestApiEndpoint) {
        this.mBuilder.scheme(API_URL_SCHEME);
        this.mBuilder.appendPath(API_URL_API_VERSION);
        this.mBuilder.appendEncodedPath(requestApiEndpoint);
        setStrictMode(true);
    }

    @NonNull
    public String build() {
        if (TextUtils.isEmpty(BuildConfig.OVERRIDE_API_HOSTNAME)) {
            String apiUrlDomainLabel = getApiUrlDomainLabel() == null ? "app" : getApiUrlDomainLabel();
            this.mBuilder.authority(String.format(API_URL_HOSTNAME_TEMPLATE, new Object[]{apiUrlDomainLabel}));
        } else {
            this.mBuilder.authority(BuildConfig.OVERRIDE_API_HOSTNAME);
        }
        return this.mBuilder.build().toString();
    }

    @Nullable
    String getApiUrlDomainLabel() {
        return "app";
    }

    private ApiUrlBuilderWithoutSession setStrictMode(boolean setStrictMode) {
        if (this.mBuilder.build().getQueryParameter(PARAM_STRICT_MODE) == null) {
            this.mBuilder.appendQueryParameter(PARAM_STRICT_MODE, setStrictMode ? "1" : "0");
        }
        return this;
    }

    void removeQueryParameter(@NonNull String keyOfQueryParameterToRemove) {
        Uri newUri = this.mBuilder.build();
        Builder newUriWithRemovedQueryParameter = newUri.buildUpon();
        newUriWithRemovedQueryParameter.clearQuery();
        for (String queryParameterName : newUri.getQueryParameterNames()) {
            if (!TextUtils.equals(queryParameterName, keyOfQueryParameterToRemove)) {
                newUriWithRemovedQueryParameter.appendQueryParameter(queryParameterName, newUri.getQueryParameter(queryParameterName));
            }
        }
        this.mBuilder = newUriWithRemovedQueryParameter;
    }

    void appendQueryParameterIfNotPresent(@NonNull String key, @Nullable String value) {
        if (this.mBuilder.build().getQueryParameter(key) == null) {
            appendQueryParameter(key, value);
        }
    }

    void appendQueryParameter(@NonNull String key, @Nullable String value) {
        this.mBuilder.appendQueryParameter(key, value);
    }
}
