package com.pipedrive.util.networking.client;

import android.support.annotation.Nullable;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

interface SecurityProviderInterface {
    @Nullable
    HostnameVerifier getHostnameVerifier();

    @Nullable
    SSLSocketFactory getSslSocketFactory();

    @Nullable
    X509TrustManager getX509TrustManager();

    void initSSLContext();
}
