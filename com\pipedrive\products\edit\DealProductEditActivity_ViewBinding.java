package com.pipedrive.products.edit;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pipedrive.R;
import com.pipedrive.views.edit.products.DealProductDiscountEditText;
import com.pipedrive.views.edit.products.DealProductDurationEditText;
import com.pipedrive.views.edit.products.DealProductPriceEditText;
import com.pipedrive.views.edit.products.DealProductQuantityEditText;
import com.pipedrive.views.edit.products.ProductTotalTextView;
import com.pipedrive.views.edit.products.ProductVariationView;

public class DealProductEditActivity_ViewBinding implements Unbinder {
    private DealProductEditActivity target;
    private View view2131820816;

    @UiThread
    public DealProductEditActivity_ViewBinding(DealProductEditActivity target) {
        this(target, target.getWindow().getDecorView());
    }

    @UiThread
    public DealProductEditActivity_ViewBinding(final DealProductEditActivity target, View source) {
        this.target = target;
        target.mToolbar = (Toolbar) Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'mToolbar'", Toolbar.class);
        target.mDealProductPriceEditText = (DealProductPriceEditText) Utils.findRequiredViewAsType(source, R.id.price, "field 'mDealProductPriceEditText'", DealProductPriceEditText.class);
        target.mDealProductQuantityEditText = (DealProductQuantityEditText) Utils.findRequiredViewAsType(source, R.id.quantity, "field 'mDealProductQuantityEditText'", DealProductQuantityEditText.class);
        target.mDealProductDiscountEditText = (DealProductDiscountEditText) Utils.findRequiredViewAsType(source, R.id.discount, "field 'mDealProductDiscountEditText'", DealProductDiscountEditText.class);
        target.mTitle = (TextView) Utils.findRequiredViewAsType(source, R.id.title, "field 'mTitle'", TextView.class);
        View view = Utils.findRequiredView(source, R.id.variation, "field 'mProductVariationView' and method 'onVariationClicked'");
        target.mProductVariationView = (ProductVariationView) Utils.castView(view, R.id.variation, "field 'mProductVariationView'", ProductVariationView.class);
        this.view2131820816 = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onVariationClicked();
            }
        });
        target.mTotalValue = (ProductTotalTextView) Utils.findRequiredViewAsType(source, R.id.totalPrice, "field 'mTotalValue'", ProductTotalTextView.class);
        target.mDealProductDurationEditText = (DealProductDurationEditText) Utils.findRequiredViewAsType(source, R.id.duration, "field 'mDealProductDurationEditText'", DealProductDurationEditText.class);
    }

    @CallSuper
    public void unbind() {
        DealProductEditActivity target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mToolbar = null;
        target.mDealProductPriceEditText = null;
        target.mDealProductQuantityEditText = null;
        target.mDealProductDiscountEditText = null;
        target.mTitle = null;
        target.mProductVariationView = null;
        target.mTotalValue = null;
        target.mDealProductDurationEditText = null;
        this.view2131820816.setOnClickListener(null);
        this.view2131820816 = null;
    }
}
