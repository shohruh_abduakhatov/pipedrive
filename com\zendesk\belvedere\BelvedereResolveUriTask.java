package com.zendesk.belvedere;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.newrelic.agent.android.api.v2.TraceFieldInterface;
import com.newrelic.agent.android.tracing.Trace;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

class BelvedereResolveUriTask extends AsyncTask<Uri, Void, List<BelvedereResult>> implements TraceFieldInterface {
    private static final String LOG_TAG = "BelvedereResolveUriTask";
    public Trace _nr_trace;
    final BelvedereStorage belvedereStorage;
    final BelvedereCallback<List<BelvedereResult>> callback;
    final Context context;
    final BelvedereLogger log;

    public void _nr_setTrace(Trace trace) {
        try {
            this._nr_trace = trace;
        } catch (Exception e) {
        }
    }

    BelvedereResolveUriTask(@NonNull Context context, @NonNull BelvedereLogger belvedereLogger, @NonNull BelvedereStorage belvedereStorage, @Nullable BelvedereCallback<List<BelvedereResult>> callback) {
        this.context = context;
        this.log = belvedereLogger;
        this.belvedereStorage = belvedereStorage;
        this.callback = callback;
    }

    protected List<BelvedereResult> doInBackground(@NonNull Uri... uris) {
        IOException e;
        FileNotFoundException e2;
        Throwable th;
        List<BelvedereResult> success = new ArrayList();
        int length = uris.length;
        int i = 0;
        while (i < length) {
            Uri uri = uris[i];
            InputStream inputStream = null;
            FileOutputStream fileOutputStream = null;
            try {
                inputStream = this.context.getContentResolver().openInputStream(uri);
                File file = this.belvedereStorage.getTempFileForGalleryImage(this.context, uri);
                if (inputStream == null || file == null) {
                    BelvedereLogger belvedereLogger = this.log;
                    String str = LOG_TAG;
                    Locale locale = Locale.US;
                    String str2 = "Unable to resolve uri. InputStream null = %s, File null = %s";
                    Object[] objArr = new Object[2];
                    objArr[0] = Boolean.valueOf(inputStream == null);
                    objArr[1] = Boolean.valueOf(file == null);
                    belvedereLogger.w(str, String.format(locale, str2, objArr));
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e3) {
                            this.log.e(LOG_TAG, "Error closing InputStream", e3);
                        }
                    }
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException e32) {
                            this.log.e(LOG_TAG, "Error closing FileOutputStream", e32);
                        }
                    }
                    i++;
                } else {
                    this.log.d(LOG_TAG, String.format(Locale.US, "Copying media file into private cache - Uri: %s - Dest: %s", new Object[]{uri, file}));
                    FileOutputStream fileOutputStream2 = new FileOutputStream(file);
                    try {
                        byte[] buf = new byte[1024];
                        while (true) {
                            int len = inputStream.read(buf);
                            if (len <= 0) {
                                break;
                            }
                            fileOutputStream2.write(buf, 0, len);
                        }
                        success.add(new BelvedereResult(file, this.belvedereStorage.getFileProviderUri(this.context, file)));
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException e322) {
                                this.log.e(LOG_TAG, "Error closing InputStream", e322);
                            }
                        }
                        if (fileOutputStream2 != null) {
                            try {
                                fileOutputStream2.close();
                            } catch (IOException e3222) {
                                this.log.e(LOG_TAG, "Error closing FileOutputStream", e3222);
                                fileOutputStream = fileOutputStream2;
                            }
                        }
                        fileOutputStream = fileOutputStream2;
                    } catch (FileNotFoundException e4) {
                        e2 = e4;
                        fileOutputStream = fileOutputStream2;
                        try {
                            this.log.e(LOG_TAG, String.format(Locale.US, "File not found error copying file, uri: %s", new Object[]{uri}), e2);
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (IOException e32222) {
                                    this.log.e(LOG_TAG, "Error closing InputStream", e32222);
                                }
                            }
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                } catch (IOException e322222) {
                                    this.log.e(LOG_TAG, "Error closing FileOutputStream", e322222);
                                }
                            }
                            i++;
                        } catch (Throwable th2) {
                            th = th2;
                        }
                    } catch (IOException e5) {
                        e322222 = e5;
                        fileOutputStream = fileOutputStream2;
                        this.log.e(LOG_TAG, String.format(Locale.US, "IO Error copying file, uri: %s", new Object[]{uri}), e322222);
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException e3222222) {
                                this.log.e(LOG_TAG, "Error closing InputStream", e3222222);
                            }
                        }
                        if (fileOutputStream != null) {
                            try {
                                fileOutputStream.close();
                            } catch (IOException e32222222) {
                                this.log.e(LOG_TAG, "Error closing FileOutputStream", e32222222);
                            }
                        }
                        i++;
                    } catch (Throwable th3) {
                        th = th3;
                        fileOutputStream = fileOutputStream2;
                    }
                    i++;
                }
            } catch (FileNotFoundException e6) {
                e2 = e6;
                this.log.e(LOG_TAG, String.format(Locale.US, "File not found error copying file, uri: %s", new Object[]{uri}), e2);
                if (inputStream != null) {
                    inputStream.close();
                }
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
                i++;
            } catch (IOException e7) {
                e32222222 = e7;
                this.log.e(LOG_TAG, String.format(Locale.US, "IO Error copying file, uri: %s", new Object[]{uri}), e32222222);
                if (inputStream != null) {
                    inputStream.close();
                }
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
                i++;
            }
        }
        return success;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e322222222) {
                this.log.e(LOG_TAG, "Error closing InputStream", e322222222);
            }
        }
        if (fileOutputStream != null) {
            try {
                fileOutputStream.close();
            } catch (IOException e3222222222) {
                this.log.e(LOG_TAG, "Error closing FileOutputStream", e3222222222);
            }
        }
        throw th;
        if (fileOutputStream != null) {
            fileOutputStream.close();
        }
        throw th;
        throw th;
    }

    protected void onPostExecute(@NonNull List<BelvedereResult> resolvedUris) {
        super.onPostExecute(resolvedUris);
        if (this.callback != null) {
            this.callback.internalSuccess(resolvedUris);
        }
    }
}
