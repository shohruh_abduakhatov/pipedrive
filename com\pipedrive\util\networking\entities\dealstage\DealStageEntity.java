package com.pipedrive.util.networking.entities.dealstage;

import android.support.annotation.Nullable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DealStageEntity {
    private static final String JSON_PARAM_DEAL_PROBABILITY = "deal_probability";
    private static final String JSON_PARAM_ID = "id";
    private static final String JSON_PARAM_NAME = "name";
    private static final String JSON_PARAM_ORDER_NR = "order_nr";
    private static final String JSON_PARAM_PIPELINE_ID = "pipeline_id";
    @Nullable
    @SerializedName("deal_probability")
    @Expose
    private Integer dealProbability;
    @Nullable
    @SerializedName("name")
    @Expose
    private String name;
    @Nullable
    @SerializedName("order_nr")
    @Expose
    private Integer order;
    @Nullable
    @SerializedName("id")
    @Expose
    private Long pipedriveId;
    @Nullable
    @SerializedName("pipeline_id")
    @Expose
    private Integer pipelineId;

    @Nullable
    Long getPipedriveId() {
        return this.pipedriveId;
    }

    @Nullable
    Integer getOrder() {
        return this.order;
    }

    @Nullable
    String getName() {
        return this.name;
    }

    @Nullable
    Integer getDealProbability() {
        return this.dealProbability;
    }

    @Nullable
    Integer getPipelineId() {
        return this.pipelineId;
    }
}
