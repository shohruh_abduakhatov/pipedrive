package com.pipedrive.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import com.pipedrive.application.Session;
import com.pipedrive.views.viewholder.ViewHolderBuilder;
import com.pipedrive.views.viewholder.contact.PersonRowViewHolderForLinking;

public class PersonListAdapterForLinking extends PeopleListAdapter {
    public PersonListAdapterForLinking(Context context, Cursor c, Session session) {
        super(context, c, session);
    }

    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return ViewHolderBuilder.inflateViewAndTagWithViewHolder(context, parent, false, new PersonRowViewHolderForLinking());
    }

    public void bindView(View view, Context context, Cursor cursor) {
        ((PersonRowViewHolderForLinking) view.getTag()).fill(this.mSession, this.mDataSource.deflateCursor(cursor), this.mSearchConstraint);
    }
}
