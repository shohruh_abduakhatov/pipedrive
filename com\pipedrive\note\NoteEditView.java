package com.pipedrive.note;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.ArrowKeyMovementMethod;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;
import com.pipedrive.R;
import com.pipedrive.model.HtmlContent;

public class NoteEditView extends EditText {
    private static final int VIEW_TAG_NOTE_STATE = 2131820709;
    private static final String VIEW_TAG_NOTE_STATE_CHANGEABLE = "changeable";
    private static final String VIEW_TAG_NOTE_STATE_VIEWABLE = "viewable";

    private enum DecorateAs {
        REMOVE_UNDERLINE,
        RESTORE_UNDERLINE
    }

    @SuppressLint({"ParcelCreator"})
    private static class URLSpanNoUnderline extends URLSpan {
        URLSpanNoUnderline(String url) {
            super(url);
        }

        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }

    public NoteEditView(Context context) {
        this(context, null);
    }

    public NoteEditView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NoteEditView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setViewable();
    }

    public void setViewable() {
        if (!TextUtils.equals((String) getTag(R.id.noteTextView), VIEW_TAG_NOTE_STATE_VIEWABLE)) {
            setTag(R.id.noteTextView, VIEW_TAG_NOTE_STATE_VIEWABLE);
            setMovementMethod(LinkMovementMethod.getInstance());
            setFocusableInTouchMode(false);
            setFocusable(false);
            setCursorVisible(false);
            decorateUrlSpans(this, DecorateAs.RESTORE_UNDERLINE);
        }
    }

    public void setChangeable() {
        if (!TextUtils.equals((String) getTag(R.id.noteTextView), VIEW_TAG_NOTE_STATE_CHANGEABLE)) {
            setTag(R.id.noteTextView, VIEW_TAG_NOTE_STATE_CHANGEABLE);
            setMovementMethod(ArrowKeyMovementMethod.getInstance());
            setFocusableInTouchMode(true);
            setFocusable(true);
            setCursorVisible(true);
            requestFocus();
            decorateUrlSpans(this, DecorateAs.REMOVE_UNDERLINE);
            setSelection(getText().length());
        }
    }

    private void decorateUrlSpans(@NonNull TextView inTextView, @NonNull DecorateAs decorateAs) {
        Spannable tvSpannable = new SpannableString(inTextView.getText());
        for (URLSpan urlSpan : (URLSpan[]) tvSpannable.getSpans(0, tvSpannable.length(), URLSpan.class)) {
            int start = tvSpannable.getSpanStart(urlSpan);
            int end = tvSpannable.getSpanEnd(urlSpan);
            tvSpannable.removeSpan(urlSpan);
            String url = urlSpan.getURL();
            tvSpannable.setSpan(decorateAs == DecorateAs.REMOVE_UNDERLINE ? new URLSpanNoUnderline(url) : new URLSpan(url), start, end, 0);
        }
        inTextView.setText(tvSpannable);
    }

    @NonNull
    public Integer getNoteHashCode() {
        return Integer.valueOf(getText().toString().hashCode());
    }

    @NonNull
    public Boolean isViewable() {
        String tag = (String) getTag(R.id.noteTextView);
        boolean z = tag != null && TextUtils.equals(tag, VIEW_TAG_NOTE_STATE_VIEWABLE);
        return Boolean.valueOf(z);
    }

    @NonNull
    public Spanned getTextAsSpanned() {
        return new SpannableStringBuilder(getText());
    }

    public void setTextAsHtmlContent(@NonNull HtmlContent htmlContent) {
        String noteContentInHTML = htmlContent.getHtmlContent();
        if (noteContentInHTML == null) {
            noteContentInHTML = "";
        }
        setText(Html.fromHtml(noteContentInHTML));
    }
}
