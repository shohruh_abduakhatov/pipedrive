package com.pipedrive.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.newrelic.agent.android.instrumentation.JSONArrayInstrumentation;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.application.Session;
import com.pipedrive.logging.Log;
import com.pipedrive.model.customfields.CustomField;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

public class CustomFieldsDataSource extends SQLTransactionManager {
    private static final int COLUMN_VALUE_COLUMN_ACTIVE_IS_ACTIVE = 1;
    private static final int COLUMN_VALUE_COLUMN_ACTIVE_IS_INACTIVE = 0;
    static final String TAG = CustomFieldsDataSource.class.getSimpleName();
    private String[] allColumns = new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_PIPEDRIVE_ID, PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_NAME, PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_KEY, PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_FIELD_DATA_TYPE, PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_ACTIVE, PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_ORDER_NR, PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_OPTIONS, PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_TYPE};

    public CustomFieldsDataSource(Session session) {
        super(session.getDatabase());
    }

    public long createCustomField(CustomField field) {
        String jSONArray;
        ContentValues values = new ContentValues();
        values.put(PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_PIPEDRIVE_ID, Long.valueOf(field.getPipedriveId()));
        values.put(PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_NAME, field.getName());
        values.put(PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_KEY, field.getKey());
        values.put(PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_FIELD_DATA_TYPE, field.getFieldDataType());
        values.put(PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_ACTIVE, Integer.valueOf(field.isActive() ? 1 : 0));
        values.put(PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_ORDER_NR, Integer.valueOf(field.getOrderNr()));
        String str = PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_OPTIONS;
        if (field.getOptions() != null) {
            JSONArray options = field.getOptions();
            jSONArray = !(options instanceof JSONArray) ? options.toString() : JSONArrayInstrumentation.toString(options);
        } else {
            jSONArray = null;
        }
        values.put(str, jSONArray);
        values.put(PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_TYPE, field.getFieldType());
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        str = PipeSQLiteHelper.TABLE_CUSTOM_FIELDS;
        return !(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.replace(str, null, values) : SQLiteInstrumentation.replace(transactionalDBConnection, str, null, values);
    }

    public void deleteAllFields(String type) {
        StringBuilder selection = new StringBuilder();
        selection.append(PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_TYPE);
        selection.append(" = ?");
        deleteAllCustomFields(selection.toString(), new String[]{type});
    }

    public void deleteAllCustomFields(String selection, String[] selectionArgs) {
        try {
            SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
            String str = PipeSQLiteHelper.TABLE_CUSTOM_FIELDS;
            if (transactionalDBConnection instanceof SQLiteDatabase) {
                SQLiteInstrumentation.delete(transactionalDBConnection, str, selection, selectionArgs);
            } else {
                transactionalDBConnection.delete(str, selection, selectionArgs);
            }
        } catch (Exception e) {
            Log.e(e);
        }
    }

    public List<CustomField> getDealFields() {
        StringBuilder selection = new StringBuilder();
        selection.append(PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_TYPE);
        selection.append(" = ?");
        return getCustomFields(selection.toString(), new String[]{"deal"});
    }

    public List<CustomField> getPersonFields() {
        StringBuilder selection = new StringBuilder();
        selection.append(PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_TYPE);
        selection.append(" = ?");
        return getCustomFields(selection.toString(), new String[]{"person"});
    }

    public List<CustomField> getOrganizationFields() {
        StringBuilder selection = new StringBuilder();
        selection.append(PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_TYPE);
        selection.append(" = ?");
        return getCustomFields(selection.toString(), new String[]{"organization"});
    }

    private List<CustomField> getCustomFields(String selection, String[] selectionArgs) {
        Cursor cursor;
        List<CustomField> fields = new ArrayList();
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String str = PipeSQLiteHelper.TABLE_CUSTOM_FIELDS;
        String[] strArr = this.allColumns;
        String str2 = PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_ORDER_NR;
        if (transactionalDBConnection instanceof SQLiteDatabase) {
            cursor = SQLiteInstrumentation.query(transactionalDBConnection, str, strArr, selection, selectionArgs, null, null, str2, null);
        } else {
            cursor = transactionalDBConnection.query(str, strArr, selection, selectionArgs, null, null, str2, null);
        }
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                fields.add(cursorToCustomField(cursor));
                cursor.moveToNext();
            }
            return fields;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private CustomField cursorToCustomField(Cursor cursor) {
        boolean z = true;
        CustomField field = new CustomField();
        field.setId(cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_ID)));
        field.setPipedriveId(cursor.getLong(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_PIPEDRIVE_ID)));
        field.setKey(cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_KEY)));
        field.setName(cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_NAME)));
        field.setOrderNr(cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_ORDER_NR)));
        if (cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_ACTIVE)) != 1) {
            z = false;
        }
        field.setActive(z);
        field.setFieldDataType(cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_FIELD_DATA_TYPE)));
        field.setFieldType(cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_TYPE)));
        String optionsJson = cursor.getString(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_CUSTOM_FIELDS_OPTIONS));
        if (!TextUtils.isEmpty(optionsJson)) {
            try {
                field.setOptions(JSONArrayInstrumentation.init(optionsJson));
            } catch (JSONException e) {
            }
        }
        return field;
    }
}
