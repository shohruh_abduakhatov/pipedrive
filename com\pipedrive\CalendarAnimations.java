package com.pipedrive;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.support.v4.view.animation.PathInterpolatorCompat;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.RotateAnimation;
import com.pipedrive.views.agenda.AgendaViewEventBus;
import com.zendesk.service.HttpConstants;

public class CalendarAnimations {
    private CalendarAnimations() {
    }

    public static void changeViewHeightWithFading(View view, int start, int end, int duration, boolean fade, float startAlpha) {
        getValueAnimatorWithFading(start, end, duration, view, fade, startAlpha).start();
    }

    public static ValueAnimator getValueAnimatorWithFading(int start, int end, int duration, final View viewToExpandOrCollapse, final boolean fade, final float startAlpha) {
        ValueAnimator animator = ValueAnimator.ofInt(new int[]{start, end});
        animator.setDuration((long) duration);
        animator.setInterpolator(PathInterpolatorCompat.create(0.4f, 0.0f, 0.2f, 1.0f));
        animator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int value = ((Integer) valueAnimator.getAnimatedValue()).intValue();
                LayoutParams layoutParams = viewToExpandOrCollapse.getLayoutParams();
                layoutParams.height = value;
                viewToExpandOrCollapse.setLayoutParams(layoutParams);
                if (fade) {
                    CalendarAnimations.startFadeAnimation(viewToExpandOrCollapse, startAlpha, HttpConstants.HTTP_BAD_REQUEST);
                }
            }
        });
        animator.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animation) {
                AgendaViewEventBus.getInstance().allDayActivityAnimationEnd();
            }
        });
        return animator;
    }

    public static void startFadeAnimation(View view, float startAlpha, int duration) {
        getValueAnimatorFading(startAlpha, duration, view).start();
    }

    private static ValueAnimator getValueAnimatorFading(float start, int duration, final View viewToFade) {
        ValueAnimator animator = ValueAnimator.ofFloat(new float[]{start, 1.0f});
        animator.setDuration((long) duration);
        animator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                viewToFade.setAlpha(((Float) valueAnimator.getAnimatedValue()).floatValue());
            }
        });
        return animator;
    }

    public static void changeViewHeight(View view, int start, int end, int duration) {
        getValueAnimatorWithFading(start, end, duration, view, false, 1.0f).start();
    }

    public static void startRotateAnimation(float startAngle, float endAngle, int duration, View viewToRotate) {
        RotateAnimation rt = new RotateAnimation(startAngle, endAngle, (float) (viewToRotate.getWidth() / 2), (float) (viewToRotate.getHeight() / 2));
        rt.setDuration((long) duration);
        rt.setFillAfter(true);
        rt.isFillEnabled();
        viewToRotate.startAnimation(rt);
    }
}
