package com.google.android.gms.tagmanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build.VERSION;
import android.text.TextUtils;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.common.util.zzh;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class zzcg implements zzaw {
    private static final String dw = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' INTEGER NOT NULL, '%s' TEXT NOT NULL,'%s' INTEGER NOT NULL);", new Object[]{"gtm_hits", "hit_id", "hit_time", "hit_url", "hit_first_send_time"});
    private final zzb aFZ;
    private volatile zzad aGa;
    private final zzax aGb;
    private final String aGc;
    private long aGd;
    private final int aGe;
    private final Context mContext;
    private zze zzaql;

    class zza implements com.google.android.gms.tagmanager.zzdf.zza {
        final /* synthetic */ zzcg aGf;

        zza(zzcg com_google_android_gms_tagmanager_zzcg) {
            this.aGf = com_google_android_gms_tagmanager_zzcg;
        }

        public void zza(zzas com_google_android_gms_tagmanager_zzas) {
            this.aGf.zzu(com_google_android_gms_tagmanager_zzas.zzcfi());
        }

        public void zzb(zzas com_google_android_gms_tagmanager_zzas) {
            this.aGf.zzu(com_google_android_gms_tagmanager_zzas.zzcfi());
            zzbo.v("Permanent failure dispatching hitId: " + com_google_android_gms_tagmanager_zzas.zzcfi());
        }

        public void zzc(zzas com_google_android_gms_tagmanager_zzas) {
            long zzcfj = com_google_android_gms_tagmanager_zzas.zzcfj();
            if (zzcfj == 0) {
                this.aGf.zzh(com_google_android_gms_tagmanager_zzas.zzcfi(), this.aGf.zzaql.currentTimeMillis());
            } else if (zzcfj + 14400000 < this.aGf.zzaql.currentTimeMillis()) {
                this.aGf.zzu(com_google_android_gms_tagmanager_zzas.zzcfi());
                zzbo.v("Giving up on failed hitId: " + com_google_android_gms_tagmanager_zzas.zzcfi());
            }
        }
    }

    class zzb extends SQLiteOpenHelper {
        final /* synthetic */ zzcg aGf;
        private boolean aGg;
        private long aGh = 0;

        zzb(zzcg com_google_android_gms_tagmanager_zzcg, Context context, String str) {
            this.aGf = com_google_android_gms_tagmanager_zzcg;
            super(context, str, null, 1);
        }

        private boolean zza(String str, SQLiteDatabase sQLiteDatabase) {
            Cursor cursor;
            String str2;
            Throwable th;
            Cursor cursor2 = null;
            String str3;
            try {
                str3 = "SQLITE_MASTER";
                String[] strArr = new String[]{"name"};
                String str4 = "name=?";
                String[] strArr2 = new String[]{str};
                Cursor query = !(sQLiteDatabase instanceof SQLiteDatabase) ? sQLiteDatabase.query(str3, strArr, str4, strArr2, null, null, null) : SQLiteInstrumentation.query(sQLiteDatabase, str3, strArr, str4, strArr2, null, null, null);
                try {
                    boolean moveToFirst = query.moveToFirst();
                    if (query == null) {
                        return moveToFirst;
                    }
                    query.close();
                    return moveToFirst;
                } catch (SQLiteException e) {
                    cursor = query;
                    try {
                        str2 = "Error querying for table ";
                        str3 = String.valueOf(str);
                        zzbo.zzdi(str3.length() == 0 ? new String(str2) : str2.concat(str3));
                        if (cursor != null) {
                            cursor.close();
                        }
                        return false;
                    } catch (Throwable th2) {
                        cursor2 = cursor;
                        th = th2;
                        if (cursor2 != null) {
                            cursor2.close();
                        }
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    cursor2 = query;
                    if (cursor2 != null) {
                        cursor2.close();
                    }
                    throw th;
                }
            } catch (SQLiteException e2) {
                cursor = null;
                str2 = "Error querying for table ";
                str3 = String.valueOf(str);
                if (str3.length() == 0) {
                }
                zzbo.zzdi(str3.length() == 0 ? new String(str2) : str2.concat(str3));
                if (cursor != null) {
                    cursor.close();
                }
                return false;
            } catch (Throwable th4) {
                th = th4;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
        }

        private void zzc(SQLiteDatabase sQLiteDatabase) {
            String str = "SELECT * FROM gtm_hits WHERE 0";
            Cursor rawQuery = !(sQLiteDatabase instanceof SQLiteDatabase) ? sQLiteDatabase.rawQuery(str, null) : SQLiteInstrumentation.rawQuery(sQLiteDatabase, str, null);
            Set hashSet = new HashSet();
            try {
                String[] columnNames = rawQuery.getColumnNames();
                for (Object add : columnNames) {
                    hashSet.add(add);
                }
                if (!hashSet.remove("hit_id") || !hashSet.remove("hit_url") || !hashSet.remove("hit_time") || !hashSet.remove("hit_first_send_time")) {
                    throw new SQLiteException("Database column missing");
                } else if (!hashSet.isEmpty()) {
                    throw new SQLiteException("Database has extra columns");
                }
            } finally {
                rawQuery.close();
            }
        }

        public SQLiteDatabase getWritableDatabase() {
            if (!this.aGg || this.aGh + 3600000 <= this.aGf.zzaql.currentTimeMillis()) {
                SQLiteDatabase sQLiteDatabase = null;
                this.aGg = true;
                this.aGh = this.aGf.zzaql.currentTimeMillis();
                try {
                    sQLiteDatabase = super.getWritableDatabase();
                } catch (SQLiteException e) {
                    this.aGf.mContext.getDatabasePath(this.aGf.aGc).delete();
                }
                if (sQLiteDatabase == null) {
                    sQLiteDatabase = super.getWritableDatabase();
                }
                this.aGg = false;
                return sQLiteDatabase;
            }
            throw new SQLiteException("Database creation failed");
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            zzan.zzfd(sQLiteDatabase.getPath());
        }

        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (VERSION.SDK_INT < 15) {
                String str = "PRAGMA journal_mode=memory";
                Cursor rawQuery = !(sQLiteDatabase instanceof SQLiteDatabase) ? sQLiteDatabase.rawQuery(str, null) : SQLiteInstrumentation.rawQuery(sQLiteDatabase, str, null);
                try {
                    rawQuery.moveToFirst();
                } finally {
                    rawQuery.close();
                }
            }
            if (zza("gtm_hits", sQLiteDatabase)) {
                zzc(sQLiteDatabase);
                return;
            }
            String zzcfy = zzcg.dw;
            if (sQLiteDatabase instanceof SQLiteDatabase) {
                SQLiteInstrumentation.execSQL(sQLiteDatabase, zzcfy);
            } else {
                sQLiteDatabase.execSQL(zzcfy);
            }
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        }
    }

    zzcg(zzax com_google_android_gms_tagmanager_zzax, Context context) {
        this(com_google_android_gms_tagmanager_zzax, context, "gtm_urls.db", 2000);
    }

    zzcg(zzax com_google_android_gms_tagmanager_zzax, Context context, String str, int i) {
        this.mContext = context.getApplicationContext();
        this.aGc = str;
        this.aGb = com_google_android_gms_tagmanager_zzax;
        this.zzaql = zzh.zzayl();
        this.aFZ = new zzb(this, this.mContext, this.aGc);
        this.aGa = new zzdf(this.mContext, new zza(this));
        this.aGd = 0;
        this.aGe = i;
    }

    private void zzcfv() {
        int zzcfw = (zzcfw() - this.aGe) + 1;
        if (zzcfw > 0) {
            List zzaag = zzaag(zzcfw);
            zzbo.v("Store full, deleting " + zzaag.size() + " hits to make room.");
            zzh((String[]) zzaag.toArray(new String[0]));
        }
    }

    private void zzh(long j, long j2) {
        SQLiteDatabase zzpf = zzpf("Error opening database for getNumStoredHits.");
        if (zzpf != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("hit_first_send_time", Long.valueOf(j2));
            try {
                String str = "gtm_hits";
                String str2 = "hit_id=?";
                String[] strArr = new String[]{String.valueOf(j)};
                if (zzpf instanceof SQLiteDatabase) {
                    SQLiteInstrumentation.update(zzpf, str, contentValues, str2, strArr);
                } else {
                    zzpf.update(str, contentValues, str2, strArr);
                }
            } catch (SQLiteException e) {
                zzbo.zzdi("Error setting HIT_FIRST_DISPATCH_TIME for hitId: " + j);
                zzu(j);
            }
        }
    }

    private void zzh(long j, String str) {
        SQLiteDatabase zzpf = zzpf("Error opening database for putHit");
        if (zzpf != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("hit_time", Long.valueOf(j));
            contentValues.put("hit_url", str);
            contentValues.put("hit_first_send_time", Integer.valueOf(0));
            try {
                String str2 = "gtm_hits";
                if (zzpf instanceof SQLiteDatabase) {
                    SQLiteInstrumentation.insert(zzpf, str2, null, contentValues);
                } else {
                    zzpf.insert(str2, null, contentValues);
                }
                this.aGb.zzcn(false);
            } catch (SQLiteException e) {
                zzbo.zzdi("Error storing hit");
            }
        }
    }

    private SQLiteDatabase zzpf(String str) {
        try {
            return this.aFZ.getWritableDatabase();
        } catch (SQLiteException e) {
            zzbo.zzdi(str);
            return null;
        }
    }

    private void zzu(long j) {
        zzh(new String[]{String.valueOf(j)});
    }

    public void dispatch() {
        zzbo.v("GTM Dispatch running...");
        if (this.aGa.zzcez()) {
            List zzaah = zzaah(40);
            if (zzaah.isEmpty()) {
                zzbo.v("...nothing to dispatch");
                this.aGb.zzcn(true);
                return;
            }
            this.aGa.zzal(zzaah);
            if (zzcfx() > 0) {
                zzdc.zzcgt().dispatch();
            }
        }
    }

    List<String> zzaag(int i) {
        Cursor query;
        SQLiteException e;
        String str;
        String valueOf;
        Throwable th;
        Cursor cursor = null;
        List<String> arrayList = new ArrayList();
        if (i <= 0) {
            zzbo.zzdi("Invalid maxHits specified. Skipping");
            return arrayList;
        }
        SQLiteDatabase zzpf = zzpf("Error opening database for peekHitIds.");
        if (zzpf == null) {
            return arrayList;
        }
        try {
            String str2 = "gtm_hits";
            String[] strArr = new String[]{"hit_id"};
            String format = String.format("%s ASC", new Object[]{"hit_id"});
            String num = Integer.toString(i);
            query = !(zzpf instanceof SQLiteDatabase) ? zzpf.query(str2, strArr, null, null, null, null, format, num) : SQLiteInstrumentation.query(zzpf, str2, strArr, null, null, null, null, format, num);
            try {
                if (query.moveToFirst()) {
                    do {
                        arrayList.add(String.valueOf(query.getLong(0)));
                    } while (query.moveToNext());
                }
                if (query != null) {
                    query.close();
                }
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    str = "Error in peekHits fetching hitIds: ";
                    valueOf = String.valueOf(e.getMessage());
                    zzbo.zzdi(valueOf.length() == 0 ? str.concat(valueOf) : new String(str));
                    if (query != null) {
                        query.close();
                    }
                    return arrayList;
                } catch (Throwable th2) {
                    th = th2;
                    cursor = query;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            query = null;
            str = "Error in peekHits fetching hitIds: ";
            valueOf = String.valueOf(e.getMessage());
            if (valueOf.length() == 0) {
            }
            zzbo.zzdi(valueOf.length() == 0 ? str.concat(valueOf) : new String(str));
            if (query != null) {
                query.close();
            }
            return arrayList;
        } catch (Throwable th3) {
            th = th3;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return arrayList;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public List<zzas> zzaah(int i) {
        String str;
        SQLiteException e;
        Throwable th;
        SQLiteException sQLiteException;
        Cursor cursor;
        List<zzas> list;
        String str2;
        ArrayList arrayList = new ArrayList();
        SQLiteDatabase zzpf = zzpf("Error opening database for peekHits");
        if (zzpf == null) {
            return arrayList;
        }
        Cursor query;
        Cursor cursor2 = null;
        try {
            str = "gtm_hits";
            String[] strArr = new String[]{"hit_id", "hit_time", "hit_first_send_time"};
            String format = String.format("%s ASC", new Object[]{"hit_id"});
            String num = Integer.toString(i);
            query = !(zzpf instanceof SQLiteDatabase) ? zzpf.query(str, strArr, null, null, null, null, format, num) : SQLiteInstrumentation.query(zzpf, str, strArr, null, null, null, null, format, num);
            List<zzas> arrayList2;
            try {
                arrayList2 = new ArrayList();
                if (query.moveToFirst()) {
                    do {
                        arrayList2.add(new zzas(query.getLong(0), query.getLong(1), query.getLong(2)));
                    } while (query.moveToNext());
                }
                if (query != null) {
                    query.close();
                }
                try {
                    str = "gtm_hits";
                    strArr = new String[]{"hit_id", "hit_url"};
                    format = String.format("%s ASC", new Object[]{"hit_id"});
                    num = Integer.toString(i);
                    Cursor query2 = !(zzpf instanceof SQLiteDatabase) ? zzpf.query(str, strArr, null, null, null, null, format, num) : SQLiteInstrumentation.query(zzpf, str, strArr, null, null, null, null, format, num);
                    try {
                        if (query2.moveToFirst()) {
                            int i2 = 0;
                            while (true) {
                                if (((SQLiteCursor) query2).getWindow().getNumRows() > 0) {
                                    ((zzas) arrayList2.get(i2)).zzpj(query2.getString(1));
                                } else {
                                    zzbo.zzdi(String.format("HitString for hitId %d too large.  Hit will be deleted.", new Object[]{Long.valueOf(((zzas) arrayList2.get(i2)).zzcfi())}));
                                }
                                int i3 = i2 + 1;
                                if (!query2.moveToNext()) {
                                    break;
                                }
                                i2 = i3;
                            }
                        }
                        if (query2 != null) {
                            query2.close();
                        }
                        return arrayList2;
                    } catch (SQLiteException e2) {
                        e = e2;
                        query = query2;
                    } catch (Throwable th2) {
                        th = th2;
                        query = query2;
                    }
                } catch (SQLiteException e3) {
                    e = e3;
                    try {
                        str = "Error in peekHits fetching hit url: ";
                        String valueOf = String.valueOf(e.getMessage());
                        zzbo.zzdi(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                        List<zzas> arrayList3 = new ArrayList();
                        Object obj = null;
                        for (zzas com_google_android_gms_tagmanager_zzas : arrayList2) {
                            if (TextUtils.isEmpty(com_google_android_gms_tagmanager_zzas.zzcfk())) {
                                if (obj != null) {
                                    break;
                                }
                                obj = 1;
                            }
                            arrayList3.add(com_google_android_gms_tagmanager_zzas);
                        }
                        if (query != null) {
                            query.close();
                        }
                        return arrayList3;
                    } catch (Throwable th3) {
                        th = th3;
                    }
                }
            } catch (SQLiteException e4) {
                sQLiteException = e4;
                cursor = query;
                list = arrayList2;
                try {
                    str2 = "Error in peekHits fetching hitIds: ";
                    str = String.valueOf(sQLiteException.getMessage());
                    zzbo.zzdi(str.length() != 0 ? str2.concat(str) : new String(str2));
                    if (cursor == null) {
                        return list;
                    }
                    cursor.close();
                    return list;
                } catch (Throwable th4) {
                    th = th4;
                    cursor2 = cursor;
                    if (cursor2 != null) {
                        cursor2.close();
                    }
                    throw th;
                }
            } catch (Throwable th5) {
                th = th5;
                cursor2 = query;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
        } catch (SQLiteException e42) {
            sQLiteException = e42;
            cursor = null;
            list = arrayList;
            str2 = "Error in peekHits fetching hitIds: ";
            str = String.valueOf(sQLiteException.getMessage());
            if (str.length() != 0) {
            }
            zzbo.zzdi(str.length() != 0 ? str2.concat(str) : new String(str2));
            if (cursor == null) {
                return list;
            }
            cursor.close();
            return list;
        } catch (Throwable th6) {
            th = th6;
            if (cursor2 != null) {
                cursor2.close();
            }
            throw th;
        }
        if (query != null) {
            query.close();
        }
        throw th;
    }

    int zzadb() {
        boolean z = true;
        long currentTimeMillis = this.zzaql.currentTimeMillis();
        if (currentTimeMillis <= this.aGd + 86400000) {
            return 0;
        }
        this.aGd = currentTimeMillis;
        SQLiteDatabase zzpf = zzpf("Error opening database for deleteStaleHits.");
        if (zzpf == null) {
            return 0;
        }
        String str = "gtm_hits";
        String str2 = "HIT_TIME < ?";
        String[] strArr = new String[]{Long.toString(this.zzaql.currentTimeMillis() - 2592000000L)};
        int delete = !(zzpf instanceof SQLiteDatabase) ? zzpf.delete(str, str2, strArr) : SQLiteInstrumentation.delete(zzpf, str, str2, strArr);
        zzax com_google_android_gms_tagmanager_zzax = this.aGb;
        if (zzcfw() != 0) {
            z = false;
        }
        com_google_android_gms_tagmanager_zzax.zzcn(z);
        return delete;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    int zzcfw() {
        Throwable th;
        Cursor cursor = null;
        SQLiteDatabase zzpf = zzpf("Error opening database for getNumStoredHits.");
        if (zzpf == null) {
            return 0;
        }
        int i;
        try {
            String str = "SELECT COUNT(*) from gtm_hits";
            cursor = !(zzpf instanceof SQLiteDatabase) ? zzpf.rawQuery(str, null) : SQLiteInstrumentation.rawQuery(zzpf, str, null);
            i = cursor.moveToFirst() ? (int) cursor.getLong(0) : 0;
            if (cursor != null) {
                cursor.close();
            }
        } catch (SQLiteException e) {
            Cursor cursor2 = cursor;
            try {
                zzbo.zzdi("Error getting numStoredHits");
                if (cursor2 == null) {
                    i = 0;
                } else {
                    cursor2.close();
                    i = 0;
                }
                return i;
            } catch (Throwable th2) {
                cursor = cursor2;
                th = th2;
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return i;
    }

    int zzcfx() {
        Cursor cursor;
        Throwable th;
        Cursor cursor2 = null;
        SQLiteDatabase zzpf = zzpf("Error opening database for getNumStoredHits.");
        if (zzpf == null) {
            return 0;
        }
        int count;
        try {
            String str = "gtm_hits";
            String[] strArr = new String[]{"hit_id", "hit_first_send_time"};
            String str2 = "hit_first_send_time=0";
            Cursor query = !(zzpf instanceof SQLiteDatabase) ? zzpf.query(str, strArr, str2, null, null, null, null) : SQLiteInstrumentation.query(zzpf, str, strArr, str2, null, null, null, null);
            try {
                count = query.getCount();
                if (query != null) {
                    query.close();
                }
            } catch (SQLiteException e) {
                cursor = query;
                try {
                    zzbo.zzdi("Error getting num untried hits");
                    if (cursor == null) {
                        count = 0;
                    } else {
                        cursor.close();
                        count = 0;
                    }
                    return count;
                } catch (Throwable th2) {
                    cursor2 = cursor;
                    th = th2;
                    if (cursor2 != null) {
                        cursor2.close();
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                cursor2 = query;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
        } catch (SQLiteException e2) {
            cursor = null;
            zzbo.zzdi("Error getting num untried hits");
            if (cursor == null) {
                cursor.close();
                count = 0;
            } else {
                count = 0;
            }
            return count;
        } catch (Throwable th4) {
            th = th4;
            if (cursor2 != null) {
                cursor2.close();
            }
            throw th;
        }
        return count;
    }

    public void zzg(long j, String str) {
        zzadb();
        zzcfv();
        zzh(j, str);
    }

    void zzh(String[] strArr) {
        if (strArr != null && strArr.length != 0) {
            SQLiteDatabase zzpf = zzpf("Error opening database for deleteHits.");
            if (zzpf != null) {
                String format = String.format("HIT_ID in (%s)", new Object[]{TextUtils.join(Table.COMMA_SEP, Collections.nCopies(strArr.length, "?"))});
                try {
                    String str = "gtm_hits";
                    if (zzpf instanceof SQLiteDatabase) {
                        SQLiteInstrumentation.delete(zzpf, str, format, strArr);
                    } else {
                        zzpf.delete(str, format, strArr);
                    }
                    this.aGb.zzcn(zzcfw() == 0);
                } catch (SQLiteException e) {
                    zzbo.zzdi("Error deleting hits");
                }
            }
        }
    }
}
