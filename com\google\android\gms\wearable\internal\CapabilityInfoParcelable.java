package com.google.android.gms.wearable.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.wearable.CapabilityInfo;
import com.google.android.gms.wearable.Node;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CapabilityInfoParcelable extends AbstractSafeParcelable implements CapabilityInfo {
    public static final Creator<CapabilityInfoParcelable> CREATOR = new zzk();
    private Set<Node> aTf;
    private final List<NodeParcelable> aTi;
    private final String mName;
    final int mVersionCode;
    private final Object zzako = new Object();

    CapabilityInfoParcelable(int i, String str, List<NodeParcelable> list) {
        this.mVersionCode = i;
        this.mName = str;
        this.aTi = list;
        this.aTf = null;
        zzcmr();
    }

    private void zzcmr() {
        zzaa.zzy(this.mName);
        zzaa.zzy(this.aTi);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        CapabilityInfoParcelable capabilityInfoParcelable = (CapabilityInfoParcelable) obj;
        if (this.mVersionCode != capabilityInfoParcelable.mVersionCode) {
            return false;
        }
        if (this.mName == null ? capabilityInfoParcelable.mName != null : !this.mName.equals(capabilityInfoParcelable.mName)) {
            return false;
        }
        if (this.aTi != null) {
            if (this.aTi.equals(capabilityInfoParcelable.aTi)) {
                return true;
            }
        } else if (capabilityInfoParcelable.aTi == null) {
            return true;
        }
        return false;
    }

    public String getName() {
        return this.mName;
    }

    public Set<Node> getNodes() {
        Set<Node> set;
        synchronized (this.zzako) {
            if (this.aTf == null) {
                this.aTf = new HashSet(this.aTi);
            }
            set = this.aTf;
        }
        return set;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.mName != null ? this.mName.hashCode() : 0) + (this.mVersionCode * 31)) * 31;
        if (this.aTi != null) {
            i = this.aTi.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        String str = this.mName;
        String valueOf = String.valueOf(this.aTi);
        return new StringBuilder((String.valueOf(str).length() + 18) + String.valueOf(valueOf).length()).append("CapabilityInfo{").append(str).append(", ").append(valueOf).append("}").toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzk.zza(this, parcel, i);
    }

    public List<NodeParcelable> zzcms() {
        return this.aTi;
    }
}
