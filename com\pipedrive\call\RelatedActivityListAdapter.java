package com.pipedrive.call;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.ActivitiesDataSource;
import com.pipedrive.views.viewholder.ViewHolderBuilder;
import com.pipedrive.views.viewholder.activity.ActivityRowViewHolder;

class RelatedActivityListAdapter extends CursorAdapter {
    @NonNull
    private final ActivitiesDataSource mDataSource = new ActivitiesDataSource(this.mSession);
    @NonNull
    private final Session mSession;

    public RelatedActivityListAdapter(@NonNull Session session, @NonNull Context context, @NonNull Cursor c) {
        super(context, c, true);
        this.mSession = session;
    }

    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return ViewHolderBuilder.inflateViewAndTagWithViewHolder(context, parent, false, new ActivityRowViewHolder());
    }

    public void bindView(View view, Context context, Cursor cursor) {
        ((ActivityRowViewHolder) view.getTag()).fill(this.mSession, context, this.mDataSource.deflateCursor(cursor));
    }
}
