package com.pipedrive.tasks.products;

import android.support.annotation.NonNull;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.pipedrive.application.Session;
import com.pipedrive.gson.GsonHelper;
import com.pipedrive.tasks.AsyncTask;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.ConnectionUtil$JsonReaderInterceptor;
import com.pipedrive.util.networking.Response;
import com.pipedrive.util.networking.entities.product.ProductEntity;
import com.pipedrive.util.products.ProductsNetworkingUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReDownloadAllProductsTask extends AsyncTask<Void, Void, Void> {
    public ReDownloadAllProductsTask(Session session) {
        super(session);
    }

    protected Void doInBackground(Void... params) {
        reDownloadAllProducts();
        return null;
    }

    public void reDownloadAllProducts() {
        ApiUrlBuilder productsUrlBuilder = new ApiUrlBuilder(getSession(), "products");
        productsUrlBuilder.appendEncodedPath(ProductEntity.URL_NESTED_PARAMS_LIST);
        ConnectionUtil.requestGetWithAutomaticPaging(productsUrlBuilder, new ConnectionUtil$JsonReaderInterceptor<Response>() {
            public Response interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull Response responseTemplate) throws IOException {
                if (jsonReader.peek() == JsonToken.NULL) {
                    jsonReader.skipValue();
                } else {
                    List<ProductEntity> downloadedProductEntities = new ArrayList();
                    if (jsonReader.peek() == JsonToken.BEGIN_ARRAY) {
                        jsonReader.beginArray();
                        while (jsonReader.hasNext()) {
                            downloadedProductEntities.add((ProductEntity) GsonHelper.fromJSON(jsonReader, ProductEntity.class));
                        }
                        jsonReader.endArray();
                    }
                    ProductsNetworkingUtil.createOrUpdateProductsIntoDBWithRelations(session, downloadedProductEntities);
                }
                return responseTemplate;
            }
        });
    }
}
