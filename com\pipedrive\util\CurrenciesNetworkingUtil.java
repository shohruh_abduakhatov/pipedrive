package com.pipedrive.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.CurrenciesDataSource;
import com.pipedrive.gson.GsonHelper;
import com.pipedrive.model.Currency;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.ConnectionUtil$JsonReaderInterceptor;
import com.pipedrive.util.networking.Response;
import com.pipedrive.util.networking.entities.currency.CurrencyEntity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CurrenciesNetworkingUtil {
    private static final String API_PARAM_CURRENCY = "currency";

    private CurrenciesNetworkingUtil() {
    }

    @WorkerThread
    public static void downloadAndStoreCurrenciesBlocking(@NonNull Session session) {
        List<Currency> currencies = downloadCurrenciesAndRatesBlocking(session);
        if (!currencies.isEmpty()) {
            CurrenciesDataSource ds = new CurrenciesDataSource(session.getDatabase());
            for (Currency currency : currencies) {
                ds.createOrUpdate(currency);
            }
        }
    }

    @WorkerThread
    @NonNull
    static List<Currency> downloadCurrenciesAndRatesBlocking(@NonNull Session session) {
        AnonymousClass1CurrenciesResponse currenciesResponse = (AnonymousClass1CurrenciesResponse) ConnectionUtil.requestGet(new ApiUrlBuilder(session, "currencies"), new ConnectionUtil$JsonReaderInterceptor<AnonymousClass1CurrenciesResponse>() {
            public AnonymousClass1CurrenciesResponse interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull AnonymousClass1CurrenciesResponse responseTemplate) throws IOException {
                JsonElement root = new JsonParser().parse(jsonReader);
                if (root.isJsonArray()) {
                    Iterator it = root.getAsJsonArray().iterator();
                    while (it.hasNext()) {
                        responseTemplate.downloadedCurrencyEntities.add(GsonHelper.fromJSON((JsonElement) it.next(), CurrencyEntity.class));
                    }
                }
                return responseTemplate;
            }
        }, new Response() {
            @NonNull
            private final List<CurrencyEntity> downloadedCurrencyEntities = new ArrayList();
        });
        List<Currency> downloadedCurrencies = new ArrayList(currenciesResponse.downloadedCurrencyEntities.size());
        for (CurrencyEntity downloadedCurrencyEntity : currenciesResponse.downloadedCurrencyEntities) {
            Currency currency = Currency.fromEntity(downloadedCurrencyEntity);
            if (currency != null) {
                downloadedCurrencies.add(currency);
            }
        }
        String currencyCodeForExchangeRequest = getCurrencyCodeForExchangeRequest(session, downloadedCurrencies);
        if (currencyCodeForExchangeRequest != null) {
            Map<String, Double> exchangeRates = downloadRatesBlocking(session, currencyCodeForExchangeRequest);
            if (exchangeRates != null) {
                for (Currency downloadedCurrency : downloadedCurrencies) {
                    if (exchangeRates.containsKey(downloadedCurrency.getCode())) {
                        downloadedCurrency.setRateToDefault(((Double) exchangeRates.get(downloadedCurrency.getCode())).doubleValue());
                    }
                }
            }
        }
        return downloadedCurrencies;
    }

    @Nullable
    static String getCurrencyCodeForExchangeRequest(@NonNull Session session, @NonNull List<Currency> currencies) {
        String userSettingsDefaultCurrencyCode = session.getUserSettingsDefaultCurrencyCode();
        if (Session.PREFS_NOT_SET_STRING.equals(userSettingsDefaultCurrencyCode)) {
            return null;
        }
        for (Currency currency : currencies) {
            if (userSettingsDefaultCurrencyCode.equals(currency.getCode())) {
                if (currency.isCustom()) {
                    return null;
                }
                return currency.getCode();
            }
        }
        return null;
    }

    @Nullable
    static Map<String, Double> downloadRatesBlocking(@NonNull Session session, @NonNull String currencyCode) {
        return ((AnonymousClass1ExchangeRatesResponse) ConnectionUtil.requestGet(new ApiUrlBuilder(session, ApiUrlBuilder.ENDPOINT_CURRENCIES_EXCHANGE_RATES).param("currency", currencyCode), new ConnectionUtil$JsonReaderInterceptor<AnonymousClass1ExchangeRatesResponse>() {
            public AnonymousClass1ExchangeRatesResponse interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull AnonymousClass1ExchangeRatesResponse responseTemplate) throws IOException {
                responseTemplate.exchangeRates = (Map) GsonHelper.fromJSON(new JsonParser().parse(jsonReader), HashMap.class);
                return responseTemplate;
            }
        }, new Response() {
            @Nullable
            private Map<String, Double> exchangeRates;
        })).exchangeRates;
    }

    @Nullable
    @WorkerThread
    public static Currency getCurrencyByCodeAndDownloadIfNotFound(@NonNull Session session, @NonNull String currencyCode) {
        if (new CurrenciesDataSource(session.getDatabase()).getCurrencyByCode(currencyCode) == null) {
            downloadAndStoreCurrenciesBlocking(session);
        }
        return new CurrenciesDataSource(session.getDatabase()).getCurrencyByCode(currencyCode);
    }
}
