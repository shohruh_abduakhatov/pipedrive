package com.zendesk.sdk.model.push;

import android.support.annotation.Nullable;
import java.util.Date;

public class PushRegistrationResponse {
    private boolean active;
    private Date createdAt;
    private String deviceType;
    private int id;
    private String identifier;
    private Date updatedAt;
    private String url;

    @Nullable
    public String getIdentifier() {
        return this.identifier;
    }

    public boolean equals(Object o) {
        boolean z = true;
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PushRegistrationResponse response = (PushRegistrationResponse) o;
        if (this.id != response.id || this.active != response.active) {
            return false;
        }
        if (this.identifier != null) {
            if (!this.identifier.equals(response.identifier)) {
                return false;
            }
        } else if (response.identifier != null) {
            return false;
        }
        if (this.deviceType != null) {
            if (!this.deviceType.equals(response.deviceType)) {
                return false;
            }
        } else if (response.deviceType != null) {
            return false;
        }
        if (this.createdAt != null) {
            if (!this.createdAt.equals(response.createdAt)) {
                return false;
            }
        } else if (response.createdAt != null) {
            return false;
        }
        if (this.updatedAt != null) {
            if (!this.updatedAt.equals(response.updatedAt)) {
                return false;
            }
        } else if (response.updatedAt != null) {
            return false;
        }
        if (this.url != null) {
            z = this.url.equals(response.url);
        } else if (response.url != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int hashCode;
        int i = 0;
        int hashCode2 = ((this.id * 31) + (this.identifier != null ? this.identifier.hashCode() : 0)) * 31;
        if (this.deviceType != null) {
            hashCode = this.deviceType.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 31;
        if (this.createdAt != null) {
            hashCode = this.createdAt.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 31;
        if (this.updatedAt != null) {
            hashCode = this.updatedAt.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 31;
        if (this.active) {
            hashCode = 1;
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 31;
        if (this.url != null) {
            i = this.url.hashCode();
        }
        return hashCode + i;
    }
}
