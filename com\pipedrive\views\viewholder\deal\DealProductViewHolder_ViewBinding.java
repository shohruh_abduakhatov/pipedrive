package com.pipedrive.views.viewholder.deal;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pipedrive.R;

public class DealProductViewHolder_ViewBinding implements Unbinder {
    private DealProductViewHolder target;

    @UiThread
    public DealProductViewHolder_ViewBinding(DealProductViewHolder target, View source) {
        this.target = target;
        target.mProductTitle = (TextView) Utils.findRequiredViewAsType(source, R.id.productTitle, "field 'mProductTitle'", TextView.class);
        target.mProductFormula = (TextView) Utils.findRequiredViewAsType(source, R.id.productFormula, "field 'mProductFormula'", TextView.class);
        target.mProductTotalPrice = (TextView) Utils.findRequiredViewAsType(source, R.id.productPrice, "field 'mProductTotalPrice'", TextView.class);
    }

    @CallSuper
    public void unbind() {
        DealProductViewHolder target = this.target;
        if (target == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.target = null;
        target.mProductTitle = null;
        target.mProductFormula = null;
        target.mProductTotalPrice = null;
    }
}
