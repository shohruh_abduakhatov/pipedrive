package com.pipedrive.tasks;

import com.pipedrive.LoginActivity;
import com.pipedrive.application.Session;
import com.pipedrive.logging.Log;

public class UnauthorizedHandlerTask extends NonSessionDependentTask<Void, Void, Void> {
    public static final String TAG = UnauthorizedHandlerTask.class.getSimpleName();

    public UnauthorizedHandlerTask(Session session) {
        super(session);
    }

    protected Void doInBackground(Void... params) {
        Log.d(TAG, "Logging out current user");
        LoginActivity.logOut(getSession().getApplicationContext(), true, getSession());
        return null;
    }
}
