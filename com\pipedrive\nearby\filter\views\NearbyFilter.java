package com.pipedrive.nearby.filter.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout.SimpleDrawerListener;
import android.view.LayoutInflater;
import android.view.View;
import com.pipedrive.R;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.filter.FilterEventBus;
import com.pipedrive.nearby.filter.FilterItemList;
import com.pipedrive.nearby.filter.NearbyFilterDataRepository;
import com.pipedrive.nearby.filter.filterlist.FilterContentRecyclerView;
import com.pipedrive.nearby.filter.filterlist.NearbyFilterListAdapter;
import com.pipedrive.nearby.model.NearbyItemType;
import com.pipedrive.nearby.toolbar.NearbyItemTypeChangesProvider;
import com.pipedrive.nearby.util.Irrelevant;
import com.pipedrive.views.filter.FilterDrawerLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Actions;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

@SuppressLint({"ViewConstructor"})
public class NearbyFilter extends FilterDrawerLayout {
    private static final int DELAY_FILTER_AVAILABILITY_MILLIS = 250;
    @Nullable
    private NearbyFilterListAdapter adapter;
    @NonNull
    private final SimpleDrawerListener drawerListener = new SimpleDrawerListener() {
        public void onDrawerOpened(View drawerView) {
            if (NearbyFilter.this.filterContentRecyclerView != null && NearbyFilter.this.session != null && NearbyFilter.this.filterContentRecyclerView.getAdapter() == null) {
                NearbyFilter.this.adapter = new NearbyFilterListAdapter(NearbyFilter.this.filterItemLists, NearbyFilter.this.session);
                if (NearbyFilter.this.isDrawerOpen()) {
                    NearbyFilter.this.filterContentRecyclerView.setAdapter(NearbyFilter.this.adapter);
                }
            }
        }
    };
    @Nullable
    private FilterButton filterButton;
    @Nullable
    private final FilterContentRecyclerView filterContentRecyclerView;
    @NonNull
    private List<FilterItemList> filterItemLists = new ArrayList();
    @Nullable
    private NearbyItemTypeChangesProvider nearbyItemTypeChangesProvider;
    @Nullable
    private Session session;
    @NonNull
    private final CompositeSubscription subscriptions = new CompositeSubscription();

    public NearbyFilter(@NonNull Context context, @NonNull LayoutInflater layoutInflater, @NonNull View parentView) {
        super(context, layoutInflater, parentView, R.layout.layout_filter_list);
        addDrawerListener(this.drawerListener);
        this.filterContentRecyclerView = (FilterContentRecyclerView) getDrawerContentView();
        lockDrawer(true);
    }

    public void setup(@NonNull Session session, @NonNull FilterButton filterButton, @NonNull NearbyItemTypeChangesProvider nearbyItemTypeChangesProvider) {
        this.filterButton = filterButton;
        this.nearbyItemTypeChangesProvider = nearbyItemTypeChangesProvider;
        this.session = session;
    }

    public void bind() {
        if (this.filterButton != null) {
            this.subscriptions.add(this.filterButton.filterButtonClicks().subscribeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Irrelevant>() {
                public void call(Irrelevant irrelevant) {
                    NearbyFilter.this.openDrawer();
                }
            }, defaultOnErrorAction()));
        }
        if (this.nearbyItemTypeChangesProvider != null && this.session == null) {
        }
    }

    @NonNull
    private Action1<Throwable> defaultOnErrorAction() {
        return new Action1<Throwable>() {
            public void call(Throwable throwable) {
            }
        };
    }

    public void releaseResources() {
        this.subscriptions.clear();
        removeDrawerListener(this.drawerListener);
    }

    private void setupFilterContent(@NonNull NearbyItemType nearbyItemType) {
        if (this.session != null) {
            this.subscriptions.add(new NearbyFilterDataRepository().getFilterList(this.session, nearbyItemType).subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread()).doOnNext(new Action1<List<FilterItemList>>() {
                public void call(List<FilterItemList> list) {
                    NearbyFilter.this.lockDrawer(true);
                    if (NearbyFilter.this.filterContentRecyclerView != null) {
                        NearbyFilter.this.filterContentRecyclerView.setAdapter(null);
                    }
                }
            }).doOnNext(new Action1<List<FilterItemList>>() {
                public void call(List<FilterItemList> filterItemLists) {
                    NearbyFilter.this.filterItemLists = filterItemLists;
                }
            }).delay(250, TimeUnit.MILLISECONDS).doOnNext(new Action1<List<FilterItemList>>() {
                public void call(List<FilterItemList> list) {
                    FilterEventBus.INSTANCE.onFilterReady();
                    NearbyFilter.this.lockDrawer(false);
                }
            }).subscribe(Actions.empty(), defaultOnErrorAction()));
        }
    }
}
