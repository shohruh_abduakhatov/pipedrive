package com.zendesk.sdk.storage;

import android.support.annotation.Nullable;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.settings.MobileSettings;
import com.zendesk.sdk.model.settings.SafeMobileSettings;
import java.util.concurrent.TimeUnit;

class StubSdkSettingsStorage implements SdkSettingsStorage {
    private static final String LOG_TAG = "StubSdkStorage";

    StubSdkSettingsStorage() {
    }

    public boolean hasStoredSdkSettings() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
        return false;
    }

    public boolean areSettingsUpToDate(long duration, TimeUnit timeUnit) {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
        return false;
    }

    @Nullable
    public SafeMobileSettings getStoredSettings() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
        return new SafeMobileSettings(new MobileSettings());
    }

    public void setStoredSettings(SafeMobileSettings mobileSettings) {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
    }

    public void deleteStoredSettings() {
        Logger.w(LOG_TAG, "Zendesk not initialised", new Object[0]);
    }
}
