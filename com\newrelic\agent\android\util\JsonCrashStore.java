package com.newrelic.agent.android.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.newrelic.agent.android.crashes.CrashStore;
import com.newrelic.agent.android.harvest.crash.Crash;
import com.newrelic.agent.android.logging.AgentLog;
import com.newrelic.agent.android.logging.AgentLogManager;
import com.newrelic.com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JsonCrashStore implements CrashStore {
    private static final String STORE_FILE = "NRCrashStore";
    private static final AgentLog log = AgentLogManager.getAgentLog();
    private final Context context;

    public JsonCrashStore(Context context) {
        this.context = context;
    }

    public void store(Crash crash) {
        synchronized (this) {
            Editor editor = this.context.getSharedPreferences(STORE_FILE, 0).edit();
            JsonObject jsonObj = crash.asJsonObject();
            jsonObj.add("uploadCount", SafeJsonPrimitive.factory(Integer.valueOf(crash.getUploadCount())));
            editor.putString(crash.getUuid().toString(), jsonObj.toString());
            editor.apply();
        }
    }

    public List<Crash> fetchAll() {
        SharedPreferences store = this.context.getSharedPreferences(STORE_FILE, 0);
        List<Crash> crashes = new ArrayList();
        synchronized (this) {
            Map<String, ?> crashStrings = store.getAll();
        }
        for (Object string : crashStrings.values()) {
            if (string instanceof String) {
                try {
                    crashes.add(Crash.crashFromJsonString((String) string));
                } catch (Exception e) {
                    log.error("Exception encountered while deserializing crash", e);
                }
            }
        }
        return crashes;
    }

    public int count() {
        int count;
        synchronized (this) {
            count = this.context.getSharedPreferences(STORE_FILE, 0).getAll().size();
        }
        return count;
    }

    public void clear() {
        synchronized (this) {
            Editor editor = this.context.getSharedPreferences(STORE_FILE, 0).edit();
            editor.clear();
            editor.apply();
        }
    }

    public void delete(Crash crash) {
        synchronized (this) {
            Editor editor = this.context.getSharedPreferences(STORE_FILE, 0).edit();
            editor.remove(crash.getUuid().toString());
            editor.apply();
        }
    }
}
