package com.pipedrive.store;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.pipedrive.application.Session;
import com.pipedrive.logging.Log;
import com.pipedrive.logging.LogJourno;
import com.pipedrive.logging.LogJourno.EVENT;
import com.pipedrive.model.BaseDatasourceEntity;

abstract class Store<T extends BaseDatasourceEntity> {
    @NonNull
    private final Session mSession;

    abstract boolean implementCreate(@NonNull T t);

    abstract boolean implementUpdate(@NonNull T t);

    public Store(@NonNull Session session) {
        this.mSession = session;
    }

    @NonNull
    final Session getSession() {
        return this.mSession;
    }

    public final boolean create(@NonNull T newEntity) {
        boolean storedOk = createWithoutSync(newEntity);
        if (storedOk) {
            triggerChangeAsync();
        }
        return storedOk;
    }

    final boolean createWithoutSync(@Nullable T newEntity) {
        boolean z = false;
        if (newEntity == null) {
            LogJourno.reportEvent(EVENT.Store_createRequestedWithNoEntity);
            Log.e(new Throwable("New entity is null! Cannot continue storing it. Expected behaviour?"));
        } else if (newEntity.isStored()) {
            LogJourno.reportEvent(EVENT.Store_createRequestedWithStoredEntity);
            Log.e(new Throwable("Cannot store existing entity as new!"));
        } else {
            cleanupBeforeCreate(newEntity);
            z = implementCreate(newEntity);
            if (z) {
                postProcessAfterCreate(newEntity);
            }
        }
        return z;
    }

    public final boolean update(@NonNull T updatedEntity) {
        boolean storedOk = updateWithoutSync(updatedEntity);
        if (storedOk) {
            triggerChangeAsync();
        }
        return storedOk;
    }

    final boolean updateWithoutSync(@Nullable T updatedEntity) {
        boolean z = false;
        if (updatedEntity == null) {
            LogJourno.reportEvent(EVENT.Store_updateRequestedWithNoEntity);
            Log.e(new Throwable("Updated entity is null! Cannot continue storing it. Expected behaviour?"));
        } else if (updatedEntity.isStored()) {
            cleanupBeforeUpdate(updatedEntity);
            z = implementUpdate(updatedEntity);
            if (z) {
                postProcessAfterUpdate(updatedEntity);
            }
        } else {
            LogJourno.reportEvent(EVENT.Store_updateRequestedWithNonStoredEntity);
            Log.e(new Throwable("Cannot store new entity as existing!"));
        }
        return z;
    }

    public final boolean delete(@NonNull T entityToDelete) {
        boolean storedOk = deleteWithoutSync(entityToDelete);
        if (storedOk) {
            triggerChangeAsync();
        }
        return storedOk;
    }

    boolean deleteWithoutSync(@Nullable T entityToDelete) {
        boolean z = false;
        if (entityToDelete == null) {
            LogJourno.reportEvent(EVENT.Store_deleteRequestedWithNoEntity);
            Log.e(new Throwable("Updated entity is null! Cannot continue storing it. Expected behaviour?"));
        } else if (entityToDelete.isStored()) {
            cleanupBeforeDelete(entityToDelete);
            z = implementDelete(entityToDelete);
            if (z) {
                postProcessAfterDelete(entityToDelete);
            }
        } else {
            LogJourno.reportEvent(EVENT.Store_deleteRequestedWithNonStoredEntity);
            Log.e(new Throwable("Cannot store new entity as existing!"));
        }
        return z;
    }

    private void triggerChangeAsync() {
        new StoreSynchronizeTask(getSession()).executeIfNotExecuting(new Void[0]);
    }

    void cleanupBeforeCreate(@NonNull T t) {
    }

    void postProcessAfterCreate(@NonNull T t) {
    }

    void cleanupBeforeUpdate(@NonNull T t) {
    }

    void postProcessAfterUpdate(@NonNull T t) {
    }

    void cleanupBeforeDelete(@NonNull T t) {
    }

    boolean implementDelete(@NonNull T t) {
        throw new RuntimeException("Not implemented!");
    }

    void postProcessAfterDelete(@NonNull T t) {
    }
}
