package com.pipedrive.tasks.search;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.search.SearchConstraint;
import com.pipedrive.gson.GsonHelper;
import com.pipedrive.tasks.AsyncTask;
import com.pipedrive.util.networking.ApiUrlBuilder;
import com.pipedrive.util.networking.ConnectionUtil;
import com.pipedrive.util.networking.ConnectionUtil$JsonReaderInterceptor;
import com.pipedrive.util.networking.Response;
import com.pipedrive.util.networking.search.DealSearchResultEntity;
import com.pipedrive.util.networking.search.OrganizationSearchResultEntity;
import com.pipedrive.util.networking.search.PersonSearchResultEntity;
import com.pipedrive.util.networking.search.SearchResultsResponse;
import java.io.IOException;
import java.util.Iterator;

public class GlobalSearchQueryTask extends AsyncTask<SearchConstraint, Integer, Void> {
    public GlobalSearchQueryTask(Session session) {
        super(session);
    }

    protected Void doInBackground(@NonNull SearchConstraint... params) {
        SearchConstraint searchConstraint = params[0];
        if (searchConstraint.isLongEnoughForSearch()) {
            ApiUrlBuilder uriBuilder = new ApiUrlBuilder(getSession(), ApiUrlBuilder.ENDPOINT_SEARCH_RESULTS);
            uriBuilder.param("term", searchConstraint.toString());
            ConnectionUtil.requestGetWithAutomaticPaging(uriBuilder, new ConnectionUtil$JsonReaderInterceptor<Response>() {
                public Response interceptReader(@NonNull Session session, @NonNull JsonReader jsonReader, @NonNull Response responseTemplate) throws IOException {
                    new SearchResultsResponseProcessor(session).process(GlobalSearchQueryTask.readSearchResultsResponse(jsonReader));
                    return responseTemplate;
                }
            });
        }
        return null;
    }

    @Nullable
    static SearchResultsResponse readSearchResultsResponse(@NonNull JsonReader jsonReader) {
        JsonElement parsedArray = new JsonParser().parse(jsonReader);
        if (!parsedArray.isJsonArray()) {
            return null;
        }
        JsonArray jsonArray = parsedArray.getAsJsonArray();
        SearchResultsResponse response = new SearchResultsResponse();
        Iterator it = jsonArray.iterator();
        while (it.hasNext()) {
            JsonElement jsonElement = (JsonElement) it.next();
            if (jsonElement.isJsonObject()) {
                JsonObject jsonObject = jsonElement.getAsJsonObject();
                JsonElement typeParam = jsonObject.get("type");
                if (typeParam != null && typeParam.isJsonPrimitive()) {
                    String typeString = typeParam.getAsString();
                    if (typeString != null) {
                        Object obj = -1;
                        switch (typeString.hashCode()) {
                            case -991716523:
                                if (typeString.equals("person")) {
                                    obj = 2;
                                    break;
                                }
                                break;
                            case 3079276:
                                if (typeString.equals("deal")) {
                                    obj = null;
                                    break;
                                }
                                break;
                            case 1178922291:
                                if (typeString.equals("organization")) {
                                    obj = 1;
                                    break;
                                }
                                break;
                        }
                        switch (obj) {
                            case null:
                                response.getDealSearchResultEntities().add(GsonHelper.fromJSON(jsonObject, DealSearchResultEntity.class));
                                break;
                            case 1:
                                response.getOrganizationSearchResultEntities().add(GsonHelper.fromJSON(jsonObject, OrganizationSearchResultEntity.class));
                                break;
                            case 2:
                                response.getPersonSearchResultEntities().add(GsonHelper.fromJSON(jsonObject, PersonSearchResultEntity.class));
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
        return response;
    }
}
