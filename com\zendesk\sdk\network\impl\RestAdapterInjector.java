package com.zendesk.sdk.network.impl;

import com.zendesk.sdk.util.BaseInjector;
import com.zendesk.sdk.util.LibraryInjector;
import com.zendesk.sdk.util.ModuleInjector;
import retrofit2.Retrofit;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestAdapterInjector {
    static Retrofit injectCachedRestAdapter(ApplicationScope applicationScope) {
        return ModuleInjector.injectCachedRestAdapterModule(applicationScope).getRetrofit();
    }

    public static Retrofit injectRestAdapter(ApplicationScope applicationScope) {
        return new Builder().baseUrl(BaseInjector.injectUrl(applicationScope)).addConverterFactory(injectGsonConverterFactory(applicationScope)).client(LibraryInjector.injectOkHttpClient(applicationScope)).build();
    }

    private static GsonConverterFactory injectGsonConverterFactory(ApplicationScope applicationScope) {
        return GsonConverterFactory.create(LibraryInjector.injectCachedGson(applicationScope));
    }
}
