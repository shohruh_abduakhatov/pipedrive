package com.pipedrive.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.pipedrive.OrganizationDetailActivity;
import com.pipedrive.R;
import com.pipedrive.activity.ActivityDetailActivity;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.call.CallSummaryListener;
import com.pipedrive.call.CallSummaryListener.CallSummaryRegistration;
import com.pipedrive.customfields.fragments.PersonCustomFieldListFragment;
import com.pipedrive.datasource.PersonsDataSource;
import com.pipedrive.deal.DealEditActivity;
import com.pipedrive.dialogs.communicationmediumlist.CommunicationMediumDialogController;
import com.pipedrive.dialogs.communicationmediumlist.CommunicationMediumListDialogFragment;
import com.pipedrive.flow.fragments.PersonFlowFragment;
import com.pipedrive.model.FlowFile;
import com.pipedrive.model.Person;
import com.pipedrive.model.contact.CommunicationMedium;
import com.pipedrive.note.NoteCreateActivity;
import com.pipedrive.person.edit.PersonUpdateActivity;
import com.pipedrive.store.StoreFlowFile;
import com.pipedrive.util.EmailHelper;
import com.pipedrive.util.PipedriveUtil;
import com.pipedrive.util.camera.CameraHelper;
import com.pipedrive.util.camera.CameraHelper.OnCameraListener;
import com.pipedrive.util.communicatiomedium.PersonCommunicationMediumUtil;
import com.pipedrive.util.filepicker.DocumentPickerHelper;
import com.pipedrive.util.filepicker.DocumentPickerHelper.OnDocumentPickerListener;
import com.pipedrive.util.flowfiles.FlowFileUploadNotificationManager;
import com.pipedrive.util.flowfiles.FlowFileUploadNotificationManager.Notification;
import com.pipedrive.views.fab.FabMenuClickListener;
import com.pipedrive.views.fab.FabWithOverlayMenu;
import com.pipedrive.views.profilepicture.ProfilePictureRoundPerson;
import java.util.Arrays;
import java.util.List;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

public final class PersonDetailFragment extends BaseFragment implements FabMenuClickListener, CommunicationMediumDialogController<Person> {
    private static final String KEY_PERSON_SQL_ID = "PERSON_SQL_ID";
    public static final String TAG = PersonDetailFragment.class.getSimpleName();
    @Nullable
    private CameraHelper mCameraHelper;
    @Nullable
    private PersonCustomFieldListFragment mCustomFieldListFragment;
    @Nullable
    private DocumentPickerHelper mDocumentPickerHelper;
    @BindView(2131820824)
    FabWithOverlayMenu mFabWithOverlayMenu;
    @Nullable
    private Subscription mFlowFileUploadNotificationManagerSubscription;
    @Nullable
    private PersonFlowFragment mFlowFragment;
    @BindView(2131820922)
    TextView mNameTextView;
    @BindView(2131820992)
    TextView mOrganizationTextView;
    private Person mPerson;
    @BindView(2131820782)
    ProfilePictureRoundPerson mProfilePictureRoundPerson;
    @Nullable
    private SetupProgressBarCallback mSetupProgressBarCallback;
    @BindView(2131820925)
    TabLayout mTabLayout;
    @Nullable
    private Unbinder mUnbinder;
    @BindView(2131820736)
    ViewPager mViewPager;

    public interface SetupProgressBarCallback {
        void setupProgressBarWithToolbar(Toolbar toolbar);
    }

    private static class PersonDetailsPagerAdapter extends FragmentStatePagerAdapter {
        private final List<BaseFragment> fragments;
        @NonNull
        private final Session session;

        public PersonDetailsPagerAdapter(@NonNull Session session, @NonNull FragmentManager fm, @NonNull List<BaseFragment> fragments) {
            super(fm);
            this.session = session;
            this.fragments = fragments;
        }

        public Fragment getItem(int position) {
            return (Fragment) this.fragments.get(position);
        }

        public int getCount() {
            return this.fragments.size();
        }

        public CharSequence getPageTitle(int position) {
            String title;
            switch (position) {
                case 0:
                    title = this.session.getApplicationContext().getString(R.string.tab_title_contact_general);
                    break;
                case 1:
                    title = this.session.getApplicationContext().getString(R.string.tab_title_contact_flow);
                    break;
                case 2:
                    title = this.session.getApplicationContext().getString(R.string.tab_title_contact_deals);
                    break;
                default:
                    title = "";
                    break;
            }
            return title.toUpperCase();
        }
    }

    public static PersonDetailFragment newInstance(@NonNull Long personSqlId) {
        Bundle args = new Bundle();
        args.putSerializable("PERSON_SQL_ID", personSqlId);
        PersonDetailFragment fragment = new PersonDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mCameraHelper = new CameraHelper();
        this.mCameraHelper.onCreate(savedInstanceState);
        this.mDocumentPickerHelper = new DocumentPickerHelper();
        if (getPersonSqlId() == null) {
            getActivity().finish();
        } else {
            setHasOptionsMenu(true);
        }
    }

    @Nullable
    private Long getPersonSqlId() {
        return (Long) getArguments().getSerializable("PERSON_SQL_ID");
    }

    private void setupActionBar() {
        if ((getContext() instanceof AppCompatActivity) && ((AppCompatActivity) getContext()).getSupportActionBar() != null) {
            ((AppCompatActivity) getContext()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.mSetupProgressBarCallback = (SetupProgressBarCallback) context;
        } catch (ClassCastException e) {
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        if (this.mCameraHelper != null) {
            this.mCameraHelper.onSaveInstanceState(outState);
        }
        super.onSaveInstanceState(outState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_person_details, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mUnbinder = ButterKnife.bind((Object) this, view);
        Session activeSession = PipedriveApp.getActiveSession();
        Long personSqlId = getPersonSqlId();
        if (activeSession != null && personSqlId != null) {
            ViewPager viewPager = this.mViewPager;
            FragmentManager childFragmentManager = getChildFragmentManager();
            r5 = new BaseFragment[3];
            PersonCustomFieldListFragment newInstance = PersonCustomFieldListFragment.newInstance(personSqlId);
            this.mCustomFieldListFragment = newInstance;
            r5[0] = newInstance;
            PersonFlowFragment newInstance2 = PersonFlowFragment.newInstance(personSqlId);
            this.mFlowFragment = newInstance2;
            r5[1] = newInstance2;
            r5[2] = DealsListFragment.newInstanceForPerson(personSqlId.longValue(), false, true);
            viewPager.setAdapter(new PersonDetailsPagerAdapter(activeSession, childFragmentManager, Arrays.asList(r5)));
            this.mTabLayout.setupWithViewPager(this.mViewPager);
            this.mFabWithOverlayMenu.setMenu(R.menu.menu_fab_person, R.id.action_add_note);
            this.mFabWithOverlayMenu.setOnMenuClickListener(this);
            if (this.mSetupProgressBarCallback != null) {
                this.mSetupProgressBarCallback.setupProgressBarWithToolbar(this.mToolbar);
            }
            setupActionBar();
        }
    }

    public void onResume() {
        super.onResume();
        final Session activeSession = PipedriveApp.getActiveSession();
        Long personSqlId = getPersonSqlId();
        if (activeSession == null || personSqlId == null) {
            getActivity().finish();
            return;
        }
        this.mPerson = (Person) new PersonsDataSource(PipedriveApp.getActiveSession().getDatabase()).findBySqlId(personSqlId.longValue());
        if (this.mPerson == null) {
            getActivity().finish();
            return;
        }
        this.mFlowFileUploadNotificationManagerSubscription = FlowFileUploadNotificationManager.INSTANCE.getFlowFileUploadNotificationBus().observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Notification>() {
            public void call(Notification notification) {
                PersonDetailFragment.this.refreshFlowView(activeSession);
            }
        });
        getActivity().supportInvalidateOptionsMenu();
        this.mNameTextView.setText(this.mPerson.getName());
        if (this.mPerson.getCompany() != null) {
            this.mOrganizationTextView.setText(this.mPerson.getCompany().getName());
            this.mOrganizationTextView.setVisibility(0);
        } else {
            this.mOrganizationTextView.setVisibility(8);
        }
        this.mProfilePictureRoundPerson.loadPicture(activeSession, this.mPerson);
        refreshFlowView(activeSession);
        if (this.mCustomFieldListFragment != null) {
            this.mCustomFieldListFragment.refreshContent(activeSession);
        }
        if (this.mCameraHelper != null) {
            this.mCameraHelper.retrieveCameraSnapshot(getContext(), new OnCameraListener() {
                public boolean snapshot(@NonNull Uri snapshot, @Nullable String snapshotFileName) {
                    FlowFile flowFileForDeal = new FlowFile();
                    flowFileForDeal.setPerson(PersonDetailFragment.this.mPerson);
                    flowFileForDeal.setUrl(snapshot.toString());
                    flowFileForDeal.setName(snapshotFileName);
                    boolean storeIsSuccess = new StoreFlowFile(activeSession).create(flowFileForDeal);
                    if (storeIsSuccess) {
                        PersonDetailFragment.this.refreshFlowView(activeSession);
                    }
                    return storeIsSuccess;
                }
            });
        }
        if (this.mDocumentPickerHelper != null) {
            this.mDocumentPickerHelper.retrievePickedDocument(getContext(), new OnDocumentPickerListener() {
                public boolean documentPicked(@NonNull Uri document, @Nullable String documentName, @Nullable Long fileSizeInBytes, @Nullable String fileType) {
                    FlowFile flowFileForDeal = new FlowFile();
                    flowFileForDeal.setPerson(PersonDetailFragment.this.mPerson);
                    flowFileForDeal.setUrl(document.toString());
                    flowFileForDeal.setName(documentName);
                    flowFileForDeal.setFileSize(fileSizeInBytes);
                    flowFileForDeal.setFileType(fileType);
                    boolean storeIsSuccess = new StoreFlowFile(activeSession).create(flowFileForDeal);
                    if (storeIsSuccess) {
                        PersonDetailFragment.this.refreshFlowView(activeSession);
                    }
                    return storeIsSuccess;
                }
            });
        }
    }

    public void onPause() {
        if (this.mFlowFileUploadNotificationManagerSubscription != null) {
            this.mFlowFileUploadNotificationManagerSubscription.unsubscribe();
            this.mFlowFileUploadNotificationManagerSubscription = null;
        }
        super.onPause();
    }

    public void onDestroyView() {
        if (this.mUnbinder != null) {
            this.mUnbinder.unbind();
        }
        super.onDestroyView();
    }

    @OnClick({2131820992})
    public void onOrganizationClicked(TextView organizationTextView) {
        if (this.mPerson.getCompany() != null && this.mPerson.getCompany().isStored()) {
            OrganizationDetailActivity.startActivity(getActivity(), Long.valueOf(this.mPerson.getCompany().getSqlId()));
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_person_detail, menu);
    }

    public void onPrepareOptionsMenu(Menu menu) {
        boolean ableToManageMenuItems;
        boolean z = false;
        if (this.mPerson != null) {
            ableToManageMenuItems = true;
        } else {
            ableToManageMenuItems = false;
        }
        if (ableToManageMenuItems) {
            PersonCommunicationMediumUtil util = new PersonCommunicationMediumUtil();
            MenuItem callMenuItem = menu.findItem(R.id.menu_call);
            if (callMenuItem != null) {
                callMenuItem.setVisible(util.hasPhones(this.mPerson));
            }
            MenuItem messageMenuItem = menu.findItem(R.id.menu_send_message);
            if (messageMenuItem != null) {
                if (util.hasEmails(this.mPerson) || util.hasPhones(this.mPerson)) {
                    z = true;
                }
                messageMenuItem.setVisible(z);
            }
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Person entity = getEntity();
        if (entity == null) {
            return false;
        }
        switch (item.getItemId()) {
            case R.id.menu_call:
                onCallMenuItemClicked(entity);
                return true;
            case R.id.menu_send_message:
                onSendMessageMenuItemClicked();
                return true;
            case R.id.menu_edit:
                onEditMenuItemClicked();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onSendMessageMenuItemClicked() {
        CommunicationMediumListDialogFragment.showSendMessageSelector(getFragmentManager(), this);
    }

    private void onCallMenuItemClicked(@NonNull Person person) {
        PersonCommunicationMediumUtil util = new PersonCommunicationMediumUtil();
        if (util.hasMultiplePhones(person)) {
            CommunicationMediumListDialogFragment.showCallSelector(getFragmentManager(), this);
        } else if (util.hasPhones(person)) {
            onCallRequested((CommunicationMedium) util.getPhones(person).get(0), person);
        }
    }

    private void onEditMenuItemClicked() {
        PersonUpdateActivity.startActivityForResult(getActivity(), this.mPerson, 0);
    }

    public void onMenuItemClicked(int itemId) {
        switch (itemId) {
            case R.id.action_take_photo:
                takePhoto();
                return;
            case R.id.action_upload_file:
                uploadDocument();
                return;
            case R.id.action_add_activity:
                ActivityDetailActivity.startActivityForPerson(getActivity(), this.mPerson);
                return;
            case R.id.action_add_note:
                NoteCreateActivity.createNoteForPerson(getActivity(), this.mPerson);
                return;
            case R.id.action_add_deal:
                DealEditActivity.startActivityForAddingDealToPerson(getActivity(), this.mPerson);
                return;
            default:
                return;
        }
    }

    private void takePhoto() {
        if (!(this.mCameraHelper == null)) {
            this.mCameraHelper.snapPhoto((Fragment) this);
        }
    }

    private void uploadDocument() {
        if (!(this.mDocumentPickerHelper == null)) {
            this.mDocumentPickerHelper.pickDocument((Fragment) this);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        boolean ableToPassResultToCameraHelper;
        boolean ableToPassResultToDocumentPickerHelper = true;
        if (this.mCameraHelper != null) {
            ableToPassResultToCameraHelper = true;
        } else {
            ableToPassResultToCameraHelper = false;
        }
        if (ableToPassResultToCameraHelper) {
            this.mCameraHelper.registerOnActivityResult(requestCode, resultCode, data);
        }
        if (this.mDocumentPickerHelper == null) {
            ableToPassResultToDocumentPickerHelper = false;
        }
        if (ableToPassResultToDocumentPickerHelper) {
            this.mDocumentPickerHelper.registerOnActivityResult(requestCode, resultCode, data);
        }
    }

    public boolean consumeBackPress() {
        return this.mFabWithOverlayMenu.consumeBackPress();
    }

    public void onCallRequested(@NonNull CommunicationMedium medium, @NonNull Person person) {
        makeCallWithCallSummary(medium.getValue(), person);
    }

    public void makeCallWithCallSummary(@Nullable String number, @NonNull Person person) {
        Session session = PipedriveApp.getActiveSession();
        boolean noSession = session == null;
        FragmentActivity nonApplicationContext = getActivity();
        boolean noNonApplicationContext = nonApplicationContext == null;
        Long personSqlId = person.isStored() ? Long.valueOf(person.getSqlId()) : null;
        boolean cannotInitializeCallRequest = noSession || noNonApplicationContext || (personSqlId == null) || (!isResumed());
        if (!cannotInitializeCallRequest) {
            CallSummaryListener.makeCallWithCallSummaryRegistration(session, nonApplicationContext, new CallSummaryRegistration(number, personSqlId));
        }
    }

    public void onSmsRequested(@NonNull CommunicationMedium medium, @NonNull Person person) {
        PipedriveUtil.sendSms(getActivity(), medium.getValue());
    }

    public void onEmailRequested(@NonNull CommunicationMedium medium, @NonNull Person person) {
        EmailHelper.composeAndSendEmail(getActivity(), medium.getValue(), person.getDropBoxAddress());
    }

    @Nullable
    public Person getEntity() {
        return this.mPerson;
    }

    @NonNull
    public List<? extends CommunicationMedium> getPhones(@NonNull Person person) {
        return new PersonCommunicationMediumUtil().getPhones(person);
    }

    @NonNull
    public List<? extends CommunicationMedium> getEmails(@NonNull Person person) {
        return new PersonCommunicationMediumUtil().getEmails(person);
    }

    public void refreshFlowView(@NonNull Session session) {
        if (this.mFlowFragment != null) {
            this.mFlowFragment.refreshContent(session);
        }
    }
}
