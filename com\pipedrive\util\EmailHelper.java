package com.pipedrive.util;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.text.Editable;
import android.text.Html;
import android.text.Html.TagHandler;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.QuoteSpan;
import com.pipedrive.R;
import com.pipedrive.application.PipedriveApp;
import com.pipedrive.application.Session;
import com.pipedrive.datasource.PipeSQLiteSharedHelper.Table;
import com.pipedrive.datetime.LocaleHelper;
import com.pipedrive.model.email.EmailMessage;
import com.pipedrive.model.email.EmailParticipant;
import com.pipedrive.sync.recents.RecentsSyncAdapter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.xml.sax.XMLReader;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

public enum EmailHelper {
    ;
    
    private static final long DELAY_RECENTS_CALL_AFTER_MAIL_SENDING_SECONDS = 5;
    private static final String HtmlTagQuote = "blockquote";
    private static final int REQUEST_SEND_EMAIL = 100;

    public static final class GreyQuoteSpan extends QuoteSpan {
        public GreyQuoteSpan() {
            super(-3355444);
        }
    }

    private static final class GreyQuoteSpanMarker {
        private GreyQuoteSpanMarker() {
        }
    }

    public static boolean composeAndSendEmailReply(Context context, EmailMessage messageToComposeEmailFrom) {
        return composeAndSendEmailReply(context, messageToComposeEmailFrom, null);
    }

    public static boolean composeAndSendEmailReply(Context context, EmailMessage messageToComposeEmailFrom, @Nullable String[] additionalBcc) {
        if (context == null || messageToComposeEmailFrom == null) {
            return false;
        }
        String[] sendEmailTo = emailParticipantListToStringArray(messageToComposeEmailFrom.getFrom());
        String[] sendEmailBcc = additionalBcc;
        String subject = decorateSubjectReply(context, messageToComposeEmailFrom.getSubject());
        String messageBody = messageToComposeEmailFrom.getBody() == null ? null : new String(messageToComposeEmailFrom.getBody());
        if (messageBody != null) {
            messageBody = decorateQuoteEmail(messageBody);
        }
        return sendEmail(context, sendEmailTo, null, sendEmailBcc, messageBody, getQuotedMessageBodyHeaderReply(context, messageToComposeEmailFrom), subject);
    }

    public static boolean composeAndSendEmailReplyAll(Context context, EmailMessage messageToComposeEmailFrom) {
        return composeAndSendEmailReplyAll(context, messageToComposeEmailFrom, null);
    }

    public static boolean composeAndSendEmailReplyAll(Context context, EmailMessage messageToComposeEmailFrom, @Nullable String[] additionalBcc) {
        if (context == null || messageToComposeEmailFrom == null) {
            return false;
        }
        List<EmailParticipant> to = new ArrayList();
        to.addAll(messageToComposeEmailFrom.getFrom());
        to.addAll(messageToComposeEmailFrom.getTo());
        String[] sendEmailTo = emailParticipantListToStringArray(to);
        String[] sendEmailCc = emailParticipantListToStringArray(messageToComposeEmailFrom.getCc());
        String[] sendEmailBcc = additionalBcc;
        String subject = decorateSubjectReply(context, messageToComposeEmailFrom.getSubject());
        String messageBody = messageToComposeEmailFrom.getBody() == null ? null : new String(messageToComposeEmailFrom.getBody());
        if (messageBody != null) {
            messageBody = decorateQuoteEmail(messageBody);
        }
        return sendEmail(context, sendEmailTo, sendEmailCc, sendEmailBcc, messageBody, getQuotedMessageBodyHeaderReply(context, messageToComposeEmailFrom), subject);
    }

    public static boolean composeAndSendEmailForward(Context context, EmailMessage messageToComposeEmailFrom) {
        return composeAndSendEmailForward(context, messageToComposeEmailFrom, null);
    }

    public static boolean composeAndSendEmailForward(Context context, EmailMessage messageToComposeEmailFrom, @Nullable String[] additionalBcc) {
        if (context == null || messageToComposeEmailFrom == null) {
            return false;
        }
        String[] sendEmailBcc = additionalBcc;
        String subject = decorateSubjectForward(context, messageToComposeEmailFrom.getSubject());
        String messageBody = messageToComposeEmailFrom.getBody() == null ? null : new String(messageToComposeEmailFrom.getBody());
        if (messageBody != null) {
            messageBody = decorateQuoteEmail(messageBody);
        }
        return sendEmail(context, null, null, sendEmailBcc, messageBody, getQuotedMessageBodyHeaderForward(context, messageToComposeEmailFrom), subject);
    }

    public static void composeAndSendEmail(Activity activity, String to, String bcc) {
        if (!TextUtils.isEmpty(to)) {
            Intent emailIntent = new Intent("android.intent.action.SENDTO");
            emailIntent.addFlags(268435456);
            emailIntent.setData(Uri.parse("mailto:"));
            emailIntent.putExtra("android.intent.extra.EMAIL", new String[]{to});
            if (!TextUtils.isEmpty(bcc)) {
                emailIntent.putExtra("android.intent.extra.BCC", new String[]{bcc});
            }
            try {
                activity.startActivityForResult(Intent.createChooser(emailIntent, activity.getString(R.string.title_sendmail_chooser_dialog)), 100);
            } catch (ActivityNotFoundException e) {
            }
        }
    }

    private static boolean sendEmail(Context context, String[] output, String[] cc, String[] bcc, String messageBody, String quotedMessageBodyHeader, String subject) {
        Intent emailIntent = new Intent("android.intent.action.SENDTO");
        emailIntent.addFlags(268435456);
        emailIntent.setData(Uri.parse("mailto:"));
        if (output != null) {
            emailIntent.putExtra("android.intent.extra.EMAIL", output);
        }
        if (cc != null) {
            emailIntent.putExtra("android.intent.extra.CC", cc);
        }
        if (bcc != null) {
            emailIntent.putExtra("android.intent.extra.BCC", bcc);
        }
        if (subject != null) {
            emailIntent.putExtra("android.intent.extra.SUBJECT", subject);
        }
        if (messageBody != null) {
            emailIntent.putExtra(IntentCompat.EXTRA_HTML_TEXT, messageBody);
            String htmlTagQuoteReplacement = "blockquoteblockquote";
            Spanned spannedMessageBody = Html.fromHtml("&nbsp;" + messageBody.replace(HtmlTagQuote, "blockquoteblockquote"), null, new TagHandler() {
                public void handleTag(boolean opening, String tag, Editable output, XMLReader xmlReader) {
                    Object obj = null;
                    if (!TextUtils.equals(tag, "blockquoteblockquote")) {
                        return;
                    }
                    if (opening) {
                        EmailHelper.handleP(output);
                        int len = output.length();
                        output.setSpan(new GreyQuoteSpanMarker(), len, len, 17);
                        return;
                    }
                    EmailHelper.handleP(output);
                    len = output.length();
                    Object[] objs = output.getSpans(0, output.length(), GreyQuoteSpanMarker.class);
                    if (objs.length != 0) {
                        obj = objs[objs.length - 1];
                    }
                    int where = output.getSpanStart(obj);
                    output.removeSpan(obj);
                    if (where != len) {
                        output.setSpan(new GreyQuoteSpan(), where, len, 33);
                    }
                }
            });
            if (StringUtils.isTrimmedAndEmpty(quotedMessageBodyHeader)) {
                emailIntent.putExtra("android.intent.extra.TEXT", spannedMessageBody);
            } else {
                SpannableStringBuilder spannedMessageBodyWithHeader = new SpannableStringBuilder(spannedMessageBody);
                spannedMessageBodyWithHeader.insert(0, "\n" + quotedMessageBodyHeader + "\n");
                emailIntent.putExtra("android.intent.extra.TEXT", spannedMessageBodyWithHeader);
            }
        }
        try {
            context.startActivity(Intent.createChooser(emailIntent, context.getString(R.string.title_sendmail_chooser_dialog)));
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }

    public static String decorateDefineBlockQuoteCSS(String email) {
        if (email == null) {
            return null;
        }
        return TextUtils.concat(new CharSequence[]{"<style>blockquote{ BORDER-LEFT:#cccccc 1px solid; MARGIN:1em 0px 0px 0px; PADDING-LEFT:1em; } </style>", email}).toString();
    }

    public static String decorateQuoteEmail(String emailToQuote) {
        if (emailToQuote == null) {
            return null;
        }
        return TextUtils.concat(new CharSequence[]{"<", HtmlTagQuote, ">", emailToQuote, "</", HtmlTagQuote, ">"}).toString();
    }

    private static String getQuotedMessageBodyHeaderReply(Context context, EmailMessage emailMessage) {
        Session session = PipedriveApp.getActiveSession();
        if (context == null || emailMessage == null || session == null) {
            return null;
        }
        StringBuilder headerBuilder = new StringBuilder();
        headerBuilder.append(TextUtils.concat(new CharSequence[]{LocaleHelper.getLocaleBasedDateTimeStringInCurrentTimeZone(session, emailMessage.getMessageTime()), Table.COMMA_SEP}));
        if (emailMessage.getFrom() != null) {
            headerBuilder.append(getQuotedMessageBodyHeaderEmails(emailMessage.getFrom()));
        }
        return context.getString(R.string.lbl_email_forward_header_reply, new Object[]{headerBuilder.toString()});
    }

    private static String getQuotedMessageBodyHeaderForward(Context context, EmailMessage emailMessage) {
        Session session = PipedriveApp.getActiveSession();
        if (context == null || emailMessage == null || session == null) {
            return null;
        }
        String cc;
        String from = emailMessage.getFrom() == null ? "" : getQuotedMessageBodyHeaderEmails(emailMessage.getFrom());
        String date = emailMessage.getMessageTime() == null ? "" : LocaleHelper.getLocaleBasedDateTimeStringInCurrentTimeZone(session, emailMessage.getMessageTime());
        String subject = emailMessage.getSubject() == null ? "" : emailMessage.getSubject();
        String to = emailMessage.getTo() == null ? "" : getQuotedMessageBodyHeaderEmails(emailMessage.getTo());
        if (emailMessage.getCc() == null) {
            cc = "";
        } else {
            cc = getQuotedMessageBodyHeaderEmails(emailMessage.getCc());
        }
        return context.getString(R.string.lbl_email_forward_header_forward, new Object[]{from, date, subject, to, cc});
    }

    private static String getQuotedMessageBodyHeaderEmails(List<EmailParticipant> emailParticipants) {
        if (emailParticipants == null) {
            return "";
        }
        StringBuilder emailParticipantsStrinBuilder = new StringBuilder();
        for (EmailParticipant emailParticipant : emailParticipants) {
            if (emailParticipant.getName() != null) {
                emailParticipantsStrinBuilder.append(TextUtils.concat(new CharSequence[]{" ", "\"", emailParticipant.getName(), "\""}));
            }
            if (emailParticipant.getEmail() != null) {
                emailParticipantsStrinBuilder.append(TextUtils.concat(new CharSequence[]{" ", "<", emailParticipant.getEmail(), ">"}));
            }
        }
        return emailParticipantsStrinBuilder.toString();
    }

    private static String[] emailParticipantListToStringArray(@Nullable List<EmailParticipant> emailParticipants) {
        if (emailParticipants == null) {
            return new String[0];
        }
        String[] emailParticipantsArray = new String[emailParticipants.size()];
        for (int i = 0; i < emailParticipantsArray.length; i++) {
            emailParticipantsArray[i] = ((EmailParticipant) emailParticipants.get(i)).getEmail();
        }
        return emailParticipantsArray;
    }

    private static String decorateSubjectForward(Context context, String baseSubject) {
        if (context == null) {
            return "";
        }
        Object[] objArr = new Object[1];
        if (baseSubject == null) {
            baseSubject = "";
        }
        objArr[0] = baseSubject;
        return context.getString(R.string.lbl_email_subject_forward, objArr);
    }

    private static String decorateSubjectReply(Context context, String baseSubject) {
        if (context == null) {
            return "";
        }
        Object[] objArr = new Object[1];
        if (baseSubject == null) {
            baseSubject = "";
        }
        objArr[0] = baseSubject;
        return context.getString(R.string.lbl_email_subject_reply, objArr);
    }

    private static void handleP(Editable text) {
        int len = text.length();
        if (len < 1 || text.charAt(len - 1) != '\n') {
            if (len != 0) {
                text.append("\n\n");
            }
        } else if (len < 2 || text.charAt(len - 2) != '\n') {
            text.append("\n");
        }
    }

    public static void onActivityResult(int requestCode) {
        if (requestCode == 100) {
            Observable.timer(5, TimeUnit.SECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Long>() {
                public void call(Long aLong) {
                    Session session = PipedriveApp.getActiveSession();
                    if (session != null) {
                        RecentsSyncAdapter.requestSyncAdapterToSync(session);
                    }
                }
            });
        }
    }
}
