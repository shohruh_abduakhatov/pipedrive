package com.pipedrive.util;

import android.animation.LayoutTransition;
import android.content.Context;
import android.os.Build.VERSION;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.pipedrive.R;

public class FullCreationFormExpansionWithTransition {
    private Context mContext;
    private ViewGroup mSceneRoot;

    public FullCreationFormExpansionWithTransition(Context context, ViewGroup sceneRoot) {
        this.mContext = context;
        this.mSceneRoot = sceneRoot;
    }

    public void startTransition(LinearLayout containerWithGravity, View viewToHide, View viewToShow) {
        if (VERSION.SDK_INT >= 19) {
            TransitionManager.beginDelayedTransition(this.mSceneRoot, (TransitionSet) TransitionInflater.from(this.mContext).inflateTransition(R.transition.minimal_form_transition_set));
        } else {
            this.mSceneRoot.setLayoutTransition(new LayoutTransition());
        }
        modifyLayout(containerWithGravity, viewToHide, viewToShow);
    }

    private void modifyLayout(LinearLayout containerWithGravity, View viewToHide, View viewToShow) {
        containerWithGravity.setGravity(0);
        viewToHide.setVisibility(8);
        viewToShow.setVisibility(0);
    }
}
