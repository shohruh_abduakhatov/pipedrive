package com.pipedrive.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.newrelic.agent.android.instrumentation.SQLiteInstrumentation;
import com.pipedrive.model.BaseDatasourceEntity;
import com.pipedrive.model.Deal;
import com.pipedrive.model.FlowFile;
import com.pipedrive.model.Organization;
import com.pipedrive.model.Person;
import com.pipedrive.util.CursorHelper;
import java.util.List;

public class FlowFilesDataSource extends BaseDataSource<FlowFile> {
    private DealsDataSource mDealsDataSource;
    private OrganizationsDataSource mOrganizationsDataSource;
    private PersonsDataSource mPersonsDataSource;

    public FlowFilesDataSource(SQLiteDatabase dbconn) {
        super(dbconn);
    }

    private DealsDataSource getDealsDataSource() {
        if (this.mDealsDataSource != null) {
            return this.mDealsDataSource;
        }
        DealsDataSource dealsDataSource = new DealsDataSource(getTransactionalDBConnection());
        this.mDealsDataSource = dealsDataSource;
        return dealsDataSource;
    }

    private PersonsDataSource getPersonsDataSource() {
        if (this.mPersonsDataSource != null) {
            return this.mPersonsDataSource;
        }
        PersonsDataSource personsDataSource = new PersonsDataSource(getTransactionalDBConnection());
        this.mPersonsDataSource = personsDataSource;
        return personsDataSource;
    }

    private OrganizationsDataSource getOrganizationsDataSource() {
        if (this.mOrganizationsDataSource != null) {
            return this.mOrganizationsDataSource;
        }
        OrganizationsDataSource organizationsDataSource = new OrganizationsDataSource(getTransactionalDBConnection());
        this.mOrganizationsDataSource = organizationsDataSource;
        return organizationsDataSource;
    }

    @NonNull
    protected String getColumnNameForPipedriveId() {
        return PipeSQLiteHelper.COLUMN_FILES_PIPEDRIVE_ID;
    }

    @NonNull
    protected String getColumnNameForSqlId() {
        return PipeSQLiteHelper.COLUMN_ID;
    }

    @NonNull
    protected String getTableName() {
        return "files";
    }

    @NonNull
    protected ContentValues getContentValues(@NonNull FlowFile flowFile) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_PIPEDRIVE_ID, Integer.valueOf(flowFile.getPipedriveId()));
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_USER_ID, flowFile.getUserId());
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_PRODUCT_ID, flowFile.getProductId());
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_PRODUCT_NAME, flowFile.getProductName());
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_EMAIL_MESSAGE_ID, flowFile.getEmailMessageId());
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_ACTIVITY_ID, flowFile.getActivityId());
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_NOTE_ID, flowFile.getNoteId());
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_LOG_ID, flowFile.getLogId());
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_FILE_NAME, flowFile.getFileName());
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_FILE_TYPE, flowFile.getFileType());
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_FILE_SIZE, flowFile.getFileSize());
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_ACTIVE, flowFile.isActive());
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_INLINE, flowFile.isInline());
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_COMMENT, flowFile.getComment());
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_REMOTE_LOCATION, flowFile.getRemoteLocation());
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_REMOTE_ID, flowFile.getRemoteId());
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_PEOPLE_NAME, flowFile.getPeopleName());
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_URL, flowFile.getUrl());
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_NAME, flowFile.getName());
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_ADD_TIME, Long.valueOf(flowFile.getAddTime().getRepresentationInUnixTime()));
        contentValues.put(PipeSQLiteHelper.COLUMN_FILES_UPDATE_TIME, Long.valueOf(flowFile.getUpdateTime().getRepresentationInUnixTime()));
        if (flowFile.getFileUploadStatus() != null) {
            contentValues.put(PipeSQLiteHelper.COLUMN_FILES_UPLOAD_STATUS, flowFile.getFileUploadStatus());
        } else {
            contentValues.putNull(PipeSQLiteHelper.COLUMN_FILES_UPLOAD_STATUS);
        }
        if (flowFile.getDeal() != null && flowFile.getDeal().isStored()) {
            contentValues.put(PipeSQLiteHelper.COLUMN_FILES_DEAL_ID, Long.valueOf(flowFile.getDeal().getSqlId()));
        }
        if (flowFile.getPerson() != null && flowFile.getPerson().isStored()) {
            contentValues.put(PipeSQLiteHelper.COLUMN_FILES_PERSON_ID, Long.valueOf(flowFile.getPerson().getSqlId()));
        }
        if (flowFile.getOrganization() != null && flowFile.getOrganization().isStored()) {
            contentValues.put(PipeSQLiteHelper.COLUMN_FILES_ORG_ID, Long.valueOf(flowFile.getOrganization().getSqlId()));
        }
        return contentValues;
    }

    public void createOrUpdate(List<FlowFile> files) {
        if (files != null && !files.isEmpty()) {
            beginTransactionNonExclusive();
            for (FlowFile file : files) {
                createOrUpdate((BaseDatasourceEntity) file);
            }
            commit();
        }
    }

    @NonNull
    public String[] getAllColumns() {
        return new String[]{PipeSQLiteHelper.COLUMN_ID, PipeSQLiteHelper.COLUMN_FILES_PIPEDRIVE_ID, PipeSQLiteHelper.COLUMN_FILES_USER_ID, PipeSQLiteHelper.COLUMN_FILES_DEAL_ID, PipeSQLiteHelper.COLUMN_FILES_PERSON_ID, PipeSQLiteHelper.COLUMN_FILES_ORG_ID, PipeSQLiteHelper.COLUMN_FILES_PRODUCT_ID, PipeSQLiteHelper.COLUMN_FILES_PRODUCT_NAME, PipeSQLiteHelper.COLUMN_FILES_EMAIL_MESSAGE_ID, PipeSQLiteHelper.COLUMN_FILES_ACTIVITY_ID, PipeSQLiteHelper.COLUMN_FILES_NOTE_ID, PipeSQLiteHelper.COLUMN_FILES_LOG_ID, PipeSQLiteHelper.COLUMN_FILES_ADD_TIME, PipeSQLiteHelper.COLUMN_FILES_UPDATE_TIME, PipeSQLiteHelper.COLUMN_FILES_FILE_NAME, PipeSQLiteHelper.COLUMN_FILES_FILE_TYPE, PipeSQLiteHelper.COLUMN_FILES_FILE_SIZE, PipeSQLiteHelper.COLUMN_FILES_ACTIVE, PipeSQLiteHelper.COLUMN_FILES_INLINE, PipeSQLiteHelper.COLUMN_FILES_COMMENT, PipeSQLiteHelper.COLUMN_FILES_REMOTE_LOCATION, PipeSQLiteHelper.COLUMN_FILES_REMOTE_ID, PipeSQLiteHelper.COLUMN_FILES_PEOPLE_NAME, PipeSQLiteHelper.COLUMN_FILES_URL, PipeSQLiteHelper.COLUMN_FILES_NAME, PipeSQLiteHelper.COLUMN_FILES_UPLOAD_STATUS};
    }

    @Nullable
    protected FlowFile deflateCursor(@NonNull Cursor cursor) {
        FlowFile flowFile = new FlowFile();
        flowFile.setSqlId(cursor.getLong(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_ID)));
        flowFile.setPipedriveId(cursor.getInt(cursor.getColumnIndex(PipeSQLiteHelper.COLUMN_FILES_PIPEDRIVE_ID)));
        flowFile.setActive(CursorHelper.getBoolean(cursor, PipeSQLiteHelper.COLUMN_FILES_ACTIVE));
        flowFile.setActivityId(CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_FILES_ACTIVITY_ID));
        flowFile.setComment(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_FILES_COMMENT));
        flowFile.setEmailMessageId(CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_FILES_EMAIL_MESSAGE_ID));
        flowFile.setFileName(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_FILES_FILE_NAME));
        flowFile.setFileSize(CursorHelper.getLong(cursor, PipeSQLiteHelper.COLUMN_FILES_FILE_SIZE));
        flowFile.setFileType(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_FILES_FILE_TYPE));
        flowFile.setInline(CursorHelper.getBoolean(cursor, PipeSQLiteHelper.COLUMN_FILES_INLINE));
        flowFile.setLogId(CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_FILES_LOG_ID));
        flowFile.setName(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_FILES_NAME));
        flowFile.setNoteId(CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_FILES_NOTE_ID));
        flowFile.setPeopleName(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_FILES_PEOPLE_NAME));
        flowFile.setProductId(CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_FILES_PRODUCT_ID));
        flowFile.setProductName(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_FILES_PRODUCT_NAME));
        flowFile.setRemoteId(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_FILES_REMOTE_ID));
        flowFile.setRemoteLocation(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_FILES_REMOTE_LOCATION));
        flowFile.setUrl(CursorHelper.getString(cursor, PipeSQLiteHelper.COLUMN_FILES_URL));
        flowFile.setUserId(CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_FILES_USER_ID));
        flowFile.setAddTime(CursorHelper.getPipedriveDateTimeFromColumnHeldInSeconds(cursor, PipeSQLiteHelper.COLUMN_FILES_ADD_TIME));
        flowFile.setUpdateTime(CursorHelper.getPipedriveDateTimeFromColumnHeldInSeconds(cursor, PipeSQLiteHelper.COLUMN_FILES_UPDATE_TIME));
        flowFile.setFileUploadStatus(CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_FILES_UPLOAD_STATUS));
        Integer dealSqlId = CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_FILES_DEAL_ID);
        if (dealSqlId != null) {
            flowFile.setDeal((Deal) getDealsDataSource().findBySqlId((long) dealSqlId.intValue()));
        }
        Integer personSqlId = CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_FILES_PERSON_ID);
        if (personSqlId != null) {
            flowFile.setPerson((Person) getPersonsDataSource().findBySqlId((long) personSqlId.intValue()));
        }
        Integer orgSqlId = CursorHelper.getInteger(cursor, PipeSQLiteHelper.COLUMN_FILES_ORG_ID);
        if (orgSqlId != null) {
            flowFile.setOrganization((Organization) getOrganizationsDataSource().findBySqlId((long) orgSqlId.intValue()));
        }
        return flowFile;
    }

    public boolean updateFlowFileUploadStatus(long flowFileSqlId, @Nullable Integer status) {
        boolean nonNullValidStatus;
        ContentValues values = new ContentValues();
        if (status != null) {
            nonNullValidStatus = true;
        } else {
            nonNullValidStatus = false;
        }
        if (nonNullValidStatus) {
            values.put(PipeSQLiteHelper.COLUMN_FILES_UPLOAD_STATUS, status);
        } else {
            values.putNull(PipeSQLiteHelper.COLUMN_FILES_UPLOAD_STATUS);
        }
        String whereClause = "_id == ?";
        String[] whereArgs = new String[]{Long.toString(flowFileSqlId)};
        SQLiteDatabase transactionalDBConnection = getTransactionalDBConnection();
        String tableName = getTableName();
        String str = "_id == ?";
        if ((!(transactionalDBConnection instanceof SQLiteDatabase) ? transactionalDBConnection.update(tableName, values, str, whereArgs) : SQLiteInstrumentation.update(transactionalDBConnection, tableName, values, str, whereArgs)) > 0) {
            return true;
        }
        return false;
    }
}
