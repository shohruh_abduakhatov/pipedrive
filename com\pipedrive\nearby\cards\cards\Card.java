package com.pipedrive.nearby.cards.cards;

import android.content.res.Resources;
import android.graphics.Rect;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindDimen;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;
import com.pipedrive.application.Session;
import com.pipedrive.nearby.cards.CardsViewEventBus;
import com.pipedrive.nearby.model.NearbyItem;
import com.pipedrive.nearby.util.Irrelevant;
import com.pipedrive.util.RxUtil;
import com.pipedrive.util.formatter.DistanceFormatter;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Actions;
import rx.subscriptions.CompositeSubscription;

public abstract class Card<NEARBY_ITEM extends NearbyItem> extends ViewHolder {
    private static final Rect CARD_CONTAINER_HIT_RECT = new Rect();
    private static final int[] SCREEN_LOCATION_ARRAY = new int[2];
    private static final int SCREEN_LOCATION_ARRAY_LENGTH = 2;
    @BindView(2131820553)
    TextView addressTextView;
    @BindDimen(2131493169)
    int bottomMargin;
    @BindView(2131820554)
    View cardContainer;
    @NonNull
    private final CompositeSubscription compositeSubscription = new CompositeSubscription();
    @BindView(2131820555)
    ViewGroup detailsContainer;
    @BindView(2131821168)
    FloatingActionButton directionsFloatingActionButton;
    @Nullable
    private Double distanceMeters;
    @BindDimen(2131493167)
    int fabContainerHeight;
    @BindView(2131820557)
    ViewGroup headerContainer;
    @BindDimen(2131493168)
    int maxCardHeight;
    @Nullable
    private NEARBY_ITEM nearbyItem;
    @Nullable
    Session session;
    @BindString(2131296862)
    String subtitleSeparator;

    Card(@NonNull ViewGroup parent, @LayoutRes @NonNull Integer layoutResId) {
        super(LayoutInflater.from(parent.getContext()).inflate(layoutResId.intValue(), parent, false));
        ButterKnife.bind((Object) this, this.itemView);
        this.cardContainer.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (((Boolean) RxUtil.blockingFirst(CardEventBus.INSTANCE.isExpanded())).booleanValue()) {
                    CardEventBus.INSTANCE.collapseCards();
                } else {
                    CardEventBus.INSTANCE.expandCards();
                }
            }
        });
    }

    public boolean shouldIgnoreTouchDownEvent(MotionEvent event) {
        this.cardContainer.getHitRect(CARD_CONTAINER_HIT_RECT);
        return !CARD_CONTAINER_HIT_RECT.contains((int) event.getX(), (int) event.getY());
    }

    private void measureCardHeightAndSendEvent() {
        if (!cardIsNotFullyVisibleOnScreen()) {
            this.cardContainer.measure(MeasureSpec.makeMeasureSpec(this.cardContainer.getWidth(), Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(this.maxCardHeight, Integer.MIN_VALUE));
            emitCardHeightEvent((this.cardContainer.getMeasuredHeight() - this.fabContainerHeight) + this.bottomMargin);
        }
    }

    private void emitCardHeightEvent(int cardHeight) {
        CardEventBus.INSTANCE.onCardsHeightChanged(Integer.valueOf(cardHeight));
    }

    private boolean cardIsNotFullyVisibleOnScreen() {
        this.cardContainer.getLocationOnScreen(SCREEN_LOCATION_ARRAY);
        int xPosition = SCREEN_LOCATION_ARRAY[0];
        if (xPosition <= 0 || ((double) xPosition) > ((double) this.cardContainer.getWidth()) * 0.3333333333333333d) {
            return true;
        }
        return false;
    }

    @CallSuper
    void expand(@NonNull Boolean animate) {
        this.detailsContainer.setVisibility(0);
        hideDirectionsFab(animate);
    }

    @CallSuper
    void collapse(@NonNull Boolean animate) {
        this.detailsContainer.setVisibility(8);
        showDirectionsFab(animate);
    }

    @NonNull
    Resources getResources() {
        return this.itemView.getContext().getResources();
    }

    @CallSuper
    public void bind(@NonNull Session session, @NonNull NEARBY_ITEM nearbyItem, @NonNull LatLng currentLocation) {
        this.session = session;
        this.nearbyItem = nearbyItem;
        addCardsViewSubscription();
        this.addressTextView.setText(nearbyItem.getAddress());
        this.distanceMeters = Double.valueOf(SphericalUtil.computeDistanceBetween(currentLocation, nearbyItem.getItemLatLng()));
        if (((Boolean) RxUtil.blockingFirst(CardEventBus.INSTANCE.isExpanded())).booleanValue()) {
            expand(Boolean.valueOf(false));
        } else {
            collapse(Boolean.valueOf(false));
        }
        if (!CardsViewEventBus.INSTANCE.isVisible().booleanValue()) {
            hideDirectionsFab(Boolean.valueOf(false));
        }
    }

    private void addCardsViewSubscription() {
        addSubscription(CardEventBus.INSTANCE.cardSelectedEvents().observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Irrelevant>() {
            public void call(Irrelevant irrelevant) {
                Card.this.measureCardHeightAndSendEvent();
            }
        }));
        addSubscription(CardsViewEventBus.INSTANCE.showEvents().observeOn(AndroidSchedulers.mainThread()).doOnNext(new Action1<Irrelevant>() {
            public void call(Irrelevant irrelevant) {
                Card.this.measureCardHeightAndSendEvent();
            }
        }).subscribe(new Action1<Irrelevant>() {
            public void call(Irrelevant irrelevant) {
                if (!((Boolean) RxUtil.blockingFirst(CardEventBus.INSTANCE.isExpanded())).booleanValue()) {
                    Card.this.showDirectionsFab(Boolean.valueOf(true));
                }
            }
        }, getDefaultOnErrorActionForCards()));
        addSubscription(CardsViewEventBus.INSTANCE.hideEvents().observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Irrelevant>() {
            public void call(Irrelevant irrelevant) {
                Card.this.hideDirectionsFab(Boolean.valueOf(true));
            }
        }, getDefaultOnErrorActionForCards()));
        addSubscription(CardEventBus.INSTANCE.collapseEvents().observeOn(AndroidSchedulers.mainThread()).doOnNext(new Action1<Irrelevant>() {
            public void call(Irrelevant irrelevant) {
                Card.this.collapse(Boolean.valueOf(true));
            }
        }).doOnNext(new Action1<Irrelevant>() {
            public void call(Irrelevant irrelevant) {
                Card.this.measureCardHeightAndSendEvent();
            }
        }).subscribe(Actions.empty(), getDefaultOnErrorActionForCards()));
        addSubscription(CardEventBus.INSTANCE.expandEvents().observeOn(AndroidSchedulers.mainThread()).doOnNext(new Action1<Irrelevant>() {
            public void call(Irrelevant irrelevant) {
                Card.this.expand(Boolean.valueOf(true));
            }
        }).doOnNext(new Action1<Irrelevant>() {
            public void call(Irrelevant irrelevant) {
                Card.this.measureCardHeightAndSendEvent();
            }
        }).subscribe(Actions.empty(), getDefaultOnErrorActionForCards()));
    }

    @OnClick({2131821168})
    void onDirectionsFabClicked() {
        if (this.nearbyItem != null) {
            CardEventBus.INSTANCE.onDirectionsFabClicked(this.nearbyItem);
        }
    }

    private void showDirectionsFab(Boolean animate) {
        if (animate.booleanValue()) {
            this.directionsFloatingActionButton.setVisibility(4);
            this.directionsFloatingActionButton.show();
            return;
        }
        this.directionsFloatingActionButton.setVisibility(0);
    }

    private void hideDirectionsFab(Boolean animate) {
        if (animate.booleanValue()) {
            this.directionsFloatingActionButton.hide();
        } else {
            this.directionsFloatingActionButton.setVisibility(4);
        }
    }

    @OnClick({2131820556})
    void onDirectionsButtonClicked() {
        if (this.nearbyItem != null) {
            CardEventBus.INSTANCE.onDirectionsButtonClicked(this.nearbyItem);
        }
    }

    void addSubscription(@NonNull Subscription subscription) {
        this.compositeSubscription.add(subscription);
    }

    @NonNull
    Action1<Throwable> getDefaultOnErrorActionForCards() {
        return new Action1<Throwable>() {
            public void call(Throwable throwable) {
            }
        };
    }

    public void unbind() {
        this.compositeSubscription.clear();
    }

    @NonNull
    String getDistanceString() {
        return DistanceFormatter.formatDistance(this.distanceMeters);
    }
}
