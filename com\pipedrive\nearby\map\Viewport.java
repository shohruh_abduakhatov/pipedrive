package com.pipedrive.nearby.map;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.newrelic.agent.android.instrumentation.GsonInstrumentation;

public final class Viewport {
    @NonNull
    private final LatLngBounds latLngBounds;
    @NonNull
    private final Double zoom;

    private Viewport(@NonNull LatLngBounds latLngBounds, @NonNull Double zoom) {
        this.latLngBounds = latLngBounds;
        this.zoom = zoom;
    }

    @NonNull
    static Viewport createFrom(@NonNull GoogleMap googleMap) {
        return new Viewport(googleMap.getProjection().getVisibleRegion().latLngBounds, Double.valueOf((double) googleMap.getCameraPosition().zoom));
    }

    @Nullable
    public static Viewport fromJsonString(@Nullable String jsonString) {
        if (jsonString == null) {
            return null;
        }
        Gson gson = new GsonBuilder().create();
        Class cls = Viewport.class;
        return (Viewport) (!(gson instanceof Gson) ? gson.fromJson(jsonString, cls) : GsonInstrumentation.fromJson(gson, jsonString, cls));
    }

    @NonNull
    public LatLngBounds getLatLngBounds() {
        return this.latLngBounds;
    }

    @NonNull
    public Double getZoom() {
        return this.zoom;
    }

    @NonNull
    public String toJsonString() {
        Gson gson = new GsonBuilder().create();
        return !(gson instanceof Gson) ? gson.toJson((Object) this) : GsonInstrumentation.toJson(gson, (Object) this);
    }
}
