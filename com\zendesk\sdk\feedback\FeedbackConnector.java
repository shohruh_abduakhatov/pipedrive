package com.zendesk.sdk.feedback;

import android.support.annotation.Nullable;
import com.zendesk.sdk.model.request.CreateRequest;
import com.zendesk.service.ZendeskCallback;
import java.io.Serializable;
import java.util.List;

public interface FeedbackConnector extends Serializable {
    boolean isValid();

    void sendFeedback(String str, @Nullable List<String> list, ZendeskCallback<CreateRequest> zendeskCallback);
}
